﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using UDI.Map;
using UDI.Map.DTO;
using UDI.Map.DTO.XML;
using UDI.Utility.Serialization;

namespace Tester
{
	class Program
	{
		static void Main(string[] args)
		{
			var mapservice = new MapService();
			var error = string.Empty;

			#region Address
			//var address = "Service Rd 49 Los Angeles 90045";
			////var address = "5110 E BELLEVUE ST Tuscon 85712 AZ";
			////var address = "51 Ave and Baseline Rd pheonix az";
			////var address = "225 Lisgar Street, Ottawa ON K2P 0C6";
			////var address = "5110 E BELLEVUE ST Tuscon";
			////var geoLocation = new UDI.Map.DTO.Geolocation { Latitude = 40.0755M, Longitude = -76.329999M };
			//var result1 = mapservice.GetAddresses(address, out error);
			#endregion

			#region Old Google
			//var geoGoogle = new UDI.Utility.Geolocation.GoogleMapsApiWrapper(System.Configuration.ConfigurationManager.AppSettings["Google_API_Key"]);
			//List<UDI.Utility.Geolocation.ApiPlacemark> geoResult = geoGoogle.Search(address, null).Placemark.ToList(); 
			#endregion

			#region MapQuest
			//var geoMain = new UDI.Map.MapService();
			//var error = string.Empty;
			//var reslut1 = geoMain.GetAddresses(address, out error);
			//var reslut2 = geoMain.GetAddressesFromGeoLocation(geoLocation, out error);
			//var reslut3 = geoMain.GetDistance("1122 e elk ave apt 125 glendale ca", "5110 E BELLEVUE ST Tuscon 85712 AZ", UDI.Map.DTO.DistanceUnit.Mile, out error);
			#endregion

			#region Zone
			//var gpx1 = new gpx { rteList = new List<rte>() };
			//var rte1 = new rte();
			//rte1.
			//gpx1.rteList.Add(new rtept {lat = "12", lon = "134" });


			//var mapservice = new MapService();
			//var xZone = XDocument.Load(@"D:\UDI\Source\Trunk\UDI.Map\Tester\Denver_Tech_Center.gpx");
			//var zone = xZone.XmlDeserialize<gpx>();
			//39.609805, -104.889139
			//39.607424, -104.911756
			//var zones = new List<string>();
			//zones.Add(@"D:\UDI\Source\Trunk\UDI.Map\Tester\DenverZones.gpx");
			//var result = mapservice.FindZone(new Geolocation { Latitude = 39.609805m, Longitude = -104.889139m }, zones);
			//var result = mapservice.FindZone(new Geolocation { Latitude = 39.607424m, Longitude = -104.911756m }, zones);

			#endregion

			#region GetAdressFromGeoLocation
			var geolocation = new Geolocation { Latitude = 39.107909M, Longitude = -94.576929M };
			//var geolocation = new Geolocation { Latitude = 34.145842M, Longitude = -118.145409M };
			//var geolocation = new Geolocation { Latitude = 39.307046M, Longitude = -94.730761M };
			//var geolocation = new Geolocation { Latitude = 39.260068M, Longitude = -94.587810M };
			//, 39.260068, -94.587810
			var result2 = mapservice.GetAddressesFromGeoLocationWithProximity(geolocation, out error);

			#endregion

			Console.WriteLine("DONE !");
			Console.ReadLine();
		}
	}
}
