﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.Map.DTO;

namespace UDI.Map.Adapter
{
	internal abstract class BaseMapAdapter
	{
		internal abstract List<DTO.Address> GetAddresses(string address, out string error);
		internal abstract List<DTO.Address> GetAddressesFromGeoLocation(DTO.Geolocation geoLocation, out string error);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="from">latlong as "40.07546,-78.329999" or location as "Clarendon Blvd, Arlington, VA"</param>
		/// <param name="to">latlong as "40.07546,-78.329999" or location as "Clarendon Blvd, Arlington, VA"</param>
		/// <param name="distanceUnit"></param>
		/// <param name="error"></param>
		/// <returns></returns>
		internal abstract decimal GetDistance(string from, string to, DTO.DistanceUnit distanceUnit, out string error);

		internal abstract ETA GetETA(Geolocation from, Geolocation to, DTO.DistanceUnit distanceUnit, out string error);

        internal abstract List<Geolocation> CalculateVehicleMovements(Geolocation from, Geolocation to, out string error);

	}
}
