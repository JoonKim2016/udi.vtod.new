﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UDI.Map.Adapter.Google.DTO
{
	public class distancematrix
	{
		public List<string> destination_addresses { get; set; }
		public List<string> origin_addresses { get; set; }
		public List<row> rows { get; set; }
		public string status { get; set; }
	}

}
