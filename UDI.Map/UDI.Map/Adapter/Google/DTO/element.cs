﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UDI.Map.Adapter.Google.DTO
{
	public class element
	{
		public distance distance { get; set; }
		public duration duration { get; set; }
		public duration_in_traffic duration_in_traffic { get; set; }
		public string status { get; set; }
	}
}
