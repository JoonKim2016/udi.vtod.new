﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UDI.Map.Adapter.Google.DTO;
using UDI.Map.DTO;
using UDI.Utility.Serialization;
using UDI.Utility.Helper;

namespace UDI.Map.Adapter.Google
{
	internal class GoogleAdapter : BaseMapAdapter
	{
		internal override List<Address> GetAddresses(string address, out string error)
		{
			throw new NotImplementedException();
		}

		internal override List<Address> GetAddressesFromGeoLocation(Geolocation geoLocation, out string error)
		{
			throw new NotImplementedException();
		}

		internal override decimal GetDistance(string from, string to, DistanceUnit distanceUnit, out string error)
		{
			throw new NotImplementedException();
		}

		internal override ETA GetETA(Map.DTO.Geolocation from, Map.DTO.Geolocation to, DistanceUnit distanceUnit, out string error)
		{
			ETA result = null;
			error = string.Empty;
			var status = string.Empty;

			//
			try
			{
				int departureTime = (int)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds;

				var googleClient = System.Configuration.ConfigurationManager.AppSettings["Google_Client"];
				var googleKey = System.Configuration.ConfigurationManager.AppSettings["Google_Key"];
				var URL = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" + from.Latitude + "," + from.Longitude + "&destinations=" + to.Latitude + "," + to.Longitude + "&mode=driving&departure_time=" + departureTime + "&language=US&client=" + googleClient;

				var signedURL = UDI.Map.Adapter.Google.Helper.Helper.Sign(URL, googleKey);

				var googleJsonResult = UDI.Utility.Helper.WebRequestHelper.ProcessWebRequest(signedURL, "", "application/json; charset=utf-8", "GET", out status);

				var googleResult = googleJsonResult.JsonDeserialize<distancematrix>();

				if (googleResult != null && googleResult.rows != null && googleResult.rows.Any())
				{
					var row = googleResult.rows.FirstOrDefault();

					if (row.elements != null && row.elements.Any())
					{
						var element = row.elements.FirstOrDefault();

						result = new ETA();

						//if (element.distance != null)
						//{
						//	result.Distance = (element.distance.value.ToDecimal() / 1000).ConvertDistance(distanceUnit, distanceUnit);
						//}

						if (element.duration != null)
						{
							result.DurationInSecond = element.duration.value;
						}
						if (element.duration_in_traffic != null)
						{
							result.DurationInTrafficInSecond = element.duration_in_traffic.value;
						}

					}



				}

				//result.Distance = googleResult.

			}
			catch (Exception ex)
			{

				error = ex.Message;
			}


			return result;
		}

        internal override List<Geolocation> CalculateVehicleMovements(Geolocation p1,Geolocation p2,out string error)
        {
            error = string.Empty;
            var status = string.Empty;
            List<Geolocation> lst = null;
            try
			    {
                    #region "Adding Directions in Path"
                    var googleKey = System.Configuration.ConfigurationManager.AppSettings["Google_Key"];
                    var googleClient = System.Configuration.ConfigurationManager.AppSettings["Google_Client"];
                    var movementFractionToBeAdded = System.Configuration.ConfigurationManager.AppSettings["Movement_Fraction"];
                    string apiUrl = "https://maps.googleapis.com/maps/api/directions/json?origin=" + p1.Latitude + "," + p1.Longitude + "&destination= " + (p2.Latitude + movementFractionToBeAdded.ToDecimal()) + "," + (p2.Longitude + movementFractionToBeAdded.ToDecimal()) + "&client=" + googleClient;
                    var signedURL = UDI.Map.Adapter.Google.Helper.Helper.Sign(apiUrl, googleKey);
                    var googleJsonResult = UDI.Utility.Helper.WebRequestHelper.ProcessWebRequest(signedURL, "", "application/json; charset=utf-8", "GET", out status);
                    var googleResult = googleJsonResult.JsonDeserialize<MapRootObject>();
                    if (googleResult.routes.Any())
                    {
                        lst = UDI.Map.Adapter.Google.Helper.Helper.DecodePolylinePoints(googleResult.routes.FirstOrDefault().overview_polyline.points);
                        
                    }
                    //if (lst!=null)
                    //{
                    //    //if (lst.Any())
                    //    //{
                    //    //    lst.RemoveAt(0);
                    //    //}
                    //}
                    #endregion
            }
            catch (Exception ex)
			{

				error = ex.Message;
			}
            return lst;
         }

	}
}
