﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using UDI.Map.DTO;
namespace UDI.Map.Adapter.Google.Helper
{
	public static class Helper
	{
		public static string Sign(string url, string keyString)
		{
			ASCIIEncoding encoding = new ASCIIEncoding();

			// converting key to bytes will throw an exception, need to replace '-' and '_' characters first.
			string usablePrivateKey = keyString.Replace("-", "+").Replace("_", "/");
			byte[] privateKeyBytes = Convert.FromBase64String(usablePrivateKey);

			Uri uri = new Uri(url);
			byte[] encodedPathAndQueryBytes = encoding.GetBytes(uri.LocalPath + uri.Query);

			// compute the hash
			HMACSHA1 algorithm = new HMACSHA1(privateKeyBytes);
			byte[] hash = algorithm.ComputeHash(encodedPathAndQueryBytes);

			// convert the bytes to string and make url-safe by replacing '+' and '/' characters
			string signature = Convert.ToBase64String(hash).Replace("+", "-").Replace("/", "_");

			// Add the signature to the existing URI.
			return uri.Scheme + "://" + uri.Host + uri.LocalPath + uri.Query + "&signature=" + signature;
		}

        public static List<Geolocation> DecodePolylinePoints(string encodedPoints)
        {
            if (encodedPoints == null || encodedPoints == "") return null;
            List<Geolocation> poly = new List<Geolocation>();
            char[] polylinechars = encodedPoints.ToCharArray();
            int index = 0;

            int currentLat = 0;
            int currentLng = 0;
            int next5bits;
            int sum;
            int shifter;
            int k = 0;
            try
            {
                while (index < polylinechars.Length)
                {
                    sum = 0;
                    shifter = 0;
                    do
                    {
                        next5bits = (int)polylinechars[index++] - 63;
                        sum |= (next5bits & 31) << shifter;
                        shifter += 5;
                    } while (next5bits >= 32 && index < polylinechars.Length);

                    if (index >= polylinechars.Length)
                        break;

                    currentLat += (sum & 1) == 1 ? ~(sum >> 1) : (sum >> 1);
                    sum = 0;
                    shifter = 0;
                    do
                    {
                        next5bits = (int)polylinechars[index++] - 63;
                        sum |= (next5bits & 31) << shifter;
                        shifter += 5;
                    } while (next5bits >= 32 && index < polylinechars.Length);

                    if (index >= polylinechars.Length && next5bits >= 32)
                        break;

                    currentLng += (sum & 1) == 1 ? ~(sum >> 1) : (sum >> 1);
                    Geolocation p = new Geolocation();
                    p.Latitude = Convert.ToDecimal((Convert.ToDouble(currentLat) / 100000.0));
                    p.Longitude = Convert.ToDecimal((Convert.ToDouble(currentLng) / 100000.0));
                    if (k > 0)
                        p.Coarse = Convert.ToDecimal(AngleBetweenThePoints(poly[k - 1].Latitude, poly[k - 1].Longitude, p.Latitude, p.Longitude));
                    else
                        p.Coarse = 0;
                    poly.Add(p);
                    k = k + 1;
                }
            }
            catch (Exception ex)
            {
            }
            return poly;
        }

        public static double AngleBetweenThePoints(decimal startLat, decimal startLon, decimal endLat, decimal endLon)
        {
            double fromLat = ConvertToRadians(decimal.ToDouble(startLat));
            double fromLng = ConvertToRadians(decimal.ToDouble(startLon));
            double toLat = ConvertToRadians(decimal.ToDouble(endLat));
            double toLng = ConvertToRadians(decimal.ToDouble(endLon));
            double dLng = toLng - fromLng;
            double heading = Math.Atan2(
                  Math.Sin(dLng) * Math.Cos(toLat),
                  Math.Cos(fromLat) * Math.Sin(toLat) - Math.Sin(fromLat) * Math.Cos(toLat) * Math.Cos(dLng));
            return wrap(RadianToDegree(heading), -180, 180);
        }

        public static double wrap(double n, double min, double max)
        {
            return (n >= min && n < max) ? n : ((n - min) % (max - min) + min);
        }

        private static double RadianToDegree(double angle)
        {
            return angle * (180.0 / Math.PI);
        }

        public static double ConvertToRadians(double angle)
        {
            return (Math.PI / 180) * angle;
        }
	}
}
