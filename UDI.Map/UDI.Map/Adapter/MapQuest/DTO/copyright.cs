﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UDI.Map.Adapter.MapQuest.DTO
{
	public class copyright
	{
		public string text { get; set; }
		public string imageUrl { get; set; }
		public string imageAltText { get; set; }
	}
}
