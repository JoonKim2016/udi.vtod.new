﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UDI.Map.Adapter.MapQuest.DTO
{
	public class info
	{
		public List<string> messages { get; set; }
		public int? statuscode { get; set; }
		public copyright copyright { get; set; }
	}
}
