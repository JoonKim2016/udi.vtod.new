﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UDI.Map.Adapter.MapQuest.DTO
{
	public class latLng
	{
		public decimal? lng { get; set; }
		public decimal? lat { get; set; }
	}
}
