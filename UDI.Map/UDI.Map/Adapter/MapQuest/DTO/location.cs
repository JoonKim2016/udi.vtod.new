﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UDI.Map.Adapter.MapQuest.DTO
{
	public class location
	{
		//County name
		public string adminArea4 { get; set; }
		//Type of location. 's' = stop (default) 'v' = via
		public string adminArea5Type { get; set; }
		//Type of location. 's' = stop (default) 'v' = via
		public string adminArea4Type { get; set; }
		//City name
		public string adminArea5 { get; set; }
		//Street address
		public string street { get; set; }
		//Country name
		public string adminArea1 { get; set; }
		//State name
		public string adminArea3 { get; set; }
		//Type of location. 's' = stop (default) 'v' = via
		public string type { get; set; }
		//Identifies the closest road to the address for routing purposes.
		public string linkId { get; set; }
		//Postal code
		public string postalCode { get; set; }
		//Specifies the side of street. 'L' = left 'R' = right 'N' = none (default)
		public string sideOfStreet { get; set; }
		public string dragPoint { get; set; }
		//Type of location. 's' = stop (default) 'v' = via
		public string adminArea1Type { get; set; }
		//The precision of the geocoded location. Refer to the Geocode Quality reference page for more information.
		public string geocodeQuality { get; set; }
		//The five character quality code for the precision of the geocoded location. Refer to the Geocode Quality reference page for more information.
		public string geocodeQualityCode { get; set; }
		//Type of location. 's' = stop (default) 'v' = via
		public string adminArea3Type { get; set; }
		//Returns the latitude/longitude for routing and is the nearest point on a road for the entrance.
		public latLng latLng { get; set; }
		//A lat/lng pair that can be helpful when showing this address as a Point of Interest.
		public displayLatLng displayLatLng { get; set; }
	}
}
