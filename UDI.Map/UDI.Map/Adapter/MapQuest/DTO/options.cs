﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UDI.Map.Adapter.MapQuest.DTO
{
	public class options
	{
		public bool? thumbMaps { get; set; }
		public bool? ignoreLatLngInput { get; set; }
		public int? maxResults { get; set; }
		public string unit { get; set; }
	}
}
