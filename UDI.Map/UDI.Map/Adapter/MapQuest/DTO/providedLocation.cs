﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UDI.Map.Adapter.MapQuest.DTO
{
	public class providedLocation
	{
		public int? id { get; set; }
		public string location { get; set; }
		public string name { get; set; }
		public string street { get; set; }
	}
}
