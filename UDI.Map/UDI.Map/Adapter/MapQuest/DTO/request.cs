﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UDI.Map.Adapter.MapQuest.DTO
{
	internal class request
	{
		public List<string> locations { get; set; }
		public location location { get; set; }
		public options options { get; set; }
	}
}
