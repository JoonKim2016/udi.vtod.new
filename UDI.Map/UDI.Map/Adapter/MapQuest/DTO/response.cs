﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UDI.Map.Adapter.MapQuest.DTO
{
	public class response
	{
		public List<result> results { get; set; }
		public options options { get; set; }
		public info info { get; set; }
		public route route { get; set; }
	}
}
