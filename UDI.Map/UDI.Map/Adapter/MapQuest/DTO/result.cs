﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UDI.Map.Adapter.MapQuest.DTO
{
	public class result
	{
		public providedLocation providedLocation { get; set; }
		public List<location> locations { get; set; }
	}
}
