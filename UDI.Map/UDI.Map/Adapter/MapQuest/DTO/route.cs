﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UDI.Map.Adapter.MapQuest.DTO
{
	public class route
	{
		public decimal distance { get; set; }
		public decimal time { get; set; }
	}
}
