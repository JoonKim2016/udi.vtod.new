﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UDI.Map.Adapter.MapQuest.DTO;
using UDI.Map.DTO;

namespace UDI.Map.Adapter.MapQuest.Helper
{
	internal static class Converter
	{
		internal static List<Address> ToAddresses(this response input)
		{
			List<Address> result = null;

			if (input != null)
			{
				result = new List<Address>();
				if (input.results != null && input.results.Any())
				{
					foreach (var itemResult in input.results)
					{
						if (itemResult.locations != null && itemResult.locations.Any())
						{
							foreach (var itemLocation in itemResult.locations)
							{
								#region Make Address
								var item = new Address
														{
															Country = itemLocation.adminArea1,
															State = itemLocation.adminArea3,
															County = itemLocation.adminArea4,
															City = itemLocation.adminArea5,
															FullStreet = itemLocation.street,
															ZipCode = itemLocation.postalCode,
															GeocodeQuality = itemLocation.geocodeQuality,
															geocodeQualityCode = itemLocation.geocodeQualityCode,
															SimpleAddress = string.Format("{0} {1} {2}", itemLocation.street, itemLocation.adminArea5, itemLocation.adminArea3),
															FullAddress = string.Format("{0} {1} {2}, {3} {4}", itemLocation.street, itemLocation.adminArea5, itemLocation.adminArea3, itemLocation.postalCode, itemLocation.adminArea1)
														};
								#endregion

								#region Add GeoLocation
								if (itemLocation.latLng != null && itemLocation.latLng.lat.HasValue && itemLocation.latLng.lng.HasValue)
								{
									item.Geolocation = new Geolocation { Latitude = itemLocation.latLng.lat.Value, Longitude = itemLocation.latLng.lng.Value };
								}
								#endregion

								#region Split Street
								if (item.GeocodeQuality == "ADDRESS" && item.geocodeQualityCode.StartsWith("L1"))
								{
									try
									{
										if (!string.IsNullOrWhiteSpace(itemLocation.street))
										{
											Regex regex = new Regex(@"^(\w+)[\s](.*)", RegexOptions.IgnoreCase);
											var matches = regex.Matches(itemLocation.street);
											if (matches.Count > 0)
											{
												if (matches[0].Groups.Count > 2)
												{
													item.StreetNo = matches[0].Groups[1].Value;
													item.Street = matches[0].Groups[2].Value;
												}
											}
										}
									}
									catch { }
								}
								else
								{
									item.Street = itemLocation.street;
								}
								#endregion

								result.Add(item);
							}
						}
					}
				}
			}

			return result;
		}
	}
}
