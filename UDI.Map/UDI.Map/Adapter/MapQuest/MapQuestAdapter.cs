﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

using UDI.Utility.Serialization;
using UDI.Utility.Helper;
using UDI.Map.Adapter.MapQuest.Helper;
using UDI.Map.Adapter.MapQuest.DTO;


namespace UDI.Map.Adapter.MapQuest
{
	internal class MapQuestAdapter : BaseMapAdapter
	{
		#region Fields
		string API_Key;
		string Geocoding_Json_Address;
		string Geocoding_Json_Address_Reverse;
		string Direction_Json_Route;
		#endregion

		#region construction
		internal MapQuestAdapter()
		{
			API_Key = System.Configuration.ConfigurationManager.AppSettings["MapQuest_API_Key"];
			var mapQuestDoc = XDocument.Parse(UDI.Map.Resource.MainResource.MapQuest);
			Geocoding_Json_Address = mapQuestDoc.Root.Element("Geocoding_Json_Address").Value;
			Geocoding_Json_Address_Reverse = mapQuestDoc.Root.Element("Geocoding_Json_Address_Reverse").Value;
			Direction_Json_Route = mapQuestDoc.Root.Element("Direction_Json_Route").Value;
		}
		#endregion

		#region internal
		internal override List<UDI.Map.DTO.Address> GetAddresses(string address, out string error)
		{
			error = string.Empty;
			List<UDI.Map.DTO.Address> result = null;
			//limitation of MapQuest. It does not accept #
			address = address.Replace("#", " ");

			#region Make request
			var request = new DTO.request();
			request.location = new DTO.location { street = address };
			request.options = new DTO.options { thumbMaps = false };
			#endregion

			#region build the query
			var jsonRequest = request.JsonSerialize(out error);
			var strRequest = WebUtility.HtmlDecode(Geocoding_Json_Address.Replace("##KEY##", API_Key).Replace("##Request##", jsonRequest));
			#endregion

			#region do the request
			var jsonResponse = WebRequestHelper.ProcessWebRequest(strRequest);
			#endregion

			#region Deserialiaze response to objects
			var response = jsonResponse.JsonDeserialize<DTO.response>(out error);
			#endregion

			if (string.IsNullOrWhiteSpace(error))
			{
				if (response.info.statuscode.HasValue && response.info.statuscode.Value == 0)
				{
					#region Make the result
					result = response.ToAddresses();
					#endregion
				}
				else
				{
					#region Make error
					error = response.info.messages[0];
					#endregion
				}
			}

			return result;
		}

		internal override List<Map.DTO.Address> GetAddressesFromGeoLocation(Map.DTO.Geolocation geoLocation, out string error)
		{
			error = string.Empty;
			List<UDI.Map.DTO.Address> result = null;

			#region Make request
			var request = new DTO.request();
			request.location = new DTO.location { latLng = new latLng { lat = geoLocation.Latitude, lng = geoLocation.Longitude } };
			request.options = new DTO.options { thumbMaps = false };
			#endregion

			#region build the query
			var jsonRequest = request.JsonSerialize(out error);
			var strRequest = WebUtility.HtmlDecode(Geocoding_Json_Address_Reverse.Replace("##KEY##", API_Key).Replace("##Request##", jsonRequest));
			#endregion

			#region do the request
			var jsonResponse = WebRequestHelper.ProcessWebRequest(strRequest);
			#endregion

			#region Deserialiaze response to objects
			var response = jsonResponse.JsonDeserialize<DTO.response>(out error);
			#endregion

			if (string.IsNullOrWhiteSpace(error))
			{
				if (string.IsNullOrWhiteSpace(error))
				{
					if (response.info.statuscode.HasValue && response.info.statuscode.Value == 0)
					{
						#region Make the result
						result = response.ToAddresses();
						#endregion
					}
					else
					{
						#region Make error
						error = response.info.messages[0];
						#endregion
					}
				}
			}

			return result;
		}

		internal override decimal GetDistance(string from, string to, UDI.Map.DTO.DistanceUnit distanceUnit, out string error)
		{
			error = string.Empty;
			decimal result = 0;
			from = from.Replace("#", " ");
			to = to.Replace("#", " ");

			#region Make request
			var request = new DTO.request();
			request.locations = new List<string>();
			request.locations.Add(from);
			request.locations.Add(to);
			request.options = new DTO.options { thumbMaps = false, unit = (distanceUnit == Map.DTO.DistanceUnit.Mile ? "m" : "k") };
			#endregion

			#region build the query
			var jsonRequest = request.JsonSerialize(out error);
			var strRequest = WebUtility.HtmlDecode(Direction_Json_Route.Replace("##KEY##", API_Key).Replace("##Request##", jsonRequest));
			#endregion

			#region do the request
			var jsonResponse = WebRequestHelper.ProcessWebRequest(strRequest);
			#endregion

			#region Deserialiaze response to objects
			var response = jsonResponse.JsonDeserialize<DTO.response>(out error);
			#endregion

			if (string.IsNullOrWhiteSpace(error))
			{
				if (string.IsNullOrWhiteSpace(error))
				{
					if (response.info.statuscode.HasValue && response.info.statuscode.Value == 0)
					{
						#region Make the result
						result = response.route.distance;
						#endregion
					}
					else
					{
						#region Make error
						error = response.info.messages[0];
						#endregion
					}
				}
			}

			return result;
		}

		internal override Map.DTO.ETA GetETA(Map.DTO.Geolocation from, Map.DTO.Geolocation to, Map.DTO.DistanceUnit distanceUnit, out string error)
		{
			throw new NotImplementedException();
		}

        internal override List<Map.DTO.Geolocation> CalculateVehicleMovements(Map.DTO.Geolocation p1, Map.DTO.Geolocation p2, out string error)
        {
            throw new NotImplementedException();
        }
		#endregion
	}
}
