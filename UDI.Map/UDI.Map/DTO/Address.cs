﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.Map.DTO
{
	public class Address
	{
		public string Country { get; set; }
		public string State { get; set; }
		public string City { get; set; }
		public string County { get; set; }
		public string FullStreet { get; set; }
		public string Street { get; set; }
		public string StreetNo { get; set; }
		public string ZipCode { get; set; }
		public string ApartmentNo { get; set; }
		public string AddressName { get; set; }
		public Geolocation Geolocation { get; set; }
		//Full address without zip and country
		public string SimpleAddress { get; set; }
		public string FullAddress { get; set; }
		public decimal Distance { get; set; }
		public string GeocodeQuality { get; set; }
		public string geocodeQualityCode { get; set; }
        public string airport_pickup_point { get; set; }
        public string address_type { get; set; }
        public string pickup_point{ get; set; }
        public string AirportCode{ get; set; }
        public string Airline{ get; set; }
        public string AirlineFlightNumber{ get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string FlightDateTime { get; set; }
	}


}
