﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UDI.Map.DTO
{
	public class ETA
	{
		//public decimal? Distance { get; set; }
		public int? DurationInSecond { get; set; }
		public int? DurationInTrafficInSecond { get; set; }
	}
}
