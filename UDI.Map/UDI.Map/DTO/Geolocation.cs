﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.Map.DTO
{
	public class Geolocation
	{
		[XmlAttribute]
		public decimal Latitude { get; set; }
		[XmlAttribute]
		public decimal Longitude { get; set; }
        [XmlAttribute]
        public decimal Coarse { get; set; }
	}
}
