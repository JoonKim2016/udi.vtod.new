﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UDI.Map.DTO.XML
{
	//[XmlRoot("gpx", Namespace = "xmlns:xsi", IsNullable = false)]
	public class gpx
	{
		[XmlAttribute("xsi", Namespace = "http://www.whited.us")]
		public string MyOther { get; set; }

		[XmlElement("rte")]
		public List<rte> rteList { get; set; }
	}
}
