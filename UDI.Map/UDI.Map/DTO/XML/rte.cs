﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UDI.Map.DTO.XML
{
	public class rte
	{
		public string name { get; set; }
		public string desc { get; set; }
		[XmlElement("rtept")]
		public List<rtept> rteptList { get; set; }
	}
}
