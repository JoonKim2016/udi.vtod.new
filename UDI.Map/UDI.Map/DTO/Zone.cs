﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UDI.Map.DTO
{
	public class Zone
	{
		[XmlAttribute]
		public string Name { get; set; }
		public List<Geolocation> Geolocations { get; set; }
	}
}
