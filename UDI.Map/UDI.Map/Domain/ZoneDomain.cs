﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UDI.Map.DTO;
using UDI.Utility.Helper;
using Math = System.Math;
namespace UDI.Map.Domain
{
	public class ZoneDomain
	{
		Zone _zone;

		private ZoneDomain()
		{
		}

		public ZoneDomain(Zone zone)
		{
			_zone = zone;
		}

		public bool IsInTheZone(Geolocation point)
		{
			var result = false;

			var myPts = _zone.Geolocations.ToArray();

			int sides = myPts.Count() - 1;
			int j = sides - 1;
			for (int i = 0; i < sides; i++)
			{
				if (myPts[i].Longitude < point.Longitude && myPts[j].Longitude >= point.Longitude || myPts[j].Longitude < point.Longitude && myPts[i].Longitude >= point.Longitude)
				{
					if (myPts[i].Latitude + (point.Longitude - myPts[i].Longitude) / (myPts[j].Longitude - myPts[i].Longitude) * (myPts[j].Latitude - myPts[i].Latitude) < point.Latitude)
					{
						result = !result;
					}
				}
				j = i;
			}

			return result;
		}

      
	}
}
