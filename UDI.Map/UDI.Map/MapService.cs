﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;
using UDI.Map.Adapter;
using UDI.Map.Domain;
using UDI.Map.DTO;
using UDI.Map.Utility;

namespace UDI.Map
{
	public class MapService
	{
		BaseMapAdapter mapAdapter;
		#region Constructors
		public MapService()
		{
			var provider = System.Configuration.ConfigurationManager.AppSettings["MapApiProvider"];
			switch (provider)
			{
				case "MapQuest":
					mapAdapter = new Adapter.MapQuest.MapQuestAdapter();
					break;
			}
		}

		public MapService(MapApiProvider provider)
		{
			//var provider = System.Configuration.ConfigurationManager.AppSettings["MapApiProvider"];
			switch (provider)
			{
				case MapApiProvider.MapQuest:
					mapAdapter = new Adapter.MapQuest.MapQuestAdapter();
					break;
				case MapApiProvider.Google:
					mapAdapter = new Adapter.Google.GoogleAdapter();
					break;
			}
		}

		#endregion

		#region Public

		public List<DTO.Address> GetAddresses(string address, out string error)
		{
			error = string.Empty;
			List<DTO.Address> result = null;

			try
			{
				result = mapAdapter.GetAddresses(address, out error);
			}
			catch (Exception ex)
			{
				error = ex.Message;
			}

			return result;
		}

		public List<DTO.Address> GetAddressesFromGeoLocation(DTO.Geolocation geoLocation, out string error)
		{
			error = string.Empty;
			List<DTO.Address> result = null;			
			List<DTO.Address> results = new List<Address>();

			try
			{
				result = mapAdapter.GetAddressesFromGeoLocation(geoLocation, out error);
			}
			catch (Exception ex)
			{
				error = ex.Message;
			}

			return result;
		}

		public List<DTO.Address> GetAddressesFromGeoLocationWithProximity(DTO.Geolocation geoLocation, out string error)
		{
			error = string.Empty;
			List<DTO.Address> result = null;
			List<DTO.Address> tempResult = null;
			List<DTO.Address> results = new List<Address>();

			var proximityValue = 0.001M;
			var proximityTimes = 3;
			try
			{
				result = mapAdapter.GetAddressesFromGeoLocation(geoLocation, out error);
				if (result == null || !result.Any() || string.IsNullOrWhiteSpace(result.First().StreetNo)) result = new List<Address>();

				for (int i = 1; i < proximityTimes + 1; i++)
				{
					if (result == null || !result.Any() || string.IsNullOrWhiteSpace(result.First().StreetNo))
					{
						var p = (proximityValue * i);
						var newGeolocation = new DTO.Geolocation { Latitude = geoLocation.Latitude, Longitude = geoLocation.Longitude - p };
						tempResult = mapAdapter.GetAddressesFromGeoLocation(newGeolocation, out error);
						if (tempResult != null && tempResult.Any() && !string.IsNullOrWhiteSpace(tempResult.First().StreetNo))
							results.AddRange(tempResult);

						newGeolocation = new DTO.Geolocation { Latitude = geoLocation.Latitude, Longitude = geoLocation.Longitude + p };
						tempResult = mapAdapter.GetAddressesFromGeoLocation(newGeolocation, out error);
						if (tempResult != null && tempResult.Any() && !string.IsNullOrWhiteSpace(tempResult.First().StreetNo))
							results.AddRange(tempResult);

						newGeolocation = new DTO.Geolocation { Latitude = geoLocation.Latitude - p, Longitude = geoLocation.Longitude };
						tempResult = mapAdapter.GetAddressesFromGeoLocation(newGeolocation, out error);
						if (tempResult != null && tempResult.Any() && !string.IsNullOrWhiteSpace(tempResult.First().StreetNo))
							results.AddRange(tempResult);

						newGeolocation = new DTO.Geolocation { Latitude = geoLocation.Latitude - p, Longitude = geoLocation.Longitude - p };
						tempResult = mapAdapter.GetAddressesFromGeoLocation(newGeolocation, out error);
						if (tempResult != null && tempResult.Any() && !string.IsNullOrWhiteSpace(tempResult.First().StreetNo))
							results.AddRange(tempResult);

						newGeolocation = new DTO.Geolocation { Latitude = geoLocation.Latitude - p, Longitude = geoLocation.Longitude + p };
						tempResult = mapAdapter.GetAddressesFromGeoLocation(newGeolocation, out error);
						if (tempResult != null && tempResult.Any() && !string.IsNullOrWhiteSpace(tempResult.First().StreetNo))
							results.AddRange(tempResult);

						newGeolocation = new DTO.Geolocation { Latitude = geoLocation.Latitude + p, Longitude = geoLocation.Longitude };
						tempResult = mapAdapter.GetAddressesFromGeoLocation(newGeolocation, out error);
						if (tempResult != null && tempResult.Any() && !string.IsNullOrWhiteSpace(tempResult.First().StreetNo))
							results.AddRange(tempResult);

						newGeolocation = new DTO.Geolocation { Latitude = geoLocation.Latitude + p, Longitude = geoLocation.Longitude - p };
						tempResult = mapAdapter.GetAddressesFromGeoLocation(newGeolocation, out error);
						if (tempResult != null && tempResult.Any() && !string.IsNullOrWhiteSpace(tempResult.First().StreetNo))
							results.AddRange(tempResult);

						newGeolocation = new DTO.Geolocation { Latitude = geoLocation.Latitude + p, Longitude = geoLocation.Longitude + p };
						tempResult = mapAdapter.GetAddressesFromGeoLocation(newGeolocation, out error);
						if (tempResult != null && tempResult.Any() && !string.IsNullOrWhiteSpace(tempResult.First().StreetNo))
							results.AddRange(tempResult);
					}
				}


				if (results != null && results.Any())
				{
					foreach (var item in results)
					{
						try
						{
							item.Distance = Convert.ToDecimal(GetDirectDistanceInMeter(Convert.ToDouble(geoLocation.Latitude), Convert.ToDouble(geoLocation.Longitude), Convert.ToDouble(item.Geolocation.Latitude), Convert.ToDouble(item.Geolocation.Longitude)));
						}
						catch
						{ }
					}
					result = new List<Address>();
					result.Add(results.OrderBy(s => s.Distance).First());
				}
				//else
				//{
				//	try
				//	{
				//		result.First().Distance = Convert.ToDecimal(GetDirectDistanceInMeter(Convert.ToDouble(geoLocation.Latitude), Convert.ToDouble(geoLocation.Longitude), Convert.ToDouble(result.First().Geolocation.Latitude), Convert.ToDouble(result.First().Geolocation.Longitude)));
				//	}
				//	catch
				//	{}
				//}


			}
			catch (Exception ex)
			{
				error = ex.Message;
			}

			return result;
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="from">latlong as "40.07546,-78.329999" or location as "Clarendon Blvd, Arlington, VA"</param>
		/// <param name="to">latlong as "40.07546,-78.329999" or location as "Clarendon Blvd, Arlington, VA"</param>
		/// <param name="distanceUnit"></param>
		/// <param name="error"></param>
		/// <returns></returns>
		public decimal GetDistance(string from, string to, DTO.DistanceUnit distanceUnit, out string error)
		{
			error = string.Empty;
			decimal result = 0;

			try
			{
				result = mapAdapter.GetDistance(from, to, distanceUnit, out error);
			}
			catch (Exception ex)
			{
				error = ex.Message;
			}

			return result;
		}

		public ETA GetETA(Map.DTO.Geolocation from, Map.DTO.Geolocation to, Map.DTO.DistanceUnit distanceUnit, out string error)
		{
			error = string.Empty;
			ETA result = null;

			try
			{
				result = mapAdapter.GetETA(from, to, distanceUnit, out error);
			}
			catch (Exception ex)
			{
				error = ex.Message;
			}

			return result;
		}

        public List<Geolocation> CalculateVehicleMovements(Map.DTO.Geolocation from, Map.DTO.Geolocation to, out string error)
        {
            error = string.Empty;
            List<Geolocation> result = null;

            try
            {
                result = mapAdapter.CalculateVehicleMovements(from, to,out error);
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }

            return result;
        }
		#endregion

		#region Zone
		public bool IsInTheZone(Geolocation point, Zone zone)
		{
			var result = false;

			var zoneDomain = new ZoneDomain(zone);

			result = zoneDomain.IsInTheZone(point);

			return result;
		}

		public Zone FindZone(string address, string zoneGpxUri)
		{
			var zones = zoneGpxUri.ToZones();
			var error = string.Empty;
			var addresses = GetAddresses(address, out error);

			if (zones != null)
			{
				if (string.IsNullOrWhiteSpace(error) && addresses != null && addresses.Any())
				{
					var addressObj = addresses.First();
					if (addressObj.Geolocation != null)
					{
						var point = new Geolocation { Latitude = addressObj.Geolocation.Latitude, Longitude = addressObj.Geolocation.Longitude };
						foreach (var zone in zones)
						{
							if (IsInTheZone(point, zone))
							{
								return zone;
							}
						}
					}
				}
			}

			return null;
		}

		public Zone FindZone(Geolocation point, XDocument zoneXml)
		{
			var zones = zoneXml.ToZones();
			if (zones != null)
			{
				foreach (var zone in zones)
				{
					if (IsInTheZone(point, zone))
					{
						return zone;
					}
				}
			}

			return null;
		}

		#region Do not Delete
		//public Zone FindZone(string address, XDocument zoneXml)
		//{
		//	var zones = zoneXml.ToZones();
		//	var error = string.Empty;
		//	var addresses = GetAddresses(address, out error);

		//	if (zones != null)
		//	{
		//		if (string.IsNullOrWhiteSpace(error) && addresses != null && addresses.Any())
		//		{
		//			var addressObj = addresses.First();
		//			if (addressObj.Geolocation != null)
		//			{
		//				var point = new Geolocation { Latitude = addressObj.Geolocation.Latitude, Longitude = addressObj.Geolocation.Longitude };
		//				foreach (var zone in zones)
		//				{
		//					if (IsInTheZone(point, zone))
		//					{
		//						return zone;
		//					}
		//				}
		//			}
		//		}
		//	}

		//	return null;
		//}

		//public Zone FindZone(Geolocation point, List<Zone> zones)
		//{
		//	if (zones != null)
		//	{
		//		foreach (var zone in zones)
		//		{
		//			if (IsInTheZone(point, zone))
		//			{
		//				return zone;
		//			}
		//		}
		//	}

		//	return null;
		//}

		//public Zone FindZone(Geolocation point, List<string> zoneGpxUris)
		//{
		//	var zones = zoneGpxUris.ToZones();
		//	if (zones != null)
		//	{
		//		foreach (var zone in zones)
		//		{
		//			if (IsInTheZone(point, zone))
		//			{
		//				return zone;
		//			}
		//		}
		//	}

		//	return null;
		//}

		//public Zone FindZone(Geolocation point, string zoneGpxUri)
		//{
		//	var zones = zoneGpxUri.ToZones();
		//	if (zones != null)
		//	{
		//		foreach (var zone in zones)
		//		{
		//			if (IsInTheZone(point, zone))
		//			{
		//				return zone;
		//			}
		//		}
		//	}

		//	return null;
		//} 
		#endregion
		#endregion


		//This result is in meter. Need to support all  to be public
		public double GetDirectDistanceInMeter(double lat1, double long1, double lat2, double long2)
		{

			var firstCordinate = new GeoCoordinate(lat1, long1);
			var secondCordinate = new GeoCoordinate(lat2, long2);

			double distance = firstCordinate.GetDistanceTo(secondCordinate);
			return distance;



			//double distance = 0;

			//double dLat = (lat2 - lat1) / 180 * Math.PI;
			//double dLong = (long2 - long1) / 180 * Math.PI;

			//double a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2)
			//			+ Math.Cos(lat2) * Math.Sin(dLong / 2) * Math.Sin(dLong / 2);
			//double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));

			////Calculate radius of earth
			//// For this you can assume any of the two points.
			//double radiusE = 6378135; // Equatorial radius, in metres
			//double radiusP = 6356750; // Polar Radius

			////Numerator part of function
			//double nr = Math.Pow(radiusE * radiusP * Math.Cos(lat1 / 180 * Math.PI), 2);
			////Denominator part of the function
			//double dr = Math.Pow(radiusE * Math.Cos(lat1 / 180 * Math.PI), 2)
			//				+ Math.Pow(radiusP * Math.Sin(lat1 / 180 * Math.PI), 2);
			//double radius = Math.Sqrt(nr / dr);

			////Calculate distance in meters.
			//distance = radius * c;
			//return distance; // distance in meters
		}

		//Mile
		public double GetDirectDistanceInMile(double lat1, double long1, double lat2, double long2)
		{
			double distance = GetDirectDistanceInMeter(lat1, long1, lat2, long2) / 1609.344;
			return distance;
		}
		#region IsValid - by Bryan
		private string[] _STATE_CODES = new string[] { "AK", "AL", "AR", "AZ", "CA", "CO", "CT", "DC", "DE", "FL", "GA", "GU", "HI", "IA", "ID", "IL", "IN", "KS", "KY", "LA", "MA", "MD", "ME", "MI", "MN", "MO", "MS", "MT", "NC", "ND", "NE", "NH", "NJ", "NM", "NV", "NY", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VA", "VT", "WA", "WI", "WV", "WY", "ON", "BC" };

		public bool IsValidAddress(string address)
		{
			//First Number should always be number
			string firstNumnerPattern = @"(^[0-9]).*";

			if (!Regex.IsMatch(address, firstNumnerPattern))
			{
				return false;
			}

			//remove US country code
			if (Regex.IsMatch(address, @".*[\s]US$", RegexOptions.IgnoreCase))
			{
				address = Regex.Replace(address, @"[,]{0,1}[\s]US$", string.Empty, RegexOptions.IgnoreCase);
			}
			else if (Regex.IsMatch(address, @".*[\s]USA$", RegexOptions.IgnoreCase))
			{
				address = Regex.Replace(address, @"[,]{0,1}[\s]USA$", string.Empty, RegexOptions.IgnoreCase);
			}

		//remove CA country code
			else if (Regex.IsMatch(address, @".*[\s]CANADA$", RegexOptions.IgnoreCase))
			{
				address = Regex.Replace(address, @"[,]{0,1}[\s]CANADA$", string.Empty, RegexOptions.IgnoreCase);
			}
			else if (Regex.IsMatch(address, @".*[\s]CA$", RegexOptions.IgnoreCase))
			{
				if (!Regex.IsMatch(address, @".*[0-9]{5}[-]{0,1}[0-9]{0,4}[,]{0,1}[\s]CA$", RegexOptions.IgnoreCase))
				{
					// Not US
					address = Regex.Replace(address, @"[,]{0,1}[\s]CA$", string.Empty, RegexOptions.IgnoreCase);
				}
			}

			//US part
			//this checks the reverse of the address string to make sure either zipcode or city state is supplied.
			Regex zipCodeRe = new Regex(@"^(\d{4}(-\d{5})?)|(\d[A-Z]\d [ABCEGHJKLMNPRSTVXY]\d[A-Z])(\s{0,}\,{0,1})[\w\d\s\.\#\,]{1,100}$");
			Regex cityStateRe = new Regex(@"^([\w]{2}(\s{0,}\,{0,1})([\w\s\d]{1,})(\s{0,}\,{0,1}))[\w\d\s\.\#\,]{1,100}$");


			address = address.ToUpper().Trim();
			string reverseAddress = new string(address.Reverse().ToArray());
			if (zipCodeRe.IsMatch(reverseAddress))
				return true;
			else if (cityStateRe.IsMatch(reverseAddress) && reverseAddress.Length > 2 && _STATE_CODES.Contains(address.Substring(address.Length - 2, 2).ToUpper()))
				return true;

			//Canda part
			string canada_zip6 = @"[A-Z][0-9][A-Z][0-9][A-Z][0-9]";
			string canada_zip3 = @"[A-Z][0-9][A-Z]";
			string[] sep = { " " };

			string[] address_part = address.Replace("-", " ").Split(sep, StringSplitOptions.RemoveEmptyEntries).ToArray();


			string last6 = address_part[address_part.Length - 2] + address_part[address_part.Length - 1];
			string last3 = address_part[address_part.Length - 1];


			if (Regex.IsMatch(last6, canada_zip6))
			{
				return true;
			}
			else if (Regex.IsMatch(last3, canada_zip3))
			{
				return true;
			}

			return false;
		}
		#endregion
	}
}
