﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

using UDI.Map.DTO;
using UDI.Map.DTO.XML;
using UDI.Utility.Serialization;
using UDI.Utility.Helper;

namespace UDI.Map.Utility
{
	public static class Converter
	{
		public static List<Zone> ToZones(this string gpxUri)
		{
			List<Zone> result = null;

			if (!string.IsNullOrWhiteSpace(gpxUri))
			{
				try
				{
					var xZone = XDocument.Load(gpxUri);
					var gpxZone = xZone.XmlDeserialize<gpx>();

					result = new List<Zone>();
					var zones = gpxZone.rteList;
					if (zones != null && zones.Any())
					{
						foreach (var zone in zones)
						{
							var zoneToBeAdded = new Zone();
							zoneToBeAdded.Name = zone.name;
							if (zone.rteptList != null && zone.rteptList.Any())
							{
								zoneToBeAdded.Geolocations = new List<Geolocation>();
								foreach (var location in zone.rteptList)
								{
									zoneToBeAdded.Geolocations.Add(new Geolocation { Latitude = location.lat, Longitude = location.lon });
								}
							}
							result.Add(zoneToBeAdded);
						}
					}

				}
				catch { }
			}

			return result;
		}

		public static List<Zone> ToZones(this XDocument gpxXml)
		{
			List<Zone> result = null;
			gpxXml.CleanNamespace().CleanComment();

			if (gpxXml != null)
			{
				try
				{
					var gpxZone = gpxXml.XmlDeserialize<gpx>();

					
					
					 result = new List<Zone>();
					var zones = gpxZone.rteList;
					if (zones != null && zones.Any())
					{
						foreach (var zone in zones)
						{
							var zoneToBeAdded = new Zone();
							zoneToBeAdded.Name = zone.name;
							if (zone.rteptList != null && zone.rteptList.Any())
							{
								zoneToBeAdded.Geolocations = new List<Geolocation>();
								foreach (var location in zone.rteptList)
								{
									zoneToBeAdded.Geolocations.Add(new Geolocation { Latitude = location.lat, Longitude = location.lon });
								}
							}
							result.Add(zoneToBeAdded);
						}
					}

				}
				catch { }
			}

			return result;
		}

		public static List<Zone> ToZones(this List<string> gpxUris)
		{
			List<Zone> result = null;

			if (gpxUris != null && gpxUris.Any())
			{
				result = new List<Zone>();
				foreach (var gpxUri in gpxUris)
				{
					var zones = gpxUri.ToZones();
					if (zones != null && gpxUri.Any())
					{
						result.AddRange(zones);
					}
				}
			}

			return result;
		}
	}
}
