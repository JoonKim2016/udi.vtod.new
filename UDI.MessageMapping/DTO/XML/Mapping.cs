﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UDI.MessageMapping.DTO.XML
{
	public class Mapping
	{
		[XmlAttribute]
		public string Type { get; set; }
		public string Input { get; set; }
		public string Output { get; set; }
	}
}
