﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UDI.MessageMapping.DTO.XML
{
	public class Mappings
	{
		[XmlElement("Mapping")]
		public List<Mapping> Items { get; set; }
	}
}
