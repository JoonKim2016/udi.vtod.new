﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UDI.MessageMapping.DTO.XML;
using System.Xml.Linq;

using UDI.Utility.Serialization;

namespace UDI.MessageMapping
{
	public class Mapper
	{
		#region Fields
		MappingConfig mappingConfig;
		string _error = string.Empty;
		string _delimiter = "##";
		#endregion

		#region Properties
		public string Delimiter
		{
			set
			{
				_delimiter = value;
			}
		}
		#endregion

		#region Construcors
		private Mapper()
		{
		}

		public Mapper(string mappingConfigPath)
		{
			try
			{
				var mappingConfigXML = XDocument.Load(mappingConfigPath);
				var error = string.Empty;
				mappingConfig = mappingConfigXML.XmlDeserialize<MappingConfig>(out error);
				if (!string.IsNullOrEmpty(error))
				{
					throw new Exception(error);
				}
			}
			catch (Exception ex)
			{
				_error = ex.Message;
			}
		}
		#endregion

		#region Public
		public string GetMappedMessage(string type, string message, out string error)
		{
			var result = message;
			error = string.Empty;

			try
			{
				if (string.IsNullOrWhiteSpace(_error))
				{

					var results = mappingConfig.Mappings.Items.Where(s => s.Type.Trim().ToLower() == type.Trim().ToLower() && s.Input.Trim().ToLower().Split(new string[] { _delimiter }, StringSplitOptions.None).Contains(message.Trim().ToLower()));
					if (results.Any())
					{
						result = results.First().Output;
					}
				}
				else
				{
					throw new Exception(_error);
				}
			}
			catch (Exception ex)
			{
				error = ex.Message;
			}

			return result;
		}

		public string GetMappedMessage(string type, string message)
		{
			string error;
			return GetMappedMessage(type, message, out error);
		}

		#endregion
	}
}
