﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UDI.Notification.Service.Controller;
using UDI.Notification.Service.Class;

namespace Test.UDI.Notification.Service
{
    class Program
    {
        static void Main(string[] args)
        {
            MessageController c = new MessageController();
            string errorMessage = "";

            #region Test email
			//SMTPEmailMessage s = new SMTPEmailMessage();
			//s.From = "TripCompare@unified-dispatch.com";
			//s.To = new List<string> { "pouyan@paryas.com" };
			//s.Subject = "Greeting";
			//s.MessageBody = "Hello";
			//s.SMTP_Host = "China";
			//s.SMTP_Port = 25;

			//if (c.SendSMTPEmailMessage(s, out errorMessage))
			//{
			//	Console.WriteLine("Sucess");
			//}
			//else
			//{
			//	Console.WriteLine(errorMessage);
			//}
            #endregion

			#region Outlook Test
			//SMTPEmailMessage s = new SMTPEmailMessage();
			//s.From = "unified.dispatch@outlook.com";
			//s.To = new List<string> { "pouyan@paryas.com" };
			//s.Subject = "Greeting";
			//s.MessageBody = "Hello";
			//s.SMTP_Host = "smtp.outlook.com";
			//s.SMTP_Port = 587;
			//s.SMTP_Credentials = new System.Net.NetworkCredential("unified.dispatch@outlook.com", "DigiKey@12");
			//s.SMTP_EnableSsl = true;
			//s.SMTP_UseDefaultCredentials = false;

			//if (c.SendSMTPEmailMessage(s, out errorMessage))
			//{
			//	Console.WriteLine("Sucess");
			//}
			//else
			//{
			//	Console.WriteLine(errorMessage);
			//}
			
			#endregion

			#region Trip Compare
			SMTPEmailMessage s = new SMTPEmailMessage();
			s.From = "TripCompare@unified-dispatch.com";
			s.To = new List<string> { "pouyan@paryas.com" };
			s.Subject = "Greeting";
			s.MessageBody = "Hello";
			s.SMTP_Host = "remote.unified-dispatch.com"; // "smtp.zerolag.com";
			s.SMTP_Port = 25;
			s.SMTP_Credentials = new System.Net.NetworkCredential("TripCompare@unified-dispatch.com", "unifdisp12");
			//s.SMTP_EnableSsl = true;
			s.AttachmentPaths = new List<string>();
			s.AttachmentPaths.Add(@"D:\UDI\Documents\SharePoint\Intelli Ride - Shared Documents\Baltimore Test File 2011-06-30.csv");
			//s.SMTP_UseDefaultCredentials = false;
			
			if (c.SendSMTPEmailMessage(s, out errorMessage))
			{
				Console.WriteLine("Sucess");
			}
			else
			{
				Console.WriteLine(errorMessage);
			}

			#endregion
			
			#region zTrip
			//SMTPEmailMessage s = new SMTPEmailMessage();
			//s.From = "NoReply@zTrip.com";
			//s.To = new List<string> { "pouyan@paryas.com" };
			//s.Subject = "Greeting";
			//s.MessageBody = "Hello";
			//s.SMTP_Host = "192.168.20.110";
			//s.SMTP_Port = 25;

			//if (c.SendSMTPEmailMessage(s, out errorMessage))
			//{
			//	Console.WriteLine("Sucess");
			//}
			//else
			//{
			//	Console.WriteLine(errorMessage);
			//}
			#endregion


            #region Test Twilio SMS
            //TwilioSMSMessage t = new TwilioSMSMessage();
            //t.Account = "AC080c631b038213e5af22fa701ee66fe1";
            //t.Token = "fb094a851711919c254be0d27fbaa049";
            //t.From = "+14423337433";
            //t.To = new List<string> { "+13474860403" };
            //t.MessageBody = "SMS test";
            //if (c.SendTwilioSMSMessage(t, out errorMessage))
            //{
            //    Console.WriteLine("Sucess");
            //}
            //else {
            //    Console.WriteLine("errorMessage");
            //}
            #endregion


            Console.WriteLine("Press any key to end this program.");
            Console.ReadLine();

        }
    }
}
