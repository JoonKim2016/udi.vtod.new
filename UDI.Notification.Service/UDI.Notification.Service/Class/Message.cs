﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UDI.Notification.Service.Class
{
    public class Message
    {
        public Message(){
            To = new List<string>();
        }

        public string From { set; get; }
        public List<string> To { set; get; }
        public string MessageBody { set; get; }
    }
}
