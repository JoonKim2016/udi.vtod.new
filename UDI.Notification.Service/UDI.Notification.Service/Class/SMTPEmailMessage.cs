﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace UDI.Notification.Service.Class
{
    public class SMTPEmailMessage:Message
    {
        public SMTPEmailMessage() {
            IsBodyHTML=false;
            SMTP_EnableSsl = false;
            SMTP_UseDefaultCredentials = false;
            SMTP_Credentials = null;
            AttachmentPaths = new List<string>();
        }


        public string Subject { set; get; }
        public bool IsBodyHTML { set; get; }
        public List<string> AttachmentPaths { set; get; }

        public string SMTP_Host { set; get; }
        public int SMTP_Port { set; get; }
        public bool SMTP_EnableSsl { set; get; }
        public bool SMTP_UseDefaultCredentials { set; get; }
        public NetworkCredential SMTP_Credentials { set; get; }
        

    }
}
