﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UDI.Notification.Service.Class
{
    public class TwilioSMSMessage : Message
    {
        public string Account { set; get; }
        public string Token { set; get; }
    }
}
