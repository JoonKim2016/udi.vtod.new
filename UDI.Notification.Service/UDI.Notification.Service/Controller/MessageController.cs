﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UDI.Notification.Service.Class;
using System.Net.Mail;

namespace UDI.Notification.Service.Controller
{
    public class MessageController
    {
        #region Public Methods
        public bool SendTwilioSMSMessage(TwilioSMSMessage m, out string errorMessage) {
            bool result = false;
            errorMessage = string.Empty;
            try
            {
                if (ValidateTwilioSMSMessage(m))
                {

                    Twilio.TwilioRestClient twilio = new Twilio.TwilioRestClient(m.Account, m.Token);
                    foreach(string t in m.To)
                    {
                        var response = twilio.SendMessage(m.From, t, m.MessageBody);
                        if (response == null || string.IsNullOrEmpty(response.Status) || response.Status.ToLower() == "failed") 
                        {
                            string error = string.Format("Unable to send SMS through Twilio. From={0}, To={1}, Message={2}, DateTime={3}", m.From, t, m.MessageBody, DateTime.Now);
                            errorMessage = string.Format("{0}\r\n{1}",errorMessage,error);
                        }
                    }

                    if (string.IsNullOrWhiteSpace(errorMessage))
                    {
                        result = true;
                    }
                    else {
                        result = false;
                    }

                    
                }
                else {
                    result = false;
                    errorMessage = "Invalid TwilioSMSMessage object.";
                }

            }
            catch (Exception ex) 
            {
                errorMessage = string.Format("Exception:{0}, StackPrintTrace:{1}", ex.Message, ex.StackTrace.ToString());
                throw new Exception(errorMessage);
            }
            return result;
        }



        public bool SendSMTPEmailMessage(SMTPEmailMessage m, out string errorMessage)
        {
            bool result = false;
            errorMessage = "";
            try
            {
                if (ValidateSMTPEmailMessage(m))
                {
                    //build msg
                    MailMessage msg = new MailMessage();
                    msg.From = new MailAddress(m.From);
                    foreach (string r in m.To) 
                    {
                        msg.To.Add(r);    
                    }
                    msg.Subject = m.Subject;
                    msg.SubjectEncoding = Encoding.UTF8;
                    msg.Body = m.MessageBody;
                    msg.IsBodyHtml = m.IsBodyHTML;
                    if (m.AttachmentPaths.Any()) {
                        foreach (string p in m.AttachmentPaths) {
                            msg.Attachments.Add(new Attachment(p));
                        }
                    }


                    //create SMTPClient and send it out
                    SmtpClient sc = new SmtpClient();
                    sc.Host = m.SMTP_Host;
                    sc.Port = m.SMTP_Port;
                    sc.EnableSsl = m.SMTP_EnableSsl;
                    sc.UseDefaultCredentials = m.SMTP_UseDefaultCredentials;
					if (m.SMTP_Credentials != null)
					{
						sc.Credentials = m.SMTP_Credentials;
					}

                    sc.DeliveryMethod = SmtpDeliveryMethod.Network;
                    
                    //send
                    sc.Send(msg);


                    result = true;
                }
                else 
                {
                   result = false;
                   errorMessage = "Invalid SMTPEmailMessage object.";
                }

            }
            catch (Exception ex)
            {
                errorMessage = string.Format("Exception:{0}, StackPrintTrace:{1}", ex.Message, ex.StackTrace.ToString());
                throw new Exception(errorMessage);
            }
            return result;
        }
        #endregion

        #region Private Method
        private bool ValidateSMTPEmailMessage(SMTPEmailMessage m)
        {
            bool result = false;

            //validate recipients
            if (m.To.Any())
            {
                result = true;
            }
            else {
                result = false;
            }

            //validate  IP address
            if (result && !string.IsNullOrWhiteSpace(m.SMTP_Host))
            {
                result = true;
            }
            else {
                result = false;
            }
            
            
            return result;
        }

        private bool ValidateTwilioSMSMessage(TwilioSMSMessage m)
        {
            bool result = false;
            //validate twilio account and token
            if (!string.IsNullOrWhiteSpace(m.Account) && !string.IsNullOrWhiteSpace(m.Token))
            {
                result = true;
            }
            else {
                result = false;
            }


            return result;
        }
        #endregion
    }
}
