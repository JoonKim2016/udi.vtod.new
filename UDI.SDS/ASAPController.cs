﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.Track;
using UDI.SDS.Helper;
using UDI.SDS.FranchiseService;
using UDI.SDS.DTO.Enum;
using UDI.SDS.DTO;
using UDI.SDS.ServiceAdapter;
using UDI.SDS.ASAPService;
using UDI.Utility.Helper;

namespace UDI.SDS
{
	public class ASAPController : BaseSetting
	{
		#region Fields
		TrackTime _trackTime;
		TokenRS _token;
		#endregion

		#region Constructors
		private ASAPController()
		{
		}

		public ASAPController(TokenRS token, TrackTime trackTime)
		{
			_token = token;
			_trackTime = trackTime;
		}

		#endregion

		#region Public
		public int CreatCharterASAPRequest(CharterAsapRequest charterAsapRequest)//AddressRecord dropoffAddress, bool asDirected, string franchiseCode, string fleetType, string SerivceType, int fareID, int payingPax, string userID)
		{
			#region Init
			int asapRequestID = 0;
			UDI.SDS.ASAPService.SecurityToken groupsToken = _token.ToASAPServiceToken();
			var asapAdapter = new ASAPServiceAdapter(groupsToken, _trackTime);
			#endregion

			asapRequestID = asapAdapter.CreatCharterASAPRequest(charterAsapRequest);

			return asapRequestID;
		}

		public int CreatAirportASAPRequest(RequestDetails airportAsapRequest)
		{
			#region Init
			int asapRequestID = 0;
			UDI.SDS.ASAPService.SecurityToken groupsToken = _token.ToASAPServiceToken();
			var asapAdapter = new ASAPServiceAdapter(groupsToken, _trackTime);
			#endregion



			asapRequestID = asapAdapter.CreatAirportASAPRequest(airportAsapRequest);

			return asapRequestID;
		}

		public StatusUpdate GetASAPRequestStatus(int requestID)
		{
			#region Init
			StatusUpdate result = null;
			UDI.SDS.ASAPService.SecurityToken groupsToken = _token.ToASAPServiceToken();
			var asapAdapter = new ASAPServiceAdapter(groupsToken, _trackTime);
			#endregion

			result = asapAdapter.GetASAPRequestStatus(requestID);

			//result.ApprovedPickupTime
			//result.DispositionCode	
			//result.ServiceIsAOR

			return result;
		}

		public void AbandonRequest(string userName, int requestID)
		{
			#region Init
			UDI.SDS.ASAPService.SecurityToken groupsToken = _token.ToASAPServiceToken();
			var asapAdapter = new ASAPServiceAdapter(groupsToken, _trackTime);
			#endregion

			asapAdapter.AbandonRequest(userName, requestID);
		}
		#endregion

		#region Properties
		public TrackTime TrackTime
		{
			get
			{
				return _trackTime;
			}
			set
			{
				_trackTime = value;
			}
		}

		public TokenRS Token
		{
			set
			{
				_token = value;
			}
		}
		#endregion
	}
}
