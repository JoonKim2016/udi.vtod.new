﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.Track;
using UDI.SDS.Helper;
using UDI.SDS.DTO.Enum;
using UDI.SDS.DTO;
using UDI.SDS.ServiceAdapter;
using UDI.SDS.MembershipService;
using UDI.SDS.UtilityService;

namespace UDI.SDS
{
	public class AccoutingController : BaseSetting
	{
		#region Fields
		TokenRS _token;
		TrackTime _trackTime;
		#endregion

		#region Constructors
		private AccoutingController()
		{
		}

		public AccoutingController(TokenRS token, TrackTime trackTime)
		{
			_token = token;
			_trackTime = trackTime;
		}
		#endregion

		#region Public

		/// <summary>
		/// Verify and create a credit card account. Return account ID.
		/// </summary>
		public int CreateCreditCardAccount(int memberId, MembershipCreditCardAccount accountDetails, string cvvNumber)
		{
            try
            {
                #region Init
                UDI.SDS.MembershipService.SecurityToken accountingToken = _token.ToMembershipServiceToken();
                var accoutningAdapter = new AccountingServiceAdapter(accountingToken, _trackTime);
                #endregion

                var result = accoutningAdapter.CreateCreditCardAccount(memberId, accountDetails, cvvNumber);

                return result;
            }
           
            catch (Exception ex)
			{
                logger.ErrorFormat("CreateCreditCardAccount:{0}", ex.ToString());
				throw ex;
			}
		}

		/// <summary>
		/// Get a list of credit card accounts.
		/// </summary>
		public List<MembershipCreditCardAccount> GetCreditCardAccounts(int memberId)
		{
			#region Init
			UDI.SDS.MembershipService.SecurityToken accountingToken = _token.ToMembershipServiceToken();
			var accoutningAdapter = new AccountingServiceAdapter(accountingToken, _trackTime);
			#endregion

			var result = accoutningAdapter.GetCreditCardAccounts(memberId);
			return result;
		}

		/// <summary>
		/// Set default credit card account.
		/// </summary>
		public bool SetDefaultCreditCardAccount(int memberId, int accountId, bool isDefault)
		{
			#region Init
			UDI.SDS.MembershipService.SecurityToken accountingToken = _token.ToMembershipServiceToken();
			var accoutningAdapter = new AccountingServiceAdapter(accountingToken, _trackTime);
			#endregion

			var result = accoutningAdapter.SetDefaultCreditCardAccount(memberId, accountId, isDefault);
			return result;
		}

		/// <summary>
		/// Delete a credit card account. 
		/// </summary>
		public bool DeleteCreditCardAccount(int accountId)
		{
			#region Init
			UDI.SDS.MembershipService.SecurityToken accountingToken = _token.ToMembershipServiceToken();
			var accoutningAdapter = new AccountingServiceAdapter(accountingToken, _trackTime);
			#endregion

			var result = accoutningAdapter.DeleteCreditCardAccount(accountId);

			return result;
		}

		/// <summary>
		/// Add a direct bill account to customer profile.
		/// </summary>
		public bool AddDirectBillAccountToCustomerProfile(int memberId, string directBillAccountNumber, bool isDefault)
		{
			#region Init
			UDI.SDS.MembershipService.SecurityToken accountingToken = _token.ToMembershipServiceToken();
			UDI.SDS.PaymentsService.SecurityToken paymentToken = _token.ToPaymentsServiceToken();
			var accoutningAdapter = new AccountingServiceAdapter(accountingToken, _trackTime);
			#endregion

			var result = accoutningAdapter.AddDirectBillAccountToCustomerProfile(memberId, directBillAccountNumber, paymentToken, isDefault);

			return result;
		}

		/// <summary>
		/// Get a list of direct bill accounts.
		/// </summary>
		public List<MembershipDirectBillAccount> GetDirectBillAccounts(int memberId)
		{
			#region Init
			UDI.SDS.MembershipService.SecurityToken accountingToken = _token.ToMembershipServiceToken();
			var accoutningAdapter = new AccountingServiceAdapter(accountingToken, _trackTime);
			#endregion

			var result = accoutningAdapter.GetDirectBillAccounts(memberId);
			return result;
		}

		/// <summary>
		/// Set default direct bill account.
		/// </summary>
		public bool SetDefaultDirectBillAccount(int memberId, int accountId, bool isDefault)
		{
			#region Init
			UDI.SDS.MembershipService.SecurityToken accountingToken = _token.ToMembershipServiceToken();
			var accoutningAdapter = new AccountingServiceAdapter(accountingToken, _trackTime);
			#endregion

			var result = accoutningAdapter.SetDefaultDirectBillAccount(memberId, accountId, isDefault);
			return result;
		}

		/// <summary>
		/// Delete a direct bill account. 
		/// </summary>
		public bool DeleteDirectBillAccount(int memberId, int accountId)
		{
			#region Init
			UDI.SDS.MembershipService.SecurityToken accountingToken = _token.ToMembershipServiceToken();
			var accoutningAdapter = new AccountingServiceAdapter(accountingToken, _trackTime);
			#endregion

			var result = accoutningAdapter.DeleteDirectBillAccount(memberId, accountId);

			return result;
		}

		/// <summary>
		/// Activate zTrip credits.
		/// </summary>
		public ActivateCreditsResponse ActivateCredits(string activationCode, int memberId)
		{
			#region Init
			UDI.SDS.MembershipService.SecurityToken accountingToken = _token.ToMembershipServiceToken();
			var accoutningAdapter = new AccountingServiceAdapter(accountingToken, _trackTime);
			#endregion

			var result = accoutningAdapter.ActivateCredits(activationCode, memberId);

			return result;
		}

		/// <summary>
		/// Get zTrip Credits Balance.
		/// </summary>
		public decimal GetCreditsBalance(int memberId)
		{
			#region Init
			UDI.SDS.MembershipService.SecurityToken accountingToken = _token.ToMembershipServiceToken();
			var accoutningAdapter = new AccountingServiceAdapter(accountingToken, _trackTime);
			#endregion

			var result = accoutningAdapter.GetCreditsBalance(memberId);

			return result;
		}

		/// <summary>
		/// Get airline rewards programs
		/// </summary>
		public List<UtilityService.AirlineReward> GetAirlineRewardsPrograms()
		{
			#region Init
			UDI.SDS.UtilityService.SecurityToken accountingToken = _token.ToUtilityServiceToken();
			var accoutningAdapter = new AccountingServiceAdapter(accountingToken, _trackTime);
			#endregion

			var result = accoutningAdapter.GetAirlineRewardsPrograms();

			return result;
		}

		/// <summary>
		/// Create membership mileage account.
		/// </summary>
		public int CreateMembershipMileageAccount(int memberId, AirlineMileageAccounts mileageAccount, bool isDefault)
		{
			#region Init
			UDI.SDS.MembershipService.SecurityToken accountingToken = _token.ToMembershipServiceToken();
			var accoutningAdapter = new AccountingServiceAdapter(accountingToken, _trackTime);
			#endregion

			var result = accoutningAdapter.CreateMembershipMileageAccount(memberId, mileageAccount, isDefault);

			return result;
		}

		public bool SetDefaultMileageAccount(int memberId, int accountID, bool isDefault)
		{
			#region Init
			UDI.SDS.MembershipService.SecurityToken accountingToken = _token.ToMembershipServiceToken();
			var accoutningAdapter = new AccountingServiceAdapter(accountingToken, _trackTime);
			#endregion

			var result = accoutningAdapter.SetDefaultMileageAccount(memberId, accountID, isDefault);

			return result;
		}

		/// <summary>
		/// Delete membership mileage account.
		/// </summary>
		public bool DeleteMembershipMileageAccount(int accountId)
		{
			#region Init
			UDI.SDS.MembershipService.SecurityToken accountingToken = _token.ToMembershipServiceToken();
			var accoutningAdapter = new AccountingServiceAdapter(accountingToken, _trackTime);
			#endregion

			var result = accoutningAdapter.DeleteMembershipMileageAccount(accountId);

			return result;
		}

		/// <summary>
		/// Get the list of all registered airline rewards for a user.
		/// </summary>
		public List<MembershipService.MembershipAirlineMileageAccount> GetMembershipAirlineMileageAccounts(int memberId)
		{
			#region Init
			UDI.SDS.MembershipService.SecurityToken accountingToken = _token.ToMembershipServiceToken();
			var accoutningAdapter = new AccountingServiceAdapter(accountingToken, _trackTime);
			#endregion

			var result = accoutningAdapter.GetMembershipAirlineMileageAccounts(memberId);

			return result;
		}

		#endregion

		#region Properties
		public TrackTime TrackTime
		{
			get
			{
				return _trackTime;
			}
			set
			{
				_trackTime = value;
			}
		}

		public TokenRS Token
		{
			set
			{
				_token = value;
			}
		}
		#endregion
	}
}
