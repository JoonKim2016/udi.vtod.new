﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace UDI.SDS
{
    public class Common_Deleted
    {
        public static ILog log;
        //public Serialization serialization = new Serialization();

		public Common_Deleted()
        {
            if (log == null)
            {
                log4net.Config.XmlConfigurator.Configure();
                log = LogManager.GetLogger(typeof(Reservation));
            }         
        }
    }
}
