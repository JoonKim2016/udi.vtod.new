﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UDI.SDS.DTO
{
	public class AffiliateInfo
	{
		public string Email { get; set; }
		public string TrackingNumber { get; set; }
		public string UserID { get; set; }
	}
}
