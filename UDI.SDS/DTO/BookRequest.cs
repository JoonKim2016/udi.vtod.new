﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UDI.SDS.DTO
{
	[Serializable]
	public class BookRequest
	{
		///// <summary>
		///// Generate by system. An unique ID.
		///// </summary>
		//public Guid BookRequestID { get; set; }

		/// <summary>
		/// Generate by system. The current UTC time. Used by UDI internal tracking.
		/// </summary>
		public DateTime RequestUtcTime { get; set; }

		/// <summary>
		/// Required. Each segment contains trip details. If Segments.Count = 2, the reservation is a round trip reservation.
		/// </summary>

		public List<Segment> Segments { get; set; }

		/// <summary>
		/// Required. All the fields for save a reservation.
		/// </summary>
		public ReservationRequest ReservationRequest { get; set; }

		/// <summary>
		/// Optional. Uniquely identifies the hotel making the request via the HotelConcierge interface. 
		/// </summary>
		public int HotelConciergeID { get; set; }

		//public int MyProperty { get; set; }
		//public BookRequest()
		//{
		//	this.BookRequestID = Guid.NewGuid();
		//}
        public int FleetTripCode { get; set; }

       
    }
}
