﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.SDS.DTO
{
	public class BookResponse
	{
		//public Guid BookRequestID { get; set; }

		public DateTime ResponseUtcTime { get; set; }

		public List<ReservationResponse> ReservationResponses { get; set; }
	}
}
