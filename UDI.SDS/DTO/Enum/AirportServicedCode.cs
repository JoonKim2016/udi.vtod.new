﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.SDS.DTO.Enum
{
	public enum AirportServicedCode
	{
		//Airport with Serviced = 2 are serviced by SuperShuttle but reservations must be booked thru the local office.
		LocalOffice=2,
	}
}
