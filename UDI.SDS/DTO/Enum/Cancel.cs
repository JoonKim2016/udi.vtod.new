﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.SDS.DTO.Enum
{
	public enum Cancel
	{
		ReservationCancelled = 0,
		TooCloseToPickupTime = 1,
		ReservationAlreadyCancelled = 2,
		ReservationPickupTimeHasExpiredOrCannotBeCancelled = 3
	}
}
