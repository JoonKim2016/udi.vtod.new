﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.SDS.DTO.Enum
{
	public enum CharterType
	{
		PointToPoint=0, 
		Hourly=1,
	}
}
