﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.SDS.DTO.Enum
{
	public class Defaults
	{
		//According to SuperShuttle's business logic, -2147483648 is set for non-landmark ID.
		//2013_09_21 10:00 AM Doug ask me to change -2147483648 to 0 for default
		public const int MasterLandmarkID = 0; //-2147483648;
		
		public const int LandmarkID = 0; //-2147483648;

		public const string LocationType = "R";

		public const string LocationName = "";

		public const string SubZipName = "";

		public const string CharterAirport = "CHT";

		public const string Country = "US";
	}
}
