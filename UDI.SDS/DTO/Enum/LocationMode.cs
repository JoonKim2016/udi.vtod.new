﻿using System;

namespace UDI.SDS.DTO.Enum
{
    public enum LocationMode
    {
        Pickup = 0,
        Dropoff = 1
    }
}
