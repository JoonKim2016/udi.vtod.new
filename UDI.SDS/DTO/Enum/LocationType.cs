﻿using System;

namespace UDI.SDS.DTO.Enum
{
	public enum LocationType
	{
		Address = 0,
		AsDirected = 1,
		Airport = 2
	}

}
