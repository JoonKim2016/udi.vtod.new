﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.SDS.DTO.Enum
{
	/// <summary>
	/// Used as SubType of vtod_log table, to distinguish different type of requests in the log table.
	/// Added by Lin on 04/11/2013.
	/// </summary>
	public enum LogType
	{
		Avail_Rate,
		Avail_PUTime,
		Book,
		Cancel,
		ResRetrieve,
        Accounting,
	}
}
