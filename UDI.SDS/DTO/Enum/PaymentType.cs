﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.SDS.DTO.Enum
{
	public enum PaymentType
	{
		Unknown = -1,
		Cash = 0,
		PaymentCard = 1,
		DirectBill = 2,
		PaymentCardHold = 5,
	}
}
