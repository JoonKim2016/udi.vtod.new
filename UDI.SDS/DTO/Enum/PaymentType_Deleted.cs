﻿using System;

namespace UDI.SDS.DTO.Enum
{
    public enum PaymentType_Deleted
    {
        CreditCard = 1,
        DirectBill = 2,
    }
}
