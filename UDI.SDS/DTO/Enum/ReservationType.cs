﻿using System;

namespace UDI.SDS.DTO.Enum
{
    public enum ReservationType
    {
        AirportReservationForLandmark = 0,  //landmark to Airport|Airport to landmark
        AirportReservationForZipcode = 1,   //Zipcode to Airport|Airport to zipcode
        PointToPointCharter = 2,    //Address to address
        HourlyCharter = 3,   //to AsDirected
        Unknown = -1,
    }
}
