﻿using System;

namespace UDI.SDS.DTO.Enum
{
	public enum SDSFleet
	{
		All = 0,
		SuperShuttle = 1,
		ExecuCar = 2,
		Taxi = 3,

		/// <summary>
		/// SuperShuttleSharedRideOnly is one of SuperShuttle Fleet. Added by Lin on 6/21/2013
		/// </summary>
		SuperShuttleSharedRideOnly,
		Unknown,
	}
}
