﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using UDI.SDS.DTO.Enum;

namespace UDI.SDS.DTO
{
	[Serializable]
	public class Location
	{
		/// <summary>
		/// Enum LocationType: Airport, Address, AsDirected
		/// </summary>
		[XmlAttribute("type")]
		public LocationType type;

		/// <summary>
		/// Enum LocationMode: Pickup, Dropoff
		/// </summary>
		[XmlAttribute("mode")]
		public LocationMode mode;

		public string LocationName { get; set; }

		public string AiportCode { get; set; }

		public string SecondryAiportCode { get; set; }

		public string Country { get; set; }

		public string StateOrProvince { get; set; }

		public string City { get; set; }

		public string PostalCode { get; set; }

		public string StreetName { get; set; }

		public string StreetNumber { get; set; }

		public string Unit { get; set; }

		public string PropertyName { get; set; }

		public int MasterLandmarkID { get; set; }

		public int LandmarkID { get; set; }

		public decimal Latitude { get; set; }

		public decimal Longitude { get; set; }

		public string LocationType { get; set; }

		public string SubZipName { get; set; }

        //public string BldgRoom { get; set; }

		public Location(LocationType type, LocationMode mode)
		{
			this.type = type;
			this.mode = mode;
		}

		public Location()
		{
			this.MasterLandmarkID = (int) UDI.SDS.DTO.Enum.Defaults.MasterLandmarkID;
		}
	}
}
