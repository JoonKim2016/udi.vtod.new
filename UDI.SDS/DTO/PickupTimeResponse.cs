﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UDI.SDS.DTO
{

	public class PickupTimeResponse
	{
		public Guid BookRequestID { get; set; }

		/// <summary>
		/// This value will be set by GetPickupTime method. The available pickup time provided by SuperShuttle.
		/// </summary>
		public DateTime AvailablePickupTime { get; set; }

		/// <summary>
		/// This value will be set by GetPickupTime method. The available pickup end time provided by SuperShuttle.
		/// </summary>
		public DateTime AvailablePickupEndTime { get; set; }

		/// <summary>
		/// This value will be set by GetPickupTime method. Indicate guaranteed pickup time provided by SuperShuttle.
		/// </summary>
		public bool IsGuaranteed { get; set; }

		/// <summary>
		/// This value will be set by GetPickupTime method. The ASAP Request ID.
		/// </summary>
		public int AsapRequestId { get; set; }

		/// <summary>
		/// A Boolean value indicates the pickup time is a scheduled pickup for a landmark with scheduled service. 
		/// </summary>
		public bool IsScheduledStop { get; set; }
	}
}
