﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UDI.SDS.DTO.Enum;
using UDI.SDS.RatesService;
using UDI.VTOD.Common.DTO.Enum;

namespace UDI.SDS.DTO
{

	public class RateResponse
	{
		public Guid BookRequestID { get; set; }

		/// <summary>
		/// Default -1. This value will be set by GetRates method. Indicates the reservation request is to the airport (0), or from the airport (1). 
		/// </summary>
		public TripDirection TripDirection { get; set; }

		/// <summary>
		/// The lowest rate. Default null. This value will be set by GetRates method. 
		/// Booking method will only use this value for airport booking.
		/// </summary>
		public RatesService.ServiceTypeRecord AirportReservationRate { get; set; }

		/// <summary>
		/// The lowest rate. Default null. This value will be set by GetRates method.
		/// Booking method will only use this value for charter booking.
		/// </summary>
		public RatesService.CharterServiceTypeRecord CharterRate { get; set; }

		/// <summary>
		/// This it for new method: "GetServiceRates" to get all kind of rates for LandMark and Zip
		/// </summary>
		public List<ServiceInfo> ServiceInfoRates { get; set; }

		/// <summary>
		/// Default null. Return all ariport reservation rates.
		/// </summary>
		public List<RatesService.ServiceTypeRecord> AirportReservationRates { get; set; }

		/// <summary>
		/// Default null. Return all charter reservation rates.
		/// </summary>
		public List<RatesService.CharterServiceTypeRecord> CharterRates { get; set; }

		/// <summary>
		/// Updated MasterLandmarkID, LocationType and SubZipName
		/// </summary>
		public Segment Segment { get; set; }

	}
}
