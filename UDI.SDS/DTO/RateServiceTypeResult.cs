﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.SDS.DTO.Enum;
using UDI.SDS.RatesService;
using UDI.SDS.ZipCodesService;

namespace UDI.SDS.DTO
{
	public class RateServiceTypeResult
	{
		public ZipCodeSplitRecord ZipCodeSplitRecord { get; set; }

		public ServiceTypes ServiceTypes { get; set; }

		public ReservationType ReservationType { get; set; }

		public bool IsZipSplit { get; set; }
	}
}
