﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UDI.SDS.DTO.Enum;

namespace UDI.SDS.DTO
{

	public class ReservationRequest
	{
		/// <summary>
		/// Required. The guest's cell phone number or day of travel contact number. 
		/// </summary>
		public string ContactNumber { get; set; }

		/// <summary>
		/// Required. The ContactNumber international dialing prefix. 
		/// </summary>
		public string ContactNumberDialingPrefix { get; set; }

		/// <summary>
		/// Optional. Comments regarding the reservation, should include major cross streets. 
		/// </summary>
		public string Comments { get; set; }

		/// <summary>
		/// Optional. The guest's email address for receiving email confirmations. 
		/// </summary>
		public string EmailAddress { get; set; }

		/// <summary>
		/// Required. The guest's first name. 
		/// </summary>
		public string FirstName { get; set; }

		/// <summary>
		/// Optional. Identifies the reservation as part of a group. 
		/// </summary>
		public int GroupID { get; set; }

		/// <summary>
		/// Optional. default = 0
		/// iPhone should pass memberID
		/// </summary>
		public int MemberID { get; set; }

		/// <summary>
		/// Required. The guest's last name. 
		/// </summary>
		public string LastName { get; set; }

		/// <summary>
		/// Optional. The luggage count the guest will be traveling with. 
		/// </summary>
		public int LuggageCount { get; set; }

		/// <summary>
		/// Required. Should be set to the WCF user name credential unless instructed otherwise.
		/// </summary>
		public string UserName { get; set; }

		/// <summary>
		/// Optional. The web affiliate code for the reservation. 
		/// </summary>
		public string SID { get; set; }

		/// <summary>
		/// Optional. Agent comments specific to the customer, not the reservation. 
		/// </summary>
		public string ServiceNotes { get; set; }

		/// <summary>
		/// Optional. Default false. Indicates the reservation requires special handling (ie skis, boxes, excess luggage). 
		/// </summary>
		public bool SpecialHandling { get; set; }

		/// <summary>
		/// Required.
		/// </summary>
		public decimal Gratuity { get; set; }

		/// <summary>
		/// Required.
		/// </summary>
		public GratuityType GratuityType { get; set; }
		
		/// <summary>
		/// Optional. 5 character alpha-numeric discount code provided by the guest. 
		/// </summary>
		public string DiscountCode { get; set; }

		/// <summary>
		/// Required. PaymentType (1 = credit card, 2 = direct bill account)
		/// </summary>
		public ReservationsServiceSecure.Payment Payment { get; set; }

		/// <summary>
		/// Show if customer needs to be picked up right away
		/// </summary>
		public bool? pickMeUpNow { get; set; }

        public bool? IsAccessible { get; set; }
        public bool? IsInboundRideNow { get; set; }
        public bool? IsOutboundRideNow { get; set; }

        public bool? SubscribeToPromotions { get; set; }

		public bool? AllowSMS { get; set; }

		//97909; is the numeric program identifier (us airways, southwest, delta etc)
		public int? RewardsID { get; set; }

		// 324579; is the customers account number with the airline
		public string RewardsAccountNumber { get; set; }

		public AffiliateInfo AffiliateInfo { get; set; }

		public string RequestVehicleNumber { get; set; }

        public string ReserveCsr { get; set; }

        public string CultureCode { get; set; }
    }
}
