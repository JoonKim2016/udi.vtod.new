﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UDI.SDS.DTO
{
	public class ReservationResponse
	{
		public List<Segment> Segments { get; set; }

		public List<ReservationsServiceSecure.ReservationConfirmation> AirportReservationConfirmations { get; set; }

		public ReservationsServiceSecure.CharterReservationConfirmation CharterReservationConfirmation { get; set; }
	}
}
