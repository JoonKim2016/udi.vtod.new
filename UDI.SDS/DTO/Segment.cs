﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using UDI.SDS.DTO.Enum;
using UDI.VTOD.Common.DTO.Enum;

namespace UDI.SDS.DTO
{
	/// <summary>
	/// Segment contains trip details.
	/// </summary>
	[Serializable]
	public class Segment
	{
		TripDirection tripDirection = TripDirection.Unknown;

		/// <summary>
		/// Required. Over 3 years old Passengers.
		/// </summary>
		public int PayingPax { get; set; }

		/// <summary>
		/// Optional. Under 3 years old passenger need Child Seat.
		/// </summary>
		public int ChildSeats { get; set; }

		/// <summary>
		/// Optional. Infant Seat.
		/// </summary>
		public int InfantSeats { get; set; }

		/// <summary>
		/// Optional. 
		/// </summary>
		public int Wheelchairs { get; set; }

		/// <summary>
		/// Required. The fleet value for the service selected by the guest.
		/// Enum Fleet: All = 0, SuperShuttle = 1, ExecuCar = 2
		/// </summary>
		public SDSFleet RequestedFleet { get; set; }

		/// <summary>
		/// Required for Hourly Charter.
		/// </summary>
		public int CharterMinutes { get; set; }

		/// <summary>
		/// Required. The pickup and dropoff locations.
		/// </summary>
		public List<Location> Locations { get; set; }

		/// <summary>
		/// Required for Charter Reservation. The desired pickup time provided by customer.
		/// </summary>
		public DateTime RequestPickupTime { get; set; }

		/// <summary>
		/// Required for airport reservation. The customer's flight arrival or departure time. 
		/// </summary>
		public DateTime FlightDateTime { get; set; }

		/// <summary>
		/// Required for airport reservation. Default value is false.
		/// </summary>
		public bool IsInternationalFlight { get; set; }

		/// <summary>
		/// Required for Outbound Airport Reservation
		/// </summary>
		public string FlightNumber { get; set; }

		/// <summary>
		/// Required for Outbound Airport Reservation
		/// </summary>
		public string AirlineCode { get; set; }

		/// <summary>
		/// Required.
		/// </summary>
		public bool IsRoundTrip { get; set; }

		/// <summary>
		/// default is Unknown
		/// </summary>
		public TripDirection TripDirection
		{
			get
			{
				return tripDirection;
			}
			set
			{
				tripDirection = value;
			}
		}


        /// <summary>
		/// Required for modifyTrip
		/// </summary>
        public string ConfirmationId { get; set; }
        public string AffiliateProviderId { get; set; }

        public byte IsInfantSeats { get; set; }

        public byte IsChildSeats { get; set; }
    }
}
