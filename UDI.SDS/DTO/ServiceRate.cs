﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.SDS.DTO
{
	/// <summary>
	/// This DTO is to unify the airportRate and CharterRate
	/// </summary>
	public class ServiceRate
	{
		/// <summary>
		/// FareID
		/// </summary>
		public int ID { get; set; }

		/// <summary>
		/// Fleet
		/// </summary>
		public string Fleet { get; set; }

		/// <summary>
		/// ServiceTypeCode
		/// </summary>
		public string Service { get; set; }
	}
}
