﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.SDS.DTO
{
	public class UserInfo
	{
		public string UserName { get; set; }

		public bool AllowClosedSplits { get; set; }

		public bool ShowSplits { get; set; }

		public int PickupTimeApproach { get; set; }
	}
}
