﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UDI.SDS.DTO.WCF
{
	public class Point 
	{
		public double Course { get; set; }
		public double Latitude { get; set; }
		public double Longitude { get; set; }
		public int VehicleNumber { get; set; }
		public int VehicleType { get; set; }
	}
}
