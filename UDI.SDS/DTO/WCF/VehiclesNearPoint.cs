﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.SDS.DTO.WCF
{
	public class VehiclesNearPoint
	{
		public double DistanceToPoint { get; set; }
		public int MinutesToClosestSedan { get; set; }
		public int MinutesToClosestShuttle { get; set; }
		public int MinutesToClosestTaxi { get; set; }
		public int MinutesToPoint { get; set; }
		public int SedansNearPoint { get; set; }
		public int ShuttlesNearPoint { get; set; }
		public int TaxisNearPoint { get; set; }
		public List<Point> VehicleLocations { get; set; }
	}
}
