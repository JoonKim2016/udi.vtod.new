﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.SDS.Helper;
using UDI.SDS.SecurityService;
using System.Configuration;
using UDI.VTOD.Common.Helper;
using UDI.Utility.Serialization;
using UDI.VTOD.Common.Abstract;
using UDI.SDS.DTO;
using UDI.SDS.DTO.Enum;
using UDI.VTOD.Common.Track;
using UDI.VTOD.Common.DTO.Enum;

namespace UDI.SDS
{
	/// <summary>
	/// Business logics for getting pickup times
	/// PickupTimes will be called by Reservation
	/// </summary>
	internal class PickupTimeService_Deleted : BaseSetting //Common
	{
		private SecurityToken token = null;
		private int asapRequestID = 0;
		private bool isGuaranteed = false;
		private bool isScheduledStop = false;
		private Guid bookRequestID;

		#region Constructor

		internal PickupTimeService_Deleted(SecurityToken token)
		{
			this.token = token;
		}

		#endregion

		#region Internal methods
		#region Deleted
		///// <summary>
		///// Return a list of PickupTimeRecord objects.
		///// </summary>
		//internal List<ReservationsService.PickupTimeRecord> GetPickupTimes(Segment segment, RateResponse rateResponse, ServiceRate serviceRate, int hotelConciergeID)
		//{
		//	try
		//	{
		//		this.bookRequestID = rateResponse.BookRequestID; ;
		//		var response = new List<ReservationsService.PickupTimeRecord>();

		//		if (rateResponse.TripDirection == TripDirection.Inbound || rateResponse.TripDirection == TripDirection.Outbound)
		//		{
		//			#region Init
		//			var masterLandmarkID = 0;
		//			var airportCode = string.Empty;
		//			bool isAccessibleServiceRequired = segment.Wheelchairs > 0 ? true : false;

		//			foreach (var addressLocation in segment.Locations)
		//			{
		//				if (addressLocation.type == LocationType.Address)
		//					masterLandmarkID = addressLocation.masterLandmarkID;

		//				if (addressLocation.type == LocationType.Airport)
		//					airportCode = addressLocation.AiportCode;
		//			}
		//			#endregion

		//			if (rateResponse.TripDirection == TripDirection.Inbound && segment.RequestPickupTime != segment.FlightDateTime && !string.IsNullOrWhiteSpace(serviceRate.Fleet) && serviceRate.Fleet.Trim().ToLower() == "ecar")
		//			{
		//				#region Validate Pickuptime
		//				ReservationsService.PickupTimeRecord pickupTimeRecord;// = new ReservationsService.PickupTimeRecord();

		//				var validate = ValidatePickupTime(segment, rateResponse, hotelConciergeID, out pickupTimeRecord);
		//				if (validate)
		//				{
		//					response.Add(pickupTimeRecord);
		//					return response;
		//				}
		//				#endregion
		//			}

		//			#region Get Pickup Time
		//			/** All airport reservations needs to call GetPickupTimes() to determine if the reservation can be serviced **/
		//			response = getPickupTimes(isAccessibleServiceRequired, hotelConciergeID, segment.IsInternationalFlight, masterLandmarkID, serviceRate, segment.FlightDateTime, (int)rateResponse.TripDirection, airportCode, segment.ChildSeats, segment.PayingPax);
		//			#endregion
		//		}
		//		else
		//		{
		//			/* return requested pickup time */
		//			var pickupTimeRecord = new ReservationsService.PickupTimeRecord();
		//			pickupTimeRecord.StartTime = segment.RequestPickupTime;
		//			//pickupTimeRecord.EndTime = segment.RequestPickupTime;
		//			response.Add(pickupTimeRecord);
		//		}

		//		return response;
		//	}
		//	catch (Exception ex)
		//	{
		//		logger.ErrorFormat("GetPickupTimes {0}", ex.StackTrace);
		//		throw ex;
		//	}
		//} 
		#endregion

		/*
		internal bool ValidatePickupTime(Segment segment, RateResponse rateResponse, int hotelConciergeID, out ReservationsService.PickupTimeRecord pickupTime)
		{
			try
			{
				var result = false;
				pickupTime = new ReservationsService.PickupTimeRecord();

				logger.InfoFormat("Send to SuperShuttle \r\n tripDirection={0}, pickupTime={1}, hotelConciergeID={2}, serviceTypeID={3}", rateResponse.TripDirection, segment.RequestPickupTime, hotelConciergeID, rateResponse.AirportReservationRate.FareID);
				using (var client = new ReservationsService.ReservationsServiceClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Domain, "Call");
					#endregion

					var validateResult = client.ValidatePickupTime(token.ToReservationsServiceToken(), (int)rateResponse.TripDirection, segment.RequestPickupTime, hotelConciergeID, rateResponse.AirportReservationRate.FareID);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "ValidatePickupTime");
					#endregion

					if (validateResult == ReservationsService.PickupTimeStatusCodesEnumeration.AllowPickup)
					{
						pickupTime.StartTime = segment.RequestPickupTime;
						//For Ecar, we do not provide end time (there is not any window)
						pickupTime.EndTime = segment.RequestPickupTime;
						result = true;
					}
				}
				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call ReservationsService.ValidatePickupTime {0}", ex.StackTrace);
				throw ex;
			}
		}
		*/
		#endregion

		#region Private methods
		#region Deleted
		///// <summary>
		///// Get a list of Guarantee Times from SuperShuttle
		///// </summary>
		//private List<ReservationsService.PickupTimeRecord> getPickupTimes(bool isAccessibleServiceRequired, int hotelConciergeID, bool isInternationalFlight, int masterLandmarkID, ServiceRate serviceRate, DateTime flightTime, int tripDirection, string airportCode, int childSeats, int payingPax)
		//{
		//	try
		//	{
		//		var errorMsg = string.Empty;
		//		var pickupTimes = new List<ReservationsService.PickupTimeRecord>();
		//		ReservationsService.PickupTimeRequest request = new ReservationsService.PickupTimeRequest();
		//		request.AccessibleServiceRequired = isAccessibleServiceRequired;
		//		request.HotelConciergeID = hotelConciergeID;
		//		request.IsInternationalFlight = isInternationalFlight;
		//		request.MasterLandmarkID = masterLandmarkID == -2147483648 ? 0 : masterLandmarkID; //use zero for MasterLandmarkId if there's no landmark
		//		request.ServiceTypeID = serviceRate.ID;
		//		request.TravelTime = flightTime;
		//		request.TripDirection = tripDirection;

		//		logger.InfoFormat("Send to SuperShuttle \r\n{0}", request.XmlSerialize());
		//		using (var client = new ReservationsService.ReservationsServiceClient())
		//		{
		//			#region Track
		//			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Domain, "Call");
		//			#endregion

		//			var result = client.GetPickupTime(token.ToReservationsServiceToken(), request);

		//			#region Track
		//			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetPickupTime");
		//			#endregion

		//			if (result.HasRecords)
		//			{
		//				pickupTimes = result.PickupTimeRecordsArray.ToList();
		//			}
		//			else
		//			{
		//				errorMsg = "GetPickupTimes";
		//			}
		//		}

		//		if (!string.IsNullOrEmpty(errorMsg))
		//			throw new Exception(string.Format(Messages.UDI_SDS_ASAP_NoPickupTimeFound, errorMsg));

		//		return pickupTimes;
		//	}
		//	catch (Exception ex)
		//	{
		//		logger.ErrorFormat("Call ReservationsService.GetPickupTime {0}", ex.StackTrace);
		//		throw ex;
		//	}
		//} 
		#endregion

		//private void ValidatePickupTime

		#endregion

		#region Properties
		public TrackTime TrackTime { get; set; }
		#endregion
	}
}
