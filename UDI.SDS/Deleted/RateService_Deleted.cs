﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.SDS.Helper;
using UDI.SDS.SecurityService;
using System.Configuration;
using UDI.VTOD.Common.Helper;
using UDI.VTOD.Common.DTO;
using UDI.Utility.Serialization;
using UDI.VTOD.Common.Abstract;
using UDI.SDS.DTO;
using UDI.SDS.DTO.Enum;
using UDI.VTOD.Common.Track;
using UDI.VTOD.Common.DTO.Enum;
using UDI.SDS.Helper;

//using UDI.SDS.ZipCodesService;

namespace UDI.SDS
{
	/// <summary>
	/// Business logics for getting rates
	/// Rates will be called by Reservation.
	/// </summary>
	internal class RateService_Deleted : BaseSetting // Common
	{
		private SecurityToken token = null;
		private UserInfo userInfo = new UserInfo { AllowClosedSplits = false, ShowSplits = false };
		private Guid bookRequestID;

		#region Constructor
		private RateService_Deleted()
		{
		}

		internal RateService_Deleted(SecurityToken token)
		{
			this.token = token;
		}

		internal RateService_Deleted(SecurityToken token, UserInfo userInfo)
		{
			this.token = token;
			this.userInfo = userInfo;
		}
		#endregion

		#region Deleted
		#region internal methods
		///// <summary>
		///// Get the lowest rate for a segment from SuperShuttle
		///// </summary>
		//internal RateResponse GetRate(Guid bookRequestID, Segment segment, string discountCode)
		//{
		//	try
		//	{
		//		#region Initial variables
		//		discountCode = discountCode.Trim();
		//		this.bookRequestID = bookRequestID;
		//		RateResponse response = new RateResponse();

		//		var tripDirection = TripDirection.Unknown;
		//		var reservationType = ReservationType.Unknown;
		//		List<RatesService.ServiceTypeRecord> airportRate = null;
		//		List<RatesService.CharterServiceTypeRecord> charterRate = null;
		//		Location pickupLocation = null;
		//		Location dropoffLocation = null;
		//		int masterLandmarkID = -2147483648;
		//		bool isSuperShuttleSharedRideOnly = false;//If true, then only return Fleet = VAN.
		//		#endregion

		//		#region Set Fleet Filter
		//		int fleetFilter;
		//		if (segment.RequestedFleet == Fleet.ExecuCar)
		//			fleetFilter = (int)Fleet.ExecuCar;
		//		else if (segment.RequestedFleet == Fleet.SuperShuttle)
		//			fleetFilter = (int)Fleet.SuperShuttle;
		//		else if (segment.RequestedFleet == Fleet.SuperShuttleSharedRideOnly)
		//		{
		//			fleetFilter = (int)Fleet.SuperShuttle;
		//			isSuperShuttleSharedRideOnly = true;
		//		}
		//		else
		//			fleetFilter = (int)Fleet.All;
		//		#endregion

		//		#region Set pickupLocation, droppffLocation
		//		foreach (var location in segment.Locations)
		//		{
		//			UDI.SDS.Helper.Utility.CheckLocationRequiredFields(location);
		//			if (location.mode == LocationMode.Pickup)
		//			{
		//				pickupLocation = new Location(location.type, location.mode);
		//				pickupLocation = location;
		//			}
		//			else
		//			{
		//				dropoffLocation = new Location(location.type, location.mode);
		//				dropoffLocation = location;
		//			}
		//		}
		//		#endregion

		//		#region Get Rates
		//		/********* Hourly Charter *********/
		//		if (dropoffLocation.type == LocationType.AsDirected)
		//		{
		//			reservationType = ReservationType.HourlyCharter;
		//			charterRate = new List<RatesService.CharterServiceTypeRecord>();
		//			charterRate = GetHourlyCharterRates(fleetFilter, pickupLocation.PostalCode, segment.CharterMinutes, segment.PayingPax, segment.Wheelchairs, segment.ChildSeats);

		//			if (charterRate != null && charterRate.FirstOrDefault() != null)
		//			{
		//				var landmarkRecord = getLandmarkRecord(charterRate.FirstOrDefault().FranchiseCode, pickupLocation);
		//				pickupLocation.masterLandmarkID = getMasterLandMarkID(landmarkRecord);
		//				pickupLocation.SubZipName = getSubZipNameFromLandmarkRecord(landmarkRecord);
		//				pickupLocation.PropertyName = getLocationNameFromLandmarkRecord(landmarkRecord);

		//				if (string.IsNullOrEmpty(pickupLocation.LocationType))
		//					pickupLocation.LocationType = getLocationType(landmarkRecord);
		//			}
		//		}
		//		/********* Airport Reservation - Outbound *********/
		//		else if (pickupLocation.type == LocationType.Airport)
		//		{
		//			//if (segment.IsRoundTrip)
		//			//{
		//			//	tripDirection = TripDirection.RoundTrip;
		//			//}
		//			//else
		//			{
		//				if (tripDirection == TripDirection.Unknown)
		//					tripDirection = TripDirection.Outbound; //From airport 
		//			}

		//			isAirportServicedOnline(pickupLocation.AiportCode);

		//			//1. Check if dropoff location is Airport too, then treat it as address
		//			if (dropoffLocation.type == LocationType.Airport)
		//			{
		//				dropoffLocation.type = LocationType.Address;
		//				checkLocationRequiredFields(dropoffLocation);
		//			}

		//			//2.Check it's Airport to Landmark or Zipcode
		//			if (dropoffLocation.type == LocationType.Address)
		//			{
		//				var landmarkRecord = getLandmarkRecord(pickupLocation.AiportCode, dropoffLocation);
		//				dropoffLocation.masterLandmarkID = getMasterLandMarkID(landmarkRecord);
		//				dropoffLocation.SubZipName = getSubZipNameFromLandmarkRecord(landmarkRecord);
		//				dropoffLocation.LocationType = getLocationType(landmarkRecord);
		//				dropoffLocation.PropertyName = getLocationNameFromLandmarkRecord(landmarkRecord);

		//				if (dropoffLocation.masterLandmarkID != -2147483648)
		//				{
		//					masterLandmarkID = dropoffLocation.masterLandmarkID;
		//					reservationType = ReservationType.AirportReservationForLandmark;
		//				}
		//				else
		//					reservationType = ReservationType.AirportReservationForZipcode;
		//			}

		//			airportRate = new List<RatesService.ServiceTypeRecord>();
		//			airportRate = getAirportReservationRates(pickupLocation.AiportCode, dropoffLocation, (int)tripDirection, reservationType, fleetFilter, segment.PayingPax, segment.Wheelchairs, segment.ChildSeats, discountCode, isSuperShuttleSharedRideOnly);
		//		}
		//		/********* Airport Reservation - Inbound *********/
		//		else if (dropoffLocation.type == LocationType.Airport)
		//		{
		//			//if (segment.IsRoundTrip)
		//			//{
		//			//	tripDirection = TripDirection.RoundTrip;
		//			//}
		//			//else
		//			{
		//				if (tripDirection == TripDirection.Unknown)
		//					tripDirection = TripDirection.Inbound; //To airport 
		//			}

		//			isAirportServicedOnline(dropoffLocation.AiportCode);

		//			//Check it's Landmark or Zipcode to Airport
		//			if (pickupLocation.type == LocationType.Address)
		//			{
		//				var landmarkRecord = getLandmarkRecord(dropoffLocation.AiportCode, pickupLocation);
		//				pickupLocation.masterLandmarkID = getMasterLandMarkID(landmarkRecord);
		//				pickupLocation.SubZipName = getSubZipNameFromLandmarkRecord(landmarkRecord);
		//				pickupLocation.LocationType = getLocationType(landmarkRecord);
		//				pickupLocation.PropertyName = getLocationNameFromLandmarkRecord(landmarkRecord);

		//				if (pickupLocation.masterLandmarkID != -2147483648)
		//				{
		//					masterLandmarkID = pickupLocation.masterLandmarkID;
		//					reservationType = ReservationType.AirportReservationForLandmark;
		//				}
		//				else
		//					reservationType = ReservationType.AirportReservationForZipcode;
		//			}

		//			airportRate = new List<RatesService.ServiceTypeRecord>();
		//			airportRate = getAirportReservationRates(dropoffLocation.AiportCode, pickupLocation, (int)tripDirection, reservationType, fleetFilter, segment.PayingPax, segment.Wheelchairs, segment.ChildSeats, discountCode, isSuperShuttleSharedRideOnly);
		//		}
		//		/********* Point to Point Charter *********/
		//		else
		//		{
		//			reservationType = ReservationType.PointToPointCharter;
		//			charterRate = new List<RatesService.CharterServiceTypeRecord>();
		//			charterRate = GetPointToPointCharterRates(fleetFilter, segment.Locations.ToList(), segment.PayingPax, segment.Wheelchairs, segment.ChildSeats);

		//			if (charterRate != null && charterRate.FirstOrDefault() != null)
		//			{
		//				var landmarkRecord = getLandmarkRecord(charterRate.FirstOrDefault().FranchiseCode, pickupLocation);
		//				pickupLocation.masterLandmarkID = getMasterLandMarkID(landmarkRecord);
		//				pickupLocation.SubZipName = getSubZipNameFromLandmarkRecord(landmarkRecord);
		//				pickupLocation.LocationType = getLocationType(landmarkRecord);
		//				pickupLocation.PropertyName = getLocationNameFromLandmarkRecord(landmarkRecord);

		//				var landmarkRecord2 = getLandmarkRecord(charterRate.FirstOrDefault().FranchiseCode, dropoffLocation);
		//				dropoffLocation.masterLandmarkID = getMasterLandMarkID(landmarkRecord2);
		//				dropoffLocation.SubZipName = getSubZipNameFromLandmarkRecord(landmarkRecord2);
		//				dropoffLocation.PropertyName = getLocationNameFromLandmarkRecord(landmarkRecord);

		//				if (string.IsNullOrEmpty(dropoffLocation.LocationType))
		//					dropoffLocation.LocationType = getLocationType(landmarkRecord2);
		//			}
		//		}

		//		#endregion

		//		#region Update location's masterlandmarkID, locationType, locationName and subzipname
		//		for (int i = 0; i < segment.Locations.Count; i++)
		//		{
		//			if (segment.Locations[i].mode == LocationMode.Pickup)
		//			{
		//				segment.Locations[i] = pickupLocation;
		//			}
		//			else
		//			{
		//				segment.Locations[i] = dropoffLocation;
		//			}
		//		}
		//		#endregion

		//		#region Set RateResponse
		//		response.BookRequestID = bookRequestID;
		//		response.AirportReservationRates = airportRate;
		//		response.CharterRates = charterRate;
		//		response.AirportReservationRate = airportRate != null ? airportRate.FirstOrDefault() : null;
		//		response.CharterRate = charterRate != null ? charterRate.FirstOrDefault() : null;
		//		response.TripDirection = tripDirection;
		//		response.Segment = segment;
		//		#endregion

		//		return response;
		//	}
		//	catch (Exception ex)
		//	{
		//		throw ex;
		//	}
		//}

		/*
		internal Location GetGeoCodeForAddress(Location location)
		{
			try
			{
				GeocoderService.AddressRecord addressRecord = new GeocoderService.AddressRecord();

				addressRecord.PostalCode = location.PostalCode;
				addressRecord.StreetName = location.StreetName;
				addressRecord.StreetNumber = location.StreetNumber;
				addressRecord.CountrySubDivision = location.StateOrProvince;
				addressRecord.Municipality = location.City;
				addressRecord.CountryCode = string.IsNullOrEmpty(location.Country) ? "US" : location.Country;
				addressRecord.Latitude = location.Latitude;
				addressRecord.Longitude = location.Longitude;

				logger.InfoFormat("Send to SuperShuttle \r\n{0}", addressRecord.XmlSerialize());


				using (var client = new GeocoderService.GeocoderClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Domain, "Call");
					#endregion

					var result = client.GeocodeAddress(token.ToGeocoderServiceToken(), addressRecord);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GeocodeAddress");
					#endregion

					if (result.HasMatches)
					{
						//get the highest match record
						var maxAccuracy = result.AddressMatchesArray.Max(a => a.Accuracy);
						var matchedAddress = result.AddressMatchesArray.Where(r => r.Accuracy == maxAccuracy).FirstOrDefault();
						location.Latitude = matchedAddress.Latitude;
						location.Longitude = matchedAddress.Longitude;
					}
					//else
					//{
					//	throw new Exception(string.Format(Messages.UDI_SDS_NoGeoCodeFound, "GetGeoCodeForAddress", location.mode));
					//}
				}
				
				return location;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call GeocoderService.GeocodeAddress {0}", ex.StackTrace);
				throw ex;
			}
		}
		*/
		#endregion 
		#endregion

		#region Deleted
		//#region private methods for Airport Reservation rates
		///// <summary>
		///// Get all rates for Airport Reservation.
		///// If no matching record found, return NULL.
		///// </summary>
		//private List<RatesService.ServiceTypeRecord> getAirportReservationRates(string airportCode, Location addressLocation, int tripDirection, ReservationType reservationType, int fleetFilter, int payingPax, int wheelchairs, int FreePax, string discountCode, bool isSuperShuttleSharedRideOnly)
		//{
		//	List<RatesService.ServiceTypeRecord> airportRate = new List<RatesService.ServiceTypeRecord>();
		//	//var result = new RatesService.ServiceTypes();
		//	var results = new List<RateServiceTypeResult>();
		//	var franchiseCode = getFranchiseCode(airportCode, addressLocation.PostalCode, tripDirection);
		//	var isZipSplit = false;

		//	logger.InfoFormat("GetAirportReservationRates Start...\r\n AirportCode:{0},Address:{1},tripDirection:{2},reservationType:{3},fleetFilter:{4},payingPax:{5},wheelchairs:{6}, franchiseCode:{7}", airportCode, addressLocation.XmlSerialize(), tripDirection, reservationType, fleetFilter, payingPax, wheelchairs, franchiseCode);

		//	#region Get all rates
		//	//GetLandmarkFares
		//	if (reservationType == ReservationType.AirportReservationForLandmark)
		//	{
		//		/* Get fares for airport reservation with landmark */
		//		//result = GetLandmarkRates(airportCode, fleetFilter, franchiseCode, addressLocation.masterLandmarkID, tripDirection, addressLocation.PostalCode, payingPax, discountCode);
		//		var landMarkResult = getLandmarkRates(airportCode, fleetFilter, franchiseCode, addressLocation.masterLandmarkID, tripDirection, addressLocation.PostalCode, payingPax, discountCode);
		//		if (landMarkResult != null && landMarkResult.HasRecords)
		//		{
		//			results.Add(new RateServiceTypeResult { ReservationType = ReservationType.AirportReservationForLandmark, ServiceTypes = landMarkResult });
		//		}
		//	}
		//	//GetZipCodeFares
		//	else if (reservationType == ReservationType.AirportReservationForZipcode)
		//	{
		//		List<ZipCodesService.ZipCodeSplitRecord> splitNames = new List<ZipCodesService.ZipCodeSplitRecord>();

		//		//get subZipName which is splitName
		//		splitNames = getSplitNamesOfServicedPostalCode(airportCode, addressLocation.PostalCode, tripDirection, out isZipSplit);

		//		foreach (var ZipCodeSplitRecord in splitNames)
		//		{
		//			/* Get fares for each subZipName */
		//			var zipCodeSplitRecordResult = getZipCodeRates(airportCode, fleetFilter, franchiseCode, tripDirection, addressLocation.PostalCode, ZipCodeSplitRecord.SubZipName, payingPax, discountCode);
		//			if (zipCodeSplitRecordResult != null)// && zipCodeSplitRecordResult.HasRecords)
		//			{
		//				results.Add(new RateServiceTypeResult { ReservationType = ReservationType.AirportReservationForZipcode, ServiceTypes = zipCodeSplitRecordResult, ZipCodeSplitRecord = ZipCodeSplitRecord });
		//			}


		//			//if (result != null && result.HasRecords)
		//			//	break;
		//		}
		//	}
		//	#endregion

		//	//Check maximum PayingPax and maximum wheelchairs, order by the lowest Rate
		//	//if (result.HasRecords)
		//	if (results != null && results.Any())
		//	{
		//		#region Split Logic
		//		if (reservationType == ReservationType.AirportReservationForLandmark || !isZipSplit)
		//		{
		//			#region No Splits => Get first one and then Return All
		//			var rates = new List<RatesService.ServiceTypeRecord>();

		//			if (payingPax <= 1) //Only compare FirstPersonFare
		//				rates = results.First().ServiceTypes.ServiceTypeRecord.OrderBy(r => r.FirstPersonFare).ToList();
		//			else //Compare calculated FirstPersonFare and AdditionalPersonFare
		//				rates = results.First().ServiceTypes.ServiceTypeRecord.OrderBy(r => r.FirstPersonFare + r.AdditionalPersonFare * (payingPax - 1)).ToList();

		//			//If SuperShuttleSharedRideOnly, only return Shared Ride Rates.
		//			if (isSuperShuttleSharedRideOnly)
		//				rates = rates.Where(r => r.ServiceTypeCode.ToLower() == "VAN".ToLower()).ToList();
		//			foreach (var rate in rates)
		//			{
		//				if (rate.MaxGuests >= (payingPax + FreePax) && rate.MaxAccessibleGuests >= wheelchairs)
		//				{
		//					airportRate.Add(rate);
		//				}
		//			}
		//			#endregion
		//		}
		//		else
		//		{
		//			var totalSplits = results.Count();
		//			var totalServicedSplits = results.Where(s => s.ZipCodeSplitRecord.Serviced == true).Count();
		//			#region All Serviced splits
		//			// just for ZipCodeSplitRecord.Serviced = true
		//			foreach (var result in results.Where(s => s.ZipCodeSplitRecord.Serviced && s.ServiceTypes.HasRecords))
		//			{
		//				var rates = new List<RatesService.ServiceTypeRecord>();

		//				if (payingPax <= 1) //Only compare FirstPersonFare
		//					rates = result.ServiceTypes.ServiceTypeRecord.OrderBy(r => r.FirstPersonFare).ToList();
		//				else //Compare calculated FirstPersonFare and AdditionalPersonFare
		//					rates = result.ServiceTypes.ServiceTypeRecord.OrderBy(r => r.FirstPersonFare + r.AdditionalPersonFare * (payingPax - 1)).ToList();

		//				//If SuperShuttleSharedRideOnly, only return Shared Ride Rates.
		//				if (isSuperShuttleSharedRideOnly)
		//					rates = rates.Where(r => r.ServiceTypeCode.ToLower() == "VAN".ToLower()).ToList();
		//				foreach (var rate in rates)
		//				{
		//					if (rate.MaxGuests >= (payingPax + FreePax) && rate.MaxAccessibleGuests >= wheelchairs)
		//					{
		//						airportRate.Add(rate);
		//					}
		//				}
		//			}
		//			#endregion

		//			if (totalServicedSplits == totalSplits)
		//			{
		//				#region All splits serviced
		//				if (userInfo.ShowSplits)
		//				{
		//					#region userInfo.ShowSplits= true and All Serviced => Retrun All Serviced
		//					#region Filter => no filter needed
		//					//no filter needed 
		//					#endregion
		//					#endregion
		//				}
		//				else
		//				{
		//					#region userInfo.ShowSplits= false and All Serviced => Retrun cheapest ones
		//					#region Filter => return cheapest one for each Fleet + ServiceTypeCode
		//					//var airportRateSorted = airportRate.OrderBy(s => s.Fleet).OrderBy(s => s.ServiceTypeCode).ToList();
		//					airportRate = airportRate.GroupBy(s => new { s.Fleet, s.ServiceTypeCode }).Select(s => s.OrderBy(r => r.FirstPersonFare + r.AdditionalPersonFare * (payingPax - 1))).Select(s => s.First()).ToList();
		//					#endregion
		//					#endregion
		//				}
		//				#endregion
		//			}
		//			else
		//			{
		//				#region Not all splits serviced, but at least one is serviced
		//				if (totalServicedSplits > 0)
		//				{
		//					if (userInfo.ShowSplits)
		//					{
		//						#region userInfo.ShowSplits= true but not all serviced => Retrun All Serviced
		//						#region Filter => no filter needed
		//						//no filter needed
		//						#endregion
		//						#endregion
		//					}
		//					else
		//					{
		//						if (userInfo.AllowClosedSplits)
		//						{
		//							#region userInfo.ShowSplits= false and userInfo.AllowClosedSplits = true,  Retrun cheapest ones
		//							#region Filter => return cheapest one for each Fleet + ServiceTypeCode
		//							airportRate = airportRate.GroupBy(s => new { s.Fleet, s.ServiceTypeCode }).Select(s => s.OrderBy(r => r.FirstPersonFare + r.AdditionalPersonFare * (payingPax - 1))).Select(s => s.First()).ToList();
		//							#endregion
		//							#endregion
		//						}
		//						else
		//						{
		//							#region userInfo.ShowSplits= false and userInfo.AllowClosedSplits = false,  Retrun nothing. Like the whose Zipcode (not split) is not serviced
		//							#region Fileter => Retrun nothing. Like the whose Zipcode (not split) is not serviced
		//							airportRate.RemoveAll(s => true);
		//							#endregion
		//							#endregion
		//						}
		//					}
		//				}
		//				#endregion
		//			}
		//		}
		//		#endregion
		//	}
		//	else
		//	{
		//		throw new Exception(string.Format(Messages.UDI_SDS_NoRateReturnedBySuperShuttle, "GetAirportReservationRates"));
		//	}

		//	if (airportRate.Count == 0)
		//		throw new Exception(string.Format(Messages.UDI_SDS_NoMatchingRate, "GetAirportReservationRates"));

		//	return airportRate;
		//}

		///// <summary>
		///// Connect to SuperShuttle to get all zipCode rates
		///// </summary>
		//private RatesService.ServiceTypes getZipCodeRates(string airportCode, int fleetFilter, string franchiseCode, int tripDirection, string postalCode, string subZipName, int payingPax, string discountCode)
		//{
		//	try
		//	{
		//		var result = new RatesService.ServiceTypes();
		//		RatesService.ZipCodeFareRequest request = new RatesService.ZipCodeFareRequest();
		//		request.AirportCode = airportCode;
		//		request.FleetFilter = fleetFilter;
		//		request.FranchiseCode = franchiseCode;
		//		request.TripDirection = tripDirection;
		//		request.ZipCode = postalCode;
		//		request.SubZipName = subZipName;
		//		request.PayingPax = payingPax;
		//		if (!string.IsNullOrEmpty(discountCode))
		//			request.DiscountId = getDiscountId(discountCode);

		//		logger.InfoFormat("Send to SuperShuttle \r\n{0}", request.XmlSerialize());

		//		#region Track
		//		if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Domain, "Call");
		//		#endregion

		//		using (var client = new RatesService.RatesServiceClient())
		//		{
		//			result = client.GetZipCodeFaresWithSurcharge(token.ToRatesServiceToken(), request);
		//		}

		//		#region Track
		//		if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetZipCodeFaresWithSurcharge");
		//		#endregion

		//		return result;
		//	}
		//	catch (Exception ex)
		//	{
		//		logger.ErrorFormat("Call RatesService.GetZipCodeFaresWithSurcharge {0}", ex.StackTrace);
		//		throw ex;
		//	}
		//}
		//#endregion 
		#endregion

		#region Deleted
		//#region private methods for Charter rates
		///// <summary>
		///// Connect to SuperShuttle to get all hourly charter rates, then return the lowest rate.
		///// If no matching record found, return NULL.
		///// </summary>
		//private List<RatesService.CharterServiceTypeRecord> GetHourlyCharterRates(int fleetFilter, string postalCode, int totalMinutes, int PayingPax, int wheelChairs, int FreePax)
		//{
		//	try
		//	{
		//		var msg = string.Empty;

		//		if (string.IsNullOrEmpty(postalCode))
		//			throw new Exception(string.Format(Messages.UDI_SDS_PostalCodeRequired, "GetHourlyCharterRates"));


		//		List<RatesService.CharterServiceTypeRecord> result = new List<RatesService.CharterServiceTypeRecord>();
		//		RatesService.HourlyCharterRateRequest request = new RatesService.HourlyCharterRateRequest();
		//		request.FleetFilter = fleetFilter;
		//		request.TotalMinutes = totalMinutes;
		//		request.PostalCode = postalCode;

		//		logger.InfoFormat("GetHourlyCharterRates Start...\r\n {0}", request.XmlSerialize());

		//		using (var client = new RatesService.RatesServiceClient())
		//		{
		//			#region Track
		//			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Domain, "Call");
		//			#endregion

		//			var rates = client.GetHourlyCharterRates(token.ToRatesServiceToken(), request).ToList();

		//			#region Track
		//			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetHourlyCharterRates");
		//			#endregion

		//			//Check maximum PayingPax and minimum minutes, order by lowest HourlyRate
		//			if (rates.Count > 0)
		//			{
		//				rates = rates.OrderBy(r => r.HourlyRate).ToList();

		//				foreach (var rate in rates)
		//				{
		//					if (rate.MaxGuests >= (PayingPax + FreePax) && rate.MaxAccessibleGuests >= wheelChairs)//rate.MinimumDurationMinutes < totalMinutes (CharterMinutes is not used in SaveReservation, so no need to check here)
		//					{
		//						result.Add(rate);
		//						break;
		//					}
		//				}
		//			}
		//			else
		//				msg = "GetHourlyCharterRates.";
		//		}

		//		if (!string.IsNullOrEmpty(msg))
		//			throw new Exception(string.Format(Messages.UDI_SDS_NoRateReturnedBySuperShuttle, msg));

		//		if (result.Count == 0)
		//			throw new Exception(string.Format(Messages.UDI_SDS_NoMatchingRate, "GetHourlyCharterRates"));

		//		return result;
		//	}
		//	catch (Exception ex)
		//	{
		//		logger.ErrorFormat("Call RatesService.GetHourlyCharterRates {0}", ex.StackTrace);
		//		throw ex;
		//	}
		//}

		///// <summary>
		///// Connect to SuperShuttle to get all point to point rates, then return the lowest rate.
		///// If no matching record found, return NULL.
		///// </summary>
		//private List<RatesService.CharterServiceTypeRecord> GetPointToPointCharterRates(int fleetFilter, List<Location> locations, int payingPax, int wheelChairs, int FreePax)
		//{
		//	try
		//	{
		//		var msg = string.Empty;

		//		List<RatesService.CharterServiceTypeRecord> result = new List<RatesService.CharterServiceTypeRecord>();
		//		RoutingService.Point startPoint = new RoutingService.Point();
		//		RoutingService.Point endPoint = new RoutingService.Point();
		//		RatesService.PointToPointCharterRateRequest request = new RatesService.PointToPointCharterRateRequest();
		//		request.FleetFilter = fleetFilter;

		//		#region Get GeoCode for each address
		//		foreach (var location in locations)
		//		{
		//			var newlocation = location;
		//			//ToDoForVTOD
		//			//if (location.Latitude == 0 || location.Longitude == 0)
		//			//	newlocation = GetGeoCodeForAddress(location);

		//			if (newlocation.mode == LocationMode.Pickup)
		//			{
		//				request.PickupZipCode = newlocation.PostalCode;
		//				request.PickupLandmarkID = newlocation.masterLandmarkID;
		//				startPoint.Latitude = (Double)newlocation.Latitude;
		//				startPoint.Longitude = (Double)newlocation.Longitude;
		//			}
		//			else
		//			{
		//				request.DropoffZipCode = newlocation.PostalCode;
		//				request.DropoffLandmarkID = newlocation.masterLandmarkID;
		//				endPoint.Latitude = (Double)newlocation.Latitude;
		//				endPoint.Longitude = (Double)newlocation.Longitude;
		//			}
		//		}
		//		#endregion

		//		#region Get Charter Location ID
		//		using (var utilityClient = new UtilityService.UtilityServiceClient())
		//		{
		//			#region Track
		//			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Domain, "Call");
		//			#endregion

		//			/** use pickup zipcode to determine which region customer is in **/
		//			var charterLocationList = utilityClient.GetCharterLocationsByPostalCode(token.ToUtilityServiceToken(), 0, request.PickupZipCode); //CharterType: 0=PointToPoint, 1=Hourly

		//			#region Track
		//			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetCharterLocationsByPostalCode");
		//			#endregion

		//			if (charterLocationList != null && charterLocationList.FirstOrDefault() != null)
		//			{
		//				request.CharterLocationsID = charterLocationList.FirstOrDefault().ID;
		//			}
		//		}
		//		#endregion

		//		#region Get Distance
		//		#region Track
		//		if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Domain, "Call");
		//		#endregion

		//		using (var routingClient = new RoutingService.RoutingServiceClient())
		//		{
		//			request.Distance = routingClient.GetDistanceBetweenPoints(token.ToRoutingServiceSecureToken(), startPoint, endPoint);
		//		}
		//		#endregion

		//		#region Track
		//		if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetDistanceBetweenPoints");
		//		#endregion


		//		logger.InfoFormat("GetPointToPointCharterRates Start...\r\n {0}", request.XmlSerialize());

		//		using (var client = new RatesService.RatesServiceClient())
		//		{
		//			#region Track
		//			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Domain, "Call");
		//			#endregion

		//			var rates = client.GetPointToPointCharterRates(token.ToRatesServiceToken(), request).ToList();

		//			#region Track
		//			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetPointToPointCharterRates");
		//			#endregion

		//			//Check maximum PayingPax and maximum wheelchairs, order by the lowest Fare
		//			if (rates.Count > 0)
		//			{
		//				rates = rates.OrderBy(r => r.Fare).ToList();

		//				foreach (var rate in rates)
		//				{
		//					if (rate.MaxGuests >= (payingPax + FreePax) && rate.MaxAccessibleGuests >= wheelChairs)
		//					{
		//						result.Add(rate);
		//						break;
		//					}
		//				}
		//			}
		//			else
		//				msg = "GetPointToPointCharterRates";
		//		}

		//		if (!string.IsNullOrEmpty(msg))
		//			throw new Exception(string.Format(Messages.UDI_SDS_NoRateReturnedBySuperShuttle, msg));

		//		if (result.Count == 0)
		//			throw new Exception(string.Format(Messages.UDI_SDS_NoMatchingRate, "GetPointToPointCharterRates"));

		//		return result;
		//	}
		//	catch (Exception ex)
		//	{
		//		logger.ErrorFormat("Call RatesService.GetPointToPointCharterRates {0}", ex.StackTrace);
		//		throw ex;
		//	}
		//}
		//#endregion 
		#endregion

		#region private methods

		#region Deleted
		//private void checkLocationRequiredFields(Location location)
		//{
		//	var msg = string.Empty;

		//	/* if Airport, then LocationCode is required */
		//	if (location.type == LocationType.Airport && string.IsNullOrEmpty(location.AiportCode))
		//		throw new Exception(string.Format(Messages.UDI_SDS_LocationCodeRequired, "CheckLocationRequiredFields"));

		//	/* if AsDirected, then mode has to be Dropoff */
		//	if (location.type == LocationType.AsDirected && location.mode == LocationMode.Pickup)
		//		throw new Exception(string.Format(Messages.UDI_SDS_AsDirected, "CheckLocationRequiredFields"));

		//	/* if Address, then PostalCode is required */
		//	if (location.type == LocationType.Address && string.IsNullOrEmpty(location.PostalCode))
		//		throw new Exception(string.Format(Messages.UDI_SDS_PostalCodeRequired, "CheckLocationRequiredFields"));

		//	if (!string.IsNullOrEmpty(msg))
		//		throw new Exception(msg);
		//} 
		#endregion

		/*
		private bool isAirportServicedOnline(string airportCode)
		{
			try
			{
				var isServicedOnline = false;
				var errorMsg = string.Empty;

				using (var utilityClient = new UtilityService.UtilityServiceClient())
				{
					var airportsRequest = new UtilityService.GetAirportsRequest { LocalizationCode = airportCode };

					logger.InfoFormat("Send to SuperShuttle \r\n{0}", airportsRequest.XmlSerialize());

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Domain, "Call");
					#endregion

					var airports = utilityClient.GetWebServicedAirports(token.ToUtilityServiceToken());
					//utilityClient.GetWebServicedAirports2(token.ToUtilityServiceToken(), airportsRequest);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetWebServicedAirports");
					#endregion
					
					if (airports.HasRecords)
					{
						foreach (var airportRecord in airports.AirportRecordsArray)
						{
							if (airportRecord.AirportCode.ToUpper().Equals(airportCode.ToUpper()))
							{
								//Airport with Serviced = 2 are serviced by SuperShuttle but reservations must be booked thru the local office.
								if (airportRecord.Serviced == 2)
								{
									#region Track
									if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Domain, "Call");
									#endregion

									errorMsg = utilityClient.GetAirportWebMessage(token.ToUtilityServiceToken(), airportCode);

									#region Track
									if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetAirportWebMessage");
									#endregion
								}
								else
								{
									isServicedOnline = true;
									break;
								}
							}
						}
					}
				}

				if (!isServicedOnline)
					throw new Exception(string.Format(Messages.UDI_SDS_AirportNotServiced, "IsAirportServicedOnline", airportCode, errorMsg));

				return isServicedOnline;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call utilityClient.GetAirportWebMessage {0}", ex.StackTrace);
				throw ex;
			}
		}
		*/

		/*
		private LandmarksService.LandmarkRecord getLandmarkRecord(string airportCode, Location location)
		{
			try
			{
				if (string.IsNullOrEmpty(location.StateOrProvince) || string.IsNullOrEmpty(location.StreetName) || string.IsNullOrEmpty(location.StreetNumber))
					return null;

				LandmarksService.LandmarkRecord landmarkRecord = null;
				LandmarksService.GetLandmarkByAddressRequest getLandmarkByAddressRequest = new LandmarksService.GetLandmarkByAddressRequest();
				LandmarksService.AddressRecord addressRecord = new LandmarksService.AddressRecord();

				addressRecord.PostalCode = location.PostalCode;
				addressRecord.StreetName = location.StreetName;
				addressRecord.StreetNumber = location.StreetNumber;
				addressRecord.CountrySubDivision = location.StateOrProvince;
				addressRecord.LocationName = location.PropertyName;
				addressRecord.Latitude = location.Latitude;
				addressRecord.Longitude = location.Longitude;

				getLandmarkByAddressRequest.Address = addressRecord;
				getLandmarkByAddressRequest.AirportCode = airportCode;
				getLandmarkByAddressRequest.AlwaysMatch = false;//By default, this value is always false

				logger.InfoFormat("Send to SuperShuttle \r\n{0}", getLandmarkByAddressRequest.XmlSerialize());

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Domain, "Call");
				#endregion

				using (var client = new LandmarksService.LandmarksServiceClient())
				{
					landmarkRecord = //client.GetLandmarkByAddress(token.ToLandmarksServiceToken(), airportCode, addressRecord);
					client.GetLandmarkByAddress2(token.ToLandmarksServiceToken(), getLandmarkByAddressRequest);
				}

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetLandmarkByAddress2");
				#endregion

				return landmarkRecord;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call LandmarksService.GetLandmarkByAddress2 {0}", ex.StackTrace);
				throw ex;
			}
		}
		*/

		/*
		private int getMasterLandMarkID(LandmarksService.LandmarkRecord landmarkRecord)
		{
			if (landmarkRecord == null)
				return -2147483648;
			else
				return landmarkRecord.MasterLandmarkID;
		}

		private string getLocationType(LandmarksService.LandmarkRecord landmarkRecord)
		{
			if (landmarkRecord == null || landmarkRecord.LandmarkAddress == null)
				return string.Empty;
			else if (string.IsNullOrEmpty(landmarkRecord.LandmarkAddress.LocationType))
				return "R";
			else
				return landmarkRecord.LandmarkAddress.LocationType;
		}

		private string getSubZipNameFromLandmarkRecord(LandmarksService.LandmarkRecord landmarkRecord)
		{
			if (landmarkRecord == null || landmarkRecord.LandmarkAddress == null)
				return string.Empty;
			else
				return landmarkRecord.LandmarkAddress.SubZipName;
		}

		private string getLocationNameFromLandmarkRecord(LandmarksService.LandmarkRecord landmarkRecord)
		{
			if (landmarkRecord == null || landmarkRecord.LandmarkAddress == null)
				return string.Empty;
			else
				return landmarkRecord.LandmarkAddress.LocationName;
		}
		*/

		/*
		private string getFranchiseCode(string airportCode, string postalCode, int tripDirection)
		{
			try
			{
				string franchiseCode = string.Empty;

				logger.InfoFormat("Send to SuperShuttle \r\n AirportCode={0},PostalCode={1},TripDirection={2}", airportCode, postalCode, tripDirection);
				
				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Domain, "Call");
				#endregion
				
				using (var client = new FranchiseService.FranchiseServiceClient())
				{
					var franchise = client.GetServiceFranchise(token.ToFranchiseServiceToken(), airportCode, postalCode, tripDirection);
					if (franchise != null)
						franchiseCode = franchise.FranchiseCode;
				}

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetServiceFranchise");
				#endregion


				return franchiseCode;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call FranchiseService.GetServiceFranchise {0}", ex.StackTrace);
				throw ex;
			}
		}
		*/

		/*
		/// <summary>
		/// Check if zip code is serviced and split
		/// </summary>
		/// <param name="airportCode"></param>
		/// <param name="postalCode"></param>
		/// <param name="tripDirection">0 = To the airport. 1 = From the airport. 2 = Roundtrip.</param>
		/// <returns>a list of split names</returns>
		private List<ZipCodesService.ZipCodeSplitRecord> getSplitNamesOfServicedPostalCode(string airportCode, string postalCode, int tripDirection, out bool isZipSplit)
		{
			try
			{
				List<ZipCodesService.ZipCodeSplitRecord> splitNames = null;

				logger.InfoFormat("Send to SuperShuttle \r\n AirportCode={0},PostalCode={1},TripDirection={2}", airportCode, postalCode, tripDirection);
				using (var client = new ZipCodesService.ZipCodesServiceClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Domain, "Call");
					#endregion

					var result = client.IsZipServiced(token.ToZipCodesServiceToken(), airportCode, postalCode, tripDirection);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "IsZipServiced");
					#endregion

					isZipSplit = result.IsZipSplit;
					if (result != null && result.IsServiced == true && result.SplitsArray.ToList().Count > 0)
						splitNames = result.SplitsArray.ToList();

				}

				if (splitNames == null)
					throw new Exception(string.Format(Messages.UDI_SDS_PostalCodeNotServiced, postalCode));
				//throw new Exception("GetSplitNamesOfServicedPostalCode:: The postalcode " + postalCode + " is not serviced.");

				return splitNames;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call ZipCodesService.IsZipServiced {0}", ex.StackTrace);
				throw ex;
			}
		}
		*/

		/*
		/// <summary>
		/// Validate the discount code against groups web service, which would return the corresponding integer DiscountID value.
		/// </summary>
		private int getDiscountId(string discountCode)
		{
			if (string.IsNullOrEmpty(discountCode))
				return 0;

			var discountId = 0;

			try
			{
				logger.InfoFormat("Send to SuperShuttle \r\n discountCode={0}", discountCode);

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Domain, "Call");
				#endregion

				using (var client = new GroupsService.GroupsServiceClient())
				{
					discountId = client.ValidateDiscountCode(token.ToGroupsServiceToken(), discountCode);
				}

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "ValidateDiscountCode");
				#endregion
				
				return discountId;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call GroupsService.IsZipServiced {0}", ex.StackTrace);
				throw ex;
			}
		}
		*/

		#endregion

		#region Properties
		public TrackTime TrackTime { get; set; }
		#endregion
	}
}
