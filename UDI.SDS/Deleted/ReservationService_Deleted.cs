﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.SDS.Helper;
using UDI.SDS.SecurityService;
using System.Configuration;
using System.Web;

using UDI.VTOD.Common.Helper;
using log4net;
using UDI.Utility.Serialization;
using UDI.VTOD.Common.Abstract;
using UDI.Utility.Helper;
using UDI.SDS.DTO;
using UDI.SDS.DTO.Enum;
using UDI.VTOD.Common.Track;
using UDI.VTOD.Common.DTO.Enum;
//using UDI.SDS.RatesService;

namespace UDI.SDS
{
	/// <summary>
	/// Reservation implements main methods for SDS Booking, Cancel and Get Status.
	/// </summary>
	public class ReservationService_Deleted : BaseSetting //Common
	{
		#region Deleted
		#region Constructors
		public ReservationService_Deleted()
		{
		}

		//public Reservation(ILog logger) {
		//	log = logger;
		//}
		#endregion

		#region Deleted
		/*
		#region Get Fees for OTA Availablity
		/// <summary>
		/// This method must be called to correctly calculate the total cost of a reservation so any applicable taxes, discounts and commissions can be applied. 
		/// This version of the method allows the client to specify a list of fees that have been added or modified. 
		/// </summary>
		public RatesService.FareScheduleWithFees GetFees(Guid bookRequestID, RatesService.ServiceTypeRecord airportFare, RatesService.CharterServiceTypeRecord charterFare, TripDirection tripDirection, bool isRoundTrip, int payingPax, byte paymentType, DateTime pickupTime, string discountCode)
		{
			try
			{
				RatesService.FareScheduleWithFees result = null;
				RatesService.FareScheduleRequestWithFees request = new RatesService.FareScheduleRequestWithFees();
				request.CurrencyID = (int)Currency.USD; //0 for USD
				request.PayingPax = (byte)payingPax;
				request.PaymentType = paymentType;
				request.DiscountCode = discountCode;

				if (tripDirection == TripDirection.Inbound || isRoundTrip)
				{
					request.InboundFareID = charterFare == null ? airportFare.FareID : charterFare.FareID;
					request.InboundTravelDate = pickupTime;
				}

				if (tripDirection == TripDirection.Outbound || isRoundTrip)
				{
					request.OutboundFareID = charterFare == null ? airportFare.FareID : charterFare.FareID;
					request.OutboundTravelDate = pickupTime;
				}

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Domain, "Call");
				#endregion

				using (var client = new RatesService.RatesServiceClient())
				{
					result = client.GetFareScheduleWithFees(token.ToRatesServiceToken(), request);
				}

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetFareScheduleWithFees");
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("GetFees BookRequestID:{0}\r\n {1}", bookRequestID, ex.ToString());
				throw ex;
			}
		}
		#endregion
		*/
		#endregion

		/*
		#region Booking
		/// <summary>
		/// Used by VTOD project.
		/// </summary>
		public BookResponse BookForSpecifiedRateAndPickupTime(BookRequest request, RateResponse rateResponseFirstSegment, UDI.SDS.RatesService.FareScheduleWithFees firstSegmentFees, PickupTimeResponse pickupTimeResponseFirstSegment, RateResponse rateResponseSecondSegment, UDI.SDS.RatesService.FareScheduleWithFees secondSegmentFees, PickupTimeResponse pickupTimeResponseSecondSegment, SecurityToken token)
		{
			try
			{
				logger.InfoFormat("BookForSpecifiedRateAndPickupTime Start...\r\n{0}");
				BookResponse response = new BookResponse();
				response.ReservationResponses = new List<ReservationResponse>();
				ReservationResponse reservationResponse = new ReservationResponse();

				//rateResponse.Segment is the first element of BookRequest.Segments, here use rateResponse.Segment because of the segment returned by Rate function has some updated fields, such as masterlandmarkID, locationType and subzipname.
				var firstSegment = rateResponseFirstSegment.Segment;
				var secondSegment = rateResponseSecondSegment != null ? rateResponseSecondSegment.Segment : null; //request.Segments.Count == 2 ? request.Segments.Last() : null;

				#region Save Reservation
				if (rateResponseFirstSegment.TripDirection == TripDirection.Inbound || rateResponseFirstSegment.TripDirection == TripDirection.Outbound || rateResponseFirstSegment.TripDirection == TripDirection.RoundTrip)
				{
					//Airport Reservation
					reservationResponse.AirportReservationConfirmations = saveAirportReservation(firstSegment, secondSegment, request.reservationRequest, rateResponseFirstSegment, firstSegmentFees, pickupTimeResponseFirstSegment, rateResponseSecondSegment, secondSegmentFees, pickupTimeResponseSecondSegment);

				}
				else
				{
					//Charter Reservation
					reservationResponse.CharterReservationConfirmation = saveCharterReservation(firstSegment, request.reservationRequest, rateResponseFirstSegment, firstSegmentFees, pickupTimeResponseFirstSegment);
				}
				#endregion

				reservationResponse.Segments = new List<Segment>();
				reservationResponse.Segments.Add(firstSegment);
				reservationResponse.Segments.Add(secondSegment);
				response.ReservationResponses.Add(reservationResponse);

				response.ResponseUtcTime = DateTime.UtcNow;
				response.BookRequestID = request.BookRequestID;
				logger.InfoFormat("BookForSpecifiedRateAndPickupTime End...\r\n{0}", response.XmlSerialize());
				return response;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("BookForSpecifiedRateAndPickupTime BookRequestID:{0}\r\n {1}", request.BookRequestID, ex.ToString());
				throw ex;
			}
		}
		#endregion
		*/

		#region Deleted
		/*
		#region Cancel Reservation
		/// <summary>
		/// The 3 parameters: lastName, phoneNumber, and postalCode are considered to be validating data. 
		/// This validates the user knows more information than just guessing the confirmation number. 
		/// One of these fields values must be supplied and match what is stored with the reservation. 
		/// userName is the name of the person cancelling the reservation.
		/// </summary>
		public bool CancelReservation(SecurityToken token, string confirmationNumber, string userName, string lastName, string phoneNumber, string postalCode)
		{
			try
			{
				logger.InfoFormat("Cancel VTOD Reservation Start...\r\n  ConfirmationNumber:{0}, userName:{1}, lastName:{2}, phoneNumber:{3}, postalCode: {4}", confirmationNumber, userName, lastName, phoneNumber, postalCode);

				int result = -1;

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Domain, "Call");
				#endregion

				using (var client = new ReservationsService.ReservationsServiceClient())
				{
					result = client.CancelReservation(token.ToReservationsServiceToken(), confirmationNumber, userName, lastName, phoneNumber, postalCode);
				}

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "CancelReservation");
				#endregion

				if (result == (int)Cancel.ReservationCancelled || result == (int)Cancel.ReservationAlreadyCancelled)
					return true;
				else if (result == (int)Cancel.TooCloseToPickupTime)
					throw new Exception(string.Format(Messages.UDI_SDS_CancelFailed_TooCloseToPickupTime, "CancelReservation"));
				else if (result == (int)Cancel.ReservationPickupTimeHasExpiredOrCannotBeCancelled)
					throw new Exception(string.Format(Messages.UDI_SDS_CancelFailed_PickupTimeExpired, "CancelReservation"));
				else
					throw new Exception(string.Format(Messages.UDI_SDS_CancelFailed_Unknown, "CancelReservation", result));
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Cancel VTOD ConfirmationNumber:{0}\r\n {1}", confirmationNumber, ex.ToString());
				throw ex;
			}
		}
		#endregion
		*/

		/*
		#region Get Status
		/// <summary>
		/// Get all reservation statuses from SDS for VTOD
		/// </summary>
		public List<DispatchUpdatesService.TripStatusRecord> GetReservationStatus(SecurityToken token, string rezSource)
		{
			try
			{
				logger.InfoFormat("GetReservationStatus Start...\r\n  rezSource={0}", rezSource);

				List<DispatchUpdatesService.TripStatusRecord> result = null;

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Domain, "Call");
				#endregion

				using (var client = new DispatchUpdatesService.DispatchUpdatesServiceClient())
				{
					result = client.GetDispatchStatusUpdates(rezSource);
				}

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetDispatchStatusUpdates");
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("GetReservationStatus: rezSource={0}\r\n {1}", rezSource, ex.ToString());
				throw ex;
			}
		}
		#endregion
		*/

		#region Private methods for Booking
		#region Airport Reservation

		/*
		/// <summary>
		/// Send airport reservation request to SDS and get confirmation back
		/// </summary>
		//private List<ReservationsServiceSecure.ReservationConfirmation> SaveAirportReservation(Segment departintSegment, Segment returningSegment, ReservationRequest request, RateResponse rateResponse, PickupTimeResponse pickupTimeResponse)
		private List<ReservationsServiceSecure.ReservationConfirmation> saveAirportReservation(Segment departintSegment, Segment returningSegment, ReservationRequest request, RateResponse rateResponseFirstSegment, UDI.SDS.RatesService.FareScheduleWithFees firstSegmentFees, PickupTimeResponse pickupTimeResponseFirstSegment, RateResponse rateResponseSecondSegment, UDI.SDS.RatesService.FareScheduleWithFees secondSegmentFees, PickupTimeResponse pickupTimeResponseSecondSegment)
		{
			try
			{
				List<ReservationsServiceSecure.ReservationConfirmation> result = null;
				var airportReservation = setAirportReservation(departintSegment, returningSegment, request, rateResponseFirstSegment, firstSegmentFees, pickupTimeResponseFirstSegment, rateResponseSecondSegment, secondSegmentFees, pickupTimeResponseSecondSegment);

				logger.InfoFormat("Send to SuperShuttle \r\n{0}", airportReservation.XmlSerialize());


				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Domain, "Call");
				#endregion

				using (var client = new ReservationsServiceSecure.ReservationsServiceSecureClient())
				{
					result = client.SaveReservation3(token.ToReservationsServiceSecureToken(), airportReservation, new ReservationsServiceSecure.ClientVersionInfo()).ToList();
					//client.SaveReservation(token.ToReservationsServiceSecureToken(), airportReservation, pickupTimeResponse.AsapRequestId).ToList();
				}

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "SaveReservation3");
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call ReservationsServiceSecure.SaveReservation {0}", ex.StackTrace);
				throw ex;
			}
		}
		*/

		/*
		/// <summary>
		/// Set the request of Airport Reservation, used by SaveAirportReservation method
		/// </summary>
		//private ReservationsServiceSecure.AirportReservation SetAirportReservation(Segment firstSegment, Segment secondSegment, ReservationRequest request, RateResponse rateResponseFirstSegment, PickupTimeResponse pickupTimeResponseFirstSegment)
		private ReservationsServiceSecure.AirportReservation setAirportReservation(Segment firstSegment, Segment secondSegment, ReservationRequest request, RateResponse rateResponseFirstSegment, UDI.SDS.RatesService.FareScheduleWithFees firstSegmentFees, PickupTimeResponse pickupTimeResponseFirstSegment, RateResponse rateResponseSecondSegment, UDI.SDS.RatesService.FareScheduleWithFees secondSegmentFees, PickupTimeResponse pickupTimeResponseSecondSegment)
		{

			ReservationsServiceSecure.AirportReservation airportReservation = new ReservationsServiceSecure.AirportReservation();
			var airportCode = string.Empty;
			airportReservation.AccessibleServiceRequired = firstSegment.Wheelchairs > 0 ? true : false;
			airportReservation.AirlineMilesDetails = null;
			airportReservation.ChangeCSR = string.Empty;
			airportReservation.ChildSeats = (byte)firstSegment.ChildSeats;
			airportReservation.Comments = request.Comments;
			airportReservation.ContactNumber = request.ContactNumber;
			airportReservation.ContactNumberDialingPrefix = request.ContactNumberDialingPrefix;
			airportReservation.EmailAddress = request.EmailAddress;
			airportReservation.FirstName = request.FirstName;
			airportReservation.FreePax = (byte)(firstSegment.ChildSeats + firstSegment.InfantSeats);
			airportReservation.GroupID = request.GroupID;
			airportReservation.InfantSeats = (byte)firstSegment.InfantSeats;
			airportReservation.LastName = request.LastName;
			airportReservation.Luggage = (byte)request.LuggageCount;
			airportReservation.PayingPax = (byte)firstSegment.PayingPax;
			airportReservation.ReserveCSR = request.ReserveCSR;
			airportReservation.ServiceNotes = request.ServiceNotes;
			airportReservation.SID = request.SID;
			airportReservation.SpecialHandling = request.SpecialHandling;
			airportReservation.TripDirection = secondSegment == null ? (int)rateResponseFirstSegment.TripDirection : (int)TripDirection.RoundTrip;


			#region GuestAddress
			foreach (var location in firstSegment.Locations)
			{
				if (
					(rateResponseFirstSegment.TripDirection == TripDirection.Inbound && location.mode == LocationMode.Pickup)//Inbound
					||
					(rateResponseFirstSegment.TripDirection == TripDirection.Outbound && location.mode == LocationMode.Dropoff)//Outbound
					||
					(rateResponseFirstSegment.TripDirection == TripDirection.RoundTrip && location.type == LocationType.Address)//RoundTrip
					)
				{
					RateService rates = new RateService(token);
					rates.TrackTime = TrackTime;

					var location_new = location;
					if (location.Latitude == 0 || location.Longitude == 0)
						location_new = rates.GetGeoCodeForAddress(location);

					airportReservation.GuestAddress = new ReservationsServiceSecure.AddressRecord();
					airportReservation.GuestAddress.CountryCode = location.Country;
					airportReservation.GuestAddress.CountrySubDivision = location.StateOrProvince;
					airportReservation.GuestAddress.Latitude = location_new.Latitude;
					airportReservation.GuestAddress.Longitude = location_new.Longitude;
					airportReservation.GuestAddress.LocationName = location.PropertyName;
					airportReservation.GuestAddress.LocationType = location.LocationType;
					airportReservation.GuestAddress.Municipality = location.City;
					airportReservation.GuestAddress.PostalCode = location.PostalCode;
					airportReservation.GuestAddress.StreetAddress = location.StreetNumber + " " + location.StreetName;
					airportReservation.GuestAddress.StreetName = location.StreetName;
					airportReservation.GuestAddress.StreetNumber = location.StreetNumber;
					airportReservation.GuestAddress.SubZipName = location.SubZipName;
					airportReservation.GuestAddress.UnitNumber = location.Unit;
					airportReservation.GuestAddress.Comments = string.Empty;
					airportReservation.GuestAddress.ID = 0;
					airportReservation.GuestAddress.PhoneNumber = string.Empty;
					airportReservation.GuestAddress.PhoneNumberDialingPrefix = string.Empty;
				}
				else
					airportCode = location.AiportCode;

			}
			#endregion

			#region Inbound, Outbound Information

			var inboundSegment = new Segment();
			var outboundSegment = new Segment();

			var inboundRate = new RateResponse();
			var outboundRate = new RateResponse();

			var inboundFees = new UDI.SDS.RatesService.FareScheduleWithFees();
			var outboundFees = new UDI.SDS.RatesService.FareScheduleWithFees();

			var inboundPickupTime = new PickupTimeResponse();
			var outboundPickupTime = new PickupTimeResponse();

			if (rateResponseFirstSegment.TripDirection == TripDirection.Inbound)
			{
				inboundSegment = firstSegment;
				outboundSegment = secondSegment;

				inboundRate = rateResponseFirstSegment;
				outboundRate = rateResponseSecondSegment;

				inboundFees = firstSegmentFees;
				outboundFees = secondSegmentFees;

				inboundPickupTime = pickupTimeResponseFirstSegment;
				outboundPickupTime = pickupTimeResponseSecondSegment;
			}

			if (rateResponseFirstSegment.TripDirection == TripDirection.Outbound)
			{
				inboundSegment = secondSegment;
				outboundSegment = firstSegment;

				inboundRate = rateResponseSecondSegment;
				outboundRate = rateResponseFirstSegment;

				inboundFees = secondSegmentFees;
				outboundFees = firstSegmentFees;

				inboundPickupTime = pickupTimeResponseSecondSegment;
				outboundPickupTime = pickupTimeResponseFirstSegment;
			}

			if (inboundSegment != null)//Inbound - To the airport
			{
				#region InboundFare
				decimal totalFare = inboundRate.AirportReservationRate.FirstPersonFare + inboundRate.AirportReservationRate.AdditionalPersonFare * (inboundSegment.PayingPax - 1);

				airportReservation.InboundFare = new ReservationsServiceSecure.Fare();
				airportReservation.InboundFare.B2BCommission = 0;
				airportReservation.InboundFare.CommissionID = 0;

				#region CompanyFees
				airportReservation.InboundFare.CompanyFees = null;
				if (inboundFees.InboundCompanyFeesList != null && inboundFees.InboundCompanyFeesList.Any())
				{
					var companyFees = new List<ReservationsServiceSecure.AirportReservationFareFeeRecord>();
					foreach (var item in inboundFees.InboundCompanyFeesList)
					{
						var fee = new ReservationsServiceSecure.AirportReservationFareFeeRecord();
						fee.FeeId = item.FeeTypeID;
						fee.FeeTypeID = item.FeeTypeID;
						fee.Amount = item.Amount;
						//fee.IsDirty = item.IsDirty;
						//fee.IsATax = item.IsATax;
						//fee.IsTaxable = item.IsTaxable;
						//fee.OverrideFee = item.OverrideFee;
						fee.TaxRate = item.TaxRate;
						companyFees.Add(fee);
						totalFare += item.Amount;
					}
					airportReservation.InboundFare.CompanyFees = companyFees;
				}
				#endregion

				airportReservation.InboundFare.CurrencySymbol = inboundRate.AirportReservationRate.CurrencySymbol;
				airportReservation.InboundFare.CurrencyType = inboundRate.AirportReservationRate.CurrencyType;
				airportReservation.InboundFare.CurrencyID = inboundRate.AirportReservationRate.CurrencyID;

				#region Discount
				airportReservation.InboundFare.DiscountAmount = inboundFees.InboundDiscount;
				airportReservation.InboundFare.DiscountID = inboundFees.DiscountID;
				#endregion

				#region DriverFees
				airportReservation.InboundFare.DriverFees = null;
				if (inboundFees.InboundDriverFeesList != null && inboundFees.InboundDriverFeesList.Any())
				{
					var driverFees = new List<ReservationsServiceSecure.AirportReservationFareFeeRecord>();
					foreach (var item in inboundFees.InboundDriverFeesList)
					{
						var fee = new ReservationsServiceSecure.AirportReservationFareFeeRecord();
						fee.FeeId = item.FeeTypeID;
						fee.FeeTypeID = item.FeeTypeID;
						fee.Amount = item.Amount;
						//fee.IsDirty = item.IsDirty;
						//fee.IsATax = item.IsATax;
						//fee.IsTaxable = item.IsTaxable;
						//fee.OverrideFee = item.OverrideFee;
						fee.TaxRate = item.TaxRate;
						driverFees.Add(fee);

						totalFare += item.Amount;
					}
					airportReservation.InboundFare.DriverFees = driverFees;
				}
				#endregion

				airportReservation.InboundFare.FareID = inboundRate.AirportReservationRate.FareID;

				#region FuelSurcharge
				airportReservation.InboundFare.FuelSurcharge = inboundRate.AirportReservationRate.FuelSurcharge;
				totalFare += inboundRate.AirportReservationRate.FuelSurcharge;
				#endregion

				airportReservation.InboundFare.Gratuity = request.Gratuity;
				airportReservation.InboundFare.ServiceTypeCode = inboundRate.AirportReservationRate.ServiceTypeCode;
				airportReservation.InboundFare.TotalSegmentCost = totalFare;
				airportReservation.InboundFare.FareTotal = totalFare;
				airportReservation.InboundFare.UserOverrideFare = false;
				#endregion

				#region InboundItinerary
				airportReservation.InboundItinerary = new ReservationsServiceSecure.Itinerary();
				airportReservation.InboundItinerary.ASAPRequestID = inboundPickupTime.AsapRequestId;
				airportReservation.InboundItinerary.FlightOriginationAirportCode = airportCode;
				airportReservation.InboundItinerary.FlightTime = inboundSegment.FlightDateTime;
				airportReservation.InboundItinerary.IsDomesticFlight = inboundSegment.IsInternationalFlight ? false : true;
				airportReservation.InboundItinerary.IsGuaranteed = inboundPickupTime.IsGuaranteed;
				airportReservation.InboundItinerary.PickupTime = inboundPickupTime.AvailablePickupTime;
				airportReservation.InboundItinerary.PickupEndTime = inboundPickupTime.AvailablePickupEndTime;
				airportReservation.InboundItinerary.UsesScheduledService = inboundPickupTime.IsScheduledStop;
				airportReservation.InboundItinerary.AirlineCode = inboundSegment.AirlineCode;
				airportReservation.InboundItinerary.FlightNumber = inboundSegment.FlightNumber;
				airportReservation.InboundItinerary.IsCancelled = false;
				airportReservation.InboundItinerary.RequestAttributes = string.Empty;
				airportReservation.InboundItinerary.RequestVehicleNumber = 0;
				#endregion

				#region Inbound Payment
				airportReservation.InboundPaymentDetails = new ReservationsServiceSecure.Payment();
				airportReservation.InboundPaymentDetails = request.Payment;
				#endregion

				airportReservation.InboundSegmentConfirmation = null;
			}

			if (outboundSegment != null)//Outbound - From the airport
			{
				#region OutboundFare
				decimal totalFare = outboundRate.AirportReservationRate.FirstPersonFare + outboundRate.AirportReservationRate.AdditionalPersonFare * (outboundSegment.PayingPax - 1);

				airportReservation.OutboundFare = new ReservationsServiceSecure.Fare();
				airportReservation.OutboundFare.B2BCommission = 0;
				airportReservation.OutboundFare.CommissionID = 0;

				#region CompanyFees
				airportReservation.OutboundFare.CompanyFees = null;
				if (outboundFees.OutboundCompanyFeesList != null && outboundFees.OutboundCompanyFeesList.Any())
				{
					var companyFees = new List<ReservationsServiceSecure.AirportReservationFareFeeRecord>();
					foreach (var item in outboundFees.OutboundCompanyFeesList)
					{
						var fee = new ReservationsServiceSecure.AirportReservationFareFeeRecord();
						fee.FeeId = item.FeeTypeID;
						fee.FeeTypeID = item.FeeTypeID;
						fee.Amount = item.Amount;
						//fee.IsDirty = item.IsDirty;
						//fee.IsATax = item.IsATax;
						//fee.IsTaxable = item.IsTaxable;
						//fee.OverrideFee = item.OverrideFee;
						fee.TaxRate = item.TaxRate;
						companyFees.Add(fee);
						totalFare += item.Amount;
					}
					airportReservation.OutboundFare.CompanyFees = companyFees;
				}
				#endregion

				airportReservation.OutboundFare.CurrencySymbol = outboundRate.AirportReservationRate.CurrencySymbol;
				airportReservation.OutboundFare.CurrencyType = outboundRate.AirportReservationRate.CurrencyType;
				airportReservation.OutboundFare.CurrencyID = outboundRate.AirportReservationRate.CurrencyID;

				#region Discount
				airportReservation.OutboundFare.DiscountAmount = outboundFees.OutboundDiscount;
				airportReservation.OutboundFare.DiscountID = outboundFees.DiscountID;
				#endregion

				#region DriverFees
				airportReservation.OutboundFare.DriverFees = null;
				if (outboundFees.OutboundDriverFeesList != null && outboundFees.OutboundDriverFeesList.Any())
				{
					var driverFees = new List<ReservationsServiceSecure.AirportReservationFareFeeRecord>();
					foreach (var item in outboundFees.OutboundDriverFeesList)
					{
						var fee = new ReservationsServiceSecure.AirportReservationFareFeeRecord();
						fee.FeeId = item.FeeTypeID;
						fee.FeeTypeID = item.FeeTypeID;
						fee.Amount = item.Amount;
						//fee.IsDirty = item.IsDirty;
						//fee.IsATax = item.IsATax;
						//fee.IsTaxable = item.IsTaxable;
						//fee.OverrideFee = item.OverrideFee;
						fee.TaxRate = item.TaxRate;
						driverFees.Add(fee);

						totalFare += item.Amount;
					}
					airportReservation.OutboundFare.DriverFees = driverFees;
				}
				#endregion

				airportReservation.OutboundFare.FareID = outboundRate.AirportReservationRate.FareID;

				#region FuelSurcharge
				airportReservation.OutboundFare.FuelSurcharge = outboundRate.AirportReservationRate.FuelSurcharge;
				totalFare += outboundRate.AirportReservationRate.FuelSurcharge;
				#endregion

				airportReservation.OutboundFare.Gratuity = request.Gratuity;
				airportReservation.OutboundFare.ServiceTypeCode = outboundRate.AirportReservationRate.ServiceTypeCode;
				airportReservation.OutboundFare.TotalSegmentCost = totalFare;
				airportReservation.OutboundFare.FareTotal = totalFare;
				airportReservation.OutboundFare.UserOverrideFare = false;
				#endregion

				#region OutboundItinerary
				airportReservation.OutboundItinerary = new ReservationsServiceSecure.Itinerary();
				airportReservation.OutboundItinerary.ASAPRequestID = outboundPickupTime.AsapRequestId;
				airportReservation.OutboundItinerary.FlightDestinationAirport = airportCode;
				airportReservation.OutboundItinerary.FlightTime = outboundSegment.FlightDateTime;
				airportReservation.OutboundItinerary.IsDomesticFlight = outboundSegment.IsInternationalFlight ? false : true;
				airportReservation.OutboundItinerary.IsGuaranteed = outboundPickupTime.IsGuaranteed;
				airportReservation.OutboundItinerary.PickupTime = outboundPickupTime.AvailablePickupTime;
				airportReservation.OutboundItinerary.PickupEndTime = outboundPickupTime.AvailablePickupEndTime;
				airportReservation.OutboundItinerary.UsesScheduledService = outboundPickupTime.IsScheduledStop;
				airportReservation.OutboundItinerary.FlightNumber = outboundSegment.FlightNumber;
				airportReservation.OutboundItinerary.AirlineCode = outboundSegment.AirlineCode;
				airportReservation.OutboundItinerary.IsCancelled = false;
				airportReservation.OutboundItinerary.RequestAttributes = string.Empty;
				airportReservation.OutboundItinerary.RequestVehicleNumber = 0;
				#endregion

				#region Outound Payment
				airportReservation.OutboundPaymentDetails = new ReservationsServiceSecure.Payment();
				airportReservation.OutboundPaymentDetails = request.Payment;
				#endregion

				airportReservation.OutboundSegmentConfirmation = null;
			}

			#endregion

			return airportReservation;
		}
		*/
		#endregion

		#region Charter Reservation
		/*
		/// <summary>
		/// Send charter reservation request to SDS and get confirmation back
		/// </summary>
		private ReservationsServiceSecure.CharterReservationConfirmation saveCharterReservation(Segment segment, ReservationRequest request, RateResponse rateResponse, UDI.SDS.RatesService.FareScheduleWithFees firstSegmentFees, PickupTimeResponse pickupTimeResponse)
		{
			try
			{
				ReservationsServiceSecure.ClientVersionInfo clientAppVersionInfo = new ReservationsServiceSecure.ClientVersionInfo();
				ReservationsServiceSecure.CharterReservationConfirmation result = null;
				var charterReservation = SetCharterReservation(segment, request, rateResponse, pickupTimeResponse);

				logger.InfoFormat("Send to SuperShuttle \r\n{0}", charterReservation.XmlSerialize());

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Domain, "Call");
				#endregion

				using (var client = new ReservationsServiceSecure.ReservationsServiceSecureClient())
				{
					result = client.SaveCharterReservation(token.ToReservationsServiceSecureToken(), charterReservation, clientAppVersionInfo);
				}

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "SaveCharterReservation");
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call ReservationsServiceSecure.SaveCharterReservation {0}", ex.StackTrace);
				throw ex;
			}
		}
		*/

		/*
		/// <summary>
		/// Set the request of Charter Reservation, used by SaveCharterReservation method
		/// </summary>
		private ReservationsServiceSecure.CharterReservation SetCharterReservation(Segment segment, ReservationRequest request, RateResponse rateResponse, PickupTimeResponse pickupTimeResponse)
		{

			ReservationsServiceSecure.CharterReservation charterReservation = new ReservationsServiceSecure.CharterReservation();
			charterReservation.ASAPRequestID = pickupTimeResponse.AsapRequestId;
			charterReservation.AccessibleServiceRequired = segment.Wheelchairs > 0 ? true : false;
			charterReservation.AirportCode = "CHT";
			charterReservation.Comments = request.Comments;
			charterReservation.ContactNumber = request.ContactNumber;
			charterReservation.ContactNumberDialingPrefix = request.ContactNumberDialingPrefix;
			charterReservation.EmailAddress = request.EmailAddress;
			charterReservation.FirstName = request.FirstName;
			charterReservation.FreePax = (byte)(segment.ChildSeats + segment.InfantSeats);
			charterReservation.GroupID = request.GroupID;
			charterReservation.LastName = request.LastName;
			charterReservation.PayingPax = (byte)segment.PayingPax;
			charterReservation.ReserveCSR = request.ReserveCSR;
			charterReservation.Direction = 2;
			charterReservation.FranchiseCode = rateResponse.CharterRate.FranchiseCode;
			charterReservation.MemberId = 0;
			charterReservation.PhoneNumber = request.ContactNumber;
			charterReservation.PhoneNumberDialingPrefix = request.ContactNumberDialingPrefix;
			charterReservation.PickMeUpNow = false;
			charterReservation.PickupDateTime = pickupTimeResponse.AvailablePickupTime;
			charterReservation.Request = string.Empty;
			charterReservation.Attributes = string.Empty;
			charterReservation.CallCenterClientID = 0;
			charterReservation.ChangeCSR = string.Empty;
			charterReservation.CharterMinutes = segment.CharterMinutes;
			charterReservation.ClientNotes = string.Empty;
			charterReservation.FranchiseCode = rateResponse.CharterRate.FranchiseCode;
			charterReservation.FranchiseLocale = GetFranchiseLocale(rateResponse.CharterRate.FranchiseCode);
			charterReservation.Sid = request.SID;

			#region Address
			foreach (var location in segment.Locations)
			{
				//Dropoff Address
				if (location.mode == LocationMode.Dropoff)
				{
					if (location.type == LocationType.AsDirected)
					{
						charterReservation.AsDirected = true;
						charterReservation.HourlyCharterFlag = true;
						charterReservation.DropoffAddress = null;
					}
					else
					{
						charterReservation.AsDirected = false;
						charterReservation.HourlyCharterFlag = false;

						RateService rates = new RateService(token);
						rates.TrackTime = TrackTime;
						var location_new = location;
						if (location.Latitude == 0 || location.Longitude == 0)
							location_new = rates.GetGeoCodeForAddress(location);

						charterReservation.DropoffAddress = new ReservationsServiceSecure.AddressRecord();
						charterReservation.DropoffAddress.CountryCode = location.Country;
						charterReservation.DropoffAddress.CountrySubDivision = location.StateOrProvince;
						charterReservation.DropoffAddress.Latitude = location_new.Latitude;
						charterReservation.DropoffAddress.Longitude = location_new.Longitude;
						charterReservation.DropoffAddress.LocationName = location.PropertyName;
						charterReservation.DropoffAddress.LocationType = location.LocationType;
						charterReservation.DropoffAddress.Municipality = location.City;
						charterReservation.DropoffAddress.PostalCode = location.PostalCode;
						charterReservation.DropoffAddress.StreetAddress = location.StreetNumber + " " + location.StreetName;
						charterReservation.DropoffAddress.StreetName = location.StreetName;
						charterReservation.DropoffAddress.StreetNumber = location.StreetNumber;
						charterReservation.DropoffAddress.SubZipName = location.SubZipName;
						charterReservation.DropoffAddress.UnitNumber = location.Unit;
						charterReservation.DropoffAddress.Comments = string.Empty;
						charterReservation.DropoffAddress.ID = 0;
					}
				}
				//Pickup Address
				else
				{
					RateService rates = new RateService(token);
					rates.TrackTime = TrackTime;

					var location_new = location;
					if (location.Latitude == 0 || location.Longitude == 0)
						location_new = rates.GetGeoCodeForAddress(location);

					charterReservation.PickupAddress = new ReservationsServiceSecure.AddressRecord();
					charterReservation.PickupAddress.CountryCode = location.Country;
					charterReservation.PickupAddress.CountrySubDivision = location.StateOrProvince;
					charterReservation.PickupAddress.Latitude = location_new.Latitude;
					charterReservation.PickupAddress.Longitude = location_new.Longitude;
					charterReservation.PickupAddress.LocationName = location.PropertyName;
					charterReservation.PickupAddress.LocationType = location.LocationType;
					charterReservation.PickupAddress.Municipality = location.City;
					charterReservation.PickupAddress.PostalCode = location.PostalCode;
					charterReservation.PickupAddress.StreetAddress = location.StreetNumber + " " + location.StreetName;
					charterReservation.PickupAddress.StreetName = location.StreetName;
					charterReservation.PickupAddress.StreetNumber = location.StreetNumber;
					charterReservation.PickupAddress.SubZipName = location.SubZipName;
					charterReservation.PickupAddress.UnitNumber = location.Unit;
				}
			}
			#endregion

			#region FareDetails
			charterReservation.FareDetails = new ReservationsServiceSecure.CharterFare();
			charterReservation.FareDetails.B2BCommission = 0;
			charterReservation.FareDetails.CommissionID = 0;
			charterReservation.FareDetails.CompanyFees = null;
			charterReservation.FareDetails.CurrencySymbol = string.Empty;
			charterReservation.FareDetails.CurrencyType = string.Empty;
			charterReservation.FareDetails.CurrencyID = rateResponse.CharterRate.CurrencyID;
			charterReservation.FareDetails.DiscountAmount = 0;
			charterReservation.FareDetails.DiscountID = 0;
			charterReservation.FareDetails.DriverFees = null;
			charterReservation.FareDetails.FareID = rateResponse.CharterRate.FareID;
			charterReservation.FareDetails.FareTotal = charterReservation.HourlyCharterFlag == true ? (decimal)(segment.CharterMinutes * 1.0 / 60) * rateResponse.CharterRate.HourlyRate : rateResponse.CharterRate.Fare;
			charterReservation.FareDetails.FuelSurcharge = 0;
			charterReservation.FareDetails.Gratuity = request.Gratuity;
			charterReservation.FareDetails.ServiceTypeCode = rateResponse.CharterRate.ServiceTypeCode;
			charterReservation.FareDetails.UserOverrideFare = false;
			charterReservation.FareDetails.Fleet = rateResponse.CharterRate.Fleet;
			#endregion

			#region Payment
			charterReservation.PaymentDetails = new ReservationsServiceSecure.Payment();
			charterReservation.PaymentDetails = request.Payment;
			#endregion

			return charterReservation;
		}
		*/

		/*
		/// <summary>
		/// Used for Charter Reservation
		/// </summary>
		private string GetFranchiseLocale(string franchiseCode)
		{
			var franchiseLocale = string.Empty;

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Domain, "Call");
			#endregion

			using (var client = new FranchiseService.FranchiseServiceClient())
			{
				var result = client.GetFranchiseRecord(token.ToFranchiseServiceToken(), franchiseCode);
				if (result != null)
					franchiseLocale = result.Locale;
			}

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetFranchiseRecord");
			#endregion

			return franchiseLocale;
		}
		*/
		#endregion
		#endregion
		#endregion 
		#endregion

		private SecurityToken token = null;
		
		#region Set Token
		/// <summary>
		/// Used for VTOD project.
		/// </summary>
		public SecurityToken GetToken(UDI.VTOD.Common.DTO.OTA.TokenRS tokenFromVTOD)
		{
			if (tokenFromVTOD != null)
			{
				token = new SecurityToken
				{
					ClientIPAddress = tokenFromVTOD.ClientIPAddress,
					ClientType = tokenFromVTOD.ClientType.Value,
					SecurityKey = tokenFromVTOD.SecurityKey,
					UserName = tokenFromVTOD.Username
				};
			}

			return token;
		}

		/*
		/// <summary>
		/// Used by VTOD project.
		/// </summary>
		public UDI.VTOD.Common.DTO.OTA.TokenRS GetToken(UDI.VTOD.Common.DTO.OTA.TokenRQ input, out UserInfo userInfo)
		{
			userInfo = null;

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Domain, "Call");
			#endregion

			using (var securityClient = new SecurityService.WCFSecurityServiceClient())
			{
				token = securityClient.Login(
					input.ClientType.Value,
					input.ClientIPAddress,
					input.Username,
					input.Password);

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "Login");
				#endregion

				#region Extension Data - UserInfo
				string allowClosedSplits = null;
				string showSplits = null;

				try
				{
					var extensionData = Utilities.GetExtensionDataMemberValue(token);
					allowClosedSplits = extensionData["AllowClosedSplits"].ToString();
					showSplits = extensionData["ShowSplits"].ToString();
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

				userInfo = new UserInfo { UserName = token.UserName, ShowSplits = showSplits.ToBool(), AllowClosedSplits = allowClosedSplits.ToBool() };
				#endregion

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "Extension Data - UserInfo");
				#endregion

				#region Log
				logger.Info(token.XmlSerialize());
				logger.InfoFormat("AllowClosedSplits: {0} , ShowSplits: {1}", allowClosedSplits, showSplits);
				#endregion

			}
			return new VTOD.Common.DTO.OTA.TokenRS { ClientIPAddress = token.ClientIPAddress, ClientType = token.ClientType, SecurityKey = token.SecurityKey, Username = token.UserName };
		}
		*/
		#endregion

		#region Deleted
		//#region Get Pickup Time(s)
		///// <summary>
		///// Return a list of PickupTimeRecord objects for the input rate.
		///// GetRate will update location's MasterlandmarkID, LocationType and SubZipName, so input rateResponse.Segment here.
		///// </summary>
		//public List<ReservationsService.PickupTimeRecord> GetPickupTimes(Segment segment, RateResponse rateResponse, ServiceRate serviceRate, int hotelConciergeID, SecurityToken token)
		//{
		//	try
		//	{
		//		List<ReservationsService.PickupTimeRecord> result = null;

		//		PickupTimeService pickupTimeService = new PickupTimeService(token);
		//		pickupTimeService.TrackTime = TrackTime;

		//		result = pickupTimeService.GetPickupTimes(segment, rateResponse, serviceRate, hotelConciergeID);

		//		logger.InfoFormat("GetPickupTimes returns...\r\n{0}", result.XmlSerialize());

		//		return result;
		//	}
		//	catch (Exception ex)
		//	{
		//		logger.ErrorFormat("GetPickupTimes BookRequestID:{0}\r\n {1}", rateResponse.BookRequestID, ex.ToString());
		//		throw ex;
		//	}
		//}
		//#endregion 
		#endregion

		#region Deleted
		//#region Get Rate
		///// <summary>
		///// Get the lowest rate and all the rates.
		///// </summary>
		//public RateResponse GetRate(Guid bookRequestID, Segment segment, SecurityToken token, string discountCode, UserInfo userInfo)
		//{
		//	try
		//	{
		//		RateService rates = new RateService(token, userInfo);
		//		rates.TrackTime = TrackTime;

		//		RateResponse rateResponse = rates.GetRate(bookRequestID, segment, discountCode);
		//		logger.InfoFormat("GetRate returns...\r\n{0}", rateResponse.XmlSerialize());

		//		return rateResponse;
		//	}
		//	catch (Exception ex)
		//	{
		//		logger.ErrorFormat("GetRate BookRequestID:{0}\r\n {1}", bookRequestID, ex.ToString());
		//		throw ex;
		//	}
		//}
		//#endregion 
		#endregion
		
		#region Properties
		public TrackTime TrackTime { get; set; }
		#endregion
	}

}