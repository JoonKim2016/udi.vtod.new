﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.Track;
using UDI.SDS.Helper;
using UDI.SDS.ServiceAdapter;

namespace UDI.SDS
{
	public class DispatchUpdatesController: BaseSetting
	{
		#region Fields
		TrackTime _trackTime;
		TokenRS _token;
		#endregion

		#region Constructors
		private DispatchUpdatesController()
		{
		}

		public DispatchUpdatesController(TokenRS token, TrackTime trackTime)
		{
			_token = token;
			_trackTime = trackTime;
		}

		#endregion

		#region Public
		public List<DispatchUpdatesService.TripStatusRecord> GetStatus(string rezSource)
		{
			#region Init
			UDI.SDS.ReservationsService.SecurityToken reservationToken = _token.ToReservationServiceToken();
			var dispatchAdapter = new DispatchUpdatesServiceAdapter(reservationToken, _trackTime);
			#endregion

			var result = dispatchAdapter.GetStatus(rezSource);

			return result;
		}
		#endregion

		#region Properties
		public TrackTime TrackTime
		{
			get
			{
				return _trackTime;
			}
			set
			{
				_trackTime = value;
			}
		}

		public TokenRS Token
		{
			set
			{
				_token = value;
			}
		}
		#endregion

	}
}
