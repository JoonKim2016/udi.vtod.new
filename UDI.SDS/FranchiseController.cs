﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.Track;
using UDI.SDS.Helper;
using UDI.SDS.FranchiseService;
using UDI.SDS.DTO.Enum;
using UDI.SDS.DTO;
using UDI.SDS.ServiceAdapter;

namespace UDI.SDS
{
	public class FranchiseController : BaseSetting
	{
		#region Fields
		TrackTime _trackTime;
		TokenRS _token;
		#endregion

		#region Constructors
		private FranchiseController()
		{
		}

		public FranchiseController(TokenRS token, TrackTime trackTime)
		{
			_token = token;
			_trackTime = trackTime;
		}

		#endregion

		#region Public
		public string GetFranchiseLocale(string franchiseCode)
		{
			#region Init
			UDI.SDS.FranchiseService.SecurityToken franchiseToken = _token.ToFranchiseServiceToken();
			var franchiseAdapter = new FranchiseServiceAdapter(franchiseToken, _trackTime);
			#endregion

			var result = franchiseAdapter.GetFranchiseLocale(franchiseCode);

			return result;
		}

		public string GetFranchiseCode(string airportCode, string postalCode, int tripDirection)
		{
			#region Init
			UDI.SDS.FranchiseService.SecurityToken franchiseToken = _token.ToFranchiseServiceToken();
			var franchiseAdapter = new FranchiseServiceAdapter(franchiseToken, _trackTime);
			#endregion

			var result = franchiseAdapter.GetFranchiseCode(airportCode,postalCode,tripDirection );
			return result;
		}

		#endregion

		#region Properties
		public TrackTime TrackTime
		{
			get
			{
				return _trackTime;
			}
			set
			{
				_trackTime = value;
			}
		}

		public TokenRS Token
		{
			set
			{
				_token = value;
			}
		}
		#endregion
	}
}
