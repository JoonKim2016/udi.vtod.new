﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.Track;
using UDI.SDS.Helper;
using UDI.SDS.GeocoderService;
using UDI.SDS.DTO.Enum;
using UDI.SDS.DTO;
using UDI.SDS.ServiceAdapter;

namespace UDI.SDS
{
	public class GeocoderController : BaseSetting
	{
		#region Fields
		TrackTime _trackTime;
		TokenRS _token;
		#endregion

		#region Constructors
		private GeocoderController()
		{
		}

		public GeocoderController(TokenRS token, TrackTime trackTime)
		{
			_token = token;
			_trackTime = trackTime;
		}

		#endregion

		#region Public
		public void ModifyAddress(UDI.SDS.DTO.Location location, out bool modified)
		{
			#region Init
			UDI.SDS.GeocoderService.SecurityToken geocoderToken = _token.ToGeocoderServiceToken();
			var geocoderAdapter = new GeocoderServiceAdapter(geocoderToken, _trackTime);
			#endregion

			geocoderAdapter.ModifyAddress(location, out modified);

			//return result;

		}
		#endregion

		#region Properties
		public TrackTime TrackTime
		{
			get
			{
				return _trackTime;
			}
			set
			{
				_trackTime = value;
			}
		}

		public TokenRS Token
		{
			set
			{
				_token = value;
			}
		}
		#endregion
	}
}
