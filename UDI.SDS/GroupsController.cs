﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.Track;
using UDI.SDS.Helper;
using UDI.SDS.FranchiseService;
using UDI.SDS.DTO.Enum;
using UDI.SDS.DTO;
using UDI.SDS.ServiceAdapter;
using UDI.SDS.GroupsService;

namespace UDI.SDS
{
	public class GroupsController : BaseSetting
	{
		#region Fields
		TrackTime _trackTime;
		TokenRS _token;
		#endregion

		#region Constructors
		private GroupsController()
		{
		}

		public GroupsController(TokenRS token, TrackTime trackTime)
		{
			_token = token;
			_trackTime = trackTime;
		}

		#endregion

		#region Public
		public int ValidateDiscountCode(string discountCode)
		{
			#region Init
			UDI.SDS.GroupsService.SecurityToken groupsToken = _token.ToGroupServiceToken();
			var groupsAdapter = new GroupsServiceAdapter(groupsToken, _trackTime);
			#endregion

			var result = groupsAdapter.ValidateDiscountCode(discountCode);

			return result;
		}

		public Discount GetDiscount(int discountID, string airportCode)
		{
			#region Init
			UDI.SDS.GroupsService.SecurityToken groupsToken = _token.ToGroupServiceToken();
			var groupsAdapter = new GroupsServiceAdapter(groupsToken, _trackTime);
			#endregion

			var result = groupsAdapter.GetDiscount(discountID, airportCode);

			return result;
		}

		public GetPromotionCodeTypeResponse GetPromotionCodeType(string promotionCode)
		{
			#region Init
			UDI.SDS.GroupsService.SecurityToken groupsToken = _token.ToGroupServiceToken();
			var groupsAdapter = new GroupsServiceAdapter(groupsToken, _trackTime);
			#endregion

			var result = groupsAdapter.GetPromotionCodeType(promotionCode);

			return result;
		}
		
		public GroupReservationDefaults GetDefaults(int discountID)
		{
			#region Init
			UDI.SDS.GroupsService.SecurityToken groupsToken = _token.ToGroupServiceToken();
			var groupsAdapter = new GroupsServiceAdapter(groupsToken, _trackTime);
			#endregion

			var result = groupsAdapter.GetDefaults(discountID);

			return result;
		}

		public LandmarksService.LandmarkRecords GetLandmarks(int groupID, string airportCode)
		{
			#region Init
			UDI.SDS.LandmarksService.SecurityToken landmarkToken = _token.ToLandmarksServiceToken();
			var landmarkAdapter = new LandmarksServiceServiceAdapter(landmarkToken, _trackTime);
			#endregion

			var result = landmarkAdapter.GetGroupLandmarks(groupID, airportCode);

			return result;
		}

		public UDI.SDS.UtilityService.AirportRecords GetLocalizedServicedAirports(int groupID, string localizationCode)
		{
			#region Init
			UDI.SDS.UtilityService.SecurityToken utilityToken = _token.ToUtilityServiceToken();
			var utilityAdapter = new UtilityServiceAdapter(utilityToken, _trackTime);
			#endregion

			var result = utilityAdapter.GetLocalizedGroupServicedAirports(groupID, localizationCode);

			return result;
		}
		#endregion

		#region Properties
		public TrackTime TrackTime
		{
			get
			{
				return _trackTime;
			}
			set
			{
				_trackTime = value;
			}
		}

		public TokenRS Token
		{
			set
			{
				_token = value;
			}
		}
		#endregion
	}
}
