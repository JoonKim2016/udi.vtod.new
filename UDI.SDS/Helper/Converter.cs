﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.SDS.DTO.Enum;
using UDI.SDS.ReservationsService;
using UDI.VTOD.Common.DTO.OTA;
using UDI.Utility.Helper;

namespace UDI.SDS.Helper
{
	public static class Converter
	{
		public static int ToMasterLandmarkID(this LandmarksService.LandmarkRecord landmarkRecord)
		{
			var result = Defaults.MasterLandmarkID;// landmarkRecord.MasterLandmarkID;

			if (landmarkRecord != null)
				result = landmarkRecord.MasterLandmarkID;

			return result;
		}

		public static int ToLandmarkID(this LandmarksService.LandmarkRecord landmarkRecord)
		{
			var result = Defaults.LandmarkID;

			if (landmarkRecord != null)
			{
				result = landmarkRecord.LandmarkID;
			}

			return result;
		}

		public static string ToLocationType(this LandmarksService.LandmarkRecord landmarkRecord)
		{
			var result = string.Empty;

			if (landmarkRecord != null && landmarkRecord.LandmarkAddress != null)
				result = landmarkRecord.LandmarkAddress.LocationType;

			if (string.IsNullOrWhiteSpace(result))
				result = Defaults.LocationType; ;

			return result;
		}

		public static string ToSubZipName(this LandmarksService.LandmarkRecord landmarkRecord)
		{
			var result = Defaults.SubZipName;

			if (landmarkRecord != null && landmarkRecord.LandmarkAddress != null)
				return landmarkRecord.LandmarkAddress.SubZipName;

			return result;
		}

		public static string ToLocationName(this LandmarksService.LandmarkRecord landmarkRecord)
		{
			var result = Defaults.LocationName;

			if (landmarkRecord != null && landmarkRecord.LandmarkAddress != null)
				return landmarkRecord.LandmarkAddress.LocationName;

			return result;
		}

		public static SDS.ReservationsServiceSecure.CreditCardTypesEnumeration ToCreditCard(this string input)
		{
			var result = SDS.ReservationsServiceSecure.CreditCardTypesEnumeration.Unknown;

			try
			{
				switch (input.ToLower())
				{
					case "mc":
					case "mastercard":
					case "master card":
						result = ReservationsServiceSecure.CreditCardTypesEnumeration.MC;
						break;
					case "amex":
					case "americanexpress":
					case "american express":
						result = ReservationsServiceSecure.CreditCardTypesEnumeration.AMEX;
						break;
					case "diners":
						result = ReservationsServiceSecure.CreditCardTypesEnumeration.DINERS;
						break;
					case "disc":
					case "discover":
						result = ReservationsServiceSecure.CreditCardTypesEnumeration.DISC;
						break;
					case "jcb":
						result = ReservationsServiceSecure.CreditCardTypesEnumeration.JCB;
						break;
					case "visa":
						result = ReservationsServiceSecure.CreditCardTypesEnumeration.VISA;
						break;
				}
			}
			catch
			{ }

			return result;
		}

		public static SDS.MembershipService.CreditCardTypesEnumeration ToMembershipServiceCreditCardType(this string input)
		{
			var result = SDS.MembershipService.CreditCardTypesEnumeration.Unknown;

			try
			{
				switch (input.ToLower())
				{
					case "mc":
					case "mastercard":
					case "master card":
						result = MembershipService.CreditCardTypesEnumeration.MC;
						break;
					case "amex":
					case "americanexpress":
					case "american express":
						result = MembershipService.CreditCardTypesEnumeration.AMEX;
						break;
					case "diners":
					case "DinersClub":
						result = MembershipService.CreditCardTypesEnumeration.DINERS;
						break;
					case "disc":
					case "discover":
						result = MembershipService.CreditCardTypesEnumeration.DISC;
						break;
					case "jcb":
						result = MembershipService.CreditCardTypesEnumeration.JCB;
						break;
					case "visa":
						result = MembershipService.CreditCardTypesEnumeration.VISA;
						break;
				}
			}
			catch
			{ }

			return result;
		}

		public static PickupTimeStatus ToPickupTimeStatus(this PickupTimeStatusCodesEnumeration input)
		{
			PickupTimeStatus result = PickupTimeStatus.Unknown;
			switch (input)
			{
				case PickupTimeStatusCodesEnumeration.AllowPickup:
					result = PickupTimeStatus.AllowPickup;
					break;
				case PickupTimeStatusCodesEnumeration.PickupExpired:
					result = PickupTimeStatus.PickupExpired;
					break;
				case PickupTimeStatusCodesEnumeration.MinAdvanceNoticeExceeded:
					result = PickupTimeStatus.MinAdvanceNoticeExceeded;
					break;
				case PickupTimeStatusCodesEnumeration.SlotClosed:
					result = PickupTimeStatus.SlotClosed;
					break;
				case PickupTimeStatusCodesEnumeration.OutboundClosed:
					result = PickupTimeStatus.OutboundClosed;
					break;
				case PickupTimeStatusCodesEnumeration.OutsideCollectionSchedule:
					result = PickupTimeStatus.OutsideCollectionSchedule;
					break;
			}

			return result;
		}

		public static SDSFleet ToSDSFleet(this string input)
		{
			SDSFleet result = SDSFleet.Unknown;
			switch (input.ToLower())
			{
				case "all":
					result = SDSFleet.All;
					break;
				case "execucar":
					result = SDSFleet.ExecuCar;
					break;
				case "none":
					result = SDSFleet.Unknown;
					break;
				case "supershuttle":
					result = SDSFleet.SuperShuttle;
					break;
				case "supershuttle_execucar":
					result = SDSFleet.All;
					break;
				case "supershuttlesharedrideonly":
					result = SDSFleet.SuperShuttleSharedRideOnly;
					break;
			}

			return result;
		}

		public static PaymentType ToSDSPaymentType(this string input)
		{
			PaymentType result = PaymentType.Unknown;
			switch (input.ToLower())
			{
				case "paymentcard":
					result = PaymentType.PaymentCard;
					break;
				case "cash":
					result = PaymentType.Cash;
					break;
				case "directbill":
					result = PaymentType.DirectBill;
					break;
				case "paymentcardhold":
					result = PaymentType.PaymentCardHold;
					break;
			}

			return result;
		}

		#region Token
		#region OTA
		public static UDI.SDS.ASAPService.SecurityToken ToASAPServiceToken(this TokenRS input)
		{
			UDI.SDS.ASAPService.SecurityToken result = null;
			if (input != null)
			{
				result = new UDI.SDS.ASAPService.SecurityToken
				{
					ClientIPAddress = input.ClientIPAddress,
					ClientType = input.ClientType.Value,
					SecurityKey = input.SecurityKey.ToGuid(),
					UserName = input.Username
				};
			}
			return result;
		}

		public static UDI.SDS.RatesService.SecurityToken ToRatesServiceToken(this TokenRS input)
		{
			UDI.SDS.RatesService.SecurityToken result = null;
			if (input != null)
			{
				result = new UDI.SDS.RatesService.SecurityToken
				{
					ClientIPAddress = input.ClientIPAddress,
					ClientType = input.ClientType.Value,
					SecurityKey = input.SecurityKey.ToGuid(),
					UserName = input.Username
				};
			}
			return result;
		}

		public static UDI.SDS.SecurityService.SecurityToken ToSecurityServiceToken(this TokenRS input)
		{
			UDI.SDS.SecurityService.SecurityToken result = null;
			if (input != null)
			{
				result = new UDI.SDS.SecurityService.SecurityToken
				{
					ClientIPAddress = input.ClientIPAddress,
					ClientType = input.ClientType.Value,
					SecurityKey = input.SecurityKey.ToGuid(),
					UserName = input.Username
				};
			}
			return result;
		}

		public static UDI.SDS.ReservationsService.SecurityToken ToReservationServiceToken(this TokenRS input)
		{
			UDI.SDS.ReservationsService.SecurityToken result = null;
			if (input != null)
			{
				result = new UDI.SDS.ReservationsService.SecurityToken
				{
					ClientIPAddress = input.ClientIPAddress,
					ClientType = input.ClientType.Value,
					SecurityKey = input.SecurityKey.ToGuid(),
					UserName = input.Username
				};
			}
			return result;
		}

		public static UDI.SDS.ReservationsServiceSecure.SecurityToken ToReservationSecureServiceToken(this TokenRS input)
		{
			UDI.SDS.ReservationsServiceSecure.SecurityToken result = null;
			if (input != null)
			{
				result = new UDI.SDS.ReservationsServiceSecure.SecurityToken
				{
					ClientIPAddress = input.ClientIPAddress,
					ClientType = input.ClientType.Value,
					SecurityKey = input.SecurityKey.ToGuid(),
					UserName = input.Username
				};
			}
			return result;
		}

		public static UDI.SDS.GeocoderService.SecurityToken ToGeocoderServiceToken(this TokenRS input)
		{
			UDI.SDS.GeocoderService.SecurityToken result = null;
			if (input != null)
			{
				result = new UDI.SDS.GeocoderService.SecurityToken
				{
					ClientIPAddress = input.ClientIPAddress,
					ClientType = input.ClientType.Value,
					SecurityKey = input.SecurityKey.ToGuid(),
					UserName = input.Username
				};
			}
			return result;
		}

		public static UDI.SDS.FranchiseService.SecurityToken ToFranchiseServiceToken(this TokenRS input)
		{
			UDI.SDS.FranchiseService.SecurityToken result = null;
			if (input != null)
			{
				result = new UDI.SDS.FranchiseService.SecurityToken
				{
					ClientIPAddress = input.ClientIPAddress,
					ClientType = input.ClientType.Value,
					SecurityKey = input.SecurityKey.ToGuid(),
					UserName = input.Username
				};
			}
			return result;
		}

		public static UDI.SDS.GroupsService.SecurityToken ToGroupServiceToken(this TokenRS input)
		{
			UDI.SDS.GroupsService.SecurityToken result = null;
			if (input != null)
			{
				result = new UDI.SDS.GroupsService.SecurityToken
				{
					ClientIPAddress = input.ClientIPAddress,
					ClientType = input.ClientType.Value,
					SecurityKey = input.SecurityKey.ToGuid(),
					UserName = input.Username
				};
			}
			return result;
		}

		public static UDI.SDS.ZipCodesService.SecurityToken ToZipCodesServiceToken(this TokenRS input)
		{
			UDI.SDS.ZipCodesService.SecurityToken result = null;
			if (input != null)
			{
				result = new UDI.SDS.ZipCodesService.SecurityToken
				{
					ClientIPAddress = input.ClientIPAddress,
					ClientType = input.ClientType.Value,
					SecurityKey = input.SecurityKey.ToGuid(),
					UserName = input.Username
				};
			}
			return result;
		}

		public static UDI.SDS.UtilityService.SecurityToken ToUtilityServiceToken(this TokenRS input)
		{
			UDI.SDS.UtilityService.SecurityToken result = null;
			if (input != null)
			{
				result = new UDI.SDS.UtilityService.SecurityToken
				{
					ClientIPAddress = input.ClientIPAddress,
					ClientType = input.ClientType.Value,
					SecurityKey = input.SecurityKey.ToGuid(),
					UserName = input.Username
				};
			}
			return result;
		}

		public static UDI.SDS.RoutingService.SecurityToken ToRoutingServiceToken(this TokenRS input)
		{
			UDI.SDS.RoutingService.SecurityToken result = null;
			if (input != null)
			{
				result = new UDI.SDS.RoutingService.SecurityToken
				{
					ClientIPAddress = input.ClientIPAddress,
					ClientType = input.ClientType.Value,
					SecurityKey = input.SecurityKey.ToGuid(),
					UserName = input.Username
				};
			}
			return result;
		}

		public static UDI.SDS.LandmarksService.SecurityToken ToLandmarksServiceToken(this TokenRS input)
		{
			UDI.SDS.LandmarksService.SecurityToken result = null;
			if (input != null)
			{
				result = new UDI.SDS.LandmarksService.SecurityToken
				{
					ClientIPAddress = input.ClientIPAddress,
					ClientType = input.ClientType.Value,
					SecurityKey = input.SecurityKey.ToGuid(),
					UserName = input.Username
				};
			}
			return result;
		}

		public static UDI.SDS.MembershipService.SecurityToken ToMembershipServiceToken(this TokenRS input)
		{
			UDI.SDS.MembershipService.SecurityToken result = null;
			if (input != null)
			{
				result = new UDI.SDS.MembershipService.SecurityToken
				{
					ClientIPAddress = input.ClientIPAddress,
					ClientType = input.ClientType.Value,
					SecurityKey = input.SecurityKey.ToGuid(),
					UserName = input.Username
				};
			}
			return result;
		}

		public static UDI.SDS.PaymentsService.SecurityToken ToPaymentsServiceToken(this TokenRS input)
		{
			UDI.SDS.PaymentsService.SecurityToken result = null;
			if (input != null)
			{
				result = new UDI.SDS.PaymentsService.SecurityToken
				{
					ClientIPAddress = input.ClientIPAddress,
					ClientType = input.ClientType.Value,
					SecurityKey = input.SecurityKey.ToGuid(),
					UserName = input.Username
				};
			}
			return result;
		}

		public static UDI.SDS.VehiclesService.SecurityToken ToVehicleServiceToken(this TokenRS input)
		{
			UDI.SDS.VehiclesService.SecurityToken result = null;
			if (input != null)
			{
				result = new UDI.SDS.VehiclesService.SecurityToken
				{
					ClientIPAddress = input.ClientIPAddress,
					ClientType = input.ClientType.Value,
					SecurityKey = input.SecurityKey.ToGuid(),
					UserName = input.Username
				};
			}
			return result;
		}

		public static UDI.SDS.ProxyService.SecurityToken ToProxyServiceToken(this TokenRS input)
		{
			UDI.SDS.ProxyService.SecurityToken result = null;
			if (input != null)
			{
				result = new UDI.SDS.ProxyService.SecurityToken
				{
					ClientIPAddress = input.ClientIPAddress,
					ClientType = input.ClientType.Value,
					SecurityKey = input.SecurityKey.ToGuid(),
					UserName = input.Username
				};
			}
			return result;
		}

		public static UDI.SDS.WMVService.SecurityToken ToWMVServiceToken(this TokenRS input)
		{
			UDI.SDS.WMVService.SecurityToken result = null;
			if (input != null)
			{
				result = new UDI.SDS.WMVService.SecurityToken
				{
					ClientIPAddress = input.ClientIPAddress,
					ClientType = input.ClientType.Value,
					SecurityKey = input.SecurityKey.ToGuid(),
					UserName = input.Username
				};
			}
			return result;
		}

		#endregion

		#region Non-OTA
		public static UDI.SDS.UtilityService.SecurityToken ToUtilityServiceToken(this UDI.SDS.SecurityService.SecurityToken input)
		{
			UDI.SDS.UtilityService.SecurityToken result = null;
			if (input != null)
			{
				result = new UtilityService.SecurityToken
				{
					ClientIPAddress = input.ClientIPAddress,
					ClientType = input.ClientType,
					SecurityKey = input.SecurityKey,
					UserName = input.UserName
				};
			}
			return result;
		}

		public static UDI.SDS.ZipCodesService.SecurityToken ToZipCodesServiceToken(this UDI.SDS.SecurityService.SecurityToken input)
		{
			UDI.SDS.ZipCodesService.SecurityToken result = null;
			if (input != null)
			{
				result = new ZipCodesService.SecurityToken
				{
					ClientIPAddress = input.ClientIPAddress,
					ClientType = input.ClientType,
					SecurityKey = input.SecurityKey,
					UserName = input.UserName
				};
			}
			return result;
		}

		public static UDI.SDS.FranchiseService.SecurityToken ToFranchiseServiceToken(this UDI.SDS.SecurityService.SecurityToken input)
		{
			UDI.SDS.FranchiseService.SecurityToken result = null;
			if (input != null)
			{
				result = new FranchiseService.SecurityToken
				{
					ClientIPAddress = input.ClientIPAddress,
					ClientType = input.ClientType,
					SecurityKey = input.SecurityKey,
					UserName = input.UserName
				};
			}
			return result;
		}

		public static UDI.SDS.LandmarksService.SecurityToken ToLandmarksServiceToken(this UDI.SDS.SecurityService.SecurityToken input)
		{
			UDI.SDS.LandmarksService.SecurityToken result = null;
			if (input != null)
			{
				result = new LandmarksService.SecurityToken
				{
					ClientIPAddress = input.ClientIPAddress,
					ClientType = input.ClientType,
					SecurityKey = input.SecurityKey,
					UserName = input.UserName
				};
			}
			return result;
		}

		public static UDI.SDS.RatesService.SecurityToken ToRatesServiceToken(this UDI.SDS.SecurityService.SecurityToken input)
		{
			UDI.SDS.RatesService.SecurityToken result = null;
			if (input != null)
			{
				result = new RatesService.SecurityToken
				{
					ClientIPAddress = input.ClientIPAddress,
					ClientType = input.ClientType,
					SecurityKey = input.SecurityKey,
					UserName = input.UserName
				};
			}
			return result;
		}

		public static UDI.SDS.GeocoderService.SecurityToken ToGeocoderServiceToken(this UDI.SDS.SecurityService.SecurityToken input)
		{
			UDI.SDS.GeocoderService.SecurityToken result = null;
			if (input != null)
			{
				result = new GeocoderService.SecurityToken
				{
					ClientIPAddress = input.ClientIPAddress,
					ClientType = input.ClientType,
					SecurityKey = input.SecurityKey,
					UserName = input.UserName
				};
			}
			return result;
		}

		public static UDI.SDS.ReservationsService.SecurityToken ToReservationsServiceToken(this UDI.SDS.SecurityService.SecurityToken input)
		{
			UDI.SDS.ReservationsService.SecurityToken result = null;
			if (input != null)
			{
				result = new ReservationsService.SecurityToken
				{
					ClientIPAddress = input.ClientIPAddress,
					ClientType = input.ClientType,
					SecurityKey = input.SecurityKey,
					UserName = input.UserName
				};
			}
			return result;
		}

		public static UDI.SDS.ReservationsServiceSecure.SecurityToken ToReservationsServiceSecureToken(this UDI.SDS.SecurityService.SecurityToken input)
		{
			UDI.SDS.ReservationsServiceSecure.SecurityToken result = null;
			if (input != null)
			{
				result = new ReservationsServiceSecure.SecurityToken
				{
					ClientIPAddress = input.ClientIPAddress,
					ClientType = input.ClientType,
					SecurityKey = input.SecurityKey,
					UserName = input.UserName
				};
			}
			return result;
		}

		public static UDI.SDS.RoutingService.SecurityToken ToRoutingServiceSecureToken(this UDI.SDS.SecurityService.SecurityToken input)
		{
			UDI.SDS.RoutingService.SecurityToken result = null;
			if (input != null)
			{
				result = new RoutingService.SecurityToken
				{
					ClientIPAddress = input.ClientIPAddress,
					ClientType = input.ClientType,
					SecurityKey = input.SecurityKey,
					UserName = input.UserName
				};
			}
			return result;
		}

		public static UDI.SDS.PaymentsService.SecurityToken ToPaymentsServiceToken(this UDI.SDS.SecurityService.SecurityToken input)
		{
			UDI.SDS.PaymentsService.SecurityToken result = null;
			if (input != null)
			{
				result = new PaymentsService.SecurityToken
				{
					ClientIPAddress = input.ClientIPAddress,
					ClientType = input.ClientType,
					SecurityKey = input.SecurityKey,
					UserName = input.UserName
				};
			}
			return result;
		}

		public static UDI.SDS.ASAPService.SecurityToken ToASAPServiceToken(this UDI.SDS.SecurityService.SecurityToken input)
		{
			UDI.SDS.ASAPService.SecurityToken result = null;
			if (input != null)
			{
				result = new ASAPService.SecurityToken
				{
					ClientIPAddress = input.ClientIPAddress,
					ClientType = input.ClientType,
					SecurityKey = input.SecurityKey,
					UserName = input.UserName
				};
			}
			return result;
		}

		public static UDI.SDS.GroupsService.SecurityToken ToGroupsServiceToken(this UDI.SDS.SecurityService.SecurityToken input)
		{
			UDI.SDS.GroupsService.SecurityToken result = null;
			if (input != null)
			{
				result = new GroupsService.SecurityToken
				{
					ClientIPAddress = input.ClientIPAddress,
					ClientType = input.ClientType,
					SecurityKey = input.SecurityKey,
					UserName = input.UserName
				};
			}
			return result;
		}

		public static UDI.SDS.MembershipService.SecurityToken ToMembershipServiceToken(this UDI.SDS.SecurityService.SecurityToken input)
		{
			UDI.SDS.MembershipService.SecurityToken result = null;
			if (input != null)
			{
				result = new MembershipService.SecurityToken
				{
					ClientIPAddress = input.ClientIPAddress,
					ClientType = input.ClientType,
					SecurityKey = input.SecurityKey,
					UserName = input.UserName
				};
			}
			return result;
		}

		#endregion
		#endregion
	}
}
