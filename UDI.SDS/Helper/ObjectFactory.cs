﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.SDS.DTO;
using UDI.SDS.DTO.Enum;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Common.Track;
using UDI.Utility.Helper;

namespace UDI.SDS.Helper
{
	public class ObjectFactory
	{

		public ReservationsServiceSecure.AirportReservation GetAirportReservationObject(Segment firstSegment, Segment secondSegment, ReservationRequest request, RatesResponse rateResponseFirstSegment, PickupTimeResponse pickupTimeResponseFirstSegment, RatesResponse rateResponseSecondSegment, PickupTimeResponse pickupTimeResponseSecondSegment)
		{

			ReservationsServiceSecure.AirportReservation airportReservation = new ReservationsServiceSecure.AirportReservation();
			
			airportReservation.AccessibleServiceRequired = firstSegment.Wheelchairs > 0 ? true : false;
			airportReservation.AirlineMilesDetails = null;
            airportReservation.CultureCode = request.CultureCode;
            if (!string.IsNullOrEmpty(request.RewardsAccountNumber) && request.RewardsID.HasValue)
			{
				airportReservation.AirlineMilesDetails = new ReservationsServiceSecure.AirlineMileageAccounts();
				airportReservation.AirlineMilesDetails.AccountNumber = request.RewardsAccountNumber;
				airportReservation.AirlineMilesDetails.RewardsID = request.RewardsID.Value;
			}
			airportReservation.ChangeCSR = string.Empty;
			airportReservation.ChildSeats = firstSegment.IsChildSeats;
            airportReservation.InfantSeats = firstSegment.IsInfantSeats;
            airportReservation.Comments = request.Comments;
			airportReservation.ContactNumber = request.ContactNumber;
			airportReservation.ContactNumberDialingPrefix = request.ContactNumberDialingPrefix;
			airportReservation.EmailAddress = request.EmailAddress;
			airportReservation.FirstName = request.FirstName;
            airportReservation.FreePax = 0;//Doug : FreePax will always be zero. That field is obsolete.
			airportReservation.GroupID = request.GroupID;
			airportReservation.LastName = request.LastName;
			airportReservation.Luggage = (byte)request.LuggageCount;
			airportReservation.PayingPax = (byte)firstSegment.PayingPax;
			airportReservation.ReserveCSR = request.ReserveCsr;
			airportReservation.ServiceNotes = request.ServiceNotes;
			airportReservation.SID = request.SID;
			airportReservation.AllowSms = request.AllowSMS;
			airportReservation.SubscribeToPromotions = request.SubscribeToPromotions.HasValue ? request.SubscribeToPromotions.Value : false;
			airportReservation.SpecialHandling = request.SpecialHandling;
			airportReservation.TripDirection = secondSegment == null ? (int)firstSegment.TripDirection : (int)TripDirection.RoundTrip;            

            #region GuestAddress
            foreach (var location in firstSegment.Locations)
			{
				if (
					(firstSegment.TripDirection == TripDirection.Inbound && location.mode == LocationMode.Pickup)//Inbound
					||
					(firstSegment.TripDirection == TripDirection.Outbound && location.mode == LocationMode.Dropoff)//Outbound
					||
					(firstSegment.TripDirection == TripDirection.RoundTrip && location.type == LocationType.Address)//RoundTrip
					)
				{
					airportReservation.GuestAddress = new ReservationsServiceSecure.AddressRecord();
					airportReservation.GuestAddress.CountryCode = location.Country;
					airportReservation.GuestAddress.CountrySubDivision = location.StateOrProvince;
					airportReservation.GuestAddress.Latitude = location.Latitude;
					airportReservation.GuestAddress.Longitude = location.Longitude;
					airportReservation.GuestAddress.LocationName = location.PropertyName;
					airportReservation.GuestAddress.LocationType = location.LocationType;
					airportReservation.GuestAddress.Municipality = location.City;
					airportReservation.GuestAddress.PostalCode = location.PostalCode;
					airportReservation.GuestAddress.StreetAddress = location.StreetNumber + " " + location.StreetName;
					airportReservation.GuestAddress.StreetName = location.StreetName;
					airportReservation.GuestAddress.StreetNumber = location.StreetNumber;
					airportReservation.GuestAddress.SubZipName = location.SubZipName;
                    airportReservation.GuestAddress.UnitNumber = location.Unit;//for adding bldgroom to the unit
					//airportReservation.GuestAddress.UnitNumber = location.Unit;
					airportReservation.GuestAddress.Comments = string.Empty;
					airportReservation.GuestAddress.ID = 0;
					airportReservation.GuestAddress.PhoneNumber = string.Empty;
					airportReservation.GuestAddress.PhoneNumberDialingPrefix = string.Empty;
				}				
			}
            
			#endregion

			#region Inbound, Outbound Information

			var inboundSegment = new Segment();
			var outboundSegment = new Segment();

			var inboundRate = new RatesResponse();
			var outboundRate = new RatesResponse();

			//var inboundFees = new UDI.SDS.RatesService.FareScheduleWithFees();
			//var outboundFees = new UDI.SDS.RatesService.FareScheduleWithFees();

			var inboundPickupTime = new PickupTimeResponse();
			var outboundPickupTime = new PickupTimeResponse();

			if (firstSegment.TripDirection == TripDirection.Inbound)
			{
				inboundSegment = firstSegment;
             
				outboundSegment = secondSegment;

				inboundRate = rateResponseFirstSegment;
				outboundRate = rateResponseSecondSegment;

				//inboundFees = firstSegmentFees;
				//outboundFees = secondSegmentFees;

				inboundPickupTime = pickupTimeResponseFirstSegment;
				outboundPickupTime = pickupTimeResponseSecondSegment;
			}

			if (firstSegment.TripDirection == TripDirection.Outbound)
			{
				inboundSegment = secondSegment;
				outboundSegment = firstSegment;

				inboundRate = rateResponseSecondSegment;
				outboundRate = rateResponseFirstSegment;

				//inboundFees = secondSegmentFees;
				//outboundFees = firstSegmentFees;

				inboundPickupTime = pickupTimeResponseSecondSegment;
				outboundPickupTime = pickupTimeResponseFirstSegment;
			}

			if (inboundSegment != null)//Inbound - To the airport
			{
				#region InboundFare
				decimal totalFare = inboundRate.ServiceInfoRates.First().FirstPassengerFare + inboundRate.ServiceInfoRates.First().AdditionalPassengerFare * (inboundSegment.PayingPax - 1);

				airportReservation.InboundFare = new ReservationsServiceSecure.Fare();
				airportReservation.InboundFare.B2BCommission = inboundRate.ServiceInfoRates.First().Commission;
				airportReservation.InboundFare.CommissionID = inboundRate.ServiceInfoRates.First().CommissionID;

				#region CompanyFees
				airportReservation.InboundFare.CompanyFees = null;
				//if (inboundFees.InboundCompanyFeesList != null && inboundFees.InboundCompanyFeesList.Any())
				if (inboundRate.ServiceInfoRates.First().CompanyFees != null && inboundRate.ServiceInfoRates.First().CompanyFees.Any())
				{
					var companyFees = new List<ReservationsServiceSecure.AirportReservationFareFeeRecord>();
					foreach (var item in inboundRate.ServiceInfoRates.First().CompanyFees)
					{
						var fee = new ReservationsServiceSecure.AirportReservationFareFeeRecord();
						fee.FeeId = 0;// Doug asked me to hardcode it to 0 // item.FeeTypeID;
						fee.FeeTypeID = item.FeeTypeID;
						fee.Amount = item.Amount;
						//fee.IsDirty = item.IsDirty;
						//fee.IsATax = item.IsATax;
						//fee.IsTaxable = item.IsTaxable;
						//fee.OverrideFee = item.OverrideFee;
						fee.TaxRate = item.TaxRate;
						companyFees.Add(fee);
						totalFare += item.Amount;
					}
					airportReservation.InboundFare.CompanyFees = companyFees;
				}
				#endregion

				airportReservation.InboundFare.CurrencySymbol = inboundRate.ServiceInfoRates.First().CurrencySymbol;
				airportReservation.InboundFare.CurrencyType = inboundRate.ServiceInfoRates.First().CurrencyType;
				airportReservation.InboundFare.CurrencyID = inboundRate.ServiceInfoRates.First().CurrencyID;

				#region Discount
				airportReservation.InboundFare.DiscountAmount = inboundRate.ServiceInfoRates.First().Discount;
				airportReservation.InboundFare.DiscountID = inboundRate.ServiceInfoRates.First().DiscountID;
				#endregion

				#region DriverFees
				airportReservation.InboundFare.DriverFees = null;
				if (inboundRate.ServiceInfoRates.First().DriverFees != null && inboundRate.ServiceInfoRates.First().DriverFees.Any())
				{
					var driverFees = new List<ReservationsServiceSecure.AirportReservationFareFeeRecord>();
					foreach (var item in inboundRate.ServiceInfoRates.First().DriverFees)
					{
						var fee = new ReservationsServiceSecure.AirportReservationFareFeeRecord();
						fee.FeeId = 0;// Doug asked me to hardcode it to 0 //  item.FeeTypeID;
						fee.FeeTypeID = item.FeeTypeID;
						fee.Amount = item.Amount;
						//fee.IsDirty = item.IsDirty;
						//fee.IsATax = item.IsATax;
						//fee.IsTaxable = item.IsTaxable;
						//fee.OverrideFee = item.OverrideFee;
						fee.TaxRate = item.TaxRate;
						driverFees.Add(fee);

						totalFare += item.Amount;
					}
					airportReservation.InboundFare.DriverFees = driverFees;
				}
				#endregion

				airportReservation.InboundFare.FareID = inboundRate.ServiceInfoRates.First().FareID;

				#region FuelSurcharge
				airportReservation.InboundFare.FuelSurcharge = inboundRate.ServiceInfoRates.First().FuelSurcharge;
				totalFare += inboundRate.ServiceInfoRates.First().FuelSurcharge;
				#endregion

				#region Gratuity

				airportReservation.InboundFare.Gratuity = inboundRate.ServiceInfoRates.First().Gratuity;
				//try
				//{
				//	if (request.GratuityType == GratuityType.Amount)
				//	{
				//		airportReservation.InboundFare.Gratuity = request.Gratuity;
				//	}
				//	else if (request.GratuityType == GratuityType.Percentage)
				//	{
				//		airportReservation.InboundFare.Gratuity = totalFare * (request.Gratuity / 100);
				//	}
				//}
				//catch
				//{ }
				#endregion
				airportReservation.InboundFare.ServiceTypeCode = inboundRate.ServiceInfoRates.First().ServiceType;
				//airportReservation.InboundFare.TotalSegmentCost = totalFare;
				//airportReservation.InboundFare.TotalSegmentCost = totalFare;
				//2014_06_27: Doug asked me to use their total
				//airportReservation.InboundFare.TotalSegmentCost = inboundRate.ServiceInfoRates.First().Total;
				//2014_07_15 Doug adked me to change the total as below: (set FareTotal and remove TotalSegmentCost)
				//airportReservation.InboundFare.TotalSegmentCost = inboundRate.ServiceInfoRates.First().Total;
				airportReservation.InboundFare.FareTotal = inboundRate.ServiceInfoRates.First().FareTotal;
				airportReservation.InboundFare.UserOverrideFare = false;
                airportReservation.InboundFare.AffiliateProviderId = inboundSegment.AffiliateProviderId;
                #endregion

                #region InboundItinerary                
                airportReservation.InboundItinerary = new ReservationsServiceSecure.Itinerary();
				airportReservation.InboundItinerary.ASAPRequestID = inboundPickupTime.AsapRequestId;
				airportReservation.InboundItinerary.FlightOriginationAirportCode = inboundSegment.Locations.Last().AiportCode;
				airportReservation.InboundItinerary.FlightDestinationAirport = inboundSegment.Locations.Last().SecondryAiportCode;
				airportReservation.InboundItinerary.FlightTime = inboundSegment.FlightDateTime;
				airportReservation.InboundItinerary.IsDomesticFlight = inboundSegment.IsInternationalFlight ? false : true;
				airportReservation.InboundItinerary.IsGuaranteed = inboundPickupTime.IsGuaranteed;
				airportReservation.InboundItinerary.PickupTime = inboundPickupTime.AvailablePickupTime;
				airportReservation.InboundItinerary.PickupEndTime = inboundPickupTime.AvailablePickupEndTime;
				airportReservation.InboundItinerary.UsesScheduledService = inboundPickupTime.IsScheduledStop;
				airportReservation.InboundItinerary.AirlineCode = inboundSegment.AirlineCode;
				airportReservation.InboundItinerary.FlightNumber = inboundSegment.FlightNumber;
				airportReservation.InboundItinerary.IsCancelled = false;
				airportReservation.InboundItinerary.RequestAttributes = string.Empty;
				airportReservation.InboundItinerary.RequestVehicleNumber = !string.IsNullOrWhiteSpace(request.RequestVehicleNumber) ? request.RequestVehicleNumber.ToInt32() : 0;
                airportReservation.InboundItinerary.IsRideNow = request.IsInboundRideNow.ToBool();
                #endregion

                #region Inbound Payment
                airportReservation.InboundPaymentDetails = new ReservationsServiceSecure.Payment();
				airportReservation.InboundPaymentDetails = request.Payment;
                #endregion

                #region InboundSegmentConfirmation for updating
                if (!string.IsNullOrEmpty(inboundSegment.ConfirmationId))
                {
                    airportReservation.InboundSegmentConfirmation = new ReservationsServiceSecure.ReservationConfirmation();
                    airportReservation.InboundSegmentConfirmation.ConfirmationNumber = inboundSegment.ConfirmationId;
                }
                else
                {
                    airportReservation.InboundSegmentConfirmation = null;
                }
                #endregion
            }

            if (outboundSegment != null)//Outbound - From the airport
			{
				#region OutboundFare
				decimal totalFare = outboundRate.ServiceInfoRates.First().FirstPassengerFare + outboundRate.ServiceInfoRates.First().AdditionalPassengerFare * (outboundSegment.PayingPax - 1);

				airportReservation.OutboundFare = new ReservationsServiceSecure.Fare();
				airportReservation.OutboundFare.B2BCommission = outboundRate.ServiceInfoRates.First().Commission;
                airportReservation.OutboundFare.CommissionID = outboundRate.ServiceInfoRates.First().CommissionID;

                #region CompanyFees
                airportReservation.OutboundFare.CompanyFees = null;
				if (outboundRate.ServiceInfoRates.First().CompanyFees != null && outboundRate.ServiceInfoRates.First().CompanyFees.Any())
				{
					var companyFees = new List<ReservationsServiceSecure.AirportReservationFareFeeRecord>();
					foreach (var item in outboundRate.ServiceInfoRates.First().CompanyFees)
					{
						var fee = new ReservationsServiceSecure.AirportReservationFareFeeRecord();
						fee.FeeId = 0;// Doug asked me to hardcode it to 0 // item.FeeTypeID;
						fee.FeeTypeID = item.FeeTypeID;
						fee.Amount = item.Amount;
						//fee.IsDirty = item.IsDirty;
						//fee.IsATax = item.IsATax;
						//fee.IsTaxable = item.IsTaxable;
						//fee.OverrideFee = item.OverrideFee;
						fee.TaxRate = item.TaxRate;
						companyFees.Add(fee);
						totalFare += item.Amount;
					}
					airportReservation.OutboundFare.CompanyFees = companyFees;
				}
				#endregion

				airportReservation.OutboundFare.CurrencySymbol = outboundRate.ServiceInfoRates.First().CurrencySymbol;
				airportReservation.OutboundFare.CurrencyType = outboundRate.ServiceInfoRates.First().CurrencyType;
				airportReservation.OutboundFare.CurrencyID = outboundRate.ServiceInfoRates.First().CurrencyID;

				#region Discount
				airportReservation.OutboundFare.DiscountAmount = outboundRate.ServiceInfoRates.First().Discount;
				airportReservation.OutboundFare.DiscountID = outboundRate.ServiceInfoRates.First().DiscountID;
				#endregion

				#region DriverFees
				airportReservation.OutboundFare.DriverFees = null;
				if (outboundRate.ServiceInfoRates.First().DriverFees != null && outboundRate.ServiceInfoRates.First().DriverFees.Any())
				{
					var driverFees = new List<ReservationsServiceSecure.AirportReservationFareFeeRecord>();
					foreach (var item in outboundRate.ServiceInfoRates.First().DriverFees)
					{
						var fee = new ReservationsServiceSecure.AirportReservationFareFeeRecord();
						fee.FeeId = 0;// Doug asked me to hardcode it to 0 //  item.FeeTypeID;
						fee.FeeTypeID = item.FeeTypeID;
						fee.Amount = item.Amount;
						//fee.IsDirty = item.IsDirty;
						//fee.IsATax = item.IsATax;
						//fee.IsTaxable = item.IsTaxable;
						//fee.OverrideFee = item.OverrideFee;
						fee.TaxRate = item.TaxRate;
						driverFees.Add(fee);

						totalFare += item.Amount;
					}
					airportReservation.OutboundFare.DriverFees = driverFees;
				}
				#endregion

				airportReservation.OutboundFare.FareID = outboundRate.ServiceInfoRates.First().FareID;

				#region FuelSurcharge
				airportReservation.OutboundFare.FuelSurcharge = outboundRate.ServiceInfoRates.First().FuelSurcharge;
				totalFare += outboundRate.ServiceInfoRates.First().FuelSurcharge;
				#endregion

				#region Gratuity
				airportReservation.OutboundFare.Gratuity = outboundRate.ServiceInfoRates.First().Gratuity;
				//try
				//{
				//	if (request.GratuityType == GratuityType.Amount)
				//	{
				//		airportReservation.OutboundFare.Gratuity = request.Gratuity;
				//	}
				//	else if (request.GratuityType == GratuityType.Percentage)
				//	{
				//		airportReservation.OutboundFare.Gratuity = totalFare * (request.Gratuity / 100);
				//	}
				//}
				//catch
				//{ }
				#endregion
				airportReservation.OutboundFare.ServiceTypeCode = outboundRate.ServiceInfoRates.First().ServiceType;
				//airportReservation.OutboundFare.TotalSegmentCost = totalFare;
				//airportReservation.OutboundFare.FareTotal = totalFare;
				//2014_06_27: Doug asked me to use their total
				//airportReservation.OutboundFare.TotalSegmentCost = outboundRate.ServiceInfoRates.First().Total;
				//airportReservation.OutboundFare.FareTotal = outboundRate.ServiceInfoRates.First().Total;
				//2014_07_15 Doug adked me to change the total as below: (set FareTotal and remove TotalSegmentCost)
				//airportReservation.InboundFare.TotalSegmentCost = inboundRate.ServiceInfoRates.First().Total;
				//airportReservation.OutboundFare.TotalSegmentCost = outboundRate.ServiceInfoRates.First().Total;
				airportReservation.OutboundFare.FareTotal = outboundRate.ServiceInfoRates.First().FareTotal;
				airportReservation.OutboundFare.UserOverrideFare = false;
                airportReservation.OutboundFare.AffiliateProviderId = outboundSegment.AffiliateProviderId;
                #endregion

                #region OutboundItinerary
                airportReservation.OutboundItinerary = new ReservationsServiceSecure.Itinerary();
				airportReservation.OutboundItinerary.ASAPRequestID = outboundPickupTime.AsapRequestId;
                airportReservation.OutboundItinerary.FlightDestinationAirport = outboundSegment.Locations.First().AiportCode;
                airportReservation.OutboundItinerary.FlightOriginationAirportCode = outboundSegment.Locations.First().SecondryAiportCode;
				airportReservation.OutboundItinerary.FlightTime = outboundSegment.FlightDateTime;
				airportReservation.OutboundItinerary.IsDomesticFlight = outboundSegment.IsInternationalFlight ? false : true;
				airportReservation.OutboundItinerary.IsGuaranteed = outboundPickupTime.IsGuaranteed;
				airportReservation.OutboundItinerary.PickupTime = outboundPickupTime.AvailablePickupTime;
				airportReservation.OutboundItinerary.PickupEndTime = outboundPickupTime.AvailablePickupEndTime;
				airportReservation.OutboundItinerary.UsesScheduledService = outboundPickupTime.IsScheduledStop;
				airportReservation.OutboundItinerary.FlightNumber = outboundSegment.FlightNumber;
				airportReservation.OutboundItinerary.AirlineCode = outboundSegment.AirlineCode;
				airportReservation.OutboundItinerary.IsCancelled = false;
				airportReservation.OutboundItinerary.RequestAttributes = string.Empty;
				airportReservation.OutboundItinerary.RequestVehicleNumber = !string.IsNullOrWhiteSpace(request.RequestVehicleNumber) ? request.RequestVehicleNumber.ToInt32() : 0;
                airportReservation.OutboundItinerary.IsRideNow = request.IsOutboundRideNow.ToBool();

                #endregion

                #region Outound Payment
                airportReservation.OutboundPaymentDetails = new ReservationsServiceSecure.Payment();
				airportReservation.OutboundPaymentDetails = request.Payment;
                #endregion

                #region OutboundSegmentConfirmation for updating
                if (!string.IsNullOrEmpty(outboundSegment.ConfirmationId))
                {
                    airportReservation.OutboundSegmentConfirmation = new ReservationsServiceSecure.ReservationConfirmation();
                    airportReservation.OutboundSegmentConfirmation.ConfirmationNumber = outboundSegment.ConfirmationId;
                }
                else
                {
                    airportReservation.OutboundSegmentConfirmation = null;
                }
                #endregion

            }

            #endregion

            return airportReservation;
		}

		public ReservationsServiceSecure.CharterReservation GetCharterReservationObjet(Segment segment, ReservationRequest request, UDI.SDS.RatesService.CharterServiceTypeRecord charterRate, PickupTimeResponse pickupTimeResponse)
		{
			ReservationsServiceSecure.CharterReservation charterReservation = new ReservationsServiceSecure.CharterReservation();
			//If it would be ASAP and approved.
			charterReservation.ASAPRequestID = pickupTimeResponse.AsapRequestId;
			charterReservation.AccessibleServiceRequired = segment.Wheelchairs > 0 ? true : false;
            //always chould be "CHT" (Charter)
            charterReservation.CultureCode = request.CultureCode;
            charterReservation.AirportCode = Defaults.CharterAirport;// "CHT";
			//TODOZtrip: 
			//charterReservation.AsDirected = true | false;
			charterReservation.Comments = request.Comments;
			charterReservation.ContactNumber = request.ContactNumber;
			charterReservation.ContactNumberDialingPrefix = request.ContactNumberDialingPrefix;
			charterReservation.EmailAddress = request.EmailAddress;
			charterReservation.FirstName = request.FirstName;
			charterReservation.FreePax = 0;//Doug : FreePax will always be zero. That field is obsolete.
            charterReservation.GroupID = request.GroupID;
			charterReservation.LastName = request.LastName;
			charterReservation.PayingPax = (byte)segment.PayingPax;
			charterReservation.ReserveCSR = request.ReserveCsr; //User Name (Expeida, ...)
			charterReservation.Direction = 2; //2 means chareter
			charterReservation.MemberId = request.MemberID; //end user (customer) membershipID
			charterReservation.PhoneNumber = request.ContactNumber;
			charterReservation.PhoneNumberDialingPrefix = request.ContactNumberDialingPrefix;
			charterReservation.PickMeUpNow = request.pickMeUpNow.HasValue ? request.pickMeUpNow.Value : false;
			charterReservation.PickupDateTime = pickupTimeResponse.AvailablePickupTime;
			charterReservation.Request = string.Empty; //For call center. empty is fine.
			charterReservation.Attributes = string.Empty; //For call center. empty is fine.
			charterReservation.CallCenterClientID = 0; //For call center. 0 is fine.
			charterReservation.ChangeCSR = string.Empty; //It's new reservation, no need to have it
			charterReservation.CharterMinutes = segment.CharterMinutes;//It's for hourly, we do not need it for Location to Location
			charterReservation.ClientNotes = string.Empty;//For call center. empty is fine.
			charterReservation.FranchiseCode = charterRate.FranchiseCode;//Get it from GetRate
			charterReservation.FranchiseLocale = charterRate.FranchiseLocale;// GetFranchiseLocale(rateResponse.CharterRate.FranchiseCode);
			//charterReservation.Sid = request.UserName; //User Name (Expeida, ...) 
			charterReservation.Sid = request.SID; //User Name (Expeida, ...) 
			charterReservation.HourlyCharterFlag = false;
			charterReservation.SubscribeToPromotions = request.SubscribeToPromotions.HasValue ? request.SubscribeToPromotions.Value : false;
			charterReservation.AllowSms = request.AllowSMS;

			#region Address
			foreach (var location in segment.Locations.Where(s => s != null))
			{
				/** Dropoff Address **/
				if (location.mode == LocationMode.Dropoff)
				{
					if (location.type == LocationType.AsDirected)
					{
						charterReservation.AsDirected = true;
						charterReservation.DropoffAddress = null;
					}
					else
					{
						charterReservation.AsDirected = false;

						////RateService rates = new RateService(token);
						////rates.TrackTime = TrackTime;
						////var location_new = location;
						////if (location.Latitude == 0 || location.Longitude == 0)
						////	location_new = rates.GetGeoCodeForAddress(location);

						charterReservation.DropoffAddress = new ReservationsServiceSecure.AddressRecord();
						charterReservation.DropoffAddress.CountryCode = location.Country;
						charterReservation.DropoffAddress.CountrySubDivision = location.StateOrProvince;
						charterReservation.DropoffAddress.Latitude = location.Latitude;
						charterReservation.DropoffAddress.Longitude = location.Longitude;
						charterReservation.DropoffAddress.LocationName = location.LocationName;
						charterReservation.DropoffAddress.LocationType = location.LocationType;
						charterReservation.DropoffAddress.Municipality = location.City;
						charterReservation.DropoffAddress.PostalCode = location.PostalCode;
						charterReservation.DropoffAddress.StreetAddress = location.StreetNumber + " " + location.StreetName;
						charterReservation.DropoffAddress.StreetName = location.StreetName;
						charterReservation.DropoffAddress.StreetNumber = location.StreetNumber;
						charterReservation.DropoffAddress.SubZipName = location.SubZipName;
						charterReservation.DropoffAddress.UnitNumber = location.Unit;
						charterReservation.DropoffAddress.Comments = string.Empty;
						charterReservation.DropoffAddress.ID = 0; //0 is fine
					}
				}
				/** Pickup Address **/
				else
				{
					////RateService rates = new RateService(token);
					////rates.TrackTime = TrackTime;

					////var location_new = location;
					////if (location.Latitude == 0 || location.Longitude == 0)
					////	location_new = rates.GetGeoCodeForAddress(location);

					charterReservation.PickupAddress = new ReservationsServiceSecure.AddressRecord();
					charterReservation.PickupAddress.CountryCode = location.Country;
					charterReservation.PickupAddress.CountrySubDivision = location.StateOrProvince;
					charterReservation.PickupAddress.Latitude = location.Latitude;
					charterReservation.PickupAddress.Longitude = location.Longitude;
					charterReservation.PickupAddress.LocationName = location.LocationName;
					charterReservation.PickupAddress.LocationType = location.LocationType;
					charterReservation.PickupAddress.Municipality = location.City;
					charterReservation.PickupAddress.PostalCode = location.PostalCode;
					charterReservation.PickupAddress.StreetAddress = location.StreetNumber + " " + location.StreetName;
					charterReservation.PickupAddress.StreetName = location.StreetName;
					charterReservation.PickupAddress.StreetNumber = location.StreetNumber;
					charterReservation.PickupAddress.SubZipName = location.SubZipName;
					charterReservation.PickupAddress.UnitNumber = location.Unit;
				}
			}
			#endregion

			#region AsDirected
			if (segment.Locations.Where(s => s != null).Count() == 1)
			{
				charterReservation.AsDirected = true;
			}
			#endregion

			#region FareDetails
			charterReservation.FareDetails = new ReservationsServiceSecure.CharterFare();
			charterReservation.FareDetails.B2BCommission = 0;
			charterReservation.FareDetails.CommissionID = 0;
			charterReservation.FareDetails.CompanyFees = null;
			charterReservation.FareDetails.CurrencySymbol = string.Empty;
			charterReservation.FareDetails.CurrencyType = string.Empty;
			charterReservation.FareDetails.CurrencyID = charterRate.CurrencyID;
			charterReservation.FareDetails.DiscountAmount = 0;
			charterReservation.FareDetails.DiscountID = 0;
			charterReservation.FareDetails.DriverFees = null;
			charterReservation.FareDetails.FareID = charterRate.FareID;
			//charterReservation.FareDetails.FareTotal = charterReservation.HourlyCharterFlag == true ? (decimal)(segment.CharterMinutes * 1.0 / 60) * rateResponse.CharterRate.HourlyRate : rateResponse.CharterRate.Fare;
			charterReservation.FareDetails.FareTotal = charterRate.Fare;// charterReservation.HourlyCharterFlag == true ? (decimal)(segment.CharterMinutes * 1.0 / 60) * rateResponse.CharterRate.HourlyRate : rateResponse.CharterRate.Fare;
			charterReservation.FareDetails.FuelSurcharge = 0;
			#region Gratuity
			if (request.GratuityType == GratuityType.Percentage)
			{
				charterReservation.FareDetails.gratuityRate = request.Gratuity / 100;// we need this  
			}
			else if (request.GratuityType == GratuityType.Amount)
			{
				charterReservation.FareDetails.Gratuity = request.Gratuity;// we need this  
			}

			#endregion
			charterReservation.FareDetails.ServiceTypeCode = charterRate.ServiceTypeCode;
			charterReservation.FareDetails.UserOverrideFare = false; //always be false
			charterReservation.FareDetails.Fleet = charterRate.Fleet;
			#endregion

			#region Payment
			charterReservation.PaymentDetails = new ReservationsServiceSecure.Payment();
			charterReservation.PaymentDetails = request.Payment; // should be the same as Airport book. DirectBill or Credit Card.
			#endregion

			return charterReservation;
		}

		public ReservationsServiceSecure.CharterReservation GetHourlyReservationObjet(Segment segment, ReservationRequest request, UDI.SDS.RatesService.CharterServiceTypeRecord charterRate, PickupTimeResponse pickupTimeResponse)
		{
			ReservationsServiceSecure.CharterReservation charterReservation = new ReservationsServiceSecure.CharterReservation();
			//If it would be ASAP and approved.
			charterReservation.ASAPRequestID = pickupTimeResponse.AsapRequestId;
			charterReservation.AccessibleServiceRequired = segment.Wheelchairs > 0 ? true : false;
			//always chould be "CHT" (Charter)
			charterReservation.AirportCode = Defaults.CharterAirport;// "CHT";
			//TODOZtrip: 
			//charterReservation.AsDirected = true | false;
			charterReservation.CharterMinutes = 60;
			charterReservation.Comments = request.Comments;
			charterReservation.ContactNumber = request.ContactNumber;
			charterReservation.ContactNumberDialingPrefix = request.ContactNumberDialingPrefix;
			charterReservation.EmailAddress = request.EmailAddress;
			charterReservation.FirstName = request.FirstName;
			charterReservation.FreePax = 0;//Doug : FreePax will always be zero. That field is obsolete.
            charterReservation.GroupID = request.GroupID;
			charterReservation.LastName = request.LastName;
			charterReservation.PayingPax = (byte)segment.PayingPax;
			charterReservation.ReserveCSR = request.ReserveCsr; //User Name (Expeida, ...)
			charterReservation.Direction = 2; //2 means chareter
			charterReservation.MemberId = request.MemberID; //end user (customer) membershipID
			charterReservation.PhoneNumber = request.ContactNumber;
			charterReservation.PhoneNumberDialingPrefix = request.ContactNumberDialingPrefix;
			charterReservation.PickMeUpNow = request.pickMeUpNow.HasValue ? request.pickMeUpNow.Value : false;
			charterReservation.PickupDateTime = pickupTimeResponse.AvailablePickupTime;
			charterReservation.Request = string.Empty; //For call center. empty is fine.
			charterReservation.Attributes = string.Empty; //For call center. empty is fine.
			charterReservation.CallCenterClientID = 0; //For call center. 0 is fine.
			charterReservation.ChangeCSR = string.Empty; //It's new reservation, no need to have it
			charterReservation.CharterMinutes = (segment.CharterMinutes < charterRate.MinimumDurationMinutes ? charterRate.MinimumDurationMinutes : segment.CharterMinutes);//It's for hourly, we do not need it for Location to Location
			charterReservation.ClientNotes = string.Empty;//For call center. empty is fine.
			charterReservation.FranchiseCode = charterRate.FranchiseCode;//Get it from GetRate
			charterReservation.FranchiseLocale = charterRate.FranchiseLocale;// GetFranchiseLocale(rateResponse.CharterRate.FranchiseCode);
			//charterReservation.Sid = request.UserName; //User Name (Expeida, ...) 
			charterReservation.Sid = request.SID; //User Name (Expeida, ...) 
			charterReservation.HourlyCharterFlag = true;
			charterReservation.SubscribeToPromotions = request.SubscribeToPromotions.HasValue ? request.SubscribeToPromotions.Value : false;
			charterReservation.AllowSms = request.AllowSMS;

			#region Address
			foreach (var location in segment.Locations.Where(s => s != null))
			{
				/** Dropoff Address **/
				if (location.mode == LocationMode.Dropoff)
				{
					if (location.type == LocationType.AsDirected)
					{
						charterReservation.AsDirected = true;
						charterReservation.DropoffAddress = null;
					}
					else
					{
						charterReservation.AsDirected = false;

						////RateService rates = new RateService(token);
						////rates.TrackTime = TrackTime;
						////var location_new = location;
						////if (location.Latitude == 0 || location.Longitude == 0)
						////	location_new = rates.GetGeoCodeForAddress(location);

						charterReservation.DropoffAddress = new ReservationsServiceSecure.AddressRecord();
						charterReservation.DropoffAddress.CountryCode = location.Country;
						charterReservation.DropoffAddress.CountrySubDivision = location.StateOrProvince;
						charterReservation.DropoffAddress.Latitude = location.Latitude;
						charterReservation.DropoffAddress.Longitude = location.Longitude;
						charterReservation.DropoffAddress.LocationName = location.LocationName;
						charterReservation.DropoffAddress.LocationType = location.LocationType;
						charterReservation.DropoffAddress.Municipality = location.City;
						charterReservation.DropoffAddress.PostalCode = location.PostalCode;
						charterReservation.DropoffAddress.StreetAddress = location.StreetNumber + " " + location.StreetName;
						charterReservation.DropoffAddress.StreetName = location.StreetName;
						charterReservation.DropoffAddress.StreetNumber = location.StreetNumber;
						charterReservation.DropoffAddress.SubZipName = location.SubZipName;
						charterReservation.DropoffAddress.UnitNumber = location.Unit;
						charterReservation.DropoffAddress.Comments = string.Empty;
						charterReservation.DropoffAddress.ID = 0; //0 is fine
					}
				}
				/** Pickup Address **/
				else
				{
					////RateService rates = new RateService(token);
					////rates.TrackTime = TrackTime;

					////var location_new = location;
					////if (location.Latitude == 0 || location.Longitude == 0)
					////	location_new = rates.GetGeoCodeForAddress(location);

					charterReservation.PickupAddress = new ReservationsServiceSecure.AddressRecord();
					charterReservation.PickupAddress.CountryCode = location.Country;
					charterReservation.PickupAddress.CountrySubDivision = location.StateOrProvince;
					charterReservation.PickupAddress.Latitude = location.Latitude;
					charterReservation.PickupAddress.Longitude = location.Longitude;
					charterReservation.PickupAddress.LocationName = location.LocationName;
					charterReservation.PickupAddress.LocationType = location.LocationType;
					charterReservation.PickupAddress.Municipality = location.City;
					charterReservation.PickupAddress.PostalCode = location.PostalCode;
					charterReservation.PickupAddress.StreetAddress = location.StreetNumber + " " + location.StreetName;
					charterReservation.PickupAddress.StreetName = location.StreetName;
					charterReservation.PickupAddress.StreetNumber = location.StreetNumber;
					charterReservation.PickupAddress.SubZipName = location.SubZipName;
					charterReservation.PickupAddress.UnitNumber = location.Unit;
				}
			}
			#endregion

			#region AsDirected
			if (segment.Locations.Where(s => s != null).Count() == 1)
			{
				charterReservation.AsDirected = true;
			}
			#endregion

			#region FareDetails
			charterReservation.FareDetails = new ReservationsServiceSecure.CharterFare();
			charterReservation.FareDetails.B2BCommission = 0;
			charterReservation.FareDetails.CommissionID = 0;
			charterReservation.FareDetails.CompanyFees = null;
			charterReservation.FareDetails.CurrencySymbol = string.Empty;
			charterReservation.FareDetails.CurrencyType = string.Empty;
			charterReservation.FareDetails.CurrencyID = charterRate.CurrencyID;
			charterReservation.FareDetails.DiscountAmount = 0;
			charterReservation.FareDetails.DiscountID = 0;
			charterReservation.FareDetails.DriverFees = null;
			charterReservation.FareDetails.FareID = charterRate.FareID;
			//charterReservation.FareDetails.FareTotal = charterReservation.HourlyCharterFlag == true ? (decimal)(segment.CharterMinutes * 1.0 / 60) * rateResponse.CharterRate.HourlyRate : rateResponse.CharterRate.Fare;
			charterReservation.FareDetails.FareTotal = charterReservation.CharterMinutes * charterRate.HourlyRate / 60;// charterReservation.HourlyCharterFlag == true ? (decimal)(segment.CharterMinutes * 1.0 / 60) * rateResponse.CharterRate.HourlyRate : rateResponse.CharterRate.Fare;
			charterReservation.FareDetails.FuelSurcharge = 0;
			#region Gratuity
			if (request.GratuityType == GratuityType.Percentage)
			{
				charterReservation.FareDetails.gratuityRate = request.Gratuity / 100;// we need this  
			}
			else if (request.GratuityType == GratuityType.Amount)
			{
				charterReservation.FareDetails.Gratuity = request.Gratuity;// we need this  
			}

			#endregion
			charterReservation.FareDetails.ServiceTypeCode = charterRate.ServiceTypeCode;
			charterReservation.FareDetails.UserOverrideFare = false; //always be false
			charterReservation.FareDetails.Fleet = charterRate.Fleet;
			#endregion

			#region Payment
			charterReservation.PaymentDetails = new ReservationsServiceSecure.Payment();
			charterReservation.PaymentDetails = request.Payment; // should be the same as Airport book. DirectBill or Credit Card.
			#endregion

			return charterReservation;
		}

		#region Old
		#region Do not delete. In future, some parts may be used.
		/// <summary>
		/// Inbound, Outbound, Fare, ...
		/// Set the request of Airport Reservation, used by SaveAirportReservation method
		/// </summary>
		//public ReservationsServiceSecure.AirportReservation GetAirportReservationObject(Segment firstSegment, Segment secondSegment, ReservationRequest request, RateResponse rateResponseFirstSegment, UDI.SDS.RatesService.FareScheduleWithFees firstSegmentFees, PickupTimeResponse pickupTimeResponseFirstSegment, RateResponse rateResponseSecondSegment, UDI.SDS.RatesService.FareScheduleWithFees secondSegmentFees, PickupTimeResponse pickupTimeResponseSecondSegment)
		//{

		//	ReservationsServiceSecure.AirportReservation airportReservation = new ReservationsServiceSecure.AirportReservation();
		//	var airportCode = string.Empty;
		//	airportReservation.AccessibleServiceRequired = firstSegment.Wheelchairs > 0 ? true : false;
		//	airportReservation.AirlineMilesDetails = null;
		//	airportReservation.ChangeCSR = string.Empty;
		//	airportReservation.ChildSeats = (byte)firstSegment.ChildSeats;
		//	airportReservation.Comments = request.Comments;
		//	airportReservation.ContactNumber = request.ContactNumber;
		//	airportReservation.ContactNumberDialingPrefix = request.ContactNumberDialingPrefix;
		//	airportReservation.EmailAddress = request.EmailAddress;
		//	airportReservation.FirstName = request.FirstName;
		//	airportReservation.FreePax = (byte)(firstSegment.ChildSeats + firstSegment.InfantSeats);
		//	airportReservation.GroupID = request.GroupID;
		//	airportReservation.InfantSeats = (byte)firstSegment.InfantSeats;
		//	airportReservation.LastName = request.LastName;
		//	airportReservation.Luggage = (byte)request.LuggageCount;
		//	airportReservation.PayingPax = (byte)firstSegment.PayingPax;
		//	airportReservation.ReserveCSR = request.ReserveCSR;
		//	airportReservation.ServiceNotes = request.ServiceNotes;
		//	airportReservation.SID = request.SID;
		//	airportReservation.SpecialHandling = request.SpecialHandling;
		//	airportReservation.TripDirection = secondSegment == null ? (int)rateResponseFirstSegment.TripDirection : (int)TripDirection.RoundTrip;


		//	#region GuestAddress
		//	foreach (var location in firstSegment.Locations)
		//	{
		//		if (
		//			(rateResponseFirstSegment.TripDirection == TripDirection.Inbound && location.mode == LocationMode.Pickup)//Inbound
		//			||
		//			(rateResponseFirstSegment.TripDirection == TripDirection.Outbound && location.mode == LocationMode.Dropoff)//Outbound
		//			||
		//			(rateResponseFirstSegment.TripDirection == TripDirection.RoundTrip && location.type == LocationType.Address)//RoundTrip
		//			)
		//		{
		//			////RateService rates = new RateService(token);
		//			////rates.TrackTime = TrackTime;

		//			////var location_new = location;
		//			////if (location.Latitude == 0 || location.Longitude == 0)
		//			////	location_new = rates.GetGeoCodeForAddress(location);

		//			airportReservation.GuestAddress = new ReservationsServiceSecure.AddressRecord();
		//			airportReservation.GuestAddress.CountryCode = location.Country;
		//			airportReservation.GuestAddress.CountrySubDivision = location.StateOrProvince;
		//			airportReservation.GuestAddress.Latitude = location.Latitude;
		//			airportReservation.GuestAddress.Longitude = location.Longitude;
		//			airportReservation.GuestAddress.LocationName = location.PropertyName;
		//			airportReservation.GuestAddress.LocationType = location.LocationType;
		//			airportReservation.GuestAddress.Municipality = location.City;
		//			airportReservation.GuestAddress.PostalCode = location.PostalCode;
		//			airportReservation.GuestAddress.StreetAddress = location.StreetNumber + " " + location.StreetName;
		//			airportReservation.GuestAddress.StreetName = location.StreetName;
		//			airportReservation.GuestAddress.StreetNumber = location.StreetNumber;
		//			airportReservation.GuestAddress.SubZipName = location.SubZipName;
		//			airportReservation.GuestAddress.UnitNumber = location.Unit;
		//			airportReservation.GuestAddress.Comments = string.Empty;
		//			airportReservation.GuestAddress.ID = 0;
		//			airportReservation.GuestAddress.PhoneNumber = string.Empty;
		//			airportReservation.GuestAddress.PhoneNumberDialingPrefix = string.Empty;
		//		}
		//		else
		//			airportCode = location.AiportCode;

		//	}
		//	#endregion

		//	#region Inbound, Outbound Information

		//	var inboundSegment = new Segment();
		//	var outboundSegment = new Segment();

		//	var inboundRate = new RateResponse();
		//	var outboundRate = new RateResponse();

		//	var inboundFees = new UDI.SDS.RatesService.FareScheduleWithFees();
		//	var outboundFees = new UDI.SDS.RatesService.FareScheduleWithFees();

		//	var inboundPickupTime = new PickupTimeResponse();
		//	var outboundPickupTime = new PickupTimeResponse();

		//	if (rateResponseFirstSegment.TripDirection == TripDirection.Inbound)
		//	{
		//		inboundSegment = firstSegment;
		//		outboundSegment = secondSegment;

		//		inboundRate = rateResponseFirstSegment;
		//		outboundRate = rateResponseSecondSegment;

		//		inboundFees = firstSegmentFees;
		//		outboundFees = secondSegmentFees;

		//		inboundPickupTime = pickupTimeResponseFirstSegment;
		//		outboundPickupTime = pickupTimeResponseSecondSegment;
		//	}

		//	if (rateResponseFirstSegment.TripDirection == TripDirection.Outbound)
		//	{
		//		inboundSegment = secondSegment;
		//		outboundSegment = firstSegment;

		//		inboundRate = rateResponseSecondSegment;
		//		outboundRate = rateResponseFirstSegment;

		//		inboundFees = secondSegmentFees;
		//		outboundFees = firstSegmentFees;

		//		inboundPickupTime = pickupTimeResponseSecondSegment;
		//		outboundPickupTime = pickupTimeResponseFirstSegment;
		//	}

		//	if (inboundSegment != null)//Inbound - To the airport
		//	{
		//		#region InboundFare
		//		decimal totalFare = inboundRate.AirportReservationRate.FirstPersonFare + inboundRate.AirportReservationRate.AdditionalPersonFare * (inboundSegment.PayingPax - 1);

		//		airportReservation.InboundFare = new ReservationsServiceSecure.Fare();
		//		airportReservation.InboundFare.B2BCommission = 0;
		//		airportReservation.InboundFare.CommissionID = 0;

		//		#region CompanyFees
		//		airportReservation.InboundFare.CompanyFees = null;
		//		if (inboundFees.InboundCompanyFeesList != null && inboundFees.InboundCompanyFeesList.Any())
		//		{
		//			var companyFees = new List<ReservationsServiceSecure.AirportReservationFareFeeRecord>();
		//			foreach (var item in inboundFees.InboundCompanyFeesList)
		//			{
		//				var fee = new ReservationsServiceSecure.AirportReservationFareFeeRecord();
		//				fee.FeeId = item.FeeTypeID;
		//				fee.FeeTypeID = item.FeeTypeID;
		//				fee.Amount = item.Amount;
		//				//fee.IsDirty = item.IsDirty;
		//				//fee.IsATax = item.IsATax;
		//				//fee.IsTaxable = item.IsTaxable;
		//				//fee.OverrideFee = item.OverrideFee;
		//				fee.TaxRate = item.TaxRate;
		//				companyFees.Add(fee);
		//				totalFare += item.Amount;
		//			}
		//			airportReservation.InboundFare.CompanyFees = companyFees;
		//		}
		//		#endregion

		//		airportReservation.InboundFare.CurrencySymbol = inboundRate.AirportReservationRate.CurrencySymbol;
		//		airportReservation.InboundFare.CurrencyType = inboundRate.AirportReservationRate.CurrencyType;
		//		airportReservation.InboundFare.CurrencyID = inboundRate.AirportReservationRate.CurrencyID;

		//		#region Discount
		//		airportReservation.InboundFare.DiscountAmount = inboundFees.InboundDiscount;
		//		airportReservation.InboundFare.DiscountID = inboundFees.DiscountID;
		//		#endregion

		//		#region DriverFees
		//		airportReservation.InboundFare.DriverFees = null;
		//		if (inboundFees.InboundDriverFeesList != null && inboundFees.InboundDriverFeesList.Any())
		//		{
		//			var driverFees = new List<ReservationsServiceSecure.AirportReservationFareFeeRecord>();
		//			foreach (var item in inboundFees.InboundDriverFeesList)
		//			{
		//				var fee = new ReservationsServiceSecure.AirportReservationFareFeeRecord();
		//				fee.FeeId = item.FeeTypeID;
		//				fee.FeeTypeID = item.FeeTypeID;
		//				fee.Amount = item.Amount;
		//				//fee.IsDirty = item.IsDirty;
		//				//fee.IsATax = item.IsATax;
		//				//fee.IsTaxable = item.IsTaxable;
		//				//fee.OverrideFee = item.OverrideFee;
		//				fee.TaxRate = item.TaxRate;
		//				driverFees.Add(fee);

		//				totalFare += item.Amount;
		//			}
		//			airportReservation.InboundFare.DriverFees = driverFees;
		//		}
		//		#endregion

		//		airportReservation.InboundFare.FareID = inboundRate.AirportReservationRate.FareID;

		//		#region FuelSurcharge
		//		airportReservation.InboundFare.FuelSurcharge = inboundRate.AirportReservationRate.FuelSurcharge;
		//		totalFare += inboundRate.AirportReservationRate.FuelSurcharge;
		//		#endregion

		//		airportReservation.InboundFare.Gratuity = request.Gratuity;
		//		airportReservation.InboundFare.ServiceTypeCode = inboundRate.AirportReservationRate.ServiceTypeCode;
		//		airportReservation.InboundFare.TotalSegmentCost = totalFare;
		//		airportReservation.InboundFare.FareTotal = totalFare;
		//		airportReservation.InboundFare.UserOverrideFare = false;
		//		#endregion

		//		#region InboundItinerary
		//		airportReservation.InboundItinerary = new ReservationsServiceSecure.Itinerary();
		//		airportReservation.InboundItinerary.ASAPRequestID = inboundPickupTime.AsapRequestId;
		//		airportReservation.InboundItinerary.FlightOriginationAirportCode = airportCode;
		//		airportReservation.InboundItinerary.FlightTime = inboundSegment.FlightDateTime;
		//		airportReservation.InboundItinerary.IsDomesticFlight = inboundSegment.IsInternationalFlight ? false : true;
		//		airportReservation.InboundItinerary.IsGuaranteed = inboundPickupTime.IsGuaranteed;
		//		airportReservation.InboundItinerary.PickupTime = inboundPickupTime.AvailablePickupTime;
		//		airportReservation.InboundItinerary.PickupEndTime = inboundPickupTime.AvailablePickupEndTime;
		//		airportReservation.InboundItinerary.UsesScheduledService = inboundPickupTime.IsScheduledStop;
		//		airportReservation.InboundItinerary.AirlineCode = inboundSegment.AirlineCode;
		//		airportReservation.InboundItinerary.FlightNumber = inboundSegment.FlightNumber;
		//		airportReservation.InboundItinerary.IsCancelled = false;
		//		airportReservation.InboundItinerary.RequestAttributes = string.Empty;
		//		airportReservation.InboundItinerary.RequestVehicleNumber = 0;
		//		#endregion

		//		#region Inbound Payment
		//		airportReservation.InboundPaymentDetails = new ReservationsServiceSecure.Payment();
		//		airportReservation.InboundPaymentDetails = request.Payment;
		//		#endregion

		//		airportReservation.InboundSegmentConfirmation = null;
		//	}

		//	if (outboundSegment != null)//Outbound - From the airport
		//	{
		//		#region OutboundFare
		//		decimal totalFare = outboundRate.AirportReservationRate.FirstPersonFare + outboundRate.AirportReservationRate.AdditionalPersonFare * (outboundSegment.PayingPax - 1);

		//		airportReservation.OutboundFare = new ReservationsServiceSecure.Fare();
		//		airportReservation.OutboundFare.B2BCommission = 0;
		//		airportReservation.OutboundFare.CommissionID = 0;

		//		#region CompanyFees
		//		airportReservation.OutboundFare.CompanyFees = null;
		//		if (outboundFees.OutboundCompanyFeesList != null && outboundFees.OutboundCompanyFeesList.Any())
		//		{
		//			var companyFees = new List<ReservationsServiceSecure.AirportReservationFareFeeRecord>();
		//			foreach (var item in outboundFees.OutboundCompanyFeesList)
		//			{
		//				var fee = new ReservationsServiceSecure.AirportReservationFareFeeRecord();
		//				fee.FeeId = item.FeeTypeID;
		//				fee.FeeTypeID = item.FeeTypeID;
		//				fee.Amount = item.Amount;
		//				//fee.IsDirty = item.IsDirty;
		//				//fee.IsATax = item.IsATax;
		//				//fee.IsTaxable = item.IsTaxable;
		//				//fee.OverrideFee = item.OverrideFee;
		//				fee.TaxRate = item.TaxRate;
		//				companyFees.Add(fee);
		//				totalFare += item.Amount;
		//			}
		//			airportReservation.OutboundFare.CompanyFees = companyFees;
		//		}
		//		#endregion

		//		airportReservation.OutboundFare.CurrencySymbol = outboundRate.AirportReservationRate.CurrencySymbol;
		//		airportReservation.OutboundFare.CurrencyType = outboundRate.AirportReservationRate.CurrencyType;
		//		airportReservation.OutboundFare.CurrencyID = outboundRate.AirportReservationRate.CurrencyID;

		//		#region Discount
		//		airportReservation.OutboundFare.DiscountAmount = outboundFees.OutboundDiscount;
		//		airportReservation.OutboundFare.DiscountID = outboundFees.DiscountID;
		//		#endregion

		//		#region DriverFees
		//		airportReservation.OutboundFare.DriverFees = null;
		//		if (outboundFees.OutboundDriverFeesList != null && outboundFees.OutboundDriverFeesList.Any())
		//		{
		//			var driverFees = new List<ReservationsServiceSecure.AirportReservationFareFeeRecord>();
		//			foreach (var item in outboundFees.OutboundDriverFeesList)
		//			{
		//				var fee = new ReservationsServiceSecure.AirportReservationFareFeeRecord();
		//				fee.FeeId = item.FeeTypeID;
		//				fee.FeeTypeID = item.FeeTypeID;
		//				fee.Amount = item.Amount;
		//				//fee.IsDirty = item.IsDirty;
		//				//fee.IsATax = item.IsATax;
		//				//fee.IsTaxable = item.IsTaxable;
		//				//fee.OverrideFee = item.OverrideFee;
		//				fee.TaxRate = item.TaxRate;
		//				driverFees.Add(fee);

		//				totalFare += item.Amount;
		//			}
		//			airportReservation.OutboundFare.DriverFees = driverFees;
		//		}
		//		#endregion

		//		airportReservation.OutboundFare.FareID = outboundRate.AirportReservationRate.FareID;

		//		#region FuelSurcharge
		//		airportReservation.OutboundFare.FuelSurcharge = outboundRate.AirportReservationRate.FuelSurcharge;
		//		totalFare += outboundRate.AirportReservationRate.FuelSurcharge;
		//		#endregion

		//		airportReservation.OutboundFare.Gratuity = request.Gratuity;
		//		airportReservation.OutboundFare.ServiceTypeCode = outboundRate.AirportReservationRate.ServiceTypeCode;
		//		airportReservation.OutboundFare.TotalSegmentCost = totalFare;
		//		airportReservation.OutboundFare.FareTotal = totalFare;
		//		airportReservation.OutboundFare.UserOverrideFare = false;
		//		#endregion

		//		#region OutboundItinerary
		//		airportReservation.OutboundItinerary = new ReservationsServiceSecure.Itinerary();
		//		airportReservation.OutboundItinerary.ASAPRequestID = outboundPickupTime.AsapRequestId;
		//		airportReservation.OutboundItinerary.FlightDestinationAirport = airportCode;
		//		airportReservation.OutboundItinerary.FlightTime = outboundSegment.FlightDateTime;
		//		airportReservation.OutboundItinerary.IsDomesticFlight = outboundSegment.IsInternationalFlight ? false : true;
		//		airportReservation.OutboundItinerary.IsGuaranteed = outboundPickupTime.IsGuaranteed;
		//		airportReservation.OutboundItinerary.PickupTime = outboundPickupTime.AvailablePickupTime;
		//		airportReservation.OutboundItinerary.PickupEndTime = outboundPickupTime.AvailablePickupEndTime;
		//		airportReservation.OutboundItinerary.UsesScheduledService = outboundPickupTime.IsScheduledStop;
		//		airportReservation.OutboundItinerary.FlightNumber = outboundSegment.FlightNumber;
		//		airportReservation.OutboundItinerary.AirlineCode = outboundSegment.AirlineCode;
		//		airportReservation.OutboundItinerary.IsCancelled = false;
		//		airportReservation.OutboundItinerary.RequestAttributes = string.Empty;
		//		airportReservation.OutboundItinerary.RequestVehicleNumber = 0;
		//		#endregion

		//		#region Outound Payment
		//		airportReservation.OutboundPaymentDetails = new ReservationsServiceSecure.Payment();
		//		airportReservation.OutboundPaymentDetails = request.Payment;
		//		#endregion

		//		airportReservation.OutboundSegmentConfirmation = null;
		//	}

		//	#endregion

		//	return airportReservation;
		//} 
		#endregion


		/// <summary>
		/// Set the request of Charter Reservation, used by SaveCharterReservation method
		/// </summary>
		//public ReservationsServiceSecure.CharterReservation GetCharterReservationObjet(Segment segment, ReservationRequest request, RateResponse rateResponse, PickupTimeResponse pickupTimeResponse, string franchiseLocale)
		//{

		//	ReservationsServiceSecure.CharterReservation charterReservation = new ReservationsServiceSecure.CharterReservation();
		//	//If it would be ASAP and approved.
		//	charterReservation.ASAPRequestID = pickupTimeResponse.AsapRequestId;
		//	charterReservation.AccessibleServiceRequired = segment.Wheelchairs > 0 ? true : false;
		//	//always chould be "CHT" (Charter)
		//	charterReservation.AirportCode = "CHT";
		//	//TODOZtrip: 
		//	//charterReservation.AsDirected = true | false;
		//	charterReservation.Comments = request.Comments;
		//	charterReservation.ContactNumber = request.ContactNumber;
		//	charterReservation.ContactNumberDialingPrefix = request.ContactNumberDialingPrefix;
		//	charterReservation.EmailAddress = request.EmailAddress;
		//	charterReservation.FirstName = request.FirstName;
		//	charterReservation.FreePax = (byte)(segment.ChildSeats + segment.InfantSeats);
		//	charterReservation.GroupID = request.GroupID;
		//	charterReservation.LastName = request.LastName;
		//	charterReservation.PayingPax = (byte)segment.PayingPax;
		//	charterReservation.ReserveCSR = request.UserName; //User Name (Expeida, ...)
		//	charterReservation.Direction = 2; //2 means chareter
		//	//ToDoZtrip: it could be 0 for expedia as it does not have membershipID, but for zTrip it should be membershipID
		//	charterReservation.MemberId = 0; //end user (customer) membershipID
		//	charterReservation.PhoneNumber = request.ContactNumber;
		//	charterReservation.PhoneNumberDialingPrefix = request.ContactNumberDialingPrefix;
		//	charterReservation.PickMeUpNow = false;
		//	charterReservation.PickupDateTime = pickupTimeResponse.AvailablePickupTime;
		//	charterReservation.Request = string.Empty; //For call center. empty is fine.
		//	charterReservation.Attributes = string.Empty; //For call center. empty is fine.
		//	charterReservation.CallCenterClientID = 0; //For call center. 0 is fine.
		//	charterReservation.ChangeCSR = string.Empty; //It's new reservation, no need to have it
		//	charterReservation.CharterMinutes = segment.CharterMinutes;//It's for hourly, we do not need it for Location to Location
		//	charterReservation.ClientNotes = string.Empty;//For call center. empty is fine.
		//	charterReservation.FranchiseCode = rateResponse.CharterRate.FranchiseCode;//Get it from GetRate
		//	charterReservation.FranchiseLocale = franchiseLocale;// GetFranchiseLocale(rateResponse.CharterRate.FranchiseCode);
		//	charterReservation.Sid = request.SID; //User Name (Expeida, ...) 

		//	#region Address
		//	foreach (var location in segment.Locations)
		//	{
		//		/** Dropoff Address **/
		//		if (location.mode == LocationMode.Dropoff)
		//		{
		//			if (location.type == LocationType.AsDirected)
		//			{
		//				charterReservation.AsDirected = true;
		//				//ToDoZTrip: always should be false for Location to Lcation
		//				charterReservation.HourlyCharterFlag = true;
		//				charterReservation.DropoffAddress = null;
		//			}
		//			else
		//			{
		//				charterReservation.AsDirected = false;
		//				charterReservation.HourlyCharterFlag = false;

		//				////RateService rates = new RateService(token);
		//				////rates.TrackTime = TrackTime;
		//				////var location_new = location;
		//				////if (location.Latitude == 0 || location.Longitude == 0)
		//				////	location_new = rates.GetGeoCodeForAddress(location);

		//				charterReservation.DropoffAddress = new ReservationsServiceSecure.AddressRecord();
		//				charterReservation.DropoffAddress.CountryCode = location.Country;
		//				charterReservation.DropoffAddress.CountrySubDivision = location.StateOrProvince;
		//				charterReservation.DropoffAddress.Latitude = location.Latitude;
		//				charterReservation.DropoffAddress.Longitude = location.Longitude;
		//				charterReservation.DropoffAddress.LocationName = location.PropertyName;
		//				charterReservation.DropoffAddress.LocationType = location.LocationType;
		//				charterReservation.DropoffAddress.Municipality = location.City;
		//				charterReservation.DropoffAddress.PostalCode = location.PostalCode;
		//				charterReservation.DropoffAddress.StreetAddress = location.StreetNumber + " " + location.StreetName;
		//				charterReservation.DropoffAddress.StreetName = location.StreetName;
		//				charterReservation.DropoffAddress.StreetNumber = location.StreetNumber;
		//				charterReservation.DropoffAddress.SubZipName = location.SubZipName;
		//				charterReservation.DropoffAddress.UnitNumber = location.Unit;
		//				charterReservation.DropoffAddress.Comments = string.Empty;
		//				charterReservation.DropoffAddress.ID = 0; //0 is fine
		//			}
		//		}
		//		/** Pickup Address **/
		//		else
		//		{
		//			////RateService rates = new RateService(token);
		//			////rates.TrackTime = TrackTime;

		//			////var location_new = location;
		//			////if (location.Latitude == 0 || location.Longitude == 0)
		//			////	location_new = rates.GetGeoCodeForAddress(location);

		//			charterReservation.PickupAddress = new ReservationsServiceSecure.AddressRecord();
		//			charterReservation.PickupAddress.CountryCode = location.Country;
		//			charterReservation.PickupAddress.CountrySubDivision = location.StateOrProvince;
		//			charterReservation.PickupAddress.Latitude = location.Latitude;
		//			charterReservation.PickupAddress.Longitude = location.Longitude;
		//			charterReservation.PickupAddress.LocationName = location.PropertyName;
		//			charterReservation.PickupAddress.LocationType = location.LocationType;
		//			charterReservation.PickupAddress.Municipality = location.City;
		//			charterReservation.PickupAddress.PostalCode = location.PostalCode;
		//			charterReservation.PickupAddress.StreetAddress = location.StreetNumber + " " + location.StreetName;
		//			charterReservation.PickupAddress.StreetName = location.StreetName;
		//			charterReservation.PickupAddress.StreetNumber = location.StreetNumber;
		//			charterReservation.PickupAddress.SubZipName = location.SubZipName;
		//			charterReservation.PickupAddress.UnitNumber = location.Unit;
		//		}
		//	}
		//	#endregion

		//	#region AsDirected
		//	if (true)
		//	{

		//	}
		//	#endregion

		//	#region FareDetails
		//	charterReservation.FareDetails = new ReservationsServiceSecure.CharterFare();
		//	charterReservation.FareDetails.B2BCommission = 0;
		//	charterReservation.FareDetails.CommissionID = 0;
		//	charterReservation.FareDetails.CompanyFees = null;
		//	charterReservation.FareDetails.CurrencySymbol = string.Empty;
		//	charterReservation.FareDetails.CurrencyType = string.Empty;
		//	charterReservation.FareDetails.CurrencyID = rateResponse.CharterRate.CurrencyID;
		//	charterReservation.FareDetails.DiscountAmount = 0;
		//	charterReservation.FareDetails.DiscountID = 0;
		//	charterReservation.FareDetails.DriverFees = null;
		//	charterReservation.FareDetails.FareID = rateResponse.CharterRate.FareID;
		//	charterReservation.FareDetails.FareTotal = charterReservation.HourlyCharterFlag == true ? (decimal)(segment.CharterMinutes * 1.0 / 60) * rateResponse.CharterRate.HourlyRate : rateResponse.CharterRate.Fare;
		//	charterReservation.FareDetails.FuelSurcharge = 0;
		//	charterReservation.FareDetails.Gratuity = request.Gratuity;// we need this
		//	charterReservation.FareDetails.ServiceTypeCode = rateResponse.CharterRate.ServiceTypeCode;
		//	charterReservation.FareDetails.UserOverrideFare = false; //always be false
		//	charterReservation.FareDetails.Fleet = rateResponse.CharterRate.Fleet;
		//	#endregion

		//	#region Payment
		//	charterReservation.PaymentDetails = new ReservationsServiceSecure.Payment();
		//	charterReservation.PaymentDetails = request.Payment; // should be the same as Airport book. DirectBill or Credit Card.
		//	#endregion

		//	return charterReservation;
		//}
		#endregion
	}
}
