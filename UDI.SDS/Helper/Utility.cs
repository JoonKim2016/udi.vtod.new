﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.SDS.DTO;
using UDI.SDS.DTO.Enum;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Common.DTO.OTA;
//using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.Helper;
using System.Configuration;
namespace UDI.SDS.Helper
{
	public static class Utility
	{
       
		public static TripType GetTripType(UDI.VTOD.Common.DTO.OTA.Service service, RateQualifier rateQualifier)
		{
			var result = TripType.Unknown;

			UDI.VTOD.Common.DTO.OTA.Pickup_Dropoff_Stop pickup = null;
			UDI.VTOD.Common.DTO.OTA.Pickup_Dropoff_Stop dropoff = null;
			UDI.VTOD.Common.DTO.OTA.StopList stops = null;

			var method = Method.UnKnown;

			if (service.Location != null)
				method = Method.GroundBook;
			else
				method = Method.GroundAvail;

			if (method == Method.GroundAvail)
			{
				pickup = service.Pickup;
				dropoff = service.Dropoff;
				stops = service.Stops;
			}
			else
			{
				pickup = service.Location.Pickup;
				dropoff = service.Location.Dropoff;
				stops = service.Location.Stops;
			}

			#region Finding if it's Airport or Charter trip
			if (result == TripType.Unknown)
			{
				if (rateQualifier != null && rateQualifier.RateQualifierValue.Trim().ToLower() == "taxi")
				{
					result = TripType.Taxi;
				}
			}

			if (result == TripType.Unknown)
			{
				if (rateQualifier != null && rateQualifier.Category != null && rateQualifier.Category.Value.Trim().ToLower() == "hourly")
				{
					result = TripType.Hourly;
				}
			}

			if (result == TripType.Unknown)
				if (stops != null && stops.Stops != null && stops.Stops.Any())
				{
					result = TripType.Airport;
				}

			if (dropoff != null)
			{
				if (result == TripType.Unknown)
					if (pickup.Airline != null || dropoff.Airline != null)
					{
						result = TripType.Airport;
					}

				if (result == TripType.Unknown)
					if (pickup.Address != null && dropoff.Address != null)
					{
						result = TripType.LocationToLocation;
					}
			}
			else
			{
				if (result == TripType.Unknown)
					if (pickup.Address != null)
					{
						result = TripType.AsDirected;
					}
			}
			#endregion

			return result;
		}
	}
}
