﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.SDS.DTO;
using UDI.SDS.DTO.Enum;
using UDI.SDS.SecurityService;
using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Common.Track;
using UDI.Utility.Serialization;
using UDI.VTOD.Common.Helper;
using UDI.SDS.Helper;
using UDI.VTOD.Common.DTO.OTA;
using UDI.SDS.ServiceAdapter;


namespace UDI.SDS.IntelliRide
{
	public class IntelliRideReservationService : BaseSetting
	{
		private SecurityToken token = null;
		private int asapRequestID = 0;
		private bool isGuaranteed = false;
		private bool isScheduledStop = false;
		private Guid bookRequestID;

		/// <summary>
		/// Used for IntelliRide project.
		/// </summary>
		private SecurityToken GetToken()
		{
			#region Track
			//if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Domain, "Call");
			#endregion

			using (var securityClient = new SecurityService.WCFSecurityServiceClient())
			{
				token = securityClient.Login(
					int.Parse(ConfigurationManager.AppSettings["SDS_ClientType"]),
					ConfigurationManager.AppSettings["SDS_ClientIPAddress"],
					ConfigurationManager.AppSettings["SDS_Username"],
					ConfigurationManager.AppSettings["SDS_Password"]);

				#region Track
				//if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "Login");
				#endregion
			}

			return token;
		}

		/// <summary>
		/// Used by IntelliRide project.
		/// </summary>
		public BookResponse GetReservationResponse(BookRequest request, UserInfo userInfo)
		{
			try
			{
				logger.InfoFormat("Reservation Start...\r\n{0}", request.XmlSerialize());
				token = GetToken();
				request.RequestUtcTime = DateTime.UtcNow;
				BookResponse response = new BookResponse();
				response.ReservationResponses = new List<ReservationResponse>();
				ReservationResponse reservationResponse = new ReservationResponse();
				var vtodToken = new TokenRS { ClientIPAddress = token.ClientIPAddress, ClientType = token.ClientType, SecurityKey = token.SecurityKey.ToString(), Username = token.UserName };

				UDI.SDS.ReservationsServiceSecure.SecurityToken reservationToken = vtodToken.ToReservationSecureServiceToken();
				var reservationAdapter = new ReservationSecureServiceAdapter(reservationToken, null);

				#region Get Rate

				RateResponse rateResponse = new RateResponse();
				rateResponse = GetRate(request.Segments.FirstOrDefault(), token, request.ReservationRequest.DiscountCode, userInfo);
				/** GetRate will update location's MasterlandmarkID, LocationType and SubZipName, so use rateResponse.Segment here **/
				var departingSegment = rateResponse.Segment;
				var returningSegment = request.Segments.Count == 2 ? request.Segments.Last() : null;

				#endregion

				#region Get pickup time
				PickupTimeResponse pickupTimeResponse = new PickupTimeResponse();
				pickupTimeResponse = GetPickupTime(departingSegment, rateResponse, request.HotelConciergeID, token);
				#endregion

				#region Save Reservation
				if (rateResponse.TripDirection == TripDirection.Inbound || rateResponse.TripDirection == TripDirection.Outbound)
				{
					/** Airport Reservation **/
					//TODoInteliRide
					//reservationResponse.AirportReservationConfirmations = reservationAdapter.SaveAirportReservation(departingSegment, returningSegment, request.reservationRequest, rateResponse, null, pickupTimeResponse, null, null, null);

				}
				else
				{
					/** Charter Reservation **/
					reservationResponse.CharterReservationConfirmation = reservationAdapter.SaveCharterReservation(departingSegment, request.ReservationRequest, rateResponse, null, pickupTimeResponse, null);
				}
				#endregion

				reservationResponse.Segments = new List<Segment>();
				reservationResponse.Segments.Add(departingSegment);
				reservationResponse.Segments.Add(returningSegment);
				response.ReservationResponses.Add(reservationResponse);

				response.ResponseUtcTime = DateTime.UtcNow;
				//response.BookRequestID = request.BookRequestID;
				logger.InfoFormat("Reservation End...\r\n{0}", response.XmlSerialize());
				return response;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("GetReservationResponse BookRequestID:{0}", ex.ToString());
				throw ex;
			}
		}

		/// <summary>
		/// Return the latest Time for the input rate. 
		/// GetRate will update location's MasterlandmarkID, LocationType and SubZipName, so input rateResponse.Segment here.
		/// </summary>
		public PickupTimeResponse GetPickupTime(Segment segment, RateResponse rateResponse, int hotelConciergeID, SecurityToken token)
		{
			try
			{
				var vtodToken = new TokenRS { ClientIPAddress = token.ClientIPAddress, ClientType = token.ClientType, SecurityKey = token.SecurityKey.ToString(), Username = token.UserName };
				ReservationsController pickupTime = new ReservationsController(vtodToken, null);
				//pickupTime.TrackTime = TrackTime;

				PickupTimeResponse pickupTimeResponse = new PickupTimeResponse();
				pickupTimeResponse = GetPickupTimeResponse(rateResponse.Segment, rateResponse, hotelConciergeID);
				logger.InfoFormat("GetPickupTime returns...\r\n{0}", pickupTimeResponse.XmlSerialize());

				return pickupTimeResponse;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("GetPickupTime BookRequestID:{0}\r\n {1}", rateResponse.BookRequestID, ex.ToString());
				throw ex;
			}
		}

		/// <summary>
		/// The 3 parameters: lastName, phoneNumber, and postalCode are considered to be validating data. 
		/// This validates the user knows more information than just guessing the confirmation number. 
		/// One of these fields values must be supplied and match what is stored with the reservation. 
		/// userName is the name of the person cancelling the reservation.
		/// </summary>
		public bool CancelIntelliRideReservation(string confirmationNumber, string userName, string lastName, string phoneNumber, string postalCode)
		{
			try
			{
				logger.InfoFormat("Cancel IntelliRide Reservation Start...\r\n  ConfirmationNumber:{0}, userName:{1}, lastName:{2}, phoneNumber:{3}, postalCode: {4}", confirmationNumber, userName, lastName, phoneNumber, postalCode);

				token = GetToken();
				int result = -1;

				#region Track
				//if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Domain, "Call");
				#endregion

				using (var client = new ReservationsService.ReservationsServiceClient())
				{
					result = client.CancelReservation(token.ToReservationsServiceToken(), confirmationNumber, userName, lastName, phoneNumber, postalCode);
				}

				#region Track
				//if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "CancelReservation");
				#endregion

				/*
				 * 0 = Reservation Cancelled 
				 * 1 = Too close to pickup time. 
				 * 2 = Reservation already cancelled. 
				 * 3 = Reservation pickup time has expired, reservation cannot be cancelled.             
				 */
				if (result == 0 || result == 2)
					return true;
				else if (result == 1)
					throw new Exception(string.Format(Messages.UDI_SDS_CancelFailed_TooCloseToPickupTime, "CancelReservation"));
				else if (result == 3)
					throw new Exception(string.Format(Messages.UDI_SDS_CancelFailed_PickupTimeExpired, "CancelReservation"));
				else
					throw new Exception(string.Format(Messages.UDI_SDS_CancelFailed_Unknown, "CancelReservation", result));
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Cancel IntelliRide ConfirmationNumber:{0}\r\n {1}", confirmationNumber, ex.ToString());
				throw ex;
			}
		}

		/// <summary>
		/// Get all reservation statuses from SDS for IntelliRide
		/// </summary>
		public List<DispatchUpdatesService.TripStatusRecord> GetIntelliRideReservationStatus(string rezSource)
		{
			try
			{
				logger.InfoFormat("GetReservationStatus Start...\r\n  rezSource={0}", rezSource);

				token = GetToken();
				List<DispatchUpdatesService.TripStatusRecord> result = null;

				#region Track
				//if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Domain, "Call");
				#endregion

				using (var client = new DispatchUpdatesService.DispatchUpdatesServiceClient())
				{
					result = client.GetDispatchStatusUpdates(rezSource);
				}

				#region Track
				//if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetDispatchStatusUpdates");
				#endregion

				if (System.Configuration.ConfigurationManager.AppSettings["IsDebug"].ToString().ToLower().Equals("true"))
					logger.InfoFormat("GetReservationStatus End...\r\n{0}", result.XmlSerialize());

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("GetReservationStatus: rezSource={0}\r\n {1}", rezSource, ex.ToString());
				throw ex;
			}
		}

		/// <summary>
		/// Get the lowest rate and all the rates.
		/// </summary>
		public RateResponse GetRate(Segment segment, SecurityToken token, string discountCode, UserInfo userInfo)
		{
			#region Init
			var vtodToken = new TokenRS { ClientIPAddress = token.ClientIPAddress, ClientType = token.ClientType, SecurityKey = token.SecurityKey.ToString(), Username = token.UserName };
			var rateController = new RateController(vtodToken, null);
			#endregion

			try
			{
				RateController rates = new RateController(vtodToken, null);

				RateResponse rateResponse = rateController.GetRatePerSegment(bookRequestID, segment, discountCode);
				logger.InfoFormat("GetRate returns...\r\n{0}", rateResponse.XmlSerialize());

				return rateResponse;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("GetRate BookRequestID:{0}\r\n {1}", bookRequestID, ex.ToString());
				throw ex;
			}
		}

		/// <summary>
		/// Validate user requested pickup time only for Ecar service
		/// </summary>
		private DateTime validatePickupTime(bool isAccessibleServiceRequired, string airportCode, int childSeats, int payingPax, int tripDirection, DateTime? pickupTime, DateTime flightTime, int hotelConciergeID, int serviceTypeID)
		{
			try
			{
				if (pickupTime > flightTime)
					throw new Exception(Messages.UDI_SDS_PickupTime_After_FlightTime);


				DateTime validPickupTime = new DateTime();

				if (pickupTime.HasValue)
				{
					logger.InfoFormat("Send to SuperShuttle \r\n tripDirection={0}, pickupTime={1}, hotelConciergeID={2}, serviceTypeID={3}", tripDirection, pickupTime, hotelConciergeID, serviceTypeID);
					using (var client = new ReservationsService.ReservationsServiceClient())
					{
						#region Track
						//if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Domain, "Call");
						#endregion

						//TOTOIntelliride
						//var result = client.ValidatePickupTime(token.ToReservationsServiceToken(), tripDirection, pickupTime.Value, hotelConciergeID, serviceTypeID);
						var result = client.ValidatePickupTime(null, tripDirection, pickupTime.Value, hotelConciergeID, serviceTypeID);

						#region Track
						//if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "ValidatePickupTime");
						#endregion

						if (string.Compare(result.ToString(), "AllowPickup", true) == 0)
							validPickupTime = pickupTime.Value;

						if (string.Compare(result.ToString(), "MinAdvanceNoticeExceeded", true) == 0)
						{
							validPickupTime = getASAPPickupTime(isAccessibleServiceRequired, airportCode, serviceTypeID, childSeats, payingPax, pickupTime.Value, flightTime, tripDirection);
						}
					}
				}
				else
				{
					throw new Exception(Messages.UDI_SDS_PickupTime_After_FlightTime);
				}

				return validPickupTime;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call ReservationsService.ValidatePickupTime {0}", ex.StackTrace);
				throw ex;
			}
		}

		/// <summary>
		/// For Inbound:
		/// If Fleet = SuperShuttle,
		///       Return the Latest Guarantee pickup time generated by GetPickupTime method;
		/// If Fleet = Ecar and pickup time provided,
		///       Validate pickup time provided and return it if it's valid;
		/// If Fleet = Ecar and no pickup time provided,
		///      Return default pickup time (lastest time) generated by GetPickupTime method;
		/// For Outbound:
		/// Return flight arrival time;
		/// </summary>
		internal PickupTimeResponse GetPickupTimeResponse(Segment segment, RateResponse rateResponse, int hotelConciergeID)
		{
			try
			{
				this.bookRequestID = rateResponse.BookRequestID;
				PickupTimeResponse response = new PickupTimeResponse();
				var pickupTime = new DateTime();

				if (rateResponse.TripDirection == TripDirection.Inbound || rateResponse.TripDirection == TripDirection.Outbound)
				{
					#region Set the GetPickupTimes required parameters for Airport Reservations
					var masterLandmarkID = 0;
					var airportCode = string.Empty;
					bool isAccessibleServiceRequired = segment.Wheelchairs > 0 ? true : false;

					foreach (var addressLocation in segment.Locations)
					{
						if (addressLocation.type == UDI.SDS.DTO.Enum.LocationType.Address)
							masterLandmarkID = addressLocation.MasterLandmarkID;

						if (addressLocation.type == UDI.SDS.DTO.Enum.LocationType.Airport)
							airportCode = addressLocation.AiportCode;
					}
					#endregion

					/** All airport reservations needs to call GetPickupTime() to determine if the reservation can be serviced **/
					pickupTime = getPickupTime(isAccessibleServiceRequired, hotelConciergeID, segment.IsInternationalFlight, masterLandmarkID, rateResponse.AirportReservationRate.FareID, segment.FlightDateTime, (int)rateResponse.TripDirection, airportCode, segment.ChildSeats, segment.PayingPax);

					/** If Fleet = Ecar and pickup time provided is valid, then return the pickup time provided; **/
					if (segment.RequestedFleet == SDSFleet.ExecuCar && segment.RequestPickupTime != null)
					{
						pickupTime = validatePickupTime(isAccessibleServiceRequired, airportCode, segment.ChildSeats, segment.PayingPax, (int)rateResponse.TripDirection, segment.RequestPickupTime, segment.FlightDateTime, hotelConciergeID, rateResponse.AirportReservationRate.FareID);
					}
				}
				else
				{
					pickupTime = segment.RequestPickupTime;
				}

				response.AsapRequestId = this.asapRequestID;
				response.AvailablePickupTime = pickupTime;
				response.AvailablePickupEndTime = pickupTime.AddMinutes(15);
				response.IsGuaranteed = this.isGuaranteed;
				response.BookRequestID = rateResponse.BookRequestID;
				return response;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		/// <summary>
		/// Get the Latest Guarantee Time from SuperShuttle
		/// </summary>
		private DateTime getPickupTime(bool isAccessibleServiceRequired, int hotelConciergeID, bool isInternationalFlight, int masterLandmarkID, int serviceTypeID, DateTime flightTime, int tripDirection, string airportCode, int childSeats, int payingPax)
		{
			try
			{
				var errorMsg = string.Empty;
				var pickupTimes = new List<ReservationsService.PickupTimeRecord>();
				DateTime availablePickupTime = new DateTime();
				ReservationsService.PickupTimeRequest request = new ReservationsService.PickupTimeRequest();
				request.AccessibleServiceRequired = isAccessibleServiceRequired;
				request.HotelConciergeID = hotelConciergeID;
				request.IsInternationalFlight = isInternationalFlight;
				request.MasterLandmarkID = masterLandmarkID == -2147483648 ? 0 : masterLandmarkID; //use zero for MasterLandmarkId if there's no landmark
				request.ServiceTypeID = serviceTypeID;
				request.TravelTime = flightTime;
				request.TripDirection = tripDirection;

				logger.InfoFormat("Send to SuperShuttle \r\n{0}", request.XmlSerialize());
				using (var client = new ReservationsService.ReservationsServiceClient())
				{
					#region Track
					//if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Domain, "Call");
					#endregion

					var result = client.GetPickupTime(token.ToReservationsServiceToken(), request);

					#region Track
					//if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetPickupTime");
					#endregion

					if (result.HasRecords)
					{
						pickupTimes = result.PickupTimeRecordsArray.OrderByDescending(p => p.StartTime).ToList(); //order by start time desc
					}
					else
					{
						errorMsg = "GetPickupTime";
					}
				}

				foreach (var time in pickupTimes)
				{
					/** 1. Pickup time is Soft Pickup Time(means ExecuCar) and Status is AllowPickup, ONLY return default pickup time(latest time) to user **/
					if (time.IsSoftPickupTime == true && time.Status.ToString().ToLower() == PickupTimeStatus.AllowPickup.ToString().ToLower())
					{
						availablePickupTime = time.StartTime;
						break;
					}
					else
					{
						/** 2. Else return the Latest Guarantee Time to user **/
						if (time.Status.ToString().ToLower() == PickupTimeStatus.AllowPickup.ToString().ToLower())
						{
							availablePickupTime = time.StartTime;
							this.isGuaranteed = time.IsGuaranteedTime;
							this.isScheduledStop = time.IsScheduledStop;

							if (time.IsGuaranteedTime == true)//Stop loop when find guaranteed time
							{
								break;
							}
						}

						/** 3. If SuperShuttle and Status is AdvanceNoticeExceeded, GO TO ASAP pickup time process **/
						else if (string.Compare(time.Status.ToString(), "MinAdvanceNoticeExceeded", true) == 0)
						{
							availablePickupTime = getASAPPickupTime(isAccessibleServiceRequired, airportCode, serviceTypeID, childSeats, payingPax, time.StartTime, flightTime, tripDirection);
							break;
						}
						else
						{
							throw new Exception(string.Format(Messages.UDI_SDS_ASAP_InvalidPickupTimeStatus, "GetPickupTime", time.Status.ToString()));

						}
					}
				}


				if (!string.IsNullOrEmpty(errorMsg))
					throw new Exception(string.Format(Messages.UDI_SDS_ASAP_NoPickupTimeFound, errorMsg));

				return availablePickupTime;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call ReservationsService.GetPickupTime {0}", ex.StackTrace);
				throw ex;
			}
		}


		/// <summary>
		/// Get the ASAP pickup time from SuperShuttle
		/// </summary>
		private DateTime getASAPPickupTime(bool isAccessibleServiceRequired, string airportCode, int serviceTypeID, int childSeats, int payingPax, DateTime recommendedPickupTime, DateTime flightTime, int tripDirection)
		{
			try
			{
				DateTime asapPickupTime = new DateTime();
				string errorMsg = string.Empty;
				ASAPService.RequestDetails asapDetails = new ASAPService.RequestDetails();
				asapDetails.AccessibleServiceRequired = isAccessibleServiceRequired;
				asapDetails.AirportCode = airportCode;
				asapDetails.FareID = serviceTypeID;
				asapDetails.FreePax = (byte)childSeats;
				asapDetails.PayingPax = (byte)payingPax;
				asapDetails.PickupTime = recommendedPickupTime;
				asapDetails.TravelTime = flightTime;
				asapDetails.TripDirection = tripDirection;

				ASAPService.StatusUpdate status = new ASAPService.StatusUpdate();

				logger.InfoFormat("Send to SuperShuttle \r\n{0}", asapDetails.XmlSerialize());
				using (var client = new ASAPService.ASAPServiceClient())
				{
					var asapToken = token.ToASAPServiceToken();

					//Create Request
					#region Track
					//if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Domain, "Call");
					#endregion

					this.asapRequestID = client.CreateRequest(asapToken, asapDetails);

					#region Track
					//if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "CreateRequest");
					#endregion

					logger.InfoFormat("SuperShuttle Returns asapRequestID={0}", asapRequestID);
					//Check Request Status

					#region Track
					//if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Domain, "Call");
					#endregion

					status = client.GetRequestStatus(asapToken, asapRequestID);

					#region Track
					//if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetRequestStatus");
					#endregion
				}

				if (status.Completed == true)
				{
					switch (status.DispositionCode.ToString().ToUpper())
					{
						case "PICKUPDENIED":
							throw new Exception(string.Format(Messages.UDI_SDS_PICKUPDENIED, "GetASAPPickupTime"));

						case "ORIGINALPICKUPTIMEAPPROVED":
							asapPickupTime = recommendedPickupTime;
							break;
						case "NEWPICKUPTIMEAPPROVED":
							asapPickupTime = status.ApprovedPickupTime;
							break;
						default:
							throw new Exception(string.Format(Messages.UDI_SDS_ASAP_DispositionCode, "GetASAPPickupTime", status.DispositionCode.ToString()));

					}
				}
				else
					throw new Exception(string.Format(Messages.UDI_SDS_ASAP_NotCompleted, "GetASAPPickupTime"));


				return asapPickupTime;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call ASAPService.CreateRequest and ASAPService.GetRequestStatus {0}", ex.StackTrace);
				throw ex;
			}
		}
	}
}
