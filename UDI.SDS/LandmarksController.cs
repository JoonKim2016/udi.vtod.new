﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.Track;
using UDI.SDS.Helper;
using UDI.SDS.LandmarksService;
using UDI.SDS.DTO.Enum;
using UDI.SDS.DTO;
using UDI.SDS.ServiceAdapter;

namespace UDI.SDS
{
	public class LandmarksController : BaseSetting
	{
		#region Fields
		TrackTime _trackTime;
		TokenRS _token;
		#endregion

		#region Constructors
		private LandmarksController()
		{
		}

		public LandmarksController(TokenRS token, TrackTime trackTime)
		{
			_token = token;
			_trackTime = trackTime;
		}

		#endregion

		#region Public
		public LandmarksService.LandmarkRecord GetLandmarkRecordByAddress(string airportCode, UDI.SDS.DTO.Location location)
		{
			#region Init
			UDI.SDS.LandmarksService.SecurityToken landmarksToken = _token.ToLandmarksServiceToken();
			var landmarksAdapter = new LandmarksServiceServiceAdapter(landmarksToken, _trackTime);
			#endregion

			LandmarksService.LandmarkRecord result = null;

			try
			{
				result = landmarksAdapter.GetLandmarkRecordByAddress(airportCode, location);

			}
			catch (Exception ex)
			{
				logger.Error("Cannot get LandmarkRecord (GetLandmarkRecordByAddress)", ex);
			}

			return result;
		}

		public LandmarksService.LandmarkRecord GetLandmarksByCoordinates(string airportCode, UDI.SDS.DTO.Location location)
		{
			#region Init
			UDI.SDS.LandmarksService.SecurityToken landmarksToken = _token.ToLandmarksServiceToken();
			var landmarksAdapter = new LandmarksServiceServiceAdapter(landmarksToken, _trackTime);
			#endregion

			LandmarksService.LandmarkRecord result = null;

			try
			{
				result = landmarksAdapter.GetLandmarksByCoordinates(airportCode, location);

			}
			catch (Exception ex)
			{
				logger.Error("Cannot get LandmarkRecord (GetLandmarksByCoordinates)", ex);
			}

			if (result == null && !string.IsNullOrWhiteSpace(airportCode))
			{
				result = GetLandmarkRecordByAddress(airportCode, location);
			}

			return result;
		}
		#endregion

		#region Properties
		public TrackTime TrackTime
		{
			get
			{
				return _trackTime;
			}
			set
			{
				_trackTime = value;
			}
		}

		public TokenRS Token
		{
			set
			{
				_token = value;
			}
		}
		#endregion
	}
}
