﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.Track;
using UDI.SDS.Helper;
using UDI.SDS.DTO.Enum;
using UDI.SDS.DTO;
using UDI.SDS.ServiceAdapter;
using UDI.SDS.MembershipService;

namespace UDI.SDS
{
	public class MembershipController : BaseSetting
	{
		#region Fields
		TokenRS _token;
		TrackTime _trackTime;
		#endregion

		#region Constructors
		private MembershipController()
		{
		}

		public MembershipController(TokenRS token, TrackTime trackTime)
		{
			_token = token;
			_trackTime = trackTime;
		}
		#endregion

		#region Public
		public void LinkReservation(long vtodTripID, int sdsRezID, int memberID)
		{
			#region Init
			UDI.SDS.MembershipService.SecurityToken membershipToken = _token.ToMembershipServiceToken();
			var membershipAdapter = new MembershipServiceAdapter(membershipToken, _trackTime);
			#endregion

			membershipAdapter.LinkReservation(vtodTripID, sdsRezID, memberID);
		}

        public void DelinkReservation(long vtodTripID, int sdsRezID, int memberID)
        {
            #region Init
            UDI.SDS.MembershipService.SecurityToken membershipToken = _token.ToMembershipServiceToken();
            var membershipAdapter = new MembershipServiceAdapter(membershipToken, _trackTime);
            #endregion

            membershipAdapter.DelinkReservation(vtodTripID, sdsRezID, memberID);
        }

        public MembershipService.MembershipRecord CreateMembership(MembershipService.MembershipRecord membership, string password)
		{
			#region Init
			UDI.SDS.MembershipService.SecurityToken membershipToken = _token.ToMembershipServiceToken();
			var membershipAdapter = new MembershipServiceAdapter(membershipToken, _trackTime);
			#endregion

			var result = membershipAdapter.CreateMembership(membership, password);

			return result;
		}

		public MembershipService.MembershipLocationRecord MemberAddLocation(MembershipService.AddressRecord addressRecord, int memberId)
		{
			#region Init
			UDI.SDS.MembershipService.SecurityToken membershipToken = _token.ToMembershipServiceToken();
			var membershipAdapter = new MembershipServiceAdapter(membershipToken, _trackTime);
			#endregion

			var result = membershipAdapter.MemberAddLocation(addressRecord, memberId);

			return result;
		}

		public bool MemberChangeEmailAddress(int memberId, string newEmailAddress)
		{
			#region Init
			UDI.SDS.MembershipService.SecurityToken membershipToken = _token.ToMembershipServiceToken();
			var membershipAdapter = new MembershipServiceAdapter(membershipToken, _trackTime);
			#endregion

			var result = membershipAdapter.MemberChangeEmailAddress(memberId, newEmailAddress);

			return result;
		}

		public bool MemberChangePassword(string emailAddress, string oldPassword, string newPassword)
		{
			#region Init
			UDI.SDS.MembershipService.SecurityToken membershipToken = _token.ToMembershipServiceToken();
			var membershipAdapter = new MembershipServiceAdapter(membershipToken, _trackTime);
			#endregion

			var result = membershipAdapter.MemberChangePassword(emailAddress, oldPassword, newPassword);

			return result;
		}

		public bool MemberDeleteLocation(int locationId)
		{
			#region Init
			UDI.SDS.MembershipService.SecurityToken membershipToken = _token.ToMembershipServiceToken();
			var membershipAdapter = new MembershipServiceAdapter(membershipToken, _trackTime);
			#endregion

			var result = membershipAdapter.MemberDeleteLocation(locationId);

			return result;
		}

		public MembershipService.PasswordRequest MemberForgotPassword(string emailAddress,EnumerationsWebSiteBrands sourceWebSite)
		{
			#region Init
			UDI.SDS.MembershipService.SecurityToken membershipToken = _token.ToMembershipServiceToken();
			var membershipAdapter = new MembershipServiceAdapter(membershipToken, _trackTime);
			#endregion

            var result = membershipAdapter.MemberForgotPassword(emailAddress, sourceWebSite);

			return result;
		}

		public MembershipService.MembershipLocationRecords MemberGetLocations(int memberId)
		{
			#region Init
			UDI.SDS.MembershipService.SecurityToken membershipToken = _token.ToMembershipServiceToken();
			var membershipAdapter = new MembershipServiceAdapter(membershipToken, _trackTime);
			#endregion

			var result = membershipAdapter.MemberGetLocations(memberId);

			return result;
		}

		public MembershipService.MembershipReservations MemberGetReservation(int memberId, int rowCount)
		{
			#region Init
			UDI.SDS.MembershipService.SecurityToken membershipToken = _token.ToMembershipServiceToken();
			var membershipAdapter = new MembershipServiceAdapter(membershipToken, _trackTime);
			#endregion

			var result = membershipAdapter.MemberGetReservation(memberId, rowCount);

			return result;
		}

		public MembershipService.MembershipRecord MemberLogin(string emailAddress, string password)
		{
			#region Init
			UDI.SDS.MembershipService.SecurityToken membershipToken = _token.ToMembershipServiceToken();
			var membershipAdapter = new MembershipServiceAdapter(membershipToken, _trackTime);
			#endregion

			var result = membershipAdapter.MemberLogin(emailAddress, password);

			return result;
		}
               
        public bool MemberUpdateLocation(MembershipService.MembershipLocationRecord locationRecord, int memberId)
		{
			#region Init
			UDI.SDS.MembershipService.SecurityToken membershipToken = _token.ToMembershipServiceToken();
			var membershipAdapter = new MembershipServiceAdapter(membershipToken, _trackTime);
			#endregion

			var result = membershipAdapter.MemberUpdateLocation(locationRecord, memberId);

			return result;
		}

		public bool MemberUpdateProfile(MembershipService.MembershipRecord membershipRecord)
		{
			#region Init
			UDI.SDS.MembershipService.SecurityToken membershipToken = _token.ToMembershipServiceToken();
			var membershipAdapter = new MembershipServiceAdapter(membershipToken, _trackTime);
			#endregion

			var result = membershipAdapter.MemberUpdateProfile(membershipRecord);

			return result;
		}

		public MembershipService.PasswordRequest MemberGetPasswordRequest(Guid requestGuid)
		{
			#region Init
			UDI.SDS.MembershipService.SecurityToken membershipToken = _token.ToMembershipServiceToken();
			var membershipAdapter = new MembershipServiceAdapter(membershipToken, _trackTime);
			#endregion

			var result = membershipAdapter.GetPasswordRequest(requestGuid);

			return result;
		}

		public bool ResetPassword(int memberID, string newPassword, Guid requestGuid)
		{
			#region Init
			UDI.SDS.MembershipService.SecurityToken membershipToken = _token.ToMembershipServiceToken();
			var membershipAdapter = new MembershipServiceAdapter(membershipToken, _trackTime);
			#endregion

			var result = membershipAdapter.ResetPassword(memberID, newPassword, requestGuid);

			return result;
		}

		public CommunicationSettings GetProfileCommunicationSettings(int memberID)
		{
			#region Init
			UDI.SDS.MembershipService.SecurityToken membershipToken = _token.ToMembershipServiceToken();
			var membershipAdapter = new MembershipServiceAdapter(membershipToken, _trackTime);
			#endregion

			var result = membershipAdapter.GetProfileCommunicationSettings(memberID);

			return result;
		}

		public UDI.SDS.MembershipService.CorporateAccount GetCorporateAccountKey(string corporation, string corporateUserId)
		{
			#region Init
			UDI.SDS.MembershipService.SecurityToken membershipToken = _token.ToMembershipServiceToken();
			var membershipAdapter = new MembershipServiceAdapter(membershipToken, _trackTime);
			#endregion

			var result = membershipAdapter.GetCorporateAccountKey(corporation, corporateUserId);

			return result;

		}

		public bool MemberDelete(string emailAddress, string password)
		{

			#region Init
			UDI.SDS.MembershipService.SecurityToken membershipToken = _token.ToMembershipServiceToken();
			var membershipAdapter = new MembershipServiceAdapter(membershipToken, _trackTime);
			#endregion

			var result = membershipAdapter.MemberDelete(emailAddress, password);
            
			return result;
		}

		public bool SetPhoneNumberVerified(bool isVerified, int memberId, string phoneNumber)
		{

			#region Init
			UDI.SDS.MembershipService.SecurityToken membershipToken = _token.ToMembershipServiceToken();
			var membershipAdapter = new MembershipServiceAdapter(membershipToken, _trackTime);
			#endregion


			var result = membershipAdapter.SetPhoneNumberVerified(isVerified, memberId, phoneNumber);

			return result;
		}

        public string CreateNewZTripCredit(decimal Amount, int memberId,ZTripCreditType requestType)
        {

            #region Init
            UDI.SDS.MembershipService.SecurityToken membershipToken = _token.ToMembershipServiceToken();
            var membershipAdapter = new MembershipServiceAdapter(membershipToken, _trackTime);
            #endregion


            string result = membershipAdapter.CreateNewZTripCredit(Amount, memberId, requestType).ActivationMessage;

            return result;
        }

        public MembershipCreditCardAccount PatchCredit(PatchCreditCardRequest request)
        {

            #region Init
            UDI.SDS.MembershipService.SecurityToken membershipToken = _token.ToMembershipServiceToken();
            var membershipAdapter = new MembershipServiceAdapter(membershipToken, _trackTime);
            #endregion
            var result = membershipAdapter.PatchCredit(request);

            return result;
        }
		
		#endregion

		#region Properties
		public TrackTime TrackTime
		{
			get
			{
				return _trackTime;
			}
			set
			{
				_trackTime = value;
			}
		}

		public TokenRS Token
		{
			set
			{
				_token = value;
			}
		}
		#endregion
	}
}
