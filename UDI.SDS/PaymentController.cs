﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.Track;
using UDI.SDS.Helper;
using UDI.SDS.RoutingService;
using UDI.SDS.DTO.Enum;
using UDI.SDS.DTO;
using UDI.SDS.ServiceAdapter;
using UDI.SDS.PaymentsService;

namespace UDI.SDS
{
	public class PaymentController : BaseSetting
	{
		#region Fields
		TrackTime _trackTime;
		TokenRS _token;
		#endregion

		#region Constructors
		private PaymentController()
		{
		}

		public PaymentController(TokenRS token, TrackTime trackTime)
		{
			_token = token;
			_trackTime = trackTime;
		}

		#endregion

		#region Public

        public PreSaleVerificationResponse PreSaleCardVerification(int accountID, decimal amount, int memberID, int merchandID)
		{
            try
            {
                #region Init
                UDI.SDS.PaymentsService.SecurityToken token = _token.ToPaymentsServiceToken();
                var paymentAdapter = new PaymentServiceAdapter(token, _trackTime);
                #endregion

                var result = paymentAdapter.PreSaleCardVerification(accountID, amount, memberID, merchandID);

                return result;
            }
            catch(Exception ex)
            {
                throw ex;
            }
		}


		//public decimal GetDistanceBetweenPoints(Point startPoint, Point endPoint)
		//{
		//	#region Init
		//	UDI.SDS.RoutingService.SecurityToken routingToken = _token.ToRoutingServiceToken();
		//	var routingAdapter = new RoutingServiceAdapter(routingToken, _trackTime);
		//	#endregion

		//	var result = routingAdapter.GetDistanceBetweenPoints(startPoint, endPoint);

		//	return result;
		//}
		#endregion

		#region Properties
		public TrackTime TrackTime
		{
			get
			{
				return _trackTime;
			}
			set
			{
				_trackTime = value;
			}
		}

		public TokenRS Token
		{
			set
			{
				_token = value;
			}
		}
		#endregion
	}
}
