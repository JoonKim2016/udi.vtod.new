﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.Track;
using UDI.SDS.Helper;
using UDI.SDS.ServiceAdapter;
using UDI.SDS.DTO.Enum;
using UDI.SDS.DTO;
using UDI.VTOD.Common.Helper;
using UDI.VTOD.Common.DTO.Enum;
using UDI.Utility.Serialization;
using UDI.SDS.LandmarksService;
using UDI.SDS.RatesService;
using UDI.Utility.Helper;

namespace UDI.SDS
{
	public class RateController : BaseSetting
	{
		#region Fields
		TrackTime _trackTime;
		TokenRS _token;
		private UserInfo userInfo = new UserInfo { AllowClosedSplits = false, ShowSplits = false, PickupTimeApproach = (int)VTOD.Common.DTO.Enum.PickupTimeApproach.Validation };
		//private Guid bookRequestID;
		#endregion

		#region Constructors
		private RateController()
		{
		}

		public RateController(TokenRS token, TrackTime trackTime)
		{
			_token = token;
			_trackTime = trackTime;
		}

		#endregion

		#region Public
		public RatesResponse GetAirportRate(List<Segment> segments, Dictionary<string, bool> subfleetPermission, string discountCode, bool? meetAndGreet, decimal gratuityRate, decimal bookingFee, int directBillAccountId, string vehicleCode, UDI.VTOD.Common.DTO.Enum.Currency currency, bool? IsAccessible , bool? IsInboundRideNow , bool? IsOutboundRideNow , string UserId,bool? RemoveCallCenterBookingFee,string CultureCode)
		{
			#region Init
			UDI.SDS.LandmarksService.SecurityToken landmarkToken = _token.ToLandmarksServiceToken();
			UDI.SDS.RatesService.SecurityToken rateToken = _token.ToRatesServiceToken();

			var landmarksController = new LandmarksController(_token, _trackTime);
			var franchiseController = new FranchiseController(_token, _trackTime);
			var groupsController = new GroupsController(_token, _trackTime);
			var utilityController = new UtilityController(_token, _trackTime);
			var zipCodeController = new ZipCodesController(_token, _trackTime);
			var geocoderController = new GeocoderController(_token, _trackTime);
			var rateAdapter = new RateServiceAdapter(rateToken, _trackTime);
			#endregion

			try
			{
				#region Init
				var result = new RatesResponse();
				var rateServiceInfoResults = new List<ServiceInfo>();
				discountCode = discountCode.Trim();
				//this.bookRequestID = bookRequestID;
				Segment inboundSegment = null;
				Segment outboundSegment = null;
				UDI.SDS.DTO.Location inboundLocation = null;
				UDI.SDS.DTO.Location outboundLocation = null;
				bool inboundLocationModified = false;
				bool outboundLocationModified = false;
				UDI.SDS.LandmarksService.LandmarkRecord landmarkRecord = null;
				bool isInboundOrigin = false;
				bool isRoundTrip = false;
				int fleetFilter;
				string airportCode = string.Empty;
				string postalCode = string.Empty;
				string inboundFranchiseCode = null;
				string outboundFranchiseCode = null;
				DateTime? inboundDate = null;
				DateTime? outboundDate = null;				
				int payingPax = 0;
				int childPax = 0;
				int masterLandmarkID = (int)UDI.SDS.DTO.Enum.Defaults.MasterLandmarkID;
				string subZipName;
				string locationName;
				string locationType;
				var reservationType = ReservationType.Unknown;
				var tripDirection = TripDirection.Unknown;
				var isZipSplit = false;
                decimal latitude =0;
                decimal longitude = 0;
				#endregion

				#region First Segment
				if (segments.First().TripDirection == TripDirection.Inbound)
				{
					inboundSegment = segments.First();
					isInboundOrigin = true;
				}
				else
				{
					outboundSegment = segments.First();
				}

				#endregion

				#region Second Segment
				if (segments.Count == 2)
				{
					if (inboundSegment == null)
						inboundSegment = segments.Last();
					else
						outboundSegment = segments.Last();
				}
				#endregion

				#region fleetFilter
				if (inboundSegment != null)
				{
					if (inboundSegment.RequestedFleet == SDSFleet.ExecuCar)
						fleetFilter = (int)SDSFleet.ExecuCar;
					else if (inboundSegment.RequestedFleet == SDSFleet.SuperShuttle)
						fleetFilter = (int)SDSFleet.SuperShuttle;
					else if (inboundSegment.RequestedFleet == SDSFleet.SuperShuttleSharedRideOnly)
					{
						fleetFilter = (int)SDSFleet.SuperShuttle;						
					}
					else
						fleetFilter = (int)SDSFleet.All;
				}
				else
				{
					if (outboundSegment.RequestedFleet == SDSFleet.ExecuCar)
						fleetFilter = (int)SDSFleet.ExecuCar;
					else if (outboundSegment.RequestedFleet == SDSFleet.SuperShuttle)
						fleetFilter = (int)SDSFleet.SuperShuttle;
					else if (outboundSegment.RequestedFleet == SDSFleet.SuperShuttleSharedRideOnly)
					{
						fleetFilter = (int)SDSFleet.SuperShuttle;
					}
					else
						fleetFilter = (int)SDSFleet.All;
				}
				#endregion

				//#region DiscountId
				//if (!string.IsNullOrWhiteSpace(discountCode))
				//{
				//	discountId = groupsController.GetDiscountId(discountCode);
				//}

				//#endregion

				#region Inbound parameters
				if (inboundSegment != null)
				{
					airportCode = inboundSegment.Locations.First().type == DTO.Enum.LocationType.Airport ? inboundSegment.Locations.First().AiportCode : inboundSegment.Locations.Last().AiportCode;
                    inboundDate = inboundSegment.FlightDateTime == null ?  inboundSegment.RequestPickupTime : inboundSegment.FlightDateTime ;
					postalCode = inboundSegment.Locations.First().type == DTO.Enum.LocationType.Address ? inboundSegment.Locations.First().PostalCode : inboundSegment.Locations.Last().PostalCode;
					inboundFranchiseCode = franchiseController.GetFranchiseCode(airportCode, postalCode, (int)TripDirection.Inbound);
					payingPax = inboundSegment.PayingPax;
					childPax = inboundSegment.ChildSeats;
					inboundLocation = inboundSegment.Locations.First().type == DTO.Enum.LocationType.Address ? inboundSegment.Locations.First() : inboundSegment.Locations.Last();
					tripDirection = TripDirection.Inbound;
				}
				#endregion

				#region Outbound parameters
				if (outboundSegment != null)
				{
					airportCode = outboundSegment.Locations.First().type == DTO.Enum.LocationType.Airport ? outboundSegment.Locations.First().AiportCode : outboundSegment.Locations.Last().AiportCode;
                    outboundDate = outboundSegment.FlightDateTime==null ?  outboundSegment.RequestPickupTime : outboundSegment.FlightDateTime ;
					postalCode = outboundSegment.Locations.First().type == DTO.Enum.LocationType.Address ? outboundSegment.Locations.First().PostalCode : outboundSegment.Locations.Last().PostalCode;
					outboundFranchiseCode = franchiseController.GetFranchiseCode(airportCode, postalCode, (int)TripDirection.Outbound);
					payingPax = outboundSegment.PayingPax;
					childPax = outboundSegment.ChildSeats;
					outboundLocation = outboundSegment.Locations.First().type == DTO.Enum.LocationType.Address ? outboundSegment.Locations.First() : outboundSegment.Locations.Last();
					tripDirection = TripDirection.Outbound;
				}
				#endregion

				#region Is Airport Serviced Online
				utilityController.IsAirportServicedOnline(airportCode);
				#endregion

				#region GeoCode the addresses
				//Inbound
				if (inboundLocation != null)
				{
					if (inboundLocation.Latitude == 0 || inboundLocation.Longitude == 0)
					{
						geocoderController.ModifyAddress(inboundLocation, out inboundLocationModified);
					}
                    latitude = inboundLocation.Latitude; // Assigning the Latitude
                    longitude = inboundLocation.Longitude; // Assigning the Longitude
				}

				//Outbound
				if (outboundLocation != null)
				{
					if (outboundLocation.Latitude == 0 || outboundLocation.Longitude == 0)
					{
						geocoderController.ModifyAddress(outboundLocation, out outboundLocationModified);
					}
                    latitude = outboundLocation.Latitude; // Assigning the Latitude
                    longitude = outboundLocation.Longitude; // Assigning the Longitude
				}

				#endregion

				#region landmarkRecord
				if (inboundSegment != null)
				{
					if (inboundLocation.Latitude == 0 || inboundLocation.Longitude == 0)
					{
						landmarkRecord = landmarksController.GetLandmarkRecordByAddress(airportCode, inboundLocation);
					}
					else
					{
						landmarkRecord = landmarksController.GetLandmarksByCoordinates(airportCode, inboundLocation);
					}
				}
				else
				{
					if (outboundLocation.Latitude == 0 || outboundLocation.Longitude == 0)
					{
						landmarkRecord = landmarksController.GetLandmarkRecordByAddress(airportCode, outboundLocation);
					}
					else
					{
						landmarkRecord = landmarksController.GetLandmarksByCoordinates(airportCode, outboundLocation);
					}
				}

				masterLandmarkID = landmarkRecord.ToMasterLandmarkID();
				subZipName = landmarkRecord.ToSubZipName();
				locationType = landmarkRecord.ToLocationType();
				locationName = landmarkRecord.ToLocationName();
                #region Override LocationName with the GroundBook LocationName
                if (string.IsNullOrEmpty(locationName) && string.IsNullOrWhiteSpace(locationName))
                {
                    if (inboundSegment != null)
                    {
                            if (inboundLocation.Latitude != 0 || inboundLocation.Longitude != 0)
                            {
                                if (!string.IsNullOrEmpty(inboundLocation.PropertyName))
                                {
                                    locationName = inboundLocation.PropertyName;
                                }
                            }
                    }
                    else
                    {
                            if (outboundLocation.Latitude != 0 || outboundLocation.Longitude != 0)
                            {
                                if (!string.IsNullOrEmpty(outboundLocation.PropertyName))
                                {
                                    locationName = outboundLocation.PropertyName;
                                }
                        }
                    }

                } 
                #endregion
				#endregion

				#region ReservationType
				if (landmarkRecord.ToMasterLandmarkID() > 0)
					reservationType = ReservationType.AirportReservationForLandmark;
				else
					reservationType = ReservationType.AirportReservationForZipcode;
				#endregion

				#region isRoundTrip
				if (inboundSegment != null && outboundSegment != null)
				{
					isRoundTrip = true;
					tripDirection = TripDirection.RoundTrip;
				}
				#endregion

				#region Update location's masterlandmarkID, locationType, locationName and subzipname
				foreach (var segment in segments)
				{
					foreach (var location in segment.Locations)
					{
						if (location.type == DTO.Enum.LocationType.Address)
						{
							location.SubZipName = subZipName;
							location.LocationType = locationType;
							location.PropertyName = locationName;
							location.MasterLandmarkID = masterLandmarkID;
						}
					}
				}
				#endregion

				#region GetRate
				if (reservationType == ReservationType.AirportReservationForLandmark)
				{
					//var serviceDetails = getServiceRates(airportCode, fleetFilter, inboundFranchiseCode, outboundFranchiseCode, inboundDate, outboundDate, isInboundOrigin, isRoundTrip, masterLandmarkID, postalCode, subZipName, payingPax, discountCode);
					var serviceDetails = rateAdapter.GetServiceRates(airportCode, fleetFilter, inboundFranchiseCode, outboundFranchiseCode, inboundDate, outboundDate, isInboundOrigin, isRoundTrip, masterLandmarkID, postalCode, subZipName, payingPax, discountCode, gratuityRate, bookingFee, directBillAccountId, vehicleCode, currency,latitude,longitude, IsAccessible,IsInboundRideNow,IsOutboundRideNow,UserId,RemoveCallCenterBookingFee, CultureCode);

                    rateServiceInfoResults = serviceDetails.SelectMany(s => s.Services).Distinct().ToList();
				}
				else if (reservationType == ReservationType.AirportReservationForZipcode)
				{
					var splitNames = zipCodeController.GetSplitNamesOfServicedPostalCode(airportCode, postalCode, (int)tripDirection, out isZipSplit);
					if (splitNames != null && splitNames.Any())
					{
						foreach (var splitName in splitNames)
						{
							//var serviceDetails = getServiceRates(airportCode, fleetFilter, inboundFranchiseCode, outboundFranchiseCode, inboundDate, outboundDate, isInboundOrigin, isRoundTrip, masterLandmarkID, postalCode, splitName.SubZipName, payingPax, discountCode);
							var serviceDetails = rateAdapter.GetServiceRates(airportCode, fleetFilter, inboundFranchiseCode, outboundFranchiseCode, inboundDate, outboundDate, isInboundOrigin, isRoundTrip, masterLandmarkID, postalCode, splitName.SubZipName, payingPax, discountCode, gratuityRate, bookingFee, directBillAccountId, vehicleCode, currency,latitude,longitude, IsAccessible, IsInboundRideNow, IsOutboundRideNow, UserId,RemoveCallCenterBookingFee, CultureCode);
							//Dictionary<int, ServiceInfo> serviceDetails2 = rateAdapter.GetServiceRates(airportCode, fleetFilter, inboundFranchiseCode, outboundFranchiseCode, inboundDate, outboundDate, isInboundOrigin, isRoundTrip, masterLandmarkID, postalCode, splitName.SubZipName, payingPax, discountCode, gratuityRate);
							var rateServiceInfoResult = serviceDetails.SelectMany(s => s.Services).Distinct().ToList();
							rateServiceInfoResult.ForEach(s => s.SplitServiced = splitName.Serviced);
							rateServiceInfoResults.AddRange(rateServiceInfoResult);
							//var xmltest = rateServiceInfoResults.XmlSerialize();
						}
					}

					#region Split Logic
					if (isZipSplit)
					{
						var totalSplitCount = splitNames.Count();
						var totalServicedSplitCount = splitNames.Where(s => s.Serviced).Count();
						if (totalSplitCount == totalServicedSplitCount)
						{
							if (userInfo.ShowSplits)
							{
								#region userInfo.ShowSplits= true and All Serviced => Retrun All Serviced
								//no filter needed 
								#endregion
							}
							else
							{
								#region userInfo.ShowSplits= false and All Serviced => Retrun cheapest ones
								//var rateServiceInfoResults111 = rateServiceInfoResults.GroupBy(s => new { s.FleetType, s.ServiceType, s.Direction }).ToList();

								rateServiceInfoResults = rateServiceInfoResults.GroupBy(s => new { s.FleetType, s.ServiceType, s.Direction }).Select(s => s.OrderBy(r => r.FirstPassengerFare + r.AdditionalPassengerFare * (payingPax - 1))).Select(s => s.First()).ToList();
								#endregion
							}
						}
						else
						{
							#region Not all splits serviced, but at least one is serviced
							if (totalServicedSplitCount > 0)
							{
								if (userInfo.ShowSplits)
								{
									#region userInfo.ShowSplits= true but not all serviced => Retrun All Serviced
									//no filter needed
									#endregion
								}
								else
								{
									if (userInfo.AllowClosedSplits)
									{
										#region userInfo.ShowSplits= false and userInfo.AllowClosedSplits = true,  Retrun cheapest serviced ones
										rateServiceInfoResults = rateServiceInfoResults.Where(s => s.SplitServiced).GroupBy(s => new { s.FleetType, s.ServiceType, s.Direction }).Select(s => s.OrderBy(r => r.FirstPassengerFare + r.AdditionalPassengerFare * (payingPax - 1))).Select(s => s.First()).ToList();
										#endregion
									}
									else
									{
										#region userInfo.ShowSplits= false and userInfo.AllowClosedSplits = false,  Retrun nothing. Like the whose Zipcode (not split) is not serviced
										// Fileter => Retrun nothing. Like the whose Zipcode (not split) is not serviced
										rateServiceInfoResults.RemoveAll(s => true);
										#endregion
									}
								}
							}
							#endregion
						}
					}
					#endregion
				}
				#endregion

				#region Filter rate
				#region for Meet and Greet
				if (meetAndGreet.HasValue && meetAndGreet.Value == true)
				{
					if (rateServiceInfoResults != null || rateServiceInfoResults.Any())
					{
						rateServiceInfoResults = rateServiceInfoResults.Where(s => s.FleetType == "ECAR" && s.ServiceType == "EMG").ToList();
					}
				}
				#endregion

				#region for max passenger#
				if (payingPax > 0)
				{
					if (rateServiceInfoResults != null || rateServiceInfoResults.Any())
					{
						rateServiceInfoResults = rateServiceInfoResults.Where(s => s.MaxGuests >= payingPax + childPax).ToList();
					}
				}
				#endregion

				#endregion

				#region No Rate found
				if (rateServiceInfoResults == null || !rateServiceInfoResults.Any())
					throw VTOD.Common.DTO.VtodException.CreateException(ExceptionType.SDS, 1003); // new Exception(Messages.General_NoMatchingRate);
				#endregion

				#region Set RateResponse
				//foreach (var segment in segments)
				//{
				//var rateResponse = new RateResponse();
				//rateResponse.Segment = segment;
				//result.BookRequestID = bookRequestID;
				//rateResponse.TripDirection = segment.TripDirection;

				result.ServiceInfoRates = new List<ServiceInfo>();
				//foreach (var serviceInfo in rateServiceInfoResults.Where(s => s.Direction.ToString().ToLower() == segment.TripDirection.ToString().ToLower()))
				foreach (var serviceInfo in rateServiceInfoResults)//.Where(s => s.Direction.ToString().ToLower() == segment.TripDirection.ToString().ToLower()))
				{
					//result.Segment = segments.Where(s => s.TripDirection.ToString().ToLower().Trim() == serviceInfo.Direction.ToString().ToLower().Trim()).FirstOrDefault();
					result.ServiceInfoRates.Add(serviceInfo);
				}

				#region Filter
				if (subfleetPermission != null && subfleetPermission.Any())
				{
					var allows = subfleetPermission.Where(s => s.Value == true).Select(s => s.Key.Trim().ToLower()).ToList();
					var rejects = subfleetPermission.Where(s => s.Value == false).Select(s => s.Key.Trim().ToLower()).ToList();

					#region filter allows
					if (allows != null && allows.Any())
					{
						if (!allows.Where(s => s.Trim().ToLower() == "all").Any())
						{
							result.ServiceInfoRates = result.ServiceInfoRates.Where(s => allows.Contains(s.SubFleet.Trim().ToLower())).ToList();
						}
					}
					#endregion

					#region filter rejects
					if (rejects != null && rejects.Any())
					{
						result.ServiceInfoRates = result.ServiceInfoRates.Where(s => !rejects.Contains(s.SubFleet.Trim().ToLower())).ToList();
					}
					#endregion
				}
				#endregion

				#region Order By
				//rateResponse.ServiceInfoRates = rateResponse.ServiceInfoRates.OrderBy(r => r.SubFleetForOrdering).ThenBy(r => r.SubFleet).ThenBy(r => r.FirstPassengerFare + r.AdditionalPassengerFare * (payingPax - 1)).ToList();
				//This is for zTrip user. In future we may need to do ordering for other users
				//result.ServiceInfoRates = result.ServiceInfoRates.OrderBy(r => r.SubFleetForOrdering).ThenBy(r => r.FareForOrdering).ThenBy(r => r.TripDirectionForOrdering).ToList();
				#endregion

				//result.Add(rateResponse);
				//}
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public List<RatesService.CharterServiceTypeRecord> GetLocationToLocationRate(List<Segment> segments, Dictionary<string, bool> subfleetPermission, string discountCode, bool? pickMeUpNow, bool? meetAndGreet, decimal gratuityRate, UDI.VTOD.Common.DTO.Enum.Currency currency)
		{
			#region Init
			UDI.SDS.RatesService.SecurityToken rateToken = _token.ToRatesServiceToken();
			var rateAdapter = new RateServiceAdapter(rateToken, _trackTime);
			var utilityController = new UtilityController(_token, _trackTime);
			var routingController = new RoutingController(_token, _trackTime);
			var geocodeController = new GeocoderController(_token, _trackTime);
			var geocoderController = new GeocoderController(_token, _trackTime);
			#endregion

			try
			{
				#region Init
				var msg = string.Empty;
				var result = new List<RatesService.CharterServiceTypeRecord>();
				var request = new RatesService.PointToPointCharterRateRequest();				
				Segment segment = null;
				
				#endregion

				#region Segment
				if (segments != null && segments.Any())
				{
					segment = segments.First();
				}
				#endregion

				#region fleetFilter
				if (segment != null)
				{
					if (segment.RequestedFleet == SDSFleet.ExecuCar || segment.RequestedFleet == SDSFleet.SuperShuttle || segment.RequestedFleet == SDSFleet.All)
						request.FleetFilter = (int)segment.RequestedFleet;
					else if (segment.RequestedFleet == SDSFleet.Taxi)
						request.FleetFilter = (int)segment.RequestedFleet;
					else //(segment.RequestedFleet == Fleet.SuperShuttleSharedRideOnly || segment.RequestedFleet == Fleet.All)
					{
						throw VTOD.Common.DTO.VtodException.CreateException(ExceptionType.SDS, 1007);// new Exception(Messages.UDI_SDS_ShareRide_LocationToLocation);
					}
				}
				#endregion

				#region Set Request Properties
				#region Pickup
				try
				{
					request.PickupZipCode = segment.Locations.Where(s => s.mode == LocationMode.Pickup).First().PostalCode;
				}
				catch 
				{}
				#endregion

				#region Dropoff
				try
				{
					request.DropoffZipCode = segment.Locations.Where(s => s.mode == LocationMode.Dropoff).First().PostalCode;
				}
				catch 
				{}
				#endregion

				//request.PickupLandmarkID = location.masterLandmarkID;
				#endregion

				#region GeoCode the addresses
				foreach (var location in segment.Locations)
				{
					if (location != null)
					{
						if (location.Latitude == 0 || location.Longitude == 0)
						{
							var modified = false;
							geocoderController.ModifyAddress(location, out modified);
						}
					}
				}
				#endregion

				#region Get Charter Location ID
				request.CharterLocationsID = utilityController.GetCharterLocationIdByPostalCode((int)CharterType.PointToPoint, request.PickupZipCode);
				#endregion

				#region Get Distance
				if (segment.Locations != null)
				{
					if (segment.Locations.Count() == 2 && segment.Locations.Where(s => s != null).Count() == 2)
					{
						var startPoint = new RoutingService.Point();
						var endPoint = new RoutingService.Point();

						startPoint.Latitude = segment.Locations.First().Latitude.ToDouble();
						startPoint.Longitude = segment.Locations.First().Longitude.ToDouble();

						endPoint.Latitude = segment.Locations.Last().Latitude.ToDouble();
						endPoint.Longitude = segment.Locations.Last().Longitude.ToDouble();
						if (startPoint.Latitude != 0 && startPoint.Longitude != 0 && endPoint.Latitude != 0 && endPoint.Longitude != 0)
						{
							request.Distance = routingController.GetDistanceBetweenPoints(startPoint, endPoint);
						}
					}
					else if (segment.Locations.Count() == 1 && segment.Locations.Where(s => s != null).Count() == 1)
					{
						request.DropOffAsDirected = true;
					}
				}
				#endregion

				#region PicMeUpNow
				if (pickMeUpNow.HasValue)
				{
					request.PickMeUpNow = pickMeUpNow.Value;
				}
				#endregion

				#region TripType
				//var tripType = TripType.Unknown;
				//if (segment.Locations.Where(s => s != null).Count() == 1)
				//{
				//	tripType = TripType.AsDirected;
				//}
				//else if (segment.Locations.Where(s => s != null).Count() == 2)
				//{
				//	tripType = TripType.LocationToLocation;
				//}

				//if (tripType == TripType.AsDirected)
				//{
				//	request.DropOffAsDirected = true;
				//}
				#endregion

				#region Landmark
				//request.PickupLandmarkID = segment.Locations.First().LandmarkID;
				//if (segment.Locations.Count() == 2)
				//	request.DropoffLandmarkID = segment.Locations.Last().LandmarkID;
				request.PickupLandmarkID = segment.Locations.First().MasterLandmarkID;
				if (segment.Locations.Count() == 2)
					request.DropoffLandmarkID = segment.Locations.Last().MasterLandmarkID;
				#endregion

				#region Gratuity
				request.GratuityRate = gratuityRate;
				#endregion

				#region Currency
				request.CurrencyID = (int)currency;
                #endregion
                logger.InfoFormat("GetPointToPointCharterRates Start...\r\n {0}", request.XmlSerialize());

				//using (var client = new RatesService.RatesServiceClient())
				//{
				var rates = rateAdapter.GetPointToPointCharterRates(request);

				//var xml =  rates.XmlSerialize();

				#region Filter rate (for Meet and Greet)
				if (meetAndGreet.HasValue && meetAndGreet.Value == true)
				{
					if (rates != null || rates.Any())
					{
						rates = rates.Where(s => s.Fleet == "ECAR" && s.ServiceTypeCode == "EMG").ToList();
					}
				}
				#endregion

				#region No Rate found
				if (rates == null || !rates.Any())
					throw VTOD.Common.DTO.VtodException.CreateException(ExceptionType.SDS, 1003); // new Exception(Messages.General_NoMatchingRate);
				#endregion

				#region Set RateResponse
				//Check maximum PayingPax and maximum wheelchairs, order by the lowest Fare
				if (rates.Count > 0)
				{


					#region Filter
					if (rates != null && rates.Any())
					{
						var allows = subfleetPermission.Where(s => s.Value == true).Select(s => s.Key.Trim().ToLower()).ToList();
						var rejects = subfleetPermission.Where(s => s.Value == false).Select(s => s.Key.Trim().ToLower()).ToList();

						#region filter allows
						if (allows != null && allows.Any())
						{
							if (!allows.Where(s => s.Trim().ToLower() == "all").Any())
							{
								rates = rates.Where(s => allows.Contains(s.SubFleet.Trim().ToLower())).ToList();
							}
						}
						#endregion

						#region filter rejects
						if (rejects != null && rejects.Any())
						{
							rates = rates.Where(s => !rejects.Contains(s.SubFleet.Trim().ToLower())).ToList();
						}
						#endregion
					}
					#endregion

					#region Order By
					//rates = rates.OrderBy(r => r.SubFleetOrderID).ThenBy(r => r.SubFleet).ThenBy(r => r.Fare).ToList();
					#endregion


					//rates = rates.OrderBy(r => r.Fare).ToList();

					foreach (var rate in rates)
					{
						if (rate.MaxGuests >= (segment.PayingPax + segment.ChildSeats) && rate.MaxAccessibleGuests >= segment.Wheelchairs)
						{
							result.Add(rate);
							//break;
						}
					}
				}
				else
					throw VTOD.Common.DTO.VtodException.CreateException(ExceptionType.SDS, 1003); // new Exception(Messages.General_NoMatchingRate);
				#endregion

				//var xml = result.XmlSerialize();

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call RatesService.GetPointToPointCharterRates {0}", ex.StackTrace);
				throw ex;
			}
		}

		public List<RatesService.CharterServiceTypeRecord> GetHourlyRate(SDSFleet fleetType, string postalCode, int totalMinutes, int payingPax, int wheelChairs, int freePax)
		{
			#region Init
			UDI.SDS.RatesService.SecurityToken rateToken = _token.ToRatesServiceToken();
			var landmarkAdapter = new RateServiceAdapter(rateToken, _trackTime);
			#endregion

			#region Initial variables
			List<CharterServiceTypeRecord> result = null;
			#endregion

			#region Set Fleet Filter
			int fleetFilter;
			if (fleetType == SDSFleet.ExecuCar)
				fleetFilter = (int)SDSFleet.ExecuCar;
			else if (fleetType == SDSFleet.SuperShuttle)
				fleetFilter = (int)SDSFleet.SuperShuttle;
			else if (fleetType == SDSFleet.SuperShuttleSharedRideOnly)
			{
				fleetFilter = (int)SDSFleet.SuperShuttle;
			}
			else
				fleetFilter = (int)SDSFleet.All;
			#endregion

			#region Get Rates
			/********* Hourly Charter *********/
			var rates = landmarkAdapter.GetHourlyCharterRates(fleetFilter, postalCode, totalMinutes /*, segment.PayingPax, segment.Wheelchairs, segment.ChildSeats*/);
			#endregion

			#region No Rate found
			if (rates == null || !rates.Any())
				throw VTOD.Common.DTO.VtodException.CreateException(ExceptionType.SDS, 1003); // new Exception(Messages.General_NoMatchingRate);
			#endregion

			#region Set RateResponse
			//Check maximum PayingPax and maximum wheelchairs, order by the lowest Fare
			if (rates.Count > 0)
			{
				result = new List<CharterServiceTypeRecord>();
				foreach (var rate in rates)
				{
					if (rate.MaxGuests >= (payingPax + freePax) && rate.MaxAccessibleGuests >= wheelChairs)
					{
						result.Add(rate);
						//break;
					}
				}
			}
			else
				throw VTOD.Common.DTO.VtodException.CreateException(ExceptionType.SDS, 1003); // new Exception(Messages.General_NoMatchingRate);
			#endregion
			return result;
		}

		public FareScheduleWithFees GetFareScheduleWithFees(FareScheduleRequestWithFees fareScheduleRequestWithFees)
		{
			#region Init
			UDI.SDS.RatesService.SecurityToken token = _token.ToRatesServiceToken();
			var rateAdapter = new RateServiceAdapter(token, _trackTime);
			#endregion

			try
			{

				return rateAdapter.GetFareScheduleWithFees(fareScheduleRequestWithFees);

			}
			catch (Exception ex)
			{
				logger.ErrorFormat("GetFareScheduleWithFees {0}", ex.StackTrace);
				throw ex;
			}
		}

		#endregion

		#region Old
		///// <summary>
		///// New method for handling both one way and also round trip
		///// </summary>
		//private List<ServiceDetail> getServiceRates(string airportCode, int fleetFilter, string inboundFranchiseCode, string outboundFranchiseCode, DateTime? inboundDate, DateTime? outboundDate, bool isInboundOrigin, bool isRoundTrip, int masterLandmarkID, string postalCode, string subZipName, int payingPax, string discountCode)
		//{
		//	#region Init
		//	UDI.SDS.RatesService.SecurityToken rateToken = _token.ToRatesServiceToken();
		//	var rateAdapter = new RateServiceAdapter(rateToken, _trackTime);
		//	#endregion

		//	var result = rateAdapter.GetServiceRates(airportCode, fleetFilter, inboundFranchiseCode, outboundFranchiseCode, inboundDate, outboundDate, isInboundOrigin, isRoundTrip, masterLandmarkID, postalCode, subZipName, payingPax, discountCode);

		//	return result;
		//}

		/// <summary>
		/// Get the lowest rate for a segment from SuperShuttle
		/// </summary>
		//public RatesResponse GetRatePerSegment(Guid bookRequestID, Segment segment, string discountCode, decimal gratuity)
		//{
		//	#region Init
		//	UDI.SDS.LandmarksService.SecurityToken landmarkToken = _token.ToLandmarksServiceToken();
		//	var landmarkAdapter = new LandmarksServiceServiceAdapter(landmarkToken, _trackTime);

		//	var utilityController = new UtilityController(_token, _trackTime);
		//	#endregion

		//	try
		//	{
		//		#region Initial variables
		//		discountCode = discountCode.Trim();
		//		this.bookRequestID = bookRequestID;
		//		RatesResponse response = new RatesResponse();

		//		var tripDirection = TripDirection.Unknown;
		//		var reservationType = ReservationType.Unknown;
		//		List<RatesService.ServiceTypeRecord> airportRate = null;
		//		List<RatesService.CharterServiceTypeRecord> charterRate = null;
		//		UDI.SDS.DTO.Location pickupLocation = null;
		//		UDI.SDS.DTO.Location dropoffLocation = null;
		//		int masterLandmarkID = (int)UDI.SDS.DTO.Enum.Defaults.MasterLandmarkID;
		//		bool isSuperShuttleSharedRideOnly = false;//If true, then only return Fleet = VAN.
		//		#endregion

		//		#region Set Fleet Filter
		//		int fleetFilter;
		//		if (segment.RequestedFleet == SDSFleet.ExecuCar)
		//			fleetFilter = (int)SDSFleet.ExecuCar;
		//		else if (segment.RequestedFleet == SDSFleet.SuperShuttle)
		//			fleetFilter = (int)SDSFleet.SuperShuttle;
		//		else if (segment.RequestedFleet == SDSFleet.SuperShuttleSharedRideOnly)
		//		{
		//			fleetFilter = (int)SDSFleet.SuperShuttle;
		//			isSuperShuttleSharedRideOnly = true;
		//		}
		//		else
		//			fleetFilter = (int)SDSFleet.All;
		//		#endregion

		//		#region Set pickupLocation, droppffLocation
		//		foreach (var location in segment.Locations)
		//		{
		//			UDI.SDS.Helper.Utility.CheckLocationRequiredFields(location);
		//			if (location.mode == LocationMode.Pickup)
		//			{
		//				pickupLocation = new UDI.SDS.DTO.Location(location.type, location.mode);
		//				pickupLocation = location;
		//			}
		//			else
		//			{
		//				dropoffLocation = new UDI.SDS.DTO.Location(location.type, location.mode);
		//				dropoffLocation = location;
		//			}
		//		}
		//		#endregion

		//		#region Get Rates
		//		/********* Hourly Charter *********/
		//		if (dropoffLocation.type == UDI.SDS.DTO.Enum.LocationType.AsDirected)
		//		{
		//			reservationType = ReservationType.HourlyCharter;
		//			charterRate = new List<RatesService.CharterServiceTypeRecord>();
		//			charterRate = getHourlyCharterRates(fleetFilter, pickupLocation.PostalCode, segment.CharterMinutes, segment.PayingPax, segment.Wheelchairs, segment.ChildSeats);

		//			if (charterRate != null && charterRate.FirstOrDefault() != null)
		//			{
		//				var landmarkRecord = landmarkAdapter.GetLandmarkRecordByAddress(charterRate.FirstOrDefault().FranchiseCode, pickupLocation);
		//				pickupLocation.MasterLandmarkID = landmarkRecord.ToMasterLandmarkID();
		//				pickupLocation.SubZipName = landmarkRecord.ToSubZipName();
		//				pickupLocation.PropertyName = landmarkRecord.ToLocationName();

		//				if (string.IsNullOrEmpty(pickupLocation.LocationType))
		//					pickupLocation.LocationType = landmarkRecord.ToLocationType();
		//			}
		//		}
		//		/********* Airport Reservation - Outbound *********/
		//		else if (pickupLocation.type == UDI.SDS.DTO.Enum.LocationType.Airport)
		//		{
		//			//if (segment.IsRoundTrip)
		//			//{
		//			//	tripDirection = TripDirection.RoundTrip;
		//			//}
		//			//else
		//			{
		//				if (tripDirection == TripDirection.Unknown)
		//					tripDirection = TripDirection.Outbound; //From airport 
		//			}

		//			utilityController.IsAirportServicedOnline(pickupLocation.AiportCode);

		//			//1. Check if dropoff location is Airport too, then treat it as address
		//			if (dropoffLocation.type == UDI.SDS.DTO.Enum.LocationType.Airport)
		//			{
		//				dropoffLocation.type = UDI.SDS.DTO.Enum.LocationType.Address;
		//				UDI.SDS.Helper.Utility.CheckLocationRequiredFields(dropoffLocation);
		//			}

		//			//2.Check it's Airport to Landmark or Zipcode
		//			if (dropoffLocation.type == UDI.SDS.DTO.Enum.LocationType.Address)
		//			{
		//				var landmarkRecord = landmarkAdapter.GetLandmarkRecordByAddress(pickupLocation.AiportCode, dropoffLocation);
		//				dropoffLocation.MasterLandmarkID = landmarkRecord.ToMasterLandmarkID();
		//				dropoffLocation.SubZipName = landmarkRecord.ToSubZipName();
		//				dropoffLocation.LocationType = landmarkRecord.ToLocationType();
		//				dropoffLocation.PropertyName = landmarkRecord.ToLocationName();

		//				if (dropoffLocation.MasterLandmarkID != -2147483648)
		//				{
		//					masterLandmarkID = dropoffLocation.MasterLandmarkID;
		//					reservationType = ReservationType.AirportReservationForLandmark;
		//				}
		//				else
		//					reservationType = ReservationType.AirportReservationForZipcode;
		//			}

		//			airportRate = new List<RatesService.ServiceTypeRecord>();
		//			airportRate = getAirportReservationRates(pickupLocation.AiportCode, dropoffLocation, (int)tripDirection, reservationType, fleetFilter, segment.PayingPax, segment.Wheelchairs, segment.ChildSeats, discountCode, isSuperShuttleSharedRideOnly);
		//		}
		//		/********* Airport Reservation - Inbound *********/
		//		else if (dropoffLocation.type == UDI.SDS.DTO.Enum.LocationType.Airport)
		//		{
		//			//if (segment.IsRoundTrip)
		//			//{
		//			//	tripDirection = TripDirection.RoundTrip;
		//			//}
		//			//else
		//			{
		//				if (tripDirection == TripDirection.Unknown)
		//					tripDirection = TripDirection.Inbound; //To airport 
		//			}

		//			utilityController.IsAirportServicedOnline(dropoffLocation.AiportCode);

		//			//Check it's Landmark or Zipcode to Airport
		//			if (pickupLocation.type == UDI.SDS.DTO.Enum.LocationType.Address)
		//			{
		//				var landmarkRecord = landmarkAdapter.GetLandmarkRecordByAddress(dropoffLocation.AiportCode, pickupLocation);
		//				pickupLocation.MasterLandmarkID = landmarkRecord.ToMasterLandmarkID();
		//				pickupLocation.SubZipName = landmarkRecord.ToSubZipName();
		//				pickupLocation.LocationType = landmarkRecord.ToLocationType();
		//				pickupLocation.PropertyName = landmarkRecord.ToLocationName();

		//				if (pickupLocation.MasterLandmarkID != -2147483648)
		//				{
		//					masterLandmarkID = pickupLocation.MasterLandmarkID;
		//					reservationType = ReservationType.AirportReservationForLandmark;
		//				}
		//				else
		//					reservationType = ReservationType.AirportReservationForZipcode;
		//			}

		//			airportRate = new List<RatesService.ServiceTypeRecord>();
		//			airportRate = getAirportReservationRates(dropoffLocation.AiportCode, pickupLocation, (int)tripDirection, reservationType, fleetFilter, segment.PayingPax, segment.Wheelchairs, segment.ChildSeats, discountCode, isSuperShuttleSharedRideOnly);
		//		}
		//		/********* Point to Point Charter *********/
		//		else
		//		{
		//			reservationType = ReservationType.PointToPointCharter;
		//			charterRate = new List<RatesService.CharterServiceTypeRecord>();
		//			charterRate = getPointToPointCharterRates(fleetFilter, segment.Locations.ToList(), segment.PayingPax, segment.Wheelchairs, segment.ChildSeats, gratuity);

		//			if (charterRate != null && charterRate.FirstOrDefault() != null)
		//			{
		//				var landmarkRecord = landmarkAdapter.GetLandmarkRecordByAddress(charterRate.FirstOrDefault().FranchiseCode, pickupLocation);
		//				pickupLocation.MasterLandmarkID = landmarkRecord.ToMasterLandmarkID();
		//				pickupLocation.SubZipName = landmarkRecord.ToSubZipName();
		//				pickupLocation.LocationType = landmarkRecord.ToLocationType();
		//				pickupLocation.PropertyName = landmarkRecord.ToLocationName();

		//				var landmarkRecord2 = landmarkAdapter.GetLandmarkRecordByAddress(charterRate.FirstOrDefault().FranchiseCode, dropoffLocation);
		//				dropoffLocation.MasterLandmarkID = landmarkRecord2.ToMasterLandmarkID();
		//				dropoffLocation.SubZipName = landmarkRecord2.ToSubZipName();
		//				dropoffLocation.PropertyName = landmarkRecord.ToLocationName();

		//				if (string.IsNullOrEmpty(dropoffLocation.LocationType))
		//					dropoffLocation.LocationType = landmarkRecord2.ToLocationType();
		//			}
		//		}

		//		#endregion

		//		#region Update location's masterlandmarkID, locationType, locationName and subzipname
		//		for (int i = 0; i < segment.Locations.Count; i++)
		//		{
		//			if (segment.Locations[i].mode == LocationMode.Pickup)
		//			{
		//				segment.Locations[i] = pickupLocation;
		//			}
		//			else
		//			{
		//				segment.Locations[i] = dropoffLocation;
		//			}
		//		}
		//		#endregion

		//		#region Set RateResponse
		//		response.BookRequestID = bookRequestID;
		//		response.AirportReservationRates = airportRate;
		//		response.CharterRates = charterRate;
		//		response.AirportReservationRate = airportRate != null ? airportRate.FirstOrDefault() : null;
		//		response.CharterRate = charterRate != null ? charterRate.FirstOrDefault() : null;
		//		response.TripDirection = tripDirection;
		//		response.Segment = segment;
		//		#endregion

		//		return response;
		//	}
		//	catch (Exception ex)
		//	{
		//		throw ex;
		//	}
		//}

		//private RatesService.FareScheduleWithFees getFees(Guid bookRequestID, RatesService.ServiceTypeRecord airportFare, RatesService.CharterServiceTypeRecord charterFare, TripDirection tripDirection, bool isRoundTrip, int payingPax, byte paymentType, DateTime pickupTime, string discountCode)
		//{
		//	#region Init
		//	UDI.SDS.RatesService.SecurityToken rateToken = _token.ToRatesServiceToken();
		//	var rateAdapter = new RateServiceAdapter(rateToken, _trackTime);
		//	#endregion

		//	var result = rateAdapter.GetFees(bookRequestID, airportFare, charterFare, tripDirection, isRoundTrip, payingPax, paymentType, pickupTime, discountCode);

		//	return result;
		//}

		/// <summary>
		/// Get all rates for Airport Reservation.
		/// If no matching record found, return NULL.
		/// </summary>
		//private List<RatesService.ServiceTypeRecord> getAirportReservationRates(string airportCode, UDI.SDS.DTO.Location addressLocation, int tripDirection, ReservationType reservationType, int fleetFilter, int payingPax, int wheelchairs, int FreePax, string discountCode, bool isSuperShuttleSharedRideOnly)
		//{
		//	#region Init
		//	UDI.SDS.RatesService.SecurityToken rateToken = _token.ToRatesServiceToken();
		//	var rateAdapter = new RateServiceAdapter(rateToken, _trackTime);

		//	var franchiseController = new FranchiseController(_token, _trackTime);

		//	var groupsController = new GroupsController(_token, _trackTime);

		//	UDI.SDS.ZipCodesService.SecurityToken zipcodesToken = _token.ToZipCodesServiceToken();
		//	var zipcodesAdapter = new ZipCodesController(_token, _trackTime);
		//	#endregion

		//	List<RatesService.ServiceTypeRecord> airportRate = new List<RatesService.ServiceTypeRecord>();
		//	//var result = new RatesService.ServiceTypes();
		//	var results = new List<RateServiceTypeResult>();
		//	var franchiseCode = franchiseController.GetFranchiseCode(airportCode, addressLocation.PostalCode, tripDirection);
		//	var isZipSplit = false;

		//	logger.InfoFormat("GetAirportReservationRates Start...\r\n AirportCode:{0},Address:{1},tripDirection:{2},reservationType:{3},fleetFilter:{4},payingPax:{5},wheelchairs:{6}, franchiseCode:{7}", airportCode, addressLocation.XmlSerialize(), tripDirection, reservationType, fleetFilter, payingPax, wheelchairs, franchiseCode);

		//	#region Get DiscoundId from DiscountCode
		//	var discountId = 0;

		//	if (!string.IsNullOrWhiteSpace(discountCode))
		//	{
		//		discountId = groupsController.GetDiscountId(discountCode);
		//	}
		//	#endregion

		//	#region Get all rates
		//	//GetLandmarkFares
		//	if (reservationType == ReservationType.AirportReservationForLandmark)
		//	{
		//		/* Get fares for airport reservation with landmark */
		//		//result = GetLandmarkRates(airportCode, fleetFilter, franchiseCode, addressLocation.masterLandmarkID, tripDirection, addressLocation.PostalCode, payingPax, discountCode);

		//		var landMarkResult = rateAdapter.GetLandmarkRates(airportCode, fleetFilter, franchiseCode, addressLocation.MasterLandmarkID, tripDirection, addressLocation.PostalCode, payingPax, discountId);
		//		if (landMarkResult != null && landMarkResult.HasRecords)
		//		{
		//			results.Add(new RateServiceTypeResult { ReservationType = ReservationType.AirportReservationForLandmark, ServiceTypes = landMarkResult });
		//		}
		//	}
		//	//GetZipCodeFares
		//	else if (reservationType == ReservationType.AirportReservationForZipcode)
		//	{
		//		List<ZipCodesService.ZipCodeSplitRecord> splitNames = new List<ZipCodesService.ZipCodeSplitRecord>();

		//		//get subZipName which is splitName
		//		splitNames = zipcodesAdapter.GetSplitNamesOfServicedPostalCode(airportCode, addressLocation.PostalCode, tripDirection, out isZipSplit);

		//		foreach (var ZipCodeSplitRecord in splitNames)
		//		{
		//			/* Get fares for each subZipName */
		//			var zipCodeSplitRecordResult = rateAdapter.GetZipCodeRates(airportCode, fleetFilter, franchiseCode, tripDirection, addressLocation.PostalCode, ZipCodeSplitRecord.SubZipName, payingPax, discountId);
		//			if (zipCodeSplitRecordResult != null)// && zipCodeSplitRecordResult.HasRecords)
		//			{
		//				results.Add(new RateServiceTypeResult { ReservationType = ReservationType.AirportReservationForZipcode, ServiceTypes = zipCodeSplitRecordResult, ZipCodeSplitRecord = ZipCodeSplitRecord });
		//			}


		//			//if (result != null && result.HasRecords)
		//			//	break;
		//		}
		//	}
		//	#endregion

		//	//Check maximum PayingPax and maximum wheelchairs, order by the lowest Rate
		//	//if (result.HasRecords)
		//	if (results != null && results.Any())
		//	{
		//		#region Split Logic
		//		if (reservationType == ReservationType.AirportReservationForLandmark || !isZipSplit)
		//		{
		//			#region No Splits => Get first one and then Return All
		//			var rates = new List<RatesService.ServiceTypeRecord>();

		//			if (payingPax <= 1) //Only compare FirstPersonFare
		//				rates = results.First().ServiceTypes.ServiceTypeRecord.OrderBy(r => r.FirstPersonFare).ToList();
		//			else //Compare calculated FirstPersonFare and AdditionalPersonFare
		//				rates = results.First().ServiceTypes.ServiceTypeRecord.OrderBy(r => r.FirstPersonFare + r.AdditionalPersonFare * (payingPax - 1)).ToList();

		//			//If SuperShuttleSharedRideOnly, only return Shared Ride Rates.
		//			if (isSuperShuttleSharedRideOnly)
		//				rates = rates.Where(r => r.ServiceTypeCode.ToLower() == "VAN".ToLower()).ToList();
		//			foreach (var rate in rates)
		//			{
		//				if (rate.MaxGuests >= (payingPax + FreePax) && rate.MaxAccessibleGuests >= wheelchairs)
		//				{
		//					airportRate.Add(rate);
		//				}
		//			}
		//			#endregion
		//		}
		//		else
		//		{
		//			var totalSplits = results.Count();
		//			var totalServicedSplits = results.Where(s => s.ZipCodeSplitRecord.Serviced == true).Count();
		//			#region All Serviced splits
		//			// just for ZipCodeSplitRecord.Serviced = true
		//			foreach (var result in results.Where(s => s.ZipCodeSplitRecord.Serviced && s.ServiceTypes.HasRecords))
		//			{
		//				var rates = new List<RatesService.ServiceTypeRecord>();

		//				if (payingPax <= 1) //Only compare FirstPersonFare
		//					rates = result.ServiceTypes.ServiceTypeRecord.OrderBy(r => r.FirstPersonFare).ToList();
		//				else //Compare calculated FirstPersonFare and AdditionalPersonFare
		//					rates = result.ServiceTypes.ServiceTypeRecord.OrderBy(r => r.FirstPersonFare + r.AdditionalPersonFare * (payingPax - 1)).ToList();

		//				//If SuperShuttleSharedRideOnly, only return Shared Ride Rates.
		//				if (isSuperShuttleSharedRideOnly)
		//					rates = rates.Where(r => r.ServiceTypeCode.ToLower() == "VAN".ToLower()).ToList();
		//				foreach (var rate in rates)
		//				{
		//					if (rate.MaxGuests >= (payingPax + FreePax) && rate.MaxAccessibleGuests >= wheelchairs)
		//					{
		//						airportRate.Add(rate);
		//					}
		//				}
		//			}
		//			#endregion

		//			if (totalServicedSplits == totalSplits)
		//			{
		//				#region All splits serviced
		//				if (userInfo.ShowSplits)
		//				{
		//					#region userInfo.ShowSplits= true and All Serviced => Retrun All Serviced
		//					#region Filter => no filter needed
		//					//no filter needed 
		//					#endregion
		//					#endregion
		//				}
		//				else
		//				{
		//					#region userInfo.ShowSplits= false and All Serviced => Retrun cheapest ones
		//					#region Filter => return cheapest one for each Fleet + ServiceTypeCode
		//					//var airportRateSorted = airportRate.OrderBy(s => s.Fleet).OrderBy(s => s.ServiceTypeCode).ToList();
		//					airportRate = airportRate.GroupBy(s => new { s.Fleet, s.ServiceTypeCode }).Select(s => s.OrderBy(r => r.FirstPersonFare + r.AdditionalPersonFare * (payingPax - 1))).Select(s => s.First()).ToList();
		//					#endregion
		//					#endregion
		//				}
		//				#endregion
		//			}
		//			else
		//			{
		//				#region Not all splits serviced, but at least one is serviced
		//				if (totalServicedSplits > 0)
		//				{
		//					if (userInfo.ShowSplits)
		//					{
		//						#region userInfo.ShowSplits= true but not all serviced => Retrun All Serviced
		//						#region Filter => no filter needed
		//						//no filter needed
		//						#endregion
		//						#endregion
		//					}
		//					else
		//					{
		//						if (userInfo.AllowClosedSplits)
		//						{
		//							#region userInfo.ShowSplits= false and userInfo.AllowClosedSplits = true,  Retrun cheapest ones
		//							#region Filter => return cheapest one for each Fleet + ServiceTypeCode
		//							airportRate = airportRate.GroupBy(s => new { s.Fleet, s.ServiceTypeCode }).Select(s => s.OrderBy(r => r.FirstPersonFare + r.AdditionalPersonFare * (payingPax - 1))).Select(s => s.First()).ToList();
		//							#endregion
		//							#endregion
		//						}
		//						else
		//						{
		//							#region userInfo.ShowSplits= false and userInfo.AllowClosedSplits = false,  Retrun nothing. Like the whose Zipcode (not split) is not serviced
		//							#region Fileter => Retrun nothing. Like the whose Zipcode (not split) is not serviced
		//							airportRate.RemoveAll(s => true);
		//							#endregion
		//							#endregion
		//						}
		//					}
		//				}
		//				#endregion
		//			}
		//		}
		//		#endregion
		//	}
		//	else
		//	{
		//		throw new Exception(Messages.General_NoMatchingRate);
		//	}

		//	if (airportRate.Count == 0)
		//		throw new Exception(Messages.General_NoMatchingRate);

		//	return airportRate;
		//}

		/// <summary>
		/// Connect to SuperShuttle to get all zipCode rates
		/// </summary>
		//private RatesService.ServiceTypes getZipCodeRates(string airportCode, int fleetFilter, string franchiseCode, int tripDirection, string postalCode, string subZipName, int payingPax, int discountId)
		//{
		//	#region Init
		//	UDI.SDS.RatesService.SecurityToken rateToken = _token.ToRatesServiceToken();
		//	var rateAdapter = new RateServiceAdapter(rateToken, _trackTime);
		//	#endregion

		//	var result = rateAdapter.GetZipCodeRates(airportCode, fleetFilter, franchiseCode, tripDirection, postalCode, subZipName, payingPax, discountId);

		//	return result;
		//}

		//private List<RatesService.CharterServiceTypeRecord> getHourlyCharterRates(int fleetFilter, string postalCode, int totalMinutes, int PayingPax, int wheelChairs, int FreePax)
		//{
		//	#region Init
		//	UDI.SDS.RatesService.SecurityToken rateToken = _token.ToRatesServiceToken();
		//	var rateAdapter = new RateServiceAdapter(rateToken, _trackTime);
		//	#endregion

		//	var result = rateAdapter.GetHourlyCharterRates(fleetFilter, postalCode, totalMinutes, PayingPax, wheelChairs, FreePax);

		//	return result;
		//}

		/// <summary>
		/// Connect to SuperShuttle to get all point to point rates, then return the lowest rate.
		/// If no matching record found, return NULL.
		/// </summary>
		//private List<RatesService.CharterServiceTypeRecord> getPointToPointCharterRates(int fleetFilter, List<UDI.SDS.DTO.Location> locations, int payingPax, int wheelChairs, int FreePax, decimal gratuityRate)
		//{
		//	#region Init
		//	UDI.SDS.RatesService.SecurityToken rateToken = _token.ToRatesServiceToken();
		//	var rateAdapter = new RateServiceAdapter(rateToken, _trackTime);

		//	//UDI.SDS.UtilityService.SecurityToken utilityToken = _token.ToUtilityServiceToken();
		//	var utilityController = new UtilityController(_token, _trackTime);

		//	//UDI.SDS.RoutingService.SecurityToken routingToken = _token.ToRoutingServiceToken();
		//	var routingController = new RoutingController(_token, _trackTime);

		//	var geocodeController = new GeocoderController(_token, _trackTime);
		//	#endregion

		//	try
		//	{
		//		var msg = string.Empty;

		//		List<RatesService.CharterServiceTypeRecord> result = new List<RatesService.CharterServiceTypeRecord>();
		//		RoutingService.Point startPoint = new RoutingService.Point();
		//		RoutingService.Point endPoint = new RoutingService.Point();
		//		RatesService.PointToPointCharterRateRequest request = new RatesService.PointToPointCharterRateRequest();
		//		request.FleetFilter = fleetFilter;

		//		#region Get GeoCode for each address
		//		foreach (var location in locations)
		//		{
		//			//var newlocation = location;
		//			//#region Modify Address - Geocode
		//			//if (location != null)
		//			//{
		//			//	geocodeController.ModifyAddress(location);
		//			//}
		//			//#endregion

		//			//if (location.Latitude == 0 || location.Longitude == 0)
		//			//	newlocation = GetGeoCodeForAddress(location);

		//			if (location.mode == LocationMode.Pickup)
		//			{
		//				request.PickupZipCode = location.PostalCode;
		//				request.PickupLandmarkID = location.MasterLandmarkID;
		//				startPoint.Latitude = (Double)location.Latitude;
		//				startPoint.Longitude = (Double)location.Longitude;
		//			}
		//			else
		//			{
		//				request.DropoffZipCode = location.PostalCode;
		//				request.DropoffLandmarkID = location.MasterLandmarkID;
		//				endPoint.Latitude = (Double)location.Latitude;
		//				endPoint.Longitude = (Double)location.Longitude;
		//			}
		//		}
		//		#endregion

		//		#region Get Charter Location ID
		//		request.CharterLocationsID = utilityController.GetCharterLocationIdByPostalCode((int)CharterType.PointToPoint, request.PickupZipCode);
		//		#endregion

		//		#region Get Distance
		//		//ToDoZTrip: If we know both point, we do this. otherwise we ignore it and pass zero
		//		request.Distance = routingController.GetDistanceBetweenPoints(startPoint, endPoint);
		//		#endregion

		//		#region Gratuity
		//		request.GratuityRate = gratuityRate;
		//		#endregion

		//		//ToDoZTrip:
		//		//this parameter 
		//		//request.PickMeUpNow = true;

		//		logger.InfoFormat("GetPointToPointCharterRates Start...\r\n {0}", request.XmlSerialize());

		//		//using (var client = new RatesService.RatesServiceClient())
		//		//{

		//		var rates = rateAdapter.GetPointToPointCharterRates(request);


		//		//Check maximum PayingPax and maximum wheelchairs, order by the lowest Fare
		//		if (rates.Count > 0)
		//		{
		//			rates = rates.OrderBy(r => r.Fare).ToList();

		//			foreach (var rate in rates)
		//			{
		//				if (rate.MaxGuests >= (payingPax + FreePax) && rate.MaxAccessibleGuests >= wheelChairs)
		//				{
		//					result.Add(rate);
		//					break;
		//				}
		//			}
		//		}
		//		else
		//			msg = "GetPointToPointCharterRates";
		//		//}

		//		if (!string.IsNullOrEmpty(msg))
		//			throw new Exception(Messages.General_NoMatchingRate);

		//		if (result.Count == 0)
		//			throw new Exception(Messages.General_NoMatchingRate);

		//		return result;
		//	}
		//	catch (Exception ex)
		//	{
		//		logger.ErrorFormat("Call RatesService.GetPointToPointCharterRates {0}", ex.StackTrace);
		//		throw ex;
		//	}
		//}
		#endregion

		#region Properties
		public TrackTime TrackTime
		{
			get
			{
				return _trackTime;
			}
			set
			{
				_trackTime = value;
			}
		}

		public TokenRS Token
		{
			set
			{
				_token = value;
			}
		}

		public UserInfo UserInfo
		{
			get
			{
				return userInfo;
			}
			set
			{
				userInfo = value;
			}
		}
		#endregion
	}
}
