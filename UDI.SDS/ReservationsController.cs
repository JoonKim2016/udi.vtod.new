﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.Track;
using UDI.SDS.Helper;
using UDI.SDS.ServiceAdapter;
using UDI.SDS.DTO;
using UDI.SDS.DTO.Enum;
using UDI.Utility.Serialization;
using UDI.VTOD.Common.Helper;
using UDI.SDS.RatesService;
using UDI.SDS.ReservationsServiceSecure;
using UDI.SDS.ReservationsService;
using UDI.Utility.Helper;
using UDI.VTOD.Common.DTO.Enum;
//using UDI.SDS.RatesService;


namespace UDI.SDS
{
	public class ReservationsController : BaseSetting
	{
		#region Fields
		TrackTime _trackTime;
		TokenRS _token;
		//private Guid bookRequestID;
		private UserInfo userInfo = new UserInfo { AllowClosedSplits = false, ShowSplits = false, PickupTimeApproach = (int)VTOD.Common.DTO.Enum.PickupTimeApproach.Validation };
		#endregion

		#region Constructors
		private ReservationsController()
		{
		}

		public ReservationsController(TokenRS token, TrackTime trackTime)
		{
			_token = token;
			_trackTime = trackTime;
		}

		#endregion

		#region Public
		public bool CancelReservation(string confirmationNumber, string userName, string lastName, string phoneNumber, string postalCode)
		{
			#region Init
			UDI.SDS.ReservationsService.SecurityToken reservationToken = _token.ToReservationServiceToken();
			var reservationAdapter = new ReservationServiceAdapter(reservationToken, _trackTime);
			#endregion

			var result = reservationAdapter.CancelReservation(confirmationNumber, userName, lastName, phoneNumber, postalCode);

			return result;
		}

		public bool CancelReservation(string agencyConfirmationNumber, string agencyContext)
		{
			#region Init
			UDI.SDS.ReservationsService.SecurityToken reservationToken = _token.ToReservationServiceToken();
			var reservationAdapter = new ReservationServiceAdapter(reservationToken, _trackTime);
			#endregion

			var result = reservationAdapter.CancelReservation(agencyConfirmationNumber, agencyContext);

			return result;
		}

		public TripStatus GetTripStatus(long? rezID)
		{
			#region Init
			UDI.SDS.ReservationsService.SecurityToken token = _token.ToReservationServiceToken();
			var reservationAdapter = new ReservationServiceAdapter(token, _trackTime);
			#endregion


			var request = new GetTripStatusRequest();
			if (rezID.HasValue)
			{
				request.RezID = rezID.Value.ToInt32();
			}

			TripStatus result = null;
			result = reservationAdapter.GetTripStatus(request);


			return result;
		}

		public TripStatus GetTripStatus(long? agencyTripID, string agencyContext)
		{
			#region Init
			UDI.SDS.ReservationsService.SecurityToken token = _token.ToReservationServiceToken();
			var reservationAdapter = new ReservationServiceAdapter(token, _trackTime);
			#endregion


			var request = new GetProviderTripStatusRequest();
			request.TripID = agencyTripID.Value.ToInt32();
			request.ProviderCode = agencyContext;

			TripStatus result = null;

			result = reservationAdapter.GetTripStatus(request);

			return result;
		}

		public UDI.SDS.ReservationsService.Reservation RetrieveReservation(string confirmationNumber, string lastName, string phoneNumber, string postalCode, int rezID)
		{
			#region Init
			UDI.SDS.ReservationsService.SecurityToken reservationToken = _token.ToReservationServiceToken();
			var reservationAdapter = new ReservationServiceAdapter(reservationToken, _trackTime);
			#endregion

			var criteria = new GetReservationCriteria { ConfirmationNumber = confirmationNumber, LastName = lastName, PhoneNumber = phoneNumber, PostalCode = postalCode, RezID = rezID };

			var result = reservationAdapter.RetrieveReservation(criteria);

			return result;
		}

		public CancelFee GetCancelFee(GetReservationCriteria request)
		{
			#region Init
			UDI.SDS.ReservationsService.SecurityToken token = _token.ToReservationServiceToken();
			var reservationAdapter = new ReservationServiceAdapter(token, _trackTime);
			#endregion


			//var request = new GetProviderTripStatusRequest();
			CancelFee result = null;

			result = reservationAdapter.GetCancelFee(request);

			return result;
		}

		public ChargeVtodTripResponse ChargeVtodTrip(ChargeVtodTripRequest chargeVtodTripRequest)
		{
			//chargeVtodTripRequest.fare
			//chargeVtodTripRequest.gratuity
			//chargeVtodTripRequest.memberId
			//chargeVtodTripRequest.fleetID
			//chargeVtodTripRequest.creditCardAccountId
			#region Init
			UDI.SDS.ReservationsServiceSecure.SecurityToken token = _token.ToReservationSecureServiceToken();
			var reservationAdapter = new ReservationSecureServiceAdapter(token, _trackTime);
			#endregion

			var result = reservationAdapter.ChargeVtodTrip(chargeVtodTripRequest);


			return result;
		}

		public GetReceiptResponse GetSDSFareDetail(int rezID)
		{
			#region Init
			UDI.SDS.ReservationsService.SecurityToken token = _token.ToReservationServiceToken();
			var reservationAdapter = new ReservationServiceAdapter(token, _trackTime);
			#endregion

			var result = reservationAdapter.GetReceipt(rezID);

			return result;
		}

		public BookResponse BookAirport(BookRequest request, RatesResponse rateResponseFirstSegment, PickupTimeResponse pickupTimeResponseFirstSegment, RatesResponse rateResponseSecondSegment, PickupTimeResponse pickupTimeResponseSecondSegment)
		{
			#region Init
			UDI.SDS.ReservationsServiceSecure.SecurityToken reservationToken = _token.ToReservationSecureServiceToken();
			var reservationAdapter = new ReservationSecureServiceAdapter(reservationToken, _trackTime);

			var geocodeController = new GeocoderController(_token, _trackTime);
			#endregion

			try
			{
				logger.InfoFormat("BookForSpecifiedRateAndPickupTime Start...\r\n");
				BookResponse response = new BookResponse();
				response.ReservationResponses = new List<ReservationResponse>();
				ReservationResponse reservationResponse = new ReservationResponse();

				//rateResponse.Segment is the first element of BookRequest.Segments, here use rateResponse.Segment because of the segment returned by Rate function has some updated fields, such as masterlandmarkID, locationType and subzipname.
				//var firstSegment = rateResponseFirstSegment.Segment;
				//var secondSegment = rateResponseSecondSegment != null ? rateResponseSecondSegment.Segment : null; //request.Segments.Count == 2 ? request.Segments.Last() : null;

				var firstSegment = request.Segments.FirstOrDefault();
				var secondSegment = request.Segments.Count() > 1 ? request.Segments.LastOrDefault() : null;


				#region Book Reservation
				//if (rateResponseFirstSegment.TripDirection == TripDirection.Inbound || rateResponseFirstSegment.TripDirection == TripDirection.Outbound || rateResponseFirstSegment.TripDirection == TripDirection.RoundTrip)
				//{
				/** Airport Reservation **/
				//reservationResponse.AirportReservationConfirmations = BookAirport(firstSegment, secondSegment, request.reservationRequest, rateResponseFirstSegment, pickupTimeResponseFirstSegment, rateResponseSecondSegment, pickupTimeResponseSecondSegment);
				reservationResponse.AirportReservationConfirmations = reservationAdapter.SaveAirportReservation(firstSegment, secondSegment, request.ReservationRequest, rateResponseFirstSegment, pickupTimeResponseFirstSegment, rateResponseSecondSegment, pickupTimeResponseSecondSegment);

				//}
				#region Do not delete. For this version we do not provide charter
				//else
				//{
				//	/** Charter Reservation **/
				//	reservationResponse.CharterReservationConfirmation = BookCharter(firstSegment, request.reservationRequest, rateResponseFirstSegment, firstSegmentFees, pickupTimeResponseFirstSegment);
				//} 
				#endregion
				#endregion

				reservationResponse.Segments = new List<UDI.SDS.DTO.Segment>();
				reservationResponse.Segments.Add(firstSegment);
				reservationResponse.Segments.Add(secondSegment);
				response.ReservationResponses.Add(reservationResponse);

				response.ResponseUtcTime = DateTime.UtcNow;
				//response.BookRequestID = request.BookRequestID;
				logger.InfoFormat("BookForSpecifiedRateAndPickupTime End...\r\n{0}", response.XmlSerialize());
				return response;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("BookForSpecifiedRateAndPickupTime BookRequestID:{0}", ex.ToString());
				throw ex;
			}
		}

		public BookResponse BookCharter(UDI.SDS.DTO.Segment segment, ReservationRequest request, UDI.SDS.RatesService.CharterServiceTypeRecord charterRate, PickupTimeResponse pickupTimeResponse)
		{
			#region Init
			UDI.SDS.ReservationsServiceSecure.SecurityToken reservationToken = _token.ToReservationSecureServiceToken();
			var reservationAdapter = new ReservationSecureServiceAdapter(reservationToken, _trackTime);

			var geocodeController = new GeocoderController(_token, _trackTime);

			var franchiseController = new FranchiseController(_token, _trackTime);
			#endregion

			#region Get FranchiseLocale

			//if (rateResponse != null && rateResponse.CharterRate != null && !string.IsNullOrEmpty(rateResponse.CharterRate.FranchiseCode))
			//{
			charterRate.FranchiseLocale = franchiseController.GetFranchiseLocale(charterRate.FranchiseCode);
			//}
			#endregion

			var CharterResult = reservationAdapter.SaveCharterReservation(segment, request, charterRate, pickupTimeResponse);

			var result = new BookResponse { ReservationResponses = new List<ReservationResponse> { new ReservationResponse { CharterReservationConfirmation = CharterResult } } };


			return result;
		}

		public BookResponse BookHourly(UDI.SDS.DTO.Segment segment, ReservationRequest request, UDI.SDS.RatesService.CharterServiceTypeRecord charterRate, PickupTimeResponse pickupTimeResponse)
		{
			#region Init
			UDI.SDS.ReservationsServiceSecure.SecurityToken reservationToken = _token.ToReservationSecureServiceToken();
			var reservationAdapter = new ReservationSecureServiceAdapter(reservationToken, _trackTime);

			var geocodeController = new GeocoderController(_token, _trackTime);

			var franchiseController = new FranchiseController(_token, _trackTime);
			#endregion

			#region Get FranchiseLocale

			//if (rateResponse != null && rateResponse.CharterRate != null && !string.IsNullOrEmpty(rateResponse.CharterRate.FranchiseCode))
			//{
			charterRate.FranchiseLocale = franchiseController.GetFranchiseLocale(charterRate.FranchiseCode);
			//}
			#endregion

			var CharterResult = reservationAdapter.SaveHourlyReservation(segment, request, charterRate, pickupTimeResponse);

			var result = new BookResponse { ReservationResponses = new List<ReservationResponse> { new ReservationResponse { CharterReservationConfirmation = CharterResult } } };


			return result;
		}

		public ReservationsService.PickupTimeRecord ValidateAirportPickupTime(UDI.SDS.DTO.Segment segment, UDI.SDS.RatesService.ServiceInfo serviceInfo, int hotelConciergeID)
		{
			#region Init
			UDI.SDS.ReservationsService.SecurityToken reservationToken = _token.ToReservationServiceToken();
			var reservationAdapter = new ReservationServiceAdapter(reservationToken, _trackTime);
			#endregion

			var result = reservationAdapter.ValidateAirportPickupTime(segment, serviceInfo, hotelConciergeID);
			return result;
		}

		public ReservationsService.PickupTimeRecord ValidateCharterPickupTime(int webAdvanceNoticeMinutes, string franshiseCode, DateTime pickupTime)
		{
			#region Init
			UDI.SDS.ReservationsService.SecurityToken reservationToken = _token.ToReservationServiceToken();
			var reservationAdapter = new ReservationServiceAdapter(reservationToken, _trackTime);
			#endregion

			var result = reservationAdapter.ValidateCharterPickupTime(webAdvanceNoticeMinutes, franshiseCode, pickupTime);
			return result;
		}

		public ReservationsService.PickupTimeRecord ValidateCharterPickupMeUpNowTime(UDI.SDS.UtilityService.PickMeUpNowServiceRequest request, DateTime pickupTime)
		{
			#region Init
			var utilityController = new UtilityController(_token, _trackTime);
			#endregion

			PickupTimeRecord result = null;

			var isPickMeUpNowServiced = utilityController.IsPickMeUpNowServiced(request);

			if (isPickMeUpNowServiced)
			{
				result = new PickupTimeRecord();
				result.StartTime = pickupTime;
				result.Status = UDI.SDS.ReservationsService.PickupTimeStatusCodesEnumeration.AllowPickup;
			}
			else
			{
				throw VTOD.Common.DTO.VtodException.CreateException(ExceptionType.SDS, 1008);// new Exception(Messages.UDI_SDS_PickMeUpNow_Not_Provided);
			}

			return result;
		}

		public List<ReservationsService.PickupTimeRecord> GetAirportPickupTimes(UDI.SDS.DTO.Segment segment, UDI.SDS.RatesService.ServiceInfo serviceInfo, /*Guid bookRequestID,*/ int hotelConciergeID, VTOD.Common.DTO.Enum.Method method)
		{
			#region Old Approch
			/*
			#region Init
			UDI.SDS.ReservationsService.SecurityToken reservationToken = _token.ToReservationServiceToken();
			var reservationAdapter = new ReservationServiceAdapter(reservationToken, _trackTime);
			#endregion

			try
			{
				//this.bookRequestID = bookRequestID;
				var response = new List<ReservationsService.PickupTimeRecord>();

				if (serviceInfo.Direction == Direction.Inbound || serviceInfo.Direction == Direction.Outbound)
				{
					#region Init
					var masterLandmarkID = 0;
					var airportCode = string.Empty;
					bool isAccessibleServiceRequired = segment.Wheelchairs > 0 ? true : false;

					foreach (var addressLocation in segment.Locations)
					{
						if (addressLocation.type == UDI.SDS.DTO.Enum.LocationType.Address)
							masterLandmarkID = addressLocation.MasterLandmarkID;

						if (addressLocation.type == UDI.SDS.DTO.Enum.LocationType.Airport)
							airportCode = addressLocation.AiportCode;
					}
					#endregion

					if (
						(
							(userInfo != null && userInfo.PickupTimeApproach == (int)VTOD.Common.DTO.Enum.PickupTimeApproach.Validation)
							&&
							(!string.IsNullOrWhiteSpace(serviceInfo.FleetType) && serviceInfo.FleetType.Trim().ToLower() == "ecar")
							&& serviceInfo.Direction == Direction.Inbound
						)
						||
						(
							serviceInfo.Direction == Direction.Outbound
							&&
							(!string.IsNullOrWhiteSpace(serviceInfo.FleetType) && serviceInfo.FleetType.Trim().ToLower() == "ecar")
						)
						||
						(
							(!string.IsNullOrWhiteSpace(serviceInfo.FleetType) && serviceInfo.FleetType.Trim().ToLower() == "ecar")
							&&
							method == VTOD.Common.DTO.Enum.Method.GroundBook
						)
					)
					{
						// Ecar -> Validate Pickuptime
						var pickupTimeRecord = ValidateAirportPickupTime(segment, serviceInfo, hotelConciergeID);
						pickupTimeRecord.IsGuaranteedTime = true;
						response.Add(pickupTimeRecord);
					}

					//if (
					//	!string.IsNullOrWhiteSpace(serviceInfo.FleetType) && serviceInfo.FleetType.Trim().ToLower() == "ecar"

					//	)
					//{
					//	// Ecar -> Validate Pickuptime
					//	var pickupTimeRecord = ValidateAirportPickupTime(segment, serviceInfo, hotelConciergeID);
					//	pickupTimeRecord.IsGuaranteedTime = true;
					//	response.Add(pickupTimeRecord);
					//}
					else
					{
						response = new List<ReservationsService.PickupTimeRecord>();
						response = reservationAdapter.GetPickupTimes(isAccessibleServiceRequired, hotelConciergeID, segment.IsInternationalFlight, masterLandmarkID, serviceInfo, segment.FlightDateTime, airportCode, segment.ChildSeats, segment.PayingPax);
					}



				}
				else
				{
					var pickupTimeRecord = new ReservationsService.PickupTimeRecord();
					pickupTimeRecord.StartTime = segment.RequestPickupTime;
					//pickupTimeRecord.EndTime = segment.RequestPickupTime;
					response.Add(pickupTimeRecord);
				}

				logger.InfoFormat("GetPickupTimes returns...\r\n{0}", response.XmlSerialize());

				return response;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("GetPickupTimes {0}", ex.StackTrace);
				throw ex;
			}
			*/
			#endregion

			#region Init
			UDI.SDS.ReservationsService.SecurityToken reservationToken = _token.ToReservationServiceToken();
			var reservationAdapter = new ReservationServiceAdapter(reservationToken, _trackTime);
			#endregion

			try
			{
				var response = new List<ReservationsService.PickupTimeRecord>();

				//if (serviceInfo.Direction == Direction.Inbound || serviceInfo.Direction == Direction.Outbound)
				//{
				#region Init
				var masterLandmarkID = 0;
				var airportCode = string.Empty;
				bool isAccessibleServiceRequired = segment.Wheelchairs > 0 ? true : false;
				var puTimeApproach = VTOD.Common.DTO.Enum.PickupTimeApproach.Unknown;
				var puTimeSelectApproach = PickupTimeSelectApproach.Unknown;

				foreach (var addressLocation in segment.Locations)
				{
					if (addressLocation.type == UDI.SDS.DTO.Enum.LocationType.Address)
						masterLandmarkID = addressLocation.MasterLandmarkID;

					if (addressLocation.type == UDI.SDS.DTO.Enum.LocationType.Airport)
						airportCode = addressLocation.AiportCode;
				}
				#endregion

				if (puTimeApproach == PickupTimeApproach.Unknown)
				{
					if (serviceInfo.FleetType.Trim().ToLower() != "ecar" || serviceInfo.FleetType.Trim().ToLower() == "taxi")
					{
						puTimeApproach = PickupTimeApproach.Suggestion;
						puTimeSelectApproach = PickupTimeSelectApproach.FlightTime;
					}
				}
				if (puTimeApproach == PickupTimeApproach.Unknown)
				{
					//RequestPickupTime is not nullable, so we check like this to see if we have it or not.
					if (segment.RequestPickupTime > System.DateTime.Now.AddDays(-1))
					{
						puTimeApproach = PickupTimeApproach.Validation;
						puTimeSelectApproach = PickupTimeSelectApproach.RequestedTime;
					}
					else
					{
						puTimeApproach = PickupTimeApproach.Suggestion;
						puTimeSelectApproach = PickupTimeSelectApproach.FlightTime;
					}
				}


				if (puTimeApproach == PickupTimeApproach.Suggestion)
				{
					var time = puTimeSelectApproach == PickupTimeSelectApproach.FlightTime ? segment.FlightDateTime : segment.RequestPickupTime;

					#region Suggestion
					response = new List<ReservationsService.PickupTimeRecord>();
					response = reservationAdapter.GetPickupTimes(isAccessibleServiceRequired, hotelConciergeID, segment.IsInternationalFlight, masterLandmarkID, serviceInfo, time, airportCode, segment.ChildSeats, segment.PayingPax);
					#endregion
				}
				else if (puTimeApproach == PickupTimeApproach.Validation)
				{
					//Select Apprach is always FlightTime
					//var time = puTimeSelectApproach == PickupTimeSelectApproach.FlightTime ? segment.FlightDateTime : segment.RequestPickupTime;
					#region Validation
					var pickupTimeRecord = ValidateAirportPickupTime(segment, serviceInfo, hotelConciergeID);
					pickupTimeRecord.IsGuaranteedTime = true;
					response.Add(pickupTimeRecord);
					#endregion
				}

				//}
				//else
				//{
				//	var pickupTimeRecord = new ReservationsService.PickupTimeRecord();
				//	pickupTimeRecord.StartTime = segment.RequestPickupTime;
				//	response.Add(pickupTimeRecord);
				//}

				logger.InfoFormat("GetPickupTimes returns...\r\n{0}", response.XmlSerialize());

				return response;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("GetPickupTimes {0}", ex.StackTrace);
				throw ex;
			}
		}

		public List<ReservationsService.PickupTimeRecord> GetCharterPickupTimes(int webAdvanceNoticeMinutes, string franchiseCode, DateTime pickupTime, VTOD.Common.DTO.Enum.Method method)
		{
			#region Init
			UDI.SDS.ReservationsService.SecurityToken reservationToken = _token.ToReservationServiceToken();
			var reservationAdapter = new ReservationServiceAdapter(reservationToken, _trackTime);
			#endregion

			try
			{
				var response = new List<ReservationsService.PickupTimeRecord>();

				var result = ValidateCharterPickupTime(webAdvanceNoticeMinutes, franchiseCode, pickupTime);
				response.Add(result);

				logger.InfoFormat("GetCharterPickupTimes returns...\r\n{0}", response.XmlSerialize());

				return response;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("GetCharterPickupTimes {0}", ex.StackTrace);
				throw ex;
			}
		}

		public void RequestEmailConfirmation(string email, string confirmationNumber, string lastName, string phoneNumber, string postalCode)
		{
			#region Init
			UDI.SDS.ReservationsService.SecurityToken reservationToken = _token.ToReservationServiceToken();
			var reservationAdapter = new ReservationServiceAdapter(reservationToken, _trackTime);
			#endregion

			try
			{

				reservationAdapter.RequestEmailConfirmation(email, confirmationNumber, lastName, phoneNumber, postalCode);

			}
			catch (Exception ex)
			{
				logger.ErrorFormat("GetCharterPickupTimes {0}", ex.StackTrace);
				throw ex;
			}
		}

		public List<ReservationsService.PickupTimeRecord> GetPickupTimes(bool isAccessibleServiceRequired, int hotelConciergeID, bool isInternationalFlight, int masterLandmarkID, ServiceInfo serviceInfo, DateTime flightTime, string airportCode, int childSeats, int payingPax)
		{
			#region Init
			UDI.SDS.ReservationsService.SecurityToken reservationToken = _token.ToReservationServiceToken();
			var reservationAdapter = new ReservationServiceAdapter(reservationToken, _trackTime);
			#endregion

			try
			{

				return reservationAdapter.GetPickupTimes(isAccessibleServiceRequired, hotelConciergeID, isInternationalFlight, masterLandmarkID, serviceInfo, flightTime, airportCode, childSeats, payingPax);

			}
			catch (Exception ex)
			{
				logger.ErrorFormat("GetPickupTimes {0}", ex.StackTrace);
				throw ex;
			}
		}

		#region Old
		//public List<ReservationsServiceSecure.ReservationConfirmation> BookAirport(Segment departintSegment, Segment returningSegment, ReservationRequest request, RateResponse rateResponseFirstSegment, UDI.SDS.RatesService.FareScheduleWithFees firstSegmentFees, PickupTimeResponse pickupTimeResponseFirstSegment, RateResponse rateResponseSecondSegment, UDI.SDS.RatesService.FareScheduleWithFees secondSegmentFees, PickupTimeResponse pickupTimeResponseSecondSegment)
		//{
		//	#region Init
		//	UDI.SDS.ReservationsServiceSecure.SecurityToken reservationToken = _token.ToReservationSecureServiceToken();
		//	var reservationAdapter = new ReservationSecureServiceAdapter(reservationToken, _trackTime);
		//	#endregion

		//	var result = reservationAdapter.SaveAirportReservation(departintSegment, returningSegment, request, rateResponseFirstSegment, firstSegmentFees, pickupTimeResponseFirstSegment, rateResponseSecondSegment, secondSegmentFees, pickupTimeResponseSecondSegment);

		//	return result;
		//} 

		/// <summary>
		/// Used by VTOD project.
		/// </summary>
		//public BookResponse Book(BookRequest request, RateResponse rateResponseFirstSegment, UDI.SDS.RatesService.FareScheduleWithFees firstSegmentFees, PickupTimeResponse pickupTimeResponseFirstSegment, RateResponse rateResponseSecondSegment, UDI.SDS.RatesService.FareScheduleWithFees secondSegmentFees, PickupTimeResponse pickupTimeResponseSecondSegment)
		//{
		//	#region Init
		//	var geocodeController = new GeocoderController(_token, _trackTime);
		//	#endregion

		//	try
		//	{
		//		logger.InfoFormat("BookForSpecifiedRateAndPickupTime Start...\r\n{0}");
		//		BookResponse response = new BookResponse();
		//		response.ReservationResponses = new List<ReservationResponse>();
		//		ReservationResponse reservationResponse = new ReservationResponse();

		//		//rateResponse.Segment is the first element of BookRequest.Segments, here use rateResponse.Segment because of the segment returned by Rate function has some updated fields, such as masterlandmarkID, locationType and subzipname.
		//		var firstSegment = rateResponseFirstSegment.Segment;
		//		var secondSegment = rateResponseSecondSegment != null ? rateResponseSecondSegment.Segment : null; //request.Segments.Count == 2 ? request.Segments.Last() : null;

		//		#region Book Reservation - Airport or Charter
		//		if (rateResponseFirstSegment.TripDirection == TripDirection.Inbound || rateResponseFirstSegment.TripDirection == TripDirection.Outbound || rateResponseFirstSegment.TripDirection == TripDirection.RoundTrip)
		//		{
		//			/** Airport Reservation **/
		//			reservationResponse.AirportReservationConfirmations = BookAirport(firstSegment, secondSegment, request.reservationRequest, rateResponseFirstSegment, firstSegmentFees, pickupTimeResponseFirstSegment, rateResponseSecondSegment, secondSegmentFees, pickupTimeResponseSecondSegment);

		//		}
		//		else
		//		{
		//			/** Charter Reservation **/
		//			reservationResponse.CharterReservationConfirmation = BookCharter(firstSegment, request.reservationRequest, rateResponseFirstSegment, firstSegmentFees, pickupTimeResponseFirstSegment);
		//		}
		//		#endregion

		//		reservationResponse.Segments = new List<Segment>();
		//		reservationResponse.Segments.Add(firstSegment);
		//		reservationResponse.Segments.Add(secondSegment);
		//		response.ReservationResponses.Add(reservationResponse);

		//		response.ResponseUtcTime = DateTime.UtcNow;
		//		response.BookRequestID = request.BookRequestID;
		//		logger.InfoFormat("BookForSpecifiedRateAndPickupTime End...\r\n{0}", response.XmlSerialize());
		//		return response;
		//	}
		//	catch (Exception ex)
		//	{
		//		logger.ErrorFormat("BookForSpecifiedRateAndPickupTime BookRequestID:{0}\r\n {1}", request.BookRequestID, ex.ToString());
		//		throw ex;
		//	}
		//} 

		//public ReservationsServiceSecure.CharterReservationConfirmation BookCharter(Segment segment, ReservationRequest request, RateResponse rateResponse, UDI.SDS.RatesService.FareScheduleWithFees firstSegmentFees, PickupTimeResponse pickupTimeResponse)
		//{
		//	#region Init
		//	UDI.SDS.ReservationsServiceSecure.SecurityToken reservationToken = _token.ToReservationSecureServiceToken();
		//	var reservationAdapter = new ReservationSecureServiceAdapter(reservationToken, _trackTime);

		//	var geocodeController = new GeocoderController(_token, _trackTime);

		//	var franchiseController = new FranchiseController(_token, _trackTime);

		//	var franchiseLocale = string.Empty;
		//	#endregion

		//	#region Get FranchiseLocale
		//	if (rateResponse != null && rateResponse.CharterRate != null && !string.IsNullOrEmpty(rateResponse.CharterRate.FranchiseCode))
		//	{
		//		franchiseLocale = franchiseController.GetFranchiseLocale(rateResponse.CharterRate.FranchiseCode);
		//	}
		//	#endregion

		//	var result = reservationAdapter.SaveCharterReservation(segment, request, rateResponse, firstSegmentFees, pickupTimeResponse, franchiseLocale);

		//	return result;
		//}
		#endregion
		#endregion

		#region Properties
		public TrackTime TrackTime
		{
			get
			{
				return _trackTime;
			}
			set
			{
				_trackTime = value;
			}
		}

		public TokenRS Token
		{
			set
			{
				_token = value;
			}
		}

		public UserInfo UserInfo
		{
			get
			{
				return userInfo;
			}
			set
			{
				userInfo = value;
			}
		}
		#endregion
	}
}
