﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.Track;
using UDI.SDS.Helper;
using UDI.SDS.RoutingService;
using UDI.SDS.DTO.Enum;
using UDI.SDS.DTO;
using UDI.SDS.ServiceAdapter;

namespace UDI.SDS
{
	public class RoutingController : BaseSetting
	{
		#region Fields
		TrackTime _trackTime;
		TokenRS _token;
		#endregion

		#region Constructors
		private RoutingController()
		{
		}

		public RoutingController(TokenRS token, TrackTime trackTime)
		{
			_token = token;
			_trackTime = trackTime;
		}

		#endregion

		#region Public
		public decimal GetDistanceBetweenPoints(Point startPoint, Point endPoint)
		{
			#region Init
			UDI.SDS.RoutingService.SecurityToken routingToken = _token.ToRoutingServiceToken();
			var routingAdapter = new RoutingServiceAdapter(routingToken, _trackTime);
			#endregion

			var result = routingAdapter.GetDistanceBetweenPoints(startPoint, endPoint);

			return result;
		}

		public RouteMetrics GetRouteMetrics(Point startPoint, Point endPoint)
		{
			#region Init
			UDI.SDS.RoutingService.SecurityToken routingToken = _token.ToRoutingServiceToken();
			var routingAdapter = new RoutingServiceAdapter(routingToken, _trackTime);
			#endregion

			var result = routingAdapter.GetRouteMetrics(startPoint, endPoint);

			return result;
		}

		#endregion

		#region Properties
		public TrackTime TrackTime
		{
			get
			{
				return _trackTime;
			}
			set
			{
				_trackTime = value;
			}
		}

		public TokenRS Token
		{
			set
			{
				_token = value;
			}
		}
		#endregion
	}
}
