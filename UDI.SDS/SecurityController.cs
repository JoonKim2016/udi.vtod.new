﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.Track;
using UDI.SDS.Helper;
using UDI.SDS.DTO;
using UDI.SDS.ServiceAdapter;

namespace UDI.SDS
{
	public class SecurityController : BaseSetting
	{
		#region Fields
		TrackTime _trackTime;
		TokenRS _token;
		#endregion

		#region Constructors
		private SecurityController()
		{
		}

		public SecurityController(TokenRS token, TrackTime trackTime)
		{
			_token = token;
			_trackTime = trackTime;
		}

		#endregion

		#region Public
		/// <summary>
		/// Used by VTOD project.
		/// </summary>
        public UDI.VTOD.Common.DTO.OTA.TokenRS GetToken(UDI.VTOD.Common.DTO.OTA.TokenRQ input, out UserInfo userInfo)
        {
            #region Init
            UDI.SDS.SecurityService.SecurityToken rateToken = _token.ToSecurityServiceToken();
            var securityAdapter = new SecurityServiceAdapter(rateToken, _trackTime);
            #endregion

            var result = securityAdapter.GetToken(input, out userInfo);

            return result;
        }

        public UDI.VTOD.Common.DTO.OTA.TokenRS GetToken(UDI.VTOD.Common.DTO.OTA.TokenRQ input)
        {
            #region Init
            UDI.SDS.SecurityService.SecurityToken rateToken = _token.ToSecurityServiceToken();
            var securityAdapter = new SecurityServiceAdapter(rateToken, _trackTime);
            #endregion

            var result = securityAdapter.GetToken(input);

            return result;
        }

		public bool ValidateToken(string userName, Guid securityKey)
		{
			#region Init
			var securityAdapter = new SecurityServiceAdapter(null, _trackTime);
			#endregion

			var result = securityAdapter.ValidateToken(userName, securityKey);

			return result;
		}
		#endregion

		#region Properties
		public TrackTime TrackTime
		{
			get
			{
				return _trackTime;
			}
			set
			{
				_trackTime = value;
			}
		}

		public TokenRS Token
		{
			set
			{
				_token = value;
			}
		}
		#endregion
	}
}
