<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:tns="http://www.SuperShuttle.com/WebServices/VTOD/Groups/DataContracts" elementFormDefault="qualified" targetNamespace="http://www.SuperShuttle.com/WebServices/VTOD/Groups/DataContracts" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:complexType name="Discount">
    <xs:annotation>
      <xs:appinfo />
      <xs:documentation>&lt;summary&gt;
            Defines a discount.
            &lt;/summary&gt;</xs:documentation>
    </xs:annotation>
    <xs:sequence>
      <xs:element name="AllZips" type="xs:boolean">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            Indicates the discount code is applicable to all service zip codes.
            &lt;/summary&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element minOccurs="0" name="CustomerReadOnly" type="xs:boolean">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            Indicates the discount code is readonly to the customer, the customer cannot alter it in the UI layer.
            &lt;/summary&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="DiscountAirports" nillable="true" type="tns:DiscountAirportRecords">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            An array of &lt;see cref="T:DiscountAirportRecord" /&gt; objects that define the valid airports for the discount.
            &lt;/summary&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="DiscountCode" nillable="true" type="xs:string">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            The 5 character discount code.
            &lt;/summary&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="DiscountID" type="xs:int">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            The discount record ID value.
            &lt;/summary&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="DiscountServiceTypes" nillable="true" type="tns:DiscountServiceTypeRecords">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            An array of &lt;see cref="T:DiscountServiceTypeRecord" /&gt; objects that define the valid service types for the discount.
            &lt;/summary&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="EndDate" type="xs:dateTime">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            The expiration date for the discount.
            &lt;/summary&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="ExecuCarDirectBillAccountNumber" nillable="true" type="xs:string">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            The ExecuCar direct bill account number associated with the discount (if any).
            &lt;/summary&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="GroupID" type="xs:int">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            For internal use only. The ID value of the GroupActivity record.
            &lt;/summary&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="GroupName" nillable="true" type="xs:string">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            The name of the group associated with the discount.
            &lt;/summary&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="HideBlueVanRates" type="xs:boolean">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            This will set whether Blue Van Rates display or not.
            &lt;/summary&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element minOccurs="0" name="HideDiscountCodeFromCustomer" type="xs:boolean">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            Indicates the discount code value should be masked to the customer.
            &lt;/summary&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="HideEcarRates" type="xs:boolean">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            This will set whether Ecar Rates display or not.
            &lt;/summary&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="HideWelcomeScreen" type="xs:boolean">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            For internal use only, indicates whether the welcome screen should be displayed on the website.
            &lt;/summary&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="StartDate" type="xs:dateTime">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            The start date for the discount.
            &lt;/summary&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="SuperShuttleDirectBillAccountNumber" nillable="true" type="xs:string">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            The SuperShuttle direct bill account number associated with the discount (if any).
            &lt;/summary&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
  <xs:element name="Discount" nillable="true" type="tns:Discount" />
  <xs:complexType name="DiscountAirportRecords">
    <xs:annotation>
      <xs:appinfo />
      <xs:documentation>&lt;summary&gt;
            Holds an array of &lt;see cref="T:DiscountAirportRecord" /&gt; objects.
            &lt;/summary&gt;
            &lt;remarks&gt;
            The &lt;b&gt;HasRecords&lt;/b&gt; property should be evalauted to check if the array is empty or populated.
            &lt;/remarks&gt;</xs:documentation>
    </xs:annotation>
    <xs:sequence>
      <xs:element minOccurs="0" name="DiscountAirportRecordsArray" nillable="true" type="tns:ArrayOfDiscountAirportRecord">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            An array of &lt;see cref="T:DiscountAirportRecord" /&gt; objects.
            &lt;/summary&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="HasRecords" type="xs:boolean">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            Indicates the &lt;see cref="F:DiscountAirportRecordsArray" /&gt; is empty or populated.
            &lt;/summary&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
  <xs:element name="DiscountAirportRecords" nillable="true" type="tns:DiscountAirportRecords" />
  <xs:complexType name="ArrayOfDiscountAirportRecord">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="DiscountAirportRecord" nillable="true" type="tns:DiscountAirportRecord" />
    </xs:sequence>
  </xs:complexType>
  <xs:element name="ArrayOfDiscountAirportRecord" nillable="true" type="tns:ArrayOfDiscountAirportRecord" />
  <xs:complexType name="DiscountAirportRecord">
    <xs:annotation>
      <xs:appinfo />
      <xs:documentation>&lt;summary&gt;
            Airport information for a specific discount.
            &lt;/summary&gt;</xs:documentation>
    </xs:annotation>
    <xs:sequence>
      <xs:element name="AirportCode" nillable="true" type="xs:string">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            The 3 character airport code.
            &lt;/summary&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="DiscountApplies" type="xs:boolean">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            A &lt;see cref="T:ystem.Boolean" /&gt; value indicates the discount can be applied to a reservation for this airport.
            &lt;/summary&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
  <xs:element name="DiscountAirportRecord" nillable="true" type="tns:DiscountAirportRecord" />
  <xs:complexType name="DiscountServiceTypeRecords">
    <xs:annotation>
      <xs:appinfo />
      <xs:documentation>&lt;summary&gt;
            Holds an array of &lt;see cref="T:DiscountServiceTypeRecord" /&gt; objects.
            &lt;/summary&gt;
            &lt;remarks&gt;
            The &lt;b&gt;HasRecords&lt;/b&gt; property should be evalauted to check if the array is empty or populated.
            &lt;/remarks&gt;</xs:documentation>
    </xs:annotation>
    <xs:sequence>
      <xs:element minOccurs="0" name="DiscountServiceTypeRecordsArray" nillable="true" type="tns:ArrayOfDiscountServiceTypeRecord">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            An array of &lt;see cref="T:DiscountServiceTypeRecord" /&gt; objects.
            &lt;/summary&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="HasRecords" type="xs:boolean">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            Indicates the &lt;see cref="F:DiscountServiceTypeRecordsArray" /&gt; is empty or populated.
            &lt;/summary&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
  <xs:element name="DiscountServiceTypeRecords" nillable="true" type="tns:DiscountServiceTypeRecords" />
  <xs:complexType name="ArrayOfDiscountServiceTypeRecord">
    <xs:sequence>
      <xs:element minOccurs="0" maxOccurs="unbounded" name="DiscountServiceTypeRecord" nillable="true" type="tns:DiscountServiceTypeRecord" />
    </xs:sequence>
  </xs:complexType>
  <xs:element name="ArrayOfDiscountServiceTypeRecord" nillable="true" type="tns:ArrayOfDiscountServiceTypeRecord" />
  <xs:complexType name="DiscountServiceTypeRecord">
    <xs:annotation>
      <xs:appinfo />
      <xs:documentation>&lt;summary&gt;
            Defines the discount values for a specific service type.
            &lt;/summary&gt;</xs:documentation>
    </xs:annotation>
    <xs:sequence>
      <xs:element name="DiscountMethod" type="xs:unsignedByte">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            The discount calculation method for this servce type.
            &lt;/summary&gt;
            &lt;remarks&gt;
            The following values are possible: &lt;br /&gt;
            &lt;b&gt;0&lt;/b&gt; No discount.
            &lt;b&gt;1&lt;/b&gt; Flat rate dollar amount per person.
            &lt;b&gt;2&lt;/b&gt; Flat rate dollar amount per party.
            &lt;b&gt;3&lt;/b&gt; Percentage of the first person fare.
            &lt;b&gt;4&lt;/b&gt; Percentage of the total fare.
            &lt;/remarks&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="OneWayDollarAmount" type="xs:decimal">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            The dollar amount subtracted from a oneway reservation when the &lt;see cref="P:DiscountMethod" /&gt; equals 1 or 2.
            &lt;/summary&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="OneWayPercentage" type="xs:int">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            The percentage value that will be subtracted from a oneway reservation when the &lt;see cref="P:DiscountMethod" /&gt; equals 3 or 4.
            &lt;/summary&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="RoundtripDollarAmount" type="xs:decimal">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            The dollar amount subtracted from a roundtrip reservation when the &lt;see cref="P:DiscountMethod" /&gt; equals 1 or 2.
            &lt;/summary&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="RoundtripPercentage" type="xs:int">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            The percentage value that will be subtracted from a roundtrip reservation when the &lt;see cref="P:DiscountMethod" /&gt; equals 3 or 4.
            &lt;/summary&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="ServiceTypeCode" nillable="true" type="xs:string">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            The 3 character service type code.
            &lt;/summary&gt;
            &lt;remarks&gt;
            This abbreviation is for internal use only and shouldn't be displayed to guests.
            &lt;/remarks&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="ServiceTypeDescription" nillable="true" type="xs:string">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            A description of the service type.
            &lt;/summary&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
  <xs:element name="DiscountServiceTypeRecord" nillable="true" type="tns:DiscountServiceTypeRecord" />
  <xs:complexType name="GroupReservationDefaults">
    <xs:annotation>
      <xs:appinfo />
      <xs:documentation>&lt;summary&gt;
            Defines default reservation values to use for a group.
            &lt;/summary&gt;</xs:documentation>
    </xs:annotation>
    <xs:sequence>
      <xs:element name="AllowInboundOutboundRoundtrip" type="xs:boolean">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            A &lt;see cref="T:ystem.Boolean" /&gt; value indicates the group is allowed to book roundtrip (inbound--&amp;gt;outbound) service.
            &lt;/summary&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="AllowInboundService" type="xs:boolean">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            A &lt;see cref="T:ystem.Boolean" /&gt; value indicates the group is allowed to book inbound.
            &lt;/summary&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="AllowOutboundInboundRoundtrip" type="xs:boolean">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            A &lt;see cref="T:ystem.Boolean" /&gt; value indicates the group is allowed to book roundtrip (oubound --&amp;gt; inbound) service.
            &lt;/summary&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="AllowOutboundService" type="xs:boolean">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            A &lt;see cref="T:ystem.Boolean" /&gt; value indicates the group is allowed to book outbound.
            &lt;/summary&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="BillingType" type="xs:unsignedByte">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            A &lt;see cref="T:ystem.Byte" /&gt; identifies the default billing type for the group.
            &lt;/summary&gt;
            &lt;remarks&gt;
            The following values are possible:&lt;br /&gt;
            &lt;b&gt;0&lt;/b&gt; - Cash&lt;br /&gt;
            &lt;b&gt;1&lt;/b&gt; - Credit Card&lt;br /&gt;
            &lt;b&gt;2&lt;/b&gt; - DirectBill&lt;br /&gt;
            &lt;b&gt;3&lt;/b&gt; - Voucher&lt;br /&gt;
            &lt;b&gt;4&lt;/b&gt; - ARC (for travel agents only)&lt;br /&gt;
            &lt;b&gt;5&lt;/b&gt; - Credit Card Hold
            &lt;b&gt;6&lt;/b&gt; - External Credit Card
            &lt;b&gt;7&lt;/b&gt; - Fare Ticket
            &lt;b&gt;8&lt;/b&gt; - Coupons
            &lt;b&gt;9&lt;/b&gt; - Disney Voucher
            &lt;b&gt;10&lt;/b&gt; - Money Oder
            &lt;b&gt;11&lt;/b&gt; - Prepaid In Acct
            &lt;b&gt;12&lt;/b&gt; - Traveler's checks
            &lt;/remarks&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="DefaultDirection" type="xs:int">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            The default direction setting for the group.
            &lt;/summary&gt;
            &lt;remarks&gt;
            Possible values are:&lt;br /&gt;
            &lt;b&gt;0&lt;/b&gt; Inbound&lt;br /&gt;
            &lt;b&gt;1&lt;/b&gt; Outbound&lt;br /&gt;
            &lt;b&gt;2&lt;/b&gt; RoundtripToFrom&lt;br /&gt;
            &lt;b&gt;3&lt;/b&gt; RoundtripFromTo&lt;br /&gt;
            &lt;/remarks&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="GroupID" type="xs:int">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            The unique ID value which identifies the group.
            &lt;/summary&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="RestrictLandmarks" type="xs:boolean">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            A &lt;see cref="T:ystem.Boolean" /&gt; value indicates travel is restricted to specific landmarks.
            &lt;/summary&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="SedanGratuityAmount" type="xs:decimal">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            The default gratuity dollar amount for ExecuCar Sedan service. This value must be greater than or equal to zero.
            &lt;/summary&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="SedanGratuityPercent" type="xs:unsignedByte">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            The default gratuity percent for ExecuCar Sedan service. This value is a whole number between 0 and 100.
            &lt;/summary&gt;
            &lt;exception cref="T:ystem.ArgumentOutOfRangeException"&gt;The value being set is less than zero and greater than 100.&lt;/exception&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="SedanGratuityType" type="xs:unsignedByte">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            A &lt;see cref="T:ystem.Byte" /&gt; value indicates the Gratuity type for ExecuCar Sedan service.
            &lt;/summary&gt;
            &lt;remarks&gt;
            The following values are possible:&lt;br /&gt;
            &lt;b&gt;0 = No default gratuity.&lt;/b&gt;
            &lt;b&gt;1 = Default gratuity is percentage of fare.&lt;/b&gt;
            &lt;b&gt;2 = Default gratuity $ amount per person.&lt;/b&gt;
            &lt;/remarks&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="VanGratuityAmount" type="xs:decimal">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            The default gratuity dollar amount for SuperShuttle Blue Van service. This value must be greater than or equal zero.
            &lt;/summary&gt;
            &lt;exception cref="T:ystem.ArgumentOutOfRangeException"&gt;The value being set is less than zero.&lt;/exception&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="VanGratuityPercent" type="xs:unsignedByte">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            The default gratuity percent for SuperShuttle Blue Van service. This value is a whole number between 0 and 100.
            &lt;/summary&gt;
            &lt;exception cref="T:ystem.ArgumentOutOfRangeException"&gt;The value being set is less than zero and greater than 100.&lt;/exception&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="VanGratuityType" type="xs:unsignedByte">
        <xs:annotation>
          <xs:appinfo />
          <xs:documentation>&lt;summary&gt;
            A &lt;see cref="T:ystem.Byte" /&gt; value indicates the Gratuity Type for SuperShuttle Blue Van service.
            &lt;/summary&gt;
            &lt;remarks&gt;
            The following values are possible:&lt;br /&gt;
            &lt;b&gt;0 = No default gratuity.&lt;/b&gt;
            &lt;b&gt;1 = Default gratuity is percentage of fare.&lt;/b&gt;
            &lt;b&gt;2 = Default gratuity is $ amount per person.&lt;/b&gt;
            &lt;/remarks&gt;</xs:documentation>
        </xs:annotation>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
  <xs:element name="GroupReservationDefaults" nillable="true" type="tns:GroupReservationDefaults" />
  <xs:complexType name="GetPromotionCodeTypeRequest">
    <xs:sequence>
      <xs:element minOccurs="0" name="PromotionCode" nillable="true" type="xs:string" />
    </xs:sequence>
  </xs:complexType>
  <xs:element name="GetPromotionCodeTypeRequest" nillable="true" type="tns:GetPromotionCodeTypeRequest" />
  <xs:complexType name="GetPromotionCodeTypeResponse">
    <xs:sequence>
      <xs:element minOccurs="0" name="PromotionCodeId" type="xs:int" />
      <xs:element minOccurs="0" name="PromotionCodeType" type="xs:int" />
    </xs:sequence>
  </xs:complexType>
  <xs:element name="GetPromotionCodeTypeResponse" nillable="true" type="tns:GetPromotionCodeTypeResponse" />
</xs:schema>