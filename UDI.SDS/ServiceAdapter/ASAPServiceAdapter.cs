﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.SDS.DTO.Enum;
//using UDI.SDS.RatesService;
using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Common.Track;
using UDI.Utility.Serialization;
using UDI.VTOD.Common.Helper;
using UDI.SDS.DTO;
using UDI.SDS.ASAPService;

namespace UDI.SDS.ServiceAdapter
{
	internal class ASAPServiceAdapter : BaseSetting
	{
		#region Fields
		SecurityToken _token;
		TrackTime _trackTime;
		#endregion

		#region Constructors
		private ASAPServiceAdapter()
		{
		}

		internal ASAPServiceAdapter(SecurityToken token, TrackTime trackTime)
		{
			_token = token;
			_trackTime = trackTime;
		}
		#endregion

		#region Internal
		internal int CreatCharterASAPRequest(CharterAsapRequest charterAsapRequest)
		{
			try
			{
				int asapRequestID = 0;
				logger.InfoFormat("Send to SuperShuttle \r\n ");
				using (var client = new ASAPService.ASAPServiceClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
					#endregion

					asapRequestID = client.CreateCharterRequest(_token, charterAsapRequest);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "CreatCharterASAPRequest");
					#endregion

				}

				return asapRequestID;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call CreatCharterASAPRequest {0}", ex.StackTrace);
				throw ex;
			}
		}

		internal int CreatAirportASAPRequest(RequestDetails airportAsapRequest)
		{
			try
			{
				int asapRequestID = 0;

				logger.InfoFormat("Send to SuperShuttle \r\n ");
				using (var client = new ASAPService.ASAPServiceClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
					#endregion

					asapRequestID = client.CreateRequest(_token, airportAsapRequest);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "CreatAirportASAPRequest");
					#endregion

				}

				return asapRequestID;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call CreatAirportASAPRequest {0}", ex.StackTrace);
				throw ex;
			}
		}

		internal StatusUpdate GetASAPRequestStatus(int requestID)
		{
			StatusUpdate result = null;

			try
			{

				logger.InfoFormat("Send to SuperShuttle \r\n ");
				using (var client = new ASAPService.ASAPServiceClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
					#endregion

					result = client.GetRequestStatus(_token, requestID);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "StatusUpdate");
					#endregion

				}

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call StatusUpdate {0}", ex.StackTrace);
				throw ex;
			}
		}

		internal void AbandonRequest(string userName, int requestID)
		{
			try
			{

				logger.InfoFormat("Send to SuperShuttle \r\n ");
				using (var client = new ASAPService.ASAPServiceClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
					#endregion

					client.AbandonRequest(_token, requestID, userName);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "AbandonRequest");
					#endregion

				}

			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call AbandonRequest {0}", ex.StackTrace);
				throw ex;
			}
		}

		#endregion

		#region Properties
		internal TrackTime TrackTime
		{
			get
			{
				return _trackTime;
			}
			set
			{
				_trackTime = value;
			}
		}

		internal SecurityToken Token
		{
			set
			{
				_token = value;
			}
		}
		#endregion
	}
}
