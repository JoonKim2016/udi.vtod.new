﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.Track;
using UDI.VTOD.Common.DTO.Enum;
using UDI.SDS.DTO;
using UDI.Utility.Helper;
using UDI.Utility.Serialization;
using UDI.VTOD.Common.DTO.OTA;
using UDI.SDS.DTO.Enum;
using UDI.VTOD.Common.Helper;
using UDI.SDS.MembershipService;
using UDI.SDS.UtilityService;
using UDI.VTOD.Common.DTO;

namespace UDI.SDS.ServiceAdapter
{
	public class AccountingServiceAdapter : BaseSetting
	{
		#region Fields
		MembershipService.SecurityToken _token_membership;
		UtilityService.SecurityToken _token_untility;
		TrackTime _trackTime;
		#endregion

		#region Constructors
		private AccountingServiceAdapter()
		{
		}

		internal AccountingServiceAdapter(MembershipService.SecurityToken token, TrackTime trackTime)
		{
			_token_membership = token;
			_trackTime = trackTime;
		}

		internal AccountingServiceAdapter(UtilityService.SecurityToken token, TrackTime trackTime)
		{
			_token_untility = token;
			_trackTime = trackTime;
		}
		#endregion

		#region Internal

		#region Credit Card

		internal int CreateCreditCardAccount(int memberId, MembershipCreditCardAccount accountDetails, string cvvNumber)
		{
			try
			{
				int accountID = 0;

				logger.InfoFormat("Send to SuperShuttle \r\n memberId={0}", memberId);
				using (var client = new MembershipService.MembershipClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
					#endregion


					if (_token_membership.UserName.Trim().ToLower() == "ztrip")
						accountID = client.VerifyAndCreateCreditCardAccount(_token_membership, memberId, accountDetails, cvvNumber);
					else
						accountID = client.CreateMembershipCreditCardAccount(_token_membership, memberId, accountDetails);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "VerifyAndCreateCreditCardAccount");
					#endregion

				}

				return accountID;
			}
			catch (System.ServiceModel.FaultException<InvalidCreditCardFault> ex)
			{
				logger.ErrorFormat("Call AccountingServiceAdapter.CreateCreditCardAccount {0}", ex.StackTrace);
                logger.ErrorFormat("Message AccountingServiceAdapter.CreateCreditCardAccount {0}", ex.Message);
                throw VtodException.CreateException(ExceptionType.Accounting, 8011);
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call MembershipService.VerifyAndCreateCreditCardAccount {0}", ex.StackTrace);
				throw ex;
			}
		}

		internal List<MembershipCreditCardAccount> GetCreditCardAccounts(int memberId)
		{
			try
			{
				List<MembershipCreditCardAccount> accounts = new List<MembershipCreditCardAccount>();

				logger.InfoFormat("Send to SuperShuttle \r\n memberId={0}", memberId);
				using (var client = new MembershipService.MembershipClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Accounting_Domain, "Call");
					#endregion

					var result = client.GetMembershipCreditCardAccounts(_token_membership, memberId);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetMembershipCreditCardAccounts");
					#endregion

					if (result.HasRecords)
					{
						accounts = result.CreditCardAccountsArray.ToList();
					}
				}

				return accounts;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call MembershipService.GetMembershipCreditCardAccounts {0}", ex.StackTrace);
				throw ex;
			}
		}

		internal bool SetDefaultCreditCardAccount(int memberId, int accountId, bool isDefault)
		{
			try
			{
				var result = false;

				logger.InfoFormat("Send to SuperShuttle \r\n memberId={0}, accountId={1}, isDefault={2}", memberId, accountId, isDefault);
				using (var client = new MembershipService.MembershipClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Accounting_Domain, "Call");
					#endregion

					result = client.SetCcAccountDefault(_token_membership, memberId, accountId, isDefault);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "SetCcAccountDefault");
					#endregion

				}

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call MembershipService.SetCcAccountDefault {0}", ex.StackTrace);
				throw ex;
			}
		}

		internal bool DeleteCreditCardAccount(int accountId)
		{
			try
			{
				var result = false;

				logger.InfoFormat("Send to SuperShuttle \r\n accountId={0}", accountId);
				using (var client = new MembershipService.MembershipClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Accounting_Domain, "Call");
					#endregion

					result = client.DeleteMembershipCreditCardAccount(_token_membership, accountId);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "DeleteMembershipCreditCardAccount");
					#endregion

				}

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call MembershipService.DeleteMembershipCreditCardAccount {0}", ex.StackTrace);
				throw ex;
			}
		}

		#endregion

		#region Direct Bill

		internal bool AddDirectBillAccountToCustomerProfile(int memberId, string directBillAccountNumber, PaymentsService.SecurityToken paymentToken, bool isDefault)
		{
			try
			{
				var result = false;
				var DbAccountID = -1;

				logger.InfoFormat("Send to SuperShuttle \r\n memberId={0}, directBillAccountNumber={1}, isDefault={2}.", memberId, directBillAccountNumber, isDefault);

				#region Get direct bill account ID

				using (var client = new PaymentsService.PaymentsServiceClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Accounting_Domain, "Call");
					#endregion

					DbAccountID = client.GetDirectBillAccountID(paymentToken, directBillAccountNumber);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetDirectBillAccountID");
					#endregion

				}

				#endregion

				logger.InfoFormat("Return from SuperShuttle \r\n DirectBillAccountID={0}", DbAccountID);

				#region If direct bill account exists, add direct bill account to customer profile

				//DbAccountID should be positive, Negative: does not exist
				if (DbAccountID > 0)
				{
					using (var client = new MembershipService.MembershipClient())
					{
						#region Track
						if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Accounting_Domain, "Call");
						#endregion

						var newRecordID = client.CreateMembershipDirectBillAccount2(_token_membership, memberId, DbAccountID, isDefault);

						#region Track
						if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "CreateMembershipDirectBillAccount2");
						#endregion

						if (newRecordID > 0)
							result = true;
					}
				}
				else
					throw UDI.VTOD.Common.DTO.VtodException.CreateException(ExceptionType.Accounting, 8006);//throw new Exception(Messages.Accounting_DirectBillAccountNotExist);

				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call PaymentsService.GetDirectBillAccountID and MembershipService.CreateMembershipDirectBillAccount2 {0}", ex.StackTrace);
				throw;
			}
		}

		internal List<MembershipDirectBillAccount> GetDirectBillAccounts(int memberId)
		{
			try
			{
				List<MembershipDirectBillAccount> accounts = new List<MembershipDirectBillAccount>();

				logger.InfoFormat("Send to SuperShuttle \r\n memberId={0}", memberId);
				using (var client = new MembershipService.MembershipClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Accounting_Domain, "Call");
					#endregion

					var result = client.GetMembershipDirectBillAccounts(_token_membership, memberId);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetMembershipDirectBillAccounts");
					#endregion

					if (result.HasRecords)
					{
						accounts = result.DirectBillAccountsArray.ToList();
					}
				}

				return accounts;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call MembershipService.GetMembershipDirectBillAccounts {0}", ex.StackTrace);
				throw ex;
			}
		}

		internal bool SetDefaultDirectBillAccount(int memberId, int accountId, bool isDefault)
		{
			try
			{
				var result = false;

				logger.InfoFormat("Send to SuperShuttle \r\n memberId={0}, accountId={1}, isDefault={2}", memberId, accountId, isDefault);
				using (var client = new MembershipService.MembershipClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Accounting_Domain, "Call");
					#endregion

					result = client.SetDbAccountDefault(_token_membership, memberId, accountId, isDefault);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "SetDbAccountDefault");
					#endregion

				}

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call MembershipService.SetDbAccountDefault {0}", ex.StackTrace);
				throw ex;
			}
		}

		internal bool DeleteDirectBillAccount(int memberId, int accountId)
		{
			try
			{
				var result = false;

				logger.InfoFormat("Send to SuperShuttle \r\n memberId={0}, accountId={1}", memberId, accountId);
				using (var client = new MembershipService.MembershipClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Accounting_Domain, "Call");
					#endregion

					result = client.DeleteMembershipDirectBillAccount(_token_membership, memberId, accountId);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "DeleteMembershipDirectBillAccount");
					#endregion

				}

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call MembershipService.DeleteMembershipDirectBillAccount {0}", ex.StackTrace);
				throw ex;
			}
		}

		#endregion

		#region zTrip Credits

		internal ActivateCreditsResponse ActivateCredits(string activationCode, int memberId)
		{
			try
			{
				var result = new MembershipService.ActivateCreditsResponse();

				logger.InfoFormat("Send to SuperShuttle \r\n memberId={0}, activationCode={1}", memberId, activationCode);

				var activateCreditsRequest = new MembershipService.ActivateCreditsRequest();
				activateCreditsRequest.ActivationCode = activationCode;
				activateCreditsRequest.MemberId = memberId;

				using (var client = new MembershipService.MembershipClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Accounting_Domain, "Call");
					#endregion

					result = client.ActivateCredits(_token_membership, activateCreditsRequest);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "ActivateCredits");
					#endregion

				}

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call MembershipService.ActivateCredits {0}", ex.StackTrace);
				throw ex;
			}
		}

		internal decimal GetCreditsBalance(int memberId)
		{
			try
			{
				decimal result = 0;

				logger.InfoFormat("Send to SuperShuttle \r\n memberId={0}", memberId);

				var creditsBalanceRequest = new MembershipService.CreditsBalanceRequest();
				creditsBalanceRequest.MemberId = memberId;

				using (var client = new MembershipService.MembershipClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Accounting_Domain, "Call");
					#endregion

					var response = client.GetCreditsBalance(_token_membership, creditsBalanceRequest);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetCreditsBalance");
					#endregion

					if (response != null && response.Balance != null)
						result = response.Balance;
				}

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call MembershipService.GetCreditsBalance {0}", ex.StackTrace);
				throw ex;
			}
		}

		#endregion

		#region Airline Rewards

		internal List<UtilityService.AirlineReward> GetAirlineRewardsPrograms()
		{
			try
			{
				var result = new List<UtilityService.AirlineReward>();

				logger.InfoFormat("Send to SuperShuttle \r\n ");
				using (var client = new UtilityService.UtilityServiceClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Accounting_Domain, "Call");
					#endregion

					var response = client.GetAirlineRewardsPrograms(_token_untility);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetAirlineRewardsPrograms");
					#endregion

					if (response.HasRecords)
					{
						result = response.AirlineRewardsArray.ToList();
					}
				}

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call UtilityService.GetAirlineRewardsPrograms {0}", ex.StackTrace);
				throw ex;
			}
		}

		internal int CreateMembershipMileageAccount(int memberId, AirlineMileageAccounts mileageAccount, bool isDefualt)
		{
			try
			{
				var result = 0;

				logger.InfoFormat("Send to SuperShuttle \r\n memberId={0}", memberId);
				using (var client = new MembershipService.MembershipClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Accounting_Domain, "Call");
					#endregion

					//result = client.CreateMembershipMileageAccount(_token_membership, memberId, mileageAccount);

					result = client.CreateMembershipMileageAccount2(_token_membership, new CreateMileageAccountRequest { MemberID = memberId, MileageAccount = mileageAccount, IsDefault = isDefualt });

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "CreateMembershipMileageAccount");
					#endregion
				}

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call MembershipService.CreateMembershipMileageAccount {0}", ex.StackTrace);
				throw ex;
			}
		}

		internal bool SetDefaultMileageAccount(int memberId, int accountID, bool isDefualt)
		{
			try
			{
				var result = false;

				logger.InfoFormat("Send to SuperShuttle \r\n memberId={0}", memberId);
				using (var client = new MembershipService.MembershipClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Accounting_Domain, "Call");
					#endregion

					//result = client.CreateMembershipMileageAccount(_token_membership, memberId, mileageAccount);

					result = client.SetDefaultMileageAccount(_token_membership, memberId, accountID, isDefualt);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "SetDefaultMileageAccount");
					#endregion
				}

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call MembershipService.CreateMembershipMileageAccount {0}", ex.StackTrace);
				throw ex;
			}
		}

		internal bool DeleteMembershipMileageAccount(int accountId)
		{
			try
			{
				var result = false;

				logger.InfoFormat("Send to SuperShuttle \r\n accountId={0}", accountId);
				using (var client = new MembershipService.MembershipClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Accounting_Domain, "Call");
					#endregion

					result = client.DeleteMembershipMileageAccount(_token_membership, accountId);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "DeleteMembershipMileageAccount");
					#endregion
				}

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call MembershipService.DeleteMembershipMileageAccount {0}", ex.StackTrace);
				throw ex;
			}
		}

		internal List<MembershipService.MembershipAirlineMileageAccount> GetMembershipAirlineMileageAccounts(int memberId)
		{
			try
			{
				var result = new List<MembershipService.MembershipAirlineMileageAccount>();

				logger.InfoFormat("Send to SuperShuttle \r\n memberId={0}", memberId);
				using (var client = new MembershipService.MembershipClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Accounting_Domain, "Call");
					#endregion

					var response = client.GetMembershipAirlineMileageAccounts(_token_membership, memberId);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetMembershipAirlineMileageAccounts");
					#endregion

					if (response.HasRecords)
					{
						result = response.AirlineMileageAccountsArray.ToList();
					}
				}

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call MembershipService.GetMembershipAirlineMileageAccounts {0}", ex.StackTrace);
				throw ex;
			}
		}
		#endregion

		#endregion

		#region Properties
		internal TrackTime TrackTime
		{
			get
			{
				return _trackTime;
			}
			set
			{
				_trackTime = value;
			}
		}

		internal MembershipService.SecurityToken Token
		{
			set
			{
				_token_membership = value;
			}
		}

		internal UtilityService.SecurityToken Token_Utility
		{
			set
			{
				_token_untility = value;
			}
		}
		#endregion

	}
}
