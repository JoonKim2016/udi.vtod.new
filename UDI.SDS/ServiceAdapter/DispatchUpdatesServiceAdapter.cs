﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.Track;
using UDI.VTOD.Common.DTO.Enum;
using UDI.SDS.DTO;
using UDI.Utility.Helper;
using UDI.Utility.Serialization;
using UDI.VTOD.Common.DTO.OTA;
using UDI.SDS.DTO.Enum;
using UDI.VTOD.Common.Helper;
using UDI.SDS.ReservationsService;

namespace UDI.SDS.ServiceAdapter
{
	internal class DispatchUpdatesServiceAdapter : BaseSetting
	{
		#region Fields
		SecurityToken _token;
		TrackTime _trackTime;
		#endregion

		#region Constructors
		private DispatchUpdatesServiceAdapter()
		{
		}

		internal DispatchUpdatesServiceAdapter(SecurityToken token, TrackTime trackTime)
		{
			_token = token;
			_trackTime = trackTime;
		}
		#endregion

		#region Internal
		/// <summary>
		/// Get all reservation statuses from SDS for VTOD
		/// </summary>
		internal List<DispatchUpdatesService.TripStatusRecord> GetStatus(string rezSource)
		{
			try
			{
				logger.InfoFormat("GetReservationStatus Start...\r\n  rezSource={0}", rezSource);

				List<DispatchUpdatesService.TripStatusRecord> result = null;

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
				#endregion

				using (var client = new DispatchUpdatesService.DispatchUpdatesServiceClient())
				{
					result = client.GetDispatchStatusUpdates(rezSource);
				}

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetDispatchStatusUpdates");
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("GetReservationStatus: rezSource={0}\r\n {1}", rezSource, ex.ToString());
				throw ex;
			}
		}
		#endregion

		#region Properties
		internal TrackTime TrackTime
		{
			get
			{
				return _trackTime;
			}
			set
			{
				_trackTime = value;
			}
		}

		internal SecurityToken Token
		{
			set
			{
				_token = value;
			}
		}
		#endregion

	}
}
