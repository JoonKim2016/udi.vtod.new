﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.SDS.DTO;
using UDI.SDS.DTO.Enum;
using UDI.SDS.GroupsService;
using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Common.Track;
using UDI.Utility.Serialization;
using System.Xml.Linq;
using System.Data;

namespace UDI.SDS.ServiceAdapter
{
	internal class ECarDrvServiceAdapter : BaseSetting
	{
		#region Fields
		TrackTime _trackTime;
		#endregion

		#region Constructors
		private ECarDrvServiceAdapter()
		{
		}

		internal ECarDrvServiceAdapter(TrackTime trackTime)
		{
			_trackTime = trackTime;
		}
		#endregion

		#region Internal
		//this method is not real time
		internal driver_info GetECarDriverInfo(string fleetId, string dispatchTripId)
		{
			string strResult = null;
			try
			{
				logger.Info("Send to SuperShuttle GetECarDriverInfo");

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
				#endregion

				//using (var client = new TaxiDrvService.TaxiDrvServiceSoapClient())
				//{
				//	//client.get_ztrip_drivers();
				//	strResult = client.driver_info_voucher(fleetId, dispatchTripId);
				//}

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetDiscountId");
				#endregion

				var result = XDocument.Parse(strResult).XmlDeserialize<driver_info>();
				if (result == null) result = new driver_info { error = strResult };
				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call GetECarDriverInfo {0}", ex.StackTrace);
				throw ex;
			}
		}

		//This method is realtime
		internal DataSet GetECarDriverInfo(string fleetId, string driverId, string vehicleId)
		{
			DataSet result = null;
			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
			#endregion

			//using (var client = new TaxiDrvService.TaxiDrvServiceSoapClient())
			//{
			//	//client.get_ztrip_drivers();
			//	result = client.get_driver_info(fleetId, driverId, vehicleId);
			//}

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetTaxiDriverInfo (Realtime)");
			#endregion

			return result;
		}

		//This method is for filtering out GetVehicle info from DDS or CCSi
		internal DataSet GetZtripDriver(string fleetId)
		{
			DataSet result = null;
			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
			#endregion

			//using (var client = new TaxiDrvService.TaxiDrvServiceSoapClient())
			//{
			//	//client.get_ztrip_drivers();
			//	result = client.get_ztrip_drivers(fleetId);
			//}

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetZtripDriver");
			#endregion

			return result;
		}
		#endregion

		#region Properties
		internal TrackTime TrackTime
		{
			get
			{
				return _trackTime;
			}
			set
			{
				_trackTime = value;
			}
		}
		#endregion
	}
}
