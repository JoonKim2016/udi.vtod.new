﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.SDS.DTO;
using UDI.SDS.DTO.Enum;
using UDI.SDS.FranchiseService;
using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Common.Track;
using UDI.Utility.Serialization;

namespace UDI.SDS.ServiceAdapter
{
	internal class FranchiseServiceAdapter : BaseSetting
	{
		#region Fields
		SecurityToken _token;
		TrackTime _trackTime;
		#endregion

		#region Constructors
		private FranchiseServiceAdapter()
		{
		}

		internal FranchiseServiceAdapter(SecurityToken token, TrackTime trackTime)
		{
			_token = token;
			_trackTime = trackTime;
		}
		#endregion

		#region Internal
		/// <summary>
		/// Used for Charter Reservation
		/// </summary>
		internal string GetFranchiseLocale(string franchiseCode)
		{
			var franchiseLocale = string.Empty;

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
			#endregion

			using (var client = new FranchiseService.FranchiseServiceClient())
			{
				var result = client.GetFranchiseRecord(_token, franchiseCode);
				if (result != null)
					franchiseLocale = result.Locale;
			}

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetFranchiseRecord");
			#endregion

			return franchiseLocale;
		}

		internal string GetFranchiseCode(string airportCode, string postalCode, int tripDirection)
		{
			try
			{
				string franchiseCode = string.Empty;

				logger.InfoFormat("Send to SuperShuttle \r\n AirportCode={0},PostalCode={1},TripDirection={2}", airportCode, postalCode, tripDirection);

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
				#endregion

				using (var client = new FranchiseService.FranchiseServiceClient())
				{
					var franchise = client.GetServiceFranchise(_token, airportCode, postalCode, tripDirection);
					if (franchise != null)
						franchiseCode = franchise.FranchiseCode;
				}

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetServiceFranchise");
				#endregion


				return franchiseCode;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call FranchiseService.GetServiceFranchise {0}", ex.StackTrace);
				throw ex;
			}
		}
		#endregion

		#region Properties
		internal TrackTime TrackTime
		{
			get
			{
				return _trackTime;
			}
			set
			{
				_trackTime = value;
			}
		}

		internal SecurityToken Token
		{
			set
			{
				_token = value;
			}
		}
		#endregion
	}
}
