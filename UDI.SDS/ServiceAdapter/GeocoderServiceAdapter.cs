﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.SDS.DTO;
using UDI.SDS.DTO.Enum;
using UDI.SDS.GeocoderService;
using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Common.Track;
using UDI.Utility.Serialization;

namespace UDI.SDS.ServiceAdapter
{
	internal class GeocoderServiceAdapter : BaseSetting
	{
		#region Fields
		SecurityToken _token;
		TrackTime _trackTime;
		#endregion

		#region Constructors
		private GeocoderServiceAdapter()
		{
		}

		internal GeocoderServiceAdapter(SecurityToken token, TrackTime trackTime)
		{
			_token = token;
			_trackTime = trackTime;
		}
		#endregion

		#region Internal
		internal void ModifyAddress(Location location, out bool modified)
		{
			modified = false; 
			try
			{
				GeocoderService.AddressRecord addressRecord = new GeocoderService.AddressRecord();

				addressRecord.PostalCode = location.PostalCode;
				addressRecord.StreetName = location.StreetName;
				addressRecord.StreetNumber = location.StreetNumber;
				addressRecord.CountrySubDivision = location.StateOrProvince;
				addressRecord.Municipality = location.City;
				addressRecord.CountryCode = string.IsNullOrEmpty(location.Country) ? "US" : location.Country;
				addressRecord.Latitude = location.Latitude;
				addressRecord.Longitude = location.Longitude;

				logger.InfoFormat("Send to SuperShuttle \r\n{0}", addressRecord.XmlSerialize());


				using (var client = new GeocoderService.GeocoderClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
					#endregion

					var result = client.GeocodeAddress(_token, addressRecord);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GeocodeAddress");
					#endregion

					if (result.HasMatches)
					{
						//get the highest match record
						var maxAccuracy = 100; // result.AddressMatchesArray.Max(a => a.Accuracy);
						var matchedAddress = result.AddressMatchesArray.Where(r => r.Accuracy == maxAccuracy);
						if (matchedAddress != null)
						{
							var selectesMatchedAddress = result.AddressMatchesArray.Where(r => r.Accuracy == maxAccuracy).First();

							location.Latitude = selectesMatchedAddress.Latitude;
							location.Longitude = selectesMatchedAddress.Longitude;
							//location.StreetNumber = selectesMatchedAddress.StreetNumber;
							//location.StreetName = selectesMatchedAddress.StreetName;
							//location.Country = selectesMatchedAddress.CountryCode;
							//location.StateOrProvince = selectesMatchedAddress.CountrySubDivision; 
							modified = true;
						}
					}
				}

			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call GeocoderService.GeocodeAddress {0}", ex.StackTrace);
			}
		}
		#endregion

		#region Properties
		internal TrackTime TrackTime
		{
			get
			{
				return _trackTime;
			}
			set
			{
				_trackTime = value;
			}
		}

		internal SecurityToken Token
		{
			set
			{
				_token = value;
			}
		}
		#endregion
	}
}
