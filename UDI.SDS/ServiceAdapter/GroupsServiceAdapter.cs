﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.SDS.DTO;
using UDI.SDS.DTO.Enum;
using UDI.SDS.GroupsService;
using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Common.Track;
using UDI.Utility.Serialization;

namespace UDI.SDS.ServiceAdapter
{
	internal class GroupsServiceAdapter : BaseSetting
	{
		#region Fields
		SecurityToken _token;
		TrackTime _trackTime;
		#endregion

		#region Constructors
		private GroupsServiceAdapter()
		{
		}

		internal GroupsServiceAdapter(SecurityToken token, TrackTime trackTime)
		{
			_token = token;
			_trackTime = trackTime;
		}
		#endregion

		#region Internal
		/// <summary>
		/// Validate the discount code against groups web service, which would return the corresponding integer DiscountID value.
		/// </summary>
		internal int ValidateDiscountCode(string discountCode)
		{
			if (string.IsNullOrEmpty(discountCode))
				return 0;

			var discountId = 0;

			try
			{
				logger.InfoFormat("Send to SuperShuttle \r\n discountCode={0}", discountCode);

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
				#endregion

				using (var client = new GroupsService.GroupsServiceClient())
				{
					discountId = client.ValidateDiscountCode(_token, discountCode);
				}

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "ValidateDiscountCode");
				#endregion

				return discountId;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call GroupsService.ValidateDiscountCode {0}", ex.StackTrace);
				throw ex;
			}
		}

		internal Discount GetDiscount(int discountID, string airportCode)
		{
			Discount result = null;
			try
			{
				logger.InfoFormat("Send to SuperShuttle \r\n discountID={0}, airportCode = {1}", discountID, airportCode);

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
				#endregion

				using (var client = new GroupsService.GroupsServiceClient())
				{
					result = client.GetDiscount(_token, discountID, airportCode);
				}

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "ValidateDiscountCode");
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call GroupsService.ValidateDiscountCode {0}", ex.StackTrace);
				throw ex;
			}
		}

		internal GetPromotionCodeTypeResponse GetPromotionCodeType(string promotionCode)
		{
			GetPromotionCodeTypeResponse result = null;
			try
			{
				logger.InfoFormat("Send to SuperShuttle \r\n discountID={0}, airportCode = {1}", promotionCode);

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
				#endregion

				var request = new GetPromotionCodeTypeRequest {PromotionCode = promotionCode };

				using (var client = new GroupsService.GroupsServiceClient())
				{
					result = client.GetPromotionCodeType(_token, request);
				}

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetPromotionCodeType");
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call GroupsService.GetPromotionCodeType {0}", ex.StackTrace);
				throw ex;
			}
		}
		
		internal GroupReservationDefaults GetDefaults(int discountID)
		{
			GroupReservationDefaults result = null;
			try
			{
				logger.InfoFormat("Send to SuperShuttle \r\n discountID={0}", discountID);

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
				#endregion

				using (var client = new GroupsService.GroupsServiceClient())
				{
					result = client.GetGroupDefaults(_token, discountID);
				}

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetDefaults");
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call GroupsService.GetDefaults {0}", ex.StackTrace);
				throw ex;
			}
		}
		#endregion

		#region Properties
		internal TrackTime TrackTime
		{
			get
			{
				return _trackTime;
			}
			set
			{
				_trackTime = value;
			}
		}

		internal SecurityToken Token
		{
			set
			{
				_token = value;
			}
		}
		#endregion
	}
}
