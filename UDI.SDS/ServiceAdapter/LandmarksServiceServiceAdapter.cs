﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.SDS.DTO;
using UDI.SDS.DTO.Enum;
using UDI.SDS.LandmarksService;
using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Common.Track;
using UDI.Utility.Serialization;
using UDI.Utility.Helper;

namespace UDI.SDS.ServiceAdapter
{
	internal class LandmarksServiceServiceAdapter : BaseSetting
	{
		#region Fields
		SecurityToken _token;
		TrackTime _trackTime;
		#endregion

		#region Constructors
		private LandmarksServiceServiceAdapter()
		{
		}

		internal LandmarksServiceServiceAdapter(SecurityToken token, TrackTime trackTime)
		{
			_token = token;
			_trackTime = trackTime;
		}
		#endregion

		#region Internal
		internal LandmarksService.LandmarkRecord GetLandmarksByCoordinates(string airportCode, Location location)
		{
			try
			{
				if (location.Latitude == 0 || location.Longitude == 0)
					return null;

				LandmarksService.LandmarkRecord result = null;
				LandmarksService.LandmarkRecords landmarkRecords = null;
				var request = new LandmarksService.GetLandmarkByCoordinatesRequest();

				request.Latitude = location.Latitude.ToDouble();
				request.Longitude = location.Longitude.ToDouble();
				request.AirportCode = airportCode;
				if (string.IsNullOrEmpty(location.StreetNumber) || location.StreetNumber.Contains("-") || location.StreetNumber.Contains("–"))//-–
				{
					request.StreetNumber = string.Empty;
				}
				else
				{
					request.StreetNumber = location.StreetNumber;
				}

				logger.InfoFormat("Send to SuperShuttle \r\n{0}", request.XmlSerialize());

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
				#endregion

				using (var client = new LandmarksService.LandmarksServiceClient())
				{
					landmarkRecords = client.GetLandmarkByCoordinates(_token, request);
				}

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetLandmarkByAddress2");
				#endregion

				if (landmarkRecords.LandmarkRecordsArray != null && landmarkRecords.LandmarkRecordsArray.Any())
				{
					result = landmarkRecords.LandmarkRecordsArray.First();
				}

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call LandmarksService.GetLandmarkByAddress2 {0}", ex.StackTrace);
				throw ex;
			}
		}

		internal LandmarksService.LandmarkRecord GetLandmarkRecordByAddress(string airportCode, Location location)
		{
			try
			{
				if (string.IsNullOrEmpty(location.StateOrProvince) || string.IsNullOrEmpty(location.StreetName) || string.IsNullOrEmpty(location.StreetNumber))
					return null;

				LandmarksService.LandmarkRecord landmarkRecord = null;
				var request = new LandmarksService.GetLandmarkByAddressRequest();
				var addressRecord = new LandmarksService.AddressRecord();

				addressRecord.PostalCode = location.PostalCode;
				addressRecord.StreetName = location.StreetName;
				addressRecord.StreetNumber = location.StreetNumber;
				addressRecord.CountrySubDivision = location.StateOrProvince;
				addressRecord.LocationName = location.PropertyName;
				addressRecord.Latitude = location.Latitude;
				addressRecord.Longitude = location.Longitude;

				request.Address = addressRecord;
				request.AirportCode = airportCode;
				request.AlwaysMatch = false;//By default, this value is always false

				logger.InfoFormat("Send to SuperShuttle \r\n{0}", request.XmlSerialize());

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
				#endregion

				using (var client = new LandmarksService.LandmarksServiceClient())
				{
					landmarkRecord = client.GetLandmarkByAddress2(_token, request);
				}

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetLandmarkByAddress2");
				#endregion

				return landmarkRecord;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call LandmarksService.GetLandmarkByAddress2 {0}", ex.StackTrace);
				throw ex;
			}
		}

		internal List<UDI.SDS.LandmarksService.LandmarkRecord> CharterSearchLandmark(int locationId, string searchExpression)
		{
			List<UDI.SDS.LandmarksService.LandmarkRecord> result = null;

			using (var landmarksClient = new LandmarksService.LandmarksServiceClient())
			{
				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
				#endregion

				var landmarksList = landmarksClient.CharterSearchLandmark(_token, locationId, searchExpression);

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "CharterSearchLandmark");
				#endregion

				if (landmarksList != null)
				{
					result = landmarksList;
				}
			}

			return result;
		}

		internal LandmarksService.LandmarkRecords GetGroupLandmarks(int groupID, string airportCode)
		{
			LandmarksService.LandmarkRecords result = null;
			try
			{
				logger.InfoFormat("Send to SuperShuttle \r\n groupID={0}, airportCode = {1}", groupID, airportCode);

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
				#endregion

				using (var landmarksClient = new LandmarksService.LandmarksServiceClient())
				{
					result = landmarksClient.GetGroupLandmarks(_token, groupID, airportCode);
				}

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GroupGetLandmarks");
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call LandmarksService.GroupGetLandmarks {0}", ex.StackTrace);
				throw ex;
			}
		}
		#endregion

		#region Properties
		internal TrackTime TrackTime
		{
			get
			{
				return _trackTime;
			}
			set
			{
				_trackTime = value;
			}
		}

		internal SecurityToken Token
		{
			set
			{
				_token = value;
			}
		}
		#endregion
	}
}
