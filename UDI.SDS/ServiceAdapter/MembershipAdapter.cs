﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.SDS.DTO.Enum;
using UDI.SDS.MembershipService;
using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Common.Track;
using UDI.Utility.Serialization;
using UDI.VTOD.Common.Helper;
using UDI.SDS.DTO;

namespace UDI.SDS.ServiceAdapter
{
	internal class MembershipServiceAdapter : BaseSetting
	{
		#region Fields
		SecurityToken _token;
		TrackTime _trackTime;
		#endregion

		#region Constructors
		private MembershipServiceAdapter()
		{
		}

        internal MembershipServiceAdapter(SecurityToken token, TrackTime trackTime)
		{
			_token = token;
			_trackTime = trackTime;
		}
		#endregion

		#region Internal
        //internal RatesService.FareScheduleWithFees GetFees(Guid bookRequestID, RatesService.ServiceTypeRecord airportFare, RatesService.CharterServiceTypeRecord charterFare, TripDirection tripDirection, bool isRoundTrip, int payingPax, byte paymentType, DateTime pickupTime, string discountCode)
        //{
        //    try
        //    {
        //        RatesService.FareScheduleWithFees result = null;
        //        RatesService.FareScheduleRequestWithFees request = new RatesService.FareScheduleRequestWithFees();
        //        request.CurrencyID = (int)Currency.USD; //0 for USD
        //        request.PayingPax = (byte)payingPax;
        //        request.PaymentType = paymentType;
        //        request.DiscountCode = discountCode;

        //        if (tripDirection == TripDirection.Inbound || isRoundTrip)
        //        {
        //            request.InboundFareID = charterFare == null ? airportFare.FareID : charterFare.FareID;
        //            request.InboundTravelDate = pickupTime;
        //        }

        //        if (tripDirection == TripDirection.Outbound || isRoundTrip)
        //        {
        //            request.OutboundFareID = charterFare == null ? airportFare.FareID : charterFare.FareID;
        //            request.OutboundTravelDate = pickupTime;
        //        }

        //        #region Track
        //        if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Domain, "Call");
        //        #endregion

        //        using (var client = new RatesService.RatesServiceClient())
        //        {
        //            result = client.GetFareScheduleWithFees(_token, request);
        //        }

        //        #region Track
        //        if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetFareScheduleWithFees");
        //        #endregion

        //        return result;
        //    }
        //    catch (Exception ex)
        //    {
        //        logger.ErrorFormat("GetFees BookRequestID:{0}\r\n {1}", bookRequestID, ex.ToString());
        //        throw ex;
        //    }
        //}
		#endregion

		#region Properties
		internal TrackTime TrackTime
		{
			get
			{
				return _trackTime;
			}
			set
			{
				_trackTime = value;
			}
		}

		internal SecurityToken Token
		{
			set
			{
				_token = value;
			}
		}
		#endregion
	}
}
