﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.Track;
using UDI.VTOD.Common.DTO.Enum;
using UDI.SDS.DTO;
using UDI.Utility.Helper;
using UDI.Utility.Serialization;
using UDI.VTOD.Common.DTO.OTA;
using UDI.SDS.DTO.Enum;
using UDI.VTOD.Common.Helper;
using UDI.SDS.MembershipService;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Web;

namespace UDI.SDS.ServiceAdapter
{
	public class MembershipServiceAdapter : BaseSetting
	{
		#region Fields
		SecurityToken _token;
		TrackTime _trackTime;
		#endregion

		#region Constructors
		private MembershipServiceAdapter()
		{
		}

		internal MembershipServiceAdapter(SecurityToken token, TrackTime trackTime)
		{
			_token = token;
			_trackTime = trackTime;
		}
		#endregion

		#region Internal

		internal MembershipService.MembershipRecord CreateMembership(MembershipService.MembershipRecord membershipRecord, string password)
		{
			try
			{
				logger.InfoFormat("Send to SuperShuttle CreateMembershipRecord \r\n ");
				using (var client = new MembershipService.MembershipClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
					#endregion

					var result = client.CreateMembershipRecord(_token, membershipRecord, password);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "CreateMembership");
					#endregion

					return result;

				}
			}
			catch (FaultException<SDS.MembershipService.ArgumentFault> ex)
			{
				if (ex.Detail.Message.ToLower().Contains("email"))
				{
					throw VTOD.Common.DTO.VtodException.CreateFieldFormatValidationException("Email", "Correct Email Address");// new Exception(Messages.Membership_WrongEmailAddress);
				}
				else
				{
					throw new Exception(ex.Detail.Message);
				}

			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call MembershipService.CreateMembership {0}", ex.StackTrace);
				throw ex;
			}
		}

		internal MembershipService.MembershipLocationRecord MemberAddLocation(MembershipService.AddressRecord addressRecord, int memberId)
		{
			try
			{
				logger.InfoFormat("Send to SuperShuttle CreateMembershipLocationRecord \r\n ");
				using (var client = new MembershipService.MembershipClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
					#endregion

					var result = client.CreateMembershipLocationRecord(_token, addressRecord, memberId);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "MemberAddLocation");
					#endregion

					return result;

				}
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call MembershipService.MemberAddLocation {0}", ex.StackTrace);
				throw ex;
			}
		}

		internal bool MemberChangeEmailAddress(int memberId, string newEmailAddress)
		{
			try
			{
				logger.InfoFormat("Send to SuperShuttle UpdateMembershipEmailAddress \r\n ");
				using (var client = new MembershipService.MembershipClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
					#endregion

					var result = client.UpdateMembershipEmailAddress(_token, memberId, newEmailAddress);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "MemberChangeEmailAddress");
					#endregion

					return result;
				}
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call MembershipService.MemberChangeEmailAddress {0}", ex.StackTrace);
				throw ex;
			}
		}

		internal bool MemberChangePassword(string emailAddress, string oldPassword, string newPassword)
		{
			try
			{
				logger.InfoFormat("Send to SuperShuttle ChangeMembershipPassword \r\n ");
				using (var client = new MembershipService.MembershipClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
					#endregion

					var result = client.ChangeMembershipPassword(_token, emailAddress, oldPassword, newPassword);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "MemberChangePassword");
					#endregion

					return result;
				}
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call MembershipService.MemberChangePassword {0}", ex.StackTrace);
				throw ex;
			}
		}

		internal bool MemberDeleteLocation(int locationId)
		{
			try
			{
				logger.InfoFormat("Send to SuperShuttle DeleteMembershipLocationRecord \r\n ");
				using (var client = new MembershipService.MembershipClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
					#endregion

					var result = client.DeleteMembershipLocationRecord(_token, locationId);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "MemberDeleteLocation");
					#endregion

					return result;
				}
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call MembershipService.MemberDeleteLocation {0}", ex.StackTrace);
				throw ex;
			}
		}

		internal MembershipService.PasswordRequest MemberForgotPassword(string emailAddress, EnumerationsWebSiteBrands sourceWebsite)
		{
			try
			{
				logger.InfoFormat("Send to SuperShuttle ForgotPasswordRequest \r\n ");
				using (var client = new MembershipService.MembershipClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
					#endregion

					//client.ResetPassword()

					var result = client.ForgotPasswordRequest(_token, emailAddress, sourceWebsite);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "MemberForgotPassword");
					#endregion

					return result;
				}
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call MembershipService.MemberForgotPassword {0}", ex.StackTrace);
				throw ex;
			}
		}

		internal MembershipService.MembershipLocationRecords MemberGetLocations(int memberId)
		{
			try
			{
				logger.InfoFormat("Send to SuperShuttle GetMembershipLocationRecords \r\n ");
				using (var client = new MembershipService.MembershipClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
					#endregion

					var result = client.GetMembershipLocationRecords(_token, memberId);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "MemberGetLocations");
					#endregion

					return result;
				}
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call MembershipService.MemberGetLocations {0}", ex.StackTrace);
				throw ex;
			}
		}

		internal MembershipService.MembershipReservations MemberGetReservation(int memberId, int rowCount)
		{
			try
			{
				logger.InfoFormat("Send to SuperShuttle GetWebMemberReservations \r\n ");

				using (var proxy = new MembershipClient())
				{
					using (new WebChannelFactory<IMembership>())
					{
						using (new OperationContextScope(proxy.InnerChannel))
						{

							#region Track
							if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
							#endregion

							if (rowCount > 0)
							{
								WebOperationContext.Current.OutgoingRequest.Headers.Add("SuperShuttle-RowCount", rowCount.ToString());
							}


							var result = proxy.GetWebMemberReservations(_token, memberId);

							#region Track
							if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "MemberGetReservation");
							#endregion

							return result;


						}
					}

				}



				//using (var client = new MembershipService.MembershipClient())
				//{
				//	#region Track
				//	if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
				//	#endregion

				//	if (rowCount > 0)
				//	{

				//		//WebOperationContext

				//		var messageHeader = MessageHeader.CreateHeader("SuperShuttle-RowCount", "http://www.SuperShuttle.com/WebServices/VTOD/Membership", rowCount.ToString());

				//		OperationContext.Current.OutgoingMessageHeaders.Add(messageHeader);

				//		//WebOperationContext.Current.OutgoingRequest.Headers.Add("SuperShuttle-RowCount", rowCount.ToString());
				//	}



				//	var result = client.GetWebMemberReservations(_token, memberId);

				//	#region Track
				//	if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "MemberGetReservation");
				//	#endregion

				//	return result;
				//}
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call MembershipService.MemberGetReservation {0}", ex.StackTrace);
				throw ex;
			}
		}

		internal MembershipService.MembershipRecord MemberLogin(string emailAddress, string password)
		{
			try
			{
				logger.InfoFormat("Send to SuperShuttle ValidateLogin \r\n ");
				using (var client = new MembershipService.MembershipClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
					#endregion

					var result = client.ValidateLogin(_token, emailAddress, password);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "MemberLogin");
					#endregion

					return result;

				}
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call MembershipService.MemberLogin {0}", ex.StackTrace);
				throw ex;
			}
		}

		internal bool MemberUpdateLocation(MembershipService.MembershipLocationRecord locationRecord, int memberId)
		{
			try
			{
				logger.InfoFormat("Send to SuperShuttle UpdateMembershipLocationRecord \r\n ");
				using (var client = new MembershipService.MembershipClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
					#endregion

					var result = client.UpdateMembershipLocationRecord(_token, locationRecord, memberId);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "MemberUpdateLocation");
					#endregion

					return result;
				}
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call MembershipService.MemberUpdateLocation {0}", ex.StackTrace);
				throw ex;
			}
		}

		internal bool MemberUpdateProfile(MembershipService.MembershipRecord membershipRecord)
		{
			try
			{
				logger.InfoFormat("Send to SuperShuttle UpdateMembershipRecord \r\n ");
				using (var client = new MembershipService.MembershipClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
					#endregion

					var result = client.UpdateMembershipRecord(_token, membershipRecord);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "MemberUpdateProfile");
					#endregion

					return result;
				}
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call MembershipService.MemberUpdateProfile {0}", ex.StackTrace);
				throw ex;
			}
		}

		internal void LinkReservation(long vtodTripID, int sdsRezID, int memberID)
		{
			try
			{
				var request = new MembershipService.LinkReservationRequest();

				request.VtodTripID = vtodTripID;
				request.rezID = sdsRezID;
				request.memberID = memberID;


				logger.InfoFormat("Send to SuperShuttle \r\n ");
				using (var client = new MembershipService.MembershipClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
					#endregion

					client.LinkReservation(_token, request);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "LinkReservation");
					#endregion

				}
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call MembershipService.LinkReservation {0}", ex.StackTrace);
				throw ex;
			}
		}

        internal void DelinkReservation(long vtodTripID, int sdsRezID, int memberID)
        {
            try
            {
                logger.InfoFormat("Send to SuperShuttle \r\n ");
                using (var client = new MembershipClient())
                {
                    #region Track
                    if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
                    #endregion

                    client.DetachRez(_token, memberID, sdsRezID, (int)vtodTripID);

                    #region Track
                    if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "DelinkReservation");
                    #endregion

                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Call MembershipService.DelinkReservation {0}", ex.StackTrace);
                throw ex;
            }
        }

        internal PasswordRequest GetPasswordRequest(Guid requestGuid)
		{
			PasswordRequest result = null;

			try
			{
				//var request = new MembershipService.ResetPasswordRequest { };


				logger.InfoFormat("Send to SuperShuttle \r\n ");
				using (var client = new MembershipService.MembershipClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
					#endregion

					result = client.GetPasswordRequest(_token, requestGuid);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "LinkReservation");
					#endregion

				}
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call MembershipService.GetPasswordRequest {0}", ex.StackTrace);
				throw ex;
			}

			return result;
		}

		internal bool ResetPassword(int memberID, string newPassword, Guid requestGuid)
		{
			var result = false;

			try
			{
				//var request = new MembershipService.ResetPasswordRequest { };


				logger.InfoFormat("Send to SuperShuttle \r\n ");
				using (var client = new MembershipService.MembershipClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
					#endregion

					result = client.ResetPassword(_token, memberID, newPassword, requestGuid);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "ResetPassword");
					#endregion

				}
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call MembershipService.ResetPassword {0}", ex.StackTrace);
				throw ex;
			}

			return result;
		}

		internal CommunicationSettings GetProfileCommunicationSettings(int memberID)
		{
			try
			{
				var request = new GetCommunicationSettingsRequest { MemberID = memberID };// = memberID;


				logger.InfoFormat("Send to SuperShuttle \r\n ");
				using (var client = new MembershipService.MembershipClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
					#endregion

					var result = client.GetProfileCommunicationSettings(_token, request);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetProfileCommunicationSettings");
					#endregion

					return result;
				}
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call MembershipService.GetProfileCommunicationSettings {0}", ex.StackTrace);
				throw ex;
			}
		}

		internal UDI.SDS.MembershipService.CorporateAccount GetCorporateAccountKey(string corporation, string corporateUserId)
		{
			try
			{
				var request = new GetCorporateKeyRequest { Corporation = corporation, UserId = corporateUserId };// = memberID;


				logger.InfoFormat("Send to SuperShuttle \r\n ");
				using (var client = new MembershipService.MembershipClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
					#endregion

					var result = client.GetCorporateAccountKey(_token, request);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetCorporateAccountKey");
					#endregion

					return result;
				}
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call MembershipService.GetCorporateAccountKey {0}", ex.StackTrace);
				throw ex;
			}
		}

		internal bool MemberDelete(string emailAddress, string password)
		{
			try
			{
				logger.InfoFormat("Send to SuperShuttle ValidateLogin \r\n ");
				using (var client = new MembershipService.MembershipClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
					#endregion

					var result = client.DeleteMembershipRecord(_token, emailAddress, password);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "MemberDelete");
					#endregion

					return result;

				}
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call MembershipService.MemberDelete {0}", ex.StackTrace);
				throw ex;
			}
		}

		internal bool SetPhoneNumberVerified(bool isVerified, int memberId, string phoneNumber)
		{
			try
			{
				logger.InfoFormat("Send to SuperShuttle ValidateLogin \r\n ");
				using (var client = new MembershipService.MembershipClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
					#endregion

					var resquest = new PhoneNumberVerificationRequest { IsVerified = isVerified, MemberId = memberId, PhoneNumber = phoneNumber };

					var result = client.SetPhoneNumberVerified(_token, resquest);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "SetPhoneNumberVerified");
					#endregion

					return result;

				}
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call MembershipService.SetPhoneNumberVerified {0}", ex.StackTrace);
				throw ex;
			}
		}

        internal ActivateCreditsResponse CreateNewZTripCredit(decimal Amount, int memberId, ZTripCreditType requestType)
        {
            try
            {
                logger.InfoFormat("Send to SuperShuttle ValidateLogin \r\n ");
                using (var client = new MembershipService.MembershipClient())
                {
                    #region Track
                    if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
                    #endregion

                    var request = new NewZTripCredit { Amount = Amount, MemberId = memberId, ZTripCreditType = requestType };

                    var result = client.CreateNewZTripCredit(_token, request);

                    #region Track
                    if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "CreateNewZTripCredit");
                    #endregion

                    return result;

                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Call MembershipService.CreateNewZTripCredit {0}", ex.StackTrace);
                throw ex;
            }
        }

        internal MembershipCreditCardAccount PatchCredit(PatchCreditCardRequest request)
        {
            try
            {
                logger.InfoFormat("Send to SuperShuttle ValidateLogin \r\n ");
                using (var client = new MembershipService.MembershipClient())
                {
                    #region Track
                    if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
                    #endregion
                    var result = client.PatchCreditCard(_token, request);
                    #region Track
                    if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "PatchCredit");
                    #endregion

                    return result;

                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Call MembershipService.PatchCredit {0}", ex.StackTrace);
                throw ex;
            }
        }

		#endregion

		#region Properties
		internal TrackTime TrackTime
		{
			get
			{
				return _trackTime;
			}
			set
			{
				_trackTime = value;
			}
		}

		internal SecurityToken Token
		{
			set
			{
				_token = value;
			}
		}
		#endregion

	}
}
