﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.SDS.RatesService
{
	public partial class CharterServiceTypeRecord
	{
		public string FranchiseLocale { get; set; }
		public int SubFleetOrderID { get; set; }

		public int GetSubFleetOrderID()
		{
			var result = 3;
			try
			{
				if (!string.IsNullOrWhiteSpace(SubFleetField))
				{
					switch (SubFleetField.ToLower())
					{
						case "ecar":
							result = 1;
							break;
						case "exprs":
						case "express":
							result = 2;
							break;
					}
				}
			}
			catch
			{ }
			return result;
		}

	}
}
