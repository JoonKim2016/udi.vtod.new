﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.SDS.RatesService
{
	public partial class ServiceInfo
	{
		public bool SplitServiced { get; set; }
		public int DiscountID { get; set; }
		public string DiscountName { get; set; }
		public string DiscountDescription { get; set; }
		public int CurrencyID { get; set; }
		public string CurrencySymbol { get; set; }
		public string CurrencyType { get; set; }
		public int LinkedID { get; set; }
		public decimal FareForOrdering { get; set; }//This is for RoundTrip Fare
		public int SubFleetForOrdering { get; set; }
		public int TripDirectionForOrdering { get; set; }

        public int CommissionID { get; set; }
       
        public int GetSubFleetOrderID()
		{
			var result = 3;
			try
			{
				if (!string.IsNullOrWhiteSpace(SubFleetField))
				{
					switch (SubFleetField.ToLower())
					{
						case "ecar":
							result = 1;
							break;
						case "exprs":
						case "express":
							result = 2;
							break;
					}
				}
			}
			catch 
			{}
			return result;
		}
	}
}
