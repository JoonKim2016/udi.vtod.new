﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.SDS.DTO;
using UDI.SDS.DTO.Enum;
using UDI.SDS.PaymentsService;
using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Common.Track;
using UDI.Utility.Serialization;
using UDI.VTOD.Common.DTO;
namespace UDI.SDS.ServiceAdapter
{
	internal class PaymentServiceAdapter : BaseSetting
	{
		#region Fields
		SecurityToken _token;
		TrackTime _trackTime;
		#endregion

		#region Constructors
		private PaymentServiceAdapter()
		{
		}

		internal PaymentServiceAdapter(SecurityToken token, TrackTime trackTime)
		{
			_token = token;
			_trackTime = trackTime;
		}
		#endregion

		#region Internal
        internal PreSaleVerificationResponse PreSaleCardVerification(int accountID, decimal amount, int memberID, int merchandID)
		{
            
            try
            {                
                #region Track
                if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
                #endregion

                var request = new PreSaleVerificationRequest();
                var sdsResult = new UDI.SDS.PaymentsService.PreSaleVerificationResponse();
                request = new PreSaleVerificationRequest();
                request.AccountId = accountID;
                request.Amount = amount;
                request.MemberId = memberID;
                request.MerchantFleetId = merchandID;

                using (var client = new PaymentsService.PaymentsServiceClient())
                {
                   sdsResult = client.PreSaleCardVerification(_token, request);
                }

                #region Track
                if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "PreSaleCardVerification");
                #endregion

                return sdsResult;
            }
            catch(Exception ex)
            {
                logger.ErrorFormat("Error : {0}", ex.StackTrace);
                throw ex;
            }
           
		}
		#endregion

		#region Properties
		internal TrackTime TrackTime
		{
			get
			{
				return _trackTime;
			}
			set
			{
				_trackTime = value;
			}
		}

		internal SecurityToken Token
		{
			set
			{
				_token = value;
			}
		}
		#endregion
	}
}
