﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.SDS.DTO.Enum;
//using UDI.SDS.RatesService;
using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Common.Track;
using UDI.Utility.Serialization;
using UDI.VTOD.Common.Helper;
using UDI.SDS.DTO;
using UDI.SDS.ProxyService;
//using UDI.SDS.ASAPService;

namespace UDI.SDS.ServiceAdapter
{
	internal class ProxyServiceAdapter : BaseSetting
	{
		#region Fields
		SecurityToken _token;
		TrackTime _trackTime;
		#endregion

		#region Constructors
		private ProxyServiceAdapter()
		{
		}

		internal ProxyServiceAdapter(SecurityToken token, TrackTime trackTime)
		{
			_token = token;
			_trackTime = trackTime;
		}
		#endregion

		#region Internal
		internal List<ProxyService.GetSdsGpsResponse> GetSdsGps(ProxyService.GetSdsGpsRequest request)
		{
			try
			{
				List<ProxyService.GetSdsGpsResponse> result = null;
				logger.InfoFormat("Send to SuperShuttle \r\n ");
				using (var client = new ProxyService.TaxiClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
					#endregion

					result = client.GetSdsGps(_token, request);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "CreatCharterASAPRequest");
					#endregion

				}

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call GetSdsGps {0}", ex.StackTrace);
				throw ex;
			}
		}
		#endregion

		#region Properties
		internal TrackTime TrackTime
		{
			get
			{
				return _trackTime;
			}
			set
			{
				_trackTime = value;
			}
		}

		internal SecurityToken Token
		{
			set
			{
				_token = value;
			}
		}
		#endregion
	}
}
