﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.SDS.DTO.Enum;
using UDI.SDS.RatesService;
using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Common.Track;
using UDI.Utility.Serialization;
using UDI.VTOD.Common.Helper;
using UDI.SDS.DTO;
using UDI.Utility.Helper;

namespace UDI.SDS.ServiceAdapter
{
	internal class RateServiceAdapter : BaseSetting
	{
		#region Fields
		SecurityToken _token;
		TrackTime _trackTime;
		#endregion

		#region Constructors
		private RateServiceAdapter()
		{
		}

		internal RateServiceAdapter(SecurityToken token, TrackTime trackTime)
		{
			_token = token;
			_trackTime = trackTime;
		}
		#endregion

		#region Internal

		/// <summary>
		/// New method for handling both one way and also round trip
		/// This method is just for airport service
		/// </summary>
		internal List<ServiceDetail> GetServiceRates(string airportCode, int fleetFilter, string inboundFranchiseCode, string outboundFranchiseCode, DateTime? inboundDate, DateTime? outboundDate, bool isInboundOrigin, bool isRoundTrip, int masterLandmarkID, string postalCode, string subZipName, int payingPax, string discountCode, decimal gratuityRate, decimal bookingFee, int directBillAccountId, string vehicleCode, UDI.VTOD.Common.DTO.Enum.Currency currency,decimal latitude,decimal longitude, bool? IsAccessible, bool? IsInboundRideNow, bool? IsOutboundRideNow,string UserId,bool? RemoveCallCenterBookingFee,string CultureCode)
		{
			try
			{
				List<ServiceDetail> result = null;
				var request = new ServiceRequest();

				request.AirportCode = airportCode;
				request.CultureCode = CultureCode; //ToDo: do not use hardcoding
				request.CurrencyID = 0; //ToDo: do not use hardcoding
				request.FleetFilter = fleetFilter;
				request.InboundFranchiseCode = !string.IsNullOrWhiteSpace(inboundFranchiseCode) ? inboundFranchiseCode : outboundFranchiseCode;
				request.OutboundFranchiseCode = !string.IsNullOrWhiteSpace(outboundFranchiseCode) ? outboundFranchiseCode : inboundFranchiseCode;
				if (inboundDate.HasValue)
					request.InboundTravelDate = inboundDate.Value;
				if (outboundDate.HasValue)
					request.OutboundTravelDate = outboundDate.Value;
				request.IsInboundOrigin = isInboundOrigin;
				request.MasterLandmarkID = masterLandmarkID < 0 ? 0 : masterLandmarkID; //the GetServiceRates will use the ZipcodeFare if masterlandmarkid is 0 otherwise it uses landmarkfare
				request.PayingPassengers = payingPax;
				request.RoundTrip = isRoundTrip;
				request.SubZipName = subZipName;
				request.Zip = postalCode;
				request.DiscountCode = discountCode;
				request.GratuityRate = gratuityRate / 100;
				request.DirectBillAccountId = directBillAccountId;
				request.VehicleType = vehicleCode.ToInt32();
				request.CurrencyID = (int)currency;
                request.IsAccessible =IsAccessible.ToBool();
                request.IsInboundRideNow =IsInboundRideNow.ToBool();
                request.IsOutboundRideNow =IsOutboundRideNow.ToBool();
                #region Latitude/longitude
                request.CustomerLatitude = latitude.ToDouble();
                request.CustomerLongitude = longitude.ToDouble();
                #endregion
                #region BookingFee
                decimal partnerBookingFeeInbound = 0;
				decimal partnerBookingFeeOutbound = 0;

				if (isRoundTrip)
				{
					partnerBookingFeeInbound = bookingFee;
					partnerBookingFeeOutbound = bookingFee;
				}
				else
				{
					if (isInboundOrigin)
						partnerBookingFeeInbound = bookingFee;
					else
						partnerBookingFeeOutbound = bookingFee;
				}

				request.PartnerBookingFeeInbound = partnerBookingFeeInbound;
				request.PartnerBookingFeeOutbound = partnerBookingFeeOutbound;
                #endregion
                //As told by Doug to comment out on 30th Jan'2017
                //request.CsrUserId= UserId;
                //request.RemoveCallCenterBookingFee =RemoveCallCenterBookingFee.ToBool();

                logger.InfoFormat("Send to SuperShuttle \r\n{0}", request.XmlSerialize());

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
				#endregion

				using (var client = new RatesService.RatesServiceClient())
				{
					result = client.GetServiceRates(_token, request);
					//result.First().Services.First().SubFleet
				}
				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetServiceRates");
				#endregion


				#region Adjust Result
				if (result != null && result.Any())
				{
					var linkedId = 0;
					foreach (ServiceDetail serviceDetail in result)
					{

						if (serviceDetail.Services != null && serviceDetail.Services.Any())
						{
							serviceDetail.Services.ForEach(s =>
							{
								s.SubFleetForOrdering = s.GetSubFleetOrderID();
								s.DiscountID = serviceDetail.DiscountID;
								s.DiscountName = serviceDetail.DiscountName;
								s.DiscountDescription = serviceDetail.DiscountStatus;
								s.CurrencyID = serviceDetail.CurrencyID;
								s.CurrencySymbol = serviceDetail.CurrencySymbol;
								s.CurrencyType = serviceDetail.CurrencyType;
								s.FareForOrdering = s.FirstPassengerFare + s.AdditionalPassengerFare * (payingPax - 1);
								s.LinkedID = 0;
                                s.Commission = s.Commission;
                                s.CommissionID = serviceDetail.CommissionID;
								s.TripDirectionForOrdering = 0;
							});

							if (serviceDetail.Services.Count() > 1)
							{
								linkedId++;
								var tripDirectionForOrdering = 0;
								decimal fareForOrdering = serviceDetail.Services.Sum(s => s.FareForOrdering);
								var subFleetForOrdering = serviceDetail.Services.Min(s => s.SubFleetForOrdering);
								serviceDetail.Services.ForEach(s =>
									{
										tripDirectionForOrdering++;
										s.LinkedID = linkedId;
										s.FareForOrdering = fareForOrdering;
										s.TripDirectionForOrdering = tripDirectionForOrdering;
										s.SubFleetForOrdering = subFleetForOrdering;
									});

							}
						}
					}
				}
				#endregion
                
				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call RatesService.GetServiceRates {0}", ex.StackTrace);
				throw ex;
			}
		}

		internal List<RatesService.CharterServiceTypeRecord> GetPointToPointCharterRates(RatesService.PointToPointCharterRateRequest request)
		{
			//request.CharterLocationsID
			//request.CurrencyID = 0;

			List<RatesService.CharterServiceTypeRecord> result = null;

			request.GratuityRate = request.GratuityRate / 100;

			using (var client = new RatesService.RatesServiceClient())
			{
				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
				#endregion

				result = client.GetPointToPointCharterRates(_token, request).ToList();

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetPointToPointCharterRates");
				#endregion

			}

			#region Adjust Result
			if (result != null && result.Any())
			{
				foreach (var item in result)
				{
					item.SubFleetOrderID = item.GetSubFleetOrderID();
				}
			}
			#endregion


			return result;
		}

		internal List<RatesService.CharterServiceTypeRecord> GetHourlyCharterRates(int fleetFilter, string postalCode, int totalMinutes/*, int PayingPax, int wheelChairs, int FreePax*/)
		{
			try
			{
				//var msg = string.Empty;

				if (string.IsNullOrEmpty(postalCode))
					throw VTOD.Common.DTO.VtodException.CreateFieldRequiredValidationException("PostalCode"); // new Exception(string.Format(Messages.UDI_SDS_PostalCodeRequired, "GetHourlyCharterRates"));


				List<RatesService.CharterServiceTypeRecord> result = new List<RatesService.CharterServiceTypeRecord>();
				RatesService.HourlyCharterRateRequest request = new RatesService.HourlyCharterRateRequest();
				request.FleetFilter = fleetFilter;
				request.TotalMinutes = totalMinutes;
				request.PostalCode = postalCode;
				//request.

				logger.InfoFormat("GetHourlyCharterRates Start...\r\n {0}", request.XmlSerialize());

				using (var client = new RatesService.RatesServiceClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
					#endregion

					result = client.GetHourlyCharterRates(_token, request).ToList();

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetHourlyCharterRates");
					#endregion

					//Check maximum PayingPax and minimum minutes, order by lowest HourlyRate
					//if (rates.Count > 0)
					//{
					//	rates = rates.OrderBy(r => r.HourlyRate).ToList();

					//	foreach (var rate in rates)
					//	{
					//		if (rate.MaxGuests >= (PayingPax + FreePax) && rate.MaxAccessibleGuests >= wheelChairs)//rate.MinimumDurationMinutes < totalMinutes (CharterMinutes is not used in SaveReservation, so no need to check here)
					//		{
					//			result.Add(rate);
					//			break;
					//		}
					//	}
					//}
					//else
					//	msg = "GetHourlyCharterRates.";
				}

				//if (!string.IsNullOrEmpty(msg))
				//	throw VTOD.Common.DTO.VtodException.CreateException(ExceptionType.SDS, 1003); // new Exception(Messages.General_NoMatchingRate);

				if (result.Count == 0)
					throw VTOD.Common.DTO.VtodException.CreateException(ExceptionType.SDS, 1003); // new Exception(Messages.General_NoMatchingRate);

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call RatesService.GetHourlyCharterRates {0}", ex.StackTrace);
				throw ex;
			}
		}

		internal FareScheduleWithFees GetFareScheduleWithFees(FareScheduleRequestWithFees fareScheduleRequestWithFees)
		{
			try
			{
				//var msg = string.Empty;
				FareScheduleWithFees result = new FareScheduleWithFees();

				using (var client = new RatesService.RatesServiceClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
					#endregion

					result = client.GetFareScheduleWithFees(_token, fareScheduleRequestWithFees);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetFareScheduleWithFees");
					#endregion

				}

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call RatesService.GetFareScheduleWithFees {0}", ex.StackTrace);
				throw ex;
			}
		}
		#endregion

		#region Properties
		internal TrackTime TrackTime
		{
			get
			{
				return _trackTime;
			}
			set
			{
				_trackTime = value;
			}
		}

		internal SecurityToken Token
		{
			set
			{
				_token = value;
			}
		}
		#endregion

		#region Old
		/// <summary>
		/// This method must be called to correctly calculate the total cost of a reservation so any applicable taxes, discounts and commissions can be applied. 
		/// This version of the method allows the client to specify a list of fees that have been added or modified. 
		/// </summary>
		//internal RatesService.FareScheduleWithFees GetFees(Guid bookRequestID, RatesService.ServiceTypeRecord airportFare, RatesService.CharterServiceTypeRecord charterFare, TripDirection tripDirection, bool isRoundTrip, int payingPax, byte paymentType, DateTime pickupTime, string discountCode)
		//{
		//	try
		//	{
		//		RatesService.FareScheduleWithFees result = null;
		//		RatesService.FareScheduleRequestWithFees request = new RatesService.FareScheduleRequestWithFees();
		//		request.CurrencyID = (int)Currency.USD; //0 for USD
		//		request.PayingPax = (byte)payingPax;
		//		request.PaymentType = paymentType;
		//		request.DiscountCode = discountCode;

		//		if (tripDirection == TripDirection.Inbound || isRoundTrip)
		//		{
		//			request.InboundFareID = charterFare == null ? airportFare.FareID : charterFare.FareID;
		//			request.InboundTravelDate = pickupTime;
		//		}

		//		if (tripDirection == TripDirection.Outbound || isRoundTrip)
		//		{
		//			request.OutboundFareID = charterFare == null ? airportFare.FareID : charterFare.FareID;
		//			request.OutboundTravelDate = pickupTime;
		//		}

		//		#region Track
		//		if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
		//		#endregion

		//		using (var client = new RatesService.RatesServiceClient())
		//		{
		//			result = client.GetFareScheduleWithFees(_token, request);
		//		}

		//		#region Track
		//		if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetFareScheduleWithFees");
		//		#endregion

		//		return result;
		//	}
		//	catch (Exception ex)
		//	{
		//		logger.ErrorFormat("GetFees BookRequestID:{0}\r\n {1}", bookRequestID, ex.ToString());
		//		throw ex;
		//	}
		//}

		/// <summary>
		/// Connect to SuperShuttle to get all landmark rates
		/// This method does not handle RoundTrip
		/// For round trip (also for all, use GetServiceRates method)
		/// </summary>
		//internal RatesService.ServiceTypes GetLandmarkRates(string airportCode, int fleetFilter, string franchiseCode, int masterLandmarkID, int tripDirection, string postalCode, int payingPax, int discountId)
		//{
		//	try
		//	{
		//		var result = new RatesService.ServiceTypes();
		//		RatesService.LandmarkFareRequest request = new RatesService.LandmarkFareRequest();
		//		request.AirportCode = airportCode;
		//		request.FleetFilter = fleetFilter;
		//		request.FranchiseCode = franchiseCode;
		//		request.MasterLandmarkID = masterLandmarkID;
		//		request.TripDirection = tripDirection;
		//		request.ZipCode = postalCode;
		//		request.PayingPax = payingPax;

		//		//if (!string.IsNullOrEmpty(discountCode))
		//		request.DiscountId = discountId; // getDiscountId(discountCode);
		//		//request
		//		logger.InfoFormat("Send to SuperShuttle \r\n{0}", request.XmlSerialize());

		//		#region Track
		//		if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
		//		#endregion

		//		using (var client = new RatesService.RatesServiceClient())
		//		{
		//			result = client.GetLandmarkFaresWithSurcharge(_token, request);
		//		}

		//		#region Track
		//		if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetLandmarkFaresWithSurcharge");
		//		#endregion

		//		return result;
		//	}
		//	catch (Exception ex)
		//	{
		//		logger.ErrorFormat("Call RatesService.GetLandmarkFaresWithSurcharge {0}", ex.StackTrace);
		//		throw ex;
		//	}
		//}

		/// <summary>
		/// Connect to SuperShuttle to get all zipCode rates
		/// </summary>
		//internal RatesService.ServiceTypes GetZipCodeRates(string airportCode, int fleetFilter, string franchiseCode, int tripDirection, string postalCode, string subZipName, int payingPax, int discountId)
		//{
		//	try
		//	{
		//		var result = new RatesService.ServiceTypes();
		//		RatesService.ZipCodeFareRequest request = new RatesService.ZipCodeFareRequest();
		//		request.AirportCode = airportCode;
		//		request.FleetFilter = fleetFilter;
		//		request.FranchiseCode = franchiseCode;
		//		request.TripDirection = tripDirection;
		//		request.ZipCode = postalCode;
		//		request.SubZipName = subZipName;
		//		request.PayingPax = payingPax;
		//		//if (!string.IsNullOrEmpty(discountCode))
		//		request.DiscountId = discountId;

		//		logger.InfoFormat("Send to SuperShuttle \r\n{0}", request.XmlSerialize());

		//		#region Track
		//		if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
		//		#endregion

		//		using (var client = new RatesService.RatesServiceClient())
		//		{
		//			result = client.GetZipCodeFaresWithSurcharge(_token, request);
		//			var resultXml = result.XmlSerialize();
		//			//var serviceRequest = new ServiceRequest();
		//			//var result2 = client.GetServiceRates(_token, request);

		//		}

		//		#region Track
		//		if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetZipCodeFaresWithSurcharge");
		//		#endregion

		//		return result;
		//	}
		//	catch (Exception ex)
		//	{
		//		logger.ErrorFormat("Call RatesService.GetZipCodeFaresWithSurcharge {0}", ex.StackTrace);
		//		throw ex;
		//	}
		//}

		#endregion
	}
}
