﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.Track;
using UDI.VTOD.Common.DTO.Enum;
using UDI.SDS.DTO;
using UDI.Utility.Helper;
using UDI.Utility.Serialization;
using UDI.VTOD.Common.DTO.OTA;
using UDI.SDS.DTO.Enum;
using UDI.VTOD.Common.Helper;
using UDI.SDS.ReservationsServiceSecure;
using UDI.VTOD.Common.DTO;
//using UDI.SDS.RatesService;

namespace UDI.SDS.ServiceAdapter
{
	internal class ReservationSecureServiceAdapter : BaseSetting
	{
		#region Fields
		SecurityToken _token;
		TrackTime _trackTime;
		#endregion

		#region Constructors
		private ReservationSecureServiceAdapter()
		{
		}

		internal ReservationSecureServiceAdapter(SecurityToken token, TrackTime trackTime)
		{
			_token = token;
			_trackTime = trackTime;
		}
		#endregion

		#region Internal
		#region Do not delete. In futur some parts may be used
		/// <summary>
		/// Send airport reservation request to SDS and get confirmation back
		/// </summary>
		//internal List<ReservationsServiceSecure.ReservationConfirmation> SaveAirportReservation(Segment departingSegment, Segment returningSegment, ReservationRequest request, RateResponse rateResponseFirstSegment, UDI.SDS.RatesService.FareScheduleWithFees firstSegmentFees, PickupTimeResponse pickupTimeResponseFirstSegment, RateResponse rateResponseSecondSegment, UDI.SDS.RatesService.FareScheduleWithFees secondSegmentFees, PickupTimeResponse pickupTimeResponseSecondSegment)
		//{
		//	#region Init
		//	var objectFactory = new Helper.ObjectFactory();
		//	#endregion

		//	try
		//	{
		//		List<ReservationsServiceSecure.ReservationConfirmation> result = null;
		//		var airportReservation = objectFactory.GetAirportReservationObject(departingSegment, returningSegment, request, rateResponseFirstSegment, firstSegmentFees, pickupTimeResponseFirstSegment, rateResponseSecondSegment, secondSegmentFees, pickupTimeResponseSecondSegment);

		//		logger.InfoFormat("Send to SuperShuttle \r\n{0}", airportReservation.XmlSerialize());


		//		#region Track
		//		if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Domain, "Call");
		//		#endregion

		//		using (var client = new ReservationsServiceSecure.ReservationsServiceSecureClient())
		//		{
		//			result = client.SaveReservation3(_token, airportReservation, new ReservationsServiceSecure.ClientVersionInfo()).ToList();
		//			//client.SaveReservation(token.ToReservationsServiceSecureToken(), airportReservation, pickupTimeResponse.AsapRequestId).ToList();
		//		}

		//		#region Track
		//		if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "SaveReservation3");
		//		#endregion

		//		return result;
		//	}
		//	catch (Exception ex)
		//	{
		//		logger.ErrorFormat("Call ReservationsServiceSecure.SaveReservation {0}", ex.StackTrace);

		//		if (ex.Message.ToLower().Contains("processing card"))
		//			throw new Exception(Messages.General_BadCreditCard);
		//		else
		//			throw ex;
		//	}
		//} 
		#endregion

		internal List<ReservationsServiceSecure.ReservationConfirmation> SaveAirportReservation(Segment departingSegment, Segment returningSegment, ReservationRequest request, RatesResponse rateResponseFirstSegment, PickupTimeResponse pickupTimeResponseFirstSegment, RatesResponse rateResponseSecondSegment, PickupTimeResponse pickupTimeResponseSecondSegment)
		{
			#region Init
			var objectFactory = new Helper.ObjectFactory();
			#endregion

			try
			{
				List<ReservationsServiceSecure.ReservationConfirmation> result = null;
				var airportReservation = objectFactory.GetAirportReservationObject(departingSegment, returningSegment, request, rateResponseFirstSegment, pickupTimeResponseFirstSegment, rateResponseSecondSegment, pickupTimeResponseSecondSegment);

				logger.InfoFormat("Send to SuperShuttle \r\n{0}", airportReservation.XmlSerialize().ToString().CleanCVVNumber().CleanCreditCardNumber());

				#region Affiliate Info
				if (request.AffiliateInfo != null)
				{
					airportReservation.AffiliateData = new AffiliateDate();
					airportReservation.AffiliateData.TrackingNumber = request.AffiliateInfo.TrackingNumber;
					airportReservation.AffiliateData.UserId = request.AffiliateInfo.UserID;
					airportReservation.AffiliateData.EmailAddress = request.AffiliateInfo.Email;
				}
				#endregion

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
				#endregion
				//airportReservation.Luggage = 12;
				//airportReservation.AirlineMilesDetails.RewardsID
				//airportReservation.ServiceNotes
				//airportReservation.Comments
				//airportReservation.SubscribeToPromotions
				//airportReservation.GuestAddress.u
				//airportReservation.InboundItinerary.RequestVehicleNumber;
				//airportReservation.OutboundItinerary.RequestVehicleNumber;
				//airportReservation.AllowSms

				using (var client = new ReservationsServiceSecure.ReservationsServiceSecureClient())
				{
					result = client.SaveReservation3(_token, airportReservation, new ReservationsServiceSecure.ClientVersionInfo()).ToList();
					//client.SaveReservation(token.ToReservationsServiceSecureToken(), airportReservation, pickupTimeResponse.AsapRequestId).ToList();
				}

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "SaveReservation3");
				#endregion

				return result;
			}
			catch (System.ServiceModel.FaultException<CreditCardDeclinedFault> ex)
			{
				logger.ErrorFormat("Call ReservationsServiceSecure.SaveCharterReservation {0}", ex.StackTrace);
				if (!string.IsNullOrWhiteSpace(ex.Message))
				{
					if (ex.Message.ToLower().Contains("prepaid") == true || (ex.Detail != null && !string.IsNullOrWhiteSpace(ex.Detail.Message) && ex.Detail.Message.ToLower().Contains("prepaid")))
					{
						throw VtodException.CreateException(ExceptionType.SDS, 2021);
					}
					else
					{
						throw VtodException.CreateException(ExceptionType.SDS, 2010);
					}
				}
				else
				{
					throw VtodException.CreateException(ExceptionType.SDS, 2010);
				}
			}
			catch (System.ServiceModel.FaultException<DirectBillPaymentFault> ex)
			{
				logger.ErrorFormat("Call ReservationsServiceSecure.SaveCharterReservation {0}", ex.StackTrace);
				if (ex.Message.Contains("password"))
				{
					throw VtodException.CreateException(ExceptionType.SDS, 2015);
				}
				else
				{
					throw VtodException.CreateException(ExceptionType.SDS, 2014);
				}
			}
			catch (System.ServiceModel.FaultException<DuplicateReservationFault> ex)
			{
				logger.ErrorFormat("Call ReservationsServiceSecure.SaveCharterReservation {0}", ex.StackTrace);
				throw VtodException.CreateException(ExceptionType.SDS, 2006);
			}
			catch (System.ServiceModel.FaultException<CreditCardExpiredFault> ex)
			{
				logger.ErrorFormat("Call ReservationsServiceSecure.SaveCharterReservation {0}", ex.StackTrace);
				throw VtodException.CreateException(ExceptionType.SDS, 2016);
			}
			catch (System.ServiceModel.FaultException<CreditCardExceptionFault> ex)
			{
				logger.ErrorFormat("Call ReservationsServiceSecure.SaveCharterReservation {0}", ex.StackTrace);
				throw VtodException.CreateException(ExceptionType.SDS, 2017);
			}
			catch (System.ServiceModel.FaultException<EventClosedFault> ex)
			{
				logger.ErrorFormat("Call ReservationsServiceSecure.SaveCharterReservation {0}", ex.StackTrace);
				throw VtodException.CreateException(ExceptionType.SDS, 2018);
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call ReservationsServiceSecure.SaveReservation {0}", ex.StackTrace);

				//if (ex.Message.ToLower().Contains("processing card"))
				//	throw new Exception(Messages.General_BadCreditCard);
				//else
				throw;
			}
		}

		internal ReservationsServiceSecure.CharterReservationConfirmation SaveCharterReservation(Segment segment, ReservationRequest request, UDI.SDS.RatesService.CharterServiceTypeRecord charterRate, PickupTimeResponse pickupTimeResponse)
		{
			#region Init
			var objectFactory = new Helper.ObjectFactory();
			#endregion

			try
			{
				ReservationsServiceSecure.ClientVersionInfo clientAppVersionInfo = new ReservationsServiceSecure.ClientVersionInfo();
				ReservationsServiceSecure.CharterReservationConfirmation result = null;
				var charterReservation = objectFactory.GetCharterReservationObjet(segment, request, charterRate, pickupTimeResponse);

				#region Affiliate Info
				if (request.AffiliateInfo != null)
				{
					charterReservation.AffiliateData = new AffiliateDate();
					charterReservation.AffiliateData.TrackingNumber = request.AffiliateInfo.TrackingNumber;
					charterReservation.AffiliateData.UserId = request.AffiliateInfo.UserID;
					charterReservation.AffiliateData.EmailAddress = request.AffiliateInfo.Email;
				}
				#endregion

				logger.InfoFormat("Send to SuperShuttle \r\n{0}", charterReservation.XmlSerialize().ToString().CleanCVVNumber().CleanCreditCardNumber());

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
				#endregion
				//charterReservation.Comments
				//charterReservation.ClientNotes
				//charterReservation.SubscribeToPromotions
				//charterReservation.subs

				using (var client = new ReservationsServiceSecure.ReservationsServiceSecureClient())
				{
					result = client.SaveCharterReservation(_token, charterReservation, clientAppVersionInfo);
				}

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "SaveCharterReservation");
				#endregion

				return result;
			}
			catch (System.ServiceModel.FaultException<CreditCardDeclinedFault> ex)
			{
				logger.ErrorFormat("Call ReservationsServiceSecure.SaveCharterReservation {0}", ex.StackTrace);
				if (!string.IsNullOrWhiteSpace(ex.Message))
				{
					if (ex.Message.ToLower().Contains("prepaid") == true || (ex.Detail != null && !string.IsNullOrWhiteSpace(ex.Detail.Message) && ex.Detail.Message.ToLower().Contains("prepaid")))
					{
						throw VtodException.CreateException(ExceptionType.SDS, 2021);
					}
					else
					{
						throw VtodException.CreateException(ExceptionType.SDS, 2010);
					}
				}
				else
				{
					throw VtodException.CreateException(ExceptionType.SDS, 2010);
				}
			}
			catch (System.ServiceModel.FaultException<DirectBillPaymentFault> ex)
			{
				logger.ErrorFormat("Call ReservationsServiceSecure.SaveCharterReservation {0}", ex.StackTrace);
				if (ex.Message.Contains("password"))
				{
					throw VtodException.CreateException(ExceptionType.SDS, 2015);
				}
				else
				{
					throw VtodException.CreateException(ExceptionType.SDS, 2014);
				}
			}
			catch (System.ServiceModel.FaultException<DuplicateReservationFault> ex)
			{
				logger.ErrorFormat("Call ReservationsServiceSecure.SaveCharterReservation {0}", ex.StackTrace);
				throw VtodException.CreateException(ExceptionType.SDS, 2006);
			}
			catch (System.ServiceModel.FaultException<CreditCardExpiredFault> ex)
			{
				logger.ErrorFormat("Call ReservationsServiceSecure.SaveCharterReservation {0}", ex.StackTrace);
				throw VtodException.CreateException(ExceptionType.SDS, 2016);
			}
			catch (System.ServiceModel.FaultException<CreditCardExceptionFault> ex)
			{
				logger.ErrorFormat("Call ReservationsServiceSecure.SaveCharterReservation {0}", ex.StackTrace);
				throw VtodException.CreateException(ExceptionType.SDS, 2017);
			}
			catch (System.ServiceModel.FaultException<EventClosedFault> ex)
			{
				logger.ErrorFormat("Call ReservationsServiceSecure.SaveCharterReservation {0}", ex.StackTrace);
				throw VtodException.CreateException(ExceptionType.SDS, 2018);
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call ReservationsServiceSecure.SaveCharterReservation {0}", ex.StackTrace);
				throw;
			}
		}

		internal ReservationsServiceSecure.CharterReservationConfirmation SaveHourlyReservation(Segment segment, ReservationRequest request, UDI.SDS.RatesService.CharterServiceTypeRecord charterRate, PickupTimeResponse pickupTimeResponse)
		{
			#region Init
			var objectFactory = new Helper.ObjectFactory();
			#endregion

			try
			{
				ReservationsServiceSecure.ClientVersionInfo clientAppVersionInfo = new ReservationsServiceSecure.ClientVersionInfo();
				ReservationsServiceSecure.CharterReservationConfirmation result = null;
				var hourlyReservation = objectFactory.GetHourlyReservationObjet(segment, request, charterRate, pickupTimeResponse);

				#region Affiliate Info
				if (request.AffiliateInfo != null)
				{
					hourlyReservation.AffiliateData = new AffiliateDate();
					hourlyReservation.AffiliateData.TrackingNumber = request.AffiliateInfo.TrackingNumber;
					hourlyReservation.AffiliateData.UserId = request.AffiliateInfo.UserID;
					hourlyReservation.AffiliateData.EmailAddress = request.AffiliateInfo.Email;
				}
				#endregion

				logger.InfoFormat("Send to SuperShuttle \r\n{0}", hourlyReservation.XmlSerialize().ToString().CleanCVVNumber().CleanCreditCardNumber());

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
				#endregion
				//charterReservation.Comments
				//charterReservation.ClientNotes
				//hourlyReservation.SubscribeToPromotions

				using (var client = new ReservationsServiceSecure.ReservationsServiceSecureClient())
				{
					result = client.SaveCharterReservation(_token, hourlyReservation, clientAppVersionInfo);
				}

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "SaveCharterReservation");
				#endregion

				return result;
			}
			catch (System.ServiceModel.FaultException<CreditCardDeclinedFault> ex)
			{
				logger.ErrorFormat("Call ReservationsServiceSecure.SaveCharterReservation {0}", ex.StackTrace);
				if (!string.IsNullOrWhiteSpace(ex.Message))
				{
					if (ex.Message.ToLower().Contains("prepaid") == true || (ex.Detail != null && !string.IsNullOrWhiteSpace(ex.Detail.Message) && ex.Detail.Message.ToLower().Contains("prepaid")))
					{
						throw VtodException.CreateException(ExceptionType.SDS, 2021);
					}
					else
					{
						throw VtodException.CreateException(ExceptionType.SDS, 2010);
					}
				}
				else
				{
					throw VtodException.CreateException(ExceptionType.SDS, 2010);
				}
			}
			catch (System.ServiceModel.FaultException<DirectBillPaymentFault> ex)
			{
				logger.ErrorFormat("Call ReservationsServiceSecure.SaveCharterReservation {0}", ex.StackTrace);
				if (ex.Message.Contains("password"))
				{
					throw VtodException.CreateException(ExceptionType.SDS, 2015);
				}
				else
				{
					throw VtodException.CreateException(ExceptionType.SDS, 2014);
				}
			}
			catch (System.ServiceModel.FaultException<DuplicateReservationFault> ex)
			{
				logger.ErrorFormat("Call ReservationsServiceSecure.SaveCharterReservation {0}", ex.StackTrace);
				throw VtodException.CreateException(ExceptionType.SDS, 2006);
			}
			catch (System.ServiceModel.FaultException<CreditCardExpiredFault> ex)
			{
				logger.ErrorFormat("Call ReservationsServiceSecure.SaveCharterReservation {0}", ex.StackTrace);
				throw VtodException.CreateException(ExceptionType.SDS, 2016);
			}
			catch (System.ServiceModel.FaultException<CreditCardExceptionFault> ex)
			{
				logger.ErrorFormat("Call ReservationsServiceSecure.SaveCharterReservation {0}", ex.StackTrace);
				throw VtodException.CreateException(ExceptionType.SDS, 2017);
			}
			catch (System.ServiceModel.FaultException<EventClosedFault> ex)
			{
				logger.ErrorFormat("Call ReservationsServiceSecure.SaveCharterReservation {0}", ex.StackTrace);
				throw VtodException.CreateException(ExceptionType.SDS, 2018);
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call ReservationsServiceSecure.SaveCharterReservation {0}", ex.StackTrace);
				throw ex;
			}
		}

		internal ChargeVtodTripResponse ChargeVtodTrip(ChargeVtodTripRequest chargeVtodTripRequest)
		{
			try
			{
				ChargeVtodTripResponse response = null;

				logger.InfoFormat("Send to SuperShuttle \r\n{0}", chargeVtodTripRequest.XmlSerialize());

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
				#endregion

				using (var client = new ReservationsServiceSecure.ReservationsServiceSecureClient())
				{
					response = client.ChargeVtodTrip(_token, chargeVtodTripRequest);
					//response.AmountCharged
				}

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "ChargeVtodTrip");
				#endregion

				logger.InfoFormat("Response from SuperShuttle \r\n{0}", response.XmlSerialize());

				return response;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call ReservationsServiceSecure.ChargeVtodTrip {0}", ex.StackTrace);
				throw ex;
			}
		}

		#region Old
		/// <summary>
		/// Send charter reservation request to SDS and get confirmation back
		/// </summary>
		//internal ReservationsServiceSecure.CharterReservationConfirmation SaveCharterReservation(Segment segment, ReservationRequest request, RateResponse rateResponse, UDI.SDS.RatesService.FareScheduleWithFees firstSegmentFees, PickupTimeResponse pickupTimeResponse, string franchiseLocale)
		//{
		//	#region Init
		//	var objectFactory = new Helper.ObjectFactory();
		//	#endregion

		//	try
		//	{
		//		ReservationsServiceSecure.ClientVersionInfo clientAppVersionInfo = new ReservationsServiceSecure.ClientVersionInfo();
		//		ReservationsServiceSecure.CharterReservationConfirmation result = null;
		//		var charterReservation = objectFactory.GetCharterReservationObjet(segment, request, rateResponse, pickupTimeResponse, franchiseLocale);

		//		//ASAP requestID
		//		//charterReservation.ASAPRequestID

		//		//We need to have both Acount# and Acount Password for DiretBill
		//		//charterReservation.PaymentDetails.DirectBillPaymentInfo.AccountPassword

		//		logger.InfoFormat("Send to SuperShuttle \r\n{0}", charterReservation.XmlSerialize());

		//		#region Track
		//		if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
		//		#endregion

		//		using (var client = new ReservationsServiceSecure.ReservationsServiceSecureClient())
		//		{
		//			result = client.SaveCharterReservation(_token, charterReservation, clientAppVersionInfo);
		//		}

		//		#region Track
		//		if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "SaveCharterReservation");
		//		#endregion

		//		return result;
		//	}
		//	catch (Exception ex)
		//	{
		//		logger.ErrorFormat("Call ReservationsServiceSecure.SaveCharterReservation {0}", ex.StackTrace);
		//		throw ex;
		//	}
		//}

		#endregion
		#endregion

		#region Properties
		internal TrackTime TrackTime
		{
			get
			{
				return _trackTime;
			}
			set
			{
				_trackTime = value;
			}
		}

		internal SecurityToken Token
		{
			set
			{
				_token = value;
			}
		}
		#endregion
	}
}
