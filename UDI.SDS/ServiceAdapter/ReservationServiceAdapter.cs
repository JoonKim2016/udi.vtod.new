﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.Track;
using UDI.VTOD.Common.DTO.Enum;
using UDI.SDS.DTO;
using UDI.Utility.Helper;
using UDI.Utility.Serialization;
using UDI.VTOD.Common.DTO.OTA;
using UDI.SDS.DTO.Enum;
using UDI.VTOD.Common.Helper;
using UDI.SDS.ReservationsService;
using UDI.SDS.Helper;
using UDI.SDS.RatesService;

namespace UDI.SDS.ServiceAdapter
{
	internal class ReservationServiceAdapter : BaseSetting
	{
		#region Fields
		UDI.SDS.ReservationsService.SecurityToken _token;
		TrackTime _trackTime;
		#endregion

		#region Constructors
		private ReservationServiceAdapter()
		{
		}

		internal ReservationServiceAdapter(UDI.SDS.ReservationsService.SecurityToken token, TrackTime trackTime)
		{
			_token = token;
			_trackTime = trackTime;
		}
		#endregion

		#region Internal
		internal bool CancelReservation(string confirmationNumber, string userName, string lastName, string phoneNumber, string postalCode)
		{
			try
			{
				logger.InfoFormat("Cancel VTOD Reservation Start...\r\n  ConfirmationNumber:{0}, userName:{1}, lastName:{2}, phoneNumber:{3}, postalCode: {4}", confirmationNumber, userName, lastName, phoneNumber, postalCode);

				int result = -1;

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
				#endregion

				using (var client = new ReservationsService.ReservationsServiceClient())
				{
					result = client.CancelReservation(_token, confirmationNumber, userName, lastName, phoneNumber, postalCode);
				}

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "CancelReservation");
				#endregion

				if (result == (int)Cancel.ReservationCancelled || result == (int)Cancel.ReservationAlreadyCancelled)
					return true;
				else if (result == (int)Cancel.TooCloseToPickupTime)
					throw VTOD.Common.DTO.VtodException.CreateException(ExceptionType.SDS, 5003);// new Exception(string.Format(Messages.UDI_SDS_CancelFailed_TooCloseToPickupTime, "CancelReservation"));
				else if (result == (int)Cancel.ReservationPickupTimeHasExpiredOrCannotBeCancelled)
					throw VTOD.Common.DTO.VtodException.CreateException(ExceptionType.SDS, 5004);//throw new Exception(string.Format(Messages.UDI_SDS_CancelFailed_PickupTimeExpired, "CancelReservation"));
				else
					throw VTOD.Common.DTO.VtodException.CreateException(ExceptionType.SDS, 5001);// new Exception(string.Format(Messages.UDI_SDS_CancelFailed_Unknown, "CancelReservation", result));
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Cancel VTOD ConfirmationNumber:{0}\r\n {1}", confirmationNumber, ex.ToString());
				throw ex;
			}
		}

		internal bool CancelReservation(string agencyConfirmationNumber, string agencyContext)
		{
			try
			{
				logger.InfoFormat("Cancel Agency Reservation Start...\r\n  agencyConfirmationNumber:{0}", agencyConfirmationNumber);

				int result = -1;
				var request = new CancelProviderTripRequest { TripID = agencyConfirmationNumber.ToInt32(), ProviderCode = agencyContext };

				CancelProviderTripResponse response = null;
				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
				#endregion

				using (var client = new ReservationsService.ReservationsServiceClient())
				{
					response = client.CancelProviderTrip(_token, request);
				}

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "CancelAgencyReservation");
				#endregion

				if (response != null)
				{
					result = response.ResponseCode;
				}

				if (result == (int)Cancel.ReservationCancelled || result == (int)Cancel.ReservationAlreadyCancelled)
					return true;
				else if (result == (int)Cancel.TooCloseToPickupTime)
					throw VTOD.Common.DTO.VtodException.CreateException(ExceptionType.SDS, 5003);//throw new Exception(string.Format(Messages.UDI_SDS_CancelFailed_TooCloseToPickupTime, "CancelReservation"));
				else if (result == (int)Cancel.ReservationPickupTimeHasExpiredOrCannotBeCancelled)
					throw VTOD.Common.DTO.VtodException.CreateException(ExceptionType.SDS, 5004);// new Exception(string.Format(Messages.UDI_SDS_CancelFailed_PickupTimeExpired, "CancelReservation"));
				else
					throw VTOD.Common.DTO.VtodException.CreateException(ExceptionType.SDS, 5001);//throw new Exception(string.Format(Messages.UDI_SDS_CancelFailed_Unknown, "CancelReservation", result));
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Cancel agencyNumber:{0}\r\n {1}", agencyConfirmationNumber, ex.ToString());
				throw ex;
			}
		}

		internal ReservationsService.PickupTimeRecord ValidateAirportPickupTime(UDI.SDS.DTO.Segment segment, ServiceInfo serviceInfo, int hotelConciergeID)
		{
			try
			{
				//PickupTimeStatus result = PickupTimeStatus.Unknown;
				var result = new ReservationsService.PickupTimeRecord();

				//FareID
				int fareID = 0;
				//if (rateResponse.ServiceInfoRates !=null && rateResponse.ServiceInfoRates.Any())
				//{
				fareID = serviceInfo.FareID;
				//}
				//else if (rateResponse.AirportReservationRate !=null)
				//{
				//	fareID = rateResponse.AirportReservationRate.FareID;
				//}

				logger.InfoFormat("Send to SuperShuttle \r\n tripDirection={0}, pickupTime={1}, hotelConciergeID={2}, serviceTypeID={3}", serviceInfo.Direction, segment.RequestPickupTime, hotelConciergeID, fareID);
				using (var client = new ReservationsService.ReservationsServiceClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
					#endregion

					//var validateResult = client.ValidatePickupTime(_token, 12, segment.RequestPickupTime, hotelConciergeID, fareID);

					var validateResult = client.ValidatePickupTime(_token, (int)serviceInfo.Direction, segment.RequestPickupTime, hotelConciergeID, fareID);


					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "ValidatePickupTime");
					#endregion

					//if (validateResult == ReservationsService.PickupTimeStatusCodesEnumeration.AllowPickup)
					//{
					result.StartTime = segment.RequestPickupTime;
					result.Status = validateResult;
					//For Ecar, we do not provide end time (there is not any window)
					result.EndTime = segment.RequestPickupTime.AddMinutes(15);
					//result = validateResult.ToPickupTimeStatus();
					//result = true;
					//}
				}
				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call ReservationsService.ValidatePickupTime {0}", ex.StackTrace);
				throw ex;
			}
		}

		internal ReservationsService.PickupTimeRecord ValidateCharterPickupTime(int webAdvanceNoticeMinutes, string franshiseCode, DateTime pickupTime)
		{
			try
			{
				//PickupTimeStatus result = PickupTimeStatus.Unknown;
				var result = new ReservationsService.PickupTimeRecord();

				logger.InfoFormat("Send to SuperShuttle \r\n webAdvanceNoticeMinutes={0}, franshiseCode={1}, pickupTime={2}", webAdvanceNoticeMinutes, franshiseCode, pickupTime);
				using (var client = new ReservationsService.ReservationsServiceClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
					#endregion

					var validateResult = client.ValidateCharterPickupTime(_token, webAdvanceNoticeMinutes, franshiseCode, pickupTime);


					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "ValidateCharterTime");
					#endregion

					result.StartTime = pickupTime;
					result.Status = validateResult;
				}
				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call ReservationsService.ValidatePickupTime {0}", ex.StackTrace);
				throw ex;
			}
		}

		/// <summary>
		/// Get a list of Guarantee Times from SuperShuttle
		/// </summary>
		internal List<ReservationsService.PickupTimeRecord> GetPickupTimes(bool isAccessibleServiceRequired, int hotelConciergeID, bool isInternationalFlight, int masterLandmarkID, ServiceInfo serviceInfo, DateTime flightTime, string airportCode, int childSeats, int payingPax)
		{
			try
			{
				var errorMsg = string.Empty;
				var pickupTimes = new List<ReservationsService.PickupTimeRecord>();
				ReservationsService.PickupTimeRequest request = new ReservationsService.PickupTimeRequest();
				request.AccessibleServiceRequired = isAccessibleServiceRequired;
				request.HotelConciergeID = hotelConciergeID;
				request.IsInternationalFlight = isInternationalFlight;
				request.MasterLandmarkID = masterLandmarkID == -2147483648 ? 0 : masterLandmarkID; //use zero for MasterLandmarkId if there's no landmark
				request.ServiceTypeID = serviceInfo.FareID;
				request.TravelTime = flightTime;
				request.TripDirection = (int)serviceInfo.Direction;

				logger.InfoFormat("Send to SuperShuttle \r\n{0}", request.XmlSerialize());
				using (var client = new ReservationsService.ReservationsServiceClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
					#endregion

					var result = client.GetPickupTime(_token, request);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetPickupTime");
					#endregion

					if (result.HasRecords)
					{
						pickupTimes = result.PickupTimeRecordsArray.ToList();
					}
					else
					{
						errorMsg = "GetPickupTimes";
					}
				}

				if (!string.IsNullOrEmpty(errorMsg))
					throw VTOD.Common.DTO.VtodException.CreateFieldRequiredValidationException("PickupTime"); //new Exception(string.Format(Messages.UDI_SDS_ASAP_NoPickupTimeFound, errorMsg));

				return pickupTimes;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call ReservationsService.GetPickupTime {0}", ex.StackTrace);
				throw ex;
			}
		}
		#endregion

		internal GetReceiptResponse GetReceipt(int rezID)
		{
			try
			{
				GetReceiptResponse result = null;
				logger.InfoFormat("Send to SuperShuttle \r\n{0}", rezID);
				using (var client = new ReservationsService.ReservationsServiceClient())
				{
					var request = new GetReceiptRequest();
					request.RezID = rezID;

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
					#endregion

					result = client.GetReceipt(_token, request);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetReceipt");
					#endregion
				}

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call ReservationsService.GetPickupTime {0}", ex.StackTrace);
				throw ex;
			}
		}

		internal TripStatus GetTripStatus(GetProviderTripStatusRequest request)
		{
			try
			{
				TripStatus result = null;
				logger.InfoFormat("Send to SuperShuttle GetTripStatus");
				logger.Info(request.XmlSerialize());
				using (var client = new ReservationsService.ReservationsServiceClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
					#endregion

					result = client.GetProviderTripStatus(_token, request);

					logger.Info(result.XmlSerialize());

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetProviderTripStatusRequest");
					#endregion
				}

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call ReservationsService.GetPickupTime {0}", ex.StackTrace);
				throw ex;
			}
		}

		internal TripStatus GetTripStatus(GetTripStatusRequest request)
		{
			try
			{
				TripStatus result = null;
				logger.InfoFormat("Send to SuperShuttle GetTripStatus");
				logger.Info(request.XmlSerialize());
				using (var client = new ReservationsService.ReservationsServiceClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
					#endregion

					result = client.GetTripStatus(_token, request);
                
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetTripStatus");
					#endregion
				}

				logger.Info(result.XmlSerialize());
				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call ReservationsService.GetTripStatus {0}", ex.StackTrace);
				throw ex;
			}
		}

		internal UDI.SDS.ReservationsService.Reservation RetrieveReservation(GetReservationCriteria request)
		{
			try
			{
				UDI.SDS.ReservationsService.Reservation result = null;
				logger.InfoFormat("Send to SuperShuttle RetrieveReservation");
				logger.Info(request.XmlSerialize());
				using (var client = new ReservationsService.ReservationsServiceClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
					#endregion

					result = client.RetrieveReservation(_token, request);

					logger.Info(result.XmlSerialize());

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "RetrieveReservation");
					#endregion
				}

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call ReservationsService.RetrieveReservation {0}", ex.StackTrace);
				throw ex;
			}
		}
		
		internal void RequestEmailConfirmation(string email, string confirmationNumber, string lastName, string phoneNumber, string postalCode)
		{
			try
			{
				//TripStatus result = null;
				logger.InfoFormat("Send to SuperShuttle RequestEmailConfirmation");
				using (var client = new ReservationsService.ReservationsServiceClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
					#endregion

					//client.RequestEmailConfirmation(_token, email, _token.UserName, confirmationNumber, lastName, phoneNumber, postalCode);
					client.RequestEmailConfirmation2(_token, email, _token.UserName, confirmationNumber,
						string.IsNullOrWhiteSpace(lastName) ? string.Empty : lastName,
						string.IsNullOrWhiteSpace(phoneNumber) ? string.Empty : phoneNumber,
						string.IsNullOrWhiteSpace(postalCode) ? string.Empty : postalCode,
						0);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "RequestEmailConfirmation");
					#endregion
				}

			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call RequestEmailConfirmation {0}", ex.StackTrace);
				throw ex;
			}
		}

		internal CancelFee GetCancelFee(GetReservationCriteria request)
		{
			try
			{
				CancelFee result = null;
				logger.InfoFormat("Send to SuperShuttle GetCancelFee");
				logger.Info(request.XmlSerialize());
				using (var client = new ReservationsService.ReservationsServiceClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
					#endregion

					result = client.GetCancelFee(_token, request);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetCancelFee");
					#endregion
				}

				logger.Info(result.XmlSerialize());
				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call ReservationsService.GetCancelFee {0}", ex.StackTrace);
				throw ex;
			}
		}

		#region Properties
		internal TrackTime TrackTime
		{
			get
			{
				return _trackTime;
			}
			set
			{
				_trackTime = value;
			}
		}

		internal UDI.SDS.ReservationsService.SecurityToken Token
		{
			set
			{
				_token = value;
			}
		}
		#endregion

	}
}
