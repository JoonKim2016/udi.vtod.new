﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.SDS.DTO;
using UDI.SDS.DTO.Enum;
using UDI.SDS.RoutingService;
using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Common.Track;
using UDI.Utility.Serialization;

namespace UDI.SDS.ServiceAdapter
{
	internal class RoutingServiceAdapter : BaseSetting
	{
		#region Fields
		SecurityToken _token;
		TrackTime _trackTime;
		#endregion

		#region Constructors
		private RoutingServiceAdapter()
		{
		}

		internal RoutingServiceAdapter(SecurityToken token, TrackTime trackTime)
		{
			_token = token;
			_trackTime = trackTime;
		}
		#endregion

		#region Internal
		internal decimal GetDistanceBetweenPoints(Point startPoint, Point endPoint)
		{
			decimal result = 0;

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
			#endregion

			using (var routingClient = new RoutingService.RoutingServiceClient())
			{
				result = routingClient.GetDistanceBetweenPoints(_token, startPoint, endPoint);
			}

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetDistanceBetweenPoints");
			#endregion

			return result;
		}

		internal RouteMetrics GetRouteMetrics(Point startPoint, Point endPoint)
		{
			RouteMetrics result = null;

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
			#endregion

			using (var routingClient = new RoutingService.RoutingServiceClient())
			{
				result = routingClient.GetRouteMetrics(_token, startPoint, endPoint);
			}

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetRouteMetrics");
			#endregion

			return result;
		}
		#endregion

		#region Properties
		internal TrackTime TrackTime
		{
			get
			{
				return _trackTime;
			}
			set
			{
				_trackTime = value;
			}
		}

		internal SecurityToken Token
		{
			set
			{
				_token = value;
			}
		}
		#endregion
	}
}
