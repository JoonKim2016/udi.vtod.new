﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.VTOD.Common.Abstract;
using UDI.SDS.SecurityService;
using UDI.VTOD.Common.Track;
using UDI.VTOD.Common.DTO.Enum;
using UDI.SDS.DTO;
using UDI.Utility.Helper;
using UDI.Utility.Serialization;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.Helper;

namespace UDI.SDS.ServiceAdapter
{
	internal class SecurityServiceAdapter : BaseSetting
	{
		#region Fields
		SecurityToken _token;
		TrackTime _trackTime;
		#endregion

		#region Constructors
		private SecurityServiceAdapter()
		{
		}

		internal SecurityServiceAdapter(SecurityToken token, TrackTime trackTime)
		{
			_token = token;
			_trackTime = trackTime;
		}
		#endregion

		#region Internal
		internal TokenRS GetToken(UDI.VTOD.Common.DTO.OTA.TokenRQ input, out UserInfo userInfo)
		{

			userInfo = null;
			SecurityToken token = null;
			TokenRS result = null;

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
			#endregion

			using (var securityClient = new SecurityService.WCFSecurityServiceClient())
			{
				token = securityClient.Login(
					input.ClientType.Value,
					input.ClientIPAddress,
					input.Username,
					input.Password);

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "Login");
				#endregion

				#region Extension Data - UserInfo
				//string allowClosedSplits = null;
				//string showSplits = null;

				//try
				//{
				//	var extensionData = Utilities.GetExtensionDataMemberValue(token);
				//	if (extensionData != null && extensionData.Any())
				//	{
				//		allowClosedSplits = extensionData["AllowClosedSplits"].ToString();
				//		showSplits = extensionData["ShowSplits"].ToString();
				//	}
				//	else
				//	{
				//		logger.Error("No Extension data found for AllowClosedSplits and ShowSplits in getting token");
				//	}
				//}
				//catch (Exception ex)
				//{
				//	logger.Error("No Extension data found for AllowClosedSplits and ShowSplits in getting token");
				//	logger.Error(ex);
				//}

				userInfo = new UserInfo { UserName = token.UserName, ShowSplits = token.ShowSplits, AllowClosedSplits = token.AllowClosedSplits };
				#endregion

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "Extension Data - UserInfo");
				#endregion

				#region Log
				logger.Info(token.XmlSerialize().ToString().CleanSecurityKey());
				//logger.InfoFormat("AllowClosedSplits: {0} , ShowSplits: {1}", allowClosedSplits, showSplits);
				#endregion

			}

			result = new VTOD.Common.DTO.OTA.TokenRS { ClientIPAddress = token.ClientIPAddress, ClientType = token.ClientType, SecurityKey = token.SecurityKey.ToString(), Username = token.UserName };
			return result;
		}

        internal TokenRS GetToken(UDI.VTOD.Common.DTO.OTA.TokenRQ input)
        {

            SecurityToken token = null;
            TokenRS result = null;

            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
            #endregion

            using (var securityClient = new SecurityService.WCFSecurityServiceClient())
            {
                token = securityClient.Login(
                    input.ClientType.Value,
                    input.ClientIPAddress,
                    input.Username,
                    input.Password);

                #region Track
                if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "Login");
                #endregion

                 #region Track
                if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "Extension Data - UserInfo");
                #endregion

                #region Log
                logger.Info(token.XmlSerialize().ToString().CleanSecurityKey());
                //logger.InfoFormat("AllowClosedSplits: {0} , ShowSplits: {1}", allowClosedSplits, showSplits);
                #endregion

            }

            result = new VTOD.Common.DTO.OTA.TokenRS { ClientIPAddress = token.ClientIPAddress, ClientType = token.ClientType, SecurityKey = token.SecurityKey.ToString(), Username = token.UserName };
            return result;
        }
        
        internal bool ValidateToken(string userName, Guid securityKey)
		{
			var result = false;
			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
			#endregion

			using (var client = new SecurityService.WCFSecurityServiceClient())
			{
				result = client.IsTokenValid(new SecurityToken { SecurityKey = securityKey, UserName = userName });
			}

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "ValidateToken");
			#endregion
			return result;
		}
		#endregion

		#region Properties
		internal TrackTime TrackTime
		{
			get
			{
				return _trackTime;
			}
			set
			{
				_trackTime = value;
			}
		}

		internal SecurityToken Token
		{
			set
			{
				_token = value;
			}
		}
		#endregion
	}
}
