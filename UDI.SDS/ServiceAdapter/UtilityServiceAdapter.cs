﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.SDS.DTO;
using UDI.SDS.DTO.Enum;
using UDI.SDS.UtilityService;
using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Common.Track;
using UDI.Utility.Serialization;

namespace UDI.SDS.ServiceAdapter
{
	internal class UtilityServiceAdapter : BaseSetting
	{
		#region Fields
		SecurityToken _token;
		TrackTime _trackTime;
		#endregion

		#region Constructors
		private UtilityServiceAdapter()
		{
		}

		internal UtilityServiceAdapter(SecurityToken token, TrackTime trackTime)
		{
			_token = token;
			_trackTime = trackTime;
		}
		#endregion

		#region Internal
		internal List<CharterLocation> GetCharterLocationsByPostalCode(int charterType, string postalCode)
		{
			List<CharterLocation> result = null;

			using (var utilityClient = new UtilityService.UtilityServiceClient())
			{
				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
				#endregion

				/** use pickup zipcode to determine which region customer is in **/
				result = utilityClient.GetCharterLocationsByPostalCode(_token, charterType, postalCode); //CharterType: 0=PointToPoint, 1=Hourly

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetCharterLocationsByPostalCode");
				#endregion

				//if (charterLocationList != null && charterLocationList.FirstOrDefault() != null)
				//{
				//	result = charterLocationList.FirstOrDefault().ID;
				//}
			}

			return result;
		}

		internal AirportRecords GetWebServicedAirports()
		{
			AirportRecords result = null;
			using (var client = new UtilityService.UtilityServiceClient())
			{

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
				#endregion

				result = client.GetWebServicedAirports(_token);
				//utilityClient.GetWebServicedAirports2(token.ToUtilityServiceToken(), airportsRequest);

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetWebServicedAirports");
				#endregion
			}

			return result;
		}

		internal AirportRecords GetDestinations()
		{
			AirportRecords result = null;
			using (var client = new UtilityService.UtilityServiceClient())
			{

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
				#endregion

				result = client.GetDestinations(_token, string.Empty);
				//utilityClient.GetWebServicedAirports2(token.ToUtilityServiceToken(), airportsRequest);

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetDestinations");
				#endregion
			}

			return result;
		}
		
		internal GetNearestAirportsResponse GetWebServicedAirports(double latitude, double longitude, int radius)
		{
			GetNearestAirportsResponse result = null;
			using (var client = new UtilityService.UtilityServiceClient())
			{

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
				#endregion

				result = client.GetNearestAirports(_token, new GetNearestAirportsRequest { LocalizationCode = "en-US", Latitude = latitude, Longitude = longitude, Radius = radius });
				//utilityClient.GetWebServicedAirports2(token.ToUtilityServiceToken(), airportsRequest);

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetWebServicedAirports");
				#endregion
			}

			return result;
		}

		internal string GetAirportWebMessage(string airportCode)
		{
			string result = string.Empty;

			using (var utilityClient = new UtilityService.UtilityServiceClient())
			{

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
				#endregion

				result = utilityClient.GetAirportWebMessage(_token, airportCode);

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetAirportWebMessage");
				#endregion
			}
			return result;
		}

		internal List<UDI.SDS.UtilityService.CharterLocation> GetCharterLocationsByType(int charterType)
		{
			List<UDI.SDS.UtilityService.CharterLocation> result = null;

			using (var utilityClient = new UtilityService.UtilityServiceClient())
			{
				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
				#endregion

				var charterLocationList = utilityClient.GetCharterLocationsByType(_token, charterType); //CharterType: 0=PointToPoint, 1=Hourly

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetCharterLocationsByType");
				#endregion

				if (charterLocationList != null)
				{
					result = charterLocationList;
				}
			}

			return result;
		}

        internal AvailableFleets GetAvailableFleets(string postalCode)
		{
			AvailableFleets result = null;

			using (var utilityClient = new UtilityService.UtilityServiceClient())
			{
				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
				#endregion

				var request = new GetAvailableFleetsRequest();
				request.postalCode = postalCode;
				result = utilityClient.GetAvailableFleets(_token, request); //CharterType: 0=PointToPoint, 1=Hourly

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetAvailableFleets");
				#endregion

			}

			return result;
		}

		internal PickMeUpNowServiceDetails IsPickMeUpNowServiced(PickMeUpNowServiceRequest request)
		{
			PickMeUpNowServiceDetails result = null;

			using (var utilityClient = new UtilityService.UtilityServiceClient())
			{
				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
				#endregion

				result = utilityClient.IsPickMeUpNowServiced(_token, request); //CharterType: 0=PointToPoint, 1=Hourly

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "IsPickMeUpNowServiced");
				#endregion

			}

			return result;

		}

		internal AirlineRecords GetAirlinesByAirport(string ariportCode)
		{
			AirlineRecords result = null;

			using (var utilityClient = new UtilityService.UtilityServiceClient())
			{
				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
				#endregion

				var request = new GetAvailableFleetsRequest();

				result = utilityClient.GetAirlinesByAirportCode(_token, ariportCode); //CharterType: 0=PointToPoint, 1=Hourly

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetAirlinesByAirport");
				#endregion

			}

			return result;
		}

		internal AirportRecords GetLocalizedGroupServicedAirports(int groupID, string localizationCode)
		{
			AirportRecords result = null;
			try
			{
				logger.InfoFormat("Send to SuperShuttle \r\n discountID={0}, airportCode = {1}", groupID, localizationCode);

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
				#endregion

				using (var utilityClient = new UtilityService.UtilityServiceClient())
				{
					result = utilityClient.GetLocalizedGroupServicedAirports(_token, groupID, localizationCode);
				}

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetLocalizedServicedAirports");
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call UtilityService.GetLocalizedServicedAirports {0}", ex.StackTrace);
				throw ex;
			}
		}


		internal GetHonorificsResponse GetHonorifics(string languageCode)
		{
			GetHonorificsResponse result = null;
			try
			{
				logger.InfoFormat("Send to SuperShuttle \r\n GetHonorifics");

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
				#endregion

				using (var utilityClient = new UtilityService.UtilityServiceClient())
				{
					result = utilityClient.GetHonorifics(_token, new GetHonorificsRequest { LanguageCode = languageCode });
				}

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetLocalizedServicedAirports");
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call UtilityService.GetHonorifics {0}", ex.StackTrace);
				throw ex;
			}
		}

		#endregion

		#region Properties
		internal TrackTime TrackTime
		{
			get
			{
				return _trackTime;
			}
			set
			{
				_trackTime = value;
			}
		}

		internal SecurityToken Token
		{
			set
			{
				_token = value;
			}
		}
		#endregion
	}
}
