﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.Track;
using UDI.VTOD.Common.DTO.Enum;
using UDI.SDS.DTO;
using UDI.Utility.Helper;
using UDI.Utility.Serialization;
using UDI.VTOD.Common.DTO.OTA;
using UDI.SDS.DTO.Enum;
using UDI.VTOD.Common.Helper;
using UDI.SDS.VehiclesService;

namespace UDI.SDS.ServiceAdapter
{
	public class VehiclesServiceAdapter : BaseSetting
	{
		#region Fields
		SecurityToken _token;
		TrackTime _trackTime;
		#endregion

		#region Constructors
		private VehiclesServiceAdapter()
		{
		}

		internal VehiclesServiceAdapter(SecurityToken token, TrackTime trackTime)
		{
			_token = token;
			_trackTime = trackTime;
		}
		#endregion

		#region Internal
		internal int GetDefaultVehicleTypeForPoint(double latitude, double longitude)
		{
			int result;

			using (var vehiclesClient = new VehiclesService.VehiclesServiceClient())
			{
				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
				#endregion

				var defaultVehicleType = vehiclesClient.GetDefaultVehicleTypeForPoint(_token, latitude, longitude);

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetDefaultVehicleTypeForPoint");
				#endregion


				result = defaultVehicleType;
			}

			return result;
		}

		internal VehiclesNearPoint GetVehiclesNearPoint(Point point)
		{
			VehiclesNearPoint result;

			using (var vehiclesClient = new VehiclesService.VehiclesServiceClient())
			{
				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
				#endregion

				result = vehiclesClient.GetVehiclesNearPoint(_token, point);

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "VehiclesNearPoint");
				#endregion
			}

			return result;
		}

		internal string GetVehiclesNearPointCompressed(Point point)
		{
			string result;

			using (var vehiclesClient = new VehiclesService.VehiclesServiceClient())
			{
				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
				#endregion

				result = vehiclesClient.GetVehiclesNearPointCompressed(_token, point);

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetVehiclesNearPointCompressed");
				#endregion
			}

			return result;
		}

		#endregion

		#region Properties
		internal TrackTime TrackTime
		{
			get
			{
				return _trackTime;
			}
			set
			{
				_trackTime = value;
			}
		}

		internal SecurityToken Token
		{
			set
			{
				_token = value;
			}
		}
		#endregion

	}
}

