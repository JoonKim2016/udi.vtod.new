﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.Track;
using UDI.VTOD.Common.DTO.Enum;
using UDI.SDS.DTO;
using UDI.Utility.Helper;
using UDI.Utility.Serialization;
using UDI.VTOD.Common.DTO.OTA;
using UDI.SDS.DTO.Enum;
using UDI.VTOD.Common.Helper;
using UDI.SDS.WMVService;

namespace UDI.SDS.ServiceAdapter
{
	public class WMVServiceAdapter : BaseSetting
	{
		#region Fields
		SecurityToken _token;
		TrackTime _trackTime;
		#endregion

		#region Constructors
		private WMVServiceAdapter()
		{
		}

		internal WMVServiceAdapter(SecurityToken token, TrackTime trackTime)
		{
			_token = token;
			_trackTime = trackTime;
		}
		#endregion

		#region Internal
		internal VehicleAndTripLocation GetDefaultVehicleTypeForPoint(string confirmationNumber)
		{
			VehicleAndTripLocation result;

			using (var client = new WMVService.WMVServiceClient())
			{
				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
				#endregion

				result = client.GetVehicleAndTripLocation(_token, confirmationNumber);

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetDefaultVehicleTypeForPoint");
				#endregion
			}

			return result;
		}
		#endregion

		#region Properties
		internal TrackTime TrackTime
		{
			get
			{
				return _trackTime;
			}
			set
			{
				_trackTime = value;
			}
		}

		internal SecurityToken Token
		{
			set
			{
				_token = value;
			}
		}
		#endregion

	}
}

