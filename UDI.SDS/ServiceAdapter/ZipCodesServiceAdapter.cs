﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.Track;
using UDI.VTOD.Common.DTO.Enum;
using UDI.SDS.DTO;
using UDI.Utility.Helper;
using UDI.Utility.Serialization;
using UDI.VTOD.Common.DTO.OTA;
using UDI.SDS.DTO.Enum;
using UDI.VTOD.Common.Helper;
using UDI.SDS.ZipCodesService;

namespace UDI.SDS.ServiceAdapter
{
	public class ZipCodesServiceAdapter : BaseSetting
	{
		#region Fields
		SecurityToken _token;
		TrackTime _trackTime;
		#endregion

		#region Constructors
		private ZipCodesServiceAdapter()
		{
		}

		internal ZipCodesServiceAdapter(SecurityToken token, TrackTime trackTime)
		{
			_token = token;
			_trackTime = trackTime;
		}
		#endregion

		#region Internal
		/// <summary>
		/// Check if zip code is serviced and split
		/// </summary>
		/// <param name="airportCode"></param>
		/// <param name="postalCode"></param>
		/// <param name="tripDirection">0 = To the airport. 1 = From the airport. 2 = Roundtrip.</param>
		/// <returns>a list of split names</returns>
		internal List<ZipCodesService.ZipCodeSplitRecord> GetSplitNamesOfServicedPostalCode(string airportCode, string postalCode, int tripDirection, out bool isZipSplit)
		{
			try
			{
				List<ZipCodesService.ZipCodeSplitRecord> splitNames = null;

				logger.InfoFormat("Send to SuperShuttle \r\n AirportCode={0},PostalCode={1},TripDirection={2}", airportCode, postalCode, tripDirection);
				using (var client = new ZipCodesService.ZipCodesServiceClient())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
					#endregion

					var result = client.IsZipServiced(_token, airportCode, postalCode, tripDirection);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "IsZipServiced");
					#endregion

					isZipSplit = result.IsZipSplit;
					if (result != null && result.IsServiced == true && result.SplitsArray.ToList().Count > 0)
						splitNames = result.SplitsArray.ToList();

				}

				if (splitNames == null)
					throw VTOD.Common.DTO.VtodException.CreateException(ExceptionType.SDS, 1005);// new Exception(string.Format(Messages.UDI_SDS_PostalCodeNotServiced, postalCode));
				//throw new Exception("GetSplitNamesOfServicedPostalCode:: The postalcode " + postalCode + " is not serviced.");

				return splitNames;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call ZipCodesService.IsZipServiced {0}", ex.StackTrace);
				throw ex;
			}
		}
		#endregion

		#region Properties
		internal TrackTime TrackTime
		{
			get
			{
				return _trackTime;
			}
			set
			{
				_trackTime = value;
			}
		}

		internal SecurityToken Token
		{
			set
			{
				_token = value;
			}
		}
		#endregion

	}
}
