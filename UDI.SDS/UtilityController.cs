﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.Track;
using UDI.SDS.Helper;
using UDI.SDS.UtilityService;
using UDI.SDS.DTO.Enum;
using UDI.SDS.DTO;
using UDI.SDS.ServiceAdapter;
using UDI.Utility.Serialization;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Common.Helper;

namespace UDI.SDS
{
	public class UtilityController : BaseSetting
	{
		#region Fields
		TrackTime _trackTime;
		TokenRS _token;
		#endregion

		#region Constructors
		private UtilityController()
		{
		}

		public UtilityController(TokenRS token, TrackTime trackTime)
		{
			_token = token;
			_trackTime = trackTime;
		}

		#endregion

		#region Public
		public int GetCharterLocationIdByPostalCode(int charterType, string postalCode)
		{
			#region Init
			UDI.SDS.UtilityService.SecurityToken utlityToken = _token.ToUtilityServiceToken();
			var utilityAdapter = new UtilityServiceAdapter(utlityToken, _trackTime);
			#endregion

			var result = 0;

			var charterLocationList = utilityAdapter.GetCharterLocationsByPostalCode(charterType, postalCode);

			if (charterLocationList != null && charterLocationList.FirstOrDefault() != null)
			{
				result = charterLocationList.FirstOrDefault().ID;
			}

			return result;
		}

		public List<UDI.SDS.UtilityService.CharterLocation> GetCharterLocationsByPostalCode(int charterType, string postalCode)
		{
			#region Init
			UDI.SDS.UtilityService.SecurityToken utlityToken = _token.ToUtilityServiceToken();
			var utilityAdapter = new UtilityServiceAdapter(utlityToken, _trackTime);
			#endregion

			var result = utilityAdapter.GetCharterLocationsByPostalCode(charterType, postalCode);
			return result;
		}

		public AirportRecords GetWebServicedAirports()
		{
			#region Init
			UDI.SDS.UtilityService.SecurityToken utlityToken = _token.ToUtilityServiceToken();
			var utilityAdapter = new UtilityServiceAdapter(utlityToken, _trackTime);
			#endregion

			var result = utilityAdapter.GetWebServicedAirports();

			return result;
		}

		public AirportRecords GetDestinations()
		{
			#region Init
			UDI.SDS.UtilityService.SecurityToken utlityToken = _token.ToUtilityServiceToken();
			var utilityAdapter = new UtilityServiceAdapter(utlityToken, _trackTime);
			#endregion

			var result = utilityAdapter.GetDestinations();

			return result;
		}
		
		//public GetNearestAirportsResponse GetWebServicedAirports(double latitude, double longitude, int radius)
		public GetNearestAirportsResponse GetWebServicedAirports(double latitude, double longitude, int radius)
		{
			#region Init
			UDI.SDS.UtilityService.SecurityToken utlityToken = _token.ToUtilityServiceToken();
			var utilityAdapter = new UtilityServiceAdapter(utlityToken, _trackTime);
			#endregion

			var result = utilityAdapter.GetWebServicedAirports(latitude, longitude, radius);

			return result;
		}

		public AvailableFleets GetAvailableFleets(string postalCode)
		{
			#region Init
			UDI.SDS.UtilityService.SecurityToken utlityToken = _token.ToUtilityServiceToken();
			var utilityAdapter = new UtilityServiceAdapter(utlityToken, _trackTime);
			#endregion

            var result = utilityAdapter.GetAvailableFleets(postalCode);

			return result;
		}

		public bool IsPickMeUpNowServiced(PickMeUpNowServiceRequest request)
		{
			#region Init
			UDI.SDS.UtilityService.SecurityToken utlityToken = _token.ToUtilityServiceToken();
			var utilityAdapter = new UtilityServiceAdapter(utlityToken, _trackTime);
			#endregion

			var result = false;

			try
			{
				var sdsResult = utilityAdapter.IsPickMeUpNowServiced(request);

				if (sdsResult != null && sdsResult.IsServiced)
				{
					result = true;
				}
			}
			catch (Exception ex)
			{
				logger.Error(ex);
			}

			return result;
		}

		public bool IsAirportServicedOnline(string airportCode)
		{
			#region Init
			UDI.SDS.UtilityService.SecurityToken utlityToken = _token.ToUtilityServiceToken();
			var utilityAdapter = new UtilityServiceAdapter(utlityToken, _trackTime);
			#endregion

			try
			{
				var isServicedOnline = false;
				var errorMsg = string.Empty;

				var airportsRequest = new UtilityService.GetAirportsRequest { LocalizationCode = airportCode };

				logger.InfoFormat("Send to SuperShuttle \r\n{0}", airportsRequest.XmlSerialize());

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
				#endregion

				var airports = utilityAdapter.GetWebServicedAirports();
				//utilityClient.GetWebServicedAirports2(token.ToUtilityServiceToken(), airportsRequest);

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.SDS_Api, "GetWebServicedAirports");
				#endregion

				if (airports.HasRecords)
				{
					var selectedAirports = airports.AirportRecordsArray.Where(s => s.AirportCode.ToLower() == airportCode.ToLower()).ToList();
					if (selectedAirports != null && selectedAirports.Any())
					{
						var selectedAirport = selectedAirports.First();
						if (selectedAirport.Serviced == (int)AirportServicedCode.LocalOffice)
						{
							errorMsg = utilityAdapter.GetAirportWebMessage(airportCode);
						}
						else
						{
							isServicedOnline = true;
						}
					}
					//foreach (var airportRecord in airports.AirportRecordsArray)
					//{
					//	if (airportRecord.AirportCode.ToUpper().Equals(airportCode.ToUpper()))
					//	{
					//		//Airport with Serviced = 2 are serviced by SuperShuttle but reservations must be booked thru the local office.
					//		if (airportRecord.Serviced == 2)
					//		{
					//			errorMsg = utilityAdapter.GetAirportWebMessage(airportCode);

					//		}
					//		else
					//		{
					//			isServicedOnline = true;
					//			break;
					//		}
					//	}
					//}

				}

				if (!isServicedOnline)
					throw VTOD.Common.DTO.VtodException.CreateException(ExceptionType.Utility, 9002);// new Exception(string.Format(Messages.UDI_SDS_AirportNotServiced, "IsAirportServicedOnline", airportCode, errorMsg));

				return isServicedOnline;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Call utilityClient.GetAirportWebMessage {0}", ex.StackTrace);
				throw ex;
			}
		}

		public List<UDI.SDS.UtilityService.CharterLocation> GetCharterLocationsByType(int charterType)
		{
			#region Init
			UDI.SDS.UtilityService.SecurityToken utlityToken = _token.ToUtilityServiceToken();
			var utilityAdapter = new UtilityServiceAdapter(utlityToken, _trackTime);
			#endregion

			var result = utilityAdapter.GetCharterLocationsByType(charterType);

			return result;
		}

		public List<UDI.SDS.LandmarksService.LandmarkRecord> CharterSearchLandmark(int locationId, string searchExpression)
		{
			#region Init
			UDI.SDS.LandmarksService.SecurityToken utlityToken = _token.ToLandmarksServiceToken();
			var landmarksAdapter = new LandmarksServiceServiceAdapter(utlityToken, _trackTime);
			#endregion

			var result = landmarksAdapter.CharterSearchLandmark(locationId, searchExpression);

			return result;
		}

		public AirlineRecords GetAirlinesByAirport(string ariportCode)
		{
			#region Init
			UDI.SDS.UtilityService.SecurityToken utlityToken = _token.ToUtilityServiceToken();
			var utilityAdapter = new UtilityServiceAdapter(utlityToken, _trackTime);
			#endregion


			var result = utilityAdapter.GetAirlinesByAirport(ariportCode);

			return result;
		}

		public GetHonorificsResponse GetHonorifics(string languageCode)
		{
			#region Init
			UDI.SDS.UtilityService.SecurityToken utlityToken = _token.ToUtilityServiceToken();
			var utilityAdapter = new UtilityServiceAdapter(utlityToken, _trackTime);
			#endregion


			var result = utilityAdapter.GetHonorifics(languageCode);

			return result;
		}
		#endregion

		#region Properties
		public TrackTime TrackTime
		{
			get
			{
				return _trackTime;
			}
			set
			{
				_trackTime = value;
			}
		}

		public TokenRS Token
		{
			set
			{
				_token = value;
			}
		}
		#endregion
	}
}
