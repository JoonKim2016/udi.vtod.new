﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.Track;
using UDI.SDS.Helper;
using UDI.SDS.FranchiseService;
using UDI.SDS.DTO.Enum;
using UDI.SDS.DTO;
using UDI.SDS.ServiceAdapter;
using System.Data;

namespace UDI.SDS
{
	public class VanDrvController : BaseSetting
	{
		#region Fields
		TrackTime _trackTime;
		TokenRS _token;
		#endregion

		#region Constructors
		private VanDrvController()
		{
		}

		public VanDrvController(TrackTime trackTime)
		{
			_trackTime = trackTime;
		}

		#endregion

		#region Public
		public driver_info GetVanDriverInfo(string fleetId, string dispatchTripId)
		{
			#region Init
			var VanDriverAdapter = new VanDrvServiceAdapter(_trackTime);
			#endregion

			var result = VanDriverAdapter.GetVanDriverInfo(fleetId, dispatchTripId);

			return result;
		}

		public DataSet GetVanDriverInfo(string fleetId, string driverId, string vehicleId)
		{
			#region Init
			var VanDriverAdapter = new VanDrvServiceAdapter(_trackTime);
			if (string.IsNullOrWhiteSpace(fleetId)) fleetId = string.Empty;
			if (string.IsNullOrWhiteSpace(driverId)) driverId = string.Empty;
			if (string.IsNullOrWhiteSpace(vehicleId)) vehicleId = string.Empty;
			#endregion

			var result = VanDriverAdapter.GetVanDriverInfo(fleetId, driverId, vehicleId);

			return result;
		}

		public DataSet GetZtripDriver(string fleetId)
		{
			#region Init
			var VanDriverAdapter = new VanDrvServiceAdapter(_trackTime);
			#endregion

			var result = VanDriverAdapter.GetZtripDriver(fleetId);

			return result;
		}

		#endregion

		#region Properties
		public TrackTime TrackTime
		{
			get
			{
				return _trackTime;
			}
			set
			{
				_trackTime = value;
			}
		}
		#endregion
	}
}
