﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.Track;
using UDI.SDS.Helper;
using UDI.SDS.DTO.Enum;
using UDI.SDS.DTO;
using UDI.SDS.ServiceAdapter;
using UDI.SDS.MembershipService;
using UDI.SDS.VehiclesService;
using UDI.Utility.Helper;
using UDI.SDS.WMVService;
using UDI.Utility.Serialization;
using System.Xml.Linq;
using UDI.Map;
using System.Configuration;
namespace UDI.SDS
{
	public class VehiclesController : BaseSetting
	{
		#region Fields
		TokenRS _token;
		TrackTime _trackTime;
		#endregion

		#region Constructors
		private VehiclesController()
		{
		}

		public VehiclesController(TokenRS token, TrackTime trackTime)
		{
			_token = token;
			_trackTime = trackTime;
		}
		#endregion

		#region Public
		public int GetDefaultVehicleTypeForPoint(double latitude, double longitude)
		{
			#region Init
			UDI.SDS.VehiclesService.SecurityToken utlityToken = _token.ToVehicleServiceToken();
			var vehiclesAdapter = new VehiclesServiceAdapter(utlityToken, _trackTime);
			#endregion

			var result = vehiclesAdapter.GetDefaultVehicleTypeForPoint(latitude, longitude);

			return result;
		}

		public VehiclesNearPoint GetSDSVehicleInfo(decimal latitude, decimal longitude, UDI.VTOD.Common.DTO.Enum.VehicleType vehicleType)
		{
			#region Init
			UDI.SDS.VehiclesService.SecurityToken token = _token.ToVehicleServiceToken();
			var vehilceAdapter = new VehiclesServiceAdapter(token, _trackTime);
			#endregion

			var point = new Point();
			point.VehicleType = (int)vehicleType;
			point.Latitude = latitude.ToDouble();
			point.Longitude = longitude.ToDouble();
			var result = vehilceAdapter.GetVehiclesNearPoint(point);
			return result;
		}
     
		public VehiclesNearPoint GetSDSVehicleInfoCompressed(decimal latitude, decimal longitude, UDI.VTOD.Common.DTO.Enum.VehicleType vehicleType)
		{
			#region Init
			UDI.SDS.VehiclesService.SecurityToken token = _token.ToVehicleServiceToken();
			var vehilceAdapter = new VehiclesServiceAdapter(token, _trackTime);
			#endregion
			VehiclesNearPoint result = null;

			var point = new Point();
			point.VehicleType = (int)vehicleType;
			point.Latitude = latitude.ToDouble();
			point.Longitude = longitude.ToDouble();
			var compressedResult = vehilceAdapter.GetVehiclesNearPointCompressed(point);

			var compressor = new UDI.Utility.Serialization.Compressor();

			var jsonResult = compressor.Decompress(compressedResult);

			//var result2 = xmlResult.CleanNamespace().XmlDataContractDeserialize<SDS.DTO.WCF.VehiclesNearPoint>();
			result = jsonResult.JsonDeserializeWithDataContract<VehiclesNearPoint>();

			return result;
		}

		public List<ProxyService.GetSdsGpsResponse> GetTaxiVehicleInfo(string fleetName)
		{
			#region Init
			UDI.SDS.ProxyService.SecurityToken token = _token.ToProxyServiceToken();
			var proxyAdapter = new ProxyServiceAdapter(token, _trackTime);
			#endregion

			var request = new ProxyService.GetSdsGpsRequest();
			request.companyName = fleetName;

			var result = proxyAdapter.GetSdsGps(request);
			return result;
		}

		/// <summary>
		/// This is for getting status of a vehicle for a booked trip. This method does not reutrn trip status. Just vehicle status
		/// </summary>
		/// <param name="confirmationNumber"></param>
		/// <returns></returns>
		public VehicleAndTripLocation GetVehicleStatusForTrip(string confirmationNumber)
		{
			#region Init
			UDI.SDS.WMVService.SecurityToken token = _token.ToWMVServiceToken();
			var wmvServiceAdapter = new WMVServiceAdapter(token, _trackTime);
			#endregion

			VehicleAndTripLocation result;

			using (var client = new WMVService.WMVServiceClient())
			{

				result = client.GetVehicleAndTripLocation(token, confirmationNumber);

			}

			return result;
		}

		#endregion

		#region Properties
		public TrackTime TrackTime
		{
			get
			{
				return _trackTime;
			}
			set
			{
				_trackTime = value;
			}
		}

		public TokenRS Token
		{
			set
			{
				_token = value;
			}
		}
		#endregion
	}
}
