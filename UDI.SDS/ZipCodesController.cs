﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.Track;
using UDI.SDS.Helper;
using UDI.SDS.ZipCodesService;
using UDI.SDS.DTO.Enum;
using UDI.SDS.DTO;
using UDI.SDS.ServiceAdapter;

namespace UDI.SDS
{
	internal class ZipCodesController : BaseSetting
	{
		#region Fields
		TrackTime _trackTime;
		TokenRS _token;
		#endregion

		#region Constructors
		private ZipCodesController()
		{
		}

		public ZipCodesController(TokenRS token, TrackTime trackTime)
		{
			_token = token;
			_trackTime = trackTime;
		}
		#endregion

		#region Public
		internal List<ZipCodesService.ZipCodeSplitRecord> GetSplitNamesOfServicedPostalCode(string airportCode, string postalCode, int tripDirection, out bool isZipSplit)
		{
			#region Init
			UDI.SDS.ZipCodesService.SecurityToken zipcodesToken = _token.ToZipCodesServiceToken();
			var zipcodesAdapter = new ZipCodesServiceAdapter(zipcodesToken, _trackTime);
			#endregion

			var result = zipcodesAdapter.GetSplitNamesOfServicedPostalCode( airportCode,  postalCode,  tripDirection, out  isZipSplit);

			return result;
		}
		#endregion

		#region Properties
		public TrackTime TrackTime
		{
			get
			{
				return _trackTime;
			}
			set
			{
				_trackTime = value;
			}
		}

		public TokenRS Token
		{
			set
			{
				_token = value;
			}
		}
		#endregion
	}
}
