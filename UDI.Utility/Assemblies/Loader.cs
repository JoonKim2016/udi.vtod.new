﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace UDI.Utility.Assemblies
{
	public class Loader
	{
		public static List<T> LoadAssembly<T>(string filePath, out string error)
		{
			error = string.Empty;
			List<T> result = new List<T>();

			Assembly assembly = Assembly.LoadFrom(filePath);

			foreach (var type in assembly.GetTypes())
			{
				if (type.GetInterfaces().Contains(typeof(T)) || type == typeof(T) || type.BaseType == typeof(T))
				{
					try
					{
						result.Add((T)Activator.CreateInstance(type));
					}
					catch (Exception ex)
					{
						error += ex.Message + "\r\n";
					}
				}
			}

			return result;
		}
	}
}
