﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.Utility.Contract
{
   public  interface ITimeHelper
    {
       DateTime? GetUTCFromLocalTime(double? latitude, double? longitude, DateTime localDateTime);
    }
}

