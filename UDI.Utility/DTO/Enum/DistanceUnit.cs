﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UDI.Utility.DTO.Enum
{
	public enum DistanceUnit
	{
		Mile,
		Kilometer,
		Meter,
		Feet
	}
}
