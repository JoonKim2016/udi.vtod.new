﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.Utility.DTO
{
	public class Error
	{
		public int Code { get; set; }
		public string Message { get; set; }
		public string Type { get; set; }
		public string Exception { get; set; }
		[XmlIgnore]
		[IgnoreDataMember]
		public WebContentFormat Format { get; set; }
	}
}
