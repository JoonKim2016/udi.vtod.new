﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UDI.Utility.DTO
{
	public class Errors
	{
		[XmlElement("Error")]
		public List<Error> Items { get; set; }
	}
}
