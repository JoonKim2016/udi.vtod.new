﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UDI.Utility.DTO
{
	public class UDIException : Exception
	{
		public Error Error { get; set; }
	}
}
