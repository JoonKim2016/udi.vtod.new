﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UDI.Utility.DTO
{
    public class TimeDetailsInfo
    {
        public string status { get; set; }
        public double rawOffset { get; set; }
        public double dstOffset { get; set; }
        public string timeZoneId { get; set; }
        public string timeZoneName { get; set; }

    }
}
