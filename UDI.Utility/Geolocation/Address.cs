﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace UDI.Utility.Geolocation
{
    [DataContract(Namespace="http://unified-data.com/services/Geolocation")]
    public class Address : IAddress
    {
        [DataMember]
        public int AddressNo { get; set; }

        [DataMember]
        public string FullAddress { get; set; }
        
        [DataMember]
        public string StreetNo { get; set; }
        [DataMember]
        public string StreetName { get; set; }
        [DataMember]
        public string StreetType { get; set; }
        [DataMember]
        public string StreetLine2 { get; set; }
        [DataMember]
        public string AptUnitNo { get; set; }
        [DataMember]
        public string BuildingLandmark { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public string StateCode { get; set; }
        [DataMember]
        public string PostalCode { get; set; }

        [DataMember] 
        public double Latitude { get; set; }

        [DataMember]
        public double Longitude { get; set; }
    }

}
