﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
 
namespace UDI.Utility.Geolocation
{ 
    public class AddressLookupResult  : IAddressLookupResult
    {
        public string InputAddress { get; set; } 
        public IList<Address> Matches { get; set; }

    }
     
}
