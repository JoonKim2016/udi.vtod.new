﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace UDI.Utility.Geolocation
{
    public class AddressValidationService
    {
        private string[] _STATE_CODES = new string[] { "AK", "AL", "AR", "AZ", "CA", "CO", "CT", "DC", "DE", "FL", "GA", "GU", "HI", "IA", "ID", "IL", "IN", "KS", "KY","LA","MA","MD","ME","MI","MN","MO","MS","MT","NC","ND","NE","NH","NJ","NM","NV","NY","OH","OK","OR","PA","RI","SC","SD","TN","TX","UT","VA","VT","WA","WI","WV","WY", "ON","BC" };

        public AddressValidationService()
        {
        }

        public bool IsValidAddress(string address)
        {
            //US part
            //this checks the reverse of the address string to make sure either zipcode or city state is supplied.
            Regex zipCodeRe = new Regex(@"^(\d{4}(-\d{5})?)|(\d[A-Z]\d [ABCEGHJKLMNPRSTVXY]\d[A-Z])(\s{0,}\,{0,1})[\w\d\s\.\#\,]{1,100}$");
            Regex cityStateRe = new Regex(@"^([\w]{2}(\s{0,}\,{0,1})([\w\s\d]{1,})(\s{0,}\,{0,1}))[\w\d\s\.\#\,]{1,100}$");
            

            address = address.ToUpper().Trim();
            string reverseAddress = new string(address.Reverse().ToArray());
            if (zipCodeRe.IsMatch(reverseAddress))
                return true;
            else if (cityStateRe.IsMatch(reverseAddress) && reverseAddress.Length > 2 && _STATE_CODES.Contains(address.Substring(address.Length - 2, 2).ToUpper()))
                return true;

            //Canda part
            string canada_zip6= @"[A-Z][0-9][A-Z][0-9][A-Z][0-9]";
            string canada_zip3=@"[A-Z][0-9][A-Z]";
            string[] sep = { " " };

            string[] address_part= address.Replace("-"," ").Split(sep,StringSplitOptions.RemoveEmptyEntries).ToArray();


            string last6 = address_part[address_part.Length - 2] + address_part[address_part.Length - 1];
            string last3 = address_part[address_part.Length - 1];


            if (Regex.IsMatch(last6, canada_zip6))
            {
                return true;
            }else if (Regex.IsMatch(last3, canada_zip3))
            {
                return true;
            }

            return false;
        }
    }
}
