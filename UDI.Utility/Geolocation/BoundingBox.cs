﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
namespace UDI.Utility.Geolocation
{ 
    public class BoundingBox : IBoundingBox
    {
        public decimal NWLat { get; set; }
        public decimal NWLong { get; set; }
        public decimal SELat { get; set; }
        public decimal SELong { get; set; } 

    }

}
