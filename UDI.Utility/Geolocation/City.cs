﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UDI.Utility.Geolocation
{
    public class City
    {
        private string _contactPhoneNo;

        public string DisplayName { get; set; }
        public string Name { get; set; }
        public string ContactPhoneNo
        {
            get
            {
                return _contactPhoneNo;
            }
            set
            {
                _contactPhoneNo = null;

                if (value != null)
                    _contactPhoneNo = value.DigitsOnly();
            }
        }

        public City(string name, string displayName, string contactPhoneNo)
        {
            Name = name;
            DisplayName = displayName;
            ContactPhoneNo = contactPhoneNo;
        }
    }
}
