﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;

namespace UDI.Utility.Geolocation
{
    [DataContract(Namespace="http://unified-data.com/services/Geolocation")]
    public class DetailedAddress  
    { 
        [DataMember]
        public string StreetNo { get; set; }

        [DataMember]
        public string StreetName { get; set; }

        [DataMember]
        public string StreetType { get; set; }

        [DataMember]
        public string StreetLine2 { get; set; }
        [DataMember]
        public string LandmarkBldg { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public string StateCode { get; set; }

        [DataMember]
        public string CountryCode { get; set; }
        [DataMember]
        public string CountryName { get; set; }

        [DataMember]
        public string PostalCode { get; set; }

        public string ToGeolocationAddress()
        {
            string addr = StreetNo + " " + StreetName + " " + StreetType;

            if (!String.IsNullOrEmpty(SuiteAptNo))
                addr += " #" + SuiteAptNo;

            if (!String.IsNullOrEmpty(City))
                addr += ", " + City;

            if (!String.IsNullOrEmpty(StateCode))
                addr += ", " + StateCode;

            if (!String.IsNullOrEmpty(PostalCode))
                addr += " " + PostalCode;

            if (!String.IsNullOrEmpty(CountryName))
                addr += " " + CountryName;

            return addr;
        }

        public string SuiteAptNo { get; set; }

        public string StreetLine1
        {
            get
            {
                string line = "";

                if (!String.IsNullOrEmpty(StreetNo))
                    line += StreetNo + " ";

                if (!String.IsNullOrEmpty(StreetName))
                    line += StreetName + " ";

                if (!String.IsNullOrEmpty(StreetType))
                    line += StreetType + " ";

                if (!String.IsNullOrEmpty(SuiteAptNo))
                    line += "#" + SuiteAptNo;

                return line.Trim();
            }
        }
        
        private string[] _STATE_CODES = new string[] { "AK", "AL", "AR", "AZ", "CA", "CO", "CT", "DC", "DE", "FL", "GA", "GU", "HI", "IA", "ID", "IL", "IN", "KS", "KY","LA","MA","MD","ME","MI","MN","MO","MS","MT","NC","ND","NE","NH","NJ","NM","NV","NY","OH","OK","OR","PA","RI","SC","SD","TN","TX","UT","VA","VT","WA","WI","WV","WY" };

        public DetailedAddress( )
        { 
        }
        public DetailedAddress(Address address)
        {
            this.City = address.City;
            this.PostalCode = address.PostalCode;
            this.StateCode = address.StateCode;
            this.StreetNo = address.StreetNo;
            this.StreetName = address.StreetName;
            this.StreetType = address.StreetType;
            this.StreetLine2 = address.StreetLine2;

        }
         
        public bool IsValidAddress(string address)
        {
            //this checks the reverse of the address string to make sure either zipcode or city state is supplied.
            Regex zipCodeRe = new Regex(@"^([\d]{5})(\s{0,}\,{0,1})[\w\d\s\.\#\,]{1,100}$");
            Regex cityStateRe = new Regex(@"^([\w]{2}(\s{0,}\,{0,1})([\w\s\d]{1,})(\s{0,}\,{0,1}))[\w\d\s\.\#\,]{1,100}$");
            address = address.Trim();
            string reverseAddress = new string(address.Reverse().ToArray());

            if (zipCodeRe.IsMatch(reverseAddress))
                return true;
            else if (cityStateRe.IsMatch(reverseAddress) && reverseAddress.Length > 2 && _STATE_CODES.Contains(address.Substring(address.Length - 2, 2).ToUpper()))
                return true;

            return false;
        } 
    }

}
