﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace UDI.Utility
{
    public static class AssemblyExtensions
    {
        public static string AssemblyName(this Assembly a)
        {
            if (!a.FullName.Contains(','))
                return a.FullName;
            return a.FullName.Split(',')[0];
        }

        public static string SingleName(this Assembly a)
        {
            string baseName = a.AssemblyName();
            return baseName.Split('.').Last();
        }
    }
}
