﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;


namespace UDI.Utility
{
    public static class StringExtensions
    {
        public static bool IsValidPhoneNumber(this string input)
        {
            string digits = input.DigitsOnly();
            return (digits.Length == 10 && !digits.StartsWith("1")) || (digits.Length == 11 && digits.StartsWith("1"));
        }

        public static string DigitsOnly(this string input)
        {
            return String.Join("", input.ToCharArray().Where(c => c >= '0' && c <= '9').Select(c => c.ToString()).ToArray());
        }

        public static string AsDataPhoneNumber(this string input)
        {
            string digits = input.DigitsOnly();
            if (digits.Length == 11 && digits.StartsWith("1"))
                return digits.Substring(1);
            else
                return digits;
        }

       

        public static string AsFormattedPhoneNumber(this string input)
        {
            string digits = input.DigitsOnly();
            if (digits.Length == 11 && digits.StartsWith("1"))
            {
                return "(" + digits.Substring(1, 3) + ") " + digits.Substring(4, 3) + "-" + digits.Substring(7, 4);
            } 
            else
                return digits;
        } 
        public static bool IsNumeric(this string input)
        {
			if (string.IsNullOrWhiteSpace(input))
			{
				return false;
			}
            return input.ToCharArray().All(x => x >= '0' && x <= '9');
        }



        public static string StreetNoFromAddressLine1(this string line1)
        {
            if (line1 != null)
            {
                string[] parts = line1.Split(' ');

                if (parts != null && parts.Length > 0)
                    return parts[0];
            }
            return null;
        }
        public static string StreetNameFromAddressLine1(this string line1)
        {
            if (line1 != null)
            {
                string[] parts = line1.Split(' ');

                if (parts != null && parts.Length > 0)
                {
                    parts = parts.Skip(1).Take(parts.Length - 2).ToArray();
                    return string.Join(" ", parts);
                }
            }
            return null;

        }
        public static string StreetTypeFromAddressLine1(this string line1)
        {
            if (line1 != null)
            {
                string[] parts = line1.Split(' ');

                if (parts != null && parts.Length > 0)
                    return parts[parts.Length - 1];

            }
            return null;
        }
    }
}
