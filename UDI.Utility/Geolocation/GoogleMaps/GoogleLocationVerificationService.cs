﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;


namespace UDI.Utility.Geolocation
{

    public class GoogleLocationVerificationService : ILocationVerificationService
    {
        private GoogleMapsApiWrapper _googleMaps;

        public GoogleLocationVerificationService(GoogleMapsApiWrapper googleMaps)
        {
            _googleMaps = googleMaps;
        }

        #region interface implementation

            public IAddressLookupResult LookupAddress(string address)
            {
                return LookupAddress(address, null);
            }

        /// <summary>
        /// Added By michael.huang Jan 30, 2012 
        /// 01. It's just a simple way to get city from google service and extract city part from JSON object
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
            public string LookupCityName(string address)
            {
                return _googleMaps.GetCityName(address);
            }

            public IAddressLookupResult LookupAddress(string address, IBoundingBox bounds)
            {
                AddressLookupResult lookupResult = new AddressLookupResult();
                GoogleMapsApiResponse googleApiResponse;

                googleApiResponse = _googleMaps.Search(address, bounds);

                lookupResult.InputAddress = address;
                lookupResult.Matches = new List<Address>();

                if (googleApiResponse.Status.code == 200)
                {
                    int matches = 0;
                    for (int i = 0; i < 3 && i < googleApiResponse.Placemark.Count; i++)
                    {
                        //if (isWithinBounds(googleApiResponse.Placemark[i].Point.Latitude, googleApiResponse.Placemark[i].Point.Longitude, bounds))
                        //{ 
                            lookupResult.Matches.Add(new Address() 
                                                    { 
                                                        FullAddress = googleApiResponse.Placemark[i].address.Replace(", USA",""),
                                                        AddressNo = matches + 1
                                                    });
                            matches++;
                        //}
                    } 
                }
                else
                {
                    //not sure if we need to handle anything.
                }

                return lookupResult;
            }

            public IAddressLookupResult LookupAddress(string streetNo, string streetName, string streetType, string streetLine2, string aptUnitNo, string city, string stateCode, string zip)
            {
                string address = buildAddressString(streetNo, streetName, streetType, streetLine2, aptUnitNo, city, stateCode, zip);

                return LookupAddress(address);
            }

            private string buildAddressString(string streetNo, string streetName, string streetType, string streetLine2, string aptUnitNo, string city, string stateCode, string zip)
            {
                string address = "";

                address = streetNo + " " + streetName + " " + streetType + ", ";

                if (!string.IsNullOrEmpty(city))
                    address += city + ", ";
                if (!string.IsNullOrEmpty(stateCode))
                    address += stateCode + " ";
                if (!string.IsNullOrEmpty(zip))
                    address += zip;

                address = address.Trim(new char[] { ' ', ',' });

                return address;
            }
        #endregion

            private bool isWithinBounds(decimal latitude, decimal longitude, IBoundingBox bounds)
            {
                if (bounds == null)
                    return true;

                //latitude must be between NWLat and SELat
                //longitude must be between NWLong and SELong
                return latitude <= bounds.NWLat && latitude >= bounds.SELat
                            && longitude >= bounds.NWLong && longitude <= bounds.SELong;

            }
    }
}
