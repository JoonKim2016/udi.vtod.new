﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using System.Web;
using System.Net;
using System.IO;

namespace UDI.Utility.Geolocation
{
	public class GoogleMapsApiResponse
	{
		public string name { get; set; }
		public ApiResponseStatus Status { get; set; }
		public IList<ApiPlacemark> Placemark { get; set; }

	}

	public class ApiResponseStatus
	{
		public int code { get; set; }
		public string request { get; set; }

	}

	public class ApiPoint
	{
		public List<decimal> Coordinates { get; set; }

		public decimal Longitude
		{
			get
			{
				return (this.Coordinates != null) ? this.Coordinates[0] : 0;
			}
		}
		public decimal Latitude
		{
			get
			{
				return (this.Coordinates != null) ? this.Coordinates[1] : 0;
			}
		}
		//more fields availabe from api, but not added here since they are not needed.
	}

	public class ApiPlacemark
	{
		public string id { get; set; }
		public string address { get; set; }

		public ApiPoint Point { get; set; }

		public ApiAddressDetails AddressDetails { get; set; }

		//more fields availabe from api, but not added here since they are not needed.
	}

	public class ApiAddressDetails
	{
		public string Accuracy { get; set; }
		public ApiCountry Country { get; set; }
	}

	public class ApiCountry
	{
		public ApiAdministrativeArea AdministrativeArea { get; set; }
		public string CountryName { get; set; }
		public string CountryNameCode  { get; set; }
	}

	public class ApiAdministrativeArea
	{
		public string AdministrativeAreaName { get; set; }
		public ApiSubAdministrativeArea SubAdministrativeArea { get; set; }
		public ApiLocality Locality { get; set; }
	}

	public class ApiSubAdministrativeArea
	{
		public string SubAdministrativeAreaName { get; set; }
		public ApiLocality Locality { get; set; }
	}
	
	public class ApiLocality
	{
		public string LocalityName { get; set; }
	}
}

#region Response Sample
#region AdministrativeArea
/*
 * {
  "name": "1122 E Elk Ave",
  "Status": {
    "code": 200,
    "request": "geocode"
  },
  "Placemark": [ {
    "id": "p1",
    "address": "1122 E Elk Ave, Glendale, CA 91205, USA",
    "AddressDetails": {
   "Accuracy" : 8,
   "Country" : {
      "AdministrativeArea" : {
         "AdministrativeAreaName" : "CA",
         "Locality" : {
            "LocalityName" : "Glendale",
            "PostalCode" : {
               "PostalCodeNumber" : "91205"
            },
            "Thoroughfare" : {
               "ThoroughfareName" : "1122 E Elk Ave"
            }
         }
      },
      "CountryName" : "USA",
      "CountryNameCode" : "US"
   }
},
    "ExtendedData": {
      "LatLonBox": {
        "north": 34.1424320,
        "south": 34.1397340,
        "east": -118.2392810,
        "west": -118.2419790
      }
    },
    "Point": {
      "coordinates": [ -118.2406300, 34.1410830, 0 ]
    }
  }, {
    "id": "p2",
    "address": "1122 E Elk Ave, Elizabethton, TN 37643, USA",
    "AddressDetails": {
   "Accuracy" : 8,
   "Country" : {
      "AdministrativeArea" : {
         "AdministrativeAreaName" : "TN",
         "Locality" : {
            "LocalityName" : "Elizabethton",
            "PostalCode" : {
               "PostalCodeNumber" : "37643"
            },
            "Thoroughfare" : {
               "ThoroughfareName" : "E Elk Ave"
            }
         }
      },
      "CountryName" : "USA",
      "CountryNameCode" : "US"
   }
},
    "ExtendedData": {
      "LatLonBox": {
        "north": 36.3503364,
        "south": 36.3476384,
        "east": -82.2070587,
        "west": -82.2097567
      }
    },
    "Point": {
      "coordinates": [ -82.2084058, 36.3489800, 0 ]
    }
  }, {
    "id": "p3",
    "address": "1122 E Elk St, Beatrice, NE 68310, USA",
    "AddressDetails": {
   "Accuracy" : 8,
   "Country" : {
      "AdministrativeArea" : {
         "AdministrativeAreaName" : "NE",
         "Locality" : {
            "LocalityName" : "Beatrice",
            "PostalCode" : {
               "PostalCodeNumber" : "68310"
            },
            "Thoroughfare" : {
               "ThoroughfareName" : "E Elk St"
            }
         }
      },
      "CountryName" : "USA",
      "CountryNameCode" : "US"
   }
},
    "ExtendedData": {
      "LatLonBox": {
        "north": 40.2692838,
        "south": 40.2665859,
        "east": -96.7371312,
        "west": -96.7398292
      }
    },
    "Point": {
      "coordinates": [ -96.7384801, 40.2679420, 0 ]
    }
  }, {
    "id": "p4",
    "address": "1122 E Elk St, Dexter, MO 63841, USA",
    "AddressDetails": {
   "Accuracy" : 8,
   "Country" : {
      "AdministrativeArea" : {
         "AdministrativeAreaName" : "MO",
         "Locality" : {
            "LocalityName" : "Dexter",
            "PostalCode" : {
               "PostalCodeNumber" : "63841"
            },
            "Thoroughfare" : {
               "ThoroughfareName" : "E Elk St"
            }
         }
      },
      "CountryName" : "USA",
      "CountryNameCode" : "US"
   }
},
    "ExtendedData": {
      "LatLonBox": {
        "north": 36.7953154,
        "south": 36.7926174,
        "east": -89.9420179,
        "west": -89.9447159
      }
    },
    "Point": {
      "coordinates": [ -89.9433673, 36.7939589, 0 ]
    }
  } ]
}
*/
#endregion
#region SubAdministrativeArea
/*
{
  "name": "Powell St",
  "Status": {
    "code": 200,
    "request": "geocode"
  },
  "Placemark": [ {
    "id": "p1",
    "address": "Powell St, San Antonio, TX 78204, USA",
    "AddressDetails": {
   "Accuracy" : 6,
   "Country" : {
      "AdministrativeArea" : {
         "AdministrativeAreaName" : "TX",
         "SubAdministrativeArea" : {
            "Locality" : {
               "DependentLocality" : {
                  "DependentLocalityName" : "Harris",
                  "PostalCode" : {
                     "PostalCodeNumber" : "78204"
                  },
                  "Thoroughfare" : {
                     "ThoroughfareName" : "Powell St"
                  }
               },
               "LocalityName" : "San Antonio"
            },
            "SubAdministrativeAreaName" : "Bexar"
         }
      },
      "CountryName" : "USA",
      "CountryNameCode" : "US"
   }
},
    "ExtendedData": {
      "LatLonBox": {
        "north": 29.4053170,
        "south": 29.4022825,
        "east": -98.5095233,
        "west": -98.5122213
      }
    },
    "Point": {
      "coordinates": [ -98.5109344, 29.4037992, 0 ]
    }
  }, {
    "id": "p2",
    "address": "Powell St, Brownfield, TX 79316, USA",
    "AddressDetails": {
   "Accuracy" : 6,
   "Country" : {
      "AdministrativeArea" : {
         "AdministrativeAreaName" : "TX",
         "SubAdministrativeArea" : {
            "Locality" : {
               "LocalityName" : "Brownfield",
               "PostalCode" : {
                  "PostalCodeNumber" : "79316"
               },
               "Thoroughfare" : {
                  "ThoroughfareName" : "Powell St"
               }
            },
            "SubAdministrativeAreaName" : "Terry"
         }
      },
      "CountryName" : "USA",
      "CountryNameCode" : "US"
   }
},
    "ExtendedData": {
      "LatLonBox": {
        "north": 33.1824644,
        "south": 33.1797664,
        "east": -102.2733432,
        "west": -102.2760411
      }
    },
    "Point": {
      "coordinates": [ -102.2744940, 33.1811010, 0 ]
    }
  }, {
    "id": "p3",
    "address": "Powell St, Emeryville, CA, USA",
    "AddressDetails": {
   "Accuracy" : 6,
   "Country" : {
      "AdministrativeArea" : {
         "AdministrativeAreaName" : "CA",
         "Locality" : {
            "LocalityName" : "Emeryville",
            "Thoroughfare" : {
               "ThoroughfareName" : "Powell St"
            }
         }
      },
      "CountryName" : "USA",
      "CountryNameCode" : "US"
   }
},
    "ExtendedData": {
      "LatLonBox": {
        "north": 37.8418289,
        "south": 37.8305593,
        "east": -122.2839021,
        "west": -122.3197130
      }
    },
    "Point": {
      "coordinates": [ -122.3014902, 37.8371782, 0 ]
    }
  }, {
    "id": "p4",
    "address": "Powell St, Middleport, OH 45760, USA",
    "AddressDetails": {
   "Accuracy" : 6,
   "Country" : {
      "AdministrativeArea" : {
         "AdministrativeAreaName" : "OH",
         "SubAdministrativeArea" : {
            "Locality" : {
               "LocalityName" : "Middleport",
               "PostalCode" : {
                  "PostalCodeNumber" : "45760"
               },
               "Thoroughfare" : {
                  "ThoroughfareName" : "Powell St"
               }
            },
            "SubAdministrativeAreaName" : "Meigs"
         }
      },
      "CountryName" : "USA",
      "CountryNameCode" : "US"
   }
},
    "ExtendedData": {
      "LatLonBox": {
        "north": 38.9966929,
        "south": 38.9939950,
        "east": -82.0620893,
        "west": -82.0703495
      }
    },
    "Point": {
      "coordinates": [ -82.0656712, 38.9946080, 0 ]
    }
  }, {
    "id": "p5",
    "address": "Powell St, Lowell, MA 01851, USA",
    "AddressDetails": {
   "Accuracy" : 6,
   "Country" : {
      "AdministrativeArea" : {
         "AdministrativeAreaName" : "MA",
         "Locality" : {
            "LocalityName" : "Lowell",
            "PostalCode" : {
               "PostalCodeNumber" : "01851"
            },
            "Thoroughfare" : {
               "ThoroughfareName" : "Powell St"
            }
         }
      },
      "CountryName" : "USA",
      "CountryNameCode" : "US"
   }
},
    "ExtendedData": {
      "LatLonBox": {
        "north": 42.6328557,
        "south": 42.6262174,
        "east": -71.3222927,
        "west": -71.3249906
      }
    },
    "Point": {
      "coordinates": [ -71.3238878, 42.6295591, 0 ]
    }
  }, {
    "id": "p6",
    "address": "Powell St, San Francisco, CA, USA",
    "AddressDetails": {
   "Accuracy" : 6,
   "Country" : {
      "AdministrativeArea" : {
         "AdministrativeAreaName" : "CA",
         "Locality" : {
            "LocalityName" : "San Francisco",
            "Thoroughfare" : {
               "ThoroughfareName" : "Powell St"
            }
         }
      },
      "CountryName" : "USA",
      "CountryNameCode" : "US"
   }
},
    "ExtendedData": {
      "LatLonBox": {
        "north": 37.8086900,
        "south": 37.7845310,
        "east": -122.4074000,
        "west": -122.4128690
      }
    },
    "Point": {
      "coordinates": [ -122.4100655, 37.7965834, 0 ]
    }
  }, {
    "id": "p7",
    "address": "Powell St, Henderson, KY, USA",
    "AddressDetails": {
   "Accuracy" : 6,
   "Country" : {
      "AdministrativeArea" : {
         "AdministrativeAreaName" : "KY",
         "SubAdministrativeArea" : {
            "Locality" : {
               "LocalityName" : "Henderson",
               "Thoroughfare" : {
                  "ThoroughfareName" : "Powell St"
               }
            },
            "SubAdministrativeAreaName" : "Henderson"
         }
      },
      "CountryName" : "USA",
      "CountryNameCode" : "US"
   }
},
    "ExtendedData": {
      "LatLonBox": {
        "north": 37.8379330,
        "south": 37.8236277,
        "east": -87.5685281,
        "west": -87.5964209
      }
    },
    "Point": {
      "coordinates": [ -87.5829708, 37.8298319, 0 ]
    }
  }, {
    "id": "p8",
    "address": "Powell St, Gurley, AL 35748, USA",
    "AddressDetails": {
   "Accuracy" : 6,
   "Country" : {
      "AdministrativeArea" : {
         "AdministrativeAreaName" : "AL",
         "SubAdministrativeArea" : {
            "Locality" : {
               "LocalityName" : "Gurley",
               "PostalCode" : {
                  "PostalCodeNumber" : "35748"
               },
               "Thoroughfare" : {
                  "ThoroughfareName" : "Powell St"
               }
            },
            "SubAdministrativeAreaName" : "Madison"
         }
      },
      "CountryName" : "USA",
      "CountryNameCode" : "US"
   }
},
    "ExtendedData": {
      "LatLonBox": {
        "north": 34.7685180,
        "south": 34.7570176,
        "east": -86.4121170,
        "west": -86.4208040
      }
    },
    "Point": {
      "coordinates": [ -86.4187490, 34.7637790, 0 ]
    }
  }, {
    "id": "p9",
    "address": "Powell St, Waskom, TX 75692, USA",
    "AddressDetails": {
   "Accuracy" : 6,
   "Country" : {
      "AdministrativeArea" : {
         "AdministrativeAreaName" : "TX",
         "SubAdministrativeArea" : {
            "Locality" : {
               "LocalityName" : "Waskom",
               "PostalCode" : {
                  "PostalCodeNumber" : "75692"
               },
               "Thoroughfare" : {
                  "ThoroughfareName" : "Powell St"
               }
            },
            "SubAdministrativeAreaName" : "Harrison"
         }
      },
      "CountryName" : "USA",
      "CountryNameCode" : "US"
   }
},
    "ExtendedData": {
      "LatLonBox": {
        "north": 32.4854512,
        "south": 32.4804307,
        "east": -94.0626871,
        "west": -94.0653851
      }
    },
    "Point": {
      "coordinates": [ -94.0642350, 32.4823460, 0 ]
    }
  }, {
    "id": "p10",
    "address": "Powell St, Brookline, MA, USA",
    "AddressDetails": {
   "Accuracy" : 6,
   "Country" : {
      "AdministrativeArea" : {
         "AdministrativeAreaName" : "MA",
         "Locality" : {
            "LocalityName" : "Brookline",
            "Thoroughfare" : {
               "ThoroughfareName" : "Powell St"
            }
         }
      },
      "CountryName" : "USA",
      "CountryNameCode" : "US"
   }
},
    "ExtendedData": {
      "LatLonBox": {
        "north": 42.3471695,
        "south": 42.3439376,
        "east": -71.1136444,
        "west": -71.1163424
      }
    },
    "Point": {
      "coordinates": [ -71.1151869, 42.3454099, 0 ]
    }
  }, {
    "id": "p11",
    "address": "Powell St, Heritage Park QLD 4118, Australia",
    "AddressDetails": {
   "Accuracy" : 6,
   "Country" : {
      "AdministrativeArea" : {
         "AdministrativeAreaName" : "QLD",
         "Locality" : {
            "LocalityName" : "Heritage Park",
            "PostalCode" : {
               "PostalCodeNumber" : "4118"
            },
            "Thoroughfare" : {
               "ThoroughfareName" : "Powell St"
            }
         }
      },
      "CountryName" : "Australia",
      "CountryNameCode" : "AU"
   }
},
    "ExtendedData": {
      "LatLonBox": {
        "north": -27.6850200,
        "south": -27.6882920,
        "east": 153.0650521,
        "west": 153.0617255
      }
    },
    "Point": {
      "coordinates": [ 153.0638751, -27.6864416, 0 ]
    }
  } ]
}
*/
#endregion
#endregion