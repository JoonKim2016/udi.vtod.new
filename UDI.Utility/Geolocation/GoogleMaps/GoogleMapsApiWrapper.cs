﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions; 
using System.Web.Script.Serialization;
using System.Web;
using System.Net;
using System.IO;
using System.Runtime.Serialization.Json;


namespace UDI.Utility.Geolocation
{  
    public class GoogleMapsApiWrapper
    {
        private string _googleApiKey; 
        private string _geoLocationUrlFormat = "http://maps.google.com/maps/geo?output={0}&key={1}&region=us&q={2}&bounds={3}";

        //make sure to bind in Ninject
        public GoogleMapsApiWrapper(string googleApiKey)
        {
            _googleApiKey = googleApiKey; 
        }

        #region public methods
            //Note: bounds does not restrict the google api query, but it does "favor" those results.
            //To filter out all addresses outside of the bounds, we'll need to filter them out on our side.
            public GoogleMapsApiResponse Search(string address, IBoundingBox bounds)
            {
                GoogleMapsApiResponse googleResponse;
                string strJsonResponse;
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                strJsonResponse = processApiRequest(address, bounds);
                googleResponse = serializer.Deserialize<GoogleMapsApiResponse>(strJsonResponse);

                return googleResponse;
            }

            /// <summary>
            /// added by michael.huang Jan 30, 2012
            /// 01. Use processApiRequest to get a JSON response from google
            /// 02. Use regular expression to extract the city name
            /// </summary>
            /// <param name="address"></param>
            /// <param name="bounds"></param>
            /// <returns></returns>
            public string GetCityName(string address)
            {
                string strJsonResponse = processApiRequest(address,null);

                string pattern=@"""Locality""[\s]{0,}:[\s]{0,}\{[\s\r\n]{0,}""LocalityName""[\s]{0,}:[\s]{0,}""(.*)"",";
                string city=Regex.Match(strJsonResponse, pattern).Groups[1].Value;

                return city;
            }

        #endregion


        #region private methods


            private string processApiRequest(string address, IBoundingBox bounds)
            {
                string apiRequest = string.Format(_geoLocationUrlFormat, "json", _googleApiKey, HttpUtility.UrlEncode(address), getBoundsParam(bounds));
            
                //make request to google maps api
                var request = WebRequest.Create(apiRequest);
                var response = (HttpWebResponse)request.GetResponse();
            
                //read into stream
                var ms = new MemoryStream();
                var buffer = new Byte[2048];
                var responseStream = response.GetResponseStream();

                int count = responseStream.Read(buffer, 0, buffer.Length);

                while (count > 0)
                {
                    ms.Write(buffer, 0, count);
                    count = responseStream.Read(buffer, 0, buffer.Length);
                }

                responseStream.Close();
                ms.Close();

                var responseBytes = ms.ToArray();
                var encoding = new System.Text.ASCIIEncoding();

                return encoding.GetString(responseBytes);
            }

            private const string _BOUNDS_FORMAT = "{0},{1}|{2},{3}";

            private string getBoundsParam(IBoundingBox bounds)
            {
                if (bounds != null)
                {
                    return string.Format(_BOUNDS_FORMAT, bounds.NWLat.ToString("N3"), bounds.NWLong.ToString("N3"), bounds.SELat.ToString("N3"), bounds.SELong.ToString("N3"));
                }

                return "";
            }


        #endregion
    }


}
