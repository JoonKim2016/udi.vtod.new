﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.Runtime.Serialization;


namespace UDI.Utility.Geolocation
{  
    public interface IAddress
    {
        int AddressNo { get; set; } 
        string FullAddress { get; set; }  
    }

}
