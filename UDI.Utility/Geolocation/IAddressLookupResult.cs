﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UDI.Utility.Geolocation
{ 
    public interface IAddressLookupResult  
    {
        string InputAddress { get; set; } 
        IList<Address> Matches { get; set; }

    }
     
}
