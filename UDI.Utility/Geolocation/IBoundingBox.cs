﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 

namespace UDI.Utility.Geolocation
{ 
    public interface IBoundingBox 
    {
        decimal NWLat { get; set; }
        decimal NWLong { get; set; }
        decimal SELat { get; set; }
        decimal SELong { get; set; } 
    }

}
