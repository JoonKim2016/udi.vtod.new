﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 

namespace UDI.Utility.Geolocation
{
    public interface ILocationVerificationService
    {
        IAddressLookupResult LookupAddress(string address);
        IAddressLookupResult LookupAddress(string address, IBoundingBox bounds);
        IAddressLookupResult LookupAddress(string streetNo, string streetName, string streetType, string streetLine2, string aptUnitNo, string city, string stateCode, string zip);
    }
}
