﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;


namespace UDI.Utility.Geolocation
{

    public class MockAddressLookupService : ILocationVerificationService
    {
        public IAddressLookupResult LookupAddress(string address)
        {
            return LookupAddress(address, null);
        }

        public IAddressLookupResult LookupAddress(string address, IBoundingBox bounds)
        {
            AddressLookupResult response = new AddressLookupResult();

            //NEED TO IMPLEMENT REAL ONE
            if (address.StartsWith("3"))
            {
                response.InputAddress = address;
                response.Matches = new List<Address>();
                response.Matches.Add(new Address() { FullAddress = address + " #1", AddressNo = 1 });
                response.Matches.Add(new Address() { FullAddress = address + " #2", AddressNo = 2 });
                response.Matches.Add(new Address() { FullAddress = address + " #3", AddressNo = 3 });
            }
            else if (address.StartsWith("9"))
            {
                response.InputAddress = address; 
            }
            else
            {
                response.InputAddress = address;
                response.Matches = new List<Address>();
                response.Matches.Add(new Address() { FullAddress = address, AddressNo = 1 });
            }

            return response;
        }

        public IAddressLookupResult LookupAddress(string streetNo, string streetName, string streetType, string streetLine2, string aptUnitNo, string city, string stateCode, string zip)
        {
            string address = buildAddressString(streetNo, streetName, streetType, streetLine2, aptUnitNo, city, stateCode, zip);

            return LookupAddress(address);
        }

        private string buildAddressString(string streetNo, string streetName, string streetType, string streetLine2, string aptUnitNo, string city, string stateCode, string zip)
        {
            string address = "";

            address = streetNo + " " + streetName + " " + streetType + ", ";

            if (!string.IsNullOrEmpty(city))
                address += city + ", ";
            if (!string.IsNullOrEmpty(stateCode))
                address += stateCode + " ";
            if (!string.IsNullOrEmpty(zip))
                address += zip;

            address = address.Trim(new char[] { ' ', ',' });

            return address;
        }
        //private void parseCity(BookingMessage msg, string[] words, ref int currIndex)
        //{
        //    string city = "";
        //    while (currIndex >= 0 && !isStreetType(words[currIndex]) && !isAptNumber(words[currIndex]))
        //    {
        //        city = words[currIndex] + city;
        //        currIndex--;
        //    }

        //    msg.City = city;
        //}


        //private const string _ZIP_REGEX = @"^\d{5}([\-]\d{4})?$";

        //private bool isZipCode(string word)
        //{
        //    Regex zipRE = new Regex(_ZIP_REGEX);

        //    return zipRE.IsMatch(word.Trim());
        //}

        ////TODO: change to check state list
        //private bool isStateCode(string word)
        //{
        //    return word.Length == 2;
        //}

        ////TODO: change to check state list
        //private bool isCity(string word)
        //{
        //    Regex re = new Regex(@"^(\w|\s)$");
        //    return re.IsMatch(word);
        //}

        ////TODO: change to check state list
        //private bool isAptNumber(string word)
        //{
        //    //#4 #4a #abd
        //    Regex re = new Regex(@"^#(\w|\d){1,}$");
        //    return re.IsMatch(word);
        //}

        //private const string[] _STREET_TYPES = new string[] { "st", "street", "av", "ave", "rd", "road", "ln", "lane", "bl", "blvd", "way", "wy", "dr", "drive" };

        //private bool isStreetType(string word)
        //{
        //    return _STREET_TYPES.Contains(word.ToUpperInvariant().Replace(".", ""));
        //}

    }
}
