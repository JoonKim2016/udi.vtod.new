﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UDI.Utility.Geolocation
{
    public class State
    { 
        public State(string code, string name)
        {
            Code = code;
            Name = name;
        }
        public string Code { get; set; } 
        public string Name { get; set; }
    }
}
