﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;


namespace UDI.Utility.Geolocation
{

    public class YahooLocationVerificationService : ILocationVerificationService
    {
        private YahooPlaceFinderWrapper _yahooApi;

        public YahooLocationVerificationService(YahooPlaceFinderWrapper yahooApi)
        {
            _yahooApi = yahooApi;
        }

        #region interface implementation

            public IAddressLookupResult LookupAddress(string address)
            {
                return LookupAddress(address, null);
            }

            public IAddressLookupResult LookupAddress(string streetNo, string streetName, string streetType, string streetLine2, string aptUnitNo, string city, string stateCode, string zip)
            {
                string address= buildAddressString(streetNo, streetName, streetType, streetLine2, aptUnitNo, city, stateCode, zip);

                return LookupAddress(address);
            }

            private string buildAddressString(string streetNo, string streetName, string streetType, string streetLine2, string aptUnitNo, string city, string stateCode, string zip)
            {
                string address = "";

                address = streetNo + " " + streetName + " " + streetType + ", ";

                if (!string.IsNullOrEmpty(city)) 
                    address += city + ", ";
                if (!string.IsNullOrEmpty(stateCode))
                    address += stateCode + " ";
                if (!string.IsNullOrEmpty(zip))
                    address += zip;

                address = address.Trim(new char[] { ' ' , ','});
                 
                return address;
            }
  
            public IAddressLookupResult LookupAddress(string address, IBoundingBox bounds)
            {
                AddressLookupResult lookupResult = new AddressLookupResult();
                YahooPlaceFinderResponse yahooResponse;

                yahooResponse = _yahooApi.Search(address);

                lookupResult.InputAddress = address;
                lookupResult.Matches = new List<Address>();

                if (yahooResponse.ResultSet != null && yahooResponse.ResultSet.Error == 0)
                {
                    int matches = 0;
                    for (int i = 0; i < 3 && i < yahooResponse.ResultSet.Results.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(yahooResponse.ResultSet.Results[i].line1))
                        {
                            lookupResult.Matches.Add(new Address()
                            {
                                FullAddress = yahooResponse.ResultSet.Results[i].FullAddress,
                                StreetNo =  yahooResponse.ResultSet.Results[i].line1.StreetNoFromAddressLine1() ,
                                StreetName = yahooResponse.ResultSet.Results[i].line1.StreetNameFromAddressLine1(),
                                StreetType = yahooResponse.ResultSet.Results[i].line1.StreetTypeFromAddressLine1(),
                                StreetLine2 = yahooResponse.ResultSet.Results[i].line2,
                                City = yahooResponse.ResultSet.Results[i].city,
                                StateCode = yahooResponse.ResultSet.Results[i].statecode,
                                PostalCode = yahooResponse.ResultSet.Results[i].uzip,
                                AddressNo = matches + 1,
                                Latitude = yahooResponse.ResultSet.Results[i].latitude,
                                Longitude = yahooResponse.ResultSet.Results[i].longitude,
                            });
                            matches++; 
                        }
                    } 
                }
                else
                {
                    //not sure if we need to handle anything.
                }

                return lookupResult;
            }

        #endregion
         
    }
}
