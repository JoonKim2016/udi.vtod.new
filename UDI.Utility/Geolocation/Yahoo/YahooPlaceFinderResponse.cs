﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions; 
using System.Web.Script.Serialization;
using System.Web;
using System.Net;
using System.IO;

namespace UDI.Utility.Geolocation
{ 
    public class YahooPlaceFinderResponse 
    {
        public YahooApiResultSet ResultSet { get; set; }
    }
    public class YahooApiResultSet
    { 
        public int Error { get; set; }
        public int Found { get; set; }
        //  public ApiResponseStatus Status { get; set; }
        public IList<YahooApiResult> Results { get; set; }


    }
    public class YahooApiResult
    {
        public string line1 { get; set; }
        public string line2 { get; set; }
        public string city { get; set; }
        public string statecode { get; set; }
        public string state { get; set; }
        public string uzip { get; set; }
        public double longitude { get; set; }
        public double latitude { get; set; }

        //more fields availabe from api, but not added here since they are not needed.

        public string FullAddress
        {
            get
            {
                return line1 + ", " + city + ", " + statecode + " " + uzip;            
            }
        }
    }  
    
}
