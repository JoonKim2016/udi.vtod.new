﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions; 
using System.Web.Script.Serialization;
using System.Web;
using System.Net;
using System.IO;


namespace UDI.Utility.Geolocation
{  
    public class YahooPlaceFinderWrapper
    {
        private string _yahooApplicationId;
        private string _geoLocationUrlFormat = "http://where.yahooapis.com/geocode?location={0}&appid={1}&locale=en_US&flags=JS";

        //make sure to bind in Ninject
        public YahooPlaceFinderWrapper(string yahooApplicationId)
        {
            _yahooApplicationId = yahooApplicationId; 
        }

        #region public methods
            //Note: bounds does not restrict the google api query, but it does "favor" those results.
            //To filter out all addresses outside of the bounds, we'll need to filter them out on our side.
            public YahooPlaceFinderResponse Search(string address)
            {
                YahooPlaceFinderResponse yahooResponse;
                string strJsonResponse;
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                strJsonResponse = processApiRequest(address);
                yahooResponse = serializer.Deserialize<YahooPlaceFinderResponse>(strJsonResponse);

                return yahooResponse;
            }

        #endregion
         
        #region private methods
         
            private string processApiRequest(string address)
            {
                string apiRequest = string.Format(_geoLocationUrlFormat, HttpUtility.UrlEncode(address), _yahooApplicationId);
            
                //make request to google maps api
                var request = WebRequest.Create(apiRequest);
                var response = (HttpWebResponse)request.GetResponse();
            
                //read into stream
                var ms = new MemoryStream();
                var buffer = new Byte[2048];
                var responseStream = response.GetResponseStream();

                int count = responseStream.Read(buffer, 0, buffer.Length);

                while (count > 0)
                {
                    ms.Write(buffer, 0, count);
                    count = responseStream.Read(buffer, 0, buffer.Length);
                }

                responseStream.Close();
                ms.Close();

                var responseBytes = ms.ToArray();
                var encoding = new System.Text.ASCIIEncoding();

                return encoding.GetString(responseBytes);
            } 

        #endregion
    }
}
