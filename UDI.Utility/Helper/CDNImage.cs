﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace UDI.Utility.Helper
{
    public class CdnImage
    {
        public string ContainerName { get; set; }
        public string FileName { get; set; }
        public string StorageName { get; set; }
        public Image Image { get; set; }
        public string CdnKey { get; set; }
       
    }
}
