﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UDI.Utility.Helper
{
	public class CacheManager
	{
		static readonly Dictionary<string, Dictionary<string, object>> mainTable = new Dictionary<string, Dictionary<string, object>>();
		private static object categoryLocker = new object();
		private static object keyLocker = new object();

		public static void Add(string category, string key, object entity)
		{
			lock (categoryLocker)
			{
				if (!mainTable.ContainsKey(category))
				{
					mainTable.Add(category, new Dictionary<string, object>());
				}
			}

			var table = mainTable[category];

			lock (keyLocker)
			{
				if (!table.ContainsKey(key))
				{
					table.Add(key, entity);
				}
			}
		}

		public static void AddReplace(string category, string key, object entity)
		{
			lock (categoryLocker)
			{
				if (!mainTable.ContainsKey(category))
				{
					mainTable.Add(category, new Dictionary<string, object>());
				}
			}

			var table = mainTable[category];

			lock (keyLocker)
			{
				if (table.ContainsKey(key))
				{
					table.Remove(key);
				}
				table.Add(key, entity);
			}
		}
		
		public static object Get(string category, string key)
		{
			if (!mainTable.ContainsKey(category))
			{
				return null;
			}

			var table = mainTable[category];
			if (!table.ContainsKey(key))
			{
				return null;
			}

			return table[key];
		}

		public static void Remove(string category, string key)
		{
			if (mainTable.ContainsKey(category))
			{
				var table = mainTable[category];
				if (table.ContainsKey(key))
				{
					table.Remove(key);
				}
			}
		}

		public static bool Exist(string category, string key)
		{
			if (!mainTable.ContainsKey(category))
			{
				return false;
			}

			var table = mainTable[category];

			if (table.ContainsKey(key))
			{
				return true;
			}

			return false;
		}

		public static Dictionary<string, Dictionary<string, object>> GetAll()
		{
			return mainTable;
		}
	}
}
