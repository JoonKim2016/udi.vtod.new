﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UDI.Utility.Helper
{ 
	public static class ConsoleHelper
	{
		public static void WriteToConsole(this object input)
		{
			if (Environment.UserInteractive)
			{
				Console.WriteLine(input.ToString());
			}
		}
	}
}
