﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using Newtonsoft.Json;
using System.Drawing;
using System.IO;

namespace UDI.Utility.Helper
{
	public static class Converter
	{
		public static byte ToByte(this object input)
		{
			byte result = 0;
			try
			{
				Byte.TryParse(input.ToString(), out result);
			}
			catch
			{ }
			return result;
		}


		public static Int32 ToInt32(this object input)
		{
			var result = 0;
			try
			{
				Int32.TryParse(input.ToString(), out result);
			}
			catch
			{ }
			return result;
		}

		public static Guid ToGuid(this object input)
		{
			Guid result;
			//try
			//{
			//	result = new Guid(input.ToString());
			//}
			//catch
			//{
			//	result = new Guid();
			//}
			Guid.TryParse(input.ToString(), out result);

			return result;
		}

		public static decimal ToDecimal(this object input)
		{
			decimal result = 0;
			try
			{
				Decimal.TryParse(input.ToString(), out result);
			}
			catch
			{ }
			return result;
		}

		public static double ToDouble(this object input)
		{
			double result = 0;
			try
			{
				double.TryParse(input.ToString(), out result);
			}
			catch
			{ }
			return result;
		}

		public static DateTime? ToDateTime(this object input)
		{
			DateTime? result = null;
			try
			{
				result = Convert.ToDateTime(input.ToString());
			}
			catch
			{ }
			return result;
		}

		public static Int64 ToInt64(this object input)
		{
			Int64 result = 0;
			try
			{
				Int64.TryParse(input.ToString(), out result);
			}
			catch
			{ }
			return result;
		}

		public static bool ToBool(this object input)
		{
			var result = false;
			try
			{
				if (input.ToString() == "1")
					return true;

				bool.TryParse(input.ToString(), out result);
			}
			catch
			{ }
			return result;
		}        

        public static string CheckFormat(this string input, string format)
		{
			switch (format)
			{
				case "muwav":
					format = "wav";
					break;
				//case "mulaw":
				//    format = "pmc";
				//    break;
			}
			var result = input.EndsWith("." + format) ? input : input + "." + format;
			return result;
		}

		public static string CheckDirEnd(this string input)
		{
			var result = input.EndsWith("\\") ? input : input + "\\";
			return result;
		}

		public static DateTime ToUtc(this DateTime input, string timeZone)
		{
			TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById(timeZone);
			var result = TimeZoneInfo.ConvertTimeToUtc(input, cstZone);
			return result;
		}

		public static DateTime FromUtc(this DateTime input, string timeZone)
		{
			TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById(timeZone);
			var result = TimeZoneInfo.ConvertTimeFromUtc(input, cstZone);
			return result;
		}

		public static DateTime? FromUtcNullAble(this DateTime input, string timeZone)
		{
			DateTime? result = null;
			try
			{
				TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById(timeZone);

				result = TimeZoneInfo.ConvertTimeFromUtc(input, cstZone);
			}
			catch {
				result = null;
			}
			return result;
		}

		public static DateTime ToAnotherTimeZone(this DateTime input, string fromTimeZone, string toTimeZone)
		{
			//input.Kind = DateTimeKind.Local;
			TimeZoneInfo fromZone = TimeZoneInfo.FindSystemTimeZoneById(fromTimeZone);
			TimeZoneInfo toZone = TimeZoneInfo.FindSystemTimeZoneById(toTimeZone);
			var dateTimeToConvert = new DateTime(input.Ticks, DateTimeKind.Unspecified);
			var result = TimeZoneInfo.ConvertTime(dateTimeToConvert, fromZone, toZone);
			return result;
		}

		public static string GetCurrentTimeZone()
		{
			var result = TimeZone.CurrentTimeZone.StandardName;
			return result;
		}

		public static decimal ConvertCurrency(this decimal cost, DTO.Enum.Currency currency, DTO.Enum.Currency requiredCurrency)
		{
			//ToDo:: should be implemented
			return cost;
		}

		public static decimal ConvertDistance(this decimal distance, DTO.Enum.DistanceUnit unit, DTO.Enum.DistanceUnit requiredUnit)
		{
			//ToDo:: Better conversion code. This is just  for Mile to Kilometer and Kilometer/Meter to Mile
			if (unit != requiredUnit)
			{
				switch (unit)
				{
					case UDI.Utility.DTO.Enum.DistanceUnit.Mile:
						if (requiredUnit == DTO.Enum.DistanceUnit.Kilometer)
						{
							distance = distance.ConvertMilesToKilometers();
						}
						if (requiredUnit == DTO.Enum.DistanceUnit.Meter)
						{
							distance = distance * 1000;
							distance = distance.ConvertMilesToKilometers();
						}
						if (requiredUnit == DTO.Enum.DistanceUnit.Feet)
						{
							distance = distance * 5280;
						}
						break;
					case UDI.Utility.DTO.Enum.DistanceUnit.Kilometer:
						if (requiredUnit == DTO.Enum.DistanceUnit.Mile)
						{
							distance = distance.ConvertKilometersToMiles();
						}
						if (requiredUnit == DTO.Enum.DistanceUnit.Meter)
						{
							distance = distance * 1000;
						}
						if (requiredUnit == DTO.Enum.DistanceUnit.Feet)
						{
							distance = distance * 3280.84m;
						}
						break;
					case UDI.Utility.DTO.Enum.DistanceUnit.Meter:
						if (requiredUnit == DTO.Enum.DistanceUnit.Mile)
						{
							distance = distance / 1000;
							distance = distance.ConvertKilometersToMiles();
						}
						if (requiredUnit == DTO.Enum.DistanceUnit.Kilometer)
						{
							distance = distance / 1000;
						}
						if (requiredUnit == DTO.Enum.DistanceUnit.Feet)
						{
							distance = distance * 3.28084m;
						}
						break;
					case DTO.Enum.DistanceUnit.Feet:
						if (requiredUnit == DTO.Enum.DistanceUnit.Mile)
						{
							distance = distance / 5280;
							distance = distance.ConvertKilometersToMiles();
						}
						if (requiredUnit == DTO.Enum.DistanceUnit.Kilometer)
						{
							distance = distance / 3280.84m;
						}
						if (requiredUnit == DTO.Enum.DistanceUnit.Meter)
						{
							distance = distance / 3.28084m;
						}
						break;

				}
			}

			return distance;
		}

		public static decimal GetPercentageAmount(this string input)
		{
			decimal result = 0;
			input = input.Trim();
			try
			{
				if (input.EndsWith("%"))
				{
					var percent = input.Substring(0, input.Length - 1);
					result = percent.ToDecimal();
				}
				else if (input.StartsWith("%"))
				{
					var percent = input.Substring(1, input.Length - 1);
					result = percent.ToDecimal();
				}
			}
			catch
			{ }

			return result;
		}

		public static string ToUtf8From1252(this string input)
		{
			System.Text.Encoding utf_8 = System.Text.Encoding.UTF8;

			Encoding wind1252 = Encoding.GetEncoding(1252);
			Encoding utf8 = Encoding.UTF8;
			byte[] wind1252Bytes = wind1252.GetBytes(input);
			byte[] utf8Bytes = Encoding.Convert(wind1252, utf8, wind1252Bytes);
			string utf8String = Encoding.UTF8.GetString(utf8Bytes);

			return utf8String;
		}

		public static string ToBase64FromImage(this Image image, System.Drawing.Imaging.ImageFormat format)
		{
			using (MemoryStream ms = new MemoryStream())
			{
				// Convert Image to byte[]
				image.Save(ms, format);
				byte[] imageBytes = ms.ToArray();

				// Convert byte[] to Base64 String
				string base64String = Convert.ToBase64String(imageBytes);
				return base64String;
			}
		}

		public static Image ToImageFromBase64(this string base64String)
		{
			// Convert Base64 String to byte[]
			byte[] imageBytes = Convert.FromBase64String(base64String);
			MemoryStream ms = new MemoryStream(imageBytes, 0,
			  imageBytes.Length);

			// Convert byte[] to Image
			ms.Write(imageBytes, 0, imageBytes.Length);
			Image image = Image.FromStream(ms, true);
			return image;
		}

		public static byte[] ToByteArrayFromImage(this System.Drawing.Image image)
		{
			MemoryStream ms = new MemoryStream();
			image.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
			return ms.ToArray();
		}

		public static Image ToImageFromByteArray(this byte[] bytes)
		{
			MemoryStream ms = new MemoryStream(bytes);
			Image returnImage = Image.FromStream(ms);
			return returnImage;
		}

		#region Private
		private static decimal ConvertMilesToKilometers(this decimal miles)
		{
			return miles * 1.609344m;
		}

		private static decimal ConvertKilometersToMiles(this decimal kilometers)
		{
			return kilometers * 0.621371192m;
		}
		#endregion
	}
}
