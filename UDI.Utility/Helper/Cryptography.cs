﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.IO.Compression;
namespace UDI.Utility.Helper
{
	public static class Cryptography
	{
		#region MD5
		public static byte[] ToMD5(this string input)
		{
			// Convert the input string to a byte array and compute the hash. 
			MD5 md5 = new MD5CryptoServiceProvider();

			var result = md5.ComputeHash(Encoding.UTF8.GetBytes(input));

			return result;
		}

		public static byte[] ToMD5(this byte[] input)
		{
			// Convert the input string to a byte array and compute the hash. 
			MD5 md5 = new MD5CryptoServiceProvider();

			var result = md5.ComputeHash(input);

			return result;
		}

		public static string ToMD5String(this string input)
		{
			// Convert the input string to a byte array and compute the hash. 
			MD5 md5 = new MD5CryptoServiceProvider();

			var result = md5.ComputeHash(Encoding.UTF8.GetBytes(input));

			return ToHexString(result);
		}

		public static string ToMD5String(this byte[] input)
		{
			MD5 md5 = new MD5CryptoServiceProvider();

			var result = md5.ComputeHash(input);

			return ToHexString(result);
		}
		#endregion

		#region SHA1
		public static byte[] ToSHA1(this string input)
		{
			// Convert the input string to a byte array and compute the hash. 
			SHA1 sha1 = new SHA1CryptoServiceProvider();

			var result = sha1.ComputeHash(Encoding.UTF8.GetBytes(input));

			return result;
		}

		public static byte[] ToSHA1(this byte[] input)
		{
			// Convert the input string to a byte array and compute the hash. 
			SHA1 sha1 = new SHA1CryptoServiceProvider();

			var result = sha1.ComputeHash(input);

			return result;
		}

		public static string ToSHA1String(this string input)
		{
			// Convert the input string to a byte array and compute the hash. 
			SHA1 sha1 = new SHA1CryptoServiceProvider();

			var result = sha1.ComputeHash(Encoding.UTF8.GetBytes(input));

			return ToHexString(result);
		}

		public static string ToSHA1String(this byte[] input)
		{
			// Convert the input string to a byte array and compute the hash. 
			SHA1 sha1 = new SHA1CryptoServiceProvider();

			var result = sha1.ComputeHash(input);

			return ToHexString(result);
		}

		#endregion


		#region Encryption

        public static string EncryptDES(string original, string key, string iv)
        {
            try
            {
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                des.Key = Encoding.ASCII.GetBytes(key);
                des.IV = Encoding.ASCII.GetBytes(iv);
                byte[] s = Encoding.ASCII.GetBytes(original);
                ICryptoTransform desencrypt = des.CreateEncryptor();
                return BitConverter.ToString(desencrypt.TransformFinalBlock(s, 0, s.Length)).Replace("-", string.Empty);
            }
            catch (Exception ex) { return original; }
        }

       
       
		public static string DecryptDES(string hexString, string key, string iv)
		{
			try
			{
				DESCryptoServiceProvider des = new DESCryptoServiceProvider();
				des.Key = Encoding.ASCII.GetBytes(key);
				des.IV = Encoding.ASCII.GetBytes(iv);

				byte[] s = new byte[hexString.Length / 2];
				int j = 0;
				for (int i = 0; i < hexString.Length / 2; i++)
				{
					s[i] = Byte.Parse(hexString[j].ToString() + hexString[j + 1].ToString(), System.Globalization.NumberStyles.HexNumber);
					j += 2;
				}
				ICryptoTransform desencrypt = des.CreateDecryptor();
				return Encoding.ASCII.GetString(desencrypt.TransformFinalBlock(s, 0, s.Length));
			}
			catch { return hexString; }
		}
		#endregion

		public static string ToHexString(this byte[] input)
		{
			StringBuilder sBuilder = new StringBuilder("0x");
			for (int i = 0; i < input.Length; i++)
			{
				sBuilder.Append(input[i].ToString("x2"));
			}
			return sBuilder.ToString();
		}
        public static string FromHexString(string hexString)
        {
            var bytes = new byte[hexString.Length / 2];
            for (var i = 0; i < bytes.Length; i++)
            {
                bytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            }

            return Encoding.Unicode.GetString(bytes); // returns: "Hello world" for "48656C6C6F20776F726C64"
        }
        public static string HexString(string str)
        {
            var sb = new StringBuilder();

            var bytes = Encoding.Unicode.GetBytes(str);
            foreach (var t in bytes)
            {
                sb.Append(t.ToString("X2"));
            }

            return sb.ToString(); 
        }
        public static string ConvertStringToHex(string asciiString)
        {
            string hex = "";
            foreach (char c in asciiString)
            {
                int tmp = c;
                hex += String.Format("{0:x2}", (uint)System.Convert.ToUInt32(tmp.ToString()));
            }
            return hex;
        }

        public static string ConvertHexToString(string HexValue)
        {
            string StrValue = "";
            while (HexValue.Length > 0)
            {
                StrValue += System.Convert.ToChar(System.Convert.ToUInt32(HexValue.Substring(0, 2), 16)).ToString();
                HexValue = HexValue.Substring(2, HexValue.Length - 2);
            }
            return StrValue;
        }
	}
}
