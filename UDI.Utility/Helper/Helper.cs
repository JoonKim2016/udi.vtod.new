﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UDI.Utility.Helper
{
	public static class Helper
	{
		public static string CleanCsvContenct(this object input)
		{
			string result = string.Empty;
			if (input != null)
			{
				result = input.ToString().Trim().Replace(",", " ").Replace(System.Environment.NewLine, " ").Replace("\t", " ").Trim();
			}
			return result;
		}
	}
}
