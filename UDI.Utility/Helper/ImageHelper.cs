﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Drawing;
using log4net;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;

namespace UDI.Utility.Helper
{
    public class ImageHelper
    {
        private ILog logger;

        public ImageHelper(ILog logger)
        {


        }


        public bool IsImageUrl(string url)
        {
            var req = (HttpWebRequest)HttpWebRequest.Create(url);
            req.Method = "HEAD";
            try
            {
                using (var resp = req.GetResponse())
                {
                    return resp.ContentType.ToLower(CultureInfo.InvariantCulture)
                               .StartsWith("image/");
                }
            }
            catch (Exception)
            {

                return false;
            }
            
        }

        public Image ScaleImage(Image image, int maxWidth, int maxHeight)
        {
            #region get aspect ratio
            var ratioX = (double)maxWidth / image.Width;
            var ratioY = (double)maxHeight / image.Height;
            var ratio = System.Math.Min(ratioX, ratioY);
            #endregion

            #region make sure new height and width does not alter aspect ratio
            var newWidth = (int)(image.Width * ratio);
            var newHeight = (int)(image.Height * ratio);
            #endregion


            #region Return Bytes
            var newImage = new Bitmap(newWidth, newHeight);

            Graphics.FromImage(newImage).DrawImage(image, 0, 0, newWidth, newHeight);
            return (newImage);
            #endregion
        }

        public byte[] ImageToByteArray(System.Drawing.Image imageIn)
        {
            var ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
            return ms.ToArray();
        }

        public bool SavePng(string path, Bitmap img, long quality = 85L)
        {
            var noErrors = true;

            #region create encoding and set the codec
            var qualityParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);
            ImageCodecInfo codec = GetEncoderInfo("image/png");
            #endregion

            #region If we have a codec issue return false we could not add;

            if (codec == null)
                noErrors = false;
            #endregion

            #region Save

            try
            {
                var encoderParams = new EncoderParameters(1);
                encoderParams.Param[0] = qualityParam;
                img.Save(path, codec, encoderParams);
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Error Saving Image To CDN Image Name \"{0}\". Error Message \"{1}\". Inner Exception {2}.",
                path, ex.Message, ex.InnerException);
            }

            #endregion

            return noErrors;
        }

        public void SavePngToStream(Bitmap img, ref MemoryStream stream, long quality = 100L)
        {
            var noErrors = true;

            #region create encoding and set the codec
            var qualityParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);
            ImageCodecInfo codec = GetEncoderInfo("image/png");
            #endregion

            #region If we have a codec issue return false we could not add;

            if (codec == null)
            {
                noErrors = false;
            }
            #endregion
            else
            {
                #region Save

                var encoderParams = new EncoderParameters(1);
                encoderParams.Param[0] = qualityParam;
                img.Save(stream, codec, encoderParams);

                #endregion
            }

        }


        public void DownloadImageFromCdnToLocalFileSystem(string webFile, string outputFile)
        {
            using (var client = new WebClient())
            {
              client.DownloadFile(webFile, outputFile);
            }
        }

        public void DownloadImageFromCdnToMemory(string url, ref Stream stream)
        {

            WebRequest req = WebRequest.Create(url);
            WebResponse response = req.GetResponse();
            stream = response.GetResponseStream();
        }

        public void DownloadImageFromCdnToMemory(string url, ref Stream stream,string version)
        {
            url=url.Replace("{version}", version);
            WebRequest req = WebRequest.Create(url);
            WebResponse response = req.GetResponse();
            stream = response.GetResponseStream();
        }

        public void SavePngToCdn(CdnImage cdnImage)
        {
            var ic = new ImageConverter();

            var stream = new MemoryStream();

            SavePngToStream(new Bitmap(cdnImage.Image), ref stream);


            try
            {
                #region get an instance of the storage account
                var storageAccount = new CloudStorageAccount(new StorageCredentials (cdnImage.StorageName, cdnImage.CdnKey), true);
                #endregion

                #region Create the blob client.
                var blobClient = storageAccount.CreateCloudBlobClient();
                #endregion
                #region Retrieve a reference to a container.
                var container = blobClient.GetContainerReference(cdnImage.ContainerName);
                #endregion
                #region Create the container if it doesn't already exist.
                container.CreateIfNotExists();
                #endregion
                #region Retrieve reference to a blob
                var blockBlob = container.GetBlockBlobReference(cdnImage.FileName);
                blockBlob.Properties.ContentType = "image/png";

                #endregion

                #region upload to blob

                stream.Position = 0;

                using (var fileStream = stream)
                {
                    blockBlob.UploadFromStream(fileStream);
                    stream.Close();
                }


                #endregion


            }
            catch (Exception ex)
            {

                logger.ErrorFormat("Error Saving Image To CDN Image Name \"{0}\". Error Message \"{1}\". Inner Exception {2}.",
               cdnImage.FileName, ex.Message, ex.InnerException);

            }

        }


        private ImageCodecInfo GetEncoderInfo(string mimeType)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();
            for (int i = 0; i < codecs.Length; i++)
                if (codecs[i].MimeType == mimeType)
                    return codecs[i];
            return null;
        }
    }
}
