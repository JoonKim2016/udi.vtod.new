﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UDI.Utility.Helper
{
	public static class Math
	{
		public static decimal AddPercentage(this decimal input, int percentage)
		{
			return input + (input * percentage / 100);
		}

		public static int RoundUp(this decimal input, int interval)
		{
			//return (System.Math.Round(input / 10, MidpointRounding.AwayFromZero) * interval).ToInt32();
			int result = (int)System.Math.Ceiling(input / 10) * 10;
			return result;
		}
	}
}
