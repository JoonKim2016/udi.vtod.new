﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace UDI.Utility.Helper
{
	public static class PaymentCardHelper
	{
		public static string ToPaymentCardType(this string input)
		{
			var result = "Unknown";

			Match match = null;

			#region Visa Card
			match = Regex.Match(input, @"^4[0-9]{12}(?:[0-9]{3})?$");

			if (match.Success)
				return "Visa";
			#endregion

			#region MasterCard
			match = Regex.Match(input, @"^5[1-5][0-9]{14}$");

			if (match.Success)
				return "MasterCard";
			#endregion

			#region American Express
			match = Regex.Match(input, @"^3[47][0-9]{13}$");

			if (match.Success)
				return "AmericanExpress";
			#endregion

			#region Diners Club
			match = Regex.Match(input, @"^3(?:0[0-5]|[68][0-9])[0-9]{11}$");

			if (match.Success)
				return "DinersClub";
			#endregion

			#region Discover
			match = Regex.Match(input, @"^6(?:011|5[0-9]{2})[0-9]{12}$");

			if (match.Success)
				return "Discover";
			#endregion

			#region JCB
			match = Regex.Match(input, @"^(?:2131|1800|35\d{3})\d{11}$");

			if (match.Success)
				return "JCB";
			#endregion

			return result;
		}
	}
}
