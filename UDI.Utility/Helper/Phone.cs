﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UDI.Utility.Helper
{
	public static class Phone
	{
		public static string CleanPhone(this object phone)
		{
			var result = phone.ToString().Replace(" ", "").Replace("(", "").Replace(")", "").Replace("-", "").Replace("_", "").Replace(".", "").Replace("+", "");

			return result;
		}

		public static string MakeStandardPhoneForamt(this object phone)
		{
			var result = phone.CleanPhone().CleanPhone10Digit();
			try
			{
				if (result.Length == 10)
				{
					var s0 = result.Substring(0, 3);
					var s1 = result.Substring(3, 3);
					var s2 = result.Substring(6, 4);
					result = string.Format("({0}) {1}-{2}", s0, s1, s2);
				}
			}
			catch
			{ }

			return result;
		}

		public static string CleanCountryCode(this object phone)
		{
			var result = phone.ToString().Replace("00", "").Replace("+", "");

			return result;
		}

		public static string CleanPhone10Digit(this object phone)
		{
			var result = phone.ToString().Replace(" ", "").Replace("(", "").Replace(")", "").Replace("-", "").Replace("_", "").Replace(".", "");
			if (result.Length == 11)
			{
				result = result.Substring(1, 10);
			}
			return result;
		}

		public static bool IsNumeric(this object input)
		{
		    Int64 number;
			try
			{
				number = System.Convert.ToInt64(input.ToString());
				return true;
			}
			catch
			{
				//Console.WriteLine(ex);
				return false;
			}
		}

        public static Tuple<string, string, string> SplitPhone(this string input)
        {
            Tuple<string, string, string> splitNumber = null;
            string countryCode = "";
            string areaCode = "";
            string numberCode = "";
            string cleanNumber = "";

            try
            {
                //Clean the input string
                cleanNumber = CleanPhone(input);

                //If input string is Numeric
                if (cleanNumber.IsNumeric())
                {
                    //US phone number that contains country code, area code and phone number
                    if (cleanNumber.Length == 11)
                    {
                        countryCode = cleanNumber.Substring(0, 1);
                        areaCode = cleanNumber.Substring(1, 3);
                        numberCode = cleanNumber.Substring(4);

                        splitNumber = new Tuple<string, string, string>(countryCode,areaCode,numberCode);
                    }
                    //US phone number that contains area code and phone number
                    else if (cleanNumber.Length == 10)
                    {                       
                        areaCode = cleanNumber.Substring(0, 3);
                        numberCode = cleanNumber.Substring(3);

                        splitNumber = new Tuple<string, string, string>(countryCode, areaCode, numberCode);
                    }
                    else
                    {                       
                        numberCode = cleanNumber;

                        splitNumber = new Tuple<string, string, string>(countryCode, areaCode, numberCode);
                    }
                }

            }
            catch { }


            return splitNumber;
        }
        
        
    
	}
}
