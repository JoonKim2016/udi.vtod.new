﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using System.Xml;
using System.Net;
using UDI.Utility.Serialization;
using UDI.Utility.DTO;
namespace UDI.Utility.Helper
{
    public class TimeHelper : Contract.ITimeHelper
    {

        public DateTime? GetUTCFromLocalTime(double? latitude, double? longitude, DateTime localDateTime)
        {
            return GetUTCFromLocalTimeFromGoogle(latitude, longitude, localDateTime);
        }

        #region Private
        private DateTime? GetUTCFromLocalTimeFromGoogle(double? latitude, double? longitude, DateTime localDateTime)
        {
            DateTime? utcTime = localDateTime.ToUniversalTime();
            RestClient client;
            string location;
            RestRequest request;
            TimeSpan time_since_midnight_1970;
            double time_stamp;
            TimeDetailsInfo timeZoneInfo=null;            
            string google_api = System.Configuration.ConfigurationManager.AppSettings["GOOGLE_API"];
            string google_timezone = System.Configuration.ConfigurationManager.AppSettings["GOOGLE_TIMEZONE_REQUEST"];

            try
             {
                 client = new RestClient(google_api);
                 request = new RestRequest(google_timezone,
                                            Method.GET );
                location = String.Format ( "{0},{1}",
                                           latitude,
                                           longitude );
                time_since_midnight_1970 = localDateTime.ToUniversalTime().Subtract(
                            new DateTime ( 1970, 1, 1, 0, 0, 0 ) );
                time_stamp = time_since_midnight_1970.TotalSeconds;
 
                request.AddParameter ( "location", location );
                request.AddParameter ( "timestamp", time_stamp );
                request.AddParameter ( "sensor", "false" );

                RestResponse response    = (RestResponse) client.Execute(request);
                timeZoneInfo = response.Content.JsonDeserialize<TimeDetailsInfo>();
                if (response.StatusDescription.Equals("OK"))
                {
                    double rawOffSet = timeZoneInfo.rawOffset;
                    double dstOffset = timeZoneInfo.dstOffset;
                    double offset = rawOffSet + dstOffset;
                    if (offset != 0)
                    {
                        TimeSpan span = TimeSpan.FromSeconds(offset);
                        utcTime = localDateTime.Subtract(span);
                    }
                    else
                        utcTime = localDateTime;
                }
            }
            catch ( Exception ex )
            {
            
            }

            return (utcTime);
            }
        
        #endregion
    } 

}
