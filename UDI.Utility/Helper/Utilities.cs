﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;

namespace UDI.Utility.Helper
{
	public class Utilities
	{
		public static Dictionary<string, object> GetExtensionDataMemberValue(IExtensibleDataObject extensibleObject)
		{
			var result = new Dictionary<string, object>();
			PropertyInfo membersProperty = typeof(ExtensionDataObject).GetProperty("Members", BindingFlags.NonPublic | BindingFlags.Instance);
			IList members = (IList)membersProperty.GetValue(extensibleObject.ExtensionData, null);

			if (members != null)
			{
				foreach (object member in members)
				{
					var nameProperty = member.GetType().GetProperty("Name");
					string name = (string)nameProperty.GetValue(member, null);
					PropertyInfo valueProperty = member.GetType().GetProperty("Value");

					object value = valueProperty.GetValue(member, null);
					PropertyInfo innerValueProperty = value.GetType().GetProperty("Value");
					var innerValue = innerValueProperty.GetValue(value, null);

					result.Add(name, innerValue);
				}
			}
			return result;
		}

		public string GetIPAddressFromContext()
		{
			string ip = null;

			try
			{
				var props = OperationContext.Current.IncomingMessageProperties;
				var endpointProperty = props[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;
				if (endpointProperty != null)
				{
					ip = endpointProperty.Address;
				}
			}
			catch
			{ }

			return ip;
		}
               
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public static string GetLetterFromNumber(int number)
        {
            string letters = "";

            while (number > 0)
            {
                int currentLetterNumber = (number - 1) % 26;
                char currentLetter = (char)(currentLetterNumber + 65);
                letters = currentLetter + letters;
                number = (number - (currentLetterNumber + 1)) / 26;
            }
            return letters;
        }


        public static int GetNumberFromLetter(string letter)
        {
            int returnValue = 0;
            string letters = letter.ToUpper();
            for (int iChar = letters.Length - 1; iChar >= 0; iChar--)
            {
                char currentLetter = letters[iChar];
                int currentLetterNumber = currentLetter - 64;
                returnValue = returnValue + currentLetterNumber * (int)System.Math.Pow(26, letters.Length - (iChar + 1));
            }
            return returnValue;
        }
    }
}
