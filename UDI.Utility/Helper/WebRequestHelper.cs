﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading;

namespace UDI.Utility.Helper
{
	public static class WebRequestHelper
	{
		static int defaultTimeOut = 100000;

		public static string ProcessWebRequest(string request)
		{
			var result = string.Empty;

			try
			{
				//make request to google maps api
				var webRequest = WebRequest.Create(request);
				var webResponse = (HttpWebResponse)webRequest.GetResponse();

				//read into stream
				var ms = new MemoryStream();
				var buffer = new Byte[2048];
				var responseStream = webResponse.GetResponseStream();

				int count = responseStream.Read(buffer, 0, buffer.Length);

				while (count > 0)
				{
					ms.Write(buffer, 0, count);
					count = responseStream.Read(buffer, 0, buffer.Length);
				}

				responseStream.Close();
				ms.Close();

				var responseBytes = ms.ToArray();
				var encoding = new System.Text.ASCIIEncoding();

				result = encoding.GetString(responseBytes);
			}
			catch (WebException ex)
			{
				using (var reader = new StreamReader(ex.Response.GetResponseStream()))
				{
					result = reader.ReadToEnd();
				}
				throw new Exception(result);
			}

			return result;
		}

		public static string ProcessWebRequest(string url, string content, string contentType, string method, string userName, string password, int timeout, Dictionary<string, string> headers, out string status)
		{
			Dictionary<string, string> responseHeader = null;
			return ProcessWebRequest(url, content, contentType, method, userName, password, timeout, headers, out  status, out responseHeader);
		}

        public static string ProcessWebRequest(string url, string content, string contentType, string method,int timeout, Dictionary<string, string> headers, out string status, out Dictionary<string, string> responseHeader)
        {
            responseHeader = null;
            return ProcessWebRequest(url, content, contentType, method, string.Empty, string.Empty, timeout, headers, out  status, out responseHeader);
        }

		//Main method
		public static string ProcessWebRequest(string url, string content, string contentType, string method, string userName, string password, int timeout, Dictionary<string, string> headers, out string status, out Dictionary<string, string> responseHeader)
		{
			var result = string.Empty;
			status = string.Empty;
			responseHeader = new Dictionary<string, string>();

			try
			{
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

				HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
				if (headers != null && headers.Any())
				{
					foreach (var item in headers)
					{
                        //If the Header type specified is one of the existing 'restricted headers', just assign the new value. 
                        //Otherwise add the new header and value.
                        switch (item.Key.ToLower())
                        {
                            case "accept":
                                request.Accept = item.Value;
                                break;
                            case "connection":
                                request.Connection = item.Value;
                                break;
                            case "content-length":
                                request.ContentLength = Convert.ToInt64(item.Value);
                                break;
                            case "content-type":
                                request.ContentType = item.Value;
                                break;
                            case "date":
                                request.Date = Convert.ToDateTime(item.Value);
                                break;
                            case "expect":
                                request.Expect = item.Value;
                                break;
                            case "host":
                                request.Host = item.Value;
                                break;
                            case "if-modified-since":
                                request.IfModifiedSince = Convert.ToDateTime(item.Value);
                                break;                            
                            case "referer":
                                request.Referer = item.Value;
                                break;
                            case "transfer-encoding":
                                request.TransferEncoding = item.Value;
                                break;
                            case "user-agent":
                                request.UserAgent = item.Value;
                                break;                           
                            default:
                                request.Headers.Add(item.Key + ":" + item.Value);
                                break;
                        }
					}
				}

				if (!string.IsNullOrWhiteSpace(userName))
				{
					NetworkCredential networkCredential = new NetworkCredential(userName, password);
					request.Credentials = networkCredential;
				}
				//NetworkCredential networkCredential = new NetworkCredential("DenverYellowCab", "denveryellowcab@12345");
				//request.Credentials = networkCredential;
                request.ProtocolVersion = HttpVersion.Version11;
				request.Method = method;
				request.Timeout = timeout;
                if(request.Accept == null) request.Accept = "*/*";
                request.UserAgent = "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; Trident/6.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; InfoPath.3; .NET4.0C; .NET4.0E)";


				Stream dataStream;
				if (!string.IsNullOrWhiteSpace(content))
				{
					byte[] byteArray = Encoding.UTF8.GetBytes(content);
					//byte[] byteArray = Encoding.ASCII.GetBytes(content);
					request.ContentType = contentType;
					request.ContentLength = byteArray.Length;
					dataStream = request.GetRequestStream();
					dataStream.Write(byteArray, 0, byteArray.Length);
					dataStream.Close();
				}
				else
				{
					//byte[] byteArray = Encoding.UTF8.GetBytes(content);
					//byte[] byteArray = Encoding.ASCII.GetBytes(content);
					request.ContentType = contentType;
					request.ContentLength = 0;
					//dataStream = request.GetRequestStream();
					//dataStream.Write(byteArray, 0, byteArray.Length);
					//dataStream.Close();
				}

				WebResponse response = request.GetResponse();

				if (response.Headers != null && response.Headers.Count > 0)
				{
					for (int i = 0; i < response.Headers.Count; ++i)
					{
						responseHeader.Add(response.Headers.Keys[i], response.Headers[i]);
					}
				}



				status = ((HttpWebResponse)response).StatusDescription;

				dataStream = response.GetResponseStream();
				StreamReader reader = new StreamReader(dataStream);
				result = reader.ReadToEnd();

				reader.Close();
				dataStream.Close();
				response.Close();

			}
			catch (WebException ex)
			{
				using (var reader = new StreamReader(ex.Response.GetResponseStream()))
				{
					result = reader.ReadToEnd();
				}
				throw new Exception(result);
			}

			return result;

		}

		public static string ProcessWebRequest(string url, string content, string contentType, string method, string userName, string password, int timeout, out string status)
		{
			return ProcessWebRequest(url, content, contentType, method, userName, password, timeout, null, out status);
		}

		public static string ProcessWebRequest(string url, string content, string contentType, string method, string userName, string password, out string status)
		{
			return ProcessWebRequest(url, content, contentType, method, userName, password, defaultTimeOut, out status);
		}

		public static string ProcessWebRequest(string url, string content, string contentType, string method, out string status)
		{
			return ProcessWebRequest(url, content, contentType, method, "", "", out status);
		}

		public static string ProcessWebRequest(string url, string content, string contentType, string method, Dictionary<string, string> headers, out string status)
		{
			return ProcessWebRequest(url, content, contentType, method, "", "", defaultTimeOut, headers, out status);
		}

		public static T ProcessJsonWebRequest<T>(string requestUrl)
		{
			T result;
			WebRequest request = WebRequest.Create(requestUrl);
			request.Method = "GET";
			request.ContentType = "application/json";
			using (var response = (HttpWebResponse)request.GetResponse())
			{
				var status = response.StatusCode;
				using (var responseStream = response.GetResponseStream())
				{
					DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(T));
					result = (T)jsonSerializer.ReadObject(responseStream);
				}
			}

			return result;
		}

		public static string ReadHtmlFromUrl(this string url)
		{
			var result = string.Empty;
			using (WebClient client = new WebClient())
			{
				result = client.DownloadString(url);
			}
			return result;
		}
	}
}
