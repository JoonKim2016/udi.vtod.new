﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace UDI.Utility.Helper
{
	public static class XmlHelper
	{
		public static XmlDocument ToXmlDocument(this XDocument xDocument)
		{
			var xmlDocument = new XmlDocument();
			using (var xmlReader = xDocument.CreateReader())
			{
				xmlDocument.Load(xmlReader);
			}
			return xmlDocument;
		}

		public static XDocument ToXDocument(this XmlDocument xmlDocument)
		{
			using (var nodeReader = new XmlNodeReader(xmlDocument))
			{
				nodeReader.MoveToContent();
				return XDocument.Load(nodeReader);
			}
		}

		public static XDocument CleanComment(this XDocument input)
		{
			input.Elements().DescendantNodesAndSelf().Where(s => s.NodeType == System.Xml.XmlNodeType.Comment).Remove();
			return input;
		}

		public static XDocument CleanNamespace(this XDocument input)
		{
			input.Elements().DescendantNodesAndSelf().Where(s => s.NodeType == System.Xml.XmlNodeType.Comment).Remove();

			foreach (XElement e in input.Root.DescendantsAndSelf())
			{
				if (e.Name.Namespace != XNamespace.None)
				{
					e.Name = XNamespace.None.GetName(e.Name.LocalName);
				}

				if (e.Attributes().Where(
						a => a.IsNamespaceDeclaration || a.Name.Namespace != XNamespace.None).Any())
				{
					e.ReplaceAttributes(e.Attributes().Select(
							a => a.IsNamespaceDeclaration ? null : a.Name.Namespace
								!= XNamespace.None ? new XAttribute(
									XNamespace.None.GetName(a.Name.LocalName), a.Value) : a));
				}
			}

			return input;
		}

		public static void SetDefaultXmlNamespace(this XElement input, XNamespace xmlns)
		{
			if (input.Name.NamespaceName == string.Empty)
				input.Name = xmlns + input.Name.LocalName;
			foreach (var e in input.Elements())
				e.SetDefaultXmlNamespace(xmlns);
		}

		public static string HtmlEncode(this string input)
		{
			try
			{
				var result = System.Web.HttpUtility.HtmlEncode(input);
				return result;
			}
			catch
			{
				return input;
			}
		}

		public static string HtmlDecode(this string input)
		{
			try
			{
				var result = System.Web.HttpUtility.HtmlDecode(input);
				return result;
			}
			catch
			{
				return input;
			}
		}
	}
}
