﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;

namespace UDI.Utility.Serialization
{
	public /*static*/ class Compressor
	{
		//public static string Compress(this string input)
		//{
		//	var bytes = Encoding.Unicode.GetBytes(input);
		//	using (var msi = new MemoryStream(bytes))
		//	using (var mso = new MemoryStream())
		//	{
		//		using (var gs = new GZipStream(mso, CompressionMode.Compress))
		//		{
		//			msi.CopyTo(gs);
		//		}
		//		return Convert.ToBase64String(mso.ToArray());
		//	}
		//}

		//public static string Decompress(this string input)
		//{
		//	var bytes = Convert.FromBase64String(input);
		//	using (var msi = new MemoryStream(bytes))
		//	using (var mso = new MemoryStream())
		//	{
		//		using (var gs = new GZipStream(msi, CompressionMode.Decompress))
		//		{
		//			gs.CopyTo(mso);
		//		}
		//		return Encoding.Unicode.GetString(mso.ToArray());
		//	}
		//}
		public string Compress(string input)
		{
			string result = "";

			using (MemoryStream output = new MemoryStream())
			{
				using (DeflateStream gzip = new DeflateStream(output, CompressionMode.Compress))
				{
					using (StreamWriter writer = new StreamWriter(gzip, System.Text.Encoding.UTF8))
					{
						writer.Write(input);
					}
				}

				result = Convert.ToBase64String(output.ToArray());
			}

			return result;
		}

		public string Decompress(string input)
		{
			string result = "";
			byte[] byteArrary = Convert.FromBase64String(input);


			using (MemoryStream inputStream = new MemoryStream(byteArrary))
			{
				using (DeflateStream gzip =
				  new DeflateStream(inputStream, CompressionMode.Decompress))
				{
					using (StreamReader reader =
					  new StreamReader(gzip, System.Text.Encoding.UTF8))
					{
						result = reader.ReadToEnd();
					}
				}
			}

			return result;
		}


	}
}
