﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.IO;
using System.Web.Script.Serialization;
using System.Runtime.Serialization.Json;
using Newtonsoft.Json;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using UDI.Utility.Helper;
using System.Drawing;

namespace UDI.Utility.Serialization
{
	public static class Serializer
	{
		//New
		#region Xml
		public static T XmlDeserialize<T>(this XDocument input, out string error)
		{
			error = string.Empty;
			var result = default(T);
			try
			{
				#region Get the element
				//var content = document.Descendants();
				//var resultElement = new XElement(typeof(T).Name, content); 
				#endregion

				#region Create the Serializer
				var serializer = new XmlSerializer(typeof(T));
				#endregion

				#region Do the serializing
				result = (T)serializer.Deserialize(input.CreateReader());
				#endregion
			}
			catch (Exception ex)
			{
				error += ex.Message + "\r\n";
			}

			return result;
		}

		public static T XmlDeserialize<T>(this XDocument input)
		{
			var error = string.Empty;
			return input.XmlDeserialize<T>(out error);
		}

		public static T XmlDataContractDeserialize<T>(this XDocument input)
		{
			DataContractSerializer ser = new DataContractSerializer(typeof(T));

			var result = (T)ser.ReadObject(input.CreateReader());

			return result;
		}

		public static XDocument XmlSerialize<T>(this T input, out string error)
		{
			var result = new XDocument();
			error = string.Empty;

			try
			{
				using (TextWriter stringWriter = new StringWriter())
				{
					System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(input.GetType());
					serializer.Serialize(stringWriter, input);
					result = XDocument.Parse(stringWriter.ToString());
				}
			}
			catch (Exception ex)
			{
				error += ex.Message + "\r\n";
			}

			return result;
		}

		public static XDocument XmlSerialize<T>(this T input)
		{
			var error = string.Empty;
			var result = input.XmlSerialize(out error);
			return result;
		}

		#endregion

		#region Json
		public static T JsonDeserialize<T>(this string input, out string error)
		{
			var result = default(T);
			error = string.Empty;

			#region JavaScriptSerializer
			/*
			JavaScriptSerializer serializer = new JavaScriptSerializer();

			try
			{
				result = serializer.Deserialize<T>(input);
			}
			catch (Exception ex)
			{
				error += ex.Message + "\r\n";
			}
			*/

			#endregion

			#region DataContractJsonSerializer
			/*
			try
			{
				DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
				using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(input)))
				{
					result = (T)ser.ReadObject(ms);
				}
			}
			catch (Exception ex)
			{
				error += ex.Message + "\r\n";
			}
			 */
			#endregion

			#region Json.net
			try
			{
				result = JsonConvert.DeserializeObject<T>(input, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
			}
			catch (Exception ex)
			{
				error += ex.Message + "\r\n";
			}
			#endregion

			return result;
		}

		public static string JsonSerialize<T>(this T input, out string error)
		{
			var result = string.Empty;
			error = string.Empty;

			#region with JavaScriptSerializer
			/*
			JavaScriptSerializer serializer = new JavaScriptSerializer();

			try
			{
				result = serializer.Serialize(input);
			}
			catch (Exception ex)
			{
				error += ex.Message + "\r\n";
			}
			*/

			#endregion

			#region DataContractJsonSerializer
			/*
			try
			{
				DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
				using (MemoryStream ms = new MemoryStream())
				{
					ser.WriteObject(ms, input);
					result = Encoding.UTF8.GetString(ms.ToArray());
				}
			}
			catch (Exception ex)
			{
				error += ex.Message + "\r\n";
			}
			 */

			#endregion

			#region Json.net
			try
			{
				result = JsonConvert.SerializeObject(input, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
			}
			catch (Exception ex)
			{
				error += ex.Message + "\r\n";
			}
			#endregion

			return result;
		}

		public static T JsonDeserialize<T>(this string input)
		{
			var error = string.Empty;
			return input.JsonDeserialize<T>(out error);
		}

		public static string JsonSerialize<T>(this T input)
		{
			var error = string.Empty;
			return input.JsonSerialize(out error);
		}

		public static string JsonSerializeWithDataContract<T>(this T input)
		{
			var result = string.Empty;

			DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
			using (MemoryStream ms = new MemoryStream())
			{
				ser.WriteObject(ms, input);
				result = Encoding.UTF8.GetString(ms.ToArray());
			}

			return result;
		}

		public static T JsonDeserializeWithDataContract<T>(this string input)
		{
			var result = default(T);

			DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
			using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(input)))
			{
				result = (T)ser.ReadObject(ms);
			}

			return result;
		}
		#endregion

		#region Base64
		public static string Base64Serialize<T>(this T input, out string error)
		{
			var result = string.Empty;
			error = string.Empty;

			try
			{
				if (input != null)
				{
					MemoryStream ms = new MemoryStream();
					new BinaryFormatter().Serialize(ms, (T)input);
					result = Convert.ToBase64String(ms.ToArray());
				}
			}
			catch (Exception ex)
			{
				error += ex.Message + "\r\n";
			}

			return result;
		}

		public static string Base64Serialize<T>(this T input)
		{
			var error = string.Empty;
			var result = input.Base64Serialize(out error);
			return result;
		}

		public static T Base64Deserialize<T>(this string input, out string error)
		{
			error = string.Empty;
			T result = default(T);

			try
			{
				byte[] bytes = Convert.FromBase64String(input);
				MemoryStream ms = new MemoryStream(bytes, 0, bytes.Length);
				ms.Write(bytes, 0, bytes.Length);
				ms.Position = 0;
				result = (T)new BinaryFormatter().Deserialize(ms);
			}
			catch (Exception ex)
			{
				error += ex.Message + "\r\n";
			}

			return result;
		}

		public static T Base64Deserialize<T>(this string input)
		{
			var error = string.Empty;
			return input.Base64Deserialize<T>(out error);
		}
		#endregion
	}

}
