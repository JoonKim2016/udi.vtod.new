﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;

namespace UDI.Utility.ServiceModel
{
	public class ErrorHandler : BehaviorExtensionElement, IErrorHandler, IServiceBehavior
	{
		public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters)
		{
		}

		public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
		{
			IErrorHandler errorHandler = new ErrorHandler();

			foreach (ChannelDispatcher channelDispatcher in serviceHostBase.ChannelDispatchers)
			{
				//ChannelDispatcher channelDispatcher = channelDispatcherBase as ChannelDispatcher;

				//if (channelDispatcher != null)
				//{
				channelDispatcher.ErrorHandlers.Add(errorHandler);
				//}
			}
		}

		public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
		{
		}

		public bool HandleError(Exception error)
		{
			Trace.TraceError(error.ToString());

			// Returning true indicates you performed your behavior.
			return true;
		}

		public void ProvideFault(Exception error, MessageVersion version, ref Message message)
		{
			var format = WebContentFormat.Xml;

			if (error is FaultException)
			{
				var detail = error.GetType().GetProperty("Detail").GetGetMethod().Invoke(error, null);
				format = ((DTO.Error)(detail)).Format;
				if (format == WebContentFormat.Default)
				{
					format = WebContentFormat.Xml;
				}
				message = Message.CreateMessage(version, "", detail, new DataContractJsonSerializer(detail.GetType()));
			}
			else
			{
				message = Message.CreateMessage(version, "", error.Message, new DataContractJsonSerializer(typeof(string)));
			}

			var wbf = new WebBodyFormatMessageProperty(format);
			message.Properties.Add(WebBodyFormatMessageProperty.Name, wbf);

			var rmp = new HttpResponseMessageProperty();
			rmp.StatusCode = System.Net.HttpStatusCode.BadRequest;
			rmp.StatusDescription = "See fault object for more information.";
			message.Properties.Add(HttpResponseMessageProperty.Name, rmp);
		}

		public override Type BehaviorType
		{
			get { return this.GetType(); }
		}

		protected override object CreateBehavior()
		{
			return this;
		}
	}
}
