﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Configuration;
using System.Text;

namespace UDI.Utility.ServiceModel
{
	public class ErrorHandlerElement : BehaviorExtensionElement
	{
		protected override object CreateBehavior()
		{
			return new ErrorHandler();
		}

		public override Type BehaviorType
		{
			get
			{
				return typeof(ErrorHandler);
			}
		}
	}
}
