﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;

namespace UDI.Utility.ServiceModel
{
	public class RestWebHttpBehavior : WebHttpBehavior
	{
		protected override void AddServerErrorHandlers(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
		{
			// clear default erro handlers.
			endpointDispatcher.ChannelDispatcher.ErrorHandlers.Clear();
			// add our own error handler.
			endpointDispatcher.ChannelDispatcher.ErrorHandlers.Add(new ErrorHandler());
		}
	}
}
