﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml;
using System.IO;

namespace UDI.Utility.Validation
{
	public static class XmlValidator
	{
		public static bool SchemaValidate(this XDocument input, string schemaUri, out string error)
		{
			return SchemaValidate(input, schemaUri, "", out error);
		}

		public static bool SchemaValidate(this XDocument input, string schemaUri, string targetNamespace, out string error)
		{
			var result = true;
			var _error = string.Empty;
			XmlSchemaSet schemas = new XmlSchemaSet();
			schemas.Add(targetNamespace, schemaUri);

			input.Validate(schemas, (sender, args) =>
			{
				_error += args.Message + "\r\n";
				result = false;
			});
			error = _error;
			return result;
		}
	}
}
