﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using UDI.VTOD.Domain.Taxi.CCSi;
using UDI.VTOD.Domain.Taxi;
using System.Web.Services;
using UDI.VTOD.Common.DTO.OTA;
using System.Configuration;
using UDI.VTOD.Domain.Van.CCSi;
using UDI.VTOD.Domain.Van;
using UDI.Utility.Helper;
using log4net;

namespace UDI.VTOD.CCSiWeb
{
	/// <summary>
	/// Summary description for Status
	/// </summary>
	public class Status : IHttpHandler
	{
		#region Fields
		static ILog log;

		#endregion

		#region Constructors
		public Status()
		{
			log4net.Config.XmlConfigurator.Configure();
			log = LogManager.GetLogger(typeof(Status));
		}
		#endregion

		public void ProcessRequest(HttpContext context)
		{
			try
			{

				#region Debug
				log.InfoFormat("CCSI Handlers QueryStrings:{0}", context.Request.QueryString.ToString());
				#endregion


				if (context.Request.QueryString.Count > 0)
				{

					#region 01_Process Query
					System.Collections.Specialized.NameValueCollection request = context.Request.QueryString;
					if (!string.IsNullOrWhiteSpace(request[UDI.VTOD.Domain.Taxi.Const.CCSi_StatusConstant.Vehicle_Type]))
					{
						if (request[UDI.VTOD.Domain.Taxi.Const.CCSi_StatusConstant.Vehicle_Type].ToUpper() == "VAN")
						{
							VanUtility utility = new VanUtility(log);
							CCSiVanService service = new CCSiVanService(utility, log);

							log.Info("Prepare to get a token");
							TokenRS token = GetToken(utility, service);
							service.ProcessPushedStatus(token, request);
						}
						else
						{
							TaxiUtility utility = new TaxiUtility(log);
							CCSiTaxiService service = new CCSiTaxiService(utility, log);

							log.Info("Prepare to get a token");
							TokenRS token = GetToken(utility, service);
							service.ProcessPushedStatus(token, request);
						}
						context.Response.ContentType = "text/plain";
						context.Response.Write("Event was inserted");
					}
					else
					{
						TaxiUtility utility = new TaxiUtility(log);
						CCSiTaxiService service = new CCSiTaxiService(utility, log);

						log.Info("Prepare to get a token");
						TokenRS token = GetToken(utility, service);
						service.ProcessPushedStatus(token, request);
					}
					context.Response.ContentType = "text/plain";
					context.Response.Write("Event was inserted");
					#endregion

					#region 02_Push to Stage for testing
					try
					{
						if (System.Configuration.ConfigurationManager.AppSettings["RePush"].ToBool())
						{
							var endpoint = System.Configuration.ConfigurationManager.AppSettings["RePushURL"] + "?" + context.Request.QueryString.ToString();
							string status;
							UDI.Utility.Helper.WebRequestHelper.ProcessWebRequest(endpoint, null, null, "GET", out status);
						}
					}
					catch (Exception ex)
					{
						log.Error(ex);
					}
					#endregion

				}
				else
				{
					context.Response.ContentType = "text/plain";
					context.Response.Write("No query string found");
				}

			}
			catch (Exception ex)
			{
				log.Error("pushing web service: ", ex);

				context.Response.ContentType = "text/plain";
				context.Response.Write(string.Format("Error: {0}", ex.Message));
			}

		}

		private TokenRS GetToken(TaxiUtility utility, CCSiTaxiService service)
		{
			TokenRS token = null;
			if (HttpContext.Current.Application["VTODToken"] != null && ((TokenRS)(HttpContext.Current.Application["VTODToken"])).CreateDateTimeUTC != null)
			{
				log.Info("Get the VTODToken from application object");
				DateTime tokenTime = ((TokenRS)(HttpContext.Current.Application["VTODToken"])).CreateDateTimeUTC;
				long diff = new TimeSpan(DateTime.UtcNow.Ticks - tokenTime.Ticks).Minutes;
				int expiration = Convert.ToInt32(ConfigurationManager.AppSettings["SDS_Token_Expiration"]);
				if (diff > expiration)
				{
					//get new token
					log.Info("Get a new SDS token");

					token = service.GetSDSToken();
					HttpContext.Current.Application["VTODToken"] = token;
				}
				else
				{
					//reuse token
					log.Info("Reuse the previous token");
					token = (TokenRS)(HttpContext.Current.Application["VTODToken"]);
				}

			}
			else {
				log.Info("Unable to get the VTODToken from application object");
				token = service.GetSDSToken();
				HttpContext.Current.Application["VTODToken"] = token;
			}
			return token;
		}

		private TokenRS GetToken(VanUtility utility, CCSiVanService service)
		{
			TokenRS token = null;
			if (HttpContext.Current.Application["VTODToken"] != null && ((TokenRS)(HttpContext.Current.Application["VTODToken"])).CreateDateTimeUTC != null)
			{
				log.Info("Get the VTODToken from application object");
				DateTime tokenTime = ((TokenRS)(HttpContext.Current.Application["VTODToken"])).CreateDateTimeUTC;
				long diff = new TimeSpan(DateTime.UtcNow.Ticks - tokenTime.Ticks).Minutes;
				int expiration = Convert.ToInt32(ConfigurationManager.AppSettings["SDS_Token_Expiration"]);
				if (diff > expiration)
				{
					//get new token
					log.Info("Get a new SDS token");

					token = service.GetSDSToken();
					HttpContext.Current.Application["VTODToken"] = token;
				}
				else
				{
					//reuse token
					log.Info("Reuse the previous token");
					token = (TokenRS)(HttpContext.Current.Application["VTODToken"]);
				}

			}
			else
			{
				log.Info("Unable to get the VTODToken from application object");
				token = service.GetSDSToken();
				HttpContext.Current.Application["VTODToken"] = token;
			}
			return token;
		}

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}



	}
}