﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using log4net;
using UDI.Utility.Helper;
using UDI.VTOD.Common.DTO.Enum;

namespace UDI.VTOD.Common.Abstract
{
	public abstract class BaseSetting
	{
		#region Fields
		//public static ILog logger;
		[XmlIgnore]
		public ILog logger;

		[XmlIgnore]
		public bool isTest = false;

		[XmlIgnore]
		public bool isDebug = false;

		[XmlIgnore]
		public bool isTrack = false;

		[XmlIgnore]
		public bool isLogInDB = false;

		[XmlIgnore]
		public bool isDetailDebug = false;

		[XmlIgnore]
		private LogOwnerType ownerType = LogOwnerType.VTOD;
		#endregion

		#region Constructors
		public BaseSetting()
		{
			#region Log configuration
			if (this.GetType().ToString().ToLower().Contains("sds"))
			{
				logger = LogManager.GetLogger(LogOwnerType.SDS.ToString());
			}
			else if (this.GetType().ToString().ToLower().Contains("taxi"))
			{
				logger = LogManager.GetLogger(LogOwnerType.Taxi.ToString());
			}
			else
			{
				logger = LogManager.GetLogger(LogOwnerType.VTOD.ToString());
			}
			log4net.Config.XmlConfigurator.Configure();
			#endregion

			#region IsTest
			try
			{
				isTest = System.Configuration.ConfigurationManager.AppSettings["IsTest"].ToBool();
			}
			catch { }
			#endregion

			#region IsTrack
			try
			{
				isTrack = System.Configuration.ConfigurationManager.AppSettings["IsTrack"].ToBool();
			}
			catch { }
			#endregion

			#region IsDebug
			try
			{
				isDebug = System.Configuration.ConfigurationManager.AppSettings["IsDebug"].ToBool();
			}
			catch { }
			#endregion

			#region IsDetailDebug
			try
			{
				isDetailDebug = System.Configuration.ConfigurationManager.AppSettings["IsDetailDebug"].ToBool();
			}
			catch { }
			#endregion

			#region IsLogInDB
			try
			{
				isLogInDB = System.Configuration.ConfigurationManager.AppSettings["IsLogInDB"].ToBool();
			}
			catch { }
			#endregion
		}

		public BaseSetting(LogOwnerType logOwnerType)
		{
			#region Log configuration
			logger = LogManager.GetLogger(logOwnerType.ToString());
			log4net.Config.XmlConfigurator.Configure();
			#endregion

			#region IsTest
			try
			{
				isTest = System.Configuration.ConfigurationManager.AppSettings["IsTest"].ToBool();
			}
			catch { }
			#endregion

			#region IsDebug
			try
			{
				isDebug = System.Configuration.ConfigurationManager.AppSettings["IsDebug"].ToBool();
			}
			catch { }
			#endregion

			#region IsLogInDB
			try
			{
				isLogInDB = System.Configuration.ConfigurationManager.AppSettings["IsLogInDB"].ToBool();
			}
			catch { }
			#endregion
		}
		#endregion

		#region Properties
		[XmlIgnore]
		public LogOwnerType OwnerType
		{
			get
			{
				return ownerType;
			}
			set
			{
				ownerType = value;
			}
		}
		#endregion
	}
}
