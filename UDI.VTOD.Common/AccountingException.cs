﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO
{
	public class AccountingException : Exception
	{
        public AccountingException(string message)
			: base(message)
		{
		}
	}
}
