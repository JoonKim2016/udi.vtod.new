﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.Const
{
    public class DriveInfoSource
    {
        public const int MTData = 3;
        public const int TCS = 2;
        public const int Local = 1;
    }
}
