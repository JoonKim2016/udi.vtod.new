﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.Const
{
   public class PhoneVerificationUrl
    {
       
       public const string VerifyPhoneURL = "type/phoneNumber/memberID/VerifyType/";
       public const string IsVerifiedURL = "IsVerified/phoneNumber/memberID";
       public const string RegistrationVerifyPhoneURL = "IsPhoneTaken/phoneNumber";
       public const string VerifyVersionURL = "App/Platform/AppVersion/PlatformVersion/";
    }
}

