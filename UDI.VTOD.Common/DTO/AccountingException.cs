﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO
{
    /// <summary>
    /// This is an object to handle the exceptions of the accounting module.
    /// </summary>
	public class AccountingException : Exception
	{
        /// <summary>
        /// This is the Constructor of the account module.
        /// </summary>
        /// <param name="message"></param>
        public AccountingException(string message)
			: base(message)
		{
		}
	}
}
