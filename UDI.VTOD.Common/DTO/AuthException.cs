﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO
{
    /// <summary>
    /// This is an object to handle the exceptions of the Authorization module.
    /// </summary>
	public class AuthException : Exception
	{
        /// <summary>
        /// This is the Constructor of the Authorization module.
        /// </summary>
        /// <param name="message"></param>
		public AuthException(string message)
			: base(message)
		{
		}
	}
}
