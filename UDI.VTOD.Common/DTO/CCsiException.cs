﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO
{
    public class CCSiException : Exception
    {
        public CCSiException(string message)
            : base(message)
        { 
        
        }
    }
}
