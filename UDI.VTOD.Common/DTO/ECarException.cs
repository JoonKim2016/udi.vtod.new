﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO
{
    /// <summary>
    /// This is an object to handle the exceptions of the ECar module.
    /// </summary>
	public class ECarException : Exception
	{
        /// <summary>
        /// This is the constructor of the Ecar Exception module.
        /// </summary>
        /// <param name="message"></param>
		public ECarException(string message): base(message)
		{

		}
	}
}
