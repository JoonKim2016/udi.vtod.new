﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.Enum
{
    /// <summary>
    /// These stores the components of the Fare Details.
    /// </summary>
	public enum ChargePurposeValue
	{
		BaseRate,
		AdditionalPassengers,
		Tax,
		Discount,
		Gratuity,
		CancelFee,
		Other_,
		Hourly,
		PerMinute,
	}
}
