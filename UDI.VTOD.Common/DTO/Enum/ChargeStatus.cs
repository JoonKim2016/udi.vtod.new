﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.Enum
{
    /// <summary>
    /// These stores the type of the Charge Status.
    /// </summary>
	public enum ChargeStatus
	{
		Charged,
		UnCharged,
		Error,
	}
}
