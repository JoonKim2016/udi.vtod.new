﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.Enum
{
    /// <summary>
    /// These stores the Confirmation Types.
    /// </summary>
	public enum ConfirmationType
	{
		Confirmation, //VTDO TripID
		ReturnConfirmation,//VTDO TripID for the second leg
		AgencyConfirmation, //For paratransit
		DispatchConfirmation, //DispatchTrip# like SDS, DDS, MK, CCSi, ...
		DispatchReturnConfirmation,//DispatchTrip# like SDS, DDS, MK, CCSi, ... for the second leg
		ASAPConfirmation, //SDS ASAP Conf#
		FareID, //FareID comes from SDS (in future maybe Taxi) in GroundAvail response.
		RezID, //RezID is specifically for SDS system. It's their table identity.
	}
}
