﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.Enum
{
    /// <summary>
    /// These stores the Contact Types.
    /// </summary>
	public enum ContactType
	{
		Person,
		Driver,
		Dispatch,
	}
}
