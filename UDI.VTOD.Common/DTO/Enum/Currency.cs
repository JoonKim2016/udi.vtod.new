﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.Enum
{
    /// <summary>
    /// These stores the Currency supported by VTOD API.
    /// </summary>
	public enum Currency
	{
		USD = 0,
        SEK = 1,
        EUR = 2,
	}
}
