﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.Enum
{
	/// <summary>
	/// Used in OTA_GroundAvailRS. To set Description of ServiceCharge and Fees.
	/// </summary>
	public enum Description
	{
		FirstPassengerFare,
		AdditionalPassengers,
		TansactionFee,
		FuelSurchargeFee,
		StateTaxes,
		CountyTaxes,
		PointToPointService,
		Hourly,
		InboundStateTax,
		InboundCountyTax,
		OutboundCountyTax,
		OutboundStateTax,
		Discount,
		InboundDiscount,
		OutboundDiscount,
		Gratuity,
		InboundBlackCarSurcharge,
		OutboundBlackCarSurcharge,
		BaseRate,
	}
}
