﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.Enum
{
    /// <summary>
    /// These stores the dispatch types supported by VTOD API.
    /// </summary>
	public enum DispatchType
	{
		//GreenTomato,
		SDS,
		ECar,
		Taxi,
        Van,
		Unkown,
	}
}
