﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.Enum
{
    /// <summary>
    /// These stores the Exception Types of VTOD API.
    /// </summary>
	public enum ExceptionType
	{
		Auth,
		General,
		Accounting,
		SDS,
		ECar,
		Membership,
		Taxi,
		Utility,
		Rate,
		Validation,
		Group,
        Van,
	}
}
