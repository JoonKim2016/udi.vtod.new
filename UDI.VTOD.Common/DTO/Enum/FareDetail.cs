﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.Enum
{
    /// <summary>
    /// These stores the components of Fare Details.
    /// </summary>
	public enum FareDetail
	{
		Minimum,
		PerDistance,
		FlagDrop,
	}
}
