﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.Enum
{
    /// <summary>
    /// These stores the types of Fare.
    /// </summary>
	public enum FareType
	{
		Fare,
		FarePolicy,
		FlatRate,
		Estimation,
	}
}
