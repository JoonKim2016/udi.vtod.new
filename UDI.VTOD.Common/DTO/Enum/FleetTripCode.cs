﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.Enum
{
   public  class FleetTripCode
    {
		public const int Unkonwn = 0;
		public const int Ecar_GreenTomato = 1;
        public const int Ecar_Aleph = 2;
        public const int Ecar_Texas = 3;

        public const int Taxi_UDI33 = 101;
        public const int Taxi_SDS = 102;
        public const int Taxi_CSCI = 103;
        public const int Taxi_MTDATA = 104;
        public const int Taxi_Texas = 105;
        public const int Taxi_AAA_Phoenix = 106;

        public const int SDS_Shuttle_Hourly = 201;
        public const int SDS_Shuttle_Charter = 202;
        public const int SDS_Shuttle_Airport = 203;

        public const int Van_CSCI = 301;

    }
}
