﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.Enum
{
    /// <summary>
    /// These stores the Fleet Types supported in VTOD API.
    /// </summary>
	public enum FleetType
	{
		All,
		SuperShuttle_ExecuCar,
		SuperShuttleSharedRideOnly,
		SuperShuttle, // = 1,
		ExecuCar, // = 2,
		Taxi, // = 3,
		Unkown,
        Van,
        BlackSedan
	}
}
