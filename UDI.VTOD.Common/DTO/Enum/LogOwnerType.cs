﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.Enum
{
    /// <summary>
    /// This is used by log4net when logging is done for every step.
    /// </summary>
	public enum LogOwnerType
	{
		Unknown,
		SDS,
		Taxi,
		VTOD,
        Accounting,
        ECar,
		Aleph,
        Van,
	}
}
