﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.Enum
{
    /// <summary>
    /// These is used by log4net to record the log owner details.
    /// </summary>
	public enum LogOwnerTypeDetail
	{
		VTOD_Controller,
		VTOD_WindowsService_SDSStatusService,
		VTOD_DB,
		VTOD_VTOD_Domain,
		VTOD_Accounting_Domain,
		VTOD_Membership_Domain,
		VTOD_Utility_Domain,
		VTOD_SDS_Domain,
        VTOD_Taxi_GreenTomato,
        VTOD_Taxi_UDI33,
        VTOD_Taxi_CCSi,
        VTOD_Taxi_Utility,
		VTOD_Taxi_Domain,
        VTOD_ECar_GreenTomato,
        VTOD_ECar_Utility,
        VTOD_ECar_Domain,
		SDS_Api,
        VTOD_ECar_Aleph,
        VTOD_ECar_Texas,
        VTOD_Taxi_MTData,
        VTOD_Van_CCSi,
        VTOD_Van_Utility,
        VTOD_Van_Domain
    }
}
