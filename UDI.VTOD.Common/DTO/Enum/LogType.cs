﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.Enum
{
    /// <summary>
    /// These is used by log4net.
    /// </summary>
	public enum LogType
	{
		Error,
		Info,
		Request,
		Response,
		Other,
	}
}
