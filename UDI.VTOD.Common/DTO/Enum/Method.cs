﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.Enum
{
    /// <summary>
    /// This is the list of the methods supported by VTOD API.
    /// </summary>
	public enum Method
	{
		//OTA - Ground
		GetToken,
		GroundAvail,
		GroundVehicleInfo,
		GroundBook,
		GroundCancel,
		GroundResRetrieve,
		GroundOriginalResRetrieve,
		GroundCancelFee,
		GroundCreateAsapRequest,
		GroundGetASAPRequestStatus,
		GroundAbandonASAPRequest,
		GroundGetPickupTime,
		GroundGetFareScheduleWithFees,
        GroundModifyBook,
        GroundGetFinalRoute,

        //SplitPayment
        SplitPaymentInvite,
        SplitPaymentUpdateStatus,
        SplitPaymentGetInvitations, 
        SplitPaymentCheckIfEnabled,     

        //Membership
        MemberCreate,
		MemberLogin,
		MemberForgotPassword,
		MemberGetPasswordRequest,
		MemberUpdateProfile,
		MemberChangeEmailAddress,
		MemberChangePassword,
		MemberGetReservation,
		MemberLinkReservation,
		MemberAddLocation,
		MemberUpdateLocation,
		MemberDeleteLocation,
		MemberGetLocations,
		MemberGetRecentLocations,
		MemberRequestEmailConfirmation,
		MemberGetLatestUnRatedTrip,
		MemberResetPassword,
		MemberGetCorporateProfile,
        MemberGetCorporateProfile2,
		MemberDelete,
        MemberRequestEmailCancellation,
        MemberGetLatestTrip,
        MemberGetReferralLink,
        //Windows service
        WindowsService_PullSDSStatus,
		WindowsService_PullTaxiStatus,
        WindowsService_PullVanStatus,
        WindowsService_PullEcarStatus,
        WindowsService_ProcessDriverInfo,
		/*Accounting Methods*/
		CreateCreditCardAccount,
        EditCreateCreditCardAccount,
		GetCreditCardAccounts,
		SetDefaultCreditCardAccount,
		DeleteCreditCardAccount,
		AddDirectBillAccountToCustomerProfile,
		GetDirectBillAccounts,
		SetDefaultDirectBillAccount,
		DeleteDirectBillAccount,
		GetAirlineRewardsPrograms,
		CreateMembershipMileageAccount,
        ModifyMembershipMileageAccount,
		DeleteMembershipMileageAccount,
		GetMembershipAirlineMileageAccounts,
		ActivateCredits,
		GetCreditsBalance,
		GetFareDetail,
        RewardSetDefaultAccount,
		/********************/

		//Utilities
		UtilityGetWebServicedAirports,
		UtilityGetCharterLocationsByType,
		UtilityVehicleGetDefaultVehicleTypeForPoint,
		UtilityLandmarkCharterSearchLandmark,
		UtilityGetAirportName,
		UtilityAvailableFleet,
		UtilityGetAirlines,
		UtilityGetTripList,
		UtilityGetAirportInstructions,
		UtilityGetHonorifics,
		UtilityGetDestinations,
		UtilityGetBookingCriteria,
        UtilityGetCorporateRequirements,
        UtilityTypeAhead,
        UtilityGetCorporatePaymentMethod,
		UtilityGetAirportPickupOptions,
        UtilityGetLeadTime,
        UtilityGetCustomerInfo,
        UtilityGetPickUpAddress,
        UtilityGetClientToken,
        //Rate
        RateSet,
		RateGet,

		//Group
		GroupValidateDiscountCode,
		GroupGetDiscount,
		GroupGetDefaults,
		GroupGetLandmarks,
		GroupGetLocalizedServicedAirports,
		GroupGetPromotionType,

		//Unknown
		UnKnown,

        //IVR
       
	}
}
