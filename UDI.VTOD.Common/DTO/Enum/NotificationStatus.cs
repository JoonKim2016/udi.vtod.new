﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.Enum
{
    /// <summary>
    /// This stores the Notification Status which are used in Push Notification.
    /// </summary>
	public enum NotificationStatus
	{
		Error = -1,
		Sent = 1,
		NotAllowed = 2,
		UnSent = 0,
	}
}
