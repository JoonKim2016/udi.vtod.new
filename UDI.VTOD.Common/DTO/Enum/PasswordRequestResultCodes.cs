﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.Enum
{
    /// <summary>
    /// These stores the codes of the modification of password.
    /// </summary>
    public enum PasswordRequestResultCodes
	{
        Unknown = -1,
        Success = 0,
        NotRegistered = 1,
        Failure = 2,
	}
}
