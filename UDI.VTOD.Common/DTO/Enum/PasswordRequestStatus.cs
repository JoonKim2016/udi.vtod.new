﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.Enum
{
    /// <summary>
    /// These stores the status of password request object.
    /// </summary>
    public enum PasswordRequestStatus
	{        
        Unknown = -1,
        Valid = 0,
        Disabled = 1,
        Expired = 2,
        Invalid = 3,
	}
}
