﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.Enum
{
    /// <summary>
    /// These stores the Payment types supported by VTOD API.
    /// </summary>
	public enum PaymentType
	{
		Unknown,
		PaymentCard,
		DirectBill,
		Cash,
		ChargedBySDS,
		ChargedByConsumer,
        CorporateBilling,
	}
}
