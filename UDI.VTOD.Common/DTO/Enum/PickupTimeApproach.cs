﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.Enum
{
    /// <summary>
    /// These stores the PickTime Approaches available in VTOD API.
    /// </summary>
	public enum PickupTimeApproach
	{
		Unknown = -1,
		Validation = 1,
		Suggestion = 2
	}
}
