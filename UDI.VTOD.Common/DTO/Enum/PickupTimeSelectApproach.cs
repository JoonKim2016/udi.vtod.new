﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.Enum
{
    //These stores the Pick up Time Select approaches in VTOD API.
	public enum PickupTimeSelectApproach
	{
		Unknown = -1,
		FlightTime = 1,
		RequestedTime = 2
	}
}
