﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.Enum
{
    /// <summary>
    /// These stores the Promotion Types in VTOD API.
    /// </summary>
	public enum PromotionType
	{
		Unknown = 0,
		CreditCode = 1,
		DiscountCode = 2,
	}
}
