﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.Enum
{
    /// <summary>
    /// These stores the Provider TYpes in VTOD API.
    /// </summary>
	public enum ProviderType
	{
		SDS,
		Taxi
	}
}
