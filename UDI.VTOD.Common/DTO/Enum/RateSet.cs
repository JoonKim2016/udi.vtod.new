﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.Enum
{
    /// <summary>
    /// These stores the Airline Code Information.
    /// </summary>
	public enum RateSet
	{
		Cancelled = -1,
		NoRate = -2 //Optional
	}
}
