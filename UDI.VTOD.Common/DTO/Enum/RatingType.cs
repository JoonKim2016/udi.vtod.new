﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.Enum
{
    /// <summary>
    /// These stores the Rating Types in VTOD API.
    /// </summary>
	public enum RatingType
	{
		Trip = 1,
		Cancel = 2,
	}
}
