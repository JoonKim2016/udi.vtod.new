﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.Enum
{
    /// <summary>
    /// These stores the Roles available in VTOD API.
    /// </summary>
	public enum Role
	{
		//Admin role
		Administrator,

		//Fleet's roles
		Fleet_All,
		Fleet_SuperShuttle_ExecuCar,
		Fleet_SuperShuttleSharedRideOnly,
		Fleet_SuperShuttle,
		Fleet_ExecuCar,
		Fleet_Taxi,

		//Method's roles
		Method_GetToken,
		Method_GroundAvail,
		Method_GroundAvail_VehicleInfo,
		Method_GroundBook,
		Method_GroundCancel,
		Method_GroundResRetrieve,
	}
}
