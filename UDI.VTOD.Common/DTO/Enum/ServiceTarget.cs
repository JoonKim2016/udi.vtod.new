﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.Enum
{
    /// <summary>
    /// These stores the environments available in VTOD API.
    /// </summary>
	public enum ServiceTarget
	{
		Test,
		Production
	}
}
