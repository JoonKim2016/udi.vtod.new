﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.Enum
{
    /// <summary>
    /// These stores the Source Website names.
    /// </summary>
    public enum SourceWebsite
	{
        Unknown = 0,
        SuperShuttle = 1,
        ExecuCar = 2,
        ExecuCarExpress = 3,
        SuperShuttleMobile = 4,
        ExecuCarMobile = 5,
        SouthwestAirlines = 6,
	}
}
