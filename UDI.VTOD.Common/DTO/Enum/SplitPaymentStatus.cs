﻿namespace UDI.VTOD.Common.DTO.Enum
{
    public enum SplitPaymentStatus
    {
        Canceled = -1,
        Declined = -2,
        Offered = 1,
        Accepted = 2,
        Completed = 10
    }
}
