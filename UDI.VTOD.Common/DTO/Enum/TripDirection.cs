﻿using System;

namespace UDI.VTOD.Common.DTO.Enum
{
    /// <summary>
    /// These stores the Trip Directions.
    /// </summary>
	public enum TripDirection
	{

		Inbound = 0,  //ToAirport
		Outbound = 1,   //FromAirport
		RoundTrip = 2,    //Round trips
		Unknown = -1,   //Others
	}
}
