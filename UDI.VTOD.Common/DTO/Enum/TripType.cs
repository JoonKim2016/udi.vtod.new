﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.Enum
{
    /// <summary>
    /// These stores the Trip Types values.
    /// </summary>
	public enum TripType
	{
		AsDirected,
		LocationToLocation,
		Airport,
		Hourly,
		Taxi,
		Unknown,
	}
}
