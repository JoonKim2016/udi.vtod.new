﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.Enum
{
    /// <summary>
    /// These stores the Vehicle Types.
    /// </summary>
	public enum VehicleType
	{
		Unknown = -1,
		ExecuCar = 1,
		Shuttle = 3,
		Taxi = 2,
	}
}
