﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO
{
    /// <summary>
    /// This is the Object which handles how to show the Exception Message.
    /// </summary>
	public class ExceptionMessage
	{
		int _code;
		string _message;
		private	ExceptionMessage()
		{

		}
        /// <summary>
        /// This is the constructor of the class Exception Message
        /// </summary>
        /// <param name="code"></param>
        /// <param name="message"></param>
		public	ExceptionMessage(int code, string message)
		{
			_code = code;
			_message = message;

		}

		public int Code
		{
			get
			{
				return _code;
			}
		}

		public string Message
		{
			get
			{
				return _message;
			}
		}
	}
}
