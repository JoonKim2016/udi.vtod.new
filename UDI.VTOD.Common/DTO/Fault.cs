﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO
{
    /// <summary>
    /// This is a generic Fault Object which handles what elements should be displayed.
    /// </summary>
	[DataContract]
	public abstract class Fault
	{
		[DataMember(EmitDefaultValue = false)]
		[XmlIgnore]
		[IgnoreDataMember]
		public WebContentFormat Format { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlIgnore]
		[IgnoreDataMember]
		public HttpStatusCode StatusCode { get; set; }

		#region Nullable
		public bool ShouldSerializeStatusCode()
		{
			return false;
		}

		public bool ShouldSerializeFormat()
		{
			return false;
		}
		#endregion
	}
}
