﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO
{
    /// <summary>
    /// This is an Object which handles the exception of the Membership domain.
    /// </summary>
	public class MembershipException : Exception
	{
        /// <summary>
        /// This is the constructor of the Membership Exception class.
        /// </summary>
        /// <param name="message"></param>
		public MembershipException(string message)
			: base(message)
		{
		}
	}
}
