﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    /// <summary>
    /// This is a Request Object which contains Member ID,Payment Card details like Card Number,Card Holder name,Expity Date and Address.
    /// This Object is used to add a card to a Member.
    /// </summary>
	[DataContract]
	public class AccountingAddMemberPaymentCardRQ : Fault
	{
     
		[DataMember(EmitDefaultValue = false)]
		public string MemberId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Nickname { get; set; }
        
		//[DataMember(EmitDefaultValue = false)]
		//public bool IsDefault { get; set; }

		//[DataMember(EmitDefaultValue = false)]
		//public CreditCardAccount CreditCardAccount { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public PaymentCard PaymentCard { get; set; }

		//[DataMember(EmitDefaultValue = false)]
		//public string CvvNumber { get; set; }

		#region Common
		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string EchoToken { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string Target { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string Version { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string PrimaryLangID { get; set; }
		#endregion
	}
}
