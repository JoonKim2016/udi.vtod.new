﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    [DataContract]
    public class AccountingEditMemberPaymentCardRQ : Fault
    {

        [DataMember(EmitDefaultValue = false)]
        public Int32 MemberId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public Int32 AccountId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CardHolderName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CVV { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ExpirationDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string  PostalCode { get; set; }

        #region Common
        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string EchoToken { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Target { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Version { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string PrimaryLangID { get; set; }
        #endregion
    }
}
