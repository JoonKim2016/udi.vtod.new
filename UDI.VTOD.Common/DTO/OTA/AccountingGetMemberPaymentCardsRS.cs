﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    /// <summary>
    /// This is a Response Object which contains the Payment Card details of a Member like Card Number,Address,Type,Expiry date.
    /// </summary>
	[DataContract]
    public class AccountingGetMemberPaymentCardsRS : Fault
	{
        [DataMember(EmitDefaultValue = false)]
		public List<PaymentCard> PaymentCards { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public CorporateCompanyRequirement CorporateCompanyRequirement { get; set; }


        [XmlElement]
		[DataMember(EmitDefaultValue = false)]
		public Success Success { get; set; }
		
		#region Common
        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string EchoToken { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Target { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Version { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string PrimaryLangID { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public ErrorList Errors { get; set; }
        #endregion
	}
}
