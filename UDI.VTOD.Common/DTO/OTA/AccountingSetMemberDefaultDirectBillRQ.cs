﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    /// <summary>
    /// This is a Request Object which contains Member ID and Account ID.
    /// This object is used to set up a Direct Bill as a default to a Member.
    /// </summary>
	[DataContract]
    public class AccountingSetMemberDefaultDirectBillRQ : Fault
	{
      
		[DataMember(EmitDefaultValue = false)]
		public int MemberId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int AccountId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsDefault { get; set; }

        #region Common
        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string EchoToken { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Target { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Version { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string PrimaryLangID { get; set; }
        #endregion
	}
}
