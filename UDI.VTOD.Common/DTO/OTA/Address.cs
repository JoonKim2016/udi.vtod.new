﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    /// <summary>
    /// This is an Address Object.It stores the components of Address.
    /// </summary>
	[DataContract]
	public class Address
	{
        [DataMember(EmitDefaultValue = false)]
        public string AddressType { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string StreetNmbr { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string BldgRoom { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string AddressLine { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string CityName { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string PostalCode { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string LocationName { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public LocationType LocationType { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public StateProv StateProv { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public CountryName CountryName { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string Latitude { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string Longitude { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public TPA_Extensions TPA_Extensions { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string StreetName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string StreetSuffix { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Unit { get; set; }
	}
}
