﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    /// <summary>
    /// These stores the components of Advance Booking like OffsetTimeUnit,OffsetUnitMultiplier and RequiredInd.
    /// </summary>
	[DataContract]
	public class AdvanceBooking
	{
		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string OffsetTimeUnit { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public int OffsetUnitMultiplier { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public bool RequiredInd { get; set; }
	}
}
