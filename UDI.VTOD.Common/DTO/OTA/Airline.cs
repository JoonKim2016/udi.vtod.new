﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    /// <summary>
    /// These stores the details of the Airline.
    /// </summary>
	[DataContract]
	public class Airline
	{
		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string Code { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string FlightNumber { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string FlightDateTime { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string CodeContext { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string CompanyShortName { get; set; }

        [DataMember(EmitDefaultValue = true)]
        [XmlAttribute]
        public bool IsCommon { get; set; }
        
	}
}
