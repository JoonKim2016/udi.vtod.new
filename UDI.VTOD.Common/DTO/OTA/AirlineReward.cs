﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    /// <summary>
    /// These stores details of Airline Rewards.
    /// </summary>
	[DataContract]
	public class AirlineReward
	{
		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public int RewardID { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public int AccountID { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string ProgramName { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string IsDefault { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string AccountNumber { get; set; }

		#region ShouldSerialize
		public bool ShouldSerializeAccountID()
		{
			return AccountID > 0;
		}
		#endregion

	}
}
