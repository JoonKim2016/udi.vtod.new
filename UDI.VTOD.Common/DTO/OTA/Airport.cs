﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    /// <summary>
    /// These stores the details of Airport and flight details.
    /// </summary>
	[DataContract]
	public class Airport
	{
        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string AirportCode { get; set; }
        
        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string AirportName { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlElement]
		public bool AllowASAPRequests { get; set; }
		
        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public bool AllowChildSeatInput { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public bool AllowInfantSeatInput { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public bool AllowWMVRequests { get; set; }
        
        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string City { get; set; }
        
        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string Country { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string CountryCode { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public bool ECARServiced { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public bool IsAffiliate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public bool IsDefaultAirport { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string PortType { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public int Serviced { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string State { get; set; }
        
        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string StateName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string ZipCode { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string Latitude { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string Longitude { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public bool DiscountApplies { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public bool? IsRideNowAllowed { get; set; }
        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public bool? PreventAccessibleBookings { get; set; }
        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public bool? PreventGratuity { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string CultureCode { get; set; }
        [DataMember(EmitDefaultValue = true)]
        [XmlElement]
        public int CurrencyId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string IsoCurrencyCode { get; set; }
    }
}
