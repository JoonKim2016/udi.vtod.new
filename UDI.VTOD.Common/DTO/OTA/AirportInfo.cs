﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.OTA
{
    /// <summary>
    /// These stores the Airport Information like Arrival and Departure details.
    /// </summary>
	[DataContract]
	public class AirportInfo
	{
		[DataMember(EmitDefaultValue = false)]
		public Arrival_Departure Arrival { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Arrival_Departure Departure { get; set; }
	}
}
