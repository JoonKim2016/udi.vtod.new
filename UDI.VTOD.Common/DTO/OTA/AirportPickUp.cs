﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    /// <summary>
    /// These stores the Airport pick up values airport port code and airport description
    /// </summary>
    [DataContract]
    public class AirportPickUp
    {
        [DataMember(EmitDefaultValue = false)]
        [XmlAttribute]
        public string airport_pickup_value { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlAttribute]
        public string airport_pickup_description { get; set; }
    }
}
