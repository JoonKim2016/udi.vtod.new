﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    /// <summary>
    /// These stores the Airport Pick Up options.
    /// </summary>
	[DataContract]
	public class AirportPickupOptions
	{
    
		[DataMember(EmitDefaultValue = false)]
        public List<NameValue> Items { get; set; }

	}
}
