﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.OTA
{
    public class AlephCoporateCreditCardInfo
    {
        public string image_url { get; set; }
        public bool is_primary_card { get; set; }
        public string card_type { get; set; }
        public bool is_expired_card { get; set; }
        public string expiration_month { get; set; }
        public string expiration_year { get; set; }
        public string nick_name { get; set; }
        public string short_nick_name { get; set; }
        public string last_four { get; set; }
        public string payment_method_token { get; set; }
        public string text_message { get; set; }
        public bool is_blocked_card { get; set; }
        public string short_card_type { get; set; }
        public bool is_company_card { get; set; }
        public bool is_verified_card { get; set; }
        public int payment_service_provider_error_id { get; set; }
    }
}
