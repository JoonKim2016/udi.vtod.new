﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    /// <summary>
    /// These stores the Corporate details like Account,Profiles and the Criteria.
    /// </summary>
	[DataContract]
	public class AlephCorporate
	{
		[DataMember(EmitDefaultValue = false)]
		public List<AlephCorporateAccount> AlephAccount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CorporateName { get; set; }
	}
}
