﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    /// <summary>
    /// This stores the Departure details.
    /// </summary>
	[DataContract]
	public class Arrival_Departure
	{
		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string LocationCode { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string Latitude { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string Longitude { get; set; }
	}
}
