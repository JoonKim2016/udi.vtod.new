﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;


namespace UDI.VTOD.Common.DTO.OTA
{
    [DataContract]
    public class BookingDetails : Fault
    {
        [DataMember(EmitDefaultValue = false)]
        public string Time { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Status { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public Int64 BookingId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string VehicleId { set; get; }

        [DataMember(EmitDefaultValue = false)]
        public string BookingChannel { set; get; }

        [DataMember(EmitDefaultValue = false)]
        public Address PickupAddress { set; get; }

    }
}
