﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    /// <summary>
    /// These stores the Calculation details of a pickup/drop off
    /// </summary>
	[DataContract]
	public class Calculation
	{
		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public decimal UnitCharge { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public decimal Quantity { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public decimal MinQuantity { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public decimal MaxQuantity { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public decimal Total { get; set; }
	}
}
