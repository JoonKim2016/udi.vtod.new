﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace UDI.VTOD.Common.DTO.OTA
{
    /// <summary>
    /// These stores the Cancellation details like ID.
    /// </summary>
	[DataContract]
	public class CancelConfirmation
	{
		[DataMember(EmitDefaultValue = false)]
		public UniqueID UniqueID { get; set; }
	}
}
