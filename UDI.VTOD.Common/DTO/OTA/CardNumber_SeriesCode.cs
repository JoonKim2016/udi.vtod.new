﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    /// <summary>
    /// These stores the details of Credit Card/Payment Card.
    /// </summary>
	[DataContract]
	public class CardNumber_SeriesCode
	{
		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string ID { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string EncryptedValue { get; set; }

		[XmlElement]
		[DataMember(EmitDefaultValue = false)]
		public string PlainText { get; set; }

		[XmlElement]
		[DataMember(EmitDefaultValue = false)]
		public string LastFourDigit { get; set; }
	}
}
