﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    /// <summary>
    /// These stores the details of the Card Type.
    /// </summary>
	[DataContract]
	public class CardType
	{
		[DataMember(EmitDefaultValue = false)]
		[XmlText]
		public string Value { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string Description { get; set; }
	}
}
