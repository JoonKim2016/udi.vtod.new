﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    /// <summary>
    /// These stores the Cash details.
    /// </summary>
	[DataContract]
	public class Cash
	{
		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public bool CashIndicator { get; set; }

	}
}
