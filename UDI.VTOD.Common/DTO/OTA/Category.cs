﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    /// <summary>
    /// These stores the Category details like value,description and code. 
    /// </summary>
	[DataContract]
	public class Category
	{
		[DataMember(EmitDefaultValue = false)]
		[XmlText]
		public string Value { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string Description { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string Code { get; set; }
	}
}
