﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    /// <summary>
    /// These stores the details of the charges.
    /// </summary>
	[DataContract]
	public class ChargePurpose
	{
		[DataMember(EmitDefaultValue = false)]
		[XmlText]
		public string Value { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string Code { get; set; }
	}
}
