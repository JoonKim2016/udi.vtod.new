﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    /// <summary>
    /// These shows the Circle details like Radius.
    /// </summary>
	[DataContract]
	public class Circle
	{
		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public decimal Radius { get; set; }
	}
}
