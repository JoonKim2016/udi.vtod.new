﻿using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    [DataContract]
    public class CommonRequest : Fault
    {
        #region Common
        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string EchoToken { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Target { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Version { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string PrimaryLangID { get; set; }
        #endregion
    }
}
