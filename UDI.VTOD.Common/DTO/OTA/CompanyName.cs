﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.OTA
{
    /// <summary>
    /// This stores the Conpany Name.
    /// </summary>
	[DataContract]
	public class CompanyName
	{
		[DataMember(EmitDefaultValue = false)]
		public string CompanyShortName { get; set; }
	}
}
