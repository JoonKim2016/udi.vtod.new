﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    /// <summary>
    /// These stores the Confirmation details like Type,ID,device names,pick up times and passenger details.
    /// </summary>
	[DataContract]
	public class Confirmation
	{
		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string Type { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string ID { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlAttribute]
        public int Segment { get; set; }


		[DataMember(EmitDefaultValue = false)]
		public TPA_Extensions TPA_Extensions { get; set; }
	}
}
