﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    /// <summary>
    /// These stores the Contact details of the Driver.
    /// </summary>
	[DataContract]
	public class Contact : Fault
	{
		[DataMember(EmitDefaultValue = false)]
		public Telephone Telephone { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string Name { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string Type { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string PhotoUrl { set; get; }

        [DataMember(EmitDefaultValue = false)]
        public string DriverId { set; get; }

        [DataMember(EmitDefaultValue = false)]
        public bool? ZtripDesignation { set; get; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime? ZtripDateOfDesignation  { set; get; }

        [DataMember(EmitDefaultValue = false)]
        public string DateOfVehicleLease  { set; get; }

        [DataMember(EmitDefaultValue = false)]
        public string NumberOfDaysOrHoursLeaseDuration { set; get; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime StartDate { set; get; }

        
	}
}
