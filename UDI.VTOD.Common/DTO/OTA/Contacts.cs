﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{//
	//[DataContract(Name = "AddMemberPaymentCardRQ")]
	[DataContract]
	public class Contacts : Fault
	{
		[DataMember(EmitDefaultValue = false)]
		[XmlElement("Contact")]
		public List<Contact> Items { get; set; }
	}
}
