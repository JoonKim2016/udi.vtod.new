﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    /// <summary>
    /// These stores the Corporate details like Account,Profiles and the Criteria.
    /// </summary>
	[DataContract]
	public class Corporate
	{
		[DataMember(EmitDefaultValue = false)]
		public CorporateAccount CorporateAccount { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public CorporateProfiles CorporateProfiles { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Criteria Criteria { get; set; }
	}
}
