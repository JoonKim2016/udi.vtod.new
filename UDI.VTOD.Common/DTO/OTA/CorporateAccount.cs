﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.OTA
{
    /// <summary>
    /// These stores the Corporate Account details like Corporation Name,User id and User Key.
    /// </summary>
	public class CorporateAccount
	{
		[DataMember(EmitDefaultValue = false)]
		public string Corporation { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string UserID { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string UserKey { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CorporateName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Account { get; set; }
	}
}
