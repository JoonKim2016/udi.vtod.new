﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    /// <summary>
    /// These stores the Corporate Company Requirement like Company Name,Account Name,Payment method and Requirements.
    /// </summary>
    [DataContract]
    public class CorporateCompanyRequirement
    {
        [DataMember(EmitDefaultValue = false)]
        public string CorporateCompanyName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<CorporationCompanyAccountRequirements> CorporateCompanyAccounts { get; set; }
    }
}
