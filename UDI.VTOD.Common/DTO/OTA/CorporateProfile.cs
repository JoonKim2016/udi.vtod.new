﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    /// <summary>
    /// These stores Corporate Profile like Provider ID,Provider Account ID,Company name,Profile ID,Payment method,Username and Company name.
    /// </summary>
	[DataContract]
	public class CorporateProfile
	{
		[DataMember(EmitDefaultValue = false)]
		public int ProviderID { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string ProviderAccountId { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string CompanyName { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string ProfileID { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string DefaultPaymentMethod { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string Username { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string PreferredCompany { get; set; }
	}
}
