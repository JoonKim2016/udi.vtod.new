﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    /// <summary>
    /// These stores the Corporate Profiles like Provider ID,Provider Account ID,Company name,Profile ID,Payment method,User name and Preferred Company name.
    /// </summary>
	[DataContract]
	public class CorporateProfiles
	{
		[XmlElement("CorporateProfile")]
		[DataMember(EmitDefaultValue = false)]
		public List<CorporateProfile> CorporateProfileList { get; set; }
	}
}
