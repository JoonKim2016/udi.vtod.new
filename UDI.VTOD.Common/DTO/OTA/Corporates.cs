﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    /// <summary>
    /// These stores the Corporate details like Corporate Profiles,Criteria and Corporate Account.
    /// </summary>
	[DataContract]
	public class Corporates
	{
		[XmlElement("Corporate")]
		[DataMember(EmitDefaultValue = false)]
		public List<Corporate> CorporateList { get; set; }
	}
}
