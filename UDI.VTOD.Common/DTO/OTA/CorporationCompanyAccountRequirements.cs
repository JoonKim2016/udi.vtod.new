﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.OTA
{
    /// <summary>
    /// These stores the Corporate Company Requirements like requirements,account name and payment methods.
    /// </summary>
    [DataContract]
    public class CorporationCompanyAccountRequirements
    {
        [DataMember(EmitDefaultValue = false)]
        public string AccountName { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public bool? IsCreditCardEligible { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<RequirementDetail> Requirements { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<string> PaymentMethods { get; set; }

        public bool ShouldSerializeIsCreditCardValidationRequired() 
        {
            return IsCreditCardEligible.HasValue;
        }
    }
}
