﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    /// <summary>
    /// These stores the Country Name
    /// </summary>
	[DataContract]
	public class CountryName
	{
		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string Code { get; set; }
	}
}
