﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    /// <summary>
    /// These stores the Criteria Information like Special Inputs.
    /// </summary>
	public class Criteria
	{
		[DataMember(EmitDefaultValue = false)]
		[XmlElement("SpecialInputs")]
		public List<NameValue> SpecialInputs { get; set; }

	}
}
