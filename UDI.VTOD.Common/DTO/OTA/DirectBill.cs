﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    /// <summary>
    /// These stores the Direct Bill details like Billing number,Passowrd,Company Name,Source,device name,pick up and drop off details
    /// </summary>
	[DataContract]
	public class DirectBill
	{
		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string ID { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string BillingNumber { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string Password { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public CompanyName CompanyName { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public TPA_Extensions TPA_Extensions { get; set; }
	}
}
