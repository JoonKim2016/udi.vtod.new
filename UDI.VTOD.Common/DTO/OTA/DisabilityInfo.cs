﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class DisabilityInfo
	{
		[DataMember(EmitDefaultValue = false)]
		public bool RequiredInd { get; set; }
	}
}
