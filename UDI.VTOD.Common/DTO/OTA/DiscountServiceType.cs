﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.OTA
{
	public class DiscountServiceType
	{
		public byte DiscountMethod { get; set; }

		public decimal OneWayDollarAmount { get; set; }

		public int OneWayPercentage { get; set; }

		public decimal RoundtripDollarAmount { get; set; }

		public int RoundtripPercentage { get; set; }

		public string ServiceTypeCode { get; set; }

		public string ServiceTypeDescription { get; set; }
	}
}
