﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;


namespace UDI.VTOD.Common.DTO.OTA
{
    [DataContract]
   public  class DispatchRecord
    {
        [DataMember(EmitDefaultValue = false)]
        public string FleetId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string KeyWordName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string KeyWordValue { get; set; }
    }
}
