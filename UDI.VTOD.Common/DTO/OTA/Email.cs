﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class Email
	{
		[DataMember(EmitDefaultValue = false)]
        [XmlAttribute("ShareMarketInd")]
		public bool ShareMarketInd { get; set; }
		[XmlText]
		[DataMember(EmitDefaultValue = false)]
		public string Value { get; set; }
	}
}
