﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class Error
	{
		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string Type { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public int Code { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlText]
		public string Value { get; set; }

		//[DataMember(EmitDefaultValue = false)]
		//[XmlIgnore]
		//[IgnoreDataMember]
		//public WebContentFormat Format { get; set; }
	}
}
