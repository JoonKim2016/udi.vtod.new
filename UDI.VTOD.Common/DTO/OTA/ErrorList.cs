﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class ErrorList
	{
		[XmlElement("Error")]
		[DataMember(EmitDefaultValue = false)]
		public List<Error> Errors { get; set; }
	}
}
