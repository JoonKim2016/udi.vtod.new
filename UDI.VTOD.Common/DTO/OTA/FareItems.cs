﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class FareItems
	{
		[DataMember(EmitDefaultValue = false)]
		[XmlElement("FareItem")]
		public List<NameValue> Items { get; set; }

	}
}
