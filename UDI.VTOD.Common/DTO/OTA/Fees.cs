﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class Fees
	{
		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string Description { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlElement("Calculation")]
		public List<Calculation> Calculations { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public ChargePurpose ChargePurpose { get; set; }
	}
}
