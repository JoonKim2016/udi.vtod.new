﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class Fleet
	{
		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string Type { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string Now { get; set; } //bool

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string Later { get; set; } //bool

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public int RestrictNowBookingMins { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public int DefaultRadius { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public bool? DisabilityVehicleInd { get; set; }
        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public int MaxPassengers { get; set; }
    }
}
