﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class Fleets
	{
		[DataMember(EmitDefaultValue = false)]
		[XmlElement("Fleet")]
		public List<Fleet> Items { get; set; }

	}
}
