﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class Geolocation
	{
		[DataMember(EmitDefaultValue = true)]
		[XmlAttribute]
		public decimal Course { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public decimal Lat { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public decimal Long { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlAttribute]
        public decimal PriorLat { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlAttribute]
        public decimal PriorLong { get; set; }
	}
}
