﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class GetFareDetailRS : Fault
	{
        /// <summary>
        /// This is a Response Object which contains Fare Details like Fare Amount,Gratuity Amount,Fuel Surcharge Amount,Discount Amount.
        /// It also contains the Payment Card Information.
        /// </summary>
		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string EchoToken { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string Target { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string Version { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string PrimaryLangID { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Success Success { get; set; }

		[DataMember(EmitDefaultValue = false)]
		//[XmlElement("FareItem")]
		public FareItems FareItems { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public PaymentInfo PaymentInfo { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public SplitPaymentInfo SplitMemberInfo { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<SplitPaymentFare> SplitRideInfo { get; set; }

        [DataMember(EmitDefaultValue = false)]
		public TripInfo TripInfo { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public ErrorList Errors { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlElement]
		public bool PickMeUpNow { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlElement]
		public string PickupLocation { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlElement]
		public string DropOffLocation { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlElement]
		public string PickupLatitude { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlElement]
		public string PickupLongitude { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlElement]
		public string DropOffLatitude { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlElement]
		public string DropOffLongitude { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlElement]
		public string PickupTime { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Contacts Contacts { get; set; }

	}
}
