﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class OTA_GroundGetFinalRouteRS : Fault
	{
        /// <summary>
        /// This is a Response Object which contains Final Route for example geolocations of the vehicle.
        /// </summary>
		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string EchoToken { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string Target { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string Version { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string PrimaryLangID { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Success Success { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<Geolocation> Geolocations { get; set; }

        [DataMember(EmitDefaultValue = false)]
		public ErrorList Errors { get; set; }
	}
}
