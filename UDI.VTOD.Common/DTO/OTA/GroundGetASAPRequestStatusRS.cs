﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class GroundGetASAPRequestStatusRS : Fault
	{
        /// <summary>
        /// This is the Response Object which contains ApprovedPickupTime,Status like  Unknown,PickupDenied,OriginalPickupTimeApproved and 
        /// New Pick Up time.
        /// </summary>
		[DataMember(EmitDefaultValue = false)]
		public DateTime ApprovedPickupTime { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string DispositionCode { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public bool Guaranteed { get; set; }

		[XmlElement]
		[DataMember(EmitDefaultValue = false)]
		public Success Success { get; set; }
		
		#region Common
        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string EchoToken { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Target { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Version { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string PrimaryLangID { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public ErrorList Errors { get; set; }
        #endregion
	}
}
