﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class GroundGetFareScheduleWithFeesRQ : Fault
	{
        /// <summary>
        /// This request object contains the Inbound Fare ID, Outbound Fare ID and travel dates.
        /// This is used to  return the charges of the inbound and outbound trip.
        /// </summary>
		[DataMember(EmitDefaultValue = false)]
		public string PromotionCode { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public int GroupID { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public int InboundFareID { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string InboundTravelDate { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public int OutboundFareID { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string OutboundTravelDate { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public byte PayingPax { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string PaymentType { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public POS POS { get; set; }

		#region Common
		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string EchoToken { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string Target { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string Version { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string PrimaryLangID { get; set; }
		#endregion
	}
}
