﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class GroundGetPickupTimeRQ : Fault
	{
        /// <summary>
        /// This request object contains the fare id,trip direction etc.
        /// This is used to find out the pick up time of a trip.
        /// </summary>
		[DataMember(EmitDefaultValue = false)]
		public DisabilityInfo DisabilityInfo { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string CodeContext { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string FareID { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string TravelTime { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string TripDirection { get; set; }

		#region Common
		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string EchoToken { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string Target { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string Version { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string PrimaryLangID { get; set; }
		#endregion
	}
}
