﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class GroundReservation
	{
		[DataMember(EmitDefaultValue = false)]
		public Passenger Passenger { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Service Service { get; set; }

		[XmlElement("RateQualifier")]
		[DataMember(EmitDefaultValue = false)]
		public List<RateQualifier> RateQualifiers { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public ServiceType ServiceType { get; set; }

	}
}
