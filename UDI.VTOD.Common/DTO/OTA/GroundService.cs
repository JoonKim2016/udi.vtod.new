﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class GroundService
	{
		[DataMember(EmitDefaultValue = false)]
		public Service Service { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public RateQualifier RateQualifier { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Restrictions Restrictions { get; set; }

		[XmlElement("ServiceCharges")]
		[DataMember(EmitDefaultValue = false)]
		public List<ServiceCharges> ServiceCharges { get; set; }

		[XmlElement("Fees")]
		[DataMember(EmitDefaultValue = false)]
		public List<Fees> Fees { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public TotalCharge TotalCharge { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Reference Reference { get; set; }
	}
}
