﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class GroupGetDefaultsRS : Fault
	{
        /// <summary>
        /// This is the Response Object which contains whether the Discount Code of the Request Object is for Inbound OutBound Trip,Outbound Inbound Trip .
        /// </summary>
		[DataMember(EmitDefaultValue = false)]
		public bool AllowInboundOutboundRoundtrip { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public bool AllowInboundService;

		[DataMember(EmitDefaultValue = false)]
		public bool AllowOutboundInboundRoundtrip { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public bool AllowOutboundService { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public byte BillingType { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public int DefaultDirection { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public int GroupID { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public bool RestrictLandmarks { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public decimal SedanGratuityAmount { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public byte SedanGratuityPercent { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public byte SedanGratuityType { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public decimal VanGratuityAmount { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public byte VanGratuityPercent { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public byte VanGratuityType { get; set; }



		#region Common
		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string EchoToken { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string Target { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string Version { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string PrimaryLangID { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public ErrorList Errors { get; set; }
		#endregion
	}
}
