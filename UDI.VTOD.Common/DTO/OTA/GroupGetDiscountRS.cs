﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class GroupGetDiscountRS : Fault
	{
        /// <summary>
        /// This is a Response Object which returns a Discount Code and Discount Service Type like OneWay Amount,OneWay Percentage,Roundtrip Amount,Roundtrip Percentage.
        /// </summary>
		[XmlElement]
		[DataMember(EmitDefaultValue = false)]
		public Success Success { get; set; }
		
		[DataMember(EmitDefaultValue = false)]
		public List<Airport> Airports { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public List<DiscountServiceType> DiscountServiceTypes { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public bool? AllZips { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public bool? CustomerReadOnly { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public bool? HideDiscountCodeFromCustomer { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public int DiscountID { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string DiscountCode { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public System.DateTime? EndDate { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public int GroupID { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string GroupName { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public bool? HideBlueVanRates { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public bool? HideEcarRates { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public bool? HideWelcomeScreen { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public System.DateTime StartDate { get; set; }

		#region Common
		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string EchoToken { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string Target { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string Version { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string PrimaryLangID { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public ErrorList Errors { get; set; }
		#endregion
	}
}
