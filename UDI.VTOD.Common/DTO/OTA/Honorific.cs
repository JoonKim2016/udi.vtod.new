﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class Honorific
	{
		[DataMember(EmitDefaultValue = false)]
		public string Gender { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string LanguageCode { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public int ID { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string Title { get; set; }
	}
}
