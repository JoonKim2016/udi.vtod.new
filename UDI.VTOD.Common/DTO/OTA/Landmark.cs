﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	public class Landmark
	{
		[DataMember(EmitDefaultValue = false)]
		public bool HasScheduledService { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Location Location { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public int? LandmarkID { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public int? MasterLandmarkID { get; set; }
	}
}
