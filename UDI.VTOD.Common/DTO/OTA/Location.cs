﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class Location
	{
		[DataMember(EmitDefaultValue = false)]
		[XmlElement]
		public int ID { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string PrimaryLangID { get; set; }
		
		[DataMember(EmitDefaultValue = false)]
		public Address Address { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Telephone Telephone { get; set; }

		#region ShouldSerialize
		public bool ShouldSerializeID()
		{
			return ID > 0;
		}
		#endregion
	}
}
