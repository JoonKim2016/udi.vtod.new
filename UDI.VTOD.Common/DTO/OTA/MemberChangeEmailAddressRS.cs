﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    public class MemberChangeEmailAddressRS : Fault
    {
        /// <summary>
        /// This is the Response Object of  Changing the Email Address of a Member.
        /// In case of success it returns Success Object.
        /// Otherwise it returns an Error Object.
        /// </summary>
        [XmlElement]
        [DataMember(EmitDefaultValue = false)]
		public Success Success { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Corporates Corporates { get; set; }

		
		#region Common
        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string EchoToken { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Target { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Version { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string PrimaryLangID { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public ErrorList Errors { get; set; }
        #endregion
    }
}
