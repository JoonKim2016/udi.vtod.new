﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    public class MemberChangePasswordRQ
    {
        /// <summary>
        /// This is the Request Object to Change the password of a Member.
        /// </summary>
        [XmlElement]
        [DataMember(EmitDefaultValue = false)]
        public string EmailAddress { get; set; }

        [XmlElement]
        [DataMember(EmitDefaultValue = false)]
        public string OldPassword { get; set; }

        [XmlElement]
        [DataMember(EmitDefaultValue = false)]
        public string NewPassword { get; set; }

        #region Common
        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string EchoToken { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Target { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Version { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string PrimaryLangID { get; set; }
        #endregion
    }
}
