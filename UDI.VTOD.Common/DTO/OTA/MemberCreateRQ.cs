﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;


namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class MemberCreateRQ
	{
        /// <summary>
        /// This is the Member Request Object which is used to create a member.To create a member user needs Member Details like Name,Zip Code,Email Address,Phone etc.
        /// </summary>
		[DataMember(EmitDefaultValue = false)]
		public MembershipRecord MembershipRecord { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public Address Address { set; get; }


        [DataMember(EmitDefaultValue = false)]
		public CorporateAccount CorporateAccount { get; set; }

		[XmlElement]
		[DataMember(EmitDefaultValue = false)]
		public string Password { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public TPA_Extensions TPA_Extensions { get; set; }
		
		#region Common
		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string EchoToken { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string Target { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string Version { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string PrimaryLangID { get; set; }
		#endregion
	}
}
