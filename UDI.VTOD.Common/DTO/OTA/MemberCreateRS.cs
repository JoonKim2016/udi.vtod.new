﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class MemberCreateRS : Fault
	{
        /// <summary>
        /// This is the Response Object of the Member Creation.If the request if successful it returns the Member ID.
        /// In case of failure it returns the Error Object.
        /// </summary>
		[XmlElement]
		[DataMember(EmitDefaultValue = false)]
		public int MemberID { get; set; }
        [XmlElement]
        [DataMember(EmitDefaultValue = false)]
        public Guid CustomerGuid { get; set; }
        
        [DataMember(EmitDefaultValue = false)]
		public Corporates Corporates { get; set; }
		
		[XmlElement]
		[DataMember(EmitDefaultValue = false)]
		public Success Success { get; set; }

		#region Common
		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string EchoToken { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string Target { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string Version { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string PrimaryLangID { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public ErrorList Errors { get; set; }
		#endregion
	}
}
