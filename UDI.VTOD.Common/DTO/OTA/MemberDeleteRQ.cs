﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class MemberDeleteRQ
    {  
        /// <summary>
        /// This Request Object is used to delete a Member from VTOD.
        /// The Request Object contains Email Address,Password etc. to delete.
        /// </summary>
        [XmlElement]
        [DataMember(EmitDefaultValue = false)]
        public string EmailAddress { get; set; }

        [XmlElement]
        [DataMember(EmitDefaultValue = false)]
        public string Password { get; set; }

        [XmlElement]
        [DataMember(EmitDefaultValue = false)]
        public string DeviceToken { get; set; }

        [XmlElement]
        [DataMember(EmitDefaultValue = false)]
        public string MemberID{ get; set; }

        #region Common
        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string EchoToken { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Target { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Version { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string PrimaryLangID { get; set; }
        #endregion
    }
}
