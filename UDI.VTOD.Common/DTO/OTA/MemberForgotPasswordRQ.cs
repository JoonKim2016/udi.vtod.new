﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class MemberForgotPasswordRQ
    { 
        /// <summary>
        /// This is the Request Object to retrieve the Password of a Member; in case user forgets the passowrd.
        /// </summary>
        [XmlElement]
        [DataMember(EmitDefaultValue = false)]
        public string EmailAddress { get; set; }

        [XmlElement]
        [DataMember(EmitDefaultValue = false)]
        public string SourceWebsite { get; set; }
        
             
        #region Common
        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string EchoToken { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Target { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Version { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string PrimaryLangID { get; set; }
        #endregion
    }
}
