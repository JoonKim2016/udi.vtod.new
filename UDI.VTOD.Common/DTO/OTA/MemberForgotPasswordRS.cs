﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class MemberForgotPasswordRS : Fault
    {       
        /*[DataMember(EmitDefaultValue = false)]
        public MemberPasswordRecord Result { get; set; } */
        /// <summary>
        /// This is the Response Object of the Forgot Password of the Member. 
        /// In case of success user receives the mail with the password and a Success Object is returned.
        /// Otherwise Error Object is returned.
        /// </summary>
        [XmlElement]
        [DataMember(EmitDefaultValue = false)]
		public Success Success { get; set; }

        #region Common
        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string EchoToken { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Target { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Version { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string PrimaryLangID { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public ErrorList Errors { get; set; }
        #endregion
    }
}
