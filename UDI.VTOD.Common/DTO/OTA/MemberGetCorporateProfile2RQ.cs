﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class MemberGetCorporateProfile2RQ
    {
        /// <summary>
        /// This is the Request Object for retrieving the Corporate Details Of a Member.
        /// Corporate ID and Member ID are passed with the Request Object.
        /// </summary>
		[XmlElement]
		[DataMember(EmitDefaultValue = false)]
		public string CorporateID { get; set; }

		[XmlElement]
		[DataMember(EmitDefaultValue = false)]
		public int MemberID { get; set; }
		
		#region Common
        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string EchoToken { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Target { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Version { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string PrimaryLangID { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public Address Address { get; set; }
        #endregion
    }
}
