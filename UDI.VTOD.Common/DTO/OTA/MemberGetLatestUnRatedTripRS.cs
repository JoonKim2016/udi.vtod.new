﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class MemberGetLatestUnRatedTripRS : Fault
    {
        ///
        /// This is Response Object Of Retrieving the Latest Unrated Trip.
        /// In case of success a list of Membership Reservation is returned with the Success Object.
        /// In case of an Error an Error Object is returned.
        ///
		//[XmlElement]
		//[DataMember(EmitDefaultValue = false)]
		public bool HasRecords { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<MembershipReservation> MemberReservationsList { get; set; }

		[XmlElement]
		[DataMember(EmitDefaultValue = false)]
		public Success Success { get; set; }
		
		#region Common
        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string EchoToken { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Target { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Version { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string PrimaryLangID { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public ErrorList Errors { get; set; }
        #endregion
    }
}
