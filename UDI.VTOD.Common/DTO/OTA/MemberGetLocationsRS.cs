﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class MemberGetLocationsRS : Fault
    {
		//[XmlElement]
		//[DataMember(EmitDefaultValue = false)]
		//public bool HasRecords { get; set; }
        /// <summary>
        /// This is the Response Object of Retreving the Locations of the Member.
        /// It returns the location details like Address,Phone Number etc.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
		public List<MemberLocation> MemberLocations { get; set; }

		[XmlElement]
		[DataMember(EmitDefaultValue = false)]
		public Success Success { get; set; }
		#region Common
        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string EchoToken { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Target { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Version { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string PrimaryLangID { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public ErrorList Errors { get; set; }
        #endregion
    }
}
