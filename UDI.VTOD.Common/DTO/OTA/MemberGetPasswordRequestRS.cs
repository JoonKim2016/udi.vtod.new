﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class MemberGetPasswordRequestRS : Fault
    {       
        /*[DataMember(EmitDefaultValue = false)]
        public MemberPasswordRecord Result { get; set; } */
        /// <summary>
        /// This is Response Object Of Validating the Password Request.In case of 
        /// success it returns Success Object.
        /// Otherwise it returns an Error Object.
        /// </summary>
		[XmlElement]
		[DataMember(EmitDefaultValue = false)]
		public Success Success { get; set; }

		[XmlElement]
		[DataMember(EmitDefaultValue = false)]
		public string EmailAddress { get; set; }

		[XmlElement]
		[DataMember(EmitDefaultValue = false)]
		public string FirstName { get; set; }

		[XmlElement]
		[DataMember(EmitDefaultValue = false)]
		public string LastName { get; set; }

		[XmlElement]
		[DataMember(EmitDefaultValue = false)]
		public int MemberID { get; set; }

		[XmlElement]
		[DataMember(EmitDefaultValue = false)]
		public Guid RequestGuid { get; set; }

		[XmlElement]
		[DataMember(EmitDefaultValue = false)]
		public string RequestResult { get; set; }

		[XmlElement]
		[DataMember(EmitDefaultValue = false)]
		public string RequestStatus { get; set; }

		[XmlElement]
		[DataMember(EmitDefaultValue = false)]
		public string SourceWebSite { get; set; }

        #region Common
        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string EchoToken { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Target { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Version { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string PrimaryLangID { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public ErrorList Errors { get; set; }
        #endregion
    }
}
