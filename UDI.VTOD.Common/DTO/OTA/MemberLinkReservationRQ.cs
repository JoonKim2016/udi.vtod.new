﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class MemberLinkReservationRQ
    {
        /// <summary>
        /// This Request Object is used to link VTOD Api to SDS.
        /// This Request Objects contains the Member ID of VTOD and RezID of SDS.
        /// </summary>
		[XmlElement]
		[DataMember(EmitDefaultValue = false)]
		public int MemberId { get; set; }

		[XmlElement]
		[DataMember(EmitDefaultValue = false)]
		public int RezID { get; set; }

		#region Common
        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string EchoToken { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Target { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Version { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string PrimaryLangID { get; set; }
        #endregion
    }
}
