﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{

    [DataContract]
    public class MemberLocation
    {
		[DataMember(EmitDefaultValue = false)]
		public Address Address { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Telephone Telephone { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string Comment { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string MemberID { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string LocationID { get; set; }
	}
}
