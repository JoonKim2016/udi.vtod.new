﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class MemberLoginRQ
    {
        /// <summary>
        /// This is the Request Object to validate the User login details with the SDS.
        /// The Request Object contains Email Address/Password for validation.
        /// </summary>
        [XmlElement]
        [DataMember(EmitDefaultValue = false)]
        public string EmailAddress { get; set; }

        [XmlElement]
        [DataMember(EmitDefaultValue = false)]
        public string Password { get; set; }

        

        #region Common
        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string EchoToken { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Target { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Version { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string PrimaryLangID { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Source { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Device { get; set; }
        #endregion
    }
}
