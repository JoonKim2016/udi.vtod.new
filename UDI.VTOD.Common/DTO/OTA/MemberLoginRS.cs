﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class MemberLoginRS : Fault
    {
        /// <summary>
        /// This is the Response Object of the Member Login.
        /// If the login is successful a Success Object is returned with the Membership Records like Email Address/Name/Phone number.
        /// In case of error an Error Object is returned.
        /// </summary>
        [XmlElement]
        [DataMember(EmitDefaultValue = false)]
		public Success Success { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public MembershipRecord MembershipRecord { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Corporates Corporates { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public Address Address { set; get; }
        
        #region Common
        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string EchoToken { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Target { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Version { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string PrimaryLangID { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public ErrorList Errors { get; set; }
        #endregion
    }
}
