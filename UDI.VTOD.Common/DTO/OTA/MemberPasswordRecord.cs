﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;
using UDI.VTOD.Common.DTO.Enum; 

namespace UDI.VTOD.Common.DTO.OTA
{

    [DataContract]
    public class MemberPasswordRecord
    {
        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string EmailAddress { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string FirstName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string LastName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public int MemberId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public Guid RequestGuid { get; set; }

        [DataMember(EmitDefaultValue = false)]        
        public PasswordRequestResultCodes RequestResult { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public PasswordRequestStatus RequestStatus { get; set; }

    }
}
