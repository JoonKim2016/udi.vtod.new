﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class MemberRequestEmailConfirmationRQ
	{
        /// <summary>
        ///  In case of changing the Email Address there is 2 factor authentication.
        ///  This is the Request Object by which user receives an Email to verify the Email Address.
        /// </summary>
        /// 
        [DataMember(EmitDefaultValue = false)]
        public Corporate Corporate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<CorporateCompanyRequirement> CorporateCompanyRequirements { get; set; }

		[XmlElement]
		[DataMember(EmitDefaultValue = false)]
		public string MemberID { get; set; }

		[XmlElement]
		[DataMember(EmitDefaultValue = false)]
		public string EmailAddress { get; set; }

		[XmlElement]
		[DataMember(EmitDefaultValue = false)]
		public UniqueID UniqueID { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Verification Verification { get; set; }

		#region Common
		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string EchoToken { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string Target { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string Version { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string PrimaryLangID { get; set; }
		#endregion
	}
}
