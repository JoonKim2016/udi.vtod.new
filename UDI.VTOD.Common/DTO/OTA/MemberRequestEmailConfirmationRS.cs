﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class MemberRequestEmailConfirmationRS : Fault
	{
        /// <summary>
        /// This is the Response Object Of the 2 factor authentication of Email Address.
        ///In case of success  a Success Object is returned.
        ///In case of error an Error Object is returned.
        /// </summary>
		[XmlElement]
		[DataMember(EmitDefaultValue = false)]
		public Success Success { get; set; }

		#region Common
		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string EchoToken { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string Target { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string Version { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string PrimaryLangID { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public ErrorList Errors { get; set; }
		#endregion
	}
}
