﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class MemberResetPasswordRQ
    {
        /// <summary>
        ///This is the Request Object to reset the passowrd.
        ///The request object contains the new passowrd.
        /// </summary>
		[XmlElement]
		[DataMember(EmitDefaultValue = false)]
		public Guid RequestGuid { get; set; }

		[XmlElement]
		[DataMember(EmitDefaultValue = false)]
		public int MemberID { get; set; }

		[XmlElement]
		[DataMember(EmitDefaultValue = false)]
		public string NewPassword { get; set; }

		#region Common
        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string EchoToken { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Target { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Version { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string PrimaryLangID { get; set; }
        #endregion
    }
}
