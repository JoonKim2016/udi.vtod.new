﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class MemberUpdateLocationRQ
    {
        /// <summary>
        /// This is the Request Object for updating the location details of the Member.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
		public MemberLocation MemberLocation { get; set; }

		//[XmlElement]
		//[DataMember(EmitDefaultValue = false)]
		//public int MemberId { get; set; }

        #region Common
        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string EchoToken { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Target { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Version { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string PrimaryLangID { get; set; }
        #endregion
    }
}
