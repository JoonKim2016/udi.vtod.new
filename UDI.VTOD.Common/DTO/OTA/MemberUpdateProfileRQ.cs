﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;


namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class MemberUpdateProfileRQ
    {
        /// <summary>
        /// This is the Request Object used for updating the user details.
        /// It contains the Membership Record details like Name,Phone Number,Email Address which member wants to update.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public MembershipRecord MembershipRecord { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public CorporateAccount CorporateAccount { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public TPA_Extensions TPA_Extensions { get; set; }
		
		#region Common
        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string EchoToken { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Target { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Version { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string PrimaryLangID { get; set; } 
        #endregion
    }
}
