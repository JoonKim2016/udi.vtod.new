﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{   

    [DataContract]
    public class MembershipRecord
    {
        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public bool? SubscribeToEmail { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Telephone Telephone { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string EmailAddress { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public Guid CustomerGuid { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string FirstName { get; set; }
               
        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string LastName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public int MemberID { get; set; }

        [DataMember(EmitDefaultValue = true)]
		[XmlElement]
		public decimal DefaultGratuityPercentage { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlElement]
		public bool? SubscribeToSms { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlElement]
		public int HonorificID { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlElement]
		public string BirthDate { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlElement]
		public string HomeAirport { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlElement]
		public bool? IsPhoneVerified { get; set; }

       
        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public bool? DisabilityVehicleInd { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public bool? IsConciergeChatEnabled { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public bool? IsBOBO { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public int? BOBOMemberID { get; set; }
    }
}
