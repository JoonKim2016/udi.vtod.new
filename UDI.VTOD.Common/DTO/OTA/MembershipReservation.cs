﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{   

    [DataContract]
    public class MembershipReservation
    {
        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        //public string DispatchConfirmation { get; set; }
		public List<Confirmation> Confirmation { get; set; }

		//[DataMember(EmitDefaultValue = false)]
		//[XmlElement]
		//public long VTODConfirmation { get; set; }
        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string DropOffStreetNumber { get; set; }
        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string DropOffStreetName { get; set; }
        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string DropOffCity { get; set; }
        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string DropOffState { get; set; }
        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string DropOffCountry { get; set; }
        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string DropOffZipCode { get; set; }
        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string DropOffAddressType { get; set; }
        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string DropOffLocation { get; set; }
        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string DropOffAddress { get; set; }
        [DataMember(EmitDefaultValue = false)]

        [XmlElement]
        public string DropoffUnitNumber  { get; set; }
        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string LastName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string FirstName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string PhoneNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public bool PickMeUpNow { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string PickupStreetNumber { get; set; }
        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string PickupStreetName { get; set; }
        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string PickupCity { get; set; }
        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string PickupState { get; set; }
        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string PickupCountry { get; set; }
        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string PickupZipCode { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string PickupAddressType { get; set; }
		[DataMember(EmitDefaultValue = false)]
		[XmlElement]
		public string PickupLocation { get; set; }
        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string PickupAddress  { get; set; }
        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string PickupUnitNumber  { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlElement]
		public string PickupLatitude { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlElement]
		public string PickupLongitude { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string DropoffLatitude { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string DropoffLongitude { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string PickupTime { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlElement]
		public string PostalCode { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlElement]
		public string WebServiceState { get; set; }

		//[DataMember(EmitDefaultValue = false)]
		//[XmlElement]
		//public int RezId { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Contacts Contacts { get; set; }
		
		[DataMember(EmitDefaultValue = false)]
		//[XmlElement("FareItem")]
		public FareItems FareItems { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public PaymentInfo PaymentInfo { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public TripInfo TripInfo { get; set; }
		
		[DataMember(EmitDefaultValue = false)]
		[XmlElement]
		public string StatusCode { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string Account { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlElement]
		public string CorporateName { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlElement]
		public string SpecialInstructions { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlElement]
		public RateQualifier RateQualifier { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public List<RequirementDetail> CorporateRequirements { get; set; }
         [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string DropOffAirport_pickup_point { get; set; }
        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string DropOffAirportCode{ get; set; }
         [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string DropOffAirline { get; set; }
         [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public string DropOffAirlineFlightNumber { get; set; }
         [DataMember(EmitDefaultValue = false)]
        [XmlElement]
         public string DropOffFlightDateTime { get; set; }
         [DataMember(EmitDefaultValue = false)]
         [XmlElement]
         public string PickupAirport_pickup_point { get; set; }
         [DataMember(EmitDefaultValue = false)]
         [XmlElement]
         public string PickupAirportCode { get; set; }
         [DataMember(EmitDefaultValue = false)]
         [XmlElement]
         public string PickupAirline { get; set; }
         [DataMember(EmitDefaultValue = false)]
         [XmlElement]
         public string PickupAirlineFlightNumber { get; set; }
         [DataMember(EmitDefaultValue = false)]
         [XmlElement]
         public string PickupFlightDateTime { get; set; }
         [DataMember(EmitDefaultValue = false)]
         [XmlElement]
         public string Remarks { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public bool? IsModify { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public SplitPaymentInfo SplitPayment { get; set; }

    }
}
