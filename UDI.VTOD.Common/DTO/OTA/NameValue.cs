﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class NameValue
	{
		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string Name { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string Value { get; set; }
	}
}
