﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.OTA
{
	public class OTA_Exception : Exception
	{
		public List<Error> Errors { get; set; }
	}
}
