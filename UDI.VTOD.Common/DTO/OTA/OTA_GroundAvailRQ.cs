﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	//[XmlRoot(Namespace = "http://www.opentravel.org/OTA/2003/05")]
	public class OTA_GroundAvailRQ
	{
        /// <summary>
        /// This is a Request Object which contains the Passenger details,Drop Off/Pick up location and also the vehicle information.
        /// This object is used to do a book ASAP.
        /// </summary>
		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
         /// <summary>
        /// Generates a Token</summary>
		public string EchoToken { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
        /// <summary>
        /// It stores the environment like Test/Production</summary>
		public string Target { get; set; }

        /// <summary>
        /// It stores the version</summary>
		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string Version { get; set; }

        /// <summary>
        /// It stores the language like "en" for English</summary>
		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string PrimaryLangID { get; set; }

        /// <summary>
        /// It stores the Currency and Country Name.</summary>
		[DataMember(EmitDefaultValue = false)]
		public POS POS { get; set; }

        /// <summary>
        /// It stores the Passenger details like Passenger Count,Passenger Name,Category e.g. Adult/Child etc.</summary>
		[XmlElement("Passengers")]
		[DataMember(EmitDefaultValue = false)]
		public List<Passengers> Passengers { get; set; }

        /// <summary>
        /// It stores the Passenger details like Passenger Count,Passenger Name,Category e.g. Adult/Child etc.</summary>
		[DataMember(EmitDefaultValue = false)]
		public ServiceType ServiceType { get; set; }

        /// <summary>
        /// It stores the Passenger Preferences details like Maximum Passengers Allowed.</summary>
		[DataMember(EmitDefaultValue = false)]
		public PassengerPrefs PassengerPrefs { get; set; }

        /// <summary>
        /// It stores the the Vehicle Information and Special Inputs like Gratuity etc.</summary>
		[XmlElement("RateQualifier")]
		[DataMember(EmitDefaultValue = false)]
		public List<RateQualifier> RateQualifiers { get; set; }

        /// <summary>
        /// It stores the the Vehicle which has been preferred by the user.</summary>
		[XmlElement("VehiclePrefs")]
		[DataMember(EmitDefaultValue = false)]
		public List<VehiclePrefs> VehiclePrefs { get; set; }

        /// <summary>
        /// It stores the the Disability Info of the user.</summary>
		[DataMember(EmitDefaultValue = false)]
		public DisabilityInfo DisabilityInfo { get; set; }

        /// <summary>
        /// It stores the the MaxPassengers, Pickup Time List, PickupLocation, Confirmation details and Driver Info</summary>
		[DataMember(EmitDefaultValue = false)]
		public TPA_Extensions TPA_Extensions { get; set; }

        /// <summary>
        /// It stores the the Pick Up Address,Dropoff Address and Airline iNformation etc.</summary>
		[XmlElement("Reference")]
		[DataMember(EmitDefaultValue = false)]
		public List<Reference> References { get; set; }
		
		[DataMember(EmitDefaultValue = false)]
		public Service Service { get; set; }
	}
}
