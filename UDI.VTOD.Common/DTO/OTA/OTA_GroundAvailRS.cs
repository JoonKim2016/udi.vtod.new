﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class OTA_GroundAvailRS : Fault
	{
        /// <summary>
        /// This is a Response Object which contains the  ID of the booking.
        /// It returns us the list of Vehicle Information like driver id,driver name,vehicle type,vehicle number and vehicle status.
        /// </summary>
		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string EchoToken { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string Target { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string Version { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string PrimaryLangID { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Success Success { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public GroundServiceList GroundServices { get; set; }
		
		[XmlElement("RateQualifier")]
		[DataMember(EmitDefaultValue = false)]
		public List<RateQualifier> RateQualifiers { get; set; }
		
		[DataMember(EmitDefaultValue = false)]
		public TPA_Extensions TPA_Extensions { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public ErrorList Errors { get; set; }
	}
}
