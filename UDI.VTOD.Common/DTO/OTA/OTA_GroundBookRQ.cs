﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class OTA_GroundBookRQ
	{
        /// <summary>
        ///The request object contains ground reservation(e.g. name,phone number,email address of the passengers),
        ///location details,passenger count and the vehicle information like taxi/ecar etc.
        /// </summary>
		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string EchoToken { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string Target { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string Version { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string PrimaryLangID { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public POS POS { get; set; }

		[XmlElement("Reference")]
		[DataMember(EmitDefaultValue = false)]
		public List<Reference> References { get; set; }

		[XmlElement("GroundReservation")]
		[DataMember(EmitDefaultValue = false)]
		public List<GroundReservation> GroundReservations { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public PaymentList Payments { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public TPA_Extensions TPA_Extensions { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Username { get; set; }
        
    }
}
