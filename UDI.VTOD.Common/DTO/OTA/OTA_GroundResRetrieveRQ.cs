﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class OTA_GroundResRetrieveRQ
	{
        /// <summary>
        /// This is a Request object which contains  the trip id and reservation criteria of the request.
        /// </summary>
		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string EchoToken { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string Target { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string Version { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string PrimaryLangID { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public POS POS { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public ReservationSearchCriteria ReservationSearchCriteria { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlElement("Reference")]
		public List<Reference> Reference { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public TPA_Extensions TPA_Extensions { get; set; }
	}
}
