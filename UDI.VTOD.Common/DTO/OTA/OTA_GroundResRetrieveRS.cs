﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class OTA_GroundResRetrieveRS : Fault
	{
        /// <summary>
        /// This object contains the booking driver details,vehicle information and the contact details of the passengers.
        /// </summary>
		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string EchoToken { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string Target { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string Version { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string PrimaryLangID { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Success Success { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public List<SummaryReservation> SummaryReservation { get; set; }
		
		[DataMember(EmitDefaultValue = false)]
		public TPA_Extensions TPA_Extensions { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public ErrorList Errors { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlElement("Reference")]
        public List<Reference> Reference { get; set; }
    }
}
