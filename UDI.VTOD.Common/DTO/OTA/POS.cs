﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class POS
	{
		[DataMember(EmitDefaultValue = false)]
		public Source Source { get; set; }
	}
}
