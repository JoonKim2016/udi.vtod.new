﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class PassengerPrefs
	{
		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public int MaximumPassengers { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public bool MeetAndGreetInd { get; set; }
	}
}
