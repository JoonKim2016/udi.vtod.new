﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class Passengers
	{
		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public int Quantity { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Category Category { get; set; }
	}
}
