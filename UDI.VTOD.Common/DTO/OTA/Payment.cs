﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class Payment
	{
		[DataMember(EmitDefaultValue = false)]
		public PaymentCard PaymentCard { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public DirectBill DirectBill { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Cash Cash { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public MiscChargeOrder MiscChargeOrder { get; set; }
	}
}
