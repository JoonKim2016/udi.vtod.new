﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class PaymentCard
	{
		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string ExpireDate { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public CardType CardType { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string CardHolderName { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public CardNumber_SeriesCode CardNumber { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public CardNumber_SeriesCode SeriesCode { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Address Address { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Nickname { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public TPA_Extensions TPA_Extensions { get; set; }
	}
}
