﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

	
namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class PaymentInfo
	{
		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string Type { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlElement]
		public PaymentCard PaymentCard { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlElement]
		public DirectBill DirectBill { get; set; }

	}
}
