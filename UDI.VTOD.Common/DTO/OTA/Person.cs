﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class Person
	{
		[DataMember(EmitDefaultValue = false)]
		public PersonName PersonName { get; set; }

		[XmlElement("Telephone")]
		[DataMember(EmitDefaultValue = false)]
		public List<Telephone> Telephones { get; set; }

		[XmlElement("Email")]
		[DataMember(EmitDefaultValue = false)]
		public List<Email> Emails { get; set; }

		[XmlElement("Address")]
		[DataMember(EmitDefaultValue = false)]
		public List<Address> Addresses { get; set; }
	}
}
