﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class PersonName
	{
		[DataMember(EmitDefaultValue = false)]
		public string GivenName { get; set; }
		[DataMember(EmitDefaultValue = false)]
		public string Surname { get; set; }
	}
}
