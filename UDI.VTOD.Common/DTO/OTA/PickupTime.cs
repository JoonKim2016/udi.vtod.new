﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;


namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class PickupTime
	{
		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string StartDateTime { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string EndDateTime { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string Status { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string Description { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public int AdvanceNoticeMinutes { get; set; }

        [DataMember(EmitDefaultValue = false)]
       
        public bool? DisplayEndTime { get; set; }
    }
}
