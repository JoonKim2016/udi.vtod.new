﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class PickupTimeList
	{
		[DataMember(EmitDefaultValue = false)]
		[XmlElement("PickupTime")]
		public List<PickupTime> PickupTimes { get; set; }
	}
}
