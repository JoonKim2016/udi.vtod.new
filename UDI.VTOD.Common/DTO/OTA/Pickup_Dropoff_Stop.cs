﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class Pickup_Dropoff_Stop
	{
		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string DateTime { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Address Address { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public AirportInfo AirportInfo { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Airline Airline { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string Remark { get; set; }

       
	}
}
