﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class RateGetRQ
	{
        /// <summary>
        /// This is a Request Object which retrieves a rate based on the  Member ID,Trip ID etc.
        /// </summary>
		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string EchoToken { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string Target { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string Version { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string PrimaryLangID { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public POS POS { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlElement("Reference")]
		public List<Reference> Reference { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public int MemberId { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public TPA_Extensions TPA_Extensions { get; set; }
	}
}
