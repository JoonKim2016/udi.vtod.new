﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class RateQualifier
	{
		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string AccountID { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string PromotionCode { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute("RateQualifier")]
		public string RateQualifierValue { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Category Category { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlElement("SpecialInputs")]
        public List<NameValue> SpecialInputs { get; set; }
        //public List<KeyValuePair<string, string>> SpecialInputs { get; set; }
    }
}
