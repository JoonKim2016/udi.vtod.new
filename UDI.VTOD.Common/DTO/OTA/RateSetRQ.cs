﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	//Just for test
	[DataContract]
	public class RateSetRQ
	{
        /// <summary>
        /// This is the Request Object which is used to set the Rate.
        /// It Contains the Member ID,Trip ID,Rate etc. to set the Rate.
        /// </summary>
		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string EchoToken { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string Target { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string Version { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string PrimaryLangID { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public POS POS { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlElement("Reference")]
		public List<Reference> Reference { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public int MemberId { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public int Rate { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string Type { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string Comment { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public List<NameValue> Reasons { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public TPA_Extensions TPA_Extensions { get; set; }
	}
}
