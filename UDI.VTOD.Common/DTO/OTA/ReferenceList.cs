﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class ReferenceList
	{
		[DataMember(EmitDefaultValue = false)]
		[XmlElement("Reference")]
		public List<Reference> References { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlElement("LinkedReference")]
		public List<LinkedReference> LinkedReferences { get; set; }
	}
}
