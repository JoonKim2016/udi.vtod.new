﻿using System.Runtime.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    [DataContract]
    public class RequirementDetail
    {
		[DataMember(EmitDefaultValue = false)]
		public string RequirementName { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string RequirementValue { get; set; }

		[DataMember(EmitDefaultValue = false)]
        public bool? IsRequired { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool? ProvideHints { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool? ShouldUseTypeaheadUri { get; set; }

		//[DataMember(EmitDefaultValue = false)]
		//public string TypeaheadBaseUri { get; set; }
    }
}
