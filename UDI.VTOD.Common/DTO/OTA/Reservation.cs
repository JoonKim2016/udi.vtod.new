﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class Reservation
	{
		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string CancelType { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Confirmation Confirmation { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public CancelConfirmation CancelConfirmation { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Passenger Passenger { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Service Service { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlElement("UniqueID")]
		public List<UniqueID> UniqueID { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Verification Verification { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public TPA_Extensions TPA_Extensions { get; set; }
	}
}
