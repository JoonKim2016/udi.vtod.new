﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class ReservationSearchCriteria
	{
		[XmlElement("Passenger")]
		[DataMember(EmitDefaultValue = false)]
		public List<Passenger> Passengers { get; set; }
	}
}
