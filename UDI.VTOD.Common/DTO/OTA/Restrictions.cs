﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class Restrictions
	{
		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public bool AdvancedBookingInd { get; set; }
		[DataMember(EmitDefaultValue = false)]
		public AdvanceBooking AdvanceBooking { get; set; }
	}
}
