﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
    public class RewardCreateMemberAccountRQ : Fault
	{
        /// <summary>
        /// This is a Request Object which contains the Member ID,Reward ID and the account number to create a member.
        /// </summary>
		[DataMember(EmitDefaultValue = false)]
		public int MemberID { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string AccountNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int RewardID { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public bool IsDefault { get; set; }


        #region Common
        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string EchoToken { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Target { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Version { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string PrimaryLangID { get; set; }
        #endregion
	}
}
