﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class Service
	{
		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public bool DisabilityVehicleInd { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string Notes { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Pickup_Dropoff_Stop Pickup { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Pickup_Dropoff_Stop Dropoff { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public StopList Stops { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Service Location { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public ServiceType ServiceType { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public ServiceLevel ServiceLevel { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public VehicleType VehicleType { get; set; }
		
		//[DataMember(EmitDefaultValue = false)]
		//public Franchise Franchise { get; set; }

		#region Nullable
		public bool ShouldSerializeDisabilityVehicleInd()
		{
			return DisabilityVehicleInd;
		}
		#endregion
	}
}
