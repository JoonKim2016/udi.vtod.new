﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	public class ServiceLevel
	{
		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string Code { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string SourceName { get; set; }
	}
}
