﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class ServiceType
	{
		[XmlText]
		[DataMember(EmitDefaultValue = false)]
		public string Value { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public bool DisabilityVehicleInd { get; set; }
	}
}
