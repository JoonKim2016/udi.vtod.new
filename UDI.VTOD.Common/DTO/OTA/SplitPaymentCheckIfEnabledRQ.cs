﻿using System.Runtime.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    [DataContract]
    public class SplitPaymentCheckIfEnabledRQ : CommonRequest
    {
        [DataMember(EmitDefaultValue = false)]
        public long TripID { get; set; }
    }
}
