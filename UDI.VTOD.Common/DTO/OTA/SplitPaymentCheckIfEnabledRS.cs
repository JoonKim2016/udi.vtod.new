﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    [DataContract]
    public class SplitPaymentCheckIfEnabledRS : CommonResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public bool? IsEnabled { get; set; }
    }
}
