﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.OTA
{
    [DataContract]
    public class SplitPaymentFare
    {
        [DataMember(EmitDefaultValue = false)]
        public FareItems FareItems { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public PaymentInfo PaymentInfo { get; set; }
    }
}
