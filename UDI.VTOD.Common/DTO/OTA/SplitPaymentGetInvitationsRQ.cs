﻿using System.Runtime.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    [DataContract]
    public class SplitPaymentGetInvitationsRQ : CommonRequest
    {
        [DataMember(EmitDefaultValue = false)]
        public string MemberBID { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public long? TripID { get; set; }
    }
}
