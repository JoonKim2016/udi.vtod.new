﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    [DataContract]
    public class SplitPaymentGetInvitationsRS : CommonResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public List<SplitPaymentInvitation> Invitations { get; set; }
    }
}
