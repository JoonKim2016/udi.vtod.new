﻿using System;
using System.Runtime.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    [DataContract]
    public class SplitPaymentInfo
    {
        [DataMember(EmitDefaultValue = false)]
        public bool? IsSplitPayment { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string SplitPaymentStatus { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public SplitPaymentUserInfo MemberA { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public SplitPaymentUserInfo MemberB { get; set; }
    }
}
