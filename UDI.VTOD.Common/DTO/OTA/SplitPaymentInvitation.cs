﻿using System.Runtime.Serialization;
using UDI.VTOD.Common.DTO.Enum;

namespace UDI.VTOD.Common.DTO.OTA
{
    [DataContract]
    public class SplitPaymentInvitation
    {
        [DataMember(EmitDefaultValue = false)]
        public long TripID { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string MemberAID { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string MemberBID { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string SplitPaymentStatus { get; set; }
    }
}
