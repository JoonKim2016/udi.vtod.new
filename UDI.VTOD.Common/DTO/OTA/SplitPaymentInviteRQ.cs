﻿using System.Runtime.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    [DataContract]
    public class SplitPaymentInviteRQ : CommonRequest
    {
        [DataMember(EmitDefaultValue = false)]
        public SplitPaymentUserInfo MemberA { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public SplitPaymentUserInfo MemberB { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public long TripID { get; set; }
    }
}
