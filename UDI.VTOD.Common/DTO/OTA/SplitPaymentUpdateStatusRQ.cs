﻿using System.Runtime.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    [DataContract]
    public class SplitPaymentUpdateStatusRQ : CommonRequest
    {
        [DataMember(EmitDefaultValue = false)]
        public string MemberAID { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string MemberBID { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public long TripID { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool Status { get; set; }
    }
}
