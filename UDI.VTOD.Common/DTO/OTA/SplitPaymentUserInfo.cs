﻿using System.Runtime.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    [DataContract]
    public class SplitPaymentUserInfo
    {
        [DataMember(EmitDefaultValue = false)]
        public string MemberID { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string FirstName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string LastName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public Telephone TelephoneInfo { get; set; }
    }
}
