﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class StateProv
	{
		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string StateCode { get; set; }
	}
}
