﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class Status
	{
		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string DateTime { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string Description { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public decimal Fare { get; set; }

		[XmlText]
		[DataMember(EmitDefaultValue = false)]
		public string Value { get; set; }
	}
}
