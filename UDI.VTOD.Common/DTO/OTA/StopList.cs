﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class StopList
	{
		[DataMember(EmitDefaultValue = false)]
		[XmlElement("Stop")]
		public List<Pickup_Dropoff_Stop> Stops { get; set; }
	}
}
