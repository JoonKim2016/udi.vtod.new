﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class SummaryReservation
	{
		[DataMember(EmitDefaultValue = false)]
		public Confirmation Confirmation { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Service Service { get; set; }
	}
}
