﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;
///using UDI.VTOD.Domain.ECar.Aleph;
namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class 
        TPA_Extensions
	{
      
		[DataMember(EmitDefaultValue = false)]
		public string Source { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string Device { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ReferrerMemberID { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public decimal? ReferrerCreditAmount { get; set; }
 
		[DataMember(EmitDefaultValue = false)]
		public string SID { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public int MaxPassengers { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public int MaxAccessibleGuests { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? RideNowBehavior { get; set; } 

		[DataMember(EmitDefaultValue = false)]
		public PickupTimeList PickupTimes { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Pickup_Dropoff_Stop PickupLocation { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public Pickup_Dropoff_Stop DropoffLocation { get; set; }

		[XmlElement("Passengers")]
		[DataMember(EmitDefaultValue = false)]
		public List<Passengers> Passengers { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public AirlineReward AirlineReward { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Confirmation ReturnConfirmation { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public List<Confirmation> Confirmations { get; set; }
		//public Confirmation DispatchConfirmation { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public List<Geolocation> Geolocations { get; set; }
		//public Confirmation DispatchConfirmation { get; set; }

		//[DataMember(EmitDefaultValue = false)]
		//public Confirmation DispatchReturnConfirmation { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Person Driver { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Vehicles Vehicles { get; set; }

        //ToDo: This is related to end user membership. After Ola finish the membership part, it's better to use that object
        [DataMember(EmitDefaultValue = false)]
        public string MemberID { get; set; }

         [DataMember(EmitDefaultValue = false)]
        public string AirportPickupPoint { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CreditCardID { get; set; }

        //[DataMember(EmitDefaultValue = false)]
		//public Vehicle Vehicle { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Statuses Statuses { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Contacts Contacts { get; set; }

		[XmlElement("RateQualifier")]
		[DataMember(EmitDefaultValue = false)]
		public List<RateQualifier> RateQualifiers { get; set; }

		[XmlElement("References")]
		[DataMember(EmitDefaultValue = false)]
		public ReferenceList References { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public TripCharacteristics TripCharacteristics { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public bool? AddressValidationRequired { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public bool? PopulateGeolocation { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Zone Zone { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public bool? PickMeUpNow { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool? IsInboundRideNow  { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public bool? IsOutboundRideNow  { get; set; }


        [DataMember(EmitDefaultValue = false)]
		public bool? IsDefault { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public bool? IsVerified { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public bool? SubscribeToPromotions { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public ServiceCharges ServiceCharges { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string TripDirection { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string SubZipName { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public int? LuggageQty { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public PaymentList Payments { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string DiscountDescription { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Verification Verification { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Criteria Criteria { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string ImageURL { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public TripInfo TripInfo { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public bool PhoneVerificationRequired { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string RequestVehicleNumber { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public bool? AllowSMS { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string DeviceToken { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string DeviceVersion { get; set; }
        //[XmlArray("Values")]
        //[XmlArrayItem("KeyValuePair")]
        ////public List<AlephCancelParameter> WebServiceState { set; get; }
        ////public string WebServiceState { set; get; }
        //public List<KeyValuePair<string, string>> WebServiceState { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string WebServiceState { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string SharedLink { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public Confirmation Confirmation { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool? IsIncentive { get; set; }


        [DataMember(EmitDefaultValue = false)]
        public long FleetId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsPhoneVerifyByCode { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool? IsRideNowAllowed { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool? IsModify { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string SecondaryImageUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string LogoImageUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlAttribute]
        public int CommissionID { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlAttribute]
        public decimal Commission { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string TripIconFontCode { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public SplitPaymentInfo SplitPayment { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string AffiliateProviderId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<Confirmation> AffiliateProviders { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ReserveCsr { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string UserId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool? RemoveCallCenterBookingFee{ get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool? IsActive { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool? IsBlockedCard { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool? IsCompanyCard { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<NameValue> SpecialInputs { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public bool? IsBOBO { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlElement]
        public int? BOBOMemberID { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public byte ChildSeats { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public byte InfantSeats { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CityCode { get; set; }


        #region ShouldSerialize
        public bool ShouldSerializeAddressValidationRequired()
		{
			return AddressValidationRequired.HasValue;
		}
		public bool ShouldSerializeMaxPassengers()
		{
			return MaxPassengers > 0;
		}
		public bool ShouldSerializeMaxAccessibleGuests()
		{
			return MaxPassengers > 0;
		}
		public bool ShouldSerializePickMeUpNow()
		{
			return PickMeUpNow.HasValue;
		}
		public bool ShouldSerializeIsDefault()
		{
			return IsDefault.HasValue;
		}
		public bool ShouldSerializeIsVerified()
		{
			return IsVerified.HasValue;
		}
        public bool ShouldSerializeIsBlockedCard()
        {
            return IsBlockedCard.HasValue;
        }
        public bool ShouldSerializeIsActive()
        {
            return IsActive.HasValue;
        }
        public bool ShouldSerializeIsCompanyCard()
        {
            return IsCompanyCard.HasValue;
        }        

        [Serializable]
        public class KeyValuePair<K, V>
        {
            public K Key { get; set; }
            public V Value { get; set; }
            public KeyValuePair() { }
            public KeyValuePair(K key, V value)
            {
                this.Key = key;
                this.Value = value;
            }
        }
		//public bool ShouldSerializeVehicles()
		//{
		//	return (Vehicles != null && Vehicles.Items != null && Vehicles.Items.Any());
		//}
		#endregion
	}
}
