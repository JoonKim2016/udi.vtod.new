﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class Telephone
	{
		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string CountryAccessCode { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string AreaCityCode { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string PhoneNumber { get; set; }
	}
}
