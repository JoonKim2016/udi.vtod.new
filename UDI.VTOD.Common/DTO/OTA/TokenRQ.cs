﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    /// <summary>
    /// This is a Token Request Object where User sends the IP Address,Username,Password
    /// </summary>
	[DataContract]
	public class TokenRQ
	{
		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string FleetType { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public int? ClientType { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string ClientIPAddress { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string Username { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string Password { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string EchoToken { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string Target { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string Version { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string PrimaryLangID { get; set; }

		#region ShouldSerialize
		public bool ShouldSerializeClientType()
		{
			return ClientType.HasValue;
		}
		#endregion

	}
}
