﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using UDI.VTOD.Common.DTO.OTA;

namespace UDI.VTOD.Common.DTO.OTA
{
    /// <summary>
    ///This is the Response Object of the validation of the Token.
    ///When User Validation is successful  Success is returned.Otherwise Error object is returned.
    /// </summary>
	[DataContract]
	public class TokenRS : Fault
	{
      
		[DataMember(EmitDefaultValue = false)]
		public int? ClientType { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string ClientIPAddress { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string Username { get; set; }

		/// <summary>
		/// For taxi, SecurityKey is the exactly the password. Just I preferd to use SecurityKey instead of password as it seems safer for end user! ;)
		/// </summary>
		[DataMember(EmitDefaultValue = false)]
		public string SecurityKey { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public ErrorList Errors { get; set; }

		[XmlElement]
		[DataMember(EmitDefaultValue = false)]
		public Success Success { get; set; }

		[XmlElement]
		[DataMember(EmitDefaultValue = false)]
		public DateTime CreateDateTimeUTC { get; set; }

		#region Nullable
		public bool ShouldSerializeClientType()
		{
			return ClientType.HasValue;
		}

		public bool ShouldSerializeSecurityKey()
		{
			return (SecurityKey != Guid.Empty.ToString() && !string.IsNullOrWhiteSpace(SecurityKey));
		}
		#endregion
	}
}
