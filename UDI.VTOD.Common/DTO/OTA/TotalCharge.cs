﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class TotalCharge
	{
		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public decimal RateTotalAmount { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public decimal EstimatedTotalAmount { get; set; }

		#region Nullable
		public bool ShouldSerializeEstimatedTotalAmount()
		{
			return EstimatedTotalAmount > 0;
		}
		#endregion

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public decimal MaxEstimatedRate { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public decimal MinEstimatedRate { get; set; }

	}
}
