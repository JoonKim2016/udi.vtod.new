﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class UniqueID
	{
		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string Type { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string ID { get; set; }

		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string ID_Context { get; set; }
	}
}
