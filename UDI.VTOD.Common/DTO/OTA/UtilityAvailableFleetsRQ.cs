﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class UtilityAvailableFleetsRQ : Fault
	{
        /// <summary>
        /// This is a Request Object which helps to retrieve the Fleet.
        /// Address,City,State and Zip Code are sent with the request object to retrieve the Fleet Details.
        /// </summary>
		[DataMember(EmitDefaultValue = false)]
		public Address Address { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Corporate Corporate { get; set; }
		
		#region Common
        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string EchoToken { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Target { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Version { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string PrimaryLangID { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public int MemberID { get; set; }
        #endregion
	}
}
