﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;


namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class UtilityGetAirlinesByAirportRQ
	{
        /// <summary>
        /// This is a Request Object which contains Airport Code.
        /// With this Airport Code; the list of flights available are found.
        /// </summary>
        [XmlElement]
        [DataMember(EmitDefaultValue = false)]
		public string AirportCode { get; set; }
		
		#region Common
		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string EchoToken { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string Target { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string Version { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string PrimaryLangID { get; set; }
		#endregion
	}
}
