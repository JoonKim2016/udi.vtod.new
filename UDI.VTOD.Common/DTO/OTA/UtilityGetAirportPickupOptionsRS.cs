﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class UtilityGetAirportPickupOptionsRS : Fault
	{
        /// <summary>
        /// This is the Response Object Of the  list of Destinations available for a Trip.
        /// It returns the list of All the Airports with City/Country Name etc.
        /// </summary>
		[XmlElement]
		[DataMember(EmitDefaultValue = false)]
		public Success Success { get; set; }

		[DataMember(EmitDefaultValue = false)]
		//[XmlElement("FareItem")]
		public AirportPickupOptions AirportPickupOptions { get; set; }

		#region Common
		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string EchoToken { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string Target { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string Version { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string PrimaryLangID { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public ErrorList Errors { get; set; }
		#endregion
	}
}
