﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    public class UtilityGetBlackoutPeriodRS:Fault
    {
        [DataMember(EmitDefaultValue = false)]
        public List<BlackoutPeriod> BlackoutPeriods { get; set; }

        [XmlElement]
        [DataMember(EmitDefaultValue = false)]
        public Success Success { get; set; }

        #region Common
        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string EchoToken { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Target { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Version { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string PrimaryLangID { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public ErrorList Errors { get; set; }
        #endregion

    }

    public class BlackoutPeriod 
    {
        [DataMember(EmitDefaultValue = false)]
        public string CityName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Reason { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string StartTime  { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string EndTime { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Message { get; set; }
    
    }
}
