﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;


namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class UtilityGetDefaultFleetTypeForPointRQ
	{
        /// <summary>
        /// This is a Request Object which returns a Fleet Type.
        /// Longitude and Latitude is passed with the Request Object.
        /// </summary>
		[XmlElement]
		[DataMember(EmitDefaultValue = false)]
		public decimal Longitude { get; set; }

		[XmlElement]
		[DataMember(EmitDefaultValue = false)]
		public decimal Latitude { get; set; }

		#region Common
		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string EchoToken { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string Target { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string Version { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string PrimaryLangID { get; set; }
		#endregion
	}
}
