﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class UtilityGetDefaultFleetTypeForPointRS : Fault
    {
        /// <summary>
        /// This is the Response Object to return the Fleet Type.
        /// In case of success a Fleet Type is returned.
        /// Otherwise an Error Object is returned.
        /// </summary>
        [XmlElement]
        [DataMember(EmitDefaultValue = false)]
        public int? FleetType { get; set; }

		[XmlElement]
		[DataMember(EmitDefaultValue = false)]
		public Success Success { get; set; }
		
		#region Common
        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string EchoToken { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Target { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Version { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string PrimaryLangID { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public ErrorList Errors { get; set; }
        #endregion

		#region ShouldSerialize
		public bool ShouldSerializeFleetType()
		{
			return FleetType.HasValue;
		}
		#endregion

    }
}
