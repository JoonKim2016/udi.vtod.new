﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class UtilityGetLocationListRS : Fault
	{
        /// <summary>
        /// This is the Response Object which retrieves the list of Locations.
        /// In case of success the list of Locations is returned.
        /// In case of error an Error Object is returned.
        /// </summary>
		[DataMember(EmitDefaultValue = false)]
		public List<Location> Locations { get; set; }

		[XmlElement]
		[DataMember(EmitDefaultValue = false)]
		public Success Success { get; set; }

		#region Common
		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string EchoToken { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string Target { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string Version { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string PrimaryLangID { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public ErrorList Errors { get; set; }
		#endregion
	}
}
