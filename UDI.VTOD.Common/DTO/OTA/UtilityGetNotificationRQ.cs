﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
     [DataContract]
    public class UtilityGetNotificationRQ : Fault
     {

         [DataMember(EmitDefaultValue = false)]
         public int minutesDispatching { get; set; }

         [DataMember(EmitDefaultValue = false)]
         public long FleetId { get; set; }

         [DataMember(EmitDefaultValue = false)]
         public int MinutesOld { get; set; }

         [XmlElement("RateQualifier")]
         [DataMember(EmitDefaultValue = false)]
         public List<RateQualifier> RateQualifiers { get; set; }

         #region Common
         [XmlAttribute]
         [DataMember(EmitDefaultValue = false)]
         public string EchoToken { get; set; }

         [XmlAttribute]
         [DataMember(EmitDefaultValue = false)]
         public string Target { get; set; }

         [XmlAttribute]
         [DataMember(EmitDefaultValue = false)]
         public string Version { get; set; }

         [XmlAttribute]
         [DataMember(EmitDefaultValue = false)]
         public string PrimaryLangID { get; set; }
         #endregion

     }
}
