﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
    [DataContract]
    public class UtilityGetNotificationRS : Fault
    {

        [XmlElement]
        [DataMember(EmitDefaultValue = false)]
        public List<OutboundNotification> Notifications { get; set; }

        [XmlElement]
        [DataMember(EmitDefaultValue = false)]
        public Success Success { get; set; }

        #region Common
        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string EchoToken { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Target { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Version { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string PrimaryLangID { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public ErrorList Errors { get; set; }
        #endregion
    }


    [DataContract]
    public class OutboundNotification
    {
        [DataMember(EmitDefaultValue = false)]
        public string VehicleNumber { get; set; }
        
        [DataMember(EmitDefaultValue = false)]
        public string BookingTripId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string TripDateTime { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string PhoneNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CustomerName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int Type { get; set; }
    }

    
}
