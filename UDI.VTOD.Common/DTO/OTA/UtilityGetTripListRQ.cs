﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class UtilityGetTripListRQ : Fault
	{
        /// <summary>
        /// This is a Request Object which sends the contains the Status(e.g. Completed,Booked,Cancelled etc.) to retrieve the Trip details.
        /// </summary>
		[DataMember(EmitDefaultValue = false)]
		public Reference Reference { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public RateQualifier RateQualifier { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Status Status { get; set; }

		#region Common
        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string EchoToken { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Target { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Version { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string PrimaryLangID { get; set; }
        #endregion
	}
}
