﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class UtilityGetTripListRS : Fault
	{
        /// <summary>
        /// This is the Response Object which contains the list of the Trip Details like Person Name,Telephone number,address  based on the status of the request object.
        /// </summary>
		[XmlElement("Reservation")]
		[DataMember(EmitDefaultValue = false)]
		public List<Reservation> Reservations { get; set; }

		[XmlElement]
		[DataMember(EmitDefaultValue = false)]
		public Success Success { get; set; }

		#region Common
        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string EchoToken { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Target { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string Version { get; set; }

        [XmlAttribute]
        [DataMember(EmitDefaultValue = false)]
        public string PrimaryLangID { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public ErrorList Errors { get; set; }
        #endregion
	}
}
