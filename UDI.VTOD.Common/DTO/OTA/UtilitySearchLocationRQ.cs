﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;


namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class UtilitySearchLocationRQ
	{
        /// <summary>
        /// This is a Request Object to Search a Location based on the Location ID.
        /// The Location ID is passed with the Request Object.
        /// </summary>
        [XmlElement]
        [DataMember(EmitDefaultValue = false)]
        public int LocationId { get; set; }

        [XmlElement]
        [DataMember(EmitDefaultValue = false)]
        public string SearchExpression { get; set; }
		
		#region Common
		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string EchoToken { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string Target { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string Version { get; set; }

		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string PrimaryLangID { get; set; }
		#endregion
	}
}
