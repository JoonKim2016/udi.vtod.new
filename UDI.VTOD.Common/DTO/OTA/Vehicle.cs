﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class Vehicle
	{
		[XmlAttribute]
		[DataMember(EmitDefaultValue = false)]
		public string ID { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Geolocation Geolocation { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Geolocation PriorGeolocation { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public decimal Course { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string DriverID { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string DriverName { get; set; }

		//[DataMember(EmitDefaultValue = false)]
		//public string VehicleNo { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string VehicleStatus { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string VehicleType { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public string FleetID { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public decimal? MinutesAway { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public decimal? MinutesAwayWithTraffic { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public decimal? Distance { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<Geolocation> Geolocations { get; set; }
	}
}
