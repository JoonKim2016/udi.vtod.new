﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class VehiclePrefs
	{
		[DataMember(EmitDefaultValue = false)]
		public VehicleType Type { get; set; }
    }
}
