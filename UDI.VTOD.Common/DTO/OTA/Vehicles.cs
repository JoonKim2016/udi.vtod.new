﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class Vehicles
	{
		[DataMember(EmitDefaultValue = false)]
		[XmlElement("Vehicle")]
		public List<Vehicle> Items { get; set; }

		//ToDo: It should be int, but because of json serilizaion problem I converted it to string
		// if it would be int, serialication remover all zeros
		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string NumberOfVehicles { get; set; }

		//ToDo: It should be decimal, but because of json serilizaion problem I converted it to string
		// if it would be decilal, serialication remover all zeros
		[DataMember(EmitDefaultValue = false)]
		[XmlAttribute]
		public string NearestVehicleETA { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlAttribute]
        public string AverageVehicleETA { get; set; }

	}
}
