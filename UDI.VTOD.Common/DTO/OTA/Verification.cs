﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class Verification
	{
		[DataMember(EmitDefaultValue = false)]
		public PersonName PersonName { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Telephone TelephoneInfo { get; set; }

		[DataMember(EmitDefaultValue = false)]
		public Address AddressInfo { get; set; }
	}
}
