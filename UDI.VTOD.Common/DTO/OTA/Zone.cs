﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO.OTA
{
	[DataContract]
	public class Zone
	{
		[DataMember(EmitDefaultValue = false)]
		public Circle Circle { get; set; }
	}
}
