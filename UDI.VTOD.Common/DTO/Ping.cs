﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Common.DTO
{
	public class Ping
	{
		public DateTime Time { get; set; }
		public string Version { get; set; }
		//[XmlIgnore]
		[IgnoreDataMember]
		public string Internal { get; set; }
	}
}
