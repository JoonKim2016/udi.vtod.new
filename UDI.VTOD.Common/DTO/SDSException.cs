﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO
{
    /// <summary>
    /// This is the object which handles the exception of SDS.
    /// </summary>
	public class SDSException : Exception
	{
        /// <summary>
        /// This is the constructor of the SDS Exception class.
        /// </summary>
        /// <param name="message"></param>
		public SDSException(string message)
			: base(message)
		{
		}
	}
}
