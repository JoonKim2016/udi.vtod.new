﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Common.DTO.OTA;

namespace UDI.VTOD.Common.DTO.Static
{
	public static class Token
	{
		public static TokenRQ TokenRQ { get; set; }
		public static TokenRS TokenRS { get; set; }
		public static DateTime DateTime { get; set; }
	}
}
