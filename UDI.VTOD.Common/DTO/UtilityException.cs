﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO
{
    /// <summary>
    /// This is the object which handles all the exceptions of all the Utility methods.
    /// </summary>
	public class UtilityException : Exception
	{
        /// <summary>
        /// This is the constructor of the UtilityException class.
        /// </summary>
        /// <param name="message"></param>
		public UtilityException(string message)
			: base(message)
		{
		}
	}
}
