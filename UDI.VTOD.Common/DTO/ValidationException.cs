﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO
{
    /// <summary>
    /// This is the Exception class which handles the validation of the inputs.
    /// </summary>
	public class ValidationException : Exception
	{
        /// <summary>
        /// This is the constructor of the this class.
        /// </summary>
        /// <param name="message"></param>
		public ValidationException(string message)
			: base(message)
		{
		}
	}
}
