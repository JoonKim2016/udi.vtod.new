﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.DTO
{
    /// <summary>
    /// This is the object which handles the exception of Taxi.
    /// </summary>
	public class VanException : Exception
	{
        /// <summary>
        /// This is the constructor of the Taxi Exception class.
        /// </summary>
        /// <param name="message"></param>
        public VanException(string message)
            : base(message)
		{

		}
	}
}
