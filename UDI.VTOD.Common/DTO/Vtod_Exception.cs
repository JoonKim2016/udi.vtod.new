﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Common.Helper;
using log4net;


namespace UDI.VTOD.Common.DTO
{
    /// <summary>
    /// This is the class which handles the exception of the VTOD API.
    /// </summary>
	public class VtodException : Exception
	{
		ExceptionMessage _exceptionMessage;
		Enum.ExceptionType _exceptionType;
        public ILog logger;

		private VtodException()
		{
            logger = LogManager.GetLogger(this.GetType());
            log4net.Config.XmlConfigurator.Configure();
		}

		public static VtodException CreateException(Enum.ExceptionType exceptionType, int code)
		{
			var message = MessageFactory.GetMessage(code);
			var ex = new VtodException { _exceptionType = exceptionType };
			ex._exceptionMessage = new ExceptionMessage(message.Code, message.Message);
			return ex;
		}

		public static VtodException CreateValidationException(string message)
		{
			var ex = new VtodException { _exceptionType = Enum.ExceptionType.Validation };
			ex._exceptionMessage = new ExceptionMessage(13001, message);
			return ex;
		}

		public static VtodException CreateFieldRequiredValidationException(string fieldName)
		{
			var message = MessageFactory.GetMessage(13002);
			var ex = new VtodException { _exceptionType = Enum.ExceptionType.Validation };
			ex._exceptionMessage = new ExceptionMessage(message.Code, string.Format(message.Message, fieldName));
			return ex;
		}

		public static VtodException CreateFieldFormatValidationException(string filedName, string fieldFormat)
		{
			var message = MessageFactory.GetMessage(13003);
			var ex = new VtodException { _exceptionType = Enum.ExceptionType.Validation };
			ex._exceptionMessage = new ExceptionMessage(message.Code, string.Format(message.Message, filedName, fieldFormat));
			return ex;
		}

		public static VtodException CreateBubbleUpException(Enum.ExceptionType exceptionType, string message)
		{
			var ex = new VtodException { _exceptionType = exceptionType };
			ex._exceptionMessage = new ExceptionMessage(-1, message);
			return ex;
		}
		
		public ExceptionMessage ExceptionMessage
		{
			get
			{
				return _exceptionMessage;
			}
		}

		public Enum.ExceptionType ExceptionType
		{
			get
			{
				return _exceptionType;
			}
		}
	}
}
