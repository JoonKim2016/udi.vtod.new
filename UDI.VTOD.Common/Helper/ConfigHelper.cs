﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using UDI.Utility.Helper;

namespace UDI.VTOD.Common.Helper
{
	public static class ConfigHelper
	{
		public static string GetSampleFilesBaseAddress()
		{
			var result = System.Configuration.ConfigurationManager.AppSettings["SampleFilesBaseAddress"];
			return result.CheckDirEnd();
		}

        public static string GetDriverPhotoBaseUrl()
        {
            var result = System.Configuration.ConfigurationManager.AppSettings["driverphoto_base_url"];
            return result;
        }

        public static string GetDriverPhotoHandlerUrl()
        {
            var result = System.Configuration.ConfigurationManager.AppSettings["driverphoto_handler_url"];
            return result;
        }

        public static string GetDriverPhotoImagesSizes()
        {
            var result = System.Configuration.ConfigurationManager.AppSettings["driverphoto_images_sizes"];
            return result;
        }

        public static string GetDriverPhotoFolderPath()
        {
            var result = System.Configuration.ConfigurationManager.AppSettings["driverphoto_folder_path"];
            return result.CheckDirEnd();
        }

        public static string GetDriverPhotoDefault()
        {
            var result = System.Configuration.ConfigurationManager.AppSettings["driverphoto_default"];
            return result;
        }
        public static string GetDriverPhotoVersionDefault()
        {
            var result = System.Configuration.ConfigurationManager.AppSettings["driverphoto_default_version"];
            return result;
        }
	    public static string GetCdnKey()
	    {
	         var result = System.Configuration.ConfigurationManager.AppSettings["azure_cdn_key"];
	        return result;
	    }


	    public static string GetCdnStorageContainerName()
	    {
            var result = System.Configuration.ConfigurationManager.AppSettings["azure_cdn_storage_container"];
            return result;
	    }

	    public static string GetCdnStorageName()
	    {
            var result = System.Configuration.ConfigurationManager.AppSettings["azure_cdn_storage"];
            return result;
	    }

	    public static bool IsTest()
	    {
            var isTest = System.Configuration.ConfigurationManager.AppSettings["IsTest"];
            var testServerNames = System.Configuration.ConfigurationManager.AppSettings["TestServerNames"];

	        return (bool.Parse(isTest) && testServerNames.ToLower().Contains(Dns.GetHostName().ToLower()));
	    }
	  
	}
}
