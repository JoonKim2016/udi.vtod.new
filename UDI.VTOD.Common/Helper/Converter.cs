﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Common.DTO.Enum;

namespace UDI.VTOD.Common.Helper
{
	public static class Converter
	{
		public static FleetType ToFleetType(this string input)
		{
			FleetType result = FleetType.Unkown;
			switch (input.ToLower())
			{
				case "all":
					result = FleetType.All;
					break;
				case "execucar":
					result = FleetType.ExecuCar;
					break;
				case "none":
					result = FleetType.Unkown;
					break;
				case "supershuttle":
					result = FleetType.SuperShuttle;
					break;
				case "supershuttle_execucar":
					result = FleetType.SuperShuttle_ExecuCar;
					break;
				case "supershuttlesharedrideonly":
					result = FleetType.SuperShuttleSharedRideOnly;
					break;
				case "taxi":
					result = FleetType.Taxi;
					break;
                case "van":
                    result = FleetType.Van;
                    break;
			}

			return result;
		}

		public static DispatchType ToDispatchType(this string input)
		{
			DispatchType result = DispatchType.Unkown;
			switch (input.ToLower())
			{
				case "greentomato":
				case "aleph":
					result = DispatchType.ECar;
					break;
			}

			return result;
		}

		public static bool ToFleetRecognized(this string input)
		{
			var result = false;
			switch (input.ToLower())
			{
				case "greentomato":
				case "aleph":
					result = true;
					break;
			}

			return result;
		}

		public static Currency ToCurrency(this string input)
		{
			Currency result = Currency.USD;
			switch (input.ToLower())
			{
				case "usd":
					result = Currency.USD;
					break;
				case "eur":
					result = Currency.EUR;
					break;
                case "sek":
                    result = Currency.SEK;
                    break;
            }

			return result;
		}

		public static CorporateName ToCoporateName(this string input)
		{
			CorporateName corporateName = CorporateName.Unknown;
			switch (input.ToLower())
			{
				case "aleph":
					corporateName = CorporateName.Aleph;
					break;
			}

			return corporateName;
		}

		public static TripDirection ToTripDirection(this string input)
		{
			TripDirection corporateName = TripDirection.Unknown;
			switch (input.Trim().ToLower())
			{
				case "toairport":
				case "inbound":
					corporateName = TripDirection.Inbound;
					break;
				case "fromairport":
				case "outbound":
					corporateName = TripDirection.Outbound;
					break;
				case "round":
				case "roundtrip":
					corporateName = TripDirection.RoundTrip;
					break;
			}

			return corporateName;
		}

	}
}
