﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using log4net;
using UDI.Utility.Helper;
using System.Drawing;
using Encoder = System.Text.Encoder;
using Image = System.Drawing.Image;

namespace UDI.VTOD.Common.Helper
{
	public class Images
	{
		private ILog _logger;

		public Images(ILog logger)
		{

			_logger = logger;
		}

		//public string GetPhotoUrl(string driverId, string fleetId)
		public string GetPhotoUrl(Guid driverUniqueId)
		{

			//return string.Format("{0}/?h=height&w=width&f={1}&d={2}", ConfigHelper.GetDriverPhotoHandlerUrl(), fleetId, driverId);
			return string.Format("{0}/?h=height&w=width&d={1}", ConfigHelper.GetDriverPhotoHandlerUrl(), driverUniqueId);
		}


		public string GetCustomSizeDefaultPhotoUrl(int width, int height)
		{
			return string.Format("{0}/default_{1}_{2}.png", ConfigHelper.GetDriverPhotoBaseUrl(), width, height);
		}
        public string GetCustomSizeDefaultPhotoUrl(int width, int height,string version)
        {
            return string.Format("{0}/default.{3}_{1}_{2}.png", ConfigHelper.GetDriverPhotoBaseUrl(), width, height,version);
        }
		//public void UpdateDriverImageSizesFromConfig(Image image, string fleetType, string dispatchUniqueKey, string driverId, string fleetId)
		public void UpdateDriverImageSizesFromConfig(Image image, Guid driverUniqueId)
		{

			try
			{
				var imageHelper = new ImageHelper(_logger);
				//_logger.InfoFormat("UpdateDriverImageSizesFromConfig: fleetType:{0}, fleetId:{1}, driverId:{2}, dispatchUniqueKey:{3}", fleetType, fleetId, driverId, dispatchUniqueKey);
				_logger.InfoFormat("UpdateDriverImageSizesFromConfig: driverUniqueId:{0}", driverUniqueId);

				//var fileName = string.Format(@"{0}_{1}_original.png", fleetId, driverId);
				var fileName = string.Format(@"{0}_original.png", driverUniqueId);

				#region Save Original File

				imageHelper.SavePngToCdn(new CdnImage() { CdnKey = ConfigHelper.GetCdnKey(), ContainerName = ConfigHelper.GetCdnStorageContainerName(), FileName = fileName, Image = image, StorageName = ConfigHelper.GetCdnStorageName() });

				#endregion

				#region Loop through file sizes in web config and save an img for each size

				foreach (var size in ConfigHelper.GetDriverPhotoImagesSizes().Split(',').Select(size => size.Split('x')))
				{
					//var alteredFileName = string.Format(@"{0}_{1}_{2}_{3}.png", fleetId, driverId, size[0], size[1]);
					var alteredFileName = string.Format(@"{0}_{1}_{2}.png", driverUniqueId, size[0], size[1]);
					var _image = imageHelper.ScaleImage(image, int.Parse(size[0]), int.Parse(size[1]));
					imageHelper.SavePngToCdn(new CdnImage() { CdnKey = ConfigHelper.GetCdnKey(), ContainerName = ConfigHelper.GetCdnStorageContainerName(), FileName = alteredFileName, Image = _image, StorageName = ConfigHelper.GetCdnStorageName() });
				}
				#endregion

			}
			catch (Exception ex)
			{
				_logger.Error("UpdateDriverImageSizesFromConfig", ex);
			}


		}

		public void UpdateDriverImage(Image image, Guid driverUniqueId, int height, int width)
		{
			var imageHelper = new ImageHelper(_logger);
			_logger.InfoFormat("UpdateDriverImage: driverUniqueId:{0}, height:{1},  width:{2}", driverUniqueId, height, width);

			#region Save Original File

			//var fileName = string.Format(@"{0}_{1}_{2}_{3}.png", fleetId, driverId, width, height);
			var fileName = string.Format(@"{0}_{1}_{2}.png", driverUniqueId, width, height);

			using (var _image = imageHelper.ScaleImage(image, height, width))
			{
				imageHelper.SavePngToCdn(new CdnImage() { CdnKey = ConfigHelper.GetCdnKey(), ContainerName = ConfigHelper.GetCdnStorageContainerName(), FileName = fileName, Image = _image, StorageName = ConfigHelper.GetCdnStorageName() });
			}

			#endregion
		}


		public void UpdateImage(Image image, string imageName, int height, int width)
		{
			var imageHelper = new ImageHelper(_logger);

			#region Save Original File

			var fileName = string.Format(@"{0}_{1}_{2}.png", imageName, width, height);

			using (var _image = imageHelper.ScaleImage(image, height, width))
			{
				imageHelper.SavePngToCdn(new CdnImage() { CdnKey = ConfigHelper.GetCdnKey(), ContainerName = ConfigHelper.GetCdnStorageContainerName(), FileName = fileName, Image = _image, StorageName = ConfigHelper.GetCdnStorageName() });
			}

			#endregion
		}
        public void UpdateImage(Image image, string imageName, int height, int width,string version)
        {
            var imageHelper = new ImageHelper(_logger);

            #region Save Original File

            var fileName = string.Format(@"{0}.{3}_{1}_{2}.png", imageName, width, height,version);

            using (var _image = imageHelper.ScaleImage(image, height, width))
            {
                imageHelper.SavePngToCdn(new CdnImage() { CdnKey = ConfigHelper.GetCdnKey(), ContainerName = ConfigHelper.GetCdnStorageContainerName(), FileName = fileName, Image = _image, StorageName = ConfigHelper.GetCdnStorageName() });
            }

            #endregion
        }
		public void UpdateDriverImage()
		{
			throw new NotImplementedException();
		}
	}
}
