﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace UDI.VTOD.Common.Helper
{
	public static class MessageFactory
	{
      
		private static List<DTO.ExceptionMessage> _messages;

       
		public static DTO.ExceptionMessage GetMessage(int code)
		{
            if (_messages == null)
            {
                loadMessages();
            }
           
			return _messages.FirstOrDefault(s => s.Code == code);            
		}
      
		private static List<DTO.ExceptionMessage> messages
		{
			get
			{
				return _messages;
			}
		}

		public static void loadMessages()
		{
			_messages = new List<DTO.ExceptionMessage>();

			#region Bubbling up
           
			_messages.Add(new DTO.ExceptionMessage(-1, ""));
			#endregion

			#region General 1-99
			_messages.Add(new DTO.ExceptionMessage(1, "Sorry we cannot proceed."));
			#endregion

			#region Network and API 301-399
			_messages.Add(new DTO.ExceptionMessage(301, "Sorry we cannot proceed."));
			//302 means we cannot connect to UDI33
			_messages.Add(new DTO.ExceptionMessage(302, "Sorry we cannot proceed."));
			//303 means we cannot connect to GreenTomato
			_messages.Add(new DTO.ExceptionMessage(303, "Sorry we cannot proceed."));
			#endregion

			#region Auth 101-199
			_messages.Add(new DTO.ExceptionMessage(101, "Sorry we cannot authorize your call."));
			_messages.Add(new DTO.ExceptionMessage(102, "You do not have permission for the found fleet."));
			#endregion

			#region Avail 1001-1999
			_messages.Add(new DTO.ExceptionMessage(1001, "Sorry we cannot proceed."));
			_messages.Add(new DTO.ExceptionMessage(1002, "No Dispatch Type found."));
			_messages.Add(new DTO.ExceptionMessage(1003, "No matching rate found."));
			_messages.Add(new DTO.ExceptionMessage(1004, "No Available Pickup Times Found."));
			_messages.Add(new DTO.ExceptionMessage(1005, "This area is not serviced."));
			_messages.Add(new DTO.ExceptionMessage(1006, "Sorry, we cannot find trip type."));
			_messages.Add(new DTO.ExceptionMessage(1007, "Sorry, we do not have Location to Location Trip for SuperShuttleSharedRideOnly."));
			_messages.Add(new DTO.ExceptionMessage(1008, "Sorry, we do not provide PickMeUpNow in this area."));
			_messages.Add(new DTO.ExceptionMessage(1009, "No Service Api found."));
			_messages.Add(new DTO.ExceptionMessage(1010, "Unable to find airport coordinate."));
			_messages.Add(new DTO.ExceptionMessage(1011, "No Fleet found."));
			_messages.Add(new DTO.ExceptionMessage(1012, "Sorry, we are unable to find this trip."));
			_messages.Add(new DTO.ExceptionMessage(1013, "Invalid address type."));
            _messages.Add(new DTO.ExceptionMessage(1014, "Invalid dispatch code."));
            _messages.Add(new DTO.ExceptionMessage(1015, "No Reference ID."));
			#endregion

			#region Booking 2001-2999
			_messages.Add(new DTO.ExceptionMessage(2001, "Sorry we cannot proceed."));
			_messages.Add(new DTO.ExceptionMessage(2002, "No Dispatch Type found."));
			_messages.Add(new DTO.ExceptionMessage(2003, "The Reference ID doesn't match any rate for the trip."));
			_messages.Add(new DTO.ExceptionMessage(2004, "The Pick Up time doesn't match any Pick Up time for the trip."));
			_messages.Add(new DTO.ExceptionMessage(2005, "The reservation cannot be completed online, call SuperShuttle directly for assistance."));
			_messages.Add(new DTO.ExceptionMessage(2006, "Duplicate trips found."));
			_messages.Add(new DTO.ExceptionMessage(2007, "Invalid Booking Request."));
			_messages.Add(new DTO.ExceptionMessage(2008, "Invalid customer information."));
			_messages.Add(new DTO.ExceptionMessage(2009, "The pickup time has already expired."));
            _messages.Add(new DTO.ExceptionMessage(2010, "Your credit card was declined."));
            _messages.Add(new DTO.ExceptionMessage(2011, "Pick me up now is not covered in this area."));
			_messages.Add(new DTO.ExceptionMessage(2012, "Pick me later is not covered in this area."));
			_messages.Add(new DTO.ExceptionMessage(2013, "Sorry, you cannot book during blackout hours."));
			_messages.Add(new DTO.ExceptionMessage(2014, "Your direct bill was declined."));
			_messages.Add(new DTO.ExceptionMessage(2015, "Invalid password provided for direct bill account."));
			_messages.Add(new DTO.ExceptionMessage(2016, "Your credit card was expired."));
			_messages.Add(new DTO.ExceptionMessage(2017, "We cannot proceed with your credit card."));
			_messages.Add(new DTO.ExceptionMessage(2018, "Time is closed for big events like the SuperBowl."));
            _messages.Add(new DTO.ExceptionMessage(2019, "Sorry!! Pick Up Airport is not supported"));
            _messages.Add(new DTO.ExceptionMessage(2020, "Sorry!! DropOff Airport is not supported"));
            _messages.Add(new DTO.ExceptionMessage(2021, "Prepaid Card is not supported."));
            _messages.Add(new DTO.ExceptionMessage(2022, "Sorry!!Trip cannot be found."));
            _messages.Add(new DTO.ExceptionMessage(2023, "Sorry, currently this feature is only for verified zTrip members. The phone number you attempted is not in our system."));
            _messages.Add(new DTO.ExceptionMessage(2024, "Sorry, we cannot split the payment in current trip status."));
            _messages.Add(new DTO.ExceptionMessage(2025, "This split payment has already been canceled."));
            _messages.Add(new DTO.ExceptionMessage(2026, "A split payment for this trip has already been accepted."));
            _messages.Add(new DTO.ExceptionMessage(2027, "Another split payment request has been sent."));
            _messages.Add(new DTO.ExceptionMessage(2028, "This trip is already completed."));
            _messages.Add(new DTO.ExceptionMessage(2029, "Sorry!! We don't support the Cash Payment Type"));
            //_messages.Add(new DTO.ExceptionMessage(2022, "Sorry!! Pick me later is not supported in this area."));
            #endregion

            #region GroundResRetrieve (Status) 3001-3999
            _messages.Add(new DTO.ExceptionMessage(3001, "Sorry we cannot proceed."));
			_messages.Add(new DTO.ExceptionMessage(3002, "No Dispatch Type found."));
			_messages.Add(new DTO.ExceptionMessage(3003, "Invalid GroundResRetrieve Request."));
			_messages.Add(new DTO.ExceptionMessage(3004, "Sorry, we are unable to find this trip."));
			#endregion

			#region Vehicle Info 4001-4999
			_messages.Add(new DTO.ExceptionMessage(4001, "Sorry we cannot proceed."));
			_messages.Add(new DTO.ExceptionMessage(4002, "No Dispatch Type found."));
			#endregion

			#region Cancel/Cancel Fee 5001-5999
			_messages.Add(new DTO.ExceptionMessage(5001, "Sorry we cannot proceed."));
			_messages.Add(new DTO.ExceptionMessage(5002, "No Dispatch Type found."));
			_messages.Add(new DTO.ExceptionMessage(5003, "Cancellation Failed. Too close to pickup time."));
			_messages.Add(new DTO.ExceptionMessage(5004, "Cancellation Failed. Reservation pickup time has expired."));
			_messages.Add(new DTO.ExceptionMessage(5005, "Invalid Cancel Request."));
			#endregion

			#region ASAP 6001-6999
			_messages.Add(new DTO.ExceptionMessage(6001, "Sorry we cannot proceed."));
			_messages.Add(new DTO.ExceptionMessage(6002, "The Pick Up time doesn't match any Pick Up times for the trip."));
			#endregion

			#region Membership 7001-7999
			_messages.Add(new DTO.ExceptionMessage(7001, "Sorry we cannot proceed."));
			_messages.Add(new DTO.ExceptionMessage(7002, "Sorry, we are unable to find this trip."));
			//There is a problem in linking trips to SDS system.
			_messages.Add(new DTO.ExceptionMessage(7003, "Sorry we cannot proceed."));
			//There was an issue while adding a new location to the SDS system.
			_messages.Add(new DTO.ExceptionMessage(7004, "Sorry we cannot proceed."));
			_messages.Add(new DTO.ExceptionMessage(7005, "Sorry, we cannot send an email now."));
			_messages.Add(new DTO.ExceptionMessage(7006, "Sorry, we cannot change your email now."));
			_messages.Add(new DTO.ExceptionMessage(7007, "Sorry, we cannot change your password now."));
			//There was an issue while deleting a location record from the SDS system.
			_messages.Add(new DTO.ExceptionMessage(7008, "Sorry we cannot proceed."));
			//There was an issue while handling a forgotten password in the SDS system.
			_messages.Add(new DTO.ExceptionMessage(7009, "Sorry we cannot proceed."));
			//There was an issue while retrieving location records from the SDS system.
			_messages.Add(new DTO.ExceptionMessage(7010, "Sorry we cannot proceed."));
			//There was an issue while retrieving a member's reservation from the SDS system.
			_messages.Add(new DTO.ExceptionMessage(7011, "Sorry we cannot proceed."));
			_messages.Add(new DTO.ExceptionMessage(7012, "Sorry, there was an issue while retrieving a member's most recent un rated trip."));
			_messages.Add(new DTO.ExceptionMessage(7013, "Sorry, we cannot validate your membership."));
			//There was an issue while updating a location record in the SDS system.
			_messages.Add(new DTO.ExceptionMessage(7014, "Sorry we cannot proceed."));
			//There was an issue while updating a member's profile in the SDS system.
			_messages.Add(new DTO.ExceptionMessage(7015, "Sorry we cannot proceed."));
			_messages.Add(new DTO.ExceptionMessage(7016, "Sorry, your email has been already registered."));
			_messages.Add(new DTO.ExceptionMessage(7017, "Sorry, we cannot reset your password."));
			_messages.Add(new DTO.ExceptionMessage(7018, "Email address is already used."));
			_messages.Add(new DTO.ExceptionMessage(7019, "Sorry, we cannot delete the member record."));
            _messages.Add(new DTO.ExceptionMessage(7020, "Sorry, your email address(es) is/are invalid."));
            _messages.Add(new DTO.ExceptionMessage(7021, "Sorry, your phone number(s) is/are invalid."));
            _messages.Add(new DTO.ExceptionMessage(7022, "Sorry, email is not allowed for Anonymous User for SDS Taxi."));
			#endregion

			#region Accounting 8001-8999
			_messages.Add(new DTO.ExceptionMessage(8001, "Sorry we cannot proceed."));
			_messages.Add(new DTO.ExceptionMessage(8002, "Sorry, we cannot set the default reward account."));
			_messages.Add(new DTO.ExceptionMessage(8003, "Failed to Set Default Credit Card Account."));
			_messages.Add(new DTO.ExceptionMessage(8004, "Failed to Delete Credit Card Account."));
			_messages.Add(new DTO.ExceptionMessage(8005, "The direct bill account number does not exist."));
			_messages.Add(new DTO.ExceptionMessage(8006, "Failed to Add Direct Bill Account."));
			_messages.Add(new DTO.ExceptionMessage(8007, "Failed to Set Default Direct Bill Account."));
			_messages.Add(new DTO.ExceptionMessage(8008, "Failed to Delete Direct Bill Account."));
			_messages.Add(new DTO.ExceptionMessage(8009, "Failed to Delete Membership Mileage Account."));
			_messages.Add(new DTO.ExceptionMessage(8010, "Returned transactionID is null."));
            _messages.Add(new DTO.ExceptionMessage(8011, "Invalid Credit Card"));
            _messages.Add(new DTO.ExceptionMessage(8012, "This member doesn't have a credit card."));
            _messages.Add(new DTO.ExceptionMessage(8013, "This credit code is only available for first time users."));
            #endregion

            #region Utility 9001-9999
            _messages.Add(new DTO.ExceptionMessage(9001, "Sorry we cannot proceed."));
			_messages.Add(new DTO.ExceptionMessage(9002, "Requested airport is not serviced."));
			_messages.Add(new DTO.ExceptionMessage(9003, "Sorry, we are unable to send message to vehicle."));
			_messages.Add(new DTO.ExceptionMessage(9004, "Sorry, there was a problem while searching for airport name."));
            _messages.Add(new DTO.ExceptionMessage(9005, "Sorry, the phone number provided is not in correct format."));
            _messages.Add(new DTO.ExceptionMessage(9006, "Sorry, area code is not correct."));
            _messages.Add(new DTO.ExceptionMessage(9007, "Sorry, country code is not correct."));
            _messages.Add(new DTO.ExceptionMessage(9008, "Your phone is not verified."));
            _messages.Add(new DTO.ExceptionMessage(9009, "Your phone  is already verified."));
            _messages.Add(new DTO.ExceptionMessage(9010, "This phone is already verified for another user."));
            _messages.Add(new DTO.ExceptionMessage(9011, "No Record Found"));
            _messages.Add(new DTO.ExceptionMessage(9012, "Input not in the proper format"));
            _messages.Add(new DTO.ExceptionMessage(9013, "Input not in the proper format"));
            #region IVR only
            _messages.Add(new DTO.ExceptionMessage(9101, "Unable to get customer information back"));
            _messages.Add(new DTO.ExceptionMessage(9102, "Unable to get pickup addresses back"));
            _messages.Add(new DTO.ExceptionMessage(9103, "Unable to get last trip back"));
            _messages.Add(new DTO.ExceptionMessage(9104, "Unable to get black out period back"));
            _messages.Add(new DTO.ExceptionMessage(9105, "Unable to get stale notifcation back"));
            _messages.Add(new DTO.ExceptionMessage(9106, "Unable to get arrival notifcation back"));
            _messages.Add(new DTO.ExceptionMessage(9107, "Unable to get night before reminder back"));
            _messages.Add(new DTO.ExceptionMessage(9108, "Unable to get last trip detail back"));

            _messages.Add(new DTO.ExceptionMessage(9151, "Invalid GetCustomerInformation request"));
            _messages.Add(new DTO.ExceptionMessage(9152, "Invalid GetPickupAddress request"));
            _messages.Add(new DTO.ExceptionMessage(9153, "Invalid GetLastTrip request"));
            _messages.Add(new DTO.ExceptionMessage(9154, "Invalid GetBlackouPeriod request"));
            _messages.Add(new DTO.ExceptionMessage(9155, "Invalid GetStaleNotifcation request"));
            _messages.Add(new DTO.ExceptionMessage(9156, "Invalid GetArrivalNotifcation request"));
            _messages.Add(new DTO.ExceptionMessage(9157, "Invalid GetNightBeforeReminder request"));
            #endregion

            
            _messages.Add(new DTO.ExceptionMessage(9014, "AppName should be passed"));
            _messages.Add(new DTO.ExceptionMessage(9015, "Lat/Long should be passed"));
            _messages.Add(new DTO.ExceptionMessage(9016, "Category should have a value."));
            _messages.Add(new DTO.ExceptionMessage(9017, "Verification Code should have a value."));
            _messages.Add(new DTO.ExceptionMessage(9018, "Verification Code is not valid."));
            _messages.Add(new DTO.ExceptionMessage(9019, "Your phone is already registered."));
            #endregion

            #region Validation Messages 13001 - 13999
            // This is general message for validation
            _messages.Add(new DTO.ExceptionMessage(13001, ""));
			// This is just for required field validation.
			_messages.Add(new DTO.ExceptionMessage(13002, "'{0}' is required."));
			// This is just for field format validation.
			_messages.Add(new DTO.ExceptionMessage(13003, "{0} should be in {1} format."));
			_messages.Add(new DTO.ExceptionMessage(13004, "Please send the correct version."));
			_messages.Add(new DTO.ExceptionMessage(13005, "Sorry, you are not allowed to get information or modify this trip."));
			_messages.Add(new DTO.ExceptionMessage(13006, "Enter a valid expiration date for your payment card."));
            _messages.Add(new DTO.ExceptionMessage(13009, "Flight time cannot be more than one year away."));
			#endregion

			#region Group and Discount 14001-14999
			_messages.Add(new DTO.ExceptionMessage(14001, "Sorry we cannot proceed."));
			_messages.Add(new DTO.ExceptionMessage(14002, "Sorry we cannot validate your code."));
			#endregion

			#region Corporate 20001 - 29999
			#region Aleph 20001 - 20199
			_messages.Add(new DTO.ExceptionMessage(20001, "Sorry we cannot proceed."));
			_messages.Add(new DTO.ExceptionMessage(20002, "Sorry we cannot validate your entries."));
			_messages.Add(new DTO.ExceptionMessage(20003, "Unable to retrieve specified reservation."));
            _messages.Add(new DTO.ExceptionMessage(20004, "Unexpected error."));
            _messages.Add(new DTO.ExceptionMessage(20005, "Badly formed request. Required field may be missing."));
            _messages.Add(new DTO.ExceptionMessage(20006, "We were unable to authenticate with the credentials provided."));
            _messages.Add(new DTO.ExceptionMessage(20007, "The specified user does not have the authorization to perform the requested operation."));
            _messages.Add(new DTO.ExceptionMessage(20008, "An expected resource or specified parameter was not found."));
            _messages.Add(new DTO.ExceptionMessage(20009, "Conflicting request."));
            _messages.Add(new DTO.ExceptionMessage(20010, "There has been an unexpected server error. Attempt your operation again."));
            _messages.Add(new DTO.ExceptionMessage(20011, "The username is required."));
            _messages.Add(new DTO.ExceptionMessage(20012, "The password is required."));
            _messages.Add(new DTO.ExceptionMessage(20013, "The username cannot exceed 64 characters."));
            _messages.Add(new DTO.ExceptionMessage(20014, "The password cannot exceed 32 characters."));
            _messages.Add(new DTO.ExceptionMessage(20015, "User was not found."));
            _messages.Add(new DTO.ExceptionMessage(20016, "The new password is required."));
            _messages.Add(new DTO.ExceptionMessage(20017, "No cars are available from the user's contracted fleets."));
            _messages.Add(new DTO.ExceptionMessage(20018, "User's password is already set. To change user's current password, provide user's current password, new password and use the HTTP PUT verb."));
            _messages.Add(new DTO.ExceptionMessage(20019, "Invalid password. You must enter your current password correctly before you can change it to the new password."));
            _messages.Add(new DTO.ExceptionMessage(20020, "Unable to retrieve corporate key from SDS."));
            _messages.Add(new DTO.ExceptionMessage(20021, "The user's corporate key cannot be blank."));
            _messages.Add(new DTO.ExceptionMessage(20022, "Unable to locate Aleph profile inputs."));
            _messages.Add(new DTO.ExceptionMessage(20023, "Error while validating Aleph profile inputs."));
            _messages.Add(new DTO.ExceptionMessage(20024, "User does not have access to the specified profile."));
            _messages.Add(new DTO.ExceptionMessage(20025, "Not all required booking criteria is present or has a value."));
            _messages.Add(new DTO.ExceptionMessage(20026, "The requested resource does not support http method 'PUT'."));
            _messages.Add(new DTO.ExceptionMessage(20027, "You cannot make this reservation because the pickup time is earlier than the current time."));
            _messages.Add(new DTO.ExceptionMessage(20028, "Your requested URL does not match your reservation request content."));
            _messages.Add(new DTO.ExceptionMessage(20029, "The requested resource does not support the http VERB presented."));
            _messages.Add(new DTO.ExceptionMessage(20030, "The resource you are looking for might have been removed, had its name changed, or is temporarily unavailable."));
            _messages.Add(new DTO.ExceptionMessage(20031, "The specified pickup address type is not supported."));
            _messages.Add(new DTO.ExceptionMessage(20032, "The specified dropoff address type is not supported."));
            _messages.Add(new DTO.ExceptionMessage(20033, "Unable to specify a required booking parameter."));
			_messages.Add(new DTO.ExceptionMessage(20034, "The driver has already been dispatched so we are unable to cancel this ride."));
			_messages.Add(new DTO.ExceptionMessage(20035, "Pick me up now is not covered in this area."));
			_messages.Add(new DTO.ExceptionMessage(20036, "Pick me later is not covered in this area."));
            _messages.Add(new DTO.ExceptionMessage(20037, "This trip cannot be cancelled as it is Dispatched/Assigned."));
			_messages.Add(new DTO.ExceptionMessage(20038, "Street Number is Required."));
			_messages.Add(new DTO.ExceptionMessage(20039, "Not all required booking criteria is present or has a value."));
			_messages.Add(new DTO.ExceptionMessage(20040, "The street number is invalid."));
            _messages.Add(new DTO.ExceptionMessage(20041, "Duplicate credit card on file, please try again."));
            _messages.Add(new DTO.ExceptionMessage(20042, "Invalid credit card number, please try again."));
            _messages.Add(new DTO.ExceptionMessage(20043, "Invalid CVV code, please try again."));
            _messages.Add(new DTO.ExceptionMessage(20044, "Invalid expiration date, please try again."));
            _messages.Add(new DTO.ExceptionMessage(20045, "Only Amex, Discover, MasterCard, and Visa are supported."));
            _messages.Add(new DTO.ExceptionMessage(20046, "We are having issues processing your credit card, please try again."));
            _messages.Add(new DTO.ExceptionMessage(20047, "This account does not except credit cards, please try again with a different account."));
            _messages.Add(new DTO.ExceptionMessage(20048, "No debit or pre-paid cards allowed, please try again."));
            _messages.Add(new DTO.ExceptionMessage(20049, "A credit card is required to book."));

            #endregion

            #endregion



        }
	}
}
