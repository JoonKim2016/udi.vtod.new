﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.Helper
{
	public static class Messages
	{
		#region Validation
		//public static string Validation_Version
		//{
		//	get
		//	{
		//		return "Please send a correct version";
		//	}
		//}
		public static string Validation_CreditCard
		{
			get
			{
				return "We are unable to process your credit card.";
			}
		}

		public static string Validation_PaymentInfo
		{
			get
			{
				return "We are unable to process your payment";
			}
		}

		public static string Validation_InvalidPaymentType
		{
			get
			{
				return "Invalid Payment Type";
			}
		}

		public static string Validation_FlightTime_Year_Away
		{
			get
			{
				return "Flight time cannot be more than one year away.";
			}
		}

		public static string Validation_LocationContainsBothPickupAddressAndAirportInfo
		{
			get
			{
				return "Booking request cannot contain both address and airport info together.";
			}
		}

		public static string Validation_ArrivalAndDepartureAirportInfo
		{
			get
			{
				return "Request cannot contain both Arrival/Depature AirportInfo.";
			}
		}

		public static string Validation_LocationDetail
		{
			get
			{
				return "The detail information in address is not valid.";
			}
		}

		public static string Validation_LocationDetailByMapApi
		{
			get
			{
				return "The address is not valid by map api.";
			}

		}

		public static string Validation_LocationNoLatlon
		{
			get
			{
				return "If the booking request doesn't enable MAP API, it must provide longitude and latitude.";
			}
		}

		//public static string Validation_InvalidAddressType
		//{
		//	get
		//	{
		//		return "Invalid address type.";
		//		//return "I'm sorry but we do not offer taxi service for this airport. Please use our Black Car service or go to the nearest taxi stand.";
		//	}
		//}

		public static string Validation_NullRequest
		{
			get
			{
				return "Sorry, you sent a null request";
			}
		}

		public static string Validation_TwoAirportsInOneSegment
		{
			get
			{
				return "Two airports are not allowed in one segment";
			}
		}

		public static string Validation_TwoReference
		{
			get
			{
				return "You should send two reference IDs for round trips";
			}
		}

		public static string Validation_ASAP_ReturnTrips
		{
			get
			{
				return "Sorry, we do not support ASAP for return trips";
			}
		}

		public static string Validation_PickupTimeNotSupported
		{
			get
			{
				return "Sorry, we do not support pickup time for this RateQualifier in this version";
			}
		}

		//public static string Validation_CardExpiredDate
		//{
		//	get
		//	{
		//		return "Please enter a valid expiration date.";
		//	}
		//}

		public static string Validation_InvalidCardType
		{
			get
			{
				return "Sorry, we do not support that Credit Card type.";
			}
		}

		public static string Validation_InvalidCreditCardNumber
		{
			get
			{
				return "Invalid CreditCardNumber.";
			}
		}

		public static string Validation_InvalidRewardIds
		{
			get
			{
				return "Please insert valid RewardId.";
			}
		}
		#endregion

		#region Notification
		public static string Notification_NotifyDriver
		{
			get
			{
				return "The rider {0} {1} has been successfully charged ${2}";
			}
		}
		#endregion
	
		
		#region General
		//public static string Validation_Wrong_DateTime_Format
		//{
		//	get
		//	{
		//		return "{0}:: The {1} is not a valid datetime string.";
		//	}
		//}
	
		//public static string General_BadRequest
		//{
		//	get
		//	{
		//		return "Bad Request";
		//	}
		//}


		//public static string General_BadDirectBill
		//{
		//	get
		//	{
		//		return "Sorry, we are unable to process your direct bill";
		//	}
		//}

		//public static string General_BadPaymentInfo
		//{
		//	get
		//	{
		//		return "Sorry, we are unable to process your payment";
		//	}
		//}


		//public static string General_NullResonse
		//{
		//	get
		//	{
		//		return "Oops, something is wrong. Sorry !";
		//	}
		//}

		//public static string General_DispatchTypeError
		//{
		//	get
		//	{
		//		return "Sorry, we cannot find DisptchType!";
		//	}
		//}

		//public static string General_NoMatchingRate
		//{
		//	get
		//	{
		//		return "No matching rate found.";
		//	}
		//}
		#endregion

		#region Membership
		//public static string Membership_Denied
		//{
		//	get
		//	{
		//		return "Sorry, you are not authenticate|authorized to use this service";
		//	}
		//}

		//public static string Membership_Exception
		//{
		//	get
		//	{
		//		return "Sorry, there is an exception for authenticating you";
		//	}
		//}

		//public static string Membership_Token_ProviderType
		//{
		//	get
		//	{
		//		return "Sorry, we cannot get token for this provider type";
		//	}
		//}
		#endregion

		#region VTOD_SDSDomain

		//public static string VTOD_SDSDomain_LatLon_FormatException
		//{
		//	get
		//	{
		//		return "{0}:: The Lat or Lon is invalid.";
		//	}
		//}

		//public static string VTOD_SDSDomain_NoDepartureNoArrival
		//{
		//	get
		//	{
		//		return "{0}:: No Departure and Arrival found in AirportInfo.";
		//	}
		//}

		//public static string VTOD_SDSDomain_NoAddress
		//{
		//	get
		//	{
		//		return "{0}:: No address found in the request.";
		//	}
		//}

		//public static string VTOD_SDSDomain_MissingPayment
		//{
		//	get
		//	{
		//		return "{0}:: Missing payment information.";
		//	}
		//}

		//public static string VTOD_SDSDomain_MissingPassengerInfo
		//{
		//	get
		//	{
		//		return "{0}:: Missing passenger information.";
		//	}
		//}

		//public static string VTOD_SDSDomain_MissingPassengerInfo_NameOrPhone
		//{
		//	get
		//	{
		//		return "{0}:: Missing passenger name or phone number.";
		//	}
		//}

		//public static string VTOD_SDSDomain_NoFare
		//{
		//	get
		//	{
		//		return "{0}:: No Fare Found.";
		//	}
		//}

		//public static string VTOD_SDSDomain_NoAvailablePickupTimes
		//{
		//	get
		//	{
		//		return "{0}:: No Available Pickup Times Found.";
		//	}
		//}

		//public static string VTOD_SDSDomain_GroundBookRQ_MissingData
		//{
		//	get
		//	{
		//		return "{0}:: Missing GroundReservations OR TPA_Extensions OR Reference in the request.";
		//	}
		//}

		//public static string VTOD_SDSDomain_GroundBook_NoMatchingRateFound
		//{
		//	get
		//	{
		//		return "{0}:: The Reference ID doesn't match any rate for the trip.";
		//	}
		//}

		//public static string VTOD_SDSDomain_GroundBook_NoMatchingPickupTimeFound
		//{
		//	get
		//	{
		//		return "{0}:: The Pickup DateTime doesn't match any available pickup time for the trip.";
		//	}
		//}

		//public static string VTOD_SDSDomain_GroundCancel_UniqueID
		//{
		//	get
		//	{
		//		return "Correct UniqueID is required for Cancellation.";
		//	}
		//}

		//public static string VTOD_SDSDomain_GroundCancel_PersonName
		//{
		//	get
		//	{
		//		return "{0}:: PersonName is required for Cancellation.";
		//	}
		//}

		//public static string VTOD_SDSDomain_GroundCancel_TelephoneInfo
		//{
		//	get
		//	{
		//		return "{0}:: TelephoneInfo is required for Cancellation.";
		//	}
		//}

		//public static string VTOD_SDSDomain_GroundCancel_PostalCode
		//{
		//	get
		//	{
		//		return "{0}:: PostalCode is required for Cancellation.";
		//	}
		//}

		//public static string VTOD_SDSDomain_GroundResRetrieve_ReferenceID
		//{
		//	get
		//	{
		//		return "{0}:: Reference ID is required.";
		//	}
		//}

		//public static string VTOD_SDSDomain_GroundResRetrieve_NoStatusFound
		//{
		//	get
		//	{
		//		return "{0}:: No Status Found for Reference ID {1}.";
		//	}
		//}

		//public static string VTOD_SDSDomain_AddressMissingData
		//{
		//	get
		//	{
		//		return "{0}:: Address Missing Data.";
		//	}
		//}

		//public static string VTOD_SDSDomain_MisingPickupTime
		//{
		//	get
		//	{
		//		return "{0}:: Pickup DateTime is required.";
		//	}
		//}

		//public static string VTOD_SDSDomain_MisingPickupLocation
		//{
		//	get
		//	{
		//		return "{0}:: Pickup Location is required.";
		//	}
		//}

		//public static string VTOD_SDSDomain_MissingPassengerNumber
		//{
		//	get
		//	{
		//		return "{0}:: Passenger number is required.";
		//	}
		//}

		#endregion

		#region UDI_SDS
		//public static string UDI_SDS_PostalCodeNotServiced
		//{
		//	get
		//	{
		//		return "GetSplitNamesOfServicedPostalCode:: The postalcode {0} is not serviced.";
		//	}
		//}

		//public static string UDI_SDS_TripType
		//{
		//	get
		//	{
		//		return "Sorry, we cannot find trip type.";
		//	}
		//}

		//public static string UDI_SDS_AirportNotServiced
		//{
		//	get
		//	{
		//		return "{0}:: The airport {1} is not serviced. {2}";
		//	}
		//}

		//public static string UDI_SDS_DefaultAirlineRewardError
		//{
		//	get
		//	{
		//		return "Sorry, we cannot set the default account";
		//	}
		//}

		//public static string UDI_SDS_LocationCodeRequired
		//{
		//	get
		//	{
		//		return "{0}:: LocationCode is required if location type is Airport.";
		//	}
		//}

		//public static string UDI_SDS_AsDirected
		//{
		//	get
		//	{
		//		return "{0}:: AsDirected is only used with Dropoff mode.";
		//	}
		//}

		//public static string UDI_SDS_PostalCodeRequired
		//{
		//	get
		//	{
		//		return "{0}:: PostalCode is required.";
		//	}
		//}

		//public static string UDI_SDS_StreetNumberRequired
		//{
		//	get
		//	{
		//		return "Street Number is required.";
		//	}
		//}

		//public static string UDI_SDS_NoGeoCodeFound
		//{
		//	get
		//	{
		//		return "{0}:: No GeoCode Found for {1} address.";
		//	}
		//}

		//public static string UDI_SDS_PICKUPDENIED
		//{
		//	get
		//	{
		//		return "{0}:: ASAP Pickup Time is denied.";
		//	}
		//}

		//public static string UDI_SDS_ASAP_DispositionCode
		//{
		//	get
		//	{
		//		return "{0}:: ASAP DispositionCode returned by SuperShuttle is {1}.";
		//	}
		//}

		//public static string UDI_SDS_ASAP_NotCompleted
		//{
		//	get
		//	{
		//		return "{0}:: ASAP Request is not completed.";
		//	}
		//}

		//public static string UDI_SDS_ASAP_NoPickupTimeFound
		//{
		//	get
		//	{
		//		return "{0}:: No pickup time returned by SuperShuttle.";
		//	}
		//}

		//public static string UDI_SDS_ASAP_ReferenceID
		//{
		//	get
		//	{
		//		return "No ReferenceID found";
		//	}
		//}

		//public static string UDI_SDS_ASAP_InvalidPickupTimeStatus
		//{
		//	get
		//	{
		//		return "{0}:: PickupTime Status returned by SuperShuttle is {1}.";
		//	}
		//}

		//public static string UDI_SDS_StatusFailed
		//{
		//	get
		//	{
		//		return "Sorry, we cannot get the status of your trip.";
		//	}
		//}

		//public static string UDI_SDS_PickupTime_After_FlightTime
		//{
		//	get
		//	{
		//		return "Pickup time cannot be same/after flight time for outbound trips.";
		//	}
		//}

		//public static string UDI_SDS_Invalid_PickupTime
		//{
		//	get
		//	{
		//		return "Pickup time is invalid.";
		//	}
		//}

		//public static string UDI_SDS_Invalid_FlightTime
		//{
		//	get
		//	{
		//		return "Flight time is invalid.";
		//	}
		//}

		//public static string UDI_SDS_Invalid_PickupTime_StatusCode
		//{
		//	get
		//	{
		//		return "The reservation cannot be completed online, call us directly for assistance 800-258-3826.";
		//	}
		//}

		//public static string UDI_SDS_CancelFailed_TooCloseToPickupTime
		//{
		//	get
		//	{
		//		return "{0}:: Cancellation Failed. Too close to pickup time.";
		//	}
		//}

		//public static string UDI_SDS_CancelFailed_PickupTimeExpired
		//{
		//	get
		//	{
		//		return "{0}:: Cancellation Failed. Reservation pickup time has expired, reservation cannot be cancelled.";
		//	}
		//}

		//public static string UDI_SDS_CancelFailed
		//{
		//	get
		//	{
		//		return "Sorry, we cannot cancel your trip";
		//	}
		//}

		//public static string UDI_SDS_CancelFailed_Unknown
		//{
		//	get
		//	{
		//		return "{0}:: Cancellation Failed. Unknown Result returned by SuperShuttle {1}.";
		//	}
		//}

		//public static string UDI_SDS_ShareRide_LocationToLocation
		//{
		//	get
		//	{
		//		return "Sorry, we do not have Location to Location Trip for SuperShuttleSharedRideOnly";
		//	}
		//}

		//public static string UDI_SDS_PickMeUpNow_Not_Provided
		//{
		//	get
		//	{
		//		return "Sorry, we do not provide PickMeUpNow in this area";
		//	}
		//}

		#endregion

		#region Taxi

		#region Taxi.Common

		//public static string Log_SkipReservationServiceLocationPickupDetailByMapApi
		//{
		//	get
		//	{
		//		return "Skip MAP API Validation.";
		//	}
		//}



		//public static string Log_SkipReservationServiceLocationDropoffDetailByMAPAPI
		//{
		//	get
		//	{
		//		return "Skip MAP API Validation.";
		//	}
		//}

		//public static string Taxi_Common_UnknownError
		//{
		//	get
		//	{
		//		return "Unknown Error";
		//	}
		//}

		//public static string Taxi_Common_NotSupportNow
		//{
		//	get
		//	{
		//		return "Not support now";
		//	}
		//}

		//public static string Taxi_Common_NoServiceAPIPreference
		//{
		//	get
		//	{
		//		return "Unable to find service api preference.";
		//	}
		//}
		//public static string Taxi_Common_UnableToFindAirportLongitudeAndLatitude
		//{
		//	get
		//	{
		//		return "Unable to find Lat.&Long by aiport code";
		//	}
		//}

		//public static string Taxi_Common_UnableToGetAvailableFleets
		//{
		//	get
		//	{
		//		return "Unable to get available fleets";
		//	}
		//}

		#region valid
		//public static string Taxi_Common_ValidReservationPassengerPrimaryPersonNameDetail
		//{
		//	get
		//	{
		//		return "First name and last name are valid.";
		//	}
		//}

		//public static string Taxi_Common_ValidReservationPassengerPrimaryTelephones
		//{
		//	get
		//	{
		//		return "Phone number(s) is/are valid.";
		//	}
		//}

		#endregion

		#region invalid
		//public static string Taxi_Status_InvalidCancelRequest
		//{
		//	get
		//	{
		//		return "Sorry, your cancel request is invalid.";
		//	}
		//}
		//public static string Taxi_Status_InvalidStatusRequest
		//{
		//	get
		//	{
		//		return "Sorry, your status request is invalid.";
		//	}
		//}
		//public static string Taxi_Common_InvalidBookingRequest
		//{
		//	get
		//	{
		//		return "Sorry, your booking request is invalid.";
		//	}
		//}

		//public static string Taxi_Common_InvalidReservation
		//{
		//	get
		//	{
		//		return "Sorry, we are unable to find any reservation in your request.";
		//	}
		//}

		//public static string Taxi_Common_InvalidReservationPassenger
		//{
		//	get
		//	{
		//		return "Sorry, we are unable to find any passenger in your request.";
		//	}
		//}

		//public static string Taxi_Common_InvalidReservationPassengerPrimary
		//{
		//	get
		//	{
		//		return "Sorry, we are unable to find any primary passenger in your request.";
		//	}
		//}

		//public static string Taxi_Common_InvalidReservationPassengerPrimaryPersonName
		//{
		//	get
		//	{
		//		return "Sorry, we are unable to find any person name of primary passenger in your request.";
		//	}
		//}

		//public static string Taxi_Common_InvalidReservationPassengerPrimaryPersonNameDetail
		//{
		//	get
		//	{
		//		return "Sorry, we are unable to find any detail part of primary passenger in your request.";
		//	}
		//}

		//public static string Taxi_Common_InvalidReservationPassengerPrimaryPhoneNumber
		//{
		//	get
		//	{
		//		return "Sorry, we are unable to find any phone numbers of primary passenger in your request.";
		//	}
		//}

		//public static string Taxi_Common_InvalidReservationPassengerPrimaryTelephones
		//{
		//	get
		//	{
		//		return "Sorry, your phone number(s) is/are invalid.";
		//	}
		//}

		//public static string Taxi_Common_UnableToCreateOrFindTaxiCustomer
		//{
		//	get
		//	{
		//		return "Unable to create taxi customer record for this reqest.";
		//	}
		//}

		//public static string Taxi_Common_InvalidReservationService
		//{
		//	get
		//	{
		//		return "Sorry, we are unable to find Reservation.Service element in your request.";
		//	}
		//}

		//public static string Taxi_Common_InvalidReservationServiceLocation
		//{
		//	get
		//	{
		//		return "Sorry, we are unable to find Reservation.Service.Location element in your request.";
		//	}
		//}

		#region pickup address
		//public static string Taxi_Common_InvalidReservationServiceLocationPickup
		//{
		//	get
		//	{
		//		return "Sorry, we are unable to find Reservation.Service.Location.Pickup element in your request.";
		//	}
		//}

		//public static string Taxi_Common_InvalidAirportPickup
		//{
		//	get
		//	{
		//		return "Sorry, we do not accept airport for Pickup address for taxi fleet.";
		//	}
		//}

		//public static string Taxi_Common_InvalidReservationServiceLocationDropOffDetail
		//{
		//	get
		//	{
		//		return "The detail information in this dropoff address is not valid.";
		//	}
		//}



		//public static string Taxi_Common_InvalidReservationServiceLocationPickupAddressLocationName
		//{
		//	get
		//	{
		//		return "We doesn't support location name now.";
		//	}
		//}

		//public static string Taxi_Common_InvalidReservationServiceLocationPickupAirportInfoContainsBothArrivalAndDepature
		//{
		//	get
		//	{
		//		return "This request contains both Arrival/Depature AirportInfo.";
		//	}
		//}

		//public static string Taxi_Common_InvalidReservationServiceLocationPickupAirportInfoArrival
		//{
		//	get
		//	{
		//		return "Invalid airportInfo at arrival";
		//	}
		//}

		//public static string Taxi_Common_InvalidReservationServiceLocationPickupAirportInfoDepature
		//{
		//	get
		//	{
		//		return "Invalid airportInfo at depature";
		//	}
		//}


		#endregion

		#region drop off address


		//public static string Taxi_Common_InvalidReservationServiceLocationDropoff
		//{
		//	get
		//	{
		//		return "Unable to find Reservation.Service.Location.Dropoff element in your request.";
		//	}
		//}

		//public static string Taxi_Common_InvalidReservationServiceLocationDropoffContainsBothPickupAddressAndAirportInfo
		//{
		//	get
		//	{
		//		return "OTA booking request cannot contain both dropoff address and airport info.";
		//	}
		//}

		//public static string Taxi_Common_InvalidReservationServiceLocationDropoffDetail
		//{
		//	get
		//	{
		//		return "The detail information in this dropoff address is not valid.";
		//	}
		//}

		//public static string Taxi_Common_InvalidReservationServiceLocationDropoffDetailByMAPAPI(string address)
		//{

		//	return string.Format("The dropoff address is not valid by MAP API. Address={0}", address);

		//}

		//public static string Taxi_Common_InvalidReservationServiceLocationDropoffAddressLocationName
		//{
		//	get
		//	{
		//		return "We doesn't support location name now.";
		//	}
		//}

		//public static string Taxi_Common_InvalidReservationServiceLocationDropoffAirportInfoContainsBothArrivalAndDepature
		//{
		//	get
		//	{
		//		return "This request contains both Arrival/Depature AirportInfo.";
		//	}
		//}

		//public static string Taxi_Common_InvalidReservationServiceLocationDropoffAirportInfoArrival
		//{
		//	get
		//	{
		//		return "Invalid airportInfo at arrival";
		//	}
		//}

		//public static string Taxi_Common_InvalidReservationServiceLocationDropoffAirportInfoDepature
		//{
		//	get
		//	{
		//		return "Invalid airportInfo at depature";
		//	}
		//}

		//public static string Taxi_Common_InvalidReservationServiceLocationPickupDateTime(string dt)
		//{
		//	return string.Format("PickupDateTime is invalid. PickupDatetime={0}", dt);
		//}


		#endregion

		//public static string Taxi_Common_InvalidReservationServiceLocationInLatAndLong
		//{
		//	get
		//	{
		//		return "Invalid longtitude and latitude information";
		//	}
		//}
		#endregion

		//public static string Taxi_Common_DuplicatedTrips
		//{
		//	get
		//	{
		//		return "Duplicate trips found.";
		//	}
		//}

		//public static string Taxi_Common_InsufficientPrivilege(string username, long fleetId)
		//{
		//	return string.Format("Insufficient privilege. User={0}, FleetId={1}", username, fleetId);
		//}

		//public static string Taxi_Common_UnableToFindFleetByAddressOrAirportInfo
		//{
		//	get
		//	{
		//		return "Unable to find fleet by pickup address or airportInfo";
		//	}
		//}

		//public static string Taxi_Common_UnableProcessByPreviousInvalidRequest
		//{
		//	get
		//	{
		//		return "Unable to prcess this request";
		//	}
		//}

		//public static string Taxi_Common_UnableToFindFleetByFleetID
		//{
		//	get
		//	{
		//		return "Unable to find fleet by fleet ID";
		//	}
		//}

		//public static string Taxi_Common_UnableCreateTrip
		//{
		//	get
		//	{
		//		return "Cannot create or get this trip by invalid paramters";
		//	}
		//}


		#endregion

		//public static string Taxi_BookingFailure
		//{
		//	get
		//	{
		//		return "Sorry, we are unable to book this trip.";
		//	}
		//}

		//public static string Taxi_StatusFailure
		//{
		//	get
		//	{
		//		return "Sorry, we are unable to do trip status.";
		//	}
		//}

		//public static string Taxi_CancelFailure
		//{
		//	get
		//	{
		//		return "Sorry, we are unable to cancel this trip.";
		//	}
		//}

		//public static string Taxi_GetVehicleInfoFailure
		//{
		//	get
		//	{
		//		return "Sorry, we are unable to get vehicle information.";
		//	}
		//}

		//public static string Taxi_GetEstimationFailure
		//{
		//	get
		//	{
		//		return "Sorry, we are unable to get estimation.";
		//	}
		//}

		//public static string Taxi_UnableToConnectUDI33
		//{
		//	get
		//	{
		//		return "Sorry, we are unable to connect backend";
		//	}
		//}

		//public static string Taxi_UnableToSendMessageToVehicle
		//{
		//	get
		//	{
		//		return "Sorry, we are unable to send message to vehicle";
		//	}
		//}

		//public static string Taxi_TripNotFound
		//{
		//	get
		//	{
		//		return "Sorry, we are unable to find this trip.";
		//	}
		//}

		//public static string Taxi_NotifyDriverFailure
		//{
		//	get
		//	{
		//		return "Failure for notifying Driver for trip {0}";
		//	}
		//}

		//public static string Taxi_UnableToLoginGreenTomato
		//{
		//	get
		//	{
		//		return "Unable to login green tomato or doesn't get a valid JSESSIONID";
		//	}
		//}

		//public static string Taxi_UnableToCancelGreenTomato
		//{
		//	get
		//	{
		//		return "Unable to cancel this trip for green tomato";
		//	}
		//}

		//public static string Taxi_UnableToGetOrCreateContactGreenTomato
		//{
		//	get
		//	{
		//		return "Unable to get or create a contact in Green Tomato";
		//	}
		//}

		//public static string Taxi_UnableToGetServiceGreenTomato
		//{
		//	get
		//	{
		//		return "Unable to get a service in Green Tomato";
		//	}
		//}

		//public static string Taxi_UnableToGetReferenceTypeGreenTomato
		//{
		//	get
		//	{
		//		return "Unable to get reference type in Green Tomato";
		//	}
		//}

		//public static string ECar_Common_DuplicatedTrips
		//{
		//	get
		//	{
		//		return "Duplicate trips found.";
		//	}
		//}

		//public static string ECar_Common_UnableProcessByPreviousInvalidRequest
		//{
		//    get
		//    {
		//        return "Unable to process this request.";
		//    }
		//}

		//public static string ECar_Common_InvalidReservationServiceLocationDropoffContainsBothPickupAddressAndAirportInfo
		//{
		//	get
		//	{
		//		return "OTA booking request cannot contain both dropoff address and airport info.";
		//	}
		//}

		//public static string ECar_Common_InvalidReservationServiceLocationDropoffDetail
		//{
		//	get
		//	{
		//		return "The detail information in this dropoff address is not valid.";
		//	}
		//}


		//public static string ECar_Common_InvalidReservationServiceLocationDropoffDetailByMAPAPI(string address)
		//{

		//	return string.Format("The dropoff address is not valid by MAP API. Address={0}", address);

		//}

		//public static string ECar_Common_SkipReservationServiceLocationDropoffDetailByMAPAPI
		//{
		//	get
		//	{
		//		return "Skip MAP API Validation.";
		//	}
		//}

		//public static string ECar_Common_InvalidReservationServiceLocationNoMAPAPIWithEmptyLongAndLat(string address)
		//{

		//    return string.Format("If the booking request doesn't enable MAP API, it must provide longitude and latitude. Address={0}", address);

		//}

		//public static string ECar_Common_SkipReservationServiceLocationDropoffDetailByMAPAPI
		//{
		//	get
		//	{
		//		return "Skip MAP API Validation.";
		//	}
		//}

		//public static string ECar_Common_InvalidReservationServiceLocationDropoffAirportInfoContainsBothArrivalAndDepature
		//{
		//	get
		//	{
		//		return "This request contains both Arrival/Depature AirportInfo.";
		//	}
		//}

		//public static string ECar_Common_InvalidAddressTypeForFleet
		//{
		//	get
		//	{
		//		return "Invalid address type. Unable to send booking request.";
		//	}
		//}

		//public static string ECar_Common_InvalidAddressTypeForFleet_PickupFromAirport
		//{
		//	get
		//	{
		//		return "I'm sorry but we do not offer Black Car Service for this airport. Please use our Taxi service or go to the nearest vehicle service stand.";
		//	}
		//}

		//public static string ECar_Common_UnableToGetAvailableFleets
		//{
		//	get
		//	{
		//		return "Unable to get available fleets";
		//	}
		//}

		//public static string ECar_Common_UnableCreateTrip
		//{
		//	get
		//	{
		//		return "Cannot create or get this trip by invalid paramters";
		//	}
		//}

		//public static string ECar_Common_InvalidReservation
		//{
		//	get
		//	{
		//		return "Sorry, we are unable to find any reservation in your request.";
		//	}
		//}

		//public static string ECar_Status_InvalidCancelRequest
		//{
		//	get
		//	{
		//		return "Sorry, your cancel request is invalid.";
		//	}
		//}

		//public static string ECar_StatusFailure
		//{
		//	get
		//	{
		//		return "Sorry, we are unable to do trip status.";
		//	}
		//}

		//public static string ECar_UnableToLoginGreenTomato
		//{
		//	get
		//	{
		//		return "Unable to login green tomato or doesn't get a valid JSESSIONID";
		//	}
		//}

		//public static string ECar_UnableToGetReferenceTypeGreenTomato
		//{
		//	get
		//	{
		//		return "Unable to get reference type in Green Tomato";
		//	}
		//}

		//public static string ECar_UnableToGetServiceGreenTomato
		//{
		//	get
		//	{
		//		return "Unable to get a service in Green Tomato";
		//	}
		//}

		//public static string ECar_UnableToGetOrCreateContactGreenTomato
		//{
		//	get
		//	{
		//		return "Unable to get or create a contact in Green Tomato";
		//	}
		//}
		//public static string ECar_GetVehicleInfoFailure
		//{
		//	get
		//	{
		//		return "Sorry, we are unable to get vehicle information.";
		//	}
		//}

		//public static string ECar_UnableToCancelGreenTomato
		//{
		//	get
		//	{
		//		return "Unable to cancel this trip for green tomato";
		//	}
		//}

		#endregion

		#region Accounting

		//public static string Accounting_SetDefaultCreditCardAccountFailed
		//{
		//	get
		//	{
		//		return "Failed to Set Default Credit Card Account .";
		//	}
		//}

		//public static string Accounting_DeleteCreditCardAccountFailed
		//{
		//	get
		//	{
		//		return "Failed to Delete Credit Card Account.";
		//	}
		//}

		//public static string Accounting_DirectBillAccountNotExist
		//{
		//	get
		//	{
		//		return "The direct bill account number does not exist.";
		//	}
		//}

		//public static string Accounting_AddDirectBillAccountToCustomerProfileFailed
		//{
		//	get
		//	{
		//		return "Failed to Add Direct Bill Account.";
		//	}
		//}

		//public static string Accounting_SetDefaultDirectBillAccountFailed
		//{
		//	get
		//	{
		//		return "Failed to Set Default Direct Bill Account.";
		//	}
		//}

		//public static string Accounting_DeleteDirectBillAccountFailed
		//{
		//	get
		//	{
		//		return "Failed to Delete Direct Bill Account.";
		//	}
		//}

		//public static string Accounting_CreateMembershipMileageAccountFailed
		//{
		//	get
		//	{
		//		return "Failed to Create Membership Mileage Account.";
		//	}
		//}

		//public static string Accounting_DeleteMembershipMileageAccountFailed
		//{
		//	get
		//	{
		//		return "Failed to Delete Membership Mileage Account.";
		//	}
		//}

		//public static string Accounting_ReturnedNullTransactionID
		//{
		//	get
		//	{
		//		return "Returned transactionID is null";
		//	}
		//}

		#endregion

		#region Membership
		//public static string Membership_LinkReservation
		//{
		//	get
		//	{
		//		return "There is a problem in linking trips to SDS system.";
		//	}
		//}

		//public static string Membership_CreateMembership
		//{
		//	get
		//	{
		//		return "There was an issue while adding a new member to the SDS system";
		//	}
		//}

		//public static string Membership_WrongMemberID
		//{
		//	get
		//	{
		//		return "Wrong memberID";
		//	}
		//}

		//public static string Membership_MemberAddLocation
		//{
		//	get
		//	{
		//		return "There was an issue while adding a new location to the SDS system";
		//	}
		//}

		//public static string Membership_SendingEmailProblem
		//{
		//	get
		//	{
		//		return "Sorry, we cannot send an email now.";
		//	}
		//}

		//public static string Membership_WrongEmailAddress
		//{
		//	get
		//	{
		//		return "Please enter a correct email address.";
		//	}
		//}

		//public static string Membership_MemberChangeEmailAddress
		//{
		//	get
		//	{
		//		return "Sorry, we cannot change your email now.";
		//	}
		//}

		//public static string Membership_MemberChangePassword
		//{
		//	get
		//	{
		//		return "Sorry, we cannot change your password now";
		//	}
		//}

		//public static string Membership_MemberDeleteLocation
		//{
		//	get
		//	{
		//		return "There was an issue while deleting a location record from the SDS system";
		//	}
		//}

		//public static string Membership_MemberForgotPassword
		//{
		//	get
		//	{
		//		return "There was an issue while handling a forgotten password in the SDS system";
		//	}
		//}

		//public static string Membership_MemberGetLocations
		//{
		//	get
		//	{
		//		return "There was an issue while retrieving location records from the SDS system";
		//	}
		//}

		//public static string Membership_MemberGetReservation
		//{
		//	get
		//	{
		//		return "There was an issue while retrieving a member's reservation from the SDS system";
		//	}
		//}

		//public static string Membership_MemberGetLatestUnRatedTrip
		//{
		//	get
		//	{
		//		return "There was an issue while retrieving a member's most recent un rated trip";
		//	}
		//}

		//public static string Membership_MemberLogin
		//{
		//	get
		//	{
		//		return "Sorry, we cannot validate your membership";
		//	}
		//}

		//public static string Membership_MemberUpdateLocation
		//{
		//	get
		//	{
		//		return "There was an issue while updating a location record in the SDS system";
		//	}
		//}

		//public static string Membership_MemberUpdateProfile
		//{
		//	get
		//	{
		//		return "There was an issue while updating a member's profile in the SDS system";
		//	}
		//}


		#endregion

		#region Utility

		//public static string Utility_VehicleGetDefaultVehicleTypeForPoint
		//{
		//	get
		//	{
		//		return "There is a problem retrieving default vehicle type for the specified point from the SDS system";
		//	}
		//}

		//public static string Utility_UtilityGetWebServicedAirports
		//{
		//	get
		//	{
		//		return "There is a problem retrieving serviced airports from the SDS system";
		//	}
		//}

		//public static string Utility_UtilityGetCharterLocationsByType
		//{
		//	get
		//	{
		//		return "There is a problem charter locations by type from the SDS system";
		//	}
		//}

		//public static string Utility_LandmarkCharterSearchLandmark
		//{
		//	get
		//	{
		//		return "There was a problem while searching for charters in the SDS system";
		//	}
		//}

		//public static string Utility_UtilityGetAirportName
		//{
		//	get
		//	{
		//		return "There was a problem while searching for airport name";
		//	}
		//}

		#endregion

		#region Validation

		//public static string NullMemberID
		//{
		//	get
		//	{
		//		return "Sorry, you should pass a MemberID";
		//	}
		//}

		//public static string Validation_OTA_GroundAvailRQ_RateQualifiers
		//{
		//	get
		//	{
		//		return "Please send a correct RateQualifier";
		//	}
		//}

		//public static string Validation_SDS_Token
		//{
		//	get
		//	{
		//		return "You should send correct SDS token";
		//	}
		//}

		//public static string Validation_SDS_AddressSegments
		//{
		//	get
		//	{
		//		return "Please send valid address segments";
		//	}
		//}

		//public static string Validation_SDS_OutboundPickupTime
		//{
		//	get
		//	{
		//		return "We do not support pickup time for outbound trip in this version. Pickup time is the same as flight time";
		//	}
		//}

		//public static string Validation_SDS_NullReference
		//{
		//	get
		//	{
		//		return "You should send reference ID";
		//	}
		//}

		//public static string Validation_Taxi_Token
		//{
		//	get
		//	{
		//		return "You should send correct username and password";
		//	}
		//}

		//public static string Validation_OTA_GroundReservation_Null
		//{
		//	get
		//	{
		//		return "You should send GroundReservation object";
		//	}
		//}

		//public static string Validation_OTA_Service_Null
		//{
		//	get
		//	{
		//		return "You should send Service object";
		//	}
		//}

		//public static string Validation_OTA_Service_Fleet_Type
		//{
		//	get
		//	{
		//		return "You should send ServiceLevel and VehicleType";
		//	}
		//}

		//public static string Validation_OTA_UniqueID_Null
		//{
		//	get
		//	{
		//		return "You should send UniqueID object";
		//	}
		//}

		//public static string Validation_NullPayment
		//{
		//	get
		//	{
		//		return "Null Payment";
		//	}
		//}

		//public static string Validation_Address
		//{
		//	get
		//	{
		//		return "Please send valid address";
		//	}
		//}

		//public static string Validation_PickupTime
		//{
		//	get
		//	{
		//		return "Pickup time is missing";
		//	}
		//}

		//public static string Validation_ExpiredPickupTime
		//{
		//	get
		//	{
		//		return "The pickup time has already expired.";
		//	}
		//}

		//public static string Validation_Accounting_InvalidMemberId
		//{
		//	get
		//	{
		//		return "MemberId is invalid.";
		//	}
		//}

		//public static string Validation_Accounting_InvalidAccountId
		//{
		//	get
		//	{
		//		return "AccountId is invalid.";
		//	}
		//}

		//public static string Validation_Trip_UserName
		//{
		//	get
		//	{
		//		return "Sorry, you are not allowed to do this request.";
		//	}
		//}

		//public static string Validation_Accounting_MissingCreditCardInformation
		//{
		//	get
		//	{
		//		return "Invalid field(s). The BillingAddress, BillingCity, BillingName, BillingState, BillingZipCode, CardType, CreditCardNumber, ExpirationDate and CvvNumber are required.";
		//	}
		//}

		//public static string Validation_Accounting_InvalidZipCode
		//{
		//	get
		//	{
		//		return "Invalid ZipCode.";
		//	}
		//}

		//public static string Validation_Accounting_InvalidAccountNumber
		//{
		//	get
		//	{
		//		return "Invalid AccountNumber.";
		//	}
		//}

		//public static string Validation_Accounting_InvalidRewardIds
		//{
		//	get
		//	{
		//		return "Invalid RewardIds.";
		//	}
		//}

		//public static string Validation_Accounting_InvalidActivationCode
		//{
		//	get
		//	{
		//		return "Invalid ActivationCode.";
		//	}
		//}

		//public static string Validation_GroundAvailableFleet_Address
		//{
		//	get
		//	{
		//		return "Country, State, City, latitude, and longitude are required!";
		//	}
		//}
		#endregion
		
		#region ECar
		public static string ECar_Common_NoServiceAPIPreference
		{
			get
			{
				return "Unable to find service api preference.";
			}
		}


		public static string ECar_Common_InvalidBookingRequest
		{
			get
			{
				return "Sorry, your booking request is invalid.";
			}
		}

		public static string ECar_BookingFailure
		{
			get
			{
				return "Sorry, we are unable to book this trip.";
			}
		}

		public static string ECar_CancelFailure
		{
			get
			{
				return "Sorry, we are unable to cancel this trip.";
			}
		}

		public static string ECar_Common_InvalidReservationPassengerPrimaryPersonNameDetail
		{
			get
			{
				return "Sorry, we are unable to find any detail part of primary passenger in your request.";
			}
		}

		public static string ECar_Common_ValidReservationPassengerPrimaryPersonNameDetail
		{
			get
			{
				return "First name and last name are valid.";
			}
		}

		public static string ECar_Common_InvalidReservationPassengerPrimaryPersonName
		{
			get
			{
				return "Sorry, we are unable to find any person name of primary passenger in your request.";
			}
		}

		public static string ECar_Common_ValidReservationPassengerPrimaryTelephones
		{
			get
			{
				return "Phone number(s) is/are valid.";
			}
		}

        public static string ECar_Common_ValidReservationPassengerPrimaryEmails
        {
            get
            {
                return "Email address(es) is/are valid.";
            }
        }

		public static string ECar_Common_InvalidReservationPassengerPrimaryTelephones
		{
			get
			{
				return "Sorry, your phone number(s) is/are invalid.";
			}
		}

        public static string ECar_Common_InvalidReservationPassengerPrimaryEmails
        {
            get
            {
                return "Sorry, your email address(es) is/are invalid.";
            }
        }

		public static string ECar_Common_InvalidReservationPassengerPrimaryPhoneNumber
		{
			get
			{
				return "Sorry, we are unable to find any phone numbers of primary passenger in your request.";
			}
		}

		public static string ECar_Common_UnableToCreateOrFindECarCustomer
		{
			get
			{
				return "Unable to create E-Car customer record for this reqest.";
			}
		}

		public static string ECar_Common_InvalidReservationPassengerPrimary
		{
			get
			{
				return "Sorry, we are unable to find any primary passenger in your request.";
			}
		}

		public static string ECar_Common_InvalidReservationPassenger
		{
			get
			{
				return "Sorry, we are unable to find any passenger in your request.";
			}
		}

		public static string ECar_Common_InvalidReservationServiceLocationPickupContainsBothPickupAddressAndAirportInfo
		{
			get
			{
				return "OTA booking request cannot contain both pickup address and airport info.";
			}
		}
        public static string ECar_Common_InvalidReservationServiceLocationDropoffContainsBothDropoffAddressAndAirportInfo
        {
            get
            {
                return "OTA booking request cannot contain both Dropoff address and airport info.";
            }
        }

        public static string ECar_Common_InvalidReservationServiceLocationPickupDetail
		{
			get
			{
				return "The detail information in this pickup address is not valid.";
			}
		}
        public static string ECar_Common_InvalidReservationServiceLocationDropoffDetail
        {
            get
            {
                return "The detail information in this dropoff address is not valid.";
            }
        }
        public static string ECar_Common_InvalidReservationServiceLocationPickupDetailByMAPAPI(string address)
		{

			return string.Format("The pickup address is not valid by MAP API. Address={0}", address);

		}
        public static string ECar_Common_InvalidReservationServiceLocationDropoffDetailByMAPAPI(string address)
        {

            return string.Format("The Dropoff address is not valid by MAP API. Address={0}", address);

        }
        public static string ECar_Common_SkipReservationServiceLocationPickupDetailByMAPAPI
		{
			get
			{
				return "Skip MAP API Validation.";
			}
		}
        public static string ECar_Common_SkipReservationServiceLocationDropoffDetailByMAPAPI
        {
            get
            {
                return "Skip MAP API Validation.";
            }
        }
        public static string ECar_Common_InvalidReservationServiceLocationNoMAPAPIWithEmptyLongAndLat(string address)
		{

			return string.Format("If the booking request doesn't enable MAP API, it must provide longitude and latitude. Address={0}", address);

		}

		public static string ECar_Common_UnknownError
		{
			get
			{
				return "Unknown Error";
			}
		}

		public static string ECar_Common_InvalidReservationServiceLocationPickupAirportInfoContainsBothArrivalAndDepature
		{
			get
			{
				return "This request contains both Arrival/Depature AirportInfo.";
			}
		}
        public static string ECar_Common_InvalidReservationServiceLocationDropoffAirportInfoContainsBothArrivalAndDepature
        {
            get
            {
                return "This request contains both Arrival/Depature AirportInfo.";
            }
        }
        public static string ECar_Common_UnableToFindAirportLongitudeAndLatitude
		{
			get
			{
				return "Unable to find Lat.&Long by aiport code";
			}
		}

		public static string ECar_Common_InvalidReservationServiceLocationPickup
		{
			get
			{
				return "Sorry, we are unable to find Reservation.Service.Location.Pickup element in your request.";
			}
		}

		public static string ECar_Common_InvalidReservationServiceLocation
		{
			get
			{
				return "Sorry, we are unable to find Reservation.Service.Location element in your request.";
			}
		}

		public static string ECar_Common_InvalidReservationService
		{
			get
			{
				return "Sorry, we are unable to find Reservation.Service element in your request.";
			}
		}

		public static string ECar_Common_UnableToFindFleetByAddressOrAirportInfo
		{
			get
			{
				return "Unable to find fleet by pickup address or airportInfo";
			}
		}

		public static string ECar_Common_UnableToFindFleetByDispatchCodeAndConsumerCode
		{
			get
			{
				return "Unable to find fleet by DipatchCode and ConsumerCode";
			}
		}

		public static string ECar_Common_UnableToFindFleetByAddressOrAirportInfoOrDispatchCodeAndConsumerCode
		{
			get
			{
				return "Unable to find fleet by pickup address or airportInfo or DipatchCode and ConsumerCode";
			}
		}

		public static string ECar_Common_UnableProcessByPreviousInvalidRequest
		{
			get
			{
				return "Unable to find fleet by pickup address or airportInfo";
			}
		}

		public static string ECar_Common_InsufficientPrivilege(string username, long fleetId)
		{
			return string.Format("Insufficient privilege. User={0}, FleetId={1}", username, fleetId);
		}

		public static string ECar_Common_InvalidReservationServiceLocationPickupDateTime(string dt)
		{
			return string.Format("PickupDateTime is invalid. PickupDatetime={0}", dt);
		}

		public static string ECar_Common_UnableToCorrectDispatchCodeAndConsumerCode
		{
			get
			{
				return "Unable to find dispatch code and consumer code";
			}
		}

		public static string ECar_Common_UnableToFindFleetByPickupAddress
		{
			get
			{
				return "Unable to find fleet by pickup address";
			}
		}


		public static string ECar_Common_DuplicatedFleetRelationSetting
		{
			get
			{
				return "Found duplicated ecar fleet relation setting";
			}
		}

		public static string ECar_GetEstimationFailure
		{
			get
			{
				return "Sorry, we are unable to get estimation.";
			}
		}

		public static string ECar_InvalidFleetUserAccountSetting
		{
			get
			{
				return "Unable to find account setting for this fleet";
			}
		}
        public static string ECar_Common_InvalidPaymentCard
        {
            get
            {
                return "Sorry, we are unable to find Payment Card Information in your request.";
            }
        }

        public static string Aleph_Invalid_CorporateInputs
        {
            get
            {
                return "Sorry, Corporate Inputs are not valid";
            }
        }
        #endregion
    }
}
