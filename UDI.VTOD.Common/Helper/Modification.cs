﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.VTOD.Common.DTO.OTA;
using UDI.Utility.Helper;

namespace UDI.VTOD.Common.Helper
{
	public static class Modification
	{
		public static void ModifyTokenRQ(ref TokenRQ input)
		{
			input.ClientIPAddress = System.Configuration.ConfigurationManager.AppSettings["SDS_TokenClientIPAddress"];
			input.ClientType = System.Configuration.ConfigurationManager.AppSettings["SDS_TokenClientType"].ToInt32();
		}

		public static void ModifyTokenRS(ref TokenRS input)
		{
			input.ClientIPAddress = System.Configuration.ConfigurationManager.AppSettings["SDS_TokenClientIPAddress"];
			input.ClientType = System.Configuration.ConfigurationManager.AppSettings["SDS_TokenClientType"].ToInt32();
		}

		public static void ModifyTokenRSForConsumer(ref TokenRS input)
		{
			input.ClientIPAddress = null;
			input.ClientType = null;
		}
	}
}
