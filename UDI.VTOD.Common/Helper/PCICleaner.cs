﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Common.Helper
{
	public static class PCICleaner
	{
		public static string CleanPassword(this string input)
		{
			var s = input.IndexOf("<Password");
			var e = input.IndexOf("</Password>");

			var startIndex = s + 9;
			var endIndex = e;

			if (s >= 0 && e >= 0 & endIndex > startIndex)
			{
				var pass = input.Substring(startIndex, endIndex - startIndex);
				return input.Replace(pass, "******");
			}
			else
			{
				return input;
			}
		}

		public static string CleanSecurityKey(this string input)
		{
			var s = input.IndexOf("<SecurityKey");
			var e = input.IndexOf("</SecurityKey>");

			var startIndex = s + 12;
			var endIndex = e;

			if (s >= 0 && e >= 0 & endIndex > startIndex)
			{
				var pass = input.Substring(startIndex, endIndex - startIndex);
				return input.Replace(pass, "******");
			}
			else
			{
				return input;
			}
		}

		public static string CleanCardNumber(this string input)
		{
			var s = input.IndexOf("<CardNumber");
			var e = input.IndexOf("</CardNumber>");

			var startIndex = s + 11;
			var endIndex = e;

			if (s >= 0 && e >= 0 & endIndex > startIndex)
			{
				var pass = input.Substring(startIndex, endIndex - startIndex);
				return input.Replace(pass, "******");
			}
			else
			{
				return input;
			}
		}

		public static string CleanSeriesCode(this string input)
		{
			var s = input.IndexOf("<SeriesCode");
			var e = input.IndexOf("</SeriesCode>");

			var startIndex = s + 11;
			var endIndex = e;

			if (s >= 0 && e >= 0 & endIndex > startIndex)
			{
				var pass = input.Substring(startIndex, endIndex - startIndex);
				return input.Replace(pass, "******");
			}
			else
			{
				return input;
			}
		}

		public static string CleanCVVNumber(this string input)
		{
			var s = input.IndexOf("<CVVNumber");
			var e = input.IndexOf("</CVVNumber>");

			var startIndex = s + 11;
			var endIndex = e;

			if (s >= 0 && e >= 0 & endIndex > startIndex)
			{
				var pass = input.Substring(startIndex, endIndex - startIndex);
				return input.Replace(pass, "******");
			}
			else
			{
				return input;
			}
		}

		public static string CleanCreditCardNumber(this string input)
		{
			var s = input.IndexOf("<CreditCardNumber");
			var e = input.IndexOf("</CreditCardNumber>");

			var startIndex = s + 11;
			var endIndex = e;

			if (s >= 0 && e >= 0 & endIndex > startIndex)
			{
				var pass = input.Substring(startIndex, endIndex - startIndex);
				return input.Replace(pass, "******");
			}
			else
			{
				return input;
			}
		}

	}
}
