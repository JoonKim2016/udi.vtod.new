﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO.Enum;

namespace UDI.VTOD.Common.Track
{
	public class TrackTime : BaseSetting, IDisposable
	{
		#region Constructor
		public TrackTime()
		{
			if (isTrack)
			{
				Stopwatch = new Stopwatch();
				Stopwatch.Start();
			}

			this.TrackTimeItems = new List<TrackTimeItem>();
			this.Owner = LogOwnerType.Unknown;
			this.TransactionID = 0; //Initiate
			this.ThreadID = System.Threading.Thread.CurrentThread.ManagedThreadId;
		}

		public TrackTime(LogOwnerType owner, Method method)
			: this()
		{
			Owner = owner;
			MethodName = method.ToString();
		}
		#endregion

		#region XML
		[XmlAttribute]
		public string MethodName { get; set; }

		[XmlAttribute]
		public int ThreadID { get; set; }

		[XmlAttribute]
		public long TransactionID { get; set; }

		[XmlAttribute]
		public long EM { get; set; }

		[XmlAttribute]
		public LogOwnerType Owner { get; set; }

		public List<TrackTimeItem> TrackTimeItems { get; set; }
		#endregion

		#region Properties
		[XmlIgnore]
		public Stopwatch Stopwatch { get; set; }
		#endregion

		#region Method
		public void AddTrackTime(LogOwnerTypeDetail owner, string action)
		{
			if (isTrack)
			{
				var em = Stopwatch.ElapsedMilliseconds;
				TrackTimeItem last = null;
				int id = 0;
				long duration;
				if (TrackTimeItems.Any())
				{
					last = TrackTimeItems.Last();
					duration = em - last.EM;
					id = last.ID + 1;
				}
				else
				{
					duration = em;
					id = 1;
				}
				var trackTime = new TrackTimeItem { EM = em, Action = action, Owner = owner, Duration = duration, ID = id };
				TrackTimeItems.Add(trackTime);
			}

			if (isDebug)
			{
				logger.InfoFormat("owner:{0}, action:{1}", owner, action);
			}
		}

		public string GetQuickAnalysis()
		{
			var result = string.Empty;
			if (isTrack)
			{
				var group = TrackTimeItems.GroupBy(s => s.Owner).Select(s => new { Duration = s.Sum(sum => sum.Duration), Owner = s.Key }).ToList();
				foreach (var item in group)
				{
					result += string.Format("{0}:{1},", item.Owner, item.Duration);
				}
			}
			return result;
		}

		#endregion

		#region Dispose
		private bool isDisposed = false;

		~TrackTime()
		{
			Dispose(false);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		public virtual void Dispose(bool disposing)
		{
			if (!isDisposed)
			{
				// Released unmanaged Resources
				if (disposing)
				{
					// Released managed Resources
					if (isTrack)
					{
						Stopwatch.Stop();
					}
				}
				isDisposed = true;
			}
		}
		#endregion
	}
}
