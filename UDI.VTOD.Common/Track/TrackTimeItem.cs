﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.VTOD.Common.DTO.Enum;

namespace UDI.VTOD.Common.Track
{
	public class TrackTimeItem
	{
		public int ID { get; set; }
		/// <summary>
		/// EM = ElapsedMilliseconds
		/// </summary>
		public long EM { get; set; }
		public long Duration { get; set; }
		public LogOwnerTypeDetail Owner { get; set; }
		public string Action { get; set; }
	}
}
