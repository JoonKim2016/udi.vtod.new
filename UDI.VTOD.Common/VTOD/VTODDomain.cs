﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Common.Track;
using UDI.VTOD.DataAccess.VTOD;
using UDI.Utility.Serialization;
using UDI.Utility.Helper;

namespace UDI.VTOD.Domain.VTOD
{
	public class VTODDomain : BaseSetting
	{
		#region Constructors
		public VTODDomain()
		{
			OwnerType = LogOwnerType.VTOD;
		}
		#endregion

		public long InsertTransaction(string methodName, string username, string callerIP, string request, string response, System.DateTime appendTime, System.DateTime? updateTime)
		{
			long result;
			using (var db = new VTODEntities())
			{
				result = db.SP_vtod_InsertTransaction(methodName, username, callerIP, request, response, appendTime, updateTime).ToList().First().Value;
			}

			return result;
		}

		public void UpdateTransaction(long id, string trackId, string response, System.DateTime updateTime)
		{
			using (var db = new VTODEntities())
			{
				db.SP_vtod_UpdateTransaction(id, trackId, response, updateTime);
			}
		}

		public long LogInDB(LogOwnerType owner, string content, LogType type, string subType, DateTime appendTime)
		{
			long result;

			using (var db = new VTODEntities())
			{
				result = db.SP_vtod_InsertLog(owner.ToString(), content, type.ToString(), subType, appendTime).ToList().First().Value;
			}

			return result;
		}

		public long InsertTrack(TrackTime trackTime)
		{
			var result = 0;
			if (isTrack)
			{
				var em = trackTime.Stopwatch.ElapsedMilliseconds;
				trackTime.EM = em;
				var xml = trackTime.XmlSerialize().ToString();
				var quickAnalysis = trackTime.GetQuickAnalysis();

				long? transactionID = null;
				if (trackTime.TransactionID != 0)
					transactionID = trackTime.TransactionID;

				using (var db = new VTODEntities())
				{
					db.SP_vtod_InsertTrack(transactionID, trackTime.MethodName, trackTime.ThreadID, em, trackTime.Owner.ToString(), quickAnalysis, xml, DateTime.Now);
				}
			}

			return result;
		}
	}
}
