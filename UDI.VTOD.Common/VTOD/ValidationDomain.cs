﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.Track;
using UDI.VTOD.Common.Helper;
using UDI.Utility.Helper;

namespace UDI.VTOD.Domain.VTOD
{
	internal class ValidationDomain : BaseSetting
	{

		internal bool Validate_GetToken(TokenRQ input, out string error)
		{
			var result = true;
			error = string.Empty;

			if (input == null)
			{
				error = Messages.Validation_Request_Null;
				return false;
			}

			#region Check Version
			if (!validate_Version(input.Version, out error))
			{
				return false;
			} 
			#endregion

			return result;
		}

		internal bool Validate_OTA_GroundAvailRQ(OTA_GroundAvailRQ input, out string error)
		{
			var result = true;
			error = string.Empty;
			
			if (input == null)
			{
				error = Messages.Validation_Request_Null;
				return false;
			}

			#region Check Version
			if (!validate_Version(input.Version, out error))
			{
				return false;
			}
			#endregion

			#region RateQualifiers
			if (input.RateQualifiers == null || !input.RateQualifiers.Any() || string.IsNullOrWhiteSpace(input.RateQualifiers.First().RateQualifierValue))
			{
				error = Messages.Validation_OTA_GroundAvailRQ_RateQualifiers;
				return false;
			}

			var rateQualifier = input.RateQualifiers.First().RateQualifierValue;
			var rateQualifiers = Enum.GetNames(typeof(Common.DTO.Enum.FleetType)).ToList();
			if (!rateQualifiers.Contains(rateQualifier))
			{
				error = Messages.Validation_OTA_GroundAvailRQ_RateQualifiers;
				return false;
			}
			#endregion

			return result;
		}

		internal bool Validate_OTA_GroundBookRQ(OTA_GroundBookRQ input, out string error)
		{
			var result = true;
			error = string.Empty;
			
			if (input == null)
			{
				error = Messages.Validation_Request_Null;
				return false;
			}

			#region Check Version
			if (!validate_Version(input.Version, out error))
			{
				return false;
			}
			#endregion

			if (input.GroundReservations == null || !input.GroundReservations.Any())
			{
				error = Messages.Validation_OTA_GroundReservation_Null;
				return false;
			}

			if (input.GroundReservations.First().RateQualifiers == null || !input.GroundReservations.First().RateQualifiers.Any() || string.IsNullOrWhiteSpace(input.GroundReservations.First().RateQualifiers.First().RateQualifierValue))
			{
				error = Messages.Validation_OTA_GroundAvailRQ_RateQualifiers;
				return false;
			}

			return result;
		}

		internal bool Validate_OTA_GroundCancelRQ(OTA_GroundCancelRQ input, out string error)
		{
			var result = true;
			error = string.Empty;

			if (input == null)
			{
				error = Messages.Validation_Request_Null;
				return false;
			}

			#region Check Version
			if (!validate_Version(input.Version, out error))
			{
				return false;
			}
			#endregion

			if (input.Reservation == null || input.Reservation.UniqueID == null || !input.Reservation.UniqueID.Any())
			{
				error = Messages.Validation_OTA_UniqueID_Null;
				return false;
			}

			if (input.TPA_Extensions == null || input.TPA_Extensions.RateQualifiers == null || !input.TPA_Extensions.RateQualifiers.Any() || string.IsNullOrWhiteSpace(input.TPA_Extensions.RateQualifiers.First().RateQualifierValue))
			{
				error = Messages.Validation_OTA_GroundAvailRQ_RateQualifiers;
				return false;
			}
			return result;
		}

		internal bool Validate_OTA_GroundResRetrieveRQ(OTA_GroundResRetrieveRQ input, out string error)
		{
			var result = true;
			error = string.Empty;
			
			if (input == null)
			{
				error = Messages.Validation_Request_Null;
				return false;
			}

			#region Check Version
			if (!validate_Version(input.Version, out error))
			{
				return false;
			}
			#endregion

			if (input.Reference == null || string.IsNullOrWhiteSpace(input.Reference.ID))
			{
				error = Messages.Validation_OTA_UniqueID_Null;
				return false;
			}

			if (input.TPA_Extensions == null || input.TPA_Extensions.RateQualifiers == null || !input.TPA_Extensions.RateQualifiers.Any() || string.IsNullOrWhiteSpace(input.TPA_Extensions.RateQualifiers.First().RateQualifierValue))
			{
				error = Messages.Validation_OTA_GroundAvailRQ_RateQualifiers;
				return false;
			}
			return result;
		}

		internal bool Validate_SDS_Token(TokenRS input, out string error)
		{
			var result = true;
			error = string.Empty;

			if (input == null || string.IsNullOrWhiteSpace(input.Username) || string.IsNullOrWhiteSpace(input.SecurityKey) || input.SecurityKey.ToGuid() == Guid.Empty)
			{
				error = Messages.Validation_SDS_Token;
				result = false;
			}

			return result;
		}

		//internal bool Validate_Taxi_Token(TokenRS input, out string error)
		//{
		//	var result = true;
		//	error = string.Empty;

		//	if (input == null || string.IsNullOrWhiteSpace(input.SecurityKey) || string.IsNullOrWhiteSpace(input.Username))
		//	{
		//		error = Messages.Validation_Taxi_Token;
		//		result = false;
		//	}

		//	return result;
		//}

		#region private
		private bool validate_Version(string version, out string error)
		{
			var result = true;
			error = string.Empty;

			var configVersion = System.Configuration.ConfigurationManager.AppSettings["Version"].Trim().ToLower();

			if (string.IsNullOrWhiteSpace(version) || version.Trim().ToLower() != configVersion)
			{
				error = Messages.Validation_Version;
				return false;
			}
			return result;
		}
		#endregion
	}
}
