﻿namespace UDI.VTOD.Converter
{
	partial class ConverterForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.txtXML = new System.Windows.Forms.TextBox();
			this.txtJSON = new System.Windows.Forms.TextBox();
			this.btnToJSON = new System.Windows.Forms.Button();
			this.btnToXML = new System.Windows.Forms.Button();
			this.dlstType = new System.Windows.Forms.ComboBox();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 44);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(40, 17);
			this.label1.TabIndex = 0;
			this.label1.Text = "XML:";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(761, 44);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(49, 17);
			this.label2.TabIndex = 1;
			this.label2.Text = "JSON:";
			// 
			// txtXML
			// 
			this.txtXML.Location = new System.Drawing.Point(15, 64);
			this.txtXML.Multiline = true;
			this.txtXML.Name = "txtXML";
			this.txtXML.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.txtXML.Size = new System.Drawing.Size(633, 622);
			this.txtXML.TabIndex = 2;
			// 
			// txtJSON
			// 
			this.txtJSON.Location = new System.Drawing.Point(764, 64);
			this.txtJSON.Multiline = true;
			this.txtJSON.Name = "txtJSON";
			this.txtJSON.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.txtJSON.Size = new System.Drawing.Size(643, 622);
			this.txtJSON.TabIndex = 3;
			// 
			// btnToJSON
			// 
			this.btnToJSON.Location = new System.Drawing.Point(655, 64);
			this.btnToJSON.Name = "btnToJSON";
			this.btnToJSON.Size = new System.Drawing.Size(103, 297);
			this.btnToJSON.TabIndex = 4;
			this.btnToJSON.Text = "To JSON >";
			this.btnToJSON.UseVisualStyleBackColor = true;
			this.btnToJSON.Click += new System.EventHandler(this.btnToJSON_Click);
			// 
			// btnToXML
			// 
			this.btnToXML.Location = new System.Drawing.Point(654, 367);
			this.btnToXML.Name = "btnToXML";
			this.btnToXML.Size = new System.Drawing.Size(104, 319);
			this.btnToXML.TabIndex = 5;
			this.btnToXML.Text = "< To XML";
			this.btnToXML.UseVisualStyleBackColor = true;
			this.btnToXML.Click += new System.EventHandler(this.btnToXML_Click);
			// 
			// dlstType
			// 
			this.dlstType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.dlstType.FormattingEnabled = true;
			this.dlstType.Items.AddRange(new object[] {
            "OTA_GroundAvailRQ",
            "OTA_GroundAvailRS",
            "OTA_GroundBookRQ",
            "OTA_GroundAvailRS",
            "OTA_GroundCancelRQ",
            "OTA_GroundCancelRS",
            "OTA_GroundResRetrieveRQ",
            "OTA_GroundResRetrieveRS",
            "TokenRQ",
            "TokenRS"});
			this.dlstType.Location = new System.Drawing.Point(15, 13);
			this.dlstType.Name = "dlstType";
			this.dlstType.Size = new System.Drawing.Size(633, 24);
			this.dlstType.TabIndex = 7;
			// 
			// ConverterForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1419, 698);
			this.Controls.Add(this.dlstType);
			this.Controls.Add(this.btnToXML);
			this.Controls.Add(this.btnToJSON);
			this.Controls.Add(this.txtJSON);
			this.Controls.Add(this.txtXML);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.MaximizeBox = false;
			this.Name = "ConverterForm";
			this.Text = "ConverterForm";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox txtXML;
		private System.Windows.Forms.TextBox txtJSON;
		private System.Windows.Forms.Button btnToJSON;
		private System.Windows.Forms.Button btnToXML;
		private System.Windows.Forms.ComboBox dlstType;
	}
}