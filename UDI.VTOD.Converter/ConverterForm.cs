﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using Newtonsoft.Json.Linq;
using UDI.Utility.Helper;
using UDI.Utility.Serialization;
using UDI.VTOD.Common.DTO.OTA;

namespace UDI.VTOD.Converter
{
	public partial class ConverterForm : Form
	{
		public ConverterForm()
		{
			InitializeComponent();
			dlstType.SelectedIndex = 0;
		}

		private void btnToJSON_Click(object sender, EventArgs e)
		{
			var result = string.Empty;

			try
			{
				switch (dlstType.SelectedIndex)
				{
					case 0:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<OTA_GroundAvailRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<OTA_GroundAvailRQ>();
						}
						break;
					case 1:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<OTA_GroundAvailRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<OTA_GroundAvailRS>();
						}
						break;
					case 2:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<OTA_GroundBookRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<OTA_GroundBookRQ>();
						}
						break;
					case 3:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<OTA_GroundAvailRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<OTA_GroundAvailRS>();
						}
						break;
					case 4:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<OTA_GroundCancelRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<OTA_GroundCancelRQ>();
						}
						break;
					case 5:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<OTA_GroundCancelRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<OTA_GroundCancelRS>();
						}
						break;
					case 6:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<OTA_GroundResRetrieveRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<OTA_GroundResRetrieveRQ>();
						}
						break;
					case 7:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<OTA_GroundResRetrieveRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<OTA_GroundResRetrieveRS>();
						}
						break;
					case 8:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<TokenRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<TokenRQ>();
						}
						break;
					case 9:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<TokenRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<TokenRS>();
						}
						break;
				}
			}
			catch (Exception ex)
			{
				result = ex.Message;
			}

			JObject json = JObject.Parse(result);
			txtJSON.Text = json.ToString();
		}

		private void btnToXML_Click(object sender, EventArgs e)
		{
			var result = string.Empty;

			try
			{
				switch (dlstType.SelectedIndex)
				{
					case 0:
						{
							//var xdoc = XDocument.Parse(txtJSON.Text).CleanComment().CleanNamespace();
							var obj = txtJSON.Text.JsonDeserialize<OTA_GroundAvailRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<OTA_GroundAvailRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 1:
						{
							var obj = txtJSON.Text.JsonDeserialize<OTA_GroundAvailRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<OTA_GroundAvailRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 2:
						{
							var obj = txtJSON.Text.JsonDeserialize<OTA_GroundBookRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<OTA_GroundBookRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 3:
						{
							var obj = txtJSON.Text.JsonDeserialize<OTA_GroundAvailRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<OTA_GroundAvailRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 4:
						{
							var obj = txtJSON.Text.JsonDeserialize<OTA_GroundCancelRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<OTA_GroundCancelRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 5:
						{
							var obj = txtJSON.Text.JsonDeserialize<OTA_GroundCancelRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<OTA_GroundCancelRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 6:
						{
							var obj = txtJSON.Text.JsonDeserialize<OTA_GroundResRetrieveRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<OTA_GroundResRetrieveRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 7:
						{
							var obj = txtJSON.Text.JsonDeserialize<OTA_GroundResRetrieveRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<OTA_GroundResRetrieveRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 8:
						{
							var obj = txtJSON.Text.JsonDeserialize<TokenRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<TokenRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 9:
						{
							var obj = txtJSON.Text.JsonDeserialize<TokenRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<TokenRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
				}
			}
			catch (Exception ex)
			{
				result = ex.Message;
			}

			txtXML.Text = result;
		}
	}
}
