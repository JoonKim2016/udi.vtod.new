﻿using System;
using System.IO;
using System.Text;
using System.Web.Script.Serialization; // Add reference: System.Web.Extensions
using System.Xml;
using System.Xml.Serialization;

namespace Helpers
{
    internal static class ParseHelpers
    {
        private static JavaScriptSerializer json;
        private static JavaScriptSerializer JSON { get { return json ?? (json = new JavaScriptSerializer()); } }

        public static Stream ToStream(this string @this)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(@this);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }


        public static T ParseXML<T>(this string @this) where T : class
        {
            var reader = XmlReader.Create(@this.Trim().ToStream(), new XmlReaderSettings() { ConformanceLevel = ConformanceLevel.Document });
            return new XmlSerializer(typeof(T)).Deserialize(reader) as T;
        }

        public static T ParseJSON<T>(this string @this) where T : class
        {
            return JSON.Deserialize<T>(@this.Trim());
        }

        public static string SerializeJSON(this object @this)
        {
            return JSON.Serialize(@this).Trim();
        }

        public static string Serialize(this object @o)
        {
            try
            {
                XmlSerializer ser = new XmlSerializer(@o.GetType(), "");
                StringBuilder sb = new StringBuilder();
                var xns = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
                XmlWriterSettings xws = new XmlWriterSettings();
                xws.OmitXmlDeclaration = true;
                using (XmlWriter writer = XmlWriter.Create(sb, xws))
                {
                    ser.Serialize(writer, o, xns);
                }
                return sb.ToString();
            }
            catch
            {
                return null;
            }

        }

        public static T Deserialize<T>(this string @s)
        {
            try
            {
                object obj = null;
                XmlSerializer ser = new XmlSerializer(typeof(T));
                byte[] byteArray = Encoding.UTF8.GetBytes(@s);
                MemoryStream ms = new MemoryStream(byteArray);
                obj = ser.Deserialize(ms);
                return (T)obj;
            }
            catch
            {
                return default(T);
            }
        }

        public static string IndentXMLString(this object @o)
        {

            string xml = @o.Serialize();

            using (MemoryStream ms = new MemoryStream())
            {
                // Create a XMLTextWriter that will send its output to a memory stream (file)
                using (XmlTextWriter xtw = new XmlTextWriter(ms, Encoding.Unicode))
                {
                    XmlDocument doc = new XmlDocument();
                    try
                    {
                        // Load the unformatted XML text string into an instance 
                        // of the XML Document Object Model (DOM)
                        doc.LoadXml(xml);
                        // Set the formatting property of the XML Text Writer to indented
                        // the text writer is where the indenting will be performed
                        xtw.Formatting = Formatting.Indented;
                        // write dom xml to the xmltextwriter
                        doc.PreserveWhitespace = true;
                        doc.WriteContentTo(xtw);
                        // Flush the contents of the text writer
                        // to the memory stream, which is simply a memory file
                        xtw.Flush();
                        // set to start of the memory stream (file)
                        ms.Seek(0, SeekOrigin.Begin);
                        // create a reader to read the contents of 
                        // the memory stream (file)
                        using (StreamReader sr = new StreamReader(ms))
                        {
                            // return the formatted string to caller
                            return sr.ReadToEnd();
                        }
                    }
                    catch 
                    {
                        return string.Empty;
                    }
                }
            }
        }

    }
}