﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Helpers;

namespace UDI.VTOD.Utilities.CorrectCounterClockwisePolygon
{
    class Program
    {
        static void Main(string[] args)
        {
            #region load file
            string fileContent=string.Empty;
            using (StreamReader sr = new StreamReader(@"C:\Baltimore.gpx"))
            {
                while (sr.Peek() != -1) 
                {
                    fileContent=sr.ReadToEnd();
                }
            }

            gpx g=fileContent.Deserialize<gpx>();

            string result=CorrectCounterClockwisePolygon(g);

            Console.WriteLine(result);

            #endregion
        }

        static string CorrectCounterClockwisePolygon(gpx g)
        {
            string result =string.Empty;

            var rpList=g.rte.rtept.ToList();
            decimal cc= 0;
            for (int index = 0; index < rpList.Count(); index++)
            {
                if ((index + 1) != g.rte.rtept.Count())
                {
                    cc = cc + (rpList[index + 1].lon - rpList[index].lon) * (rpList[index + 1].lat - rpList[index].lat);
                }
                else {
                    cc = cc + (rpList[0].lon - rpList[index].lon) * (rpList[0].lat - rpList[index].lat);
                }
            }

            if (cc > 0)
            {
                //clockwise
                List<gpxRteRtept> tempList = new List<gpxRteRtept>();
                for (int index = 0; index < rpList.Count(); index++)
                {
                    tempList.Add(rpList[index]);
                }
                rpList = tempList;
            }
            string pol=string.Join(", ", rpList.Select(x => x.lon.ToString() + " " + x.lat.ToString()).ToArray());
            result = string.Format("geography::STGeomFromText('POLYGON (({0}))',4326)",pol);

            return result;
        }


    }

}
