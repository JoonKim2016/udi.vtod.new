//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UDI.VTOD.DataAccess.VTOD
{
    using System;
    
    public partial class CompletedTripInfo
    {
        public long Id { get; set; }
        public Nullable<System.DateTime> PickupDateTime { get; set; }
        public string DispatchTripId { get; set; }
    }
}
