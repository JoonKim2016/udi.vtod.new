//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UDI.VTOD.DataAccess.VTOD
{
    using System;
    
    public partial class SP_Taxi_GetFleet_User_Result
    {
        public long Id { get; set; }
        public long FleetId { get; set; }
        public int UserId { get; set; }
        public string AccountNumber { get; set; }
        public System.DateTime AppendTime { get; set; }
    }
}
