//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UDI.VTOD.DataAccess.VTOD
{
    using System;
    using System.Collections.Generic;
    
    public partial class ecar_fleet
    {
        public ecar_fleet()
        {
            this.ecar_fleet_api_reference = new HashSet<ecar_fleet_api_reference>();
            this.ecar_fleet_rate = new HashSet<ecar_fleet_rate>();
            this.ecar_fleet_servicearea_city = new HashSet<ecar_fleet_servicearea_city>();
            this.ecar_fleet_servicearea_polygon = new HashSet<ecar_fleet_servicearea_polygon>();
            this.ecar_fleet_servicearea_relation = new HashSet<ecar_fleet_servicearea_relation>();
            this.ecar_fleet_servicearea_zip = new HashSet<ecar_fleet_servicearea_zip>();
            this.ecar_fleet_user = new HashSet<ecar_fleet_user>();
            this.ecar_fleet_zone = new HashSet<ecar_fleet_zone>();
            this.ecar_fleet_blackout = new HashSet<ecar_fleet_blackout>();
            this.ecar_mtdata_configuration = new HashSet<ecar_mtdata_configuration>();
            this.ecar_trip = new HashSet<ecar_trip>();
        }
    
        public long Id { get; set; }
        public long ProviderId { get; set; }
        public bool Status { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public string PhoneNumber { get; set; }
        public string WebSite { get; set; }
        public string ServerUTCOffset { get; set; }
        public int RestrictBookingOption { get; set; }
        public Nullable<int> RestrictBookingMins { get; set; }
        public Nullable<int> ExpiredBookingHrs { get; set; }
        public System.DateTime AppendTime { get; set; }
        public Nullable<long> SDS_FleetMerchantID { get; set; }
        public Nullable<bool> IVREnable { get; set; }
        public string CCSiFleetId { get; set; }
        public string CCSiSource { get; set; }
        public string UDI33DNI { get; set; }
        public string UDI33IPAddress { get; set; }
        public Nullable<int> UDI33PortNo { get; set; }
        public Nullable<int> UDI33ReceivedTimeout { get; set; }
        public string UDI33ForceZone { get; set; }
        public Nullable<int> DetectFastMeter { get; set; }
        public Nullable<decimal> MinPriceFastMeter { get; set; }
        public string VehicleAttr { get; set; }
        public Nullable<long> VehicleBits { get; set; }
        public string DriverAttr { get; set; }
        public Nullable<long> DriverBits { get; set; }
        public Nullable<int> HandleStreetRange { get; set; }
        public Nullable<bool> OverWriteFixedPrice { get; set; }
        public Nullable<int> PickMeUpNowOption { get; set; }
        public Nullable<decimal> MaxEstimatedRate { get; set; }
        public Nullable<decimal> MinEstimatedRate { get; set; }
        public Nullable<bool> IsEstimated { get; set; }
        public Nullable<int> DispatchCityCode { get; set; }
        public Nullable<int> DriverInfo { get; set; }
        public Nullable<bool> SplitPayementEnabled { get; set; }
    
        public virtual ICollection<ecar_fleet_api_reference> ecar_fleet_api_reference { get; set; }
        public virtual ICollection<ecar_fleet_rate> ecar_fleet_rate { get; set; }
        public virtual ICollection<ecar_fleet_servicearea_city> ecar_fleet_servicearea_city { get; set; }
        public virtual ICollection<ecar_fleet_servicearea_polygon> ecar_fleet_servicearea_polygon { get; set; }
        public virtual ICollection<ecar_fleet_servicearea_relation> ecar_fleet_servicearea_relation { get; set; }
        public virtual ICollection<ecar_fleet_servicearea_zip> ecar_fleet_servicearea_zip { get; set; }
        public virtual ICollection<ecar_fleet_user> ecar_fleet_user { get; set; }
        public virtual ICollection<ecar_fleet_zone> ecar_fleet_zone { get; set; }
        public virtual ICollection<ecar_fleet_blackout> ecar_fleet_blackout { get; set; }
        public virtual ICollection<ecar_mtdata_configuration> ecar_mtdata_configuration { get; set; }
        public virtual ICollection<ecar_trip> ecar_trip { get; set; }
    }
}
