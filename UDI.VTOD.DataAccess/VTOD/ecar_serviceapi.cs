//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UDI.VTOD.DataAccess.VTOD
{
    using System;
    using System.Collections.Generic;
    
    public partial class ecar_serviceapi
    {
        public ecar_serviceapi()
        {
            this.ecar_fleet_api_reference = new HashSet<ecar_fleet_api_reference>();
            this.ecar_trip = new HashSet<ecar_trip>();
        }
    
        public long Id { get; set; }
        public string Name { get; set; }
        public System.DateTime AppendTime { get; set; }
    
        public virtual ICollection<ecar_fleet_api_reference> ecar_fleet_api_reference { get; set; }
        public virtual ICollection<ecar_trip> ecar_trip { get; set; }
    }
}
