//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UDI.VTOD.DataAccess.VTOD
{
    using System;
    using System.Collections.Generic;
    
    public partial class sds_trip
    {
        public sds_trip()
        {
            this.vtod_map_sds_ecar_trip = new HashSet<vtod_map_sds_ecar_trip>();
            this.sds_trip_history = new HashSet<sds_trip_history>();
        }
    
        public long Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public bool IsRoundTrip { get; set; }
        public int PayingPax { get; set; }
        public int FreePax { get; set; }
        public bool AccessibleServiceRequired { get; set; }
        public string ContactNumberDialingPrefix { get; set; }
        public string ContactNumber { get; set; }
        public string PickupLocation { get; set; }
        public Nullable<decimal> PickupLocationLatitude { get; set; }
        public Nullable<decimal> PickupLocationLongitude { get; set; }
        public string DropoffLocation { get; set; }
        public Nullable<decimal> DropoffLocationLatitude { get; set; }
        public Nullable<decimal> DropoffLocationLongitude { get; set; }
        public string RequestedFleet { get; set; }
        public Nullable<System.DateTime> FlightDateTime { get; set; }
        public Nullable<bool> IsInternationalFlight { get; set; }
        public string FlightNumber { get; set; }
        public string AirlineCode { get; set; }
        public Nullable<int> CharterMinutes { get; set; }
        public string ConfirmationNumber { get; set; }
        public Nullable<long> RezID { get; set; }
    
        public virtual ICollection<vtod_map_sds_ecar_trip> vtod_map_sds_ecar_trip { get; set; }
        public virtual vtod_trip vtod_trip { get; set; }
        public virtual ICollection<sds_trip_history> sds_trip_history { get; set; }
    }
}
