//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UDI.VTOD.DataAccess.VTOD
{
    using System;
    using System.Collections.Generic;
    
    public partial class sds_user_subfleet
    {
        public long id { get; set; }
        public int userId { get; set; }
        public string subfleet { get; set; }
        public bool Allow { get; set; }
    
        public virtual my_aspnet_users my_aspnet_users { get; set; }
    }
}
