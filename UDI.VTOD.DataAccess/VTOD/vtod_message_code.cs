//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UDI.VTOD.DataAccess.VTOD
{
    using System;
    using System.Collections.Generic;
    
    public partial class vtod_message_code
    {
        public long id { get; set; }
        public string code { get; set; }
    
        public virtual vtod_message vtod_message { get; set; }
    }
}
