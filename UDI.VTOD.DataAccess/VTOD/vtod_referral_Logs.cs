//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UDI.VTOD.DataAccess.VTOD
{
    using System;
    using System.Collections.Generic;
    
    public partial class vtod_referral_logs
    {
        public long Id { get; set; }
        public int ReferrerSDSMemberId { get; set; }
        public string RefereePhoneNumber { get; set; }
        public System.DateTime CreatedOn { get; set; }
    }
}
