﻿CREATE TABLE [Configuration].[AppCategoryKey_Settings] (
    [ID]          INT            IDENTITY (1, 1) NOT NULL,
    [AppID]       INT            NULL,
    [CategoryID]  INT            NULL,
    [KeyID]       INT            NULL,
    [Type]        NVARCHAR (100) NULL,
    [Value]       NVARCHAR (50)  NULL,
    [Description] NVARCHAR (100) NULL,
    [AppendTime]  DATETIME       NULL,
    [PolygonID]   BIGINT         NULL,
    [ZipCodeID]   BIGINT         NULL,
    [StartDate]   DATE           NULL,
    [EndDate]     DATE           NULL,
    [StartTime]   TIME (7)       NULL,
    [EndTime]     TIME (7)       NULL,
    [DayList]     NVARCHAR (100) NULL,
    [Priority]    INT            NULL,
    [IsEnabled]   BIT            CONSTRAINT [DF_AppCategoryKey_Settings_IsEnabled] DEFAULT ((0)) NULL,
    [GroupID]     BIGINT         NULL,
    CONSTRAINT [PK__AppCateg__3214EC279ED316BD] PRIMARY KEY CLUSTERED ([ID] ASC)
);

