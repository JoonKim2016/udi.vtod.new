﻿CREATE TABLE [Configuration].[App_Settings] (
    [ID]         INT            IDENTITY (1, 1) NOT NULL,
    [Name]       NVARCHAR (100) NULL,
    [AppendTime] DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);

