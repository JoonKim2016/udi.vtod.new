﻿CREATE TABLE [Configuration].[ConfType] (
    [ID]    INT           IDENTITY (1, 1) NOT NULL,
    [Value] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Configuration.ConfType] PRIMARY KEY CLUSTERED ([ID] ASC)
);

