﻿CREATE TABLE [Configuration].[Group] (
    [ID]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (50)  NOT NULL,
    [UserID]      NVARCHAR (128) NOT NULL,
    [AppendTime]  DATETIME       NOT NULL,
    [Description] NVARCHAR (250) NULL,
    [IsEnabled]   BIT            CONSTRAINT [DF_Configuration_Group_IsEnabled] DEFAULT ((1)) NULL,
    CONSTRAINT [PK_Group] PRIMARY KEY CLUSTERED ([ID] ASC)
);

