﻿CREATE TABLE [Configuration].[Key] (
    [ID]          INT            IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (100) NULL,
    [Description] NVARCHAR (500) NULL,
    [AppendTime]  DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);

