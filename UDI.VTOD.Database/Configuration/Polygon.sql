﻿CREATE TABLE [Configuration].[Polygon] (
    [ID]          INT            IDENTITY (1, 1) NOT NULL,
    [PolygonID]   INT            NULL,
    [Description] NVARCHAR (100) NULL,
    [Type]        NVARCHAR (10)  NULL,
    CONSTRAINT [PK_Polygon] PRIMARY KEY CLUSTERED ([ID] ASC)
);

