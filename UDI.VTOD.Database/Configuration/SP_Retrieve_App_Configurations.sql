﻿

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Configuration].[SP_Retrieve_App_Configurations]
@App nvarchar(100),
@Longitude nvarchar(100),
@Latitude nvarchar(100),
@ZipCode nvarchar(100),
@Category nvarchar(100),
@Key nvarchar(100),
@LocalTime datetime
AS
BEGIN
BEGIN TRY
declare @appID int
declare @polygonID int
declare @point geography
declare @count int
declare @DayName nvarchar(5)
declare @zipCodeID int
declare @CategoryID int
declare @KeyID int
declare @Temptbl table
(
   Id int,
   CategoryId int,
   KeyId int
)
Begin TRANSACTION;

---Find the Point based on the Latitude/Longitude
set @point=geography::STGeomFromText('POINT('+cast(@Longitude as nvarchar(50))+' '+cast(@Latitude as nvarchar(50))+')',4326);

---Find LocalDateTime is blank or not
if(@LocalTime is  null)
begin
set @LocalTime=getdate()
end

--Find Polygon ID
 SELECT @polygonID=ISNULL(Configuration.Polygon.ID,NULL)
 FROM dbo.vtod_polygon inner join
 Configuration.Polygon
 on dbo.vtod_polygon.id=Configuration.Polygon.PolygonID
 WHERE   polygon.STIntersects(@point)=1

 --Find ZipCode ID
SELECT @zipCodeID=ISNULL(Configuration.ZipCode.ID,NULL)
 FROM Configuration.ZipCode
 WHERE [ZipCode]=@ZipCode 

 --Find AppID
select @appID=ID from [Configuration].[App_Settings] where [Name]=@App 

---Find DayName
select @DayName=(datepart(dw,@LocalTime))

---Find CategoryId
SELECT @CategoryID=ISNULL(Configuration.Category.ID,NULL)
 FROM [Configuration].[Category]
 WHERE Name=@Category

 ---Find KeyId
SELECT @KeyID=ISNULL(Configuration.[Key].ID,NULL)
 FROM [Configuration].[Key]
 WHERE Name=@Key


if(@polygonID is not null)
begin
--Day : 1 for Sunday,2 for Monday,3 for Tuesday and so on (POLYGON ID)
insert into @Temptbl(Id,CategoryId,KeyId) select ID,CategoryID,KeyID  from [Configuration].[AppCategoryKey_Settings]
where AppID=@appID and CAST(@LocalTime as date)  between ISNULL(StartDate,@LocalTime) and ISNULL(EndDate,DateAdd(mi,10,@LocalTime)) 
and CAST(@LocalTime as time) between ISNULL(StartTime,CAST(@LocalTime as time)) and ISNULL(EndTime,CAST(DateAdd(mi,10,@LocalTime) as time)) 
and  DayList like '%' + @DayName + '%' and PolygonID=@polygonID and IsEnabled=1

---Filtering based on Polygon And StartDate and EndDate but no Daylist
insert into @Temptbl(Id,CategoryId,KeyId) select ID,CategoryID,KeyID  from [Configuration].[AppCategoryKey_Settings]
where AppID=@appID and CAST(@LocalTime as date) between ISNULL(StartDate,@LocalTime) and ISNULL(EndDate,DateAdd(mi,10,@LocalTime))  
and CAST(@LocalTime as time) between ISNULL(StartTime,CAST(@LocalTime as time)) and ISNULL(EndTime,CAST(DateAdd(mi,10,@LocalTime) as time)) 
and  DayList is null and PolygonID=@polygonID and IsEnabled=1

---Find data based not  based on  no specific date but with POLYGON ID
--insert into @Temptbl(Id,CategoryId,KeyId) select ID,CategoryID,KeyID from [Configuration].[AppCategoryKey_Settings]
--where AppID=@appID  and PolygonID=@polygonID and (StartTime is null and EndTime is null) and DayList is null
end

if(@zipCodeID is not null)
begin
--Day : 1 for Sunday,2 for Monday,3 for Tuesday and so on ( ZIPCODE ID)
insert into @Temptbl(Id,CategoryId,KeyId) select ID,CategoryID,KeyID from [Configuration].[AppCategoryKey_Settings]
where AppID=@appID and CAST(@LocalTime as date) between ISNULL(StartDate,@LocalTime) and ISNULL(EndDate,DateAdd(mi,10,@LocalTime)) 
and CAST(@LocalTime as time) between ISNULL(StartTime,CAST(@LocalTime as time)) and ISNULL(EndTime,CAST(DateAdd(mi,10,@LocalTime) as time)) 
and  DayList like '%' + @DayName + '%' and ZipCodeID=@zipCodeID
and IsEnabled=1

-----Find data based not  based no  the specific date but with ZIPCODE ID
--insert into @Temptbl(Id,CategoryId,KeyId) select ID,CategoryID,KeyID  from [Configuration].[AppCategoryKey_Settings]
--where AppID=@appID  and ZipCodeID=@zipCodeID and (StartTime is null and EndTime is null) and DayList is null

---Filtering based on ZIPCODE ID And StartDate and EndDate but no Daylist
insert into @Temptbl(Id,CategoryId,KeyId) select ID,CategoryID,KeyID from [Configuration].[AppCategoryKey_Settings]
where AppID=@appID and CAST(@LocalTime as date) between ISNULL(StartDate,@LocalTime) and ISNULL(EndDate,DateAdd(mi,10,@LocalTime)) 
and CAST(@LocalTime as time) between ISNULL(StartTime,CAST(@LocalTime as time)) and ISNULL(EndTime,CAST(DateAdd(mi,10,@LocalTime) as time)) 
and  DayList is null and ZipCodeID=@zipCodeID and IsEnabled=1
end

---Removing the rows that does not have that category
if @CategoryID is not null
begin
delete from @Temptbl where CategoryId<>@CategoryID
end

---Removing the rows that does not have that key
if @KeyID is not null
begin
delete from @Temptbl where KeyId<>@KeyID
end
---If Polygon and Zipcode is empty pick up based on Category and Key
if(@polygonID is  null and @zipCodeID is  null)
begin
if(@CategoryID is not null and @KeyId is null)
begin
insert into @Temptbl(Id,CategoryId,KeyId) select ID,CategoryID,KeyID  from [Configuration].[AppCategoryKey_Settings]
where AppID=@appID and CategoryId=@CategoryID and IsEnabled=1
end
if(@CategoryID is not null and @KeyID is not null)
begin
insert into @Temptbl(Id,CategoryId,KeyId) select ID,CategoryID,KeyID  from [Configuration].[AppCategoryKey_Settings]
where AppID=@appID and CategoryId=@CategoryID and KeyID=@KeyID and IsEnabled=1
end
end
---Count the number of rows in TempDB table
select @count=ISNULL(count(*),NULL)  from @Temptbl

if(@count IS NOT NULL AND @count>0)
BEGIN

if @CategoryID is not null and @KeyID is not null
begin
---select rows with ids in temp table
SELECT * FROM (select [Configuration].[AppCategoryKey_Settings].ID AS ID,[Configuration].[Category].[Name] AS CategoryName,[Configuration].[Key].Name AS KeyName,[Configuration].[AppCategoryKey_Settings].[Value],
[Configuration].[AppCategoryKey_Settings].[Type],[Priority]
from [Configuration].[Category] inner join
[Configuration].[AppCategoryKey_Settings] on 
[Configuration].[Category].[ID]=[Configuration].[AppCategoryKey_Settings].[CategoryID]
inner join [Configuration].[Key] on [Configuration].[Key].[ID]=[Configuration].[AppCategoryKey_Settings].[KeyID]
where [Configuration].[AppCategoryKey_Settings].[AppID]=@appID 
and [Configuration].[AppCategoryKey_Settings].id  in (select distinct id from @Temptbl)
and CategoryID=@CategoryID and KeyID=@KeyID and IsEnabled=1
and CAST(@LocalTime as date) between ISNULL(StartDate,@LocalTime) and ISNULL(EndDate,DateAdd(mi,10,@LocalTime)) 
and CAST(@LocalTime as time) between ISNULL(StartTime,CAST(@LocalTime as time)) and ISNULL(EndTime,CAST(DateAdd(mi,10,@LocalTime) as time)) 

union 
---select rows based on no Polygon ID and no ZipCode ID but with daylist
select [Configuration].[AppCategoryKey_Settings].ID AS ID,[Configuration].[Category].[Name] AS CategoryName,[Configuration].[Key].Name AS KeyName,[Configuration].[AppCategoryKey_Settings].[Value],
[Configuration].[AppCategoryKey_Settings].[Type],[Priority]
from [Configuration].[Category] inner join
[Configuration].[AppCategoryKey_Settings] on 
[Configuration].[Category].[ID]=[Configuration].[AppCategoryKey_Settings].[CategoryID]
inner join [Configuration].[Key] on [Configuration].[Key].[ID]=[Configuration].[AppCategoryKey_Settings].[KeyID]
where [Configuration].[AppCategoryKey_Settings].[AppID]=@appID 
and [Configuration].[AppCategoryKey_Settings].ZipCodeID is null and [Configuration].[AppCategoryKey_Settings].PolygonID is null 
and CategoryID=@CategoryID and KeyID=@KeyID and IsEnabled=1
and CAST(@LocalTime as date) between ISNULL(StartDate,@LocalTime) and ISNULL(EndDate,DateAdd(mi,10,@LocalTime)) 
and CAST(@LocalTime as time) between ISNULL(StartTime,CAST(@LocalTime as time)) and ISNULL(EndTime,CAST(DateAdd(mi,10,@LocalTime) as time)) 
and  DayList like '%' + @DayName + '%'
union 
--select rows based on no Polygon ID and no ZipCode ID and no daylist
select [Configuration].[AppCategoryKey_Settings].ID AS ID,[Configuration].[Category].[Name] AS CategoryName,[Configuration].[Key].Name AS KeyName,[Configuration].[AppCategoryKey_Settings].[Value],
[Configuration].[AppCategoryKey_Settings].[Type],[Priority]
from [Configuration].[Category] inner join
[Configuration].[AppCategoryKey_Settings] on 
[Configuration].[Category].[ID]=[Configuration].[AppCategoryKey_Settings].[CategoryID]
inner join [Configuration].[Key] on [Configuration].[Key].[ID]=[Configuration].[AppCategoryKey_Settings].[KeyID]

where [Configuration].[AppCategoryKey_Settings].[AppID]=@appID 
and [Configuration].[AppCategoryKey_Settings].ZipCodeID is null and [Configuration].[AppCategoryKey_Settings].PolygonID is null 
and CAST(@LocalTime as date) between ISNULL(StartDate,@LocalTime) and ISNULL(EndDate,DateAdd(mi,10,@LocalTime)) 
and CAST(@LocalTime as time) between ISNULL(StartTime,CAST(@LocalTime as time)) and ISNULL(EndTime,CAST(DateAdd(mi,10,@LocalTime) as time)) 
and  DayList is null and CategoryID=@CategoryID and KeyID=@KeyID and IsEnabled=1
)T
ORDER BY ISNULL(T.Priority,1),T.ID

end

if @CategoryID is not null and @KeyID is  null
begin
---select rows with ids in temp table
SELECT * FROM(select [Configuration].[AppCategoryKey_Settings].ID AS ID,[Configuration].[Category].[Name] AS CategoryName,[Configuration].[Key].Name AS KeyName,[Configuration].[AppCategoryKey_Settings].[Value],
[Configuration].[AppCategoryKey_Settings].[Type],[Priority]
from [Configuration].[Category] inner join
[Configuration].[AppCategoryKey_Settings] on 
[Configuration].[Category].[ID]=[Configuration].[AppCategoryKey_Settings].[CategoryID]
inner join [Configuration].[Key] on [Configuration].[Key].[ID]=[Configuration].[AppCategoryKey_Settings].[KeyID]
where [Configuration].[AppCategoryKey_Settings].[AppID]=@appID 
and [Configuration].[AppCategoryKey_Settings].id  in ( select distinct id from @Temptbl)
and CategoryID=@CategoryID and IsEnabled=1
and CAST(@LocalTime as date) between ISNULL(StartDate,@LocalTime) and ISNULL(EndDate,DateAdd(mi,10,@LocalTime)) 
and CAST(@LocalTime as time) between ISNULL(StartTime,CAST(@LocalTime as time)) and ISNULL(EndTime,CAST(DateAdd(mi,10,@LocalTime) as time)) 
union 
---select rows based on no Polygon ID and ZipCode ID and dayname has a value
select [Configuration].[AppCategoryKey_Settings].ID AS ID,[Configuration].[Category].[Name] AS CategoryName,[Configuration].[Key].Name AS KeyName,[Configuration].[AppCategoryKey_Settings].[Value],
[Configuration].[AppCategoryKey_Settings].[Type],[Priority]
from [Configuration].[Category] inner join
[Configuration].[AppCategoryKey_Settings] on 
[Configuration].[Category].[ID]=[Configuration].[AppCategoryKey_Settings].[CategoryID]
inner join [Configuration].[Key] on [Configuration].[Key].[ID]=[Configuration].[AppCategoryKey_Settings].[KeyID]
where [Configuration].[AppCategoryKey_Settings].[AppID]=@appID 
and [Configuration].[AppCategoryKey_Settings].ZipCodeID is null and [Configuration].[AppCategoryKey_Settings].PolygonID is null 
and CategoryID=@CategoryID  and IsEnabled=1
and CAST(@LocalTime as date) between ISNULL(StartDate,@LocalTime) and ISNULL(EndDate,DateAdd(mi,10,@LocalTime)) 
and CAST(@LocalTime as time) between ISNULL(StartTime,CAST(@LocalTime as time)) and ISNULL(EndTime,CAST(DateAdd(mi,10,@LocalTime) as time)) 
and  DayList like '%' + @DayName + '%'
union 
-----select rows based on no Polygon ID and no ZipCode ID and no daylist
select [Configuration].[AppCategoryKey_Settings].ID AS ID,[Configuration].[Category].[Name] AS CategoryName,[Configuration].[Key].Name AS KeyName,[Configuration].[AppCategoryKey_Settings].[Value],
[Configuration].[AppCategoryKey_Settings].[Type],[Priority]
from [Configuration].[Category] inner join
[Configuration].[AppCategoryKey_Settings] on 
[Configuration].[Category].[ID]=[Configuration].[AppCategoryKey_Settings].[CategoryID]
inner join [Configuration].[Key] on [Configuration].[Key].[ID]=[Configuration].[AppCategoryKey_Settings].[KeyID]
where [Configuration].[AppCategoryKey_Settings].[AppID]=@appID 
and [Configuration].[AppCategoryKey_Settings].ZipCodeID is null and [Configuration].[AppCategoryKey_Settings].PolygonID is null 
and CAST(@LocalTime as date) between ISNULL(StartDate,@LocalTime) and ISNULL(EndDate,DateAdd(mi,10,@LocalTime)) 
and CAST(@LocalTime as time) between ISNULL(StartTime,CAST(@LocalTime as time)) and ISNULL(EndTime,CAST(DateAdd(mi,10,@LocalTime) as time)) 
and  DayList is null  and CategoryID=@CategoryID and IsEnabled=1
)T
ORDER BY ISNULL(T.Priority,1),T.ID

end

if @CategoryID is  null and @KeyID is  null
begin
---select rows with ids in temp table
SELECT * FROM (select [Configuration].[AppCategoryKey_Settings].ID AS ID,[Configuration].[Category].[Name] AS CategoryName,[Configuration].[Key].Name AS KeyName,[Configuration].[AppCategoryKey_Settings].[Value],
[Configuration].[AppCategoryKey_Settings].[Type],[Priority]
from [Configuration].[Category] inner join
[Configuration].[AppCategoryKey_Settings] on 
[Configuration].[Category].[ID]=[Configuration].[AppCategoryKey_Settings].[CategoryID]
inner join [Configuration].[Key] on [Configuration].[Key].[ID]=[Configuration].[AppCategoryKey_Settings].[KeyID]
where [Configuration].[AppCategoryKey_Settings].[AppID]=@appID and IsEnabled=1
and [Configuration].[AppCategoryKey_Settings].id  in (select  distinct id from @Temptbl)
and CAST(@LocalTime as date) between ISNULL(StartDate,@LocalTime) and ISNULL(EndDate,DateAdd(mi,10,@LocalTime)) 
and CAST(@LocalTime as time) between ISNULL(StartTime,CAST(@LocalTime as time)) and ISNULL(EndTime,CAST(DateAdd(mi,10,@LocalTime) as time)) 
union 
---select rows based on no Polygon ID and ZipCode ID and daylist
select [Configuration].[AppCategoryKey_Settings].ID AS ID,[Configuration].[Category].[Name] AS CategoryName,[Configuration].[Key].Name AS KeyName,[Configuration].[AppCategoryKey_Settings].[Value],
[Configuration].[AppCategoryKey_Settings].[Type],[Priority]
from [Configuration].[Category] inner join
[Configuration].[AppCategoryKey_Settings] on 
[Configuration].[Category].[ID]=[Configuration].[AppCategoryKey_Settings].[CategoryID]
inner join [Configuration].[Key] on [Configuration].[Key].[ID]=[Configuration].[AppCategoryKey_Settings].[KeyID]
where [Configuration].[AppCategoryKey_Settings].[AppID]=@appID 
and [Configuration].[AppCategoryKey_Settings].ZipCodeID is null and [Configuration].[AppCategoryKey_Settings].PolygonID is null 
and IsEnabled=1
and CAST(@LocalTime as date) between ISNULL(StartDate,@LocalTime) and ISNULL(EndDate,DateAdd(mi,10,@LocalTime)) 
and CAST(@LocalTime as time) between ISNULL(StartTime,CAST(@LocalTime as time)) and ISNULL(EndTime,CAST(DateAdd(mi,10,@LocalTime) as time)) 
and  DayList like '%' + @DayName + '%'
union 
---select rows based on no Polygon ID and ZipCode ID and no daylist
select [Configuration].[AppCategoryKey_Settings].ID AS ID,[Configuration].[Category].[Name] AS CategoryName,[Configuration].[Key].Name AS KeyName,[Configuration].[AppCategoryKey_Settings].[Value],
[Configuration].[AppCategoryKey_Settings].[Type],[Priority]
from [Configuration].[Category] inner join
[Configuration].[AppCategoryKey_Settings] on 
[Configuration].[Category].[ID]=[Configuration].[AppCategoryKey_Settings].[CategoryID]
inner join [Configuration].[Key] on [Configuration].[Key].[ID]=[Configuration].[AppCategoryKey_Settings].[KeyID]
where [Configuration].[AppCategoryKey_Settings].[AppID]=@appID 
and [Configuration].[AppCategoryKey_Settings].ZipCodeID is null and [Configuration].[AppCategoryKey_Settings].PolygonID is null 
and CAST(@LocalTime as date) between ISNULL(StartDate,@LocalTime) and ISNULL(EndDate,DateAdd(mi,10,@LocalTime)) 
and CAST(@LocalTime as time) between ISNULL(StartTime,CAST(@LocalTime as time)) and ISNULL(EndTime,CAST(DateAdd(mi,10,@LocalTime) as time)) 
and  DayList is null   and IsEnabled=1 
)T
ORDER BY ISNULL(T.Priority,1),T.ID

end
END
ELSE
BEGIN
if @CategoryID is  null and @KeyID is  null
begin
---select rows with no Category Id,no KeyId and Daylist
SELECT * FROM (select [Configuration].[AppCategoryKey_Settings].ID AS ID,[Configuration].[Category].[Name] AS CategoryName,[Configuration].[Key].Name AS KeyName,[Configuration].[AppCategoryKey_Settings].[Value],
[Configuration].[AppCategoryKey_Settings].[Type],[Priority]
from [Configuration].[Category] inner join
[Configuration].[AppCategoryKey_Settings] on 
[Configuration].[Category].[ID]=[Configuration].[AppCategoryKey_Settings].[CategoryID]
inner join [Configuration].[Key] on [Configuration].[Key].[ID]=[Configuration].[AppCategoryKey_Settings].[KeyID]
where [Configuration].[AppCategoryKey_Settings].[AppID]=@appID 
and [Configuration].[AppCategoryKey_Settings].ZipCodeID is null and [Configuration].[AppCategoryKey_Settings].PolygonID is null 
and CAST(@LocalTime as date) between ISNULL(StartDate,@LocalTime) and ISNULL(EndDate,DateAdd(mi,10,@LocalTime)) 
and CAST(@LocalTime as time) between ISNULL(StartTime,CAST(@LocalTime as time)) and ISNULL(EndTime,CAST(DateAdd(mi,10,@LocalTime) as time)) 
and  DayList like '%' + @DayName + '%'
and IsEnabled=1
union 
---select rows with no Category Id,no KeyId and no Daylist
select [Configuration].[AppCategoryKey_Settings].ID AS ID,[Configuration].[Category].[Name] AS CategoryName,[Configuration].[Key].Name AS KeyName,[Configuration].[AppCategoryKey_Settings].[Value],
[Configuration].[AppCategoryKey_Settings].[Type],[Priority]
from [Configuration].[Category] inner join
[Configuration].[AppCategoryKey_Settings] on 
[Configuration].[Category].[ID]=[Configuration].[AppCategoryKey_Settings].[CategoryID]
inner join [Configuration].[Key] on [Configuration].[Key].[ID]=[Configuration].[AppCategoryKey_Settings].[KeyID]
where [Configuration].[AppCategoryKey_Settings].[AppID]=@appID 
and [Configuration].[AppCategoryKey_Settings].ZipCodeID is null and [Configuration].[AppCategoryKey_Settings].PolygonID is null 
and CAST(@LocalTime as date) between ISNULL(StartDate,@LocalTime) and ISNULL(EndDate,DateAdd(mi,10,@LocalTime)) 
and CAST(@LocalTime as time) between ISNULL(StartTime,CAST(@LocalTime as time)) and ISNULL(EndTime,CAST(DateAdd(mi,10,@LocalTime) as time)) 
and  DayList is null
and IsEnabled=1 )T
ORDER BY ISNULL(T.Priority,1),T.ID

--union 
--select [Configuration].[AppCategoryKey_Settings].ID AS ID,[Configuration].[Category].[Name] AS CategoryName,[Configuration].[Key].Name AS KeyName,[Configuration].[AppCategoryKey_Settings].[Value],
--[Configuration].[AppCategoryKey_Settings].[Type],[Priority]
--from [Configuration].[Category] inner join
--[Configuration].[AppCategoryKey_Settings] on 
--[Configuration].[Category].[ID]=[Configuration].[AppCategoryKey_Settings].[CategoryID]
--inner join [Configuration].[Key] on [Configuration].[Key].[ID]=[Configuration].[AppCategoryKey_Settings].[KeyID]
--where [Configuration].[AppCategoryKey_Settings].[AppID]=@appID 
--and [Configuration].[AppCategoryKey_Settings].ZipCodeID is null and [Configuration].[AppCategoryKey_Settings].PolygonID is null 
--and CAST(@LocalTime as date) between ISNULL(StartDate,@LocalTime) and ISNULL(EndDate,DateAdd(mi,10,@LocalTime)) 
--and CAST(@LocalTime as time) between ISNULL(StartTime,CAST(@LocalTime as time)) and ISNULL(EndTime,CAST(DateAdd(mi,10,@LocalTime) as time)) 
--and  DayList like '%' + @DayName + '%'  and IsEnabled=1
end

if @CategoryID is not null and @KeyID is  null
begin
if @polygonID is null and @zipCodeID is null
begin
---select rows with  Category Id,no KeyId and Daylist
SELECT * FROM (select [Configuration].[AppCategoryKey_Settings].ID AS ID,[Configuration].[Category].[Name] AS CategoryName,[Configuration].[Key].Name AS KeyName,[Configuration].[AppCategoryKey_Settings].[Value],
[Configuration].[AppCategoryKey_Settings].[Type],[Priority]
from [Configuration].[Category] inner join
[Configuration].[AppCategoryKey_Settings] on 
[Configuration].[Category].[ID]=[Configuration].[AppCategoryKey_Settings].[CategoryID]
inner join [Configuration].[Key] on [Configuration].[Key].[ID]=[Configuration].[AppCategoryKey_Settings].[KeyID]
where [Configuration].[AppCategoryKey_Settings].[AppID]=@appID 
and [Configuration].[AppCategoryKey_Settings].ZipCodeID is null and [Configuration].[AppCategoryKey_Settings].PolygonID is null 
and CAST(@LocalTime as date) between ISNULL(StartDate,@LocalTime) and ISNULL(EndDate,DateAdd(mi,10,@LocalTime)) 
and CAST(@LocalTime as time) between ISNULL(StartTime,CAST(@LocalTime as time)) 
and ISNULL(EndTime,CAST(DateAdd(mi,10,@LocalTime) as time))  and CategoryID=@CategoryID
and  DayList like '%' + @DayName + '%'
and IsEnabled=1
union 
---select rows with  Category Id,no KeyId and no Daylist
select [Configuration].[AppCategoryKey_Settings].ID AS ID,[Configuration].[Category].[Name] AS CategoryName,[Configuration].[Key].Name AS KeyName,[Configuration].[AppCategoryKey_Settings].[Value],
[Configuration].[AppCategoryKey_Settings].[Type],[Priority]
from [Configuration].[Category] inner join
[Configuration].[AppCategoryKey_Settings] on 
[Configuration].[Category].[ID]=[Configuration].[AppCategoryKey_Settings].[CategoryID]
inner join [Configuration].[Key] on [Configuration].[Key].[ID]=[Configuration].[AppCategoryKey_Settings].[KeyID]
where [Configuration].[AppCategoryKey_Settings].[AppID]=@appID 
and [Configuration].[AppCategoryKey_Settings].ZipCodeID is null and [Configuration].[AppCategoryKey_Settings].PolygonID is null 
and CAST(@LocalTime as date) between ISNULL(StartDate,@LocalTime) and ISNULL(EndDate,DateAdd(mi,10,@LocalTime)) 
and CAST(@LocalTime as time) between ISNULL(StartTime,CAST(@LocalTime as time)) and ISNULL(EndTime,CAST(DateAdd(mi,10,@LocalTime) as time)) 
and CategoryID=@CategoryID
and  DayList is null
and IsEnabled=1)T
ORDER BY ISNULL(T.Priority,1),T.ID

--union 
--select [Configuration].[AppCategoryKey_Settings].ID AS ID,[Configuration].[Category].[Name] AS CategoryName,[Configuration].[Key].Name AS KeyName,[Configuration].[AppCategoryKey_Settings].[Value],
--[Configuration].[AppCategoryKey_Settings].[Type],[Priority]
--from [Configuration].[Category] inner join
--[Configuration].[AppCategoryKey_Settings] on 
--[Configuration].[Category].[ID]=[Configuration].[AppCategoryKey_Settings].[CategoryID]
--inner join [Configuration].[Key] on [Configuration].[Key].[ID]=[Configuration].[AppCategoryKey_Settings].[KeyID]
--where [Configuration].[AppCategoryKey_Settings].[AppID]=@appID 
--and [Configuration].[AppCategoryKey_Settings].ZipCodeID is null and [Configuration].[AppCategoryKey_Settings].PolygonID is null 
--and CAST(@LocalTime as date) between ISNULL(StartDate,@LocalTime) and ISNULL(EndDate,DateAdd(mi,10,@LocalTime)) 
--and CAST(@LocalTime as time) between ISNULL(StartTime,CAST(@LocalTime as time)) and ISNULL(EndTime,CAST(DateAdd(mi,10,@LocalTime) as time)) 
--and  DayList like '%' + @DayName + '%'  and CategoryID=@CategoryID and IsEnabled=1
end
end

if @CategoryID is not null and @KeyID is not null
begin
---select rows with Category Id,KeyId and Daylist
SELECT * FROM(select [Configuration].[AppCategoryKey_Settings].ID AS ID,[Configuration].[Category].[Name] AS CategoryName,[Configuration].[Key].Name AS KeyName,[Configuration].[AppCategoryKey_Settings].[Value],
[Configuration].[AppCategoryKey_Settings].[Type],[Priority]
from [Configuration].[Category] inner join
[Configuration].[AppCategoryKey_Settings] on 
[Configuration].[Category].[ID]=[Configuration].[AppCategoryKey_Settings].[CategoryID]
inner join [Configuration].[Key] on [Configuration].[Key].[ID]=[Configuration].[AppCategoryKey_Settings].[KeyID]
where [Configuration].[AppCategoryKey_Settings].[AppID]=@appID 
and [Configuration].[AppCategoryKey_Settings].ZipCodeID is null and [Configuration].[AppCategoryKey_Settings].PolygonID is null 
and CAST(@LocalTime as date) between ISNULL(StartDate,@LocalTime) and ISNULL(EndDate,DateAdd(mi,10,@LocalTime)) 
and CAST(@LocalTime as time) between ISNULL(StartTime,CAST(@LocalTime as time)) and ISNULL(EndTime,CAST(DateAdd(mi,10,@LocalTime) as time)) 
and CategoryID=@CategoryID and KeyID=@KeyID and IsEnabled=1
and  DayList like '%' + @DayName + '%' 
union 
---select rows with Category Id,KeyId and no Daylist
select [Configuration].[AppCategoryKey_Settings].ID AS ID,[Configuration].[Category].[Name] AS CategoryName,[Configuration].[Key].Name AS KeyName,[Configuration].[AppCategoryKey_Settings].[Value],
[Configuration].[AppCategoryKey_Settings].[Type],[Priority]
from [Configuration].[Category] inner join
[Configuration].[AppCategoryKey_Settings] on 
[Configuration].[Category].[ID]=[Configuration].[AppCategoryKey_Settings].[CategoryID]
inner join [Configuration].[Key] on [Configuration].[Key].[ID]=[Configuration].[AppCategoryKey_Settings].[KeyID]
where [Configuration].[AppCategoryKey_Settings].[AppID]=@appID 
and [Configuration].[AppCategoryKey_Settings].ZipCodeID is null and [Configuration].[AppCategoryKey_Settings].PolygonID is null 
and CAST(@LocalTime as date) between ISNULL(StartDate,@LocalTime) and ISNULL(EndDate,DateAdd(mi,10,@LocalTime)) 
and CAST(@LocalTime as time) between ISNULL(StartTime,CAST(@LocalTime as time)) and ISNULL(EndTime,CAST(DateAdd(mi,10,@LocalTime) as time)) 
and CategoryID=@CategoryID and KeyID=@KeyID and IsEnabled=1
and  DayList is null )T
ORDER BY ISNULL(T.Priority,1),T.ID

--union 
--select [Configuration].[AppCategoryKey_Settings].ID AS ID,[Configuration].[Category].[Name] AS CategoryName,[Configuration].[Key].Name AS KeyName,[Configuration].[AppCategoryKey_Settings].[Value],
--[Configuration].[AppCategoryKey_Settings].[Type],[Priority]
--from [Configuration].[Category] inner join
--[Configuration].[AppCategoryKey_Settings] on 
--[Configuration].[Category].[ID]=[Configuration].[AppCategoryKey_Settings].[CategoryID]
--inner join [Configuration].[Key] on [Configuration].[Key].[ID]=[Configuration].[AppCategoryKey_Settings].[KeyID]
--where [Configuration].[AppCategoryKey_Settings].[AppID]=@appID 
--and [Configuration].[AppCategoryKey_Settings].ZipCodeID is null and [Configuration].[AppCategoryKey_Settings].PolygonID is null 
--and CAST(@LocalTime as date) between ISNULL(StartDate,@LocalTime) and ISNULL(EndDate,DateAdd(mi,10,@LocalTime)) 
--and CAST(@LocalTime as time) between ISNULL(StartTime,CAST(@LocalTime as time)) and ISNULL(EndTime,CAST(DateAdd(mi,10,@LocalTime) as time)) 
--and  DayList like '%' + @DayName + '%'   and CategoryID=@CategoryID and KeyID=@KeyID and IsEnabled=1
end

END
--IF(@polygonID is null)
--BEGIN
  
--	---removing the duplicates

--	if(@count=0)
--	begin
--	    ----IF THERE IS VALUE IN THAT PERIOD AND WHERE POLYGON IS null
--		select @ConfigId=ISNULL(max(ID),0)  from [Configuration].[AppCategoryKey_Settings]
--		where AppID=@appID and CAST(@LocalTime as date) between StartDate and EndDate 
--		and CAST(@LocalTime as time) between StartTime and EndTime 
--		and  DayList like '%' + @DayName + '%'
--		if(@ConfigId=0)
--		begin
--				----IF THERE IS VALUE for that key where polygon id is null and time period is also null
--				select @ConfigId=ISNULL(max(ID),0) from  [Configuration].[AppCategoryKey_Settings]
--				where KeyID in ( select [KeyID] from [Configuration].[AppCategoryKey_Settings] 
--				where AppID=@appID	group by [AppID],[CategoryID],[KeyID]
--				having count(*)>1)  and PolygonID is null and (startdate is null or enddate is null)
--				and (StartTime is null or EndTime is null)
--		end
--	end 
--	select [Configuration].[Category].[Name] AS CategoryName,[Configuration].[Key].Name AS KeyName,[Configuration].[AppCategoryKey_Settings].[Value],
--	[Configuration].[AppCategoryKey_Settings].[Type] 
--	from [Configuration].[Category] inner join
--	[Configuration].[AppCategoryKey_Settings] on 
--	[Configuration].[Category].[ID]=[Configuration].[AppCategoryKey_Settings].[CategoryID]
--	inner join [Configuration].[Key] on [Configuration].[Key].[ID]=[Configuration].[AppCategoryKey_Settings].[KeyID]
--	where [Configuration].[AppCategoryKey_Settings].[AppID]=@appID and PolygonID is NULL
--	and  [Configuration].[AppCategoryKey_Settings].ID not in  (select ID from  [Configuration].[AppCategoryKey_Settings]
--	where KeyID in ( select [KeyID] from [Configuration].[AppCategoryKey_Settings] 
--	where AppID=1
--	group by [AppID],[CategoryID],[KeyID]
--	having count(*)>1) and PolygonID is null)
--	---adding the values where we have the accurate id based on LocalTime
--	union
--	select [Configuration].[Category].[Name] AS CategoryName,[Configuration].[Key].Name AS KeyName,[Configuration].[AppCategoryKey_Settings].[Value],
--	[Configuration].[AppCategoryKey_Settings].[Type] 
--	from [Configuration].[Category] inner join
--	[Configuration].[AppCategoryKey_Settings] on 
--	[Configuration].[Category].[ID]=[Configuration].[AppCategoryKey_Settings].[CategoryID]
--	inner join [Configuration].[Key] on [Configuration].[Key].[ID]=[Configuration].[AppCategoryKey_Settings].[KeyID]
--	where [Configuration].[AppCategoryKey_Settings].[AppID]=@appID 
--	and PolygonID is NULL and [Configuration].[AppCategoryKey_Settings].id  in (select id from @Temptbl)
--	union
--	select [Configuration].[Category].[Name] AS CategoryName,[Configuration].[Key].Name AS KeyName,[Configuration].[AppCategoryKey_Settings].[Value],
--	[Configuration].[AppCategoryKey_Settings].[Type] 
--	from [Configuration].[Category] inner join
--	[Configuration].[AppCategoryKey_Settings] on 
--	[Configuration].[Category].[ID]=[Configuration].[AppCategoryKey_Settings].[CategoryID]
--	inner join [Configuration].[Key] on [Configuration].[Key].[ID]=[Configuration].[AppCategoryKey_Settings].[KeyID]
--	where [Configuration].[AppCategoryKey_Settings].[AppID]=@appID 
--	and PolygonID is NULL and [Configuration].[AppCategoryKey_Settings].id  in (@ConfigId)

-- END
-- ELSE
-- BEGIN

--	if(@count>0)
--		begin
--			---removing the duplicates
--			select   [Configuration].[Category].[Name] AS CategoryName,[Configuration].[Key].Name AS KeyName,[Configuration].[AppCategoryKey_Settings].
--			[Value],
--			[Configuration].[AppCategoryKey_Settings].[Type]
--			from [Configuration].[Category] inner join
--			 [Configuration].[AppCategoryKey_Settings] on 
--			 [Configuration].[Category].[ID]=[Configuration].[AppCategoryKey_Settings].[CategoryID]
--			 inner join [Configuration].[Key] on [Configuration].[Key].[ID]=[Configuration].[AppCategoryKey_Settings].[KeyID]
--			 where [Configuration].[AppCategoryKey_Settings].[AppID]=@appID
--			 and (PolygonID =@polygonID or PolygonID is null) and 
--			 [Configuration].[AppCategoryKey_Settings].ID not in (select ID from  [Configuration].[AppCategoryKey_Settings]
--			 where KeyID in ( select [KeyID] from [Configuration].[AppCategoryKey_Settings] 
--			 where AppID=@appID
--			 group by [AppID],[CategoryID],[KeyID]
--			 having count(*)>1)  and (PolygonID =@polygonID or PolygonID is null)) 
--			 union
--			---adding the values where we have the accurate id based on LocalTime
--			select [Configuration].[Category].[Name] AS CategoryName,[Configuration].[Key].Name AS KeyName,[Configuration].[AppCategoryKey_Settings].[Value],
--			[Configuration].[AppCategoryKey_Settings].[Type] 
--			from [Configuration].[Category] inner join
--			[Configuration].[AppCategoryKey_Settings] on 
--			[Configuration].[Category].[ID]=[Configuration].[AppCategoryKey_Settings].[CategoryID]
--			inner join [Configuration].[Key] on [Configuration].[Key].[ID]=[Configuration].[AppCategoryKey_Settings].[KeyID]
--			where [Configuration].[AppCategoryKey_Settings].[AppID]=@appID 
--			and (PolygonID =@polygonID or PolygonID is null) and [Configuration].[AppCategoryKey_Settings].id  in (select id from @Temptbl)
--		end
--   else
--		begin
--			---retrieve the config id without any time based on the polygonid
--		   	select @ConfigId=ISNULL(max(ID),0)  from [Configuration].[AppCategoryKey_Settings]
--			where AppID=@appID and CAST(@LocalTime as date) between StartDate and EndDate 
--			and CAST(@LocalTime as time) between StartTime and EndTime 
--			and  DayList like '%' + @DayName + '%' and  (PolygonID =@polygonID) 

--			---retrieve the config id without time is null and based on the polygonid
--			if(@ConfigId=0)
--			begin
--				select @ConfigId=ISNULL(max(ID),0) from  [Configuration].[AppCategoryKey_Settings]
--				where KeyID in ( select [KeyID] from [Configuration].[AppCategoryKey_Settings] 
--				where AppID=@appID	group by [AppID],[CategoryID],[KeyID]
--				having count(*)>1)  and (PolygonID =@polygonID)  and (startdate is null or enddate is null)
--				and (StartTime is null or EndTime is null)
--			end 
		
--			 ---removing the duplicates
--			select   [Configuration].[Category].[Name] AS CategoryName,[Configuration].[Key].Name AS KeyName,[Configuration].[AppCategoryKey_Settings].[Value],
--			[Configuration].[AppCategoryKey_Settings].[Type]
--			from [Configuration].[Category] inner join
--			[Configuration].[AppCategoryKey_Settings] on 
--			[Configuration].[Category].[ID]=[Configuration].[AppCategoryKey_Settings].[CategoryID]
--			inner join [Configuration].[Key] on [Configuration].[Key].[ID]=[Configuration].[AppCategoryKey_Settings].[KeyID]
--			where [Configuration].[AppCategoryKey_Settings].[AppID]=@appID
--			and (PolygonID =@polygonID or PolygonID is null) and 
--			[Configuration].[AppCategoryKey_Settings].ID not in (select ID from  [Configuration].[AppCategoryKey_Settings]
--			where KeyID in ( select [KeyID] from [Configuration].[AppCategoryKey_Settings] 
--			where AppID=@appID	group by [AppID],[CategoryID],[KeyID]
--			having count(*)>1)  and (PolygonID =@polygonID or PolygonID is null)) 
--			 union
--			---adding the values based on @ConfigId
--			select [Configuration].[Category].[Name] AS CategoryName,[Configuration].[Key].Name AS KeyName,[Configuration].[AppCategoryKey_Settings].[Value],
--			[Configuration].[AppCategoryKey_Settings].[Type] 
--			from [Configuration].[Category] inner join
--			[Configuration].[AppCategoryKey_Settings] on 
--			[Configuration].[Category].[ID]=[Configuration].[AppCategoryKey_Settings].[CategoryID]
--			inner join [Configuration].[Key] on [Configuration].[Key].[ID]=[Configuration].[AppCategoryKey_Settings].[KeyID]
--			where [Configuration].[AppCategoryKey_Settings].[AppID]=@appID 
--			and (PolygonID =@polygonID or PolygonID is null) and [Configuration].[AppCategoryKey_Settings].id  
--			in (@ConfigId)
			
	
--		end

-- END

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
     IF @@TRANCOUNT > 0
    ROLLBACK TRANSACTION;
	DECLARE @ErrorNumber INT = ERROR_NUMBER();
    DECLARE @ErrorLine INT = ERROR_LINE();
    DECLARE @ProcErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
    DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
    DECLARE @ErrorState INT = ERROR_STATE();

 RAISERROR(@ProcErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH
end



