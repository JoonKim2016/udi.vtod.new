﻿CREATE TABLE [Configuration].[ZipCode] (
    [ID]          INT            IDENTITY (1, 1) NOT NULL,
    [ZipCode]     INT            NULL,
    [Description] NVARCHAR (100) NULL,
    [Type]        NVARCHAR (10)  NULL,
    CONSTRAINT [PK_ZipCode] PRIMARY KEY CLUSTERED ([ID] ASC)
);

