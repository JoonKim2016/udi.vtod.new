﻿
CREATE PROCEDURE [Configuration].[sp_GetMainSettingById]
@id int
AS
BEGIN
	SET NOCOUNT ON;

    select s.*, c.Name as Category, k.Name as KeyType, z.ZipCode, a.Name as AppName
	from Configuration.AppCategoryKey_Settings s
	inner join Configuration.App_Settings a on s.AppId = a.ID
	inner join Configuration.Category c on s.CategoryID = c.ID
	inner join Configuration.[Key] k on s.KeyID = k.ID
	inner join Configuration.ZipCode z on s.ZipCodeID = z.ID
	where s.ID = @id 
END
