﻿
CREATE PROCEDURE [Configuration].[sp_GetMainSettings]
@category int,
@key int,
@zipcode int,
@enabled bit,
@pageIndex int,
@pageSize int,
@totalCount int output
AS
BEGIN
	SET NOCOUNT ON;

	select @totalCount = count(1) 
	from Configuration.AppCategoryKey_Settings s
	inner join Configuration.Category c on s.CategoryID = c.ID
	inner join Configuration.[Key] k on s.KeyID = k.ID
	inner join Configuration.ZipCode z on s.ZipCodeID = z.ID
	where (@category = 0 or s.CategoryID = @category)
		and (@key = 0 or s.KeyID = @key)
		and (@zipcode = 0 or s.ZipCodeID = @zipcode)
		and s.IsEnabled = @enabled
		and s.GroupID is null

    select * from
	(
		select row_number() over (order by s.ID) as rownum, s.*, c.Name as Category, k.Name as KeyType, z.ZipCode  
		from Configuration.AppCategoryKey_Settings s
		inner join Configuration.Category c on s.CategoryID = c.ID
		inner join Configuration.[Key] k on s.KeyID = k.ID
		inner join Configuration.ZipCode z on s.ZipCodeID = z.ID
		where (@category = 0 or CategoryID = @category)
			and (@key = 0 or KeyID = @key)
			and (@zipcode = 0 or ZipCodeID = @zipcode)
			and IsEnabled = @enabled
			and s.GroupID is null
	) t
	where rownum > (@pageSize * (@pageIndex - 1)) and rownum <= @pageSize * @pageIndex
END
