﻿
CREATE PROCEDURE [Configuration].[sp_GetPopOvers]
@from varchar(16),
@to varchar(16),
@name varchar(50),
@userId nvarchar(128),
@pageIndex int,
@pageSize int,
@totalCount int output
AS
BEGIN
	SET NOCOUNT ON;

	select @totalCount = count(1) 
	from Configuration.[Group]
	where (@from is null or @from = '' or [AppendTime] >= CONVERT(Datetime, @from, 120))
		and (@to is null or @to = '' or [AppendTime] < DATEADD(d, 1, CONVERT(Datetime, @to, 120)))
		and (@userId is null or @userId = '' or UserID = @userId)
		and Name like '%'+@name+'%'

    select * from
	(
		select row_number() over (order by ID desc) as rownum, Name, UserID, AppendTime, Description, IsEnabled  
		from Configuration.[Group]
		where (@from is null or @from = '' or [AppendTime] >= CONVERT(Datetime, @from, 120))
			and (@to is null or @to = '' or [AppendTime] < DATEADD(d, 1, CONVERT(Datetime, @to, 120)))
			and (@userId is null or @userId = '' or UserID = @userId)
			and Name like '%'+@name+'%'
	) t
	where rownum > (@pageSize * (@pageIndex - 1)) and rownum <= @pageSize * @pageIndex
END
