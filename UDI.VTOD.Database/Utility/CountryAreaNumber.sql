﻿CREATE TABLE [Utility].[CountryAreaNumber] (
    [ID]          INT IDENTITY (1, 1) NOT NULL,
    [CountryCode] INT NULL,
    [AreaCode]    INT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);

