﻿CREATE TABLE [Utility].[Group] (
    [ID]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (100) NULL,
    [Type]        INT            NULL,
    [IsBroadcast] INT            NULL,
    [AppendTime]  DATETIME2 (7)  NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);

