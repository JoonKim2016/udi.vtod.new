﻿CREATE TABLE [Utility].[GroupMember] (
    [ID]           BIGINT        IDENTITY (1, 1) NOT NULL,
    [GroupID]      BIGINT        NULL,
    [MemberID]     BIGINT        NULL,
    [EnableMember] BIT           NULL,
    [InsertedTime] DATETIME2 (0) NULL,
    [AppendTime]   DATETIME2 (0) NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);

