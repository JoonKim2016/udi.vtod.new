﻿CREATE TABLE [Utility].[Notification_Configuration] (
    [Id]                            BIGINT         IDENTITY (1, 1) NOT NULL,
    [FleetTripCode]                 NVARCHAR (500) NULL,
    [Status]                        NVARCHAR (500) NULL,
    [Event]                         NVARCHAR (100) NULL,
    [ImageUrl]                      NVARCHAR (100) NULL,
    [Text]                          NVARCHAR (200) NULL,
    [NotificationType]              NVARCHAR (50)  NULL,
    [ExpirationInMinutes]           INT            NULL,
    [EventPickUpTimeBasedonUTCTime] INT            NULL,
    [IsEnabled]                     BIT            NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

