﻿CREATE TABLE [Utility].[PushNotification] (
    [ID]                          BIGINT         IDENTITY (1, 1) NOT NULL,
    [NotificationConfigurationID] INT            NULL,
    [WorkFlow]                    INT            NULL,
    [Text]                        NVARCHAR (200) NULL,
    [Event]                       NVARCHAR (100) NULL,
    [ImageUrl]                    NVARCHAR (200) NULL,
    [VtodTripID]                  BIGINT         NULL,
    [NotificationType]            INT            NULL,
    [MemberID]                    BIGINT         NULL,
    [GroupID]                     BIGINT         NULL,
    [PushTimeUTC]                 DATETIME2 (7)  NULL,
    [PushType]                    NVARCHAR (50)  NULL,
    [ExpirationTime]              DATETIME2 (7)  NULL,
    [InsertedTime]                DATETIME2 (7)  NOT NULL,
    [AppendTime]                  DATETIME2 (7)  NULL,
    [UserName]                    NVARCHAR (100) NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);

