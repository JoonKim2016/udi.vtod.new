﻿CREATE TABLE [Utility].[ReferralTrip] (
    [ID]                 BIGINT          IDENTITY (1, 1) NOT NULL,
    [VtodTripID]            BIGINT          NOT NULL,
	[TripStatus]			nvarchar(50)	NULL,
    [SDSMemberID]           INT          NOT NULL,
    [ReferrerSDSMemberID]         INT          NOT NULL,
    [ReferralCreditFlow] INT             NOT NULL,
    [ReferralCreditAmount]     DECIMAL (10, 2) NULL,
    [ErrorMessage]       NVARCHAR (1000) NULL,
    [CreatedOn]       DATETIME2 (0)   NOT NULL,
    [CreditEarnedOn]         DATETIME2 (0)   NULL,
	[UpdatedOn]         DATETIME2 (0)   NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);

