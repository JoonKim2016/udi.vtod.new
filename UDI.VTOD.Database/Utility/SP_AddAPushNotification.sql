﻿
CREATE PROCEDURE [Utility].[SP_AddAPushNotification] 
@Event nvarchar(100),
@VtodTripID bigint,
@MemberID bigint,
@PushTimeUTC datetime2(7),
@PushType nvarchar(50),
@ExpirationTime datetime2(7),
@UserName nvarchar(100),
@Text nvarchar(200)
AS
BEGIN
	SET NOCOUNT ON;

    declare @ConfigurationID int
	declare @Template nvarchar(200)
	declare @NotificationType int

	select top 1 @ConfigurationID = ID, @Template = [Text], @NotificationType = NotificationType
	from Utility.Notification_Configuration 
	where [Event] = @Event and IsEnabled = 1

	declare @tb table
	(
		ID int IDENTITY(1,1) NOT NULL,
		Txt nvarchar(50)
	)

	insert into @tb(Txt)
	select Item  from [dbo].[SplitString](@Text, ',')

	declare @idx int
	declare @count int
	declare @txt nvarchar(50)
	set @idx = 1
	select @count = count(1) from @tb

	while @idx <= @count
	begin
		select @txt = Txt from @tb where ID = @idx
		set @Template = replace(@Template, '{' + cast(@idx as varchar(5)) + '}', @txt)

		set @idx = @idx + 1
	end

	insert into [Utility].[PushNotification]
           ([NotificationConfigurationID]
           ,[WorkFlow]
           ,[Text]
           ,[Event]
           ,[ImageUrl]
           ,[VtodTripID]
           ,[NotificationType]
           ,[MemberID]
           ,[GroupID]
           ,[PushTimeUTC]
           ,[PushType]
           ,[ExpirationTime]
           ,[InsertedTime]
           ,[AppendTime]
           ,[UserName])
     values
           (@ConfigurationID
           ,1
           ,@Template
           ,@Event
           ,null
           ,@VtodTripID
           ,@NotificationType
           ,@MemberID
           ,null
           ,@PushTimeUTC
           ,@PushType
           ,@ExpirationTime
           ,getdate()
           ,getdate()
           ,@UserName) 
END
