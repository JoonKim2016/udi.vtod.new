﻿
Create PROCEDURE [Utility].[SP_CheckVerification] 
@PhoneNumber nvarchar(100),
@MemberID bigint output
AS
BEGIN
	SET NOCOUNT ON;

    declare @count bigint

	select @count = count(1) 
	from Utility.Verification
	where Item = @PhoneNumber

	if @count = 0
	begin
		set @MemberID = -2
	end
	else
	begin
		select @count = count(1) 
		from Utility.Verification
		where Item = @PhoneNumber and IsVerified = 1
		
		if @count = 0
		begin
			set @MemberID = -1
		end	
		else
		begin
			select top 1 @MemberID = MemberID
			from Utility.Verification
			where Item = @PhoneNumber and IsVerified = 1
		end
	end
END
