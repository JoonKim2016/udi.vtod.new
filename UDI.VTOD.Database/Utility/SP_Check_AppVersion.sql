﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Utility].[SP_Check_AppVersion]
@App nvarchar(100),
@AppVersion nvarchar(100),
@Platform  nvarchar(100),
@PlatformVersion  nvarchar(100)
AS
BEGIN
BEGIN TRY
declare @count int
declare @NUMBER int
 BEGIN TRANSACTION;

   
   
 select @count=count(*) from vtod.Utility.vtod_AppVersionDetails
 where  App=@App and AppVersion=@AppVersion and Platform=@Platform and PlatformVersion=@PlatformVersion
 
 IF(@count=0)
 BEGIN
 IF(@Platform='IPHONE')
 BEGIN
  select @NUMBER=count(*) from vtod.Utility.vtod_AppVersionDetails
 where  App=@App and AppVersion=@AppVersion and Platform='IOS' and PlatformVersion=@PlatformVersion
 IF(@NUMBER>0)
 BEGIN
 SET @Platform='IOS'
 END
 END
  IF(@Platform='IOS')
 BEGIN
  select @NUMBER=count(*) from vtod.Utility.vtod_AppVersionDetails
 where  App=@App and AppVersion=@AppVersion and Platform='IPHONE' and PlatformVersion=@PlatformVersion
  IF(@NUMBER>0)
 BEGIN
 SET @Platform='IPHONE'
 END
 END

 END

 if(@count>=1)
 begin
 select top 1 [App]
      ,[AppVersion]
      ,[Platform]
      ,[PlatformVersion]
      ,[IsActive]
      ,[PushTimeUTC]
      ,[AppendTime]
      ,[AppVersionSupported]
      ,[PlatformVersionSupported] from vtod.Utility.vtod_AppVersionDetails
 where  App=@App and AppVersion=@AppVersion and Platform=@Platform and PlatformVersion=@PlatformVersion
 end
 else
 begin
 select @count=count(*) from vtod.Utility.vtod_AppVersionDetails
 where  App=@App and AppVersion=@AppVersion and Platform=@Platform 
 if(@count>=1)
 begin
  select @count=count(*) from vtod.Utility.vtod_AppVersionDetails
 where  App=@App and AppVersion=@AppVersion and Platform=@Platform and Lower(PlatformVersion)='all'
 if(@count>=1)
 begin
 select  top 1 [App]
      ,[AppVersion]
      ,[Platform]
      ,[PlatformVersion]
      ,[IsActive]
      ,[PushTimeUTC]
      ,[AppendTime]
      ,[AppVersionSupported]
      ,[PlatformVersionSupported] from vtod.Utility.vtod_AppVersionDetails
 where  App=@App and AppVersion=@AppVersion and Platform=@Platform and Lower(PlatformVersion)='all'
 end
 else
 begin
  select @count=count(*) from vtod.Utility.vtod_AppVersionDetails
 where  App=@App and AppVersion=@AppVersion and Lower(Platform)='all' and Lower(PlatformVersion)='all'
 if(@count>=1)
 begin
  select top 1 [App]
      ,[AppVersion]
      ,[Platform]
      ,[PlatformVersion]
      ,[IsActive]
      ,[PushTimeUTC]
      ,[AppendTime]
      ,[AppVersionSupported]
      ,[PlatformVersionSupported] from vtod.Utility.vtod_AppVersionDetails
 where  App=@App and AppVersion=@AppVersion and Lower(Platform)='all' and Lower(PlatformVersion)='all'
 end
 end
 end
 else
 begin
 select @count=count(*) from vtod.Utility.vtod_AppVersionDetails
 where  App=@App and AppVersion=@AppVersion and Platform=@Platform and Lower(PlatformVersion)='all'
 if(@count>=1)
 begin
 select  top 1 [App]
      ,[AppVersion]
      ,[Platform]
      ,[PlatformVersion]
      ,[IsActive]
      ,[PushTimeUTC]
      ,[AppendTime]
      ,[AppVersionSupported]
      ,[PlatformVersionSupported] from vtod.Utility.vtod_AppVersionDetails
 where  App=@App and AppVersion=@AppVersion and Platform=@Platform and Lower(PlatformVersion)='all'
 end
 else
 begin
  select @count=count(*) from vtod.Utility.vtod_AppVersionDetails
 where  App=@App and AppVersion=@AppVersion and Lower(Platform)='all' and Lower(PlatformVersion)='all'
 if(@count>=1)
 begin
  select top 1 [App]
      ,[AppVersion]
      ,[Platform]
      ,[PlatformVersion]
      ,[IsActive]
      ,[PushTimeUTC]
      ,[AppendTime]
      ,[AppVersionSupported]
      ,[PlatformVersionSupported] from vtod.Utility.vtod_AppVersionDetails
 where  App=@App and AppVersion=@AppVersion and Lower(Platform)='all' and Lower(PlatformVersion)='all'
 end
 end
 end
 end
 if(@count=0)
 begin

SELECT  [Id]
      ,[App]
      ,[AppVersion]
      ,[Platform]
      ,[PlatformVersion]
      ,[IsActive]
      ,[PushTimeUTC]
      ,[AppendTime]
      ,[AppVersionSupported]
      ,[PlatformVersionSupported]
  FROM [vtod].[Utility].[vtod_AppVersionDetails]
  where 1=2
 end
 COMMIT TRANSACTION;
END TRY
BEGIN CATCH
     IF @@TRANCOUNT > 0
    ROLLBACK TRANSACTION;
	DECLARE @ErrorNumber INT = ERROR_NUMBER();
    DECLARE @ErrorLine INT = ERROR_LINE();
    DECLARE @ProcErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
    DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
    DECLARE @ErrorState INT = ERROR_STATE();

 RAISERROR(@ProcErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH
end

