﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Utility].[SP_PushNotificationAleph]
@MemberID bigint null,
@GroupID bigint null,
@VTodTripID bigint null,
@PushTimeUTC datetime2(7) null,
@PushType nvarchar(50) null,
@NotificationType  nvarchar(50) null,
@Text nvarchar(200) null,
@UserName nvarchar(100) null
AS
BEGIN
BEGIN TRY
   BEGIN TRANSACTION;
   DECLARE @notifyType nvarchar(50)
   if(UPPER(@NotificationType)='SMS')
   BEGIN
  SET  @notifyType=2;
   END
   if(UPPER(@NotificationType)='PUSH')
   BEGIN
   SET  @notifyType=1;
   END
INSERT INTO [Utility].[PushNotification]
           ([WorkFlow]
			,Text
           ,[VtodTripID]
           ,[MemberID]
           ,[GroupID]
           ,[PushTimeUTC]
           ,[PushType]
           ,[ExpirationTime]
           ,[InsertedTime]
           ,[AppendTime],NotificationType,UserName)
		   values(1,@Text,@VTodTripID,@MemberID,@GroupID,@PushTimeUTC,@PushType,DATEADD(minute,30,@PushTimeUTC),getdate(),getdate(),@notifyType,@UserName)

 COMMIT TRANSACTION;

END TRY
BEGIN CATCH
     IF @@TRANCOUNT > 0
    ROLLBACK TRANSACTION;
	DECLARE @ErrorNumber INT = ERROR_NUMBER();
    DECLARE @ErrorLine INT = ERROR_LINE();
    DECLARE @ProcErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
    DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
    DECLARE @ErrorState INT = ERROR_STATE();

 RAISERROR(@ProcErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH
end


