﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Utility].[SP_PushNotificationInsertion]
@MemberID bigint null,
@GroupID bigint null,
@VTodTripID bigint null,
@PushTimeUTC datetime2(7) null,
@PushType nvarchar(50) null,
@FleetTripCode int null,
@Status nvarchar(200),
@Vehiclenumber nvarchar(200),
@Firstname nvarchar(200),
@AllowSMS bit,
@UserName nvarchar(100) null,
@PushNotificationWorkflow int null
AS
BEGIN
BEGIN TRY
   BEGIN TRANSACTION;
   
 declare @FleetCode nvarchar(50)
 set @FleetCode=CONVERT(varchar,@FleetTripCode)
 if(@AllowSMS=1)
begin
INSERT INTO [Utility].[PushNotification]
           ([WorkFlow]
			,Text
           ,Event
           ,ImageUrl
           ,[VtodTripID]
           ,[MemberID]
           ,[GroupID]
           ,[PushTimeUTC]
           ,[PushType]
           ,[ExpirationTime]
           ,[InsertedTime]
           ,[AppendTime],NotificationType,NotificationConfigurationID,UserName)
     
	  select @PushNotificationWorkflow,[Text],[Event],[ImageUrl],@VTodTripID,@MemberID,@GroupID,DATEADD(MINUTE,-EventPickUpTimeBasedonUTCTime,@PushTimeUTC)
           ,@PushType
           ,DATEADD(MINUTE,ExpirationInMinutes,@PushTimeUTC)
           ,GETDATE()
           ,GETDATE(),NotificationType,Id,@UserName
    from  [Utility].[Notification_Configuration]
   where 
   (Lower(Status) LIKE '%,' + @Status + ',%' 
  OR
      Lower(Status) LIKE @Status + ',%' 
      OR
      Lower(Status) LIKE '%,' + @Status 
      OR 
      Lower(Status) =  @Status)
    
   and 
   (FleetTripCode LIKE '%,' + @FleetCode + ',%' 
  OR
      FleetTripCode LIKE @FleetCode + ',%' 
      OR
      FleetTripCode LIKE '%,' + @FleetCode 
      OR 
      FleetTripCode =  @FleetCode
    OR 
      FleetTripCode =  'All')
   
   and Id not in  (select NotificationConfigurationID from [Utility].[PushNotification] where   
   MemberID=@MemberID and VtodTripID=@VTodTripID) and IsEnabled=1
   and Event<>'SplitPayment'
end
else if(@AllowSMS=0)
begin
INSERT INTO [Utility].[PushNotification]
           ([WorkFlow]
			,Text
           ,Event
           ,ImageUrl
           ,[VtodTripID]
           ,[MemberID]
           ,[GroupID]
           ,[PushTimeUTC]
           ,[PushType]
           ,[ExpirationTime]
           ,[InsertedTime]
           ,[AppendTime],NotificationType,NotificationConfigurationID,UserName)
     
	  select @PushNotificationWorkflow,[Text],[Event],[ImageUrl],@VTodTripID,@MemberID,@GroupID,DATEADD(MINUTE,-EventPickUpTimeBasedonUTCTime,@PushTimeUTC)
           ,@PushType
           ,DATEADD(MINUTE,ExpirationInMinutes,@PushTimeUTC)
           ,GETDATE()
           ,GETDATE(),NotificationType,Id,@UserName
    from  [Utility].[Notification_Configuration]
   where 
   (Lower(Status) LIKE '%,' + @Status + ',%' 
  OR
      Lower(Status) LIKE @Status + ',%' 
      OR
      Lower(Status) LIKE '%,' + @Status 
      OR 
      Lower(Status) =  @Status)
    
   and 
   (FleetTripCode LIKE '%,' + @FleetCode + ',%' 
  OR
      FleetTripCode LIKE @FleetCode + ',%' 
      OR
      FleetTripCode LIKE '%,' + @FleetCode 
      OR 
      FleetTripCode =  @FleetCode
    OR 
      FleetTripCode =  'All')
   
   and Id not in  (select NotificationConfigurationID from [Utility].[PushNotification] where   
   MemberID=@MemberID and VtodTripID=@VTodTripID) and IsEnabled=1 and NotificationType=1  and Event<>'SplitPayment'
end
   if @Firstname is not null
   begin
   update [Utility].[PushNotification] set Text=Replace(Text,'{FirstName}', @Firstname)
   where  Id in (select Id from [Utility].[PushNotification]
   where VtodTripID=@VTodTripID  and Text is not null)
   end 
   if @Vehiclenumber is not null
   begin
   update [Utility].[PushNotification] set Text=Replace(Text,'{CarNumber}', @Vehiclenumber)
   where Id in (select Id from [Utility].[PushNotification]
   where VtodTripID=@VTodTripID  and Text is not null)
   end
   else
   begin
   update [Utility].[PushNotification] set Text=Replace(Text,'Look for car number {CarNumber}!', '')
   where Id in (select Id from [Utility].[PushNotification]
   where VtodTripID=@VTodTripID  and Text is not null)
   end
   ----vtod trip id insertion start-------
   -- update [Utility].[PushNotification] set Text=Convert(VarChar(100),Text) + Convert(VarChar(10),' Trip ID: ') + Convert(VarChar(10),@VTodTripID)
   --where Id in (select Id from [Utility].[PushNotification]
   --where VtodTripID=@VTodTripID  and Text is not null)
    ----vtod trip id insertion end-------
	if (Lower(@Status)='canceled') or (Lower(@Status)='completed')
	begin
   update dbo.vtod_trip_process set CheckTripStatus=1
    where Id=@VTodTripID 
	end
 COMMIT TRANSACTION;

END TRY
BEGIN CATCH
     IF @@TRANCOUNT > 0
    ROLLBACK TRANSACTION;
	DECLARE @ErrorNumber INT = ERROR_NUMBER();
    DECLARE @ErrorLine INT = ERROR_LINE();
    DECLARE @ProcErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
    DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
    DECLARE @ErrorState INT = ERROR_STATE();

 RAISERROR(@ProcErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH
end

