﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Utility].[SP_TripReminderNotificationUpdation]
@MemberID bigint null,
@VTodTripID bigint null,
@WorkFlow int,
@NextFlow int
AS
BEGIN
BEGIN TRY
  BEGIN TRANSACTION;
  DECLARE @Id nvarchar(50)

 select @Id=Id from [Utility].[PushNotification]
 where  [VtodTripID]=@VTodTripID and [MemberID]=@MemberID and WorkFlow=4
 and Event='TripReminder'

update [Utility].[PushNotification]
set WorkFlow=@NextFlow,PushTimeUTC=GETUTCDATE()
where ID=@Id
 COMMIT TRANSACTION;

END TRY
BEGIN CATCH
     IF @@TRANCOUNT > 0
    ROLLBACK TRANSACTION;
	DECLARE @ErrorNumber INT = ERROR_NUMBER();
    DECLARE @ErrorLine INT = ERROR_LINE();
    DECLARE @ProcErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
    DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
    DECLARE @ErrorState INT = ERROR_STATE();

 RAISERROR(@ProcErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH
end


