﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Utility].[SP_Utility_CheckTripStatus]  
	@StartTime datetime null,
	@EndTime datetime null,
	@CreditType int,
	@CreditFlow int,
	@NextCreditFlow int
AS
BEGIN
--1 Notification
---2 Referral
---11 Notification + Referral
	SET NOCOUNT ON;
	BEGIN TRY

DECLARE @FirstTable TABLE (id_table int identity(1,1) not null,tripID bigint,ID INT,ProcessType int,memberID bigint,FirstName nvarchar(100),FleetTripCode int,Workflow int)

--update Utility.ReferralTrip
--set ReferralCreditFlow=@CreditFlow
--where ReferralCreditFlow=@NextCreditFlow
--and ReferralCreditType=@CreditType

insert into @FirstTable(tripID,ID,ProcessType,memberID,FleetTripCode,WorkFlow) 
select vtod_trip.ID,0,1,MemberID,FleetTripCode,0 from vtod_trip inner join
vtod_trip_process on  vtod_trip.Id=vtod_trip_process.Id
 where   PickupDateTimeUTC between @StartTime and  @EndTime
and vtod_trip.UserID=11
and vtod_trip_process.CheckTripStatus is null or vtod_trip_process.CheckTripStatus<1

 --insert into @FirstTable(tripID,ID,ProcessType,memberID,FleetTripCode,WorkFlow) 
 --select vtod_trip.ID,Utility.ReferralTrip.ID,2,vtod_trip.MemberID,FleetTripCode,0 from vtod_trip INNER JOIN
 --Utility.ReferralTrip ON vtod_trip.ID=Utility.ReferralTrip.TripID
 -- where  [ReferralCreditType]=@CreditType
 --    and [ReferralCreditFlow]=@CreditFlow
	-- and PickupDateTimeUTC  between @StartTime and  @EndTime
	-- and vtod_trip.UserID=11
	----- AND  vtod_trip.Id  in (select id from [dbo].[vtod_trip_process] where vtod_trip_process.CheckTripStatus is null or vtod_trip_process.CheckTripStatus<1)

--update   Utility.ReferralTrip set
--ReferralCreditFlow=@NextCreditFlow
--where ID IN (select ID from @FirstTable where ProcessType=2)
	

update @FirstTable  set ProcessType=11
where tripID in (select tripID from @FirstTable GROUP BY TRIPID  HAVING COUNT(*)>1)

DELETE from @FirstTable
where id_table  in (
   select min(id_table) from @FirstTable
   group by tripID
   having count(*) > 1)

update a  set FirstName=b.FirstName from 
 @FirstTable a inner join sds_trip as b
on  b.id= a.tripID

update a  set FirstName=b.FirstName from 
 @FirstTable a inner join ecar_trip as b
on  b.id= a.tripID

update a  set FirstName=b.FirstName from 
 @FirstTable a inner join taxi_trip as b
on  b.id= a.tripID

update a  set FirstName=b.FirstName from 
 @FirstTable a inner join van_trip as b
on  b.id= a.tripID

/*Trip Reminder*/

/*Start*/

update [Utility].[PushNotification]
set workflow=-5
where id in(
select [Utility].[PushNotification].Id from vtod_trip inner join
[Utility].[PushNotification] on vtod_trip.id=[Utility].[PushNotification].VtodTripID
where   PickupDateTimeUTC<PushTimeUTC and vtod_trip.UserID=11 and WorkFlow=4)

insert into @FirstTable(tripID,ID,ProcessType,memberID,FleetTripCode,WorkFlow) 
select vtod_trip.ID,0,1,vtod_trip.MemberID,FleetTripCode,WorkFlow from vtod_trip inner join
[Utility].[PushNotification] on vtod_trip.id=[Utility].[PushNotification].VtodTripID
where   PickupDateTimeUTC>=PushTimeUTC and vtod_trip.UserID=11 and WorkFlow=4

/*End*/

select distinct * from  @FirstTable order by tripID desc

   
END TRY
BEGIN CATCH
     IF @@TRANCOUNT > 0
    ROLLBACK TRANSACTION;
	DECLARE @ErrorNumber INT = ERROR_NUMBER();
    DECLARE @ErrorLine INT = ERROR_LINE();
    DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
    DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
    DECLARE @ErrorState INT = ERROR_STATE();

 RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH
END

