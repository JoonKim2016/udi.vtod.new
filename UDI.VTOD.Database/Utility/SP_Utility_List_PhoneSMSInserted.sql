﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Utility].[SP_Utility_List_PhoneSMSInserted]  
	@Status int,
	@PrevStatus int,
	@DuplicateStatus int,
	@ModeID int,
	@TypeID int,
	@ExceptionStatus int
AS
BEGIN
Declare @numRow int
Declare @ID INT
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	BEGIN TRY
	
    -- Insert statements for procedure here
  if(@PrevStatus = 1)
  begin
   BEGIN TRANSACTION

	---Start the Processing of sending SMS checking the number 
    Update Utility.VerificationHistory set VerificationFlow=@Status
       where ID in (select max(ID) from Utility.VerificationHistory where  VerificationFlow=@PrevStatus
	and ExpirationTimeUTC>GetDate() and TypeID=@TypeID
	GROUP BY(item))

	---Making the other id duplicate

	Update Utility.VerificationHistory set VerificationFlow=@DuplicateStatus
       where ID not in (select max(ID) from Utility.VerificationHistory where  VerificationFlow=@Status
	and ExpirationTimeUTC>GetDate()  and TypeID=@TypeID
	GROUP BY(item)) AND VerificationFlow=@PrevStatus


	

	select @numRow=Count(*) from Utility.VerificationHistory where  VerificationFlow=@Status
	and ExpirationTimeUTC>GetDate()  and TypeID=@TypeID
	GROUP BY(MemberID) having count(*) > 1
	if(@numRow>1)
	begin
	UPDATE Utility.VerificationHistory SET VerificationFlow=@ExceptionStatus
		where ID NOT IN (select max(ID) from Utility.VerificationHistory where  VerificationFlow=@Status
	and ExpirationTimeUTC>GetDate()  and TypeID=@TypeID
	GROUP BY MemberID) AND verificationflow=@Status  and TypeID=@TypeID
	end

	COMMIT TRANSACTION
   SELECT Utility.VerificationHistory.ID,Utility.VerificationHistory.Item,Utility.VerificationHistory.MemberID,Utility.VerificationHistory.VerificationFlow
   ,Utility.VerificationHistory.IsVerified,Utility.VerificationHistory.AppendTime,Utility.VerificationHistory.InsertedTimeUTC,VerificationType,VerificationCode
   
   from Utility.VerificationHistory 
   INNER JOIN
   Utility.Verification ON Utility.VerificationHistory.Item=Utility.Verification.Item
   AND Utility.VerificationHistory.MemberID=Utility.Verification.MemberID
   where Utility.VerificationHistory.ID in (
   select max(Utility.VerificationHistory.ID) from Utility.VerificationHistory 
   where  VerificationFlow=@status
	and ExpirationTimeUTC>GetDate() and Utility.VerificationHistory.TypeID=@TypeID 
	
	GROUP BY(Utility.VerificationHistory.item)) 
	
	end
	else
	begin

	select @numRow=Count(*) from Utility.VerificationHistory where  VerificationFlow=@Status
	and ExpirationTimeUTC>GetDate()  and TypeID=@TypeID
	GROUP BY(MemberID) having count(*) > 1
	if(@numRow>1)
	begin
	
	UPDATE Utility.VerificationHistory SET VerificationFlow=@ExceptionStatus
		where ID NOT IN (select max(ID) from Utility.VerificationHistory where  VerificationFlow=@Status
	and ExpirationTimeUTC>GetDate()  and TypeID=@TypeID
	GROUP BY MemberID) AND verificationflow=@Status  and TypeID=@TypeID
	end
	--SELECT * from Utility.VerificationHistory where ID in (
 --  select max(ID) from Utility.VerificationHistory where  VerificationFlow=@status
	--and ExpirationTimeUTC>GetDate()  and TypeID=@TypeID
	--GROUP BY (item))

	SELECT distinct Utility.VerificationHistory.ID,Utility.VerificationHistory.Item,Utility.VerificationHistory.MemberID,Utility.VerificationHistory.VerificationFlow
   ,Utility.VerificationHistory.IsVerified,Utility.VerificationHistory.AppendTime,Utility.VerificationHistory.InsertedTimeUTC,VerificationType,VerificationCode
   
   from Utility.VerificationHistory 
   INNER JOIN
   Utility.Verification ON Utility.VerificationHistory.Item=Utility.Verification.Item
   AND Utility.VerificationHistory.MemberID=Utility.Verification.MemberID
   where Utility.VerificationHistory.ID in  (
   select max(Utility.VerificationHistory.ID) from Utility.VerificationHistory INNER JOIN
   Utility.Verification ON Utility.VerificationHistory.Item=Utility.Verification.Item
   AND Utility.VerificationHistory.MemberID=Utility.Verification.MemberID
    where  Utility.VerificationHistory.VerificationFlow=@Status
	and Utility.VerificationHistory.ExpirationTimeUTC>GetDate()  and Utility.VerificationHistory.TypeID=@TypeID
	GROUP BY (Utility.VerificationHistory.item)
	

	)
	end
	END TRY
BEGIN CATCH
     IF @@TRANCOUNT > 0
    ROLLBACK TRANSACTION;
	DECLARE @ErrorNumber INT = ERROR_NUMBER();
    DECLARE @ErrorLine INT = ERROR_LINE();
    DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
    DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
    DECLARE @ErrorState INT = ERROR_STATE();

 RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH
END

