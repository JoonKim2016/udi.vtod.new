﻿



CREATE  PROCEDURE [Utility].[SP_Utility_PhoneSMS_Code_Verification]  

  
  @Type nvarchar(50),
  @PhoneNumber nvarchar(100),
  @MemberId bigint,
  @VerificationFlow int,
  @IsVerified bit,
  @Code bigint,
  @PrevVerificationFlow int
AS 
BEGIN
declare @Status int
declare @count int
BEGIN TRY


   BEGIN TRANSACTION;
	
		set @Status=0
		update [Utility].[Verification]
		set [IsVerified]=@IsVerified
		where TypeID=@Type and [Item]=@PhoneNumber and [MemberID]=@MemberId and [VerificationCode]=@Code
		
		IF @@ROWCOUNT = 0
		begin
		set @Status=1
		end
		if(@Status=0)
		begin
		update [Utility].[VerificationHistory]
		set [IsVerified]=@IsVerified,[VerificationFlow]=@VerificationFlow
		where TypeID=@Type and [Item]=@PhoneNumber and [MemberID]=@MemberId 

		delete from [Utility].[Verification]
		where TypeID=@Type and [Item]=@PhoneNumber and [MemberID]<>@MemberId 
		and [IsVerified]=0

		update [Utility].[VerificationHistory]
		set [IsVerified]=0,[VerificationFlow]=5
		where TypeID=@Type and [Item]=@PhoneNumber and [MemberID]<>@MemberId 

		end
	

    COMMIT TRANSACTION;
select @Status
END TRY
BEGIN CATCH
     IF @@TRANCOUNT > 0
    ROLLBACK TRANSACTION;
	DECLARE @ErrorNumber INT = ERROR_NUMBER();
    DECLARE @ErrorLine INT = ERROR_LINE();
    DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
    DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
    DECLARE @ErrorState INT = ERROR_STATE();

 RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH
END



