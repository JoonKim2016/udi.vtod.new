﻿

CREATE PROCEDURE [Utility].[SP_Utility_PhoneSMS_Insertion] 
   @ModeID int,
   @Mode varchar(50),
   @TypeID int,
   @Type varchar(50),
   @Item nvarchar(100),
   @MemberID bigint null,
   @MemberUserName nvarchar(100) null,
   @VerificationFlow int,
   @ExpirationTimeUTC int,
   @VerificationType int,
   @VerificationCode bigint
AS 
BEGIN

BEGIN TRY
   BEGIN TRANSACTION;
    INSERT INTO [Utility].VerificationHistory
           ([TypeID]
           ,[Type]
           ,[Item]
           ,[IsVerified]
           ,[MemberID]
           ,[MemberUserName]
           ,[VerificationFlow]
           ,[ExpirationTimeUTC]
           ,[AppendTime],[InsertedTimeUTC])
     VALUES
           (@TypeID, 
           @Type,
           @Item,
           0,
           @MemberID,
           @MemberUserName,
           @VerificationFlow,
           DATEADD(MINUTE, @ExpirationTimeUTC,GETUTCDATE()),
           GETDATE(),GETUTCDATE())
 INSERT INTO [Utility].[Verification]
           ([TypeID]
           ,[Type]
           ,[Item]
           ,[IsVerified]
           ,[MemberID]
           ,[AppendTime],[VerificationType],[VerificationCode])
     VALUES
           (@ModeID,
		   @Mode,
           @Item,
           0,
           @MemberID,
           GETDATE(),@VerificationType,@VerificationCode)

    COMMIT TRANSACTION;

END TRY
BEGIN CATCH
     IF @@TRANCOUNT > 0
    ROLLBACK TRANSACTION;
	DECLARE @ErrorNumber INT = ERROR_NUMBER();
    DECLARE @ErrorLine INT = ERROR_LINE();
    DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
    DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
    DECLARE @ErrorState INT = ERROR_STATE();

 RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH
END
