﻿



CREATE  PROCEDURE [Utility].[SP_Utility_PhoneSMS_Updation]  
  @ID BIGINT,
  @VerificationFlow int,
  @IsVerified bit,
  @TypeID int,
  @ErrorMsg nvarchar(500)
AS 
BEGIN

BEGIN TRY
Declare @phoneNumber varchar(100)
DECLARE @memberID bigint
Declare @DuplicateID bigint
--if LEN(@ID) > 0 SET @ID = @ID + ',' 
--CREATE TABLE #ARRAY(ID bigint)
--declare @S bigint
--WHILE LEN(@ID) > 0 BEGIN
--   SELECT @S = LTRIM(SUBSTRING(@ID, 1, CHARINDEX(',', @ID) - 1))
--   INSERT INTO #ARRAY (ID) VALUES (@S)
--   SELECT @ID = SUBSTRING(@ID, CHARINDEX(',', @ID) + 1, LEN(@ID))
--END

   BEGIN TRANSACTION;
		
		 select @phoneNumber=item,@memberID=MemberID from [Utility].VerificationHistory
		  where ID =@ID   

		 update  [Utility].VerificationHistory
		set VerificationFlow=@VerificationFlow,IsVerified=@IsVerified,ErrorMessage=@ErrorMsg
		 where ID =@ID   and MemberID=@memberID and item=@phoneNumber

		
		

		  update [Utility].VerificationHistory 
		  set VerificationFlow=5 where ID<>@ID and item=@phoneNumber and MemberID=@memberID

		   
		update  [Utility].Verification
		set IsVerified=@IsVerified
		 where item=@phoneNumber and   MemberID=@MemberID and  TypeID=@TypeID

		
		
    COMMIT TRANSACTION;

END TRY
BEGIN CATCH
     IF @@TRANCOUNT > 0
    ROLLBACK TRANSACTION;
	DECLARE @ErrorNumber INT = ERROR_NUMBER();
    DECLARE @ErrorLine INT = ERROR_LINE();
    DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
    DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
    DECLARE @ErrorState INT = ERROR_STATE();

 RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH
END



