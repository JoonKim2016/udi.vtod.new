﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Utility].[SP_Utility_PhoneSMS_Verification]
	@phoneNUmber nvarchar(100),
	@memberID bigint,
	@TypeID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF(@memberID>0)
	BEGIN
	SELECT ISNULL(IsVerified,0) from Utility.Verification where ID in 
	(select max(ID) from 
	Utility.Verification where item=@phoneNUmber and MemberID=@memberID and TypeID=@TypeID
	group by(item))
	END
	ELSE
	BEGIN
	SELECT ISNULL(IsVerified,0) from Utility.Verification where ID in 
	(select max(ID) from 
	Utility.Verification where item=@phoneNUmber and  TypeID=@TypeID
	group by(item))
	END

	
END
