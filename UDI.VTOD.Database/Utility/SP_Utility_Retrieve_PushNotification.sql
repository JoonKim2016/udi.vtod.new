﻿



CREATE  PROCEDURE [Utility].[SP_Utility_Retrieve_PushNotification] 
  @currentWorkFlowStatus int,
  @nextWorkFlowStatus int,
  @pushType nvarchar(50),
  @broadcastWorkFlowStatus int

AS 
BEGIN


BEGIN TRY
declare @pos int
declare @len int
declare @value nvarchar(50)

   BEGIN TRANSACTION;
   DECLARE @pushNotificationTable TABLE (ID bigint,memberID bigint null,DisplayText nvarchar(500) null,Event nvarchar(100) null,ImageUrl nvarchar(max),IsBroadcast int null,NotificationType int null,VtodTripID bigint null)

   set @pos = 0
   set @len = 0		

		WHILE CHARINDEX(',', @pushType, @pos+1)>0
		BEGIN
		set @len = CHARINDEX(',', @pushType, @pos+1) - @pos
		set @value = SUBSTRING(@pushType, @pos, @len)

		if(@value=1)
		begin
		insert into @pushNotificationTable(ID,memberID,DisplayText,Event,ImageUrl,NotificationType,VtodTripID)
		select ID,MemberID,Text,Event,ImageUrl,NotificationType,VtodTripID from [Utility].[PushNotification] where PushType=@value
		and MemberID is not null and workflow=@currentWorkFlowStatus and PushTimeUTC<=GETUTCDATE()
	

		update [Utility].[PushNotification]
		set workflow=@nextWorkFlowStatus
		where PushType=@value
		and MemberID is not null and workflow=@currentWorkFlowStatus  and PushTimeUTC<=GETUTCDATE()
		

		
		insert into @pushNotificationTable(ID,memberID,DisplayText,Event,ImageUrl,NotificationType,VtodTripID)
		select [Utility].[PushNotification].ID,[Utility].[GroupMember].MemberID,Text,Event,ImageUrl,NotificationType,VtodTripID from [Utility].[PushNotification] 
		inner join [Utility].[GroupMember] on
		[Utility].[PushNotification].GroupID=[Utility].[GroupMember].[GroupID]
		where PushType=@value and workflow=@currentWorkFlowStatus AND  [Utility].[PushNotification].MemberID IS NULL  and PushTimeUTC<=GETUTCDATE()
	

		update [Utility].[PushNotification]
		set workflow=@nextWorkFlowStatus
		where ID in (select [Utility].[PushNotification].ID   from [Utility].[PushNotification] 
		inner join [Utility].[GroupMember] on
		[Utility].[PushNotification].GroupID=[Utility].[GroupMember].[GroupID]
		where PushType=@value and workflow=@currentWorkFlowStatus  AND  [Utility].[PushNotification].MemberID IS NULL  and PushTimeUTC<=GETUTCDATE()) and PushTimeUTC<=GETUTCDATE()
		
		
		insert into @pushNotificationTable(ID,memberID,DisplayText,Event,ImageUrl,IsBroadcast,NotificationType,VtodTripID)
		select [Utility].[PushNotification].ID,[Utility].[Group].ID,Text,Event,ImageUrl,IsBroadcast,NotificationType,VtodTripID from [Utility].[PushNotification]
		INNER JOIN [Utility].[Group] ON [Utility].[PushNotification].GroupID=[Utility].[Group].ID
		where IsBroadcast=@broadcastWorkFlowStatus AND
		 PushType=@value and workflow=@currentWorkFlowStatus  and PushTimeUTC<=GETUTCDATE()
		
		
		update [Utility].[PushNotification]
		set workflow=@nextWorkFlowStatus
		where ID in (
		select [Utility].[PushNotification].ID from [Utility].[PushNotification]
		INNER JOIN [Utility].[Group] ON [Utility].[PushNotification].GroupID=[Utility].[Group].ID
		where IsBroadcast=@broadcastWorkFlowStatus AND
		 PushType=@value and workflow=@currentWorkFlowStatus  and PushTimeUTC<=GETUTCDATE()) and PushTimeUTC<=GETUTCDATE()
		

		end
		else if(@value=2)
		begin
		insert into @pushNotificationTable(ID,memberID,DisplayText,Event,ImageUrl,NotificationType,VtodTripID)
		select ID,MemberID,Text,Event,ImageUrl,NotificationType,VtodTripID from [Utility].[PushNotification] where PushType=@value
		and MemberID is not null and workflow=@currentWorkFlowStatus and PushTimeUTC <= GETUTCDATE() 
		

		update [Utility].[PushNotification]
		set workflow=@nextWorkFlowStatus
		where PushType=@value
		and MemberID is not null and workflow=@currentWorkFlowStatus and PushTimeUTC <= GETUTCDATE()
		

		insert into @pushNotificationTable(ID,memberID,DisplayText,Event,ImageUrl,NotificationType,VtodTripID)
		select [Utility].[PushNotification].ID,[Utility].[GroupMember].MemberID,Text,Event,ImageUrl,NotificationType,VtodTripID from [Utility].[PushNotification] 
		inner join [Utility].[GroupMember] on
		[Utility].[PushNotification].GroupID=[Utility].[GroupMember].[GroupID]
		where PushType=@value and workflow=@currentWorkFlowStatus and PushTimeUTC <= GETUTCDATE()  AND  [Utility].[PushNotification].MemberID IS NULL
	

		update [Utility].[PushNotification]
		set workflow=@nextWorkFlowStatus
		where ID in (select [Utility].[PushNotification].ID  from [Utility].[PushNotification] 
		inner join [Utility].[GroupMember] on
		[Utility].[PushNotification].GroupID=[Utility].[GroupMember].[GroupID]
		where PushType=@value and workflow=@currentWorkFlowStatus and PushTimeUTC <= GETUTCDATE()  AND  [Utility].[PushNotification].MemberID IS NULL)  and PushTimeUTC<=GETUTCDATE()
		
		
			insert into @pushNotificationTable(ID,memberID,DisplayText,Event,ImageUrl,IsBroadcast,NotificationType,VtodTripID)
		select [Utility].[PushNotification].ID,[Utility].[Group].ID,Text,Event,ImageUrl,IsBroadcast,NotificationType,VtodTripID from [Utility].[PushNotification]
		INNER JOIN [Utility].[Group] ON [Utility].[PushNotification].GroupID=[Utility].[Group].ID
		where IsBroadcast=@broadcastWorkFlowStatus AND
		 PushType=@value and workflow=@currentWorkFlowStatus  and PushTimeUTC<=GETUTCDATE()

		 update [Utility].[PushNotification]
		set workflow=@nextWorkFlowStatus
		where ID in (
		select [Utility].[PushNotification].ID from [Utility].[PushNotification]
		INNER JOIN [Utility].[Group] ON [Utility].[PushNotification].GroupID=[Utility].[Group].ID
		where IsBroadcast=@broadcastWorkFlowStatus AND
		 PushType=@value and workflow=@currentWorkFlowStatus  and PushTimeUTC<=GETUTCDATE())  and PushTimeUTC<=GETUTCDATE()
		end

		set @pos = CHARINDEX(',', @pushType, @pos+@len) +1
	END
	select * from @pushNotificationTable
    COMMIT TRANSACTION;

	
END TRY
BEGIN CATCH
     IF @@TRANCOUNT > 0
    ROLLBACK TRANSACTION;
	DECLARE @ErrorNumber INT = ERROR_NUMBER();
    DECLARE @ErrorLine INT = ERROR_LINE();
    DECLARE @ProcErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
    DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
    DECLARE @ErrorState INT = ERROR_STATE();

 RAISERROR(@ProcErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH
END


