﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Utility].[SP_ValidateAreaCountryCode] 

@phoneNumber varchar(50)

AS
BEGIN
Declare @status int
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select @status= COUNT(CountryCode)
	from Utility.CountryAreaNumber
	where CountryCode=SUBSTRING(@phoneNumber,1,1)

	if(@status>0)
	begin
	select @status= COUNT(AreaCode)
	from Utility.CountryAreaNumber
	where AreaCode=SUBSTRING(@phoneNumber,2,3)
	if(@status=0)
	begin
	select @status=2
	end
	else
	begin
	select @status=3
	end
	end
	else
	begin
	select @status=1
	end
    select @status

END



