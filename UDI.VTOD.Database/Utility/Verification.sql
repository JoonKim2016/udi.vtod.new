﻿CREATE TABLE [Utility].[Verification] (
    [ID]               BIGINT         IDENTITY (1, 1) NOT NULL,
    [TypeID]           INT            NOT NULL,
    [Type]             VARCHAR (50)   NOT NULL,
    [Item]             NVARCHAR (100) NOT NULL,
    [IsVerified]       BIT            NOT NULL,
    [MemberID]         BIGINT         NULL,
    [AppendTime]       DATETIME2 (0)  NOT NULL,
    [VerificationType] INT            NULL,
    [VerificationCode] BIGINT         NULL,
    CONSTRAINT [PK_Verification] PRIMARY KEY CLUSTERED ([ID] ASC)
);

