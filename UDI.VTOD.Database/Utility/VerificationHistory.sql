﻿CREATE TABLE [Utility].[VerificationHistory] (
    [ID]                BIGINT         IDENTITY (1, 1) NOT NULL,
    [TypeID]            INT            NOT NULL,
    [Type]              VARCHAR (50)   NOT NULL,
    [Item]              NVARCHAR (100) NOT NULL,
    [IsVerified]        BIT            NOT NULL,
    [MemberID]          BIGINT         NULL,
    [MemberUserName]    NVARCHAR (100) NULL,
    [VerificationFlow]  INT            NOT NULL,
    [InsertedTimeUTC]   DATETIME2 (0)  NULL,
    [ExpirationTimeUTC] DATETIME2 (0)  NOT NULL,
    [ErrorMessage]      NVARCHAR (500) NULL,
    [AppendTime]        DATETIME2 (0)  NOT NULL,
    CONSTRAINT [PK_VerificationHistory] PRIMARY KEY CLUSTERED ([ID] ASC)
);

