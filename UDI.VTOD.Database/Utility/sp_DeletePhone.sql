﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Utility].[sp_DeletePhone]
	-- Add the parameters for the stored procedure here
	@phoneNumber varchar(100),
	@memberID BIGINT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	delete from utility.Verification
	where item=@phoneNumber and MemberID=@memberID

	delete from utility.VerificationHistory
	where item=@phoneNumber and MemberID=@memberID
	
END
