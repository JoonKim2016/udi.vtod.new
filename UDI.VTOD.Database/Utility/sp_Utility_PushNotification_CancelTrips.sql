﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Utility].[sp_Utility_PushNotification_CancelTrips]
@VtodTripID bigint,
@workflowStatus int,
@MemberID bigint,
@filterStatus int
AS
BEGIN

BEGIN TRY
declare @pos int
declare @len int
declare @value nvarchar(50)
 BEGIN TRANSACTION;
	SET NOCOUNT ON;
update [Utility].[PushNotification]
		set workflow=@workflowStatus
		where VtodTripID=VtodTripID
		and MemberID=@MemberID and WorkFlow=@filterStatus
    COMMIT TRANSACTION;
END TRY
BEGIN CATCH
     IF @@TRANCOUNT > 0
    ROLLBACK TRANSACTION;
	DECLARE @ErrorNumber INT = ERROR_NUMBER();
    DECLARE @ErrorLine INT = ERROR_LINE();
    DECLARE @ProcErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
    DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
    DECLARE @ErrorState INT = ERROR_STATE();

 RAISERROR(@ProcErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH
END

