﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Utility].[sp_Utility_PushNotification_Updation]
@ID nvarchar(2000),
@workflowStatus int
AS
BEGIN

BEGIN TRY
declare @pos int
declare @len int
declare @value nvarchar(50)
 BEGIN TRANSACTION;
	SET NOCOUNT ON;

   set @pos = 0
		set @len = 0		

		WHILE CHARINDEX(',', @ID, @pos+1)>0
		BEGIN

		 set @len = CHARINDEX(',', @ID, @pos+1) - @pos
		 set @value = SUBSTRING(@ID, @pos, @len)

   		update [Utility].[PushNotification]
		set workflow=@workflowStatus
		where ID in (@value)
		
		set @pos = CHARINDEX(',', @ID, @pos+@len) +1
	END
	
    COMMIT TRANSACTION;
END TRY
BEGIN CATCH
     IF @@TRANCOUNT > 0
    ROLLBACK TRANSACTION;
	DECLARE @ErrorNumber INT = ERROR_NUMBER();
    DECLARE @ErrorLine INT = ERROR_LINE();
    DECLARE @ProcErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
    DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
    DECLARE @ErrorState INT = ERROR_STATE();

 RAISERROR(@ProcErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH
END


