﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Utility].[sp_VerifyPhone]
 @Item varchar(100) null,
 @MemberID bigint null,
 @ModeID int null,
 @ModeDesc varchar(100),
 @TypeID int,
 @Type varchar(100),
 @ExpirationTimeUTC int,
 @VerificationFlow int,
 @MemberUserName varchar(100),
 @VerificationType int,
 @VerificationCode varchar(10)

 
AS
BEGIN
declare @count int
declare @memberCount int
declare @Status int

  ---@Status=1 No insertion needed.The phone is verified for that user already.
  ---@status=6 No insertion needed.The phone is verified for other user already.
  ---@Status=2 Both Inserion is required
  ----@Status=3,4	Updation is required in Verification table and insertion in verification history table.
  ----@Status=5 Only insertion is required in Verification History table
  

	SET NOCOUNT ON;

	
	select @count=ISNULL(count(*),0) from Utility.Verification where Item=@Item and TypeID=@ModeID and IsVerified=1

	if(@count>0)
	begin
	select  @status=1 ----No insertion/updation required throw exsception
	select @memberCount=ISNULL(count(*),0) from Utility.Verification where Item=@Item and TypeID=@ModeID and IsVerified=1 and MemberID=@MemberID
	if(@memberCount>0)
	begin
	select  @status=1
	end
	else
	begin
	select  @status=6
	end
	end
	else
	begin
	select @count=ISNULL(count(*),0) from Utility.Verification where Item=@Item and TypeID=@ModeID
	if(@count=0)
	begin
	select @count=ISNULL(count(*),0) from Utility.Verification where MemberID=@MemberID and TypeID=@ModeID
	if(@count=0)
	begin
	select @Status=2 ---Insert in both the tables
	exec [Utility].[SP_Utility_PhoneSMS_Insertion]  @ModeID, @ModeDesc,@TypeID,@Type,@Item,@MemberID,@MemberUserName,@VerificationFlow,@ExpirationTimeUTC,@VerificationType,@VerificationCode
	end
	else
	begin
	select @status=3
	update Utility.Verification set Isverified=0,item=@Item,VerificationType=@VerificationType,VerificationCode=@VerificationCode where MemberID=@MemberID and  TypeID=@ModeID 

	INSERT INTO [Utility].VerificationHistory
           ([TypeID]
           ,[Type]
           ,[Item]
           ,[IsVerified]
           ,[MemberID]
           ,[MemberUserName]
           ,[VerificationFlow]
           ,[ExpirationTimeUTC]
           ,[AppendTime],[InsertedTimeUTC])
     VALUES
           (@TypeID, 
           @Type,
           @Item,
           0,
           @MemberID,
           @MemberUserName,
           @VerificationFlow,
           DATEADD(MINUTE, @ExpirationTimeUTC,GETUTCDATE()),
           GETDATE(),GETUTCDATE())
	end
	end
	else 
	begin
	select @count=ISNULL(count(*),0) from Utility.Verification where MemberID=@MemberID and TypeID=@ModeID
	if(@count=0)
	BEGIN
	select @status = 4
	update Utility.Verification set [IsVerified]=0,MemberID=@MemberID,AppendTime=GETDATE(),VerificationType=@VerificationType,VerificationCode=@VerificationCode where item=@item and TypeID=@ModeID 
	INSERT INTO [Utility].VerificationHistory
           ([TypeID]
           ,[Type]
           ,[Item]
           ,[IsVerified]
           ,[MemberID]
           ,[MemberUserName]
           ,[VerificationFlow]
           ,[ExpirationTimeUTC]
           ,[AppendTime],[InsertedTimeUTC])
     VALUES
           (@TypeID, 
           @Type,
           @Item,
           0,
           @MemberID,
           @MemberUserName,
           @VerificationFlow,
           DATEADD(MINUTE, @ExpirationTimeUTC,GETUTCDATE()),
           GETDATE(),GETUTCDATE())
	END
	else
	begin
	select @status=5
	update Utility.Verification set [IsVerified]=0,item=@item,AppendTime=GETDATE(),VerificationType=@VerificationType,VerificationCode=@VerificationCode where MemberID=@MemberID and TypeID=@ModeID
	INSERT INTO [Utility].VerificationHistory
           ([TypeID]
           ,[Type]
           ,[Item]
           ,[IsVerified]
           ,[MemberID]
           ,[MemberUserName]
           ,[VerificationFlow]
           ,[ExpirationTimeUTC]
           ,[AppendTime],[InsertedTimeUTC])
     VALUES
           (@TypeID, 
           @Type,
           @Item,
           0,
           @MemberID,
           @MemberUserName,
           @VerificationFlow,
           DATEADD(MINUTE, @ExpirationTimeUTC,GETUTCDATE()),
           GETDATE(),GETUTCDATE())
	end
	end
	end
	
	select @status
	
END
