﻿CREATE TABLE [Utility].[vtod_AppVersionDetails] (
    [Id]                       BIGINT         IDENTITY (1, 1) NOT NULL,
    [App]                      NVARCHAR (100) NULL,
    [AppVersion]               NVARCHAR (100) NULL,
    [Platform]                 NVARCHAR (100) NULL,
    [PlatformVersion]          NVARCHAR (100) NULL,
    [IsActive]                 BIT            NULL,
    [PushTimeUTC]              DATETIME       NULL,
    [AppendTime]               DATETIME       NULL,
    [AppVersionSupported]      BIT            NULL,
    [PlatformVersionSupported] BIT            NULL,
    [Notes]                    NVARCHAR (200) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

