﻿

-- =============================================
-- Author:		Jose Reynoso
-- Create date: 6/12/2016
-- Description:	This stored procedure generates a .csv file containing
--				Texas Taxi Cashiering Feed data in the 
--				C:\Exports\TaxiCashiering\TransportFiles
--				directory.  It then uploads this file into Texas Taxi SFTP directory
--				and creates a backup of said file in 
--				C:\Exports\TexasTaxiCashiering\Archive.
-- =============================================
CREATE PROCEDURE [dbo].[GenerateTaxiCashieringFeedDataFile] 
	@FleetType INT = 31 -- Texas Austin
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	BEGIN TRY

		-- Create variables used for file name i.e. Fleet Name + Version + Datetime
		DECLARE @CurrentDate DATE = GETDATE(), @CurrentTime TIME = GETDATE(), @DateForFile VARCHAR(20), @TimeForFile VARCHAR(20), @TimestampForFile VARCHAR(40),
				@FileName VARCHAR(80), @FleetName VARCHAR(40), @Bcp VARCHAR(8000), @Version VARCHAR(4) = 'V1', @Cmd VARCHAR(8000)

		SELECT @DateForFile = REPLACE(REPLACE(@CurrentDate, '-',''),' ', '')

		SELECT @TimeForFile = LEFT(REPLACE(REPLACE(REPLACE(@CurrentTime, ':',''),'.',''),' ',''),6)

		SELECT @TimestampForFile = @DateForFile + '_' + @TimeForFile

		SELECT @FleetName = (SELECT TOP(1) REPLACE(REPLACE(REPLACE(ISNULL([Name],'TexasTaxi'),' ',''),'/',''),'-','') AS [Name] 
							FROM [dbo].[taxi_fleet] WHERE Id = 31)	

		SELECT @FileName = @FleetName + '_' + @Version + '_' + @TimestampForFile

		-- Empty staging table.
		TRUNCATE TABLE [dbo].[TaxiCashierFeedTemporary]
		TRUNCATE TABLE [dbo].[TaxiCashierFeedExport]

		-- Load temporary table with data that will be exported.
		-- Load all records that we have not previously loaded.  The [dbo].[TaxiCashierFeedHistory] table contains
		-- all records that we have exported.  First we load all non PaymentCard records.
		INSERT INTO [dbo].[TaxiCashierFeedTemporary] ([VtodTripId], [DispatchTripId], [Gratuity], [GratuityRate], [FareAmount], [TotalFareAmount], [PickupDateTime]
			,[PickMeUpNow], [ChargeTry], [ChargeStatus], [PaymentType], [IsFlatRate], [EmailAddress]
			,[FirstName], [LastName], [PhoneNumber], [PickupFullAddress], [PickupAirport], [DropOffFullAddress]
			,[DropOffAirport], [TransactionNumber], [AmountCharged], [CreditsApplied], [ChargingException]
			,[DriverID], [DriverFirstName], [DriverLastName], [FleetId])
		SELECT 
				F.[VtodTripId], F.[DispatchTripId], F.[Gratuity], F.[GratuityRate], F.[FareAmount], F.[TotalFareAmount], F.[PickupDateTime]
				,F.[PickMeUpNow], F.[ChargeTry], F.[ChargeStatus], F.[PaymentType], F.[IsFlatRate], F.[EmailAddress]
				,F.[FirstName], F.[LastName], F.[PhoneNumber], F.[PickupFullAddress], F.[PickupAirport], F.[DropOffFullAddress]
				,F.[DropOffAirport], F.[TransactionNumber], F.[AmountCharged], F.[CreditsApplied], F.[ChargingException]
				,F.[DriverID], F.[DriverFirstName], F.[DriverLastName], F.[FleetId]
		FROM [dbo].[vwTaxiCashierFeed] F
		LEFT JOIN [dbo].[TaxiCashierFeedHistory] FH ON F.VtodTripId = FH.VtodTripId
		WHERE FH.VtodTripId IS NULL
		AND F.[PaymentType] <> 'PaymentCard'

		-- Next we load all PaymentCard records that successfully charged.
		INSERT INTO [dbo].[TaxiCashierFeedTemporary] ([VtodTripId], [DispatchTripId], [Gratuity], [GratuityRate], [FareAmount], [TotalFareAmount], [PickupDateTime]
			,[PickMeUpNow], [ChargeTry], [ChargeStatus], [PaymentType], [IsFlatRate], [EmailAddress]
			,[FirstName], [LastName], [PhoneNumber], [PickupFullAddress], [PickupAirport], [DropOffFullAddress]
			,[DropOffAirport], [TransactionNumber], [AmountCharged], [CreditsApplied], [ChargingException]
			,[DriverID], [DriverFirstName], [DriverLastName], [FleetId])
		SELECT 
				F.[VtodTripId], F.[DispatchTripId], F.[Gratuity], F.[GratuityRate], F.[FareAmount], F.[TotalFareAmount], F.[PickupDateTime]
				,F.[PickMeUpNow], F.[ChargeTry], F.[ChargeStatus], F.[PaymentType], F.[IsFlatRate], F.[EmailAddress]
				,F.[FirstName], F.[LastName], F.[PhoneNumber], F.[PickupFullAddress], F.[PickupAirport], F.[DropOffFullAddress]
				,F.[DropOffAirport], F.[TransactionNumber], F.[AmountCharged], F.[CreditsApplied], F.[ChargingException]
				,F.[DriverID], F.[DriverFirstName], F.[DriverLastName], F.[FleetId]
		FROM [dbo].[vwTaxiCashierFeed] F
		LEFT JOIN [dbo].[TaxiCashierFeedHistory] FH ON F.VtodTripId = FH.VtodTripId
		WHERE FH.VtodTripId IS NULL
		AND F.[PaymentType] = 'PaymentCard'
		AND F.[ChargeStatus] = 'Charged'

		-- Lastly we load all PaymentCard records that failed 3 times.  The system attempts to charge a PaymentCard a total of 3 times and we
		-- only want to capture the third attempt.
		INSERT INTO [dbo].[TaxiCashierFeedTemporary] ([VtodTripId], [DispatchTripId], [Gratuity], [GratuityRate], [FareAmount], [TotalFareAmount], [PickupDateTime]
			,[PickMeUpNow], [ChargeTry], [ChargeStatus], [PaymentType], [IsFlatRate], [EmailAddress]
			,[FirstName], [LastName], [PhoneNumber], [PickupFullAddress], [PickupAirport], [DropOffFullAddress]
			,[DropOffAirport], [TransactionNumber], [AmountCharged], [CreditsApplied], [ChargingException]
			,[DriverID], [DriverFirstName], [DriverLastName], [FleetId])
		SELECT 
				F.[VtodTripId], F.[DispatchTripId], F.[Gratuity], F.[GratuityRate], F.[FareAmount], F.[TotalFareAmount], F.[PickupDateTime]
				,F.[PickMeUpNow], F.[ChargeTry], F.[ChargeStatus], F.[PaymentType], F.[IsFlatRate], F.[EmailAddress]
				,F.[FirstName], F.[LastName], F.[PhoneNumber], F.[PickupFullAddress], F.[PickupAirport], F.[DropOffFullAddress]
				,F.[DropOffAirport], F.[TransactionNumber], F.[AmountCharged], F.[CreditsApplied], F.[ChargingException]
				,F.[DriverID], F.[DriverFirstName], F.[DriverLastName], F.[FleetId]
		FROM [dbo].[vwTaxiCashierFeed] F
		LEFT JOIN [dbo].[TaxiCashierFeedHistory] FH ON F.VtodTripId = FH.VtodTripId
		WHERE FH.VtodTripId IS NULL
		AND F.[PaymentType] = 'PaymentCard'
		AND F.[ChargeStatus] = 'Error'
		AND F.[ChargeTry] = 3
		
		-- Load temporary table with header and body data that will be exported.
		INSERT INTO [dbo].[TaxiCashierFeedExport] ([VtodTripId], [DispatchTripId], [Gratuity], [GratuityRate], [FareAmount], [TotalFareAmount], [PickupDateTime]
			,[PickMeUpNow], [ChargeTry], [ChargeStatus], [PaymentType], [IsFlatRate], [EmailAddress]
			,[FirstName], [LastName], [PhoneNumber], [PickupFullAddress], [PickupAirport], [DropOffFullAddress]
			,[DropOffAirport], [TransactionNumber], [AmountCharged], [CreditsApplied], [ChargingException]
			,[DriverID], [DriverFirstName], [DriverLastName])
		SELECT  
			'VtodTripId', 'DispatchTripId', 'Gratuity', 'GratuityRate', 'FareAmount', 'TotalFareAmount', 'PickupDateTime'
			,'PickMeUpNow', 'ChargeTry', 'ChargeStatus', 'PaymentType', 'IsFlatRate', 'EmailAddress'
			,'FirstName', 'LastName', 'PhoneNumber', 'PickupFullAddress', 'PickupAirport', 'DropOffFullAddress'
			,'DropOffAirport', 'TransactionNumber', 'AmountCharged', 'CreditsApplied', 'ChargingException'
			,'DriverID', 'DriverFirstName', 'DriverLastName'

		-- Load temporary table with header and body data that will be exported.
		INSERT INTO [dbo].[TaxiCashierFeedExport] ([VtodTripId], [DispatchTripId], [Gratuity], [GratuityRate], [FareAmount], [TotalFareAmount], [PickupDateTime]
			,[PickMeUpNow], [ChargeTry], [ChargeStatus], [PaymentType], [IsFlatRate], [EmailAddress]
			,[FirstName], [LastName], [PhoneNumber], [PickupFullAddress], [PickupAirport], [DropOffFullAddress]
			,[DropOffAirport], [TransactionNumber], [AmountCharged], [CreditsApplied], [ChargingException]
			,[DriverID], [DriverFirstName], [DriverLastName])
		SELECT 
			[VtodTripId], [DispatchTripId], [Gratuity], [GratuityRate], [FareAmount], [TotalFareAmount], [PickupDateTime]
			,[PickMeUpNow], [ChargeTry], [ChargeStatus], [PaymentType], [IsFlatRate], [EmailAddress]
			,[FirstName], [LastName], [PhoneNumber], [PickupFullAddress], [PickupAirport], [DropOffFullAddress]
			,[DropOffAirport], [TransactionNumber], [AmountCharged], [CreditsApplied], [ChargingException]
			,[DriverID], [DriverFirstName], [DriverLastName]
		FROM [dbo].[TaxiCashierFeedTemporary]
		
		-- Delete files in TransportFiles directory.
		EXEC MASTER..XP_CMDSHELL 'del /Q /F "C:\Exports\TaxiCashiering\TransportFiles\*.*"'
		
		-- Generate .csv file in C:\Exports\TaxiCashiering\TransportFiles\.
		SELECT @Bcp = 'bcp "SELECT [VtodTripId], [DispatchTripId], [Gratuity], [GratuityRate], [FareAmount], [TotalFareAmount], [PickupDateTime]'
		SELECT @Bcp = @Bcp + ',[PickMeUpNow], [ChargeTry], [ChargeStatus], [PaymentType], [IsFlatRate], [EmailAddress]' 
		SELECT @Bcp = @Bcp + ',[FirstName], [LastName], [PhoneNumber], [PickupFullAddress], [PickupAirport], [DropOffFullAddress]' 
		SELECT @Bcp = @Bcp + ',[DropOffAirport], [TransactionNumber], [AmountCharged], [CreditsApplied], [ChargingException]' 
		SELECT @Bcp = @Bcp + ',[DriverID], [DriverFirstName], [DriverLastName] ' 
		SELECT @Bcp = @Bcp + 'FROM [vtod].[dbo].[TaxiCashierFeedExport];" queryout ' 
		SELECT @Bcp = @Bcp + '"C:\Exports\TaxiCashiering\TransportFiles\' + @FileName + '.csv" -c -T -t","' 		
		EXEC MASTER..XP_CMDSHELL @Bcp 

		-- Upload file into SFTP Server.
		SELECT @Cmd = '"C:\Program Files (x86)\WinSCP\WinSCP.com" /script=C:\Exports\TaxiCashiering\Scripts\Austin.txt'

		-- Create variables and temporary table for upload attempts into SFTP server.
		DECLARE @Rc INT, @NumberOfAttemptsToUploadToSftp INT = 3, @HasErrorOccurredWithSftpUpload INT = 0
		--CREATE TABLE #Output (Id INT IDENTITY(1,1), Output NVARCHAR(255) NULL)	

		-- Attempt to upload file into SFTP server a total of 3 times.
		WHILE @NumberOfAttemptsToUploadToSftp > 0
		BEGIN

			-- Execute WinSCP upload script and store output into temporary table
			INSERT [dbo].[TaxiCashieringFeedFileLog] ([Output]) EXEC @Rc = MASTER..XP_CMDSHELL @Cmd
			SELECT * FROM [dbo].[TaxiCashieringFeedFileLog] WHERE [Output] IS NOT NULL ORDER BY Id

			IF (SELECT COUNT(*) FROM [dbo].[TaxiCashieringFeedFileLog] WHERE [Output] IS NOT NULL AND LOWER([Output]) LIKE '%error%') > 0
			BEGIN
				SELECT @Bcp = 'bcp "SELECT [Output] FROM [vtod].[dbo].[TaxiCashieringFeedFileLog] WHERE [Output] IS NOT NULL ORDER BY Id"'
				SELECT @Bcp = @Bcp + ' queryout "C:\Exports\TaxiCashiering\Log\' + @FileName 
				SELECT @Bcp = @Bcp + '_Log' + CAST(@NumberOfAttemptsToUploadToSftp AS VARCHAR(10)) + '.csv" -T -c'
				EXEC MASTER..XP_CMDSHELL @Bcp
				SELECT @HasErrorOccurredWithSftpUpload = 1
				TRUNCATE TABLE [dbo].[TaxiCashieringFeedFileLog]
			END
			ELSE 
			BEGIN
				SELECT @HasErrorOccurredWithSftpUpload = 0
				BREAK;
			END

			-- Decrement counter
			SELECT @NumberOfAttemptsToUploadToSftp = @NumberOfAttemptsToUploadToSftp -1

		END

		-- Rar file and move to C:\Exports\TaxiCashiering\Archive\ directory.
		SELECT @Cmd = '"C:\Program Files\WinRAR\WinRAR.exe" a -df -ep'
		SELECT @Cmd = @Cmd + ' C:\Exports\TaxiCashiering\Archive\' + @FileName + '.rar C:\Exports\TaxiCashiering\TransportFiles\' + @FileName + '.csv'
		EXEC MASTER..XP_CMDSHELL @Cmd

		IF @HasErrorOccurredWithSftpUpload = 1
		BEGIN
			-- Capture all of the VtodTripId.
			INSERT INTO [dbo].[TaxiCashierFeedHistory] ([VtodTripId], [DateTimestamp])
			SELECT CAST([VtodTripId] AS INT) AS VtoidTripId, GETDATE()
			FROM [dbo].[TaxiCashierFeedTemporary]

			DECLARE @SftpFileUploadError VARCHAR(200) 
			SELECT @SftpFileUploadError = 'Error with SFTP upload file ' + @FileName
			RAISERROR (@SftpFileUploadError, 16, 1);	
		END


	END TRY 
	BEGIN CATCH
	
		PRINT 'Completed with error...' + CHAR(13)
		
		PRINT 
			'		' + CAST(ERROR_NUMBER() as varchar(32)) 
			+ ' ' + CAST(ERROR_SEVERITY() as varchar(32))
			+ ' ' + CAST(ERROR_STATE() as varchar(32))
			+ ' ' + CAST(ERROR_PROCEDURE() as varchar(256))
			+ ' ' + CAST(ERROR_LINE() as varchar(32))
			+ ' ' + CAST(ERROR_MESSAGE() as nvarchar(4000))
			+ CHAR(13)
			
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();			


		DECLARE @Error VARCHAR(MAX)
							
		SET @Error =  CAST(ERROR_NUMBER() as varchar(32)) 
			+ ' ' + CAST(ERROR_SEVERITY() as varchar(32))
			+ ' ' + CAST(ERROR_STATE() as varchar(32))
			+ ' ' + CAST(ERROR_PROCEDURE() as varchar(256))
			+ ' ' + CAST(ERROR_LINE() as varchar(32))
			+ ' ' + CAST(ERROR_MESSAGE() as varchar(4000))
		
		INSERT INTO [dbo].[TaxiCashieringFeedLog] ([Date], [Error], [StoredProcedure])
		SELECT GETDATE(), @Error, '[dbo].[GenerateTexasTaxiCashieringFeedDataFile]'
						
		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);	

	END CATCH
	
END


