﻿
CREATE PROCEDURE [dbo].[GetPreAuthCreditCardInfo]  
   @pFleetId bigint,
   @pUserId int
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      SELECT 
         Id, 
         FleetId, 
         UserId, 
         AccountNumber, 
         PreAuthCreditCardAmount, 
         PreAuthCreditCardOption, 
         AppendTime
      FROM dbo.taxi_fleet_user 
      WHERE FleetId = @pFleetId AND UserId = @pUserId
         ORDER BY Id

   END

