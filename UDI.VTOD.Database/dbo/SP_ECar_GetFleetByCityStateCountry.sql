﻿
CREATE PROCEDURE [dbo].[SP_ECar_GetFleetByCityStateCountry]  
   @pCity nvarchar(100),
   @pState nvarchar(50),
   @pCountry nvarchar(50)
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      SELECT 
         t1.Id, 
         t1.ProviderId, 
         t1.Status, 
         t1.Name, 
         t1.Alias, 
         t1.PhoneNumber, 
         t1.WebSite, 
         t1.ServerUTCOffset, 
         t1.RestrictBookingMins, 
         t1.ExpiredBookingHrs, 
         t1.AppendTime
      FROM 
         dbo.ecar_fleet  AS t1 
            INNER JOIN dbo.ecar_fleet_servicearea_city  AS t2 
            ON (t1.Id = t2.FleetId)
      WHERE 
         t1.Status= 1 AND 
         t2.Status=1 AND 
         t2.City = @pCity AND 
         t2.State = @pState AND 
         t2.Country = @pCountry
         ORDER BY t2.Ordering
END

