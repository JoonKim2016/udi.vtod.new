﻿

CREATE PROCEDURE [dbo].[SP_ECar_GetFleetByCoordinate2]  
   @pLatitude decimal(18, 15),
   @pLongitude decimal(18, 15)
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

	  declare @point geography
	  set @point=geography::STGeomFromText('POINT('+cast(@pLongitude as nvarchar(50))+' '+cast(@pLatitude as nvarchar(50))+')',4326);


      SELECT 
         t1.Id, 
         t1.ProviderId, 
         t1.Status, 
         t1.Name, 
         t1.Alias, 
         t1.PhoneNumber, 
         t1.WebSite, 
         t1.ServerUTCOffset, 
         t1.RestrictBookingOption, 
         t1.RestrictBookingMins, 
         t1.ExpiredBookingHrs, 
         t1.AppendTime
      FROM 
         dbo.ecar_fleet  AS t1 
            INNER JOIN dbo.ecar_fleet_servicearea_polygon  AS t2 
            ON (t1.Id = t2.FleetId) 
            INNER JOIN dbo.vtod_polygon  AS t3 
            ON (t2.PolygonId = t3.id)
      WHERE 
		 t1.Status= 1 AND 
         t2.Status= 1 AND 
		 t3.polygon.STIntersects(@point)=1
         ORDER BY t2.Ordering      
   END

