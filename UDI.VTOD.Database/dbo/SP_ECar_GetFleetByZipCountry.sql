﻿
CREATE PROCEDURE [dbo].[SP_ECar_GetFleetByZipCountry]  
   @pZip nvarchar(10),
   @pCountry nvarchar(50)
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

	  set @pZip=case when @pZip like '%-%' then Substring(@pZip,0,6) else @pZip end;
 

      SELECT 
         t1.Id, 
         t1.ProviderId, 
         t1.Status, 
         t1.Name, 
         t1.Alias, 
         t1.PhoneNumber, 
         t1.WebSite, 
         t1.ServerUTCOffset, 
         t1.RestrictBookingMins, 
         t1.ExpiredBookingHrs, 
         t1.AppendTime
      FROM 
         dbo.ecar_fleet  AS t1 
            INNER JOIN dbo.ecar_fleet_servicearea_zip  AS t2 
            ON (t1.Id = t2.FleetId)
      WHERE 
         t1.Status= 1 AND 
         t2.Status= 1 AND 
         t2.Zip = @pZip AND 
		 t2.Country = @pCountry
         ORDER BY t2.Ordering

   END

