﻿
   
CREATE PROCEDURE [dbo].[SP_ECar_GetTripStatusByTripIDandStatus]  
   @pTripID bigint,
   @pStatus nvarchar(50)
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON


      IF @pStatus IN ( N'Book', N'Booked' )

         SELECT DISTINCT 
            Id, 
            TripID, 
            Status, 
            StatusTime, 
            VehicleNumber, 
            VehicleLongitude, 
            VehicleLatitude, 
            DriverName, 
            DriverId, 
            ETA, 
            Fare, 
            CommentForStatus, 
            OriginalStatus, 
            AppendTime
         FROM dbo.ecar_trip_status
         WHERE TripID = @pTripID AND Status IN ( N'Book', N'Booked' )
      ELSE 

         IF @pStatus IN ( N'Accept', N'Accepted' )


            SELECT DISTINCT 
               Id, 
               TripID, 
               Status, 
               StatusTime, 
               VehicleNumber, 
               VehicleLongitude, 
               VehicleLatitude, 
               DriverName, 
               DriverId, 
               ETA, 
               Fare, 
               CommentForStatus, 
               OriginalStatus, 
               AppendTime
            FROM dbo.ecar_trip_status
            WHERE TripID = @pTripID AND Status IN ( N'Accept', N'Accepted', N'IN TRANSIT', N'INTRANSIT' )
         ELSE 


            IF @pStatus IN ( N'Complete', N'Completed' )


               SELECT DISTINCT 
                  Id, 
                  TripID, 
                  Status, 
                  StatusTime, 
                  VehicleNumber, 
                  VehicleLongitude, 
                  VehicleLatitude, 
                  DriverName, 
                  DriverId, 
                  ETA, 
                  Fare, 
                  CommentForStatus, 
                  OriginalStatus, 
                  AppendTime
               FROM dbo.ecar_trip_status
               WHERE TripID = @pTripID AND Status IN ( N'Complete', N'Completed' )
            ELSE 


               SELECT DISTINCT 
                  Id, 
                  TripID, 
                  Status, 
                  StatusTime, 
                  VehicleNumber, 
                  VehicleLongitude, 
                  VehicleLatitude, 
                  DriverName, 
                  DriverId, 
                  ETA, 
                  Fare, 
                  CommentForStatus, 
                  OriginalStatus, 
                  AppendTime
               FROM dbo.ecar_trip_status
               WHERE TripID = @pTripID AND Status IN ( @pStatus )

   END


