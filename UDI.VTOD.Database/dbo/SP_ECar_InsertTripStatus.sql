﻿

CREATE PROCEDURE [dbo].[SP_ECar_InsertTripStatus]  
   @pTripID bigint,
   @pStatus nvarchar(50),
   @pStatusTime datetime2(7),
   @pVehicleNumber nvarchar(50),
   @pVehicleLatitude decimal(18, 6),
   @pVehicleLongitude decimal(18, 6),
   @pFare decimal(10, 2),
   @pETA int,
   @pDriverName nvarchar(50),
   @pDriverID nvarchar(50),
   @pCommentForStatus nvarchar(50),
   @pOriginalStatus nvarchar(50)
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      DECLARE
         @pCount int

      SET @pCount = 
         (
            SELECT count(*)
            FROM dbo.ecar_trip_status
            WHERE 
               TripID = @pTripID AND 
               Status = @pStatus AND 
               (VehicleNumber = @pVehicleNumber OR (VehicleNumber IS NULL AND @pVehicleNumber IS NULL)) AND 
               (VehicleLatitude = @pVehicleLatitude OR (VehicleLatitude IS NULL AND @pVehicleLatitude IS NULL)) AND 
               (VehicleLongitude = @pVehicleLongitude OR (VehicleLongitude IS NULL AND @pVehicleLongitude IS NULL)) AND 
               (ETA = @pETA OR ETA IS NULL) AND 
               (Fare = @pFare OR (Fare IS NULL AND @pFare IS NULL))
         )

      IF @pCount < 1
         INSERT into dbo.ecar_trip_status(
            TripID, 
            Status, 
            StatusTime, 
            VehicleNumber, 
            VehicleLatitude, 
            VehicleLongitude, 
            Fare, 
            ETA, 
            DriverName, 
            CommentForStatus, 
            OriginalStatus, 
            DriverId)
            VALUES (
               @pTripID, 
               @pStatus, 
               @pStatusTime, 
               @pVehicleNumber, 
               @pVehicleLatitude, 
               @pVehicleLongitude, 
               @pFare, 
               @pETA, 
               @pDriverName, 
               @pCommentForStatus, 
               @pOriginalStatus, 
               @pDriverID)

   END

