﻿

   
CREATE PROCEDURE [dbo].[SP_ECar_IsDuplicateBookingByAddress]  
   @pFirstName nvarchar(100),
   @pLastName nvarchar(100),
   @pPhoneNumber nvarchar(20),
   @pAddress nvarchar(500),
   @pStreet nvarchar(200),
   @pCity nvarchar(50),
   @pCountry nvarchar(10),
   @pPickupDate datetime2(7),
   @pBlockedInterval int,
   @pMemberId nvarchar(50)
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      IF ((@pMemberId IS NOT NULL) AND (@pMemberId <> ''))
         SELECT count(*) AS DuplicateBooking
         FROM 
            dbo.ecar_trip  AS t1 
               INNER JOIN dbo.vtod_trip  AS t2 
               ON (t1.Id = t2.Id)
         WHERE 
            t2.MemberID = @pMemberId AND 
            @pPickupDate BETWEEN dateadd(minute, -@pBlockedInterval, t2.PickupDateTime) AND dateadd(minute, @pBlockedInterval, t2.PickupDateTime) AND 
            (t2.FinalStatus NOT IN ( 
            N'Charged', 
            N'UnCharged', 
            N'UnCompleted', 
            N'Completed', 
            N'Complete', 
            N'Canceled', 
            N'Cancel', 
            N'NoShow', 
            N'Cancel', 
            N'No Show', 
            N'NoShow', 
            N'NoTrip', 
            N'No Trip', 
            N'TripNotFound', 
            N'Trip Not Found', 
            N'Error' ) OR t2.FinalStatus IS NULL)
      ELSE 
         SELECT count(*) AS DuplicateBooking
         FROM 
            dbo.ecar_trip  AS t1 
               INNER JOIN dbo.vtod_trip  AS t2 
               ON (t1.Id = t2.Id)
         WHERE 
            t1.FirstName = @pFirstName AND 
            t1.LastName = @pLastName AND 
            t1.PhoneNumber = @pPhoneNumber AND 
            @pPickupDate BETWEEN dateadd(minute, -@pBlockedInterval, t2.PickupDateTime) AND dateadd(minute, @pBlockedInterval, t2.PickupDateTime) AND 
            (t2.FinalStatus NOT IN ( 
            N'Charged', 
            N'UnCharged', 
            N'UnCompleted', 
            N'Completed', 
            N'Complete', 
            N'Canceled', 
            N'Cancel', 
            N'NoShow', 
            N'Cancel', 
            N'No Show', 
            N'NoShow', 
            N'NoTrip', 
            N'No Trip', 
            N'TripNotFound', 
            N'Trip Not Found', 
            N'Error' ) OR t2.FinalStatus IS NULL)

   END

