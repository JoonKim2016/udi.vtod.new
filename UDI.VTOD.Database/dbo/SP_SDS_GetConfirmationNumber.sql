﻿

   CREATE PROCEDURE [dbo].[SP_SDS_GetConfirmationNumber]  
   @pVTODID bigint
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      SELECT ConfirmationNumber
      FROM dbo.sds_trip
      WHERE Id = @pVTODID

   END


