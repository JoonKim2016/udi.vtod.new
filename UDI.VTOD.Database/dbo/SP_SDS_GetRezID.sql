﻿
   CREATE PROCEDURE [dbo].[SP_SDS_GetRezID]  
   @pVTODID bigint
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      SELECT RezID
      FROM dbo.sds_trip
      WHERE Id = @pVTODID

   END
