﻿


CREATE PROCEDURE [dbo].[SP_SDS_GetUserInfo]  
   @pUserName nvarchar(50)
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON


	    SELECT 
         Id, 
         UserName, 
         RezSource, 
         AllowClosedSplits, 
         ShowSplits, 
         PickupTimeApproach, 
         AppendTime, 
         ModifyTime
      FROM dbo.sds_userinfo
      WHERE UserName = @pUserName


   END
