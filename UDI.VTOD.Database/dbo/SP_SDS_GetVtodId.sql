﻿

CREATE PROCEDURE [dbo].[SP_SDS_GetVtodId]  
   @pRezID bigint
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      SELECT Id
      FROM dbo.sds_trip
      WHERE RezID = @pRezID

   END
