﻿
CREATE PROCEDURE [dbo].[SP_SDS_InsertUserInfo]  
   @pUserName nvarchar(50),
   @pAllowClosedSplits bit,
   @pShowSplits bit
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      DECLARE
         @pCount int

      SET @pCount = 
         (
            SELECT count(*)
            FROM dbo.sds_userinfo
            WHERE UserName = @pUserName
         )

      IF @pCount > 0

         UPDATE dbo.sds_userinfo
            SET 
               AllowClosedSplits = @pAllowClosedSplits, 
               ShowSplits = @pShowSplits, 
               ModifyTime = getdate()
         WHERE UserName = @pUserName
      ELSE 


         INSERT into dbo.sds_userinfo(
            UserName, 
            AllowClosedSplits, 
            ShowSplits, 
            AppendTime, 
            ModifyTime)
            VALUES (
               @pUserName, 
               @pAllowClosedSplits, 
               @pShowSplits, 
               getdate(), 
               getdate())

   END

