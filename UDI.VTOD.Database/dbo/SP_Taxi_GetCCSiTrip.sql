﻿
CREATE PROCEDURE [dbo].[SP_Taxi_GetCCSiTrip]  
   @pTripId nvarchar(50),
   @pCCSiFleetId nvarchar(50)
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      SELECT TOP (1) t1.Id
      FROM 
         dbo.taxi_trip  AS t1 
            INNER JOIN dbo.taxi_fleet  AS t2 
            ON (t1.FleetId = t2.Id) 
            INNER JOIN dbo.taxi_fleet_api_reference  AS t3 
            ON (t2.Id = t3.FleetId) 
            INNER JOIN dbo.taxi_serviceapi  AS t4 
            ON (t3.ServiceAPIId = t4.Id)
      WHERE 
         t1.DispatchTripId = @pTripId AND 
         t2.CCSiFleetId = @pCCSiFleetId AND 
         t4.Name = 'CCSi' AND 
         t3.Name = 'Status'

   END

