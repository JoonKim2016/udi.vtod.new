﻿


CREATE PROCEDURE [dbo].[SP_Taxi_GetCompletedTripList]  
   @pTripID bigint,
   @pFleetID bigint
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      SELECT TOP (100) A.Id, A.PickupDateTime, B.DispatchTripId
      FROM 
         dbo.vtod_trip  AS A 
            INNER JOIN dbo.taxi_trip  AS B 
            ON A.Id = B.Id
      WHERE 
         A.Id > @pTripID AND 
         B.FleetId = @pFleetID AND 
         A.FinalStatus IN ( N'Completed', N'Complete', N'MeterOff', N'Meter Off' )

   END
