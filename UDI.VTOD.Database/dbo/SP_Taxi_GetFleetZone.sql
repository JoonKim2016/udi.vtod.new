﻿


CREATE PROCEDURE [dbo].[SP_Taxi_GetFleetZone]  
   @pFleetID bigint,
   @pLatitude decimal(18, 15),
   @pLongitude decimal(18, 15)
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

       declare @point geography
	  set @point=geography::STGeomFromText('POINT('+cast(@pLongitude as nvarchar(50))+' '+cast(@pLatitude as nvarchar(50))+')',4326);



      SELECT 
         A.Id, 
         A.FleetId, 
         A.Type, 
         A.Name, 
         A.PolygonId, 
         A.ZipId, 
         A.CityId, 
         A.CenterLatitude, 
         A.CenterLongitude, 
         A.AppendTime
      FROM 
         dbo.taxi_fleet_zone  AS A 
            INNER JOIN dbo.vtod_polygon  AS B 
            ON A.PolygonId = B.id
      WHERE A.FleetId = @pFleetID AND  B.polygon.STIntersects(@point)=1   



   END
