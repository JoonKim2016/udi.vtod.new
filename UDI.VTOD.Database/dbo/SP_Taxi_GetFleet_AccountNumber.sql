﻿

CREATE PROCEDURE [dbo].[SP_Taxi_GetFleet_AccountNumber]  
   @pFleetID bigint,
   @pUserName nvarchar(256)
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      SELECT A.AccountNumber AS AccountNumber
      FROM 
         dbo.taxi_fleet_user  AS A 
            INNER JOIN dbo.my_aspnet_users  AS B 
            ON A.UserId = B.id
      WHERE A.FleetId = @pFleetID AND B.name = @pUserName

   END

