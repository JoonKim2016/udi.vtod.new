﻿

CREATE PROCEDURE [dbo].[SP_Taxi_GetFleet_User2]  
   @fleetId bigint,
   @NAME nvarchar(256)
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      SELECT TOP (1) 
         t1.Id, 
         t1.FleetId, 
         t1.UserId, 
         t1.AccountNumber, 
         t1.PreAuthCreditCardAmount, 
         t1.PreAuthCreditCardOption, 
         t1.AppendTime,
		 t1.RemarkForPickUpCash,
		 t1.RemarkForDropOffCash,
		 t1.RemarkForPickUpCreditCard,
		 t1.RemarkForDropOffCreditCard,
		 t1.CashAccountNumber
      FROM 
         dbo.taxi_fleet_user  AS t1 
            INNER JOIN dbo.my_aspnet_users  AS t2 
            ON (t1.UserId = t2.id)
      WHERE t1.FleetId = @fleetId AND t2.name = @NAME
         ORDER BY t2.id

   END
