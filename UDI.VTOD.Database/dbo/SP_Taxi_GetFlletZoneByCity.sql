﻿


CREATE PROCEDURE [dbo].[SP_Taxi_GetFlletZoneByCity]  
   @pFleetID bigint,
   @pCity nvarchar(100)
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      SELECT 
         t1.Id, 
         t1.FleetId, 
         t1.Type, 
         t1.Name, 
         t1.PolygonId, 
         t1.ZipId, 
         t1.CityId, 
         t1.CenterLatitude, 
         t1.CenterLongitude, 
         t1.AppendTime
      FROM 
         dbo.taxi_fleet_zone  AS t1 
            INNER JOIN dbo.taxi_fleet_servicearea_city  AS t2 
            ON (t1.FleetId = t2.FleetId AND t1.CityId = t2.Id)
      WHERE 
         t1.FleetId = @pFleetID AND 
         t1.Type = 'City' AND 
         t2.City = @pCity

   END
