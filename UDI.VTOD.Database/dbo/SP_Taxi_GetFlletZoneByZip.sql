﻿

CREATE PROCEDURE [dbo].[SP_Taxi_GetFlletZoneByZip]  
   @pFleetID bigint,
   @pZip nvarchar(10)
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      SELECT 
         t1.Id, 
         t1.FleetId, 
         t1.Type, 
         t1.Name, 
         t1.PolygonId, 
         t1.ZipId, 
         t1.CityId, 
         t1.CenterLatitude, 
         t1.CenterLongitude, 
         t1.AppendTime
      FROM 
         dbo.taxi_fleet_zone  AS t1 
            INNER JOIN dbo.taxi_fleet_servicearea_zip  AS t2 
            ON (t1.FleetId = t2.FleetId AND t1.ZipId = t2.Id)
      WHERE 
         t1.FleetId = @pFleetID AND 
         t1.Type = 'Zip' AND 
         t2.Zip = @pZip

   END

