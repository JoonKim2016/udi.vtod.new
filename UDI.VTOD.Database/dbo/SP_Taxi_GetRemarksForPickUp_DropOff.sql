﻿


CREATE  PROCEDURE [dbo].[SP_Taxi_GetRemarksForPickUp_DropOff]
@UserName nvarchar(100),
@FleetID bigint



AS 
BEGIN
declare @UserID int
DECLARE @RemarksTable TABLE(RemarkForPickUpCash NVARCHAR(200),RemarkForDropOffCash NVARCHAR(200),RemarkForPickUpCreditCard NVARCHAR(200),RemarkForDropOffCreditCard NVARCHAR(200))

BEGIN TRY
   BEGIN TRANSACTION;
   select @UserID=Id from dbo.my_aspnet_users where UPPER(name)=UPPER(@UserName)
   insert into @RemarksTable
   select ISNULL(RemarkForPickUpCash,'') AS RemarkForPickUpCash,ISNULL(RemarkForDropOffCash,'') AS RemarkForDropOffCash,
   ISNULL(RemarkForPickUpCreditCard,'') AS RemarkForPickUpCreditCard
   ,ISNULL(RemarkForDropOffCreditCard,'') AS RemarkForDropOffCreditCard from dbo.taxi_fleet_user where UserId=@UserID and FleetId=@FleetID
   COMMIT TRANSACTION;

END TRY
BEGIN CATCH
     IF @@TRANCOUNT > 0
    ROLLBACK TRANSACTION;
	DECLARE @ErrorNumber INT = ERROR_NUMBER();
    DECLARE @ErrorLine INT = ERROR_LINE();
    DECLARE @ProcErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
    DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
    DECLARE @ErrorState INT = ERROR_STATE();

 RAISERROR(@ProcErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH
select * from @RemarksTable
END

