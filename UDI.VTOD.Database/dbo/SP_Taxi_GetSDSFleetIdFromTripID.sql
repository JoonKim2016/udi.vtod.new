﻿


CREATE PROCEDURE [dbo].[SP_Taxi_GetSDSFleetIdFromTripID]  
   @pTripID bigint
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      SELECT SDS_FleetMerchantID
      FROM dbo.taxi_fleet
      WHERE Id = 
         (
            SELECT FleetId
            FROM dbo.taxi_trip
            WHERE Id = @pTripID
         )

   END
