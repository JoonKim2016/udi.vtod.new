﻿

CREATE PROCEDURE [dbo].[SP_Taxi_GetTripIDsForUpdatingDriverInfo]
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON


      SELECT Id
      FROM dbo.vtod_trip
      WHERE 
         FleetType = 'Taxi' AND 
         FinalStatus IN ( 
         N'Completed', 
         N'Complete', 
         N'Canceled', 
         N'Cancel', 
         N'NoShow', 
         N'Cancel', 
         N'No Show', 
         N'NoShow', 
         N'NoTrip', 
         N'No Trip', 
         N'TripNotFound', 
         N'Trip Not Found' ) AND 
         (
         DriverID = '' OR 
         DriverID IS NULL OR 
         DriverName = '' OR 
         DriverName IS NULL) AND 
         dateadd(day, 2, PickupDateTimeUTC) > getutcdate()

   END

