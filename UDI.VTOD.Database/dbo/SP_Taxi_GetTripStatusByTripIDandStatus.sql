﻿

   

CREATE PROCEDURE [dbo].[SP_Taxi_GetTripStatusByTripIDandStatus]  
   @pTripID bigint,
   @pStatus nvarchar(50)
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON


      IF @pStatus IN ( N'Book', N'Booked' )

         SELECT DISTINCT 
            Id, 
            TripID, 
            Status, 
            StatusTime, 
            VehicleNumber, 
            VehicleLongitude, 
            VehicleLatitude, 
            DriverName, 
            DriverId, 
            ETA, 
            ETAWithTraffic, 
            Fare, 
            DispatchFare, 
            CommentForStatus, 
            OriginalStatus, 
            AppendTime
         FROM dbo.taxi_trip_status 
         WHERE TripID = @pTripID AND taxi_trip_status.Status IN ( N'Book', N'Booked' )
      ELSE 


         IF @pStatus IN ( N'Accept', N'Accepted' )

            SELECT DISTINCT 
               Id, 
               TripID, 
               Status, 
               StatusTime, 
               VehicleNumber, 
               VehicleLongitude, 
               VehicleLatitude, 
               DriverName, 
               DriverId, 
               ETA, 
               ETAWithTraffic, 
               Fare, 
               DispatchFare, 
               CommentForStatus, 
               OriginalStatus, 
               AppendTime
            FROM dbo.taxi_trip_status 
            WHERE TripID = @pTripID AND Status IN ( N'Accept', N'Accepted', N'IN TRANSIT', N'INTRANSIT' )
         ELSE 


            IF @pStatus IN ( N'Complete', N'Completed', N'MeterOff', N'Meter Off' )

               SELECT DISTINCT 
                  Id, 
                  TripID, 
                  Status, 
                  StatusTime, 
                  VehicleNumber, 
                  VehicleLongitude, 
                  VehicleLatitude, 
                  DriverName, 
                  DriverId, 
                  ETA, 
                  ETAWithTraffic, 
                  Fare, 
                  DispatchFare, 
                  CommentForStatus, 
                  OriginalStatus, 
                  AppendTime
               FROM dbo.taxi_trip_status 
               WHERE TripID = @pTripID AND Status IN ( N'Complete', N'Completed' )
            ELSE 

               SELECT DISTINCT 
                  Id, 
                  TripID, 
                  Status, 
                  StatusTime, 
                  VehicleNumber, 
                  VehicleLongitude, 
                  VehicleLatitude, 
                  DriverName, 
                  DriverId, 
                  ETA, 
                  ETAWithTraffic, 
                  Fare, 
                  DispatchFare, 
                  CommentForStatus, 
                  OriginalStatus, 
                  AppendTime
               FROM dbo.taxi_trip_status 
               WHERE TripID = @pTripID AND Status IN ( @pStatus )

   END

