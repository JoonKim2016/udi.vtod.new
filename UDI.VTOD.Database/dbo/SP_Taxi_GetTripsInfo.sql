﻿
   

CREATE PROCEDURE [dbo].[SP_Taxi_GetTripsInfo]  
   @pStartTime datetime2(7),
   @pEndTime datetime2(7)
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      SELECT 
         A.Id, 
         B.DispatchTripId, 
         A.MemberID, 
         A.CreditCardID, 
         A.FinalStatus, 
         A.Gratuity, 
         A.GratuityRate, 
         A.TotalFareAmount, 
         A.PickupDateTime, 
         A.PickupDateTimeUTC, 
         A.BookingServerDateTime, 
         A.PickMeUpNow, 
         A.ChargeStatus, 
         A.PaymentType, 
         A.TripStartTime, 
         A.TripEndTime, 
         A.ConsumerSource, 
         A.ConsumerDevice, 
         A.PromisedETA, 
         A.DriverAcceptedETA, 
         B.FirstName, 
         B.LastName, 
         B.PhoneNumber, 
         B.CabNumber, 
         B.PickupAddressType, 
         B.PickupLongitude, 
         B.PickupLatitude, 
         B.PickupBuilding, 
         B.PickupStreetNo, 
         B.PickupStreetName, 
         B.PickupStreetType, 
         B.PickupAptNo, 
         B.PickupCity, 
         B.PickupStateCode, 
         B.PickupZipCode, 
         B.PickupCountryCode, 
         B.PickupFullAddress, 
         B.PickupAirport, 
         B.DropOffAddressType, 
         B.DropOffLongitude, 
         B.DropOffLatitude, 
         B.DropOffBuilding, 
         B.DropOffStreetNo, 
         B.DropOffStreetName, 
         B.DropOffStreetType, 
         B.DropOffAptNo, 
         B.DropOffCity, 
         B.DropOffStateCode, 
         B.DropOffZipCode, 
         B.DropOffCountryCode, 
         B.DropOffFullAddress, 
         B.DropOffAirport, 
         C.Id AS FleetID, 
         C.Name AS FleetName, 
         D.DriverID, 
         D.VehicleID, 
         D.FirstName, 
         D.LastName, 
         D.CellPhone, 
         D.zTripQualified
      FROM 
         dbo.vtod_trip  AS A 
            INNER JOIN dbo.taxi_trip  AS B 
            ON A.Id = B.Id 
            INNER JOIN dbo.taxi_fleet  AS C 
            ON B.FleetId = C.Id 
            LEFT JOIN dbo.vtod_driver_info  AS D 
            ON A.DriverInfoID = D.Id
      WHERE 
         A.UserID = 11 AND 
         A.FinalStatus <> 'Error' AND 
         A.PickupDateTime <= @pEndTime AND 
         A.PickupDateTime >= @pStartTime
         ORDER BY A.PickupDateTime DESC, A.Id DESC

   END

