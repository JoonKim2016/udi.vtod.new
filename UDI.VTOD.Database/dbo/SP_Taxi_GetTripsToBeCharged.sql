﻿


CREATE PROCEDURE [dbo].[SP_Taxi_GetTripsToBeCharged]
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      SELECT 
         A.Id, 
         A.UserID, 
         A.FleetType,
		 A.FleetTripCode, 
         A.MemberID, 
         A.CreditCardID, 
         A.DirectBillAccountID, 
         A.FinalStatus, 
         A.Gratuity, 
         A.GratuityRate, 
         A.TotalFareAmount, 
         A.PickupDateTime, 
         A.PickupDateTimeUTC, 
         A.BookingServerDateTime, 
         A.PickMeUpNow, 
         A.ChargeTry, 
         A.ChargeStatus, 
         A.PaymentType, 
         A.TripStartTime, 
         A.TripEndTime, 
         A.Error, 
         A.ConsumerSource, 
         A.ConsumerDevice, 
         A.ConsumerRef, 
         A.DriverName, 
         A.DriverID, 
         A.PromisedETA, 
         A.DriverAcceptedETA, 
         A.DriverInfoID, 
         A.AppendTime,
		 A.IsSplitPayment,
		 A.SplitPaymentStatus,
		 A.SplitPaymentMemberB,
		 A.MemberBCreditCardID,
         T.FirstName, 
         T.LastName, 
         T.PhoneNumber, 
         T.FleetId, 
         T.RezId, 
         T.ServiceAPIId, 
         T.TripType, 
         T.APIInformation, 
         T.CabNumber, 
         T.MinutesAway, 
         T.Expires, 
         T.NumberOfPassenger, 
         T.DriverNotes, 
         T.PickupFlatRateZone, 
         T.DropOffFlatRateZone, 
         T.AccountNumber, 
         T.PickupAddressType, 
         T.PickupLongitude, 
         T.PickupLatitude, 
         T.PickupBuilding, 
         T.PickupStreetNo, 
         T.PickupStreetName, 
         T.PickupStreetType, 
         T.PickupAptNo, 
         T.PickupCity, 
         T.PickupStateCode, 
         T.PickupZipCode, 
         T.PickupCountryCode, 
         T.PickupFullAddress, 
         T.PickupAirport, 
         T.DropOffAddressType, 
         T.DropOffLongitude, 
         T.DropOffLatitude, 
         T.DropOffBuilding, 
         T.DropOffStreetNo, 
         T.DropOffStreetName, 
         T.DropOffStreetType, 
         T.DropOffAptNo, 
         T.DropOffCity, 
         T.DropOffStateCode, 
         T.DropOffZipCode, 
         T.DropOffCountryCode, 
         T.DropOffFullAddress, 
         T.DropOffAirport, 
         T.DispatchTripId, 
         T.RemarkForPickup, 
         T.RemarkForDropOff, 
         T.wheelchairAccessible, 
         T.NumOfTry
      FROM 
         dbo.vtod_trip  AS A 
            INNER JOIN dbo.taxi_trip  AS T 
            ON A.Id = T.Id
      WHERE 
         A.UserID = 11 AND 
         A.ChargeTry < 3 AND 
         dateadd(day, 1, A.PickupDateTimeUTC) >= getutcdate() AND 
         T.ServiceAPIId <> 3 AND 
         A.FinalStatus IN ( N'Completed', N'Complete', N'MeterOff', N'Meter Off' ) AND 
         (A.ChargeStatus <> 'Charged' OR A.ChargeStatus IS NULL) AND 
         A.PaymentType = 'PaymentCard' AND 
         A.TotalFareAmount IS NOT NULL AND 
         A.TotalFareAmount > 0

   END
