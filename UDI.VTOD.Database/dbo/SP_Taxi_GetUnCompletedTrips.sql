﻿


CREATE PROCEDURE [dbo].[SP_Taxi_GetUnCompletedTrips]
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON


      SELECT 
         A.Id, 
         A.UserID, 
         A.FleetType, 
         A.MemberID, 
         A.CreditCardID, 
         A.DirectBillAccountID, 
         A.FinalStatus, 
         A.Gratuity, 
         A.GratuityRate, 
         A.TotalFareAmount, 
         A.PickupDateTime, 
         A.PickupDateTimeUTC, 
         A.BookingServerDateTime, 
         A.PickMeUpNow, 
         A.ChargeTry, 
         A.ChargeStatus, 
         A.PaymentType, 
         A.TripStartTime, 
         A.TripEndTime, 
         A.Error, 
         A.ConsumerSource, 
         A.ConsumerDevice, 
         A.ConsumerRef, 
         A.DriverName, 
         A.DriverID, 
         A.PromisedETA, 
         A.DriverAcceptedETA, 
         A.DriverInfoID, 
         A.AppendTime, 
         B.FirstName, 
         B.LastName, 
         B.PhoneNumber, 
         B.FleetId, 
         B.RezId, 
         B.ServiceAPIId, 
         B.TripType, 
         B.APIInformation, 
         B.CabNumber, 
         B.MinutesAway, 
         B.Expires, 
         B.NumberOfPassenger, 
         B.DriverNotes, 
         B.PickupFlatRateZone, 
         B.DropOffFlatRateZone, 
         B.AccountNumber, 
         B.PickupAddressType, 
         B.PickupLongitude, 
         B.PickupLatitude, 
         B.PickupBuilding, 
         B.PickupStreetNo, 
         B.PickupStreetName, 
         B.PickupStreetType, 
         B.PickupAptNo, 
         B.PickupCity, 
         B.PickupStateCode, 
         B.PickupZipCode, 
         B.PickupCountryCode, 
         B.PickupFullAddress, 
         B.PickupAirport, 
         B.DropOffAddressType, 
         B.DropOffLongitude, 
         B.DropOffLatitude, 
         B.DropOffBuilding, 
         B.DropOffStreetNo, 
         B.DropOffStreetName, 
         B.DropOffStreetType, 
         B.DropOffAptNo, 
         B.DropOffCity, 
         B.DropOffStateCode, 
         B.DropOffZipCode, 
         B.DropOffCountryCode, 
         B.DropOffFullAddress, 
         B.DropOffAirport, 
         B.DispatchTripId, 
         B.RemarkForPickup, 
         B.RemarkForDropOff, 
         B.wheelchairAccessible, 
         B.NumOfTry
      FROM 
         dbo.vtod_trip  AS A 
            INNER JOIN dbo.taxi_trip  AS B 
            ON A.Id = B.Id
      WHERE 
         A.FleetType = 'Taxi' AND 
         B.ServiceAPIId IN  (1, 4) AND 
         (A.FinalStatus NOT IN ( 
         N'FastMeter', 
         N'MeterOff', 
         N'Meter Off', 
         N'Completed', 
         N'Complete', 
         N'Canceled', 
         N'Cancel', 
         N'NoShow', 
         N'Cancel', 
         N'No Show', 
         N'NoShow', 
         N'NoTrip', 
         N'No Trip', 
         N'TripNotFound', 
         N'Trip Not Found', 
         N'Error' ) OR A.FinalStatus IS NULL OR (A.FinalStatus IN ( N'MeterOff', N'Meter Off' ) AND (A.TotalFareAmount = 0 OR A.TotalFareAmount IS NULL))) AND 
         A.PickupDateTimeUTC > dateadd(hour, -12, CAST(getutcdate() AS datetime2))

   END