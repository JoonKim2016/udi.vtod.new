﻿CREATE PROCEDURE [dbo].[SP_VTOD_GetLatestTripIDForMember]  
   @pMemberID nvarchar(50)
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      SELECT TOP (5) 
         A.Id 
      FROM 
         dbo.vtod_trip  AS A 
      WHERE 
         A.MemberID = @pMemberID AND 
			 A.FinalStatus IN ( N'Accept', N'Arrived',N'Assign', N'Assigned' ,N'InService', N'InTransit' ,N'PickUp', N'Accepted'  )
		
		AND 
         dateadd(hour, 5, CAST(getutcdate() AS datetime2)) > A.PickupDateTimeUTC
		AND 
        dateadd(hour, -5, CAST(getutcdate() AS datetime2)) < A.PickupDateTimeUTC
         ORDER BY A.PickupDateTimeUTC DESC, A.Id DESC

   END


