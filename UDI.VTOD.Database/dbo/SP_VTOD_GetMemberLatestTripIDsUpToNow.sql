﻿CREATE PROCEDURE [dbo].[SP_VTOD_GetMemberLatestTripIDsUpToNow]  
   @pMemberID nvarchar(50)
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      SELECT TOP (5) 
         A.Id 
      FROM 
         dbo.vtod_trip  AS A 
            LEFT JOIN dbo.taxi_trip  AS T 
            ON A.Id = T.Id
      WHERE 
         A.MemberID = @pMemberID AND A.FleetTripCode<> 2 AND  
		 (A.FinalStatus is null or A.FinalStatus <> 'error')
		 and
         (
			 (A.FleetType = 'Taxi' AND T.ServiceAPIId <> 3 AND A.FinalStatus IN ( N'Completed', N'Complete' )) OR 
			(A.FleetType = 'Taxi' AND T.ServiceAPIId = 3) OR 
			(A.FleetType <> 'Taxi')
		)
		AND 
         dateadd(day, 1, CAST(getdate() AS datetime2)) > A.PickupDateTime
		AND 
        dateadd(day, -50, CAST(getdate() AS datetime2)) < A.PickupDateTime
         ORDER BY A.PickupDateTime DESC, A.Id DESC

   END
