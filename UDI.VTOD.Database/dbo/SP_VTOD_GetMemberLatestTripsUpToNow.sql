﻿

CREATE PROCEDURE [dbo].[SP_VTOD_GetMemberLatestTripsUpToNow]  
   @pMemberID nvarchar(50)
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      SELECT TOP (5) 
         A.Id, 
         A.UserID, 
         A.FleetType, 
         A.MemberID, 
         A.CreditCardID, 
         A.DirectBillAccountID, 
         A.FinalStatus, 
         A.Gratuity, 
         A.GratuityRate, 
         A.TotalFareAmount, 
         A.PickupDateTime, 
         A.PickupDateTimeUTC, 
         A.BookingServerDateTime, 
         A.PickMeUpNow, 
         A.ChargeTry, 
         A.ChargeStatus, 
         A.PaymentType, 
         A.TripStartTime, 
         A.TripEndTime, 
         A.Error, 
         A.ConsumerSource, 
         A.ConsumerDevice, 
         A.ConsumerRef, 
         A.DriverName, 
         A.DriverID, 
         A.PromisedETA, 
         A.DriverAcceptedETA, 
         A.DriverInfoID, 
         A.AppendTime
      FROM 
         dbo.vtod_trip  AS A 
            LEFT JOIN dbo.taxi_trip  AS T 
            ON A.Id = T.Id
      WHERE 
         A.MemberID = @pMemberID AND 
         ((
         A.FleetType = 'Taxi' AND 
         T.ServiceAPIId <> 3 AND 
         A.FinalStatus IN ( N'Completed', N'Complete' )) OR (A.FleetType = 'Taxi' AND T.ServiceAPIId = 3) OR (A.FleetType <> 'Taxi')) AND 
         dateadd(day, 1, CAST(getdate() AS datetime2)) > A.PickupDateTime AND 
         dateadd(day, -50, CAST(getdate() AS datetime2)) < A.PickupDateTime
         ORDER BY A.PickupDateTime DESC, A.Id DESC

   END
