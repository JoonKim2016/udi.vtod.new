﻿

CREATE PROCEDURE [dbo].[SP_Van_GetTripIDsForBookedNotification]
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      SELECT DISTINCT A.Id AS VtodTripID
      FROM 
         dbo.vtod_trip  AS A 
            INNER JOIN dbo.van_trip  AS T 
            ON A.Id = T.Id 
            INNER JOIN dbo.vtod_trip_notification  AS B 
            ON A.Id = B.Id 
            INNER JOIN dbo.van_trip_status  AS C 
            ON A.Id = C.TripID
      WHERE 
         T.ServiceAPIId <> 3 AND 
         C.Status IN ( N'Book', N'Booked' ) AND 
         A.UserID = 11 AND 
         (B.Booked IS NULL OR B.Booked = 0 OR (A.Id NOT IN 
         (
            SELECT Id
            FROM dbo.vtod_trip_notification
         ))) AND 
         dateadd(hour, 4, A.PickupDateTimeUTC) > getutcdate()

   END

