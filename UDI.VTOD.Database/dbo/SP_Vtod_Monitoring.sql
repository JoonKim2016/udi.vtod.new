﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Vtod_Monitoring]
AS

BEGIN
BEGIN TRY
DECLARE @currentTime TIME
DECLARE @MyCursor CURSOR;
declare @Query nvarchar(100)
declare @EmailList nvarchar(100)
declare @MailType nvarchar(100)
declare @MailBody nvarchar(100)
declare @MailSubject nvarchar(100)
declare @Operator nvarchar(100)
declare @Number nvarchar(100)
declare @count int
declare @Id int
declare @Operation nvarchar(100)
declare @results bit
BEGIN TRANSACTION;
 BEGIN
 set @currentTime=convert(Time,GETUTCDATE())
    SET @MyCursor = CURSOR FOR
    select Id,Query,EmailList,MailType,MailBody,MailSubject,Operator,OperatingNumbers from  vtod_Monitoring where [IsEnable]=1
 and [JobType]=1 and  @currentTime between [StartUTCTime] and [EndUTCTime]

    OPEN @MyCursor 
    FETCH NEXT FROM @MyCursor 
    INTO @Id,@Query,@EmailList,@MailType,@MailBody,@MailSubject,@Operator,@Number

    WHILE @@FETCH_STATUS = 0
    BEGIN
				if(ISNULL(@Operator,'')='')
				BEGIN
				set @MailBody=REPLACE(@MailBody , 'Type', @MailType )
				set @MailSubject=REPLACE(@MailSubject , 'Type', @MailType )
				EXEC msdb.dbo.sp_send_dbmail
				@profile_name = 'VTOD_API_Archiving',
				@recipients = @EmailList,
				@subject =  @MailSubject,
				@query =@Query, 
				@body=@MailBody,
				@attach_query_result_as_file=1,
				@query_attachment_filename = 'Results.csv',
				@query_result_separator = ';',	
				@body_format ='HTML';
				END
				ELSE
				BEGIN
				declare @numRows int
				exec sp_executesql @Query, N'@numRows int output', @count output
				---EXEC sp_executesql @Query, N'@count int output', @count OUTPUT
				
				if(@Operator='>')
				begin
					IF(@count>@Number)
					BEGIN
						set @MailBody=REPLACE(@MailBody , 'Type', @MailType )
						set @MailSubject=REPLACE(@MailSubject , 'Type', @MailType )
						set @MailBody=@MailBody + convert(nvarchar(200),@count);
						EXEC msdb.dbo.sp_send_dbmail
						@profile_name = 'VTOD_API_Archiving',
						@recipients = @EmailList,
						@subject =  @MailSubject,
						@body=@MailBody,
						@body_format ='HTML';
					END
				end
				if(@Operator='<')
				begin
					IF(@count<@Number)
					BEGIN
						set @MailBody=REPLACE(@MailBody , 'Type', @MailType )
						set @MailSubject=REPLACE(@MailSubject , 'Type', @MailType )
						set @MailBody=@MailBody + convert(nvarchar(200),@count);
						EXEC msdb.dbo.sp_send_dbmail
						@profile_name = 'VTOD_API_Archiving',
						@recipients = @EmailList,
						@subject =  @MailSubject,
						@body=@MailBody,
						@body_format ='HTML';
					END
				end
				if(@Operator='=')
				begin
					IF(@count=@Number)
					BEGIN
						set @MailBody=REPLACE(@MailBody , 'Type', @MailType )
						set @MailSubject=REPLACE(@MailSubject , 'Type', @MailType )
						set @MailBody=@MailBody + convert(nvarchar(200),@count);
						EXEC msdb.dbo.sp_send_dbmail
						@profile_name = 'VTOD_API_Archiving',
						@recipients = @EmailList,
						@subject =  @MailSubject,
						@body=@MailBody,
						@body_format ='HTML';
					END
				end
				if(@Operator='>=')
				begin
					IF(@count>=@Number)
					BEGIN
						set @MailBody=REPLACE(@MailBody , 'Type', @MailType )
						set @MailSubject=REPLACE(@MailSubject , 'Type', @MailType )
						set @MailBody=@MailBody + convert(nvarchar(200),@count);
						EXEC msdb.dbo.sp_send_dbmail
						@profile_name = 'VTOD_API_Archiving',
						@recipients = @EmailList,
						@subject =  @MailSubject,
						@body=@MailBody,
						@body_format ='HTML';
					END
				end
				if(@Operator='<=')
				begin
					IF(@count=@Number)
					BEGIN
						set @MailBody=REPLACE(@MailBody , 'Type', @MailType )
						set @MailSubject=REPLACE(@MailSubject , 'Type', @MailType )
						set @MailBody=@MailBody + convert(nvarchar(200),@count);
						EXEC msdb.dbo.sp_send_dbmail
						@profile_name = 'VTOD_API_Archiving',
						@recipients = @EmailList,
						@subject =  @MailSubject,
						@body=@MailBody,
						@body_format ='HTML';
					END
				end
				if(@Operator='<>')
				begin
					IF(@count<>@Number)
					BEGIN
						set @MailBody=REPLACE(@MailBody , 'Type', @MailType )
						set @MailSubject=REPLACE(@MailSubject , 'Type', @MailType )
						set @MailBody=@MailBody + convert(nvarchar(200),@count);
						EXEC msdb.dbo.sp_send_dbmail
						@profile_name = 'VTOD_API_Archiving',
						@recipients = @EmailList,
						@subject =  @MailSubject,
						@body=@MailBody,
						@body_format ='HTML';
					END
				end
				END;
      FETCH NEXT FROM @MyCursor 
      INTO @Id,@Query,@EmailList,@MailType,@MailBody,@MailSubject,@Operator,@Number 
    END; 

    CLOSE @MyCursor ;
    DEALLOCATE @MyCursor;
END;
COMMIT TRANSACTION;
END TRY
BEGIN CATCH
     IF @@TRANCOUNT > 0
    ROLLBACK TRANSACTION;
	DECLARE @ErrorNumber INT = ERROR_NUMBER();
    DECLARE @ErrorLine INT = ERROR_LINE();
    DECLARE @ProcErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
    DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
    DECLARE @ErrorState INT = ERROR_STATE();

 RAISERROR(@ProcErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH
end

