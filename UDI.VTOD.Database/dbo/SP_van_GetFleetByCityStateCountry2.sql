﻿


CREATE PROCEDURE [dbo].[SP_van_GetFleetByCityStateCountry2]  
   @pCity nvarchar(100),
   @pState nvarchar(50),
   @pCountry nvarchar(50)
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      SELECT 
         t1.Id, 
         t1.ProviderId, 
         t1.SDS_FleetMerchantID, 
         t1.Status, 
         t1.Name, 
         t1.Alias, 
         t1.PhoneNumber, 
         t1.WebSite, 
	    t1.IVREnable, 
         t1.CCSiFleetId, 
         t1.CCSiSource, 
         t1.UDI33DNI, 
         t1.UDI33IPAddress, 
         t1.UDI33PortNo, 
         t1.UDI33ReceivedTimeout, 
         t1.UDI33ForceZone, 
         t1.ServerUTCOffset, 
         t1.RestrictBookingOption, 
         t1.RestrictBookingMins, 
         t1.DetectFastMeter, 
         t1.MinPriceFastMeter, 
         t1.VehicleAttr, 
         t1.VehicleBits, 
         t1.DriverAttr, 
         t1.DriverBits, 
         t1.HandleStreetRange, 
         t1.OverWriteFixedPrice, 
         t1.PickMeUpNowOption, 
         t1.AppendTime,
		 t1.MaxEstimatedRate,
		 t1.MinEstimatedRate,
		 t1.IsEstimated,
		 t1.DispatchCityCode,
		 t1.DriverInfo
      FROM 
         dbo.van_fleet  AS t1 
            INNER JOIN dbo.van_fleet_servicearea_city  AS t2 
            ON (t1.Id = t2.FleetId)
      WHERE 
         t1.Status =1 AND 
         t2.Status =1 AND 
         t2.City = @pCity AND 
         t2.State = @pState AND 
         t2.Country = @pCountry
         ORDER BY t2.Ordering

   END

