﻿



CREATE PROCEDURE [dbo].[SP_van_GetFleetZoneByAirport]  
   @pFleetID bigint,
   @pAirortName nvarchar(100)
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      SELECT 
         Id, 
         FleetId, 
         Type, 
         Name, 
         PolygonId, 
         ZipId, 
         CityId, 
         CenterLatitude, 
         CenterLongitude, 
         AppendTime
      FROM dbo.van_fleet_zone
      WHERE 
         FleetId = @pFleetID AND 
         Type = 'Airport' AND 
         Name = @pAirortName

   END

