﻿


create PROCEDURE [dbo].[SP_van_GetSDSFleetIdFromTripID]  
   @pTripID bigint
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      SELECT SDS_FleetMerchantID
      FROM dbo.van_fleet
      WHERE Id = 
         (
            SELECT FleetId
            FROM dbo.van_trip
            WHERE Id = @pTripID
         )

   END
