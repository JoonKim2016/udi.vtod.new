﻿

   

CREATE PROCEDURE [dbo].[SP_van_InsertTripStatus]  
   @pTripID bigint,
   @pStatus nvarchar(50),
   @pStatusTime datetime2(7),
   @pVehicleNumber nvarchar(50),
   @pVehicleLatitude decimal(18, 6),
   @pVehicleLongitude decimal(18, 6),
   @pFare decimal(10, 2),
   @pDispatchFare decimal(10, 2),
   @pETA int,
   @pETAWithTraffic int,
   @pDriverName nvarchar(50),
   @pDriverID nvarchar(50),
   @pCommentForStatus nvarchar(50),
   @pOriginalStatus nvarchar(50)
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      DECLARE
         @pCount int

      SET @pCount = 
         (
            SELECT count(*)
            FROM dbo.van_trip_status
            WHERE 
               TripID = @pTripID AND 
               Status = @pStatus AND 
               (VehicleNumber = @pVehicleNumber OR (VehicleNumber IS NULL AND @pVehicleNumber IS NULL)) AND 
               (VehicleLatitude = @pVehicleLatitude OR (VehicleLatitude IS NULL AND @pVehicleLatitude IS NULL)) AND 
               (VehicleLongitude = @pVehicleLongitude OR (VehicleLongitude IS NULL AND @pVehicleLongitude IS NULL)) AND 
               (ETA = @pETA OR (ETA IS NULL AND @pETA IS NULL)) AND 
               (ETAWithTraffic = @pETAWithTraffic OR (ETAWithTraffic IS NULL AND @pETAWithTraffic IS NULL)) AND 
               (Fare = @pFare OR (Fare IS NULL AND @pFare IS NULL)) AND 
               (DispatchFare = @pDispatchFare OR (DispatchFare IS NULL AND @pDispatchFare IS NULL))
         )

      IF @pCount < 1
         INSERT into dbo.van_trip_status(
            TripID, 
            Status, 
            StatusTime, 
            VehicleNumber, 
            VehicleLatitude, 
            VehicleLongitude, 
            Fare, 
            DispatchFare, 
            ETA, 
            ETAWithTraffic, 
            DriverName, 
            CommentForStatus, 
            OriginalStatus, 
            DriverId)
            VALUES (
               @pTripID, 
               @pStatus, 
               @pStatusTime, 
               @pVehicleNumber, 
               @pVehicleLatitude, 
               @pVehicleLongitude, 
               @pFare, 
               @pDispatchFare, 
               @pETA, 
               @pETAWithTraffic, 
               @pDriverName, 
               @pCommentForStatus, 
               @pOriginalStatus, 
               @pDriverID)

   END



