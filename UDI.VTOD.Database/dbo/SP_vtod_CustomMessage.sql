﻿
CREATE PROCEDURE [dbo].[SP_vtod_CustomMessage]  
   @pUserName nvarchar(256),
   @pCode bigint,
   @pLanguageCode nvarchar(6)
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      SELECT A.message
      FROM 
         dbo.vtod_message  AS A 
            INNER JOIN dbo.vtod_message_code  AS B 
            ON A.messageCodeID = B.id 
            INNER JOIN dbo.vtod_message_language  AS C 
            ON A.messagelanguageID = C.id 
            INNER JOIN dbo.my_aspnet_users  AS D 
            ON A.UserID = D.id
      WHERE 
         CAST(B.code AS float(53)) = @pCode AND 
         C.Code = @pLanguageCode AND 
         D.name = @pUserName

   END
