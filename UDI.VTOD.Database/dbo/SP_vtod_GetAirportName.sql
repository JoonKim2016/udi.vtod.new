﻿
CREATE PROCEDURE [dbo].[SP_vtod_GetAirportName]  
   @pLatitude decimal(18, 15),
   @pLongitude decimal(18, 15)
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

	  SET  NOCOUNT  ON
	  declare @point geography
	  set @point=geography::STGeomFromText('POINT('+cast(@pLongitude as nvarchar(50))+' '+cast(@pLatitude as nvarchar(50))+')',4326);

      SELECT Name AS AirportName
      FROM dbo.vtod_polygon
      WHERE Type = 'Airport' AND 
	  polygon.STIntersects(@point)=1



   END

