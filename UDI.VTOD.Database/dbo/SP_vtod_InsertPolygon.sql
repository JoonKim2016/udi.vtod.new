﻿

CREATE PROCEDURE [dbo].[SP_vtod_InsertPolygon]  
   @pPolygonName nvarchar(50),
   @pPolygonDetails nvarchar(500),
   @pPolygonType nvarchar(50),
   @pPolygonLatitude decimal(18, 15),
   @pPolygonLongitude decimal(18, 15),
   @pPolygon nvarchar(max)
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

	  
      INSERT into dbo.vtod_polygon(
         Name, 
         Details, 
         Type, 
         CenterLatitude, 
         CenterLongitude, 
         polygon, 
         AppendTime)
         VALUES (
            @pPolygonName, 
            @pPolygonDetails, 
            @pPolygonType, 
            @pPolygonLatitude, 
            @pPolygonLongitude, 
            geography::STGeomFromText(@pPolygon,4326), 
            getdate());

   END

