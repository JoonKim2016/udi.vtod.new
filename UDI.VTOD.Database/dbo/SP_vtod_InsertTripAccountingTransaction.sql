﻿


CREATE PROCEDURE [dbo].[SP_vtod_InsertTripAccountingTransaction]  
   @pTripID bigint,
   @pTransactionNumber nvarchar(50),
   @pAmountCharged decimal(10, 2),
   @pCreditsApplied decimal(10, 2),
   @pError nvarchar(500),
   @pAppendTime datetime2(7)
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON


      INSERT into dbo.vtod_trip_account_transaction(
         TripID, 
         TransactionNumber, 
         AmountCharged, 
         CreditsApplied, 
         Error, 
         AppendTime)
         VALUES (
            @pTripID, 
            @pTransactionNumber, 
            @pAmountCharged, 
            @pCreditsApplied, 
            @pError, 
            isnull(@pAppendTime, getdate()))

   END
