﻿


create PROCEDURE [dbo].[SP_vtod_Insert_check_trip_status]  
   @pTripId bigint
AS 
   BEGIN
   BEGIN TRY
   BEGIN TRANSACTION;
      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON


      INSERT into dbo.vtod_trip_process(
         Id)
         VALUES (
            @pTripId)

    COMMIT TRANSACTION;

END TRY
BEGIN CATCH
     IF @@TRANCOUNT > 0
    ROLLBACK TRANSACTION;
	DECLARE @ErrorNumber INT = ERROR_NUMBER();
    DECLARE @ErrorLine INT = ERROR_LINE();
    DECLARE @ProcErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
    DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
    DECLARE @ErrorState INT = ERROR_STATE();

 RAISERROR(@ProcErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH
end