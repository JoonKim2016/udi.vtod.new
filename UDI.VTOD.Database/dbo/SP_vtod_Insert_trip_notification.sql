﻿


CREATE PROCEDURE [dbo].[SP_vtod_Insert_trip_notification]  
   @pTripId bigint
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON


      INSERT into dbo.vtod_trip_notification(
         Id, 
         BookedError, 
         AssignedError, 
         CompletedError, 
         CanceledError)
         VALUES (
            @pTripId, 
            '', 
            '', 
            '', 
            '')

   END
