﻿
CREATE PROCEDURE [dbo].[SP_vtod_Update_FinalStatus]  
   @pTripID bigint,
   @pStatus nvarchar(50),
   @pFareAmount decimal(10, 2),
   @pGratuity decimal(10, 2),
   @pTripStartTime datetime2(7),
   @pTripEndTime datetime2(7),
   @pDriverAcceptedETA int
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON
	 
	  begin
      UPDATE dbo.vtod_trip
         SET 
            FinalStatus = @pStatus, 
            Gratuity = @pGratuity, 
            TotalFareAmount = @pFareAmount, 
            TripStartTime = @pTripStartTime, 
            TripEndTime = @pTripEndTime, 
            DriverAcceptedETA = @pDriverAcceptedETA
      WHERE Id = @pTripID

	   if @pStatus IN ( N'Completed')
	   begin
	    Merge [vtod].[dbo].[vtod_member_info] as T
		Using( 
				Select memberId from [vtod].[dbo].[vtod_trip]
				Where id = @pTripID
		     ) AS S

        On(T.SDSMemberId = S.memberId)
        When matched then
         UPDATE set T.HasBeenBooked = 1 	
        When not matched by target then
	     insert (SDSMemberId,FirstName,LastName,ReferralCode,ReferralLink,HasBeenBooked) VALUES(S.memberId,'','','','',1);    
	   end

	  end
   END
