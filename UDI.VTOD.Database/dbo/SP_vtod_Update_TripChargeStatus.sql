﻿

CREATE PROCEDURE [dbo].[SP_vtod_Update_TripChargeStatus]  
   @pTripID bigint,
   @pChargeStatus nvarchar(50),
   @pChargeTry int,
   @pError nvarchar(500)
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      UPDATE dbo.vtod_trip
         SET 
            ChargeStatus = @pChargeStatus, 
            ChargeTry = @pChargeTry, 
            Error = @pError
      WHERE Id = @pTripID
	  	 
	  IF @pChargeStatus IN ( N'Charged')
		begin
			--Update Referral Trip

			-- Get Member ID
			DECLARE @MemberID int = 0
			Select @MemberID = SDSMemberID
			from Utility.ReferralTrip
			where VtodTripID =  @pTripID

			-- Get credited trip
			DECLARE @tripIdInProgressOrError int = 0
			Select @tripIdInProgressOrError = VtodTripID
			from Utility.ReferralTrip
			where SDSMemberID =  @MemberID and ReferralCreditFlow in (-1,2,10)

			IF (@MemberID>0 and @tripIdInProgressOrError=0)
				begin
				-- Update TripStatus as 'Charged' and ReferralCreditFlow as 'Inprogress'
				Update Utility.ReferralTrip 
				set TripStatus = @pChargeStatus , ReferralCreditFlow = 2 
				where VtodTripID =  @pTripID and ReferralCreditFlow = 1

				--Update the other trips  as 'Trip_Duplicate'
				update Utility.ReferralTrip 
				set ReferralCreditFlow = 12 
				where VtodTripID <> @pTripID and SDSMemberID = @MemberID 
				end
	    end

   END
