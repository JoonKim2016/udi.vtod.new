﻿
CREATE PROCEDURE [dbo].[SP_vtod_Update_TripDriverInfoID]  
   @pTripID bigint,
   @pDriverInfoID bigint
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON
	  declare @driverId int
	  select @driverId= [DriverID] from [dbo].[vtod_driver_info]
	  where [Id]=@pDriverInfoID
	  
	  if(@driverId>0)
	  begin
      UPDATE dbo.vtod_trip
         SET 
            DriverInfoID = @pDriverInfoID
      WHERE Id = @pTripID
	  end

   END
