﻿

CREATE PROCEDURE [dbo].[SP_vtod_Update_trip_notification]  
   @pTripID bigint,
   @pName nvarchar(50),
   @pStatus int,
   @pError nvarchar(max)
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      IF @pName IN ( N'Book', N'Booked' )
         UPDATE dbo.vtod_trip_notification
            SET 
               Booked = @pStatus, 
               BookedTime = getdate(), 
               BookedError = @pError
         WHERE Id = @pTripID
      ELSE 


         IF @pName IN ( N'Assign', N'Assigned' )
            UPDATE dbo.vtod_trip_notification
               SET 
                  Assigned = @pStatus, 
                  AssignedTime = getdate(), 
                  AssignedError = @pError
            WHERE Id = @pTripID
         ELSE 


            IF @pName IN ( N'Complete', N'Completed' )
               UPDATE dbo.vtod_trip_notification
                  SET 
                     Completed = @pStatus, 
                     CompletedTime = getdate(), 
                     CompletedError = @pError
               WHERE Id = @pTripID
            ELSE 
               BEGIN

                  IF @pName IN ( N'Cancel', N'Cancelled', N'Canceled' )
                     UPDATE dbo.vtod_trip_notification
                        SET 
                           Canceled = @pStatus, 
                           CanceledTime = getdate(), 
                           CanceledError = @pError
                     WHERE Id = @pTripID
               END

   END
