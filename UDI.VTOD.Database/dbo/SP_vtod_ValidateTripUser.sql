﻿
CREATE PROCEDURE [dbo].[SP_vtod_ValidateTripUser]  
   @pTripID bigint,
   @pUserName nvarchar(50)
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON


      SELECT 
         CASE 
            WHEN count(*) > 0 THEN cast(1 as bit)
            ELSE cast(0 as bit)
         END AS Valid
      FROM 
         dbo.vtod_trip  AS A 
            INNER JOIN dbo.my_aspnet_users  AS B 
            ON A.UserID = B.id
      WHERE A.Id = @pTripID AND B.name = @pUserName

   END
