﻿CREATE TABLE [dbo].[TaxiCashierFeedHistory] (
    [Id]            INT      IDENTITY (1, 1) NOT NULL,
    [VtodTripId]    INT      NOT NULL,
    [DateTimestamp] DATETIME NULL,
    CONSTRAINT [PK_TaxiCashierFeedHistory] PRIMARY KEY CLUSTERED ([Id] ASC)
);

