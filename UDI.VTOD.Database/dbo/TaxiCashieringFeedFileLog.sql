﻿CREATE TABLE [dbo].[TaxiCashieringFeedFileLog] (
    [Id]     INT           IDENTITY (1, 1) NOT NULL,
    [Output] VARCHAR (MAX) NULL,
    CONSTRAINT [PK_TaxiCashieringFeedFileLog] PRIMARY KEY CLUSTERED ([Id] ASC)
);

