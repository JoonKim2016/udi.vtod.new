﻿CREATE TABLE [dbo].[TaxiCashieringFeedLog] (
    [Id]              INT           IDENTITY (1, 1) NOT NULL,
    [Date]            DATETIME      NULL,
    [Error]           VARCHAR (MAX) NULL,
    [Location]        VARCHAR (50)  NULL,
    [StoredProcedure] VARCHAR (50)  NULL,
    [DynamicSql]      VARCHAR (MAX) NULL,
    CONSTRAINT [PK_TaxiCashieringFeedLog] PRIMARY KEY CLUSTERED ([Id] ASC)
);

