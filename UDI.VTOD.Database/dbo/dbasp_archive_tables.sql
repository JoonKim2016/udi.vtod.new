﻿


CREATE PROCEDURE [dbo].[dbasp_archive_tables] 
(
	  @days_threshold int
	, @batch_size int
)
AS
BEGIN
	/*******************************************************************************************************************
	**
	**                              Copyright 2015
	**                                Property of
	**                            Unified-Dispatch, Inc.
	**
	*******************************************************************************************************************
	**
	**	Usage: archives all vtod database tables into the vtod_archive database.
	**
	*******************************************************************************************************************
	**
	**	Development History
	** 
	**	Case Number		WHO		Description
	**	--------------	------	-----------------------------------------------------------------------------------
	**	20150510			BC		Initial Revision.
	**
	**
	*******************************************************************************************************************/

/*

first batch 
	
	vtod_trip
	vtod_WS_State
	PushNotification
	ReferralTrip


second batch

	ecar_trip
	ecar_trip_status
	sds_trip
	taxi_trip
	van_trip
	vtod_trip_account_transaction
	vtod_trip_notification
	vtod_trip_rate
	vtod_trip_process ---added by tanushri


third batch

	taxi_trip_status
	van_trip_status
	vtod_trip_rate_reason
	

*/

	SET NOCOUNT ON

	IF ISNULL(@days_threshold, 0) < 60	-- must be at least 60 days
		BEGIN
			SET @days_threshold =  60	-- 1 year
		END
	
	IF ISNULL(@batch_size, 0) <  100
		BEGIN
			SET @batch_size = 100 
		END



	DECLARE	
		  @from_date datetime
		, @MaxRowCount bigint
		, @MinRowID bigint
		, @MaxRowID bigint
		, @counter int
		,@Rows bigint,
		@Ws_State_Rows bigint,
		@count_rows int

	SET @from_date = DATEADD(day, -@days_threshold, GETDATE())


	DECLARE @tmp_trip_id TABLE (trip_id bigint not null, rowid bigint identity(1,1) not null primary key (rowid))


	-- #################################################
	--	1. Archiving
	-- #################################################



	
	

	----Insert Push Notification Start----

	INSERT INTO vtod_archive.Utility.PushNotification
           (ID,NotificationConfigurationID
           ,WorkFlow
           ,Text
           ,Event
           ,ImageUrl
           ,VtodTripID
           ,NotificationType
           ,MemberID
           ,GroupID
           ,PushTimeUTC
           ,PushType
           ,ExpirationTime
           ,InsertedTime
           ,AppendTime
           ,UserName)
			SELECT ID
			,NotificationConfigurationID
			,WorkFlow
			,Text
			,Event
			,ImageUrl
			,VtodTripID
			,NotificationType
			,MemberID
			,GroupID
			,PushTimeUTC
			,PushType
			,ExpirationTime
			,InsertedTime
			,AppendTime
			,UserName
			FROM vtod.Utility.PushNotification
			where PushTimeUTC < @from_date
	SELECT @Rows=@@ROWCOUNT
	insert into vtod_archive.dbo.Archiving_Status(ObjectName,RowsInserted,AppendTime)
	values('PushNotification',ISNULL(@Rows,0),GETDATE())

	----Insert Push Notification End-------

	----Insert Referral Trip Start----

	INSERT INTO vtod_archive.Utility.ReferralTrip
           (ID,TripID
           ,MemberID
           ,ReferrerID
           ,ReferralCreditType
           ,ReferralCreditFlow
           ,ReferralCredit
           ,ErrorMessage
           ,ClientIPAddress
           ,ClientTimeStamp
           ,ClientHardwareID
           ,ClientOS
           ,ClientOSVersion
           ,InsertedTime
           ,AppendTime)
			SELECT ID
			,TripID
			,MemberID
			,ReferrerID
			,ReferralCreditType
			,ReferralCreditFlow
			,ReferralCredit
			,ErrorMessage
			,ClientIPAddress
			,ClientTimeStamp
			,ClientHardwareID
			,ClientOS
			,ClientOSVersion
			,InsertedTime
			,AppendTime
			FROM vtod.Utility.ReferralTrip
			where AppendTime < @from_date
	SELECT @Rows=@@ROWCOUNT
	insert into vtod_archive.dbo.Archiving_Status(ObjectName,RowsInserted,AppendTime)
	values('ReferralTrip',ISNULL(@Rows,0),GETDATE())

	----Insert Referral Trip End-------

	INSERT @tmp_trip_id (trip_id)
	SELECT [Id] 
	from vtod.dbo.vtod_trip (nolock) 
	where PickupDateTime < @from_date

	SET @MaxRowCount = ROWCOUNT_BIG()

	SET @MinRowID = 1
	SET @MaxRowID = @batch_size

	if(@MaxRowCount=0)
	begin
	--- Inserting data in archiving status table in case there is no records to insert
	select @count_rows=count(1) from vtod_archive.dbo.Archiving_Status where AppendTime=DATEADD(D, 0, DATEDIFF(D, 0, GETDATE()))
	if((@count_rows=0))
	
	begin
	insert into vtod_archive.dbo.Archiving_Status(ObjectName,RowsInserted,AppendTime)
	values('vtod_WS_State',@count_rows,GETDATE())
	insert into vtod_archive.dbo.Archiving_Status(ObjectName,RowsInserted,AppendTime)
	values('vtod_trip',@count_rows,GETDATE())
	insert into vtod_archive.dbo.Archiving_Status(ObjectName,RowsInserted,AppendTime)
	values('ecar_trip',@count_rows,GETDATE())
	insert into vtod_archive.dbo.Archiving_Status(ObjectName,RowsInserted,AppendTime)
	values('ecar_trip_status',@count_rows,GETDATE())
	insert into vtod_archive.dbo.Archiving_Status(ObjectName,RowsInserted,AppendTime)
	values('sds_trip',@count_rows,GETDATE())
	insert into vtod_archive.dbo.Archiving_Status(ObjectName,RowsInserted,AppendTime)
	values('taxi_trip',@count_rows,GETDATE())
	insert into vtod_archive.dbo.Archiving_Status(ObjectName,RowsInserted,AppendTime)
	values('van_trip',@count_rows,GETDATE())
	insert into vtod_archive.dbo.Archiving_Status(ObjectName,RowsInserted,AppendTime)
	values('vtod_trip_account_transaction',@count_rows,GETDATE())
	insert into vtod_archive.dbo.Archiving_Status(ObjectName,RowsInserted,AppendTime)
	values('vtod_trip_notification',@count_rows,GETDATE())
	insert into vtod_archive.dbo.Archiving_Status(ObjectName,RowsInserted,AppendTime)
	values('vtod_trip_rate',@count_rows,GETDATE())
	insert into vtod_archive.dbo.Archiving_Status(ObjectName,RowsInserted,AppendTime)
	values('vtod_trip_process',@count_rows,GETDATE())
	insert into vtod_archive.dbo.Archiving_Status(ObjectName,RowsInserted,AppendTime)
	values('taxi_trip_status',@count_rows,GETDATE())
	insert into vtod_archive.dbo.Archiving_Status(ObjectName,RowsInserted,AppendTime)
	values('van_trip_status',@count_rows,GETDATE())
	insert into vtod_archive.dbo.Archiving_Status(ObjectName,RowsInserted,AppendTime)
	values('vtod_trip_rate_reason',@count_rows,GETDATE())
	end
	end
	WHILE (@MinRowID <= @MaxRowCount)
		BEGIN

			--	SELECT @MinRowID, @MaxRowID


			INSERT vtod_archive.dbo.vtod_trip (
				  [Id]
				, [UserID]
				, [FleetType]
				, [MemberID]
				, [CreditCardID]
				, [DirectBillAccountID]
				, [FinalStatus]
				, [Gratuity]
				, [GratuityRate]
				, [TotalFareAmount]
				, [PickupDateTime]
				, [PickupDateTimeUTC]
				, [BookingServerDateTime]
				, [PickMeUpNow]
				, [ChargeTry]
				, [ChargeStatus]
				, [PaymentType]
				, [TripStartTime]
				, [TripEndTime]
				, [Error]
				, [ConsumerSource]
				, [ConsumerDevice]
				, [ConsumerRef]
				, [DriverName]
				, [DriverID]
				, [PromisedETA]
				, [DriverAcceptedETA]
				, [DriverInfoID]
				, [AppendTime]
				,[EmailAddress] 
				,[FleetTripCode]
				,[IsFlatRate]
			)
			SELECT
				  v.[Id]
				, v.[UserID]
				, v.[FleetType]
				, v.[MemberID]
				, v.[CreditCardID]
				, v.[DirectBillAccountID]
				, v.[FinalStatus]
				, v.[Gratuity]
				, v.[GratuityRate]
				, v.[TotalFareAmount]
				, v.[PickupDateTime]
				, v.[PickupDateTimeUTC]
				, v.[BookingServerDateTime]
				, v.[PickMeUpNow]
				, v.[ChargeTry]
				, v.[ChargeStatus]
				, v.[PaymentType]
				, v.[TripStartTime]
				, v.[TripEndTime]
				, v.[Error]
				, v.[ConsumerSource]
				, v.[ConsumerDevice]
				, v.[ConsumerRef]
				, v.[DriverName]
				, v.[DriverID]
				, v.[PromisedETA]
				, v.[DriverAcceptedETA]
				, v.[DriverInfoID]
				, v.[AppendTime]
				,v.[EmailAddress]
				,v.[FleetTripCode]
				,v.[IsFlatRate]
			FROM dbo.vtod_trip as v (nolock)
			JOIN @tmp_trip_id as t
				ON t.trip_id = v.Id
			WHERE t.rowid BETWEEN @MinRowID and @MaxRowID
			and not exists	(
							select 1
							from vtod_archive.dbo.vtod_trip as a (nolock)
							where a.id = v.id
						)

	SELECT @Rows=@@ROWCOUNT
	
	insert into vtod_archive.dbo.Archiving_Status(ObjectName,RowsInserted,AppendTime)
	values('vtod_trip',ISNULL(@Rows,0),GETDATE())

			INSERT INTO vtod_archive.dbo.ecar_trip (
				  [Id]
				, [FirstName]
				, [LastName]
				, [PhoneNumber]
				, [FleetId]
				, [ServiceAPIId]
				, [APIInformation]
				, [ECarNumber]
				, [MinutesAway]
				, [Expires]
				, [NumberOfPassenger]
				, [DriverNotes]
				, [AccountNumber]
				, [PickupAddressType]
				, [PickupLongitude]
				, [PickupLatitude]
				, [PickupBuilding]
				, [PickupStreetNo]
				, [PickupStreetName]
				, [PickupStreetType]
				, [PickupAptNo]
				, [PickupCity]
				, [PickupStateCode]
				, [PickupZipCode]
				, [PickupCountryCode]
				, [PickupFullAddress]
				, [PickupAirport]
				, [DropOffAddressType]
				, [DropOffLongitude]
				, [DropOffLatitude]
				, [DropOffBuilding]
				, [DropOffStreetNo]
				, [DropOffStreetName]
				, [DropOffStreetType]
				, [DropOffAptNo]
				, [DropOffCity]
				, [DropOffStateCode]
				, [DropOffZipCode]
				, [DropOffCountryCode]
				, [DropOffFullAddress]
				, [DropOffAirport]
				, [DispatchTripId]
				, [ConsumerConfirmationId]
				, [DriverGivenName]
				, [DriverSurName]
				, [ApplicationName]
				, [Device]
				, [RefernceNumber]
				, [RemarkForPickup]
				, [RemarkForDropOff]
			)
			SELECT
				  v.[Id]
				, v.[FirstName]
				, v.[LastName]
				, v.[PhoneNumber]
				, v.[FleetId]
				, v.[ServiceAPIId]
				, v.[APIInformation]
				, v.[ECarNumber]
				, v.[MinutesAway]
				, v.[Expires]
				, v.[NumberOfPassenger]
				, v.[DriverNotes]
				, v.[AccountNumber]
				, v.[PickupAddressType]
				, v.[PickupLongitude]
				, v.[PickupLatitude]
				, v.[PickupBuilding]
				, v.[PickupStreetNo]
				, v.[PickupStreetName]
				, v.[PickupStreetType]
				, v.[PickupAptNo]
				, v.[PickupCity]
				, v.[PickupStateCode]
				, v.[PickupZipCode]
				, v.[PickupCountryCode]
				, v.[PickupFullAddress]
				, v.[PickupAirport]
				, v.[DropOffAddressType]
				, v.[DropOffLongitude]
				, v.[DropOffLatitude]
				, v.[DropOffBuilding]
				, v.[DropOffStreetNo]
				, v.[DropOffStreetName]
				, v.[DropOffStreetType]
				, v.[DropOffAptNo]
				, v.[DropOffCity]
				, v.[DropOffStateCode]
				, v.[DropOffZipCode]
				, v.[DropOffCountryCode]
				, v.[DropOffFullAddress]
				, v.[DropOffAirport]
				, v.[DispatchTripId]
				, v.[ConsumerConfirmationId]
				, v.[DriverGivenName]
				, v.[DriverSurName]
				, v.[ApplicationName]
				, v.[Device]
				, v.[RefernceNumber]
				, v.[RemarkForPickup]
				, v.[RemarkForDropOff]
			FROM dbo.ecar_trip as v (nolock)
			JOIN @tmp_trip_id as t
				ON t.trip_id = v.Id
			WHERE t.rowid BETWEEN @MinRowID and @MaxRowID
			and not exists	(
							select 1
							from vtod_archive.dbo.ecar_trip as a (nolock)
							where a.id = v.id
						)

	SELECT @Rows=@@ROWCOUNT
	insert into vtod_archive.dbo.Archiving_Status(ObjectName,RowsInserted,AppendTime)
	values('ecar_trip',ISNULL(@Rows,0),GETDATE())

			INSERT INTO vtod_archive.dbo.ecar_trip_status (
				  [Id]
				, [TripID]
				, [Status]
				, [StatusTime]
				, [VehicleNumber]
				, [VehicleLongitude]
				, [VehicleLatitude]
				, [DriverName]
				, [DriverId]
				, [ETA]
				, [Fare]
				, [CommentForStatus]
				, [OriginalStatus]
				, [AppendTime]
			)
			SELECT 
				  v.[Id]
				, v.[TripID]
				, v.[Status]
				, v.[StatusTime]
				, v.[VehicleNumber]
				, v.[VehicleLongitude]
				, v.[VehicleLatitude]
				, v.[DriverName]
				, v.[DriverId]
				, v.[ETA]
				, v.[Fare]
				, v.[CommentForStatus]
				, v.[OriginalStatus]
				, v.[AppendTime]
			FROM dbo.ecar_trip_status as v (nolock)
			JOIN @tmp_trip_id as t
				ON t.trip_id = v.TripID
			WHERE t.rowid BETWEEN @MinRowID and @MaxRowID
			and not exists	(
							select 1
							from vtod_archive.dbo.ecar_trip_status as a (nolock)
							where a.id = v.id
						)

	SELECT @Rows=@@ROWCOUNT
	
	insert into vtod_archive.dbo.Archiving_Status(ObjectName,RowsInserted,AppendTime)
	values('ecar_trip_status',ISNULL(@Rows,0),GETDATE())
	
			INSERT INTO vtod_archive.dbo.sds_trip (
				  [Id]
				, [LastName]
				, [FirstName]
				, [IsRoundTrip]
				, [PayingPax]
				, [FreePax]
				, [AccessibleServiceRequired]
				, [ContactNumberDialingPrefix]
				, [ContactNumber]
				, [PickupLocation]
				, [PickupLocationLatitude]
				, [PickupLocationLongitude]
				, [DropoffLocation]
				, [DropoffLocationLatitude]
				, [DropoffLocationLongitude]
				, [RequestedFleet]
				, [FlightDateTime]
				, [IsInternationalFlight]
				, [FlightNumber]
				, [AirlineCode]
				, [CharterMinutes]
				, [ConfirmationNumber]
				, [RezID]
			)
			SELECT
				  v.[Id]
				, v.[LastName]
				, v.[FirstName]
				, v.[IsRoundTrip]
				, v.[PayingPax]
				, v.[FreePax]
				, v.[AccessibleServiceRequired]
				, v.[ContactNumberDialingPrefix]
				, v.[ContactNumber]
				, v.[PickupLocation]
				, v.[PickupLocationLatitude]
				, v.[PickupLocationLongitude]
				, v.[DropoffLocation]
				, v.[DropoffLocationLatitude]
				, v.[DropoffLocationLongitude]
				, v.[RequestedFleet]
				, v.[FlightDateTime]
				, v.[IsInternationalFlight]
				, v.[FlightNumber]
				, v.[AirlineCode]
				, v.[CharterMinutes]
				, v.[ConfirmationNumber]
				, v.[RezID]
			FROM dbo.sds_trip as v (nolock)
			JOIN @tmp_trip_id as t
				ON t.trip_id = v.Id
			WHERE t.rowid BETWEEN @MinRowID and @MaxRowID
			and not exists	(
							select 1
							from vtod_archive.dbo.sds_trip as a (nolock)
							where a.id = v.id
						)

	SELECT @Rows=@@ROWCOUNT
	insert into vtod_archive.dbo.Archiving_Status(ObjectName,RowsInserted,AppendTime)
	values('sds_trip',ISNULL(@Rows,0),GETDATE())

			INSERT INTO vtod_archive.dbo.taxi_trip (
				  [Id]
				, [FirstName]
				, [LastName]
				, [PhoneNumber]
				, [FleetId]
				, [RezId]
				, [ServiceAPIId]
				, [TripType]
				, [APIInformation]
				, [CabNumber]
				, [MinutesAway]
				, [Expires]
				, [NumberOfPassenger]
				, [DriverNotes]
				, [PickupFlatRateZone]
				, [DropOffFlatRateZone]
				, [AccountNumber]
				, [PickupAddressType]
				, [PickupLongitude]
				, [PickupLatitude]
				, [PickupBuilding]
				, [PickupStreetNo]
				, [PickupStreetName]
				, [PickupStreetType]
				, [PickupAptNo]
				, [PickupCity]
				, [PickupStateCode]
				, [PickupZipCode]
				, [PickupCountryCode]
				, [PickupFullAddress]
				, [PickupAirport]
				, [DropOffAddressType]
				, [DropOffLongitude]
				, [DropOffLatitude]
				, [DropOffBuilding]
				, [DropOffStreetNo]
				, [DropOffStreetName]
				, [DropOffStreetType]
				, [DropOffAptNo]
				, [DropOffCity]
				, [DropOffStateCode]
				, [DropOffZipCode]
				, [DropOffCountryCode]
				, [DropOffFullAddress]
				, [DropOffAirport]
				, [DispatchTripId]
				, [RemarkForPickup]
				, [RemarkForDropOff]
				, [wheelchairAccessible]
				, [NumOfTry]
			)
			SELECT
				  v.[Id]
				, v.[FirstName]
				, v.[LastName]
				, v.[PhoneNumber]
				, v.[FleetId]
				, v.[RezId]
				, v.[ServiceAPIId]
				, v.[TripType]
				, v.[APIInformation]
				, v.[CabNumber]
				, v.[MinutesAway]
				, v.[Expires]
				, v.[NumberOfPassenger]
				, v.[DriverNotes]
				, v.[PickupFlatRateZone]
				, v.[DropOffFlatRateZone]
				, v.[AccountNumber]
				, v.[PickupAddressType]
				, v.[PickupLongitude]
				, v.[PickupLatitude]
				, v.[PickupBuilding]
				, v.[PickupStreetNo]
				, v.[PickupStreetName]
				, v.[PickupStreetType]
				, v.[PickupAptNo]
				, v.[PickupCity]
				, v.[PickupStateCode]
				, v.[PickupZipCode]
				, v.[PickupCountryCode]
				, v.[PickupFullAddress]
				, v.[PickupAirport]
				, v.[DropOffAddressType]
				, v.[DropOffLongitude]
				, v.[DropOffLatitude]
				, v.[DropOffBuilding]
				, v.[DropOffStreetNo]
				, v.[DropOffStreetName]
				, v.[DropOffStreetType]
				, v.[DropOffAptNo]
				, v.[DropOffCity]
				, v.[DropOffStateCode]
				, v.[DropOffZipCode]
				, v.[DropOffCountryCode]
				, v.[DropOffFullAddress]
				, v.[DropOffAirport]
				, v.[DispatchTripId]
				, v.[RemarkForPickup]
				, v.[RemarkForDropOff]
				, v.[wheelchairAccessible]
				, v.[NumOfTry]
			FROM dbo.taxi_trip as v (nolock)
			JOIN @tmp_trip_id as t
				ON t.trip_id = v.Id
			WHERE t.rowid BETWEEN @MinRowID and @MaxRowID
			and not exists	(
							select 1
							from vtod_archive.dbo.taxi_trip as a (nolock)
							where a.id = v.id
						)

	SELECT @Rows=@@ROWCOUNT
	insert into vtod_archive.dbo.Archiving_Status(ObjectName,RowsInserted,AppendTime)
	values('taxi_trip',ISNULL(@Rows,0),GETDATE())


	INSERT INTO vtod_archive.dbo.van_trip (
				  [Id]
				, [FirstName]
				, [LastName]
				, [PhoneNumber]
				, [FleetId]
				, [RezId]
				, [ServiceAPIId]
				, [TripType]
				, [APIInformation]
				, [CabNumber]
				, [MinutesAway]
				, [Expires]
				, [NumberOfPassenger]
				, [DriverNotes]
				, [PickupFlatRateZone]
				, [DropOffFlatRateZone]
				, [AccountNumber]
				, [PickupAddressType]
				, [PickupLongitude]
				, [PickupLatitude]
				, [PickupBuilding]
				, [PickupStreetNo]
				, [PickupStreetName]
				, [PickupStreetType]
				, [PickupAptNo]
				, [PickupCity]
				, [PickupStateCode]
				, [PickupZipCode]
				, [PickupCountryCode]
				, [PickupFullAddress]
				, [PickupAirport]
				, [DropOffAddressType]
				, [DropOffLongitude]
				, [DropOffLatitude]
				, [DropOffBuilding]
				, [DropOffStreetNo]
				, [DropOffStreetName]
				, [DropOffStreetType]
				, [DropOffAptNo]
				, [DropOffCity]
				, [DropOffStateCode]
				, [DropOffZipCode]
				, [DropOffCountryCode]
				, [DropOffFullAddress]
				, [DropOffAirport]
				, [DispatchTripId]
				, [RemarkForPickup]
				, [RemarkForDropOff]
				, [wheelchairAccessible]
				, [NumOfTry]
			)
			SELECT
				  v.[Id]
				, v.[FirstName]
				, v.[LastName]
				, v.[PhoneNumber]
				, v.[FleetId]
				, v.[RezId]
				, v.[ServiceAPIId]
				, v.[TripType]
				, v.[APIInformation]
				, v.[CabNumber]
				, v.[MinutesAway]
				, v.[Expires]
				, v.[NumberOfPassenger]
				, v.[DriverNotes]
				, v.[PickupFlatRateZone]
				, v.[DropOffFlatRateZone]
				, v.[AccountNumber]
				, v.[PickupAddressType]
				, v.[PickupLongitude]
				, v.[PickupLatitude]
				, v.[PickupBuilding]
				, v.[PickupStreetNo]
				, v.[PickupStreetName]
				, v.[PickupStreetType]
				, v.[PickupAptNo]
				, v.[PickupCity]
				, v.[PickupStateCode]
				, v.[PickupZipCode]
				, v.[PickupCountryCode]
				, v.[PickupFullAddress]
				, v.[PickupAirport]
				, v.[DropOffAddressType]
				, v.[DropOffLongitude]
				, v.[DropOffLatitude]
				, v.[DropOffBuilding]
				, v.[DropOffStreetNo]
				, v.[DropOffStreetName]
				, v.[DropOffStreetType]
				, v.[DropOffAptNo]
				, v.[DropOffCity]
				, v.[DropOffStateCode]
				, v.[DropOffZipCode]
				, v.[DropOffCountryCode]
				, v.[DropOffFullAddress]
				, v.[DropOffAirport]
				, v.[DispatchTripId]
				, v.[RemarkForPickup]
				, v.[RemarkForDropOff]
				, v.[wheelchairAccessible]
				, v.[NumOfTry]
			FROM dbo.van_trip as v (nolock)
			JOIN @tmp_trip_id as t
				ON t.trip_id = v.Id
			WHERE t.rowid BETWEEN @MinRowID and @MaxRowID
			and not exists	(
							select 1
							from vtod_archive.dbo.van_trip as a (nolock)
							where a.id = v.id
						)

	SELECT @Rows=@@ROWCOUNT
	insert into vtod_archive.dbo.Archiving_Status(ObjectName,RowsInserted,AppendTime)
	values('van_trip',ISNULL(@Rows,0),GETDATE())
			INSERT INTO vtod_archive.dbo.vtod_trip_account_transaction (
				  [Id]
				, [TripID]
				, [TransactionNumber]
				, [AmountCharged]
				, [CreditsApplied]
				, [Error]
				, [AppendTime]
			)
			SELECT 
				  v.[Id]
				, v.[TripID]
				, v.[TransactionNumber]
				, v.[AmountCharged]
				, v.[CreditsApplied]
				, v.[Error]
				, v.[AppendTime]
			FROM dbo.vtod_trip_account_transaction as v (nolock)
			JOIN @tmp_trip_id as t
				ON t.trip_id = v.TripID
			WHERE t.rowid BETWEEN @MinRowID and @MaxRowID
			and not exists	(
							select 1
							from vtod_archive.dbo.vtod_trip_account_transaction as a (nolock)
							where a.id = v.id
						)

	SELECT @Rows=@@ROWCOUNT
	insert into vtod_archive.dbo.Archiving_Status(ObjectName,RowsInserted,AppendTime)
	values('vtod_trip_account_transaction',ISNULL(@Rows,0),GETDATE())


			INSERT INTO vtod_archive.dbo.vtod_trip_notification (
				  [Id]
				, [Booked]
				, [BookedTime]
				, [BookedError]
				, [Assigned]
				, [AssignedTime]
				, [AssignedError]
				, [Completed]
				, [CompletedTime]
				, [CompletedError]
				, [Canceled]
				, [CanceledTime]
				, [CanceledError]
			)
			SELECT 
				  v.[Id]
				, v.[Booked]
				, v.[BookedTime]
				, v.[BookedError]
				, v.[Assigned]
				, v.[AssignedTime]
				, v.[AssignedError]
				, v.[Completed]
				, v.[CompletedTime]
				, v.[CompletedError]
				, v.[Canceled]
				, v.[CanceledTime]
				, v.[CanceledError]
			FROM dbo.vtod_trip_notification as v (nolock)
			JOIN @tmp_trip_id as t
				ON t.trip_id = v.Id
			WHERE t.rowid BETWEEN @MinRowID and @MaxRowID
			and not exists	(
							select 1
							from vtod_archive.dbo.vtod_trip_notification as a (nolock)
							where a.id = v.id
						)


	SELECT @Rows=@@ROWCOUNT
	insert into vtod_archive.dbo.Archiving_Status(ObjectName,RowsInserted,AppendTime)
	values('vtod_trip_notification',ISNULL(@Rows,0),GETDATE())

			INSERT INTO vtod_archive.dbo.vtod_trip_rate (
				  [Id]
				, [TripID]
				, [Rate]
				, [Comment]
				, [Type]
				, [AppendTime]
			)
			SELECT 
				  v.[Id]
				, v.[TripID]
				, v.[Rate]
				, v.[Comment]
				, v.[Type]
				, v.[AppendTime]
			FROM dbo.vtod_trip_rate as v (nolock)
			JOIN @tmp_trip_id as t
				ON t.trip_id = v.TripID
			WHERE t.rowid BETWEEN @MinRowID and @MaxRowID
			and not exists	(
							select 1
							from vtod_archive.dbo.vtod_trip_rate as a (nolock)
							where a.id = v.id
						)

	SELECT @Rows=@@ROWCOUNT
	insert into vtod_archive.dbo.Archiving_Status(ObjectName,RowsInserted,AppendTime)
	values('vtod_trip_rate',ISNULL(@Rows,0),GETDATE())
			

INSERT INTO vtod_archive.dbo.vtod_trip_process
           (Id
           ,CheckTripStatus)
			SELECT 
				  v.Id
				, v.CheckTripStatus
			FROM dbo.vtod_trip_process as v (nolock)
			JOIN @tmp_trip_id as t
				ON t.trip_id = v.Id
			WHERE t.rowid BETWEEN @MinRowID and @MaxRowID
			and not exists	(
							select 1
							from vtod_archive.dbo.vtod_trip_process as a (nolock)
							where a.id = v.id
						)

	SELECT @Rows=@@ROWCOUNT
	insert into vtod_archive.dbo.Archiving_Status(ObjectName,RowsInserted,AppendTime)
	values('vtod_trip_process',ISNULL(@Rows,0),GETDATE())


	INSERT INTO vtod_archive.dbo.taxi_trip_status (
				  [Id]
				, [TripID]
				, [Status]
				, [StatusTime]
				, [VehicleNumber]
				, [VehicleLongitude]
				, [VehicleLatitude]
				, [DriverName]
				, [DriverId]
				, [ETA]
				, [ETAWithTraffic]
				, [Fare]
				, [DispatchFare]
				, [CommentForStatus]
				, [OriginalStatus]
				, [AppendTime]
			)
			SELECT 
				  v.[Id]
				, v.[TripID]
				, v.[Status]
				, v.[StatusTime]
				, v.[VehicleNumber]
				, v.[VehicleLongitude]
				, v.[VehicleLatitude]
				, v.[DriverName]
				, v.[DriverId]
				, v.[ETA]
				, v.[ETAWithTraffic]
				, v.[Fare]
				, v.[DispatchFare]
				, v.[CommentForStatus]
				, v.[OriginalStatus]
				, v.[AppendTime]
			FROM dbo.taxi_trip_status as v (nolock)
			JOIN @tmp_trip_id as t
				ON t.trip_id = v.TripID
			WHERE t.rowid BETWEEN @MinRowID and @MaxRowID
			and not exists	(
							select 1
							from vtod_archive.dbo.taxi_trip_status as a (nolock)
							where a.id = v.id
						)

	SELECT @Rows=@@ROWCOUNT
	insert into vtod_archive.dbo.Archiving_Status(ObjectName,RowsInserted,AppendTime)
	values('taxi_trip_status',ISNULL(@Rows,0),GETDATE())


	
	INSERT INTO vtod_archive.dbo.van_trip_status (
				  [Id]
				, [TripID]
				, [Status]
				, [StatusTime]
				, [VehicleNumber]
				, [VehicleLongitude]
				, [VehicleLatitude]
				, [DriverName]
				, [DriverId]
				, [ETA]
				, [ETAWithTraffic]
				, [Fare]
				, [DispatchFare]
				, [CommentForStatus]
				, [OriginalStatus]
				, [AppendTime]
			)
			SELECT 
				  v.[Id]
				, v.[TripID]
				, v.[Status]
				, v.[StatusTime]
				, v.[VehicleNumber]
				, v.[VehicleLongitude]
				, v.[VehicleLatitude]
				, v.[DriverName]
				, v.[DriverId]
				, v.[ETA]
				, v.[ETAWithTraffic]
				, v.[Fare]
				, v.[DispatchFare]
				, v.[CommentForStatus]
				, v.[OriginalStatus]
				, v.[AppendTime]
			FROM dbo.van_trip_status as v (nolock)
			JOIN @tmp_trip_id as t
				ON t.trip_id = v.TripID
			WHERE t.rowid BETWEEN @MinRowID and @MaxRowID
			and not exists	(
							select 1
							from vtod_archive.dbo.van_trip_status as a (nolock)
							where a.id = v.id
						)

	SELECT @Rows=@@ROWCOUNT
	insert into vtod_archive.dbo.Archiving_Status(ObjectName,RowsInserted,AppendTime)
	values('van_trip_status',ISNULL(@Rows,0),GETDATE())


	INSERT INTO vtod_archive.dbo.vtod_trip_rate_reason (
				  [Id]
				, [RateId]
				, [Key]
				, [Value]
			)
			SELECT 
				  v.[Id]
				, v.[RateId]
				, v.[Key]
				, v.[Value]
			FROM dbo.vtod_trip_rate_reason as v (nolock)
			JOIN dbo.vtod_trip_rate as vt (nolock)
				on vt.Id = v.RateId
			JOIN @tmp_trip_id as t
				ON t.trip_id = vt.TripID
			WHERE t.rowid BETWEEN @MinRowID and @MaxRowID
			and not exists	(
							select 1
							from vtod_archive.dbo.vtod_trip_rate_reason as a (nolock)
							where a.id = v.id
						)

	SELECT @Rows=@@ROWCOUNT
	insert into vtod_archive.dbo.Archiving_Status(ObjectName,RowsInserted,AppendTime)
	values('vtod_trip_rate_reason',ISNULL(@Rows,0),GETDATE())


	
		INSERT vtod_archive.dbo.vtod_WS_State (
		  [Id]
		, [ReferenceId]
		, [TableName]
		,[Key]
		, [State]
		, [AppendTime]
	)
	SELECT
		  [Id]
		, [ReferenceId]
		, [TableName]
		,[Key]
		, [State]
		, [AppendTime]
	
	FROM dbo.vtod_WS_State as v (nolock)
			JOIN @tmp_trip_id as t
				ON t.trip_id = v.ReferenceId
			WHERE t.rowid BETWEEN @MinRowID and @MaxRowID
			and not exists	(
							select 1
							from vtod_archive.dbo.vtod_WS_State as a (nolock)
							where a.id = v.id
						)

	SELECT @Rows=@@ROWCOUNT

	insert into vtod_archive.dbo.Archiving_Status(ObjectName,RowsInserted,AppendTime)
	values('vtod_WS_State',ISNULL(@Rows,0),GETDATE())

	-- #################################################
	--	2. Deleting
	-- #################################################
		
			DELETE v
			FROM dbo.vtod_trip_rate_reason as v (nolock)
			JOIN vtod_archive.dbo.vtod_trip_rate_reason as a (nolock)
				on a.Id = v.Id
			JOIN dbo.vtod_trip_rate as vt (nolock)
					on vt.Id = v.RateId
			JOIN @tmp_trip_id as t
					ON t.trip_id = vt.TripID
			WHERE t.rowid BETWEEN @MinRowID and @MaxRowID

			DELETE v
			FROM dbo.taxi_trip_status as v (nolock)
			JOIN vtod_archive.dbo.taxi_trip_status as a (nolock)
				on a.Id = v.Id
			JOIN @tmp_trip_id as t
					ON t.trip_id = v.TripID
			WHERE t.rowid BETWEEN @MinRowID and @MaxRowID
			

			DELETE v
			FROM dbo.van_trip_status as v (nolock)
			JOIN vtod_archive.dbo.van_trip_status as a (nolock)
				on a.Id = v.Id
			JOIN @tmp_trip_id as t
					ON t.trip_id = v.TripID
			WHERE t.rowid BETWEEN @MinRowID and @MaxRowID

			DELETE v
			FROM dbo.vtod_trip_process as v (nolock)
			JOIN vtod_archive.dbo.vtod_trip_process as a (nolock)
				on a.Id = v.Id
			JOIN @tmp_trip_id as t
					ON t.trip_id = v.Id
			WHERE t.rowid BETWEEN @MinRowID and @MaxRowID
			
	
			DELETE v
			FROM dbo.vtod_trip_rate as v (nolock)
			JOIN vtod_archive.dbo.vtod_trip_rate as a (nolock)
				on a.Id = v.Id
			JOIN @tmp_trip_id as t
					ON t.trip_id = v.TripID
			WHERE t.rowid BETWEEN @MinRowID and @MaxRowID


			DELETE v
			FROM dbo.vtod_trip_notification as v (nolock)
			JOIN vtod_archive.dbo.vtod_trip_notification as a (nolock)
				on a.Id = v.Id
			JOIN @tmp_trip_id as t
					ON t.trip_id = v.Id
			WHERE t.rowid BETWEEN @MinRowID and @MaxRowID


			DELETE v
			FROM dbo.vtod_trip_account_transaction as v (nolock)
			JOIN vtod_archive.dbo.vtod_trip_account_transaction as a (nolock)
				on a.Id = v.Id
			JOIN @tmp_trip_id as t
					ON t.trip_id = v.TripID
			WHERE t.rowid BETWEEN @MinRowID and @MaxRowID


			DELETE v
			FROM dbo.taxi_trip as v (nolock)
			JOIN vtod_archive.dbo.taxi_trip as a (nolock)
				on a.Id = v.Id
			JOIN @tmp_trip_id as t
					ON t.trip_id = v.Id
			WHERE t.rowid BETWEEN @MinRowID and @MaxRowID

			DELETE v
			FROM dbo.van_trip as v (nolock)
			JOIN vtod_archive.dbo.van_trip as a (nolock)
				on a.Id = v.Id
			JOIN @tmp_trip_id as t
					ON t.trip_id = v.Id
			WHERE t.rowid BETWEEN @MinRowID and @MaxRowID

			DELETE v
			FROM dbo.sds_trip as v (nolock)
			JOIN vtod_archive.dbo.sds_trip as a (nolock)
				on a.Id = v.Id
			JOIN @tmp_trip_id as t
					ON t.trip_id = v.Id
			WHERE t.rowid BETWEEN @MinRowID and @MaxRowID


			DELETE v
			FROM dbo.ecar_trip_status as v (nolock)
			JOIN vtod_archive.dbo.ecar_trip_status as a (nolock)
				on a.Id = v.Id
			JOIN @tmp_trip_id as t
					ON t.trip_id = v.TripID
			WHERE t.rowid BETWEEN @MinRowID and @MaxRowID


			DELETE v
			FROM dbo.ecar_trip as v (nolock)
			JOIN vtod_archive.dbo.ecar_trip as a (nolock)
				on a.Id = v.Id
			JOIN @tmp_trip_id as t
					ON t.trip_id = v.Id
			WHERE t.rowid BETWEEN @MinRowID and @MaxRowID

		DELETE v
		FROM dbo.vtod_WS_State as v (nolock)
			JOIN vtod_archive.dbo.vtod_WS_State as a (nolock)
				on a.Id = v.Id
			JOIN @tmp_trip_id as t
					ON t.trip_id = v.ReferenceId
			WHERE t.rowid BETWEEN @MinRowID and @MaxRowID


			DELETE v
			FROM dbo.vtod_trip as v (nolock)
			JOIN vtod_archive.dbo.vtod_trip as a (nolock)
				on a.Id = v.Id
			JOIN @tmp_trip_id as t
					ON t.trip_id = v.Id
			WHERE t.rowid BETWEEN @MinRowID and @MaxRowID



			SET @MinRowID = @MinRowID + @batch_size
			SET @MaxRowID = @MaxRowID + @batch_size

		END

		DELETE v
		FROM vtod.Utility.PushNotification as v (nolock)
		JOIN vtod_archive.Utility.PushNotification as a (nolock)
			on a.ID = v.ID
		where  v.PushTimeUTC < @from_date

		DELETE v
		FROM vtod.Utility.ReferralTrip as v (nolock)
		JOIN vtod_archive.Utility.ReferralTrip as a (nolock)
			on a.ID = v.ID
		where v.AppendTime < @from_date


	

	RETURN 0
END

/*****************************************************************************
**
**  Test Case 1: with all parameters
**               
**


select min(AppendTime), max(AppendTime), count(1) as rowcount_vtod_trip from dbo.vtod_trip (nolock)

select min(AppendTime), max(AppendTime), count(1) as rowcount_ARCHIVED_vtod_trip from vtod_archive.dbo.vtod_trip (nolock)

select min(AppendTime), max(AppendTime), count(1) as rowcount_vtod_WS_State from dbo.vtod_WS_State (nolock)

select min(AppendTime), max(AppendTime), count(1) as rowcount_ARCHIVED_vtod_WS_State from vtod_archive.dbo.vtod_WS_State (nolock)


DECLARE @RC int

EXEC @RC = dbo.dbasp_archive_tables 
	  @days_threshold = 60
	, @batch_size = 20000


PRINT 'Return Code:  ' + LTRIM(STR(@RC))


select min(AppendTime), max(AppendTime), count(1) as rowcount_vtod_trip from dbo.vtod_trip (nolock)

select min(AppendTime), max(AppendTime), count(1) as rowcount_ARCHIVED_vtod_trip from vtod_archive.dbo.vtod_trip (nolock)

select min(AppendTime), max(AppendTime), count(1) as rowcount_vtod_WS_State from dbo.vtod_WS_State (nolock)

select min(AppendTime), max(AppendTime), count(1) as rowcount_ARCHIVED_vtod_WS_State from vtod_archive.dbo.vtod_WS_State (nolock)


select top 20 'vtod_trip' as 'vtod_trip', * from dbo.vtod_trip (nolock) order by id desc
select top 20 'ARCHIVED_vtod_trip' as 'ARCHIVED_vtod_trip', * from vtod_archive.dbo.vtod_trip (nolock) order by id desc




*****************************************************************************/

