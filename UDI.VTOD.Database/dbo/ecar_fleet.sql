﻿CREATE TABLE [dbo].[ecar_fleet] (
    [Id]                    BIGINT         IDENTITY (3, 1) NOT NULL,
    [ProviderId]            BIGINT         NOT NULL,
    [Status]                BIT            CONSTRAINT [DF__ecar_flee__Statu__7D0E9093] DEFAULT ((1)) NOT NULL,
    [Name]                  NVARCHAR (40)  DEFAULT (NULL) NULL,
    [Alias]                 NVARCHAR (50)  DEFAULT (NULL) NULL,
    [PhoneNumber]           NVARCHAR (50)  DEFAULT (NULL) NULL,
    [WebSite]               NVARCHAR (100) DEFAULT (NULL) NULL,
    [ServerUTCOffset]       NVARCHAR (50)  DEFAULT (NULL) NULL,
    [RestrictBookingOption] INT            DEFAULT ((1)) NOT NULL,
    [RestrictBookingMins]   INT            DEFAULT (NULL) NULL,
    [ExpiredBookingHrs]     INT            DEFAULT ((4)) NULL,
    [AppendTime]            DATETIME       DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ecar_fleet_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);

