﻿CREATE TABLE [dbo].[ecar_fleet_api_reference] (
    [Id]           BIGINT        IDENTITY (19, 1) NOT NULL,
    [FleetId]      BIGINT        NOT NULL,
    [ServiceAPIId] BIGINT        NOT NULL,
    [Name]         NVARCHAR (40) DEFAULT (NULL) NULL,
    [AppendTime]   DATETIME      DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ecar_fleet_api_reference_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [ecar_fleet_api_reference$ecar_fleet_api_reference_ibfk_1] FOREIGN KEY ([FleetId]) REFERENCES [dbo].[ecar_fleet] ([Id]),
    CONSTRAINT [ecar_fleet_api_reference$ecar_fleet_api_reference_ibfk_2] FOREIGN KEY ([ServiceAPIId]) REFERENCES [dbo].[ecar_serviceapi] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [ecar_fleet_serviceapipreference_ibfk_2]
    ON [dbo].[ecar_fleet_api_reference]([ServiceAPIId] ASC);


GO
CREATE NONCLUSTERED INDEX [Idx_fleetID]
    ON [dbo].[ecar_fleet_api_reference]([FleetId] ASC);


GO
CREATE NONCLUSTERED INDEX [IdxFleetIDServiceAPIID]
    ON [dbo].[ecar_fleet_api_reference]([FleetId] ASC, [ServiceAPIId] ASC);

