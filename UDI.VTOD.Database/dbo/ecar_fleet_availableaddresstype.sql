﻿CREATE TABLE [dbo].[ecar_fleet_availableaddresstype] (
    [Id]               BIGINT   IDENTITY (3, 1) NOT NULL,
    [FleetId]          BIGINT   NOT NULL,
    [AddressToAddress] BIT      CONSTRAINT [DF__ecar_flee__Addre__0880433F] DEFAULT ((1)) NOT NULL,
    [AddressToAirport] BIT      CONSTRAINT [DF__ecar_flee__Addre__09746778] DEFAULT ((0)) NOT NULL,
    [AddressToNull]    BIT      CONSTRAINT [DF__ecar_flee__Addre__0A688BB1] DEFAULT ((1)) NOT NULL,
    [AirportToAddress] BIT      CONSTRAINT [DF__ecar_flee__Airpo__0B5CAFEA] DEFAULT ((1)) NOT NULL,
    [AirportToAirport] BIT      CONSTRAINT [DF__ecar_flee__Airpo__0C50D423] DEFAULT ((0)) NOT NULL,
    [AirportToNull]    BIT      CONSTRAINT [DF__ecar_flee__Airpo__0D44F85C] DEFAULT ((1)) NOT NULL,
    [AppendTime]       DATETIME DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ecar_fleet_availableaddresstype_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IdxFleet]
    ON [dbo].[ecar_fleet_availableaddresstype]([FleetId] ASC);

