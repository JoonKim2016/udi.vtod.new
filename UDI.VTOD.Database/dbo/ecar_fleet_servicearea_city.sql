﻿CREATE TABLE [dbo].[ecar_fleet_servicearea_city] (
    [Id]         BIGINT         IDENTITY (1744, 1) NOT NULL,
    [FleetId]    BIGINT         NOT NULL,
    [Status]     BIT            CONSTRAINT [DF__ecar_flee__Statu__0F2D40CE] DEFAULT ((1)) NOT NULL,
    [City]       NVARCHAR (100) DEFAULT (NULL) NULL,
    [State]      NVARCHAR (50)  DEFAULT (NULL) NULL,
    [Country]    NVARCHAR (50)  DEFAULT (NULL) NULL,
    [Ordering]   INT            NOT NULL,
    [AppendTime] DATETIME       DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ecar_fleet_servicearea_city_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [ecar_fleet_servicearea_city$ecar_fleet_servicearea_city_ibfk_1] FOREIGN KEY ([FleetId]) REFERENCES [dbo].[ecar_fleet] ([Id]),
    CONSTRAINT [ecar_fleet_servicearea_city$Idx_ServiceAreaCity] UNIQUE NONCLUSTERED ([FleetId] ASC, [City] ASC, [State] ASC, [Country] ASC, [Ordering] ASC)
);


GO
CREATE NONCLUSTERED INDEX [Idx_EcarStatusCityStateCountry]
    ON [dbo].[ecar_fleet_servicearea_city]([Status] ASC, [City] ASC, [State] ASC, [Country] ASC, [Ordering] ASC);


GO
CREATE NONCLUSTERED INDEX [IdxFleetIDStatusCityStateCountry]
    ON [dbo].[ecar_fleet_servicearea_city]([FleetId] ASC, [Status] ASC, [City] ASC, [State] ASC, [Country] ASC);

