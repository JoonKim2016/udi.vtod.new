﻿CREATE TABLE [dbo].[ecar_fleet_servicearea_polygon] (
    [Id]         BIGINT         IDENTITY (4, 1) NOT NULL,
    [FleetId]    BIGINT         NOT NULL,
    [Status]     BIT            CONSTRAINT [DF__ecar_flee__Statu__13F1F5EB] DEFAULT ((1)) NOT NULL,
    [Type]       NVARCHAR (20)  DEFAULT (N'Area') NOT NULL,
    [Name]       NVARCHAR (100) NOT NULL,
    [PolygonId]  BIGINT         NOT NULL,
    [Ordering]   INT            NOT NULL,
    [AppendTime] DATETIME       DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ecar_fleet_servicearea_polygon_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [ecar_fleet_servicearea_polygon$ecar_fleet_servicearea_polygon_ibfk_1] FOREIGN KEY ([FleetId]) REFERENCES [dbo].[ecar_fleet] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [FleetId]
    ON [dbo].[ecar_fleet_servicearea_polygon]([FleetId] ASC);


GO
CREATE NONCLUSTERED INDEX [Idx_StatusName]
    ON [dbo].[ecar_fleet_servicearea_polygon]([Status] ASC, [Name] ASC, [Ordering] ASC);


GO
CREATE NONCLUSTERED INDEX [Idx_StatusNameFleetId]
    ON [dbo].[ecar_fleet_servicearea_polygon]([Status] ASC, [Name] ASC, [FleetId] ASC, [Ordering] ASC);


GO
CREATE NONCLUSTERED INDEX [IdxFleetIDStatustypeName]
    ON [dbo].[ecar_fleet_servicearea_polygon]([FleetId] ASC, [Status] ASC, [Type] ASC, [Name] ASC);

