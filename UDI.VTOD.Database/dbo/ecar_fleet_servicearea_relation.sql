﻿CREATE TABLE [dbo].[ecar_fleet_servicearea_relation] (
    [Id]                 BIGINT         IDENTITY (14, 1) NOT NULL,
    [FleetId]            BIGINT         NOT NULL,
    [Status]             BIT            CONSTRAINT [DF__ecar_flee__Statu__16CE6296] DEFAULT ((1)) NOT NULL,
    [ConsumerCode]       NVARCHAR (50)  NOT NULL,
    [DispatchSystemCode] NVARCHAR (50)  NOT NULL,
    [DispatchServiceId]  NVARCHAR (250) DEFAULT (NULL) NULL,
    [Description]        NVARCHAR (100) DEFAULT (NULL) NULL,
    [Ordering]           INT            NOT NULL,
    [AppendTime]         DATETIME       DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ecar_fleet_servicearea_relation_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [ecar_fleet_servicearea_relation$ecar_fleet_servicearea_relation_ibfk_1] FOREIGN KEY ([FleetId]) REFERENCES [dbo].[ecar_fleet] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IdxFleetIDStatusConsumerCodeDispatchsystemCode]
    ON [dbo].[ecar_fleet_servicearea_relation]([FleetId] ASC, [Status] ASC, [ConsumerCode] ASC, [DispatchSystemCode] ASC);

