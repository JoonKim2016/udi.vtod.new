﻿CREATE TABLE [dbo].[ecar_fleet_servicearea_zip] (
    [Id]         BIGINT        IDENTITY (1076, 1) NOT NULL,
    [FleetId]    BIGINT        NOT NULL,
    [Status]     BIT           CONSTRAINT [DF__ecar_flee__Statu__1A9EF37A] DEFAULT ((1)) NOT NULL,
    [Zip]        NVARCHAR (10) DEFAULT (NULL) NULL,
    [Country]    NVARCHAR (50) DEFAULT (NULL) NULL,
    [Ordering]   INT           NOT NULL,
    [AppendTime] DATETIME      DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ecar_fleet_servicearea_zip_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [ecar_fleet_servicearea_zip$ecar_fleet_servicearea_zip_ibfk_1] FOREIGN KEY ([FleetId]) REFERENCES [dbo].[ecar_fleet] ([Id]),
    CONSTRAINT [ecar_fleet_servicearea_zip$Idx_ServiceAreaZip] UNIQUE NONCLUSTERED ([FleetId] ASC, [Zip] ASC, [Country] ASC, [Ordering] ASC)
);


GO
CREATE NONCLUSTERED INDEX [Idx_ECarStatusZipCountry]
    ON [dbo].[ecar_fleet_servicearea_zip]([Status] ASC, [Zip] ASC, [Country] ASC, [Ordering] ASC);


GO
CREATE NONCLUSTERED INDEX [IdxFleetIDStatusZipCountry]
    ON [dbo].[ecar_fleet_servicearea_zip]([FleetId] ASC, [Status] ASC, [Zip] ASC, [Country] ASC);

