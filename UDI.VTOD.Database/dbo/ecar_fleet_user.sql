﻿CREATE TABLE [dbo].[ecar_fleet_user] (
    [Id]            BIGINT        IDENTITY (9, 1) NOT NULL,
    [FleetId]       BIGINT        NOT NULL,
    [UserId]        INT           NOT NULL,
    [AccountNumber] NVARCHAR (50) DEFAULT (NULL) NULL,
    [AppendTime]    DATETIME      DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ecar_fleet_user_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [ecar_fleet_user$ecar_fleet_user_ibfk_1] FOREIGN KEY ([FleetId]) REFERENCES [dbo].[ecar_fleet] ([Id]),
    CONSTRAINT [ecar_fleet_user$ecar_fleet_user_ibfk_2] FOREIGN KEY ([UserId]) REFERENCES [dbo].[my_aspnet_users] ([id])
);


GO
CREATE NONCLUSTERED INDEX [IdxFleetUserFleetIDUserId]
    ON [dbo].[ecar_fleet_user]([FleetId] ASC, [UserId] ASC);


GO
CREATE NONCLUSTERED INDEX [UserId]
    ON [dbo].[ecar_fleet_user]([UserId] ASC);

