﻿CREATE TABLE [dbo].[ecar_forbiddencancelstatus] (
    [Id]         BIGINT        IDENTITY (1, 1) NOT NULL,
    [Status]     NVARCHAR (50) NOT NULL,
    [AppendTime] DATETIME      DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ecar_forbiddencancelstatus_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [Idx_Status]
    ON [dbo].[ecar_forbiddencancelstatus]([Status] ASC);

