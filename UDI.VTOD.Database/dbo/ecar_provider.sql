﻿CREATE TABLE [dbo].[ecar_provider] (
    [Id]         BIGINT        IDENTITY (3, 1) NOT NULL,
    [Name]       NVARCHAR (40) DEFAULT (NULL) NULL,
    [AppendTime] DATETIME      DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ecar_provider_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);

