﻿CREATE TABLE [dbo].[ecar_serviceapi] (
    [Id]         BIGINT        IDENTITY (3, 1) NOT NULL,
    [Name]       NVARCHAR (40) DEFAULT (NULL) NULL,
    [AppendTime] DATETIME      DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ecar_serviceapi_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);

