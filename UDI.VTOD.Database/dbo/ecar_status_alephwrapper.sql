﻿CREATE TABLE [dbo].[ecar_status_alephwrapper] (
    [Id]         BIGINT        IDENTITY (13, 1) NOT NULL,
    [Source]     NVARCHAR (50) NOT NULL,
    [Target]     NVARCHAR (50) NOT NULL,
    [AppendTime] DATETIME      DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ecar_status_alephwrapper_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IdxSource]
    ON [dbo].[ecar_status_alephwrapper]([Source] ASC);

