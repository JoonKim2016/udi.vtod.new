﻿CREATE TABLE [dbo].[ecar_status_frontendwrapper] (
    [Id]             BIGINT        IDENTITY (23, 1) NOT NULL,
    [UserId]         INT           NOT NULL,
    [VTODECarStatus] NVARCHAR (50) NOT NULL,
    [FrontEndStatus] NVARCHAR (50) NOT NULL,
    [AppendTime]     DATETIME      DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ecar_status_frontendwrapper_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IdxUserIdVTODECarStatus]
    ON [dbo].[ecar_status_frontendwrapper]([UserId] ASC, [VTODECarStatus] ASC);

