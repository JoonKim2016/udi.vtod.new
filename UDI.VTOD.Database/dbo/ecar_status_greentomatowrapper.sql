﻿CREATE TABLE [dbo].[ecar_status_greentomatowrapper] (
    [Id]         BIGINT        IDENTITY (11, 1) NOT NULL,
    [Source]     NVARCHAR (50) NOT NULL,
    [Target]     NVARCHAR (50) NOT NULL,
    [AppendTime] DATETIME      DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ecar_status_greentomatowrapper_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IdxSource]
    ON [dbo].[ecar_status_greentomatowrapper]([Source] ASC);

