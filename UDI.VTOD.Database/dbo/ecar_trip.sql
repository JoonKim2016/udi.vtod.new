﻿CREATE TABLE [dbo].[ecar_trip] (
    [Id]                     BIGINT         NOT NULL,
    [FirstName]              NVARCHAR (100) NOT NULL,
    [LastName]               NVARCHAR (100) NOT NULL,
    [PhoneNumber]            NVARCHAR (20)  NOT NULL,
    [FleetId]                BIGINT         NOT NULL,
    [ServiceAPIId]           BIGINT         NOT NULL,
    [APIInformation]         NVARCHAR (100) DEFAULT (NULL) NULL,
    [ECarNumber]             NVARCHAR (20)  DEFAULT (NULL) NULL,
    [MinutesAway]            INT            DEFAULT (NULL) NULL,
    [Expires]                DATETIME2 (0)  DEFAULT (NULL) NULL,
    [NumberOfPassenger]      INT            DEFAULT (NULL) NULL,
    [DriverNotes]            NVARCHAR (400) DEFAULT (NULL) NULL,
    [AccountNumber]          NVARCHAR (50)  DEFAULT (NULL) NULL,
    [PickupAddressType]      NVARCHAR (20)  DEFAULT (NULL) NULL,
    [PickupLongitude]        FLOAT (53)     DEFAULT (NULL) NULL,
    [PickupLatitude]         FLOAT (53)     DEFAULT (NULL) NULL,
    [PickupBuilding]         NVARCHAR (30)  DEFAULT (NULL) NULL,
    [PickupStreetNo]         NVARCHAR (20)  DEFAULT (NULL) NULL,
    [PickupStreetName]       NVARCHAR (50)  DEFAULT (NULL) NULL,
    [PickupStreetType]       NVARCHAR (20)  DEFAULT (NULL) NULL,
    [PickupAptNo]            NVARCHAR (15)  DEFAULT (NULL) NULL,
    [PickupCity]             NVARCHAR (30)  DEFAULT (NULL) NULL,
    [PickupStateCode]        NVARCHAR (10)  DEFAULT (NULL) NULL,
    [PickupZipCode]          NVARCHAR (20)  DEFAULT (NULL) NULL,
    [PickupCountryCode]      NVARCHAR (10)  DEFAULT (NULL) NULL,
    [PickupFullAddress]      NVARCHAR (200) DEFAULT (NULL) NULL,
    [PickupAirport]          NVARCHAR (100) DEFAULT (NULL) NULL,
    [DropOffAddressType]     NVARCHAR (20)  DEFAULT (NULL) NULL,
    [DropOffLongitude]       FLOAT (53)     DEFAULT (NULL) NULL,
    [DropOffLatitude]        FLOAT (53)     DEFAULT (NULL) NULL,
    [DropOffBuilding]        NVARCHAR (30)  DEFAULT (NULL) NULL,
    [DropOffStreetNo]        NVARCHAR (20)  DEFAULT (NULL) NULL,
    [DropOffStreetName]      NVARCHAR (50)  DEFAULT (NULL) NULL,
    [DropOffStreetType]      NVARCHAR (20)  DEFAULT (NULL) NULL,
    [DropOffAptNo]           NVARCHAR (15)  DEFAULT (NULL) NULL,
    [DropOffCity]            NVARCHAR (30)  DEFAULT (NULL) NULL,
    [DropOffStateCode]       NVARCHAR (10)  DEFAULT (NULL) NULL,
    [DropOffZipCode]         NVARCHAR (20)  DEFAULT (NULL) NULL,
    [DropOffCountryCode]     NVARCHAR (10)  DEFAULT (NULL) NULL,
    [DropOffFullAddress]     NVARCHAR (200) DEFAULT (NULL) NULL,
    [DropOffAirport]         NVARCHAR (100) DEFAULT (NULL) NULL,
    [DispatchTripId]         NVARCHAR (40)  DEFAULT (NULL) NULL,
    [ConsumerConfirmationId] NVARCHAR (40)  DEFAULT (NULL) NULL,
    [DriverGivenName]        NVARCHAR (50)  DEFAULT (NULL) NULL,
    [DriverSurName]          NVARCHAR (50)  DEFAULT (NULL) NULL,
    [ApplicationName]        NVARCHAR (50)  DEFAULT (NULL) NULL,
    [Device]                 NVARCHAR (50)  DEFAULT (NULL) NULL,
    [RefernceNumber]         NVARCHAR (50)  DEFAULT (NULL) NULL,
    [RemarkForPickup]        NVARCHAR (200) DEFAULT (NULL) NULL,
    [RemarkForDropOff]       NVARCHAR (200) DEFAULT (NULL) NULL,
    CONSTRAINT [PK_ecar_trip_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [ecar_trip$ecar_fleet_Id_ibfk_1] FOREIGN KEY ([FleetId]) REFERENCES [dbo].[ecar_fleet] ([Id]),
    CONSTRAINT [ecar_trip$ecar_ServiceAPIId_ibfk_1] FOREIGN KEY ([ServiceAPIId]) REFERENCES [dbo].[ecar_serviceapi] ([Id]),
    CONSTRAINT [ecar_trip$ecar_trip_Id_vtodtrip_1] FOREIGN KEY ([Id]) REFERENCES [dbo].[vtod_trip] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [ecar_fleet_Id_ibfk_1]
    ON [dbo].[ecar_trip]([FleetId] ASC);


GO
CREATE NONCLUSTERED INDEX [ecar_ServiceAPIId_ibfk_1]
    ON [dbo].[ecar_trip]([ServiceAPIId] ASC);


GO
CREATE NONCLUSTERED INDEX [IdxFirstNameLastNamePhoneNumber]
    ON [dbo].[ecar_trip]([FirstName] ASC, [LastName] ASC, [PhoneNumber] ASC);

