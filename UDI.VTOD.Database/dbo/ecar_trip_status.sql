﻿CREATE TABLE [dbo].[ecar_trip_status] (
    [Id]               BIGINT          IDENTITY (245, 1) NOT NULL,
    [TripID]           BIGINT          NOT NULL,
    [Status]           NVARCHAR (50)   NOT NULL,
    [StatusTime]       DATETIME        DEFAULT (NULL) NULL,
    [VehicleNumber]    NVARCHAR (50)   DEFAULT (NULL) NULL,
    [VehicleLongitude] DECIMAL (18, 6) DEFAULT (NULL) NULL,
    [VehicleLatitude]  DECIMAL (18, 6) DEFAULT (NULL) NULL,
    [DriverName]       NVARCHAR (50)   DEFAULT (NULL) NULL,
    [DriverId]         NVARCHAR (50)   DEFAULT (NULL) NULL,
    [ETA]              INT             DEFAULT (NULL) NULL,
    [Fare]             DECIMAL (10, 2) DEFAULT (NULL) NULL,
    [CommentForStatus] NVARCHAR (50)   DEFAULT (NULL) NULL,
    [OriginalStatus]   NVARCHAR (50)   DEFAULT (N'Unknown') NOT NULL,
    [AppendTime]       DATETIME        DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ecar_trip_status_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [ecar_trip_status$ecar_trip_status_ibfk_1] FOREIGN KEY ([TripID]) REFERENCES [dbo].[vtod_trip] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IdxECarTripStatus]
    ON [dbo].[ecar_trip_status]([TripID] ASC, [Status] ASC, [VehicleNumber] ASC, [VehicleLatitude] ASC, [VehicleLongitude] ASC, [ETA] ASC, [Fare] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AppendTime]
    ON [dbo].[ecar_trip_status]([AppendTime] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Status]
    ON [dbo].[ecar_trip_status]([Status] ASC);


GO
CREATE NONCLUSTERED INDEX [TripID]
    ON [dbo].[ecar_trip_status]([TripID] ASC);

