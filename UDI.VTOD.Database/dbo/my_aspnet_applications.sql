﻿CREATE TABLE [dbo].[my_aspnet_applications] (
    [id]          INT            IDENTITY (2, 1) NOT NULL,
    [name]        NVARCHAR (256) DEFAULT (NULL) NULL,
    [description] NVARCHAR (256) DEFAULT (NULL) NULL,
    CONSTRAINT [PK_my_aspnet_applications_id] PRIMARY KEY CLUSTERED ([id] ASC)
);

