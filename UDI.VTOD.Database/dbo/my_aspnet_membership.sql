﻿CREATE TABLE [dbo].[my_aspnet_membership] (
    [userId]                                 INT            DEFAULT ((0)) NOT NULL,
    [Email]                                  NVARCHAR (128) DEFAULT (NULL) NULL,
    [Comment]                                NVARCHAR (255) DEFAULT (NULL) NULL,
    [Password]                               NVARCHAR (128) NOT NULL,
    [PasswordKey]                            NCHAR (32)     DEFAULT (NULL) NULL,
    [PasswordFormat]                         SMALLINT       DEFAULT (NULL) NULL,
    [PasswordQuestion]                       NVARCHAR (255) DEFAULT (NULL) NULL,
    [PasswordAnswer]                         NVARCHAR (255) DEFAULT (NULL) NULL,
    [IsApproved]                             SMALLINT       DEFAULT (NULL) NULL,
    [LastActivityDate]                       DATETIME2 (0)  DEFAULT (NULL) NULL,
    [LastLoginDate]                          DATETIME2 (0)  DEFAULT (NULL) NULL,
    [LastPasswordChangedDate]                DATETIME2 (0)  DEFAULT (NULL) NULL,
    [CreationDate]                           DATETIME2 (0)  DEFAULT (NULL) NULL,
    [IsLockedOut]                            SMALLINT       DEFAULT (NULL) NULL,
    [LastLockedOutDate]                      DATETIME2 (0)  DEFAULT (NULL) NULL,
    [FailedPasswordAttemptCount]             BIGINT         DEFAULT (NULL) NULL,
    [FailedPasswordAttemptWindowStart]       DATETIME2 (0)  DEFAULT (NULL) NULL,
    [FailedPasswordAnswerAttemptCount]       BIGINT         DEFAULT (NULL) NULL,
    [FailedPasswordAnswerAttemptWindowStart] DATETIME2 (0)  DEFAULT (NULL) NULL,
    CONSTRAINT [PK_my_aspnet_membership_userId] PRIMARY KEY CLUSTERED ([userId] ASC)
);

