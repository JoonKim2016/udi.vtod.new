﻿CREATE TABLE [dbo].[my_aspnet_profiles] (
    [userId]          INT             NOT NULL,
    [valueindex]      NVARCHAR (MAX)  NULL,
    [stringdata]      NVARCHAR (MAX)  NULL,
    [binarydata]      VARBINARY (MAX) NULL,
    [lastUpdatedDate] DATETIME        DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_my_aspnet_profiles_userId] PRIMARY KEY CLUSTERED ([userId] ASC)
);

