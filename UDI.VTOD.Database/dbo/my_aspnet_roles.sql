﻿CREATE TABLE [dbo].[my_aspnet_roles] (
    [id]            INT            IDENTITY (33, 1) NOT NULL,
    [applicationId] INT            NOT NULL,
    [name]          NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_my_aspnet_roles_id] PRIMARY KEY CLUSTERED ([id] ASC)
);

