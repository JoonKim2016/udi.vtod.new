﻿CREATE TABLE [dbo].[my_aspnet_schemaversion] (
    [version] INT DEFAULT (NULL) NULL
);


GO
CREATE CLUSTERED INDEX [schemaversion]
    ON [dbo].[my_aspnet_schemaversion]([version] ASC);

