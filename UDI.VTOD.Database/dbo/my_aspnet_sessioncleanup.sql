﻿CREATE TABLE [dbo].[my_aspnet_sessioncleanup] (
    [LastRun]         DATETIME2 (0) NOT NULL,
    [IntervalMinutes] INT           NOT NULL,
    [ApplicationId]   INT           NOT NULL,
    CONSTRAINT [PK_my_aspnet_sessioncleanup_ApplicationId] PRIMARY KEY CLUSTERED ([ApplicationId] ASC)
);

