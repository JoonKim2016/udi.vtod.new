﻿CREATE TABLE [dbo].[my_aspnet_sessions] (
    [SessionId]     NVARCHAR (191)  NOT NULL,
    [ApplicationId] INT             NOT NULL,
    [Created]       DATETIME2 (0)   NOT NULL,
    [Expires]       DATETIME2 (0)   NOT NULL,
    [LockDate]      DATETIME2 (0)   NOT NULL,
    [LockId]        INT             NOT NULL,
    [Timeout]       INT             NOT NULL,
    [Locked]        SMALLINT        NOT NULL,
    [SessionItems]  VARBINARY (MAX) NULL,
    [Flags]         INT             NOT NULL,
    CONSTRAINT [PK_my_aspnet_sessions_SessionId] PRIMARY KEY CLUSTERED ([SessionId] ASC, [ApplicationId] ASC)
);

