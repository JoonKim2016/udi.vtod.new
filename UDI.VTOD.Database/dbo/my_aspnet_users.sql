﻿CREATE TABLE [dbo].[my_aspnet_users] (
    [id]               INT            IDENTITY (1, 1) NOT NULL,
    [applicationId]    INT            NOT NULL,
    [name]             NVARCHAR (256) NOT NULL,
    [isAnonymous]      SMALLINT       CONSTRAINT [DF__my_aspnet__isAno__7720AD13] DEFAULT ((1)) NOT NULL,
    [lastActivityDate] DATETIME2 (0)  CONSTRAINT [DF__my_aspnet__lastA__7814D14C] DEFAULT (NULL) NULL,
    CONSTRAINT [PK_my_aspnet_users_id] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [Idx_Name]
    ON [dbo].[my_aspnet_users]([name] ASC);

