﻿CREATE TABLE [dbo].[my_aspnet_usersinroles] (
    [userId] INT DEFAULT ((0)) NOT NULL,
    [roleId] INT DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_my_aspnet_usersinroles_userId] PRIMARY KEY CLUSTERED ([userId] ASC, [roleId] ASC)
);

