﻿CREATE TABLE [dbo].[sds_status_frontendwrapper] (
    [Id]             BIGINT        IDENTITY (166, 1) NOT NULL,
    [UserId]         INT           NOT NULL,
    [VTODSDSStatus]  NVARCHAR (50) NOT NULL,
    [FrontEndStatus] NVARCHAR (50) NOT NULL,
    [AppendTime]     DATETIME      DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_sds_status_frontendwrapper_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [sds_status_frontendwrapper$sds_status_frontendwrapper_ibfk_1] FOREIGN KEY ([UserId]) REFERENCES [dbo].[my_aspnet_users] ([id])
);


GO
CREATE NONCLUSTERED INDEX [index2]
    ON [dbo].[sds_status_frontendwrapper]([UserId] ASC);

