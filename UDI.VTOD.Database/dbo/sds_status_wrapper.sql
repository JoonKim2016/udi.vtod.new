﻿CREATE TABLE [dbo].[sds_status_wrapper] (
    [Id]         BIGINT        IDENTITY (106, 1) NOT NULL,
    [Source]     NVARCHAR (50) NOT NULL,
    [Target]     NVARCHAR (50) NOT NULL,
    [AppendTime] DATETIME      DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_sds_status_wrapper_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [Idx_Source_Target]
    ON [dbo].[sds_status_wrapper]([Source] ASC);

