﻿CREATE TABLE [dbo].[sds_user_subfleet] (
    [id]       BIGINT        IDENTITY (4, 1) NOT NULL,
    [userId]   INT           NOT NULL,
    [subfleet] NVARCHAR (50) NOT NULL,
    [Allow]    BIT           NOT NULL,
    CONSTRAINT [PK_sds_user_subfleet_id] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [sds_user_subfleet$sds_user_subfleet_ibfk_1] FOREIGN KEY ([userId]) REFERENCES [dbo].[my_aspnet_users] ([id])
);


GO
CREATE NONCLUSTERED INDEX [ix_UserID]
    ON [dbo].[sds_user_subfleet]([userId] ASC);

