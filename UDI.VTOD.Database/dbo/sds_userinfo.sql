﻿CREATE TABLE [dbo].[sds_userinfo] (
    [Id]                 BIGINT        IDENTITY (1, 1) NOT NULL,
    [UserName]           NVARCHAR (50) NOT NULL,
    [RezSource]          NVARCHAR (45) CONSTRAINT [DF__sds_useri__RezSo__0D0FEE32] DEFAULT (NULL) NULL,
    [AllowClosedSplits]  BIT           CONSTRAINT [DF__sds_useri__Allow__0E04126B] DEFAULT ((0)) NOT NULL,
    [ShowSplits]         BIT           CONSTRAINT [DF__sds_useri__ShowS__0EF836A4] DEFAULT ((0)) NOT NULL,
    [PickupTimeApproach] INT           CONSTRAINT [DF__sds_useri__Picku__0FEC5ADD] DEFAULT ((1)) NOT NULL,
    [AppendTime]         DATETIME2 (0) NOT NULL,
    [ModifyTime]         DATETIME2 (0) NOT NULL,
    CONSTRAINT [PK_sds_userinfo_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_username]
    ON [dbo].[sds_userinfo]([UserName] ASC);

