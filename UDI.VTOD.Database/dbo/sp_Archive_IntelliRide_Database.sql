﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE sp_Archive_IntelliRide_Database
	
	  @days_threshold int
	, @batch_size int
AS
BEGIN
SET NOCOUNT ON

	IF ISNULL(@days_threshold, 0) < 60	
		BEGIN
			SET @days_threshold =  60	
		END
	
	IF ISNULL(@batch_size, 0) <  100
		BEGIN
			SET @batch_size = 100 
		END

Declare @BackupDbName nvarchar(200) 
DECLARE	@FromDate datetime
declare @TripId bigint
Set @BackupDbName='E:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\Backup\IntelliRide' + Cast(GETDATE() as varchar(50)) + '.bak'

select @BackupDbName

SET @FromDate = DATEADD(day, -@days_threshold, GETDATE())
select @FromDate

  -- Insert statements for procedure here
 BACKUP DATABASE IntelliRide 
TO DISK = @BackupDbName

select TOP 1 @TripId =  tripid from [IntelliRide].[Status] where AppendTime < @FromDate
order by tripid desc

delete [IntelliRide].[Status] where tripid < @TripId


delete [Trapeze].[Event] where [TripID] < @TripId

delete [Trapeze].[Trip] where id < @TripId

delete [MC].[BatchTripHistory] where [TripID] < @TripId

delete [IntelliRide].[Trip] where id < @TripId

truncate table [IntelliRideLog].[CCSi].[StatusLog];
truncate table [IntelliRideLog].[dbo].[Errors];
truncate table [IntelliRideLog].[dbo].[Log];
	
END
