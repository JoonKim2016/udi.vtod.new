﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_vtod_TripReminder]
	@memberID bigint
AS
BEGIN
	 DECLARE @getMembershipTable TABLE (ID bigint,ConfirmationNumber varchar(100) null,DispatchTripId varchar(500) null,
	 FleetTripCode bigint null,LastName nvarchar(100) null,PhoneNumber nvarchar(100) null,PickMeUpNow bit,PickupLocation nvarchar(200) null,
	 PickupCity nvarchar(200) null,PickupStateCode nvarchar(200) null,PickupZipCode nvarchar(200) null,PickupFullAddress nvarchar(200) null,
	 PickupBuilding nvarchar(200) null,FleetId nvarchar(10) null,DropoffLocation nvarchar(200) null,DropOffFullAddress nvarchar(200) null,
	 DropOffCity nvarchar(200) null, DropOffStateCode nvarchar(200) null,DropOffZipCode nvarchar(200) null,PickupDateTime datetime2(7) null,
	 FinalStatus nvarchar(200) null,UserID nvarchar(200) null,MemberID nvarchar(200) null,DropOffBuilding nvarchar(200) null)

	 insert into @getMembershipTable(ID,FinalStatus,PickMeUpNow,PickupDateTime,UserID,MemberID,FleetTripCode) 
	 Select 
	 [dbo].[vtod_trip].Id,[dbo].[vtod_trip].FinalStatus,
	 [dbo].[vtod_trip].PickMeUpNow,
	 [dbo].[vtod_trip].PickupDateTime,[dbo].[vtod_trip].UserID,[dbo].[vtod_trip].MemberID,[dbo].[vtod_trip].FleetTripCode
	 from dbo.vtod_trip
	 WHERE   dbo.vtod_trip.MemberID=@memberID
	and  dbo.vtod_trip.PickupDateTimeUTC  between GETUTCDATE() and  DateAdd(minute,10,GETUTCDATE())   
	and ISNULL(LOWER([dbo].[vtod_trip].FinalStatus),'') NOT IN ('completed','canceled','noshow','nodriveravailable')
	and [dbo].[vtod_trip].PickMeUpNow=0


	 



	 UPDATE   t1 
   SET t1.PickupLocation = t2.PickupLocation,
    t1.PickupFullAddress = t2.PickupLocation,
   t1.PickupBuilding='',
   t1.PickupCity='',
  t1.PickupZipCode='',
  t1.PickupStateCode='',
  t1.DropoffLocation=t2.DropoffLocation,
   t1.DropOffFullAddress=t2.DropoffLocation,
  t1.DropoffBuilding='',
  t1.DropoffCity='',
  t1.DropoffZipCode='',
  t1.DispatchTripId='',
  t1.ConfirmationNumber=t2.ConfirmationNumber,
  t1.LastName=t2.LastName,
  t1.PhoneNumber=t2.[ContactNumberDialingPrefix]+ t2.[ContactNumber]
  FROM @getMembershipTable  t1
  INNER JOIN dbo.sds_trip  t2
  ON t1.Id = t2.Id


  
	 UPDATE   t1 
  SET t1.PickupLocation = t2.PickupFullAddress,t1.PickupBuilding=t2.PickupBuilding,t1.PickupCity=t2.PickupCity,
  t1.PickupZipCode=t2.PickupZipCode,
   t1.PickupFullAddress = t2.PickupFullAddress,
  t1.PickupStateCode=t2.PickupStateCode,
  t1.DropoffLocation=t2.DropoffFullAddress,
  t1.DropOffFullAddress=t2.DropOffFullAddress,
  t1.DropoffBuilding=t2.DropoffBuilding,
  t1.DropoffCity=t1.DropoffCity,
  t1.DropoffZipCode=t2.DropoffZipCode,
  t1.DispatchTripId=t2.DispatchTripId,
  t1.ConfirmationNumber='',
  t1.LastName=t2.LastName,
   t1.PhoneNumber=t2.[PhoneNumber]
  FROM @getMembershipTable  t1
  INNER JOIN dbo.ecar_trip  t2
  ON t1.Id = t2.Id


   UPDATE   t1 
  SET t1.PickupLocation = t2.PickupFullAddress,t1.PickupBuilding=t2.PickupBuilding,t1.PickupCity=t2.PickupCity,
  t1.PickupZipCode=t2.PickupZipCode,
   t1.PickupFullAddress = t2.PickupFullAddress,
  t1.PickupStateCode=t2.PickupStateCode,
  t1.DropoffLocation=t2.DropOffFullAddress,
   t1.DropOffFullAddress=t2.DropOffFullAddress,
  t1.DropoffBuilding=t2.DropoffBuilding,
  t1.DropoffCity=t1.DropoffCity,
  t1.DropoffZipCode=t2.DropoffZipCode,
  t1.DispatchTripId=t2.DispatchTripId,
  t1.ConfirmationNumber='',
  t1.PhoneNumber=t2.[PhoneNumber],
  t1.LastName=t2.LastName
  FROM @getMembershipTable  t1
  INNER JOIN dbo.taxi_trip  t2
  ON t1.Id = t2.Id


    UPDATE   t1 
  SET t1.PickupLocation = t2.PickupFullAddress,t1.PickupBuilding=t2.PickupBuilding,t1.PickupCity=t2.PickupCity,
  t1.PickupZipCode=t2.PickupZipCode,
   t1.PickupFullAddress = t2.PickupFullAddress,
  t1.PickupStateCode=t2.PickupStateCode,
  t1.DropoffLocation=t2.DropOffFullAddress,
   t1.DropOffFullAddress=t2.DropOffFullAddress,
  t1.DropoffBuilding=t2.DropoffBuilding,
  t1.DropoffCity=t1.DropoffCity,
  t1.DropoffZipCode=t2.DropoffZipCode,
  t1.DispatchTripId=t2.DispatchTripId,
  t1.ConfirmationNumber='',
  t1.PhoneNumber=t2.[PhoneNumber],
  t1.LastName=t2.LastName
  FROM @getMembershipTable  t1
  INNER JOIN dbo.van_trip  t2
  ON t1.Id = t2.Id

	select * from @getMembershipTable
END
