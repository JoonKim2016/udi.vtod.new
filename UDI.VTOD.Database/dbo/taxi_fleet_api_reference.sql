﻿CREATE TABLE [dbo].[taxi_fleet_api_reference] (
    [Id]                   BIGINT        IDENTITY (73, 1) NOT NULL,
    [FleetId]              BIGINT        NOT NULL,
    [ServiceAPIId]         BIGINT        NOT NULL,
    [ExtendedServiceAPIId] BIGINT        DEFAULT (NULL) NULL,
    [Name]                 NVARCHAR (40) DEFAULT (NULL) NULL,
    [AppendTime]           DATETIME      DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_taxi_fleet_api_reference_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [taxi_fleet_api_reference$taxi_fleet_api_reference_ibfk_1] FOREIGN KEY ([FleetId]) REFERENCES [dbo].[taxi_fleet] ([Id]),
    CONSTRAINT [taxi_fleet_api_reference$taxi_fleet_api_reference_ibfk_2] FOREIGN KEY ([ServiceAPIId]) REFERENCES [dbo].[taxi_serviceapi] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [Idx_fleetID]
    ON [dbo].[taxi_fleet_api_reference]([FleetId] ASC);


GO
CREATE NONCLUSTERED INDEX [IdxFleetIDServiceAPIID]
    ON [dbo].[taxi_fleet_api_reference]([FleetId] ASC, [ServiceAPIId] ASC);


GO
CREATE NONCLUSTERED INDEX [taxi_fleet_serviceapipreference_ibfk_2]
    ON [dbo].[taxi_fleet_api_reference]([ServiceAPIId] ASC);

