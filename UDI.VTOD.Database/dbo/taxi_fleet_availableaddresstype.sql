﻿CREATE TABLE [dbo].[taxi_fleet_availableaddresstype] (
    [Id]               BIGINT   IDENTITY (13, 1) NOT NULL,
    [FleetId]          BIGINT   NOT NULL,
    [AddressToAddress] BIT      CONSTRAINT [DF__taxi_flee__Addre__2C88998B] DEFAULT ((1)) NOT NULL,
    [AddressToAirport] BIT      CONSTRAINT [DF__taxi_flee__Addre__2D7CBDC4] DEFAULT ((0)) NOT NULL,
    [AddressToNull]    BIT      CONSTRAINT [DF__taxi_flee__Addre__2E70E1FD] DEFAULT ((1)) NOT NULL,
    [AirportToAddress] BIT      CONSTRAINT [DF__taxi_flee__Airpo__2F650636] DEFAULT ((1)) NOT NULL,
    [AirportToAirport] BIT      CONSTRAINT [DF__taxi_flee__Airpo__30592A6F] DEFAULT ((0)) NOT NULL,
    [AirportToNull]    BIT      CONSTRAINT [DF__taxi_flee__Airpo__314D4EA8] DEFAULT ((1)) NOT NULL,
    [AppendTime]       DATETIME DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_taxi_fleet_availableaddresstype_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IdxFleet]
    ON [dbo].[taxi_fleet_availableaddresstype]([FleetId] ASC);

