﻿CREATE TABLE [dbo].[taxi_fleet_blackout] (
    [Id]                          INT      IDENTITY (1, 1) NOT NULL,
    [FleetId]                     BIGINT   NOT NULL,
    [BlackoutStartFleetLocalTime] DATETIME NULL,
    [BlackoutEndFleetLocalTime]   DATETIME NULL,
    CONSTRAINT [PK_taxi_fleet_blackout] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [AK_taxi_fleet_blackout_Default] UNIQUE NONCLUSTERED ([FleetId] ASC)
	)