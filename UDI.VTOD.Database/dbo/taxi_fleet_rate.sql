﻿CREATE TABLE [dbo].[taxi_fleet_rate] (
    [Id]                  BIGINT        IDENTITY (16, 1) NOT NULL,
    [FleetId]             BIGINT        NOT NULL,
    [InitialFreeDistance] FLOAT (53)    DEFAULT ((0)) NOT NULL,
    [InitialAmount]       FLOAT (53)    DEFAULT ((0)) NOT NULL,
    [PerDistanceAmount]   FLOAT (53)    DEFAULT ((0)) NOT NULL,
    [DistanceUnit]        NVARCHAR (10) DEFAULT (N'Mile') NOT NULL,
    [AmountUnit]          NVARCHAR (3)  DEFAULT (N'USD') NOT NULL,
    [AppendTime]          DATETIME      DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_taxi_fleet_rate_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [taxi_fleet_rate$FK_Fleet_ID] FOREIGN KEY ([FleetId]) REFERENCES [dbo].[taxi_fleet] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [Idx_FleetId]
    ON [dbo].[taxi_fleet_rate]([FleetId] ASC);

