﻿CREATE TABLE [dbo].[taxi_fleet_servicearea_city] (
    [Id]         BIGINT         IDENTITY (895, 1) NOT NULL,
    [FleetId]    BIGINT         NOT NULL,
    [Status]     BIT            CONSTRAINT [DF__taxi_flee__Statu__38EE7070] DEFAULT ((1)) NOT NULL,
    [City]       NVARCHAR (100) DEFAULT (NULL) NULL,
    [State]      NVARCHAR (50)  DEFAULT (NULL) NULL,
    [Country]    NVARCHAR (50)  DEFAULT (NULL) NULL,
    [Ordering]   INT            NOT NULL,
    [AppendTime] DATETIME       DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_taxi_fleet_servicearea_city_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [taxi_fleet_servicearea_city$taxi_fleet_servicearea_city_ibfk_1] FOREIGN KEY ([FleetId]) REFERENCES [dbo].[taxi_fleet] ([Id]),
    CONSTRAINT [taxi_fleet_servicearea_city$Idx_ServiceAreaCity] UNIQUE NONCLUSTERED ([FleetId] ASC, [City] ASC, [State] ASC, [Country] ASC, [Ordering] ASC)
);


GO
CREATE NONCLUSTERED INDEX [Idx_StatusCityStateCountry]
    ON [dbo].[taxi_fleet_servicearea_city]([Status] ASC, [City] ASC, [State] ASC, [Country] ASC, [Ordering] ASC);

