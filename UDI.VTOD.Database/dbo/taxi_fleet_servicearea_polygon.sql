﻿CREATE TABLE [dbo].[taxi_fleet_servicearea_polygon] (
    [Id]         BIGINT         IDENTITY (1, 1) NOT NULL,
    [FleetId]    BIGINT         NOT NULL,
    [Status]     BIT            CONSTRAINT [DF__taxi_flee__Statu__3DB3258D] DEFAULT ((1)) NOT NULL,
    [Type]       NVARCHAR (20)  CONSTRAINT [DF__taxi_fleet__Type__3EA749C6] DEFAULT (N'Area') NOT NULL,
    [Name]       NVARCHAR (100) NOT NULL,
    [PolygonId]  BIGINT         NOT NULL,
    [Ordering]   INT            NOT NULL,
    [AppendTime] DATETIME       CONSTRAINT [DF__taxi_flee__Appen__3F9B6DFF] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_taxi_fleet_servicearea_polygon_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [taxi_fleet_servicearea_polygon$taxi_fleet_servicearea_polygon_ibfk_1] FOREIGN KEY ([FleetId]) REFERENCES [dbo].[taxi_fleet] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [FleetId]
    ON [dbo].[taxi_fleet_servicearea_polygon]([FleetId] ASC);


GO
CREATE NONCLUSTERED INDEX [Idx_StatusName]
    ON [dbo].[taxi_fleet_servicearea_polygon]([Status] ASC, [Name] ASC, [Ordering] ASC);


GO
CREATE NONCLUSTERED INDEX [Idx_StatusNameFleetId]
    ON [dbo].[taxi_fleet_servicearea_polygon]([Status] ASC, [Name] ASC, [FleetId] ASC, [Ordering] ASC);

