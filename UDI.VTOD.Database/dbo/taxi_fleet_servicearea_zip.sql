﻿CREATE TABLE [dbo].[taxi_fleet_servicearea_zip] (
    [Id]         BIGINT        IDENTITY (660, 1) NOT NULL,
    [FleetId]    BIGINT        NOT NULL,
    [Status]     BIT           CONSTRAINT [DF__taxi_flee__Statu__408F9238] DEFAULT ((1)) NOT NULL,
    [Zip]        NVARCHAR (10) DEFAULT (NULL) NULL,
    [Country]    NVARCHAR (50) DEFAULT (NULL) NULL,
    [Ordering]   INT           NOT NULL,
    [AppendTime] DATETIME      DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_taxi_fleet_servicearea_zip_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [taxi_fleet_servicearea_zip$taxi_fleet_servicearea_zip_ibfk_1] FOREIGN KEY ([FleetId]) REFERENCES [dbo].[taxi_fleet] ([Id]),
    CONSTRAINT [taxi_fleet_servicearea_zip$Idx_ServiceAreaZip] UNIQUE NONCLUSTERED ([FleetId] ASC, [Zip] ASC, [Country] ASC, [Ordering] ASC)
);


GO
CREATE NONCLUSTERED INDEX [Idx_StatusZipCountry]
    ON [dbo].[taxi_fleet_servicearea_zip]([Status] ASC, [Zip] ASC, [Country] ASC, [Ordering] ASC);

