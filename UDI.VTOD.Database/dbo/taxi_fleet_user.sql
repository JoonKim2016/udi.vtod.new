﻿CREATE TABLE [dbo].[taxi_fleet_user] (
    [Id]                         BIGINT        IDENTITY (31, 1) NOT NULL,
    [FleetId]                    BIGINT        NOT NULL,
    [UserId]                     INT           NOT NULL,
    [AccountNumber]              NVARCHAR (50) DEFAULT (NULL) NULL,
    [PreAuthCreditCardAmount]    DECIMAL (11)  DEFAULT ((6)) NOT NULL,
    [PreAuthCreditCardOption]    BIT           CONSTRAINT [DF__taxi_flee__PreAu__46486B8E] DEFAULT ((0)) NOT NULL,
    [AppendTime]                 DATETIME      DEFAULT (getdate()) NOT NULL,
    [RemarkForPickUpCash]        VARCHAR (200) NULL,
    [RemarkForDropOffCash]       VARCHAR (200) NULL,
    [RemarkForPickUpCreditCard]  VARCHAR (200) NULL,
    [RemarkForDropOffCreditCard] VARCHAR (200) NULL,
    [CashAccountNumber]          NVARCHAR (50) NULL,
    CONSTRAINT [PK_taxi_fleet_user_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [taxi_fleet_user$taxi_fleet_user_ibfk_1] FOREIGN KEY ([FleetId]) REFERENCES [dbo].[taxi_fleet] ([Id]),
    CONSTRAINT [taxi_fleet_user$taxi_fleet_user_ibfk_2] FOREIGN KEY ([UserId]) REFERENCES [dbo].[my_aspnet_users] ([id])
);


GO
CREATE NONCLUSTERED INDEX [Idx_FleetIdUserId]
    ON [dbo].[taxi_fleet_user]([FleetId] ASC, [UserId] ASC);


GO
CREATE NONCLUSTERED INDEX [UserId]
    ON [dbo].[taxi_fleet_user]([UserId] ASC);

