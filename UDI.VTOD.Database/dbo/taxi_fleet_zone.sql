﻿CREATE TABLE [dbo].[taxi_fleet_zone] (
    [Id]              BIGINT           IDENTITY (82, 1) NOT NULL,
    [FleetId]         BIGINT           NOT NULL,
    [Type]            NVARCHAR (20)    DEFAULT (N'Area') NOT NULL,
    [Name]            NVARCHAR (50)    DEFAULT (NULL) NULL,
    [PolygonId]       BIGINT           DEFAULT (NULL) NULL,
    [ZipId]           BIGINT           DEFAULT (NULL) NULL,
    [CityId]          BIGINT           DEFAULT (NULL) NULL,
    [CenterLatitude]  DECIMAL (18, 15) DEFAULT (NULL) NULL,
    [CenterLongitude] DECIMAL (18, 15) DEFAULT (NULL) NULL,
    [AppendTime]      DATETIME         DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_taxi_fleet_zone_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [taxi_fleet_zone$taxi_fleet_zone_ibfk_1] FOREIGN KEY ([FleetId]) REFERENCES [dbo].[taxi_fleet] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [Idx_FleetIdTypeName]
    ON [dbo].[taxi_fleet_zone]([FleetId] ASC, [Type] ASC, [Name] ASC);

