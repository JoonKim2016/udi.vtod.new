﻿CREATE TABLE [dbo].[taxi_fleet_zone_flatrate] (
    [Id]            BIGINT       IDENTITY (80, 1) NOT NULL,
    [PickupZoneId]  BIGINT       NOT NULL,
    [DropoffZoneId] BIGINT       NOT NULL,
    [Amount]        FLOAT (53)   DEFAULT ((0)) NOT NULL,
    [AmountUnit]    NVARCHAR (3) DEFAULT (N'USD') NOT NULL,
    [AppendTime]    DATETIME     DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_taxi_fleet_zone_flatrate_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [taxi_fleet_zone_flatrate$FK_DO_Zone] FOREIGN KEY ([DropoffZoneId]) REFERENCES [dbo].[taxi_fleet_zone] ([Id]),
    CONSTRAINT [taxi_fleet_zone_flatrate$FK_PU_Zone] FOREIGN KEY ([PickupZoneId]) REFERENCES [dbo].[taxi_fleet_zone] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [FK_DO_Zone_idx]
    ON [dbo].[taxi_fleet_zone_flatrate]([DropoffZoneId] ASC);


GO
CREATE NONCLUSTERED INDEX [FK_Zone_idx]
    ON [dbo].[taxi_fleet_zone_flatrate]([PickupZoneId] ASC);

