﻿CREATE TABLE [dbo].[taxi_forbiddencancelstatus] (
    [Id]         BIGINT        IDENTITY (10, 1) NOT NULL,
    [Status]     NVARCHAR (50) NOT NULL,
    [AppendTime] DATETIME      DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_taxi_forbiddencancelstatus_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [Idx_Status]
    ON [dbo].[taxi_forbiddencancelstatus]([Status] ASC);

