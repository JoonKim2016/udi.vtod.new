﻿CREATE TABLE [dbo].[taxi_mtdata_configuration] (
    [Id]                              INT            IDENTITY (1, 1) NOT NULL,
    [FleetId]                         BIGINT         NULL,
    [BookingServiceEndpointName]      NVARCHAR (200) NULL,
    [OsiServiceEndpointName]          NVARCHAR (200) NULL,
    [AddressServiceEndpointName]      NVARCHAR (200) NULL,
    [AuthenticateServiceEndpointName] NVARCHAR (200) NULL,
    CONSTRAINT [PK_taxi_mtdata_configuration] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Fleet_Configuration] FOREIGN KEY ([FleetId]) REFERENCES [dbo].[taxi_fleet] ([Id])
);

