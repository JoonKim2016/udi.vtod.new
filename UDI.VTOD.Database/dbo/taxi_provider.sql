﻿CREATE TABLE [dbo].[taxi_provider] (
    [Id]         BIGINT        IDENTITY (13, 1) NOT NULL,
    [Name]       NVARCHAR (40) DEFAULT (NULL) NULL,
    [AppendTime] DATETIME      DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_taxi_provider_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);

