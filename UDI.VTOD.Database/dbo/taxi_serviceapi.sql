﻿CREATE TABLE [dbo].[taxi_serviceapi] (
    [Id]         BIGINT        IDENTITY (4, 1) NOT NULL,
    [Name]       NVARCHAR (40) DEFAULT (NULL) NULL,
    [AppendTime] DATETIME      DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_taxi_serviceapi_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);

