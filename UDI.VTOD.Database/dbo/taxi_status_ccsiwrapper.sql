﻿CREATE TABLE [dbo].[taxi_status_ccsiwrapper] (
    [Id]                BIGINT        IDENTITY (30, 1) NOT NULL,
    [SourceEventId]     INT           NOT NULL,
    [SourceEventStatus] NVARCHAR (50) DEFAULT (NULL) NULL,
    [Target]            NVARCHAR (50) NOT NULL,
    [AppendTime]        DATETIME      DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_taxi_status_ccsiwrapper_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IdxSource]
    ON [dbo].[taxi_status_ccsiwrapper]([SourceEventId] ASC, [SourceEventStatus] ASC);

