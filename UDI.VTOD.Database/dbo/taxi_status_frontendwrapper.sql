﻿CREATE TABLE [dbo].[taxi_status_frontendwrapper] (
    [Id]             BIGINT        IDENTITY (67, 1) NOT NULL,
    [UserId]         INT           NOT NULL,
    [VTODTaxiStatus] NVARCHAR (50) NOT NULL,
    [FrontEndStatus] NVARCHAR (50) NOT NULL,
    [AppendTime]     DATETIME      DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_taxi_status_frontendwrapper_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IdxUserIdVTODTaxiStatus]
    ON [dbo].[taxi_status_frontendwrapper]([UserId] ASC, [VTODTaxiStatus] ASC);

