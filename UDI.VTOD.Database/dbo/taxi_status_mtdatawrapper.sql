﻿CREATE TABLE [dbo].[taxi_status_mtdatawrapper] (
    [Id]         BIGINT        IDENTITY (1, 1) NOT NULL,
    [Source]     NVARCHAR (50) NOT NULL,
    [Target]     NVARCHAR (50) NOT NULL,
    [AppendTime] DATETIME      DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_taxi_status_mtdatawrapper_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);

