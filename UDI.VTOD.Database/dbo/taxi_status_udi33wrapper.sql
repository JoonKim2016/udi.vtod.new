﻿CREATE TABLE [dbo].[taxi_status_udi33wrapper] (
    [Id]         BIGINT        IDENTITY (69, 1) NOT NULL,
    [Source]     NVARCHAR (50) NOT NULL,
    [Target]     NVARCHAR (50) NOT NULL,
    [AppendTime] DATETIME      DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_taxi_status_udi33wrapper_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [Idx_Source_Target]
    ON [dbo].[taxi_status_udi33wrapper]([Source] ASC);

