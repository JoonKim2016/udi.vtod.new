﻿CREATE TABLE [dbo].[taxi_trip] (
    [Id]                   BIGINT         NOT NULL,
    [FirstName]            NVARCHAR (48)  NOT NULL,
    [LastName]             NVARCHAR (36)  NOT NULL,
    [PhoneNumber]          NVARCHAR (20)  NOT NULL,
    [FleetId]              BIGINT         NOT NULL,
    [RezId]                INT            DEFAULT (NULL) NULL,
    [ServiceAPIId]         BIGINT         NOT NULL,
    [TripType]             NVARCHAR (20)  DEFAULT (NULL) NULL,
    [APIInformation]       NVARCHAR (100) DEFAULT (NULL) NULL,
    [CabNumber]            NVARCHAR (20)  DEFAULT (NULL) NULL,
    [MinutesAway]          INT            DEFAULT (NULL) NULL,
    [Expires]              DATETIME2 (0)  DEFAULT (NULL) NULL,
    [NumberOfPassenger]    INT            DEFAULT (NULL) NULL,
    [DriverNotes]          NVARCHAR (400) DEFAULT (NULL) NULL,
    [PickupFlatRateZone]   NVARCHAR (100) DEFAULT (NULL) NULL,
    [DropOffFlatRateZone]  NVARCHAR (100) DEFAULT (NULL) NULL,
    [AccountNumber]        NVARCHAR (50)  DEFAULT (NULL) NULL,
    [PickupAddressType]    NVARCHAR (20)  DEFAULT (NULL) NULL,
    [PickupLongitude]      FLOAT (53)     DEFAULT (NULL) NULL,
    [PickupLatitude]       FLOAT (53)     DEFAULT (NULL) NULL,
    [PickupBuilding]       NVARCHAR (30)  DEFAULT (NULL) NULL,
    [PickupStreetNo]       NVARCHAR (20)  DEFAULT (NULL) NULL,
    [PickupStreetName]     NVARCHAR (60)  DEFAULT (NULL) NULL,
    [PickupStreetType]     NVARCHAR (20)  DEFAULT (NULL) NULL,
    [PickupAptNo]          NVARCHAR (15)  DEFAULT (NULL) NULL,
    [PickupCity]           NVARCHAR (30)  DEFAULT (NULL) NULL,
    [PickupStateCode]      NVARCHAR (10)  DEFAULT (NULL) NULL,
    [PickupZipCode]        NVARCHAR (20)  DEFAULT (NULL) NULL,
    [PickupCountryCode]    NVARCHAR (10)  DEFAULT (NULL) NULL,
    [PickupFullAddress]    NVARCHAR (100) DEFAULT (NULL) NULL,
    [PickupAirport]        NVARCHAR (100) DEFAULT (NULL) NULL,
    [DropOffAddressType]   NVARCHAR (20)  DEFAULT (NULL) NULL,
    [DropOffLongitude]     FLOAT (53)     DEFAULT (NULL) NULL,
    [DropOffLatitude]      FLOAT (53)     DEFAULT (NULL) NULL,
    [DropOffBuilding]      NVARCHAR (30)  DEFAULT (NULL) NULL,
    [DropOffStreetNo]      NVARCHAR (20)  DEFAULT (NULL) NULL,
    [DropOffStreetName]    NVARCHAR (50)  DEFAULT (NULL) NULL,
    [DropOffStreetType]    NVARCHAR (20)  DEFAULT (NULL) NULL,
    [DropOffAptNo]         NVARCHAR (15)  DEFAULT (NULL) NULL,
    [DropOffCity]          NVARCHAR (30)  DEFAULT (NULL) NULL,
    [DropOffStateCode]     NVARCHAR (10)  DEFAULT (NULL) NULL,
    [DropOffZipCode]       NVARCHAR (20)  DEFAULT (NULL) NULL,
    [DropOffCountryCode]   NVARCHAR (10)  DEFAULT (NULL) NULL,
    [DropOffFullAddress]   NVARCHAR (200) DEFAULT (NULL) NULL,
    [DropOffAirport]       NVARCHAR (100) DEFAULT (NULL) NULL,
    [DispatchTripId]       NVARCHAR (40)  DEFAULT (NULL) NULL,
    [RemarkForPickup]      NVARCHAR (200) DEFAULT (NULL) NULL,
    [RemarkForDropOff]     NVARCHAR (200) DEFAULT (NULL) NULL,
    [wheelchairAccessible] BIT            NOT NULL,
    [NumOfTry]             INT            DEFAULT ((0)) NOT NULL,
    [MemberBFirstName]     NVARCHAR (48)  NULL,
    [MemberBLastName]      NVARCHAR (36)  NULL,
    CONSTRAINT [PK_taxi_trip_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [taxi_trip$api] FOREIGN KEY ([ServiceAPIId]) REFERENCES [dbo].[taxi_serviceapi] ([Id]),
    CONSTRAINT [taxi_trip$fleet] FOREIGN KEY ([FleetId]) REFERENCES [dbo].[taxi_fleet] ([Id]),
    CONSTRAINT [taxi_trip$taxi_trip_ibfk_1] FOREIGN KEY ([Id]) REFERENCES [dbo].[vtod_trip] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [api_idx]
    ON [dbo].[taxi_trip]([ServiceAPIId] ASC);


GO
CREATE NONCLUSTERED INDEX [fleet_idx]
    ON [dbo].[taxi_trip]([FleetId] ASC);


GO
CREATE NONCLUSTERED INDEX [Idx_Address]
    ON [dbo].[taxi_trip]([FirstName] ASC, [LastName] ASC, [PhoneNumber] ASC, [PickupFullAddress] ASC, [PickupStreetName] ASC, [PickupCity] ASC, [PickupCountryCode] ASC);


GO
CREATE NONCLUSTERED INDEX [Idx_ServiceAPIIdDispatchTripId]
    ON [dbo].[taxi_trip]([ServiceAPIId] ASC, [DispatchTripId] ASC);


GO
CREATE NONCLUSTERED INDEX [Idx_CustomerIdPickupAirport]
    ON [dbo].[taxi_trip]([FirstName] ASC, [LastName] ASC, [PhoneNumber] ASC, [PickupAirport] ASC);

