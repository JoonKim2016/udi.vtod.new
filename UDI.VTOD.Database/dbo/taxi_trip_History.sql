﻿CREATE TABLE [dbo].[taxi_trip_History] (
    [Id]                  INT            IDENTITY (1, 1) NOT NULL,
    [VtodTripId]          INT            NULL,
    [DispatchTripId]      NVARCHAR (50)  NULL,
    [MemberID]            NVARCHAR (50)  NULL,
    [PickUpFullAddress]   NVARCHAR (200) NULL,
    [DropOffAddress]      NVARCHAR (200) NULL,
    [PickupDateTime]      DATETIME       NULL,
    [PickupDateTimeUTC]   DATETIME       NULL,
    [PaymentType]         NVARCHAR (50)  NULL,
    [CreditCardID]        NVARCHAR (50)  NULL,
    [DirectBillAccountID] NVARCHAR (50)  NULL,
    [AppendTime]          DATETIME       NULL,
    CONSTRAINT [PK_taxi_trip_History] PRIMARY KEY CLUSTERED ([Id] ASC)
);

