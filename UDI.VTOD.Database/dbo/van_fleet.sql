﻿CREATE TABLE [dbo].[van_fleet] (
    [Id]                    BIGINT          IDENTITY (1, 1) NOT NULL,
    [ProviderId]            BIGINT          NOT NULL,
    [SDS_FleetMerchantID]   BIGINT          CONSTRAINT [DF__van_flee__SDS_F__10E07F16] DEFAULT (NULL) NULL,
    [Status]                BIT             CONSTRAINT [DF__van_flee__Statu__11D4A34F] DEFAULT ((1)) NOT NULL,
    [Name]                  NVARCHAR (40)   CONSTRAINT [DF__van_fleet__Name__12C8C788] DEFAULT (NULL) NULL,
    [Alias]                 NVARCHAR (50)   CONSTRAINT [DF__van_flee__Alias__13BCEBC1] DEFAULT (NULL) NULL,
    [PhoneNumber]           NVARCHAR (50)   CONSTRAINT [DF__van_flee__Phone__14B10FFA] DEFAULT (NULL) NULL,
    [WebSite]               NVARCHAR (100)  CONSTRAINT [DF__van_flee__WebSi__15A53433] DEFAULT (NULL) NULL,
    [IVREnable]             BIT             CONSTRAINT [DF_van_fleet_IVREnable] DEFAULT ((0)) NOT NULL,
    [CCSiFleetId]           NVARCHAR (50)   CONSTRAINT [DF__van_flee__CCSiF__1699586C] DEFAULT (NULL) NULL,
    [CCSiSource]            NVARCHAR (50)   CONSTRAINT [DF__van_flee__CCSiS__178D7CA5] DEFAULT (NULL) NULL,
    [UDI33DNI]              NVARCHAR (20)   CONSTRAINT [DF__van_flee__UDI33__1881A0DE] DEFAULT (NULL) NULL,
    [UDI33IPAddress]        NVARCHAR (16)   CONSTRAINT [DF__van_flee__UDI33__1975C517] DEFAULT (NULL) NULL,
    [UDI33PortNo]           INT             CONSTRAINT [DF__van_flee__UDI33__1A69E950] DEFAULT (NULL) NULL,
    [UDI33ReceivedTimeout]  INT             CONSTRAINT [DF__van_flee__UDI33__1B5E0D89] DEFAULT (NULL) NULL,
    [UDI33ForceZone]        NVARCHAR (500)  CONSTRAINT [DF__van_flee__UDI33__1C5231C2] DEFAULT (NULL) NULL,
    [ServerUTCOffset]       NVARCHAR (50)   CONSTRAINT [DF__van_flee__Serve__1D4655FB] DEFAULT (NULL) NULL,
    [RestrictBookingOption] INT             CONSTRAINT [DF__van_flee__Restr__1E3A7A34] DEFAULT ((1)) NOT NULL,
    [RestrictBookingMins]   INT             CONSTRAINT [DF__van_flee__Restr__1F2E9E6D] DEFAULT (NULL) NULL,
    [DetectFastMeter]       INT             CONSTRAINT [DF__van_flee__Detec__2022C2A6] DEFAULT ((0)) NOT NULL,
    [MinPriceFastMeter]     DECIMAL (10, 2) CONSTRAINT [DF__van_flee__MinPr__2116E6DF] DEFAULT ((0.00)) NOT NULL,
    [VehicleAttr]           NVARCHAR (50)   CONSTRAINT [DF__van_flee__Vehic__220B0B18] DEFAULT (NULL) NULL,
    [VehicleBits]           BIGINT          CONSTRAINT [DF__van_flee__Vehic__22FF2F51] DEFAULT (NULL) NULL,
    [DriverAttr]            NVARCHAR (50)   CONSTRAINT [DF__van_flee__Drive__23F3538A] DEFAULT (NULL) NULL,
    [DriverBits]            BIGINT          CONSTRAINT [DF__van_flee__Drive__24E777C3] DEFAULT (NULL) NULL,
    [HandleStreetRange]     INT             CONSTRAINT [DF__van_flee__Handl__25DB9BFC] DEFAULT (NULL) NULL,
    [OverWriteFixedPrice]   BIT             CONSTRAINT [DF__van_flee__OverW__26CFC035] DEFAULT ((0)) NOT NULL,
    [PickMeUpNowOption]     INT             CONSTRAINT [DF__van_flee__PickM__27C3E46E] DEFAULT ((1)) NULL,
    [MaxEstimatedRate]      DECIMAL (10, 2) NULL,
    [MinEstimatedRate]      DECIMAL (10, 2) NULL,
    [IsEstimated]           BIT             NULL,
    [AppendTime]            DATETIME        CONSTRAINT [DF__van_flee__Appen__28B808A7] DEFAULT (getdate()) NOT NULL,
    [DispatchCityCode]      INT             NULL,
    [DriverInfo]            INT             NULL,
    CONSTRAINT [PK_van_fleet_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [van_fleet$van_fleet_ibfk_1] FOREIGN KEY ([ProviderId]) REFERENCES [dbo].[van_provider] ([Id])
);

