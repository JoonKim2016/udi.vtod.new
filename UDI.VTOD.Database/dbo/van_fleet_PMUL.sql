﻿CREATE TABLE [dbo].[van_fleet_PMUL] (
    [Id]                   INT      IDENTITY (1, 1) NOT NULL,
    [FleetId]              BIGINT   NULL,
    [PickUpLaterStartDate] DATETIME NULL,
    [PickUpLaterEndDate]   DATETIME NULL,
    CONSTRAINT [PK_van_fleet_PMUL] PRIMARY KEY CLUSTERED ([Id] ASC)
);

