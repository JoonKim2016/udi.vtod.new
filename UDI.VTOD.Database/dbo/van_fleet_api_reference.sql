﻿CREATE TABLE [dbo].[van_fleet_api_reference] (
    [Id]                   BIGINT        IDENTITY (1, 1) NOT NULL,
    [FleetId]              BIGINT        NOT NULL,
    [ServiceAPIId]         BIGINT        NOT NULL,
    [ExtendedServiceAPIId] BIGINT        CONSTRAINT [DF__van_fleet__Exten__79D2FC8C] DEFAULT (NULL) NULL,
    [Name]                 NVARCHAR (40) CONSTRAINT [DF__van_fleet___Name__7AC720C5] DEFAULT (NULL) NULL,
    [AppendTime]           DATETIME      CONSTRAINT [DF__van_fleet__Appen__7BBB44FE] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_van_fleet_api_reference_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [van_fleet_api_reference$van_fleet_api_reference_ibfk_1] FOREIGN KEY ([FleetId]) REFERENCES [dbo].[van_fleet] ([Id]),
    CONSTRAINT [van_fleet_api_reference$van_fleet_api_reference_ibfk_2] FOREIGN KEY ([ServiceAPIId]) REFERENCES [dbo].[van_serviceapi] ([Id])
);

