﻿CREATE TABLE [dbo].[van_fleet_availableaddresstype] (
    [Id]               BIGINT   IDENTITY (1, 1) NOT NULL,
    [FleetId]          BIGINT   NOT NULL,
    [AddressToAddress] BIT      CONSTRAINT [DF__van_flee__Addre__2C88998B] DEFAULT ((1)) NOT NULL,
    [AddressToAirport] BIT      CONSTRAINT [DF__van_flee__Addre__2D7CBDC4] DEFAULT ((0)) NOT NULL,
    [AddressToNull]    BIT      CONSTRAINT [DF__van_flee__Addre__2E70E1FD] DEFAULT ((1)) NOT NULL,
    [AirportToAddress] BIT      CONSTRAINT [DF__van_flee__Airpo__2F650636] DEFAULT ((1)) NOT NULL,
    [AirportToAirport] BIT      CONSTRAINT [DF__van_flee__Airpo__30592A6F] DEFAULT ((0)) NOT NULL,
    [AirportToNull]    BIT      CONSTRAINT [DF__van_flee__Airpo__314D4EA8] DEFAULT ((1)) NOT NULL,
    [AppendTime]       DATETIME CONSTRAINT [DF__van_fleet__Appen__1E45672C] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_van_fleet_availableaddresstype_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);

