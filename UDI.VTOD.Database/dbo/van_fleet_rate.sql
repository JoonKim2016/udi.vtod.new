﻿CREATE TABLE [dbo].[van_fleet_rate] (
    [Id]                  BIGINT        IDENTITY (1, 1) NOT NULL,
    [FleetId]             BIGINT        NOT NULL,
    [InitialFreeDistance] FLOAT (53)    CONSTRAINT [DF__van_fleet__Initi__4D6A6A69] DEFAULT ((0)) NOT NULL,
    [InitialAmount]       FLOAT (53)    CONSTRAINT [DF__van_fleet__Initi__4E5E8EA2] DEFAULT ((0)) NOT NULL,
    [PerDistanceAmount]   FLOAT (53)    CONSTRAINT [DF__van_fleet__PerDi__4F52B2DB] DEFAULT ((0)) NOT NULL,
    [DistanceUnit]        NVARCHAR (10) CONSTRAINT [DF__van_fleet__Dista__5046D714] DEFAULT (N'Mile') NOT NULL,
    [AmountUnit]          NVARCHAR (3)  CONSTRAINT [DF__van_fleet__Amoun__513AFB4D] DEFAULT (N'USD') NOT NULL,
    [AppendTime]          DATETIME      CONSTRAINT [DF__van_fleet__Appen__522F1F86] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_van_fleet_rate_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [van_fleet_rate$FK_Fleet_ID] FOREIGN KEY ([FleetId]) REFERENCES [dbo].[van_fleet] ([Id])
);

