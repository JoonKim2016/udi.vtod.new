﻿CREATE TABLE [dbo].[van_fleet_servicearea_city] (
    [Id]         BIGINT         IDENTITY (1, 1) NOT NULL,
    [FleetId]    BIGINT         NOT NULL,
    [Status]     BIT            CONSTRAINT [DF__van_flee__Statu__38EE7070] DEFAULT ((1)) NOT NULL,
    [City]       NVARCHAR (100) CONSTRAINT [DF__van_fleet___City__57E7F8DC] DEFAULT (NULL) NULL,
    [State]      NVARCHAR (50)  CONSTRAINT [DF__van_fleet__State__58DC1D15] DEFAULT (NULL) NULL,
    [Country]    NVARCHAR (50)  CONSTRAINT [DF__van_fleet__Count__59D0414E] DEFAULT (NULL) NULL,
    [Ordering]   INT            NOT NULL,
    [AppendTime] DATETIME       CONSTRAINT [DF__van_fleet__Appen__5AC46587] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_van_fleet_servicearea_city_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [van_fleet_servicearea_city$van_fleet_servicearea_city_ibfk_1] FOREIGN KEY ([FleetId]) REFERENCES [dbo].[van_fleet] ([Id]),
    CONSTRAINT [van_fleet_servicearea_city$Idx_ServiceAreaCity] UNIQUE NONCLUSTERED ([FleetId] ASC, [City] ASC, [State] ASC, [Country] ASC, [Ordering] ASC)
);

