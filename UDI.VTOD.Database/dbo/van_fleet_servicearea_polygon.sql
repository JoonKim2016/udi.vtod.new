﻿CREATE TABLE [dbo].[van_fleet_servicearea_polygon] (
    [Id]         BIGINT         IDENTITY (1, 1) NOT NULL,
    [FleetId]    BIGINT         NOT NULL,
    [Status]     BIT            CONSTRAINT [DF__van_flee__Statu__3DB3258D] DEFAULT ((1)) NOT NULL,
    [Type]       NVARCHAR (20)  CONSTRAINT [DF__van_fleet__Type__3EA749C6] DEFAULT (N'Area') NOT NULL,
    [Name]       NVARCHAR (100) NOT NULL,
    [PolygonId]  BIGINT         NOT NULL,
    [Ordering]   INT            NOT NULL,
    [AppendTime] DATETIME       CONSTRAINT [DF__van_flee__Appen__3F9B6DFF] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_van_fleet_servicearea_polygon_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [van_fleet_servicearea_polygon$van_fleet_servicearea_polygon_ibfk_1] FOREIGN KEY ([FleetId]) REFERENCES [dbo].[van_fleet] ([Id])
);

