﻿CREATE TABLE [dbo].[van_fleet_servicearea_zip] (
    [Id]         BIGINT        IDENTITY (1, 1) NOT NULL,
    [FleetId]    BIGINT        NOT NULL,
    [Status]     BIT           CONSTRAINT [DF__van_flee__Statu__408F9238] DEFAULT ((1)) NOT NULL,
    [Zip]        NVARCHAR (10) CONSTRAINT [DF__van_fleet_s__Zip__66361833] DEFAULT (NULL) NULL,
    [Country]    NVARCHAR (50) CONSTRAINT [DF__van_fleet__Count__672A3C6C] DEFAULT (NULL) NULL,
    [Ordering]   INT           NOT NULL,
    [AppendTime] DATETIME      CONSTRAINT [DF__van_fleet__Appen__681E60A5] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_van_fleet_servicearea_zip_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [van_fleet_servicearea_zip$van_fleet_servicearea_zip_ibfk_1] FOREIGN KEY ([FleetId]) REFERENCES [dbo].[van_fleet] ([Id]),
    CONSTRAINT [van_fleet_servicearea_zip$Idx_ServiceAreaZip] UNIQUE NONCLUSTERED ([FleetId] ASC, [Zip] ASC, [Country] ASC, [Ordering] ASC)
);

