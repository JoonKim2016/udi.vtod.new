﻿CREATE TABLE [dbo].[van_fleet_user] (
    [Id]                         BIGINT        IDENTITY (1, 1) NOT NULL,
    [FleetId]                    BIGINT        NOT NULL,
    [UserId]                     INT           NOT NULL,
    [AccountNumber]              NVARCHAR (50) CONSTRAINT [DF__van_fleet__Accou__6BEEF189] DEFAULT (NULL) NULL,
    [PreAuthCreditCardAmount]    DECIMAL (11)  CONSTRAINT [DF__van_fleet__PreAu__6CE315C2] DEFAULT ((6)) NOT NULL,
    [PreAuthCreditCardOption]    BIT           CONSTRAINT [DF__van_flee__PreAu__46486B8E] DEFAULT ((0)) NOT NULL,
    [AppendTime]                 DATETIME      CONSTRAINT [DF__van_fleet__Appen__6ECB5E34] DEFAULT (getdate()) NOT NULL,
    [RemarkForPickUpCash]        VARCHAR (200) NULL,
    [RemarkForDropOffCash]       VARCHAR (200) NULL,
    [RemarkForPickUpCreditCard]  VARCHAR (200) NULL,
    [RemarkForDropOffCreditCard] VARCHAR (200) NULL,
    CONSTRAINT [PK_van_fleet_user_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [van_fleet_user$van_fleet_user_ibfk_1] FOREIGN KEY ([FleetId]) REFERENCES [dbo].[van_fleet] ([Id]),
    CONSTRAINT [van_fleet_user$van_fleet_user_ibfk_2] FOREIGN KEY ([UserId]) REFERENCES [dbo].[my_aspnet_users] ([id])
);

