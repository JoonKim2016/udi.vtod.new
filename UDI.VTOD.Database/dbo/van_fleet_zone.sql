﻿CREATE TABLE [dbo].[van_fleet_zone] (
    [Id]              BIGINT           IDENTITY (1, 1) NOT NULL,
    [FleetId]         BIGINT           NOT NULL,
    [Type]            NVARCHAR (20)    CONSTRAINT [DF__van_fleet___Type__73901351] DEFAULT (N'Area') NOT NULL,
    [Name]            NVARCHAR (50)    CONSTRAINT [DF__van_fleet___Name__7484378A] DEFAULT (NULL) NULL,
    [PolygonId]       BIGINT           CONSTRAINT [DF__van_fleet__Polyg__75785BC3] DEFAULT (NULL) NULL,
    [ZipId]           BIGINT           CONSTRAINT [DF__van_fleet__ZipId__766C7FFC] DEFAULT (NULL) NULL,
    [CityId]          BIGINT           CONSTRAINT [DF__van_fleet__CityI__7760A435] DEFAULT (NULL) NULL,
    [CenterLatitude]  DECIMAL (18, 15) CONSTRAINT [DF__van_fleet__Cente__7854C86E] DEFAULT (NULL) NULL,
    [CenterLongitude] DECIMAL (18, 15) CONSTRAINT [DF__van_fleet__Cente__7948ECA7] DEFAULT (NULL) NULL,
    [AppendTime]      DATETIME         CONSTRAINT [DF__van_fleet__Appen__7A3D10E0] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_van_fleet_zone_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [van_fleet_zone$van_fleet_zone_ibfk_1] FOREIGN KEY ([FleetId]) REFERENCES [dbo].[van_fleet] ([Id])
);

