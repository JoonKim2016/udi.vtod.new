﻿CREATE TABLE [dbo].[van_fleet_zone_flatrate] (
    [Id]            BIGINT       IDENTITY (1, 1) NOT NULL,
    [PickupZoneId]  BIGINT       NOT NULL,
    [DropoffZoneId] BIGINT       NOT NULL,
    [Amount]        FLOAT (53)   CONSTRAINT [DF__van_fleet__Amoun__7E0DA1C4] DEFAULT ((0)) NOT NULL,
    [AmountUnit]    NVARCHAR (3) CONSTRAINT [DF__van_fleet__Amoun__7F01C5FD] DEFAULT (N'USD') NOT NULL,
    [AppendTime]    DATETIME     CONSTRAINT [DF__van_fleet__Appen__7FF5EA36] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_van_fleet_zone_flatrate_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [van_fleet_zone_flatrate$FK_DO_Zone] FOREIGN KEY ([DropoffZoneId]) REFERENCES [dbo].[van_fleet_zone] ([Id]),
    CONSTRAINT [van_fleet_zone_flatrate$FK_PU_Zone] FOREIGN KEY ([PickupZoneId]) REFERENCES [dbo].[van_fleet_zone] ([Id])
);

