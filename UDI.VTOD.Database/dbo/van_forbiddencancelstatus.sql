﻿CREATE TABLE [dbo].[van_forbiddencancelstatus] (
    [Id]         BIGINT        IDENTITY (1, 1) NOT NULL,
    [Status]     NVARCHAR (50) NOT NULL,
    [AppendTime] DATETIME      CONSTRAINT [DF__van_forbi__Appen__04BA9F53] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_van_forbiddencancelstatus_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);

