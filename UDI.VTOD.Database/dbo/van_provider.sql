﻿CREATE TABLE [dbo].[van_provider] (
    [Id]         BIGINT        IDENTITY (1, 1) NOT NULL,
    [Name]       NVARCHAR (40) CONSTRAINT [DF__van_provid__Name__0E7913B7] DEFAULT (NULL) NULL,
    [AppendTime] DATETIME      CONSTRAINT [DF__van_provi__Appen__0F6D37F0] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_van_provider_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);

