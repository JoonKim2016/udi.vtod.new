﻿CREATE TABLE [dbo].[van_serviceapi] (
    [Id]         BIGINT        IDENTITY (1, 1) NOT NULL,
    [Name]       NVARCHAR (40) CONSTRAINT [DF__van_servic__Name__170E59B8] DEFAULT (NULL) NULL,
    [AppendTime] DATETIME      CONSTRAINT [DF__van_servi__Appen__18027DF1] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_van_serviceapi_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);

