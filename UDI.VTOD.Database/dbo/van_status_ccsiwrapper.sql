﻿CREATE TABLE [dbo].[van_status_ccsiwrapper] (
    [Id]                BIGINT        IDENTITY (1, 1) NOT NULL,
    [SourceEventId]     INT           NOT NULL,
    [SourceEventStatus] NVARCHAR (50) CONSTRAINT [DF__van_statu__Sourc__133DC8D4] DEFAULT (NULL) NULL,
    [Target]            NVARCHAR (50) NOT NULL,
    [AppendTime]        DATETIME      CONSTRAINT [DF__van_statu__Appen__1431ED0D] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_van_status_ccsiwrapper_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);

