﻿CREATE TABLE [dbo].[van_status_frontendwrapper] (
    [Id]             BIGINT        IDENTITY (1, 1) NOT NULL,
    [UserId]         INT           NOT NULL,
    [VTODVanStatus]  NVARCHAR (50) NOT NULL,
    [FrontEndStatus] NVARCHAR (50) NOT NULL,
    [AppendTime]     DATETIME      CONSTRAINT [DF__van_statu__Appen__355DD6AE] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_van_status_frontendwrapper_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);

