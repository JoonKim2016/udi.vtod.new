﻿CREATE TABLE [dbo].[van_trip_status] (
    [Id]               BIGINT          IDENTITY (1, 1) NOT NULL,
    [TripID]           BIGINT          NOT NULL,
    [Status]           NVARCHAR (50)   NOT NULL,
    [StatusTime]       DATETIME        CONSTRAINT [DF__van_trip___Statu__38F95D68] DEFAULT (NULL) NULL,
    [VehicleNumber]    NVARCHAR (50)   CONSTRAINT [DF__van_trip___Vehic__39ED81A1] DEFAULT (NULL) NULL,
    [VehicleLongitude] DECIMAL (18, 6) CONSTRAINT [DF__van_trip___Vehic__3AE1A5DA] DEFAULT (NULL) NULL,
    [VehicleLatitude]  DECIMAL (18, 6) CONSTRAINT [DF__van_trip___Vehic__3BD5CA13] DEFAULT (NULL) NULL,
    [DriverName]       NVARCHAR (50)   CONSTRAINT [DF__van_trip___Drive__3CC9EE4C] DEFAULT (NULL) NULL,
    [DriverId]         NVARCHAR (50)   CONSTRAINT [DF__van_trip___Drive__3DBE1285] DEFAULT (NULL) NULL,
    [ETA]              INT             CONSTRAINT [DF__van_trip_st__ETA__3EB236BE] DEFAULT (NULL) NULL,
    [ETAWithTraffic]   INT             CONSTRAINT [DF__van_trip___ETAWi__3FA65AF7] DEFAULT (NULL) NULL,
    [Fare]             DECIMAL (10, 2) CONSTRAINT [DF__van_trip_s__Fare__409A7F30] DEFAULT (NULL) NULL,
    [DispatchFare]     DECIMAL (10, 2) CONSTRAINT [DF__van_trip___Dispa__418EA369] DEFAULT (NULL) NULL,
    [CommentForStatus] NVARCHAR (50)   CONSTRAINT [DF__van_trip___Comme__4282C7A2] DEFAULT (NULL) NULL,
    [OriginalStatus]   NVARCHAR (50)   CONSTRAINT [DF__van_trip___Origi__4376EBDB] DEFAULT (N'Unknown') NOT NULL,
    [AppendTime]       DATETIME        CONSTRAINT [DF__van_trip___Appen__446B1014] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_van_trip_status_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [van_trip_status$van_trip_status_ibfk_1] FOREIGN KEY ([TripID]) REFERENCES [dbo].[van_trip] ([Id])
);

