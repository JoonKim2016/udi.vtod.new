﻿CREATE TABLE [dbo].[vtod_Monitoring] (
    [ID]               INT             IDENTITY (1, 1) NOT NULL,
    [Query]            NVARCHAR (1000) NULL,
    [Description]      NVARCHAR (200)  NULL,
    [IsEnable]         BIT             NULL,
    [EmailList]        NVARCHAR (500)  NULL,
    [MailType]         NVARCHAR (100)  NULL,
    [StartUTCTime]     TIME (7)        NULL,
    [EndUTCTime]       TIME (7)        NULL,
    [JobType]          INT             NULL,
    [MailBody]         NVARCHAR (MAX)  NULL,
    [MailSubject]      NVARCHAR (500)  NULL,
    [Operator]         NVARCHAR (100)  NULL,
    [OperatingNumbers] NVARCHAR (100)  NULL,
    [AppendTime]       DATETIME        NULL
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Always=1,Day=2,Night=3', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'vtod_Monitoring', @level2type = N'COLUMN', @level2name = N'JobType';

