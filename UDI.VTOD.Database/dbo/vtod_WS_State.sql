﻿CREATE TABLE [dbo].[vtod_WS_State] (
    [Id]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [ReferenceId] BIGINT         NOT NULL,
    [TableName]   NVARCHAR (100) NOT NULL,
    [Key]         NVARCHAR (100) NOT NULL,
    [State]       NVARCHAR (MAX) NULL,
    [AppendTime]  DATETIME       CONSTRAINT [DF__vtod_WS_S__Appen__74994623] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK__vtod_WS___3214EC074E4B9997] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [Idx_RefId_TableName]
    ON [dbo].[vtod_WS_State]([ReferenceId] ASC, [TableName] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_vtod_WS_State]
    ON [dbo].[vtod_WS_State]([Key] ASC);


GO
CREATE NONCLUSTERED INDEX [idx01_vtod_WS_State]
    ON [dbo].[vtod_WS_State]([AppendTime] ASC);

