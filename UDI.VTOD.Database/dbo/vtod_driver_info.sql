﻿CREATE TABLE [dbo].[vtod_driver_info] (
    [Id]                BIGINT         IDENTITY (2780, 1) NOT NULL,
    [UniqueID]          NCHAR (36)     DEFAULT (NULL) NULL,
    [FleetType]         NVARCHAR (50)  NOT NULL,
    [DriverID]          NVARCHAR (50)  DEFAULT (NULL) NULL,
    [VehicleID]         NVARCHAR (50)  DEFAULT (NULL) NULL,
    [FirstName]         NVARCHAR (50)  DEFAULT (NULL) NULL,
    [LastName]          NVARCHAR (50)  DEFAULT (NULL) NULL,
    [CellPhone]         NVARCHAR (50)  DEFAULT (NULL) NULL,
    [HomePhone]         NVARCHAR (50)  DEFAULT (NULL) NULL,
    [AlternatePhone]    NVARCHAR (50)  DEFAULT (NULL) NULL,
    [QualificationDate] DATETIME2 (0)  DEFAULT (NULL) NULL,
    [zTripQualified]    BIT            CONSTRAINT [DF__vtod_driv__zTrip__1940BAED] DEFAULT (NULL) NULL,
    [StartDate]         DATETIME2 (0)  DEFAULT (NULL) NULL,
    [HrDays]            NVARCHAR (45)  DEFAULT (NULL) NULL,
    [LeaseDate]         DATETIME2 (0)  DEFAULT (NULL) NULL,
    [PhotoUrl]          NVARCHAR (150) DEFAULT (NULL) NULL,
    [DispatchUniqueKey] NVARCHAR (100) DEFAULT (NULL) NULL,
    [CryptoDriver]      BINARY (20)    NOT NULL,
    [AppendDate]        DATETIME2 (0)  NOT NULL,
    [FleetID]           INT            NULL,
	[DriverNumber]      INT            NULL,
    CONSTRAINT [PK_vtod_driver_info_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [FkCryptoDriver]
    ON [dbo].[vtod_driver_info]([CryptoDriver] ASC);


GO
CREATE NONCLUSTERED INDEX [idx_DriverId]
    ON [dbo].[vtod_driver_info]([DriverID] ASC);


GO
CREATE NONCLUSTERED INDEX [idx_UniqueID]
    ON [dbo].[vtod_driver_info]([UniqueID] ASC);

