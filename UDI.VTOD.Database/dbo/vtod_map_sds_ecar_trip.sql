﻿CREATE TABLE [dbo].[vtod_map_sds_ecar_trip] (
    [Id]         BIGINT   IDENTITY (1, 1) NOT NULL,
    [SDSTripID]  BIGINT   NOT NULL,
    [ECarTripId] BIGINT   NOT NULL,
    [AppendTime] DATETIME DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_vtod_map_sds_ecar_trip_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [vtod_map_sds_ecar_trip$ecar_Trip_Id_1] FOREIGN KEY ([ECarTripId]) REFERENCES [dbo].[ecar_trip] ([Id]),
    CONSTRAINT [vtod_map_sds_ecar_trip$SDS_Trip_Id_1] FOREIGN KEY ([SDSTripID]) REFERENCES [dbo].[sds_trip] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [ecar_Trip_Id_1]
    ON [dbo].[vtod_map_sds_ecar_trip]([ECarTripId] ASC);


GO
CREATE NONCLUSTERED INDEX [SDS_Trip_Id_1]
    ON [dbo].[vtod_map_sds_ecar_trip]([SDSTripID] ASC);

