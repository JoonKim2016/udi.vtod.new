﻿CREATE TABLE [dbo].[vtod_member_info](
	[Id]					bigint			IDENTITY (1, 1) NOT NULL,
	[SDSMemberId]			int				NOT NULL,
	[FirstName]			    nvarchar(20)	NOT NULL,
	[LastName]				nvarchar(20)	NOT NULL,
	[ReferralCode]			nvarchar(25)	NOT NULL,
	[ReferralLink]			nvarchar(100)	NOT NULL,
	[ReferralCreditAmount]  decimal (10, 2) NULL,
	[ReferrerSDSMemberID]   INT	NULL,
	[CreditEarnedOn]		datetime	    NULL, 
    [HasBeenBooked]			BIT             NOT NULL, 
    CONSTRAINT [PK_vtod_member_info] PRIMARY KEY ([Id])
)
 GO
CREATE NONCLUSTERED INDEX [Idx_SDSMemberId]
    ON [dbo].[vtod_member_info](SDSMemberId ASC);