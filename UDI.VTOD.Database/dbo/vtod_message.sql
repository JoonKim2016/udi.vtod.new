﻿CREATE TABLE [dbo].[vtod_message] (
    [id]                BIGINT        IDENTITY (9, 1) NOT NULL,
    [UserID]            INT           NOT NULL,
    [message]           NVARCHAR (85) DEFAULT (NULL) NULL,
    [description]       NVARCHAR (45) DEFAULT (NULL) NULL,
    [messageCodeID]     BIGINT        DEFAULT (NULL) NULL,
    [messagelanguageID] BIGINT        DEFAULT (NULL) NULL,
    CONSTRAINT [PK_vtod_message_id] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [vtod_message$FK_Error_ID] FOREIGN KEY ([id]) REFERENCES [dbo].[vtod_message_code] ([id]),
    CONSTRAINT [vtod_message$Fk_Lang_ID] FOREIGN KEY ([id]) REFERENCES [dbo].[vtod_message_language] ([id])
);

