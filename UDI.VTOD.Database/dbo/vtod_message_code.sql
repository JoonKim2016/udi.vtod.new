﻿CREATE TABLE [dbo].[vtod_message_code] (
    [id]   BIGINT       IDENTITY (4, 1) NOT NULL,
    [code] NVARCHAR (5) DEFAULT (NULL) NULL,
    CONSTRAINT [PK_vtod_message_code_id] PRIMARY KEY CLUSTERED ([id] ASC)
);

