﻿CREATE TABLE [dbo].[vtod_message_language] (
    [id]   BIGINT        IDENTITY (3, 1) NOT NULL,
    [Code] NVARCHAR (6)  DEFAULT (NULL) NULL,
    [Name] NVARCHAR (45) DEFAULT (NULL) NULL,
    CONSTRAINT [PK_vtod_message_language_id] PRIMARY KEY CLUSTERED ([id] ASC)
);

