﻿CREATE TABLE [dbo].[vtod_polygon] (
    [id]              BIGINT            IDENTITY (1, 1) NOT NULL,
    [Name]            VARCHAR (50)      NOT NULL,
    [Details]         VARCHAR (500)     CONSTRAINT [DF__vtod_poly__Detai__07E124C2] DEFAULT (NULL) NULL,
    [Type]            VARCHAR (20)      CONSTRAINT [DF__vtod_polyg__Type__08D548F2] DEFAULT (N'Airport') NOT NULL,
    [polygon]         [sys].[geography] NOT NULL,
    [CenterLatitude]  DECIMAL (18, 15)  CONSTRAINT [DF__vtod_poly__Cente__09C96D34] DEFAULT (NULL) NULL,
    [CenterLongitude] DECIMAL (18, 15)  CONSTRAINT [DF__vtod_poly__Cente__0ABD915C] DEFAULT (NULL) NULL,
    [Country]         VARCHAR (50)      CONSTRAINT [DF__vtod_poly__Count__0BB1B1A5] DEFAULT (NULL) NULL,
    [State]           VARCHAR (50)      CONSTRAINT [DF__vtod_poly__State__0CA5D91E] DEFAULT (NULL) NULL,
    [City]            VARCHAR (100)     CONSTRAINT [DF__vtod_polyg__City__0D992E17] DEFAULT (NULL) NULL,
    [AppendTime]      DATETIME          CONSTRAINT [DF__vtod_poly__Appen__0E8E2350] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK__vtod_pol__3213E83F48061801] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [UQ__vtod_pol__737584F6456FF35C] UNIQUE NONCLUSTERED ([Name] ASC)
);


GO
CREATE SPATIAL INDEX [SIndx]
    ON [dbo].[vtod_polygon] ([polygon]);

