﻿CREATE TABLE [dbo].[vtod_referral_credit_configuration] (
    [Id]				BIGINT          IDENTITY (1, 1) NOT NULL,
    [ReferralType]		TINYINT         NOT NULL,
    [ReferralTypeDesc]  AS              (CASE [ReferralType] WHEN (1) THEN 'Referral New User' WHEN (2) THEN 'Referral Trip Completed' ELSE 'Undefined' END),	
    [CreditAmount]      DECIMAL (10,2)  NOT NULL,
    [StartDate]         DATETIME        NULL,
    [EndDate]           DATETIME        NULL, 
    CONSTRAINT [PK_vtod_referral_credi_configuration] PRIMARY KEY ([Id]),    
);


