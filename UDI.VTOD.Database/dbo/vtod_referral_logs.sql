﻿CREATE TABLE [dbo].[vtod_referral_logs](
	[Id]					bigint			IDENTITY (1, 1) NOT NULL,
	[ReferrerSDSMemberId]   INT				NOT NULL,
	[RefereePhoneNumber]	nvarchar(20)	NOT NULL,
	[CreatedOn]				datetime		NOT NULL,
 CONSTRAINT [PK_vtod_referral_Logs] PRIMARY KEY CLUSTERED (	[Id] ASC)
 );

 GO
CREATE NONCLUSTERED INDEX [Idx_ReferrerSDSMemberId]
    ON [dbo].[vtod_referral_Logs](ReferrerSDSMemberId ASC);