﻿CREATE TABLE [dbo].[vtod_synonym] (
    [Id]    BIGINT        IDENTITY (1, 1) NOT NULL,
    [Key]   NVARCHAR (10) NOT NULL,
    [Value] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_vtod_synonym] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_vtod_synonym_key]
    ON [dbo].[vtod_synonym]([Key] ASC);

