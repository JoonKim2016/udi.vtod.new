﻿CREATE TABLE [dbo].[vtod_trip] (
    [Id]                    BIGINT          IDENTITY (25708, 1) NOT NULL,
    [UserID]                INT             NOT NULL,
    [FleetType]             NVARCHAR (50)   NOT NULL,
    [FleetTripCode]         INT             NULL,
    [MemberID]              NVARCHAR (50)   CONSTRAINT [DF__vtod_trip__Membe__34E8D562] DEFAULT (NULL) NULL,
    [CreditCardID]          NVARCHAR (50)   CONSTRAINT [DF__vtod_trip__Credi__35DCF99B] DEFAULT (NULL) NULL,
    [DirectBillAccountID]   NVARCHAR (50)   CONSTRAINT [DF__vtod_trip__Direc__36D11DD4] DEFAULT (NULL) NULL,
    [FinalStatus]           NVARCHAR (50)   CONSTRAINT [DF__vtod_trip__Final__37C5420D] DEFAULT (NULL) NULL,
    [Gratuity]              DECIMAL (10, 2) CONSTRAINT [DF__vtod_trip__Gratu__38B96646] DEFAULT (NULL) NULL,
    [GratuityRate]          DECIMAL (10, 2) CONSTRAINT [DF__vtod_trip__Gratu__39AD8A7F] DEFAULT (NULL) NULL,
    [TotalFareAmount]       DECIMAL (10, 2) CONSTRAINT [DF__vtod_trip__Total__3AA1AEB8] DEFAULT (NULL) NULL,
    [PickupDateTime]        DATETIME2 (0)   CONSTRAINT [DF__vtod_trip__Picku__3B95D2F1] DEFAULT (NULL) NULL,
    [PickupDateTimeUTC]     DATETIME2 (0)   CONSTRAINT [DF__vtod_trip__Picku__3C89F72A] DEFAULT (NULL) NULL,
    [BookingServerDateTime] DATETIME        CONSTRAINT [DF__vtod_trip__Booki__3D7E1B63] DEFAULT (getdate()) NULL,
    [PickMeUpNow]           BIT             CONSTRAINT [DF__vtod_trip__PickM__3E723F9C] DEFAULT (NULL) NULL,
    [ChargeTry]             INT             CONSTRAINT [DF__vtod_trip__Charg__3F6663D5] DEFAULT ((0)) NOT NULL,
    [ChargeStatus]          NVARCHAR (50)   CONSTRAINT [DF__vtod_trip__Charg__405A880E] DEFAULT (NULL) NULL,
    [PaymentType]           NVARCHAR (50)   CONSTRAINT [DF__vtod_trip__Payme__414EAC47] DEFAULT (N'Unknown') NOT NULL,
    [TripStartTime]         DATETIME2 (0)   CONSTRAINT [DF__vtod_trip__TripS__4242D080] DEFAULT (NULL) NULL,
    [TripEndTime]           DATETIME2 (0)   CONSTRAINT [DF__vtod_trip__TripE__4336F4B9] DEFAULT (NULL) NULL,
    [Error]                 NVARCHAR (500)  CONSTRAINT [DF__vtod_trip__Error__442B18F2] DEFAULT (NULL) NULL,
    [ConsumerSource]        NVARCHAR (50)   CONSTRAINT [DF__vtod_trip__Consu__451F3D2B] DEFAULT (NULL) NULL,
    [ConsumerDevice]        NVARCHAR (50)   CONSTRAINT [DF__vtod_trip__Consu__46136164] DEFAULT (NULL) NULL,
    [ConsumerRef]           NVARCHAR (50)   CONSTRAINT [DF__vtod_trip__Consu__4707859D] DEFAULT (NULL) NULL,
    [DriverName]            NVARCHAR (100)  CONSTRAINT [DF__vtod_trip__Drive__47FBA9D6] DEFAULT (NULL) NULL,
    [DriverID]              NVARCHAR (50)   CONSTRAINT [DF__vtod_trip__Drive__48EFCE0F] DEFAULT (NULL) NULL,
    [PromisedETA]           INT             CONSTRAINT [DF__vtod_trip__Promi__49E3F248] DEFAULT (NULL) NULL,
    [DriverAcceptedETA]     INT             CONSTRAINT [DF__vtod_trip__Drive__4AD81681] DEFAULT (NULL) NULL,
    [DriverInfoID]          BIGINT          CONSTRAINT [DF__vtod_trip__Drive__4BCC3ABA] DEFAULT (NULL) NULL,
    [AppendTime]            DATETIME2 (0)   NOT NULL,
    [EmailAddress]          NVARCHAR (200)  NULL,
    [IsFlatRate]            BIT             NULL,
    [SplitPaymentMemberB]   NVARCHAR (50)   NULL,
    [SplitPaymentStatus]    INT             NULL,
    [IsSplitPayment]        BIT             NULL,
    [MemberBCreditCardID]   NVARCHAR (50)   NULL,
    [IsBOBO] BIT NULL, 
    [BOBOMemberID] NVARCHAR(50) NULL, 
    CONSTRAINT [PK_vtod_trip_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [vtod_trip$FK_DriverInfo] FOREIGN KEY ([DriverInfoID]) REFERENCES [dbo].[vtod_driver_info] ([Id]),
    CONSTRAINT [vtod_trip$Fk_UserID] FOREIGN KEY ([UserID]) REFERENCES [dbo].[my_aspnet_users] ([id])
);


GO
CREATE NONCLUSTERED INDEX [FK_DriverInfo_idx]
    ON [dbo].[vtod_trip]([DriverInfoID] ASC);


GO
CREATE NONCLUSTERED INDEX [idx01_vtod_trip]
    ON [dbo].[vtod_trip]([PickupDateTimeUTC] ASC, [FleetType] ASC, [FinalStatus] ASC, [TotalFareAmount] ASC);


GO
CREATE NONCLUSTERED INDEX [idx02_vtod_trip]
    ON [dbo].[vtod_trip]([PickupDateTimeUTC] ASC, [UserID] ASC, [FleetType] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_FinalStatus]
    ON [dbo].[vtod_trip]([FinalStatus] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_fleettype]
    ON [dbo].[vtod_trip]([FleetType] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MemberID]
    ON [dbo].[vtod_trip]([MemberID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PickupDateTime]
    ON [dbo].[vtod_trip]([PickupDateTime] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PickupUTC]
    ON [dbo].[vtod_trip]([PickupDateTimeUTC] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_TotalFare]
    ON [dbo].[vtod_trip]([TotalFareAmount] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_UserID]
    ON [dbo].[vtod_trip]([UserID] ASC);


GO
CREATE NONCLUSTERED INDEX [idx03_vtod_trip]
    ON [dbo].[vtod_trip]([AppendTime] ASC);

