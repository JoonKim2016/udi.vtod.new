﻿CREATE TABLE [dbo].[vtod_trip_account_transaction] (
    [Id]                BIGINT          IDENTITY (37, 1) NOT NULL,
    [TripID]            BIGINT          NOT NULL,
    [TransactionNumber] NVARCHAR (50)   DEFAULT (NULL) NULL,
    [AmountCharged]     DECIMAL (10, 2) DEFAULT (NULL) NULL,
    [CreditsApplied]    DECIMAL (10, 2) DEFAULT (NULL) NULL,
    [Error]             NVARCHAR (500)  DEFAULT (NULL) NULL,
    [AppendTime]        DATETIME2 (0)   NOT NULL,
    CONSTRAINT [PK_vtod_trip_account_transaction_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [vtod_trip_account_transaction$tripID] FOREIGN KEY ([TripID]) REFERENCES [dbo].[vtod_trip] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [tripID_idx]
    ON [dbo].[vtod_trip_account_transaction]([TripID] ASC);

