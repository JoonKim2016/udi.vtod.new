﻿CREATE TABLE [dbo].[vtod_trip_notification] (
    [Id]             BIGINT         NOT NULL,
    [Booked]         INT            DEFAULT (NULL) NULL,
    [BookedTime]     DATETIME2 (0)  DEFAULT (NULL) NULL,
    [BookedError]    NVARCHAR (MAX) NULL,
    [Assigned]       INT            DEFAULT (NULL) NULL,
    [AssignedTime]   DATETIME2 (0)  DEFAULT (NULL) NULL,
    [AssignedError]  NVARCHAR (MAX) NULL,
    [Completed]      INT            DEFAULT (NULL) NULL,
    [CompletedTime]  DATETIME2 (0)  DEFAULT (NULL) NULL,
    [CompletedError] NVARCHAR (MAX) NULL,
    [Canceled]       INT            DEFAULT (NULL) NULL,
    [CanceledTime]   DATETIME2 (0)  DEFAULT (NULL) NULL,
    [CanceledError]  NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_vtod_trip_notification_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [vtod_trip_notification$vtod_trip_ibfk_1] FOREIGN KEY ([Id]) REFERENCES [dbo].[vtod_trip] ([Id])
);

