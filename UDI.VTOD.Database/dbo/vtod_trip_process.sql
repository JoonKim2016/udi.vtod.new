﻿CREATE TABLE [dbo].[vtod_trip_process] (
    [Id]              BIGINT NOT NULL,
    [CheckTripStatus] INT    NULL,
    CONSTRAINT [PK_vtod_trip_process] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_vtod_trip_process_vtod_trip_process1] FOREIGN KEY ([Id]) REFERENCES [dbo].[vtod_trip] ([Id])
);

