﻿CREATE TABLE [dbo].[vtod_trip_rate] (
    [Id]         BIGINT          IDENTITY (2756, 1) NOT NULL,
    [TripID]     BIGINT          NOT NULL,
    [Rate]       INT             NOT NULL,
    [Comment]    NVARCHAR (1000) DEFAULT (NULL) NULL,
    [Type]       INT             DEFAULT ((1)) NULL,
    [AppendTime] DATETIME2 (0)   NOT NULL,
    CONSTRAINT [PK_vtod_trip_rate_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [vtod_trip_rate$trip] FOREIGN KEY ([TripID]) REFERENCES [dbo].[vtod_trip] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_Rate]
    ON [dbo].[vtod_trip_rate]([Rate] ASC);


GO
CREATE NONCLUSTERED INDEX [trip_idx]
    ON [dbo].[vtod_trip_rate]([TripID] ASC);

