﻿CREATE TABLE [dbo].[vtod_trip_rate_reason] (
    [Id]     BIGINT          IDENTITY (1255, 1) NOT NULL,
    [RateId] BIGINT          NOT NULL,
    [Key]    NVARCHAR (50)   NOT NULL,
    [Value]  NVARCHAR (1000) NOT NULL,
    CONSTRAINT [PK_vtod_trip_rate_reason_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [vtod_trip_rate_reason$RateID] FOREIGN KEY ([RateId]) REFERENCES [dbo].[vtod_trip_rate] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [RateID_idx]
    ON [dbo].[vtod_trip_rate_reason]([RateId] ASC);

