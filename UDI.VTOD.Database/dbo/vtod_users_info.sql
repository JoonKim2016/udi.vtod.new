﻿CREATE TABLE [dbo].[vtod_users_info] (
    [id]           INT           NOT NULL,
    [StreetNumber] NVARCHAR (50) DEFAULT (NULL) NULL,
    [StreetName]   NVARCHAR (50) DEFAULT (NULL) NULL,
    [City]         NVARCHAR (50) DEFAULT (NULL) NULL,
    [PostalCode]   NVARCHAR (50) DEFAULT (NULL) NULL,
    [State]        NVARCHAR (10) DEFAULT (NULL) NULL,
    [Country]      NVARCHAR (50) DEFAULT (NULL) NULL,
    [PhoneNumber]  NVARCHAR (50) DEFAULT (NULL) NULL,
    [AppendTime]   DATETIME2 (0) NOT NULL,
    CONSTRAINT [PK_vtod_users_info_id] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [vtod_users_info$vtod_users_info_ibfk_1] FOREIGN KEY ([id]) REFERENCES [dbo].[my_aspnet_users] ([id])
);

