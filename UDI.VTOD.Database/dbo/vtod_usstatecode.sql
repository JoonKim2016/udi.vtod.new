﻿CREATE TABLE [dbo].[vtod_usstatecode] (
    [Id]         BIGINT        IDENTITY (52, 1) NOT NULL,
    [Name]       NVARCHAR (50) NOT NULL,
    [Code]       NVARCHAR (5)  DEFAULT (NULL) NULL,
    [AppendTime] DATETIME      DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_vtod_usstatecode_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);

