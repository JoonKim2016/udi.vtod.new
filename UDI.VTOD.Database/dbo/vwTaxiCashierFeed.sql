﻿










CREATE VIEW [dbo].[vwTaxiCashierFeed]
AS

SELECT  TOP 100 PERCENT
	 VT.[Id] as VtodTripId
	,TT.[DispatchTripId]
	,ISNULL(VT.[Gratuity], 0) AS Gratuity
	,ISNULL(VT.[GratuityRate], 0) AS GratuityRate
	,(ISNULL(VT.[TotalFareAmount], 0) - ISNULL(VT.[Gratuity], 0)) AS FareAmount
	,ISNULL(VT.[TotalFareAmount], 0) AS TotalFareAmount
	,ISNULL(VT.[PickupDateTime], '') AS PickupDateTime
	,CASE WHEN VT.[PickMeUpNow] = 1 THEN 'True' ELSE 'False' END AS PickMeUpNow
	,VT.[ChargeTry]
	,ISNULL(VT.[ChargeStatus], '') AS ChargeStatus
	,VT.[PaymentType]
	,CASE WHEN VT.[IsFlatRate] = 1 THEN 'True' ELSE 'False' END AS IsFlatRate
	,ISNULL(VT.[EmailAddress], '') AS EmailAddress
	,TT.[FirstName]
	,TT.[LastName]
	,TT.[PhoneNumber]
	,REPLACE(ISNULL(TT.[PickupFullAddress],''),',','') AS PickupFullAddress
	,ISNULL(TT.[PickupAirport], '') AS PickupAirport
	,REPLACE(ISNULL(TT.[DropOffFullAddress], ''),',','') AS DropOffFullAddress
	,REPLACE(ISNULL(TT.[DropOffAirport], ''),',','') AS DropOffAirport
	,ISNULL(TAT.[TransactionNumber], '') AS TransactionNumber
	,ISNULL(TAT.[AmountCharged], 0) AS AmountCharged
	,ISNULL(TAT.[CreditsApplied], 0) AS CreditsApplied
	,ISNULL(TAT.[Error], '') AS ChargingException
	,ISNULL(DI.[DriverID], '') AS DriverID
	,ISNULL(DI.[FirstName], '') AS DriverFirstName
	,ISNULL(DI.[LastName], '') AS DriverLastName
	,TT.FleetId
FROM [dbo].[vtod_trip] VT 
INNER JOIN [dbo].[taxi_trip] TT ON VT.Id = TT.Id
LEFT JOIN [dbo].[vtod_driver_info] DI ON VT.[DriverInfoId] = DI.Id
LEFT JOIN 
(
	SELECT 
		RANK() OVER (PARTITION BY TripID ORDER BY Id DESC) AS LastRecord
		,[Id]
		,[TripID]
		,ISNULL([TransactionNumber],'') AS TransactionNumber
		,ISNULL([AmountCharged],0) AS AmountCharged
		,ISNULL([CreditsApplied],0) AS CreditsApplied
		,ISNULL([Error],'') AS Error
		,[AppendTime]
	FROM [dbo].[vtod_trip_account_transaction]
) TAT ON TT.Id = TAT.TripID AND TAT.LastRecord = 1	
WHERE VT.FinalStatus = 'Completed' 
AND VT.UserId = 11
/* AND TT.FleetId = 31 Only Texas Austin*/ 
ORDER BY VT.Id DESC;

















