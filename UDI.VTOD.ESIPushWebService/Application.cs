//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UDI.VTOD.ESIPushWebService
{
    using System;
    using System.Collections.Generic;
    
    public partial class Application
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Application()
        {
            this.Callouts = new HashSet<Callout>();
        }
    
        public int ApplicationId { get; set; }
        public string ApplicationName { get; set; }
        public int Ordering { get; set; }
        public int MaxAttempts { get; set; }
        public int RetryInterval { get; set; }
        public bool Status { get; set; }
        public int ExpiredInterval { get; set; }
        public Nullable<System.TimeSpan> BlackoutStart { get; set; }
        public Nullable<System.TimeSpan> BlackoutEnd { get; set; }
        public System.DateTime UpdatedOn { get; set; }
        public string ReportName { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Callout> Callouts { get; set; }
    }
}
