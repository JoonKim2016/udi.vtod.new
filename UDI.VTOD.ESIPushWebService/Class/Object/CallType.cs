﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTDataPushService.Class.Object
{
    public class CallType
    {
        public const int Voice = 1;
        public const int SMS = 2;
    }
}