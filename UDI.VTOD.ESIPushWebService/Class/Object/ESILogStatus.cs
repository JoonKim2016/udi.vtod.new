﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTDataPushService.Class.Object
{
    public class ESILogStatus
    {
        public const int InvalidRequest = -1;
        public const int Created= 0;
        public const int CalloutGenerated = 1;
        public const int DisabledOutbnoundNotification = -2;
    }
}
