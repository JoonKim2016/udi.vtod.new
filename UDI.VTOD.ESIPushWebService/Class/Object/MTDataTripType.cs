﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTDataPushService.Class.Object
{
    public class MTDataTripType
    {
        public const int UnknownMTDTrip = 0;
        public const int MTDTripForZTrip = 1;
        public const int MTDTripForIVR = 2;
        public const int MTDTripForIVRNotInVTOD = 3;
    }
}