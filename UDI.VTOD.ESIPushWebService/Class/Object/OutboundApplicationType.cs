﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UDI.VTOD.ESIPushWebService.Class.Object
{
    public class OutboundApplicationType
    {
        public static string ArrivalNotification = "ArrivalNotification";
        public static string NightBeforeReminder = "NightBeforeReminder";
        public static string StaleNotification = "StaleNotification";
    }
}