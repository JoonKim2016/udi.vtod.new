﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UDI.VTOD.ESIPushWebService.Class.Object
{
    public class OutboundPreferenceStatus
    {
        public static int Off = 0;
        public static int On_WithoutMarketingMessage = 1;
        public static int On_WithMarketingMessage = 2;
    }
}