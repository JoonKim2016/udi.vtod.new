﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTDataPushService.Class.Object
{
    public class ValidationCode
    {
        public const string NoError ="NoError";
        public const string InvalidMTDBookingId = "InvalidMTDBookingId";
        public const string InvalidEXTBookingRef = "InvalidEXTBookingRef";
        public const string InvalidPickupTime = "InvalidPickupTime";
        public const string InvalidPhoneNumber = "InvalidPhoneNumber";
        public const string InvalidVehicleId = "InvalidVehicleId";
    }
}