﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MTDataPushService.Class.Object;
using System.Text.RegularExpressions;
using UDI.VTOD.ESIPushWebService;
using UDI.VTOD.ESIPushWebService.BookingWebServiceReference;

namespace MTDataPushService.Class
{
    public class Validation
    {
        private ILog logger = null;
        public Validation(ILog log)
        {
            logger = log;
        }

        public string ValidateNotificaitonBookingRequest(ESIBooking Booking)
        {
            string result = ValidationCode.NoError;

            try
            {
                //add validation logic

                //TODO: need to check 
                //01. MTDBookingID
                //02. phone
                //03. PickupTime

                if (Booking.MTDBookingID<=0)
                {
                    result = ValidationCode.InvalidMTDBookingId;
                    logger.WarnFormat("InvalidMTDBookingId = {0}", Booking.MTDBookingID);
                    return result;
                }

                string phonenumber = string.Empty;
                var phone=Booking.Locations.Where(x => x.LocationType== ESIBookingLocationType.Pickup).Select(x => x.Customer.Phone).FirstOrDefault();
                if (phone.Equals(string.Empty) || !ValidateClientPhoneNumber(phone,out phonenumber))
                {
                    result = ValidationCode.InvalidPhoneNumber;
                    logger.WarnFormat("InvalidPhoneNumber. MTDBookingId={0}", Booking.MTDBookingID);
                    return result;
                }

                DateTime pickupTime = new DateTime();
                if (!Booking.Locations.Where(x => x.LocationType == ESIBookingLocationType.Pickup).Any() || !DateTime.TryParse(Booking.Locations.Where(x => x.LocationType == ESIBookingLocationType.Pickup).FirstOrDefault().PickupTime, out pickupTime))
                {
                    result = ValidationCode.InvalidPickupTime;
                    logger.WarnFormat("InvalidPickupTime. MTDBookingId={0}", Booking.MTDBookingID);
                    return result;
                }

                //string vehicleId = string.Empty;
                //if (Booking.CurrentVehicle == null)
                //{
                //    result = ValidationCode.InvalidVehicleId;
                //    logger.WarnFormat("InvalidVechileId. MTDBookingId={0}", Booking.MTDBookingID);
                //    return result;
                //}

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
            return result;
        }

        public string ValidateUpdateBookingStatusRequest(ESIBookingStatus BookingStatus)
        {
            string result = ValidationCode.NoError;

            try
            {
                //add validation logic

                //Required fields
                //01. MTDBookingID
                //02. BookingStatus

                //TODO: need to check 
                //01. Booking not found
                //02. Booking status invalid

                if (BookingStatus.MTDBookingID <= 0)
                {
                    result = ValidationCode.InvalidMTDBookingId;
                    logger.WarnFormat("InvalidMTDBookingId = {0}", BookingStatus.MTDBookingID);
                    return result;
                }

                
                //using (ESIPushServiceEntities context = new ESIPushServiceEntities())
                //{
                //    if (!context.Trips.Where(x => x.BookingId == BookingStatus.MTDBookingID).Any())
                //    {
                //        result = ValidationCode.InvalidMTDBookingId;
                //        logger.WarnFormat("Unable to find this bookingId from DB = {0}", BookingStatus.MTDBookingID);
                //        return result;
                //    }
                //}

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
            return result;
        }

        public string ValidateOutloadBookingRequest(ESIBookingIdentifier BookingIdentifier)
        {
            string result = ValidationCode.NoError;

            try
            {
                //add validation logic

                //Required fields
                //01. MTDBookingID


                //TODO: need to check 
                //01. Booking not found
                //02. Booking status invalid

                if (BookingIdentifier.MTDBookingID <= 0)
                {
                    result = ValidationCode.InvalidMTDBookingId;
                    logger.WarnFormat("InvalidMTDBookingId = {0}", BookingIdentifier.MTDBookingID);
                    return result;
                }

                using (ESIPushServiceEntities context = new ESIPushServiceEntities())
                {
                    if (!context.Trips.Where(x => x.BookingId == BookingIdentifier.MTDBookingID).Any())
                    {
                        result = ValidationCode.InvalidMTDBookingId;
                        logger.WarnFormat("Unable to find this bookingId from DB = {0}", BookingIdentifier.MTDBookingID);
                        return result;
                    }
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
            return result;
        }

        public string ValidateRingoutRequestRequest(ESIRingoutRequest RingoutRequest1)
        {
            string result = ValidationCode.NoError;

            try
            {
                //add validation logic

                //Required fields
                //01. MTDBookingID

                //TODO: need to check 
                //01. Booking not found
                //02. Booking status invalid
                //03. ContactDetailsInvalid 

                if (RingoutRequest1.BookingIdentifier.MTDBookingID <= 0)
                {
                    result = ValidationCode.InvalidMTDBookingId;
                    logger.WarnFormat("InvalidMTDBookingId = {0}", RingoutRequest1.BookingIdentifier.MTDBookingID);
                    return result;
                }

                using (ESIPushServiceEntities context = new ESIPushServiceEntities())
                {
                    if (!context.Trips.Where(x => x.BookingId == RingoutRequest1.BookingIdentifier.MTDBookingID).Any())
                    {
                        result = ValidationCode.InvalidMTDBookingId;
                        logger.WarnFormat("Unable to find this bookingId from DB = {0}", RingoutRequest1.BookingIdentifier.MTDBookingID);
                        return result;
                    }
                }


            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
            return result;
        }

        public string NotifyCheckbackResponse(ESICheckbackResponse CheckbackResponse)
        {
            string result = ValidationCode.NoError;

            try
            {
                //add validation logic

                //Required fields
                //01. MTDBookingID
                //02. EXTBookingRef
                //03. BookingStatus
                //04. RemoteReference
                //05. ResponseText

                //TODO: need to check 
                //01. Booking not found
                //02. Booking status invalid


          
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
            return result;
        }


        private bool ValidateClientPhoneNumber(string source, out string target)
        {
            bool result = false;
            target = string.Empty;
            try
            {
                source = source.Replace("-", "");
                target = Regex.Match(source, "([0-9]+)").Groups[1].Value;

                if (!string.IsNullOrWhiteSpace(target) && target.Length >= 10 && target.Length <= 11)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Message={0}", ex.Message);
                logger.ErrorFormat("InnerException={0}", ex.InnerException);
                logger.ErrorFormat("StackTrace={0}", ex.StackTrace);
            }

            return result;
        }
    }
}