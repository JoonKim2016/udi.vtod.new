﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Xml.Serialization;

namespace MTDataPushService
{
    /// <summary>
    /// Summary description for ExternalDispatchSystemIntegration
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]

  

    public class ExternalDispatchSystemIntegration : System.Web.Services.WebService, IExternalDispatchSystemIntegrationSoap
    {
     


        private ILog logger = null;
        private ProcessingModule pm=null;
        public ExternalDispatchSystemIntegration()
        {
            log4net.Config.XmlConfigurator.Configure();
            logger = LogManager.GetLogger(typeof(ExternalDispatchSystemIntegration));
            pm = new ProcessingModule(logger);
        }

        public ESIAuthenticateResult Authenticate(string SystemID, string UserName, string Password)
        {
            //no implementation on this, token is blank
            return new ESIAuthenticateResult {
                Error = ESIAuthenticateError.None,
                Succeeded = true,
                Token = string.Empty
            };
        }

        [WebMethod]
        public ESIBookingSummaryResult NotifyBooking(string AuthenticationToken, ESIBooking Booking)
        {
            ESIBookingSummaryResult result = new ESIBookingSummaryResult { Error= ESIBookingResultError.UnknownError,Succeeded=false,ErrorMessage="Unknown Error"};
            try
            {
                if (pm.AuthenicateToken(AuthenticationToken))
                {
                    result=pm.NotifyBooking(Booking);
                }
                else
                {
                    result.Error = ESIBookingResultError.NotAuthenticated;
                    result.ErrorMessage = "Invalid token";
                }
            }
            catch (Exception ex) {
                result.Error = ESIBookingResultError.UnknownError;
                result.ErrorMessage = ex.Message;
            }
            return result;
        }

        [WebMethod]
        public ESIBookingResult NotifyCheckbackResponse(string AuthenticationToken, ESICheckbackResponse CheckbackResponse)
        {
            ESIBookingResult result = new ESIBookingResult { Error = ESIBookingResultError.UnknownError, Succeeded = false, ErrorMessage = "Unknown Error" };
            try
            {
                if (pm.AuthenicateToken(AuthenticationToken))
                {
                    result = pm.NotifyCheckbackResponse(CheckbackResponse);
                }
                else
                {
                    result.Error = ESIBookingResultError.NotAuthenticated;
                    result.ErrorMessage = "Invalid token";
                }
            }
            catch (Exception ex)
            {
                result.Error = ESIBookingResultError.UnknownError;
                result.ErrorMessage = ex.Message;
            }
            return result;
        }

        [WebMethod]
        public ESIBookingResult OutloadBooking(string AuthenticationToken, ESIBookingIdentifier BookingIdentifier)
        {
            ESIBookingResult result = new ESIBookingResult { Error = ESIBookingResultError.UnknownError, Succeeded = false, ErrorMessage = "Unknown Error" };
            try
            {
                if (pm.AuthenicateToken(AuthenticationToken))
                {
                    result = pm.OutloadBooking(BookingIdentifier);
                }
                else
                {
                    result.Error = ESIBookingResultError.NotAuthenticated;
                    result.ErrorMessage = "Invalid token";
                }
            }
            catch (Exception ex)
            {
                result.Error = ESIBookingResultError.UnknownError;
                result.ErrorMessage = ex.Message;
            }
            return result;
        }

        [WebMethod]
        public ESIRingoutResult RingoutRequest(string AuthenticationToken,  ESIRingoutRequest RingoutRequest1)
        {
            ESIRingoutResult result = new ESIRingoutResult { Error = ESIRingoutResultError.UnknownError, Succeeded = false, ErrorMessage = "Unknown Error" };
            try
            {
                if (pm.AuthenicateToken(AuthenticationToken))
                {
                    result = pm.RingoutRequest(RingoutRequest1);
                }
                else
                {
                    result.Error = ESIRingoutResultError.NotAuthenticated;
                    result.ErrorMessage = "Invalid token";
                }
            }
            catch (Exception ex)
            {
                result.Error = ESIRingoutResultError.UnknownError;
                result.ErrorMessage = ex.Message;
            }
            return result;
        }

        [WebMethod]
        public ESIBookingResult UpdateBookingStatus(string AuthenticationToken, ESIBookingStatus BookingStatus)
        {
            ESIBookingResult result = new ESIBookingResult { Error = ESIBookingResultError.UnknownError, Succeeded = false, ErrorMessage = "Unknown Error" };
            try
            {
                if (pm.AuthenicateToken(AuthenticationToken))
                {
                    result = pm.UpdateBookingStatus(BookingStatus);
                }
                else
                {
                    result.Error = ESIBookingResultError.NotAuthenticated;
                    result.ErrorMessage = "Invalid token";
                }
            }
            catch (Exception ex)
            {
                result.Error = ESIBookingResultError.UnknownError;
                result.ErrorMessage = ex.Message;
            }
            return result;
        }
    }
}
