﻿using System.Diagnostics;
using System.Web.Services;
using System.ComponentModel;
using System.Web.Services.Protocols;
using System;
using System.Xml.Serialization;

namespace MTDataPushService
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Web.Services.WebServiceBindingAttribute(Name = "ExternalDispatchSystemIntegrationSoap", Namespace = "http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration")]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ESIBaseResult))]
    public interface IExternalDispatchSystemIntegrationSoap
    {

        /// <remarks/>
        [System.Web.Services.WebMethodAttribute()]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration/Au" +
            "thenticate", RequestNamespace = "http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration", ResponseNamespace = "http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        ESIAuthenticateResult Authenticate(string SystemID, string UserName, string Password);

        /// <remarks/>
        [System.Web.Services.WebMethodAttribute()]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration/No" +
            "tifyBooking", RequestNamespace = "http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration", ResponseNamespace = "http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        ESIBookingSummaryResult NotifyBooking(string AuthenticationToken, ESIBooking Booking);

        /// <remarks/>
        [System.Web.Services.WebMethodAttribute()]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration/Up" +
            "dateBookingStatus", RequestNamespace = "http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration", ResponseNamespace = "http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        ESIBookingResult UpdateBookingStatus(string AuthenticationToken, ESIBookingStatus BookingStatus);

        /// <remarks/>
        [System.Web.Services.WebMethodAttribute()]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration/Ou" +
            "tloadBooking", RequestNamespace = "http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration", ResponseNamespace = "http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        ESIBookingResult OutloadBooking(string AuthenticationToken, ESIBookingIdentifier BookingIdentifier);

        /// <remarks/>
        [System.Web.Services.WebMethodAttribute()]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration/No" +
            "tifyCheckbackResponse", RequestNamespace = "http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration", ResponseNamespace = "http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        ESIBookingResult NotifyCheckbackResponse(string AuthenticationToken, ESICheckbackResponse CheckbackResponse);

        /// <remarks/>
        [System.Web.Services.WebMethodAttribute()]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration/Ri" +
            "ngoutRequest", RequestNamespace = "http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration", ResponseNamespace = "http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        ESIRingoutResult RingoutRequest(string AuthenticationToken, [System.Xml.Serialization.XmlElementAttribute("RingoutRequest")] ESIRingoutRequest RingoutRequest1);
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration")]
    public partial class ESIAuthenticateResult : ESIBaseResult
    {

        private string tokenField;

        private ESIAuthenticateError errorField;

        /// <remarks/>
        public string Token
        {
            get
            {
                return this.tokenField;
            }
            set
            {
                this.tokenField = value;
            }
        }

        /// <remarks/>
        public ESIAuthenticateError Error
        {
            get
            {
                return this.errorField;
            }
            set
            {
                this.errorField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration")]
    public enum ESIAuthenticateError
    {

        /// <remarks/>
        None,

        /// <remarks/>
        NotAuthenticated,

        /// <remarks/>
        WebMethodAccessDenied,

        /// <remarks/>
        UnknownError,

        /// <remarks/>
        InvalidCredentials,
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ESIRingoutResult))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ESIBookingResult))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ESIBookingSummaryResult))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ESIAuthenticateResult))]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration")]
    public partial class ESIBaseResult
    {

        private string errorMessageField;

        private bool succeededField;

        /// <remarks/>
        public string ErrorMessage
        {
            get
            {
                return this.errorMessageField;
            }
            set
            {
                this.errorMessageField = value;
            }
        }

        /// <remarks/>
        public bool Succeeded
        {
            get
            {
                return this.succeededField;
            }
            set
            {
                this.succeededField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration")]
    public partial class ESIRingoutRequest
    {

        private ESIBookingIdentifier bookingIdentifierField;

        private int sequenceNumberField;

        private ESIRingoutRequestType calloutTypeField;

        /// <remarks/>
        public ESIBookingIdentifier BookingIdentifier
        {
            get
            {
                return this.bookingIdentifierField;
            }
            set
            {
                this.bookingIdentifierField = value;
            }
        }

        /// <remarks/>
        public int SequenceNumber
        {
            get
            {
                return this.sequenceNumberField;
            }
            set
            {
                this.sequenceNumberField = value;
            }
        }

        /// <remarks/>
        public ESIRingoutRequestType CalloutType
        {
            get
            {
                return this.calloutTypeField;
            }
            set
            {
                this.calloutTypeField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ESIBookingStatus))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ESIBooking))]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration")]
    public partial class ESIBookingIdentifier
    {

        private long mTDBookingIDField;

        private string eXTBookingRefField;

        /// <remarks/>
        public long MTDBookingID
        {
            get
            {
                return this.mTDBookingIDField;
            }
            set
            {
                this.mTDBookingIDField = value;
            }
        }

        /// <remarks/>
        public string EXTBookingRef
        {
            get
            {
                return this.eXTBookingRefField;
            }
            set
            {
                this.eXTBookingRefField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration")]
    public partial class ESIBookingStatus : ESIBookingIdentifier
    {

        private ESIMTDBookingStatus bookingStatusField;

        private long mTDNewBookingIDField;

        private ESIBookingDriver driverField;

        private ESIBookingVehicle vehicleField;

        /// <remarks/>
        public ESIMTDBookingStatus BookingStatus
        {
            get
            {
                return this.bookingStatusField;
            }
            set
            {
                this.bookingStatusField = value;
            }
        }

        /// <remarks/>
        public long MTDNewBookingID
        {
            get
            {
                return this.mTDNewBookingIDField;
            }
            set
            {
                this.mTDNewBookingIDField = value;
            }
        }

        /// <remarks/>
        public ESIBookingDriver Driver
        {
            get
            {
                return this.driverField;
            }
            set
            {
                this.driverField = value;
            }
        }

        /// <remarks/>
        public ESIBookingVehicle Vehicle
        {
            get
            {
                return this.vehicleField;
            }
            set
            {
                this.vehicleField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration")]
    public enum ESIMTDBookingStatus
    {

        /// <remarks/>
        Dispatch,

        /// <remarks/>
        Accepted,

        /// <remarks/>
        Recalled,

        /// <remarks/>
        PickedUp,

        /// <remarks/>
        Complete,

        /// <remarks/>
        NoJob,

        /// <remarks/>
        NoCarAvailable,

        /// <remarks/>
        CoveredByAlternate,

        /// <remarks/>
        Cancelled,

        /// <remarks/>
        Rebooked,

        /// <remarks/>
        AutoDiscard,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration")]
    public partial class ESIBookingDriver
    {

        private int driverIDField;

        private string driverNumberField;

        private string driverNameField;

        private string driverSurnameField;

        private string displayNameField;

        private ESINameIDPairing[] driverConditionsField;

        /// <remarks/>
        public int DriverID
        {
            get
            {
                return this.driverIDField;
            }
            set
            {
                this.driverIDField = value;
            }
        }

        /// <remarks/>
        public string DriverNumber
        {
            get
            {
                return this.driverNumberField;
            }
            set
            {
                this.driverNumberField = value;
            }
        }

        /// <remarks/>
        public string DriverName
        {
            get
            {
                return this.driverNameField;
            }
            set
            {
                this.driverNameField = value;
            }
        }

        /// <remarks/>
        public string DriverSurname
        {
            get
            {
                return this.driverSurnameField;
            }
            set
            {
                this.driverSurnameField = value;
            }
        }

        /// <remarks/>
        public string DisplayName
        {
            get
            {
                return this.displayNameField;
            }
            set
            {
                this.displayNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Condition")]
        public ESINameIDPairing[] DriverConditions
        {
            get
            {
                return this.driverConditionsField;
            }
            set
            {
                this.driverConditionsField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration")]
    public partial class ESINameIDPairing
    {

        private int idField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int ID
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration")]
    public partial class ESIBookingVehicle
    {

        private int vehicleIDField;

        private string carNumberField;

        private string taxiLicenceNumberField;

        private ESINameIDPairing[] vehicleConditionsField;

        private ESINameIDPairing[] workingConditionsField;

        /// <remarks/>
        public int VehicleID
        {
            get
            {
                return this.vehicleIDField;
            }
            set
            {
                this.vehicleIDField = value;
            }
        }

        /// <remarks/>
        public string CarNumber
        {
            get
            {
                return this.carNumberField;
            }
            set
            {
                this.carNumberField = value;
            }
        }

        /// <remarks/>
        public string TaxiLicenceNumber
        {
            get
            {
                return this.taxiLicenceNumberField;
            }
            set
            {
                this.taxiLicenceNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Condition")]
        public ESINameIDPairing[] VehicleConditions
        {
            get
            {
                return this.vehicleConditionsField;
            }
            set
            {
                this.vehicleConditionsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Condition")]
        public ESINameIDPairing[] WorkingConditions
        {
            get
            {
                return this.workingConditionsField;
            }
            set
            {
                this.workingConditionsField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration")]
    public partial class ESIBooking : ESIBookingIdentifier
    {

        private ESIBookingAccount accountField;

        private int areaNumberField;

        private ESIBookingContact contactField;

        private string bookingRemarksField;

        private ESINameIDPairing cityField;

        private ESINameIDPairing fleetField;

        private int gradingField;

        private ESINameIDPairing productField;

        private ESINameIDPairing serviceField;

        private bool timeBookingField;

        private ESIBookingLocation[] locationsField;

        private ESINameIDPairing[] bookingConditionsField;

        private ESIBookingDriver currentDriverField;

        private ESIBookingVehicle currentVehicleField;

        private string createdDateField;

        private ESINameIDPairing createdOperatorField;

        private string requestedDateField;

        /// <remarks/>
        public ESIBookingAccount Account
        {
            get
            {
                return this.accountField;
            }
            set
            {
                this.accountField = value;
            }
        }

        /// <remarks/>
        public int AreaNumber
        {
            get
            {
                return this.areaNumberField;
            }
            set
            {
                this.areaNumberField = value;
            }
        }

        /// <remarks/>
        public ESIBookingContact Contact
        {
            get
            {
                return this.contactField;
            }
            set
            {
                this.contactField = value;
            }
        }

        /// <remarks/>
        public string BookingRemarks
        {
            get
            {
                return this.bookingRemarksField;
            }
            set
            {
                this.bookingRemarksField = value;
            }
        }

        /// <remarks/>
        public ESINameIDPairing City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        public ESINameIDPairing Fleet
        {
            get
            {
                return this.fleetField;
            }
            set
            {
                this.fleetField = value;
            }
        }

        /// <remarks/>
        public int Grading
        {
            get
            {
                return this.gradingField;
            }
            set
            {
                this.gradingField = value;
            }
        }

        /// <remarks/>
        public ESINameIDPairing Product
        {
            get
            {
                return this.productField;
            }
            set
            {
                this.productField = value;
            }
        }

        /// <remarks/>
        public ESINameIDPairing Service
        {
            get
            {
                return this.serviceField;
            }
            set
            {
                this.serviceField = value;
            }
        }

        /// <remarks/>
        public bool TimeBooking
        {
            get
            {
                return this.timeBookingField;
            }
            set
            {
                this.timeBookingField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Location")]
        public ESIBookingLocation[] Locations
        {
            get
            {
                return this.locationsField;
            }
            set
            {
                this.locationsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Condition")]
        public ESINameIDPairing[] BookingConditions
        {
            get
            {
                return this.bookingConditionsField;
            }
            set
            {
                this.bookingConditionsField = value;
            }
        }

        /// <remarks/>
        public ESIBookingDriver CurrentDriver
        {
            get
            {
                return this.currentDriverField;
            }
            set
            {
                this.currentDriverField = value;
            }
        }

        /// <remarks/>
        public ESIBookingVehicle CurrentVehicle
        {
            get
            {
                return this.currentVehicleField;
            }
            set
            {
                this.currentVehicleField = value;
            }
        }

        /// <remarks/>
        public string CreatedDate
        {
            get
            {
                return this.createdDateField;
            }
            set
            {
                this.createdDateField = value;
            }
        }

        /// <remarks/>
        public ESINameIDPairing CreatedOperator
        {
            get
            {
                return this.createdOperatorField;
            }
            set
            {
                this.createdOperatorField = value;
            }
        }

        /// <remarks/>
        public string RequestedDate
        {
            get
            {
                return this.requestedDateField;
            }
            set
            {
                this.requestedDateField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration")]
    public partial class ESIBookingAccount
    {

        private int accountIDField;

        private string accountNumberField;

        private string accountNameField;

        private string orderNumberField;

        private string costCentreField;

        /// <remarks/>
        public int AccountID
        {
            get
            {
                return this.accountIDField;
            }
            set
            {
                this.accountIDField = value;
            }
        }

        /// <remarks/>
        public string AccountNumber
        {
            get
            {
                return this.accountNumberField;
            }
            set
            {
                this.accountNumberField = value;
            }
        }

        /// <remarks/>
        public string AccountName
        {
            get
            {
                return this.accountNameField;
            }
            set
            {
                this.accountNameField = value;
            }
        }

        /// <remarks/>
        public string OrderNumber
        {
            get
            {
                return this.orderNumberField;
            }
            set
            {
                this.orderNumberField = value;
            }
        }

        /// <remarks/>
        public string CostCentre
        {
            get
            {
                return this.costCentreField;
            }
            set
            {
                this.costCentreField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration")]
    public partial class ESIBookingContact
    {

        private string nameField;

        private string phoneField;

        private string eMailField;

        /// <remarks/>
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public string Phone
        {
            get
            {
                return this.phoneField;
            }
            set
            {
                this.phoneField = value;
            }
        }

        /// <remarks/>
        public string EMail
        {
            get
            {
                return this.eMailField;
            }
            set
            {
                this.eMailField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration")]
    public partial class ESIBookingLocation
    {

        private ESIBookingLocationType locationTypeField;

        private int sequenceNumberField;

        private ESIBookingContact customerField;

        private string pickupTimeField;

        private ESIBookingLocationCalloutOptions calloutOptionsField;

        private ESIBookingAddress addressField;

        private string remarksField;

        private int pAXField;

        /// <remarks/>
        public ESIBookingLocationType LocationType
        {
            get
            {
                return this.locationTypeField;
            }
            set
            {
                this.locationTypeField = value;
            }
        }

        /// <remarks/>
        public int SequenceNumber
        {
            get
            {
                return this.sequenceNumberField;
            }
            set
            {
                this.sequenceNumberField = value;
            }
        }

        /// <remarks/>
        public ESIBookingContact Customer
        {
            get
            {
                return this.customerField;
            }
            set
            {
                this.customerField = value;
            }
        }

        /// <remarks/>
        public string PickupTime
        {
            get
            {
                return this.pickupTimeField;
            }
            set
            {
                this.pickupTimeField = value;
            }
        }

        /// <remarks/>
        public ESIBookingLocationCalloutOptions CalloutOptions
        {
            get
            {
                return this.calloutOptionsField;
            }
            set
            {
                this.calloutOptionsField = value;
            }
        }

        /// <remarks/>
        public ESIBookingAddress Address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>
        public string Remarks
        {
            get
            {
                return this.remarksField;
            }
            set
            {
                this.remarksField = value;
            }
        }

        /// <remarks/>
        public int PAX
        {
            get
            {
                return this.pAXField;
            }
            set
            {
                this.pAXField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration")]
    public enum ESIBookingLocationType
    {

        /// <remarks/>
        Pickup,

        /// <remarks/>
        Dropoff,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration")]
    public partial class ESIBookingLocationCalloutOptions
    {

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration")]
    public partial class ESIBookingAddress
    {

        private ESINameIDPairing placeField;

        private string unitField;

        private string numberField;

        private ESINameIDPairing streetField;

        private ESINameIDPairing designationField;

        private ESINameIDPairing standardDesignationField;

        private ESINameIDPairing suburbField;

        /// <remarks/>
        public ESINameIDPairing Place
        {
            get
            {
                return this.placeField;
            }
            set
            {
                this.placeField = value;
            }
        }

        /// <remarks/>
        public string Unit
        {
            get
            {
                return this.unitField;
            }
            set
            {
                this.unitField = value;
            }
        }

        /// <remarks/>
        public string Number
        {
            get
            {
                return this.numberField;
            }
            set
            {
                this.numberField = value;
            }
        }

        /// <remarks/>
        public ESINameIDPairing Street
        {
            get
            {
                return this.streetField;
            }
            set
            {
                this.streetField = value;
            }
        }

        /// <remarks/>
        public ESINameIDPairing Designation
        {
            get
            {
                return this.designationField;
            }
            set
            {
                this.designationField = value;
            }
        }

        /// <remarks/>
        public ESINameIDPairing StandardDesignation
        {
            get
            {
                return this.standardDesignationField;
            }
            set
            {
                this.standardDesignationField = value;
            }
        }

        /// <remarks/>
        public ESINameIDPairing Suburb
        {
            get
            {
                return this.suburbField;
            }
            set
            {
                this.suburbField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration")]
    public enum ESIRingoutRequestType
    {

        /// <remarks/>
        COA,

        /// <remarks/>
        MOA,

        /// <remarks/>
        CCO,

        /// <remarks/>
        EOA,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration")]
    public partial class ESICheckbackResponse
    {

        private string remoteReferenceField;

        private ESIBookingIdentifier bookingIdentifierField;

        private string responseTextField;

        /// <remarks/>
        public string RemoteReference
        {
            get
            {
                return this.remoteReferenceField;
            }
            set
            {
                this.remoteReferenceField = value;
            }
        }

        /// <remarks/>
        public ESIBookingIdentifier BookingIdentifier
        {
            get
            {
                return this.bookingIdentifierField;
            }
            set
            {
                this.bookingIdentifierField = value;
            }
        }

        /// <remarks/>
        public string ResponseText
        {
            get
            {
                return this.responseTextField;
            }
            set
            {
                this.responseTextField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration")]
    public partial class ESIRingoutResult : ESIBaseResult
    {

        private ESIRingoutResultError errorField;

        /// <remarks/>
        public ESIRingoutResultError Error
        {
            get
            {
                return this.errorField;
            }
            set
            {
                this.errorField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration")]
    public enum ESIRingoutResultError
    {

        /// <remarks/>
        None,

        /// <remarks/>
        NotAuthenticated,

        /// <remarks/>
        MebMethodAccessDenied,

        /// <remarks/>
        UnknownError,

        /// <remarks/>
        BookingNotFound,

        /// <remarks/>
        ContactDetailsInvalid,
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ESIBookingSummaryResult))]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration")]
    public partial class ESIBookingResult : ESIBaseResult
    {

        private ESIBookingResultError errorField;

        /// <remarks/>
        public ESIBookingResultError Error
        {
            get
            {
                return this.errorField;
            }
            set
            {
                this.errorField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration")]
    public enum ESIBookingResultError
    {

        /// <remarks/>
        None,

        /// <remarks/>
        NotAuthenticated,

        /// <remarks/>
        MebMethodAccessDenied,

        /// <remarks/>
        UnknownError,

        /// <remarks/>
        BookingNotFound,

        /// <remarks/>
        BookingStatusInvalid,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://schemas.mtdata.com.au/external/2.23.0/ExternalDispatchSystemIntegration")]
    public partial class ESIBookingSummaryResult : ESIBookingResult
    {

        private ESIBookingIdentifier bookingIdentifierField;

        /// <remarks/>
        public ESIBookingIdentifier BookingIdentifier
        {
            get
            {
                return this.bookingIdentifierField;
            }
            set
            {
                this.bookingIdentifierField = value;
            }
        }
    }
}
