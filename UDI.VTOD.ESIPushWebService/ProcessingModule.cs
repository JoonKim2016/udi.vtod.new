﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HelperClass;
using MTDataPushService.Class;
using MTDataPushService.Class.Object;
using System.Configuration;
using UDI.VTOD.ESIPushWebService;
using UDI.VTOD.DataAccess.VTOD;
using UDI.VTOD.ESIPushWebService.BookingWebServiceReference;
using UDI.VTOD.ESIPushWebService.Class.Object;
using UDI.VTOD.ESIPushWebService.OSIWebServiceReference;

namespace MTDataPushService
{
    public class ProcessingModule
    {
        private static readonly object ObjSync = new object();
        private static readonly string IVR_UserName = ConfigurationManager.AppSettings["MTData_IVR_Username"];
        private static readonly string IVR_Password = ConfigurationManager.AppSettings["MTData_IVR_Password"];
        private static readonly string IVR_SystemID = ConfigurationManager.AppSettings["MTData_IVR_SystemID"];

        

        //private string timeZone = TimeZone.CurrentTimeZone.ToString();
        private ILog logger = null;
        private Validation v = null;
        public ProcessingModule(ILog log)
        {
            logger = log;
            v = new Validation(logger);
            //timeZone = ConfigurationManager.AppSettings["TimeZone"];

            

        }

        public bool AuthenicateToken(string AuthenticationToken)
        {
            return true;
        }

        public ESIBookingSummaryResult NotifyBooking(ESIBooking Booking)
        {
            logger.InfoFormat("in notifiybooking");
            ESIBookingSummaryResult result = new ESIBookingSummaryResult();
            ESILog esiLog = new ESILog { AppendTime = DateTime.Now, IsAbleToDeserializeObject = false, Method = System.Reflection.MethodBase.GetCurrentMethod().Name, Status = ESILogStatus.Created };

            try
            {
                #region serialize request
                string request = Booking.Serialize();
                esiLog.Request = request;
                esiLog.IsAbleToDeserializeObject = true;
                #endregion

                #region validate request
                if (v.ValidateNotificaitonBookingRequest(Booking) == ValidationCode.NoError)
                {

                    //int tripType = GetMTDataTripType(long.Parse(Booking.EXTBookingRef), Booking.MTDBookingID.ToString(), Booking.Contact.Phone);
                    string phone=Booking.Locations.Where(x => x.LocationType== ESIBookingLocationType.Pickup).Select(x=>x.Customer.Phone).FirstOrDefault();
                    int? dispatchFleetId = (Booking.Fleet!=null)?Booking.Fleet.ID:(int?)null;
                    long? vtodId = string.IsNullOrEmpty(Booking.EXTBookingRef)? (long?)null : Convert.ToInt64(Booking.EXTBookingRef);
                    int tripType = GetMTDataTripType(vtodId, Booking.MTDBookingID.ToString(), phone,dispatchFleetId );

                    #region insert trip to ESIPushService
                    using (ESIPushServiceEntities context = new ESIPushServiceEntities())
                    {
                        #region convert UTC time to local time
                        DateTime dt = DateTime.Parse(Booking.Locations.Where(x => x.LocationType == ESIBookingLocationType.Pickup).FirstOrDefault().PickupTime);
                        //TimeZoneInfo zone = TimeZoneInfo.FindSystemTimeZoneById(timeZone);
                        //DateTime currentTime = TimeZoneInfo.ConvertTimeFromUtc(dt, zone);
                        //Just save UTC time, there is no requirement for time
                        #endregion

                        //if the trip is booked from Ztrip, skip that
                        #region save esilog and trip
                        esiLog.Status = ESILogStatus.Created;
                        if (tripType == MTDataTripType.MTDTripForIVR || tripType == MTDataTripType.MTDTripForIVRNotInVTOD || tripType == MTDataTripType.MTDTripForZTrip)
                        {
                            Trip t=context.Trips.Where(x => x.BookingId == Booking.MTDBookingID).FirstOrDefault();
                            if (t != null)
                            {
                                //existing trip
                                if (Booking.CurrentDriver != null)
                                {
                                    t.DriverId = Booking.CurrentDriver.DriverID.ToString();
                                }

                                if (Booking.CurrentVehicle != null)
                                {
                                    t.VehicleId = Booking.CurrentVehicle.VehicleID.ToString();
                                }

                                t.DispatchFleetId = dispatchFleetId;
                            }
                            else
                            {
                                //new trip

                                t = new Trip {
                                    AppendTime = DateTime.Now,
                                    PhoneNumber = phone,
                                    PickupTime = dt,
                                    BookingId = Booking.MTDBookingID,
                                    EXTBookingRef = vtodId.HasValue ? vtodId.Value.ToString() : null,
                                    TripType = tripType,
                                    VTODID = vtodId,
                                    DispatchFleetId=dispatchFleetId
                                    
                                };

                                if (Booking.CurrentDriver != null)
                                {
                                    t.DriverId = Booking.CurrentDriver.DriverID.ToString();
                                }

                                if (Booking.CurrentVehicle != null)
                                {
                                    t.VehicleId = Booking.CurrentVehicle.VehicleID.ToString();
                                }

                                esiLog.Trips.Add(t);

                            }
                        }
                        context.ESILogs.Add(esiLog);
                        context.SaveChanges();
                        #endregion

                    }
                    #endregion

                    #region make response
                    result.BookingIdentifier = new ESIBookingIdentifier { MTDBookingID = Booking.MTDBookingID, EXTBookingRef = string.IsNullOrEmpty(Booking.EXTBookingRef)?string.Empty: Booking.EXTBookingRef };
                    
                    result.Error = ESIBookingResultError.None;
                    result.ErrorMessage = string.Empty;
                    result.Succeeded = true;
                    #endregion
                }
                else {
                    result.Error = ESIBookingResultError.UnknownError;
                    result.ErrorMessage = "Invalid request";
                }
                #endregion
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                result.Error = ESIBookingResultError.UnknownError;
                result.ErrorMessage = ex.Message;
            }

            #region save esi log when there is an exception
            if (esiLog.Id == 0)
            {
                try
                {
                    using (ESIPushServiceEntities context = new ESIPushServiceEntities())
                    {
                        esiLog.Status = ESILogStatus.InvalidRequest;
                        context.ESILogs.Add(esiLog);
                        context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message);
                    result.Error = ESIBookingResultError.UnknownError;
                    result.ErrorMessage = ex.Message;
                }
            }
            #endregion

            return result;
        }

        public ESIBookingResult UpdateBookingStatus(ESIBookingStatus BookingStatus)
        {
           // string requestMsg = System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString();
            //if (System.ServiceModel.OperationContext.Current != null) { logger.InfoFormat("SOAP:OperationContext.Current.RequestContext.RequestMessage=\r\n{0}\r\n", requestMsg); }

            ESIBookingResult result = new ESIBookingResult();
            ESILog esiLog = new ESILog { AppendTime = DateTime.Now, IsAbleToDeserializeObject = false, Method = System.Reflection.MethodBase.GetCurrentMethod().Name, Status = ESILogStatus.Created };
            string dispatchFleetID = "";
            try
            {
                #region serialize request
                string request = BookingStatus.Serialize();
                esiLog.Request = request;
                esiLog.IsAbleToDeserializeObject = true;
                #endregion

                
                long? vtodId = string.IsNullOrEmpty(BookingStatus.EXTBookingRef) ? (long?)null : Convert.ToInt64(BookingStatus.EXTBookingRef);
                int tripType = GetMTDataTripType(vtodId, BookingStatus.MTDBookingID.ToString(),out dispatchFleetID);

                #region validate request
                if (v.ValidateUpdateBookingStatusRequest(BookingStatus) == ValidationCode.NoError)
                {
                    #region update trip status for VTOD
                    vtod_trip vtodTrip = null;
                    if (tripType == MTDataTripType.MTDTripForZTrip || tripType == MTDataTripType.MTDTripForIVR)
                    {
                        if (!string.IsNullOrEmpty(BookingStatus.EXTBookingRef))
                        {
                            UpdateVTODTaxiTripStatus(Convert.ToInt64(BookingStatus.EXTBookingRef), BookingStatus.MTDBookingID.ToString(), BookingStatus.BookingStatus.ToString());
                            vtodTrip = GetVTODTripByVTODId(Convert.ToInt64(BookingStatus.EXTBookingRef), BookingStatus.MTDBookingID.ToString());
                        }
                    }
                    #endregion

                    #region update trip status for ESI DB
                    using (ESIPushServiceEntities context = new ESIPushServiceEntities())
                    {
                        esiLog.Status = ESILogStatus.Created;
                        context.ESILogs.Add(esiLog);

                        if (tripType == MTDataTripType.MTDTripForIVR || tripType == MTDataTripType.MTDTripForIVRNotInVTOD || tripType == MTDataTripType.MTDTripForZTrip)
                        {

                            var bookingInformation=GetBookingInformation(GetToken(), BookingStatus.MTDBookingID.ToString());

                            var trip = context.Trips.Where(x => x.BookingId == BookingStatus.MTDBookingID).FirstOrDefault();
                            if (trip == null)
                            {
                                #region new trip
                                trip = new Trip { Status = BookingStatus.BookingStatus.ToString(), ESILogId = esiLog.Id, BookingId = BookingStatus.MTDBookingID, AppendTime = DateTime.Now, TripType = tripType, EXTBookingRef = BookingStatus.EXTBookingRef };

                                if (bookingInformation != null) {
                                    trip.PhoneNumber = bookingInformation.Booking.Contact.Number;
                                    DateTime dt;
                                    if (DateTime.TryParse(bookingInformation.Booking.PickupTime, out dt) && (DateTime.Now.AddDays(-30).CompareTo(dt)<=0) && (DateTime.Now.AddDays(30).CompareTo(dt) >= 0))
                                    {
                                        logger.InfoFormat("Update pickupdate time to {0}", dt.ToString("yyyy/MM/ddTHH:mm:ss"));
                                        trip.PickupTime = dt;
                                    }

                                }

                                if (!string.IsNullOrWhiteSpace(BookingStatus.MTDBookingID.ToString()) && !string.IsNullOrWhiteSpace(BookingStatus.EXTBookingRef)) {
                                    trip.VTODID = GetVTODId(BookingStatus.MTDBookingID.ToString(), Convert.ToInt64(BookingStatus.EXTBookingRef));
                                }

                                if (vtodTrip != null)
                                {
                                    trip.PhoneNumber = vtodTrip.taxi_trip.PhoneNumber;
                                    trip.PickupTime = vtodTrip.PickupDateTime;
                                }

                                if (BookingStatus.MTDNewBookingID > 0)
                                {
                                    trip.NewBookingId = BookingStatus.MTDNewBookingID;
                                }

                                if (BookingStatus.Driver != null)
                                {
                                    trip.DriverId = BookingStatus.Driver.DriverID.ToString();
                                }

                                if (BookingStatus.Vehicle != null)
                                {
                                    trip.VehicleId = BookingStatus.Vehicle.VehicleID.ToString();
                                }

                                context.Trips.Add(trip);

                                #endregion

                            }
                            else
                            {
                                #region existing trip
                                trip.Status = BookingStatus.BookingStatus.ToString();

                                if (BookingStatus.MTDNewBookingID > 0)
                                {
                                    trip.NewBookingId = BookingStatus.MTDNewBookingID;
                                }

                                if (BookingStatus.Driver != null)
                                {
                                    trip.DriverId = BookingStatus.Driver.DriverID.ToString();
                                }

                                if (BookingStatus.Vehicle != null)
                                {
                                    trip.VehicleId = BookingStatus.Vehicle.VehicleID.ToString();
                                }
                                #endregion
                            }
                        }
                        context.SaveChanges();
                    }
                    #endregion


                    #region make response
                    result.Error = ESIBookingResultError.None;
                    result.ErrorMessage = string.Empty;
                    result.Succeeded = true;
                    #endregion
                }
                else {
                    result.Error = ESIBookingResultError.UnknownError;
                    result.ErrorMessage = "Invalid request";
                }
                #endregion
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                logger.Error(ex.InnerException.Message);
                logger.Error(ex.StackTrace.ToString());
                result.Error = ESIBookingResultError.UnknownError;
                result.ErrorMessage = ex.Message;
            }

            #region save esi log when there is an exception
            if (esiLog.Id == 0)
            {
                try
                {
                    using (ESIPushServiceEntities context = new ESIPushServiceEntities())
                    {
                        esiLog.Status = ESILogStatus.InvalidRequest;
                        context.ESILogs.Add(esiLog);
                        context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message);
                    result.Error = ESIBookingResultError.UnknownError;
                    result.ErrorMessage = ex.Message;
                }
            }
            #endregion

            return result;

        }


        public ESIBookingResult OutloadBooking(ESIBookingIdentifier BookingIdentifier)
        {
            List<OutboundNotificationPreference> preferences = LoadOutboundNotificationPreference();
            ESIBookingResult result = new ESIBookingResult();
            ESILog esiLog = new ESILog { AppendTime = DateTime.Now, IsAbleToDeserializeObject = false, Method = System.Reflection.MethodBase.GetCurrentMethod().Name, Status = ESILogStatus.Created };
            bool EnableOutboundWhiteList =Convert.ToBoolean(ConfigurationManager.AppSettings["EnableOutboundWhiteList"]);
            List<string> OutboundWhiteList = ConfigurationManager.AppSettings["EnableOutboundWhiteList"].Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).ToList();
            bool enableTheCallout = true;
            string dispatchFleetID = "";




            try
            {
                #region serialize request
                string request = BookingIdentifier.Serialize();
                esiLog.Request = request;
                esiLog.IsAbleToDeserializeObject = true;
                #endregion

                #region validate request
                if (v.ValidateOutloadBookingRequest(BookingIdentifier) == ValidationCode.NoError)
                {
                    long? vtodId = string.IsNullOrEmpty(BookingIdentifier.EXTBookingRef) ? (long?)null : Convert.ToInt64(BookingIdentifier.EXTBookingRef);
                    int tripType = GetMTDataTripType(vtodId, BookingIdentifier.MTDBookingID.ToString(),out dispatchFleetID);
                    if (tripType == MTDataTripType.MTDTripForIVR || tripType == MTDataTripType.MTDTripForIVRNotInVTOD)
                    {

                        #region add a esilog, call, attempt, esilog_trip_call
                        using (ESIPushServiceEntities context = new ESIPushServiceEntities())
                        {
                            esiLog.Status = ESILogStatus.DisabledOutbnoundNotification;

                            //get trip
                            var trip = context.Trips.Where(x => x.BookingId == BookingIdentifier.MTDBookingID).FirstOrDefault();

                            //whitelist
                            if ((EnableOutboundWhiteList) && (OutboundWhiteList.Where(x => x == trip.PhoneNumber).Any()))
                            {
                                enableTheCallout = true;
                            }
                            else if (!EnableOutboundWhiteList)
                            {
                                enableTheCallout = true;
                            }
                            else {
                                enableTheCallout = false;
                            }


                            //add esilog
                            esiLog.Status = ESILogStatus.Created;


                            //get MTDFleet
                            //string dispatchFleetId = GetDispatchFleetId(GetToken(), trip.PhoneNumber, GetDispatchFleetIdsForIVR());
                            string dispatchFleetId = GetDispatchFleetIdByMTDBookingId(GetToken(), BookingIdentifier.MTDBookingID.ToString(), GetDispatchFleetIdsForIVR());


                            var pList = preferences.Where(x => x.ESIFleetId == Convert.ToInt64(dispatchFleetId) && x.ApplicationId==3).ToList();

                            if (pList != null && pList.Any() && pList.Where(x => x.CallType == 1 && (x.Status == 1||x.Status==2)).Any() && enableTheCallout)
                            {
                                if (!CheckDuplication(trip.Id, CallType.Voice,3))
                                {

                                    //create voice outbound notification
                                    OutboundNotificationPreference p = pList.Where(x => x.CallType == 1 && (x.Status == 1 || x.Status == 2)).FirstOrDefault();
                                    var voicecallId = InjectVoiceCall(OutboundApplicationType.StaleNotification, trip, p);
                                    context.ESILog_Trip_Call.Add(new ESILog_Trip_Call { AppendTime = DateTime.Now, ESILogId = esiLog.Id, TripId = trip.Id, CallId = voicecallId, CallType = CallType.Voice, OutboundSystemId = 1, ApplicationId = 3 });
                                    esiLog.Status = ESILogStatus.CalloutGenerated;
                                }
                                else {
                                    logger.Warn("Duplicated Voice outbound notification. Ignore this ringout.");
                                }

                            }


                            if (pList != null && pList.Any() && pList.Where(x => x.CallType == 2 && (x.Status == 1 || x.Status == 2)).Any() && enableTheCallout)
                            {
                                //prevent duplicate ringout
                                if (!CheckDuplication(trip.Id,CallType.SMS,3))
                                {
                                    //create sms outbound notification
                                    OutboundNotificationPreference p = pList.Where(x => x.CallType == 2 && (x.Status == 1 || x.Status == 2)).FirstOrDefault();
                                    var smscallId = InjectSMSCall(OutboundApplicationType.StaleNotification, trip, p);
                                    context.ESILog_Trip_Call.Add(new ESILog_Trip_Call { AppendTime = DateTime.Now, ESILogId = esiLog.Id, TripId = trip.Id, CallId = smscallId, CallType = CallType.SMS, OutboundSystemId = 1, ApplicationId = 3});
                                    esiLog.Status = ESILogStatus.CalloutGenerated;
                                }
                                else{
                                    logger.Warn("Duplicated SMS outbound notification. Ignore this ringout.");
                                }
                                
                            }


                           

                            //save
                            context.ESILogs.Add(esiLog);
                            context.SaveChanges();

                        }
                        #endregion
                    }


                    #region make response
                    result.Error = ESIBookingResultError.None;
                    result.ErrorMessage = string.Empty;
                    result.Succeeded = true;
                    #endregion
                }
                else {
                    result.Error = ESIBookingResultError.UnknownError;
                    result.ErrorMessage = "Invalid request";
                }
                #endregion
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                logger.Error(ex.InnerException.Message);
                logger.Error(ex.StackTrace.ToString());
                result.Error = ESIBookingResultError.UnknownError;
                result.ErrorMessage = ex.Message;
            }

            #region save esi log when there is an exception
            if (esiLog.Id == 0)
            {
                try
                {
                    using (ESIPushServiceEntities context = new ESIPushServiceEntities())
                    {
                        esiLog.Status = ESILogStatus.InvalidRequest;
                        context.ESILogs.Add(esiLog);
                        context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message);
                    result.Error = ESIBookingResultError.UnknownError;
                    result.ErrorMessage = ex.Message;
                }
            }
            #endregion

            return result;

        }




        public ESIRingoutResult RingoutRequest(ESIRingoutRequest RingoutRequest1)
        {
            logger.Info("In RingoutRequest");
            List<OutboundNotificationPreference> preferences = LoadOutboundNotificationPreference();
            ESIRingoutResult result = new ESIRingoutResult();
            ESILog esiLog = new ESILog { AppendTime = DateTime.Now, IsAbleToDeserializeObject = false, Method = System.Reflection.MethodBase.GetCurrentMethod().Name, Status = ESILogStatus.Created };
            bool EnableOutboundWhiteList = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableOutboundWhiteList"]);
            List<string> OutboundWhiteList = ConfigurationManager.AppSettings["OutboundWhiteList"].Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).ToList();
            bool enableTheCallout = true;
            string dispatchFleetID = "";


            try
            {
                #region serialize request
                string request = RingoutRequest1.Serialize();
                esiLog.Request = request;
                esiLog.IsAbleToDeserializeObject = true;
                #endregion

                #region validate request
                if (v.ValidateRingoutRequestRequest(RingoutRequest1) == ValidationCode.NoError)
                {
                    long? vtodId = string.IsNullOrEmpty(RingoutRequest1.BookingIdentifier.EXTBookingRef) ? (long?)null :Convert.ToInt64(RingoutRequest1.BookingIdentifier.EXTBookingRef);
                    int tripType = GetMTDataTripType(vtodId, RingoutRequest1.BookingIdentifier.MTDBookingID.ToString(),out dispatchFleetID);
                    if (tripType == MTDataTripType.MTDTripForIVR || tripType == MTDataTripType.MTDTripForIVRNotInVTOD)
                    {
                        logger.Info("Generate call out");
                        #region add a esilog, call, attempt, esilog_trip_call
                        using (ESIPushServiceEntities context = new ESIPushServiceEntities())
                        {
                            //get trip
                            var trip = context.Trips.Where(x => x.BookingId == RingoutRequest1.BookingIdentifier.MTDBookingID).FirstOrDefault();

                            if (trip != null)
                            {
                                esiLog.Status = ESILogStatus.DisabledOutbnoundNotification;


                                //whitelist
                                if ((EnableOutboundWhiteList) && (OutboundWhiteList.Where(x => x == trip.PhoneNumber).Any()))
                                {
                                    enableTheCallout = true;
                                }
                                else if (!EnableOutboundWhiteList)
                                {
                                    enableTheCallout = true;
                                }
                                else {
                                    enableTheCallout = false;
                                }


                                //get MTDFleet
                                ///string dispatchFleetId = GetDispatchFleetId(GetToken(), trip.PhoneNumber, GetDispatchFleetIdsForIVR());
                                string dispatchFleetId = GetDispatchFleetIdByMTDBookingId(GetToken(), RingoutRequest1.BookingIdentifier.MTDBookingID.ToString(), GetDispatchFleetIdsForIVR());
                                var pList = preferences.Where(x => x.ESIFleetId == Convert.ToInt64(dispatchFleetId) && x.ApplicationId == 1).ToList();

                                if (pList != null && pList.Any() && pList.Where(x => x.CallType == 1 && (x.Status == 1||x.Status==2)).Any() && enableTheCallout)
                                {
                                    if (!CheckDuplication(trip.Id, CallType.Voice,1))
                                    {
                                        logger.Info("create voice outbound notification");
                                        //create voice outbound notification
                                        OutboundNotificationPreference p = pList.Where(x => x.CallType == 1 && (x.Status == 1 || x.Status == 2)).FirstOrDefault();
                                        var voicecallId = InjectVoiceCall(OutboundApplicationType.ArrivalNotification, trip, p);
                                        context.ESILog_Trip_Call.Add(new ESILog_Trip_Call { AppendTime = DateTime.Now, ESILogId = esiLog.Id, TripId = trip.Id, CallId = voicecallId, CallType = CallType.Voice, OutboundSystemId = 1,ApplicationId=1 });
                                        esiLog.Status = ESILogStatus.CalloutGenerated;
                                    }
                                    else {
                                        logger.Warn("Duplicated Voice outbound notification. Ignore this ringout.");
                                    }
                                }


                                if (pList != null && pList.Any() && pList.Where(x => x.CallType == 2 && (x.Status == 1 || x.Status == 2)).Any() && enableTheCallout)
                                {
                                    if (!CheckDuplication(trip.Id, CallType.SMS,1))
                                    {
                                        logger.Info("create sms outbound notification");
                                        //create sms outbound notification
                                        OutboundNotificationPreference p = pList.Where(x => x.CallType == 2 && (x.Status == 1 || x.Status == 2)).FirstOrDefault();
                                        var smscallId = InjectSMSCall(OutboundApplicationType.ArrivalNotification, trip, p);
                                        context.ESILog_Trip_Call.Add(new ESILog_Trip_Call { AppendTime = DateTime.Now, ESILogId = esiLog.Id, TripId = trip.Id, CallId = smscallId, CallType = CallType.SMS, OutboundSystemId = 1, ApplicationId = 1 });
                                        esiLog.Status = ESILogStatus.CalloutGenerated;
                                    }
                                    else {
                                        logger.Warn("Duplicated SMS outbound notification. Ignore this ringout.");
                                    }
                                }

                                
                                
                                
                            }
                            else {
                                logger.Info("unable to find this esi trip");
                                esiLog.Status = ESILogStatus.InvalidRequest;
                            }

                            //save
                            context.ESILogs.Add(esiLog);
                            context.SaveChanges();
                        }
                        #endregion
                    }
                    else {
                        logger.InfoFormat("trip type is {0}. we don't need to send outbound notificaiton", tripType);
                    }

                    #region make response
                    result.Error = ESIRingoutResultError.None;
                    result.ErrorMessage = string.Empty;
                    result.Succeeded = true;
                    #endregion
                }
                else {
                    logger.Warn("Invalid request");
                    result.Error = ESIRingoutResultError.UnknownError;
                    result.ErrorMessage = "Invalid request";
                }
                #endregion
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                string rs = "";
                foreach (var eve in e.EntityValidationErrors)
                {
                    rs = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    Console.WriteLine(rs);

                    foreach (var ve in eve.ValidationErrors)
                    {
                        rs += "<br />" + string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw new Exception(rs);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                logger.Error(ex.InnerException.Message);
                logger.Error(ex.StackTrace.ToString());
                result.Error = ESIRingoutResultError.UnknownError;
                result.ErrorMessage = ex.Message;
            }
           


            #region save esi log when there is an exception
            if (esiLog.Id == 0)
            {
                try
                {
                    using (ESIPushServiceEntities context = new ESIPushServiceEntities())
                    {
                        esiLog.Status = ESILogStatus.InvalidRequest;
                        context.ESILogs.Add(esiLog);
                        context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message);
                    result.Error = ESIRingoutResultError.UnknownError;
                    result.ErrorMessage = ex.Message;
                }
            }
            #endregion


            return result;

        }


        public ESIBookingResult NotifyCheckbackResponse(ESICheckbackResponse CheckbackResponse)
        {
            ESIBookingResult result = new ESIBookingResult();
            ESILog esiLog = new ESILog { AppendTime = DateTime.Now, IsAbleToDeserializeObject = false, Method = System.Reflection.MethodBase.GetCurrentMethod().Name, Status = ESILogStatus.Created };

            try
            {
                #region serialize request
                string request = CheckbackResponse.Serialize();
                esiLog.Request = request;
                esiLog.IsAbleToDeserializeObject = true;
                #endregion

                #region validate request
                if (v.NotifyCheckbackResponse(CheckbackResponse) == ValidationCode.NoError)
                {
                    #region make response
                    result.Error = ESIBookingResultError.None;
                    result.ErrorMessage = string.Empty;
                    result.Succeeded = true;
                    #endregion
                }
                else {
                    result.Error = ESIBookingResultError.UnknownError;
                    result.ErrorMessage = "Invalid request";
                }
                #endregion
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                logger.Error(ex.InnerException.Message);
                logger.Error(ex.StackTrace.ToString());
                result.Error = ESIBookingResultError.UnknownError;
                result.ErrorMessage = ex.Message;
            }

            #region save esi log when there is an exception
            if (esiLog.Id == 0)
            {
                try
                {
                    using (ESIPushServiceEntities context = new ESIPushServiceEntities())
                    {
                        esiLog.Status = ESILogStatus.InvalidRequest;
                        context.ESILogs.Add(esiLog);
                        context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message);
                    result.Error = ESIBookingResultError.UnknownError;
                    result.ErrorMessage = ex.Message;
                }
            }
            #endregion

            return result;

        }

        private List<OutboundNotificationPreference> LoadOutboundNotificationPreference() {
            List<OutboundNotificationPreference> preferences = new List<OutboundNotificationPreference>();
            using (ESIPushServiceEntities context = new ESIPushServiceEntities()) {
                preferences =context.OutboundNotificationPreferences.Select(x => x).ToList();
            }
            return preferences;
        }

        #region Private methods
        private long? InjectVoiceCall(string outboundApplicationType, Trip t,OutboundNotificationPreference p)
        {
            long? callId=null;
            using (ININWSEntities context = new ININWSEntities())
            {
                //get application
                Application a = context.Applications.Where(x => x.ApplicationName == outboundApplicationType).FirstOrDefault();

                //The application has to be turned on
                if (a.Status)
                {
                    var vtodFleetId = GetVTODFleetId(t.BookingId.ToString());

                    //add a callout
                    Callout c = new Callout
                    {
                        ApplicationId = a.ApplicationId,
                        CallType = CallType.Voice,
                        ReceivedDate = DateTime.Now,
                        UpdatedOn = DateTime.Now,
                        WinServiceLogId = null,
                        Status = 0,
                        FleetId = Convert.ToInt32(vtodFleetId.ToString()),
                        ReceivedUtcDate = DateTime.UtcNow,
                        TripCount = 1,
                    };

                    //add trip info
                    c.TripInfoes.Add(new TripInfo { 
                        ANI = "3474860777",
                        FleetId=(p.VTODTaxiFleetId.HasValue)?int.Parse(p.VTODTaxiFleetId.Value.ToString()):0,
                        ConfirmationNo=t.BookingId.ToString(),
                        TripTime=t.PickupTime,
                        UpdatedOn=DateTime.Now,
                        Status = "Assigned",
                    });


                    //add attempt
                    c.Attempts.Add(new Attempt { Ordering = 1, Status = 0, Value = t.PhoneNumber, UpdatedOn = DateTime.Now, AvailableTime = DateTime.Now, ExpiredDate = DateTime.Now.AddMinutes(a.ExpiredInterval) });

                    //save
                    context.Callouts.Add(c);
                    context.SaveChanges();
                    callId = c.Id;
                }
            }
            return callId;
        }

        private long? InjectSMSCall(string outboundApplicationType, Trip t,OutboundNotificationPreference p)
        {
            long? callId = null;
            try
            {
                using (ININWSEntities context = new ININWSEntities())
                {
                    //get application
                    Application a = context.Applications.Where(x => x.ApplicationName == outboundApplicationType).FirstOrDefault();

                    //The application has to be turned on
                    if (a.Status && (p.Status == OutboundPreferenceStatus.On_WithMarketingMessage || p.Status == OutboundPreferenceStatus.On_WithoutMarketingMessage))
                    {
                        var vtodFleetId = GetVTODFleetId(t.BookingId.ToString());

                        //add a callout
                        Callout c = new Callout
                        {
                            ApplicationId = a.ApplicationId,
                            CallType = CallType.SMS,
                            ReceivedDate = DateTime.Now,
                            UpdatedOn = DateTime.Now,
                            WinServiceLogId = null,
                            Status = 0,
                            FleetId = Convert.ToInt32(vtodFleetId.ToString()),
                            ReceivedUtcDate = DateTime.UtcNow,
                            TripCount = 1
                        };

                        //add trip info
                        c.TripInfoes.Add(new TripInfo
                        {
                            ANI = "3474860777",
                            FleetId = (p.VTODTaxiFleetId.HasValue) ? int.Parse(p.VTODTaxiFleetId.Value.ToString()) : 0,
                            ConfirmationNo = t.BookingId.ToString(),
                            TripTime = t.PickupTime,
                            UpdatedOn = DateTime.Now,
                            Status = "Assigned",
                        });


                        //add attempt
                        c.Attempts.Add(new Attempt { Ordering = 1, Status = 0, Value = t.PhoneNumber, UpdatedOn = DateTime.Now, AvailableTime = DateTime.Now, ExpiredDate = DateTime.Now.AddMinutes(a.ExpiredInterval) });

                        //save
                        context.Callouts.Add(c);
                        context.SaveChanges();
                        callId = c.Id;


                        //send SMS
                        string message = GetOutbountSMSByApplication(a, p, t);
                        if (p.Status == OutboundPreferenceStatus.On_WithMarketingMessage)
                        {
                            message = string.Format("{0} {1}", message, p.MessageForMarketing);
                        }
                        else if (p.Status == OutboundPreferenceStatus.On_WithoutMarketingMessage)
                        {
                            message = string.Format("{0}", message);
                        }

                        if (SendSMSMessage(t.PhoneNumber, message, t.BookingId))
                        {
                            c.Attempts.FirstOrDefault().Status = 2;
                            c.Attempts.FirstOrDefault().UpdatedOn = DateTime.Now;
                            c.Status = 1;
                            c.UpdatedOn = DateTime.Now;
                        }
                        else {
                            c.Attempts.FirstOrDefault().Status = 5;
                            c.Attempts.FirstOrDefault().UpdatedOn = DateTime.Now;
                            c.Status = 7;
                            c.UpdatedOn = DateTime.Now;
                        }

                        context.SaveChanges();

                    }
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                logger.ErrorFormat(e.Message);
                logger.ErrorFormat(e.InnerException.Message);
                logger.ErrorFormat(e.StackTrace.ToString());

                string rs = "";
                foreach (var eve in e.EntityValidationErrors)
                {
                    rs = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    logger.ErrorFormat(rs);

                    foreach (var ve in eve.ValidationErrors)
                    {
                        rs += "<br />" + string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw new Exception(rs);
            }
            catch (Exception ex)
            {
                logger.ErrorFormat(ex.Message);
                logger.ErrorFormat(ex.InnerException.Message);
                logger.ErrorFormat(ex.StackTrace.ToString());
            }
            return callId;
        }


        private string GetOutbountSMSByApplication(Application a,OutboundNotificationPreference p,Trip t)
        {
            if (a.ApplicationName == OutboundApplicationType.ArrivalNotification)
            {
                return string.Format(p.Message,t.VehicleId);
            }else  
            {
                return p.Message;
            }

        }
        

        private bool IsSMSBlocked(string phoneNumber)
        {
            bool result = true;

            using (UDI.VTOD.ESIPushWebService.SMSServiceReference.UDISMSWebSvcClient client = new UDI.VTOD.ESIPushWebService.SMSServiceReference.UDISMSWebSvcClient())
            {
                UDI.VTOD.ESIPushWebService.SMSServiceReference.SMSContactInfo info = new UDI.VTOD.ESIPushWebService.SMSServiceReference.SMSContactInfo();
                result = client.IsSMSBlocked(phoneNumber, "ESIPushService", out info);
            }


            return result;
        }

        //private string LoadSMSTemplate(string outboundApplicationType,Trip t)
        //{
        //    string template = "";

        //    if (outboundApplicationType == "ArrivalNotification")
        //    {
        //        template = string.Format("Car {0} is on the way. Next time use our new zTrip app to book your ride and track the driver! www.zTrip.com", t.VehicleId);
        //    }
        //    else if (outboundApplicationType == "StaleNotification")
        //    {
        //        template = "We're working hard to get you a ride. Next time use our zTrip app to book your ride and track the driver! www.zTrip.com";
        //    }

        //    return template;
        //}


        private bool SendSMSMessage(string to, string msg,long MTDBookingId)
        {
            bool result = false;
            string from = "8888888888";
            //string DNI = "0190010004";
            string DNI = "0100170004";
            
            using (UDI.VTOD.ESIPushWebService.SMSServiceReference.UDISMSWebSvcClient client = new UDI.VTOD.ESIPushWebService.SMSServiceReference.UDISMSWebSvcClient())
            {
                result=client.SMSSendMessage(from, to, msg, DNI, MTDBookingId);
            }
            return result;
        }


        private long? GetVTODFleetId(string MTDBookingId)
        {
            #region get list dispatch fleet for MTData IVR
            List<int> dispatchFleetIDsForIVR = new List<int>();
            dispatchFleetIDsForIVR = GetDispatchFleetIdsForIVR();
            #endregion
            string token = GetToken();
            string dispatchFleetId = GetDispatchFleetIdByMTDBookingId(token, MTDBookingId, dispatchFleetIDsForIVR);

            long? vtodFleetId = null;

            using (VTODEntities context = new VTODEntities())
            {
                long serviceAPIId = context.taxi_serviceapi.Where(x => x.Name == "MTData").FirstOrDefault().Id;
                vtodFleetId = context.taxi_fleet_api_reference.Where(x => x.taxi_fleet.IVREnable && x.taxi_fleet.CCSiFleetId == dispatchFleetId && x.ServiceAPIId == serviceAPIId).Select(x => x.FleetId).Distinct().FirstOrDefault();
            }

            return vtodFleetId;
        }

        //private long? GetVTODFleetId( string phoneNumber)
        //{
        //    #region get list dispatch fleet for MTData IVR
        //    List<int> dispatchFleetIDsForIVR = new List<int>();
        //    dispatchFleetIDsForIVR = GetDispatchFleetIdsForIVR();
        //    #endregion
        //    string token = GetToken();
        //    string dispatchFleetId = GetDispatchFleetId(token, phoneNumber, dispatchFleetIDsForIVR);

        //    long? vtodFleetId = null;

        //    using (VTODEntities context = new VTODEntities())
        //    {
        //        long serviceAPIId = context.taxi_serviceapi.Where(x => x.Name == "MTData").FirstOrDefault().Id;
        //        vtodFleetId = context.taxi_fleet_api_reference.Where(x=>x.taxi_fleet.IVREnable && x.taxi_fleet.CCSiFleetId == dispatchFleetId && x.ServiceAPIId== serviceAPIId).Select(x=>x.FleetId).Distinct().FirstOrDefault();
        //    }

        //    return vtodFleetId;
        //}

        private vtod_trip GetVTODTripByVTODId(long vtodId, string dispatchTripId)
        {
            vtod_trip t = null;

            using (VTODEntities context = new VTODEntities())
            {
                t= context.vtod_trip.Where(x => x.Id == vtodId && x.taxi_trip.DispatchTripId==dispatchTripId).FirstOrDefault();
                if (t != null)
                {
                    t.taxi_trip = context.taxi_trip.Where(x => x.Id == vtodId && x.DispatchTripId == dispatchTripId  ).FirstOrDefault();
                }
            }

            return t;
        }


        private long GetVTODId(string phoneNumber,string MTDBookingId)
        {
            long vtodTripId = -1;

            #region get list dispatch fleet for MTData IVR
            List<int> dispatchFleetIDsForIVR = new List<int>();
            dispatchFleetIDsForIVR = GetDispatchFleetIdsForIVR();
            #endregion
            string token = GetToken();
            //string dispatchFleetId = GetDispatchFleetId(token, phoneNumber, dispatchFleetIDsForIVR);
            string dispatchFleetId = GetDispatchFleetIdByMTDBookingId(GetToken(), MTDBookingId, GetDispatchFleetIdsForIVR());
            using (VTODEntities context = new VTODEntities()) {
                vtodTripId=context.taxi_trip.Where(x => x.taxi_fleet.CCSiFleetId == dispatchFleetId && x.DispatchTripId == MTDBookingId).FirstOrDefault().Id;
            }

            return vtodTripId;
        }

        private List<int> GetDispatchFleetIdsForIVR() {
            List<int> dispatchFleetIds = new List<int>();
            using (VTODEntities context = new VTODEntities())
            {
                long serviceAPIId = context.taxi_serviceapi.Where(x => x.Name == "MTData").FirstOrDefault().Id;
                List<string> dispatchStrings = context.taxi_fleet_api_reference.Where(x => x.taxi_fleet.IVREnable && x.ServiceAPIId == serviceAPIId).Select(x => x.taxi_fleet.CCSiFleetId).Distinct().ToList();
                dispatchFleetIds=dispatchStrings.Select(x => Convert.ToInt32(x)).ToList();
            }
            return dispatchFleetIds;
        }

        private List<int> GetDispatchFleetIdsForVTOD()
        {
            List<int> dispatchFleetIds = new List<int>();
            using (VTODEntities context = new VTODEntities())
            {
                long serviceAPIId = context.taxi_serviceapi.Where(x => x.Name == "MTData").FirstOrDefault().Id;
                List<string> dispatchStrings = context.taxi_fleet_api_reference.Where(x =>  x.ServiceAPIId == serviceAPIId).Select(x => x.taxi_fleet.CCSiFleetId).Distinct().ToList();
                dispatchFleetIds = dispatchStrings.Select(x => Convert.ToInt32(x)).ToList();
            }
            return dispatchFleetIds;
        }

        private string GetToken()
        {
            string token = "";
            lock (ObjSync)
            {
                using (UDI.VTOD.ESIPushWebService.AuthenicationWebServiceReference.AuthenticationWebServiceSoapClient client = new UDI.VTOD.ESIPushWebService.AuthenicationWebServiceReference.AuthenticationWebServiceSoapClient())
                {
                    var result= client.Authenticate(UDI.VTOD.ESIPushWebService.AuthenicationWebServiceReference.BookingChannelType.SolidusIvr, IVR_UserName, IVR_Password);
                    if (result.Succeeded)
                    {
                        token = result.Token;
                    }
                }
            }
            return token;
        }

        private OSIBookingResult GetBookingInformation(string token, string MTDBookingID) {
            OSIBookingResult result = null;
            using (UDI.VTOD.ESIPushWebService.OSIWebServiceReference.OSIWebServiceSoapClient client = new UDI.VTOD.ESIPushWebService.OSIWebServiceReference.OSIWebServiceSoapClient())
            {
                result = client.GetBookingByID(token, Convert.ToInt64(MTDBookingID));
                if (!result.Succeeded)
                {
                    result = null;
                }
            }
            return result;
        }

        private string GetDispatchFleetIdByMTDBookingId(string token, string MTDBookingID, List<int> dispatchFleetIds)
        {
            string dispatchFleetId = "";

            using (UDI.VTOD.ESIPushWebService.OSIWebServiceReference.OSIWebServiceSoapClient client = new UDI.VTOD.ESIPushWebService.OSIWebServiceReference.OSIWebServiceSoapClient())
            {
                var result = client.GetBookingByID(token, Convert.ToInt64(MTDBookingID));
                if (result.Succeeded)
                {
                    //dispatchFleetId = result.Fleets.FirstOrDefault().ID.ToString(); //get the first one for temporary
                    dispatchFleetId = result.Booking.Fleet.ID.ToString();
                }

                if (!dispatchFleetId.Where(x => x.ToString() == dispatchFleetId).Any())
                {
                    dispatchFleetId = "";
                }
            }
            return dispatchFleetId;
        }

        private string GetDispatchFleetId(string token, string PhoneNumber, List<int> dispatchFleetIds)
        {
            string dispatchFleetId = "";

            using (UDI.VTOD.ESIPushWebService.BookingWebServiceReference.BookingWebServiceSoapClient client = new UDI.VTOD.ESIPushWebService.BookingWebServiceReference.BookingWebServiceSoapClient())
            {
                var result = client.GetFleet(token, PhoneNumber, FleetKeywordType.PhoneNumber);
                if (result.Succeeded)
                {
                    //dispatchFleetId = result.Fleets.FirstOrDefault().ID.ToString(); //get the first one for temporary
                    dispatchFleetId = "3";
                }

                if (!dispatchFleetId.Where(x => x.ToString() == dispatchFleetId).Any())
                {
                    dispatchFleetId = "";
                }
            }
            return dispatchFleetId;
        }


        private int GetMTDataTripType(long? VTODId, string dispatchTripId,out string dispatchFleetId)
        {
            int type = MTDataTripType.UnknownMTDTrip;
            dispatchFleetId = "";
            try
            {

                //check VTOD
                if (VTODId.HasValue)
                {
                    using (VTODEntities context = new VTODEntities())
                    {
                        var t = context.taxi_trip.Where(x => x.Id == VTODId && x.DispatchTripId == dispatchTripId).FirstOrDefault();
                        if (t != null)
                        {
                            var f = context.taxi_fleet.Where(x => x.Id == t.FleetId).FirstOrDefault();
                            dispatchFleetId = f.CCSiFleetId;
                            if (f.IVREnable)
                            {
                                type = MTDataTripType.MTDTripForIVR;
                            }
                            else {
                                type = MTDataTripType.MTDTripForZTrip;
                            }
                        }
                        else {
                            type = MTDataTripType.UnknownMTDTrip;
                        }
                    }
                }

                if (type == MTDataTripType.UnknownMTDTrip)
                {
                    //check ESILog
                    //using (ESIPushServiceEntities context = new ESIPushServiceEntities())
                    //{
                    //    var BookingId = long.Parse(dispatchTripId);
                    //    var t = context.Trips.Where(x => x.BookingId == BookingId).FirstOrDefault();
                    //    if (t != null)
                    //    {
                    //        type = t.TripType;
                    //    }
                    //    else {
                    //        type = MTDataTripType.UnknownMTDTrip;
                    //    }
                    //}
                    List<int> dispatchFleetIDsForIVR = new List<int>();
                    dispatchFleetIDsForIVR = GetDispatchFleetIdsForIVR();
                    dispatchFleetId = GetDispatchFleetIdByMTDBookingId(GetToken(), dispatchTripId, dispatchFleetIDsForIVR);
                    if (string.IsNullOrWhiteSpace(dispatchFleetId))
                    {
                        type = MTDataTripType.UnknownMTDTrip;
                    }
                    else {
                        type = MTDataTripType.MTDTripForIVR;
                    }
                    
                }

            }
            catch (Exception ex) {
                logger.Error(ex.Message);
                logger.Error(ex.InnerException.Message);
                logger.Error(ex.StackTrace.ToString());
            }

            logger.InfoFormat("TripType={0}, VTODId={1}, MTDBookingId={2}, MTDFleetID={3}",type,VTODId,dispatchTripId,dispatchFleetId);

            return type;
        }


        private long? GetVTODId(string MTDBookingId, long vtodId)
        {
            long? tripId = null;
            using (VTODEntities context = new VTODEntities())
            {
                if (context.taxi_trip.Where(x => x.Id == vtodId && x.DispatchTripId == MTDBookingId).Any())
                {
                    tripId = vtodId;
                }
                else {
                    tripId = null;
                }

            }
            return tripId;

        }

        private long? GetVTODId(string MTDBookingId)
        {
            long? tripId = null;
            List<long> dispatchFleetIDsForVTOD = new List<long>();
            dispatchFleetIDsForVTOD = GetDispatchFleetIdsForVTOD().Select(x=>Convert.ToInt64(x.ToString())).ToList();
            using (VTODEntities context = new VTODEntities())
            {
                if (context.taxi_trip.Where(x => dispatchFleetIDsForVTOD.Contains(x.FleetId) && x.DispatchTripId==MTDBookingId).Any()) {
                    tripId=context.taxi_trip.Where(x => dispatchFleetIDsForVTOD.Contains(x.FleetId) && x.DispatchTripId == MTDBookingId).Select(X=>X.Id).FirstOrDefault();
                }

            }
            return tripId;

        }


        private int GetMTDataTripType(long? vtodTripId, string dispatchTripId,string phoneNumber, int? dispatchFleetId)
        {
            int type = MTDataTripType.UnknownMTDTrip;
            vtod_trip trip = null;
            if (vtodTripId.HasValue)
            {
                trip = GetVTODTripByVTODId(vtodTripId.Value, dispatchTripId);
            }
            List<int> dispatchFleetIDsForIVR = GetDispatchFleetIdsForIVR();

            if (trip != null)
            {
                // Ztrip or IVR trip
                if (dispatchFleetIDsForIVR.Where(x => x.ToString() == trip.taxi_trip.FleetId.ToString()).Any())
                {
                    //IVR trip
                    type = MTDataTripType.MTDTripForIVR;
                }
                else {
                    //Ztrip
                    type = MTDataTripType.MTDTripForZTrip;
                }

            }
            else {
                //agent trip or other trip
                if (dispatchFleetId.HasValue && dispatchFleetIDsForIVR.Where(x=>x== dispatchFleetId.Value).Any())
                {
                    type = MTDataTripType.MTDTripForIVRNotInVTOD;
                }
                else
                {
                    using (ESIPushServiceEntities context = new ESIPushServiceEntities())
                    {
                        long mtdBookingId = Convert.ToInt64(dispatchTripId);
                        var t=context.Trips.Where(x => x.BookingId == mtdBookingId).FirstOrDefault();
                        if (t != null)
                        {
                            type = t.TripType;
                        }
                        else {
                            type = MTDataTripType.UnknownMTDTrip;
                        }
                    }

                    
                }
            }
            logger.InfoFormat("TripType={0}, VTODId={1}, MTDBookingId={2}, Phone={3}, DispatchFleetID={4}", type, vtodTripId, dispatchTripId,phoneNumber,dispatchFleetId);

            return type;
        }

        //private int GetMTDataTripType(string phoneNumber, string MTDBookingId)
        //{
        //    int type = MTDataTripType.UnknownMTDTrip;

        //    #region get list dispatch fleet for MTData IVR
        //    List<int> dispatchFleetIDsForVTOD = GetDispatchFleetIdsForVTOD();
        //    List<int> dispatchFleetIDsForIVR = GetDispatchFleetIdsForIVR();
        //    #endregion
        //    string token = GetToken();
        //    string dispatchFleetId = GetDispatchFleetId(token, phoneNumber, dispatchFleetIDsForVTOD);

        //    if (!dispatchFleetId.Equals(string.Empty))
        //    {
        //        if (dispatchFleetIDsForIVR.Where(x => x.ToString() == dispatchFleetId).Any())
        //        {
        //            if()
        //        }
        //        else {
        //            type = MTDataTripType.MTDTripForZTrip;
        //        }

        //    }
        //    else {
        //        type = MTDataTripType.UnknownMTDTrip;
        //    }

        //    return type;
        //}


        private bool CheckDuplication(long TripId, int callType,long applicationId)
        {
            bool isDuplicated = false;

            var transactionOptions = new System.Transactions.TransactionOptions();
            //set it to read uncommited
            transactionOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted;
            //create the transaction scope, passing our options in
            using (var transactionScope = new System.Transactions.TransactionScope(
                System.Transactions.TransactionScopeOption.Required,
                transactionOptions)
            )

            using (ESIPushServiceEntities context = new ESIPushServiceEntities())
            {
                if (context.ESILog_Trip_Call.Where(x => x.TripId == TripId && x.CallType == callType && x.ApplicationId==applicationId).Any())
                {
                    isDuplicated = true;
                }
                else {
                    isDuplicated = false;
                }
            }
            return isDuplicated;
        }

        private void UpdateVTODTaxiTripStatus(long vtodTripId,string MTDBookingId, string status)
        {

            bool isAbleToUpdateStatus = bool.Parse(ConfigurationManager.AppSettings["UpdateVTODTripStatus"]);
            if (isAbleToUpdateStatus)
            {
                using (VTODEntities context = new VTODEntities())
                {

                    vtod_trip vtod_trip =context.vtod_trip.Where(x => x.Id == vtodTripId && x.taxi_trip.DispatchTripId == MTDBookingId).FirstOrDefault();

                    if (vtod_trip == null)
                    {

                        long serviceAPIId = context.taxi_serviceapi.Where(x => x.Name == "MTData").FirstOrDefault().Id;
                        vtod_trip = context.vtod_trip.Where(x => x.taxi_trip.DispatchTripId == MTDBookingId && x.taxi_trip.ServiceAPIId == serviceAPIId).FirstOrDefault();
                    }
                    
                    //find VTOD status
                    string vtodStatus = context.taxi_status_mtdatawrapper.Where(x => x.Source == status).FirstOrDefault().Target;

                    //find fleet
                    var fleet = context.taxi_fleet.Where(x => x.Id == vtod_trip.taxi_trip.FleetId).FirstOrDefault();

                    //add trip status
                    context.taxi_trip_status.Add(new taxi_trip_status
                    {
                        AppendTime = DateTime.Now,
                        Status = vtodStatus,
                        TripID = vtod_trip.Id,
                        StatusTime = UDI.Utility.Helper.Converter.FromUtc(DateTime.UtcNow, fleet.ServerUTCOffset),
                        OriginalStatus = status
                    });

                    //update final status
                    vtod_trip.FinalStatus = vtodStatus;

                    
                    context.SaveChanges();
                }
            }

        }

        #endregion

    }



}