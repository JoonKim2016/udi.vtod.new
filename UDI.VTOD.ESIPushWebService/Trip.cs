//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UDI.VTOD.ESIPushWebService
{
    using System;
    using System.Collections.Generic;
    
    public partial class Trip
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Trip()
        {
            this.ESILog_Trip_Call = new HashSet<ESILog_Trip_Call>();
        }
    
        public long Id { get; set; }
        public long BookingId { get; set; }
        public Nullable<long> NewBookingId { get; set; }
        public Nullable<long> DispatchFleetId { get; set; }
        public long ESILogId { get; set; }
        public Nullable<long> VTODID { get; set; }
        public int TripType { get; set; }
        public string EXTBookingRef { get; set; }
        public string Status { get; set; }
        public string PhoneNumber { get; set; }
        public Nullable<System.DateTime> PickupTime { get; set; }
        public string DriverId { get; set; }
        public string VehicleId { get; set; }
        public System.DateTime AppendTime { get; set; }
    
        public virtual ESILog ESILog { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ESILog_Trip_Call> ESILog_Trip_Call { get; set; }
    }
}
