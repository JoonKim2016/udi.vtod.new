﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Security.Policy;
using System.Threading;
using System.Web;
using log4net;
using UDI.Utility.Helper;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.Helper;
using UDI.VTOD.Common.Abstract;

namespace UDI.VTOD.Handlers
{
	/// <summary>
	/// Summary description for Image
	/// </summary>
	public class ImageHandler : BaseSetting, IHttpHandler
	{

		private Images images;
		private ImageHelper imageHelper;


		public ImageHandler()
		{
			imageHelper = new ImageHelper(logger);


			images = new Images(logger);
		}

		public void ProcessRequest(HttpContext context)
		{
			logger.InfoFormat("Context:{0}", context.Request.Url.AbsoluteUri);
			string customDefaultImg = string.Empty;
			int h = 86;
			int w = 86;
			string v = string.Empty;

			try
			{


				#region Get args from QS
				#region Set the Default Image Dimensions
				//get the width x height
				//// for new height of image
				string size = ConfigHelper.GetDriverPhotoImagesSizes().Split(',')[0];
				h = int.Parse(size.Split('x')[0]);
				w = int.Parse(size.Split('x')[1]);
				#endregion

				try
				{
					#region Use the custom image dimensions if the users defines them
					//image height
                    if (context.Request.QueryString["h"] != null)
                    {
                        h = int.Parse(context.Request.QueryString["h"]);
                    }
					//// for new width of image
                    if (context.Request.QueryString["w"] != null)
                    {
                        w = int.Parse(context.Request.QueryString["w"]);
                    }
					//image version
					if (context.Request.QueryString["v"] != null)
					{
						v = context.Request.QueryString["v"];
						if (v.ToString().ToLower() == "version".ToLower())
							v = string.Empty;
					}
					#endregion
				}
				catch
				{
					//v = context.Request.QueryString["v"];
					//if (v.ToString().ToLower() == "version".ToLower())
					//	v = string.Empty;
				}


				Guid driverUniqueId = Guid.Empty;
				try
				{
					driverUniqueId = context.Request.QueryString["d"].ToGuid();
				}
				catch
				{ }

				//string fleetId = context.Request.QueryString["f"];
				#endregion
				var fileUrl = string.Format("{0}/{1}_{2}_{3}.png", ConfigHelper.GetDriverPhotoBaseUrl(), driverUniqueId, w, h);

				if (string.IsNullOrEmpty(v))
				{
					//get dimensions being requested with the default file
					customDefaultImg = images.GetCustomSizeDefaultPhotoUrl(w, h);
				}
				else
				{
					//get dimensions being requested with the default file
					customDefaultImg = images.GetCustomSizeDefaultPhotoUrl(w, h, v);
				}

				if (driverUniqueId != Guid.Empty)
				{
					if (!imageHelper.IsImageUrl(fileUrl))
					{
						logger.Info("!imageHelper.IsImageUrl(fileUrl)");

						//get the original resize on a new thread

						//scale the image

						Stream stream = new MemoryStream();
						var orginalImage = string.Format("{0}/{1}_original.png", ConfigHelper.GetDriverPhotoBaseUrl(), driverUniqueId);

						#region Try to Download the original photo from CDN if fails log and return default photo
						try
						{
							#region Download Original Image from the CDN
							imageHelper.DownloadImageFromCdnToMemory(orginalImage, ref stream);
							#endregion
						}
						catch (Exception ex)
						{
							#region Log Error then redirect
							logger.ErrorFormat(
										  "Error Downloading Image From CDN Image Name \"{0}\" error occcured in Handler ProcessRequest(HttpContext context). Error Message \"{1}\". Inner Exception {2}.",
										  orginalImage, ex.Message, ex.InnerException);

							logger.Error(ex);

							RedirectToDefault(context, customDefaultImg, w, h);

							return;
							#endregion
						}
						#endregion


						#region Update the driver image
						using (stream)
						{
							images.UpdateDriverImage(Image.FromStream(stream), driverUniqueId, h, w);
						}
						#endregion

						logger.InfoFormat("Redirected:{0}", fileUrl);

						#region  return the resized photo
						//RedirectToDefault(context, customDefaultImg, w, h);
						context.Response.Redirect(fileUrl, false);
						#endregion

					}
					else
					{
						#region If we get here that means we've created the img in the past
						logger.InfoFormat("Redirected:{0}", fileUrl);
						context.Response.Redirect(fileUrl, false);
						#endregion
					}
				}
				else
				{
					logger.ErrorFormat("driverUniqueId == Guid.Empty. So redirect to default photo");
					if (string.IsNullOrEmpty(v))
						RedirectToDefault(context, customDefaultImg, w, h);
					else
					{
						try
						{
							RedirectToDefault(context, customDefaultImg, w, h, v);
						}
						catch
						{
							var v2 = v.Split('.').First();

							customDefaultImg = images.GetCustomSizeDefaultPhotoUrl(w, h, v2);
							RedirectToDefault(context, customDefaultImg, w, h, v2);

						}
					}
				}
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("An error occurred retrieving your photo. Please check your query string parameters.");
				logger.Error(ex);
				if (string.IsNullOrEmpty(v))
					RedirectToDefault(context, customDefaultImg, w, h);
				else
					RedirectToDefault(context, customDefaultImg, w, h, v);
			}
		}

		private void RedirectToDefault(HttpContext context, string customDefaultImg, int w, int h)
		{
			if (imageHelper.IsImageUrl(customDefaultImg))
			{
				context.Response.Redirect(customDefaultImg, false);
			}
			else
			{
				CreateCustomDefaultPhoto(w, h);

				context.Response.Redirect(customDefaultImg, false);
			}
		}
		private void RedirectToDefault(HttpContext context, string customDefaultImg, int w, int h, string v)
		{
			if (imageHelper.IsImageUrl(customDefaultImg))
			{
				context.Response.Redirect(customDefaultImg, false);
			}
			else
			{
				CreateCustomDefaultPhoto(w, h, v);

				context.Response.Redirect(customDefaultImg, false);
			}
		}

		private void CreateCustomDefaultPhoto(int width, int height)
		{
			Stream stream = new MemoryStream();
			imageHelper.DownloadImageFromCdnToMemory(
				  ConfigHelper.GetDriverPhotoDefault(), ref stream);

			using (stream)
			{
				images.UpdateImage(Image.FromStream(stream), "default", height, width);
			}
		}
		private void CreateCustomDefaultPhoto(int width, int height, string v)
		{
			Stream stream = new MemoryStream();
			imageHelper.DownloadImageFromCdnToMemory(ConfigHelper.GetDriverPhotoVersionDefault(), ref stream, v);

			using (stream)
			{
				images.UpdateImage(Image.FromStream(stream), "default", height, width, v);
			}
		}
		public bool IsReusable { get; private set; }
	}
}