﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.LoadTester
{
	public class RequestDTO
	{
		public bool rbtnXMLChecked { get; set; }
		public bool rbtnJSONChecked { get; set; }
		public bool rbtnGetChecked { get; set; }
		public bool rbtnPostChecked { get; set; }
		public string txtRequestHeaderText { get; set; }
		public string txtUrlRequestText { get; set; }
		public string txtRequestText { get; set; }
	}
}
