﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using log4net;
using Newtonsoft.Json.Linq;

using UDI.Utility.Helper;
using UDI.VTOD.Common.DTO.OTA;
using UDI.Utility.Serialization;
using System.Threading;

namespace UDI.VTOD.LoadTester
{
	public partial class TesterForm : Form
	{
		public ILog logger;

		#region Constructors
		public TesterForm()
		{
			InitializeComponent();
			cmbLoadRequest.SelectedIndex = 0;

			#region Log configuration
			logger = LogManager.GetLogger("LoadTest");
			log4net.Config.XmlConfigurator.Configure();
			#endregion

		}
		#endregion

		private bool gotURLs = false;

		private void btnBrowse_Click(object sender, EventArgs e)
		{
			LoadTempaltesFromURL();
		}

		private void btnRequest_Click(object sender, EventArgs e)
		{

			txtResult.Clear();
			var requestDTO = new RequestDTO();
			requestDTO.rbtnGetChecked = rbtnGet.Checked;
			requestDTO.rbtnJSONChecked = rbtnJSON.Checked;
			requestDTO.rbtnPostChecked = rbtnPost.Checked;
			requestDTO.rbtnXMLChecked = rbtnXML.Checked;
			requestDTO.txtRequestHeaderText = txtRequestHeader.Text;
			requestDTO.txtUrlRequestText = txtUrlRequest.Text;
			requestDTO.txtRequestText = txtRequest.Text;
			MutliTaskCall(txtThreadNumber.Text.ToInt32(), requestDTO);

		}

		private void cmbLoadRequest_SelectedIndexChanged(object sender, EventArgs e)
		{
			GetURLs();
		}

		#region Private
		private void AddColouredText(RichTextBox textBox, string strTextToAdd)
		{
			//Use the RichTextBox to create the initial RTF code
			textBox.Clear();
			textBox.AppendText(strTextToAdd);
			string strRTF = textBox.Rtf;
			textBox.Clear();

			/* 
			 * ADD COLOUR TABLE TO THE HEADER FIRST 
			 * */

			// Search for colour table info, if it exists (which it shouldn't)
			// remove it and replace with our one
			int iCTableStart = strRTF.IndexOf("colortbl;");

			if (iCTableStart != -1) //then colortbl exists
			{
				//find end of colortbl tab by searching
				//forward from the colortbl tab itself
				int iCTableEnd = strRTF.IndexOf('}', iCTableStart);
				strRTF = strRTF.Remove(iCTableStart, iCTableEnd - iCTableStart);

				//now insert new colour table at index of old colortbl tag
				strRTF = strRTF.Insert(iCTableStart,
					// CHANGE THIS STRING TO ALTER COLOUR TABLE
					"colortbl ;\\red255\\green0\\blue0;\\red0\\green128\\blue0;\\red0\\green0\\blue255;}");
			}

			//colour table doesn't exist yet, so let's make one
			else
			{
				// find index of start of header
				int iRTFLoc = strRTF.IndexOf("\\rtf");
				// get index of where we'll insert the colour table
				// try finding opening bracket of first property of header first                
				int iInsertLoc = strRTF.IndexOf('{', iRTFLoc);

				// if there is no property, we'll insert colour table
				// just before the end bracket of the header
				if (iInsertLoc == -1) iInsertLoc = strRTF.IndexOf('}', iRTFLoc) - 1;

				// insert the colour table at our chosen location                
				strRTF = strRTF.Insert(iInsertLoc,
					// CHANGE THIS STRING TO ALTER COLOUR TABLE
					"{\\colortbl ;\\red128\\green0\\blue0;\\red0\\green128\\blue0;\\red0\\green0\\blue255;}");
			}

			/*
			 * NOW PARSE THROUGH RTF DATA, ADDING RTF COLOUR TAGS WHERE WE WANT THEM
			 * In our colour table we defined:
			 * cf1 = red  
			 * cf2 = green
			 * cf3 = blue             
			 * */

			for (int i = 0; i < strRTF.Length; i++)
			{
				if (strRTF[i] == '<')
				{
					//add RTF tags after symbol 
					//Check for comments tags 
					if (strRTF[i + 1] == '!')
						strRTF = strRTF.Insert(i + 4, "\\cf2 ");
					else
						strRTF = strRTF.Insert(i + 1, "\\cf1 ");
					//add RTF before symbol
					strRTF = strRTF.Insert(i, "\\cf3 ");

					//skip forward past the characters we've just added
					//to avoid getting trapped in the loop
					i += 6;
				}
				else if (strRTF[i] == '>')
				{
					//add RTF tags after character
					strRTF = strRTF.Insert(i + 1, "\\cf0 ");
					//Check for comments tags
					if (strRTF[i - 1] == '-')
					{
						strRTF = strRTF.Insert(i - 2, "\\cf3 ");
						//skip forward past the 6 characters we've just added
						i += 8;
					}
					else
					{
						strRTF = strRTF.Insert(i, "\\cf3 ");
						//skip forward past the 6 characters we've just added
						i += 6;
					}
				}
			}
			textBox.Rtf = strRTF;
		}

		private void GetURLs()
		{
			//bool loadTemplateFromURL = false;

			//if (bool.TryParse(ConfigurationManager.AppSettings["LoadTemplateFromURL"], out loadTemplateFromURL))
			//{
			if (!gotURLs)
			{
				//if (loadTemplateFromURL)
				//{
				//get list
				string url = ConfigurationManager.AppSettings["SampleDirectory"];

				using (WebClient client = new WebClient())
				{
					string page = client.DownloadString(url);
					Regex re = new Regex(@"<A HREF=""([/\.\w\d\s]+\.[xmljson]+)"">", RegexOptions.IgnoreCase);
					MatchCollection mCol = re.Matches(page);
					if (mCol.Count > 0)
					{
						cmbLoadRequest.Items.Clear();
						foreach (Match m in mCol)
						{
							if (m.Groups[1].Value.Contains("RQ.xml") || m.Groups[1].Value.Contains("RQ.json"))
							{
								cmbLoadRequest.Items.Add(m.Groups[1].Value);
							}
						}
						gotURLs = true;
					}
				}
				//}
			}

			//}
		}

		private void LoadTempaltesFromURL()
		{
			string REST_URL = System.Configuration.ConfigurationManager.AppSettings["URL"];
			string SampleDirectory = System.Configuration.ConfigurationManager.AppSettings["SampleDirectory"];
			using (WebClient client = new WebClient())
			{
				#region URL
				#region Parameter
				string parameter = "";
				if (cmbLoadRequest.Text.Contains(".xml"))
				{
					parameter = "?media=xml";
					rbtnXML.Checked = true;
				}
				else
				{
					parameter = "";
					rbtnJSON.Checked = true;
				}
				#endregion

				#region Action
				string action = "";
				if (cmbLoadRequest.Text.Contains("Token"))
				{
					action = "GetToken";
				}
				else if (cmbLoadRequest.Text.Contains("GroundAvail_VehicleInfo"))
				{
					action = "GroundAvail_VehicleInfo";
				}
				else if (cmbLoadRequest.Text.Contains("GroundAvail"))
				{
					action = "GroundAvail";
				}
				else if (cmbLoadRequest.Text.Contains("GroundBook"))
				{
					action = "GroundBook";
				}
				else if (cmbLoadRequest.Text.Contains("GroundResRetrieve"))
				{
					action = "GroundResRetrieve";
				}
				else if (cmbLoadRequest.Text.Contains("GroundCancel"))
				{
					action = "GroundCancel";
				}
				#endregion

				txtUrlRequest.Text = REST_URL + action + parameter;
				#endregion

				#region Header
				string header = "";
				if (!cmbLoadRequest.Text.Contains("Taxi_"))
				{
					if (cmbLoadRequest.Text.Contains("Token"))
					{
						header = "";
					}
					else
					{
						header = "Username:##;SecurityKey:##";
					}
				}
				else
				{
					header = "Username:Taxi;SecurityKey:P@ssw0rd";
				}
				AddColouredText(txtRequestHeader, header);

				#endregion

				#region Shows request content
				var fileName = cmbLoadRequest.Text;
				var fileNames = fileName.Split('/');
				if (fileNames.Count() > 1)
				{
					fileName = fileNames.Last();
				}
				string request = client.DownloadString(SampleDirectory + fileName);
				AddColouredText(txtRequest, request);
				#endregion
			}
		}
		#endregion

		#region MultiTasking
		private async void MutliTaskCall(int threadNumber, RequestDTO requesetDTO)
		{
			lblTime.Text = string.Empty;
			lblGUID.Text = Guid.NewGuid().ToString();
			Stopwatch sp = new Stopwatch();
			sp.Start();
			var tasks = new List<Task>();
			//SemaphoreSlim throttler = new SemaphoreSlim(2);
			//ThreadPool.SetMaxThreads(2, 2);

			for (int i = 0; i < threadNumber; i++)
			{

				Task task = new Task(new Action<object>(
					CallApi), requesetDTO, TaskCreationOptions.AttachedToParent);
				tasks.Add(task);
			}

			foreach (var item in tasks)
			{
				item.Start();
				logger.InfoFormat("----Main Start----");
			}

			await Task.WhenAll(tasks);
			//await Task.Run(() => { });
			sp.Stop();
			lblTime.Text = string.Format("{0} ElapsedMilliseconds", sp.ElapsedMilliseconds);

			logger.InfoFormat("----Main End----");
			logger.InfoFormat("----- {0} ------\r\n ElapsedMilliseconds: {1} \r\n {2} \r\n ----- END ------ \r\n", lblGUID.Text, sp.ElapsedMilliseconds, txtResult.Text);

		}

		private void CallApi(object request)
		{
			var threadID = System.Threading.Thread.CurrentThread.ManagedThreadId;
			logger.InfoFormat("----Thread {0} Start----", threadID);
			var startTime = DateTime.Now;
			Stopwatch sp = new Stopwatch();
			sp.Start();

			var requestDTO = (RequestDTO)request;
			var headers = new Dictionary<string, string>();
			var status = string.Empty;
			var contentType = string.Empty;
			var method = string.Empty;
			var response = string.Empty;
			var trackId = Guid.NewGuid().ToString();
			var requestString = string.Empty;

			if (requestDTO.rbtnXMLChecked)
				contentType = "application/xml; charset=utf-8";
			if (requestDTO.rbtnJSONChecked)
				contentType = "application/json; charset=utf-8";

			if (requestDTO.rbtnGetChecked)
				method = "GET";
			else if (requestDTO.rbtnPostChecked)
				method = "POST";

			try
			{
				if (!string.IsNullOrWhiteSpace(requestDTO.txtRequestHeaderText))
				{
					var header = requestDTO.txtRequestHeaderText.EndsWith(";") ? requestDTO.txtRequestHeaderText.Substring(0, requestDTO.txtRequestHeaderText.Length - 1) : requestDTO.txtRequestHeaderText;
					var headerStrings = header.Split(';');
					foreach (var item in headerStrings)
					{
						headers.Add(item.Split(':')[0], item.Split(':')[1]);
					}
				}

				#region Prepair Response - add TrackId to it
				if (txtUrlRequest.Text.ToLower().Contains("groundavail"))
				{
					if (requestDTO.rbtnXMLChecked)
					{
						var xRequest = XDocument.Parse(requestDTO.txtRequestText).CleanComment();//.CleanNamespace();
						var oRequest = xRequest.XmlDeserialize<OTA_GroundAvailRQ>();//.JsonSerialize<AuthRequest>();
						oRequest.EchoToken = trackId;
						requestString = oRequest.XmlSerialize().ToString();
					}
					else if (requestDTO.rbtnJSONChecked)
					{
						var oRequest = requestDTO.txtRequestText.JsonDeserialize<OTA_GroundAvailRQ>();//.JsonSerialize<AuthRequest>();
						oRequest.EchoToken = trackId;
						requestString = oRequest.JsonSerialize();
					}
				}
				if (txtUrlRequest.Text.ToLower().Contains("groundbook"))
				{
					if (requestDTO.rbtnXMLChecked)
					{
						var xRequest = XDocument.Parse(requestDTO.txtRequestText).CleanComment();//.CleanNamespace();
						var oRequest = xRequest.XmlDeserialize<OTA_GroundBookRQ>();//.JsonSerialize<AuthRequest>();
						oRequest.EchoToken = trackId;
						requestString = oRequest.XmlSerialize().ToString();
					}
					else if (requestDTO.rbtnJSONChecked)
					{
						var oRequest = requestDTO.txtRequestText.JsonDeserialize<OTA_GroundBookRQ>();//.JsonSerialize<AuthRequest>();
						oRequest.EchoToken = trackId;
						requestString = oRequest.JsonSerialize();
					}
				}
				if (txtUrlRequest.Text.ToLower().Contains("groundcancel"))
				{
					if (requestDTO.rbtnXMLChecked)
					{
						var xRequest = XDocument.Parse(requestDTO.txtRequestText).CleanComment();//.CleanNamespace();
						var oRequest = xRequest.XmlDeserialize<OTA_GroundCancelRQ>();//.JsonSerialize<AuthRequest>();
						oRequest.EchoToken = trackId;
						requestString = oRequest.XmlSerialize().ToString();
					}
					else if (requestDTO.rbtnJSONChecked)
					{
						var oRequest = requestDTO.txtRequestText.JsonDeserialize<OTA_GroundCancelRQ>();//.JsonSerialize<AuthRequest>();
						oRequest.EchoToken = trackId;
						requestString = oRequest.JsonSerialize();
					}
				}
				if (txtUrlRequest.Text.ToLower().Contains("groundresretrieve"))
				{
					if (requestDTO.rbtnXMLChecked)
					{
						var xRequest = XDocument.Parse(requestDTO.txtRequestText).CleanComment();//.CleanNamespace();
						var oRequest = xRequest.XmlDeserialize<OTA_GroundResRetrieveRQ>();//.JsonSerialize<AuthRequest>();
						oRequest.EchoToken = trackId;
						requestString = oRequest.XmlSerialize().ToString();
					}
					else if (requestDTO.rbtnJSONChecked)
					{
						var oRequest = requestDTO.txtRequestText.JsonDeserialize<OTA_GroundResRetrieveRQ>();//.JsonSerialize<AuthRequest>();
						oRequest.EchoToken = trackId;
						requestString = oRequest.JsonSerialize();
					}
				}
				if (txtUrlRequest.Text.ToLower().Contains("gettoken"))
				{
					if (requestDTO.rbtnXMLChecked)
					{
						var xRequest = XDocument.Parse(requestDTO.txtRequestText).CleanComment();//.CleanNamespace();
						var oRequest = xRequest.XmlDeserialize<TokenRQ>();//.JsonSerialize<AuthRequest>();
						oRequest.EchoToken = trackId;
						requestString = oRequest.XmlSerialize().ToString();
					}
					else if (requestDTO.rbtnJSONChecked)
					{
						var oRequest = requestDTO.txtRequestText.JsonDeserialize<TokenRQ>();//.JsonSerialize<AuthRequest>();
						oRequest.EchoToken = trackId;
						requestString = oRequest.JsonSerialize();
					}
				}
				#endregion

				logger.InfoFormat("----Thread {0} Start Call----", threadID);

				response = UDI.Utility.Helper.WebRequestHelper.ProcessWebRequest(
								requestDTO.txtUrlRequestText,
								requestString,
								contentType,
								method,
								headers,
								out status);
				//System.Threading.Thread.Sleep(120000);
				logger.InfoFormat("----Thread {0} End of call----", threadID);
			}
			catch (Exception ex)
			{
				response = ex.Message;
			}

			#region Formatting

			try
			{
				if (requestDTO.rbtnXMLChecked)
				{
					response = XDocument.Parse(response).CleanNamespace().CleanComment().ToString();
				}
				else if (requestDTO.rbtnJSONChecked)
				{
					JObject json = JObject.Parse(response);
					response = json.ToString();
				}
			}
			catch { }
			#endregion

			sp.Stop();
			var endTime = DateTime.Now;

			txtResult.Invoke(new Action(() =>
			{
				txtResult.Text += string.Format("* TrackID: {0}, ThreadID: {1}, ElapsedMilliseconds: {2}, Start: {3}, End: {4}  \r\n", trackId, threadID, sp.ElapsedMilliseconds, startTime, endTime);
			}));
			logger.InfoFormat("----Thread {0} End----", threadID);
		}
		#endregion

	}

}
