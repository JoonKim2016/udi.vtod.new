﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UDI.VTOD.Domain.ECar.Aleph;

namespace UDI.VTOD.Utilities.AlephAPIUnitTests
{
    [TestClass]
    public class Corporation : TestBase
    {
        [TestMethod]
        public void GetCorporationRequirements()
        {
            var client = new AlephAPICall();

            var response = client.GetCorporateRequirements(
                string.Empty, string.Empty, string.Empty, UserName, Password);

            Assert.IsNotNull(response);

        }

        [TestMethod]
        public void GetCorporationPaymentMethods()
        {
            var client = new AlephAPICall();

            var response = client.GetCorporatePaymentMethods(null, null, UserName, Password);
            Assert.IsNotNull(response);

            response = client.GetCorporatePaymentMethods("One Account, Inc", null, UserName, Password);
            Assert.IsNotNull(response);

            response = client.GetCorporatePaymentMethods("One Account, Inc", "Test Account", UserName, Password);
            Assert.IsNotNull(response);
        }
    }
}
