﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using UDI.Utility.Helper;
using UDI.Utility.Serialization;
using UDI.VTOD.Utilities.AlephConsoleTester.Request;
using UDI.VTOD.Utilities.AlephConsoleTester.Response;



namespace UDI.VTOD.Utilities.AlephConsoleTester
{
	class Program
	{
		static void Main(string[] args)
		{

			#region General Setup

			Dictionary<string, string> requestHeader = new Dictionary<string, string>();
			Dictionary<string, string> responseHeader = new Dictionary<string, string>();

			string HttpStatus = "";
			int timeout = 30000;
			string url = "https://dev-clientapi.sedanmagic.com";
			string urlSuffix = "";
			string content = "";
			string statusResult = "";
			//string userName = "wissam@unified-dispatch.com";
			//string userName = "chang@unified-dispatch.com";
			//string userName = "pouyan@unified-dispatch.com";
			//string userName = "alephtest1@unified-dispatch.com";
			string userName = "alephtest5@unified-dispatch.com";
			//string password = "pass123";
			//string password = "qqqqqq";
			//string alephPassword = "cfd45da827c941b5b4cf15938eb792a0";
			//string alephPassword = "cfd45da8-27c9-41b5-b4cf-15938eb792a0";
			//string alephPassword = "6c79fd33-7c86-462a-a5ef-f57b2b444817";
			//string alephPassword = "5a3cceaf-cd8c-4146-8407-c9d12230100e";
			string alephPassword = "98172411-260d-4578-9c4f-56e60bbccf0c";
			//string userPassBase64 = "d2lzc2FtQHVuaWZpZWQtZGlzcGF0Y2guY29tOnBhc3MxMjM=";

			#endregion

			#region General approach for making sure user's password is synced with Brain
			#region 1- Unregister (Delete) User
			requestHeader = new Dictionary<string, string>();
			responseHeader = new Dictionary<string, string>();
            DeleteUserRequest deleteUserRequest = new DeleteUserRequest();


            //Define url suffix for this command
            urlSuffix = "/user/ztrip/registration";

            //Add Content        
            deleteUserRequest.username = userName;

            //Serialize content class
            content = deleteUserRequest.JsonSerialize();

            //Add Headers
            requestHeader.Add("Accept", "application/json");
            requestHeader.Add("Api-Key", "06913B08-5228-4CB2-BD77-E6485C529ACE");
            requestHeader.Add("Content-Type", "application/json");
            //requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, alephPassword));
            requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, alephPassword));

            statusResult = "";

            try
            {
                statusResult = ProcessWebRequestEx(url + urlSuffix, content, "application/json", "DELETE", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out responseHeader);
                //regResponse = statusResult.JsonDeserialize<RegistrationResponse>();
            }
            catch (Exception e)
            {
                statusResult = e.Message;
            }


			#endregion Delete User

			#region 2- Register User again
            requestHeader = new Dictionary<string, string>();
            responseHeader = new Dictionary<string, string>();
            RegisterUserRequest registrationRequest = new RegisterUserRequest();
            RegisterUserResponse registrationResponse = new RegisterUserResponse();

            //Define url suffix for this command
            urlSuffix = "/user/ztrip/registration";


            //Add Content            
            registrationRequest.username = userName;
            registrationRequest.password = alephPassword;

            //Serialize content class
            content = registrationRequest.JsonSerialize();



            //Add Headers
            requestHeader.Add("Accept", "application/json");
            requestHeader.Add("Api-Key", "06913B08-5228-4CB2-BD77-E6485C529ACE");
            requestHeader.Add("Content-Type", "application/json");

            statusResult = "";

            try
            {
                statusResult = ProcessWebRequestEx(url + urlSuffix, content, "application/json", "POST", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out responseHeader);
                registrationResponse = statusResult.JsonDeserialize<RegisterUserResponse>();
            }
            catch (Exception e)
            {
                statusResult = e.Message;
            }

			#endregion Register User
			#endregion

			//************** Phase 1 ***********************

			#region registrationverification
			/*
            requestHeader = new Dictionary<string, string>();
            responseHeader = new Dictionary<string, string>();
            GetProfilesResponse getProfilesResponse = new GetProfilesResponse();


            //Define url suffix for this command
            urlSuffix = "/registrationverification/1011/Test_Fleet_1/TD-01/Chang";


            //Add Headers
            requestHeader.Add("Accept", "application/json");
            requestHeader.Add("Api-Key", "06913B08-5228-4CB2-BD77-E6485C529ACE");
            requestHeader.Add("Content-Type", "application/json");
            requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, alephPassword));


            content = "";
            statusResult = "";

            try
            {
                statusResult = ProcessWebRequestEx(url + urlSuffix, content, "application/json", "POST", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out responseHeader);
                getProfilesResponse.profiles = statusResult.JsonDeserialize<List<UserProfile>>();

            }
            catch (Exception e)
            {
                statusResult = e.Message;
            }


            Console.Write(statusResult);
            Console.Read();
			*/
			#endregion

			#region Add Device (Register User)
			/*
            requestHeader = new Dictionary<string, string>();
            responseHeader = new Dictionary<string, string>();
            RegistrationRequest regRequest = new RegistrationRequest();
            RegistrationResponse regResponse = new RegistrationResponse();


            //Define url suffix for this command
            urlSuffix = "/registration/1/Sedan_Magic_Aleph/1/wissam/XXX";
            
            
            //Add Content
            regRequest.provider_id = 1;
            regRequest.provider_account_id = "1";
            regRequest.company_name = "Sedan_Magic_Aleph";
            regRequest.profile_id = "wissam";
            regRequest.username = "wissam@unified-dispatch.com";
            regRequest.email_address = "wissam@unified-dispatch.com";
            regRequest.password = "pass123";
            regRequest.first_name = "Wissam";
            regRequest.last_name = "Ahmed";
            regRequest.device_token = "XXX";
            regRequest.phone_number = "777-222-1111";
            regRequest.platform_name = "Android";
            regRequest.platform_version = "4.4";


            //Serialize content class
            content = regRequest.JsonSerialize();


            //Add Headers
            requestHeader.Add("Accept", "application/json");
            requestHeader.Add("Api-Key", "06913B08-5228-4CB2-BD77-E6485C529ACE");
            requestHeader.Add("Content-Type", "application/json");
            requestHeader.Add("Content-Length", content.Length.ToString());

            statusResult = "";
            //string url = string.Format("{0}/{1}", GetHttpURL(ConfigurationManager.AppSettings["GreenTomatoAPI_URL_Status"]), esp.Trip.ecar_trip.DispatchTripId);
            statusResult = ProcessWebRequestEx(url + urlSuffix, content, "application/json", "POST", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out responseHeader);         
            regResponse = statusResult.JsonDeserialize<RegistrationResponse>();
            
            Console.Write(statusResult);
            Console.Read();
             */
			#endregion Add Device (Register User)

			#region Update Device Registration Info
			/*
            requestHeader = new Dictionary<string, string>();
            responseHeader = new Dictionary<string, string>();
            RegistrationRequest updRequest = new RegistrationRequest();
            RegistrationResponse updResponse = new RegistrationResponse();


            //Define url suffix for this command
            urlSuffix = "/registration/1/Sedan_Magic_Aleph/1/wissam/XXX";


            //Add Content
            updRequest.provider_id = 1;
            updRequest.provider_account_id = "1";
            updRequest.company_name = "Sedan_Magic_Aleph";
            updRequest.profile_id = "wissam";
            updRequest.username = "wissam@unified-dispatch.com";
            updRequest.email_address = "wissam@unified-dispatch.com";
            updRequest.password = "pass123";
            updRequest.first_name = "Wissam";
            updRequest.last_name = "Ahmed";
            updRequest.device_token = "XXX";
            updRequest.phone_number = "666-222-1111";
            updRequest.platform_name = "Android";
            updRequest.platform_version = "4.4";


            //Serialize content class
            content = updRequest.JsonSerialize();


            //Add Headers
            requestHeader.Add("Accept", "application/json");
            requestHeader.Add("Api-Key", "06913B08-5228-4CB2-BD77-E6485C529ACE");
            requestHeader.Add("Content-Type", "application/json");
            requestHeader.Add("Content-Length", content.Length.ToString());
            requestHeader.Add("Authorization", "Basic " + userPassBase64);


            statusResult = "";


            try
            {
                statusResult = ProcessWebRequestEx(url + urlSuffix, content, "application/json", "PUT", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out responseHeader);
                updResponse = statusResult.JsonDeserialize<RegistrationResponse>();
            }
            catch (Exception e)
            {
                statusResult = e.Message;
            }



            Console.Write(statusResult);
            Console.Read();
            */

			#endregion Update Device Registration Info

			#region Get Device Registration Info
			/*
            requestHeader = new Dictionary<string, string>();
            responseHeader = new Dictionary<string, string>();
            RegistrationRequest regRequest = new RegistrationRequest();
            RegistrationResponse regResponse = new RegistrationResponse();


            //Define url suffix for this command
            urlSuffix = "/registration/1/Sedan_Magic_Aleph/1/wissam/XXX";
            
                     
            //Add Headers
            requestHeader.Add("Accept", "application/json");
            requestHeader.Add("Api-Key", "06913B08-5228-4CB2-BD77-E6485C529ACE");
            requestHeader.Add("Content-Type", "application/json");
            requestHeader.Add("Authorization", "Basic " + userPassBase64);

            statusResult = "";

            try
            {
                statusResult = ProcessWebRequestEx(url + urlSuffix, content, "application/json", "GET", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out responseHeader);
                regResponse = statusResult.JsonDeserialize<RegistrationResponse>();
            }
            catch (Exception e)
            {
                statusResult = e.Message;
            }
            
            
            
            Console.Write(statusResult);
            Console.Read();
            */
			#endregion Get Device Registration Info

			#region Add Reservation
			/*
            requestHeader = new Dictionary<string, string>();
            responseHeader = new Dictionary<string, string>();
            ReservationRequest addReservationRequest = new ReservationRequest();
            ReservationResponse addReservationResponse = new ReservationResponse();
            ReservationAddress pickupAddress = new ReservationAddress();
            ReservationAddress dropoffAddress = new ReservationAddress();
            AlephBookingCriteria bookingCriteria = null;

            //Define url suffix for this command
            urlSuffix = "/reservation/1/Sedan_Magic_Aleph/1/wissam";


            //Add Content
            
            //PickupAddress
            pickupAddress.street_no = "10";
            pickupAddress.street_name = "Main St";
            pickupAddress.city = "Warwick";
            pickupAddress.state = "NY";
            pickupAddress.zip_code = "10990";
            pickupAddress.country = "USA";
            pickupAddress.auto_complete = "10 Main Street Warwick, NY 10990";
            pickupAddress.address_type = "Airport";
            pickupAddress.airport_code = "EWR";
            pickupAddress.airline = "American";
            pickupAddress.flight_number = "2046";
            pickupAddress.city_of_origin = "Newark";
            pickupAddress.Latitude = 41.306085;
            pickupAddress.Longitude = -74.44756;


            //DropOffAddress
            dropoffAddress.street_no = "32";
            dropoffAddress.street_name = "W 42 St";
            dropoffAddress.city = "Manhattan";
            dropoffAddress.state = "NY";
            dropoffAddress.zip_code = "10036";
            dropoffAddress.country = "USA";
            dropoffAddress.auto_complete = "W 42 St New York, NY 10036";
            dropoffAddress.address_type = "Street Address";
            dropoffAddress.Latitude = 41.38673;
            dropoffAddress.Longitude = -74.452986;

            
            addReservationRequest.provider_id = 1;
            addReservationRequest.provider_account_id = "1";
            addReservationRequest.company_name = "Sedan_Magic_Aleph";
            addReservationRequest.profile_id = "wissam";
            addReservationRequest.requested_datetime = DateTime.Now;
            addReservationRequest.payment_method = "voucher";
            addReservationRequest.special_instructions = "THIS IS A TEST.  DO NOT DISPATCH...";
            addReservationRequest.pickup = pickupAddress;
            addReservationRequest.dropoff = dropoffAddress;
            addReservationRequest.booking_criteria = new List<AlephBookingCriteria>();


            
            //Booking Criteria
            bookingCriteria = new AlephBookingCriteria();
            bookingCriteria.booking_criterion_name = "Comp. Req. One";
            bookingCriteria.booking_criterion_value = "XXX";
            addReservationRequest.booking_criteria.Add(bookingCriteria);


            bookingCriteria = new AlephBookingCriteria();
            bookingCriteria.booking_criterion_name = "Comp. Req. Two";
            bookingCriteria.booking_criterion_value = "YYY";
            addReservationRequest.booking_criteria.Add(bookingCriteria);
            

            //Serialize content class
            content = addReservationRequest.JsonSerialize();


            //Add Headers
            requestHeader.Add("Accept", "application/json");
            requestHeader.Add("Api-Key", "06913B08-5228-4CB2-BD77-E6485C529ACE");
            requestHeader.Add("Content-Type", "application/json");
            requestHeader.Add("Authorization", "Basic " + userPassBase64);
            requestHeader.Add("Content-Length", content.Length.ToString());

                        
            statusResult = "";

            try
            {
                statusResult = ProcessWebRequestEx(url + urlSuffix, content, "application/json", "POST", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out responseHeader);
                addReservationResponse = statusResult.JsonDeserialize<ReservationResponse>();
            }
            catch (Exception e)
            {
                statusResult = e.Message;
            }



            Console.Write(statusResult);
            Console.Read();
            */

			#endregion Add Reservation

			#region Modify Reservation
			/*
            requestHeader = new Dictionary<string, string>();
            responseHeader = new Dictionary<string, string>();
            ModifyReservationRequest modifyReservationRequest = new ModifyReservationRequest();
            ReservationResponse modifyReservationResponse = new ReservationResponse();
            ReservationAddress pickupAddress = new ReservationAddress();
            ReservationAddress dropoffAddress = new ReservationAddress();
            AlephBookingCriteria bookingCriteria = null;

            //Define url suffix for this command
            urlSuffix = "/reservation/1/Sedan_Magic_Aleph/1/wissam/0122203605";


            //Add Content
            
            //PickupAddress
            pickupAddress.street_no = "10";
            pickupAddress.street_name = "Main St";
            pickupAddress.city = "Warwick";
            pickupAddress.state = "NY";
            pickupAddress.zip_code = "10990";
            pickupAddress.country = "USA";
            pickupAddress.auto_complete = "10 Main Street Warwick, NY 10990";
            pickupAddress.address_type = "Airport";
            pickupAddress.airport_code = "EWR";
            pickupAddress.airline = "American";
            pickupAddress.flight_number = "2046";
            pickupAddress.city_of_origin = "Newark";
            pickupAddress.Latitude = 41.306085;
            pickupAddress.Longitude = -74.44756;


            //DropOffAddress
            dropoffAddress.street_no = "32";
            dropoffAddress.street_name = "W 42 St";
            dropoffAddress.city = "Manhattan";
            dropoffAddress.state = "NY";
            dropoffAddress.zip_code = "10036";
            dropoffAddress.country = "USA";
            dropoffAddress.auto_complete = "W 42 St New York, NY 10036";
            dropoffAddress.address_type = "Street Address";
            dropoffAddress.Latitude = 41.38673;
            dropoffAddress.Longitude = -74.452986;


            
            modifyReservationRequest.provider_id = 1;
            modifyReservationRequest.provider_account_id = "1";
            modifyReservationRequest.company_name = "Sedan_Magic_Aleph";
            modifyReservationRequest.profile_id = "wissam";
            modifyReservationRequest.vendor_confirmation_no = "0122203605";
            modifyReservationRequest.requested_datetime = DateTime.Now;
            modifyReservationRequest.payment_method = "voucher";
            modifyReservationRequest.special_instructions = "THIS IS A TEST.  DO NOT DISPATCH...";
            modifyReservationRequest.pickup = pickupAddress;
            modifyReservationRequest.dropoff = dropoffAddress;
            modifyReservationRequest.booking_criteria = new List<AlephBookingCriteria>();

            //Booking Criteria
            bookingCriteria = new AlephBookingCriteria();
            bookingCriteria.booking_criterion_name = "Comp. Req. One";
            bookingCriteria.booking_criterion_value = "XXX";
            modifyReservationRequest.booking_criteria.Add(bookingCriteria);


            bookingCriteria = new AlephBookingCriteria();
            bookingCriteria.booking_criterion_name = "Comp. Req. Two";
            bookingCriteria.booking_criterion_value = "YYY";
            modifyReservationRequest.booking_criteria.Add(bookingCriteria);


            //Serialize content class
            content = modifyReservationRequest.JsonSerialize();


            //Add Headers
            requestHeader.Add("Accept", "application/json");
            requestHeader.Add("Api-Key", "06913B08-5228-4CB2-BD77-E6485C529ACE");
            requestHeader.Add("Content-Type", "application/json");
            requestHeader.Add("Authorization", "Basic " + userPassBase64);
            requestHeader.Add("Content-Length", content.Length.ToString());

                        
            statusResult = "";

            try
            {
                statusResult = ProcessWebRequestEx(url + urlSuffix, content, "application/json", "PUT", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out responseHeader);
                modifyReservationResponse = statusResult.JsonDeserialize<ReservationResponse>();
            }
            catch (Exception e)
            {
                statusResult = e.Message;
            }



            Console.Write(statusResult);
            Console.Read();
            
            */
			#endregion Modify Reservation

			#region Get Reservations (Profile Id)
			/*            
            requestHeader = new Dictionary<string, string>();
            responseHeader = new Dictionary<string, string>();
            ReservationsResponse getResvWithProfileIdResponse = new ReservationsResponse();
            

            //Define url suffix for this command
            urlSuffix = "/reservation/1/Sedan_Magic_Aleph/1/wissam";
                          

            //Add Headers
            requestHeader.Add("Accept", "application/json");
            requestHeader.Add("Api-Key", "06913B08-5228-4CB2-BD77-E6485C529ACE");
            requestHeader.Add("Content-Type", "application/json");
            requestHeader.Add("Authorization", "Basic " + userPassBase64);


            content = "";
            statusResult = "";

            try 
            {
                statusResult = ProcessWebRequestEx(url + urlSuffix, content, "application/json", "GET", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out responseHeader);
               
                getResvWithProfileIdResponse.reservations = statusResult.JsonDeserialize<List<ReservationResponse>>();
            }
            catch(Exception e)
            {
                statusResult = e.Message;
            }
                
                

            Console.Write(statusResult);
            Console.Read();
            */
			#endregion Get Reservations (Profile Id)

			#region Get Reservation (Confirmation Number)
			/*
            requestHeader = new Dictionary<string, string>();
            responseHeader = new Dictionary<string, string>();
            ReservationResponse getResvWithConfNumResponse = new ReservationResponse();


            //Define url suffix for this command
            urlSuffix = "/reservation/1/Sedan_Magic_Aleph/1/wissam/0122203605";


            //Add Headers
            requestHeader.Add("Accept", "application/json");
            requestHeader.Add("Api-Key", "06913B08-5228-4CB2-BD77-E6485C529ACE");
            requestHeader.Add("Content-Type", "application/json");
            requestHeader.Add("Authorization", "Basic " + userPassBase64);


            content = "";
            statusResult = "";

            try
            {
                statusResult = ProcessWebRequestEx(url + urlSuffix, content, "application/json", "GET", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out responseHeader);
                getResvWithConfNumResponse = statusResult.JsonDeserialize<ReservationResponse>();
            }
            catch (Exception e)
            {
                statusResult = e.Message;
            }


            Console.Write(statusResult);
            Console.Read();
            */
			#endregion Get Reservation (Confirmation Number)

			#region Delete Reservation
			/*
            requestHeader = new Dictionary<string, string>();
            responseHeader = new Dictionary<string, string>();
            ReservationResponse delResvResponse = new ReservationResponse();


            //Define url suffix for this command
            urlSuffix = "/reservation/1/Sedan_Magic_Aleph/1/wissam/0122203605";


            //Add Headers
            requestHeader.Add("Accept", "application/json");
            requestHeader.Add("Api-Key", "06913B08-5228-4CB2-BD77-E6485C529ACE");
            requestHeader.Add("Content-Type", "application/json");
            requestHeader.Add("Authorization", "Basic " + userPassBase64);


            content = "";
            statusResult = "";

            try
            {
                statusResult = ProcessWebRequestEx(url + urlSuffix, content, "application/json", "DELETE", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out responseHeader);
                delResvResponse = statusResult.JsonDeserialize<ReservationResponse>();
            }
            catch (Exception e)
            {
                statusResult = e.Message;
            }



            Console.Write(statusResult);
            Console.Read();
            */
			#endregion Delete Reservation

			#region Get Booking Criteria
			#region For Book later
			/*
            requestHeader = new Dictionary<string, string>();
            responseHeader = new Dictionary<string, string>();
            GetBookingCriteriaResponse getBookingCriteraResponse = new GetBookingCriteriaResponse();


            //Define url suffix for this command
            urlSuffix = "/accountuserbookingcriterion/1/Sedan_Magic_Aleph/1/wissam";


            //Add Headers
            requestHeader.Add("Accept", "application/json");
            requestHeader.Add("Api-Key", "06913B08-5228-4CB2-BD77-E6485C529ACE");
            requestHeader.Add("Content-Type", "application/json");
			//requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, alephPassword));
			requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, password));


            content = "";
            statusResult = "";

            try
            {
                statusResult = ProcessWebRequestEx(url + urlSuffix, content, "application/json", "GET", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out responseHeader);
                getBookingCriteraResponse.booking_criteria = statusResult.JsonDeserialize<List<AlephBookingCriteria>>();

            }
            catch (Exception e)
            {
                statusResult = e.Message;
            }


            Console.Write(statusResult);
            Console.Read();
			*/

			#endregion            #endregion Get Booking Criteria

			#region For Book Now
			/*
			requestHeader = new Dictionary<string, string>();
			responseHeader = new Dictionary<string, string>();
			GetBookingCriteriaResponse getBookingCriteraResponse = new GetBookingCriteriaResponse();


			//Define url suffix for this command
			urlSuffix = "/corporate/bookingcriteria";


			//Add Headers
			requestHeader.Add("Accept", "application/json");
			requestHeader.Add("Api-Key", "06913B08-5228-4CB2-BD77-E6485C529ACE");
			requestHeader.Add("Content-Type", "application/json");
			//requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, alephPassword));
			requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, password));


			content = "";
			statusResult = "";

			try
			{
				statusResult = ProcessWebRequestEx(url + urlSuffix, content, "application/json", "GET", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out responseHeader);
				getBookingCriteraResponse.booking_criteria = statusResult.JsonDeserialize<List<AlephBookingCriteria>>();

			}
			catch (Exception e)
			{
				statusResult = e.Message;
			}


			Console.Write(statusResult);
			Console.Read();
			*/
			#endregion
			#endregion Get Booking Criteria

			#region Verify User
			/*
            requestHeader = new Dictionary<string, string>();
            responseHeader = new Dictionary<string, string>();
            VerifyUserRequest verificationRequest = new VerifyUserRequest();
            VerifyUserResponse verificationResponse = new VerifyUserResponse();

            //Define url suffix for this command
            urlSuffix = "/user/verification";


            //Add Content            
            verificationRequest.username = userName;
            //verificationRequest.username = "";

            //Serialize content class
            content = verificationRequest.JsonSerialize();



            //Add Headers
            requestHeader.Add("Accept", "application/json");
            requestHeader.Add("Api-Key", "06913B08-5228-4CB2-BD77-E6485C529ACE");
            requestHeader.Add("Content-Type", "application/json");

            statusResult = "";

            try
            {
                statusResult = ProcessWebRequestEx(url + urlSuffix, content, "application/json", "POST", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out responseHeader);
                verificationResponse = statusResult.JsonDeserialize<VerifyUserResponse>();
            }
            catch (Exception e)
            {
                statusResult = e.Message;
            }



            Console.Write(statusResult);
            Console.Read();
            */
			#endregion Verify User

			#region Delete User
			/*
			requestHeader = new Dictionary<string, string>();
			responseHeader = new Dictionary<string, string>();
			DeleteUserRequest deleteUserRequest = new DeleteUserRequest();


			//Define url suffix for this command
			urlSuffix = "/user/ztrip/registration";

			//Add Content        
			deleteUserRequest.username = userName;

			//Serialize content class
			content = deleteUserRequest.JsonSerialize();

			//Add Headers
			requestHeader.Add("Accept", "application/json");
			requestHeader.Add("Api-Key", "06913B08-5228-4CB2-BD77-E6485C529ACE");
			requestHeader.Add("Content-Type", "application/json");
			//requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, alephPassword));
			requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, password));

			statusResult = "";

			try
			{
				statusResult = ProcessWebRequestEx(url + urlSuffix, content, "application/json", "DELETE", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out responseHeader);
				//regResponse = statusResult.JsonDeserialize<RegistrationResponse>();
			}
			catch (Exception e)
			{
				statusResult = e.Message;
			}



			Console.Write(statusResult);
			Console.Read();
			*/
			#endregion Delete User

			#region Register User
			/*
			requestHeader = new Dictionary<string, string>();
			responseHeader = new Dictionary<string, string>();
			RegisterUserRequest registrationRequest = new RegisterUserRequest();
			RegisterUserResponse registrationResponse = new RegisterUserResponse();

			//Define url suffix for this command
			urlSuffix = "/user/ztrip/registration";


			//Add Content            
			registrationRequest.username = userName;
			registrationRequest.password = alephPassword;

			//Serialize content class
			content = registrationRequest.JsonSerialize();



			//Add Headers
			requestHeader.Add("Accept", "application/json");
			requestHeader.Add("Api-Key", "06913B08-5228-4CB2-BD77-E6485C529ACE");
			requestHeader.Add("Content-Type", "application/json");

			statusResult = "";

			try
			{
				statusResult = ProcessWebRequestEx(url + urlSuffix, content, "application/json", "POST", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out responseHeader);
				registrationResponse = statusResult.JsonDeserialize<RegisterUserResponse>();
			}
			catch (Exception e)
			{
				statusResult = e.Message;
			}



			Console.Write(statusResult);
			Console.Read();
			*/
			#endregion Register User

			#region Get User Profiles

			//requestHeader = new Dictionary<string, string>();
			//responseHeader = new Dictionary<string, string>();
			//GetProfilesResponse getProfilesResponse = new GetProfilesResponse();


			/////Define url suffix for this command
			//urlSuffix = "/user/profile";


			/////Add Headers
			//requestHeader.Add("Accept", "application/json");
			//requestHeader.Add("Api-Key", "06913B08-5228-4CB2-BD77-E6485C529ACE");
			//requestHeader.Add("Content-Type", "application/json");
			//requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, alephPassword));


			//content = "";
			//statusResult = "";

			//try
			//{
			//    statusResult = ProcessWebRequestEx(url + urlSuffix, content, "application/json", "GET", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out responseHeader);
			//    getProfilesResponse.profiles = statusResult.JsonDeserialize<List<UserProfile>>();

			//}
			//catch (Exception e)
			//{
			//    statusResult = e.Message;
			//}


			//Console.Write(statusResult);
			//Console.Read();

			#endregion Get User Profiles

			#region Change User Password
			/*
            requestHeader = new Dictionary<string, string>();
            responseHeader = new Dictionary<string, string>();
            ChangeUserPasswordRequest changeUserPasswordRequest = new ChangeUserPasswordRequest();


            //Define url suffix for this command
            urlSuffix = "/user/ztrip/registration";


            //Add Content        
            changeUserPasswordRequest.username = userName;
            changeUserPasswordRequest.password = alephPassword;
            changeUserPasswordRequest.new_password = alephPassword;

            //Serialize content class
            content = changeUserPasswordRequest.JsonSerialize();
            

            //Add Headers
            requestHeader.Add("Accept", "application/json");
            requestHeader.Add("Api-Key", "06913B08-5228-4CB2-BD77-E6485C529ACE");
            requestHeader.Add("Content-Type", "application/json");
            requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, alephPassword));
            statusResult = "";

            try
            {
                statusResult = ProcessWebRequestEx(url + urlSuffix, content, "application/json", "PUT", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out responseHeader);
                //regResponse = statusResult.JsonDeserialize<RegistrationResponse>();
            }
            catch (Exception e)
            {
                statusResult = e.Message;
            }
            

            Console.Write(statusResult);
            Console.Read();
            */
			#endregion Change User Password

			#region Get User Status
			/*
            requestHeader = new Dictionary<string, string>();
            responseHeader = new Dictionary<string, string>();
            RegistrationRequest regRequest = new RegistrationRequest();
            RegistrationResponse regResponse = new RegistrationResponse();


            //Define url suffix for this command
            urlSuffix = "/accountuserstatus/1/Sedan_Magic_Aleph/1/wissam";
            
                     
            //Add Headers
            requestHeader.Add("Accept", "application/json");
            requestHeader.Add("Api-Key", "06913B08-5228-4CB2-BD77-E6485C529ACE");
            requestHeader.Add("Content-Type", "application/json");
            requestHeader.Add("Authorization", "Basic " + userPassBase64);

            statusResult = "";

            try
            {
                statusResult = ProcessWebRequestEx(url + urlSuffix, content, "application/json", "GET", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out responseHeader);
                regResponse = statusResult.JsonDeserialize<RegistrationResponse>();
            }
            catch (Exception e)
            {
                statusResult = e.Message;
            }
            
            
            
            Console.Write(statusResult);
            Console.Read();
            */
			#endregion Get User Status

			#region Get Available Vehicles
			/*
            requestHeader = new Dictionary<string, string>();
            responseHeader = new Dictionary<string, string>();
            GetAvailableVehiclesResponse getAvailableVehiclesResponse = new GetAvailableVehiclesResponse();


            //Define url suffix for this command
            urlSuffix = "/vehicle/available"+"/47.6510/-122.3470/1.2/3";



            //Add Headers
            requestHeader.Add("Accept", "application/json");
            requestHeader.Add("Api-Key", "06913B08-5228-4CB2-BD77-E6485C529ACE");
            requestHeader.Add("Content-Type", "application/json");
            requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, alephPassword));


            content = "";
            statusResult = "";

            try
            {
                statusResult = ProcessWebRequestEx(url + urlSuffix, content, "application/json", "GET", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out responseHeader);
                getAvailableVehiclesResponse.vehicles = statusResult.JsonDeserialize<List<AvailableVehicle>>();
            }
            catch (Exception e)
            {
                statusResult = e.Message;
            }


            Console.Write(statusResult);
            Console.Read();
            */

			#endregion Get Available Vehicles

			//************** Phase 2 ***********************

			#region Company Requirements
			/*
            requestHeader = new Dictionary<string, string>();
            responseHeader = new Dictionary<string, string>();
            ReservationResponse delResvResponse = new ReservationResponse();


            //Define url suffix for this command
			urlSuffix = "/corporate/requirements";

			//Add Headers
            requestHeader.Add("Accept", "application/json");
            requestHeader.Add("Api-Key", "06913B08-5228-4CB2-BD77-E6485C529ACE");
            requestHeader.Add("Content-Type", "application/json");
			requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, alephPassword));


            content = "";
            statusResult = "";

            try
            {
                statusResult = ProcessWebRequestEx(url + urlSuffix, content, "application/json", "GET", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out responseHeader);
                delResvResponse = statusResult.JsonDeserialize<ReservationResponse>();
            }
            catch (Exception e)
            {
                statusResult = e.Message;
            }



            Console.Write(statusResult);
            Console.Read();
			*/
			#endregion Company Requirements

			#region typeahead_base_uri
			/*
            requestHeader = new Dictionary<string, string>();
            responseHeader = new Dictionary<string, string>();
            ReservationResponse delResvResponse = new ReservationResponse();


            //Define url suffix for this command
			urlSuffix = "/corporate/requirements/typeahead/one%20account%2C%20inc/test%20account/billing%20%20code/10/t";
			//urlSuffix = "/corporate/requirements/typeahead/one account, inc/test account/billing  code/10/t";

			//Add Headers
            requestHeader.Add("Accept", "application/json");
            requestHeader.Add("Api-Key", "06913B08-5228-4CB2-BD77-E6485C529ACE");
            requestHeader.Add("Content-Type", "application/json");
			requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, alephPassword));


            content = "";
            statusResult = "";

            try
            {
                statusResult = ProcessWebRequestEx(url + urlSuffix, content, "application/json", "GET", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out responseHeader);
                delResvResponse = statusResult.JsonDeserialize<ReservationResponse>();
            }
            catch (Exception e)
            {
                statusResult = e.Message;
            }



            Console.Write(statusResult);
            Console.Read();
			*/
			#endregion Company Requirements

			#region Paymentmethod
			/*
			requestHeader = new Dictionary<string, string>();
			responseHeader = new Dictionary<string, string>();
			ReservationResponse delResvResponse = new ReservationResponse();


			//Define url suffix for this command
			urlSuffix = "/corporate/paymentmethod";


			//Add Headers
			requestHeader.Add("Accept", "application/json");
			requestHeader.Add("Api-Key", "06913B08-5228-4CB2-BD77-E6485C529ACE");
			requestHeader.Add("Content-Type", "application/json");
			requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, alephPassword));


			content = "";
			statusResult = "";

			try
			{
				statusResult = ProcessWebRequestEx(url + urlSuffix, content, "application/json", "GET", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out responseHeader);
				delResvResponse = statusResult.JsonDeserialize<ReservationResponse>();
			}
			catch (Exception e)
			{
				statusResult = e.Message;
			}



			Console.Write(statusResult);
			Console.Read();
			*/
			#endregion Paymentmethod

			#region Add Reservation (company_phone_number was added to the reservation object)
			// 1- Save company_phone_number as Fleet Phone# in database
			// 2- Save company_name as Fleet Name

			/*
            requestHeader = new Dictionary<string, string>();
            responseHeader = new Dictionary<string, string>();
            ReservationRequest addReservationRequest = new ReservationRequest();
            ReservationResponse addReservationResponse = new ReservationResponse();
            ReservationAddress pickupAddress = new ReservationAddress();
            ReservationAddress dropoffAddress = new ReservationAddress();
            AlephBookingCriteria bookingCriteria = null;

            //Define url suffix for this command
            urlSuffix = "/reservation/1/Sedan_Magic_Aleph/1/pouyan";


            //Add Content
            
            //PickupAddress
            pickupAddress.street_no = "10";
            pickupAddress.street_name = "Main St";
            pickupAddress.city = "Warwick";
            pickupAddress.state = "NY";
            pickupAddress.zip_code = "10990";
            pickupAddress.country = "USA";
            pickupAddress.auto_complete = "10 Main Street Warwick, NY 10990";
            pickupAddress.address_type = "Airport";
            pickupAddress.airport_code = "EWR";
            pickupAddress.airline = "American";
            pickupAddress.flight_number = "2046";
            pickupAddress.city_of_origin = "Newark";
            pickupAddress.Latitude = 41.306085;
            pickupAddress.Longitude = -74.44756;


            //DropOffAddress
            dropoffAddress.street_no = "32";
            dropoffAddress.street_name = "W 42 St";
            dropoffAddress.city = "Manhattan";
            dropoffAddress.state = "NY";
            dropoffAddress.zip_code = "10036";
            dropoffAddress.country = "USA";
            dropoffAddress.auto_complete = "W 42 St New York, NY 10036";
            dropoffAddress.address_type = "Street Address";
            dropoffAddress.Latitude = 41.38673;
            dropoffAddress.Longitude = -74.452986;

            
            addReservationRequest.provider_id = 1;
            addReservationRequest.provider_account_id = "1";
            addReservationRequest.company_name = "Sedan_Magic_Aleph";
            addReservationRequest.profile_id = "pouyan";
            addReservationRequest.requested_datetime = DateTime.Now.AddDays(5);
            addReservationRequest.payment_method = "voucher";
            addReservationRequest.special_instructions = "THIS IS A TEST.  DO NOT DISPATCH...";
            addReservationRequest.pickup = pickupAddress;
            addReservationRequest.dropoff = dropoffAddress;
            addReservationRequest.booking_criteria = new List<AlephBookingCriteria>();


            
            //Booking Criteria
            bookingCriteria = new AlephBookingCriteria();
            bookingCriteria.booking_criterion_name = "Comp. Req. One";
            bookingCriteria.booking_criterion_value = "XXX";
            addReservationRequest.booking_criteria.Add(bookingCriteria);


            bookingCriteria = new AlephBookingCriteria();
            bookingCriteria.booking_criterion_name = "Comp. Req. Two";
            bookingCriteria.booking_criterion_value = "YYY";
            addReservationRequest.booking_criteria.Add(bookingCriteria);
            

            //Serialize content class
            content = addReservationRequest.JsonSerialize();


            //Add Headers
            requestHeader.Add("Accept", "application/json");
            requestHeader.Add("Api-Key", "06913B08-5228-4CB2-BD77-E6485C529ACE");
            requestHeader.Add("Content-Type", "application/json");
			requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, alephPassword));
			requestHeader.Add("Content-Length", content.Length.ToString());

                        
            statusResult = "";

            try
            {
                statusResult = ProcessWebRequestEx(url + urlSuffix, content, "application/json", "POST", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out responseHeader);
                addReservationResponse = statusResult.JsonDeserialize<ReservationResponse>();
            }
            catch (Exception e)
            {
                statusResult = e.Message;
            }



            Console.Write(statusResult);
            Console.Read();
			*/
			#endregion Add Reservation

			#region Display ALL reservations regardless of source of creation
			/*
			requestHeader = new Dictionary<string, string>();
			responseHeader = new Dictionary<string, string>();
			ReservationResponse delResvResponse = new ReservationResponse();


			//Define url suffix for this command
			//urlSuffix = "/corporate/reservation/corporation_name/account_name";
			//urlSuffix = "/corporate/reservation";
			urlSuffix = "/corporate/reservation?count=10";


			//Add Headers
			requestHeader.Add("Accept", "application/json");
			requestHeader.Add("Api-Key", "06913B08-5228-4CB2-BD77-E6485C529ACE");
			requestHeader.Add("Content-Type", "application/json");
			requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, alephPassword));


			content = "";
			statusResult = "";

			try
			{
				statusResult = ProcessWebRequestEx(url + urlSuffix, content, "application/json", "GET", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out responseHeader);
				delResvResponse = statusResult.JsonDeserialize<ReservationResponse>();
			}
			catch (Exception e)
			{
				statusResult = e.Message;
			}



			Console.Write(statusResult);
			Console.Read();
			*/
			#endregion

			#region PickupPoint
			/*
			requestHeader = new Dictionary<string, string>();
			responseHeader = new Dictionary<string, string>();
			ReservationResponse delResvResponse = new ReservationResponse();


			//Define url suffix for this command
			//urlSuffix = "/corporate/reservation/corporation_name/account_name";
			urlSuffix = "/airport/pickuppoint";


			//Add Headers
			requestHeader.Add("Accept", "application/json");
			requestHeader.Add("Api-Key", "06913B08-5228-4CB2-BD77-E6485C529ACE");
			requestHeader.Add("Content-Type", "application/json");
			requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, alephPassword));


			content = "";
			statusResult = "";

			try
			{
				statusResult = ProcessWebRequestEx(url + urlSuffix, content, "application/json", "GET", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out responseHeader);
				delResvResponse = statusResult.JsonDeserialize<ReservationResponse>();
			}
			catch (Exception e)
			{
				statusResult = e.Message;
			}



			Console.Write(statusResult);
			Console.Read();
			*/
			#endregion

			#region Resend Confirmation Email
			//requestHeader = new Dictionary<string, string>();



			////Define url suffix for this command
			//urlSuffix = "/corporate/reservation/confirmationemail" + "/VTOD%20Testing%20Corporation%201/VTOD-One/61060";



			////Add Headers
			//requestHeader.Add("Accept", "application/json");
			//requestHeader.Add("Api-Key", "06913B08-5228-4CB2-BD77-E6485C529ACE");
			//requestHeader.Add("Content-Type", "application/json");
			//requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, alephPassword));


			//content = "";
			//statusResult = "";

			//try
			//{
			//    statusResult = ProcessWebRequestEx(url + urlSuffix, content, "application/json", "GET", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out responseHeader);
			//    Console.Write(statusResult);
			//    // getAvailableVehiclesResponse.vehicles = statusResult.JsonDeserialize<List<AvailableVehicle>>();
			//}
			//catch (Exception e)
			//{
			//    statusResult = e.Message;
			//}


			//Console.Write(statusResult);
			//Console.Read();
			#endregion

			#region Device Register
			//requestHeader = new Dictionary<string, string>();
			//responseHeader = new Dictionary<string, string>();
			////RegisterUserRequest registrationRequest = new RegisterUserRequest();
			////RegisterUserResponse registrationResponse = new RegisterUserResponse();
			//RegisterDevice alephRegisterDeviceRequest = new RegisterDevice();
			//urlSuffix = "/user/device/" + "AWK334-23IDO-YT543";




			//requestHeader.Add("Accept", "application/json");
			//requestHeader.Add("Api-Key", "06913B08-5228-4CB2-BD77-E6485C529ACE");
			//requestHeader.Add("Content-Type", "application/json");
			//requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, alephPassword));


			//content = "";
			//statusResult = "";

			//#region GET
			//try
			//{
			//    statusResult = ProcessWebRequestEx(url + urlSuffix, content, "application/json", "GET", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out responseHeader);
			//    Console.Write(statusResult);
			//    // getAvailableVehiclesResponse.vehicles = statusResult.JsonDeserialize<List<AvailableVehicle>>();
			//}
			//catch (Exception e)
			//{
			//    statusResult = e.Message;
			//} 
			//#endregion

			//#region PUT
			//try
			//{

			//    #region Add Content
			//    alephRegisterDeviceRequest.device_token = "AWK334-23IDO-YT543";
			//    alephRegisterDeviceRequest.platform_name = "Android";
			//    alephRegisterDeviceRequest.platform_version = "5.0";
			//    alephRegisterDeviceRequest.phone_number = "17778881234";
			//    alephRegisterDeviceRequest.push_notification_token = null;
			//    alephRegisterDeviceRequest.push_notifications = false;
			//    alephRegisterDeviceRequest.sms_notifications = false;
			//    alephRegisterDeviceRequest.is_primary_device = true;
			//    content = alephRegisterDeviceRequest.JsonSerialize();
			//    #endregion Add Content
			//    statusResult = ProcessWebRequestEx(url + urlSuffix, content, "application/json", "POST", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out responseHeader);
			//    Console.Write(statusResult);

			//}
			//catch (Exception e)
			//{
			//    statusResult = e.Message;
			//} 
			//#endregion
			//#region DELETE
			//try
			//{
			//    alephRegisterDeviceRequest.device_token = "AWK334-23IDO-YT543";
			//    alephRegisterDeviceRequest.platform_name = "Android";
			//    alephRegisterDeviceRequest.platform_version = "6.0";
			//    alephRegisterDeviceRequest.phone_number = "17778881234";
			//    alephRegisterDeviceRequest.push_notification_token = null;
			//    alephRegisterDeviceRequest.push_notifications = false;
			//    alephRegisterDeviceRequest.sms_notifications = false;
			//    alephRegisterDeviceRequest.is_primary_device = true;
			//    content = alephRegisterDeviceRequest.JsonSerialize();
			//    statusResult = ProcessWebRequestEx(url + urlSuffix, content, "application/json", "PUT", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out responseHeader);
			//    Console.Write(statusResult);
			//    // getAvailableVehiclesResponse.vehicles = statusResult.JsonDeserialize<List<AvailableVehicle>>();
			//}
			//catch (Exception e)
			//{
			//    statusResult = e.Message;
			//}
			//#region POST
			//#endregion
			//try
			//{
			//    alephRegisterDeviceRequest.device_token = "AWK334-23IDO-YT543";
			//    alephRegisterDeviceRequest.platform_name = "Andriod";
			//    alephRegisterDeviceRequest.platform_version = "6.0";
			//    alephRegisterDeviceRequest.phone_number = "17778881234";
			//    alephRegisterDeviceRequest.push_notification_token = null;
			//    alephRegisterDeviceRequest.push_notifications = false;
			//    alephRegisterDeviceRequest.sms_notifications = false;
			//    alephRegisterDeviceRequest.is_primary_device = true;
			//    content = alephRegisterDeviceRequest.JsonSerialize();
			//    statusResult = ProcessWebRequestEx(url + urlSuffix, content, "application/json", "DELETE", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out responseHeader);
			//    Console.Write(statusResult);
			//    // getAvailableVehiclesResponse.vehicles = statusResult.JsonDeserialize<List<AvailableVehicle>>();
			//}
			//catch (Exception e)
			//{
			//    statusResult = e.Message;
			//}

			//#endregion
			//Console.Write(statusResult);
			//Console.Read();

			#endregion

			#region Lead Time
			//requestHeader = new Dictionary<string, string>();
			//responseHeader = new Dictionary<string, string>();
			//urlSuffix = "/corporate/location/config/40.7484/-73.9857";
			//requestHeader.Add("Accept", "application/json");
			//requestHeader.Add("Api-Key", "06913B08-5228-4CB2-BD77-E6485C529ACE");
			//requestHeader.Add("Content-Type", "application/json");
			//requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, alephPassword));


			//content = "";
			//statusResult = "";

			//#region GET
			//try
			//{
			//    statusResult = ProcessWebRequestEx(url + urlSuffix, content, "application/json", "GET", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out responseHeader);
			//    Console.Write(statusResult);
			//}
			//catch (Exception e)
			//{
			//    statusResult = e.Message;
			//}
			//#endregion
			//Console.Write(statusResult);
			//#region GET
			//urlSuffix = "/corporate/location/config/40.7484/-73.9857/Test Provider 1/";
			//try
			//{
			//    statusResult = ProcessWebRequestEx(url + urlSuffix, content, "application/json", "GET", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out responseHeader);
			//    Console.Write(statusResult);
			//}
			//catch (Exception e)
			//{
			//    statusResult = e.Message;
			//}
			//#endregion
			//Console.Write(statusResult);
			//#region GET
			//urlSuffix = "/corporate/location/config/40.7484/-73.9857/Test Provider 2/Test_Fleet_2/";
			//try
			//{
			//    statusResult = ProcessWebRequestEx(url + urlSuffix, content, "application/json", "GET", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out responseHeader);
			//    Console.Write(statusResult);
			//}
			//catch (Exception e)
			//{
			//    statusResult = e.Message;
			//}
			//#endregion
			//Console.Write(statusResult);
			#endregion

			#region GetCorporateUserProfile
			//requestHeader = new Dictionary<string, string>();



			////Define url suffix for this command
			//urlSuffix = "/corporate/user/profile" + "/40.759392/-73.969523";



			////Add Headers
			//requestHeader.Add("Accept", "application/json");
			//requestHeader.Add("Api-Key", "06913B08-5228-4CB2-BD77-E6485C529ACE");
			//requestHeader.Add("Content-Type", "application/json");
			//requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, alephPassword));


			//content = "";
			//statusResult = "";

			//try
			//{
			//	statusResult = ProcessWebRequestEx(url + urlSuffix, content, "application/json", "GET", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out responseHeader);
			//	Console.Write(statusResult);
			//	// getAvailableVehiclesResponse.vehicles = statusResult.JsonDeserialize<List<AvailableVehicle>>();
			//}
			//catch (Exception e)
			//{
			//	statusResult = e.Message;
			//}


			//Console.Write(statusResult);
			//Console.Read();
            #endregion

            #region GetUserPaymentMethodInfo
            //requestHeader = new Dictionary<string, string>();



            ////Define url suffix for this command
            //urlSuffix = "/userpaymentmethodinfo/QA4EY/Release%20EY%20CC";



            ////Add Headers
            //requestHeader.Add("Accept", "application/json");
            //requestHeader.Add("Api-Key", "06913B08-5228-4CB2-BD77-E6485C529ACE");
            //requestHeader.Add("Content-Type", "application/json");
            //requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, alephPassword));


            //content = "";
            //statusResult = "";

            //try
            //{
            //    statusResult = ProcessWebRequestEx(url + urlSuffix, content, "application/json", "GET", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out responseHeader);
            //    Console.Write(statusResult);
            //    // getAvailableVehiclesResponse.vehicles = statusResult.JsonDeserialize<List<AvailableVehicle>>();
            //}
            //catch (Exception e)
            //{
            //    statusResult = e.Message;
            //}


            //Console.Write(statusResult);
            //Console.Read();
            #endregion
            

            #region AddUserPaymentMethodInfo
            requestHeader = new Dictionary<string, string>();



            //Define url suffix for this command
            urlSuffix = "/userpaymentmethodinfo/QA4EY/Release%20EY%20CC/c6d5125a-b4fd-4296-80d3-c35873ed7c40";

            AddCreditCard _newCreditCard = new AddCreditCard();
            _newCreditCard.profile_id = "QA4EY";
            _newCreditCard.account_name = "Release EY CC";
            _newCreditCard.payment_method_nonce = "c6d5125a-b4fd-4296-80d3-c35873ed7c40l";
            _newCreditCard.nick_name = "Personal";
            _newCreditCard.is_primary = true;
            _newCreditCard.insert_ts = new DateTime(2016,8,31);
            //Add Headers
            requestHeader.Add("Accept", "application/json");
            requestHeader.Add("Api-Key", "06913B08-5228-4CB2-BD77-E6485C529ACE");
            requestHeader.Add("Content-Type", "application/json");
            requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, alephPassword));


            content = "";
            content = _newCreditCard.JsonSerialize();
            statusResult = "";

            try
            {
                statusResult = ProcessWebRequestEx(url + urlSuffix, content, "application/json", "POST", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out responseHeader);
                Console.Write(statusResult);
                // getAvailableVehiclesResponse.vehicles = statusResult.JsonDeserialize<List<AvailableVehicle>>();
            }
            catch (Exception e)
            {
                statusResult = e.Message;
            }


            Console.Write(statusResult);
            Console.Read();
            #endregion


            #region EditUserPaymentMethodInfo
            requestHeader = new Dictionary<string, string>();



            //Define url suffix for this command
            urlSuffix = "/userpaymentmethodinfo/QA4EY/Release%20EY%20CC/c6d5125a-b4fd-4296-80d3-c35873ed7c40";

           

            //Add Headers
            requestHeader.Add("Accept", "application/json");
            requestHeader.Add("Api-Key", "06913B08-5228-4CB2-BD77-E6485C529ACE");
            requestHeader.Add("Content-Type", "application/json");
            requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, alephPassword));


            content = "";
            content = _newCreditCard.JsonSerialize();

            try
            {
                statusResult = ProcessWebRequestEx(url + urlSuffix, content, "application/json", "PUT", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out responseHeader);
                Console.Write(statusResult);
                // getAvailableVehiclesResponse.vehicles = statusResult.JsonDeserialize<List<AvailableVehicle>>();
            }
            catch (Exception e)
            {
                statusResult = e.Message;
            }


            Console.Write(statusResult);
            Console.Read();
            #endregion


            #region DeleteUserPaymentMethodInfo
            requestHeader = new Dictionary<string, string>();



            //Define url suffix for this command
            urlSuffix = "/userpaymentmethodinfo/QA4EY/Release%20EY%20CC/9sh69w";



            //Add Headers
            requestHeader.Add("Accept", "application/json");
            requestHeader.Add("Api-Key", "06913B08-5228-4CB2-BD77-E6485C529ACE");
            requestHeader.Add("Content-Type", "application/json");
            requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, alephPassword));


            content = "";
            statusResult = "";

            try
            {
                statusResult = ProcessWebRequestEx(url + urlSuffix, content, "application/json", "DELETE", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out responseHeader);
                Console.Write(statusResult);
                // getAvailableVehiclesResponse.vehicles = statusResult.JsonDeserialize<List<AvailableVehicle>>();
            }
            catch (Exception e)
            {
                statusResult = e.Message;
            }


            Console.Write(statusResult);
            Console.Read();
            #endregion
        }


        //Make calls to REST service
        private static string ProcessWebRequestEx(string url, string content, string contentType, string method, string userName, string password, int timeout, Dictionary<string, string> headers, out string status, out Dictionary<string, string> responseHeader)
		{
			var result = string.Empty;
			status = string.Empty;
			responseHeader = new Dictionary<string, string>();


			try
			{
				ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

				HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
				if (headers != null && headers.Any())
				{
					foreach (var item in headers)
					{
						//If the Header type specified is one of the existing 'restricted headers', just assign the new value. 
						//Otherwise add the new header and value.
						switch (item.Key.ToLower())
						{
							case "accept":
								request.Accept = item.Value;
								break;
							case "connection":
								request.Connection = item.Value;
								break;
							case "content-length":
								request.ContentLength = Convert.ToInt64(item.Value);
								break;
							case "content-type":
								request.ContentType = item.Value;
								break;
							case "date":
								request.Date = Convert.ToDateTime(item.Value);
								break;
							case "expect":
								request.Expect = item.Value;
								break;
							case "host":
								request.Host = item.Value;
								break;
							case "if-modified-since":
								request.IfModifiedSince = Convert.ToDateTime(item.Value);
								break;
							case "referer":
								request.Referer = item.Value;
								break;
							case "transfer-encoding":
								request.TransferEncoding = item.Value;
								break;
							case "user-agent":
								request.UserAgent = item.Value;
								break;
							default:
								request.Headers.Add(item.Key + ":" + item.Value);
								break;
						}
					}
				}

				if (!string.IsNullOrWhiteSpace(userName))
				{
					NetworkCredential networkCredential = new NetworkCredential(userName, password);
					request.Credentials = networkCredential;
				}
				//NetworkCredential networkCredential = new NetworkCredential("DenverYellowCab", "denveryellowcab@12345");
				//request.Credentials = networkCredential;
				request.ProtocolVersion = HttpVersion.Version11;
				request.Method = method;
				request.Timeout = timeout;
				if (request.Accept == null) request.Accept = "*/*";
				request.UserAgent = "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; Trident/6.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; InfoPath.3; .NET4.0C; .NET4.0E)";


				Stream dataStream;
				if (!string.IsNullOrWhiteSpace(content))
				{
					byte[] byteArray = Encoding.UTF8.GetBytes(content);
					//byte[] byteArray = Encoding.ASCII.GetBytes(content);
					request.ContentType = contentType;
					request.ContentLength = byteArray.Length;
					dataStream = request.GetRequestStream();
					dataStream.Write(byteArray, 0, byteArray.Length);
					dataStream.Close();
				}
				//else
				//{
				//	dataStream = request.GetRequestStream();
				//}
				WebResponse response = request.GetResponse();

				if (response.Headers != null && response.Headers.Count > 0)
				{
					for (int i = 0; i < response.Headers.Count; ++i)
					{
						responseHeader.Add(response.Headers.Keys[i], response.Headers[i]);
					}
				}


				status = ((HttpWebResponse)response).StatusDescription;

				dataStream = response.GetResponseStream();
				StreamReader reader = new StreamReader(dataStream);
				result = reader.ReadToEnd();

				reader.Close();
				dataStream.Close();
				response.Close();

			}
			catch (WebException ex)
			{
				using (var reader = new StreamReader(ex.Response.GetResponseStream()))
				{
					result = reader.ReadToEnd();
				}
				throw new Exception(result, ex);
			}

			return result;

		}


		//Encode username and password as base64
		private static string Base64EncodeForBasicAuthentication(string user, string pass)
		{
			var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(user.Trim() + ":" + pass.Trim());
			return System.Convert.ToBase64String(plainTextBytes);
		}

	}
}
