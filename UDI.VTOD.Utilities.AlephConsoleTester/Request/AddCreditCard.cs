﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Utilities.AlephConsoleTester.Request
{
    public class AddCreditCard
    {
        public string profile_id { get; set; }
        public string account_name { get; set; }
        public string payment_method_nonce { get; set; }
        public string nick_name { get; set; }
        public bool is_primary { get; set; }

        public DateTime insert_ts { get; set; }
    }
}
