﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Utilities.AlephConsoleTester.Request
{
    public class AlephVehicle
    {
        public string provider_name { get; set; }
        public string company_name { get; set; }
        public string company_phone_number { get; set; }
        public string driver_first_name { get; set; }

        public string driver_last_name { get; set; }
        public string driver_phone_number { get; set; }
        public string driver_call_number { get; set; }
        public string car_no { get; set; }

        public string year { get; set; }
        public string make { get; set; }
        public string model { get; set; }
        public string color { get; set; }

        public string vehicle_type { get; set; }
        public int max_no_of_passengers { get; set; }
        public int max_no_of_luggage { get; set; }
        public string license_plate_number { get; set; }
    }
}
