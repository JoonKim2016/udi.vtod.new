﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Utilities.AlephConsoleTester.Request
{
    public class ChangeUserPasswordRequest
    {
        public string username { get; set; }
        public string password { get; set; }
        public string new_password { get; set; }
    }
}
