﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Utilities.AlephConsoleTester.Request
{
     public   class RegisterDevice
    {
        public string device_token { get; set; }
        public string platform_name { get; set; }
        public string platform_version { get; set; }
        public string phone_number { get; set; }
        public string push_notification_token { get; set; }
        public bool push_notifications { get; set; }
        public bool sms_notifications { get; set; }
        public bool is_primary_device { get; set; }
    }
}
