﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Utilities.AlephConsoleTester.Request
{
    public class RegisterUserRequest
    {
        public string username { get; set; }
        public string password { get; set; }
    }
}
