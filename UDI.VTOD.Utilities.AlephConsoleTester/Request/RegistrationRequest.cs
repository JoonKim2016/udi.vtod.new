﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Utilities.AlephConsoleTester.Request
{
    public class RegistrationRequest
    {
        public int provider_id { get; set; }
        public string provider_account_id { get; set; }
        public string company_name { get; set; }
        public string profile_id { get; set; }
        public string username { get; set; }
        public string email_address { get; set; }
        public string password { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string device_token { get; set; }
        public string phone_number { get; set; }
        public string platform_name { get; set; }
        public string platform_version { get; set; }
    }
}
