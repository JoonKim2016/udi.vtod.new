﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Utilities.AlephConsoleTester.Request
{
    public class ReservationAddress
    {
        public string street_no { get; set; }
        public string street_name { get; set; }

        public string city { get; set; }
        public string state { get; set; }

        public string zip_code { get; set; }
        public string country { get; set; }

        public string auto_complete { get; set; }
        public string address_type { get; set; }

        public string airport_code { get; set; }
        public string airline { get; set; }

        public string flight_number { get; set; }
        public string pickup_point { get; set; }

        public string airport_pickup_point { get; set; }
        public string city_of_origin { get; set; }

        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
