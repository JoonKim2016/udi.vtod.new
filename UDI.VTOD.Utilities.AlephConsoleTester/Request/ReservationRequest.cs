﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Utilities.AlephConsoleTester.Request
{
    public class ReservationRequest
    {
        public int provider_id { get; set; }
        public string provider_account_id { get; set; }
        public string company_name { get; set; }
        public string profile_id { get; set; }
        public string originating_system_reference_number { get; set; }
        public DateTime requested_datetime { get; set; }         
        public ReservationAddress pickup { get; set; }
        public ReservationAddress dropoff { get; set; }
        public string payment_method { get; set; }
        public string special_instructions { get; set; }
        public List<AlephBookingCriteria> booking_criteria { get; set; }
        public List<Passenger> passengers { get; set; }
        public List<GatewayTrackingNumber> gateway_tracking_numbers { get; set; }
    }
}
