﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Utilities.AlephConsoleTester.Response
{
    public class AvailableVehicle
    {
        public int provider_id { get; set; }
        public string company_name { get; set; }
        public string car_no { get; set; }
        public string vehicle_type { get; set; }
        public decimal latitude { get; set; }
        public decimal longitude { get; set; }
        public decimal ETA { get; set; }//Minutes
        public decimal distance { get; set; }//Miles
        public decimal velocity { get; set; }//MilesPerHour(MPH)
    }
}
