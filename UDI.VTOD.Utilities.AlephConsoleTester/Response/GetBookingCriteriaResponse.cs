﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Utilities.AlephConsoleTester.Request;

namespace UDI.VTOD.Utilities.AlephConsoleTester.Response
{
    public class GetBookingCriteriaResponse
    {
        public List<AlephBookingCriteria> booking_criteria { get; set; } 
    }
}
