﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Utilities.AlephConsoleTester.Response
{
    public class GetProfilesResponse
    {
        public List<UserProfile> profiles { get; set; }
    }
}
