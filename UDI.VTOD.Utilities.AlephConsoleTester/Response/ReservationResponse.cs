﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Utilities.AlephConsoleTester.Request;

namespace UDI.VTOD.Utilities.AlephConsoleTester.Response
{
    public class ReservationResponse
    {
        public int provider_id { get; set; }
        public string provider_account_id { get; set; }
        public string company_name { get; set; }
        public string profile_id { get; set; }
        public string originating_system_reference_number { get; set; }
        public string vendor_confirmation_no { get; set; }
        public DateTime requested_datetime { get; set; }
        public DateTime cancelled_timestamp { get; set; }
        public ReservationAddress pickup { get; set; }
        public ReservationAddress dropoff { get; set; }
        public string payment_method { get; set; }
        public string special_instructions { get; set; }
        public string provider_vehicle_id { get; set; }
        public bool is_asap { get; set; }
        public bool is_dropoff_as_directed { get; set; }
        public string provider_subaccount_id { get; set; }
        public List<AlephBookingCriteria> booking_criteria { get; set; }
        public List<StatusMessage> status_messages { get; set; }
        public List<Passenger> passengers { get; set; }
        public List<GatewayTrackingNumber> gateway_tracking_numbers { get; set; }
        public AlephVehicle assigned_vehicle { get; set; }
    }
}
