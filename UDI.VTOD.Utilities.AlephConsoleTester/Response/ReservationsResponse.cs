﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Utilities.AlephConsoleTester.Response
{
    public class ReservationsResponse
    {
        public List<ReservationResponse> reservations { get; set; }
    }
}
