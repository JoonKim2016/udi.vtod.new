﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Utilities.ConsoleTester
{
	public class rtept
	{
		public string lat { get; set; }
		public string lon { get; set; }
	}

	public class rte
	{
		public string name { get; set; }
		public string desc { get; set; }
		public string code { get; set; }
		public string shortName { get; set; }
		public string country { get; set; }
		public string state { get; set; }
		public string city { get; set; }
		public List<rtept> rtept { get; set; }
	}

	public class gpx
	{
		public List<rte> rte { get; set; }
	}

	public class airport
	{
		public gpx gpx { get; set; }
	}
}
