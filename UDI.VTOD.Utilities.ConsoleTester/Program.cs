﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using System.IO;
using System.Drawing;
using System.Data;
using UDI.Map.DTO;
using System.Resources;
using System.Reflection;
using UDI.VTOD.Utilities.ConsoleTester.Properties;
using Math = System.Math;
using System.Net;

//using AutoMapper;
namespace UDI.VTOD.Utilities.ConsoleTester
{
	class Program
	{
		//todo move this to a image class in an app layer
		public static Image ScaleImage(Image image, int maxWidth, int maxHeight)
		{
			var ratioX = (double)maxWidth / image.Width;
			var ratioY = (double)maxHeight / image.Height;
			var ratio = Math.Min(ratioX, ratioY);

			var newWidth = (int)(image.Width * ratio);
			var newHeight = (int)(image.Height * ratio);

			var newImage = new Bitmap(newWidth, newHeight);
			Graphics.FromImage(newImage).DrawImage(image, 0, 0, newWidth, newHeight);
			return newImage;
		}


        public List<Coordinate> DecodePolylinePoints(string encodedPoints)
        {
            if (encodedPoints == null || encodedPoints == "") return null;
            List<Coordinate> poly = new List<Coordinate>();
            char[] polylinechars = encodedPoints.ToCharArray();
            int index = 0;

            int currentLat = 0;
            int currentLng = 0;
            int next5bits;
            int sum;
            int shifter;
            int k = 0;
            try
            {
                while (index < polylinechars.Length)
                {
                   
                    // calculate next latitude
                    sum = 0;
                    shifter = 0;
                    do
                    {
                        next5bits = (int)polylinechars[index++] - 63;
                        sum |= (next5bits & 31) << shifter;
                        shifter += 5;
                    } while (next5bits >= 32 && index < polylinechars.Length);

                    if (index >= polylinechars.Length)
                        break;

                    currentLat += (sum & 1) == 1 ? ~(sum >> 1) : (sum >> 1);

                    //calculate next longitude
                    sum = 0;
                    shifter = 0;
                    do
                    {
                        next5bits = (int)polylinechars[index++] - 63;
                        sum |= (next5bits & 31) << shifter;
                        shifter += 5;
                    } while (next5bits >= 32 && index < polylinechars.Length);

                    if (index >= polylinechars.Length && next5bits >= 32)
                        break;

                    currentLng += (sum & 1) == 1 ? ~(sum >> 1) : (sum >> 1);
                    Coordinate p = new Coordinate();
                    p.Latitude = Convert.ToDouble(currentLat) / 100000.0;
                    p.Longitude = Convert.ToDouble(currentLng) / 100000.0;
                    if (k > 0)
                        p.Angle = AngleBetweenThePoints(poly[k - 1].Latitude, poly[k - 1].Longitude, p.Latitude, p.Longitude);
                        //p.Angle = AngleBetweenThePoints(34.135129735217596, -118.1345148012042, 34.127497490212626, -118.14012229442596);
                    else
                        p.Angle = 0;
                    poly.Add(p);
                    k = k + 1;
                }
            }
            catch (Exception ex)
            {
                // logo it
            }
            return poly;
        }

        public double AngleBetweenThePoints(double startLat, double startLon, double endLat, double endLon)
        {
            double fromLat = ConvertToRadians(startLat);
            double fromLng = ConvertToRadians(startLon);
            double toLat = ConvertToRadians(endLat);
            double toLng = ConvertToRadians(endLon);
            double dLng = toLng - fromLng;
            double heading = Math.Atan2(
                  Math.Sin(dLng) * Math.Cos(toLat),
                  Math.Cos(fromLat) * Math.Sin(toLat) - Math.Sin(fromLat) * Math.Cos(toLat) * Math.Cos(dLng));
            return wrap(RadianToDegree(heading), -180, 180);
        }
         public  double wrap(double n, double min, double max)
        {
            return (n >= min && n < max) ? n : ((n - min)%(max - min) + min);
        }
        private double RadianToDegree(double angle)
        {
            return angle * (180.0 / Math.PI);
        }
        public double ConvertToRadians(double angle)
        {
            return (Math.PI / 180) * angle;
        }
		static void Main(string[] args)
		{
           // var str = "{\"Success\": {},\"TPA_Extensions\": {\"Vehicles\": {\"Items\": [{\"Course\": 339.23522822552,\"DriverID\":\"\",\"DriverName\":\"\",\"FleetID\":\"5\",\"VehicleNo\": \"7\", \"VehicleStatus\":\"AVAILABLE\", \"VehicleType\": \"\"}],\"NearestVehicleETA\": \"3\",\"NumberOfVehicles\": \"20\"}}}";
           // AvailRS1 response = str.JsonDeserialize<AvailRS1>();
           // #region commented
           // //#region "Adding Directions in Path"
           // //var googleKey = System.Configuration.ConfigurationManager.AppSettings["Google_Key"];
           // //var googleClient = System.Configuration.ConfigurationManager.AppSettings["Google_Client"];
           // //string apiUrl = "https://maps.googleapis.com/maps/api/directions/json?origin=34.135129735217596,-118.1345148012042&destination=34.127497490212626,-118.14012229442596&client=" + googleClient;
           // //var signedURL = UDI.Map.Adapter.Google.Helper.Helper.Sign(apiUrl, googleKey);
           // //var status = string.Empty;
           // //Program p1 = new Program();
           // //var googleJsonResult = UDI.Utility.Helper.WebRequestHelper.ProcessWebRequest(signedURL, "", "application/json; charset=utf-8", "GET", out status);
           // //var googleResult = googleJsonResult.JsonDeserialize<RootObject>();
           // //List<Coordinate> lst = p1.DecodePolylinePoints(googleResult.routes.FirstOrDefault().overview_polyline.points);
           // //#endregion
           // //#region Regex Test
           // ////Regex regex = new Regex(@"^(\w+)[\s](.*)", RegexOptions.IgnoreCase);
           // //    Regex regex = new Regex(@"^([0-9-–]+[\s]{0,}[0-9\/]{0,})[\s](.*)", RegexOptions.IgnoreCase);
           // //    var matches = regex.Matches("16 1/2 Marengo Ave".Trim());
           // //    //if (matches.Count > 0)
           // //    //{
           // //    if (matches.Count > 0 && matches[0].Groups.Count == 3)
           // //    {
           // //        //if (matches[0].Groups[1].Value.IsNumeric())
           // //        //{
           // //        var streetNumber = matches[0].Groups[1].Value;
           // //        var street = matches[0].Groups[2].Value;
           // //        //}
           // //    }
           // //    else
           // //    {
           // //        //street = addr.Address.AddressLine.Trim();
           // //    }
           // //#endregion

           // //#region Math
           // //int result = (int)System.Math.Ceiling(102M / 10) * 10;


           // //#endregion

           // //#region Bob Preston's web service example
           // //using (var client = new TaxiDrvService.TaxiDrvServiceSoapClient())
           // //{
           // //    #region Driver Info #1
           // //    //var drv = client.driver_info_voucher("17", "130323");
           // //    //var drv2 = client.driver_info_voucher("10", "6425534");
           // //    //var obj = XDocument.Parse(drv).XmlDeserialize<driver_info>();
           // //    //if (obj == null) obj.error = drv;
           // //    //var drv2 = client.driver_info_voucher("10", "5827626");
           // //    //var obj2 = XDocument.Parse(drv2).XmlDeserialize<driver_info>();
           // //    //if (obj2 == null) obj2 = new driver_info { error = drv2 }; 
           // //    #endregion

           // //    #region Drive Info #2
           // //    var drvDataSet = client.get_driver_info("17", "", "680");
           // //    //var drvDataSet1 = client.get_driver_info("17", "", "624");
           // //    //var drvDataSet2 = client.get_driver_info("10", "", "3509");

           // //    var drvDataTable = drvDataSet.Tables[0];
           // //    foreach (DataRow row in drvDataTable.Rows)
           // //    {
           // //        string driverID = row["DriverID"].ToString();
           // //        string VehicleID = row["VehicleID"].ToString();
           // //        string FirstName = row["FirstName"].ToString();
           // //        string LastName = row["LastName"].ToString();
           // //        string CellPhone = row["CellPhone"].ToString();
           // //        string HomePhone = row["HomePhone"].ToString();
           // //        string AlternatePhone = row["AlternatePhone"].ToString();
           // //        DateTime? QualificationDate = row["QualificationDate"].ToDateTime();
           // //        bool zTripQualified = row["zTripQualified"].ToBool();
           // //        DateTime? StartDate = row["StartDate"].ToDateTime();
           // //        string HrDays = row["HrDays"].ToString();
           // //        DateTime? LeaseDate = row["LeaseDate"].ToDateTime();

           // //        byte[] imageBytes = null;
           // //        if (row["Image"] != null && row["Image"] != System.DBNull.Value)
           // //        {
           // //            imageBytes = (byte[])row["Image"];
           // //            File.WriteAllBytes("Foo2.png", imageBytes);
           // //            var ms = new MemoryStream(imageBytes);
           // //            var returnImage = Image.FromStream(ms);
           // //            ScaleImage(returnImage, 100, 100).Save("foo_small2.png");
           // //        }
           // //    }

           // //    #endregion
           // //}
           // //#endregion

           // //#region Google ETA
           // ////// Note: Generally, you should store your private key someplace safe
           // ////// and read them into your code

           // ////const string keyString = "YOUR_PRIVATE_KEY";

           // ////// The URL shown in these examples is a static URL which should already
           // ////// be URL-encoded. In practice, you will likely have code
           // ////// which assembles your URL from user or web service input
           // ////// and plugs those values into its parameters.
           // ////const string urlString = "YOUR_URL_TO_SIGN";

           // ////string inputUrl = null;
           // ////string inputKey = null;

           // ////Console.WriteLine("Enter the URL (must be URL-encoded) to sign: ");
           // ////inputUrl = Console.ReadLine();
           // ////if (inputUrl.Length == 0)
           // ////{
           // ////	inputUrl = urlString;
           // ////}

           // ////Console.WriteLine("Enter the Private key to sign the URL: ");
           // ////inputKey = Console.ReadLine();
           // ////if (inputKey.Length == 0)
           // ////{
           // ////	inputKey = keyString;
           // ////}

           // ////Console.WriteLine(SignUrl.GoogleSignedUrl.Sign(inputUrl, inputKey));

           // //#endregion

           // //#region Google Map Example for gettting ETA
           // //////var baseURL = 
           // ////var originLatitude = "34.137020";
           // ////var originLongitude = "-118.237228";
           // ////var destinationLatitude = "34.146126";
           // ////var destinationLongitude = "-118.145980";
           // //////var departureTime = 1412016814;
           // //////var xxxx = System.DateTime.UtcNow.Second;

           // ////int departureTime = (int)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds;


           // ////var URL = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" + originLatitude + "," + originLongitude + "&destinations=" + destinationLatitude + "," + destinationLongitude + "&mode=driving&departure_time=" + departureTime + "&language=US&client=gme-supershuttleinternational";

           // ////var signedURL = SignUrl.GoogleSignedUrl.Sign(URL, "uaRX-lr193ytVx-QpR09ry7ydnE=");
           // ////var error = string.Empty;
           // ////var map = new UDI.Map.MapService(Map.DTO.MapApiProvider.Google);
           // ////var result = map.GetETA(new Map.DTO.Geolocation { Latitude = 34.137020M, Longitude = -118.237228M },
           // ////	new Map.DTO.Geolocation { Latitude = 34.146126M, Longitude = -118.145980M }, DistanceUnit.Mile, out error);
           // //#endregion

           // //#region Getting Driver from BLOB
           // ////long driverID = 1487;

           // ////using (var db = new DataAccess.VTOD.VTODEntities())
           // ////{
           // ////	var driverDAO = db.vtod_driver_info.FirstOrDefault(s => s.Id == driverID);

           // ////	if (driverDAO != null && driverDAO.Image != null)
           // ////	{
           // ////		using (var ms = new MemoryStream(driverDAO.Image))
           // ////		{
           // ////			Image image = Image.FromStream(ms);
           // ////			image.Save(string.Format(@"D:\{0}.png", driverDAO.DriverID), System.Drawing.Imaging.ImageFormat.Png);
           // ////		}
           // ////	}


           // ////}
           // //#endregion

           // //#region Update Airport State, City, Country
           // ////WCFSecurityService.SecurityToken token;
           // ////UtilityService.AirportRecords serviceAirports;

           // ////using (var clinet = new WCFSecurityService.WCFSecurityServiceClient())
           // ////{
           // ////	token = clinet.Login(1, "10.40.115.73", "zTrip", "n3uqPBpKY5QF3qK");
           // ////}
           // ////using (var client = new UtilityService.UtilityServiceClient())
           // ////{
           // ////	var utilityToken = new UtilityService.SecurityToken { ClientIPAddress = token.ClientIPAddress, ClientType = token.ClientType, SecurityKey = token.SecurityKey, UserName = token.UserName };

           // ////	serviceAirports = client.GetWebServicedAirports(utilityToken);
           // ////}

           // ////using (var db = new DataAccess.VTOD.VTODEntities())
           // ////{
           // ////	foreach (var item in serviceAirports.AirportRecordsArray)
           // ////	{
           // ////		//Console.WriteLine("AirportCode:" + item.AirportCode);
           // ////		//Console.WriteLine("AirportName:" + item.AirportName);
           // ////		//Console.WriteLine("******************");
           // ////		//Console.Read();

           // ////		var dao = db.vtod_polygon.Where(s => s.Name.ToLower().Trim() == item.AirportCode.ToLower().Trim()).FirstOrDefault();
           // ////		if (dao != null)
           // ////		{
           // ////			dao.Details = item.AirportName;
           // ////			dao.Country = item.CountryCode;
           // ////			dao.State = item.State;
           // ////			dao.City = item.City;

           // ////		}
           // ////	}
           // ////	db.SaveChanges();
           // ////}
           // //#endregion

           // //#region Check lacking airport in Vtod DB compared to SDS.GetServicedAirports method
           // ////WCFSecurityService.SecurityToken token;
           // ////UtilityService.AirportRecords serviceAirports;

           // ////using (var clinet = new WCFSecurityService.WCFSecurityServiceClient())
           // ////{
           // ////	token = clinet.Login(1, "10.40.115.73", "zTrip", "n3uqPBpKY5QF3qK");
           // ////}
           // ////using (var client = new UtilityService.UtilityServiceClient())
           // ////{
           // ////	var utilityToken = new UtilityService.SecurityToken { ClientIPAddress = token.ClientIPAddress, ClientType = token.ClientType, SecurityKey = token.SecurityKey, UserName = token.UserName };

           // ////	serviceAirports = client.GetWebServicedAirports(utilityToken);
           // ////}

           // ////string newAirports = string.Empty;

           // ////using (var db = new DataAccess.VTOD.VTODEntities())
           // ////{
           // ////	foreach (var item in serviceAirports.AirportRecordsArray)
           // ////	{
           // ////		//		//Console.WriteLine("AirportCode:" + item.AirportCode);
           // ////		//		//Console.WriteLine("AirportName:" + item.AirportName);
           // ////		//		//Console.WriteLine("******************");
           // ////		//		//Console.Read();

           // ////		var dao = db.vtod_polygon.Where(s => s.Name.ToLower().Trim() == item.AirportCode.ToLower().Trim()).FirstOrDefault();
           // ////		if (dao == null && item.PortType == 0 && item.Serviced == 1)
           // ////		{
           // ////			newAirports += item.AirportCode + ", ";
           // ////			//			dao.Details = item.AirportName;
           // ////			//			dao.Country = item.CountryCode;
           // ////			//			dao.State = item.State;
           // ////			//			dao.City = item.City;

           // ////		}
           // ////	}
           // ////	//	db.SaveChanges();

           // ////	File.WriteAllText(@"C:\newAirports.txt", newAirports);
           // ////}
           // //#endregion

           // //#region Add new element to the "AirportName.json"
           // //var fileText = File.ReadAllText(@"D:\UDI\Sync\Git\udi.vtod\UDI.VTOD.Utilities.ConsoleTester\AirportName.03.json");
           // //var airports = fileText.JsonDeserialize<airport>();

           // //using (var db = new UDI.VTOD.DataAccess.VTOD.VTODEntities())
           // //{
           // //    foreach (var item in airports.gpx.rte)
           // //    {
           // //        var itemCode = item.name.Substring(0, 3);
           // //        if (itemCode == itemCode.ToUpper())
           // //        {
           // //            var dao = db.vtod_polygon.Where(s => s.Name.ToLower().Trim() == itemCode.ToLower().Trim()).FirstOrDefault();
           // //            if (dao != null)
           // //            {
           // //                item.country = dao.Country;
           // //                item.state = dao.State;
           // //                item.city = dao.City;
           // //                item.shortName = dao.Details;
           // //                item.code = dao.Name;
           // //                //			dao.Details = item.AirportName;
           // //                //			dao.Country = item.CountryCode;
           // //                //			dao.State = item.State;
           // //                //			dao.City = item.City;

           // //            }
           // //        }
           // //        else
           // //        {
           // //            Console.WriteLine("AirportCode:" + itemCode);
           // //        }
           // //    }
           // //}
           // //var newFileText = airports.JsonSerialize<airport>();
           // //File.WriteAllText(@"C:\AirportName.json", newFileText);
           // //#endregion
            
           // #endregion
           //// Mapper.CreateMap<AvailRS1, AvailRS>().ForMember(dto => dto.TPA_Extensions1, opt => opt.ResolveUsing(s => s.TPA_Extensions));
           // Mapper.CreateMap<AvailRS1, AvailRS>();
           // Mapper.CreateMap<TPA_Extensions1,TPA_Extensions>();
           // var responseItem = Mapper.Map<AvailRS1, AvailRS>(response);
        }
		

	}
    public class AvailRS1
    {
        //public string EchoToken { get; set; }
        //public string Target { get; set; }
        //public string Version { get; set; }
        //public string PrimaryLangID { get; set; }
        //public Success Success { get; set; }
        //public GroundServiceList GroundServices { get; set; }
        //public List<RateQualifier> RateQualifiers { get; set; }
        //public TPA_Extensions1 TPA_Extensions1 { get; set; }
        //public ErrorList Errors { get; set; }
    }
    public class AvailRS2 
    {
        //public string EchoToken { get; set; }
        //public string Target { get; set; }
        //public string Version { get; set; }
        //public string PrimaryLangID { get; set; }
        //public Success Success { get; set; }
        //public GroundServiceList GroundServices { get; set; }
        //public List<RateQualifier> RateQualifiers { get; set; }
        //public TPA_Extensions TPA_Extensions1 { get; set; }
        //public ErrorList Errors { get; set; }
    }
    //public class TPA_Extensions
    //{
    //    public Vehicles Vehicles { get; set; }
    //}
    //public class TPA_Extensions1
    //{
       
    //    public Vehicles1 Vehicles1 { get; set; }
    //}
    //public class Vehicles
    //{

    //    public List<Items> Items { get; set; }
    //}
    //public class Vehicles1
    //{

    //    public List<Items> Items { get; set; }
    //}
    //public class Items
    //{
    //    public decimal Course { get; set; }
    //    public string DriverID { get; set; }
    //    public string DriverName { get; set; }
    //    public string FleetID { get; set; }
    //    public string VehicleNo { get; set; }
    //    public string VehicleStatus { get; set; }
    //    public string VehicleType { get; set; }
    //}
    //public class DriverInfo
    //{
    //    public string DriverID { get; set; }
    //    public string DriverName { get; set; }
    //}
    public class Coordinate
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double Angle { get; set; }
    }
}
