﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UDI.VTOD.Utilities.ConsoleTester
{
	//[XmlElement()]
	public class driver_info
	{
		public string id { get; set; }
		public string name { get; set; }
		public string error { get; set; }
	}
}
