﻿using System;
using log4net;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Domain.Taxi;
using UDI.VTOD.Domain.Taxi.MTData;
using UDI.VTOD.Utilities.MTDataConsoleTester.MTData.AddressWebService;
using UDI.VTOD.Utilities.MTDataConsoleTester.MTData.BookingWebService;
using Address = UDI.VTOD.Utilities.MTDataConsoleTester.MTData.AddressWebService.Address;
using AddressType = UDI.VTOD.Utilities.MTDataConsoleTester.MTData.BookingWebService.AddressType;
using AuthenticationWebServiceSoapClient = UDI.VTOD.Utilities.MTDataConsoleTester.MTData.AuthenticationWebService.AuthenticationWebServiceSoapClient;
using BookingChannelType = UDI.VTOD.MTData.AuthenticationWebService.BookingChannelType;
using Designation = UDI.VTOD.Utilities.MTDataConsoleTester.MTData.BookingWebService.Designation;
using Fleet = UDI.VTOD.Utilities.MTDataConsoleTester.MTData.BookingWebService.Fleet;
using GEOLocation = UDI.VTOD.Utilities.MTDataConsoleTester.MTData.AddressWebService.GEOLocation;
using Street = UDI.VTOD.Utilities.MTDataConsoleTester.MTData.BookingWebService.Street;
using Suburb = UDI.VTOD.Utilities.MTDataConsoleTester.MTData.BookingWebService.Suburb;

namespace UDI.VTOD.Utilities.MTDataConsoleTester
{
	class Program
	{
		static void Main(string[] args)
		{
			#region Authentication

			const string username = "zTripDev";
			const string password = "Devel0pingzTr1p";
			string token;

			#region Test
			//using (var client = new AuthenticationWebServiceSoapClient())
			//{
			//	var authResult = client.Authenticate(BookingChannelType.GenesisIvr, username, password);

			//	if (authResult.Succeeded)
			//	{
			//		token = authResult.Token;
			//	}
			//	else
			//	{
			//		Console.WriteLine(authResult.ErrorMessage);
			//		Console.ReadKey();
			//		return;
			//	}
			//}
			#endregion

			#region Prod
			using (var clinet = new MTData.AuthenticationProductionWebService.AuthenticationWebServiceSoapClient())
			{
				var authResult = clinet.Authenticate(BookingChannelType.GenesisIvr, username, password);

				if (authResult.Succeeded)
				{
					token = authResult.Token;
				}
				else
				{
					Console.WriteLine(authResult.ErrorMessage);
					Console.ReadKey();
					return;
				}
			}

			#endregion


			#endregion

			#region GetFleetVacanVehiclesForBookingLocation

			//using (var client = new BookingWebServiceSoapClient())
			//{
			//	var location = new BookingLocationInfo
			//	{
			//		FleetId = 2,
			//		Latitude = 39.8121008,
			//		Longitude = -105.0661883,
			//		Radius = 100
			//	};

			//	var result = client.GetFleetVacantVehiclesForBookingLocation(token, location);
			//}

			#endregion

			#region ConvertLatLongToAddress

			//Address pickupAddress = null;
			//Address dropoffAddress = null;

			//using (var client = new AddressWebServiceSoapClient())
			//{
			//	var setting = new ConvertLatLongToAddressSettings
			//	{
			//		FleetID = 2,
			//		Latitude = 39.8121008,
			//		Longitude = -105.0661883
			//	};

			//	var result = client.ConvertLatLongToAddress(token, setting);
			//	if (result.Succeeded)
			//	{
			//		Console.WriteLine("{0} {1} {2}, {3}, {4}", result.Address.Number, result.Address.Street.Name,
			//			result.Address.Designation.Name, result.Address.Suburb.Name, result.Address.Street.Postcode);

			//		pickupAddress = result.Address;
			//	}

			//	// 39.8758344,-104.9582562
			//	setting = new ConvertLatLongToAddressSettings
			//	{
			//		FleetID = 2,
			//		Latitude = 39.8758344,
			//		Longitude = -104.9582562
			//	};
			//	result = client.ConvertLatLongToAddress(token, setting);
			//	if (result.Succeeded)
			//	{
			//		dropoffAddress = result.Address;
			//	}
			//}

			#endregion

			#region GetQuickLocationFleet

			//using (var client = new BookingWebServiceSoapClient())
			//{
			//	var result = client.GetQuickLocationFleet(token, 2);
			//}

			#endregion

			#region ImportBooking

			//if (pickupAddress != null)
			//{
			//    using (var client = new BookingWebServiceSoapClient())
			//    {
			//        var booking = new ImportBookingSettings { Booking = new Booking { Locations = new Location[2] } };

			//        booking.Booking.ContactName = "Tester Test";
			//        booking.Booking.ContactPhoneNumber = "6266911111";
			//        booking.Booking.Fleet = new Fleet
			//        {
			//            ID = 2
			//        };

			//        booking.Booking.Locations[0] = new Location
			//        {
			//            LocationType = LocationType.PickUp,
			//            AddressType = AddressType.Street,
			//            Address = new BookingWebService.Address
			//            {
			//                Number = pickupAddress.Number,
			//                Street = new BookingWebService.Street
			//                {
			//                    ID = pickupAddress.Street.ID,
			//                    Postcode = pickupAddress.Street.Postcode
			//                },
			//                Designation = new BookingWebService.Designation
			//                {
			//                    ID = pickupAddress.Designation.ID,
			//                },
			//                Suburb = new BookingWebService.Suburb
			//                {
			//                    ID = pickupAddress.Suburb.ID
			//                }
			//            },
			//            TimeMode = TimeMode.SpecificTime,
			//            Time = DateTime.Now.AddHours(3),
			//            CustomerName = "Tester Test",
			//            CustomerEmail = "Tester@uu.com",
			//            CustomerPhoneNumber = "6266911111"
			//        };

			//        booking.Booking.Conditions = new BookingCondition[1];
			//        booking.Booking.Conditions[0] = new BookingCondition
			//        {
			//            ID = 1
			//        };

			//        booking.Booking.PaymentMethod = new PaymentMethod
			//        {
			//            ID = 1
			//        };

			//        booking.Booking.Locations[1] = new Location
			//        {
			//            LocationType = LocationType.DropOff,
			//            AddressType = AddressType.None
			//        };

			//        booking.RemoteSystemID = "zTrip";
			//        //booking.RemoteReference = "1";

			//        try
			//        {
			//            var result = client.ImportBooking(token, booking);
			//        }
			//        catch (Exception ex)
			//        {
			//            throw;
			//        }
			//    }
			//}

			#endregion

			#region GetChargeEstimate

			//if (pickupAddress != null && dropoffAddress != null)
			//{
			//	using (var client = new BookingWebServiceSoapClient())
			//	{
			//		var request = new ChargeEstimateSettings()
			//		{
			//			Fleet = new Fleet { ID = 2 },
			//			Time = DateTime.Now.AddHours(3),
			//			PickUpAddressType = AddressType.Street,
			//			PickUpAddress = new MTData.BookingWebService.Address
			//			{
			//				Number = pickupAddress.Number,
			//				Street = new Street
			//				{
			//					ID = pickupAddress.Street.ID,
			//					Postcode = pickupAddress.Street.Postcode
			//				},
			//				Designation = new Designation
			//				{
			//					ID = pickupAddress.Designation.ID,
			//				},
			//				Suburb = new Suburb
			//				{
			//					ID = pickupAddress.Suburb.ID
			//				}
			//			},
			//			DropOffAddressType = AddressType.Street,
			//			DropOffAddress = new MTData.BookingWebService.Address
			//			{
			//				Number = dropoffAddress.Number,
			//				Street = new Street
			//				{
			//					ID = dropoffAddress.Street.ID,
			//					Postcode = dropoffAddress.Street.Postcode
			//				},
			//				Designation = new Designation
			//				{
			//					ID = dropoffAddress.Designation.ID,
			//				},
			//				Suburb = new Suburb
			//				{
			//					ID = dropoffAddress.Suburb.ID
			//				}
			//			}
			//		};

			//		var result = client.GetChargeEstimate(token, request);
			//	}
			//}

			#endregion

			#region GetBooking

			//using (var client = new BookingWebServiceSoapClient())
			//{
			//	var result = client.GetBooking(token, 4449798);
			//}

			#endregion

			#region CancelBooking

			//using (var client = new BookingWebServiceSoapClient())
			//{
			//	//var result = client.CancelBooking(token, 4449798);
			//}

			#endregion

			#region MapAddresses

			//using (var client = new AddressWebServiceSoapClient())
			//{
			//	var request = new MappedAddressRequest()
			//	{
			//		FleetID = 2,
			//		Addresses = new[]
			//		{
			//			new TextAddress
			//			{
			//				Number = "750",
			//				StreetName = "Colorado Blvd",
			//				Locality1 = "Denver",
			//				AdminLevel1 = "CO",
			//				PostCode = "80206",
			//				GEOLocation = new GEOLocation
			//				{
			//					Latitude = 39.7283253,
			//					Longitude = -104.9405976
			//				}
			//			}
			//		}
			//	};

			//	var result = client.MapAddresses(token, request);
			//}

			#endregion

			#region SendBookingDriverMessage

			//var tokenRS = new TokenRS()
			//{
			//	ClientIPAddress = "10.40.115.73",
			//	ClientType = 1,
			//	SecurityKey = "bfa26e2f-4519-4863-a229-27fd7d49c882"
			//};

			//var requestNotifyDriver = new UtilityNotifyDriverRQ
			//{
			//	TripID = 61672,
			//	Message = "Test message"
			//};

			//try
			//{
			//	var logger = LogManager.GetLogger(LogOwnerType.Taxi.ToString());

			//	var result = new MTDataTaxiService(new TaxiUtility(logger), logger).NotifyDriver(tokenRS,
			//		requestNotifyDriver, null);
			//}
			//catch (Exception ex)
			//{
			//	Console.WriteLine(ex.ToString());
			//}

			#endregion

			#region SendDriverMessageForCity
			using (var client = new ProductionBookingWebService.BookingWebServiceSoapClient())
			{

				var result = client.SendDriverMessage1(token, 7, "We are testing. If you get this please call me back at 310-926-1187. Thanks", 1);
			}
			#endregion

			Console.ReadKey();
		}
	}
}
