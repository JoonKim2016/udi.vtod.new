﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using PhotoUploadv1;
using UDI.VTOD.Common.Helper;
using System.IO;

namespace UDI.VTOD.Utilities.PhotoUploader
{

    public class Driver : vtod_driver_info
    {
        public long FleetId { get; set; }

    }
    public class PhotoUpload
    {
        private ILog logger;
        //Here is the once-per-class call to initialize the log object

        public PhotoUpload()
        {
            logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            logger.Info("Hello");
        }

        public void UpdateProdDriverPhotos()
        {

            Console.WriteLine("UpdateProdDriverPhotos");

            Console.WriteLine("Should pull about 1k. Skip should get to 90");
 
            int skip = 10;

            for (int i = 0; i < 2010; i += skip)
            {

                Console.WriteLine("In Loop..............");

                var drivers = GetPhotos(i, skip);
                
                if(!drivers.Any())
                    break;
                
                Console.WriteLine("skip" + skip);

                var idsAdded = "," + string.Join(",", drivers.Select(b => b.Id).ToArray());
               
                Console.WriteLine(idsAdded);
                
                File.AppendAllText(@"updatedthese.txt",idsAdded);

                Console.WriteLine("Wrote IDs to text file");

                //iterate through photos
                foreach (var driver in drivers)
                {
                    //upload to CDN
                    UploadPhotos(driver);
                    //update the driver info table url
                    UpdatePhotoUrl(driver);
                }

                Console.WriteLine("Short rest so I don't tax the db");

                Thread.Sleep(5000);
            }

            //test log for net write each url 
            //null out all photo_urls

        }

        private void UpdatePhotoUrl(Driver driver)
        {
            #region set the driver image url
            var images = new Images(logger);
            using (var db = new vtodEntities())
            {
                var vtodDriver = db.vtod_driver_info.FirstOrDefault(s => s.Id == driver.Id);
                if (vtodDriver != null)
                {
                    //get driver fleet for this
                    vtodDriver.PhotoUrl = images.GetPhotoUrl(driver.DriverID, driver.FleetId.ToString());
                }

                db.SaveChanges();
            }

            #endregion
        }

        private void UploadPhotos(Driver driver)
        {
            var images = new Images(logger);

            #region Add/Update image if the image has changed

            var thread =
                new Thread(
                    () =>
                        images.UpdateDriverImageSizesFromConfig(
                            Image.FromStream(new MemoryStream(driver.Image)), driver.DriverID,
                          driver.FleetId.ToString()));
            thread.Start();

            #endregion


        }

        private IEnumerable<Driver> GetPhotos(int skip, int take)
        {
            var drivers = new List<Driver>();


            using (var db = new vtodEntities())
            {
                var vtodDrivers =
                    db.vtod_trip.Join(db.taxi_trip, vtodTrip => vtodTrip.Id, taxiTrip => taxiTrip.Id,
                        (vtodTrip, taxiTrip) => new { vtodTrip, taxiTrip })
                        .Join(db.vtod_driver_info, @t => @t.vtodTrip.DriverInfoID, driver => driver.Id,
                            (@t, driver) => new { @t, driver })
                        .Where(@t => @t.driver.Image != null)
                        .Select(@t => new { Driver = @t.driver, TaxiTripFleetId = @t.@t.taxiTrip.FleetId }).Distinct().OrderBy(b => b.Driver.Id).Skip(skip).Take(take);


                drivers.AddRange(vtodDrivers.Select(vtodDriverInfo => new Driver { DriverID = vtodDriverInfo.Driver.DriverID, Image = vtodDriverInfo.Driver.Image, FleetId = vtodDriverInfo.TaxiTripFleetId, Id = vtodDriverInfo.Driver.Id }));
            }


            return drivers;
        }
    }
}






