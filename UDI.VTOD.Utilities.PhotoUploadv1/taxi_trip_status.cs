//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PhotoUploadv1
{
    using System;
    using System.Collections.Generic;
    
    public partial class taxi_trip_status
    {
        public long Id { get; set; }
        public long TripID { get; set; }
        public string Status { get; set; }
        public Nullable<System.DateTime> StatusTime { get; set; }
        public string VehicleNumber { get; set; }
        public Nullable<decimal> VehicleLongitude { get; set; }
        public Nullable<decimal> VehicleLatitude { get; set; }
        public string DriverName { get; set; }
        public string DriverId { get; set; }
        public Nullable<int> ETA { get; set; }
        public Nullable<int> ETAWithTraffic { get; set; }
        public Nullable<decimal> Fare { get; set; }
        public Nullable<decimal> DispatchFare { get; set; }
        public string CommentForStatus { get; set; }
        public string OriginalStatus { get; set; }
        public System.DateTime AppendTime { get; set; }
    
        public virtual taxi_trip taxi_trip { get; set; }
    }
}
