﻿namespace UDI.VTOD.Utilities.UI
{
	partial class CompressorForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.txtDeCompressed = new System.Windows.Forms.TextBox();
			this.btnCompress = new System.Windows.Forms.Button();
			this.btnDeCompress = new System.Windows.Forms.Button();
			this.txtCompressed = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// txtDeCompressed
			// 
            this.txtDeCompressed.Location = new System.Drawing.Point(12, 12);
            this.txtDeCompressed.Multiline = true;
            this.txtDeCompressed.Name = "txtDeCompressed";
            this.txtDeCompressed.Size = new System.Drawing.Size(498, 597);
            this.txtDeCompressed.TabIndex = 0;
			// 
			// btnCompress
			// 
			this.btnCompress.Location = new System.Drawing.Point(516, 65);
			this.btnCompress.Name = "btnCompress";
			this.btnCompress.Size = new System.Drawing.Size(125, 217);
			this.btnCompress.TabIndex = 1;
			this.btnCompress.Text = "Compress >>";
			this.btnCompress.UseVisualStyleBackColor = true;
			this.btnCompress.Click += new System.EventHandler(this.btnCompress_Click);
			// 
			// btnDeCompress
			// 
			this.btnDeCompress.Location = new System.Drawing.Point(516, 364);
			this.btnDeCompress.Name = "btnDeCompress";
			this.btnDeCompress.Size = new System.Drawing.Size(125, 193);
			this.btnDeCompress.TabIndex = 2;
			this.btnDeCompress.Text = "<< Decompress";
			this.btnDeCompress.UseVisualStyleBackColor = true;
			this.btnDeCompress.Click += new System.EventHandler(this.btnDeCompress_Click);
			// 
			// txtCompressed
			// 
			this.txtCompressed.Location = new System.Drawing.Point(647, 12);
			this.txtCompressed.Multiline = true;
			this.txtCompressed.Name = "txtCompressed";
			this.txtCompressed.Size = new System.Drawing.Size(498, 597);
			this.txtCompressed.TabIndex = 3;
			// 
			// CompressorForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1158, 635);
			this.Controls.Add(this.txtCompressed);
			this.Controls.Add(this.btnDeCompress);
			this.Controls.Add(this.btnCompress);
            this.Controls.Add(this.txtDeCompressed);
			this.Name = "CompressorForm";
			this.Text = "CompressorForm";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox txtDeCompressed;
		private System.Windows.Forms.Button btnCompress;
		private System.Windows.Forms.Button btnDeCompress;
		private System.Windows.Forms.TextBox txtCompressed;
	}
}