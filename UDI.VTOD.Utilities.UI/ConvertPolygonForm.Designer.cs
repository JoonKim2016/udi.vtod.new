﻿namespace UDI.VTOD.Utilities.UI
{
    partial class ConvertPolygonForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.txtMTDataGeolocations = new System.Windows.Forms.TextBox();
            this.btnConvertGPX = new System.Windows.Forms.Button();
            this.btnPushVtod = new System.Windows.Forms.Button();
            this.txtGPX = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txtMTDataGeolocations
            // 
            this.txtMTDataGeolocations.Location = new System.Drawing.Point(9, 10);
            this.txtMTDataGeolocations.Margin = new System.Windows.Forms.Padding(2);
            this.txtMTDataGeolocations.Multiline = true;
            this.txtMTDataGeolocations.Name = "txtMTDataGeolocations";
            this.txtMTDataGeolocations.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtMTDataGeolocations.Size = new System.Drawing.Size(374, 486);
            this.txtMTDataGeolocations.TabIndex = 0;
            // 
            // btnConvertGPX
            // 
            this.btnConvertGPX.Location = new System.Drawing.Point(387, 53);
            this.btnConvertGPX.Margin = new System.Windows.Forms.Padding(2);
            this.btnConvertGPX.Name = "btnConvertGPX";
            this.btnConvertGPX.Size = new System.Drawing.Size(94, 176);
            this.btnConvertGPX.TabIndex = 1;
            this.btnConvertGPX.Text = "GPX>>";
            this.btnConvertGPX.UseVisualStyleBackColor = true;
            this.btnConvertGPX.Click += new System.EventHandler(this.btnConvertGPX_Click);
            // 
            // btnPushVtod
            // 
            this.btnPushVtod.Location = new System.Drawing.Point(387, 296);
            this.btnPushVtod.Margin = new System.Windows.Forms.Padding(2);
            this.btnPushVtod.Name = "btnPushVtod";
            this.btnPushVtod.Size = new System.Drawing.Size(94, 157);
            this.btnPushVtod.TabIndex = 2;
            this.btnPushVtod.Text = "<< Push In Vtod";
            this.btnPushVtod.UseVisualStyleBackColor = true;
            this.btnPushVtod.Click += new System.EventHandler(this.btnPushVtod_Click);
            // 
            // txtGPX
            // 
            this.txtGPX.Location = new System.Drawing.Point(485, 10);
            this.txtGPX.Margin = new System.Windows.Forms.Padding(2);
            this.txtGPX.Multiline = true;
            this.txtGPX.Name = "txtGPX";
            this.txtGPX.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtGPX.Size = new System.Drawing.Size(374, 486);
            this.txtGPX.TabIndex = 3;
            // 
            // ConvertPolygonForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(868, 516);
            this.Controls.Add(this.txtGPX);
            this.Controls.Add(this.btnPushVtod);
            this.Controls.Add(this.btnConvertGPX);
            this.Controls.Add(this.txtMTDataGeolocations);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "ConvertPolygonForm";
            this.Text = "Convert Polygon Form";
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox txtMTDataGeolocations;
        private System.Windows.Forms.Button btnConvertGPX;
        private System.Windows.Forms.Button btnPushVtod;
        private System.Windows.Forms.TextBox txtGPX;
	}
}