﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UDI.Utility.Serialization;
using System.Xml;
using Newtonsoft.Json;

namespace UDI.VTOD.Utilities.UI
{
    public partial class ConvertPolygonForm : Form
    {
        public string gpxFileFormat;
        string gelocationData;
        public ConvertPolygonForm()
        {
            InitializeComponent();
        }

        private void btnConvertGPX_Click(object sender, EventArgs e)
        {

            var result = txtMTDataGeolocations.Text;
            if (result != null)
            {
                string xml = result;
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);
                string jsonvALUE = Newtonsoft.Json.JsonConvert.SerializeXmlNode(doc);
                //var mtdataResponse = jsonvALUE.JsonDeserialize<GetHostedOrganisationsResult>();
                var mtdataResponse = JsonConvert.DeserializeObject<MTDataHostedOrganisationsRS>(jsonvALUE);
                    if (mtdataResponse != null && mtdataResponse.GetHostedOrganisationsResult.HostedOrganisations != null)
                    {
                            if (mtdataResponse.GetHostedOrganisationsResult.HostedOrganisations.HostedOrganisationInfo != null)
                            {
                                if (mtdataResponse.GetHostedOrganisationsResult.HostedOrganisations.HostedOrganisationInfo.BookingZones != null)
                                {
                                    if (mtdataResponse.GetHostedOrganisationsResult.HostedOrganisations.HostedOrganisationInfo.BookingZones.HostedBookingZoneInfo != null)
                                    {
                                        if (mtdataResponse.GetHostedOrganisationsResult.HostedOrganisations.HostedOrganisationInfo.BookingZones.HostedBookingZoneInfo.Polygon != null)
                                        {
                                            if (mtdataResponse.GetHostedOrganisationsResult.HostedOrganisations.HostedOrganisationInfo.BookingZones.HostedBookingZoneInfo.Polygon.Points != null)
                                            {
                                                gelocationData = "<rte><name>" + mtdataResponse.GetHostedOrganisationsResult.HostedOrganisations.HostedOrganisationInfo.BookingZones.HostedBookingZoneInfo.FleetName + "</name><desc/>";
                                                foreach (UDI.VTOD.Utilities.UI.GEOLocationList item in mtdataResponse.GetHostedOrganisationsResult.HostedOrganisations.HostedOrganisationInfo.BookingZones.HostedBookingZoneInfo.Polygon.Points.GEOLocation)
                                                {
                                                    string lat = item.Latitude;
                                                    string longitude = item.Longitude;
                                                    gelocationData = gelocationData + "<rtept lat=\"" + lat + "\" lon=\"" + longitude + "\"/>";

                                                }
                                                gelocationData = gelocationData + "</rte></gpx>";
                                            }
                                        }

                                    }
                                }
                            }
                    }
                if (gelocationData != string.Empty)
                {
                    gpxFileFormat = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
                    gpxFileFormat = gpxFileFormat + "<gpx xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" version=\"1.1\" creator=\"Bing Maps\" xmlns=\"http://www.topografix.com/GPX/1/1\">";
                    gpxFileFormat = gpxFileFormat + "<metadata><name>Polygon</name><desc/></metadata>";
                }
                txtGPX.Text = gpxFileFormat + gelocationData;
            }
        }

        private void btnPushVtod_Click(object sender, EventArgs e)
        {

        }


    }
}
