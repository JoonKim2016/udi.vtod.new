﻿namespace UDI.VTOD.Utilities.UI
{
	partial class ConverterForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtXML = new System.Windows.Forms.TextBox();
            this.txtJSON = new System.Windows.Forms.TextBox();
            this.btnToJSON = new System.Windows.Forms.Button();
            this.btnToXML = new System.Windows.Forms.Button();
            this.dlstType = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 36);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "XML:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(571, 36);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "JSON:";
            // 
            // txtXML
            // 
            this.txtXML.Location = new System.Drawing.Point(11, 52);
            this.txtXML.Margin = new System.Windows.Forms.Padding(2);
            this.txtXML.Multiline = true;
            this.txtXML.Name = "txtXML";
            this.txtXML.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtXML.Size = new System.Drawing.Size(476, 506);
            this.txtXML.TabIndex = 2;
            // 
            // txtJSON
            // 
            this.txtJSON.Location = new System.Drawing.Point(573, 52);
            this.txtJSON.Margin = new System.Windows.Forms.Padding(2);
            this.txtJSON.Multiline = true;
            this.txtJSON.Name = "txtJSON";
            this.txtJSON.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtJSON.Size = new System.Drawing.Size(483, 506);
            this.txtJSON.TabIndex = 3;
            // 
            // btnToJSON
            // 
            this.btnToJSON.Location = new System.Drawing.Point(491, 52);
            this.btnToJSON.Margin = new System.Windows.Forms.Padding(2);
            this.btnToJSON.Name = "btnToJSON";
            this.btnToJSON.Size = new System.Drawing.Size(77, 241);
            this.btnToJSON.TabIndex = 4;
            this.btnToJSON.Text = "To JSON >";
            this.btnToJSON.UseVisualStyleBackColor = true;
            this.btnToJSON.Click += new System.EventHandler(this.btnToJSON_Click);
            // 
            // btnToXML
            // 
            this.btnToXML.Location = new System.Drawing.Point(490, 298);
            this.btnToXML.Margin = new System.Windows.Forms.Padding(2);
            this.btnToXML.Name = "btnToXML";
            this.btnToXML.Size = new System.Drawing.Size(78, 259);
            this.btnToXML.TabIndex = 5;
            this.btnToXML.Text = "< To XML";
            this.btnToXML.UseVisualStyleBackColor = true;
            this.btnToXML.Click += new System.EventHandler(this.btnToXML_Click);
            // 
            // dlstType
            // 
            this.dlstType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dlstType.FormattingEnabled = true;
            this.dlstType.Items.AddRange(new object[] {
            "OTA_GroundAvailRQ",
            "OTA_GroundAvailRS",
            "OTA_GroundBookRQ",
            "OTA_GroundBookRS",
            "OTA_GroundCancelRQ",
            "OTA_GroundCancelRS",
            "OTA_GroundResRetrieveRQ",
            "OTA_GroundResRetrieveRS",
            "TokenRQ",
            "TokenRS",
            "MemberCreateRQ",
            "MemberCreateRS",
            "MemberChangeEmailAddressRQ",
            "MemberChangeEmailAddressRS",
            "MemberLoginRQ",
            "MemberLoginRS",
            "MemberForgotPasswordRQ",
            "MemberForgotPasswordRS",
            "MemberUpdateProfileRQ",
            "MemberUpdateProfileRS",
            "MemberChangePasswordRQ",
            "MemberChangePasswordRS",
            "MemberGetReservationRQ",
            "MemberGetReservationRS",
            "MemberAddLocationRQ",
            "MemberAddLocationRS",
            "MemberUpdateLocationRQ",
            "MemberUpdateLocationRS",
            "MemberDeleteLocationRQ",
            "MemberDeleteLocationRS",
            "MemberGetLocationsRQ",
            "MemberGetLocationsRS",
            "AccountingAddMemberCreditCardRQ",
            "AccountingAddMemberCreditCardRS",
            "AccountingGetMemberPaymentCardsRQ",
            "AccountingGetMemberPaymentCardsRS",
            "AccountingSetMemberDefaultPaymentCardRQ",
            "AccountingSetMemberDefaultPaymentCardRS",
            "AccountingDeleteMemberPaymentCardRQ",
            "AccountingDeleteMemberPaymentCardRS",
            "AccountingAddMemberDirectBillRQ",
            "AccountingAddMemberDirectBillRS",
            "AccountingGetMemberDirectBillsRQ",
            "AccountingGetMemberDirectBillsRS",
            "AccountingDeleteMemberDirectBillRQ",
            "AccountingDeleteMemberDirectBillRS",
            "AccountingSetMemberDefaultDirectBillRQ",
            "AccountingSetMemberDefaultDirectBillRS",
            "UtilityGetServicedAirportsRQ",
            "UtilityGetServicedAirportsRS",
            "UtilityGetDefaultFleetTypeForPointRQ",
            "UtilityGetDefaultFleetTypeForPointRS",
            "UtilityGetLocationListRQ",
            "UtilityGetLocationListRS",
            "UtilitySearchLocationRQ",
            "UtilitySearchLocationRS",
            "GetAirlineProgramsRQ",
            "GetAirlineProgramsRS",
            "RewardCreateMemberAccountRQ",
            "RewardCreateMemberAccountRS",
            "RewardDeleteMemberAccountRQ",
            "RewardDeleteMemberAccountRS",
            "RewardGetMemberAccountsRQ",
            "RewardGetMemberAccountsRS",
            "ZTripCreditActivateMemberCreditRQ",
            "ZTripCreditActivateMemberCreditRS",
            "ZTripCreditGetMemberBalanceRQ",
            "ZTripCreditGetMemberBalanceRS",
            "GroundGetASAPRequestStatusRQ",
            "GroundGetASAPRequestStatusRS",
            "GroundAbandonASAPRequestRQ",
            "GroundAbandonASAPRequestRS",
            "GetFareDetailRQ",
            "GetFareDetailRS",
            "RateSetRQ",
            "RateSetRS",
            "RateGetRQ",
            "RateGetRS",
            "UtilityGetAirportNameRQ",
            "UtilityGetAirportNameRS",
            "UtilityAvailableFleetsRQ",
            "UtilityAvailableFleetsRS",
            "UtilityGetAirlinesByAirportRQ",
            "UtilityGetAirlinesByAirportRS",
            "UtilityGetLeadTimeRQ",
            "UtilityGetLeadTimeRS",
            "RequestEmailConfirmationRQ",
            "RequestEmailConfirmationRS",
            "RewardSetDefaultAccountRQ",
            "RewardSetDefaultAccountRS",
            "MemberGetLatestUnRatedTripRQ",
            "MemberGetLatestUnRatedTripRS",
            "UtilityGetTripListRQ",
            "UtilityGetTripListRS",
            "UtilityGetAirportInstructionsRQ",
            "UtilityGetAirportInstructionsRS",
            "GroupValidateDiscountCodeRQ",
            "GroupValidateDiscountCodeRS",
            "GroupGetDiscountRQ",
            "GroupGetDiscountRS",
            "GroupGetDefaultsRQ",
            "GroupGetDefaultsRS",
            "GroupGetLandmarksRQ",
            "GroupGetLandmarksRS",
            "GroupGetLocalizedServicedAirportsRQ",
            "GroupGetLocalizedServicedAirportsRS",
            "MemberGetPasswordRequestRQ",
            "MemberGetPasswordRequestRS",
            "GroupGetPromotionTypeRQ",
            "GroupGetPromotionTypeRS",
            "MemberResetPasswordRQ",
            "MemberResetPasswordRS",
            "UtilityGetHonorificsRQ",
            "UtilityGetHonorificsRS",
            "UtilityGetDestinationsRQ",
            "UtilityGetDestinationsRS",
            "MemberLinkReservationRQ",
            "MemberLinkReservationRS",
            "UtilityGetBookingCriteriaRQ",
            "UtilityGetBookingCriteriaRS",
            "MemberGetCorporateProfileRQ",
            "MemberGetCorporateProfileRS",
            "GroundGetPickupTimeRQ",
            "GroundGetPickupTimeRS",
            "GroundGetFareScheduleWithFeesRQ",
            "GroundGetFareScheduleWithFeesRS",
            "MemberDeleteRQ",
            "MemberDeleteRS",
            "UtilityGetCorporateRequirementsRQ",
            "UtilityGetCorporateRequirementsRS",
            "UtilityGetCorporatePaymentMethodRQ",
            "UtilityGetCorporatePaymentMethodRS",
            "UtilityGetAirportPickupOptionsRQ",
            "UtilityGetAirportPickupOptionsRS",
            "(something wrong in index)",
            "SplitPaymentInviteRQ",
            "SplitPaymentInviteRS",
            "SplitPaymentUpdateStatusRQ",
            "SplitPaymentUpdateStatusRS",
            "SplitPaymentGetInvitationsRQ",
            "SplitPaymentGetInvitationsRS",
            "SplitPaymentCheckIfEnabledRQ",
            "SplitPaymentCheckIfEnabledRS"});
            this.dlstType.Location = new System.Drawing.Point(11, 11);
            this.dlstType.Margin = new System.Windows.Forms.Padding(2);
            this.dlstType.Name = "dlstType";
            this.dlstType.Size = new System.Drawing.Size(476, 21);
            this.dlstType.TabIndex = 7;
            // 
            // ConverterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1064, 567);
            this.Controls.Add(this.dlstType);
            this.Controls.Add(this.btnToXML);
            this.Controls.Add(this.btnToJSON);
            this.Controls.Add(this.txtJSON);
            this.Controls.Add(this.txtXML);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "ConverterForm";
            this.Text = "ConverterForm";
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox txtXML;
		private System.Windows.Forms.TextBox txtJSON;
		private System.Windows.Forms.Button btnToJSON;
		private System.Windows.Forms.Button btnToXML;
		private System.Windows.Forms.ComboBox dlstType;
	}
}