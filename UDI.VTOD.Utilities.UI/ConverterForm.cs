﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using Newtonsoft.Json.Linq;
using UDI.Utility.Helper;
using UDI.Utility.Serialization;
using UDI.VTOD.Common.DTO.OTA;

namespace UDI.VTOD.Utilities.UI
{
	public partial class ConverterForm : Form
	{
		public ConverterForm()
		{
			InitializeComponent();
			dlstType.SelectedIndex = 0;
		}

		private void btnToJSON_Click(object sender, EventArgs e)
		{
			var result = string.Empty;

			try
			{
				switch (dlstType.SelectedIndex)
				{
					case 0:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<OTA_GroundAvailRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<OTA_GroundAvailRQ>();
						}
						break;
					case 1:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<OTA_GroundAvailRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<OTA_GroundAvailRS>();
						}
						break;
					case 2:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<OTA_GroundBookRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<OTA_GroundBookRQ>();
						}
						break;
					case 3:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<OTA_GroundBookRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<OTA_GroundBookRS>();
						}
						break;
					case 4:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<OTA_GroundCancelRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<OTA_GroundCancelRQ>();
						}
						break;
					case 5:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<OTA_GroundCancelRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<OTA_GroundCancelRS>();
						}
						break;
					case 6:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<OTA_GroundResRetrieveRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<OTA_GroundResRetrieveRQ>();
						}
						break;
					case 7:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<OTA_GroundResRetrieveRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<OTA_GroundResRetrieveRS>();
						}
						break;
					case 8:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<TokenRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<TokenRQ>();
						}
						break;
					case 9:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<TokenRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<TokenRS>();
						}
						break;
					case 10:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<MemberCreateRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<MemberCreateRQ>();
						}
						break;
					case 11:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<MemberCreateRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<MemberCreateRS>();
						}
						break;
					case 12:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<MemberChangeEmailAddressRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<MemberChangeEmailAddressRQ>();
						}
						break;
					case 13:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<MemberChangeEmailAddressRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<MemberChangeEmailAddressRS>();
						}
						break;
					case 14:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<MemberLoginRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<MemberLoginRQ>();
						}
						break;
					case 15:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<MemberLoginRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<MemberLoginRS>();
						}
						break;
					case 16:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<MemberForgotPasswordRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<MemberForgotPasswordRQ>();
						}
						break;
					case 17:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<MemberForgotPasswordRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<MemberForgotPasswordRS>();
						}
						break;
					case 18:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<MemberUpdateProfileRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<MemberUpdateProfileRQ>();
						}
						break;
					case 19:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<MemberUpdateProfileRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<MemberUpdateProfileRS>();
						}
						break;
					case 20:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<MemberChangePasswordRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<MemberChangePasswordRQ>();
						}
						break;
					case 21:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<MemberChangePasswordRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<MemberChangePasswordRS>();
						}
						break;
					case 22:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<MemberGetReservationRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<MemberGetReservationRQ>();
						}
						break;
					case 23:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<MemberGetReservationRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<MemberGetReservationRS>();
						}
						break;
					case 24:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<MemberAddLocationRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<MemberAddLocationRQ>();
						}
						break;
					case 25:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<MemberAddLocationRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<MemberAddLocationRS>();
						}
						break;
					case 26:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<MemberUpdateLocationRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<MemberUpdateLocationRQ>();
						}
						break;
					case 27:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<MemberUpdateLocationRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<MemberUpdateLocationRS>();
						}
						break;
					case 28:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<MemberDeleteLocationRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<MemberDeleteLocationRQ>();
						}
						break;
					case 29:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<MemberDeleteLocationRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<MemberDeleteLocationRS>();
						}
						break;
					case 30:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<MemberGetLocationsRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<MemberGetLocationsRQ>();
						}
						break;
					case 31:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<MemberGetLocationsRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<MemberGetLocationsRS>();
						}
						break;
					case 32:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<AccountingAddMemberPaymentCardRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<AccountingAddMemberPaymentCardRQ>();
						}
						break;
					case 33:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<AccountingEditMemberPaymentCardRQ>();//.JsonSerialize<AuthRequest>();
                            result = obj.JsonSerialize<AccountingEditMemberPaymentCardRQ>();
						}
						break;
					case 34:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<AccountingGetMemberPaymentCardsRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<AccountingGetMemberPaymentCardsRQ>();
						}
						break;
					case 35:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<AccountingGetMemberPaymentCardsRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<AccountingGetMemberPaymentCardsRS>();
						}
						break;
					case 36:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<AccountingSetMemberDefaultPaymentCardRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<AccountingSetMemberDefaultPaymentCardRQ>();
						}
						break;
					case 37:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<AccountingSetMemberDefaultPaymentCardRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<AccountingSetMemberDefaultPaymentCardRS>();
						}
						break;
					case 38:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<AccountingDeleteMemberPaymentCardRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<AccountingDeleteMemberPaymentCardRQ>();
						}
						break;
					case 39:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<AccountingDeleteMemberPaymentCardRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<AccountingDeleteMemberPaymentCardRS>();
						}
						break;
					case 40:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<AccountingAddMemberDirectBillRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<AccountingAddMemberDirectBillRQ>();
						}
						break;
					case 41:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<AccountingAddMemberDirectBillRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<AccountingAddMemberDirectBillRS>();
						}
						break;
					case 42:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<AccountingGetMemberDirectBillsRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<AccountingGetMemberDirectBillsRQ>();
						}
						break;
					case 43:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<AccountingGetMemberDirectBillsRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<AccountingGetMemberDirectBillsRS>();
						}
						break;
					case 44:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<AccountingDeleteMemberDirectBillRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<AccountingDeleteMemberDirectBillRQ>();
						}
						break;
					case 45:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<AccountingDeleteMemberDirectBillRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<AccountingDeleteMemberDirectBillRS>();
						}
						break;
					case 46:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<AccountingSetMemberDefaultDirectBillRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<AccountingSetMemberDefaultDirectBillRQ>();
						}
						break;
					case 47:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<AccountingSetMemberDefaultDirectBillRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<AccountingSetMemberDefaultDirectBillRS>();
						}
						break;
					case 48:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<UtilityGetServicedAirportsRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<UtilityGetServicedAirportsRQ>();
						}
						break;
					case 49:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<UtilityGetServicedAirportsRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<UtilityGetServicedAirportsRS>();
						}
						break;
					case 50:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<UtilityGetDefaultFleetTypeForPointRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<UtilityGetDefaultFleetTypeForPointRQ>();
						}
						break;
					case 51:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<UtilityGetDefaultFleetTypeForPointRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<UtilityGetDefaultFleetTypeForPointRS>();
						}
						break;
					case 52:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<UtilityGetLocationListRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<UtilityGetLocationListRQ>();
						}
						break;
					case 53:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<UtilityGetLocationListRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<UtilityGetLocationListRS>();
						}
						break;
					case 54:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<UtilitySearchLocationRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<UtilitySearchLocationRQ>();
						}
						break;
					case 55:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<UtilitySearchLocationRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<UtilitySearchLocationRS>();
						}
						break;
					case 56:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<RewardGetAirlineProgramsRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<RewardGetAirlineProgramsRQ>();
						}
						break;
					case 57:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<RewardGetAirlineProgramsRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<RewardGetAirlineProgramsRS>();
						}
						break;
					case 58:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<RewardCreateMemberAccountRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<RewardCreateMemberAccountRQ>();
						}
						break;
					case 59:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<RewardCreateMemberAccountRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<RewardCreateMemberAccountRS>();
						}
						break;
					case 60:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<RewardDeleteMemberAccountRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<RewardDeleteMemberAccountRQ>();
						}
						break;
					case 61:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<RewardDeleteMemberAccountRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<RewardDeleteMemberAccountRS>();
						}
						break;
					case 62:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<RewardGetMemberAccountsRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<RewardGetMemberAccountsRQ>();
						}
						break;
					case 63:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<RewardGetMemberAccountsRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<RewardGetMemberAccountsRS>();
						}
						break;
					case 64:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<ZTripCreditActivateMemberCreditRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<ZTripCreditActivateMemberCreditRQ>();
						}
						break;
					case 65:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<ZTripCreditActivateMemberCreditRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<ZTripCreditActivateMemberCreditRS>();
						}
						break;
					case 66:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<ZTripCreditGetMemberBalanceRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<ZTripCreditGetMemberBalanceRQ>();
						}
						break;
					case 67:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<ZTripCreditGetMemberBalanceRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<ZTripCreditGetMemberBalanceRS>();
						}
						break;
					case 68:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<GroundGetASAPRequestStatusRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<GroundGetASAPRequestStatusRQ>();
						}
						break;
					case 69:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<GroundGetASAPRequestStatusRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<GroundGetASAPRequestStatusRS>();
						}
						break;
					case 70:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<GroundAbandonASAPRequestRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<GroundAbandonASAPRequestRQ>();
						}
						break;
					case 71:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<GroundAbandonASAPRequestRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<GroundAbandonASAPRequestRS>();
						}
						break;
					case 72:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<GetFareDetailRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<GetFareDetailRQ>();
						}
						break;
					case 73:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<GetFareDetailRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<GetFareDetailRS>();
						}
						break;
					case 74:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<RateSetRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<RateSetRQ>();
						}
						break;
					case 75:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<RateSetRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<RateSetRS>();
						}
						break;
					case 76:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<RateGetRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<RateGetRQ>();
						}
						break;
					case 77:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<RateGetRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<RateGetRS>();
						}
						break;
					case 78:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<UtilityGetAirportNameRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<UtilityGetAirportNameRQ>();
						}
						break;
					case 79:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<UtilityGetAirportNameRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<UtilityGetAirportNameRS>();
						}
						break;
					case 80:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<UtilityAvailableFleetsRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<UtilityAvailableFleetsRQ>();
						}
						break;
					case 81:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<UtilityAvailableFleetsRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<UtilityAvailableFleetsRS>();
						}
						break;
					case 82:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<UtilityGetAirlinesByAirportRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<UtilityGetAirlinesByAirportRQ>();
						}
						break;
					case 83:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<UtilityGetAirlinesByAirportRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<UtilityGetAirlinesByAirportRS>();
						}
						break;
					case 84:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<MemberRequestEmailConfirmationRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<MemberRequestEmailConfirmationRQ>();
						}
						break;
					case 85:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<MemberRequestEmailConfirmationRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<MemberRequestEmailConfirmationRS>();
						}
						break;
					case 86:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<RewardSetDefaultAccountRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<RewardSetDefaultAccountRQ>();
						}
						break;
					case 87:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<RewardSetDefaultAccountRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<RewardSetDefaultAccountRS>();
						}
						break;
					case 88:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<MemberGetLatestUnRatedTripRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<MemberGetLatestUnRatedTripRQ>();
						}
						break;
					case 89:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<MemberGetLatestUnRatedTripRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<MemberGetLatestUnRatedTripRS>();
						}
						break;
					case 90:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<UtilityGetTripListRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<UtilityGetTripListRQ>();
						}
						break;
					case 91:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<UtilityGetTripListRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<UtilityGetTripListRS>();
						}
						break;
					case 92:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<UtilityGetAirportInstructionsRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<UtilityGetAirportInstructionsRQ>();
						}
						break;
					case 93:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<UtilityGetAirportInstructionsRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<UtilityGetAirportInstructionsRS>();
						}
						break;
					case 94:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<GroupValidateDiscountCodeRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<GroupValidateDiscountCodeRQ>();
						}
						break;
					case 95:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<GroupValidateDiscountCodeRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<GroupValidateDiscountCodeRS>();
						}
						break;
					case 96:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<GroupGetDiscountRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<GroupGetDiscountRQ>();
						}
						break;
					case 97:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<GroupGetDiscountRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<GroupGetDiscountRS>();
						}
						break;

					case 98:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<GroupGetDefaultsRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<GroupGetDefaultsRQ>();
						}
						break;
					case 99:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<GroupGetDefaultsRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<GroupGetDefaultsRS>();
						}
						break;
					case 100:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<GroupGetLandmarksRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<GroupGetLandmarksRQ>();
						}
						break;
					case 101:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<GroupGetLandmarksRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<GroupGetLandmarksRS>();
						}
						break;
					case 102:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<GroupGetLocalizedServicedAirportsRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<GroupGetLocalizedServicedAirportsRQ>();
						}
						break;
					case 103:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<GroupGetLocalizedServicedAirportsRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<GroupGetLocalizedServicedAirportsRS>();
						}
						break;
					case 104:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<MemberGetPasswordRequestRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<MemberGetPasswordRequestRQ>();
						}
						break;
					case 105:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<MemberGetPasswordRequestRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<MemberGetPasswordRequestRS>();
						}
						break;
					case 106:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<GroupGetPromotionTypeRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<GroupGetPromotionTypeRQ>();
						}
						break;
					case 107:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<GroupGetPromotionTypeRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<GroupGetPromotionTypeRS>();
						}
						break;
					case 108:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<MemberResetPasswordRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<MemberResetPasswordRQ>();
						}
						break;
					case 109:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<MemberResetPasswordRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<MemberResetPasswordRS>();
						}
						break;
					case 110:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<UtilityGetHonorificsRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<UtilityGetHonorificsRQ>();
						}
						break;
					case 111:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<UtilityGetHonorificsRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<UtilityGetHonorificsRS>();
						}
						break;
					case 112:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<UtilityGetDestinationsRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<UtilityGetDestinationsRQ>();
						}
						break;
					case 113:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<UtilityGetDestinationsRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<UtilityGetDestinationsRS>();
						}
						break;
					case 114:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<MemberLinkReservationRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<MemberLinkReservationRQ>();
						}
						break;
					case 115:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<MemberLinkReservationRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<MemberLinkReservationRS>();
						}
						break;
					case 116:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<UtilityGetBookingCriteriaRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<UtilityGetBookingCriteriaRQ>();
						}
						break;
					case 117:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<UtilityGetBookingCriteriaRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<UtilityGetBookingCriteriaRS>();
						}
						break;
					case 118:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<MemberGetCorporateProfileRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<MemberGetCorporateProfileRQ>();
						}
						break;
					case 119:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<MemberGetCorporateProfileRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<MemberGetCorporateProfileRS>();
						}
						break;
					case 120:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<GroundGetPickupTimeRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<GroundGetPickupTimeRQ>();
						}
						break;
					case 121:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<GroundGetPickupTimeRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<GroundGetPickupTimeRS>();
						}
						break;
					case 122:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<GroundGetFareScheduleWithFeesRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<GroundGetFareScheduleWithFeesRQ>();
						}
						break;
					case 123:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<GroundGetFareScheduleWithFeesRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<GroundGetFareScheduleWithFeesRS>();
						}
						break;
					case 124:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<MemberDeleteRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<MemberDeleteRQ>();
						}
						break;
					case 125:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<MemberDeleteRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize<MemberDeleteRS>();
						}
						break;
                    case 126:
                        {
                            var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
                            var obj = xdoc.XmlDeserialize<UtilityGetCorporateRequirementsRQ>();//.JsonSerialize<AuthRequest>();
                            result = obj.JsonSerialize();
                        }
                        break;
                    case 127:
                        {
                            var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
                            var obj = xdoc.XmlDeserialize<UtilityGetCorporateRequirementsRS>();//.JsonSerialize<AuthRequest>();
                            result = obj.JsonSerialize();
                        }
                        break;
					case 128:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<UtilityGetCorporateRequirementsRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize();
						}
						break;
					case 129:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<UtilityGetCorporateRequirementsRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize();
						}
						break;
					case 130:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<UtilityGetAirportPickupOptionsRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize();
						}
						break;
					case 131:
						{
							var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
							var obj = xdoc.XmlDeserialize<UtilityGetAirportPickupOptionsRS>();//.JsonSerialize<AuthRequest>();
							result = obj.JsonSerialize();
						}
						break;
                    case 132:
                        {
                            var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
                            var obj = xdoc.XmlDeserialize<UtilityGetLeadTimeRS>();//.JsonSerialize<AuthRequest>();
                            result = obj.JsonSerialize();
                        }
                        break;
                    case 133:
                        {
                            var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
                            var obj = xdoc.XmlDeserialize<MemberRequestEmailCancellationRQ>();//.JsonSerialize<AuthRequest>();
                            result = obj.JsonSerialize<MemberRequestEmailCancellationRQ>();
                        }
                        break;
                    case 134:
                        {
                            var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
                            var obj = xdoc.XmlDeserialize<MemberRequestEmailCancellationRS>();//.JsonSerialize<AuthRequest>();
                            result = obj.JsonSerialize<MemberRequestEmailCancellationRS>();
                        }
                        break;
                    case 135:
                        {
                            var xdoc = XDocument.Parse(txtXML.Text).CleanComment();
                            var obj = xdoc.XmlDeserialize<SplitPaymentInviteRQ>();
                            result = obj.JsonSerialize();
                        }
                        break;
                    case 136:
                        {
                            var xdoc = XDocument.Parse(txtXML.Text).CleanComment();
                            var obj = xdoc.XmlDeserialize<SplitPaymentInviteRS>();
                            result = obj.JsonSerialize();
                        }
                        break;
                    case 137:
                        {
                            var xdoc = XDocument.Parse(txtXML.Text).CleanComment();
                            var obj = xdoc.XmlDeserialize<SplitPaymentUpdateStatusRQ>();
                            result = obj.JsonSerialize();
                        }
                        break;
                    case 138:
                        {
                            var xdoc = XDocument.Parse(txtXML.Text).CleanComment();
                            var obj = xdoc.XmlDeserialize<SplitPaymentUpdateStatusRS>();
                            result = obj.JsonSerialize();
                        }
                        break;
                    case 139:
                        {
                            var xdoc = XDocument.Parse(txtXML.Text).CleanComment();
                            var obj = xdoc.XmlDeserialize<SplitPaymentGetInvitationsRQ>();
                            result = obj.JsonSerialize();
                        }
                        break;
                    case 140:
                        {
                            var xdoc = XDocument.Parse(txtXML.Text).CleanComment();
                            var obj = xdoc.XmlDeserialize<SplitPaymentGetInvitationsRS>();
                            result = obj.JsonSerialize();
                        }
                        break;
                    case 141:
                        {
                            var xdoc = XDocument.Parse(txtXML.Text).CleanComment();
                            var obj = xdoc.XmlDeserialize<SplitPaymentCheckIfEnabledRQ>();
                            result = obj.JsonSerialize();
                        }
                        break;
                    case 142:
                        {
                            var xdoc = XDocument.Parse(txtXML.Text).CleanComment();
                            var obj = xdoc.XmlDeserialize<SplitPaymentCheckIfEnabledRS>();
                            result = obj.JsonSerialize();
                        }
                        break;

                    case 143:
                        {
                            var xdoc = XDocument.Parse(txtXML.Text).CleanComment();//.CleanNamespace();
                            var obj = xdoc.XmlDeserialize<UtilityGetClientTokenRQ>();//.JsonSerialize<AuthRequest>();
                            result = obj.JsonSerialize<UtilityGetClientTokenRQ>();
                        }
                        break;

                }

                JObject json = JObject.Parse(result);
                txtJSON.Text = json.ToString();
			}
			catch (Exception ex)
			{
                txtJSON.Text = ex.Message;
			}
		}

		private void btnToXML_Click(object sender, EventArgs e)
		{
			var result = string.Empty;

			try
			{
				switch (dlstType.SelectedIndex)
				{
					case 0:
						{
							//var xdoc = XDocument.Parse(txtJSON.Text).CleanComment().CleanNamespace();
							var obj = txtJSON.Text.JsonDeserialize<OTA_GroundAvailRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<OTA_GroundAvailRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 1:
						{
							var obj = txtJSON.Text.JsonDeserialize<OTA_GroundAvailRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<OTA_GroundAvailRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 2:
						{
							var obj = txtJSON.Text.JsonDeserialize<OTA_GroundBookRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<OTA_GroundBookRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 3:
						{
							var obj = txtJSON.Text.JsonDeserialize<OTA_GroundAvailRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<OTA_GroundAvailRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 4:
						{
							var obj = txtJSON.Text.JsonDeserialize<OTA_GroundCancelRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<OTA_GroundCancelRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 5:
						{
							var obj = txtJSON.Text.JsonDeserialize<OTA_GroundCancelRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<OTA_GroundCancelRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 6:
						{
							var obj = txtJSON.Text.JsonDeserialize<OTA_GroundResRetrieveRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<OTA_GroundResRetrieveRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 7:
						{
							var obj = txtJSON.Text.JsonDeserialize<OTA_GroundResRetrieveRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<OTA_GroundResRetrieveRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 8:
						{
							var obj = txtJSON.Text.JsonDeserialize<TokenRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<TokenRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 9:
						{
							var obj = txtJSON.Text.JsonDeserialize<TokenRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<TokenRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 10:
						{
							var obj = txtJSON.Text.JsonDeserialize<MemberCreateRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<MemberCreateRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 11:
						{
							var obj = txtJSON.Text.JsonDeserialize<MemberCreateRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<MemberCreateRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 12:
						{
							var obj = txtJSON.Text.JsonDeserialize<MemberChangeEmailAddressRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<MemberChangeEmailAddressRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 13:
						{
							var obj = txtJSON.Text.JsonDeserialize<MemberChangeEmailAddressRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<MemberChangeEmailAddressRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 14:
						{
							var obj = txtJSON.Text.JsonDeserialize<MemberLoginRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<MemberLoginRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 15:
						{
							var obj = txtJSON.Text.JsonDeserialize<MemberLoginRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<MemberLoginRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 16:
						{
							var obj = txtJSON.Text.JsonDeserialize<MemberForgotPasswordRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<MemberForgotPasswordRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 17:
						{
							var obj = txtJSON.Text.JsonDeserialize<MemberForgotPasswordRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<MemberForgotPasswordRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 18:
						{
							var obj = txtJSON.Text.JsonDeserialize<MemberUpdateProfileRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<MemberUpdateProfileRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 19:
						{
							var obj = txtJSON.Text.JsonDeserialize<MemberUpdateProfileRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<MemberUpdateProfileRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 20:
						{
							var obj = txtJSON.Text.JsonDeserialize<MemberChangePasswordRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<MemberChangePasswordRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 21:
						{
							var obj = txtJSON.Text.JsonDeserialize<MemberChangePasswordRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<MemberChangePasswordRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 22:
						{
							var obj = txtJSON.Text.JsonDeserialize<MemberGetReservationRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<MemberGetReservationRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 23:
						{
							var obj = txtJSON.Text.JsonDeserialize<MemberGetReservationRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<MemberGetReservationRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 24:
						{
							var obj = txtJSON.Text.JsonDeserialize<MemberGetReservationRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<MemberGetReservationRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 25:
						{
							var obj = txtJSON.Text.JsonDeserialize<MemberAddLocationRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<MemberAddLocationRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 26:
						{
							var obj = txtJSON.Text.JsonDeserialize<MemberUpdateLocationRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<MemberUpdateLocationRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 27:
						{
							var obj = txtJSON.Text.JsonDeserialize<MemberUpdateLocationRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<MemberUpdateLocationRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 28:
						{
							var obj = txtJSON.Text.JsonDeserialize<MemberDeleteLocationRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<MemberDeleteLocationRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 29:
						{
							var obj = txtJSON.Text.JsonDeserialize<MemberDeleteLocationRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<MemberDeleteLocationRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 30:
						{
							var obj = txtJSON.Text.JsonDeserialize<MemberGetLocationsRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<MemberGetLocationsRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 31:
						{
							var obj = txtJSON.Text.JsonDeserialize<MemberGetLocationsRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<MemberGetLocationsRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 32:
						{
							var obj = txtJSON.Text.JsonDeserialize<AccountingAddMemberPaymentCardRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<AccountingAddMemberPaymentCardRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 33:
						{
							var obj = txtJSON.Text.JsonDeserialize<AccountingEditMemberPaymentCardRQ>();//.JsonSerialize<AuthRequest>();
                            result = obj.XmlSerialize<AccountingEditMemberPaymentCardRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 34:
						{
							var obj = txtJSON.Text.JsonDeserialize<AccountingGetMemberPaymentCardsRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<AccountingGetMemberPaymentCardsRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 35:
						{
							var obj = txtJSON.Text.JsonDeserialize<AccountingGetMemberPaymentCardsRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<AccountingGetMemberPaymentCardsRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 36:
						{
							var obj = txtJSON.Text.JsonDeserialize<AccountingSetMemberDefaultPaymentCardRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<AccountingSetMemberDefaultPaymentCardRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 37:
						{
							var obj = txtJSON.Text.JsonDeserialize<AccountingSetMemberDefaultPaymentCardRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<AccountingSetMemberDefaultPaymentCardRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 38:
						{
							var obj = txtJSON.Text.JsonDeserialize<AccountingDeleteMemberPaymentCardRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<AccountingDeleteMemberPaymentCardRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 39:
						{
							var obj = txtJSON.Text.JsonDeserialize<AccountingDeleteMemberPaymentCardRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<AccountingDeleteMemberPaymentCardRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 40:
						{
							var obj = txtJSON.Text.JsonDeserialize<AccountingAddMemberDirectBillRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<AccountingAddMemberDirectBillRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 41:
						{
							var obj = txtJSON.Text.JsonDeserialize<AccountingAddMemberDirectBillRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<AccountingAddMemberDirectBillRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 42:
						{
							var obj = txtJSON.Text.JsonDeserialize<AccountingGetMemberDirectBillsRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<AccountingGetMemberDirectBillsRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 43:
						{
							var obj = txtJSON.Text.JsonDeserialize<AccountingGetMemberDirectBillsRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<AccountingGetMemberDirectBillsRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 44:
						{
							var obj = txtJSON.Text.JsonDeserialize<AccountingDeleteMemberDirectBillRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<AccountingDeleteMemberDirectBillRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 45:
						{
							var obj = txtJSON.Text.JsonDeserialize<AccountingDeleteMemberDirectBillRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<AccountingDeleteMemberDirectBillRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 46:
						{
							var obj = txtJSON.Text.JsonDeserialize<AccountingSetMemberDefaultDirectBillRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<AccountingSetMemberDefaultDirectBillRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 47:
						{
							var obj = txtJSON.Text.JsonDeserialize<AccountingSetMemberDefaultDirectBillRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<AccountingSetMemberDefaultDirectBillRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 48:
						{
							var obj = txtJSON.Text.JsonDeserialize<UtilityGetServicedAirportsRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<UtilityGetServicedAirportsRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 49:
						{
							var obj = txtJSON.Text.JsonDeserialize<UtilityGetServicedAirportsRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<UtilityGetServicedAirportsRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 50:
						{
							var obj = txtJSON.Text.JsonDeserialize<UtilityGetDefaultFleetTypeForPointRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<UtilityGetDefaultFleetTypeForPointRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 51:
						{
							var obj = txtJSON.Text.JsonDeserialize<UtilityGetDefaultFleetTypeForPointRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<UtilityGetDefaultFleetTypeForPointRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 52:
						{
							var obj = txtJSON.Text.JsonDeserialize<UtilityGetLocationListRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<UtilityGetLocationListRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 53:
						{
							var obj = txtJSON.Text.JsonDeserialize<UtilityGetLocationListRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<UtilityGetLocationListRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 54:
						{
							var obj = txtJSON.Text.JsonDeserialize<UtilitySearchLocationRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<UtilitySearchLocationRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 55:
						{
							var obj = txtJSON.Text.JsonDeserialize<UtilitySearchLocationRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<UtilitySearchLocationRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 56:
						{
							var obj = txtJSON.Text.JsonDeserialize<RewardGetAirlineProgramsRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<RewardGetAirlineProgramsRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 57:
						{
							var obj = txtJSON.Text.JsonDeserialize<RewardGetAirlineProgramsRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<RewardGetAirlineProgramsRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 58:
						{
							var obj = txtJSON.Text.JsonDeserialize<RewardCreateMemberAccountRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<RewardCreateMemberAccountRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 59:
						{
							var obj = txtJSON.Text.JsonDeserialize<RewardCreateMemberAccountRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<RewardCreateMemberAccountRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 60:
						{
							var obj = txtJSON.Text.JsonDeserialize<RewardDeleteMemberAccountRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<RewardDeleteMemberAccountRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 61:
						{
							var obj = txtJSON.Text.JsonDeserialize<RewardDeleteMemberAccountRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<RewardDeleteMemberAccountRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 62:
						{
							var obj = txtJSON.Text.JsonDeserialize<RewardGetMemberAccountsRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<RewardGetMemberAccountsRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 63:
						{
							var obj = txtJSON.Text.JsonDeserialize<RewardGetMemberAccountsRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<RewardGetMemberAccountsRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 64:
						{
							var obj = txtJSON.Text.JsonDeserialize<ZTripCreditActivateMemberCreditRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<ZTripCreditActivateMemberCreditRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 65:
						{
							var obj = txtJSON.Text.JsonDeserialize<ZTripCreditActivateMemberCreditRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<ZTripCreditActivateMemberCreditRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 66:
						{
							var obj = txtJSON.Text.JsonDeserialize<ZTripCreditGetMemberBalanceRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<ZTripCreditGetMemberBalanceRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 67:
						{
							var obj = txtJSON.Text.JsonDeserialize<ZTripCreditGetMemberBalanceRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<ZTripCreditGetMemberBalanceRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 68:
						{
							var obj = txtJSON.Text.JsonDeserialize<GroundGetASAPRequestStatusRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<GroundGetASAPRequestStatusRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 69:
						{
							var obj = txtJSON.Text.JsonDeserialize<GroundGetASAPRequestStatusRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<GroundGetASAPRequestStatusRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 70:
						{
							var obj = txtJSON.Text.JsonDeserialize<GroundAbandonASAPRequestRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<GroundAbandonASAPRequestRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 71:
						{
							var obj = txtJSON.Text.JsonDeserialize<GroundAbandonASAPRequestRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<GroundAbandonASAPRequestRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 72:
						{
							var obj = txtJSON.Text.JsonDeserialize<GetFareDetailRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<GetFareDetailRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 73:
						{
							var obj = txtJSON.Text.JsonDeserialize<GetFareDetailRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<GetFareDetailRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 74:
						{
							var obj = txtJSON.Text.JsonDeserialize<RateSetRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<RateSetRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 75:
						{
							var obj = txtJSON.Text.JsonDeserialize<RateSetRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<RateSetRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 76:
						{
							var obj = txtJSON.Text.JsonDeserialize<RateGetRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<RateGetRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 77:
						{
							var obj = txtJSON.Text.JsonDeserialize<RateGetRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<RateGetRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 78:
						{
							var obj = txtJSON.Text.JsonDeserialize<UtilityGetAirportNameRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<UtilityGetAirportNameRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 79:
						{
							var obj = txtJSON.Text.JsonDeserialize<UtilityGetAirportNameRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<UtilityGetAirportNameRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 80:
						{
							var obj = txtJSON.Text.JsonDeserialize<UtilityAvailableFleetsRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<UtilityAvailableFleetsRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 81:
						{
							var obj = txtJSON.Text.JsonDeserialize<UtilityAvailableFleetsRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<UtilityAvailableFleetsRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 82:
						{
							var obj = txtJSON.Text.JsonDeserialize<UtilityGetAirlinesByAirportRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<UtilityGetAirlinesByAirportRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 83:
						{
							var obj = txtJSON.Text.JsonDeserialize<UtilityGetAirlinesByAirportRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<UtilityGetAirlinesByAirportRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 84:
						{
							var obj = txtJSON.Text.JsonDeserialize<MemberRequestEmailConfirmationRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<MemberRequestEmailConfirmationRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 85:
						{
							var obj = txtJSON.Text.JsonDeserialize<MemberRequestEmailConfirmationRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<MemberRequestEmailConfirmationRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 86:
						{
							var obj = txtJSON.Text.JsonDeserialize<RewardSetDefaultAccountRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<RewardSetDefaultAccountRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 87:
						{
							var obj = txtJSON.Text.JsonDeserialize<RewardSetDefaultAccountRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<RewardSetDefaultAccountRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 88:
						{
							var obj = txtJSON.Text.JsonDeserialize<MemberGetLatestUnRatedTripRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<MemberGetLatestUnRatedTripRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 89:
						{
							var obj = txtJSON.Text.JsonDeserialize<MemberGetLatestUnRatedTripRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<MemberGetLatestUnRatedTripRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 90:
						{
							var obj = txtJSON.Text.JsonDeserialize<UtilityGetTripListRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<UtilityGetTripListRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 91:
						{
							var obj = txtJSON.Text.JsonDeserialize<UtilityGetTripListRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<UtilityGetTripListRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 92:
						{
							var obj = txtJSON.Text.JsonDeserialize<UtilityGetAirportInstructionsRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<UtilityGetAirportInstructionsRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 93:
						{
							var obj = txtJSON.Text.JsonDeserialize<UtilityGetAirportInstructionsRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<UtilityGetAirportInstructionsRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 94:
						{
							var obj = txtJSON.Text.JsonDeserialize<GroupValidateDiscountCodeRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<GroupValidateDiscountCodeRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 95:
						{
							var obj = txtJSON.Text.JsonDeserialize<GroupValidateDiscountCodeRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<GroupValidateDiscountCodeRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 96:
						{
							var obj = txtJSON.Text.JsonDeserialize<GroupGetDiscountRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<GroupGetDiscountRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 97:
						{
							var obj = txtJSON.Text.JsonDeserialize<GroupGetDiscountRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<GroupGetDiscountRS>().CleanComment().CleanNamespace().ToString();
						}
						break;

					case 98:
						{
							var obj = txtJSON.Text.JsonDeserialize<GroupGetDefaultsRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<GroupGetDefaultsRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 99:
						{
							var obj = txtJSON.Text.JsonDeserialize<GroupGetDefaultsRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<GroupGetDefaultsRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 100:
						{
							var obj = txtJSON.Text.JsonDeserialize<GroupGetLandmarksRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<GroupGetLandmarksRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 101:
						{
							var obj = txtJSON.Text.JsonDeserialize<GroupGetLandmarksRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<GroupGetLandmarksRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 102:
						{
							var obj = txtJSON.Text.JsonDeserialize<GroupGetLocalizedServicedAirportsRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<GroupGetLocalizedServicedAirportsRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 103:
						{
							var obj = txtJSON.Text.JsonDeserialize<GroupGetLocalizedServicedAirportsRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<GroupGetLocalizedServicedAirportsRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 104:
						{
							var obj = txtJSON.Text.JsonDeserialize<MemberGetPasswordRequestRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<MemberGetPasswordRequestRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 105:
						{
							var obj = txtJSON.Text.JsonDeserialize<MemberGetPasswordRequestRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<MemberGetPasswordRequestRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 106:
						{
							var obj = txtJSON.Text.JsonDeserialize<GroupGetPromotionTypeRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<GroupGetPromotionTypeRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 107:
						{
							var obj = txtJSON.Text.JsonDeserialize<GroupGetPromotionTypeRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<GroupGetPromotionTypeRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 108:
						{
							var obj = txtJSON.Text.JsonDeserialize<MemberResetPasswordRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<MemberResetPasswordRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 109:
						{
							var obj = txtJSON.Text.JsonDeserialize<MemberResetPasswordRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<MemberResetPasswordRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 110:
						{
							var obj = txtJSON.Text.JsonDeserialize<UtilityGetHonorificsRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<UtilityGetHonorificsRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 111:
						{
							var obj = txtJSON.Text.JsonDeserialize<UtilityGetHonorificsRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<UtilityGetHonorificsRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 112:
						{
							var obj = txtJSON.Text.JsonDeserialize<UtilityGetDestinationsRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<UtilityGetDestinationsRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 113:
						{
							var obj = txtJSON.Text.JsonDeserialize<UtilityGetDestinationsRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<UtilityGetDestinationsRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 114:
						{
							var obj = txtJSON.Text.JsonDeserialize<MemberLinkReservationRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<MemberLinkReservationRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 115:
						{
							var obj = txtJSON.Text.JsonDeserialize<MemberLinkReservationRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<MemberLinkReservationRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 116:
						{
							var obj = txtJSON.Text.JsonDeserialize<UtilityGetBookingCriteriaRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<UtilityGetBookingCriteriaRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 117:
						{
							var obj = txtJSON.Text.JsonDeserialize<UtilityGetBookingCriteriaRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<UtilityGetBookingCriteriaRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 118:
						{
							var obj = txtJSON.Text.JsonDeserialize<MemberGetCorporateProfileRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<MemberGetCorporateProfileRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 119:
						{
							var obj = txtJSON.Text.JsonDeserialize<MemberGetCorporateProfileRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<MemberGetCorporateProfileRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 120:
						{
							var obj = txtJSON.Text.JsonDeserialize<GroundGetPickupTimeRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<GroundGetPickupTimeRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 121:
						{
							var obj = txtJSON.Text.JsonDeserialize<GroundGetPickupTimeRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<GroundGetPickupTimeRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 122:
						{
							var obj = txtJSON.Text.JsonDeserialize<GroundGetFareScheduleWithFeesRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<GroundGetFareScheduleWithFeesRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 123:
						{
							var obj = txtJSON.Text.JsonDeserialize<GroundGetFareScheduleWithFeesRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<GroundGetFareScheduleWithFeesRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 124:
						{
							var obj = txtJSON.Text.JsonDeserialize<MemberDeleteRQ>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<MemberDeleteRQ>().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 125:
						{
							var obj = txtJSON.Text.JsonDeserialize<MemberDeleteRS>();//.JsonSerialize<AuthRequest>();
							result = obj.XmlSerialize<MemberDeleteRS>().CleanComment().CleanNamespace().ToString();
						}
						break;
                    case 126:
                        {
                            var obj = txtJSON.Text.JsonDeserialize<UtilityGetCorporateRequirementsRQ>();//.JsonSerialize<AuthRequest>();
                            result = obj.XmlSerialize().CleanComment().CleanNamespace().ToString();
                        }
                        break;
                    case 127:
                        {
                            var obj = txtJSON.Text.JsonDeserialize<UtilityGetCorporateRequirementsRS>();//.JsonSerialize<AuthRequest>();
                            result = obj.XmlSerialize().CleanComment().CleanNamespace().ToString();
                        }
                        break;
					case 128:
						{
							var obj = txtJSON.Text.JsonDeserialize<UtilityGetCorporateRequirementsRQ>();
							result = obj.XmlSerialize().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 129:
						{
							var obj = txtJSON.Text.JsonDeserialize<UtilityGetCorporateRequirementsRS>();
							result = obj.XmlSerialize().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 130:
						{
							var obj = txtJSON.Text.JsonDeserialize<UtilityGetAirportPickupOptionsRQ>();
							result = obj.XmlSerialize().CleanComment().CleanNamespace().ToString();
						}
						break;
					case 131:
						{
							var obj = txtJSON.Text.JsonDeserialize<UtilityGetAirportPickupOptionsRS>();
							result = obj.XmlSerialize().CleanComment().CleanNamespace().ToString();
						}
						break;

                    case 132:
                        {
                            var obj = txtJSON.Text.JsonDeserialize<UtilityGetLeadTimeRS>();
                            result = obj.XmlSerialize().CleanComment().CleanNamespace().ToString();
                        }
                        break;

                    case 133:
                        {
                            var obj = txtJSON.Text.JsonDeserialize<MemberRequestEmailCancellationRQ>();//.JsonSerialize<AuthRequest>();
                            result = obj.XmlSerialize<MemberRequestEmailCancellationRQ>().CleanComment().CleanNamespace().ToString();
                        }
                        break;
                    case 134:
                        {
                            var obj = txtJSON.Text.JsonDeserialize<MemberRequestEmailCancellationRS>();//.JsonSerialize<AuthRequest>();
                            result = obj.XmlSerialize<MemberRequestEmailCancellationRS>().CleanComment().CleanNamespace().ToString();
                        }
                        break;
                    case 135:
                        {
                            var obj = txtJSON.Text.JsonDeserialize<SplitPaymentInviteRQ>();
                            result = obj.XmlSerialize().CleanComment().CleanNamespace().ToString();
                        }
                        break;
                    case 136:
                        {
                            var obj = txtJSON.Text.JsonDeserialize<SplitPaymentInviteRS>();
                            result = obj.XmlSerialize().CleanComment().CleanNamespace().ToString();
                        }
                        break;
                    case 137:
                        {
                            var obj = txtJSON.Text.JsonDeserialize<SplitPaymentUpdateStatusRQ>();
                            result = obj.XmlSerialize().CleanComment().CleanNamespace().ToString();
                        }
                        break;
                    case 138:
                        {
                            var obj = txtJSON.Text.JsonDeserialize<SplitPaymentUpdateStatusRS>();
                            result = obj.XmlSerialize().CleanComment().CleanNamespace().ToString();
                        }
                        break;
                    case 139:
                        {
                            var obj = txtJSON.Text.JsonDeserialize<SplitPaymentGetInvitationsRQ>();
                            result = obj.XmlSerialize().CleanComment().CleanNamespace().ToString();
                        }
                        break;
                    case 140:
                        {
                            var obj = txtJSON.Text.JsonDeserialize<SplitPaymentGetInvitationsRS>();
                            result = obj.XmlSerialize().CleanComment().CleanNamespace().ToString();
                        }
                        break;
                    case 141:
                        {
                            var obj = txtJSON.Text.JsonDeserialize<SplitPaymentCheckIfEnabledRQ>();
                            result = obj.XmlSerialize().CleanComment().CleanNamespace().ToString();
                        }
                        break;
                    case 142:
                        {
                            var obj = txtJSON.Text.JsonDeserialize<SplitPaymentCheckIfEnabledRS>();
                            result = obj.XmlSerialize().CleanComment().CleanNamespace().ToString();
                        }
                        break;

                    case 143:
                        {
                            var obj = txtJSON.Text.JsonDeserialize<UtilityGetClientTokenRS>();
                            result = obj.XmlSerialize().CleanComment().CleanNamespace().ToString();
                        }
                        break;
                }
			}
			catch (Exception ex)
			{
				result = ex.Message;
			}

			txtXML.Text = result;
		}
	}
}
