﻿namespace UDI.VTOD.Utilities.UI
{
	partial class InsertFleetServiceAreaPolygon
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label3 = new System.Windows.Forms.Label();
			this.txtPolygonGpx = new System.Windows.Forms.TextBox();
			this.btnInsert = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.comboBoxPolygonType = new System.Windows.Forms.ComboBox();
			this.SuspendLayout();
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(9, 37);
			this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(67, 13);
			this.label3.TabIndex = 4;
			this.label3.Text = "Polygon Gpx";
			// 
			// txtPolygonGpx
			// 
			this.txtPolygonGpx.Location = new System.Drawing.Point(99, 37);
			this.txtPolygonGpx.Margin = new System.Windows.Forms.Padding(2);
			this.txtPolygonGpx.MaxLength = 65536;
			this.txtPolygonGpx.Multiline = true;
			this.txtPolygonGpx.Name = "txtPolygonGpx";
			this.txtPolygonGpx.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.txtPolygonGpx.Size = new System.Drawing.Size(525, 356);
			this.txtPolygonGpx.TabIndex = 5;
			// 
			// btnInsert
			// 
			this.btnInsert.Location = new System.Drawing.Point(99, 396);
			this.btnInsert.Margin = new System.Windows.Forms.Padding(2);
			this.btnInsert.Name = "btnInsert";
			this.btnInsert.Size = new System.Drawing.Size(94, 30);
			this.btnInsert.TabIndex = 6;
			this.btnInsert.Text = "Insert";
			this.btnInsert.UseVisualStyleBackColor = true;
			this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(9, 10);
			this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(75, 13);
			this.label2.TabIndex = 7;
			this.label2.Text = "Polygon Type:";
			// 
			// comboBoxPolygonType
			// 
			this.comboBoxPolygonType.FormattingEnabled = true;
			this.comboBoxPolygonType.Items.AddRange(new object[] {
            "Airport",
            "Area",
            "Hotel"});
			this.comboBoxPolygonType.Location = new System.Drawing.Point(100, 9);
			this.comboBoxPolygonType.Name = "comboBoxPolygonType";
			this.comboBoxPolygonType.Size = new System.Drawing.Size(245, 21);
			this.comboBoxPolygonType.TabIndex = 9;
			// 
			// InsertFleetServiceAreaPolygon
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(645, 436);
			this.Controls.Add(this.comboBoxPolygonType);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.btnInsert);
			this.Controls.Add(this.txtPolygonGpx);
			this.Controls.Add(this.label3);
			this.Margin = new System.Windows.Forms.Padding(2);
			this.MaximizeBox = false;
			this.Name = "InsertFleetServiceAreaPolygon";
			this.Text = "InsertFleetServiceAreaPolygon";
			this.Load += new System.EventHandler(this.InsertFleetServiceAreaPolygon_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox txtPolygonGpx;
		private System.Windows.Forms.Button btnInsert;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxPolygonType;
	}
}