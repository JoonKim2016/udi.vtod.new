﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

using UDI.VTOD.DataAccess.VTOD;
using UDI.Utility.Serialization;
using UDI.Map.DTO.XML;
using UDI.Utility.Helper;

namespace UDI.VTOD.Utilities.UI
{
    public partial class InsertFleetServiceAreaPolygon : Form
    {
        public InsertFleetServiceAreaPolygon()
        {
            InitializeComponent();
        }

        private void InsertFleetServiceAreaPolygon_Load(object sender, EventArgs e)
        {
            init();
        }

        #region private
        private void init()
        {
            //loadFleet();
        }

        //private void loadFleet()
        //{
        //	//MessageBox.Show("Hello");
        //	var fleets = new List<taxi_fleet>();
        //	using (var db = new VTODEntities())
        //	{
        //		fleets = db.taxi_fleet.ToList();
        //	}

        //	cmbFleet.DataSource = fleets;
        //	cmbFleet.DisplayMember = "Name";
        //	cmbFleet.ValueMember = "Id";
        //}
        #endregion

        #region Events
        private void btnInsert_Click(object sender, EventArgs e)
        {
            string succ = "";
            try
            {
                //txtResult.Text = string.Empty;
                //lblFeeltID.Text = "FleetID:";
                if ((!string.IsNullOrWhiteSpace(txtPolygonGpx.Text)) || (!string.IsNullOrWhiteSpace(comboBoxPolygonType.Items[this.comboBoxPolygonType.SelectedIndex].ToString())))
                {
                    var gpxDoc = XDocument.Parse(txtPolygonGpx.Text);

                    //var mapService = new Map.MapService();
                    //var lat = txtPolygonName.Text.Split(',').First().Trim().ToDecimal();
                    //var lon = txtPolygonName.Text.Split(',').Last().Trim().ToDecimal();
                    //var result = mapService.FindZone(new Map.DTO.Geolocation { Latitude = lat, Longitude = lon}, gpxDoc);

                    var polygonName = string.Empty;
                    var polygonDetails = string.Empty;
                    var polygonString = string.Empty;
                    var polygonType = comboBoxPolygonType.Items[this.comboBoxPolygonType.SelectedIndex].ToString();
                    var lat = 0.0M;
                    var lon = 0.0M;

                    //long fleetId = cmbFleet.SelectedValue.ToInt64();


                    var gpxObj = gpxDoc.CleanComment().CleanNamespace().XmlDeserialize<gpx>();

                    if (gpxObj != null && gpxObj.rteList != null && gpxObj.rteList.Any())
                    {
                        succ = "Inserted - ";

                        foreach (rte gp in gpxObj.rteList)
                        {
                            try
                            {
                                var result = getPolygon(gp, out polygonString, out lat, out lon, out polygonName, out polygonDetails);

                                if (string.IsNullOrEmpty(result) && !string.IsNullOrWhiteSpace(polygonString) && !string.IsNullOrWhiteSpace(polygonName))
                                {
                                    using (var db = new DataAccess.VTOD.VTODEntities())
                                    {
                                        db.SP_vtod_InsertPolygon(polygonName, polygonDetails, polygonType, lat, lon, polygonString);
                                    }

                                    succ = succ + Environment.NewLine + polygonName;
                                }
                                else
                                {
                                    MessageBox.Show(string.Format("Error: {0}: \n\r PolygonString: {1}", result, polygonString));
                                }
                            }
                            catch (Exception ex) { MessageBox.Show(string.Format("Error: {0}: \n\r PolygonString: {1}", ex.Message, polygonString)); }


                        }

                        MessageBox.Show(succ);
                    }

                }
                else
                {
                    MessageBox.Show(string.Format("Please fill boxes"));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #endregion

        #region private
        private string getPolygon(rte rteObj, out string gpxString, out decimal latitude, out decimal longitude, out string name, out string details)
        {
            var result = string.Empty;
            gpxString = string.Empty;
            latitude = 0;
            longitude = 0;
            name = "";
            details = "";

            try
            {

                if (rteObj != null)
                {
                    #region Extract Name and Details
					name = rteObj.name.Trim();

					#region For Airport
					//name = rteObj.name.Trim().Substring(0, 3);

					//try
					//{
					//	details = rteObj.name.Trim().Substring(rteObj.name.Trim().IndexOf('(')).Replace("(", "").Replace(")", "");
					//}
					//catch
					//{} 
					#endregion

                    #endregion

                    #region Extract LAT and LON

                    latitude = rteObj.desc.Split(',').First().Trim().ToDecimal();
                    longitude = rteObj.desc.Split(',').Last().Trim().ToDecimal();

                    #endregion

                    #region Make PolygonString for DB
                    //'Polygon((0 0,0 3,3 0,0 0),(1 1,1 2,2 1,1 1))'

                    var sb = new StringBuilder();
                    sb.Append("Polygon((");

                    if (!(sb.ToString().EndsWith(",") || sb.ToString().EndsWith("(")))
                    {
                        sb.Append(",(");
                    }

                    foreach (var rtept in rteObj.rteptList)
                    {
                        if (!(sb.ToString().EndsWith(",") || sb.ToString().EndsWith("(")))
                        {
                            sb.Append(",");
                        }

                        sb.AppendFormat("{0} {1}", rtept.lat, rtept.lon);
                    }

                    sb.Append(")");

                    sb.Append(")");

                    gpxString = sb.ToString();
                    #endregion
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }


            return result;
        }
        #endregion


    }
}
