﻿namespace UDI.VTOD.Utilities.UI
{
	partial class LoadTesterForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtUrlRequest = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnRequest = new System.Windows.Forms.Button();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.rbtnPost = new System.Windows.Forms.RadioButton();
            this.rbtnGet = new System.Windows.Forms.RadioButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.rbtnXML = new System.Windows.Forms.RadioButton();
            this.rbtnJSON = new System.Windows.Forms.RadioButton();
            this.cmbLoadRequest = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.txtThreadNumber = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtRequestHeader = new System.Windows.Forms.RichTextBox();
            this.txtRequest = new System.Windows.Forms.RichTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lblGUID = new System.Windows.Forms.Label();
            this.txtResult = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.lblTime = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.txtUrlRequest);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.btnRequest);
            this.panel1.Controls.Add(this.btnBrowse);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.cmbLoadRequest);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(9, 10);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1094, 71);
            this.panel1.TabIndex = 0;
            // 
            // txtUrlRequest
            // 
            this.txtUrlRequest.Location = new System.Drawing.Point(44, 44);
            this.txtUrlRequest.Margin = new System.Windows.Forms.Padding(2);
            this.txtUrlRequest.Name = "txtUrlRequest";
            this.txtUrlRequest.Size = new System.Drawing.Size(501, 20);
            this.txtUrlRequest.TabIndex = 23;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 46);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "URL:";
            // 
            // btnRequest
            // 
            this.btnRequest.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold);
            this.btnRequest.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnRequest.Location = new System.Drawing.Point(890, 34);
            this.btnRequest.Margin = new System.Windows.Forms.Padding(2);
            this.btnRequest.Name = "btnRequest";
            this.btnRequest.Size = new System.Drawing.Size(192, 26);
            this.btnRequest.TabIndex = 20;
            this.btnRequest.Text = "Submit Request";
            this.btnRequest.UseVisualStyleBackColor = true;
            this.btnRequest.Click += new System.EventHandler(this.btnRequest_Click);
            // 
            // btnBrowse
            // 
            this.btnBrowse.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnBrowse.Location = new System.Drawing.Point(890, 9);
            this.btnBrowse.Margin = new System.Windows.Forms.Padding(2);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnBrowse.Size = new System.Drawing.Size(192, 22);
            this.btnBrowse.TabIndex = 19;
            this.btnBrowse.Text = "Load Request";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.rbtnPost);
            this.panel3.Controls.Add(this.rbtnGet);
            this.panel3.Location = new System.Drawing.Point(570, 30);
            this.panel3.Margin = new System.Windows.Forms.Padding(2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(155, 30);
            this.panel3.TabIndex = 18;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 6);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Method:";
            // 
            // rbtnPost
            // 
            this.rbtnPost.AutoSize = true;
            this.rbtnPost.Checked = true;
            this.rbtnPost.Location = new System.Drawing.Point(52, 6);
            this.rbtnPost.Margin = new System.Windows.Forms.Padding(2);
            this.rbtnPost.Name = "rbtnPost";
            this.rbtnPost.Size = new System.Drawing.Size(54, 17);
            this.rbtnPost.TabIndex = 1;
            this.rbtnPost.TabStop = true;
            this.rbtnPost.Text = "POST";
            this.rbtnPost.UseVisualStyleBackColor = true;
            // 
            // rbtnGet
            // 
            this.rbtnGet.AutoSize = true;
            this.rbtnGet.Location = new System.Drawing.Point(107, 6);
            this.rbtnGet.Margin = new System.Windows.Forms.Padding(2);
            this.rbtnGet.Name = "rbtnGet";
            this.rbtnGet.Size = new System.Drawing.Size(47, 17);
            this.rbtnGet.TabIndex = 2;
            this.rbtnGet.Text = "GET";
            this.rbtnGet.UseVisualStyleBackColor = true;
            this.rbtnGet.CheckedChanged += new System.EventHandler(this.rbtnGet_CheckedChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.rbtnXML);
            this.panel2.Controls.Add(this.rbtnJSON);
            this.panel2.Location = new System.Drawing.Point(730, 33);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(155, 24);
            this.panel2.TabIndex = 17;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 3);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "Media:";
            // 
            // rbtnXML
            // 
            this.rbtnXML.AutoSize = true;
            this.rbtnXML.Checked = true;
            this.rbtnXML.Location = new System.Drawing.Point(52, 3);
            this.rbtnXML.Margin = new System.Windows.Forms.Padding(2);
            this.rbtnXML.Name = "rbtnXML";
            this.rbtnXML.Size = new System.Drawing.Size(47, 17);
            this.rbtnXML.TabIndex = 14;
            this.rbtnXML.TabStop = true;
            this.rbtnXML.Text = "XML";
            this.rbtnXML.UseVisualStyleBackColor = true;
            this.rbtnXML.CheckedChanged += new System.EventHandler(this.rbtnXML_CheckedChanged);
            // 
            // rbtnJSON
            // 
            this.rbtnJSON.AutoSize = true;
            this.rbtnJSON.Location = new System.Drawing.Point(101, 3);
            this.rbtnJSON.Margin = new System.Windows.Forms.Padding(2);
            this.rbtnJSON.Name = "rbtnJSON";
            this.rbtnJSON.Size = new System.Drawing.Size(53, 17);
            this.rbtnJSON.TabIndex = 15;
            this.rbtnJSON.Text = "JSON";
            this.rbtnJSON.UseVisualStyleBackColor = true;
            this.rbtnJSON.CheckedChanged += new System.EventHandler(this.rbtnJSON_CheckedChanged);
            // 
            // cmbLoadRequest
            // 
            this.cmbLoadRequest.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLoadRequest.FormattingEnabled = true;
            this.cmbLoadRequest.Items.AddRange(new object[] {
            "SDS Token request - XML",
            "SDS Token request - JSON",
            "----",
            "GroundAvail request - XML",
            "GroundAvail request - JSON",
            "----",
            "GroundBook request - XML",
            "GroundBook request - JSON",
            "----",
            "GroundResRetrieve request - XML",
            "GroundResRetrieve request - JSON",
            "----",
            "Cancel request - XML",
            "Cancel request - JSON"});
            this.cmbLoadRequest.Location = new System.Drawing.Point(568, 9);
            this.cmbLoadRequest.Margin = new System.Windows.Forms.Padding(2);
            this.cmbLoadRequest.Name = "cmbLoadRequest";
            this.cmbLoadRequest.Size = new System.Drawing.Size(318, 21);
            this.cmbLoadRequest.TabIndex = 13;
            this.cmbLoadRequest.SelectedIndexChanged += new System.EventHandler(this.cmbLoadRequest_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(287, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Unified Disptach - VTOD api tester";
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.txtThreadNumber);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.txtRequestHeader);
            this.panel4.Controls.Add(this.txtRequest);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Location = new System.Drawing.Point(9, 85);
            this.panel4.Margin = new System.Windows.Forms.Padding(2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(545, 517);
            this.panel4.TabIndex = 1;
            // 
            // txtThreadNumber
            // 
            this.txtThreadNumber.Location = new System.Drawing.Point(101, 35);
            this.txtThreadNumber.Margin = new System.Windows.Forms.Padding(2);
            this.txtThreadNumber.Name = "txtThreadNumber";
            this.txtThreadNumber.Size = new System.Drawing.Size(55, 20);
            this.txtThreadNumber.TabIndex = 20;
            this.txtThreadNumber.Text = "1";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(13, 35);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(84, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "Thread Number:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F);
            this.label8.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label8.Location = new System.Drawing.Point(13, 120);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(31, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Body";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F);
            this.label7.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label7.Location = new System.Drawing.Point(13, 54);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "Header";
            // 
            // txtRequestHeader
            // 
            this.txtRequestHeader.Font = new System.Drawing.Font("Consolas", 7.8F);
            this.txtRequestHeader.Location = new System.Drawing.Point(12, 80);
            this.txtRequestHeader.Margin = new System.Windows.Forms.Padding(2);
            this.txtRequestHeader.Name = "txtRequestHeader";
            this.txtRequestHeader.Size = new System.Drawing.Size(517, 39);
            this.txtRequestHeader.TabIndex = 16;
            this.txtRequestHeader.Text = "";
            // 
            // txtRequest
            // 
            this.txtRequest.Font = new System.Drawing.Font("Consolas", 7.8F);
            this.txtRequest.Location = new System.Drawing.Point(12, 136);
            this.txtRequest.Margin = new System.Windows.Forms.Padding(2);
            this.txtRequest.Name = "txtRequest";
            this.txtRequest.Size = new System.Drawing.Size(516, 366);
            this.txtRequest.TabIndex = 15;
            this.txtRequest.Text = "";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold);
            this.label5.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label5.Location = new System.Drawing.Point(5, 9);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Request:";
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.lblGUID);
            this.panel5.Controls.Add(this.txtResult);
            this.panel5.Controls.Add(this.label9);
            this.panel5.Controls.Add(this.lblTime);
            this.panel5.Controls.Add(this.label6);
            this.panel5.Location = new System.Drawing.Point(568, 85);
            this.panel5.Margin = new System.Windows.Forms.Padding(2);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(535, 517);
            this.panel5.TabIndex = 2;
            // 
            // lblGUID
            // 
            this.lblGUID.AutoSize = true;
            this.lblGUID.Location = new System.Drawing.Point(81, 9);
            this.lblGUID.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGUID.Name = "lblGUID";
            this.lblGUID.Size = new System.Drawing.Size(10, 13);
            this.lblGUID.TabIndex = 20;
            this.lblGUID.Text = "-";
            // 
            // txtResult
            // 
            this.txtResult.Location = new System.Drawing.Point(9, 51);
            this.txtResult.Margin = new System.Windows.Forms.Padding(2);
            this.txtResult.Multiline = true;
            this.txtResult.Name = "txtResult";
            this.txtResult.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtResult.Size = new System.Drawing.Size(515, 462);
            this.txtResult.TabIndex = 19;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(12, 26);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 13);
            this.label9.TabIndex = 18;
            this.label9.Text = "Time:";
            // 
            // lblTime
            // 
            this.lblTime.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblTime.AutoSize = true;
            this.lblTime.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblTime.Location = new System.Drawing.Point(52, 26);
            this.lblTime.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTime.Name = "lblTime";
            this.lblTime.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblTime.Size = new System.Drawing.Size(10, 13);
            this.lblTime.TabIndex = 17;
            this.lblTime.Text = "-";
            this.lblTime.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold);
            this.label6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label6.Location = new System.Drawing.Point(12, 9);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Response:";
            // 
            // LoadTesterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1111, 611);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "LoadTesterForm";
            this.Text = "TesterForm";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.RadioButton rbtnGet;
		private System.Windows.Forms.RadioButton rbtnPost;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ComboBox cmbLoadRequest;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.RadioButton rbtnJSON;
		private System.Windows.Forms.RadioButton rbtnXML;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Button btnRequest;
		private System.Windows.Forms.Button btnBrowse;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.RichTextBox txtRequestHeader;
		private System.Windows.Forms.RichTextBox txtRequest;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Panel panel5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox txtUrlRequest;
		private System.Windows.Forms.Label lblTime;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox txtThreadNumber;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.TextBox txtResult;
		private System.Windows.Forms.Label lblGUID;
	}
}