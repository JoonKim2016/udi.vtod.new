﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace UDI.VTOD.Utilities.UI
{
    public class MTDataHostedOrganisationsRS
    {

        public GetHostedOrganisations GetHostedOrganisationsResult { get; set; }
    }

    public class GetHostedOrganisations
    {
        
        public string Error { get; set; }
        public string ErrorMessage { get; set; }
        public string Succeeded { get; set; }
        public HostedOrganisation HostedOrganisations { get; set; }
    }
    public class HostedOrganisation
    {
        public HostedOrganisationList HostedOrganisationInfo { get; set; }
    }
    public class HostedOrganisationList
    {
        public string Identifier { get; set; }
        public string Name { get; set; }
        public string AccountManager { get; set; }
        public string PrimaryContactName { get; set; }
        public string PrimaryContactPhone { get; set; }
        public string OutOfZoneMessage { get; set; }
        public string Options { get; set; }
        public BookingZonesList BookingZones { get; set; }
    }
    public class BookingZonesList
    {
        public HostedBookingZoneList HostedBookingZoneInfo { get; set; }
    }
    public class HostedBookingZoneList
     {
        public string Identifier { get; set; }
        public string Name { get; set; }
        public string ZOrder { get; set; }
        public string CityID { get; set; }
        public string CityName { get; set; }
        public string FleetID { get; set; }
        public string FleetName { get; set; }
        public string ServiceID { get; set; }
        public string ProductID { get; set; }
       public PolygonList Polygon { get; set; }
     }
    public class PolygonList
    {
        public PointsList Points { get; set; }
    }
    public class PointsList
    {
        public List<GEOLocationList> GEOLocation { get; set; }
    }
    public class GEOLocationList
    {
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}
