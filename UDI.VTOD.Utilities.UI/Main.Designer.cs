﻿namespace UDI.VTOD.Utilities.UI
{
	partial class Main
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnFleetServiceAreaPolygon = new System.Windows.Forms.Button();
			this.btnTester = new System.Windows.Forms.Button();
			this.btnLoadTester = new System.Windows.Forms.Button();
			this.btnConverter = new System.Windows.Forms.Button();
			this.btnCompressor = new System.Windows.Forms.Button();
			this.btnConvertPolygon = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// btnFleetServiceAreaPolygon
			// 
			this.btnFleetServiceAreaPolygon.Location = new System.Drawing.Point(9, 136);
			this.btnFleetServiceAreaPolygon.Margin = new System.Windows.Forms.Padding(2);
			this.btnFleetServiceAreaPolygon.Name = "btnFleetServiceAreaPolygon";
			this.btnFleetServiceAreaPolygon.Size = new System.Drawing.Size(281, 38);
			this.btnFleetServiceAreaPolygon.TabIndex = 0;
			this.btnFleetServiceAreaPolygon.Text = "Insert Fleet Service Area Polygon";
			this.btnFleetServiceAreaPolygon.UseVisualStyleBackColor = true;
			this.btnFleetServiceAreaPolygon.Click += new System.EventHandler(this.btnFleetServiceAreaPolygon_Click);
			// 
			// btnTester
			// 
			this.btnTester.Location = new System.Drawing.Point(9, 10);
			this.btnTester.Margin = new System.Windows.Forms.Padding(2);
			this.btnTester.Name = "btnTester";
			this.btnTester.Size = new System.Drawing.Size(280, 34);
			this.btnTester.TabIndex = 1;
			this.btnTester.Text = "Tester";
			this.btnTester.UseVisualStyleBackColor = true;
			this.btnTester.Click += new System.EventHandler(this.btnTester_Click);
			// 
			// btnLoadTester
			// 
			this.btnLoadTester.Location = new System.Drawing.Point(9, 49);
			this.btnLoadTester.Margin = new System.Windows.Forms.Padding(2);
			this.btnLoadTester.Name = "btnLoadTester";
			this.btnLoadTester.Size = new System.Drawing.Size(280, 38);
			this.btnLoadTester.TabIndex = 2;
			this.btnLoadTester.Text = "Load Tester";
			this.btnLoadTester.UseVisualStyleBackColor = true;
			this.btnLoadTester.Click += new System.EventHandler(this.btnLoadTester_Click);
			// 
			// btnConverter
			// 
			this.btnConverter.Location = new System.Drawing.Point(9, 92);
			this.btnConverter.Margin = new System.Windows.Forms.Padding(2);
			this.btnConverter.Name = "btnConverter";
			this.btnConverter.Size = new System.Drawing.Size(280, 39);
			this.btnConverter.TabIndex = 3;
			this.btnConverter.Text = "Converter";
			this.btnConverter.UseVisualStyleBackColor = true;
			this.btnConverter.Click += new System.EventHandler(this.btnConverter_Click);
			// 
			// btnCompressor
			// 
			this.btnCompressor.Location = new System.Drawing.Point(8, 179);
			this.btnCompressor.Margin = new System.Windows.Forms.Padding(2);
			this.btnCompressor.Name = "btnCompressor";
			this.btnCompressor.Size = new System.Drawing.Size(281, 38);
			this.btnCompressor.TabIndex = 4;
			this.btnCompressor.Text = "Compressor";
			this.btnCompressor.UseVisualStyleBackColor = true;
			this.btnCompressor.Click += new System.EventHandler(this.btnCompressor_Click);
			// 
			// btnConvertPolygon
			// 
			this.btnConvertPolygon.Location = new System.Drawing.Point(8, 221);
			this.btnConvertPolygon.Margin = new System.Windows.Forms.Padding(2);
			this.btnConvertPolygon.Name = "btnConvertPolygon";
			this.btnConvertPolygon.Size = new System.Drawing.Size(281, 38);
			this.btnConvertPolygon.TabIndex = 5;
			this.btnConvertPolygon.Text = "Convert To Polygon [From MTData]";
			this.btnConvertPolygon.UseVisualStyleBackColor = true;
			this.btnConvertPolygon.Click += new System.EventHandler(this.btnConvertPolygon_Click);
			// 
			// Main
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(304, 266);
			this.Controls.Add(this.btnConvertPolygon);
			this.Controls.Add(this.btnCompressor);
			this.Controls.Add(this.btnConverter);
			this.Controls.Add(this.btnLoadTester);
			this.Controls.Add(this.btnTester);
			this.Controls.Add(this.btnFleetServiceAreaPolygon);
			this.Margin = new System.Windows.Forms.Padding(2);
			this.MaximizeBox = false;
			this.Name = "Main";
			this.Text = "Main";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button btnFleetServiceAreaPolygon;
		private System.Windows.Forms.Button btnTester;
		private System.Windows.Forms.Button btnLoadTester;
		private System.Windows.Forms.Button btnConverter;
		private System.Windows.Forms.Button btnCompressor;
        private System.Windows.Forms.Button btnConvertPolygon;
	}
}

