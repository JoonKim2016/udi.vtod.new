﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UDI.VTOD.Utilities.UI
{
	public partial class Main : Form
	{
		public Main()
		{
			InitializeComponent();
		}

		private void btnFleetServiceAreaPolygon_Click(object sender, EventArgs e)
		{
			var frm = new InsertFleetServiceAreaPolygon();
			frm.Show();
		}

		private void btnTester_Click(object sender, EventArgs e)
		{
			var frm = new TesterForm();
			frm.Show();
		}

		private void btnLoadTester_Click(object sender, EventArgs e)
		{
			var frm = new LoadTesterForm();
			frm.Show();
		}

		private void btnConverter_Click(object sender, EventArgs e)
		{
			var frm = new ConverterForm();
			frm.Show();
		}

		private void btnCompressor_Click(object sender, EventArgs e)
		{
			var frm = new CompressorForm();
			frm.Show();
		}

        private void btnConvertPolygon_Click(object sender, EventArgs e)
        {
            var frm = new ConvertPolygonForm();
            frm.Show();
        }
	}
}
