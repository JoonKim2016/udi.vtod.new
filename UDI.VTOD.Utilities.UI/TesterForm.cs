﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

using Newtonsoft.Json.Linq;
using UDI.Utility.Helper;

namespace UDI.VTOD.Utilities.UI
{

	public partial class TesterForm : Form
	{
		#region Constructors
		public TesterForm()
		{
			InitializeComponent();
			cmbLoadRequest.SelectedIndex = 0;
		}
		#endregion

		private bool gotURLs = false;

		private void btnBrowse_Click(object sender, EventArgs e)
		{
			loadTempaltesFromURL();
		}

		private Dictionary<string, string> GetToken()
		{
			Dictionary<string, string> token = new Dictionary<string, string>();

			try
			{
				string status;
				var response = string.Empty;

			    //var test33 = ConfigurationManager.AppSettings["FleetType"];
			    //var test44 = ConfigurationManager.AppSettings["EchoToken"];
			    //var test66 = ConfigurationManager.AppSettings["Target"];
       //            var test77 = ConfigurationManager.AppSettings["Version"];
			    //var test222 = ConfigurationManager.AppSettings["PrimaryLangID"]; 
       //           var test22 = ConfigurationManager.AppSettings["Username"];
			    //var test2 = ConfigurationManager.AppSettings["Password"];

				var headers = new Dictionary<string, string>();
				response = UDI.Utility.Helper.WebRequestHelper.ProcessWebRequest(
						ConfigurationManager.AppSettings["URL"] + "GetToken?media=xml",
						string.Format(@"<?xml version=""1.0"" encoding=""UTF-8""?><TokenRQ FleetType=""{0}"" EchoToken=""{1}"" Target=""{2}"" Version=""{3}"" PrimaryLangID=""{4}""><Username>{5}</Username><Password>{6}</Password></TokenRQ>",
                        ConfigurationManager.AppSettings["FleetType"],
                        ConfigurationManager.AppSettings["EchoToken"], ConfigurationManager.AppSettings["Target"],
                        ConfigurationManager.AppSettings["Version"], ConfigurationManager.AppSettings["PrimaryLangID"], 
                         ConfigurationManager.AppSettings["Username"], System.Web.HttpUtility.HtmlEncode( ConfigurationManager.AppSettings["Password"])),
						"application/xml; charset=utf-8",
						"POST",
						headers,
						out status);
				response = XDocument.Parse(response).CleanNamespace().CleanComment().ToString();
				//extract
				string username = Regex.Match(response, "<Username>(.*)</Username>").Groups[1].Value;
				string securityKey = Regex.Match(response, "<SecurityKey>(.*)</SecurityKey>").Groups[1].Value;

				token.Add("Username", username);
				token.Add("SecurityKey", securityKey);
			}
			catch (Exception ex)
			{
				txtResponse.Text = ex.Message;
			}

			return token;
		}

		private void btnRequest_Click(object sender, EventArgs e)
		{
			lblTime.Text = string.Empty;
			Stopwatch sp = new Stopwatch();
			sp.Start();

			txtResponse.Clear();
			var headers = new Dictionary<string, string>();
			var status = string.Empty;
			var contentType = string.Empty;
			var method = string.Empty;
			var response = string.Empty;

			if (rbtnXML.Checked)
				contentType = "application/xml; charset=utf-8";
				//contentType = "application/xml; charset=ASCII";
			if (rbtnJSON.Checked)
				contentType = "application/json; charset=utf-8";
				//contentType = "application/json; charset=ASCII";

			if (rbtnGet.Checked)
				method = "GET";
			else if (rbtnPost.Checked)
				method = "POST";
			else if (rbtnDelete.Checked)
				method = "DELETE";
            else if (rbtnDelete.Checked)
                method = "DELETE";
            else if (rbtnPut.Checked)
                method = "PUT";

			try
			{

				//load token automatically
				//edited by mike 20130916
				headers = GetToken();

				headers.Add("ContextCode", "101");
				headers.Add("ContextLanguageCode", "en-US");

				/*
				if (!string.IsNullOrWhiteSpace(txtRequestHeader.Text))
				{
					var header = txtRequestHeader.Text.EndsWith(";") ? txtRequestHeader.Text.Substring(0, txtRequestHeader.Text.Length - 1) : txtRequestHeader.Text;
					var headerStrings = header.Split(';');
					foreach (var item in headerStrings)
					{
						headers.Add(item.Split(':')[0], item.Split(':')[1]);
					}
				}
				*/

				response = UDI.Utility.Helper.WebRequestHelper.ProcessWebRequest(
						txtUrlRequest.Text,
						txtRequest.Text,
						contentType,
						method,
						headers,
						out status);
			}
			catch (Exception ex)
			{
				response = ex.Message;
			}

			#region Formatting
			try
			{
				if (rbtnXML.Checked)
				{
					response = XDocument.Parse(response).CleanNamespace().CleanComment().ToString();
				}
				else if (rbtnJSON.Checked)
				{
					JObject json = JObject.Parse(response);
					response = json.ToString();
				}
			}
			catch { }
			#endregion

			#region Colorizing
			AddColouredText(txtResponse, response);
			#endregion

			sp.Stop();
			lblTime.Text = string.Format("{0} milisecond", sp.ElapsedMilliseconds);
		}

		private void cmbLoadRequest_SelectedIndexChanged(object sender, EventArgs e)
		{
			GetURLs();
		}

		#region Private
		private void AddColouredText(RichTextBox textBox, string strTextToAdd)
		{
			//Use the RichTextBox to create the initial RTF code
			textBox.Clear();
			textBox.AppendText(strTextToAdd);
			string strRTF = textBox.Rtf;
			textBox.Clear();

			/* 
			 * ADD COLOUR TABLE TO THE HEADER FIRST 
			 * */

			// Search for colour table info, if it exists (which it shouldn't)
			// remove it and replace with our one
			int iCTableStart = strRTF.IndexOf("colortbl;");

			if (iCTableStart != -1) //then colortbl exists
			{
				//find end of colortbl tab by searching
				//forward from the colortbl tab itself
				int iCTableEnd = strRTF.IndexOf('}', iCTableStart);
				strRTF = strRTF.Remove(iCTableStart, iCTableEnd - iCTableStart);

				//now insert new colour table at index of old colortbl tag
				strRTF = strRTF.Insert(iCTableStart,
					// CHANGE THIS STRING TO ALTER COLOUR TABLE
					"colortbl ;\\red255\\green0\\blue0;\\red0\\green128\\blue0;\\red0\\green0\\blue255;}");
			}

			//colour table doesn't exist yet, so let's make one
			else
			{
				// find index of start of header
				int iRTFLoc = strRTF.IndexOf("\\rtf");
				// get index of where we'll insert the colour table
				// try finding opening bracket of first property of header first                
				int iInsertLoc = strRTF.IndexOf('{', iRTFLoc);

				// if there is no property, we'll insert colour table
				// just before the end bracket of the header
				if (iInsertLoc == -1) iInsertLoc = strRTF.IndexOf('}', iRTFLoc) - 1;

				// insert the colour table at our chosen location                
				strRTF = strRTF.Insert(iInsertLoc,
					// CHANGE THIS STRING TO ALTER COLOUR TABLE
					"{\\colortbl ;\\red128\\green0\\blue0;\\red0\\green128\\blue0;\\red0\\green0\\blue255;}");
			}

			/*
			 * NOW PARSE THROUGH RTF DATA, ADDING RTF COLOUR TAGS WHERE WE WANT THEM
			 * In our colour table we defined:
			 * cf1 = red  
			 * cf2 = green
			 * cf3 = blue             
			 * */

			for (int i = 0; i < strRTF.Length; i++)
			{
				if (strRTF[i] == '<')
				{
					//add RTF tags after symbol 
					//Check for comments tags 
					if (strRTF[i + 1] == '!')
						strRTF = strRTF.Insert(i + 4, "\\cf2 ");
					else
						strRTF = strRTF.Insert(i + 1, "\\cf1 ");
					//add RTF before symbol
					strRTF = strRTF.Insert(i, "\\cf3 ");

					//skip forward past the characters we've just added
					//to avoid getting trapped in the loop
					i += 6;
				}
				else if (strRTF[i] == '>')
				{
					//add RTF tags after character
					strRTF = strRTF.Insert(i + 1, "\\cf0 ");
					//Check for comments tags
					if (strRTF[i - 1] == '-')
					{
						strRTF = strRTF.Insert(i - 2, "\\cf3 ");
						//skip forward past the 6 characters we've just added
						i += 8;
					}
					else
					{
						strRTF = strRTF.Insert(i, "\\cf3 ");
						//skip forward past the 6 characters we've just added
						i += 6;
					}
				}
			}
			textBox.Rtf = strRTF;
		}

		private void GetURLs()
		{
			//bool loadTemplateFromURL = false;

			//if (bool.TryParse(ConfigurationManager.AppSettings["LoadTemplateFromURL"], out loadTemplateFromURL))
			//{
			try
			{
				if (!gotURLs)
				{
					//if (loadTemplateFromURL)
					//{
					//get list
					string url = ConfigurationManager.AppSettings["SampleDirectory"];

					using (WebClient client = new WebClient())
					{
						string page = client.DownloadString(url);
						Regex re = new Regex(@"<A HREF=""([/\.\w\d\s]+\.[xmljson]+)"">", RegexOptions.IgnoreCase);
						MatchCollection mCol = re.Matches(page);
						if (mCol.Count > 0)
						{
							cmbLoadRequest.Items.Clear();
							foreach (Match m in mCol)
							{
								if (m.Groups[1].Value.Contains("RQ.xml") || m.Groups[1].Value.Contains("RQ.json"))
								{
									cmbLoadRequest.Items.Add(m.Groups[1].Value);
								}
							}
							gotURLs = true;
						}
					}
					//}
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}

			//}
		}

		private void loadTempaltesFromURL()
		{
			string REST_URL = System.Configuration.ConfigurationManager.AppSettings["URL"];
			string SampleDirectory = System.Configuration.ConfigurationManager.AppSettings["SampleDirectory"];
			using (WebClient client = new WebClient())
			{
				#region URL
				#region Parameter
				string parameter = "";
				if (cmbLoadRequest.Text.Contains(".xml"))
				{
					parameter = "?media=xml";
					rbtnXML.Checked = true;
				}
				else
				{
					parameter = "";
					rbtnJSON.Checked = true;
				}
				#endregion

				#region Action
				string action = "";
                if (cmbLoadRequest.Text.Contains("Token") && !cmbLoadRequest.Text.Contains("UtilityGetClientToken"))
                {
                    action = "GetToken";
				}
				else if (cmbLoadRequest.Text.Contains("GroundAvail_VehicleInfo"))
				{
					action = "Ground/VehicleInfo";
				}
				else if (cmbLoadRequest.Text.Contains("GroundAvail"))
				{
					action = "Ground/Avail";
				}
				else if (cmbLoadRequest.Text.Contains("GroundBook"))
				{
					action = "Ground/Book";
				}
				else if (cmbLoadRequest.Text.Contains("GroundCreateAsapRequest"))
				{
					action = "Ground/CreateAsapRequest";
				}
				else if (cmbLoadRequest.Text.Contains("GroundResRetrieve"))
				{
					action = "Ground/ResRetrieve";
				}
				else if (cmbLoadRequest.Text.Contains("GroundOriginalResRetrieve"))
				{
					action = "Ground/OriginalResRetrieve";
				}
				else if (cmbLoadRequest.Text.Contains("GroundCancelFee"))
				{
					action = "Ground/CancelFee";
				}
				else if (cmbLoadRequest.Text.Contains("GroundCancel"))
				{
					action = "Ground/Cancel";
				}
				else if (cmbLoadRequest.Text.Contains("GroundGetASAPRequestStatus"))
				{
					action = "Ground/GetASAPRequestStatus";
				}
				else if (cmbLoadRequest.Text.Contains("GroundAbandonASAPRequest"))
				{
					action = "Ground/AbandonASAPRequest";
				}
				else if (cmbLoadRequest.Text.Contains("GroundGetPickupTime"))
				{
					action = "Ground/GetPickupTime";
				}
				else if (cmbLoadRequest.Text.Contains("GroundGetFareScheduleWithFees"))
				{
					action = "Ground/GetFareScheduleWithFees";
				}
				else if (cmbLoadRequest.Text.Contains("AccountingAddMemberPaymentCard"))
				{
					action = "Accounting/AddMemberPaymentCard";
				}
                else if (cmbLoadRequest.Text.Contains("AccountingEditMemberPaymentCard"))
                {
                    action = "Accounting/EditMemberPaymentCard";
                }
				else if (cmbLoadRequest.Text.Contains("AccountingGetMemberPaymentCards"))
				{
					action = "Accounting/GetMemberPaymentCards";
				}
				else if (cmbLoadRequest.Text.Contains("AccountingSetMemberDefaultPaymentCard"))
				{
					action = "Accounting/SetMemberDefaultPaymentCard";
				}
				else if (cmbLoadRequest.Text.Contains("AccountingDeleteMemberPaymentCard"))
				{
					action = "Accounting/DeleteMemberPaymentCard";
				}
				else if (cmbLoadRequest.Text.Contains("AccountingAddMemberDirectBill"))
				{
					action = "Accounting/AddMemberDirectBill";
				}
				else if (cmbLoadRequest.Text.Contains("AccountingGetMemberDirectBills"))
				{
					action = "Accounting/GetMemberDirectBills";
				}
				else if (cmbLoadRequest.Text.Contains("AccountingSetMemberDefaultDirectBill"))
				{
					action = "Accounting/SetMemberDefaultDirectBill";
				}
				else if (cmbLoadRequest.Text.Contains("AccountingDeleteMemberDirectBill"))
				{
					action = "Accounting/DeleteMemberDirectBill";
				}
				else if (cmbLoadRequest.Text.Contains("GetFareDetail"))
				{
					action = "Accounting/GetFareDetail";
				}
				else if (cmbLoadRequest.Text.Contains("RewardGetAirlinePrograms"))
				{
					action = "Reward/GetAirlinePrograms";
				}
				else if (cmbLoadRequest.Text.Contains("RewardCreateMemberAccount"))
				{
					action = "Reward/CreateMemberAccount";
				}
				else if (cmbLoadRequest.Text.Contains("RewardDeleteMemberAccount"))
				{
					action = "Reward/DeleteMemberAccount";
				}
                else if (cmbLoadRequest.Text.Contains("RewardModifyMemberAccount"))
                {
                    action = "Reward/RewardModifyMemberAccount";
                }
				else if (cmbLoadRequest.Text.Contains("RewardGetMemberAccounts"))
				{
					action = "Reward/GetMemberAccounts";
				}
				else if (cmbLoadRequest.Text.Contains("RewardSetDefaultAccount"))
				{
					action = "Reward/SetDefaultAccount";
				}
				else if (cmbLoadRequest.Text.Contains("ZTripCreditActivateMemberCredit"))
				{
					action = "ZTripCredit/ActivateMemberCredit";
				}
				else if (cmbLoadRequest.Text.Contains("ZTripCreditGetMemberBalance"))
				{
					action = "ZTripCredit/GetMemberBalance";
				}
				else if (cmbLoadRequest.Text.Contains("MemberCreateRQ"))
				{
					action = "Member/Create";
				}
				else if (cmbLoadRequest.Text.Contains("MemberChangeEmailAddressRQ"))
				{
					action = "Member/ChangeEmailAddress";
				}
				else if (cmbLoadRequest.Text.Contains("MemberLoginRQ"))
				{
					action = "Member/Login";
				}
				else if (cmbLoadRequest.Text.Contains("MemberForgotPasswordRQ"))
				{
					action = "Member/ForgotPassword";
				}
				else if (cmbLoadRequest.Text.Contains("MemberGetPasswordRequestRQ"))
				{
					action = "Member/GetPasswordRequest";
				}
				else if (cmbLoadRequest.Text.Contains("MemberUpdateProfileRQ"))
				{
					action = "Member/UpdateProfile";
				}
				else if (cmbLoadRequest.Text.Contains("MemberChangePasswordRQ"))
				{
					action = "Member/ChangePassword";
				}
				else if (cmbLoadRequest.Text.Contains("MemberGetReservationRQ"))
				{
					action = "Member/GetReservation";
				}
				else if (cmbLoadRequest.Text.Contains("MemberAddLocationRQ"))
				{
					action = "Member/AddLocation";
				}
				else if (cmbLoadRequest.Text.Contains("MemberUpdateLocationRQ"))
				{
					action = "Member/UpdateLocation";
				}
				else if (cmbLoadRequest.Text.Contains("MemberDeleteLocationRQ"))
				{
					action = "Member/DeleteLocation";
				}
				else if (cmbLoadRequest.Text.Contains("MemberGetLocationsRQ"))
				{
					action = "Member/GetLocations";
				}
				else if (cmbLoadRequest.Text.Contains("MemberGetRecentLocationsRQ"))
				{
					action = "Member/GetRecentLocations";
				}
				else if (cmbLoadRequest.Text.Contains("RequestEmailConfirmationRQ"))
				{
					action = "Member/RequestEmailConfirmation";
				}
                else if (cmbLoadRequest.Text.Contains("RequestEmailCancellationRQ"))
                {
                    action = "Member/RequestEmailCancellation";
                }
				else if (cmbLoadRequest.Text.Contains("MemberGetLatestUnRatedTripRQ"))
				{
					action = "Member/GetLatestUnRatedTrip";
				}
				else if (cmbLoadRequest.Text.Contains("MemberResetPasswordRQ"))
				{
					action = "Member/ResetPassword";
				}
				else if (cmbLoadRequest.Text.Contains("MemberLinkReservationRQ"))
				{
					action = "Member/LinkReservation";
				}
				else if (cmbLoadRequest.Text.Contains("MemberGetCorporateProfileRQ"))
				{
					action = "Member/GetCorporateProfile";
				}
				else if (cmbLoadRequest.Text.Contains("MemberDeleteRQ"))
				{
					action = "Member/Delete";
				}
				else if (cmbLoadRequest.Text.Contains("UtilityGetAirportPickupOptions"))
				{
					action = "Utility/GetAirportPickupOptions";
				}
				else if (cmbLoadRequest.Text.Contains("UtilityGetServicedAirportsRQ"))
				{
					action = "Utility/GetServicedAirports";
				}
				else if (cmbLoadRequest.Text.Contains("UtilityGetDefaultFleetTypeForPoint"))
				{
					action = "Utility/GetDefaultFleetTypeForPoint";
				}
				else if (cmbLoadRequest.Text.Contains("UtilityGetLocationList"))
				{
					action = "Utility/GetLocationList";
				}
				else if (cmbLoadRequest.Text.Contains("UtilitySearchLocation"))
				{
					action = "Utility/SearchLocation";
				}
				else if (cmbLoadRequest.Text.Contains("UtilityAvailableFleets"))
				{
					action = "Utility/AvailableFleets";
				}
                else if (cmbLoadRequest.Text.Contains("UtilityGetLeadTime"))
                {
                    action = "Utility/GetLeadTime";
                }
				else if (cmbLoadRequest.Text.Contains("UtilityGetAirportName"))
				{
					action = "Utility/GetAirportName";
				}
				else if (cmbLoadRequest.Text.Contains("UtilityGetAirlinesByAirport"))
				{
					action = "Utility/GetAirlinesByAirport";
				}
				else if (cmbLoadRequest.Text.Contains("UtilityGetTripList"))
				{
					action = "Utility/GetTripList";
				}
				else if (cmbLoadRequest.Text.Contains("UtilityGetAirportInstructions"))
				{
					action = "Utility/GetAirportInstructions";
				}
				else if (cmbLoadRequest.Text.Contains("UtilityGetHonorifics"))
				{
					action = "Utility/GetHonorifics";
				}
				else if (cmbLoadRequest.Text.Contains("UtilityGetDestinations"))
				{
					action = "Utility/GetDestinations";
				}
				else if (cmbLoadRequest.Text.Contains("UtilityGetBookingCriteria"))
				{
					action = "Utility/GetBookingCriteria";
				}
				else if (cmbLoadRequest.Text.Contains("UtilityGetCorporateRequirements"))
				{
					action = "Utility/GetCorporateRequirements";
				}
                else if (cmbLoadRequest.Text.Contains("UtilityTypeAhead"))
                {
                    action = "Utility/TypeAhead";
                }
                else if (cmbLoadRequest.Text.Contains("UtilityGetCorporatePaymentMethod"))
                {
                    action = "Utility/GetCorporatePaymentMethod";
                }
				else if (cmbLoadRequest.Text.Contains("RateSet"))
				{
					action = "Rate/Set";
				}
				else if (cmbLoadRequest.Text.Contains("RateGet"))
				{
					action = "Rate/Get";
				}
				else if (cmbLoadRequest.Text.Contains("GetDiscount"))
				{
					action = "Group/GetDiscount";
				}
				else if (cmbLoadRequest.Text.Contains("ValidateDiscountCode"))
				{
					action = "Group/ValidateDiscountCode";
				}

				else if (cmbLoadRequest.Text.Contains("GetDefaults"))
				{
					action = "Group/GetDefaults";
				}
				else if (cmbLoadRequest.Text.Contains("GetLandmarks"))
				{
					action = "Group/GetLandmarks";
				}
				else if (cmbLoadRequest.Text.Contains("GetLocalizedServicedAirports"))
				{
					action = "Group/GetLocalizedServicedAirports";
				}
				else if (cmbLoadRequest.Text.Contains("GetPromotionType"))
				{
					action = "Group/GetPromotionType";
				}
                else if (cmbLoadRequest.Text.Contains("GetCustomerInfo"))
                {
                    action = "Utility/GetCustomerInfo";
                }

                else if (cmbLoadRequest.Text.Contains("GetPickupAddress"))
                {
                    action = "Utility/GetPickUpAddress";
                }
                else if (cmbLoadRequest.Text.Contains("GetLastTrip"))
                {
                    action = "Utility/GetLastTrip";
                }
                else if (cmbLoadRequest.Text.Contains("GetBlackoutPeriod"))
                {
                    action = "Utility/GetBlackoutPeriod";
                }
                else if (cmbLoadRequest.Text.Contains("GetStaleNotification"))
                {
                    action = "Utility/GetStaleNotification";
                }
                else if (cmbLoadRequest.Text.Contains("GetArrivalNotification"))
                {
                    action = "Utility/GetArrivalNotification";
                }
                else if (cmbLoadRequest.Text.Contains("GetNightBeforeReminder"))
                {
                    action = "Utility/GetNightBeforeReminder";
                }
                else if (cmbLoadRequest.Text.Contains("MemberGetLatestTripRQ"))
                {
                    action = "Member/GetLatestTrip";
                }
                else if (cmbLoadRequest.Text.Contains("SplitPaymentInviteRQ"))
                {
                    action = "SplitPayment/Invite";
                }
                else if (cmbLoadRequest.Text.Contains("SplitPaymentUpdateStatusRQ"))
                {
                    action = "SplitPayment/UpdateStatus";
                }
                else if (cmbLoadRequest.Text.Contains("SplitPaymentGetInvitationsRQ"))
                {
                    action = "SplitPayment/GetInvitations";
                }
                else if (cmbLoadRequest.Text.Contains("SplitPaymentCheckIfEnabledRQ"))
                {
                    action = "SplitPayment/CheckIfEnabled";
                }
                else if (cmbLoadRequest.Text.Contains("UtilityGetClientToken"))
                {
                    action = "Utility/GetClientToken";
                }
                #endregion

                txtUrlRequest.Text = REST_URL + action + parameter;
				#endregion

				#region Header
				string header = "";
				if (!cmbLoadRequest.Text.Contains("Taxi_"))
				{
					if (cmbLoadRequest.Text.Contains("Token"))
					{
						header = "";
					}
					else
					{
						header = "Username:##;SecurityKey:##";
					}
				}
				else
				{
					//header = "Username:Taxi;SecurityKey:P@ssw0rd";
					header = "Username:Udi;SecurityKey:##";
				}
				AddColouredText(txtRequestHeader, header);

				#endregion

				#region Shows request content
				var fileName = cmbLoadRequest.Text;
				var fileNames = fileName.Split('/');
				if (fileNames.Count() > 1)
				{
					fileName = fileNames.Last();
				}
				string request = client.DownloadString(SampleDirectory + fileName);
				AddColouredText(txtRequest, request);
				#endregion
			}
		}

		private void rbtnJSON_CheckedChanged(object sender, EventArgs e)
		{
			if (rbtnJSON.Checked && !string.IsNullOrWhiteSpace(txtUrlRequest.Text))
			{
				//http://localhost:52542/Rest/Ground/Book?media=xml
				txtUrlRequest.Text = txtUrlRequest.Text.Replace("?media=xml", "");
			}
		}

		private void rbtnXML_CheckedChanged(object sender, EventArgs e)
		{
			if (rbtnXML.Checked && !string.IsNullOrWhiteSpace(txtUrlRequest.Text))
			{

				txtUrlRequest.Text = txtUrlRequest.Text + "?media=xml";
			}
		}

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void rbtnPut_CheckedChanged(object sender, EventArgs e)
        {

        }

        //private void LoadTemplateFromFile()
        //{
        //	var sampleDirectory = System.Configuration.ConfigurationManager.AppSettings["SampleDirectory"];
        //	var baseUrl = System.Configuration.ConfigurationManager.AppSettings["URL"];
        //	var fileName = string.Empty;
        //	txtUrlRequest.Text = string.Empty;
        //	txtRequest.Clear();
        //	txtResponse.Clear();
        //	txtRequestHeader.Clear();
        //	var request = string.Empty;
        //	var needHeader = true;

        //	try
        //	{
        //		switch (cmbLoadRequest.SelectedIndex)
        //		{
        //			case 0:
        //				//Load SDS Token request - XML
        //				fileName = sampleDirectory + "TokenRQ.xml";
        //				txtUrlRequest.Text = baseUrl + "GetToken?media=xml";
        //				rbtnXML.Checked = true;
        //				needHeader = false;

        //				break;
        //			case 1:
        //				//Load SDS Token request - JSON
        //				fileName = sampleDirectory + "TokenRQ.json";
        //				txtUrlRequest.Text = baseUrl + "GetToken";
        //				rbtnJSON.Checked = true;
        //				needHeader = false;
        //				break;
        //			case 3:
        //				//Load GroundAvail request - XML
        //				fileName = sampleDirectory + "OTA_GroundAvailRQ.xml";
        //				txtUrlRequest.Text = baseUrl + "GroundAvail?media=xml";
        //				rbtnXML.Checked = true;
        //				break;
        //			case 4:
        //				//Load GroundAvail request - JSON
        //				fileName = sampleDirectory + "OTA_GroundAvailRQ.json";
        //				txtUrlRequest.Text = baseUrl + "GroundAvail";
        //				rbtnJSON.Checked = true;
        //				break;
        //			case 6:
        //				//Load GroundBook request - XML
        //				fileName = sampleDirectory + "OTA_GroundBookRQ.xml";
        //				txtUrlRequest.Text = baseUrl + "GroundBook?media=xml";
        //				rbtnXML.Checked = true;
        //				break;
        //			case 7:
        //				//Load GroundBook request - JSON
        //				fileName = sampleDirectory + "OTA_GroundBookRQ.json";
        //				txtUrlRequest.Text = baseUrl + "GroundBook";
        //				rbtnJSON.Checked = true;
        //				break;
        //			case 9:
        //				//Load GroundResRetrieve request - XML
        //				fileName = sampleDirectory + "OTA_GroundResRetrieveRQ.xml";
        //				txtUrlRequest.Text = baseUrl + "GroundResRetrieve?media=xml";
        //				rbtnXML.Checked = true;
        //				break;
        //			case 10:
        //				//Load GroundResRetrieve request - JSON
        //				fileName = sampleDirectory + "OTA_GroundResRetrieveRQ.json";
        //				txtUrlRequest.Text = baseUrl + "GroundResRetrieve";
        //				rbtnJSON.Checked = true;
        //				break;
        //			case 12:
        //				//Load Cancel request - XML
        //				fileName = sampleDirectory + "OTA_GroundCancelRQ.xml";
        //				txtUrlRequest.Text = baseUrl + "GroundCancel?media=xml";
        //				rbtnXML.Checked = true;
        //				break;
        //			case 13:
        //				//Load Cancel request - JSON
        //				fileName = sampleDirectory + "OTA_GroundCancelRQ.json";
        //				txtUrlRequest.Text = baseUrl + "GroundCancel";
        //				rbtnJSON.Checked = true;
        //				break;
        //		}

        //		if (!string.IsNullOrWhiteSpace(fileName))
        //		{
        //			if (rbtnXML.Checked)
        //			{
        //				request = XDocument.Load(fileName).ToString();
        //			}
        //			else if (rbtnJSON.Enabled)
        //			{
        //				var wc = new WebClient();

        //				using (var sourceStream = wc.OpenRead(fileName))
        //				{
        //					using (StreamReader sr = new StreamReader(sourceStream))
        //					{
        //						request = sr.ReadToEnd();
        //					}
        //				}
        //			}

        //			AddColouredText(txtRequest, request);

        //			if (needHeader)
        //			{
        //				AddColouredText(txtRequestHeader, "ClientType:##;ClientIPAddress:##;SecurityKey:##;Username:##;");
        //			}
        //		}

        //	}
        //	catch (Exception ex)
        //	{
        //		MessageBox.Show(ex.Message);
        //	}
        //}

        #endregion

        private void txtRequest_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.Control && e.KeyCode == Keys.V)
            //{
            //    txtRequest.Text += (string)Clipboard.GetData("Text");
            //    e.Handled = true;
            //}
        }
    }

}
