﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Utility.Common.Const
{
   public class NotificationType
    {
       public const int PushNotification = 1;
       public const int SMSNotification = 2;
    }
}
