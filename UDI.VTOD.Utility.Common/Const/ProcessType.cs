﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Utility.Common.Cost
{
    public class ProcessType
    {
        public const int Referral = 2;
        public const int Notification = 1;
        public const int Referral_Notification = 11;
    }
}
