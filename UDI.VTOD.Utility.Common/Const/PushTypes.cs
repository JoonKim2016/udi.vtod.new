﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Utility.Common.Const
{
   public class PushTypes
    {
        public const int Now = 2;
        public const int Later = 1;
        public const int Recursive = 11;
    }
}
