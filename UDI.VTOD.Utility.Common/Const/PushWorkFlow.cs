﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Utility.Common.Const
{
   public class PushWorkFlow
    {
       public const int PushWorkFlow_Error = -1;
       public const int PushWorkFlow_Exception = -2;
       public const int PushWorkFlow_Inserted = 1;
       public const int PushWorkFlow_Inprogress = 2;
       public const int PushWorkFlow_Inserted_TripReminder = 4;
       public const int PushWorkFlow_Completed = 10;
       public const int PushWorkFlow_Cancelled = -3;
       public const int PushWorkFlow_TripReminder_NotSent = -5; 
    }
}
