﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Utility.Common.Cost
{
    public class ReferrerCreditFlow
    {
        public const int Credit_SDS_Error = -1;
        public const int Credit_Branch_Error = -2;
        public const int Credit_Inserted = 1;
        public const int Credit_Inprogress = 2;
        public const int Credit_Completed = 10;
        public const int Trip_Cancelled = 11;
        public const int Trip_Duplicate = 12;
    }
}
