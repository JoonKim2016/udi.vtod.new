﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Utility.Common.Cost
{
  public  class ReferrerCreditType
    {
        public const int Register = 1;
        public const int CompletedTrip = 2;
        public const int CreditCompleted = 3;
    }
}
