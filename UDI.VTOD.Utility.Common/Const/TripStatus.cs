﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Utility.Common.Const
{
   public class TripStatus
    {
        public const string Booked = "Booked";
        public const string Cancelled = "Canceled";
        public const string Completed = "Completed";
    }
}
