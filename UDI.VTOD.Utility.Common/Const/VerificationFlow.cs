﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Utility.Common.Cost
{
    /// <summary>
    /// Constants for the Steps of the Verification
    /// </summary>
    #region Class
    public class VerificationFlow
	{
		#region Phone_SMS
		/// <summary>
		/// Error happened
		/// </summary>
		public const int Phone_SMS_Error = -1;
        public const int Phone_SMS_Exception= -2;
		/// <summary>
		/// It's ready for SMS outbound to send a text to be verified.
		/// </summary>

		public const int Phone_SMS_Inserted = 1;
		public const int Phone_SMS_Inserted_InProgress = 2;
		public const int Phone_SMS_Sent = 3;
        public const int Phone_SMS_Sent_InProgress = 4;
        public const int Phone_SMS_Duplicate = 5;
        public const int Phone_SMS_Completed = 10;
		#endregion

		#region Phone_Call
		public const int Phone_Call_Error = -1;
		public const int Phone_Call_Inserted = 11;
		public const int Phone_Call_Inserted_InProgress = 12;
		public const int Phone_Call_Sent = 13;
		public const int Phone_Call_Sent_InProgress = 14;
		public const int Phone_Call_Completed = 20;
		#endregion
    }
    #endregion
}
