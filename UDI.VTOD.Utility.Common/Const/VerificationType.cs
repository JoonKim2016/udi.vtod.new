﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Utility.Common.Const
{
    public class VerificationType
    {
        public const int VerificationByText = 1;
        public const int VerificationByCode = 2;
    }
}
