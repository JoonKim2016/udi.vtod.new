﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Utility.Common.Cost
{
    /// <summary>
    /// Constants for the Type of the Verification
    /// </summary>
    #region Class
    public  class VerificationTypeDesc
    {
        public const string Phone_SMS = "SMS";
       // public const string Phone = "Phone";

        /// <summary>
        /// This is to verify phone number by IVR
        /// </summary>
       public const string Phone_Call = "Phone";
      // public const string Email = "Email";
    }
    #endregion
}
