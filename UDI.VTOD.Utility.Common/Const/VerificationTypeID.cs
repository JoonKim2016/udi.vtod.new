﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Utility.Common.Cost
{
    /// <summary>
    /// Constants for Verification ID
    /// </summary>
    #region Class

    public class VerificationTypeID
	{
		/// <summary>
		/// This is to verify phone number by SMS
		/// </summary>
        public const int Phone_SMS = 1;
        //public const int Phone = 1;

		/// <summary>
		/// This is to verify phone number by IVR
		/// </summary>
		public const int Phone_Call = 2;
        //public const int Email = 2;


    }
    #endregion
}
