﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using UDI.Utility.Helper;

namespace UDI.VTOD.Utility.Common.Contracts
{
    #region Class
    public abstract class BaseSetting
	{
		#region Fields
		public bool isDebug = false;
		public bool isTest = false;
		public ILog logger; 
		#endregion

		#region Constructor
		public BaseSetting()
		{
			logger = LogManager.GetLogger(this.GetType());
			log4net.Config.XmlConfigurator.Configure();

			#region IsDebug
			try
			{
				isDebug = System.Configuration.ConfigurationManager.AppSettings["IsDebug"].ToBool();
			}
			catch { }
			#endregion

			#region IsTest
			try
			{
				isTest = System.Configuration.ConfigurationManager.AppSettings["IsTest"].ToBool();
			}
			catch { }
			#endregion

		}
		#endregion
    }
    #endregion
}
