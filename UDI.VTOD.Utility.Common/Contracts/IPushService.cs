﻿using System.Collections.Generic;
using UDI.VTOD.Utility.Common.Const;
using UDI.VTOD.Utility.Common.DTO;

namespace UDI.VTOD.Utility.Common.Contracts
{
    /// <summary>
    /// Interface for push notification services
    /// </summary>
    public interface IPushService
    {
        string ServiceProvider { get; }

        /// <summary>
        /// Send notification to target devices
        /// </summary>
        /// <param name="endpoints">For Parse, it's member id</param>
        /// <param name="contentType"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        /// 
        
        //TODO: What is the reason that we have "List<string> endpoints" instead of "string endpoint" ?
        bool SendNotification(List<string> endpoints, PushNotification notification);
              

        /// <summary>
        /// Broadcast notification to all devices
        /// </summary>
        /// <param name="contentType"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        bool BroadcastNotification(PushNotification notification);
    }
}
