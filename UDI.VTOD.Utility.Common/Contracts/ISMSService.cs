﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Utility.Common.Contracts
{
    #region Interface
    public interface ISMSService
    {
        #region Methods 

        bool Send(UDI.VTOD.Utility.Common.DTO.MessageDetails m,ref string errorMessage);
       // bool Receive(UDI.VTOD.Utility.Common.DTO.MessageDetails m, string errorMessage,int? status);
        List<string> Receive(UDI.VTOD.Utility.Common.DTO.MessageDetails m, ref string errorMessage);

        #endregion
    }
    #endregion
}
    