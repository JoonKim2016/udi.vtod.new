﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Utility.Common.DTO
{
   public  class BranchCredit
    {
        public string branch_key { set; get; }
         public string branch_secret { set; get; }
         public string identity { set; get; }
         public string amount { set; get; }
    }
}
