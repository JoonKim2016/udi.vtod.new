﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Utility.Common.DTO
{
    public class Configuration
    {
        public UDI.VTOD.Common.DTO.OTA.Address Address { set; get; }
        public DateTime? LocalDateTime { set; get; }
        public string AppName { set; get; }
        public string Category { set; get; }
        public string Key { set; get; }
    }
}
