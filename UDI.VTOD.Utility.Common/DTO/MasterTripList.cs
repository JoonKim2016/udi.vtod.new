﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Utility.Common.DTO
{
  public  class MasterTripList
    {
        public Int64? tripID { set; get; }
        public int? ID { set; get; }
        public string Status { set; get; }
        public int? processType { set; get; }
        public Int64? memberID { set; get; }
        public string Firstname { set; get; }
        public string VehicleNumber { set; get; }
        public string SharedETALink { set; get; }
        public int? FleetTripCode { set; get; }
        public int? WorkFlow { set; get; }
    }
}
