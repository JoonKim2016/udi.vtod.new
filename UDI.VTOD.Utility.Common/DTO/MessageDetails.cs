﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Utility.Common.DTO
{
    /// <summary>
    /// Message Object 
    /// </summary>
    #region Class
    public class MessageDetails
    {

        #region Constructor

        public MessageDetails()
        {
            To = new List<string>();
        }

        #endregion

        #region Properties

        public string From { set; get; }
        public List<string> To { set; get; }
        public string MessageBody { set; get; }
        public DateTime MessageSentDate { set; get; }
        #endregion
    }
    #endregion
}
