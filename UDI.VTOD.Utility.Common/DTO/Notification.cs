﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Utility.Common.DTO
{
  public  class Notification
    {
        public string Text { set; get; }
        public string PushType { set; get; }
        public string EmailAddress { set; get; }
    }
}
