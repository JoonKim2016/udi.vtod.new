﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Common.DTO.OTA;

namespace UDI.VTOD.Utility.Common.DTO
{
    /// <summary>
    /// Class to store the details of a record of Verification table
    /// </summary>
    #region Class
    public class PhoneInfo
    {
        #region Properties

        public Int64 ID { get; set; }
        public string PhoneNumber { get; set; }
        public Int64? MemberID { get; set; }
		public int VerificationFlow { get; set; }
        public bool IsVerified { get; set; }
        public DateTime AppendTime { get; set; }
        public DateTime InsertedTimeUTC { get; set; }
        public int VerificationType { get; set; }
        public int VerificationCode { get; set; }
        #endregion
    }
    #endregion
}
