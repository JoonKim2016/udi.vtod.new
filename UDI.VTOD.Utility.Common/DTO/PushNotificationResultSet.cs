﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Utility.Common.DTO
{
  public  class PushNotificationResultSet
    {
        public Int64? ID { get; set; }
        public string memberID { get; set; }
        public string DisplayText { get; set; }
        public string Event { get; set; }
        public string ImageURL { get; set; }
        public int? IsBroadcast { get; set; }
        public int? NotificationTypes { get; set; }
        public Int64? VtodTripID { get; set; }
    }
}
