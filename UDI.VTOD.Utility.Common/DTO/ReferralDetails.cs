﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Utility.Common.DTO
{
    public class ReferralDetails
    {
        public Int64 ID { get; set; }
        public Int64? TripID { get; set; }
        public Int64? MemberID { get; set; }
        public Int64? ReferrerMemberID { get; set; }
        public decimal? ReferralCreditAmount { get; set; }
        public int? ReferralCreditType { get; set; }
        public int? ReferralCreditFlow { get; set; }
    }

}
