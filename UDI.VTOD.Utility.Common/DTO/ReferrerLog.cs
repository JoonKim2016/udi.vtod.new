﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Utility.Common.DTO
{
    public class ReferralLog
    {
        public Token Token { get; set; }
        public string ReferralCode { get; set; }
        public string RefereePhoneNumber { get; set; }
    }
}
