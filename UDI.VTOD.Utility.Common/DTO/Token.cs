﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Utility.Common.DTO
{
  public  class Token
    {
      public string Username { get; set; }
      public string SecurityKey { get; set; }
    }
}
