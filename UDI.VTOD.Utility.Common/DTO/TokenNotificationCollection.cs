﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Common.DTO.OTA;
namespace UDI.VTOD.Utility.Common.DTO
{
   public class TokenNotificationCollection
    {
       #region Constructor
       public TokenNotificationCollection()
        {
            NotificationCollection = new List<PushNotification>();
        }
        #endregion

        #region Properties
        public TokenRS Token { get; set; }
        public List<PushNotification> NotificationCollection { get; set; }
        #endregion
    }
}
