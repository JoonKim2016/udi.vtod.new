﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Common.DTO.OTA;

namespace UDI.VTOD.Utility.Common.DTO
{
    #region Class
    public class TokenPhoneCollection
    {
        #region Constructor
        public TokenPhoneCollection()
        {
            PhoneCollection = new List<PhoneInfo>();
        }
        #endregion

        #region Properties
        public TokenRS Token { get; set; }
        public List<PhoneInfo> PhoneCollection { get; set; }
        #endregion
    }
    #endregion
}
