﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Common.DTO.OTA;

namespace UDI.VTOD.Utility.Common.DTO
{
    #region Class
    public class TokenReferralRegisterCollection
    {
        #region Constructor
        public TokenReferralRegisterCollection()
        {
            TokenRegisterCollection = new List<ReferralDetails>();
        }
        #endregion

        #region Properties
        public TokenRS Token { get; set; }
        public List<ReferralDetails> TokenRegisterCollection { get; set; }
        #endregion
    }
    #endregion
}
