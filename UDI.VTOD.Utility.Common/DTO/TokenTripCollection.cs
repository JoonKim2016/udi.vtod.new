﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Common.DTO.OTA;
namespace UDI.VTOD.Utility.Common.DTO
{
    public class TokenTripCollection
    {
      
            #region Constructor
            public TokenTripCollection()
            {
                TripCollection = new List<MasterTripList>();
            }
            #endregion

            #region Properties
            public TokenRS Token { get; set; }
            public List<MasterTripList> TripCollection { get; set; }
            #endregion
    }
}
