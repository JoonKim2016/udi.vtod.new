﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Utility.Common
{
	public static class Helper
	{
		public static IEnumerable<IList<T>> Chunkify<T>(this IList<T> list, int chunkCount)
		{
			int totalCNT = list.Count();
			bool dividable = false;
			if ((totalCNT % chunkCount) == 0)
				dividable = true;
			else
				dividable = false;

			if (totalCNT <= chunkCount)
			{
				for (int index = 0; index < totalCNT; index++)
				{
					yield return list.Skip(index).Take(1).ToList();
				}
			}
			else
			{
				for (int index = 1; index <= chunkCount; index++)
				{
					int skip = (totalCNT / chunkCount) * (index - 1);
					int amount = 0;

					if (!dividable && (index.Equals(chunkCount)))
						amount = totalCNT - skip;
					else
						amount = (totalCNT / chunkCount);

					yield return list.Skip(skip).Take(amount).ToList();
				}
			}

		}
      


	}
}
