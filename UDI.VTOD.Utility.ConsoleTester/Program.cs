﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Utility.ConsoleTester
{
	class Program
	{
		static void Main(string[] args)
		{
			var headers = new Dictionary<string, string>();
			var status = string.Empty;
			var contentType = string.Empty;
			var method = string.Empty;
			var response = string.Empty;
			var url = string.Empty;
			var content = string.Empty;

			#region TestController
			/*
			contentType = "application/xml; charset=utf-8";
			method = "GET";
			url = "http://localhost:63290/Tests/TestObj/2";
			response = UDI.Utility.Helper.WebRequestHelper.ProcessWebRequest(
					url,
					content,
					contentType,
					method,
					headers,
					out status);

			url = "http://localhost:63290/Tests/TestObj2/2/4";
			response = UDI.Utility.Helper.WebRequestHelper.ProcessWebRequest(
					url,
					content,
					contentType,
					method,
					headers,
					out status);

			url = "http://localhost:63290/Tests/TestObj3/2/Yo-yo";
			response = UDI.Utility.Helper.WebRequestHelper.ProcessWebRequest(
					url,
					content,
					contentType,
					method,
					headers,
					out status);

			url = "http://localhost:63290/Tests/TestObj3/2";
			response = UDI.Utility.Helper.WebRequestHelper.ProcessWebRequest(
					url,
					content,
					contentType,
					method,
					headers,
					out status);


			contentType = "application/xml; charset=utf-8";
			method = "DELETE";
			url = "http://localhost:63290/Tests/TestObj/2";
			response = UDI.Utility.Helper.WebRequestHelper.ProcessWebRequest(
					url,
					content,
					contentType,
					method,
					headers,
					out status);

			contentType = "application/json; charset=utf-8";
			response = UDI.Utility.Helper.WebRequestHelper.ProcessWebRequest(
					url,
					content,
					contentType,
					method,
					headers,
					out status);


			contentType = "application/json; charset=utf-8";
			method = "POST";
			url = "http://localhost:63290/Tests/GetTestByPost";
			response = UDI.Utility.Helper.WebRequestHelper.ProcessWebRequest(
					url,
					content,
					contentType,
					method,
					headers,
					out status);

			*/

			#endregion

			#region VerificationController
            //contentType = "application`/xml; charset=utf-8";
            contentType = "application/json; charset=utf-8";
			method = "GET";
            url = "http://localhost:63290/Verification/Phone/IsVerified/2034281783/12345/";

			response = UDI.Utility.Helper.WebRequestHelper.ProcessWebRequest(
					url,
					content,
					contentType,
					method,
					headers,
					out status);

			url = "http://localhost:63290/Verification/Phone/SMS/310928541";

			response = UDI.Utility.Helper.WebRequestHelper.ProcessWebRequest(
					url,
					content,
					contentType,
					method,
					headers,
					out status);

			url = "http://localhost:63290/Verification/Phone/IsVerified/3109876543/123654";

			response = UDI.Utility.Helper.WebRequestHelper.ProcessWebRequest(
					url,
					content,
					contentType,
					method,
					headers,
					out status);

			Console.WriteLine("Done :)");
			Console.ReadLine();
			#endregion
		}
	}
}
