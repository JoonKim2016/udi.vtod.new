﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace UDI.VTOD.Utility.Model
{
    public class CategoryAttributes
    {
        public string ID;
        public string Name;
        public string Value;
        public string Type;
        public string Priority;
    }
}
