﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace UDI.VTOD.Utility.Model
{
    public class ConfigCategory
    {
        public string CategoryType;
        public List<CategoryAttributes> Attributes { get; set; }
    }
}
