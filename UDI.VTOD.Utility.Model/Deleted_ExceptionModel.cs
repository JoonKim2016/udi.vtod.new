﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Utility.Model
{
	public class Deleted_ExceptionModel
	{
		public string Type { get; set; }
		public int Code { get; set; }
		public string Message { get; set; }
	}
}
