﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UDI.VTOD.Utility.Model
{
	public class IsVerifiedRS
	{
		public bool? Verified { get; set; }

		public Success Success { get; set; }
	}
}