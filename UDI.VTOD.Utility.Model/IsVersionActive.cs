﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace UDI.VTOD.Utility.Model
{
   
   public class IsVersionActiveRS
    {
        public Success Success { get; set; }
        public bool? IsActive { get; set; }
       [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsAppVersionSupported { get; set; }
       [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsPlatformVersionSupported { get; set; }
    }
}
