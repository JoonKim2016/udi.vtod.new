﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Utility.Model
{
    public class MemberCreditHistory
    {
        public Success Success { get; set; }
        public List<Credit> Credits { get; set; }
    }

    public class Credit
    {
        public int ReferrerMemberID { get; set; }
        public string ReferrerFullName { get; set; }
        public int RefereeMemberID { get; set; }
        public string RefereeFullName { get; set; }
        public string CreditType { get; set; }
        public decimal CreditAmount { get; set; }
        public DateTime CreditedOn { get; set; }
    }
}
