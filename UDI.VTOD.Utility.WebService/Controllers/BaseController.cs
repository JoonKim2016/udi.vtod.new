﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using UDI.Utility.Serialization;
using UDI.VTOD.Common.DTO;

namespace UDI.VTOD.Utility.WebService.Controllers
{
    public class BaseController : ApiController
    {
        protected HttpResponseMessage ToHttpException(VtodException vtodException, HttpRequestMessage request)
        {

            var contentType = "application/json";
            if (request.Content.Headers.ContentType != null && !string.IsNullOrWhiteSpace(request.Content.Headers.ContentType.MediaType))
                contentType = request.Content.Headers.ContentType.MediaType;

            var exceptionModel = new UDI.VTOD.Utility.Model.Error { Code = vtodException.ExceptionMessage.Code, Value = vtodException.ExceptionMessage.Message, Type = vtodException.ExceptionType.ToString() };
            var errors = new List<UDI.VTOD.Utility.Model.Error>();
            errors.Add(exceptionModel);
            string exceptionContent = string.Empty;
            if (contentType.ToLower().Contains("xml"))
            {
                exceptionContent = errors.XmlSerialize().ToString();
            }
            else
            {
                exceptionContent = errors.JsonSerialize();
            }
            return new HttpResponseMessage(System.Net.HttpStatusCode.BadRequest) { Content = new StringContent(exceptionContent) };
        }        

    }
}