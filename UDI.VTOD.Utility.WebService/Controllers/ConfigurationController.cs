﻿using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UDI.VTOD.Common.DTO;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Utility.Application;
using UDI.VTOD.Utility.Common.Contracts;
using UDI.VTOD.Utility.Model;
using UDI.Utility.Serialization;
using System.Text.RegularExpressions;
using UDI.VTOD.Common.Helper;
using UDI.Utility.Helper;
namespace UDI.VTOD.Utility.WebService.Controllers
{
    public class ConfigurationController : BaseController
    {
        public ILog logger;
        #region Constructor
        public ConfigurationController()
		{
			logger = LogManager.GetLogger(this.GetType());
			log4net.Config.XmlConfigurator.Configure();
		}
		#endregion

        #region Configuration Settings
        [HttpPost]
        [Route("Configuration")]
        public IHttpActionResult RetrieveAppSettings([FromBody]UDI.VTOD.Utility.Common.DTO.Configuration ConfigurationDetails)
        {
            IHttpActionResult response;
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process - Retrieving AppSettings of getting the inputs:{0}:", stopwatch.ElapsedMilliseconds);
                #endregion
                #region Get Header
                var token = new TokenRS();
                var userName = Request.Headers.Where(s => s.Key == "Username").FirstOrDefault();
                var securityKey = Request.Headers.Where(s => s.Key == "SecurityKey").FirstOrDefault();
                logger.InfoFormat("Username:", userName.Value.FirstOrDefault());
                logger.InfoFormat("securityKey:", securityKey.Value.FirstOrDefault());
                try
                {
                    if (userName.Key != null)
                        token.Username = userName.Value.FirstOrDefault();
                }
                catch (Exception ex)
                {
                    logger.Error("UserName:", ex);
                }
                try
                {
                    if (securityKey.Key != null)
                        token.SecurityKey = securityKey.Value.FirstOrDefault();
                }
                catch (Exception ex)
                {
                    logger.Error("SecurityKey:", ex);
                }
                try
                {
                    if (ConfigurationDetails == null)
                    {
                        throw VtodException.CreateException(ExceptionType.Utility, 9013);
                    }
                    else 
                    {
                       if(string.IsNullOrEmpty(ConfigurationDetails.AppName))
                       {
                          throw VtodException.CreateException(ExceptionType.Utility, 9014);
                       }
                        #region "Commented Validation"
                       //if (ConfigurationDetails.Address != null)
                       //{
                       //    if (ConfigurationDetails.Address.Latitude == null || ConfigurationDetails.Address.Longitude == null)
                       //    {
                       //        //throw VtodException.CreateException(ExceptionType.Utility, 9015);
                       //    }
                       //}
                       //else
                       //{
                       //    throw VtodException.CreateException(ExceptionType.Utility, 9015);
                       //} 
                        if (ConfigurationDetails.Key != null && ConfigurationDetails.Category==null)
                        {
                            throw VtodException.CreateException(ExceptionType.Utility, 9016);
                        }
                        #endregion

                    }
                }
                catch (VtodException ex)
                {
                    logger.Error(ex);
                    throw ex;
                }
                #endregion

                #region Call Controller
                AppConfigurationRS appSettings = null;
                logger.InfoFormat("Start-Configuration Controller");
                var controller = new QueryController();
                controller.logger = logger;
                logger.InfoFormat("Start-Retrieve Configuration");
                appSettings = controller.RetrieveConfiguration(token, ConfigurationDetails);
                appSettings.Success = new Model.Success();
                logger.InfoFormat("End-Retrieve Configuration");
                logger.InfoFormat("End-Configuration Controller");
                #endregion

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End Process---RetrieveAppSettings:{0}:", stopwatch.ElapsedMilliseconds);
                #endregion
                return Ok(appSettings);
            }

            catch (VtodException ex)
            {

                logger.Error(ex);
                response = ResponseMessage((ToHttpException(ex, Request)));
                return response;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                response = ResponseMessage(ToHttpException(VtodException.CreateException(ExceptionType.Auth, 1), Request));
                return response;
            }
        }
        #endregion
        
    }
}
