﻿using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UDI.VTOD.Common.DTO;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Utility.Application;
using UDI.VTOD.Utility.Common.Contracts;
using UDI.VTOD.Utility.Model;
using UDI.Utility.Serialization;
using System.Text.RegularExpressions;
using UDI.VTOD.Common.Helper;
using UDI.Utility.Helper;
using log4net;
namespace UDI.VTOD.Utility.WebService.Controllers
{
    public class NotificationController : BaseController
    {
        public ILog logger;
        #region Constructor
        public NotificationController()
		{
			logger = LogManager.GetLogger(this.GetType());
			log4net.Config.XmlConfigurator.Configure();
		}
		#endregion
        #region AlephPushNotification
        [HttpPost]
        [Route("Notification")]
        public IHttpActionResult Add([FromBody]UDI.VTOD.Utility.Common.DTO.Notification NotificationArray)
        {
            IHttpActionResult response;
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process - Add Notification of getting the inputs:{0}:", stopwatch.ElapsedMilliseconds);
                #endregion
                #region Get Header
                var token = new TokenRS();
                var userName = Request.Headers.Where(s => s.Key == "Username").FirstOrDefault();
                var securityKey = Request.Headers.Where(s => s.Key == "SecurityKey").FirstOrDefault();
                logger.InfoFormat("Username:", userName.Value.FirstOrDefault());
                logger.InfoFormat("securityKey:", securityKey.Value.FirstOrDefault());
                try
                {
                    if (userName.Key != null)
                        token.Username = userName.Value.FirstOrDefault();
                }
                catch (Exception ex)
                {
                    logger.Error("UserName:", ex);
                }
                try
                {
                    if (securityKey.Key != null)
                        token.SecurityKey = securityKey.Value.FirstOrDefault();
                }
                catch (Exception ex)
                {
                    logger.Error("SecurityKey:", ex);
                }
                #endregion

                #region Call Controller
                logger.InfoFormat("Start-Notification Controller");
                var controller = new QueryController();
                controller.logger = logger;
                logger.InfoFormat("Start-Add Notification");
                controller.AddNotification(token, NotificationArray.Text, NotificationArray.PushType, NotificationArray.EmailAddress);
                logger.InfoFormat("End-Add Notification");
                logger.InfoFormat("End-Notification Controller");
                #endregion

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End Process---Add Notification:{0}:", stopwatch.ElapsedMilliseconds);
                #endregion
                var result = new NotificationRS { Success = new Model.Success() };
                return Ok(result);
            }

            catch (VtodException ex)
            {

                logger.Error(ex);
                response = ResponseMessage((ToHttpException(ex, Request)));
                return response;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                response = ResponseMessage(ToHttpException(VtodException.CreateException(ExceptionType.Auth, 1), Request));
                return response;
            }
        }
        #endregion
        
    }
}