﻿using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UDI.VTOD.Common.DTO;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Utility.Application;
using UDI.VTOD.Utility.Common.Contracts;
using UDI.VTOD.Utility.Model;
using UDI.Utility.Serialization;
using System.Text.RegularExpressions;
using UDI.VTOD.Common.Helper;
using UDI.Utility.Helper;
using UDI.VTOD.Utility.DataAccess.VTOD;
using UDI.Notification.Service.Controller;
using UDI.Notification.Service.Class;
using UDI.VTOD.Utility.Domain.Referral;
using UDI.SDS.MembershipService;

namespace UDI.VTOD.Utility.WebService.Controllers
{
	public class ReferralController : BaseController
	{
		#region Fields
		public ILog logger;
		#endregion
        		
        public ReferralController()
		{
			logger = LogManager.GetLogger(this.GetType());
			log4net.Config.XmlConfigurator.Configure();
		}

        [HttpGet]
        [Route("Referral/GetMemberRewardHistory/{memberID:int}")]
        public IHttpActionResult GetMemberRewardHistory(int memberID)
        {
            IHttpActionResult response;

            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process - GetMemberRewardHistory :{0}:", stopwatch.ElapsedMilliseconds);
                #endregion

                #region Get Header
                var token = new TokenRS();
                var userName = Request.Headers.Where(s => s.Key == "Username").FirstOrDefault();
                var securityKey = Request.Headers.Where(s => s.Key == "SecurityKey").FirstOrDefault();
                logger.InfoFormat("Username:", userName.Value.FirstOrDefault());
                logger.InfoFormat("securityKey:", securityKey.Value.FirstOrDefault());
                try
                {
                    if (userName.Key != null)
                        token.Username = userName.Value.FirstOrDefault();
                }
                catch (Exception ex)
                {
                    logger.Error("UserName:", ex);
                }
                try
                {
                    if (securityKey.Key != null)
                        token.SecurityKey = securityKey.Value.FirstOrDefault();
                }
                catch (Exception ex)
                {
                    logger.Error("SecurityKey:", ex);
                }

                #endregion


                List<Credit> creditHistoris = new List<Credit>();

                using (var db = new DataAccess.VTOD.VTODEntities())
                {
                    #region adding ReferralNewUser Credit
                    var memberInfo = db.vtod_member_info.Where(x => x.SDSMemberId.Equals(memberID) && x.CreditEarnedOn.HasValue).FirstOrDefault();
                    if (memberInfo != null)
                    {                        
                        Credit newReferralCredit = new Credit();
                        newReferralCredit.CreditedOn = memberInfo.CreditEarnedOn.Value;
                        newReferralCredit.CreditAmount = memberInfo.ReferralCreditAmount.Value;
                        newReferralCredit.ReferrerMemberID = memberInfo.ReferrerSDSMemberID.Value;
                        newReferralCredit.CreditType = ZTripCreditType.ReferralNewUser.ToString();

                        //find Referrer
                        var referrerMemberInfo = db.vtod_member_info.Where(x => x.SDSMemberId.Equals(newReferralCredit.ReferrerMemberID)).FirstOrDefault();
                        if (referrerMemberInfo != null)
                        {
                            newReferralCredit.ReferrerFullName = referrerMemberInfo.FirstName+" "+ referrerMemberInfo.LastName;
                        }

                        creditHistoris.Add(newReferralCredit);

                    }
                    #endregion

                    #region adding ReferralNewUser Credit
                    var creditedTrips = db.ReferralTrips.Where(x => x.ReferrerSDSMemberID.Equals(memberID) && x.CreditEarnedOn.HasValue).ToList();

                    foreach (var reditedTrip in creditedTrips)
                    {
                        Credit referralTripCompletedCredit = new Credit();
                        referralTripCompletedCredit.CreditedOn = reditedTrip.CreditEarnedOn.Value;
                        referralTripCompletedCredit.CreditAmount = reditedTrip.ReferralCreditAmount.Value;
                        referralTripCompletedCredit.RefereeMemberID = reditedTrip.SDSMemberID;
                        referralTripCompletedCredit.CreditType = ZTripCreditType.ReferralTripCompleted.ToString();

                        //find Referree
                        var refereeMemberInfo = db.vtod_member_info.Where(x => x.SDSMemberId.Equals(referralTripCompletedCredit.RefereeMemberID)).FirstOrDefault();
                        if (refereeMemberInfo != null)
                        {
                            referralTripCompletedCredit.RefereeFullName = refereeMemberInfo.FirstName + " " + refereeMemberInfo.LastName;
                        }
                        creditHistoris.Add(referralTripCompletedCredit);
                    }

                    #endregion
                }   

                stopwatch.Stop();
                logger.InfoFormat("End the Process - GetMemberRewardHistory  : {0}:", stopwatch.ElapsedMilliseconds);
                
                var result = new MemberCreditHistory { Success = new Model.Success() , Credits = creditHistoris };
                return Ok(result);
            }
            catch (VtodException ex)
            {

                logger.Error(ex);
                logger.Error(ex.ExceptionMessage.Code);
                response = ResponseMessage((ToHttpException(ex, Request)));
                return response;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                response = ResponseMessage(ToHttpException(VtodException.CreateException(ExceptionType.Auth, 1), Request));
                return response;
            }
        }

        [HttpPost]        
        public IHttpActionResult AddReferral([FromBody]UDI.VTOD.Utility.Common.DTO.ReferralLog referralLog)
        {
            IHttpActionResult response;

            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process - AddReferrer WebHook:{0}:", stopwatch.ElapsedMilliseconds);
                #endregion                

                #region Controller
                var controller = new QueryController();
                controller.logger = logger;
                logger.InfoFormat("Start-Call Controller");
                logger.InfoFormat("Start-Add Referrer details");

                //Map Token To TokenRS
                TokenRS token = new TokenRS();
                token.Username = referralLog.Token.Username;
                token.SecurityKey = referralLog.Token.SecurityKey;                

                //check if already registered on referral log                
                bool existPhoneNumber = false;
                using (var db = new DataAccess.VTOD.VTODEntities())
                {
                    existPhoneNumber = db.vtod_referral_logs.Any(x => x.RefereePhoneNumber.Equals(referralLog.RefereePhoneNumber));
                }

                if (!controller.IsPhoneTaken(token, referralLog.RefereePhoneNumber) && !existPhoneNumber)
                {
                    vtod_referral_logs newLog = new vtod_referral_logs();
                    newLog.RefereePhoneNumber = referralLog.RefereePhoneNumber;
                    newLog.ReferrerSDSMemberId = Convert.ToInt32(Utilities.GetNumberFromLetter(referralLog.ReferralCode));
                    newLog.CreatedOn = DateTime.Now;

                    //Domain looks like a repository (Why did we name it as domain?)
                    ReferralDomain referralDomain = new ReferralDomain();
                    referralDomain.AddReferral(newLog);


                    //Sending SMS
                    var account = System.Configuration.ConfigurationManager.AppSettings["Notification_SMS_Twilio_Account"];
                    var twilioToken = System.Configuration.ConfigurationManager.AppSettings["Notification_SMS_Twilio_Token"];
                    var from = System.Configuration.ConfigurationManager.AppSettings["Notification_SMS_Twilio_From"];

                    MessageController messageController = new MessageController();
                    TwilioSMSMessage twilioSMSMessage = new TwilioSMSMessage();

                    twilioSMSMessage.Account = account;
                    twilioSMSMessage.Token = twilioToken;
                    twilioSMSMessage.From = from;
                    twilioSMSMessage.To = new List<string> { referralLog.RefereePhoneNumber.CleanPhone() };
                    twilioSMSMessage.MessageBody = System.Configuration.ConfigurationManager.AppSettings["zTripDownLoadLinkAndMsg"];

                    var errorMessage = string.Empty;

                    if (messageController.SendTwilioSMSMessage(twilioSMSMessage, out errorMessage))
                    {
                        logger.InfoFormat("Referral Link SMS sent for Phone#: {0} sent successfully", referralLog.RefereePhoneNumber);                      

                    }
                    else
                    {
                        logger.ErrorFormat("Error for Referral Link sending SMS for Phone#: {0}", referralLog.RefereePhoneNumber);                    
                    }
                }
                else
                {
                    throw VtodException.CreateException(ExceptionType.Utility, 9019);
                }
                
                logger.InfoFormat("End-Add Referrer details");
                logger.InfoFormat("End-Call Controller");
                #endregion
                

                stopwatch.Stop();
                logger.InfoFormat("End the Process - AddReferrer WebHook : {0}:", stopwatch.ElapsedMilliseconds);
                var result = new ReferrerRS { Success = new Model.Success() };
                return Ok(result);
            }
            catch (VtodException ex)
            {

                logger.Error(ex);
                logger.Error(ex.ExceptionMessage.Code);
                response = ResponseMessage((ToHttpException(ex, Request)));
                return response;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                response = ResponseMessage(ToHttpException(VtodException.CreateException(ExceptionType.Auth, 1), Request));
                return response;
            }
        }

        #region IsReferral Enable
        /// <summary>
        /// This method is just for zTrip and needs memberID.
        /// If we need to expand this method for non zTrip user, we need to complete Verification/UserPhone method
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <param name="memberID"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Referral/WebHook/IsReferralEnable")]
        public IHttpActionResult IsReferralEnable()
        {
            IHttpActionResult response;
            try
            {
                int IsReferralActive = System.Configuration.ConfigurationManager.AppSettings["EnableReferral"].ToInt32();
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process - IsReferralEnable of getting the inputs:{0}:", stopwatch.ElapsedMilliseconds);
                #endregion
                bool? isReferralActive;
                #region Get Header
                var token = new TokenRS();
                var userName = Request.Headers.Where(s => s.Key == "Username").FirstOrDefault();
                var securityKey = Request.Headers.Where(s => s.Key == "SecurityKey").FirstOrDefault();
                logger.InfoFormat("Username:", userName.Value.FirstOrDefault());
                logger.InfoFormat("securityKey:", securityKey.Value.FirstOrDefault());
                try
                {
                    if (userName.Key != null)
                        token.Username = userName.Value.FirstOrDefault();
                }
                catch (Exception ex)
                {
                    logger.Error("UserName:", ex);
                }
                try
                {
                    if (securityKey.Key != null)
                        token.SecurityKey = securityKey.Value.FirstOrDefault();
                }
                catch (Exception ex)
                {
                    logger.Error("SecurityKey:", ex);
                }
                #endregion

                #region Call Controller
                if (IsReferralActive == 1)
                {
                    logger.InfoFormat("Referral is On");
                    isReferralActive = true;
                }
                else
                {
                    logger.InfoFormat("Referral is Off");
                    isReferralActive = false;
                }
                #endregion

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process - IsReferralEnable : {0}:", stopwatch.ElapsedMilliseconds);
                #endregion
                var result = new ReferralActiveRS { IsReferralEnable = isReferralActive, Success = new Model.Success() };
                return Ok(result);
            }

            catch (VtodException ex)
            {

                logger.Error(ex);
                response = ResponseMessage((ToHttpException(ex, Request)));
                return response;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                response = ResponseMessage(ToHttpException(VtodException.CreateException(ExceptionType.Auth, 1), Request));
                return response;
            }
        }
        #endregion
        		
	}
    
}
