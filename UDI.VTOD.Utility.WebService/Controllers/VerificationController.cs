﻿using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UDI.VTOD.Common.DTO;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Utility.Application;
using UDI.VTOD.Utility.Common.Contracts;
using UDI.VTOD.Utility.Model;
using UDI.Utility.Serialization;
using System.Text.RegularExpressions;
using UDI.VTOD.Common.Helper;
using UDI.Utility.Helper;
namespace UDI.VTOD.Utility.WebService.Controllers
{
	public class VerificationController : BaseController
	{
		#region Fields
		public ILog logger;
		#endregion

		#region Constructor
		public VerificationController()
		{
			logger = LogManager.GetLogger(this.GetType());
			log4net.Config.XmlConfigurator.Configure();
		}
		#endregion

		#region Verify Phone
		/// <summary>
		/// This method is just for zTrip and needs memberID.
		/// If we need to expand this method for non zTrip user, we need to complete Verification/UserPhone method.
		/// </summary>
		/// <param name="type"></param>
		/// <param name="phoneNumber"></param>
		/// <param name="memberID"></param>
		/// <returns></returns>
		[HttpGet]
        [Route("Verification/Phone/{type}/{phoneNumber}/{memberID:int}/{VerifyType?}")]
        public IHttpActionResult VerifyPhone(string type, string phoneNumber, int memberID, string VerifyType = null)
		{
            IHttpActionResult response;
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
                int IsPhoneVerificationRequired = System.Configuration.ConfigurationManager.AppSettings["EnablePhoneVerification"].ToInt32();
				logger.InfoFormat("Start the Process - VerifyPhone of getting the inputs:{0}:", stopwatch.ElapsedMilliseconds);
				#endregion

				#region Get Header
				var token = new TokenRS();
				var userName = Request.Headers.Where(s => s.Key == "Username").FirstOrDefault();
				var securityKey = Request.Headers.Where(s => s.Key == "SecurityKey").FirstOrDefault();
				logger.InfoFormat("Username:", userName.Value.FirstOrDefault());
				logger.InfoFormat("securityKey:", securityKey.Value.FirstOrDefault());
				try
				{
					if (userName.Key != null)
						token.Username = userName.Value.FirstOrDefault();
				}
				catch (Exception ex)
				{
					logger.Error("UserName:", ex);
				}
				try
				{
					if (securityKey.Key != null)
						token.SecurityKey = securityKey.Value.FirstOrDefault();
				}
				catch (Exception ex)
				{
					logger.Error("SecurityKey:", ex);
				}

				#endregion
               
				#region Call Controller
                if (IsPhoneVerificationRequired == 1)
                {
                    logger.InfoFormat("Phone Verification is On");
                    logger.InfoFormat("Start-Call Controller");
                    var controller = new QueryController();
                    controller.logger = logger;

                    logger.InfoFormat("Start-Verify Phone Number Pattern");

                    controller.VerifyPhoneNumberPattern(token, phoneNumber);

                    logger.InfoFormat("End-Verify Phone Number Pattern");
                    logger.InfoFormat("Start- Method PhoneSMSVerification");
                    controller.PhoneSMSVerification(token, type, phoneNumber, memberID, VerifyType);
                    logger.InfoFormat("End-Method PhoneSMSVerification");
                    logger.InfoFormat("End-Call Controller");
                }
                else
                {
                    logger.InfoFormat("Phone Verification is Off");
                }
				#endregion
				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process - VerifyPhone : {0}:", stopwatch.ElapsedMilliseconds);
				#endregion
                var result = new VerifyPhoneRS { Success = new Model.Success() };
                return Ok(result);
			}
            catch (VtodException ex)
            {

                logger.Error(ex);
                logger.Error(ex.ExceptionMessage.Code);
                response = ResponseMessage((ToHttpException(ex, Request)));
                return response;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                response = ResponseMessage(ToHttpException(VtodException.CreateException(ExceptionType.Auth, 1), Request));
                return response;
            }
		}

		#endregion

        #region Code Verify Phone
        /// <summary>
        /// This method is just for zTrip and needs memberID.
        /// If we need to expand this method for non zTrip user, we need to complete Verification/UserPhone method.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="phoneNumber"></param>
        /// <param name="memberID"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Verification/Phone/Code/{type}/{phoneNumber}/{memberID:int}/{verificationCode?}")]
        public IHttpActionResult VerifyCode(string type, string phoneNumber, int memberID, string verificationCode=null)
        {
            IHttpActionResult response;
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                int IsPhoneVerificationRequired = System.Configuration.ConfigurationManager.AppSettings["EnablePhoneVerification"].ToInt32();
                logger.InfoFormat("Start the Process - VerifyCode of getting the inputs:{0}:", stopwatch.ElapsedMilliseconds);
                #endregion

                #region Get Header
                var token = new TokenRS();
                var userName = Request.Headers.Where(s => s.Key == "Username").FirstOrDefault();
                var securityKey = Request.Headers.Where(s => s.Key == "SecurityKey").FirstOrDefault();
                logger.InfoFormat("Username:", userName.Value.FirstOrDefault());
                logger.InfoFormat("securityKey:", securityKey.Value.FirstOrDefault());
                try
                {
                    if (userName.Key != null)
                        token.Username = userName.Value.FirstOrDefault();
                }
                catch (Exception ex)
                {
                    logger.Error("UserName:", ex);
                }
                try
                {
                    if (securityKey.Key != null)
                        token.SecurityKey = securityKey.Value.FirstOrDefault();
                }
                catch (Exception ex)
                {
                    logger.Error("SecurityKey:", ex);
                }
                if (string.IsNullOrEmpty(verificationCode))
                {
                    throw VtodException.CreateException(ExceptionType.Utility, 9017);
                }
                #endregion

                #region Call Controller
                if (IsPhoneVerificationRequired == 1)
                {
                    logger.InfoFormat("Phone Verification is On");
                    logger.InfoFormat("Start-Call Controller");
                    var controller = new QueryController();
                    controller.logger = logger;

                    logger.InfoFormat("Start-Verify Phone Number Pattern");

                    controller.VerifyPhoneNumberPattern(token, phoneNumber);

                    logger.InfoFormat("End-Verify Phone Number Pattern");
                    logger.InfoFormat("Start- Method VerifyCode");
                    controller.CodeVerification(token, type, phoneNumber, memberID, verificationCode);
                    logger.InfoFormat("End-Method VerifyCode");
                    logger.InfoFormat("End-Call Controller");
                }
                else
                {
                    logger.InfoFormat("Phone Verification is Off");
                }
                #endregion
                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process - VerifyPhone : {0}:", stopwatch.ElapsedMilliseconds);
                #endregion
                var result = new VerifyCodeRS { Success = new Model.Success() };
                return Ok(result);
            }
            catch (VtodException ex)
            {

                logger.Error(ex);
                logger.Error(ex.ExceptionMessage.Code);
                response = ResponseMessage((ToHttpException(ex, Request)));
                return response;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                response = ResponseMessage(ToHttpException(VtodException.CreateException(ExceptionType.Auth, 1), Request));
                return response;
            }
        }

        #endregion

		#region IsVerfied
		/// <summary>
		/// This method is just for zTrip and needs memberID.
		/// If we need to expand this method for non zTrip user, we need to complete Verification/UserPhone method
		/// </summary>
		/// <param name="phoneNumber"></param>
		/// <param name="memberID"></param>
		/// <returns></returns>
		[HttpGet]
		[Route("Verification/Phone/IsVerified/{phoneNumber}/{memberID:int}")]
		public IHttpActionResult IsPhoneVerified(string phoneNumber, int memberID)
		{
            IHttpActionResult response;
            try
            {
                #region Init
                int IsPhoneVerificationRequired = System.Configuration.ConfigurationManager.AppSettings["EnablePhoneVerification"].ToInt32();
                bool? isVerify; 
                #endregion
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process - IsPhoneVerified of getting the inputs:{0}:", stopwatch.ElapsedMilliseconds);
                logger.InfoFormat("IsPhoneVerificationRequired Value in Web Config [0]:", IsPhoneVerificationRequired);
				#endregion

				#region Get Header
				var token = new TokenRS();
				var userName = Request.Headers.Where(s => s.Key == "Username").FirstOrDefault();
				var securityKey = Request.Headers.Where(s => s.Key == "SecurityKey").FirstOrDefault();
				logger.InfoFormat("Username:", userName.Value.FirstOrDefault());
				logger.InfoFormat("securityKey:", securityKey.Value.FirstOrDefault());
				try
				{
					if (userName.Key != null)
						token.Username = userName.Value.FirstOrDefault();
				}
				catch (Exception ex)
				{
					logger.Error("UserName:", ex);
				}
				try
				{
					if (securityKey.Key != null)
						token.SecurityKey = securityKey.Value.FirstOrDefault();
				}
				catch (Exception ex)
				{
					logger.Error("SecurityKey:", ex);
				}
				#endregion

				#region Call Controller
                logger.InfoFormat("Start-Call Controller");
				var controller = new QueryController();
				controller.logger = logger;
              
                if (IsPhoneVerificationRequired == 1)
                {
                    logger.InfoFormat("Phone Verification is On");
                    logger.InfoFormat("Start-Verify Phone Number Pattern");
                    controller.VerifyPhoneNumberPattern(token, phoneNumber);
                    logger.InfoFormat("End-Verify Phone Number Pattern");
                    isVerify = controller.IsPhoneVerify(token, phoneNumber, memberID);
                }
                else
                {
                    logger.InfoFormat("Phone Verification is Off");
                    isVerify = true;
                }
                logger.InfoFormat("IsVerified : {0}:", isVerify);
				var result = new IsVerifiedRS { Verified = isVerify , Success = new Model.Success() };
                logger.InfoFormat("End-Call Controller");
				#endregion

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process - IsPhoneVerified : {0}:", stopwatch.ElapsedMilliseconds);
				#endregion
				return Ok(result);
            }
        
            catch (VtodException ex)
            {

                logger.Error(ex);
                response = ResponseMessage((ToHttpException(ex, Request)));
                return response;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                response = ResponseMessage(ToHttpException(VtodException.CreateException(ExceptionType.Auth, 1), Request));
                return response;
            }
		}

		#endregion

        #region IsRegisteredPhoneVerified
        /// <summary>
        /// This method is just for zTrip and needs memberID.
        /// If we need to expand this method for non zTrip user, we need to complete Verification/UserPhone method
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <param name="memberID"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Verification/Phone/IsPhoneTaken/{phoneNumber}")]
        public IHttpActionResult IsPhoneTaken(string phoneNumber)
        {
            IHttpActionResult response;
            try
            {
                int IsPhoneVerificationRequired = System.Configuration.ConfigurationManager.AppSettings["EnablePhoneVerification"].ToInt32();
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process - IsPhoneTaken of getting the inputs:{0}:", stopwatch.ElapsedMilliseconds);
                #endregion
                bool isVerify;
                #region Get Header
                var token = new TokenRS();
                var userName = Request.Headers.Where(s => s.Key == "Username").FirstOrDefault();
                var securityKey = Request.Headers.Where(s => s.Key == "SecurityKey").FirstOrDefault();
                logger.InfoFormat("Username:", userName.Value.FirstOrDefault());
                logger.InfoFormat("securityKey:", securityKey.Value.FirstOrDefault());
                try
                {
                    if (userName.Key != null)
                        token.Username = userName.Value.FirstOrDefault();
                }
                catch (Exception ex)
                {
                    logger.Error("UserName:", ex);
                }
                try
                {
                    if (securityKey.Key != null)
                        token.SecurityKey = securityKey.Value.FirstOrDefault();
                }
                catch (Exception ex)
                {
                    logger.Error("SecurityKey:", ex);
                }
                #endregion

                #region Call Controller
                //if (IsPhoneVerificationRequired == 1)
                //{
                    logger.InfoFormat("Phone Verification is On");
                    logger.InfoFormat("Start-Call Controller");
                    var controller = new QueryController();
                    controller.logger = logger;
                    if (IsPhoneVerificationRequired == 1)
                    {
                        logger.InfoFormat("Start-Verify Phone Number Pattern");
                        controller.VerifyPhoneNumberPattern(token, phoneNumber);
                        logger.InfoFormat("End-Verify Phone Number Pattern");
                    }
                    isVerify = controller.IsPhoneTaken(token, phoneNumber);
                    logger.InfoFormat("IsPhoneTaken : {0}:", isVerify);
                    logger.InfoFormat("End-Call Controller");
                //}
                //else
                //{
                   // logger.InfoFormat("Phone Verification is Off");
                    //isVerify = true;
                //}
                #endregion

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process - IsPhoneTaken : {0}:", stopwatch.ElapsedMilliseconds);
                #endregion
                var result = new IsPhoneTakenRS { IsPhoneTaken = isVerify, Success = new Model.Success() };
                return Ok(result);
            }

            catch (VtodException ex)
            {

                logger.Error(ex);
                response = ResponseMessage((ToHttpException(ex, Request)));
                return response;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                response = ResponseMessage(ToHttpException(VtodException.CreateException(ExceptionType.Auth, 1), Request));
                return response;
            }
        }

        [HttpGet]
        [Route("Verification/Version/{App}/{Platform}/{AppVersion}/{PlatformVersion}")]
        public IHttpActionResult VerifyAppVersion(string App,string Platform,string AppVersion,string PlatformVersion)
        {
            IHttpActionResult response;
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process - VerifyAppVersion of getting the inputs:{0}:", stopwatch.ElapsedMilliseconds);
                #endregion
                IsVersionActiveRS isVerify;
                #region Get Header
                var token = new TokenRS();
                var userName = Request.Headers.Where(s => s.Key == "Username").FirstOrDefault();
                var securityKey = Request.Headers.Where(s => s.Key == "SecurityKey").FirstOrDefault();
                logger.InfoFormat("Username:", userName.Value.FirstOrDefault());
                logger.InfoFormat("securityKey:", securityKey.Value.FirstOrDefault());
                try
                {
                    if (userName.Key != null)
                        token.Username = userName.Value.FirstOrDefault();
                }
                catch (Exception ex)
                {
                    logger.Error("UserName:", ex);
                }
                try
                {
                    if (securityKey.Key != null)
                        token.SecurityKey = securityKey.Value.FirstOrDefault();
                }
                catch (Exception ex)
                {
                    logger.Error("SecurityKey:", ex);
                }
                #endregion

                #region Call Controller
                logger.InfoFormat("Start-Call Controller");
                var controller = new QueryController();
                controller.logger = logger;
                logger.InfoFormat("Start-Verify App Version Of the Controller");
                isVerify = controller.IsVerifyAppVersion(token, App,Platform,AppVersion,PlatformVersion);
                isVerify.Success = new Model.Success();
                logger.InfoFormat("End-Verify App Version Of the Controller");
                logger.InfoFormat("VerifyAppVersion : {0}:", isVerify);
                logger.InfoFormat("End-Call Controller");
                #endregion

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process - IsPhoneTaken : {0}:", stopwatch.ElapsedMilliseconds);
                #endregion
                return Ok(isVerify);
            }

            catch (VtodException ex)
            {

                logger.Error(ex);
                response = ResponseMessage((ToHttpException(ex, Request)));
                return response;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                response = ResponseMessage(ToHttpException(VtodException.CreateException(ExceptionType.Auth, 1), Request));
                return response;
            }
        }
        #endregion

        #region DeletePhone
        [HttpDelete]
        [Route("Verification/Phone/{phoneNumber}/{memberID:int}")]
        public IHttpActionResult DeletePhone(string phoneNumber, int memberID)
        {
            IHttpActionResult response;
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process - DeletePhone of getting the inputs:{0}:", stopwatch.ElapsedMilliseconds);
                #endregion

                #region Get Header
                var token = new TokenRS();
                var userName = Request.Headers.Where(s => s.Key == "Username").FirstOrDefault();
                var securityKey = Request.Headers.Where(s => s.Key == "SecurityKey").FirstOrDefault();
                logger.InfoFormat("Username:", userName.Value.FirstOrDefault());
                logger.InfoFormat("securityKey:", securityKey.Value.FirstOrDefault());
                try
                {
                    if (userName.Key != null)
                        token.Username = userName.Value.FirstOrDefault();
                }
                catch (Exception ex)
                {
                    logger.Error("UserName:", ex);
                }
                try
                {
                    if (securityKey.Key != null)
                        token.SecurityKey = securityKey.Value.FirstOrDefault();
                }
                catch (Exception ex)
                {
                    logger.Error("SecurityKey:", ex);
                }

                #endregion

                #region Call Controller
                logger.InfoFormat("Start-Call Controller");
                var controller = new QueryController();
                controller.logger = logger;
                logger.InfoFormat("Start-Verify Phone Number Pattern");
                controller.VerifyPhoneNumberPattern(token, phoneNumber);
                logger.InfoFormat("End-Verify Phone Number Pattern");
                logger.InfoFormat("Start- Method DeletePhone");
                bool? res= controller.DeletePhone(token,phoneNumber, memberID);
                logger.InfoFormat("End-Method DeletePhone");
                logger.InfoFormat("End-Call Controller");
                #endregion

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process - DeletePhone : {0}:", stopwatch.ElapsedMilliseconds);
                #endregion
                return Ok("Success");
            }
            catch (VtodException ex)
            {

                logger.Error(ex);
                logger.Error(ex.ExceptionMessage.Code);
                response = ResponseMessage((ToHttpException(ex, Request)));
                return response;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                response = ResponseMessage(ToHttpException(VtodException.CreateException(ExceptionType.Auth, 1), Request));
                return response;
            }
        }
        
        #endregion
       
	}
}
