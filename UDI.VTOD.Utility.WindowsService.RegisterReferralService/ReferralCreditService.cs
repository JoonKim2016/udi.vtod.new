﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using log4net;
using UDI.VTOD.Utility.Application;
using UDI.SDS.MembershipService;
using UDI.VTOD.Utility.DataAccess.VTOD;
using UDI.VTOD.Utility.Common.Cost;
using UDI.VTOD.Common.DTO.OTA;

namespace UDI.VTOD.WindowsService.ReferralCreditService
{
	partial class ReferralCreditService : ServiceBase
	{
		#region Fields
		private System.Timers.Timer timer = new System.Timers.Timer();
		static ILog log;
		static bool isRunning;
		//static int test = 0;
		#endregion

		#region Console
		static void Main(string[] args)
		{
			ReferralCreditService service = new ReferralCreditService();

			if (Environment.UserInteractive)
			{
				service.OnStart(args);
				Console.WriteLine("Press any key to stop program");
				Console.Read();
				service.OnStop();
			}
			else
			{
				ServiceBase.Run(service);
			}

		}
		#endregion

		#region Constructors
        public ReferralCreditService()
        {
            isRunning = false;
            InitializeComponent();
            log4net.Config.XmlConfigurator.Configure();
            log = LogManager.GetLogger(typeof(ReferralCreditService));

            //#region EventLog
            //if (!System.Diagnostics.EventLog.SourceExists("VTODTStatus"))
            //{
            //    System.Diagnostics.EventLog.CreateEventSource(
            //        "VTODTStatus", "VTODTStatus");
            //}
            //eventLog.Source = "VTODTStatus";
            //eventLog.Log = "VTODTStatus";
            //#endregion
        }
		#endregion

		#region Event
		protected override void OnStart(string[] args)
		{
            //eventLog.WriteEntry(DateTime.Now.ToString() + " ReferralCreditService has been started...");
            //(DateTime.Now.ToString() + " ReferralCreditService has been started...").WriteToConsole();
            log.Info(DateTime.Now.ToString() + " ReferralCreditService has been started...");
			timer.AutoReset = true;
			timer.Interval = 5;
			timer.Elapsed += OnElapsedEvent;
			timer.Start();
		}

		protected override void OnStop()
		{
			timer.Stop();
            eventLog.WriteEntry(DateTime.Now.ToString() + " ReferralCreditService has been stopped...");
            //(DateTime.Now.ToString() + " ReferralCreditService has been stopped...").WriteToConsole();
            log.Info(DateTime.Now.ToString() + " ReferralCreditService has been Stopped...");
		}
		#endregion

		#region Timer function
		static void OnElapsedEvent(object sender, ElapsedEventArgs e)
		{
            int referralEnabled = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["EnableReferral"]);

            #region Set interval from app.config
            if (((System.Timers.Timer)sender).Interval == 5)
			{
				var interval = 10000;

				try
				{
					interval = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["Interval"]);
					((System.Timers.Timer)sender).Interval = interval;
					Console.WriteLine("timer.Interval = " + interval.ToString());
				}
				catch (Exception)
				{
				}
			}
			#endregion

			if (!isRunning && referralEnabled==1)
			{
				isRunning = true;

                TokenRQ tokenRQ = new TokenRQ();

                #region Read From Config
                tokenRQ.Username = System.Configuration.ConfigurationManager.AppSettings["SDS_Username"];
                tokenRQ.Password = System.Configuration.ConfigurationManager.AppSettings["SDS_Password"];
                tokenRQ.Version = System.Configuration.ConfigurationManager.AppSettings["Version"];               
                #endregion

                //Modify Token
                Common.Helper.Modification.ModifyTokenRQ(ref tokenRQ);               

                var token = new TokenRS();

                //Validate Token
                var sds = new UDI.SDS.SecurityController(null, null);
                token = sds.GetToken(tokenRQ);
                if (token == null)
                {   
                    throw new Exception("Invalid Token Object");                 
                }
                
                var membershipController = new UDI.SDS.MembershipController(token, null);

                //Getting qualified Trips/members
                using (var db = new Utility.DataAccess.VTOD.VTODEntities())
                {
                    #region FirstTripCredit
                    DateTime currentTime = DateTime.Now;
                    decimal firstTripCreditAmount = db.vtod_referral_credit_configuration
                                            
                                            .Where(x => x.ReferralType == (int)ZTripCreditType.ReferralTripCompleted
                                                     && x.StartDate < currentTime && x.EndDate > currentTime)
                                            .Select(x => x.CreditAmount).FirstOrDefault();
                    //TripStatus  will be  'Charged' or 'SDSTrip-Completed' or NULL  
                    //if TripStatus is Null, We can't claim
                    var referralTripsNeedToGetCredit = db.ReferralTrips.Where(x => !string.IsNullOrEmpty(x.TripStatus)
                                                                          && (x.ReferralCreditFlow == (int)ReferrerCreditFlow.Credit_Inprogress || x.ReferralCreditFlow == (int)ReferrerCreditFlow.Credit_SDS_Error)
                                                                          && !x.CreditEarnedOn.HasValue).ToList();
                    

                    foreach (ReferralTrip trip in referralTripsNeedToGetCredit)
                    {
                        trip.ReferralCreditAmount = trip.ReferralCreditAmount.HasValue ? trip.ReferralCreditAmount.Value : firstTripCreditAmount;

                        var creditResult = membershipController.CreateNewZTripCredit(trip.ReferralCreditAmount.Value, trip.ReferrerSDSMemberID,ZTripCreditType.ReferralTripCompleted);

                        trip.UpdatedOn = DateTime.Now;

                        if (creditResult.Contains("Success"))
                        {
                            trip.CreditEarnedOn = DateTime.Now;
                            trip.ReferralCreditFlow = ReferrerCreditFlow.Credit_Completed;
                        }
                        else
                        {
                            //Error in SDS
                            trip.ErrorMessage = "Error in SDS";                            
                            trip.ReferralCreditFlow = ReferrerCreditFlow.Credit_SDS_Error;
                        }
                    }
                    #endregion

                    #region  New Member Referral Credit

                    List<vtod_member_info> memberNeedToGetCredit = db.vtod_member_info.Where(x => x.ReferralCreditAmount.HasValue && !x.CreditEarnedOn.HasValue).ToList();

                    var verificationDomain = new UDI.VTOD.Utility.Domain.Verification.VerificationDomain();                    
                    
                    foreach (vtod_member_info memberCredit in memberNeedToGetCredit)
                    {                        
                        if (verificationDomain.IsPhoneVerify(memberCredit.FirstRegisteredPhoneNumber, memberCredit.SDSMemberId) ?? false)
                        {
                            var creditResult = membershipController.CreateNewZTripCredit(memberCredit.ReferralCreditAmount.Value, memberCredit.SDSMemberId, ZTripCreditType.ReferralNewUser);

                            if (creditResult.Contains("Success"))
                            {
                                memberCredit.CreditEarnedOn = DateTime.Now;
                                log.InfoFormat("MemberID:{0} , got the ${1} credit ",memberCredit.SDSMemberId, memberCredit.ReferralCreditAmount.Value);
                            }
                        }
                        else
                        {
                            log.InfoFormat("MemberID:{0} , Phone Number {1} has not been verified yet!", memberCredit.SDSMemberId, memberCredit.FirstRegisteredPhoneNumber);
                        }
                    }
                    #endregion

                    db.SaveChanges();
                }

                isRunning = false;
			}
		}
        #endregion
    }
    
}
