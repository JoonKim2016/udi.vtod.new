﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using log4net;
using UDI.VTOD.Utility.Application;
using UDI.VTOD.Common.DTO.OTA;

namespace UDI.VTOD.WindowsService.SMSOutboundService
{
	partial class SMSOutboundService : ServiceBase
	{
		#region Fields
		private System.Timers.Timer timer = new System.Timers.Timer();
		static ILog log;
		static bool isRunning;
		//static int test = 0;
		#endregion

		#region Console
		static void Main(string[] args)
		{
			SMSOutboundService service = new SMSOutboundService();

			if (Environment.UserInteractive)
			{
				service.OnStart(args);
				Console.WriteLine("Press any key to stop program");
				Console.Read();
				service.OnStop();
			}
			else
			{
				ServiceBase.Run(service);
			}

		}
		#endregion

		#region Constructors
        public SMSOutboundService()
        {
            isRunning = false;
            InitializeComponent();
            log4net.Config.XmlConfigurator.Configure();
            log = LogManager.GetLogger(typeof(SMSOutboundService));
        }
		#endregion

		#region Event
		protected override void OnStart(string[] args)
		{
			//eventLog.WriteEntry(DateTime.Now.ToString() + " SMSOutboundService has been started...");
			//(DateTime.Now.ToString() + " SMSOutboundService has been started...").WriteToConsole();
			log.Info(DateTime.Now.ToString() + " SMSOutboundService has been started...");
			timer.AutoReset = true;
			timer.Interval = 5;
			timer.Elapsed += OnElapsedEvent;
			timer.Start();
		}

		protected override void OnStop()
		{
			timer.Stop();
			eventLog.WriteEntry(DateTime.Now.ToString() + " SMSOutboundService has been stopped...");
			//(DateTime.Now.ToString() + " SMSOutboundService has been stopped...").WriteToConsole();
			log.Info(DateTime.Now.ToString() + " SMSOutboundService has been Stopped...");
		}
		#endregion

		#region Timer function
		static void OnElapsedEvent(object sender, ElapsedEventArgs e)
		{

			#region Set interval from app.config
			if (((System.Timers.Timer)sender).Interval == 5)
			{
				var interval = 10000;

				try
				{
					interval = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["Interval"]);
					((System.Timers.Timer)sender).Interval = interval;
					Console.WriteLine("timer.Interval = " + interval.ToString());
				}
				catch (Exception)
				{
				}
			}
			#endregion

			if (!isRunning)
			{
				isRunning = true;

                TokenRQ token = new TokenRQ();

                #region Read From Config
                token.Username = System.Configuration.ConfigurationManager.AppSettings["SDS_Username"];
                token.Password = System.Configuration.ConfigurationManager.AppSettings["SDS_Password"];
                token.Version = System.Configuration.ConfigurationManager.AppSettings["Version"];
               
                #endregion
                #region Modify Token
                UDI.VTOD.Common.Helper.Modification.ModifyTokenRQ(ref token); 
                #endregion
                var queryController = new QueryController();
                queryController.ProcessSMSOutbound(token);
                isRunning = false;
			}
		}
		#endregion
	}
}
