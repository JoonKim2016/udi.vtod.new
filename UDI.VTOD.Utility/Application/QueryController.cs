﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Utility.Domain.Verification;
using UDI.VTOD.Utility.Common.DTO;
using UDI.VTOD.Utility.Common;
using UDI.VTOD.Common.DTO.OTA;
using log4net;
using UDI.VTOD.Utility.Common.Contracts;
using UDI.VTOD.Common.DTO;
using UDI.Utility.Helper;
using UDI.VTOD.Utility.Common.Const;
using UDI.VTOD.Utility.DataAccess.VTOD;
using UDI.VTOD.Utility.Domain.Push;
using UDI.VTOD.Utility.Domain.VTODWebService;
using UDI.VTOD.Utility.Model;
namespace UDI.VTOD.Utility.Application
{
    #region Class

    public class QueryController : BaseSetting
    {
        #region Lock Objects

        private static object smsInboundLocker = new object();
        private static object smsOutboundLocker = new object();
        private static object referralRegisterLocker = new object();
        private static object checkTripStatusLocker = new object();
        private static readonly object pushNotificationLocker = new object();

        #endregion
        
        #region Web Service

        public void PhoneSMSVerification(TokenRS token, string type, string phoneNumber, Int64 MemberID, string VerifyType)
        {
            try
            {
                #region Init

                bool isAuthenticate = false;
                var sds = new UDI.SDS.SecurityController(null, null);

                #endregion

                #region Authenticate

                if (isDebug)
                {
                    isAuthenticate = true;
                }
                else
                {
                    isAuthenticate = sds.ValidateToken(token.Username, token.SecurityKey.ToGuid());
                }

                if (!isAuthenticate)
                {
                    logger.InfoFormat("User Authentication Failure");
                    throw new Exception("Error");

                }
                logger.InfoFormat("User Authentication Success");

                #endregion

                #region Call Domain Method

                var verificationDomain = new UDI.VTOD.Utility.Domain.Verification.VerificationDomain();
                logger.InfoFormat("Start of the Process PhoneSMS Insertion");
                verificationDomain.PhoneSMSInsertion(type, phoneNumber, MemberID, VerifyType);
                logger.InfoFormat("End of the Process PhoneSMS Insertion");

                #endregion
            }
            catch (VtodException ex)
            {
                logger.Error("PhoneSMSInsertion:", ex);
                throw (ex);

            }
            catch (Exception ex)
            {
                logger.Error("VerifyPhone:", ex);
                throw ex;
            }
        }

        public void CodeVerification(TokenRS token, string type, string phoneNumber, Int64 MemberID, string VerificationCode)
        {
            try
            {
                #region Init

                bool isAuthenticate = false;
                var sds = new UDI.SDS.SecurityController(null, null);

                #endregion

                #region Authenticate

                if (isDebug)
                {
                    isAuthenticate = true;
                }
                else
                {
                    isAuthenticate = sds.ValidateToken(token.Username, token.SecurityKey.ToGuid());
                }

                if (!isAuthenticate)
                {
                    logger.InfoFormat("User Authentication Failure");
                    throw new Exception("Error");

                }
                logger.InfoFormat("User Authentication Success");

                #endregion

                #region Modify Token
                UDI.VTOD.Common.Helper.Modification.ModifyTokenRS(ref token);
                #endregion

                #region Call Domain Method

                var verificationDomain = new UDI.VTOD.Utility.Domain.Verification.VerificationDomain();
                logger.InfoFormat("Start of the Process CodeVerification");
                verificationDomain.CodeUpdation(type, phoneNumber, MemberID, VerificationCode, token);
                logger.InfoFormat("End of the Process CodeVerification");

                #endregion
            }
            catch (VtodException ex)
            {
                logger.Error("CodeVerification:", ex);
                throw (ex);

            }
            catch (Exception ex)
            {
                logger.Error("CodeVerification:", ex);
                throw ex;
            }
        }

        public bool? IsPhoneVerify(TokenRS token, string phoneNumber, Int64 MemberID)
        {
            try
            {
               
                #region Init

                bool isAuthenticate = false;
                var sds = new UDI.SDS.SecurityController(null, null);

                #endregion

                #region Authenticate

                if (isDebug)
                {
                    isAuthenticate = true;
                }
                else
                {
                    isAuthenticate = sds.ValidateToken(token.Username, token.SecurityKey.ToGuid());
                }

                if (!isAuthenticate)
                {
                    logger.InfoFormat("User Authentication Failure");
                    throw new Exception("Error");

                }
                logger.InfoFormat("User Authentication Success");

                #endregion


                #region Call Domain Method

                var verificationDomain = new UDI.VTOD.Utility.Domain.Verification.VerificationDomain();
                bool? isVerify = false;
                logger.InfoFormat("Start of the Process IsPhoneVerify : inputs phonenumber{0},memberID{1}:", phoneNumber,
                    MemberID);

                isVerify = verificationDomain.IsPhoneVerify(phoneNumber, MemberID);

                logger.InfoFormat("End of the Process IsPhoneVerify with the status :", isVerify);
                return isVerify;

                #endregion
            }
            catch (VtodException ex)
            {
                logger.Error("IsPhoneVerify:", ex);
                throw ex;
            }
            catch (Exception ex)
            {
                logger.Error("IsPhoneVerify:", ex);
                throw ex;
            }
        }

        public bool IsPhoneTaken(TokenRS token, string phoneNumber)
        {
            try
            {
                #region Init

                bool isAuthenticate = false;
                var sds = new UDI.SDS.SecurityController(null, null);

                #endregion

                #region Authenticate

                if (isDebug)
                {
                    isAuthenticate = true;
                }
                else
                {
                    isAuthenticate = sds.ValidateToken(token.Username, token.SecurityKey.ToGuid());
                }

                if (!isAuthenticate)
                {
                    logger.InfoFormat("User Authentication Failure");
                    throw new Exception("Error");

                }
                logger.InfoFormat("User Authentication Success");

                #endregion


                #region Call Domain Method

                var verificationDomain = new VerificationDomain();

                logger.InfoFormat("Start of the Process IsPhoneTaken : inputs phonenumber{0}:", phoneNumber);

                bool isVerify = verificationDomain.IsPhoneTaken(phoneNumber);

                logger.InfoFormat("End of the Process IsPhoneTaken with the status :", isVerify);
                return isVerify;

                #endregion
            }
            catch (VtodException ex)
            {
                logger.Error("IsPhoneTaken:", ex);
                throw ex;
            }
            catch (Exception ex)
            {
                logger.Error("IsPhoneTaken:", ex);
                throw ex;
            }
        }

        public void VerifyPhoneNumberPattern(TokenRS token, string phoneNumber)
        {
            try
            {
                #region Init

                bool isAuthenticate = false;
                var sds = new UDI.SDS.SecurityController(null, null);

                #endregion

                #region Authenticate

                if (isDebug)
                {
                    isAuthenticate = true;
                }
                else
                {
                    isAuthenticate = sds.ValidateToken(token.Username, token.SecurityKey.ToGuid());
                }

                if (!isAuthenticate)
                {
                    logger.InfoFormat("User Authentication Failure");
                    throw new Exception("Error");

                }
                logger.InfoFormat("User Authentication Success");

                #endregion

                #region Call Domain Method

                var verificationDomain = new UDI.VTOD.Utility.Domain.Verification.VerificationDomain();
                logger.InfoFormat("Start of the Process CountryCode/Area Code Verification");
                verificationDomain.VerifyPhoneNumberPattern(phoneNumber);
                logger.InfoFormat("End of the Process CountryCode/Area Code Verification");

                #endregion
            }
            catch (VtodException ex)
            {
                logger.Error("VerifyPhoneNumberPattern:", ex);
                logger.Error(ex.ExceptionMessage.Code);
                throw (ex);

            }
            catch (Exception ex)
            {
                logger.Error("VerifyPhoneNumberPattern:", ex);
                throw ex;
            }
        }
                

        public bool? DeletePhone(TokenRS token, string phoneNumber, Int64 MemberID)
        {
            try
            {
                #region Init

                bool isAuthenticate = false;
                var sds = new UDI.SDS.SecurityController(null, null);

                #endregion

                #region Authenticate

                if (isDebug)
                {
                    isAuthenticate = true;
                }
                else
                {
                    isAuthenticate = sds.ValidateToken(token.Username, token.SecurityKey.ToGuid());
                }

                if (!isAuthenticate)
                {
                    logger.InfoFormat("User Authentication Failure");
                    throw new Exception("Error");

                }
                logger.InfoFormat("User Authentication Success");

                #endregion

                #region Call Domain Method

                var verificationDomain = new UDI.VTOD.Utility.Domain.Verification.VerificationDomain();
                logger.InfoFormat("Start of the Delete Phone Number");
                bool? deletionStatus = verificationDomain.DeletePhone(phoneNumber, MemberID);
                logger.InfoFormat("End  of the Delete Phone Numbe");
                return deletionStatus;

                #endregion
            }
            catch (VtodException ex)
            {
                logger.Error("VerifyPhoneNumberPattern:", ex);
                logger.Error(ex.ExceptionMessage.Code);
                throw (ex);

            }
            catch (Exception ex)
            {
                logger.Error("VerifyPhoneNumberPattern:", ex);
                throw ex;
            }
        }


        public void AddNotification(TokenRS token, string Text, string PushType, string EmailAddress)
        {
            try
            {
                #region Init

                bool isAuthenticate = false;
                var sds = new UDI.SDS.SecurityController(null, null);

                #endregion

                #region Authenticate

                if (isDebug)
                {
                    isAuthenticate = true;
                }
                else
                {
                    isAuthenticate = sds.ValidateToken(token.Username, token.SecurityKey.ToGuid());
                }

                if (!isAuthenticate)
                {
                    logger.InfoFormat("User Authentication Failure");
                    throw new Exception("Error");

                }
                logger.InfoFormat("User Authentication Success");

                #endregion
				#region Modify Token
				UDI.VTOD.Common.Helper.Modification.ModifyTokenRS(ref token);
				#endregion
                #region Call Domain Method

                var notificationDomain = new UDI.VTOD.Utility.Domain.Push.NotificationDomain();
                logger.InfoFormat("Start of the Process Add Notification");
                notificationDomain.AddNotification(token,Text, PushType, EmailAddress);
                logger.InfoFormat("End of the Process Add Notification");

                #endregion
            }
            catch (VtodException ex)
            {
                logger.Error("AddNotification:", ex);
                logger.Error(ex.ExceptionMessage.Code);
                throw (ex);

            }
            catch (Exception ex)
            {
                logger.Error("AddNotification:", ex);
                throw ex;
            }
        }

        public IsVersionActiveRS IsVerifyAppVersion(TokenRS token, string App, string Platform, string AppVersion, string PlatformVersion)
        {
            try
            {
                #region Init

                bool isAuthenticate = false;
                var sds = new UDI.SDS.SecurityController(null, null);

                #endregion

                #region Authenticate

                if (isDebug)
                {
                    isAuthenticate = true;
                }
                else
                {
                    isAuthenticate = sds.ValidateToken(token.Username, token.SecurityKey.ToGuid());
                }

                if (!isAuthenticate)
                {
                    logger.InfoFormat("User Authentication Failure");
                    throw new Exception("Error");

                }
                logger.InfoFormat("User Authentication Success");

                #endregion


                #region Call Domain Method

                var verificationDomain = new UDI.VTOD.Utility.Domain.Verification.VerificationDomain();
                IsVersionActiveRS isVerify = null;
                logger.InfoFormat("Start of the Process IsVerifyAppVersion : inputs App {0}:,{1}:,{2}:,{3}:", App, Platform, AppVersion, PlatformVersion);

                isVerify = verificationDomain.IsVerifyAppVersion(App, Platform, AppVersion, PlatformVersion);

                logger.InfoFormat("End of the Process IsVerifyAppVersion with the status :", isVerify);
                return isVerify;

                #endregion
            }
            catch (VtodException ex)
            {
                logger.Error("IsVerifyAppVersion:", ex);
                throw ex;
            }
            catch (Exception ex)
            {
                logger.Error("IsVerifyAppVersion:", ex);
                throw ex;
            }
        }

        public AppConfigurationRS RetrieveConfiguration(TokenRS token, UDI.VTOD.Utility.Common.DTO.Configuration ConfigurationDetails)
        {
            try
            {
                #region Init

                bool isAuthenticate = false;
                var sds = new UDI.SDS.SecurityController(null, null);

                #endregion

                #region Authenticate

                if (isDebug)
                {
                    isAuthenticate = true;
                }
                else
                {
                    isAuthenticate = sds.ValidateToken(token.Username, token.SecurityKey.ToGuid());
                }

                if (!isAuthenticate)
                {
                    logger.InfoFormat("User Authentication Failure");
                    throw new Exception("Error");

                }
                logger.InfoFormat("User Authentication Success");

                #endregion


                #region Call Domain Method

                var configDomain = new UDI.VTOD.Utility.Domain.Configuration.ConfigurationDomain();
                AppConfigurationRS appConfiguration = null;
                logger.InfoFormat("Start of the Process RetrieveConfiguration");

                appConfiguration = configDomain.ConfigurationDetails(ConfigurationDetails);

                logger.InfoFormat("End of the Process RetrieveConfiguration");
                return appConfiguration;

                #endregion
            }
            catch (VtodException ex)
            {
                logger.Error("RetrieveConfiguration:", ex);
                throw ex;
            }
            catch (Exception ex)
            {
                logger.Error("RetrieveConfiguration:", ex);
                throw ex;
            }
        }
        #endregion

        #region Windows Services

        public void ProcessSMSInbound(TokenRQ tokenRQ)
        {
            try
            {
                #region InBound               

                #region Init

                var sds = new UDI.SDS.SecurityController(null, null);

                #endregion

                logger.InfoFormat("Windows Service start for the Inbound process");
                lock (smsInboundLocker)
                {
                    #region Get Token

                    var token = new TokenRS();

                    #region Validate Token

                    token = sds.GetToken(tokenRQ);
                    if (token == null)
                    {
                        #region Token Error

                        logger.InfoFormat("Invalid Token Object");
                        logger.InfoFormat("User Authentication Failure");
                        throw new Exception("Error");

                        #endregion
                    }

                    #endregion

                    #endregion

                    #region TPL

                    var threadNumber = System.Configuration.ConfigurationManager.AppSettings["ThreadNumber"].ToInt32();
                    List<Task> tasks = new List<Task>();

                    #region Call Domain Method

                    var verificationDomain = new UDI.VTOD.Utility.Domain.Verification.VerificationDomain();
                    logger.InfoFormat("Start of retrieving the phone numbers with the status 3");
                    List<PhoneInfo> verifications =
                        verificationDomain.ListPhoneSMSInserted(
                            UDI.VTOD.Utility.Common.Cost.VerificationFlow.Phone_SMS_Sent, 0, 0,
                            UDI.VTOD.Utility.Common.Cost.VerificationFlow.Phone_SMS_Exception);
                    logger.InfoFormat("List Of Records for InBound:{0}:,", verifications.Count());
                    logger.InfoFormat("End of retrieving the phone numbers with the status 3");

                    verifications = verifications.Where(p => p.VerificationType == UDI.VTOD.Utility.Common.Const.VerificationType.VerificationByText).ToList();
                    #endregion

                    #region Chunkify

                    logger.InfoFormat("Start - Dividing the List of phone numbers with respect to thread count");
                    var chunks = verifications.Chunkify(threadNumber).ToList();
                    logger.InfoFormat("End - Dividing the List of phone numbers with respect to thread count");

                    #endregion

                    #region Run TPL

                    foreach (var chunk in chunks)
                    {
                        #region Add Token Object To the PhoneCollection

                        TokenPhoneCollection boundToken = new TokenPhoneCollection();
                        boundToken.PhoneCollection.AddRange(chunk);
                        boundToken.Token = token;

                        #endregion

                        #region Add a Task

                        Task task = new Task(new Action<object>(processSMSInbound), boundToken,
                            TaskCreationOptions.AttachedToParent);
                        tasks.Add(task);

                        #endregion
                    }

                    if (tasks.Count > 0)
                    {
                        foreach (var item in tasks)
                        {
                            item.Start();
                        }
                    }
                    Task.WaitAll(tasks.ToArray());

                    #endregion

                    #region Finalize

                    #endregion

                    #endregion
                }
                logger.InfoFormat("Windows Service end for the Inbound process");

                #endregion
            }
            catch (Exception ex)
            {
                logger.Error("ProcessSMSInbound", ex);
                string.Format(DateTime.Now.ToString() + " Error: {0}", ex).WriteToConsole();
                //throw ex;
            }
        }

        public void ProcessSMSOutbound(TokenRQ tokenRQ)
        {
            try
            {
                #region Outbound

                #region Init

                var sds = new UDI.SDS.SecurityController(null, null);

                #endregion

                logger.InfoFormat("Windows Service start for the Outbound process");
                lock (smsOutboundLocker)
                {
                    #region Get Token

                    var token = new TokenRS();

                    #region Validate Token
                    token = sds.GetToken(tokenRQ);
                    if (token == null)
                    {
                        logger.InfoFormat("Invalid Token Object");
                        logger.InfoFormat("User Authentication Failure");
                        throw new Exception("Error");
                    }
                    #endregion

                    #endregion

                    #region TPL
                    var threadNumber = ConfigurationManager.AppSettings["ThreadNumber"].ToInt32();
                    List<Task> tasks = new List<Task>();

                    #region Call Domain Method

                    var verificationDomain = new VerificationDomain();
                    logger.InfoFormat("Start of retrieving the phone numbers with the status 1");
                    List<PhoneInfo> verifications =
                        verificationDomain.ListPhoneSMSInserted(
                            Common.Cost.VerificationFlow.Phone_SMS_Inserted_InProgress,
                            Common.Cost.VerificationFlow.Phone_SMS_Inserted,
                            Common.Cost.VerificationFlow.Phone_SMS_Duplicate,
                            Common.Cost.VerificationFlow.Phone_SMS_Exception);
                    logger.InfoFormat("List Of Records for OutBound:{0}:,", verifications.Count());
                    logger.InfoFormat("End of retrieving the phone numbers with the status 1");
                    #endregion

                    #region Chunkify
                    logger.InfoFormat("Start - Dividing the List of phone numbers with respect to thread count");
                    var chunks = verifications.Chunkify(threadNumber);
                    logger.InfoFormat("End - Dividing the List of phone numbers with respect to thread count");
                    #endregion

                    #region Run TPL
                    foreach (var chunk in chunks)
                    {

                        #region Add Token Object To the PhoneCollection
                        TokenPhoneCollection outboundToken = new TokenPhoneCollection();
                        outboundToken.PhoneCollection.AddRange(chunk);
                        outboundToken.Token = token;
                        #endregion

                        #region Add a Task
                        Task task = new Task(new Action<object>(processSMSOutbound), outboundToken,
                            TaskCreationOptions.AttachedToParent);
                        tasks.Add(task);
                        #endregion
                    }

                    if (tasks.Count > 0)
                    {
                        foreach (var item in tasks)
                        {
                            item.Start();
                        }
                    }
                    Task.WaitAll(tasks.ToArray());

                    #endregion

                    #region Finalize

                    #endregion

                    #endregion
                }
                logger.InfoFormat("Windows Service start for the Outbound process");

                #endregion
            }
            catch (Exception ex)
            {
                logger.Error("ProcessSMSOutbound", ex);
                string.Format(DateTime.Now.ToString() + " Error: {0}:", ex).WriteToConsole();
                //throw ex;
            }
        }
        public void CheckTripStatus(TokenRQ tokenRQ)
        {
            try
            {
                #region Check Trip Status

                #region Init

                var sds = new UDI.SDS.SecurityController(null, null);
                List<MasterTripList> processTypeList = new List<MasterTripList>();
                Dictionary<string, string> tokenObj = new Dictionary<string, string>();
                string tripReminderStatus = System.Configuration.ConfigurationManager.AppSettings["TripReminderStatusList"];
                int referralEnabled = System.Configuration.ConfigurationManager.AppSettings["EnableReferral"].ToInt32();
                int pushNotificationEnabled = System.Configuration.ConfigurationManager.AppSettings["EnablePushNotification"].ToInt32();

                #endregion

                logger.InfoFormat("Windows Service start for the CheckTripStatus process");
                lock (checkTripStatusLocker)
                {
                    var token = new TokenRS();

                    #region Validate Token

                    token = sds.GetToken(tokenRQ);
                    if (token == null)
                    {
                        #region Token Error

                        logger.InfoFormat("Invalid Token Object");
                        logger.InfoFormat("User Authentication Failure");
                        throw new Exception("Error");

                        #endregion
                    }

                    tokenObj.Add(token.Username, token.SecurityKey);

                    #endregion
                    
                    logger.InfoFormat("Windows Service start for the Check Trip Status process");
                    var referralDomain = new Domain.Referral.ReferralDomain();

                    List<MasterTripList> tripList = referralDomain.VTODTripStatus();
                    logger.InfoFormat("Count of the TripList :{0}", tripList.Count());
                    
                    var status = string.Empty;
                    if (tripList != null && tripList.Any())
                    {
                        #region Get the status of the Trip

                        foreach (var tripItem in tripList)
                        {                            
                            vtod_trip trip = null;
                            MasterTripList processTypeItem = new MasterTripList();                            
                            using (var db = new DataAccess.VTOD.VTODEntities())
                            {
                                 trip = db.vtod_trip.Where(s => s.Id == tripItem.tripID).FirstOrDefault();
                            }
                            if (trip != null)
                            {
                                VTOD_Utility utilityObj = new VTOD_Utility();

                                try
                                {
                                    OTA_GroundResRetrieveRS statusRS = null;

                                    if (trip.FleetTripCode == UDI.VTOD.Common.DTO.Enum.FleetTripCode.Taxi_UDI33
                                        || trip.FleetTripCode == UDI.VTOD.Common.DTO.Enum.FleetTripCode.Taxi_SDS
                                        || trip.FleetTripCode == UDI.VTOD.Common.DTO.Enum.FleetTripCode.Taxi_MTDATA
                                        || trip.FleetTripCode == UDI.VTOD.Common.DTO.Enum.FleetTripCode.Taxi_CSCI)
                                    {
                                        #region Taxi Fleet                                                                                                                                              
                                        statusRS = utilityObj.Status(tokenObj, tripItem.tripID == null ? 0 : tripItem.tripID.ToInt64(), "Taxi");
                                        UpdateTripItem(statusRS, tripItem);
                                        #endregion
                                    }
                                    else if (trip.FleetTripCode == UDI.VTOD.Common.DTO.Enum.FleetTripCode.Ecar_GreenTomato
                                         || trip.FleetTripCode == UDI.VTOD.Common.DTO.Enum.FleetTripCode.SDS_Shuttle_Airport
                                         || trip.FleetTripCode == UDI.VTOD.Common.DTO.Enum.FleetTripCode.SDS_Shuttle_Charter
                                         || trip.FleetTripCode == UDI.VTOD.Common.DTO.Enum.FleetTripCode.SDS_Shuttle_Hourly
                                         || trip.FleetTripCode == UDI.VTOD.Common.DTO.Enum.FleetTripCode.Ecar_Aleph
                                         || trip.FleetTripCode == UDI.VTOD.Common.DTO.Enum.FleetTripCode.Ecar_Texas)
                                    {

                                        using (var db = new DataAccess.VTOD.VTODEntities())
                                        {
                                            var ecarTrip = db.ecar_trip.Where(x => x.Id == tripItem.tripID).FirstOrDefault();
                                            var sdsTrip = db.sds_trip.Where(x => x.Id == tripItem.tripID).FirstOrDefault();

                                            if (sdsTrip != null)
                                            {
                                                #region Super shuttle                                                
                                                statusRS = utilityObj.Status(tokenObj, tripItem.tripID == null ? 0 : tripItem.tripID.ToInt64(), "SuperShuttle_ExecuCar");
                                                UpdateTripItem(statusRS, tripItem);
                                                #endregion
                                            }
                                            else if (ecarTrip != null)
                                            {
                                                #region Ecar                                                
                                                statusRS = utilityObj.Status(tokenObj, tripItem.tripID == null ? 0 : tripItem.tripID.ToInt64(), "ExecuCar");
                                                UpdateTripItem(statusRS, tripItem);
                                                #endregion
                                            }
                                        }
                                    }
                                    else if (trip.FleetTripCode == UDI.VTOD.Common.DTO.Enum.FleetTripCode.Van_CSCI)
                                    {
                                        #region van Fleet
                                        statusRS = utilityObj.Status(tokenObj, tripItem.tripID == null ? 0 : tripItem.tripID.ToInt64(), "Van");
                                        UpdateTripItem(statusRS, tripItem);
                                        #endregion
                                    }
                                }
                                catch (Exception ex)
                                {
                                    logger.ErrorFormat("Error:", ex.Message);
                                }
                            }
                        }

                        #endregion
                    }
                    #region Retrieve Status of the Configuration table
                    String statusList = null;
                     using (var db = new DataAccess.VTOD.VTODEntities())
                     {
                         List<string> templist = db.Notification_Configuration.Where(p=>p.IsEnabled==true).Select(p => p.Status.ToLower()).Distinct().ToList();
                         statusList = string.Join(",", templist);
                     }
                    
                    #endregion

                    #region Push Notification Status Pull
                    TokenTripCollection notificationToken = new TokenTripCollection();
                    TokenTripCollection tripReminderToken = new TokenTripCollection();
                    if (pushNotificationEnabled == 1)
                     {
                         notificationToken.TripCollection =
                        (List<MasterTripList>)
                            tripList.Where(
                                p => p.Status != null && statusList.Contains(p.Status.ToLower().ToString()) && p.WorkFlow != PushWorkFlow.PushWorkFlow_Inserted_TripReminder)
                                .ToList<MasterTripList>();
                        tripReminderToken.TripCollection = (List<MasterTripList>)
                            tripList.Where(
                                p => p.Status != null && tripReminderStatus.ToLower().ToString().Contains(p.Status.ToLower().ToString()) && p.WorkFlow== PushWorkFlow.PushWorkFlow_Inserted_TripReminder)
                                .ToList<MasterTripList>();
                    }
                    notificationToken.Token = token;
                    tripReminderToken.Token = token;
                    logger.InfoFormat("Push Notification Token Object Trips:{0}:", notificationToken.TripCollection.Count());
                    logger.InfoFormat("Trip Reminder Notification Token Object Trips:{0}:", tripReminderToken.TripCollection.Count());
                    #endregion

					#region TPL

					logger.InfoFormat("Start---- TPL Process");
                    List<Task> tasks = new List<Task>();
                    

                    #region Push Notification
                    Task taskpushNotification = new Task(new Action<object>(processInsertNotificationTrips), notificationToken,
                                    TaskCreationOptions.AttachedToParent);
                    tasks.Add(taskpushNotification);

                    #endregion

                    #region Update Trip Reminder Trips
                    Task tasktripReminderNotification = new Task(new Action<object>(processTripReimderTrips), tripReminderToken,
                                    TaskCreationOptions.AttachedToParent);
                    tasks.Add(tasktripReminderNotification);

                    #endregion
                    logger.InfoFormat("End---- Adding a Task");
					

					#region

					if (tasks.Count > 0)
                    {
                        foreach (var item in tasks)
                        {
                            item.Start();
                        }
                    }
                    Task.WaitAll(tasks.ToArray());

                    #region

                    #endregion

                    #region Finalize

                    #endregion

                    logger.InfoFormat("End---- TPL Process");

                    #endregion
                }
                logger.InfoFormat("Windows Service end for the Check Trip Status  process");

                #endregion

                #endregion

                #endregion
            }
            catch (Exception ex)
            {
                logger.Error("CheckTripStatus", ex);
                string.Format(DateTime.Now.ToString() + " Error: {0}:", ex).WriteToConsole();
            }
        }

        private void UpdateTripItem(OTA_GroundResRetrieveRS statusRS, MasterTripList tripItem)
        {
            if (statusRS != null && statusRS.TPA_Extensions != null && statusRS.TPA_Extensions.Statuses != null &&
                                        statusRS.TPA_Extensions.Statuses.Status != null &&
                                        statusRS.TPA_Extensions.Statuses.Status.Count() > 0)
            {
                var statusObj = statusRS.TPA_Extensions.Statuses.Status.FirstOrDefault();
                if (statusObj.Value != null)
                {
                    tripItem.Status = statusObj.Value.ToLower();
                    if (statusRS.TPA_Extensions != null && statusRS.TPA_Extensions.Vehicles != null && statusRS.TPA_Extensions.Vehicles.Items != null && statusRS.TPA_Extensions.Vehicles.Items.Count() != 0)
                    {
                        if (statusRS.TPA_Extensions.Vehicles.Items.FirstOrDefault().ID != null)
                            tripItem.VehicleNumber = statusRS.TPA_Extensions.Vehicles.Items.FirstOrDefault().ID;

                        if (!string.IsNullOrWhiteSpace(statusRS.TPA_Extensions.SharedLink))
                            tripItem.SharedETALink = statusRS.TPA_Extensions.SharedLink;
                    }
                }
            }
        }

        public void SendNotification(TokenRQ tokenRQ)
        {
            #region Init
            var sds = new UDI.SDS.SecurityController(null, null);
            PushNotificationDomain pushDomain = new PushNotificationDomain();
            #endregion
            lock (pushNotificationLocker)
            {

                logger.InfoFormat("Windows Service start for the SendNotification process");
                try
                {
                    List<PushNotificationResultSet> newNotifications = new List<PushNotificationResultSet>();
                    var PushTypes = string.Format("{0},{1},", Common.Const.PushTypes.Now, Common.Const.PushTypes.Later);
                    logger.InfoFormat("PushTypes Allowed:{0}", PushTypes);

                    #region Retrieve notifications
                    using (var db = new VTODEntities())
                    {
                        newNotifications = pushDomain.ListPushNotifications(PushWorkFlow.PushWorkFlow_Inserted, PushWorkFlow.PushWorkFlow_Inprogress, PushTypes, 1);
                        logger.InfoFormat("Number of Notifications Received", newNotifications.Count());
                    }
                    #endregion

                    #region Push notifications

                    logger.InfoFormat("Start---- Push notifications");


                    var tasks = new List<Task>();

                    if (newNotifications.Any())
                    {
                        var notifications = CombineGroupMemebers(newNotifications);
                        #region Get Token

                        var token = new TokenRS();

                        #region Validate Token

                        token = sds.GetToken(tokenRQ);
                        if (token == null)
                        {
                            #region Token Error

                            logger.InfoFormat("Invalid Token Object");
                            logger.InfoFormat("User Authentication Failure");
                            throw new Exception("Error");

                            #endregion
                        }

                        #endregion

                        #endregion

                        #region Run TPL

                        #region Add Token Object To the Push Notification Collection

                        TokenNotificationCollection pushNotificationToken = new TokenNotificationCollection();
                        pushNotificationToken.NotificationCollection =
                              (List<PushNotification>)notifications.Where(
                                p => p.NotificationTypes == UDI.VTOD.Utility.Common.Const.NotificationType.PushNotification).ToList<PushNotification>();
                        pushNotificationToken.Token = token;

                        #endregion

                        #region Add Token Object To the SMS Notfication Collection

                        TokenNotificationCollection smsNotificationToken = new TokenNotificationCollection();
                        smsNotificationToken.NotificationCollection =
                            (List<PushNotification>)
                                notifications.Where(
                                     p => p.NotificationTypes == UDI.VTOD.Utility.Common.Const.NotificationType.SMSNotification).ToList<PushNotification>();
                        smsNotificationToken.Token = token;

                        #endregion
                        #endregion

                        #region Add  Task

                        Task taskPushNotification = new Task(new Action<object>(processPushNotification), pushNotificationToken,
                        TaskCreationOptions.AttachedToParent);
                        tasks.Add(taskPushNotification);
                        Task taskSMSNotfication = new Task(new Action<object>(processSMSNotification), smsNotificationToken,
                        TaskCreationOptions.AttachedToParent);
                        tasks.Add(taskSMSNotfication);

                        #endregion
                    }

                    // -- do not delete it
                    //if (retryNotifications.Count > 0)
                    //{
                    //    var retryNotificationsTask = new Task(processPushNotification, retryNotifications,
                    //        TaskCreationOptions.AttachedToParent);
                    //    tasks.Add(retryNotificationsTask);
                    //}

                    if (tasks.Any())
                    {
                        foreach (var item in tasks)
                        {
                            item.Start();
                        }

                        Task.WaitAll(tasks.ToArray());
                    }


                    logger.InfoFormat("End---- Push notifications");

                    #endregion
                }
                catch (Exception ex)
                {
                    logger.Error("SendNotification", ex);
                    string.Format(DateTime.Now + " Error: {0}:", ex).WriteToConsole();
                }
            }
        }



        #endregion

        #region Private Methods

        private void processSMSInbound(object verificationListChunck)
        {
            #region TypeCasting

            var verificationDomain = new UDI.VTOD.Utility.Domain.Verification.VerificationDomain();
            TokenPhoneCollection phoneCollection = (TokenPhoneCollection) verificationListChunck;

            #endregion

            #region Call Domain Method

            logger.InfoFormat("Start of the Inbound Process of the Verification Domain");
            verificationDomain.InBoundProcess(phoneCollection);
            logger.InfoFormat("End of the Inbound Process of the Verification Domain");

            #endregion


        }

        private void processSMSOutbound(object verificationListChunck)
        {
            #region TypeCasting 

            var verificationDomain = new VerificationDomain();
            TokenPhoneCollection phoneCollection = (TokenPhoneCollection) verificationListChunck;

            #endregion

            #region Call Domain Method

            logger.InfoFormat("Start of the Outbound Process of the Verification Domain");
            verificationDomain.OutBoundProcess(phoneCollection);
            logger.InfoFormat("End of the Outbound Process of the Verification Domain");

            #endregion

        }
        
        private void processTripReimderTrips(object tripReminderListChunck)
        {

            #region TypeCasting
            var tasks = new List<Task>();
            var pushDomain = new UDI.VTOD.Utility.Domain.Push.PushNotificationDomain();
            TokenTripCollection pushCollection = (TokenTripCollection)tripReminderListChunck;
            List<MasterTripList> tripCollection = (List<MasterTripList>)pushCollection.TripCollection;
            var threadNumber = ConfigurationManager.AppSettings["ThreadNumber"].ToInt32();
            #endregion
            if (tripCollection == null || tripCollection.Count() == 0) return;

            #region Chunkify
            logger.InfoFormat("Start - Dividing the List of records with respect to thread count");
            var chunks = pushCollection.TripCollection.Chunkify(threadNumber).ToList();
            logger.InfoFormat("End - Dividing the List of records with respect to thread count");

            #endregion

            #region Run TPL

            foreach (var chunk in chunks)
            {
                TokenTripCollection tokenNotification = new TokenTripCollection();
                tokenNotification.Token = pushCollection.Token;
                tokenNotification.TripCollection = (List<MasterTripList>)chunk;
                Task taskNotificationChunk = new Task(new Action<object>(pushDomain.UpdatePushNotification), tokenNotification,
                         TaskCreationOptions.AttachedToParent);
                tasks.Add(taskNotificationChunk);
            }
            if (tasks.Any())
            {
                foreach (var item in tasks)
                {
                    item.Start();
                }

                Task.WaitAll(tasks.ToArray());
            }
            #endregion
        }
        private void processInsertNotificationTrips(object notificationListChunck)
        {
           
            #region TypeCasting
            var tasks = new List<Task>();
            var pushDomain = new UDI.VTOD.Utility.Domain.Push.PushNotificationDomain();
            TokenTripCollection pushCollection = (TokenTripCollection)notificationListChunck;
            List<MasterTripList> tripCollection = (List<MasterTripList>)pushCollection.TripCollection;
            var threadNumber = ConfigurationManager.AppSettings["ThreadNumber"].ToInt32();
            #endregion
            if (tripCollection == null || tripCollection.Count() == 0) return;

            #region Chunkify
            logger.InfoFormat("Start - Dividing the List of records with respect to thread count");
            var chunks = pushCollection.TripCollection.Chunkify(threadNumber).ToList();
            logger.InfoFormat("End - Dividing the List of records with respect to thread count");

            #endregion

            #region Run TPL

            foreach (var chunk in chunks)
            {
                TokenTripCollection tokenNotification = new TokenTripCollection();
                tokenNotification.Token = pushCollection.Token;
                tokenNotification.TripCollection = (List<MasterTripList>)chunk;
                Task taskNotificationChunk = new Task(new Action<object>(pushDomain.InsertPushNotification), tokenNotification,
                         TaskCreationOptions.AttachedToParent);
                tasks.Add(taskNotificationChunk);
            }
            if (tasks.Any())
            {
                foreach (var item in tasks)
                {
                    item.Start();
                }

                Task.WaitAll(tasks.ToArray());
            }
            #endregion
        }
        /// <summary>
        /// Combine notifications with same id
        /// </summary>
        /// <param name="origNotifications"></param>
        /// <returns></returns>
        private List<PushNotification> CombineGroupMemebers(List<PushNotificationResultSet> origNotifications)
        {
            var notificationIds = origNotifications.Select(m => m.ID ?? 0).Distinct();

            return (from id in notificationIds
                    let notificationsWithSameId = origNotifications.Where(m => m.ID == id).ToList()
                    let n = notificationsWithSameId.FirstOrDefault()
                    where n != null
                    select new PushNotification
                    {
                        ID = n.ID,
                        DisplayText = n.DisplayText,
                        Event = n.Event,
                        ImageURL=n.ImageURL,
                        IsBroadcast = n.IsBroadcast ?? 0,
                        VtodTripID=n.VtodTripID ?? 0,
                        NotificationTypes=n.NotificationTypes ?? 0,
                        //TODO: When do we have more than one memberID ?
                        MemberIDList = notificationsWithSameId.Select(m => m.memberID).ToList()
                    }).ToList();
        }

        private void processPushNotification(object notificationList)
        {
            #region Init
            var tasks = new List<Task>();
            PushNotificationDomain pushDomain = new PushNotificationDomain();
            var threadNumber = ConfigurationManager.AppSettings["ThreadNumber"].ToInt32();
            TokenNotificationCollection notification = (TokenNotificationCollection)notificationList;
            #endregion
            if (notificationList == null || notification.NotificationCollection.Count()==0) return;

            #region Chunkify
            logger.InfoFormat("Start - Dividing the List of records with respect to thread count");
            var chunks = notification.NotificationCollection.Chunkify(threadNumber).ToList();
            logger.InfoFormat("End - Dividing the List of records with respect to thread count");
            #endregion

            #region Run TPL

            foreach (var chunk in chunks)
            {
                TokenNotificationCollection tokenNotification = new TokenNotificationCollection();
                tokenNotification.Token = notification.Token;
                tokenNotification.NotificationCollection = (List<PushNotification>)chunk;
                Task taskPushNotificationChunk = new Task(new Action<object>(pushDomain.processPushNotification), tokenNotification,
                         TaskCreationOptions.AttachedToParent);
                tasks.Add(taskPushNotificationChunk);
            }
            if (tasks.Any())
            {
                foreach (var item in tasks)
                {
                    item.Start();
                }

                Task.WaitAll(tasks.ToArray());
            }
            #endregion

        }

        private void processSMSNotification(object notificationList)
        {
            #region Init
            PushNotificationDomain pushDomain = new PushNotificationDomain();
            var tasks = new List<Task>();
            var threadNumber = ConfigurationManager.AppSettings["ThreadNumber"].ToInt32();
            TokenNotificationCollection notification = (TokenNotificationCollection)notificationList;
            #endregion
            if (notificationList == null || notification.NotificationCollection.Count() == 0) return;
            #region Chunkify
            logger.InfoFormat("Start - Dividing the List of records with respect to thread count");
            var chunks = notification.NotificationCollection.Chunkify(threadNumber).ToList();
            logger.InfoFormat("End - Dividing the List of records with respect to thread count");

            #endregion

            #region Run TPL

            foreach (var chunk in chunks)
            {
                TokenNotificationCollection tokenNotification = new TokenNotificationCollection();
                tokenNotification.Token = notification.Token;
                tokenNotification.NotificationCollection = (List<PushNotification>)chunk;
                Task taskSMSNotificationChunk = new Task(new Action<object>(pushDomain.processSMSNotification), tokenNotification,
                         TaskCreationOptions.AttachedToParent);
                tasks.Add(taskSMSNotificationChunk);
            }
            if (tasks.Any())
            {
                foreach (var item in tasks)
                {
                    item.Start();
                }

                Task.WaitAll(tasks.ToArray());
            }
            #endregion

        }

        #endregion
    }

}
