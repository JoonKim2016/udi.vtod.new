﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Utility.Domain;
using UDI.Utility.Helper;
using UDI.VTOD.Utility.Common.DTO;
using UDI.VTOD.Common.DTO;
using System.Text.RegularExpressions;
using System.ServiceModel.Web;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using UDI.Utility.Serialization;
using UDI.SDS;
using UDI.SDS.MembershipService;
using System.Runtime.Serialization;
using UDI.VTOD.Common.Track;
using UDI.VTOD.Common.Abstract;
using UDI.SDS.Helper;
using UDI.SDS.DTO.Enum;
using UDI.SDS.DTO;
using UDI.SDS.ServiceAdapter;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Utility.Model;
namespace UDI.VTOD.Utility.Domain.Configuration
{
    public class ConfigurationDomain
    {
        public ILog logger;
	    #region Constructor
        public ConfigurationDomain()
		{
			logger = LogManager.GetLogger(this.GetType());
			log4net.Config.XmlConfigurator.Configure();
		}
		#endregion
        public AppConfigurationRS ConfigurationDetails(UDI.VTOD.Utility.Common.DTO.Configuration ConfigurationDetails)
        {
            string AppName = ConfigurationDetails.AppName;
            string Latitude = null;
            string Longitude = null;
            string ZipCode = null;
            string Category = null;
            string Key = string.Empty;
            DateTime? LocalTime = ConfigurationDetails.LocalDateTime;
            if(ConfigurationDetails!=null)
            {
                if (ConfigurationDetails.Address != null)
                {
                    if (!string.IsNullOrWhiteSpace(ConfigurationDetails.Address.Latitude) || !string.IsNullOrWhiteSpace(ConfigurationDetails.Address.Longitude))
                    { 
                        Latitude = ConfigurationDetails.Address.Latitude;
                        Longitude = ConfigurationDetails.Address.Longitude;
                    }
                    if (!string.IsNullOrWhiteSpace(ConfigurationDetails.Address.PostalCode))
                    {
                        ZipCode = ConfigurationDetails.Address.PostalCode;
                    }
                    if (!string.IsNullOrWhiteSpace(ConfigurationDetails.Category))
                    {
                        Category = ConfigurationDetails.Category;
                    }
                    if (!string.IsNullOrWhiteSpace(ConfigurationDetails.Key))
                    {
                        Key = ConfigurationDetails.Key;
                    }
                }
            }
            List<ConfigCategory> configurationDetails = new List<ConfigCategory>();
            AppConfigurationRS result = new AppConfigurationRS();
            try
            {
                #region Call Domain Method
                logger.InfoFormat("Start - Retreiving records from Configuration table");
                using (var db = new DataAccess.VTOD.VTODEntities())
                {
                    var configurationList = db.SP_Retrieve_App_Configurations(AppName, Longitude, Latitude, ZipCode, Category, Key, LocalTime).ToList();
                    logger.InfoFormat("Count of Configuration list retrieved :{0}:", configurationList.Count());
                    if (configurationList != null && configurationList.Any())
                    {
                        var groupedConfigurationList = configurationList.GroupBy(s => s.CategoryName);
                        foreach (var configItem in groupedConfigurationList)
                        {
                            List<CategoryAttributes> attributes = new List<CategoryAttributes>();
                            foreach (var value in configItem)
                            {
                                CategoryAttributes _item = null;
                                _item = new CategoryAttributes();
                                _item.ID = value.ID.ToString();
                                _item.Name = value.KeyName;
                                _item.Value = value.Value;
                                _item.Type = value.Type;
                                _item.Priority = value.Priority.ToString();
                                attributes.Add(_item);
                            }
                            configurationDetails.Add(new ConfigCategory() { CategoryType = configItem.Key, Attributes = attributes });


                        }
                    }

                }
                logger.InfoFormat("End - Retreiving records from Configuration table");

                #endregion
            }
            catch (Exception ex)
            {
                logger.Error("ConfigurationDetails:", ex);
            }
            result.Category = configurationDetails;
            return result;
        }

    }
}
