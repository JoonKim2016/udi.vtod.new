﻿using Microsoft.Azure.NotificationHubs;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Utility.Common.Contracts;
using UDI.VTOD.Utility.Common.DTO;

namespace UDI.VTOD.Utility.Domain.Push
{
    public class AzurePushNotificationService : IPushService
    {
        private readonly string _AZURE_END_POINT;
        private readonly string _AZURE_HUB_NAME;
        private NotificationHubClient _Hub;
        private string _serviceProvider;
        
        public string ServiceProvider
        {
            get
            {
                return _serviceProvider;
            }
        }

        public AzurePushNotificationService(string provider)
        {
            _AZURE_END_POINT = ConfigurationManager.AppSettings["AZURE_END_POINT"];
            _AZURE_HUB_NAME  = ConfigurationManager.AppSettings["AZURE_HUB_NAME"];
            _Hub = NotificationHubClient.CreateClientFromConnectionString(_AZURE_END_POINT, _AZURE_HUB_NAME);
            _serviceProvider = provider;
        }
        
        public bool SendNotification(List<string> memberIDs, PushNotification notification)
        {
            //There is no reason to pass memberIDs , since notification has memberid
            return SendNotificationToANH( notification);            
        }

        public bool BroadcastNotification(PushNotification notification)
        {         
            return SendNotificationToANH(notification);
        }

        private bool SendNotificationToANH(PushNotification notification)
        {
            //Currently  we don't handle image and Need to send to both apn and gcm 
           
            string gcmPayload = new JObject(
                      new JProperty("data", 
                                    new JObject(new JProperty("message", notification.DisplayText),
                                                new JProperty("event", notification.Event),
                                                new JProperty("MemberId", notification.MemberIDList.First()))
                                    )).ToString(Newtonsoft.Json.Formatting.None);

            string applePayload = new JObject(
                 new JProperty("aps", new JObject(new JProperty("alert", notification.DisplayText))),
                 new JProperty("event", notification.Event),
                 new JProperty("MemberId", notification.MemberIDList.First()),
                 new JProperty("NotificationId", notification.ID)
                 ).ToString(Newtonsoft.Json.Formatting.None);

            if (notification.MemberIDList.FirstOrDefault() != null )
            {
                _Hub.SendAppleNativeNotificationAsync(applePayload, notification.MemberIDList.First());
                _Hub.SendGcmNativeNotificationAsync(gcmPayload, notification.MemberIDList.First());
            }
            else
            {
                //broadcasting
                _Hub.SendAppleNativeNotificationAsync(applePayload);
                _Hub.SendGcmNativeNotificationAsync(gcmPayload);
            }

            return true;
        }
    }
}
