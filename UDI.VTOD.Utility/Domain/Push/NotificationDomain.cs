﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Utility.Domain;
using UDI.Utility.Helper;
using UDI.VTOD.Utility.Common.DTO;
using UDI.VTOD.Common.DTO;
using System.Text.RegularExpressions;
using System.ServiceModel.Web;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using UDI.Utility.Serialization;
using UDI.SDS;
using UDI.SDS.MembershipService;
using System.Runtime.Serialization;
using UDI.VTOD.Common.Track;
using UDI.VTOD.Common.Abstract;
using UDI.SDS.Helper;
using UDI.SDS.DTO.Enum;
using UDI.SDS.DTO;
using UDI.SDS.ServiceAdapter;
using UDI.VTOD.Common.DTO.Enum;
namespace UDI.VTOD.Utility.Domain.Push
{
    public class NotificationDomain
    {
       public ILog logger;

		/// <summary>
		/// Inserts the records in the Notification table
		/// </summary>
		#region Constructor
       public NotificationDomain()
		{

			logger = LogManager.GetLogger(this.GetType());
			log4net.Config.XmlConfigurator.Configure();
		}
		#endregion
        #region AddNotification
        public void AddNotification(TokenRS token, string Text, string PushType, string EmailAddress)
        {
            try
            {
                #region Call Domain Method
                using (var db = new DataAccess.VTOD.VTODEntities())
                {
                    logger.InfoFormat("Insert the notification details in the Push Notification table: inputs Text:{0},PushType:{1}:,EmailAddress:{2}:", Text, PushType, EmailAddress);
                    #region Call Domain Method
                    try
                    {
                        var membershipController = new UDI.SDS.MembershipController(token, TrackTime);
                        var corporateAccount = membershipController.GetCorporateAccountKey("Aleph", EmailAddress);
                        if (corporateAccount != null && !string.IsNullOrWhiteSpace(corporateAccount.UserKey))
                        {
                            db.SP_PushNotificationAleph(corporateAccount.MembershipRecord.MemberID, null, null, DateTime.UtcNow, UDI.VTOD.Utility.Common.Const.PushTypes.Later.ToString(), PushType, Text,token.Username);
                            logger.InfoFormat("Insertion successful");
                        }
                    }
                    catch (Exception ex1)
                    {
                        logger.Error("AddReferre:", ex1);
                        logger.Error("Insertion Failure");
                        throw ex1;
                    }
                    #endregion


                }
                #endregion
            }
            catch (VtodException ex)
            {
                logger.Error("AddReferrer:", ex);
                throw (ex);
            }
            catch (Exception ex)
            {
                logger.Error("AddReferrer:", ex);
                throw (ex);
            }

        }
        #endregion

        #region Properties
        public TrackTime TrackTime { get; set; }
        #endregion
    }
}
