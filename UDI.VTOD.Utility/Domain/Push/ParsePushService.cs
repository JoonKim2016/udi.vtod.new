﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using UDI.Utility.Serialization;
using UDI.VTOD.Common.DTO;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Utility.Common.Const;
using UDI.VTOD.Utility.Common.Contracts;
using UDI.VTOD.Utility.Common.DTO;

namespace UDI.VTOD.Utility.Domain.Push
{
    /// <summary>
    /// Implementation of Parse's push notification service
    /// </summary>
    public class ParsePushService : IPushService
    {
        private static readonly string pushUrl = ConfigurationManager.AppSettings["Push_Parse_Url"];
        private static readonly int timeout = int.Parse(ConfigurationManager.AppSettings["Push_Parse_Timeout"]);
        private static readonly string applicationID = ConfigurationManager.AppSettings["Push_Parse_ApplicationID"];
        private static readonly string restAPIKey = ConfigurationManager.AppSettings["Push_Parse_Rest_API_Key"];
        private static readonly string channelPreface = ConfigurationManager.AppSettings["Push_Parse_ChannelPreface"];
        private static readonly string deviceType = ConfigurationManager.AppSettings["Push_Parse_DeviceType"];
        public string _serviceProvider { get; private set; }
        
        public ParsePushService(string provider)
        {
            _serviceProvider = provider;
        }

        public string ServiceProvider
        {
            get
            {
                return _serviceProvider;
            }
        }

        public bool SendNotification(List<string> endpoints, PushNotification notification)
        {
            try
            {
                #region build request

                var where = new Dictionary<string, object>();
                if (endpoints == null || !endpoints.Any())
                {
                    // broadcast
                    var t = deviceType.Split('|');
                    var deviceTypeQuery = new Dictionary<string, object>
                    {
                        {"$in", t }
                    };
                    where.Add("deviceType", deviceTypeQuery);
                }
                else
                {
                    // send to target user(s)
                    var channelObject = new Dictionary<string, object>
                    {
                        {"$in", endpoints.Select(ep => string.Format("{0}{1}", channelPreface, ep)).ToList()}
                    };
                    where.Add("channels", channelObject);
                }

                var data = new Dictionary<string, object>();
                if (!string.IsNullOrWhiteSpace(notification.Event))
                {
                    data.Add("event", notification.Event);
                }
                data.Add("sound", "push_notification.aif");
                if (endpoints != null && endpoints.Any())
                {
                    string memberid=endpoints.Select(ep => string.Format("{0}", ep)).FirstOrDefault();
                    data.Add("MemberId", memberid);
                }
                if(!string.IsNullOrWhiteSpace(notification.DisplayText))
                {
                    data.Add("alert", notification.DisplayText);
                }
                if (!string.IsNullOrWhiteSpace(notification.ImageURL))
                {
                    data.Add("img", notification.ImageURL);
                }
                var request = new Dictionary<string, object>
                {
                    {"data", data}, {"where", where}
                };
                #endregion

                var headers = new Dictionary<string, string>
                {
                    {"X-Parse-Application-Id", applicationID},
                    {"X-Parse-REST-API-Key", restAPIKey}
                };

                string status;
                Dictionary<string, string> responseHeader;

                ProcessWebRequestEx(pushUrl, request.JsonSerialize(), "application/json", "POST", string.Empty,
                    string.Empty, timeout, headers, out status, out responseHeader);


                return true;
            }
            catch (Exception ex)
            {
                var vtodException = VtodException.CreateBubbleUpException(ExceptionType.General, ex.ToString());
                throw vtodException;
            }
        }
             

        public bool BroadcastNotification(PushNotification notification)
       
        {
            return SendNotification(null, notification);
        }

        #region Utilities

        //Make HttpWebRequest
        private static string ProcessWebRequestEx(string url, string content, string contentType, string method, string userName, 
            string password, int timeout, Dictionary<string, string> headers, out string status, out Dictionary<string, string> responseHeader)
        {
            var result = string.Empty;
            status = string.Empty;
            responseHeader = new Dictionary<string, string>();

            try
            {
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                if (headers != null && headers.Any())
                {
                    foreach (var item in headers)
                    {
                        //If the Header type specified is one of the existing 'restricted headers', just assign the new value. 
                        //Otherwise add the new header and value.
                        switch (item.Key.ToLower())
                        {
                            case "accept":
                                request.Accept = item.Value;
                                break;
                            case "connection":
                                request.Connection = item.Value;
                                break;
                            case "content-length":
                                request.ContentLength = Convert.ToInt64(item.Value);
                                break;
                            case "content-type":
                                request.ContentType = item.Value;
                                break;
                            case "date":
                                request.Date = Convert.ToDateTime(item.Value);
                                break;
                            case "expect":
                                request.Expect = item.Value;
                                break;
                            case "host":
                                request.Host = item.Value;
                                break;
                            case "if-modified-since":
                                request.IfModifiedSince = Convert.ToDateTime(item.Value);
                                break;
                            case "referer":
                                request.Referer = item.Value;
                                break;
                            case "transfer-encoding":
                                request.TransferEncoding = item.Value;
                                break;
                            case "user-agent":
                                request.UserAgent = item.Value;
                                break;
                            default:
                                request.Headers.Add(item.Key + ":" + item.Value);
                                break;
                        }
                    }
                }

                if (!string.IsNullOrWhiteSpace(userName))
                {
                    NetworkCredential networkCredential = new NetworkCredential(userName, password);
                    request.Credentials = networkCredential;
                }
                //NetworkCredential networkCredential = new NetworkCredential("DenverYellowCab", "denveryellowcab@12345");
                //request.Credentials = networkCredential;
                request.ProtocolVersion = HttpVersion.Version11;
                request.Method = method;
                request.Timeout = timeout;
                //if (request.Accept == null) request.Accept = "*/*";
                //request.UserAgent = "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; Trident/6.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; InfoPath.3; .NET4.0C; .NET4.0E)";

                Stream dataStream;
                if (!string.IsNullOrWhiteSpace(content))
                {
                    byte[] byteArray = Encoding.UTF8.GetBytes(content);
                    //byte[] byteArray = Encoding.ASCII.GetBytes(content);
                    request.ContentType = contentType;
                    request.ContentLength = byteArray.Length;
                    dataStream = request.GetRequestStream();
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    dataStream.Close();
                }
                //else
                //{
                //	dataStream = request.GetRequestStream();
                //}
                WebResponse response = request.GetResponse();

                if (response.Headers != null && response.Headers.Count > 0)
                {
                    for (int i = 0; i < response.Headers.Count; ++i)
                    {
                        responseHeader.Add(response.Headers.Keys[i], response.Headers[i]);
                    }
                }

                status = ((HttpWebResponse)response).StatusDescription;

                dataStream = response.GetResponseStream();
                if (dataStream != null)
                {
                    StreamReader reader = new StreamReader(dataStream);
                    result = reader.ReadToEnd();

                    reader.Close();
                    dataStream.Close();
                }
                
                response.Close();
            }
            catch (WebException ex)
            {
                using (var reader = new StreamReader(ex.Response.GetResponseStream()))
                {
                    result = reader.ReadToEnd();
                }
                throw new Exception(result, ex);
            }

            return result;

        }

        #endregion

    }
}
