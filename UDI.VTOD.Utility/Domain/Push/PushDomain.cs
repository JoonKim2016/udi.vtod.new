﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
namespace UDI.VTOD.Utility.Domain.Push
{
   public class PushDomain
    {
       public ILog logger;
      

       public List<UDI.VTOD.Utility.Common.DTO.PushNotification> ListPushNotification(int? currentStatus, int? nextStatus, int pushType, int? broadCast)
		{

			try
			{
				#region Call Domain Method
               
				using (var db = new DataAccess.VTOD.VTODEntities())
				{
                    List<UDI.VTOD.Utility.Common.DTO.PushNotification> result = new List<UDI.VTOD.Utility.Common.DTO.PushNotification>();
                    logger.InfoFormat("Start - Retrieving the Notification List from the database with Status:{0}:", currentStatus);
                    var listNotification = db.SP_Utility_Retrieve_PushNotification(currentStatus, nextStatus, pushType, broadCast).OrderBy(m => m.ID).ToList();
                    if (listNotification != null && listNotification.Any())
					{
                        foreach (var notificationItem in listNotification)
						{
                            var notificationInfo = notificationItem;

                            result.Add(new UDI.VTOD.Utility.Common.DTO.PushNotification() { ID = notificationInfo.ID, memberID = notificationInfo.memberID, DisplayText = notificationInfo.DisplayText, ImageUrl = notificationInfo.ImageUrl, Event = notificationInfo.Event, IsBroadcast=notificationInfo.IsBroadcast });

						}
					}
                    logger.InfoFormat("End - Retrieving the Notifications from the database.Count{0}:", listNotification.Count());
					return result;
				}
				#endregion
			}
			catch (Exception ex)
			{
                logger.Error("ListPushNotification:", ex);
				throw (ex);
			}
		}

        public void PushNotificationCompleted(long? ID,)
        {
                 db.sp_Utility_PushNotification_Updation(string.Format("{0},", notification.ID), result ? PushWorkFlow.PushWorkFlow_Completed : PushWorkFlow.PushWorkFlow_Error);
        }
    }
}
