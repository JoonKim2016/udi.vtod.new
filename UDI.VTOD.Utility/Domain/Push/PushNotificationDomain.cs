﻿
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Utility.Domain;
using UDI.Utility.Helper;
using UDI.VTOD.Utility.Common.DTO;
using UDI.VTOD.Common.DTO;
using System.Text.RegularExpressions;
using System.ServiceModel.Web;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using UDI.Utility.Serialization;
using UDI.SDS;
using UDI.SDS.MembershipService;
using System.Runtime.Serialization;
using UDI.VTOD.Common.Track;
using UDI.VTOD.Common.Abstract;
using UDI.SDS.Helper;
using UDI.SDS.DTO.Enum;
using UDI.SDS.DTO;
using UDI.SDS.ServiceAdapter;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Utility.Domain.Verification;
using UDI.VTOD.Utility.Common.Contracts;
using System.Configuration;

namespace UDI.VTOD.Utility.Domain.Push
{
  public  class PushNotificationDomain
    {
      	public ILog logger;

		/// <summary>
		/// Inserts the records in the Push Notification table
		/// </summary>
		/// <param name="type"></param>
		/// <param name="phoneNumber"></param>
		/// <param name="memberID"></param>
		/// 
		#region Constructor
        public PushNotificationDomain()
		{

			logger = LogManager.GetLogger(this.GetType());
			log4net.Config.XmlConfigurator.Configure();
		}
		#endregion
        public List<PushNotificationResultSet> ListPushNotifications(int currentStatus, int nextStatus, string PushTypes, int broadcast)
        {
           List<PushNotificationResultSet> notificationList = new List<PushNotificationResultSet>();
           try
           {
               logger.InfoFormat("Starts---Push Notification  table retrieval");
                 using (var db = new DataAccess.VTOD.VTODEntities())
                    {
                        var newNotifications = db.SP_Utility_Retrieve_PushNotification(currentStatus, nextStatus, PushTypes, broadcast).OrderBy(m => m.ID).ToList();
                         if (newNotifications != null && newNotifications.Any())
                            {
                                foreach (var noficationItem in newNotifications)
                                {
                                    logger.InfoFormat("Notification ID:{0}", noficationItem.ID);
                                    logger.InfoFormat("Member ID:{0}", noficationItem.memberID);
                                    logger.InfoFormat("Display Text:{0}", noficationItem.DisplayText);
                                    logger.InfoFormat("Image URL:{0}", noficationItem.ImageUrl);
                                    logger.InfoFormat("Event:{0}", noficationItem.Event);
                                    logger.InfoFormat("TripID:{0}", noficationItem.VtodTripID);
                                    notificationList.Add(new UDI.VTOD.Utility.Common.DTO.PushNotificationResultSet() { ID = noficationItem.ID, memberID = noficationItem.memberID.ToString(), DisplayText = noficationItem.DisplayText, ImageURL = noficationItem.ImageUrl, Event = noficationItem.Event, IsBroadcast = noficationItem.IsBroadcast, NotificationTypes = noficationItem.NotificationType, VtodTripID = noficationItem .VtodTripID});
                                }
                             }

                     }
                 logger.InfoFormat("End----Push Notification  table retrieval");
            }
            catch (Exception ex)
            {
                logger.Error("ListPushNotifications:", ex);
            }
           return notificationList;
        }

      public void NotificationUpdation(Int64? ID,int? Status)
        {
            try
            {
                logger.InfoFormat("Push Notification  table updation starts");
                using (var db = new DataAccess.VTOD.VTODEntities())
                {
                    logger.InfoFormat("Inputs:ID:{0},Status{1}:", ID, Status);
                    db.sp_Utility_PushNotification_Updation(string.Format("{0},", ID), Status);
                    logger.InfoFormat("Updation successful");
                }
                logger.InfoFormat("Push Notification  table updation ends");
            }
            catch (Exception ex)
            {
                logger.Error("NotificationUpdation:", ex);
                using (var db = new DataAccess.VTOD.VTODEntities())
                {
                    logger.Error("Updation with the error status");
                    db.sp_Utility_PushNotification_Updation(string.Format("{0},", ID), Status);
                }
            }
        }
      public void processPushNotification(object objNotifications)
      {           
            TokenNotificationCollection notifications = (TokenNotificationCollection)objNotifications;

            List<PushNotification> notificationList = (List<PushNotification>)notifications.NotificationCollection;


            //SendBoth : Temp solution for US3311    
            bool sendBoth = ConfigurationManager.AppSettings["SendBoth"].ToBool();

            if (sendBoth)
            {
                SendBoth(notificationList);
            }
            else
            {
                IPushService pushService = PushServiceFactory.GetServiceInstance();

                try
                {
                    foreach (PushNotification item in notificationList)
                    {
                        List<string> endpoints = item.MemberIDList;
                        logger.InfoFormat("Start of the Notification Calling {0} : EndPoints {1}", pushService.ServiceProvider, string.Join(",", item.MemberIDList));
                        var result = item.IsBroadcast == 1 ? pushService.BroadcastNotification(item) : pushService.SendNotification(endpoints, item);
                        logger.InfoFormat("End of the Notification Calling {0}", pushService.ServiceProvider);
                        logger.InfoFormat("Start of the Notification Domain Method");
                        NotificationUpdation(item.ID, result ? UDI.VTOD.Utility.Common.Const.PushWorkFlow.PushWorkFlow_Completed : UDI.VTOD.Utility.Common.Const.PushWorkFlow.PushWorkFlow_Error);
                        logger.InfoFormat("End of the Notification Domain Method");
                    }
                }
                catch (Exception ex)
                {
                    logger.Error("processPushNotification", ex);
                }
            }
            
        }
                
        private void SendBoth(List<PushNotification> notificationList)
        {
            IPushService parsePushService = new ParsePushService("Parse");
            IPushService azurePushNotificationService = new AzurePushNotificationService("AZure");

            try
            {
                foreach (PushNotification item in notificationList)
                {
                    List<string> endpoints = item.MemberIDList;
                    logger.InfoFormat("Start of the Notification Calling bothe Azure and Parse : EndPoints {0}", string.Join(",", item.MemberIDList));

                    bool parseResult;
                    bool azureResult;

                    if (item.IsBroadcast == 1)
                    {
                        parseResult = parsePushService.BroadcastNotification(item);
                        azureResult = azurePushNotificationService.BroadcastNotification(item);
                    }
                    else
                    {
                        parseResult = parsePushService.SendNotification(endpoints, item);
                        azureResult = azurePushNotificationService.SendNotification(endpoints, item);
                    }

                    logger.InfoFormat("End of the Notification Calling bothe Azure and Parse");
                    logger.InfoFormat("Start of the Notification Domain Method");
                    NotificationUpdation(item.ID, (azureResult || parseResult) ? UDI.VTOD.Utility.Common.Const.PushWorkFlow.PushWorkFlow_Completed : UDI.VTOD.Utility.Common.Const.PushWorkFlow.PushWorkFlow_Error);
                    logger.InfoFormat("End of the Notification Domain Method");
                }
            }
            catch (Exception ex)
            {
                logger.Error("processPushNotification", ex);
            }
        }


      public void processSMSNotification(object objNotifications)
      {
          #region TypeCasting
          TokenNotificationCollection notifications = (TokenNotificationCollection)objNotifications;
          List<PushNotification> notificationList = (List<PushNotification>)notifications.NotificationCollection;
          string twillioStatus = null;
          bool smsSendStatus = false;
          SMSDomin smsSendObj = new SMSDomin();
           UDI.VTOD.Utility.Domain.VTODWebService.VTOD_Utility utilityObj=new UDI.VTOD.Utility.Domain.VTODWebService.VTOD_Utility();
           string phoneNumber = null;
         
          #endregion
          try
          {
              logger.InfoFormat("Start of the processSMSNotification of the domain method.");
              foreach (PushNotification item in notificationList)
              {
                  UDI.VTOD.Utility.Common.DTO.MessageDetails messageObj = new UDI.VTOD.Utility.Common.DTO.MessageDetails();
                  string tableName = utilityObj.getTableName((Int64)item.VtodTripID);
                  using (var db = new DataAccess.VTOD.VTODEntities())
                  {
                      logger.InfoFormat("Vtod Trip ID:{0}:", item.VtodTripID);
                      var trip = db.vtod_trip.Include(tableName).Where(s => s.Id == item.VtodTripID).FirstOrDefault();
                      if (trip != null && trip.sds_trip != null)
                      {
                          phoneNumber = trip.sds_trip.ContactNumberDialingPrefix + trip.sds_trip.ContactNumber;
                      }
                      #region Get the Phone Number
                      if (trip != null && trip.ecar_trip != null)
                      {
                          phoneNumber = trip.ecar_trip.PhoneNumber;
                      }
                      else if (trip != null && trip.taxi_trip != null)
                      {
                          phoneNumber = trip.taxi_trip.PhoneNumber;
                      } 
                      #endregion
                      logger.InfoFormat("Phonenumber:{0}:", phoneNumber);
                  }

                    if (!string.IsNullOrWhiteSpace(phoneNumber))
                    {
                        messageObj.To.Add(phoneNumber);
                    }

                    #region Twilio
                    logger.InfoFormat("Twilio Sending the SMS");
                      messageObj.MessageBody = item.DisplayText;
                      smsSendStatus = smsSendObj.Send(messageObj, ref twillioStatus);
                      logger.InfoFormat("Twilio Sent the SMS status:{0}:", smsSendStatus);
                      #endregion
                      logger.InfoFormat("Start of the Notification Domain Method");
                      #region Database Updation
                      NotificationUpdation(item.ID, smsSendStatus ? UDI.VTOD.Utility.Common.Const.PushWorkFlow.PushWorkFlow_Completed : UDI.VTOD.Utility.Common.Const.PushWorkFlow.PushWorkFlow_Error); 
                      #endregion
                      logger.InfoFormat("End of the Notification Domain Method");
              }
              logger.InfoFormat("End of the processSMSNotification of the domain method.");
          }
          catch (Exception ex)
          {
              logger.Error("processPushNotification", ex);
          }
      }
        public void UpdatePushNotification(object objNotifications)
        {
            #region Init
            #region TypeCasting
            TokenTripCollection notifications = (TokenTripCollection)objNotifications;
            List<MasterTripList> tripList = (List<MasterTripList>)notifications.TripCollection;
            #endregion
            UDI.VTOD.Utility.Domain.VTODWebService.VTOD_Utility utilityObj = new UDI.VTOD.Utility.Domain.VTODWebService.VTOD_Utility();
            #endregion
            try
            {
                if (tripList != null && tripList.Count() > 0)
                {
                    logger.InfoFormat("Push Notification  table updation starts");
                    using (var db = new DataAccess.VTOD.VTODEntities())
                    {
                        foreach (var tripItem in tripList)
                        {
                            logger.InfoFormat("Trip Id :{0}", tripItem.tripID);
                            logger.InfoFormat("Notification Token :{0}", notifications.Token);
                            logger.InfoFormat("Member ID :{0}", tripItem.memberID);
                            if (tripItem.memberID != null && tripItem.memberID != 0)
                            {
                                var membershipController = new UDI.SDS.MembershipController(notifications.Token, TrackTime);
                                logger.InfoFormat("Start--Get Profile Communication Settings");
                                var memberDetails = membershipController.GetProfileCommunicationSettings((Int32)tripItem.memberID);
                                logger.InfoFormat("End--Get Profile Communication Settings");
                                logger.InfoFormat("Push Notification  table updation starts");
                                db.SP_TripReminderNotificationUpdation((Int64)tripItem.memberID,(Int64)tripItem.tripID,UDI.VTOD.Utility.Common.Const.PushWorkFlow.PushWorkFlow_Inserted_TripReminder, UDI.VTOD.Utility.Common.Const.PushWorkFlow.PushWorkFlow_Inserted);
                            }
                        }
                    }
                    logger.InfoFormat("Push Notification  table updation ends");
                }
            }
            catch (Exception ex)
            {
                logger.Error("UpdatePushNotification:", ex);
            }
        }
        public void InsertPushNotification(object objNotifications)
      {
          #region Init
          #region TypeCasting
          TokenTripCollection notifications = (TokenTripCollection)objNotifications;
          List<MasterTripList> tripList = (List<MasterTripList>)notifications.TripCollection;
          #endregion
          VTODWebService.VTOD_Utility utilityObj = new VTODWebService.VTOD_Utility(); 
          #endregion
          try
          {
              if (tripList != null && tripList.Count() > 0)
              {
                  logger.InfoFormat("Push Notification  table insertion starts");
                  using (var db = new DataAccess.VTOD.VTODEntities())
                  {
                      foreach (var tripItem in tripList)
                      {
                          logger.InfoFormat("Trip Id :{0}", tripItem.tripID);
                          logger.InfoFormat("Notification Token :{0}", notifications.Token);
                          logger.InfoFormat("Member ID :{0}", tripItem.memberID);
                            if (tripItem.memberID != null && tripItem.memberID != 0)
                            {
                                var membershipController = new UDI.SDS.MembershipController(notifications.Token, TrackTime);
                                logger.InfoFormat("Start--Get Profile Communication Settings");
                                var memberDetails = membershipController.GetProfileCommunicationSettings((Int32)tripItem.memberID);
                                logger.InfoFormat("End--Get Profile Communication Settings");
                                logger.InfoFormat("Push Notification  table insertion starts");
                                logger.InfoFormat("Member Details :{0}", memberDetails);
                                if (tripItem.Status.ToLower().ToString() ==
                                          UDI.VTOD.Utility.Common.Const.TripStatus.Cancelled.ToString().ToLower())
                                {
                                    db.sp_Utility_PushNotification_CancelTrips((Int64)tripItem.tripID, Common.Const.PushWorkFlow.PushWorkFlow_Cancelled, (Int64)tripItem.memberID, Common.Const.PushWorkFlow.PushWorkFlow_Inserted);
                                }
                                db.SP_PushNotificationInsertion((Int64)tripItem.memberID, null, (Int64)tripItem.tripID, (DateTime)DateTime.UtcNow, Common.Const.PushTypes.Later.ToString(), tripItem.FleetTripCode, tripItem.Status, tripItem.VehicleNumber, tripItem.Firstname, memberDetails.AllowSms, notifications.Token.Username, Common.Const.PushWorkFlow.PushWorkFlow_Inserted);
                            }

                            //BOBO case                            
                            if(tripItem.memberID.HasValue && tripItem.memberID == 0 && tripItem.tripID.HasValue)  
                            {
                                if (db.vtod_trip.Where(x => x.Id == tripItem.tripID.Value && x.IsBOBO == true).Any())
                                {                                    
                                    db.SP_AddAPushNotification("BOBOSMS", tripItem.tripID, tripItem.memberID, DateTime.UtcNow, "1", DateTime.UtcNow.AddMinutes(30) , "ZTrip", tripItem.SharedETALink);                                    
                                }
                            }
                      }
                  }
                  logger.InfoFormat("Push Notification  table insertion ends");
              }
          }
          catch (Exception ex)
          {
              logger.Error("InsertPushNotification:", ex);
          }
      }
      #region Properties
      public TrackTime TrackTime { get; set; }
      #endregion
    }
}
