﻿using System.Configuration;
using UDI.VTOD.Utility.Common.Const;
using UDI.VTOD.Utility.Common.Contracts;

namespace UDI.VTOD.Utility.Domain.Push
{
    public class PushServiceFactory
    {
        public static IPushService GetServiceInstance()
        {            
            var pushProvider = ConfigurationManager.AppSettings["Push_Provider"];
            switch (pushProvider)
            {
                case PushProvider.Parse:
                    return new ParsePushService(pushProvider);
                case PushProvider.Azure:
                    return new AzurePushNotificationService(pushProvider);
                default:
                    return null;
            }
        }        
    }
}
