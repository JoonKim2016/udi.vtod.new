﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Utility.Domain;
using UDI.Utility.Helper;
using UDI.VTOD.Utility.Common.DTO;
using UDI.VTOD.Common.DTO;
using System.Text.RegularExpressions;
using System.ServiceModel.Web;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using UDI.Utility.Serialization;
using UDI.SDS;
using UDI.SDS.MembershipService;
using System.Runtime.Serialization;
using UDI.VTOD.Utility.DataAccess.VTOD;

namespace UDI.VTOD.Utility.Domain.Referral
{
  public  class ReferralDomain
    {
      	public ILog logger;

		/// <summary>
		/// Inserts the records in the Referral table
		/// </summary>
		/// <param name="type"></param>
		/// <param name="phoneNumber"></param>
		/// <param name="memberID"></param>
		/// 
		#region Constructor
        public ReferralDomain()
		{

			logger = LogManager.GetLogger(this.GetType());
			log4net.Config.XmlConfigurator.Configure();
		}
		#endregion

        #region "Public Methods"
        #region AddReferral
        public void AddReferral(vtod_referral_logs referral)
        {
            try
            {                
                using (var db = new DataAccess.VTOD.VTODEntities())
                {
                    db.vtod_referral_logs.Add(referral);
                    db.SaveChanges();

                    logger.Info("Insertion successful");
                }             
            }
            catch (VtodException ex)
            {
                logger.Error("AddReferrer:", ex);
                throw (ex);
            }
            catch (Exception ex)
            {
                logger.Error("AddReferrer:", ex);
                throw (ex);
            }

        }
        #endregion

       
        #endregion


        #region "Check VTOD Trip Status"
        public List<MasterTripList> VTODTripStatus()
        {
            List<MasterTripList> recentTrips = new List<MasterTripList>();
            try
            {
               
                #region Call Domain Method
                logger.InfoFormat("Start - Retreiving records from Vtod_Trip");
                DateTime StartTime = DateTime.UtcNow.AddHours(-2);
                DateTime EndTime = DateTime.UtcNow.AddHours(+35);
                using (var db = new DataAccess.VTOD.VTODEntities())
                {

                    var tripList = db.SP_Utility_CheckTripStatus(StartTime, EndTime, Common.Cost.ReferrerCreditType.CompletedTrip, Common.Cost.ReferrerCreditFlow.Credit_Inserted, Common.Cost.ReferrerCreditFlow.Credit_Inprogress).ToList();
                    logger.InfoFormat("Count of Trip list retrieved :{0}:", tripList.Count());
                    if (tripList != null && tripList.Any())
                    {
                        foreach (var tripItem in tripList)
                        {
                            MasterTripList newMasterTripList = new MasterTripList();                           
                            newMasterTripList.tripID = tripItem.tripID == null ? 0 : tripItem.tripID;
                            newMasterTripList.ID = tripItem.ID == null ? 0 : tripItem.ID;
                            newMasterTripList.processType = tripItem.ProcessType == null ? 0 : tripItem.ProcessType;
                            newMasterTripList.memberID = tripItem.memberID == null ? 0 : tripItem.memberID;
                            newMasterTripList.Firstname = tripItem.FirstName;
                            newMasterTripList.FleetTripCode = tripItem.FleetTripCode == null ? 0 : tripItem.FleetTripCode;
                            newMasterTripList.WorkFlow = tripItem.Workflow == null ? 0 : tripItem.Workflow;

                            recentTrips.Add(newMasterTripList);
                        }
                    }

                }
                logger.InfoFormat("End - Retreiving records from Vtod_Trip");
                
                #endregion
            }
            catch (Exception ex)
            {
                logger.Error("VTODTripStatus:", ex);
                //throw (ex);
            }
            return recentTrips;
        }

        #endregion

        //#region Cancelled Trip List
        //public void RemoveCancelledTripList(List<MasterTripList> lstCancelledTrip)
        //{
        //    try
        //    {
        //        #region Call Domain Method
        //        logger.InfoFormat("Start-Updation in the Referral table for cancelled trip");
        //        string cancelledList = null;
        //        if (lstCancelledTrip != null && lstCancelledTrip.Count > 0)
        //        {
        //            if (lstCancelledTrip.Count > 1)
        //            {
        //                cancelledList = string.Join(",", lstCancelledTrip.Select(p => p.ID).ToArray());
        //                cancelledList = cancelledList + ",";
        //            }
        //            else if (lstCancelledTrip.Count == 1)
        //            {
        //                cancelledList = lstCancelledTrip[0].ID.ToString() + ",";
        //            }
        //            using (var db = new DataAccess.VTOD.VTODEntities())
        //            {
        //                db.SP_Utility_RegisterReferral_Updation(cancelledList, UDI.VTOD.Utility.Common.Cost.ReferrerCreditFlow.Trip_Cancelled, UDI.VTOD.Utility.Common.Cost.ReferrerCreditType.CompletedTrip, 0, 0, UDI.VTOD.Utility.Common.Cost.TypesOfOperations.Cancelled);
        //            }
        //        }
        //        logger.InfoFormat("Number Of records:{0}:", lstCancelledTrip.Count());
        //        logger.InfoFormat("End-Updation in the Referral table for the cancelled trip");
        //        #endregion
        //    }
        //    catch (Exception ex)
        //    {
        //        logger.Error("RemoveCancelledTripList:", ex);
        //        //throw (ex);
        //    }
        //}
        //#endregion

    //    #region "Booking Referral List Updation"
    //    /// <summary>
    //    /// Updates the Referral table 
    //    /// </summary>
    //    /// <param name="ID"></param>
    //    /// <param name="ReferralFlow"></param>
    //    /// 
    //    public void ReferralBookingProcess(List<MasterTripList> referralObject)
    //    {
    //        try
    //        {
    //            #region Call Domain Method
    //            logger.InfoFormat("Start-Updation in the Referral table");
    //            string bookCompletedList = null;
    //            string revertedList = null;
    //            if (referralObject != null && referralObject.Count > 0)
    //            {

				//	#region Completed
				//	if (referralObject.Count > 1)
				//	{
				//		bookCompletedList = string.Join(",", referralObject.Where(p => p.Status.ToLower().ToString() == UDI.VTOD.Utility.Common.Const.TripStatus.Completed.ToString().ToLower().ToString()).Select(p => p.ID).ToArray());
				//		bookCompletedList = bookCompletedList + ",";
				//		revertedList = string.Join(",", referralObject.Where(p => p.Status.ToLower().ToString() != UDI.VTOD.Utility.Common.Const.TripStatus.Completed.ToString().ToLower().ToString()).Select(p => p.ID).ToArray());
				//	}
				//	else if (referralObject.Count == 1)
				//	{
				//		if (referralObject[0].Status.ToLower().ToString() == UDI.VTOD.Utility.Common.Const.TripStatus.Completed.ToString().ToLower().ToString())
				//		{
				//			bookCompletedList = referralObject[0].ID.ToString() + ",";
				//		}
				//		if (referralObject[0].Status.ToLower().ToString() != UDI.VTOD.Utility.Common.Const.TripStatus.Completed.ToString().ToLower().ToString())
				//		{
				//			revertedList = referralObject[0].ID.ToString() + ",";
				//		}
				//	}
				//	using (var db = new DataAccess.VTOD.VTODEntities())
				//	{
				//		db.SP_Utility_RegisterReferral_Updation(bookCompletedList, UDI.VTOD.Utility.Common.Cost.ReferrerCreditFlow.Credit_Completed, UDI.VTOD.Utility.Common.Cost.ReferrerCreditType.CompletedTrip, UDI.VTOD.Utility.Common.Cost.ReferrerCreditFlow.Credit_Inserted, UDI.VTOD.Utility.Common.Cost.ReferrerCreditType.CreditCompleted, UDI.VTOD.Utility.Common.Cost.TypesOfOperations.Book);
				//		db.SP_Utility_RegisterReferral_Updation(revertedList, UDI.VTOD.Utility.Common.Cost.ReferrerCreditFlow.Credit_Inserted, UDI.VTOD.Utility.Common.Cost.ReferrerCreditType.CompletedTrip, 0, 0, UDI.VTOD.Utility.Common.Cost.TypesOfOperations.Cancelled);
				//	}
				//	#endregion

				//	#region Another Status

				//	#endregion
				//}
    //            logger.InfoFormat("Number Of records:{0}:", referralObject.Count());
    //            logger.InfoFormat("End-Updation in the Referral table");
    //            #endregion
    //        }
    //        catch (Exception ex)
    //        {
    //            logger.Error("ReferralBookingProcess:", ex);
    //            //throw (ex);
    //        }

    //    }
    //    #endregion

        //#region "Register Credit Process(SDS)"
        //public void ReferralRegisterCredit(UDI.VTOD.Utility.Common.DTO.TokenReferralRegisterCollection referralRegisterList)
        //{

        //    try
        //    {
        //        #region Call Domain Method
        //        using (var db = new DataAccess.VTOD.VTODEntities())
        //        {


        //            logger.InfoFormat("Start - Referral Credit Process for Registration of Referral Domain");
        //            #region Loop through the Parameter "Referral Register Collection"
        //            if (referralRegisterList != null && referralRegisterList.TokenRegisterCollection.Any())
        //            {

        //                foreach (var item in referralRegisterList.TokenRegisterCollection)
        //                {
        //                    #region Looping through each referral register record
        //                    logger.InfoFormat("Member ID {0}:", item.MemberID);
        //                    logger.InfoFormat("Amount {0}:", item.ReferralCreditAmount);
        //                    #region Call SDS
        //                    logger.InfoFormat("Start--Push Credit to SDS");
        //                    bool result = false;
        //                    var sdsMembershipController = new UDI.SDS.MembershipController(referralRegisterList.Token, null);
        //                    try
        //                    {
        //                        string sdsResult = sdsMembershipController.CreateNewZTripCredit((decimal)item.ReferralCreditAmount, item.MemberID.ToInt32(), ZTripCreditType.ReferralNewUser);
        //                        #region Update DB
        //                        if (sdsResult.Contains("Success"))
        //                        {
        //                            logger.InfoFormat("Start--Updation of the Referral table");
        //                            ReferralUpdation(item.ID, UDI.VTOD.Utility.Common.Cost.ReferrerCreditFlow.Credit_Completed);
        //                            logger.InfoFormat("End--Updation of the Referral table");
        //                        }
        //                        else
        //                        {
        //                            logger.InfoFormat("Error in SDS");
        //                            ReferralUpdation(item.ID, UDI.VTOD.Utility.Common.Cost.ReferrerCreditFlow.Credit_SDS_Error);
        //                            logger.InfoFormat("Updation of the Referral table finished with the sds error code");
        //                        }
        //                        #endregion

        //                    }
        //                    catch(Exception exp)
        //                    {
        //                        logger.InfoFormat("Error in SDS:{0}:", exp.Message.ToString());
        //                        ReferralUpdation(item.ID, UDI.VTOD.Utility.Common.Cost.ReferrerCreditFlow.Credit_SDS_Error);
        //                        logger.InfoFormat("Updation of the Referral table finished");
        //                    }
        //                    logger.InfoFormat("End--Push Credit to SDS");
        //                    #endregion
                           
        //                    #endregion
        //                }
        //            }
        //            #endregion
        //            logger.InfoFormat("End - Referral Credit Process for Registration of Referral Domain");
        //        }
        //        #endregion

        //    }

        //    catch (Exception ex)
        //    {
        //        logger.Error("ReferralRegisterCredit:", ex);
        //    }

        //}

        //#endregion

        //#region "Book Credit Process(SDS/Branch)"
        //public void ReferralBookCredit(UDI.VTOD.Utility.Common.DTO.TokenReferralRegisterCollection referralRegisterList)
        //{

        //    try
        //    {
        //        #region Call Domain Method
        //        using (var db = new DataAccess.VTOD.VTODEntities())
        //        {


        //            logger.InfoFormat("Start - Referral Credit Process for Booking of Referral Domain");
        //            #region Loop through the Parameter "Referral Book Collection"
        //            if (referralRegisterList != null && referralRegisterList.TokenRegisterCollection.Any())
        //            {

        //                foreach (var item in referralRegisterList.TokenRegisterCollection)
        //                {
        //                    #region Looping through each referral register record
        //                    logger.InfoFormat("Member ID {0}:", item.MemberID);
        //                    logger.InfoFormat("Amount {0}:", item.ReferralCreditAmount);
        //                    #region Call SDS
        //                    logger.InfoFormat("Start-Push Credit to SDS");
        //                    bool result = false;
        //                    var sdsMembershipController = new UDI.SDS.MembershipController(referralRegisterList.Token, null);
        //                    try
        //                    {
        //                        string sdsResult = sdsMembershipController.CreateNewZTripCredit((decimal)item.ReferralCreditAmount, item.ReferrerMemberID.ToInt32(), ZTripCreditType.ReferralTripCompleted);
        //                        if (sdsResult.Contains("Success"))
        //                        {
        //                            #region Call Branch
        //                            logger.InfoFormat("Start-Push Credit to Branch");
        //                            try
        //                            {
        //                                SendBranchCredit(item.MemberID.ToString(), System.Math.Round((decimal)item.ReferralCreditAmount).ToString());
        //                                #region Update DB
        //                                logger.InfoFormat("Start-Updation of the Referral table");
        //                                ReferralUpdation(item.ID, UDI.VTOD.Utility.Common.Cost.ReferrerCreditFlow.Credit_Completed);
        //                                logger.InfoFormat("End-Updation of the Referral table");
        //                                #endregion
        //                            }
        //                            catch(Exception e)
        //                            {
        //                                logger.InfoFormat("Error in BranchS:{0}:", e.Message.ToString());
        //                                ReferralUpdation(item.ID, UDI.VTOD.Utility.Common.Cost.ReferrerCreditFlow.Credit_Branch_Error);
        //                                logger.InfoFormat("Updation of the Referral table done");
        //                            }
        //                            logger.InfoFormat("End-Push Credit to Branch");
        //                            #endregion
                                   
        //                        }
        //                        else
        //                        {
        //                            logger.InfoFormat("Error in SDS");
        //                            ReferralUpdation(item.ID, UDI.VTOD.Utility.Common.Cost.ReferrerCreditFlow.Credit_SDS_Error);
        //                            logger.InfoFormat("Updation of the Referral table done with the sds error code");
        //                        }
        //                    }
        //                    catch(Exception exp)
        //                    {
        //                        logger.InfoFormat("Error in SDS:{0}:",exp.Message.ToString());
        //                        ReferralUpdation(item.ID, UDI.VTOD.Utility.Common.Cost.ReferrerCreditFlow.Credit_SDS_Error);
        //                        logger.InfoFormat("Updation of the Referral table done");
        //                    }
        //                    logger.InfoFormat("End-Push Credit to SDS");
        //                    #endregion
                        
        //                    #endregion
        //                }
        //            }
        //            #endregion
        //            logger.InfoFormat("End - Referral Credit Process for Booking of Referral Domain");
        //        }
        //        #endregion

        //    }

        //    catch (Exception ex)
        //    {
        //        logger.Error("ReferralRegisterCredit:", ex);
        //    }

        //}

        //#endregion

        #region Send Credit to branch
        public void SendBranchCredit(string memberID, string amount)
        {

            logger.InfoFormat("Start--Branch Push Credit");
            #region Init
            var headers = new Dictionary<string, string>();
            var contentType = string.Empty;
            var method = string.Empty;
            var response = string.Empty;
            var url = string.Empty;
            var content = string.Empty;
            var branchKey = string.Empty;
            var branchSecurityKey = string.Empty;
            var status = string.Empty;
            branchKey = System.Configuration.ConfigurationManager.AppSettings["BranchKey"];
            branchSecurityKey = System.Configuration.ConfigurationManager.AppSettings["BranchSecret"];
            logger.InfoFormat("Branch Key", branchKey);
            logger.InfoFormat("Branch Secret", branchSecurityKey);
            logger.InfoFormat("Member ID", memberID);
            logger.InfoFormat("Amount", amount);
            logger.InfoFormat("Start--Branch Push Credit");
            UDI.VTOD.Utility.Common.DTO.BranchCredit branchCreditParameter = new UDI.VTOD.Utility.Common.DTO.BranchCredit();
            #endregion

            #region Parameters
            url = System.Configuration.ConfigurationManager.AppSettings["BranchURL"];
            url = url + "?branch-key=" + branchKey;
            contentType = "application/json; charset=utf-8";
            method = "POST";
            branchCreditParameter.branch_key = branchKey;
            branchCreditParameter.branch_secret = branchSecurityKey;
            branchCreditParameter.identity = memberID;
            branchCreditParameter.amount = amount;
            #endregion

            logger.InfoFormat("Username{0},Securitykey{1}", branchKey, branchSecurityKey);
            try
            {
                content = branchCreditParameter.JsonSerialize().ToString();
                response = UDI.Utility.Helper.WebRequestHelper.ProcessWebRequest(
                           url,
                           content,
                           contentType,
                           method,
                           headers,
                          out status);
                logger.InfoFormat("Response Branch", response);
                logger.InfoFormat("End--Branch Push Credit");
            }
            catch (Exception ex)
            {
                #region Return
                logger.ErrorFormat(ex.ToString());
                throw ex;
                #endregion
            }
        }
        #endregion

        #region "Referral Table Updation"
        /// <summary>
        /// Updates the Referral table 
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="ReferralFlow"></param>
        //public void ReferralUpdation(Int64 ID, int ReferralFlow)
        //{
        //    try
        //    {
        //        #region Call Domain Method
        //        logger.InfoFormat("Start-Updation in the Referral table: inputs ID:{0},Referral Flow:{1}:", ID, ReferralFlow);
        //        using (var db = new DataAccess.VTOD.VTODEntities())
        //        {
        //            db.SP_Utility_RegisterReferral_Updation(ID.ToString(), ReferralFlow, 0, UDI.VTOD.Utility.Common.Cost.ReferrerCreditFlow.Trip_Duplicate, 0, UDI.VTOD.Utility.Common.Cost.TypesOfOperations.Register);
        //        }
        //        logger.InfoFormat("End-Updation in the Referral table");
        //        #endregion
        //    }
        //    catch (Exception ex)
        //    {
        //        logger.Error("ReferralUpdation:", ex);
        //        //throw (ex);
        //    }

        //}

        #endregion

        //#region "Retrieve Referral Details"
        ///// <summary>
        ///// Retrieves the Information from the Referral table
        ///// </summary>
        ///// <param name="status"></param>
        ///// <param name="prevstatus"></param>
        ///// <param name="duplicatestatus"></param>
        ///// <returns></returns>
        //public List<UDI.VTOD.Utility.Common.DTO.ReferralDetails> ListReferralRegister(int registerType, int bookType, int status, int nextStatus)
        //{
        //    List<UDI.VTOD.Utility.Common.DTO.ReferralDetails> result = new List<UDI.VTOD.Utility.Common.DTO.ReferralDetails>();
        //    try
        //    {
        //        #region Call Domain Method
        //        using (var db = new DataAccess.VTOD.VTODEntities())
        //        {
                   
        //            logger.InfoFormat("Start - Retrieving the Referral details from the Referral-Trip table with the Status:{0}:", status);
        //            var listReferrerDetails = db.sp_Utility_GetReferralDetails(registerType, bookType, status, nextStatus).ToList();
        //            if (listReferrerDetails != null && listReferrerDetails.Any())
        //            {
        //                foreach (var referralItem in listReferrerDetails)
        //                {
        //                    var referralInfo = referralItem;
        //                    if (referralInfo.ReferralCreditType == UDI.VTOD.Utility.Common.Cost.ReferrerCreditType.Register)
        //                    {
        //                    result.Add(new UDI.VTOD.Utility.Common.DTO.ReferralDetails() { ID = referralInfo.ID, TripID = referralInfo.TripID, MemberID = referralInfo.MemberID, ReferrerMemberID = referralInfo.ReferrerID, ReferralCreditAmount = referralInfo.ReferralCredit, ReferralCreditFlow = referralInfo.ReferralCreditFlow, ReferralCreditType = referralInfo.ReferralCreditType });
                            
        //                    }
        //                    else
                                
        //                    {
        //                        if(referralInfo.ReferralCreditType == UDI.VTOD.Utility.Common.Cost.ReferrerCreditType.CompletedTrip || referralInfo.ReferralCreditType == UDI.VTOD.Utility.Common.Cost.ReferrerCreditType.CreditCompleted)
        //                        {

        //                            result.Add(new UDI.VTOD.Utility.Common.DTO.ReferralDetails() { ID = referralInfo.ID, TripID = referralInfo.TripID, MemberID = referralInfo.ReferrerID, ReferrerMemberID = referralInfo.ReferrerID, ReferralCreditAmount = referralInfo.ReferralCredit, ReferralCreditFlow = referralInfo.ReferralCreditFlow, ReferralCreditType = referralInfo.ReferralCreditType });
                           
        //                        }
        //                    }
        //                }
        //            }
        //            logger.InfoFormat("End - Retrieving the Referral details from the Referral-Trip table with the Status:.Count{0}:", listReferrerDetails.Count());
                    
        //        }
        //        #endregion
        //    }
        //    catch (Exception ex)
        //    {
        //        logger.Error("ListPhoneSMSInserted:", ex);
        //        //throw (ex);
        //    }
        //    return result;
        //}

        //#endregion 
        
    }
}
