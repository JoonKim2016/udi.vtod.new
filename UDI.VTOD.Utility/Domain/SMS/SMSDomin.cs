﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Utility.Common.Contracts;
using UDI.VTOD.Utility.Common.DTO;
namespace UDI.VTOD.Utility.Domain.Verification
{
    #region Class
    public class SMSDomin : ISMSService
    {
        #region Public Methods

        #region Send SMS

        public bool Send(MessageDetails message, ref string errorMessage)
        {
            #region Provider Domain Method
            var smsDomain = SMSFactory.CreateProvider();

            bool result = smsDomain.Send(message, ref errorMessage);

            #endregion
            return result;
		}

        #endregion
        //public bool Receive(UDI.VTOD.Utility.Common.DTO.MessageDetails m ,string errorMessage,int status)
        //{
        //    var smsDomain = SMSFactory.CreateProvider();
        //    bool result = smsDomain.Receive(m, errorMessage, status);
        //    return result;
        //}

        #region Receive SMS

        public List<string> Receive(UDI.VTOD.Utility.Common.DTO.MessageDetails m, ref string errorMessage)
        {

            #region Provider Domain Method

            var smsDomain = SMSFactory.CreateProvider();
            List<string> receivedSMS = new List<string>();
            receivedSMS = smsDomain.Receive(m, ref errorMessage);

            #endregion
            return receivedSMS;
        }

        #endregion
        #endregion
    }
    #endregion
}
