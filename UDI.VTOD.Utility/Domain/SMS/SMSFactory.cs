﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Utility.Common.Contracts;
using UDI.VTOD.Utility.Domain.SMS.TwilioService;

namespace UDI.VTOD.Utility.Domain.Verification
{
	public class SMSFactory
	{

		public static Common.Contracts.ISMSService CreateProvider()
		{
			ISMSService result = null;
			string smsProvider = System.Configuration.ConfigurationManager.AppSettings["SMSProvider"].ToString();
			if (smsProvider == Common.Cost.SMSProvider.Twilio)
			{
				return new TwilioDomain();
			}

			return result;
		}
	}
}
