﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using Twilio; 
using log4net;
using UDI.VTOD.Utility.Common.DTO;
using UDI.Utility.Helper;
namespace UDI.VTOD.Utility.Domain.SMS.TwilioService
{
    #region Class
    
    public class TwilioDomain : Common.Contracts.ISMSService
    {
        #region Fields
        public ILog logger;
        #endregion

        #region Constructor
        public TwilioDomain()
        {
            logger = LogManager.GetLogger(this.GetType());
            log4net.Config.XmlConfigurator.Configure();
        }
        #endregion

        #region Public Method
        /// <summary>
        /// To send SMS to the user through Twilio
        /// </summary>
        /// <param name="m"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        #region SMS Send
        public bool Send(MessageDetails m, ref string errorMessage)
        {
            bool result = false;
            errorMessage = string.Empty;
            #region Parameters
            TwilioRequest twilioObj = new TwilioRequest();
            twilioObj.From = m.From;
            twilioObj.MessageBody = m.MessageBody;
            twilioObj.To = m.To;
            #endregion
            #region Configuration  AppSettings
            twilioObj.Account = System.Configuration.ConfigurationManager.AppSettings["Notification_SMS_Twilio_Account"];
            twilioObj.Token = System.Configuration.ConfigurationManager.AppSettings["Notification_SMS_Twilio_Token"];
            twilioObj.From = System.Configuration.ConfigurationManager.AppSettings["Notification_SMS_Twilio_From"];
            #endregion
            logger.InfoFormat("Start the SMS Sending  - From:{0},To:{1},Account:{2},Token:{3},Messagebody:{4}:", twilioObj.From, twilioObj.To[0].ToString(), twilioObj.Account, twilioObj.Token, twilioObj.MessageBody);
            try
            {
                #region Twilio
                if (ValidateTwilioSMSMessage(twilioObj))
                {
                    TwilioRestClient twilio = new TwilioRestClient(twilioObj.Account, twilioObj.Token);
                    foreach (string t in m.To)
                    {
                        #region Twilio SMS Sent                        
                        var response = twilio.SendMessage(twilioObj.From, t, twilioObj.MessageBody);

                        if (response == null || string.IsNullOrEmpty(response.Status) || response.Status.ToLower() == "failed")
                        {
                            #region Log
                            string error = string.Format("Unable to send SMS through Twilio. From={0}, To={1}, Message={2}, DateTime={3}:", m.From, t, m.MessageBody, DateTime.Now);
                            if (response.RestException != null)
                            {
                                errorMessage = string.Format("{0}\r\n{1}\r\n{2}:", errorMessage, error, response.RestException.Message.ToString());
                            }
                            else
                            {
                                errorMessage = string.Format("{0}\r\n{1}:", errorMessage, error);
                            }
                          
                            logger.InfoFormat("Error in Twilio:", errorMessage);
                            #endregion
                        }
                        #endregion
                    }

                    #region Twilio SMS Success/Failure
                    if (string.IsNullOrWhiteSpace(errorMessage))
                    {
                        result = true;
                        logger.InfoFormat("Success in Sending SMS");
                    }
                    else
                    {
                        result = false;
                        logger.InfoFormat("Failure in Sending SMS");
                    }
                    #endregion


                }
                else
                {
                    #region Log
                    result = false;
                    errorMessage = "Invalid TwilioSMSMessage object.";
                    logger.InfoFormat("Failure in Sending SMS:", errorMessage);
                    #endregion
                }
                #endregion

            }
            catch (Exception ex)
            {
                #region Exception
                logger.Error(ex);
                errorMessage = string.Format("Exception:{0}, StackPrintTrace:{1}:", ex.Message, ex.StackTrace.ToString());
                throw new Exception(errorMessage);
                #endregion
            }
            return result;
        } 
        #endregion

        /// <summary>
        /// Receives all the messages which Twilio has received from a user
        /// </summary>
        /// <param name="m"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>

        #region SMS Receive
        public List<string> Receive(UDI.VTOD.Utility.Common.DTO.MessageDetails m, ref string errorMessage)
        {
            #region Init
            errorMessage = string.Empty;
            List<string> MessageList = new List<string>();
            UDI.VTOD.Utility.Domain.SMS.TwilioService.TwilioRequest twilioObj = new UDI.VTOD.Utility.Domain.SMS.TwilioService.TwilioRequest(); 
            #endregion
            #region Read From Config
            twilioObj.Account = System.Configuration.ConfigurationManager.AppSettings["Notification_SMS_Twilio_Account"];
            twilioObj.Token = System.Configuration.ConfigurationManager.AppSettings["Notification_SMS_Twilio_Token"];
            twilioObj.To.Add(System.Configuration.ConfigurationManager.AppSettings["Notification_SMS_Twilio_From"]);
            int margin = System.Configuration.ConfigurationManager.AppSettings["SMSInboundMarginTimeInMinutes"].ToInt32();
            #endregion
            logger.InfoFormat("Start the Receiving SMS  - From:{0},To:{1},Account:{2},Token:{3}:", m.From, twilioObj.To[0].ToString(), twilioObj.Account, twilioObj.Token);
            try
            {
                #region Twilio
                if (ValidateTwilioSMSMessage(twilioObj))
                {
                    Twilio.TwilioRestClient twilio = new Twilio.TwilioRestClient(twilioObj.Account, twilioObj.Token);
                    UDI.VTOD.Utility.Domain.SMS.TwilioService.TwilioResponse messageReceivedObj = new UDI.VTOD.Utility.Domain.SMS.TwilioService.TwilioResponse();
                    var twilioParameters = new MessageListRequest();
                    if (twilioObj.To != null && twilioObj.To.Any())
                    {
                        twilioParameters.To = twilioObj.To[0].ToString();
                    }
                    if (m.From != null && m.From.Any())
                    {
                       #region Twilio Receive
		               twilioParameters.From = m.From.ToString();
                       twilioParameters.DateSent = m.MessageSentDate.Subtract(TimeSpan.FromMinutes(margin));
                       twilioParameters.DateSentComparison = ComparisonType.GreaterThanOrEqualTo;
                        var Messages = twilio.ListMessages(twilioParameters);
                        logger.InfoFormat("SMS Received number :{0}:", twilioParameters.To);
                        logger.InfoFormat("Count of SMS Received :{0}:", Messages.Messages.Count());
	                    #endregion;
                        if (Messages == null || Messages.Messages.Count() == 0 || !Messages.Messages.Any())
                        {

                          #region Log
		                    string error = string.Format("Unable to receive SMS through Twilio. From={0}, To={1},  DateTime={2}:", twilioParameters.From, twilioParameters.To, DateTime.Now);
                            if (Messages.RestException != null)
                            {
                                errorMessage = string.Format("{0}\r\n{1}\r\n{2}:", errorMessage, error, Messages.RestException.Message.ToString());
                            }
                            else
                            {
                                errorMessage = string.Format("{0}\r\n{1}:", errorMessage, error);
                            }
                            logger.InfoFormat("Error in Receiving:", errorMessage); 
	                    #endregion
                        }
                        else
                        {
                            #region Sending the List of SMS 
                            MessageList.AddRange(Messages.Messages.Where(p => p.DateSent > m.MessageSentDate).OrderByDescending(s => s.DateSent).Select(p => p.Body).AsEnumerable().Take(1));
                            //MessageList.AddRange(Messages.Messages.OrderByDescending(s => s.DateSent).Select(p => p.Body).AsEnumerable().Take(1));
                            return MessageList; 
                            #endregion
                        }
                    }

                    else
                    {
                      #region Log
		                logger.InfoFormat("From{0} not Found:", m.From);
                        errorMessage = "From{0} not Found";
                        return MessageList; 
	                    #endregion
                    }
                }
                else
                {
                    #region Log
		            errorMessage = "Invalid TwilioSMSMessage object.";
                    logger.InfoFormat("Error In Twilio Object:", errorMessage);
                    return MessageList;
	               #endregion
                } 
                #endregion
            }
            catch (Exception ex)
            {
               #region Log
		        logger.Error(ex);
                errorMessage = string.Format("Exception:{0}, StackPrintTrace:{1}:", ex.Message, ex.StackTrace.ToString());
                throw new Exception(errorMessage); 
	        #endregion
            }
            return MessageList;;
        } 
        #endregion

        #endregion

        #region Private Method

        private bool ValidateTwilioSMSMessage(UDI.VTOD.Utility.Domain.SMS.TwilioService.TwilioRequest m)
        {
            bool result = false;
            //validate twilio account and token
            if (!string.IsNullOrWhiteSpace(m.Account) && !string.IsNullOrWhiteSpace(m.Token))
            {
                result = true;
            }
            else
            {
                result = false;
            }


            return result;
        }
        #endregion
    }

    #endregion
}
