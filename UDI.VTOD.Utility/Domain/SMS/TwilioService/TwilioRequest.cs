﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Utility.Domain.SMS.TwilioService
{
    #region Class
    /// <summary>
    /// The Message Object specific to Twilio
    /// </summary>
    public class TwilioRequest: UDI.VTOD.Utility.Common.DTO.MessageDetails
    {
        #region Properties
        public string Account { set; get; }
        public string Token { set; get; } 
        #endregion
    }
    #endregion
}
