﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Utility.Domain.SMS.TwilioService
{
#region Class

    public class TwilioResponse : UDI.VTOD.Utility.Common.DTO.MessageDetails
    {
        #region Properties

        public string Message { set; get; }
        public string DateReceived { set; get; }

        #endregion
    }

#endregion
}
