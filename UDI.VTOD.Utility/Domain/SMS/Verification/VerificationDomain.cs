﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Utility.Domain;
using UDI.Utility.Helper;
using UDI.VTOD.Utility.Common.DTO;
using UDI.VTOD.Common.DTO;
using System.Text.RegularExpressions;
using System.ServiceModel.Web;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using UDI.Utility.Serialization;
using UDI.VTOD.Utility.Model;
namespace UDI.VTOD.Utility.Domain.Verification
{
	#region Class

	public class VerificationDomain
	{

		public ILog logger;

		/// <summary>
		/// Inserts the records in the Verification table
		/// </summary>
		/// <param name="type"></param>
		/// <param name="phoneNumber"></param>
		/// <param name="memberID"></param>
		/// 
		#region Constructor
		public VerificationDomain()
		{

			logger = LogManager.GetLogger(this.GetType());
			log4net.Config.XmlConfigurator.Configure();
		}
		#endregion

        #region "Public Method"

        #region "PhoneSMS Records Insertion"
        public void PhoneSMSInsertion(string type, string phoneNumber, Int64 memberID, string VerifyType)
		{
			try
			{
				#region Parameters
                int typeID, verificationID;
                string typeDesc, verificationDesc;
                int? rtvalue=0;
                int? randomNumber = 0;
                int? verType = 0;
				#endregion

                #region Determining the Verification Mode
                verificationID = UDI.VTOD.Utility.Common.Cost.VerificationID.Phone;
                verificationDesc = UDI.VTOD.Utility.Common.Cost.VerificationDesc.Phone; 
                #endregion
				#region Determining the TypeID,Type
				if (type.ToUpper() == UDI.VTOD.Utility.Common.Cost.VerificationTypeDesc.Phone_SMS)
				{
                    typeID = UDI.VTOD.Utility.Common.Cost.VerificationTypeID.Phone_SMS;
                    typeDesc = UDI.VTOD.Utility.Common.Cost.VerificationTypeDesc.Phone_SMS;
				}
				else
				{
					typeID = UDI.VTOD.Utility.Common.Cost.VerificationTypeID.Phone_Call;
                    typeDesc = UDI.VTOD.Utility.Common.Cost.VerificationTypeDesc.Phone_Call;

				}
				#endregion

                #region Generating a Verification Code
                if (!string.IsNullOrEmpty(VerifyType) && !string.IsNullOrWhiteSpace(VerifyType))
                {
                    if (VerifyType.ToLower() == "code")
                    {
                        Random rnd = new Random();
                        randomNumber = rnd.Next(1000, 9999); //Generating a four digit verification code
                        verType = UDI.VTOD.Utility.Common.Const.VerificationType.VerificationByCode;
                    }
                    else
                    {
                        verType = UDI.VTOD.Utility.Common.Const.VerificationType.VerificationByText;
                    }
                }
                else
                {
                    verType = UDI.VTOD.Utility.Common.Const.VerificationType.VerificationByText;
                }
                #endregion
                #region Call Domain Method

                logger.InfoFormat("Insertion in the Verification table: inputs typeID:{0},typeDesc:{1},phoneNumber:{2},memberID:{3}:", typeID, typeDesc, phoneNumber, memberID);
				using (var db = new DataAccess.VTOD.VTODEntities())
				{

					int expTime = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SMSExpirationWindowTimeInMinutes"]);
                    logger.InfoFormat("Expiration Time {0}:", expTime);
                    rtvalue = db.sp_VerifyPhone(phoneNumber, memberID, verificationID, verificationDesc, typeID, typeDesc, expTime, UDI.VTOD.Utility.Common.Cost.VerificationFlow.Phone_SMS_Inserted, null, verType, randomNumber.ToString()).First();
                    if (rtvalue == 1)
                    {
                        logger.InfoFormat("Your phone is already verified.:Status{0}:", rtvalue);
                        throw VtodException.CreateException(VTOD.Common.DTO.Enum.ExceptionType.Utility, 9009);
                    }
                    else if (rtvalue == 6)
                    {
                        logger.InfoFormat("This phone is already verified for another user.:Status{0}:", rtvalue);
                        throw VtodException.CreateException(VTOD.Common.DTO.Enum.ExceptionType.Utility, 9010);
                    }
                    logger.InfoFormat("Status{0}:", rtvalue);
                    logger.InfoFormat("Insertion Completed in the Verification table");
                }
				#endregion
			}
            catch (VtodException ex)
            {
                logger.Error("PhoneSMSInsertion:", ex);
                throw (ex);

            }
			catch (Exception ex)
			{
				logger.Error("PhoneSMSInsertion:", ex);
				throw (ex);

			}

		}
        #endregion

		#region "Phone Verification"
		/// <summary>
		/// Verifies a phone number by checking with the Isverify field of the Verification table
		/// </summary>
		/// <param name="type"></param>
		/// <param name="phoneNumber"></param>
		/// <param name="memberID"></param>
		/// <returns></returns>
        public bool? IsPhoneVerify(string phoneNumber, Int64 memberID)
		{
			try
			{
				#region Parameter Declaration

				bool? isVerify = false;
				int status = Common.Cost.VerificationFlow.Phone_SMS_Completed;
                int TypeID = Common.Cost.VerificationID.Phone;
				#endregion

				#region Call Domain Method
				using (var db = new DataAccess.VTOD.VTODEntities())
				{
                    logger.InfoFormat("Verify the Phone number from Verification table: inputs phoneNumber:{0},memberID:{1},typeID:{2}:", phoneNumber, memberID, TypeID);
                    var listVerify = db.SP_Utility_PhoneSMS_Verification(phoneNumber, memberID, TypeID).FirstOrDefault();
					if (listVerify != null)
					{
						isVerify = listVerify;
					}
					logger.InfoFormat("Verified the phone number with status as :", isVerify);

				}
				#endregion

				return isVerify;
			}
            catch (VtodException ex)
            {
                logger.Error("IsPhoneVerify:", ex);
                throw (ex);
            }
			catch (Exception ex)
			{
				logger.Error("IsPhoneVerify:", ex);
				throw (ex);
			}

		}
        
		#endregion

        #region DeletePhone
        public bool? DeletePhone(string phoneNumber,Int64 memberID)
        {
            try
            {
                #region Parameter Declaration

                bool? rtvalue = false;
                #endregion

                #region Call Domain Method
                using (var db = new DataAccess.VTOD.VTODEntities())
                {
                    logger.InfoFormat("Delete the Phone number from Verification table: inputs phoneNumber:{0},memberID:{1}:", phoneNumber, memberID);
                    db.sp_DeletePhone(phoneNumber, memberID);
                    logger.Error("Deletion Successful");
                  
                }
                #endregion

                return rtvalue;
            }
            catch (VtodException ex)
            {
                logger.Error("DeletePhone:", ex);
                throw (ex);
            }
            catch (Exception ex)
            {
                logger.Error("DeletePhone:", ex);
                throw (ex);
            }

        } 
        #endregion

        #region "Phone Number Validation During Registration"
        /// <summary>
        /// Checking at the Time Of Registration whether the phone is Valid or not
        /// </summary>
        /// <param name="phoneNumber"></param>        
        /// <returns></returns>
        public bool IsPhoneTaken(string phoneNumber)
		{
			try
			{
				#region Parameter Declaration

				bool isVerify = false;
                int TypeID = Common.Cost.VerificationID.Phone;
				#endregion

				#region Call Domain Method
				using (var db = new DataAccess.VTOD.VTODEntities())
				{
                    logger.InfoFormat("Verify the Phone number from Verification table: inputs phoneNumber:{0},typeID:{1}:", phoneNumber, TypeID);
                    var listVerify = db.SP_Utility_PhoneSMS_Verification(phoneNumber, 0, TypeID).FirstOrDefault();
                    if (listVerify != null)
                    {
                        isVerify = listVerify.Value;
                    }
                    logger.InfoFormat("Verified the phone number with status as :", isVerify);
				}
				#endregion

				return isVerify;
			}
            catch (VtodException ex)
            {
                logger.Error("RegistrationPhoneVerificationStatus:", ex);
                throw (ex);
            }
			catch (Exception ex)
			{
                logger.Error("RegistrationPhoneVerificationStatus:", ex);
				throw (ex);
			}

		}
        #endregion

        #region "Phone SMS Updation"
        /// <summary>
		/// Updates the Verification table 
		/// a)When SMS is sent through Twilio
		/// b)When Twilio number receives a message with body : OK/Agree
		/// </summary>
		/// <param name="ID"></param>
		/// <param name="VerificationFlow"></param>
		/// <param name="IsVerified"></param>
		public void PhoneSMSUpdation(Int64 ID, int VerificationFlow, bool IsVerified,string errorStatus)
		{
			try			
            {
				#region Call Domain Method
               int  TypeID = UDI.VTOD.Utility.Common.Cost.VerificationID.Phone;
               logger.InfoFormat("Start-Updation in the Verification table: inputs ID:{0},VerificationFlow:{1},IsVerified:{2},typeID:{3}:,Error Status:{4}:", ID, VerificationFlow, IsVerified, TypeID, errorStatus);
				using (var db = new DataAccess.VTOD.VTODEntities())
				{
                    db.SP_Utility_PhoneSMS_Updation(ID, VerificationFlow, IsVerified, TypeID, errorStatus);
				}
				logger.InfoFormat("End-Updation in the Verification table");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("PhoneSMSUpdation:", ex);
				throw (ex);
			}

		}

		#endregion



        #region "Code Verification"

        public void CodeUpdation(string type, string phoneNumber, Int64 memberID,string verificationCode, TokenRS token)
        {
            int? ret = 0;
            try
            {
                #region Call Domain Method
                int TypeID = UDI.VTOD.Utility.Common.Cost.VerificationID.Phone;
                logger.InfoFormat("Start-Updation in the Verification table: inputs type:{0},phoneNumber:{1},memberID:{2},VerificationCode:{3}:",type, phoneNumber, memberID, verificationCode);
                using (var db = new DataAccess.VTOD.VTODEntities())
                {
                    ret=db.SP_Utility_PhoneSMS_Code_Verification(TypeID.ToString(), phoneNumber, memberID, UDI.VTOD.Utility.Common.Cost.VerificationFlow.Phone_SMS_Completed, true, verificationCode.ToInt32(), UDI.VTOD.Utility.Common.Cost.VerificationFlow.Phone_SMS_Sent).First();
                }
                if (ret == 1)
                {
                    logger.InfoFormat("Your Code is not valid");
                    throw VtodException.CreateException(VTOD.Common.DTO.Enum.ExceptionType.Utility, 9018);
                }
                else
                {
                    #region Update SDS
                    if (memberID > 0)
                    {
                        try
                        {
                            logger.InfoFormat("Start-SDS Verification");
                            var sdsMembershipController = new UDI.SDS.MembershipController(token, null);
                            phoneNumber = phoneNumber.Substring(1, phoneNumber.Length - 1);
                            bool sdsResult = sdsMembershipController.SetPhoneNumberVerified(true, memberID.ToInt32(), phoneNumber);
                            logger.InfoFormat("End-SDS Verification");
                        }
                        catch (System.ServiceModel.EndpointNotFoundException ex)
                        {
                            try
                            {
                                if ((ex.InnerException as System.Net.WebException).Response.Headers.GetKey(0) == "SuperShuttle-StatusMessage")
                                {
                                    logger.Error((ex.InnerException as System.Net.WebException).Response.Headers.GetValues(0));
                                }
                            }
                            catch (Exception exendpoint)
                            {
                                logger.Error("System.ServiceModel.EndpointNotFoundException");
                                logger.Error(exendpoint.Message);
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.Error(ex.Message);
                        }
                    }
                    #endregion
                }

                logger.InfoFormat("End-Updation in the Verification table");
                #endregion
            }
            catch (Exception ex)
            {
                logger.Error("CodeUpdation:", ex);
                throw (ex);
            }

        }

        #endregion

        #region Verify Phone Number Pattern
        public void VerifyPhoneNumberPattern(string phoneNumber)
        {
            try
            {
            string pattern1 = "^\\d{11}$";
            logger.InfoFormat("Phone To be Verified Number {0}", phoneNumber);
            bool match = Regex.IsMatch(phoneNumber, pattern1);
            logger.InfoFormat("Match with the regex:Status{0}:", match);
            if(!match)
            {
                logger.InfoFormat("Phone Number is not valid");
                logger.InfoFormat("9005");
                throw VtodException.CreateException(VTOD.Common.DTO.Enum.ExceptionType.Utility, 9005);
            }
            else
            {
                logger.InfoFormat("Phone Number is  valid");
                logger.InfoFormat("Start-Verifying Area and Country Code");
                VerifyAreaCountryCode(phoneNumber);
                logger.InfoFormat("End-Verifying Area and Country Code");
            }

            }
            catch (VtodException ex)
            {
                logger.Error("PhoneSMSInsertion:", ex);
                logger.Error(ex.ExceptionMessage.Code);
                throw (ex);

            }
            catch (Exception ex)
            {
                logger.Error("PhoneSMSInsertion:", ex);
                throw (ex);

            }
                
        }
       
        public void VerifyAreaCountryCode(string phoneNumber)
        {
            try
            {
            int? errorStatus = 0;
            using (var db = new DataAccess.VTOD.VTODEntities())
            {
               errorStatus = db.SP_ValidateAreaCountryCode(phoneNumber).First();
               logger.InfoFormat("Error Status Received:{0}:", errorStatus);
                if(errorStatus==1)
                {
                    logger.InfoFormat("Country Code does not match");
                    throw VtodException.CreateException(VTOD.Common.DTO.Enum.ExceptionType.Utility, 9007);
                }
                else if (errorStatus == 2)
                {
                    logger.InfoFormat("Area Code does not match");
                    throw VtodException.CreateException(VTOD.Common.DTO.Enum.ExceptionType.Utility, 9006);
                }
                else
                {
                    logger.InfoFormat("Country Code/Area Code matches");
                }
             }
            }
            catch (VtodException ex)
            {
                logger.Error("VerifyAreaCountryCode:", ex);
                throw (ex);
            }
			catch (Exception ex)
			{
                logger.Error("VerifyAreaCountryCode:", ex);
				throw (ex);

			}
        }
        #endregion

		#region "Retrieve List PhoneSMS"
		/// <summary>
		/// Retrieves the Information from the Verification table
		/// </summary>
		/// <param name="status"></param>
		/// <param name="prevstatus"></param>
		/// <param name="duplicatestatus"></param>
		/// <returns></returns>
		public List<PhoneInfo> ListPhoneSMSInserted(int status, int prevstatus, int duplicatestatus,int exceptionstatus)
		{

			try
			{
				#region Call Domain Method
                int TypeID = UDI.VTOD.Utility.Common.Cost.VerificationTypeID.Phone_SMS;
                int ModeID = UDI.VTOD.Utility.Common.Cost.VerificationID.Phone;
				using (var db = new DataAccess.VTOD.VTODEntities())
				{
					List<PhoneInfo> result = new List<PhoneInfo>();
					//result = null;
					logger.InfoFormat("Start - Retrieving the Phone numbers from the Verification table with the Status:{0}:", status);
                    var listPhoneNumbers = db.SP_Utility_List_PhoneSMSInserted(status, prevstatus, duplicatestatus, ModeID, TypeID, exceptionstatus).ToList();
					if (listPhoneNumbers != null && listPhoneNumbers.Any())
					{
						foreach (var phoneItem in listPhoneNumbers)
						{
							var phoneInfo = phoneItem;

                            result.Add(new PhoneInfo() {  ID = phoneInfo.ID, PhoneNumber = phoneInfo.Item, MemberID = phoneInfo.MemberID,
                                                          VerificationFlow = phoneInfo.VerificationFlow, IsVerified = phoneInfo.IsVerified,
                                                          AppendTime = phoneInfo.AppendTime, InsertedTimeUTC = (DateTime)phoneInfo.InsertedTimeUTC,
                                                          VerificationType = phoneInfo.VerificationType.ToInt32(), VerificationCode=phoneInfo.VerificationCode.ToInt32()});
						}
					}
					logger.InfoFormat("End - Retrieving the Phone numbers from the Verification table.Count{0}:", listPhoneNumbers.Count());
					return result;
				}
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("ListPhoneSMSInserted:", ex);
				throw (ex);
			}
		}

		#endregion

		#region "OutBound Process"
		/// <summary>
		/// This is a windows service where Twilio sends SMS to the numbers(based on the records of the Verification table) to Verify
		/// </summary>
		/// <param name="PhoneList"></param>
		public void OutBoundProcess(TokenPhoneCollection PhoneList)
		{
			string twillioStatus = null;
			bool smsSendStatus = false;
			try
			{
				#region Call Domain Method
				using (var db = new DataAccess.VTOD.VTODEntities())
				{
					SMSDomin smsSendObj = new SMSDomin();

					logger.InfoFormat("Start - Outbound Process of Verification Domain");
					#region Loop through the Parameter "PhoneList"
					if (PhoneList != null && PhoneList.PhoneCollection.Any())
					{

						foreach (var item in PhoneList.PhoneCollection)
						{
							#region Looping through each phone number
							logger.InfoFormat("Phone Number {0}:", item.PhoneNumber);
							MessageDetails messageObj = new MessageDetails();
							messageObj.To.Add(item.PhoneNumber);
							#region Twilio
							logger.InfoFormat("Twilio Sending the SMS");
                            if (item.VerificationType == Common.Const.VerificationType.VerificationByText)
                            {
                                messageObj.MessageBody = System.Configuration.ConfigurationManager.AppSettings["MessageBody"];
                            }
                            else if (item.VerificationType == Common.Const.VerificationType.VerificationByCode)
                            {
                                messageObj.MessageBody = System.Configuration.ConfigurationManager.AppSettings["MessageBodyByCode"];
                                messageObj.MessageBody=messageObj.MessageBody.Replace("[CODE]", item.VerificationCode.ToString());
                            }
                            else
                            {
                                messageObj.MessageBody = System.Configuration.ConfigurationManager.AppSettings["MessageBody"];
                            }
							smsSendStatus = smsSendObj.Send(messageObj, ref twillioStatus);
							logger.InfoFormat("Twilio Sent the SMS status:{0}:", smsSendStatus);
							#endregion
							if (smsSendStatus == false)
							{
								#region Update DB ERROR
								logger.InfoFormat("Updation of the Verification table with Error Code");
                                PhoneSMSUpdation(item.ID, Common.Cost.VerificationFlow.Phone_SMS_Error, false, twillioStatus);
								#endregion

							}
							else
							{
								#region update DB SMS is sent
								logger.InfoFormat("Updation of the Verification table with Code : 3 ");
                                PhoneSMSUpdation(item.ID, Common.Cost.VerificationFlow.Phone_SMS_Sent, false,"");
								#endregion
							}
							#endregion
						}
					}
                   
					#endregion
					logger.InfoFormat("End - Outbound Process of Verification Domain");
				}
				#endregion

			}

			catch (Exception ex)
			{
				logger.Error("OutBoundProcess:", ex);
				throw (ex);
			}

		}

		#endregion

		#region "InBound Process"
		/// <summary>
		/// This is a windows service where the Twilio(based on the records of the Verification table) checks the body of the message received and updates the table accordingly.
		/// </summary>
		/// <param name="PhoneList"></param>
		public void InBoundProcess(UDI.VTOD.Utility.Common.DTO.TokenPhoneCollection PhoneList)
		{

			#region Init			
            string twillioStatus = null;
			bool smsSendStatus = false;
			string twllioResponse = System.Configuration.ConfigurationManager.AppSettings["TwilioResponse"];
			List<string> receivedTwilioItem = new List<string>();
			string verificationSuccess = System.Configuration.ConfigurationManager.AppSettings["TwilioSuccess"]; ;
			string verificationFailure = System.Configuration.ConfigurationManager.AppSettings["TwilioFailure"];
			#endregion
			try
			{
				#region Database Call
				using (var db = new DataAccess.VTOD.VTODEntities())
				{
					SMSDomin smsSendObj = new SMSDomin();
					logger.InfoFormat("Start - InBound Process of Verification Domain");
					#region Loop through the phonelist parameter

					if (PhoneList != null && PhoneList.PhoneCollection.Any())
					{

						foreach (var item in PhoneList.PhoneCollection)
						{

							logger.InfoFormat("Phone Number {0}:", item);

							#region Twilio
							UDI.VTOD.Utility.Common.DTO.MessageDetails messageObj = new UDI.VTOD.Utility.Common.DTO.MessageDetails();
							messageObj.From = item.PhoneNumber.ToString();
                            messageObj.MessageSentDate = item.InsertedTimeUTC;
							logger.InfoFormat("Start - Twilio Receiving the SMS");
							receivedTwilioItem = smsSendObj.Receive(messageObj, ref twillioStatus);
							logger.InfoFormat("End - Twilio Receiving the SMS");
							#endregion

							#region Parsing the SMS List
							if (twillioStatus != null || receivedTwilioItem.Any())
							{
                              
								foreach (string responseItem in receivedTwilioItem)
								{
									#region Verification Success
                                    logger.InfoFormat("Twilio SMS Received :PhoneNumber{0}:,Message:{1}", item.PhoneNumber, responseItem);
									if (Array.IndexOf(twllioResponse.Split(','), responseItem) >= 0)
									{
										messageObj.To.Add(item.PhoneNumber);
										messageObj.MessageBody = verificationSuccess;
										item.IsVerified = true;
										logger.InfoFormat("Verification Success :PhoneNumber{0}:", item.PhoneNumber);
									}
									#endregion
								}

								#region Verification Failure
								if (item.IsVerified == false)
								{
									messageObj.To.Add(item.PhoneNumber);
									messageObj.MessageBody = verificationFailure;
									item.IsVerified = false;
									logger.InfoFormat("Verification Failure :PhoneNumber{0}:", item.PhoneNumber);

								}
								#endregion
							}
							else
							{
								#region Log
                                logger.InfoFormat("Received No SMS from Twilio :PhoneNumber{0}", item.PhoneNumber);
								#endregion
							}
							#endregion

							#region Update DB
							if (receivedTwilioItem == null || receivedTwilioItem.Count() == 0)
							{
                                logger.InfoFormat("Error in Twilio while receiving SMS:PhoneNumber{0},Error{1}:",item.PhoneNumber, twillioStatus);
                                PhoneSMSUpdation(item.ID, item.VerificationFlow, item.IsVerified, twillioStatus);
							}
							else
							{
								logger.InfoFormat("Updation of the Verification table with Code : 10 ");
                                PhoneSMSUpdation(item.ID, UDI.VTOD.Utility.Common.Cost.VerificationFlow.Phone_SMS_Completed, item.IsVerified,"");


							}
							#endregion

							#region Update SDS
							if (item.MemberID != null && item.MemberID > 0)
							{
								try
								{
                                    if (receivedTwilioItem != null)
                                    {
                                        if (receivedTwilioItem.Any())
                                        {
                                            logger.InfoFormat("Start-SDS Verification");
                                            var sdsMembershipController = new UDI.SDS.MembershipController(PhoneList.Token, null);
                                            string phoneNumber = item.PhoneNumber.Substring(1, item.PhoneNumber.Length - 1);
                                            bool sdsResult = sdsMembershipController.SetPhoneNumberVerified(item.IsVerified, item.MemberID.ToInt32(), phoneNumber);
                                            logger.InfoFormat("End-SDS Verification");
                                        }
                                    }
								}
                                catch (System.ServiceModel.EndpointNotFoundException ex)
								{
                                    try
                                    {
                                        if ((ex.InnerException as System.Net.WebException).Response.Headers.GetKey(0) == "SuperShuttle-StatusMessage")
                                        {
                                            logger.Error((ex.InnerException as System.Net.WebException).Response.Headers.GetValues(0));
                                        }
                                    }
                                    catch (Exception exendpoint)
                                    {
                                        logger.Error("System.ServiceModel.EndpointNotFoundException");
                                        logger.Error(exendpoint.Message);
                                    }
                                }
								catch (Exception ex)
								{
                                  logger.Error(ex.Message);
                              }
							}

							#endregion

							#region Send Acknowledge
                            if (receivedTwilioItem != null && receivedTwilioItem.Count() > 0)
							{
								logger.InfoFormat("Twilio Sending the Acknowledgement");
								smsSendStatus = smsSendObj.Send(messageObj, ref twillioStatus);
								logger.InfoFormat("Twilio Sent the Acknowledgement: Status {0}:", smsSendStatus);
							}
							#endregion
						}
					}
					#endregion
					logger.InfoFormat("End - Inbound Process of Verification Domain");
				}
				#endregion

			}

			catch (Exception ex)
			{
				logger.Error("InBoundProcess:", ex);
				throw (ex);
			}

		}

		#endregion

        #region Verify App Version
        public IsVersionActiveRS IsVerifyAppVersion(string App,string Platform,string AppVersion,string PlatformVersion)
        {
            var result = new IsVersionActiveRS();
            var Reasons = new List<UDI.VTOD.Utility.Model.Error>();
             try
            {
                #region Parameter Declaration

                bool? isVerify = false;
                #endregion

                #region Call Domain Method
                using (var db = new DataAccess.VTOD.VTODEntities())
                {
                    logger.InfoFormat("Retrieve the App Version Verification  from AppVersionVerification  table: inputs App:{0},Platform:{1},AppVersion:{2}:,PlatformVersion:{3}:", App, Platform, AppVersion,PlatformVersion);
                    var listVerify = db.SP_Check_AppVersion(App, AppVersion, Platform, PlatformVersion).ToList();
                    if (listVerify != null && listVerify.Any())
                    {
                        var list = listVerify.FirstOrDefault();
                        result.IsActive = list.IsActive;
                        if(isVerify==false)
                        {
                            result.IsAppVersionSupported = list.AppVersionSupported ?? false;
                            result.IsPlatformVersionSupported = list.PlatformVersionSupported ?? false;
                        }
                    }
                    else
                    {
                         result.IsActive =  true;
                         result.IsAppVersionSupported = true;
                         result.IsPlatformVersionSupported = true;
                    }
                    logger.InfoFormat("App Version is verified :", isVerify);

                }
                #endregion

                return result;
            }
            catch (VtodException ex)
            {
                logger.Error("IsVerifyAppVersion:", ex);
                throw (ex);
            }
            catch (Exception ex)
            {
                logger.Error("IsVerifyAppVersion:", ex);
                throw (ex);
            }

        } 
        #endregion

		#endregion
	}

	

	#endregion
}
