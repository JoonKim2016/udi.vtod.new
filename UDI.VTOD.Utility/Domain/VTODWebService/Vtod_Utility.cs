﻿
using UDI.Utility.Serialization;
using UDI.Utility.Helper;
using UDI.Utility.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using Twilio;
using log4net;
using UDI.VTOD.Utility.Common.DTO;
using UDI.VTOD.Common.DTO.OTA;
using System.Xml.Linq;
namespace UDI.VTOD.Utility.Domain.VTODWebService
{
  
    public class VTOD_Utility
    {
        #region Fields
        public ILog logger;
        string vtodApiVersion = System.Configuration.ConfigurationManager.AppSettings["VtodApiVersion"];
        string vtodApiUrl = System.Configuration.ConfigurationManager.AppSettings["VtodApiUrl"];
		string contectType = "application/json; charset=utf-8";
        string method = "POST";
        #endregion

        #region Constructor
        public VTOD_Utility()
        {
            logger = LogManager.GetLogger(this.GetType());
            log4net.Config.XmlConfigurator.Configure();
        }
    
        #endregion
        public OTA_GroundResRetrieveRS Status(Dictionary<string, string> token, long TripID,string rateQualifierValue)
        {
            OTA_GroundResRetrieveRS result = null;
            try
            {
                var status = string.Empty;

                #region Make JSON Request
                var groundStatusRQ = new OTA_GroundResRetrieveRQ();
                groundStatusRQ.Version = vtodApiVersion;
                groundStatusRQ.Reference = new List<Reference>();
                string ID = TripID.ToString();
                groundStatusRQ.Reference.Add(new Reference { Type = VTOD.Common.DTO.Enum.ConfirmationType.Confirmation.ToString(), ID = ID });
                groundStatusRQ.TPA_Extensions = new TPA_Extensions();
                groundStatusRQ.TPA_Extensions.RateQualifiers = new List<RateQualifier>();
                groundStatusRQ.TPA_Extensions.RateQualifiers.Add(new RateQualifier { RateQualifierValue = rateQualifierValue });
                groundStatusRQ.TPA_Extensions.LuggageQty = 0;

                var jsonGroundStatusRQ = groundStatusRQ.JsonSerialize();
                #endregion

				#region Header
				Dictionary<string, string> headers = new Dictionary<string, string>();
				headers.Add("UserName", token.FirstOrDefault().Key);
				headers.Add("SecurityKey", token.FirstOrDefault().Value);
				#endregion

                #region Log
                logger.Info(string.Format("Status RQ: {0}", jsonGroundStatusRQ.ToString()));
                #endregion
                logger.Info(string.Format("VTOD API URL : {0}", vtodApiUrl));
                var vtodResult = UDI.Utility.Helper.WebRequestHelper.ProcessWebRequest(
                    vtodApiUrl + "Ground/ResRetrieve",
                    jsonGroundStatusRQ.ToString(),
                    contectType,
                    method,
					headers,
                    out status
                );

                #region Log
                
                logger.Info(string.Format("Status RS: {0}", vtodResult));
                #endregion

				//result = XDocument.Parse(vtodResult).XmlDeserialize<UDI.VTOD.Common.DTO.OTA.OTA_GroundResRetrieveRS>();
				result = vtodResult.JsonDeserialize<UDI.VTOD.Common.DTO.OTA.OTA_GroundResRetrieveRS>();

                logger.InfoFormat("Response Of Status Request :{0}", result);
                return result;
            }
            catch(Exception ex)
            {
                logger.ErrorFormat("Error:{0}:",ex);
                return result;
            }
        }

        public string getTableName(Int64 tripID)
        {
          string tableName = "";
          try
          {
             using (var db = new DataAccess.VTOD.VTODEntities())
             {
                 var trip=db.vtod_trip.Where(p => p.Id == tripID);
                 if(trip!=null)
                 {
                     if (trip.FirstOrDefault().FleetTripCode == UDI.VTOD.Common.DTO.Enum.FleetTripCode.Taxi_CSCI || trip.FirstOrDefault().FleetTripCode == UDI.VTOD.Common.DTO.Enum.FleetTripCode.Taxi_MTDATA || trip.FirstOrDefault().FleetTripCode == UDI.VTOD.Common.DTO.Enum.FleetTripCode.Taxi_SDS || trip.FirstOrDefault().FleetTripCode == UDI.VTOD.Common.DTO.Enum.FleetTripCode.Taxi_UDI33)
                    {
                        tableName = "taxi_trip";
                    }
                    else if (trip.FirstOrDefault().FleetTripCode == UDI.VTOD.Common.DTO.Enum.FleetTripCode.SDS_Shuttle_Airport || trip.FirstOrDefault().FleetTripCode == UDI.VTOD.Common.DTO.Enum.FleetTripCode.SDS_Shuttle_Charter || trip.FirstOrDefault().FleetTripCode == UDI.VTOD.Common.DTO.Enum.FleetTripCode.SDS_Shuttle_Hourly)
                    {
                         tableName = "sds_trip";
                    }
                    else if (trip.FirstOrDefault().FleetTripCode == UDI.VTOD.Common.DTO.Enum.FleetTripCode.Ecar_Aleph || trip.FirstOrDefault().FleetTripCode == UDI.VTOD.Common.DTO.Enum.FleetTripCode.Ecar_GreenTomato)
                    {
                         tableName = "ecar_trip";
                    }
                 }
             }
           }
            catch(Exception ex)
            {
                logger.ErrorFormat("Error:{0}:", ex);
            }
          return tableName;
        }

        
       
    }
}