﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using UDI.VTOD.Common.DTO;
using UDI.VTOD.Common.DTO.OTA;

namespace UDI.VTOD.WebService.Contract
{
	[ServiceContract]
	public interface IREST
	{
		#region Examples
		#region Ping
		#region Json
		[OperationContract]
		[WebInvoke(Method = "GET",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Ping")]
		Ping JsonPing();
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "GET",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Ping?media=xml")]
		[XmlSerializerFormat]
		Ping XmlPing();
		#endregion
		#endregion

		#region Fault Sample
		#region Json
		[OperationContract]
		[WebInvoke(Method = "GET",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Error")]
		void JsonError();
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "GET",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Error?media=xml")]
		[XmlSerializerFormat()]//SupportFaults = true)]
		void XmlError();
		#endregion
		#endregion
		#endregion

		#region GetToken
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "GetToken")]
		TokenRS JsonGetToken(TokenRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "GetToken?media=xml")]
		[XmlSerializerFormat]
		TokenRS XmlGetToken(TokenRQ input);
		#endregion
		#endregion

		#region Ground
		#region GroundAvail
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Ground/Avail")]
		OTA_GroundAvailRS JsonGroundAvail(OTA_GroundAvailRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Ground/Avail?media=xml")]
		[XmlSerializerFormat]
		OTA_GroundAvailRS XmlGroundAvail(OTA_GroundAvailRQ input);
		#endregion
		#endregion

		#region GroundVehicleInfo
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Ground/VehicleInfo")]
		OTA_GroundAvailRS JsonGroundVehicleInfo(OTA_GroundAvailRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Ground/VehicleInfo?media=xml")]
		[XmlSerializerFormat]
		OTA_GroundAvailRS XmlGroundVehicleInfo(OTA_GroundAvailRQ input);
		#endregion
		#endregion

		#region GroundBook
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Ground/Book")]
		OTA_GroundBookRS JsonGroundBook(OTA_GroundBookRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Ground/Book?media=xml")]
		[XmlSerializerFormat]
		OTA_GroundBookRS XmlGroundBook(OTA_GroundBookRQ input);
		#endregion
		#endregion

		#region GroundCancel
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Ground/Cancel")]
		OTA_GroundCancelRS JsonGroundCancel(OTA_GroundCancelRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Ground/Cancel?media=xml")]
		[XmlSerializerFormat]
		OTA_GroundCancelRS XmlGroundCancel(OTA_GroundCancelRQ input);
		#endregion
		#endregion

		#region GroundResRetrieve
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Ground/ResRetrieve")]
		OTA_GroundResRetrieveRS JsonGroundResRetrieve(OTA_GroundResRetrieveRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Ground/ResRetrieve?media=xml")]
		[XmlSerializerFormat]
		OTA_GroundResRetrieveRS XmlGroundResRetrieve(OTA_GroundResRetrieveRQ input);
		#endregion
		#endregion

		#region GroundOriginalResRetrieve
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Ground/OriginalResRetrieve")]
		OTA_GroundResRetrieveRS JsonGroundOriginalResRetrieve(OTA_GroundResRetrieveRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Ground/OriginalResRetrieve?media=xml")]
		[XmlSerializerFormat]
		OTA_GroundResRetrieveRS XmlGroundOriginalResRetrieve(OTA_GroundResRetrieveRQ input);
		#endregion
		#endregion
		
		#region GroundCancelFee
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Ground/CancelFee")]
		OTA_GroundResRetrieveRS JsonGroundCancelFee(OTA_GroundResRetrieveRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Ground/CancelFee?media=xml")]
		[XmlSerializerFormat]
		OTA_GroundResRetrieveRS XmlGroundCancelFee(OTA_GroundResRetrieveRQ input);
		#endregion
		#endregion

		#region GroundGetPickupTime
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Ground/GetPickupTime")]
		GroundGetPickupTimeRS JsonGroundGetPickupTime(GroundGetPickupTimeRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Ground/GetPickupTime?media=xml")]
		[XmlSerializerFormat]
		GroundGetPickupTimeRS XmlGroundGetPickupTime(GroundGetPickupTimeRQ input);
		#endregion
		#endregion

		#region GroundGetFareScheduleWithFees
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Ground/GetFareScheduleWithFees")]
		GroundGetFareScheduleWithFeesRS JsonGroundGetFareScheduleWithFees(GroundGetFareScheduleWithFeesRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Ground/GetFareScheduleWithFees?media=xml")]
		[XmlSerializerFormat]
		GroundGetFareScheduleWithFeesRS XmlGroundGetFareScheduleWithFees(GroundGetFareScheduleWithFeesRQ input);
		#endregion
		#endregion


        #region GroundModifyBook
        #region Json
        [OperationContract]
        [WebInvoke(Method = "PUT",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Ground/Book")]
        OTA_GroundBookRS JsonGroundModifyBook(OTA_GroundBookRQ input);
        #endregion

        #region Xml
        [OperationContract]
        [WebInvoke(Method = "PUT",
            ResponseFormat = WebMessageFormat.Xml,
            RequestFormat = WebMessageFormat.Xml,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Ground/Book?media=xml")]
        [XmlSerializerFormat]
        OTA_GroundBookRS XmlGroundModifyBook(OTA_GroundBookRQ input);
        #endregion
        #endregion

        #region GetFinalRoute
        #region Json
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Ground/GetFinalRoute")]
        OTA_GroundGetFinalRouteRS JsonGroundGetFinalRoute(OTA_GroundGetFinalRouteRQ input);
        #endregion

        #region Xml
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Xml,
            RequestFormat = WebMessageFormat.Xml,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Ground/GetFinalRoute?media=xml")]
        [XmlSerializerFormat]
        OTA_GroundGetFinalRouteRS XmlGroundGetFinalRoute(OTA_GroundGetFinalRouteRQ input);
        #endregion
        #endregion

        #endregion

        #region Split Payment

        #region SplitPaymentInvite
        #region Json
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "SplitPayment/Invite")]
        SplitPaymentInviteRS JsonSplitPaymentInvite(SplitPaymentInviteRQ input);
        #endregion

        #region Xml
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Xml,
            RequestFormat = WebMessageFormat.Xml,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "SplitPayment/Invite?media=xml")]
        [XmlSerializerFormat]
        SplitPaymentInviteRS XmlSplitPaymentInvite(SplitPaymentInviteRQ input);
        #endregion
        #endregion

        #region SplitPaymentUpdateStatus
        #region Json
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "SplitPayment/UpdateStatus")]
        SplitPaymentUpdateStatusRS JsonSplitPaymentUpdateStatus(SplitPaymentUpdateStatusRQ input);
        #endregion

        #region Xml
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Xml,
            RequestFormat = WebMessageFormat.Xml,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "SplitPayment/UpdateStatus?media=xml")]
        [XmlSerializerFormat]
        SplitPaymentUpdateStatusRS XmlSplitPaymentUpdateStatus(SplitPaymentUpdateStatusRQ input);
        #endregion

        #endregion

        #region SplitPaymentGetInvitations
        #region Json
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "SplitPayment/GetInvitations")]
        SplitPaymentGetInvitationsRS JsonSplitPaymentGetInvitations(SplitPaymentGetInvitationsRQ input);
        #endregion

        #region Xml
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Xml,
            RequestFormat = WebMessageFormat.Xml,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "SplitPayment/GetInvitations?media=xml")]
        [XmlSerializerFormat]
        SplitPaymentGetInvitationsRS XmlSplitPaymentGetInvitations(SplitPaymentGetInvitationsRQ input);
        #endregion

        #endregion

        #region SplitPaymentCheckIfEabled
        #region Json
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "SplitPayment/CheckIfEnabled")]
        SplitPaymentCheckIfEnabledRS JsonSplitPaymentCheckIfEnabled(SplitPaymentCheckIfEnabledRQ input);
        #endregion

        #region Xml
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Xml,
            RequestFormat = WebMessageFormat.Xml,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "SplitPayment/CheckIfEnabled?media=xml")]
        [XmlSerializerFormat]
        SplitPaymentCheckIfEnabledRS XmlSplitPaymentCheckIfEnabled(SplitPaymentCheckIfEnabledRQ input);
        #endregion
        #endregion

        #endregion

        #region Ground ASAP
        #region GroundCreateASAPRequest
        #region Json
        [OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Ground/CreateASAPRequest")]
		OTA_GroundAvailRS JsonGroundCreateAsapRequest(OTA_GroundAvailRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Ground/CreateASAPRequest?media=xml")]
		[XmlSerializerFormat]
		OTA_GroundAvailRS XmlGroundCreateAsapRequest(OTA_GroundAvailRQ input);
		#endregion
		#endregion

		#region GroundGetASAPRequestStatus
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Ground/GetASAPRequestStatus")]
		GroundGetASAPRequestStatusRS JsonGroundGetASAPRequestStatus(GroundGetASAPRequestStatusRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Ground/GetASAPRequestStatus?media=xml")]
		[XmlSerializerFormat]
		GroundGetASAPRequestStatusRS XmlGroundGetASAPRequestStatus(GroundGetASAPRequestStatusRQ input);
		#endregion
		#endregion

		#region GroundAbandonASAPRequest
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Ground/AbandonASAPRequest")]
		GroundAbandonASAPRequestRS JsonGroundAbandonASAPRequest(GroundAbandonASAPRequestRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Ground/AbandonASAPRequest?media=xml")]
		[XmlSerializerFormat]
		GroundAbandonASAPRequestRS XmlGroundAbandonASAPRequest(GroundAbandonASAPRequestRQ input);
		#endregion
		#endregion
		#endregion

		#region Membership

		#region MemberCreate
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Member/Create")]
		MemberCreateRS JsonMemberCreate(MemberCreateRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Member/Create?media=xml")]
		[XmlSerializerFormat]
		MemberCreateRS XmlMemberCreate(MemberCreateRQ input);
		#endregion
		#endregion

		#region MemberAddLocation
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Member/AddLocation")]
		MemberAddLocationRS JsonMemberAddLocation(MemberAddLocationRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Member/AddLocation?media=xml")]
		[XmlSerializerFormat]
		MemberAddLocationRS XmlMemberAddLocation(MemberAddLocationRQ input);
		#endregion
		#endregion

		#region MemberChangeEmailAddress
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Member/ChangeEmailAddress")]
		MemberChangeEmailAddressRS JsonMemberChangeEmailAddress(MemberChangeEmailAddressRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Member/ChangeEmailAddress?media=xml")]
		[XmlSerializerFormat]
		MemberChangeEmailAddressRS XmlMemberChangeEmailAddress(MemberChangeEmailAddressRQ input);
		#endregion
		#endregion

		#region MemberChangePassword
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Member/ChangePassword")]
		MemberChangePasswordRS JsonMemberChangePassword(MemberChangePasswordRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Member/ChangePassword?media=xml")]
		[XmlSerializerFormat]
		MemberChangePasswordRS XmlMemberChangePassword(MemberChangePasswordRQ input);
		#endregion
		#endregion

		#region MemberDeleteLocation
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Member/DeleteLocation")]
		MemberDeleteLocationRS JsonMemberDeleteLocation(MemberDeleteLocationRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Member/DeleteLocation?media=xml")]
		[XmlSerializerFormat]
		MemberDeleteLocationRS XmlMemberDeleteLocation(MemberDeleteLocationRQ input);
		#endregion
		#endregion

		#region MemberForgotPassword
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Member/ForgotPassword")]
		MemberForgotPasswordRS JsonMemberForgotPassword(MemberForgotPasswordRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Member/ForgotPassword?media=xml")]
		[XmlSerializerFormat]
		MemberForgotPasswordRS XmlMemberForgotPassword(MemberForgotPasswordRQ input);
		#endregion
		#endregion

		#region MemberGetPasswordRequest
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Member/GetPasswordRequest")]
		MemberGetPasswordRequestRS JsonMemberGetPasswordRequest(MemberGetPasswordRequestRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Member/GetPasswordRequest?media=xml")]
		[XmlSerializerFormat]
		MemberGetPasswordRequestRS XmlMemberGetPasswordRequest(MemberGetPasswordRequestRQ input);
		#endregion
		#endregion

		#region MemberResetPassword
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Member/ResetPassword")]
		MemberResetPasswordRS JsonMemberResetPassword(MemberResetPasswordRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Member/ResetPassword?media=xml")]
		[XmlSerializerFormat]
		MemberResetPasswordRS XmlMemberResetPassword(MemberResetPasswordRQ input);
		#endregion
		#endregion
		
		#region MemberGetLocations
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Member/GetLocations")]
		MemberGetLocationsRS JsonMemberGetLocations(MemberGetLocationsRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Member/GetLocations?media=xml")]
		[XmlSerializerFormat]
		MemberGetLocationsRS XmlMemberGetLocations(MemberGetLocationsRQ input);
		#endregion
		#endregion

		#region MemberGetRecentLocations
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Member/GetRecentLocations")]
		MemberGetLocationsRS JsonMemberGetRecentLocations(MemberGetLocationsRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Member/GetRecentLocations?media=xml")]
		[XmlSerializerFormat]
		MemberGetLocationsRS XmlMemberGetRecentLocations(MemberGetLocationsRQ input);
		#endregion
		#endregion
		
		#region MemberGetReservation
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Member/GetReservation")]
		MemberGetReservationRS JsonMemberGetReservation(MemberGetReservationRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Member/GetReservation?media=xml")]
		[XmlSerializerFormat]
		MemberGetReservationRS XmlMemberGetReservation(MemberGetReservationRQ input);
		#endregion
		#endregion

		#region MemberLinkReservation
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Member/LinkReservation")]
		MemberLinkReservationRS JsonMemberLinkReservation(MemberLinkReservationRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Member/LinkReservation?media=xml")]
		[XmlSerializerFormat]
		MemberLinkReservationRS XmlMemberLinkReservation(MemberLinkReservationRQ input);
		#endregion
		#endregion
		
		#region MemberLogin
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Member/Login")]
		MemberLoginRS JsonMemberLogin(MemberLoginRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Member/Login?media=xml")]
		[XmlSerializerFormat]
		MemberLoginRS XmlMemberLogin(MemberLoginRQ input);
        #endregion
        #endregion


        #region MemberGetReferralLink
        #region Json
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Member/GetReferralLink")]
        MemberGetReferralLinkRS JsonMemberGetReferralLink(MemberGetReferralLinkRQ input);
        #endregion

        #region Xml
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Xml,
            RequestFormat = WebMessageFormat.Xml,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Member/GetReferralLink?media=xml")]
        [XmlSerializerFormat]
        MemberGetReferralLinkRS XmlMemberGetReferralLink(MemberGetReferralLinkRQ input);
        #endregion
        #endregion

        #region MemberUpdateLocation
        #region Json
        [OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Member/UpdateLocation")]
		MemberUpdateLocationRS JsonMemberUpdateLocation(MemberUpdateLocationRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Member/UpdateLocation?media=xml")]
		[XmlSerializerFormat]
		MemberUpdateLocationRS XmlMemberUpdateLocation(MemberUpdateLocationRQ input);
		#endregion
		#endregion

		#region MemberUpdateProfile
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Member/UpdateProfile")]
		MemberUpdateProfileRS JsonMemberUpdateProfile(MemberUpdateProfileRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Member/UpdateProfile?media=xml")]
		[XmlSerializerFormat]
		MemberUpdateProfileRS XmlMemberUpdateProfile(MemberUpdateProfileRQ input);
		#endregion
		#endregion

		#region MemberRequestEmailConfirmation
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Member/RequestEmailConfirmation")]
		MemberRequestEmailConfirmationRS JsonMemberRequestEmailConfirmation(MemberRequestEmailConfirmationRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Member/RequestEmailConfirmation?media=xml")]
		[XmlSerializerFormat]
		MemberRequestEmailConfirmationRS XmlMemberRequestEmailConfirmation(MemberRequestEmailConfirmationRQ input);
		#endregion
		#endregion

		#region MemberGetLatestUnRatedTrip
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Member/GetLatestUnRatedTrip")]
		MemberGetLatestUnRatedTripRS JsonMemberGetLatestUnRatedTrip(MemberGetLatestUnRatedTripRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Member/GetLatestUnRatedTrip?media=xml")]
		[XmlSerializerFormat]
		MemberGetLatestUnRatedTripRS XmlMemberGetLatestUnRatedTrip(MemberGetLatestUnRatedTripRQ input);
		#endregion
		#endregion

        #region MemberGetLatestTrip
        #region Json
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Member/GetLatestTrip")]
        MemberGetLatestTripRS JsonMemberGetLatestTrip(MemberGetLatestTripRQ input);
        #endregion

        #region Xml
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Xml,
            RequestFormat = WebMessageFormat.Xml,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Member/GetLatestTrip?media=xml")]
        [XmlSerializerFormat]
        MemberGetLatestTripRS XmlMemberGetLatestTrip(MemberGetLatestTripRQ input);
        #endregion
        #endregion

		#region MemberGetCorporateProfile
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Member/GetCorporateProfile")]
		MemberGetCorporateProfileRS JsonMemberGetCorporateProfile(MemberGetCorporateProfileRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Member/GetCorporateProfile?media=xml")]
		[XmlSerializerFormat]
		MemberGetCorporateProfileRS XmlMemberGetCorporateProfile(MemberGetCorporateProfileRQ input);
		#endregion
		#endregion

        #region MemberGetCorporateProfile2
        #region Json
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Member/GetCorporateProfile2")]
        MemberGetCorporateProfile2RS JsonMemberGetCorporateProfile2(MemberGetCorporateProfile2RQ input);
        #endregion

        #region Xml
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Xml,
            RequestFormat = WebMessageFormat.Xml,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Member/GetCorporateProfile2?media=xml")]
        [XmlSerializerFormat]
        MemberGetCorporateProfile2RS XmlMemberGetCorporateProfile2(MemberGetCorporateProfile2RQ input);
        #endregion
        #endregion

		#region DeleteMember
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Member/Delete")]
		MemberDeleteRS JsonMemberDelete(MemberDeleteRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Member/Delete?media=xml")]
		[XmlSerializerFormat]
		MemberDeleteRS XmlMemberDelete(MemberDeleteRQ input);
		#endregion
		#endregion

        #region MemberRequestEmailCancellation
        #region Json
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Member/RequestEmailCancellation")]
        MemberRequestEmailCancellationRS JsonMemberRequestEmailCancellation(MemberRequestEmailCancellationRQ input);
        #endregion

        #region Xml
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Xml,
            RequestFormat = WebMessageFormat.Xml,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Member/RequestEmailCancellation?media=xml")]
        [XmlSerializerFormat]
        MemberRequestEmailCancellationRS XmlMemberRequestEmailCancellation(MemberRequestEmailCancellationRQ input);
        #endregion
        #endregion
		#endregion

		#region Accounting

		#region AccountingAddMemberPaymentCard
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Accounting/AddMemberPaymentCard")]
		AccountingAddMemberPaymentCardRS JsonAccountingAddPaymentCard(AccountingAddMemberPaymentCardRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Accounting/AddMemberPaymentCard?media=xml")]
		[XmlSerializerFormat]
		AccountingAddMemberPaymentCardRS XmlAccountingAddPaymentCard(AccountingAddMemberPaymentCardRQ input);
		#endregion
		#endregion

		#region AccountingGetMemberPaymentCards
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Accounting/GetMemberPaymentCards")]
		AccountingGetMemberPaymentCardsRS JsonAccountingGetMemberPaymentCards(AccountingGetMemberPaymentCardsRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Accounting/GetMemberPaymentCards?media=xml")]
		[XmlSerializerFormat]
		AccountingGetMemberPaymentCardsRS XmlAccountingGetMemberPaymentCards(AccountingGetMemberPaymentCardsRQ input);
		#endregion
		#endregion

		#region AccountingSetMemberDefaultCreditCard
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Accounting/SetMemberDefaultPaymentCard")]
		AccountingSetMemberDefaultPaymentCardRS JsonAccountingSetMemberDefaultPaymentCard(AccountingSetMemberDefaultPaymentCardRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Accounting/SetMemberDefaultPaymentCard?media=xml")]
		[XmlSerializerFormat]
		AccountingSetMemberDefaultPaymentCardRS XmlAccountingSetMemberDefaultPaymentCard(AccountingSetMemberDefaultPaymentCardRQ input);
		#endregion
		#endregion

		#region AccountingDeleteMemberPaymentCard
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Accounting/DeleteMemberPaymentCard")]
		AccountingDeleteMemberPaymentCardRS JsonAccountingDeleteMemberPaymentCard(AccountingDeleteMemberPaymentCardRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Accounting/DeleteMemberPaymentCard?media=xml")]
		[XmlSerializerFormat]
		AccountingDeleteMemberPaymentCardRS XmlAccountingDeleteMemberPaymentCard(AccountingDeleteMemberPaymentCardRQ input);
		#endregion
		#endregion

		#region AccountingAddMemberDirectBill
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Accounting/AddMemberDirectBill")]
		AccountingAddMemberDirectBillRS JsonAccountingAddMemberDirectBill(AccountingAddMemberDirectBillRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Accounting/AddMemberDirectBill?media=xml")]
		[XmlSerializerFormat]
		AccountingAddMemberDirectBillRS XmlAccountingAddMemberDirectBill(AccountingAddMemberDirectBillRQ input);
		#endregion
		#endregion

		#region AccountingGetMemberDirectBills
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Accounting/GetMemberDirectBills")]
		AccountingGetMemberDirectBillsRS JsonAccountingGetMemberDirectBills(AccountingGetMemberDirectBillsRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Accounting/GetMemberDirectBills?media=xml")]
		[XmlSerializerFormat]
		AccountingGetMemberDirectBillsRS XmlAccountingGetMemberDirectBills(AccountingGetMemberDirectBillsRQ input);
		#endregion
		#endregion

		#region AccountingSetMemberDefaultDirectBill
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Accounting/SetMemberDefaultDirectBill")]
		AccountingSetMemberDefaultDirectBillRS JsonAccountingSetMemberDefaultDirectBill(AccountingSetMemberDefaultDirectBillRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Accounting/SetMemberDefaultDirectBill?media=xml")]
		[XmlSerializerFormat]
		AccountingSetMemberDefaultDirectBillRS XmlAccountingSetMemberDefaultDirectBill(AccountingSetMemberDefaultDirectBillRQ input);
		#endregion
		#endregion

		#region AccountingDeleteMemberDirectBill
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Accounting/DeleteMemberDirectBill")]
		AccountingDeleteMemberDirectBillRS JsonAccountingDeleteMemberDirectBill(AccountingDeleteMemberDirectBillRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Accounting/DeleteMemberDirectBill?media=xml")]
		[XmlSerializerFormat]
		AccountingDeleteMemberDirectBillRS XmlAccountingDeleteMemberDirectBill(AccountingDeleteMemberDirectBillRQ input);
		#endregion
		#endregion

		#region GetFareDetail
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Accounting/GetFareDetail")]
		GetFareDetailRS JsonGetFareDetail(GetFareDetailRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Accounting/GetFareDetail?media=xml")]
		[XmlSerializerFormat]
		GetFareDetailRS XmlGetFareDetail(GetFareDetailRQ input);
		#endregion
		#endregion

        #region AccountingEditMemberPaymentCard
        #region Json
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Accounting/EditMemberPaymentCard")]
        AccountingEditMemberPaymentCardRS JsonAccountingEditPaymentCard(AccountingEditMemberPaymentCardRQ input);
        #endregion

        #region Xml
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Xml,
            RequestFormat = WebMessageFormat.Xml,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Accounting/EditMemberPaymentCard?media=xml")]
        [XmlSerializerFormat]
        AccountingEditMemberPaymentCardRS XmlAccountingEditPaymentCard(AccountingEditMemberPaymentCardRQ input);
        #endregion
        #endregion


        #endregion

        #region Utilities
        #region GetDestinations
        #region Json
        [OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Utility/GetDestinations")]
		UtilityGetDestinationsRS JsonUtilityGetDestinations(UtilityGetDestinationsRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Utility/GetDestinations?media=xml")]
		[XmlSerializerFormat]
		UtilityGetDestinationsRS XmlUtilityGetDestinations(UtilityGetDestinationsRQ input);
		#endregion
		#endregion

		#region GetServicedAirports
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Utility/GetServicedAirports")]
		UtilityGetServicedAirportsRS JsonUtilityGetServicedAirports(UtilityGetServicedAirportsRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Utility/GetServicedAirports?media=xml")]
		[XmlSerializerFormat]
		UtilityGetServicedAirportsRS XmlUtilityGetServicedAirports(UtilityGetServicedAirportsRQ input);
		#endregion
		#endregion

		#region UtilityGetDefaultFleetTypeForPoint
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Utility/GetDefaultFleetTypeForPoint")]
		UtilityGetDefaultFleetTypeForPointRS JsonGetDefaultFleetTypeForPoint(UtilityGetDefaultFleetTypeForPointRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Utility/GetDefaultFleetTypeForPoint?media=xml")]
		[XmlSerializerFormat]
		UtilityGetDefaultFleetTypeForPointRS XmlGetDefaultFleetTypeForPoint(UtilityGetDefaultFleetTypeForPointRQ input);
		#endregion
		#endregion

		#region UtilityGetLocationList
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Utility/GetLocationList")]
		UtilityGetLocationListRS JsonUtilityGetLocationList(UtilityGetLocationListRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Utility/GetLocationList?media=xml")]
		[XmlSerializerFormat]
		UtilityGetLocationListRS XmlUtilityGetLocationList(UtilityGetLocationListRQ input);
		#endregion
		#endregion

		#region UtilitySearchLocation
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Utility/SearchLocation")]
		UtilitySearchLocationRS JsonUtilitySearchLocation(UtilitySearchLocationRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Utility/SearchLocation?media=xml")]
		[XmlSerializerFormat]
		UtilitySearchLocationRS XmlUtilitySearchLocation(UtilitySearchLocationRQ input);
		#endregion
		#endregion

        #region UtilityGetAirportName
        #region Json
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Utility/GetAirportName")]
        UtilityGetAirportNameRS JsonUtilityGetAirportName(UtilityGetAirportNameRQ input);
        #endregion

        #region Xml
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Xml,
            RequestFormat = WebMessageFormat.Xml,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Utility/GetAirportName?media=xml")]
        [XmlSerializerFormat]
        UtilityGetAirportNameRS XmlUtilityGetAirportName(UtilityGetAirportNameRQ input);
        #endregion
        #endregion

		#region UtilityAvailableFleets
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Utility/AvailableFleets")]
		UtilityAvailableFleetsRS JsonUtilityAvailableFleets(UtilityAvailableFleetsRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Utility/AvailableFleets?media=xml")]
		[XmlSerializerFormat]
		UtilityAvailableFleetsRS XmlUtilityAvailableFleets(UtilityAvailableFleetsRQ input);
		#endregion
		#endregion



        #region UtilityGetLeadTime
        #region Json
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Utility/GetLeadTime")]
        UtilityGetLeadTimeRS JsonUtilityGetLeadTime(UtilityGetLeadTimeRQ input);
        #endregion

        #region Xml
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Xml,
            RequestFormat = WebMessageFormat.Xml,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Utility/GetLeadTime?media=xml")]
        [XmlSerializerFormat]
        UtilityGetLeadTimeRS XmlUtilityGetLeadTime(UtilityGetLeadTimeRQ input);
        #endregion
        #endregion

        #region UtilityGetClientToken
        #region Json
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Utility/GetClientToken")]
        UtilityGetClientTokenRS JsonUtilityGetClientToken(UtilityGetClientTokenRQ input);
        #endregion

        #region Xml
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Xml,
            RequestFormat = WebMessageFormat.Xml,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Utility/GetClientToken?media=xml")]
        [XmlSerializerFormat]
        UtilityGetClientTokenRS XmlUtilityGetClientToken(UtilityGetClientTokenRQ input);
        #endregion
        #endregion

        #region UtilityGetAirlines
        #region Json
        [OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Utility/GetAirlinesByAirport")]
		UtilityGetAirlinesByAirportRS JsonUtilityGetAirlinesByAirportRS(UtilityGetAirlinesByAirportRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Utility/GetAirlinesByAirport?media=xml")]
		[XmlSerializerFormat]
		UtilityGetAirlinesByAirportRS XmlUtilityGetAirlinesByAirportRS(UtilityGetAirlinesByAirportRQ input);
		#endregion
		#endregion

		#region UtilityGetTripList
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Utility/GetTripList")]
		UtilityGetTripListRS JsonUtilityGetTripList(UtilityGetTripListRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Utility/GetTripList?media=xml")]
		[XmlSerializerFormat]
		UtilityGetTripListRS XmlUtilityGetTripList(UtilityGetTripListRQ input);
		#endregion
		#endregion

		#region UtilityGetAirportInstructions
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Utility/GetAirportInstructions")]
		UtilityGetAirportInstructionsRS JsonUtilityGetAirportInstructions(UtilityGetAirportInstructionsRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Utility/GetAirportInstructions?media=xml")]
		[XmlSerializerFormat]
		UtilityGetAirportInstructionsRS XmlUtilityGetAirportInstructions(UtilityGetAirportInstructionsRQ input);
		#endregion
		#endregion

		#region UtilityGetHonorifics
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Utility/GetHonorifics")]
		UtilityGetHonorificsRS JsonUtilityGetHonorifics(UtilityGetHonorificsRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Utility/GetHonorifics?media=xml")]
		[XmlSerializerFormat]
		UtilityGetHonorificsRS XmlUtilityGetHonorifics(UtilityGetHonorificsRQ input);
		#endregion
		#endregion

		#region UtilityUtilityGetBookingCriteria
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Utility/GetBookingCriteria")]
		UtilityGetBookingCriteriaRS JsonUtilityGetBookingCriteria(UtilityGetBookingCriteriaRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Utility/GetBookingCriteria?media=xml")]
		[XmlSerializerFormat]
		UtilityGetBookingCriteriaRS XmlUtilityGetBookingCriteria(UtilityGetBookingCriteriaRQ input);
		#endregion
		#endregion

		#region UtilityGetCorporateRequirements
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Utility/GetCorporateRequirements")]
		UtilityGetCorporateRequirementsRS JsonUtilityGetCorporateRequirements(UtilityGetCorporateRequirementsRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Utility/GetCorporateRequirements?media=xml")]
		[XmlSerializerFormat]
		UtilityGetCorporateRequirementsRS XmlUtilityGetCorporateRequirements(UtilityGetCorporateRequirementsRQ input);
		#endregion
		#endregion

		#region UtilityTypeAhead
		#region Json
		[OperationContract]
		[WebInvoke(Method = "GET",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Utility/TypeAhead/{username}/{corporate}/{corporateCompanyName}/{accountName}/{requirementName}/{letters}")]
		List<string> JsonUtilityTypeAhead(string username, string corporate, string corporateCompanyName, string accountName, string requirementName, string letters);
		#endregion

		#region Xml
		//*************************
		//We do not support XML for this method
		//*************************
		//[OperationContract]
		//[WebInvoke(Method = "GET",
		//	ResponseFormat = WebMessageFormat.Xml,
		//	RequestFormat = WebMessageFormat.Xml,
		//	BodyStyle = WebMessageBodyStyle.Bare,
		//				UriTemplate = "Utility/TypeAhead/{corporate}/{corporateCompanyName}/{accountName}/{requirementName}/{letters}?media=xml")]
		//[XmlSerializerFormat]
		//List<string> XmlUtilityTypeAhead(string corporate, string corporateCompanyName, string accountName, string requirementName, string letters);
		#endregion
		#endregion

        #region UtilityGetCorporatePaymentMethod
        #region Json
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Utility/GetCorporatePaymentMethod")]
        UtilityGetCorporateRequirementsRS JsonUtilityGetCorporatePaymentMethod(UtilityGetCorporateRequirementsRQ input);
        #endregion

        #region Xml
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Xml,
            RequestFormat = WebMessageFormat.Xml,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Utility/GetCorporatePaymentMethod?media=xml")]
        [XmlSerializerFormat]
        UtilityGetCorporateRequirementsRS XmlUtilityGetCorporatePaymentMethod(UtilityGetCorporateRequirementsRQ input);
        #endregion
        #endregion

		#region UtilityGetAirportPickupOption
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Utility/GetAirportPickupOptions")]
		UtilityGetAirportPickupOptionsRS JsonUtilityGetAirportPickupOptions(UtilityGetAirportPickupOptionsRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Utility/GetAirportPickupOptions?media=xml")]
		[XmlSerializerFormat]
		UtilityGetAirportPickupOptionsRS XmlUtilityGetAirportPickupOptions(UtilityGetAirportPickupOptionsRQ input);
		#endregion
		#endregion

        #region UtilityGetCustomerInfo
        #region Json
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Utility/GetCustomerInfo")]
        UtilityCustomerInfoRS JsonUtilityGetCustomerInfo(UtilityCustomerInfoRQ input);
        #endregion

        #region Xml
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Xml,
            RequestFormat = WebMessageFormat.Xml,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Utility/GetCustomerInfo?media=xml")]
        [XmlSerializerFormat]
        UtilityCustomerInfoRS XmlUtilityGetCustomerInfo(UtilityCustomerInfoRQ input);
        #endregion
        #endregion

        #region UtilityGetLastTrip
        #region Json
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Utility/GetLastTrip")]
        UtilityGetLastTripRS JsonUtilityGetLastTrip(UtilityGetLastTripRQ input);
        #endregion

        #region Xml
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Xml,
            RequestFormat = WebMessageFormat.Xml,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Utility/GetLastTrip?media=xml")]
        [XmlSerializerFormat]
        UtilityGetLastTripRS XmlUtilityGetLastTrip(UtilityGetLastTripRQ input);
        #endregion
        #endregion

        #region UtilityGetPickUpAddress
        #region Json
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Utility/GetPickUpAddress")]
        UtilityGetPickupAddressRS JsonUtilityGetPickUpAddress(UtilityGetPickupAddressRQ input);
        #endregion

        #region Xml
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Xml,
            RequestFormat = WebMessageFormat.Xml,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Utility/GetPickUpAddress?media=xml")]
        [XmlSerializerFormat]
        UtilityGetPickupAddressRS XmlUtilityGetPickUpAddress(UtilityGetPickupAddressRQ input);
        #endregion
        #endregion

        #region UtilityGetBlackoutPeriod
        #region Json
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Utility/GetBlackoutPeriod")]
        UtilityGetBlackoutPeriodRS JsonUtilityGetBlackoutPeriod(UtilityGetBlackoutPeriodRQ input);
        #endregion

        #region Xml
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Xml,
            RequestFormat = WebMessageFormat.Xml,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Utility/GetBlackoutPeriod?media=xml")]
        [XmlSerializerFormat]
        UtilityGetBlackoutPeriodRS XmlUtilityGetBlackoutPeriod(UtilityGetBlackoutPeriodRQ input);
        #endregion
        #endregion

        #region UtilityGetStaleNotification
        #region Json
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Utility/GetStaleNotification")]
        UtilityGetNotificationRS JsonUtilityGetStaleNotification(UtilityGetNotificationRQ input);
        #endregion

        #region Xml
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Xml,
            RequestFormat = WebMessageFormat.Xml,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Utility/GetStaleNotification?media=xml")]
        [XmlSerializerFormat]
        UtilityGetNotificationRS XmlUtilityGetStaleNotification(UtilityGetNotificationRQ input);
        #endregion
        #endregion

        #region UtilityGetArrivalNotification
        #region Json
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Utility/GetArrivalNotification")]
        UtilityGetNotificationRS JsonUtilityGetArrivalNotification(UtilityGetNotificationRQ input);
        #endregion

        #region Xml
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Xml,
            RequestFormat = WebMessageFormat.Xml,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Utility/GetArrivalNotification?media=xml")]
        [XmlSerializerFormat]
        UtilityGetNotificationRS XmlUtilityGetArrivalNotification(UtilityGetNotificationRQ input);
        #endregion
        #endregion

        #region UtilityGetNightBeforeReminder
        #region Json
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Utility/GetNightBeforeReminder")]
        UtilityGetNotificationRS JsonUtilityGetNightBeforeReminder(UtilityGetNotificationRQ input);
        #endregion

        #region Xml
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Xml,
            RequestFormat = WebMessageFormat.Xml,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Utility/GetNightBeforeReminder?media=xml")]
        [XmlSerializerFormat]
        UtilityGetNotificationRS XmlUtilityGetNightBeforeReminder(UtilityGetNotificationRQ input);
        #endregion
        #endregion




		#endregion

		#region Reward
		#region RewardGetAirlinePrograms
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Reward/GetAirlinePrograms")]
		RewardGetAirlineProgramsRS JsonRewardGetAirlineRewardsPrograms(RewardGetAirlineProgramsRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Reward/GetAirlinePrograms?media=xml")]
		[XmlSerializerFormat]
		RewardGetAirlineProgramsRS XmlRewardGetAirlineRewardsPrograms(RewardGetAirlineProgramsRQ input);
		#endregion
		#endregion

		#region RewardCreateMemberAccount
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Reward/CreateMemberAccount")]
		RewardCreateMemberAccountRS JsonRewardCreateMemberAccount(RewardCreateMemberAccountRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Reward/CreateMemberAccount?media=xml")]
		[XmlSerializerFormat]
		RewardCreateMemberAccountRS XmlRewardCreateMemberAccount(RewardCreateMemberAccountRQ input);
		#endregion
		#endregion

		#region RewardDeleteMemberAccount
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Reward/DeleteMemberAccount")]
		RewardDeleteMemberAccountRS JsonRewardDeleteMemberAccount(RewardDeleteMemberAccountRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Reward/DeleteMemberAccount?media=xml")]
		[XmlSerializerFormat]
		RewardDeleteMemberAccountRS XmlRewardDeleteMemberAccount(RewardDeleteMemberAccountRQ input);
		#endregion
		#endregion

		#region RewardGetMemberAccounts
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Reward/GetMemberAccounts")]
		RewardGetMemberAccountsRS JsonRewardGetMemberAccounts(RewardGetMemberAccountsRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Reward/GetMemberAccounts?media=xml")]
		[XmlSerializerFormat]
		RewardGetMemberAccountsRS XmlRewardGetMemberAccounts(RewardGetMemberAccountsRQ input);
		#endregion
		#endregion

		#region RewardSetDefaultAccount
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Reward/SetDefaultAccount")]
		RewardSetDefaultAccountRS JsonRewardSetDefaultAccount(RewardSetDefaultAccountRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Reward/SetDefaultAccount?media=xml")]
		[XmlSerializerFormat]
		RewardSetDefaultAccountRS XmlRewardSetDefaultAccount(RewardSetDefaultAccountRQ input);
		#endregion
		#endregion


        #region RewardModifyMemberAccount
        #region Json
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Reward/RewardModifyMemberAccount")]
        RewardModifyMemberAccountRS JsonRewardModifyMemberAccount(RewardModifyMemberAccountRQ input);
        #endregion

        #region Xml
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Xml,
            RequestFormat = WebMessageFormat.Xml,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Reward/RewardModifyMemberAccount?media=xml")]
        [XmlSerializerFormat]
        RewardModifyMemberAccountRS XmlRewardModifyMemberAccount(RewardModifyMemberAccountRQ input);
        #endregion
        #endregion
		#endregion

		#region ZTripCredit
		#region ZTripCreditActivateMemberCredit
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "ZTripCredit/ActivateMemberCredit")]
		ZTripCreditActivateMemberCreditRS JsonZTripCreditActivateMemberCredit(ZTripCreditActivateMemberCreditRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "ZTripCredit/ActivateMemberCredit?media=xml")]
		[XmlSerializerFormat]
		ZTripCreditActivateMemberCreditRS XmlZTripCreditActivateMemberCredit(ZTripCreditActivateMemberCreditRQ input);
		#endregion
		#endregion

		#region ZTripCreditGetMemberBalance
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "ZTripCredit/GetMemberBalance")]
		ZTripCreditGetMemberBalanceRS JsonZTripCreditGetMemberBalance(ZTripCreditGetMemberBalanceRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "ZTripCredit/GetMemberBalance?media=xml")]
		[XmlSerializerFormat]
		ZTripCreditGetMemberBalanceRS XmlZTripCreditGetMemberBalance(ZTripCreditGetMemberBalanceRQ input);
		#endregion
		#endregion 
		#endregion

		#region Rate
		#region SetRate
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Rate/Set")]
		RateSetRS JsonRateSet(RateSetRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Rate/Set?media=xml")]
		[XmlSerializerFormat]
		RateSetRS XmlRateSet(RateSetRQ input);
		#endregion
		#endregion

		#region GetRate
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Rate/Get")]
		RateGetRS JsonRateGet(RateGetRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Rate/Get?media=xml")]
		[XmlSerializerFormat]
		RateGetRS XmlRateGet(RateGetRQ input);
		#endregion
		#endregion

		#endregion

		#region Group
		#region ValidateDiscount
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Group/ValidateDiscountCode")]
		GroupValidateDiscountCodeRS JsonGroupValidateDiscountCode(GroupValidateDiscountCodeRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Group/ValidateDiscountCode?media=xml")]
		[XmlSerializerFormat]
		GroupValidateDiscountCodeRS XmlGroupValidateDiscountCode(GroupValidateDiscountCodeRQ input);
		#endregion
		#endregion

		#region GetDiscount
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Group/GetDiscount")]
		GroupGetDiscountRS JsonGroupGetDiscount(GroupGetDiscountRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Group/GetDiscount?media=xml")]
		[XmlSerializerFormat]
		GroupGetDiscountRS XmlGroupGetDiscount(GroupGetDiscountRQ input);
		#endregion
		#endregion

		#region GroupGetPromotionType
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Group/GetPromotionType")]
		GroupGetPromotionTypeRS JsonGroupGetPromotionType(GroupGetPromotionTypeRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Group/GetPromotionType?media=xml")]
		[XmlSerializerFormat]
		GroupGetPromotionTypeRS XmlGroupGetPromotionType(GroupGetPromotionTypeRQ input);
		#endregion
		#endregion
		
		#region Group GetDefaults
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Group/GetDefaults")]
		GroupGetDefaultsRS JsonGroupGetDefaults(GroupGetDefaultsRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Group/GetDefaults?media=xml")]
		[XmlSerializerFormat]
		GroupGetDefaultsRS XmlGroupGetDefaults(GroupGetDefaultsRQ input);
		#endregion
		#endregion

		#region Group GetLandmarks
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Group/GetLandmarks")]
		GroupGetLandmarksRS JsonGroupGetLandmarks(GroupGetLandmarksRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Group/GetLandmarks?media=xml")]
		[XmlSerializerFormat]
		GroupGetLandmarksRS XmlGroupGetLandmarks(GroupGetLandmarksRQ input);
		#endregion
		#endregion

		#region Group GetLocalizedServicedAirports
		#region Json
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Json,
			RequestFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Group/GetLocalizedServicedAirports")]
		GroupGetLocalizedServicedAirportsRS JsonGroupGetLocalizedServicedAirports(GroupGetLocalizedServicedAirportsRQ input);
		#endregion

		#region Xml
		[OperationContract]
		[WebInvoke(Method = "POST",
			ResponseFormat = WebMessageFormat.Xml,
			RequestFormat = WebMessageFormat.Xml,
			BodyStyle = WebMessageBodyStyle.Bare,
			UriTemplate = "Group/GetLocalizedServicedAirports?media=xml")]
		[XmlSerializerFormat]
		GroupGetLocalizedServicedAirportsRS XmlGroupGetLocalizedServicedAirports(GroupGetLocalizedServicedAirportsRQ input);
		#endregion
		#endregion
		#endregion
	}
}
