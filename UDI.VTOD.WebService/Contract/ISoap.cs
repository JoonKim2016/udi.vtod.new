﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using UDI.VTOD.Common.DTO;

namespace UDI.VTOD.WebService.Contract
{
	[ServiceContract]
	public interface ISOAP
	{
		[OperationContract]
		Ping Ping();
	}
}
