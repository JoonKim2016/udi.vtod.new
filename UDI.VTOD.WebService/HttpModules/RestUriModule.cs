﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace UDI.VTOD.WebService.HttpModules
{
	public class RestUriModule : IHttpModule
	{
		#region IHttpModule Members
		public void Dispose()
		{ }

		public void Init(HttpApplication app)
		{

			app.BeginRequest += delegate
			{
				HttpContext ctx = HttpContext.Current;
				string path = ctx.Request.AppRelativeCurrentExecutionFilePath;

				int i = path.IndexOf('/', 2);

				var isProcess = false;

				#region isProcess
				var lastPart = path.Split('/').LastOrDefault();
				if (!string.IsNullOrWhiteSpace(lastPart))
				{
					if (!lastPart.Contains('.'))
					{
						//if (!lastPart.EndsWith("/"))
						//{
						isProcess = true;
						//}
					}
				}
				#endregion

				//if (i > 0 && !path.EndsWith("/"))
				if (isProcess)
				{
					string svc = path.Substring(0, i) + ".svc";
					string rest = path.Substring(i, path.Length - i);
					ctx.RewritePath(svc, rest, ctx.Request.QueryString.ToString(), false);
				}
			};

		}
		#endregion
	}
}
