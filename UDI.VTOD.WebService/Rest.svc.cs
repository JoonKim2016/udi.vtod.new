﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Xml.Linq;
using UDI.Utility.DTO;
using UDI.VTOD.Common.DTO;
using UDI.VTOD.Common.DTO.OTA;
using UDI.Utility.Serialization;
using System.IO;
using UDI.VTOD.Controller;
using System.ServiceModel.Channels;
using System.Diagnostics;
using UDI.VTOD.Common.Abstract;
using System.Xml.Serialization;

namespace UDI.VTOD.WebService
{
	public class REST : BaseSetting, Contract.IREST
	{
		#region Examples
		#region Ping
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/ping
		/// </summary>
		/// <returns></returns>
		public Ping JsonPing()
		{
			#region Log
			var stopwatch = new Stopwatch();
			stopwatch.Start();
			logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
			#endregion

			var result = UDI.VTOD.Controller.QueryController.Ping();

			#region Log
			stopwatch.Stop();
			logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
			#endregion

			return result;
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/ping?media=xml
		/// </summary>
		/// <returns></returns>
		public Ping XmlPing()
		{
			#region Log
			var stopwatch = new Stopwatch();
			stopwatch.Start();
			logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
			#endregion

			var result = UDI.VTOD.Controller.QueryController.Ping();

			#region Log
			stopwatch.Stop();
			logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
			#endregion

			return result;
		}
		#endregion
		#endregion

		#region Fault Sample
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Error
		/// </summary>
		public void JsonError()
		{
			QueryController.Error(WebContentFormat.Json);
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Error?media=xml
		/// </summary>
		public void XmlError()
		{
			QueryController.Error(WebContentFormat.Xml);
		}
		#endregion
		#endregion
		#endregion

		#region GetToken
		/// <summary> This method is used to return a Token Response Object containing Username,Security Key and ClientIPAddress
		/// URI: http://localhost:52542/Rest/GetToken
		/// </summary>
		/// <param name="input" type="TokenRQ"></param>
		/// <returns type="TokenRS"></returns>
		public TokenRS JsonGetToken(TokenRQ input)
		{
			try
			{
				var queryController = new QueryController();
				return queryController.GetToken(input);
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.TokenRS();
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.TokenRS>(response);
				throw faultException;
			}
		}

		/// <summary>
		/// URI: http://localhost:52542/Rest/GetToken?media=xml
		/// </summary>
		/// <param name="input"></param>
		/// <returns></returns>
		public TokenRS XmlGetToken(TokenRQ input)
		{
			try
			{
				var queryController = new QueryController();
				return queryController.GetToken(input);
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.TokenRS();
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.TokenRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Ground
		#region GroundAvail
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Ground/Avail
		/// </summary>
		
		public OTA_GroundAvailRS JsonGroundAvail(OTA_GroundAvailRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.GroundAvail(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
                //Just for testing Bitbucket
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.OTA_GroundAvailRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.OTA_GroundAvailRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Ground/Avail?media=xml
		/// </summary>
		
		public OTA_GroundAvailRS XmlGroundAvail(OTA_GroundAvailRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();

				var result = queryController.GroundAvail(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.OTA_GroundAvailRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.OTA_GroundAvailRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region GroundVehicleInfo
		#region JSON
		/// <summary>
		/// URI: http://localhost:52542/Rest/Ground/VehicleInfo
		/// </summary>
		/// <param name="input"></param>
		/// <returns></returns>
		public OTA_GroundAvailRS JsonGroundVehicleInfo(OTA_GroundAvailRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.GroundVehicleInfo(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;

			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.OTA_GroundAvailRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.OTA_GroundAvailRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region XML
		/// <summary>
		/// URI: http://localhost:52542/Rest/Ground/VehicleInfo?media=xml
		/// </summary>
		/// <param name="input"></param>
		/// <returns></returns>
		public OTA_GroundAvailRS XmlGroundVehicleInfo(OTA_GroundAvailRQ input)
		{
			try
			{
				Stopwatch sp = new Stopwatch();
				sp.Start();

				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.GroundVehicleInfo(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				sp.Stop();
				logger.InfoFormat("GroundVehicleInfo Duration {0} ms", sp.ElapsedMilliseconds);


				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.OTA_GroundAvailRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.OTA_GroundAvailRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region GroundBook
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Ground/Book
		/// </summary>		
		public OTA_GroundBookRS JsonGroundBook(OTA_GroundBookRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.GroundBook(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.OTA_GroundBookRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.OTA_GroundBookRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Ground/Book?media=xml
		/// </summary>		
		public OTA_GroundBookRS XmlGroundBook(OTA_GroundBookRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.GroundBook(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.OTA_GroundBookRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.OTA_GroundBookRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region GroundCancel
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Ground/Cancel
		/// </summary>		
		public OTA_GroundCancelRS JsonGroundCancel(OTA_GroundCancelRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.GroundCancel(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.OTA_GroundCancelRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.OTA_GroundCancelRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region XML
		/// <summary>
		/// URI: http://localhost:52542/Rest/Ground/Cancel?media=xml
		/// </summary>		
		public OTA_GroundCancelRS XmlGroundCancel(OTA_GroundCancelRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.GroundCancel(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.OTA_GroundCancelRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.OTA_GroundCancelRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region GroundResRetrieve
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Ground/ResRetrieve
		/// </summary>		
		public OTA_GroundResRetrieveRS JsonGroundResRetrieve(OTA_GroundResRetrieveRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.GroundResRetrieve(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.OTA_GroundResRetrieveRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.OTA_GroundResRetrieveRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region XML
		/// <summary>
		/// URI: http://localhost:52542/Rest/Ground/ResRetrieve?media=xml
		/// </summary>		
		public OTA_GroundResRetrieveRS XmlGroundResRetrieve(OTA_GroundResRetrieveRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.GroundResRetrieve(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.OTA_GroundResRetrieveRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.OTA_GroundResRetrieveRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region GroundOriginalResRetrieve
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Ground/OriginalResRetrieve
		/// </summary>		
		public OTA_GroundResRetrieveRS JsonGroundOriginalResRetrieve(OTA_GroundResRetrieveRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.GroundOriginalResRetrieve(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.OTA_GroundResRetrieveRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.OTA_GroundResRetrieveRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region XML
		/// <summary>
		/// URI: http://localhost:52542/Rest/Ground/OriginalResRetrieve?media=xml
		/// </summary>
		
		public OTA_GroundResRetrieveRS XmlGroundOriginalResRetrieve(OTA_GroundResRetrieveRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.GroundOriginalResRetrieve(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.OTA_GroundResRetrieveRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.OTA_GroundResRetrieveRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region GroundCancelFee
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Ground/CancelFee
		/// </summary>		
		public OTA_GroundResRetrieveRS JsonGroundCancelFee(OTA_GroundResRetrieveRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.GroundCancelFee(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.OTA_GroundResRetrieveRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.OTA_GroundResRetrieveRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region XML
		/// <summary>
		/// URI: http://localhost:52542/Rest/Ground/CancelFee?media=xml
		/// </summary>		
		public OTA_GroundResRetrieveRS XmlGroundCancelFee(OTA_GroundResRetrieveRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.GroundCancelFee(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.OTA_GroundResRetrieveRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.OTA_GroundResRetrieveRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

        #region GroundModifyBook
        #region Json
        /// <summary>
        /// URI: http://localhost:52542/Rest/Ground/Book
        /// </summary>       
        public OTA_GroundBookRS JsonGroundModifyBook(OTA_GroundBookRQ input)
        {
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
                #endregion

                var queryController = new QueryController();
                var result = queryController.GroundModifyBook(input);

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
                #endregion

                return result;
            }
            catch (OTA_Exception ex)
            {
                var response = new UDI.VTOD.Common.DTO.OTA.OTA_GroundBookRS();
                response.EchoToken = input.EchoToken;
                response.Target = input.Target;
                response.PrimaryLangID = input.PrimaryLangID;
                response.Errors = new ErrorList();
                response.Errors.Errors = ex.Errors;
                response.Format = WebContentFormat.Json;
                if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
                {
                    response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }

                var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.OTA_GroundBookRS>(response);
                throw faultException;
            }
        }
        #endregion

        #region Xml
        /// <summary>
        /// URI: http://localhost:52542/Rest/Ground/Book?media=xml
        /// </summary>       
        public OTA_GroundBookRS XmlGroundModifyBook(OTA_GroundBookRQ input)
        {
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
                #endregion

                var queryController = new QueryController();
                var result = queryController.GroundModifyBook(input);

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
                #endregion

                return result;
            }
            catch (OTA_Exception ex)
            {
                var response = new UDI.VTOD.Common.DTO.OTA.OTA_GroundBookRS();
                response.EchoToken = input.EchoToken;
                response.Target = input.Target;
                response.PrimaryLangID = input.PrimaryLangID;
                response.Errors = new ErrorList();
                response.Errors.Errors = ex.Errors;
                response.Format = WebContentFormat.Xml;
                if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
                {
                    response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }

                var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.OTA_GroundBookRS>(response);
                throw faultException;
            }
        }
        #endregion
        #endregion

		#region UtilityAvailableFleets
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Utility/AvailableFleets
		/// </summary>		
		public UtilityAvailableFleetsRS JsonUtilityAvailableFleets(UtilityAvailableFleetsRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.UtilityAvailableFleets(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.OTA_GroundResRetrieveRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.OTA_GroundResRetrieveRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region XML
		/// <summary>
		/// URI: http://localhost:52542/Rest/Utility/AvailableFleets
		/// </summary>
		public UtilityAvailableFleetsRS XmlUtilityAvailableFleets(UtilityAvailableFleetsRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.UtilityAvailableFleets(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.OTA_GroundResRetrieveRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.OTA_GroundResRetrieveRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion


        #region UtilityGetLeadTime
        #region Json
        /// <summary>
        /// URI: http://localhost:52542/Rest/Utility/GetLeadTime
		/// </summary>
        public UtilityGetLeadTimeRS JsonUtilityGetLeadTime(UtilityGetLeadTimeRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.UtilityGetLeadTime(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.UtilityGetLeadTimeRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

                var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilityGetLeadTimeRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region XML
		/// <summary>
        /// URI: http://localhost:52542/Rest/Utility/GetLeadTime
		/// </summary>
        public UtilityGetLeadTimeRS XmlUtilityGetLeadTime(UtilityGetLeadTimeRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.UtilityGetLeadTime(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
                var response = new UDI.VTOD.Common.DTO.OTA.UtilityGetLeadTimeRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

                var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilityGetLeadTimeRS>(response);
				throw faultException;
			}
		}
        #endregion
        #endregion

        #region UtilityGetClientToken
        #region Json
        /// <summary>
        /// URI: http://localhost:52542/Rest/Utility/GetClientToken
        /// </summary>
        public UtilityGetClientTokenRS JsonUtilityGetClientToken(UtilityGetClientTokenRQ input)
        {
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
                #endregion

                var queryController = new QueryController();
                var result = queryController.UtilityGetClientToken(input);

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
                #endregion

                return result;
            }
            catch (OTA_Exception ex)
            {
                var response = new UDI.VTOD.Common.DTO.OTA.UtilityGetClientTokenRS();
                response.EchoToken = input.EchoToken;
                response.Target = input.Target;
                response.PrimaryLangID = input.PrimaryLangID;
                response.Errors = new ErrorList();
                response.Errors.Errors = ex.Errors;
                response.Format = WebContentFormat.Json;
                if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
                {
                    response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }

                var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilityGetClientTokenRS>(response);
                throw faultException;
            }
        }
        #endregion
        #region XML
        /// <summary>
        /// URI: http://localhost:52542/Rest/Utility/GetClientToken
        /// </summary>
        public UtilityGetClientTokenRS XmlUtilityGetClientToken(UtilityGetClientTokenRQ input)
        {
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
                #endregion

                var queryController = new QueryController();
                var result = queryController.UtilityGetClientToken(input);

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
                #endregion

                return result;
            }
            catch (OTA_Exception ex)
            {
                var response = new UDI.VTOD.Common.DTO.OTA.UtilityGetClientTokenRS();
                response.EchoToken = input.EchoToken;
                response.Target = input.Target;
                response.PrimaryLangID = input.PrimaryLangID;
                response.Errors = new ErrorList();
                response.Errors.Errors = ex.Errors;
                response.Format = WebContentFormat.Xml;
                if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
                {
                    response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }

                var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilityGetClientTokenRS>(response);
                throw faultException;
            }
        }
        #endregion
        #endregion

        #region GroundGetPickupTime
        #region Json
        /// <summary>
        /// URI: http://localhost:52542/Rest/Ground/GetPickupTime
        /// </summary>
        public GroundGetPickupTimeRS JsonGroundGetPickupTime(GroundGetPickupTimeRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.GroundGetPickupTime(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.GroundGetPickupTimeRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.GroundGetPickupTimeRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region XML
		/// <summary>
		/// URI: http://localhost:52542/Rest/Ground/GetPickupTime?media=xml
		/// </summary>
		public GroundGetPickupTimeRS XmlGroundGetPickupTime(GroundGetPickupTimeRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.GroundGetPickupTime(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.GroundGetPickupTimeRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.GroundGetPickupTimeRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region GroundGetFareScheduleWithFees
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Ground/GetFareScheduleWithFees
		/// </summary>
		public GroundGetFareScheduleWithFeesRS JsonGroundGetFareScheduleWithFees(GroundGetFareScheduleWithFeesRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.GroundGetFareScheduleWithFees(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.GroundGetFareScheduleWithFeesRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.GroundGetFareScheduleWithFeesRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region XML
		/// <summary>
		/// URI: http://localhost:52542/Rest/Ground/GetFareScheduleWithFees?media=xml
		/// </summary>
		public GroundGetFareScheduleWithFeesRS XmlGroundGetFareScheduleWithFees(GroundGetFareScheduleWithFeesRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.GroundGetFareScheduleWithFees(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.GroundGetFareScheduleWithFeesRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.GroundGetFareScheduleWithFeesRS>(response);
				throw faultException;
			}
		}
        #endregion
        #endregion

        #region GroundGetFinalRoute
        #region Json
        /// <summary>
        /// URI: http://localhost:52542/Rest/Ground/GetFinalRoute
        /// </summary>
        public OTA_GroundGetFinalRouteRS JsonGroundGetFinalRoute(OTA_GroundGetFinalRouteRQ input)
        {
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
                #endregion

                var queryController = new QueryController();
                var result = queryController.GroundGetFinalRoute(input);

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
                #endregion

                return result;
            }
            catch (OTA_Exception ex)
            {
                var response = new UDI.VTOD.Common.DTO.OTA.OTA_GroundGetFinalRouteRS();
                response.EchoToken = input.EchoToken;
                response.Target = input.Target;
                response.PrimaryLangID = input.PrimaryLangID;
                response.Errors = new ErrorList();
                response.Errors.Errors = ex.Errors;
                response.Format = WebContentFormat.Json;
                if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
                {
                    response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }

                var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.OTA_GroundGetFinalRouteRS>(response);
                throw faultException;
            }
        }
        #endregion

        #region XML
        /// <summary>
        /// URI: http://localhost:52542/Rest/Ground/GetFinalRoute?media=xml
        /// </summary>
        public OTA_GroundGetFinalRouteRS XmlGroundGetFinalRoute(OTA_GroundGetFinalRouteRQ input)
        {
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
                #endregion

                var queryController = new QueryController();
                var result = queryController.GroundGetFinalRoute(input);

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
                #endregion

                return result;
            }
            catch (OTA_Exception ex)
            {
                var response = new UDI.VTOD.Common.DTO.OTA.OTA_GroundGetFinalRouteRS();
                response.EchoToken = input.EchoToken;
                response.Target = input.Target;
                response.PrimaryLangID = input.PrimaryLangID;
                response.Errors = new ErrorList();
                response.Errors.Errors = ex.Errors;
                response.Format = WebContentFormat.Xml;
                if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
                {
                    response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }

                var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.OTA_GroundGetFinalRouteRS>(response);
                throw faultException;
            }
        }
        #endregion
        #endregion

        #endregion

        #region Ground ASAP
        #region GroundCreateASAPRequest
        #region JSON
        /// <summary>
        /// URI: http://localhost:52542/Rest/Ground/CreateASAPRequest
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public OTA_GroundAvailRS JsonGroundCreateAsapRequest(OTA_GroundAvailRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.GroundCreateAsapRequest(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;

			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.OTA_GroundAvailRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.OTA_GroundAvailRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region XML
		/// <summary>
		/// URI: http://localhost:52542/Rest/Ground/CreateASAPRequest?media=xml
		/// </summary>
		/// <param name="input"></param>
		/// <returns></returns>
		public OTA_GroundAvailRS XmlGroundCreateAsapRequest(OTA_GroundAvailRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.GroundCreateAsapRequest(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.OTA_GroundAvailRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.OTA_GroundAvailRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region GroundGetASAPRequestStatus
		#region JSON
		/// <summary>
		/// URI: http://localhost:52542/Rest/Ground/CreateASAPRequest
		/// </summary>
		/// <param name="input"></param>
		/// <returns></returns>
		public GroundGetASAPRequestStatusRS JsonGroundGetASAPRequestStatus(GroundGetASAPRequestStatusRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion
				//GroundGetASAPRequestStatusRS result = null;
				var queryController = new QueryController();
				var result = queryController.GroundGetASAPRequestStatus(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;

			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.OTA_GroundAvailRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.OTA_GroundAvailRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region XML
		/// <summary>
		/// URI: http://localhost:52542/Rest/Ground/CreateASAPRequest?media=xml
		/// </summary>
		/// <param name="input"></param>
		/// <returns></returns>
		public GroundGetASAPRequestStatusRS XmlGroundGetASAPRequestStatus(GroundGetASAPRequestStatusRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.GroundGetASAPRequestStatus(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.OTA_GroundAvailRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.OTA_GroundAvailRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region GroundAbandonASAPRequest
		#region JSON
		/// <summary>
		/// URI: http://localhost:52542/Rest/Ground/CreateASAPRequest
		/// </summary>
		/// <param name="input"></param>
		/// <returns></returns>
		public GroundAbandonASAPRequestRS JsonGroundAbandonASAPRequest(GroundAbandonASAPRequestRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion
				//GroundGetASAPRequestStatusRS result = null;
				var queryController = new QueryController();
				var result = queryController.GroundAbandonASAPRequest(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;

			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.OTA_GroundAvailRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.OTA_GroundAvailRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region XML
		/// <summary>
		/// URI: http://localhost:52542/Rest/Ground/CreateASAPRequest?media=xml
		/// </summary>
		/// <param name="input"></param>
		/// <returns></returns>
		public GroundAbandonASAPRequestRS XmlGroundAbandonASAPRequest(GroundAbandonASAPRequestRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.GroundAbandonASAPRequest(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.OTA_GroundAvailRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.OTA_GroundAvailRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion
		#endregion

		#region Accounting

		#region AccountingAddPaymentCard
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Accounting/AddMemberPaymentCard
		/// </summary>
		public AccountingAddMemberPaymentCardRS JsonAccountingAddPaymentCard(AccountingAddMemberPaymentCardRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.AccountingAddPaymentCard(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.AccountingAddMemberPaymentCardRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.AccountingAddMemberPaymentCardRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Accounting/AddMemberPaymentCard?media=xml
		/// </summary>
		public AccountingAddMemberPaymentCardRS XmlAccountingAddPaymentCard(AccountingAddMemberPaymentCardRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();

				var result = queryController.AccountingAddPaymentCard(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.AccountingAddMemberPaymentCardRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.AccountingAddMemberPaymentCardRS>(response, new FaultReason(ex.Message));
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region AccountingGetMemberPaymentCards
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Accounting/GetMemberPaymentCards
		/// </summary>
		public AccountingGetMemberPaymentCardsRS JsonAccountingGetMemberPaymentCards(AccountingGetMemberPaymentCardsRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.AccountingGetMemberPaymentCards(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.AccountingGetMemberPaymentCardsRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.AccountingGetMemberPaymentCardsRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Accounting/GetMemberPaymentCards?media=xml
		/// </summary>
		public AccountingGetMemberPaymentCardsRS XmlAccountingGetMemberPaymentCards(AccountingGetMemberPaymentCardsRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();

				var result = queryController.AccountingGetMemberPaymentCards(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.AccountingGetMemberPaymentCardsRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.AccountingGetMemberPaymentCardsRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region AccountingSetMemberDefaultPaymentCard
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Accounting/SetMemberDefaultPaymentCard
		/// </summary>
		public AccountingSetMemberDefaultPaymentCardRS JsonAccountingSetMemberDefaultPaymentCard(AccountingSetMemberDefaultPaymentCardRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.AccountingSetMemberDefaultPaymentCard(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.AccountingSetMemberDefaultPaymentCardRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.AccountingSetMemberDefaultPaymentCardRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Accounting/SetMemberDefaultPaymentCard?media=xml
		/// </summary>
		public AccountingSetMemberDefaultPaymentCardRS XmlAccountingSetMemberDefaultPaymentCard(AccountingSetMemberDefaultPaymentCardRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();

				var result = queryController.AccountingSetMemberDefaultPaymentCard(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.AccountingSetMemberDefaultPaymentCardRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.AccountingSetMemberDefaultPaymentCardRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region AccountingDeleteMemberPaymentCard
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Accounting/DeleteMemberPaymentCard
		/// </summary>
		public AccountingDeleteMemberPaymentCardRS JsonAccountingDeleteMemberPaymentCard(AccountingDeleteMemberPaymentCardRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.AccountingDeleteMemberPaymentCard(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.AccountingDeleteMemberPaymentCardRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.AccountingDeleteMemberPaymentCardRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Accounting/DeleteMemberPaymentCard?media=xml
		/// </summary>
		public AccountingDeleteMemberPaymentCardRS XmlAccountingDeleteMemberPaymentCard(AccountingDeleteMemberPaymentCardRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();

				var result = queryController.AccountingDeleteMemberPaymentCard(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.AccountingDeleteMemberPaymentCardRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.AccountingDeleteMemberPaymentCardRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region AccountingAddMemberDirectBill
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Accounting/AddMemberDirectBill
		/// </summary>
		public AccountingAddMemberDirectBillRS JsonAccountingAddMemberDirectBill(AccountingAddMemberDirectBillRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.AccountingAddMemberDirectBill(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.AccountingAddMemberDirectBillRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.AccountingAddMemberDirectBillRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Accounting/AddMemberDirectBill?media=xml
		/// </summary>
		public AccountingAddMemberDirectBillRS XmlAccountingAddMemberDirectBill(AccountingAddMemberDirectBillRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();

				var result = queryController.AccountingAddMemberDirectBill(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.AccountingAddMemberDirectBillRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.AccountingAddMemberDirectBillRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region AccountingGetMemberDirectBills
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Accounting/GetMemberDirectBills
		/// </summary>
		public AccountingGetMemberDirectBillsRS JsonAccountingGetMemberDirectBills(AccountingGetMemberDirectBillsRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.AccountingGetMemberDirectBills(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.AccountingGetMemberDirectBillsRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.AccountingGetMemberDirectBillsRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Accounting/GetMemberDirectBills?media=xml
		/// </summary>
		public AccountingGetMemberDirectBillsRS XmlAccountingGetMemberDirectBills(AccountingGetMemberDirectBillsRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();

				var result = queryController.AccountingGetMemberDirectBills(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.AccountingGetMemberDirectBillsRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.AccountingGetMemberDirectBillsRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region AccountingSetMemberDefaultDirectBill
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Accounting/SetMemberDefaultDirectBill
		/// </summary>
		public AccountingSetMemberDefaultDirectBillRS JsonAccountingSetMemberDefaultDirectBill(AccountingSetMemberDefaultDirectBillRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.AccountingSetMemberDefaultDirectBill(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.AccountingSetMemberDefaultDirectBillRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.AccountingSetMemberDefaultDirectBillRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Accounting/SetMemberDefaultDirectBill?media=xml
		/// </summary>
		public AccountingSetMemberDefaultDirectBillRS XmlAccountingSetMemberDefaultDirectBill(AccountingSetMemberDefaultDirectBillRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();

				var result = queryController.AccountingSetMemberDefaultDirectBill(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.AccountingSetMemberDefaultDirectBillRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.AccountingSetMemberDefaultDirectBillRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region AccountingDeleteMemberDirectBill
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Accounting/DeleteMemberDirectBill
		/// </summary>
		public AccountingDeleteMemberDirectBillRS JsonAccountingDeleteMemberDirectBill(AccountingDeleteMemberDirectBillRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.AccountingDeleteMemberDirectBill(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.AccountingDeleteMemberDirectBillRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.AccountingDeleteMemberDirectBillRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Accounting/DeleteMemberDirectBill?media=xml
		/// </summary>
		public AccountingDeleteMemberDirectBillRS XmlAccountingDeleteMemberDirectBill(AccountingDeleteMemberDirectBillRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();

				var result = queryController.AccountingDeleteMemberDirectBill(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.AccountingDeleteMemberDirectBillRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.AccountingDeleteMemberDirectBillRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region GetFareDetail
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Accounting/DeleteMemberDirectBill
		/// </summary>
		public GetFareDetailRS JsonGetFareDetail(GetFareDetailRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.GetFareDetail(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.AccountingDeleteMemberDirectBillRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.AccountingDeleteMemberDirectBillRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Accounting/DeleteMemberDirectBill?media=xml
		/// </summary>
		public GetFareDetailRS XmlGetFareDetail(GetFareDetailRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();

				var result = queryController.GetFareDetail(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.AccountingDeleteMemberDirectBillRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.AccountingDeleteMemberDirectBillRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

        #region AccountingEditPaymentCard
        #region Json
        /// <summary>
        /// URI: http://localhost:52542/Rest/Accounting/EditMemberPaymentCard
        /// </summary>
        public AccountingEditMemberPaymentCardRS JsonAccountingEditPaymentCard(AccountingEditMemberPaymentCardRQ input)
        {
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
                #endregion

                var queryController = new QueryController();
                var result = queryController.AccountingEditPaymentCard(input);

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
                #endregion

                return result;
            }
            catch (OTA_Exception ex)
            {
                var response = new UDI.VTOD.Common.DTO.OTA.AccountingEditMemberPaymentCardRS();
                response.EchoToken = input.EchoToken;
                response.Target = input.Target;
                response.PrimaryLangID = input.PrimaryLangID;
                response.Errors = new ErrorList();
                response.Errors.Errors = ex.Errors;
                response.Format = WebContentFormat.Json;
                if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
                {
                    response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }

                var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.AccountingEditMemberPaymentCardRS>(response);
                throw faultException;
            }
        }
        #endregion

        #region Xml
        /// <summary>
        /// URI: http://localhost:52542/Rest/Accounting/EditMemberPaymentCard?media=xml
        /// </summary>
        public AccountingEditMemberPaymentCardRS XmlAccountingEditPaymentCard(AccountingEditMemberPaymentCardRQ input)
        {
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
                #endregion

                var queryController = new QueryController();

                var result = queryController.AccountingEditPaymentCard(input);

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
                #endregion

                return result;
            }
            catch (OTA_Exception ex)
            {
                var response = new UDI.VTOD.Common.DTO.OTA.AccountingEditMemberPaymentCardRS();
                response.EchoToken = input.EchoToken;
                response.Target = input.Target;
                response.PrimaryLangID = input.PrimaryLangID;
                response.Errors = new ErrorList();
                response.Errors.Errors = ex.Errors;
                response.Format = WebContentFormat.Xml;
                if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
                {
                    response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }

                var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.AccountingEditMemberPaymentCardRS>(response, new FaultReason(ex.Message));
                throw faultException;
            }
        }
        #endregion
        #endregion
		#endregion

		#region Membership

		#region MemberCreate
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Member/Create
		/// </summary>
		public MemberCreateRS JsonMemberCreate(MemberCreateRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				MemberCreateRS result = null;
				var queryController = new QueryController();
				result = queryController.MemberCreate(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.MemberCreateRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberCreateRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Member/Create?media=xml
		/// </summary>
		public MemberCreateRS XmlMemberCreate(MemberCreateRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				MemberCreateRS result = null;
				var queryController = new QueryController();

				result = queryController.MemberCreate(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.MemberCreateRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberCreateRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region MemberAddLocation
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Member/AddLocation
		/// </summary>
		public MemberAddLocationRS JsonMemberAddLocation(MemberAddLocationRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				MemberAddLocationRS result = null;
				var queryController = new QueryController();
				result = queryController.MemberAddLocation(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.MemberAddLocationRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberAddLocationRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Member/AddLocation?media=xml
		/// </summary>
		public MemberAddLocationRS XmlMemberAddLocation(MemberAddLocationRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				MemberAddLocationRS result = null;
				var queryController = new QueryController();

				result = queryController.MemberAddLocation(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.MemberAddLocationRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberAddLocationRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region MemberChangeEmailAddress
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Member/ChangeEmailAddress
		/// </summary>
		public MemberChangeEmailAddressRS JsonMemberChangeEmailAddress(MemberChangeEmailAddressRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				MemberChangeEmailAddressRS result = null;
				var queryController = new QueryController();
				result = queryController.MemberChangeEmailAddress(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.MemberChangeEmailAddressRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberChangeEmailAddressRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Member/ChangeEmailAddress?media=xml
		/// </summary>
		public MemberChangeEmailAddressRS XmlMemberChangeEmailAddress(MemberChangeEmailAddressRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				MemberChangeEmailAddressRS result = null;
				var queryController = new QueryController();

				result = queryController.MemberChangeEmailAddress(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.MemberChangeEmailAddressRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberChangeEmailAddressRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region MemberChangePassword
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Member/ChangePassword
		/// </summary>
		public MemberChangePasswordRS JsonMemberChangePassword(MemberChangePasswordRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				MemberChangePasswordRS result = null;
				var queryController = new QueryController();
				result = queryController.MemberChangePassword(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.MemberChangePasswordRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberChangePasswordRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Member/ChangePassword?media=xml
		/// </summary>
		public MemberChangePasswordRS XmlMemberChangePassword(MemberChangePasswordRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				MemberChangePasswordRS result = null;
				var queryController = new QueryController();

				result = queryController.MemberChangePassword(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.MemberChangePasswordRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberChangePasswordRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region MemberDeleteLocation
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Member/DeleteLocation
		/// </summary>
		public MemberDeleteLocationRS JsonMemberDeleteLocation(MemberDeleteLocationRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				MemberDeleteLocationRS result = null;
				var queryController = new QueryController();
				result = queryController.MemberDeleteLocation(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.MemberDeleteLocationRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberDeleteLocationRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Member/DeleteLocation?media=xml
		/// </summary>
		public MemberDeleteLocationRS XmlMemberDeleteLocation(MemberDeleteLocationRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				MemberDeleteLocationRS result = null;
				var queryController = new QueryController();

				result = queryController.MemberDeleteLocation(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.MemberDeleteLocationRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberDeleteLocationRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region MemberForgotPassword
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Member/ForgotPassword
		/// </summary>
		public MemberForgotPasswordRS JsonMemberForgotPassword(MemberForgotPasswordRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				MemberForgotPasswordRS result = null;
				var queryController = new QueryController();
				result = queryController.MemberForgotPassword(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.MemberForgotPasswordRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberForgotPasswordRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Member/ForgotPassword?media=xml
		/// </summary>
		public MemberForgotPasswordRS XmlMemberForgotPassword(MemberForgotPasswordRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				MemberForgotPasswordRS result = null;
				var queryController = new QueryController();

				result = queryController.MemberForgotPassword(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.MemberForgotPasswordRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberForgotPasswordRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region MemberGetPasswordRequest
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Member/GetPasswordRequest
		/// </summary>
		public MemberGetPasswordRequestRS JsonMemberGetPasswordRequest(MemberGetPasswordRequestRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				MemberGetPasswordRequestRS result = null;
				var queryController = new QueryController();
				result = queryController.MemberGetPasswordRequest(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.MemberGetPasswordRequestRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberGetPasswordRequestRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Member/GetPasswordRequest?media=xml
		/// </summary>
		public MemberGetPasswordRequestRS XmlMemberGetPasswordRequest(MemberGetPasswordRequestRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				MemberGetPasswordRequestRS result = null;
				var queryController = new QueryController();

				result = queryController.MemberGetPasswordRequest(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.MemberGetPasswordRequestRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberGetPasswordRequestRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region MemberResetPasswordRequest
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Member/ResetPassword
		/// </summary>
		public MemberResetPasswordRS JsonMemberResetPassword(MemberResetPasswordRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				MemberResetPasswordRS result = null;
				var queryController = new QueryController();
				result = queryController.MemberResetPassword(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.MemberResetPasswordRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberResetPasswordRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Member/ResetPassword?media=xml
		/// </summary>
		public MemberResetPasswordRS XmlMemberResetPassword(MemberResetPasswordRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				MemberResetPasswordRS result = null;
				var queryController = new QueryController();

				result = queryController.MemberResetPassword(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.MemberResetPasswordRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberResetPasswordRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region MemberGetLocations
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Member/GetLocations
		/// </summary>
		public MemberGetLocationsRS JsonMemberGetLocations(MemberGetLocationsRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				MemberGetLocationsRS result = null;
				var queryController = new QueryController();
				result = queryController.MemberGetLocations(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.MemberGetLocationsRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberGetLocationsRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Member/GetLocations?media=xml
		/// </summary>
		public MemberGetLocationsRS XmlMemberGetLocations(MemberGetLocationsRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				MemberGetLocationsRS result = null;
				var queryController = new QueryController();

				result = queryController.MemberGetLocations(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.MemberGetLocationsRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberGetLocationsRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region MemberGetRecentLocations
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Member/GetRecentLocations
		/// </summary>
		public MemberGetLocationsRS JsonMemberGetRecentLocations(MemberGetLocationsRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				MemberGetLocationsRS result = null;
				var queryController = new QueryController();
				result = queryController.MemberGetRecentLocations(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.MemberGetLocationsRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberGetLocationsRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Member/GetRecentLocations?media=xml
		/// </summary>
		public MemberGetLocationsRS XmlMemberGetRecentLocations(MemberGetLocationsRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				MemberGetLocationsRS result = null;
				var queryController = new QueryController();

				result = queryController.MemberGetRecentLocations(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.MemberGetLocationsRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberGetLocationsRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region MemberGetReservation
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Member/GetReservation
		/// </summary>
		public MemberGetReservationRS JsonMemberGetReservation(MemberGetReservationRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				MemberGetReservationRS result = null;
				var queryController = new QueryController();
				result = queryController.MemberGetReservation(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.MemberGetReservationRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberGetReservationRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Member/GetReservation?media=xml
		/// </summary>
		public MemberGetReservationRS XmlMemberGetReservation(MemberGetReservationRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				MemberGetReservationRS result = null;
				var queryController = new QueryController();

				result = queryController.MemberGetReservation(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.MemberGetReservationRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberGetReservationRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region MemberLinkReservation
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Member/LinkReservation
		/// </summary>
		public MemberLinkReservationRS JsonMemberLinkReservation(MemberLinkReservationRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				MemberLinkReservationRS result = null;
				var queryController = new QueryController();
				result = queryController.MemberLinkReservation(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.MemberLinkReservationRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberLinkReservationRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Member/LinkReservation?media=xml
		/// </summary>
		public MemberLinkReservationRS XmlMemberLinkReservation(MemberLinkReservationRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				MemberLinkReservationRS result = null;
				var queryController = new QueryController();

				result = queryController.MemberLinkReservation(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.MemberLinkReservationRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberLinkReservationRS>(response);
				throw faultException;
			}
		}
        #endregion
        #endregion


        #region MemberGetReferralLink
        #region Json
        /// <summary>
        /// URI: http://localhost:52542/Rest/Member/GetReferralLink
        /// </summary>
        public MemberGetReferralLinkRS JsonMemberGetReferralLink(MemberGetReferralLinkRQ input)
        {
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
                #endregion

                MemberGetReferralLinkRS result = null;
                var queryController = new QueryController();
                result = queryController.MemberGetReferralLink(input);

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
                #endregion

                return result;
            }
            catch (OTA_Exception ex)
            {
                var response = new UDI.VTOD.Common.DTO.OTA.MemberLoginRS();
                response.EchoToken = input.EchoToken;
                response.Target = input.Target;
                response.PrimaryLangID = input.PrimaryLangID;
                response.Errors = new ErrorList();
                response.Errors.Errors = ex.Errors;
                response.Format = WebContentFormat.Json;
                if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
                {
                    response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }

                var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberLoginRS>(response);
                throw faultException;
            }
        }
        #endregion

        #region Xml
        /// <summary>
        /// URI: http://localhost:52542/Rest/Member/GetReferralLink?media=xml
        /// </summary>
        public MemberGetReferralLinkRS XmlMemberGetReferralLink(MemberGetReferralLinkRQ input)
        {
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
                #endregion

                MemberGetReferralLinkRS result = null;
                var queryController = new QueryController();

                result = queryController.MemberGetReferralLink(input);

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
                #endregion

                return result;
            }
            catch (OTA_Exception ex)
            {
                var response = new UDI.VTOD.Common.DTO.OTA.MemberLoginRS();
                response.EchoToken = input.EchoToken;
                response.Target = input.Target;
                response.PrimaryLangID = input.PrimaryLangID;
                response.Errors = new ErrorList();
                response.Errors.Errors = ex.Errors;
                response.Format = WebContentFormat.Xml;
                if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
                {
                    response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }

                var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberLoginRS>(response);
                throw faultException;
            }
        }
        #endregion
        #endregion

        #region MemberLogin
        #region Json
        /// <summary>
        /// URI: http://localhost:52542/Rest/Member/Login
        /// </summary>
        public MemberLoginRS JsonMemberLogin(MemberLoginRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				MemberLoginRS result = null;
				var queryController = new QueryController();
				result = queryController.MemberLogin(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.MemberLoginRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberLoginRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Member/Login?media=xml
		/// </summary>
		public MemberLoginRS XmlMemberLogin(MemberLoginRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				MemberLoginRS result = null;
				var queryController = new QueryController();

				result = queryController.MemberLogin(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.MemberLoginRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberLoginRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region MemberUpdateLocation
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Member/UpdateLocation
		/// </summary>
		public MemberUpdateLocationRS JsonMemberUpdateLocation(MemberUpdateLocationRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				MemberUpdateLocationRS result = null;
				var queryController = new QueryController();
				result = queryController.MemberUpdateLocation(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.MemberUpdateLocationRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberUpdateLocationRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Member/UpdateLocation?media=xml
		/// </summary>
		public MemberUpdateLocationRS XmlMemberUpdateLocation(MemberUpdateLocationRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				MemberUpdateLocationRS result = null;
				var queryController = new QueryController();

				result = queryController.MemberUpdateLocation(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.MemberUpdateLocationRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberUpdateLocationRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region MemberUpdateProfile
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Member/UpdateProfile
		/// </summary>
		public MemberUpdateProfileRS JsonMemberUpdateProfile(MemberUpdateProfileRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				MemberUpdateProfileRS result = null;
				var queryController = new QueryController();
				result = queryController.MemberUpdateProfile(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.MemberUpdateProfileRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberUpdateProfileRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Member/UpdateProfile?media=xml
		/// </summary>
		public MemberUpdateProfileRS XmlMemberUpdateProfile(MemberUpdateProfileRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				MemberUpdateProfileRS result = null;
				var queryController = new QueryController();

				result = queryController.MemberUpdateProfile(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.MemberUpdateProfileRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberUpdateProfileRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region MemberRequestEmailConfirmation
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Member/RequestEmailConfirmation
		/// </summary>
		public MemberRequestEmailConfirmationRS JsonMemberRequestEmailConfirmation(MemberRequestEmailConfirmationRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				MemberRequestEmailConfirmationRS result = null;
				var queryController = new QueryController();
				result = queryController.MemberRequestEmailConfirmation(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.MemberRequestEmailConfirmationRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberRequestEmailConfirmationRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Member/RequestEmailConfirmation?media=xml
		/// </summary>
		public MemberRequestEmailConfirmationRS XmlMemberRequestEmailConfirmation(MemberRequestEmailConfirmationRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				MemberRequestEmailConfirmationRS result = null;
				var queryController = new QueryController();

				result = queryController.MemberRequestEmailConfirmation(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.MemberRequestEmailConfirmationRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberRequestEmailConfirmationRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region MemberRequestEmailConfirmation
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Member/GetLatestUnRatedTrip
		/// </summary>
		public MemberGetLatestUnRatedTripRS JsonMemberGetLatestUnRatedTrip(MemberGetLatestUnRatedTripRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				MemberGetLatestUnRatedTripRS result = null;
				var queryController = new QueryController();
				result = queryController.MemberGetLatestUnRatedTrip(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.MemberGetLatestUnRatedTripRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberGetLatestUnRatedTripRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Member/GetLatestUnRatedTrip?media=xml
		/// </summary>
		public MemberGetLatestUnRatedTripRS XmlMemberGetLatestUnRatedTrip(MemberGetLatestUnRatedTripRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				MemberGetLatestUnRatedTripRS result = null;
				var queryController = new QueryController();

				result = queryController.MemberGetLatestUnRatedTrip(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.MemberGetLatestUnRatedTripRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberGetLatestUnRatedTripRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

        #region MemberGetLatestTrip
        #region Json
        /// <summary>
		/// URI: http://localhost:52542/Rest/Member/GetLatestTrip
		/// </summary>
        public MemberGetLatestTripRS JsonMemberGetLatestTrip(MemberGetLatestTripRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				MemberGetLatestTripRS result = null;
				var queryController = new QueryController();
				result = queryController.MemberGetLatestTrip(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.MemberGetLatestTripRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberGetLatestTripRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Member/GetLatestTrip?media=xml
		/// </summary>
        public MemberGetLatestTripRS XmlMemberGetLatestTrip(MemberGetLatestTripRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				MemberGetLatestTripRS result = null;
				var queryController = new QueryController();

				result = queryController.MemberGetLatestTrip(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.MemberGetLatestTripRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberGetLatestTripRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region MemberGetCorporateProfile
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Member/GetCorporateProfile
		/// </summary>
		public MemberGetCorporateProfileRS JsonMemberGetCorporateProfile(MemberGetCorporateProfileRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				MemberGetCorporateProfileRS result = null;
				var queryController = new QueryController();
				result = queryController.MemberGetCorporateProfile(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.MemberGetCorporateProfileRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberGetCorporateProfileRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Member/GetCorporateProfile?media=xml
		/// </summary>
		public MemberGetCorporateProfileRS XmlMemberGetCorporateProfile(MemberGetCorporateProfileRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				MemberGetCorporateProfileRS result = null;
				var queryController = new QueryController();

				result = queryController.MemberGetCorporateProfile(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.MemberGetCorporateProfileRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberGetCorporateProfileRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion


        #region MemberGetCorporateProfile2
        #region Json
        /// <summary>
        /// URI: http://localhost:52542/Rest/Member/GetCorporateProfile2
        /// </summary>
        public MemberGetCorporateProfile2RS JsonMemberGetCorporateProfile2(MemberGetCorporateProfile2RQ input)
        {
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
                #endregion

                MemberGetCorporateProfile2RS result = null;
                var queryController = new QueryController();
                result = queryController.MemberGetCorporateProfile2(input);

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
                #endregion

                return result;
            }
            catch (OTA_Exception ex)
            {
                var response = new UDI.VTOD.Common.DTO.OTA.MemberGetCorporateProfile2RS();
                response.EchoToken = input.EchoToken;
                response.Target = input.Target;
                response.PrimaryLangID = input.PrimaryLangID;
                response.Errors = new ErrorList();
                response.Errors.Errors = ex.Errors;
                response.Format = WebContentFormat.Json;
                if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
                {
                    response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }

                var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberGetCorporateProfile2RS>(response);
                throw faultException;
            }
        }
        #endregion

        #region Xml
        /// <summary>
        /// URI: http://localhost:52542/Rest/Member/GetCorporateProfile2?media=xml
        /// </summary>
        public MemberGetCorporateProfile2RS XmlMemberGetCorporateProfile2(MemberGetCorporateProfile2RQ input)
        {
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
                #endregion

                MemberGetCorporateProfile2RS result = null;
                var queryController = new QueryController();

                result = queryController.MemberGetCorporateProfile2(input);

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
                #endregion

                return result;
            }
            catch (OTA_Exception ex)
            {
                var response = new UDI.VTOD.Common.DTO.OTA.MemberGetCorporateProfile2RS();
                response.EchoToken = input.EchoToken;
                response.Target = input.Target;
                response.PrimaryLangID = input.PrimaryLangID;
                response.Errors = new ErrorList();
                response.Errors.Errors = ex.Errors;
                response.Format = WebContentFormat.Xml;
                if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
                {
                    response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }

                var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberGetCorporateProfile2RS>(response);
                throw faultException;
            }
        }
        #endregion
        #endregion

		#region MemberDelete
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Member/Delete
		/// </summary>
		public MemberDeleteRS JsonMemberDelete(MemberDeleteRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				MemberDeleteRS result = null;
				var queryController = new QueryController();
				result = queryController.MemberDelete(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.MemberDeleteRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberDeleteRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Member/GetCorporateProfile?media=xml
		/// </summary>
		public MemberDeleteRS XmlMemberDelete(MemberDeleteRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				MemberDeleteRS result = null;
				var queryController = new QueryController();

				result = queryController.MemberDelete(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.MemberDeleteRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberDeleteRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

        #region MemberRequestEmailCancellation
        #region Json
        /// <summary>
        /// URI: http://localhost:52542/Rest/Member/RequestEmailCancellation
        /// </summary>
        public MemberRequestEmailCancellationRS JsonMemberRequestEmailCancellation(MemberRequestEmailCancellationRQ input)
        {
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
                #endregion

                MemberRequestEmailCancellationRS result = null;
                var queryController = new QueryController();
                result = queryController.MemberRequestEmailCancellation(input);

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
                #endregion

                return result;
            }
            catch (OTA_Exception ex)
            {
                var response = new UDI.VTOD.Common.DTO.OTA.MemberRequestEmailCancellationRS();
                response.EchoToken = input.EchoToken;
                response.Target = input.Target;
                response.PrimaryLangID = input.PrimaryLangID;
                response.Errors = new ErrorList();
                response.Errors.Errors = ex.Errors;
                response.Format = WebContentFormat.Json;
                if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
                {
                    response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }

                var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberRequestEmailCancellationRS>(response);
                throw faultException;
            }
        }
        #endregion

        #region Xml
        /// <summary>
        /// URI: http://localhost:52542/Rest/Member/RequestEmailCancellation?media=xml
        /// </summary>
        public MemberRequestEmailCancellationRS XmlMemberRequestEmailCancellation(MemberRequestEmailCancellationRQ input)
        {
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
                #endregion

                MemberRequestEmailCancellationRS result = null;
                var queryController = new QueryController();

                result = queryController.MemberRequestEmailCancellation(input);

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
                #endregion

                return result;
            }
            catch (OTA_Exception ex)
            {
                var response = new UDI.VTOD.Common.DTO.OTA.MemberRequestEmailCancellationRS();
                response.EchoToken = input.EchoToken;
                response.Target = input.Target;
                response.PrimaryLangID = input.PrimaryLangID;
                response.Errors = new ErrorList();
                response.Errors.Errors = ex.Errors;
                response.Format = WebContentFormat.Xml;
                if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
                {
                    response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }

                var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.MemberRequestEmailCancellationRS>(response);
                throw faultException;
            }
        }
        #endregion
        #endregion

		#endregion

		#region Utilities
		#region UtilityGetDestinations
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Utility/GetDestinations
		/// </summary>
		public UtilityGetDestinationsRS JsonUtilityGetDestinations(UtilityGetDestinationsRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				UtilityGetDestinationsRS result = null;
				var queryController = new QueryController();
				result = queryController.UtilityGetDestinations(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.UtilityGetDestinationsRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilityGetDestinationsRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Utility/GetDestinations?media=xml
		/// </summary>
		public UtilityGetDestinationsRS XmlUtilityGetDestinations(UtilityGetDestinationsRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				UtilityGetDestinationsRS result = null;
				var queryController = new QueryController();

				result = queryController.UtilityGetDestinations(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.UtilityGetDestinationsRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilityGetDestinationsRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region UtilityGetServicedAirports
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Utility/GetServicedAirports
		/// </summary>
		public UtilityGetServicedAirportsRS JsonUtilityGetServicedAirports(UtilityGetServicedAirportsRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				UtilityGetServicedAirportsRS result = null;
				var queryController = new QueryController();
				result = queryController.GetServicedAirports(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.UtilityGetServicedAirportsRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilityGetServicedAirportsRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Utility/GetServicedAirports?media=xml
		/// </summary>
		public UtilityGetServicedAirportsRS XmlUtilityGetServicedAirports(UtilityGetServicedAirportsRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				UtilityGetServicedAirportsRS result = null;
				var queryController = new QueryController();

				result = queryController.GetServicedAirports(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.UtilityGetServicedAirportsRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilityGetServicedAirportsRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region GetDefaultFleetTypeForPoint
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Utility/GetDefaultVehicleTypeForPoint
		/// </summary>
		public UtilityGetDefaultFleetTypeForPointRS JsonGetDefaultFleetTypeForPoint(UtilityGetDefaultFleetTypeForPointRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				UtilityGetDefaultFleetTypeForPointRS result = null;
				var queryController = new QueryController();
				result = queryController.GetDefaultVehicleTypeForPoint(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.UtilityGetDefaultFleetTypeForPointRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilityGetDefaultFleetTypeForPointRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Utility/GetDefaultVehicleTypeForPoint?media=xml
		/// </summary>
		public UtilityGetDefaultFleetTypeForPointRS XmlGetDefaultFleetTypeForPoint(UtilityGetDefaultFleetTypeForPointRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				UtilityGetDefaultFleetTypeForPointRS result = null;
				var queryController = new QueryController();

				result = queryController.GetDefaultVehicleTypeForPoint(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.UtilityGetDefaultFleetTypeForPointRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilityGetDefaultFleetTypeForPointRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region UtilityGetLocationList
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Utility/GetCharterLocationsByType
		/// </summary>
		public UtilityGetLocationListRS JsonUtilityGetLocationList(UtilityGetLocationListRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				UtilityGetLocationListRS result = null;
				var queryController = new QueryController();
				result = queryController.GetCharterLocationsByType(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.UtilityGetLocationListRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilityGetLocationListRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Utility/GetCharterLocationsByType?media=xml
		/// </summary>
		public UtilityGetLocationListRS XmlUtilityGetLocationList(UtilityGetLocationListRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				UtilityGetLocationListRS result = null;
				var queryController = new QueryController();

				result = queryController.GetCharterLocationsByType(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.UtilityGetLocationListRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilityGetLocationListRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region UtilitySearchLocation
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Utility/CharterSearchLandmark
		/// </summary>
		public UtilitySearchLocationRS JsonUtilitySearchLocation(UtilitySearchLocationRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				UtilitySearchLocationRS result = null;
				var queryController = new QueryController();
				result = queryController.CharterSearchLandmark(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.UtilitySearchLocationRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilitySearchLocationRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Utility/CharterSearchLandmark?media=xml
		/// </summary>
		public UtilitySearchLocationRS XmlUtilitySearchLocation(UtilitySearchLocationRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				UtilitySearchLocationRS result = null;
				var queryController = new QueryController();

				result = queryController.CharterSearchLandmark(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.UtilitySearchLocationRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilitySearchLocationRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region UtilityGetAirportName
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Utility/GetAirportName
		/// </summary>
		public UtilityGetAirportNameRS JsonUtilityGetAirportName(UtilityGetAirportNameRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				UtilityGetAirportNameRS result = null;
				var queryController = new QueryController();
				result = queryController.GetAirportName(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.UtilityGetAirportNameRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilityGetAirportNameRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Utility/GetAirportName?media=xml
		/// </summary>
		public UtilityGetAirportNameRS XmlUtilityGetAirportName(UtilityGetAirportNameRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				UtilityGetAirportNameRS result = null;
				var queryController = new QueryController();

				result = queryController.GetAirportName(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.UtilityGetAirportNameRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilityGetAirportNameRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region UtilityGetAirlines
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Utility/GetAirlines
		/// </summary>
		public UtilityGetAirlinesByAirportRS JsonUtilityGetAirlinesByAirportRS(UtilityGetAirlinesByAirportRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				UtilityGetAirlinesByAirportRS result = null;
				var queryController = new QueryController();
				result = queryController.UtilityGetAirlinesByAirport(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.UtilityGetAirlinesByAirportRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilityGetAirlinesByAirportRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Utility/GetAirportName?media=xml
		/// </summary>
		public UtilityGetAirlinesByAirportRS XmlUtilityGetAirlinesByAirportRS(UtilityGetAirlinesByAirportRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				UtilityGetAirlinesByAirportRS result = null;
				var queryController = new QueryController();

				result = queryController.UtilityGetAirlinesByAirport(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.UtilityGetAirlinesByAirportRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilityGetAirlinesByAirportRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region UtilityGetTripList
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Utility/GetTripList
		/// </summary>
		public UtilityGetTripListRS JsonUtilityGetTripList(UtilityGetTripListRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				UtilityGetTripListRS result = null;
				var queryController = new QueryController();
				result = queryController.UtilityGetTripList(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.UtilityGetTripListRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilityGetTripListRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Utility/GetTripList?media=xml
		/// </summary>
		public UtilityGetTripListRS XmlUtilityGetTripList(UtilityGetTripListRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				UtilityGetTripListRS result = null;
				var queryController = new QueryController();

				result = queryController.UtilityGetTripList(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.UtilityGetTripListRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilityGetTripListRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region UtilityGetAirportInstructions
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Utility/GetAirportInstructions
		/// </summary>
		public UtilityGetAirportInstructionsRS JsonUtilityGetAirportInstructions(UtilityGetAirportInstructionsRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				UtilityGetAirportInstructionsRS result = null;
				var queryController = new QueryController();
				result = queryController.UtilityGetAirportInstructions(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.UtilityGetTripListRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilityGetTripListRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Utility/GetAirportInstructions?media=xml
		/// </summary>
		public UtilityGetAirportInstructionsRS XmlUtilityGetAirportInstructions(UtilityGetAirportInstructionsRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				UtilityGetAirportInstructionsRS result = null;
				var queryController = new QueryController();

				result = queryController.UtilityGetAirportInstructions(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.UtilityGetTripListRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilityGetTripListRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region UtilityGetHonorifics
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Utility/GetHonorifics
		/// </summary>
		public UtilityGetHonorificsRS JsonUtilityGetHonorifics(UtilityGetHonorificsRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				UtilityGetHonorificsRS result = null;
				var queryController = new QueryController();
				result = queryController.UtilityGetHonorifics(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.UtilityGetHonorificsRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilityGetHonorificsRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Utility/GetHonorifics?media=xml
		/// </summary>
		public UtilityGetHonorificsRS XmlUtilityGetHonorifics(UtilityGetHonorificsRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				UtilityGetHonorificsRS result = null;
				var queryController = new QueryController();

				result = queryController.UtilityGetHonorifics(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.UtilityGetHonorificsRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilityGetHonorificsRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region UtilityGetBookingCriteria
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Utility/GetBookingCriteria
		/// </summary>
		public UtilityGetBookingCriteriaRS JsonUtilityGetBookingCriteria(UtilityGetBookingCriteriaRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				UtilityGetBookingCriteriaRS result = null;
				var queryController = new QueryController();
				result = queryController.UtilityGetBookingCriteria(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.UtilityGetBookingCriteriaRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilityGetBookingCriteriaRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Utility/GetBookingCriteria
		/// </summary>
		public UtilityGetBookingCriteriaRS XmlUtilityGetBookingCriteria(UtilityGetBookingCriteriaRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				UtilityGetBookingCriteriaRS result = null;
				var queryController = new QueryController();

				result = queryController.UtilityGetBookingCriteria(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.UtilityGetBookingCriteriaRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilityGetBookingCriteriaRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region UtilityGetCorporateRequirements
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Utility/GetCorporateRequirements
		/// </summary>
		/// <param name="input"></param>
		/// <returns></returns>
		public UtilityGetCorporateRequirementsRS JsonUtilityGetCorporateRequirements(UtilityGetCorporateRequirementsRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.UtilityGetCorporateRequirements(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UtilityGetCorporateRequirementsRS
				{
					EchoToken = input.EchoToken,
					Target = input.Target,
					PrimaryLangID = input.PrimaryLangID,
					Errors = new ErrorList { Errors = ex.Errors },
					Format = WebContentFormat.Json
				};
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UtilityGetCorporateRequirementsRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Utility/GetCorporateRequirements?media=xml
		/// </summary>
		/// <param name="input"></param>
		/// <returns></returns>
		public UtilityGetCorporateRequirementsRS XmlUtilityGetCorporateRequirements(UtilityGetCorporateRequirementsRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.UtilityGetCorporateRequirements(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UtilityGetCorporateRequirementsRS
				{
					EchoToken = input.EchoToken,
					Target = input.Target,
					PrimaryLangID = input.PrimaryLangID,
					Errors = new ErrorList { Errors = ex.Errors },
					Format = WebContentFormat.Xml
				};
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UtilityGetCorporateRequirementsRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region UtilityTypeAhead
		#region Json
		/// <summary>
        /// URI: http://localhost:55344/Rest/Utility/TypeAhead/{username}/{corporate}/{corporateCompanyName}/{accountName}/{requirementName}/{letters}
		/// </summary>
        /// <param name="username">Aleph user name</param>
		/// <param name="corporate">Corporation</param>
        /// <param name="corporateCompanyName">Corporation company name</param>
		/// <param name="accountName">Account name</param>
        /// <param name="requirementName">Requirement name</param>
		/// <param name="letters">User input string</param>
		/// <returns></returns>
        public List<string> JsonUtilityTypeAhead(string username, string corporate, string corporateCompanyName, string accountName, string requirementName, string letters)
		{
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
                #endregion

                var queryController = new QueryController();
                var result = queryController.UtilityTypeAhead(username, corporate, corporateCompanyName, accountName, requirementName, letters);

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
                #endregion

                return result;
            }
            catch (OTA_Exception ex)
            {
                var faultException = new FaultException<List<string>>(null);
                throw faultException;
            }
		}
		#endregion

		#region XML
		//*************************
		//We do not support XML for this method
		//*************************

		/// <summary>
		/// URI: http://localhost:55344/Rest/Utility/TypeAhead/{corporate}/{corporateCompanyName}/{accountName}/{requirementName}/{letters}?media=xml
		/// </summary>
		/// <param name="input"></param>
		/// <returns></returns>
		//public List<string> XmlUtilityTypeAhead(string corporate, string corporateCompanyName, string accountName, string requirementName, string letters)
		//{
		//	return new List<string> { "Teabag1", "Teabag2", "Teacup1", "TeaLeave", "Test1", "Test2", "Test3", "Test4", "Test5", "Type1" };
		//}
		#endregion
		#endregion

        #region UtilityGetCorporatePaymentMethod
        #region Json
        /// <summary>
        /// URI: http://localhost:52542/Rest/Utility/GetCorporatePaymentMethod
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public UtilityGetCorporateRequirementsRS JsonUtilityGetCorporatePaymentMethod(UtilityGetCorporateRequirementsRQ input)
        {
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
                #endregion

                var queryController = new QueryController();
                var result = queryController.UtilityGetCorporatePaymentMethod(input);

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
                #endregion

                return result;
            }
            catch (OTA_Exception ex)
            {
                var response = new UtilityGetCorporateRequirementsRS
                {
                    EchoToken = input.EchoToken,
                    Target = input.Target,
                    PrimaryLangID = input.PrimaryLangID,
                    Errors = new ErrorList { Errors = ex.Errors },
                    Format = WebContentFormat.Json
                };
                if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
                {
                    response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }

                var faultException = new FaultException<UtilityGetCorporateRequirementsRS>(response);
                throw faultException;
            }
        }
        #endregion

        #region Xml
        /// <summary>
        /// URI: http://localhost:52542/Rest/Utility/GetCorporatePaymentMethod?media=xml
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public UtilityGetCorporateRequirementsRS XmlUtilityGetCorporatePaymentMethod(UtilityGetCorporateRequirementsRQ input)
        {
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
                #endregion

                var queryController = new QueryController();
                var result = queryController.UtilityGetCorporatePaymentMethod(input);

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
                #endregion

                return result;
            }
            catch (OTA_Exception ex)
            {
                var response = new UtilityGetCorporateRequirementsRS
                {
                    EchoToken = input.EchoToken,
                    Target = input.Target,
                    PrimaryLangID = input.PrimaryLangID,
                    Errors = new ErrorList { Errors = ex.Errors },
                    Format = WebContentFormat.Xml
                };
                if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
                {
                    response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }

                var faultException = new FaultException<UtilityGetCorporateRequirementsRS>(response);
                throw faultException;
            }
        }
        #endregion
        #endregion

		#region UtilityGetAirportPickupOptions
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Utility/GetAirportPickupOptions
		/// </summary>
		public UtilityGetAirportPickupOptionsRS JsonUtilityGetAirportPickupOptions(UtilityGetAirportPickupOptionsRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				UtilityGetAirportPickupOptionsRS result = null;
				var queryController = new QueryController();
				result = queryController.UtilityGetAirportPickupOptions(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.UtilityGetAirportPickupOptionsRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilityGetAirportPickupOptionsRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Utility/GetAirportPickupOptions?media=xml
		/// </summary>
		public UtilityGetAirportPickupOptionsRS XmlUtilityGetAirportPickupOptions(UtilityGetAirportPickupOptionsRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				UtilityGetAirportPickupOptionsRS result = null;
				var queryController = new QueryController();

				result = queryController.UtilityGetAirportPickupOptions(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.UtilityGetAirportPickupOptionsRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilityGetAirportPickupOptionsRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

        #region UtilityGetCustomerInfo
        #region Json
        /// <summary>
        /// URI: http://localhost:52542/Rest/Utility/GetCustomerInfo
        /// </summary>
        public UtilityCustomerInfoRS JsonUtilityGetCustomerInfo(UtilityCustomerInfoRQ input)
        {
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
                #endregion

                var queryController = new QueryController();
                var result = queryController.UtilityGetCustomerInfo(input);

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
                #endregion

                return result;
            }
            catch (OTA_Exception ex)
            {
                var response = new UDI.VTOD.Common.DTO.OTA.UtilityCustomerInfoRS();
                response.EchoToken = input.EchoToken;
                response.Target = input.Target;
                response.PrimaryLangID = input.PrimaryLangID;
                response.Errors = new ErrorList();
                response.Errors.Errors = ex.Errors;
                response.Format = WebContentFormat.Json;
                if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
                {
                    response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }

                var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilityCustomerInfoRS>(response);
                throw faultException;
            }
        }
        #endregion

        #region Xml
        /// <summary>
        /// URI: http://localhost:52542/Rest/Utility/GetCustomerInfo?media=xml
        /// </summary>        
        /// <returns></returns>
        public UtilityCustomerInfoRS XmlUtilityGetCustomerInfo(UtilityCustomerInfoRQ input)
        {
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
                #endregion

                var queryController = new QueryController();

                var result = queryController.UtilityGetCustomerInfo(input);

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
                #endregion

                return result;
            }
            catch (OTA_Exception ex)
            {
                var response = new UDI.VTOD.Common.DTO.OTA.UtilityCustomerInfoRS();
                response.EchoToken = input.EchoToken;
                response.Target = input.Target;
                response.PrimaryLangID = input.PrimaryLangID;
                response.Errors = new ErrorList();
                response.Errors.Errors = ex.Errors;
                response.Format = WebContentFormat.Xml;
                if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
                {
                    response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }

                var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilityCustomerInfoRS>(response);
                throw faultException;
            }
        }
        #endregion
        #endregion

        #region UtilityGetPickUpAddress
        #region Json
        /// <summary>
        /// URI: http://localhost:52542/Rest/Utility/GetPickUpAddress
        /// </summary>
        
        /// <returns></returns>
        public UtilityGetPickupAddressRS JsonUtilityGetPickUpAddress(UtilityGetPickupAddressRQ input)
        {
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
                #endregion

                var queryController = new QueryController();
                var result = queryController.UtilityGetPickUpAddress(input);

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
                #endregion

                return result;
            }
            catch (OTA_Exception ex)
            {
                var response = new UDI.VTOD.Common.DTO.OTA.UtilityGetPickupAddressRS();
                response.EchoToken = input.EchoToken;
                response.Target = input.Target;
                response.PrimaryLangID = input.PrimaryLangID;
                response.Errors = new ErrorList();
                response.Errors.Errors = ex.Errors;
                response.Format = WebContentFormat.Json;
                if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
                {
                    response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }

                var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilityGetPickupAddressRS>(response);
                throw faultException;
            }
        }
        #endregion

        #region Xml
        /// <summary>
        /// URI: http://localhost:52542/Rest/Utility/GetPickUpAddress?media=xml
        /// </summary>
        
        /// <returns></returns>
        public UtilityGetPickupAddressRS XmlUtilityGetPickUpAddress(UtilityGetPickupAddressRQ input)
        {
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
                #endregion

                var queryController = new QueryController();

                var result = queryController.UtilityGetPickUpAddress(input);

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
                #endregion

                return result;
            }
            catch (OTA_Exception ex)
            {
                var response = new UDI.VTOD.Common.DTO.OTA.UtilityGetPickupAddressRS();
                response.EchoToken = input.EchoToken;
                response.Target = input.Target;
                response.PrimaryLangID = input.PrimaryLangID;
                response.Errors = new ErrorList();
                response.Errors.Errors = ex.Errors;
                response.Format = WebContentFormat.Xml;
                if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
                {
                    response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }

                var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilityGetPickupAddressRS>(response);
                throw faultException;
            }
        }
        #endregion
        #endregion


        #region UtilityGetLastTrip
        #region Json
        /// <summary>
        /// URI: http://localhost:52542/Rest/Utility/GetLastTrip
        /// </summary>
        
        /// <returns></returns>
        public UtilityGetLastTripRS JsonUtilityGetLastTrip(UtilityGetLastTripRQ input)
        {
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
                #endregion

                var queryController = new QueryController();
                var result = queryController.UtilityGetLastTrip(input);

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
                #endregion

                return result;
            }
            catch (OTA_Exception ex)
            {
                var response = new UDI.VTOD.Common.DTO.OTA.UtilityGetLastTripRS();
                response.EchoToken = input.EchoToken;
                response.Target = input.Target;
                response.PrimaryLangID = input.PrimaryLangID;
                response.Errors = new ErrorList();
                response.Errors.Errors = ex.Errors;
                response.Format = WebContentFormat.Json;
                if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
                {
                    response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }

                var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilityGetLastTripRS>(response);
                throw faultException;
            }
        }
        #endregion

        #region Xml
        /// <summary>
        /// URI: http://localhost:52542/Rest/Utility/GetLastTrip?media=xml
        /// </summary>
        
        /// <returns></returns>
        public UtilityGetLastTripRS XmlUtilityGetLastTrip(UtilityGetLastTripRQ input)
        {
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
                #endregion

                var queryController = new QueryController();

                var result = queryController.UtilityGetLastTrip(input);

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
                #endregion

                return result;
            }
            catch (OTA_Exception ex)
            {
                var response = new UDI.VTOD.Common.DTO.OTA.UtilityGetLastTripRS();
                response.EchoToken = input.EchoToken;
                response.Target = input.Target;
                response.PrimaryLangID = input.PrimaryLangID;
                response.Errors = new ErrorList();
                response.Errors.Errors = ex.Errors;
                response.Format = WebContentFormat.Xml;
                if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
                {
                    response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }

                var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilityGetLastTripRS>(response);
                throw faultException;
            }
        }
        #endregion
        #endregion

       

        #region GetBlackoutPeriod
        #region Json
        /// <summary>
        /// URI: http://localhost:52542/Rest/Utility/GetBlackoutPeriod
        /// </summary>
        
        /// <returns></returns>
        public UtilityGetBlackoutPeriodRS JsonUtilityGetBlackoutPeriod(UtilityGetBlackoutPeriodRQ input)
        {
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
                #endregion

                var queryController = new QueryController();
                var result = queryController.UtilityGetBlackoutPeriod(input);

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
                #endregion

                return result;
            }
            catch (OTA_Exception ex)
            {
                var response = new UDI.VTOD.Common.DTO.OTA.UtilityGetBlackoutPeriodRS();
                response.EchoToken = input.EchoToken;
                response.Target = input.Target;
                response.PrimaryLangID = input.PrimaryLangID;
                response.Errors = new ErrorList();
                response.Errors.Errors = ex.Errors;
                response.Format = WebContentFormat.Json;
                if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
                {
                    response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }

                var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilityGetBlackoutPeriodRS>(response);
                throw faultException;
            }
        }
        #endregion
        #region Xml
        /// <summary>
        /// URI: http://localhost:52542/Rest/Utility/GetBlackoutPeriod?media=xml
        /// </summary>
        
        /// <returns></returns>
        public UtilityGetBlackoutPeriodRS XmlUtilityGetBlackoutPeriod(UtilityGetBlackoutPeriodRQ input)
        {
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
                #endregion

                var queryController = new QueryController();

                var result = queryController.UtilityGetBlackoutPeriod(input);

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
                #endregion

                return result;
            }
            catch (OTA_Exception ex)
            {
                var response = new UDI.VTOD.Common.DTO.OTA.UtilityGetBlackoutPeriodRS();
                response.EchoToken = input.EchoToken;
                response.Target = input.Target;
                response.PrimaryLangID = input.PrimaryLangID;
                response.Errors = new ErrorList();
                response.Errors.Errors = ex.Errors;
                response.Format = WebContentFormat.Xml;
                if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
                {
                    response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }

                var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilityGetBlackoutPeriodRS>(response);
                throw faultException;
            }
        }
        #endregion
        #endregion

        #region GetStaleNotification
        #region Json
        /// <summary>
        /// URI: http://localhost:52542/Rest/Utility/GetStaleNotification
        /// </summary>
        
        /// <returns></returns>
        public UtilityGetNotificationRS JsonUtilityGetStaleNotification(UtilityGetNotificationRQ input)
        {
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
                #endregion

                var queryController = new QueryController();
                var result = queryController.UtilityGetStaleNotification(input);

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
                #endregion

                return result;
            }
            catch (OTA_Exception ex)
            {
                var response = new UDI.VTOD.Common.DTO.OTA.UtilityGetNotificationRS();
                response.EchoToken = input.EchoToken;
                response.Target = input.Target;
                response.PrimaryLangID = input.PrimaryLangID;
                response.Errors = new ErrorList();
                response.Errors.Errors = ex.Errors;
                response.Format = WebContentFormat.Json;
                if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
                {
                    response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }

                var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilityGetNotificationRS>(response);
                throw faultException;
            }
        }
        #endregion
        #region Xml
        /// <summary>
        /// URI: http://localhost:52542/Rest/Utility/GetStaleNotification?media=xml
        /// </summary>
        
        /// <returns></returns>
        public UtilityGetNotificationRS XmlUtilityGetStaleNotification(UtilityGetNotificationRQ input)
        {
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
                #endregion

                var queryController = new QueryController();

                var result = queryController.UtilityGetStaleNotification(input);

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
                #endregion

                return result;
            }
            catch (OTA_Exception ex)
            {
                var response = new UDI.VTOD.Common.DTO.OTA.UtilityGetNotificationRS();
                response.EchoToken = input.EchoToken;
                response.Target = input.Target;
                response.PrimaryLangID = input.PrimaryLangID;
                response.Errors = new ErrorList();
                response.Errors.Errors = ex.Errors;
                response.Format = WebContentFormat.Xml;
                if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
                {
                    response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }

                var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilityGetNotificationRS>(response);
                throw faultException;
            }
        }
        #endregion
        #endregion

        #region GetArrivalNotification
        #region Json
        /// <summary>
        /// URI: http://localhost:52542/Rest/Utility/GetArrivalNotification
        /// </summary>
        
        /// <returns></returns>
        public UtilityGetNotificationRS JsonUtilityGetArrivalNotification(UtilityGetNotificationRQ input)
        {
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
                #endregion

                var queryController = new QueryController();
                var result = queryController.UtilityGetArrivalNotification(input);

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
                #endregion

                return result;
            }
            catch (OTA_Exception ex)
            {
                var response = new UDI.VTOD.Common.DTO.OTA.UtilityGetNotificationRS();
                response.EchoToken = input.EchoToken;
                response.Target = input.Target;
                response.PrimaryLangID = input.PrimaryLangID;
                response.Errors = new ErrorList();
                response.Errors.Errors = ex.Errors;
                response.Format = WebContentFormat.Json;
                if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
                {
                    response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }

                var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilityGetNotificationRS>(response);
                throw faultException;
            }
        }
        #endregion
        #region Xml
        /// <summary>
        /// URI: http://localhost:52542/Rest/Utility/GetArrivalNotification?media=xml
        /// </summary>
        
        /// <returns></returns>
        public UtilityGetNotificationRS XmlUtilityGetArrivalNotification(UtilityGetNotificationRQ input)
        {
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
                #endregion

                var queryController = new QueryController();

                var result = queryController.UtilityGetArrivalNotification(input);

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
                #endregion

                return result;
            }
            catch (OTA_Exception ex)
            {
                var response = new UDI.VTOD.Common.DTO.OTA.UtilityGetNotificationRS();
                response.EchoToken = input.EchoToken;
                response.Target = input.Target;
                response.PrimaryLangID = input.PrimaryLangID;
                response.Errors = new ErrorList();
                response.Errors.Errors = ex.Errors;
                response.Format = WebContentFormat.Xml;
                if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
                {
                    response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }

                var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilityGetNotificationRS>(response);
                throw faultException;
            }
        }
        #endregion
        #endregion

        #region GetNightBeforeReminder
        #region Json
        /// <summary>
        /// URI: http://localhost:52542/Rest/Utility/GetNightBeforeRememinder
        /// </summary>
        
        /// <returns></returns>
        public UtilityGetNotificationRS JsonUtilityGetNightBeforeReminder(UtilityGetNotificationRQ input)
        {
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
                #endregion

                var queryController = new QueryController();
                var result = queryController.UtilityGetNightBeforeReminder(input);

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
                #endregion

                return result;
            }
            catch (OTA_Exception ex)
            {
                var response = new UDI.VTOD.Common.DTO.OTA.UtilityGetNotificationRS();
                response.EchoToken = input.EchoToken;
                response.Target = input.Target;
                response.PrimaryLangID = input.PrimaryLangID;
                response.Errors = new ErrorList();
                response.Errors.Errors = ex.Errors;
                response.Format = WebContentFormat.Json;
                if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
                {
                    response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }

                var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilityGetNotificationRS>(response);
                throw faultException;
            }
        }
        #endregion
        #region Xml
        /// <summary>
        /// URI: http://localhost:52542/Rest/Utility/GetNightBeforeReminder?media=xml
        /// </summary>
        
        /// <returns></returns>
        public UtilityGetNotificationRS XmlUtilityGetNightBeforeReminder(UtilityGetNotificationRQ input)
        {
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
                #endregion

                var queryController = new QueryController();

                var result = queryController.UtilityGetNightBeforeReminder(input);

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
                #endregion

                return result;
            }
            catch (OTA_Exception ex)
            {
                var response = new UDI.VTOD.Common.DTO.OTA.UtilityGetNotificationRS();
                response.EchoToken = input.EchoToken;
                response.Target = input.Target;
                response.PrimaryLangID = input.PrimaryLangID;
                response.Errors = new ErrorList();
                response.Errors.Errors = ex.Errors;
                response.Format = WebContentFormat.Xml;
                if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
                {
                    response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }

                var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.UtilityGetNotificationRS>(response);
                throw faultException;
            }
        }
        #endregion
        #endregion
        
        #endregion

     

        #region Reward
        #region RewardGetAirlineRewardsPrograms
        #region Json
        /// <summary>
		/// URI: http://localhost:52542/Rest/GetAirlineRewardsPrograms
		/// </summary>
		
		/// <returns></returns>
		public RewardGetAirlineProgramsRS JsonRewardGetAirlineRewardsPrograms(RewardGetAirlineProgramsRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.GetAirlineRewardsPrograms(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.RewardGetAirlineProgramsRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.RewardGetAirlineProgramsRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/GetAirlineRewardsPrograms?media=xml
		/// </summary>
		
		/// <returns></returns>
		public RewardGetAirlineProgramsRS XmlRewardGetAirlineRewardsPrograms(RewardGetAirlineProgramsRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();

				var result = queryController.GetAirlineRewardsPrograms(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.RewardGetAirlineProgramsRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.RewardGetAirlineProgramsRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region RewardCreateMemberAccount
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/CreateMembershipMileageAccount
		/// </summary>
		
		/// <returns></returns>
		public RewardCreateMemberAccountRS JsonRewardCreateMemberAccount(RewardCreateMemberAccountRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.RewardCreateMemberAccount(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.RewardCreateMemberAccountRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.RewardCreateMemberAccountRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/CreateMembershipMileageAccount?media=xml
		/// </summary>
		
		/// <returns></returns>
		public RewardCreateMemberAccountRS XmlRewardCreateMemberAccount(RewardCreateMemberAccountRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();

				var result = queryController.RewardCreateMemberAccount(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.RewardCreateMemberAccountRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.RewardCreateMemberAccountRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region DeleteMembershipMileageAccount
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/DeleteMembershipMileageAccount
		/// </summary>
		
		/// <returns></returns>
		public RewardDeleteMemberAccountRS JsonRewardDeleteMemberAccount(RewardDeleteMemberAccountRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.RewardDeleteMemberAccount(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.RewardDeleteMemberAccountRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.RewardDeleteMemberAccountRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/DeleteMembershipMileageAccount?media=xml
		/// </summary>
		
		/// <returns></returns>
		public RewardDeleteMemberAccountRS XmlRewardDeleteMemberAccount(RewardDeleteMemberAccountRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();

				var result = queryController.RewardDeleteMemberAccount(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.RewardDeleteMemberAccountRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.RewardDeleteMemberAccountRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region GetMembershipAirlineMileageAccounts
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/GetMembershipAirlineMileageAccounts
		/// </summary>
		
		/// <returns></returns>
		public RewardGetMemberAccountsRS JsonRewardGetMemberAccounts(RewardGetMemberAccountsRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.RewardGetMemberAccounts(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.RewardGetMemberAccountsRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.RewardGetMemberAccountsRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/GetMembershipAirlineMileageAccounts?media=xml
		/// </summary>
		
		/// <returns></returns>
		public RewardGetMemberAccountsRS XmlRewardGetMemberAccounts(RewardGetMemberAccountsRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();

				var result = queryController.RewardGetMemberAccounts(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.RewardGetMemberAccountsRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.RewardGetMemberAccountsRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region RewardSetDefaultAccount
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/GetMembershipAirlineMileageAccounts
		/// </summary>
		
		/// <returns></returns>
		public RewardSetDefaultAccountRS JsonRewardSetDefaultAccount(RewardSetDefaultAccountRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.RewardSetDefaultAccount(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.RewardSetDefaultAccountRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.RewardSetDefaultAccountRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/GetMembershipAirlineMileageAccounts?media=xml
		/// </summary>
		
		/// <returns></returns>
		public RewardSetDefaultAccountRS XmlRewardSetDefaultAccount(RewardSetDefaultAccountRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();

				var result = queryController.RewardSetDefaultAccount(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.RewardSetDefaultAccountRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.RewardSetDefaultAccountRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion


        #region RewardModifyMemberAccount
        #region Json
        /// <summary>
        /// URI: http://localhost:52542/Rest/ModifyMembershipMileageAccount
        /// </summary>
        
        /// <returns></returns>
        public RewardModifyMemberAccountRS JsonRewardModifyMemberAccount(RewardModifyMemberAccountRQ input)
        {
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
                #endregion

                var queryController = new QueryController();
                var result = queryController.RewardModifyMemberAccount(input);

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
                #endregion

                return result;
            }
            catch (OTA_Exception ex)
            {
                var response = new UDI.VTOD.Common.DTO.OTA.RewardModifyMemberAccountRS();
                response.EchoToken = input.EchoToken;
                response.Target = input.Target;
                response.PrimaryLangID = input.PrimaryLangID;
                response.Errors = new ErrorList();
                response.Errors.Errors = ex.Errors;
                response.Format = WebContentFormat.Json;
                if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
                {
                    response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }

                var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.RewardModifyMemberAccountRS>(response);
                throw faultException;
            }
        }
        #endregion

        #region Xml
        /// <summary>
        /// URI: http://localhost:52542/Rest/ModifyMembershipMileageAccount?media=xml
        /// </summary>
        
        /// <returns></returns>
        public RewardModifyMemberAccountRS XmlRewardModifyMemberAccount(RewardModifyMemberAccountRQ input)
        {
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
                #endregion

                var queryController = new QueryController();

                var result = queryController.RewardModifyMemberAccount(input);

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
                #endregion

                return result;
            }
            catch (OTA_Exception ex)
            {
                var response = new UDI.VTOD.Common.DTO.OTA.RewardModifyMemberAccountRS();
                response.EchoToken = input.EchoToken;
                response.Target = input.Target;
                response.PrimaryLangID = input.PrimaryLangID;
                response.Errors = new ErrorList();
                response.Errors.Errors = ex.Errors;
                response.Format = WebContentFormat.Xml;
                if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
                {
                    response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }

                var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.RewardModifyMemberAccountRS>(response);
                throw faultException;
            }
        }
        #endregion
        #endregion
		#endregion

		#region ZTripCredit
		#region ZTripCreditActivateMemberCredit
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/ActivateCredits
		/// </summary>
		
		/// <returns></returns>
		public ZTripCreditActivateMemberCreditRS JsonZTripCreditActivateMemberCredit(ZTripCreditActivateMemberCreditRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.ZTripCreditActivateMemberCredit(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.ZTripCreditActivateMemberCreditRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.ZTripCreditActivateMemberCreditRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/ActivateCredits?media=xml
		/// </summary>
		
		/// <returns></returns>
		public ZTripCreditActivateMemberCreditRS XmlZTripCreditActivateMemberCredit(ZTripCreditActivateMemberCreditRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();

				var result = queryController.ZTripCreditActivateMemberCredit(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.ZTripCreditActivateMemberCreditRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.ZTripCreditActivateMemberCreditRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region ZTripCreditGetMemberBalance
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/GetCreditsBalance
		/// </summary>
		
		/// <returns></returns>
		public ZTripCreditGetMemberBalanceRS JsonZTripCreditGetMemberBalance(ZTripCreditGetMemberBalanceRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.ZTripCreditGetMemberBalance(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.ZTripCreditGetMemberBalanceRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.ZTripCreditGetMemberBalanceRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/GetCreditsBalance?media=xml
		/// </summary>
		
		/// <returns></returns>
		public ZTripCreditGetMemberBalanceRS XmlZTripCreditGetMemberBalance(ZTripCreditGetMemberBalanceRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();

				var result = queryController.ZTripCreditGetMemberBalance(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.ZTripCreditGetMemberBalanceRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.ZTripCreditGetMemberBalanceRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#endregion

		#region Rate
		#region SetRate
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Ground/ResRetrieve
		/// </summary>
		
		/// <returns></returns>
		public RateSetRS JsonRateSet(RateSetRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.RateSet(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.RateSetRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.RateSetRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region XML
		/// <summary>
		/// URI: http://localhost:52542/Rest/Ground/ResRetrieve?media=xml
		/// </summary>
		
		/// <returns></returns>
		public RateSetRS XmlRateSet(RateSetRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.RateSet(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.RateSetRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.RateSetRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region GetRate
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Ground/ResRetrieve
		/// </summary>
		
		/// <returns></returns>
		public RateGetRS JsonRateGet(RateGetRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.RateGet(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.RateSetRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.RateSetRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region XML
		/// <summary>
		/// URI: http://localhost:52542/Rest/Ground/ResRetrieve?media=xml
		/// </summary>
		
		/// <returns></returns>
		public RateGetRS XmlRateGet(RateGetRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.RateGet(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.RateSetRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.RateSetRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#endregion

		#region Group
		#region ValidateDiscount
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Group/ValidateDiscountCode
		/// </summary>
		
		/// <returns></returns>
		public GroupValidateDiscountCodeRS JsonGroupValidateDiscountCode(GroupValidateDiscountCodeRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.GroupValidateDiscountCode(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.GroupValidateDiscountCodeRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.GroupValidateDiscountCodeRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Group/ValidateDiscountCode?media=xml
		/// </summary>
		
		/// <returns></returns>
		public GroupValidateDiscountCodeRS XmlGroupValidateDiscountCode(GroupValidateDiscountCodeRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();

				var result = queryController.GroupValidateDiscountCode(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.GroupValidateDiscountCodeRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.GroupValidateDiscountCodeRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region GetDiscount
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Group/GetDiscount
		/// </summary>
		
		/// <returns></returns>
		public GroupGetDiscountRS JsonGroupGetDiscount(GroupGetDiscountRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.GroupGetDiscount(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.GroupGetDiscountRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.GroupGetDiscountRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Group/GroupGetDiscount?media=xml
		/// </summary>
		
		/// <returns></returns>
		public GroupGetDiscountRS XmlGroupGetDiscount(GroupGetDiscountRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();

				var result = queryController.GroupGetDiscount(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.GroupGetDiscountRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.GroupGetDiscountRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region GetDiscount
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Group/GetPromotionType
		/// </summary>
		
		/// <returns></returns>
		public GroupGetPromotionTypeRS JsonGroupGetPromotionType(GroupGetPromotionTypeRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.GroupGetPromotionType(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.GroupGetPromotionTypeRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.GroupGetPromotionTypeRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Group/GetPromotionType?media=xml
		/// </summary>
		
		/// <returns></returns>
		public GroupGetPromotionTypeRS XmlGroupGetPromotionType(GroupGetPromotionTypeRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();

				var result = queryController.GroupGetPromotionType(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.GroupGetPromotionTypeRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.GroupGetPromotionTypeRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region  Group GetDefaults
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Group/GetDefaultsRQ
		/// </summary>
		
		/// <returns></returns>
		public GroupGetDefaultsRS JsonGroupGetDefaults(GroupGetDefaultsRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.GroupGetDefaults(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.GroupGetDefaultsRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.GroupGetDefaultsRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Group/GetDefaultsRQ?media=xml
		/// </summary>
		
		/// <returns></returns>
		public GroupGetDefaultsRS XmlGroupGetDefaults(GroupGetDefaultsRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();

				var result = queryController.GroupGetDefaults(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.GroupGetDefaultsRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.GroupGetDefaultsRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region  Group GetLandmarks
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Group/GetLandmarksRQ
		/// </summary>
		
		/// <returns></returns>
		public GroupGetLandmarksRS JsonGroupGetLandmarks(GroupGetLandmarksRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.GroupGetLandmarks(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.GroupGetLandmarksRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.GroupGetLandmarksRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Group/GetLandmarksRQ?media=xml
		/// </summary>
		
		/// <returns></returns>
		public GroupGetLandmarksRS XmlGroupGetLandmarks(GroupGetLandmarksRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();

				var result = queryController.GroupGetLandmarks(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.GroupGetLandmarksRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.GroupGetLandmarksRS>(response);
				throw faultException;
			}
		}
		#endregion
		#endregion

		#region  Group GetLocalizedServicedAirports
		#region Json
		/// <summary>
		/// URI: http://localhost:52542/Rest/Group/GetLandmarksRQ
		/// </summary>
		
		/// <returns></returns>
		public GroupGetLocalizedServicedAirportsRS JsonGroupGetLocalizedServicedAirports(GroupGetLocalizedServicedAirportsRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();
				var result = queryController.GroupGetLocalizedServicedAirports(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.GroupGetLocalizedServicedAirportsRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Json;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.GroupGetLocalizedServicedAirportsRS>(response);
				throw faultException;
			}
		}
		#endregion

		#region Xml
		/// <summary>
		/// URI: http://localhost:52542/Rest/Group/GetLandmarksRQ?media=xml
		/// </summary>
		
		/// <returns></returns>
		public GroupGetLocalizedServicedAirportsRS XmlGroupGetLocalizedServicedAirports(GroupGetLocalizedServicedAirportsRQ input)
		{
			try
			{
				#region Log
				var stopwatch = new Stopwatch();
				stopwatch.Start();
				logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
				#endregion

				var queryController = new QueryController();

				var result = queryController.GroupGetLocalizedServicedAirports(input);

				#region Log
				stopwatch.Stop();
				logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
				#endregion

				return result;
			}
			catch (OTA_Exception ex)
			{
				var response = new UDI.VTOD.Common.DTO.OTA.GroupGetLocalizedServicedAirportsRS();
				response.EchoToken = input.EchoToken;
				response.Target = input.Target;
				response.PrimaryLangID = input.PrimaryLangID;
				response.Errors = new ErrorList();
				response.Errors.Errors = ex.Errors;
				response.Format = WebContentFormat.Xml;
				if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
				{
					response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
				}

				var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.GroupGetLocalizedServicedAirportsRS>(response);
				throw faultException;
			}
		}
        #endregion
        #endregion
        #endregion

        #region Split Payment
        
        #region SplitPaymentInvite
        #region Json
        /// <summary>
        /// URI: http://localhost:52542/Rest/SplitPayment/Invite
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public SplitPaymentInviteRS JsonSplitPaymentInvite(SplitPaymentInviteRQ input)
        {
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
                #endregion

                var queryController = new QueryController();
                var result = queryController.SplitPaymentInvite(input);

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
                #endregion

                return result;
            }
            catch (OTA_Exception ex)
            {
                var response = new SplitPaymentInviteRS();
                response.EchoToken = input.EchoToken;
                response.Target = input.Target;
                response.PrimaryLangID = input.PrimaryLangID;
                response.Errors = new ErrorList();
                response.Errors.Errors = ex.Errors;
                response.Format = WebContentFormat.Json;
                if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
                {
                    response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }

                var faultException = new FaultException<SplitPaymentInviteRS>(response);
                throw faultException;
            }
        }
        #endregion

        #region Xml
        /// <summary>
        /// URI: http://localhost:52542/Rest/SplitPayment/Invite?media=xml
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public SplitPaymentInviteRS XmlSplitPaymentInvite(SplitPaymentInviteRQ input)
        {
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
                #endregion

                var queryController = new QueryController();
                var result = queryController.SplitPaymentInvite(input);

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
                #endregion

                return result;
            }
            catch (OTA_Exception ex)
            {
                var response = new SplitPaymentInviteRS();
                response.EchoToken = input.EchoToken;
                response.Target = input.Target;
                response.PrimaryLangID = input.PrimaryLangID;
                response.Errors = new ErrorList();
                response.Errors.Errors = ex.Errors;
                response.Format = WebContentFormat.Xml;
                if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
                {
                    response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }

                var faultException = new FaultException<SplitPaymentInviteRS>(response);
                throw faultException;
            }
        }
        #endregion
        #endregion

        #region SplitPaymentUpdateStatus
        #region Json
        /// <summary>
        /// URI: http://localhost:52542/Rest/SplitPayment/UpdateStatus
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public SplitPaymentUpdateStatusRS JsonSplitPaymentUpdateStatus(SplitPaymentUpdateStatusRQ input)
        {
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
                #endregion

                var queryController = new QueryController();
                var result = queryController.SplitPaymentUpdateStatus(input);

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
                #endregion

                return result;
            }
            catch (OTA_Exception ex)
            {
                var response = new SplitPaymentUpdateStatusRS();
                response.EchoToken = input.EchoToken;
                response.Target = input.Target;
                response.PrimaryLangID = input.PrimaryLangID;
                response.Errors = new ErrorList();
                response.Errors.Errors = ex.Errors;
                response.Format = WebContentFormat.Json;
                if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
                {
                    response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }

                var faultException = new FaultException<SplitPaymentUpdateStatusRS>(response);
                throw faultException;
            }
        }
        #endregion

        #region Xml
        /// <summary>
        /// URI: http://localhost:52542/Rest/SplitPayment/UpdateStatus?media=xml
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public SplitPaymentUpdateStatusRS XmlSplitPaymentUpdateStatus(SplitPaymentUpdateStatusRQ input)
        {
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
                #endregion

                var queryController = new QueryController();
                var result = queryController.SplitPaymentUpdateStatus(input);

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
                #endregion

                return result;
            }
            catch (OTA_Exception ex)
            {
                var response = new SplitPaymentUpdateStatusRS();
                response.EchoToken = input.EchoToken;
                response.Target = input.Target;
                response.PrimaryLangID = input.PrimaryLangID;
                response.Errors = new ErrorList();
                response.Errors.Errors = ex.Errors;
                response.Format = WebContentFormat.Xml;
                if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
                {
                    response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }

                var faultException = new FaultException<SplitPaymentUpdateStatusRS>(response);
                throw faultException;
            }
        }
        #endregion
        #endregion

        #region SplitPaymentGetInvitations
        #region Json
        /// <summary>
        /// URI: http://localhost:52542/Rest/SplitPayment/GetInvitations
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public SplitPaymentGetInvitationsRS JsonSplitPaymentGetInvitations(SplitPaymentGetInvitationsRQ input)
        {
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
                #endregion

                var queryController = new QueryController();
                var result = queryController.SplitPaymentGetInvitations(input);

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
                #endregion

                return result;
            }
            catch (OTA_Exception ex)
            {
                var response = new SplitPaymentGetInvitationsRS();
                response.EchoToken = input.EchoToken;
                response.Target = input.Target;
                response.PrimaryLangID = input.PrimaryLangID;
                response.Errors = new ErrorList();
                response.Errors.Errors = ex.Errors;
                response.Format = WebContentFormat.Json;
                if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
                {
                    response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }

                var faultException = new FaultException<SplitPaymentGetInvitationsRS>(response);
                throw faultException;
            }
        }
        #endregion

        #region Xml
        /// <summary>
        /// URI: http://localhost:52542/Rest/SplitPayment/GetInvitations?media=xml
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public SplitPaymentGetInvitationsRS XmlSplitPaymentGetInvitations(SplitPaymentGetInvitationsRQ input)
        {
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
                #endregion

                var queryController = new QueryController();
                var result = queryController.SplitPaymentGetInvitations(input);

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
                #endregion

                return result;
            }
            catch (OTA_Exception ex)
            {
                var response = new SplitPaymentGetInvitationsRS();
                response.EchoToken = input.EchoToken;
                response.Target = input.Target;
                response.PrimaryLangID = input.PrimaryLangID;
                response.Errors = new ErrorList();
                response.Errors.Errors = ex.Errors;
                response.Format = WebContentFormat.Xml;
                if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
                {
                    response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }

                var faultException = new FaultException<SplitPaymentGetInvitationsRS>(response);
                throw faultException;
            }
        }
        #endregion
        #endregion

        #region SplitPaymentCheckIfEnabled
        #region Json
        /// <summary>
        /// URI: http://localhost:52542/Rest/SplitPayment/CheckIfEnabled
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public SplitPaymentCheckIfEnabledRS JsonSplitPaymentCheckIfEnabled(SplitPaymentCheckIfEnabledRQ input)
        {
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
                #endregion

                var queryController = new QueryController();
                var result = queryController.SplitPaymentCheckIfEnabled(input);

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
                #endregion

                return result;
            }
            catch (OTA_Exception ex)
            {
                var response = new SplitPaymentCheckIfEnabledRS();
                response.EchoToken = input.EchoToken;
                response.Target = input.Target;
                response.PrimaryLangID = input.PrimaryLangID;
                response.Errors = new ErrorList();
                response.Errors.Errors = ex.Errors;
                response.Format = WebContentFormat.Json;
                if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
                {
                    response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }

                var faultException = new FaultException<SplitPaymentCheckIfEnabledRS>(response);
                throw faultException;
            }
        }
        #endregion

        #region Xml
        /// <summary>
        /// URI: http://localhost:52542/Rest/SplitPayment/CheckIfEnabled?media=xml
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public SplitPaymentCheckIfEnabledRS XmlSplitPaymentCheckIfEnabled(SplitPaymentCheckIfEnabledRQ input)
        {
            try
            {
                #region Log
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                logger.InfoFormat("Start the Process: input:{0}", stopwatch.ElapsedMilliseconds);
                #endregion

                var queryController = new QueryController();
                var result = queryController.SplitPaymentCheckIfEnabled(input);

                #region Log
                stopwatch.Stop();
                logger.InfoFormat("End the Process: {0}", stopwatch.ElapsedMilliseconds);
                #endregion

                return result;
            }
            catch (OTA_Exception ex)
            {
                var response = new SplitPaymentCheckIfEnabledRS();
                response.EchoToken = input.EchoToken;
                response.Target = input.Target;
                response.PrimaryLangID = input.PrimaryLangID;
                response.Errors = new ErrorList();
                response.Errors.Errors = ex.Errors;
                response.Format = WebContentFormat.Xml;
                if (ex.GetType() == typeof(OTA_Exception_Unauthorized))
                {
                    response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                }

                var faultException = new FaultException<SplitPaymentCheckIfEnabledRS>(response);
                throw faultException;
            }
        }
        #endregion
        #endregion

        #endregion
    }
}
