﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;
using UDI.VTOD.Common.DTO.OTA;
using UDI.Utility.Serialization;
using System.Xml;
using UDI.VTOD.Common.DTO;

namespace UDI.VTOD.WebService.ServiceModel
{
	public class ErrorHandler : BehaviorExtensionElement, IErrorHandler, IServiceBehavior
	{
		public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters)
		{
		}

		public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
		{
			IErrorHandler errorHandler = new ErrorHandler();

			foreach (ChannelDispatcher channelDispatcher in serviceHostBase.ChannelDispatchers)
			{
				//ChannelDispatcher channelDispatcher = channelDispatcherBase as ChannelDispatcher;

				//if (channelDispatcher != null)
				//{
				channelDispatcher.ErrorHandlers.Add(errorHandler);
				//}
			}
		}

		public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
		{
		}

		public bool HandleError(Exception error)
		{
			Trace.TraceError(error.ToString());

			// Returning true indicates you performed your behavior.
			return true;
		}

		public void ProvideFault(Exception error, MessageVersion version, ref Message message)
		{
			string contextCode = null;
			string contextLanguageCode = "en-US";
			string username = null;
			#region Get Header
			var requestMessages = OperationContext.Current.IncomingMessageProperties[HttpRequestMessageProperty.Name] as HttpRequestMessageProperty;

			try
			{
				if (requestMessages.Headers["ContextCode"] != null)
					contextCode = requestMessages.Headers["ContextCode"];
				if (requestMessages.Headers["ContextLanguageCode"] != null)
					contextLanguageCode = requestMessages.Headers["ContextLanguageCode"];
				if (requestMessages.Headers["Username"] != null)
					username = requestMessages.Headers["Username"];
			}
			catch { }
			#endregion

			var format = WebContentFormat.Xml;
			var statusCode = System.Net.HttpStatusCode.BadRequest;
			if (error is FaultException)
			{
				var detail = error.GetType().GetProperty("Detail").GetGetMethod().Invoke(error, null);
				var fault = (Fault)(detail);
				format = fault.Format;
				if (fault.StatusCode != 0)
				{
					statusCode = fault.StatusCode;
				}
				switch (format)
				{
					case WebContentFormat.Json:
						message = Message.CreateMessage(version, "", detail, new DataContractJsonSerializer(detail.GetType()));
						break;
					default:
						message = Message.CreateMessage(version, "", detail, new XmlMessagingSerializer(detail.GetType()));
						break;
				}
			}
			else
			{
				message = Message.CreateMessage(version, "", error.Message, new DataContractJsonSerializer(typeof(string)));
			}

			var wbf = new WebBodyFormatMessageProperty(format);
			message.Properties.Add(WebBodyFormatMessageProperty.Name, wbf);

			var rmp = new HttpResponseMessageProperty();
			rmp.StatusCode = statusCode;
			rmp.StatusDescription = "See fault object for more information.";
			#region Add Header
			if (!string.IsNullOrWhiteSpace(contextCode) && !string.IsNullOrWhiteSpace(username))
			{
				try
				{
					string contextMessage = null;
					#region Get Message from DB
					var queryController = new UDI.VTOD.Controller.QueryController();
					contextMessage = queryController.GetContextErrorCode(username, contextCode, contextLanguageCode);
					#endregion

					if (!string.IsNullOrWhiteSpace(contextMessage))
					{
						rmp.Headers.Add("ContextMessage", contextMessage);
					}
				}
				catch
				{ }
			}

			#endregion
			message.Properties.Add(HttpResponseMessageProperty.Name, rmp);
		}

		public override Type BehaviorType
		{
			get { return this.GetType(); }
		}

		protected override object CreateBehavior()
		{
			return this;
		}
	}
}
