﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Configuration;
using System.Text;

namespace UDI.VTOD.WebService.ServiceModel
{
	public class RestWebHttpBehaviorExtention : BehaviorExtensionElement
	{
		public override Type BehaviorType
		{
			get { return typeof(RestWebHttpBehavior); }
		}

		protected override object CreateBehavior()
		{
			return new RestWebHttpBehavior();
		}
	}
}
