﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using UDI.VTOD.Common.DTO;

namespace UDI.VTOD.WebService
{
	public class SOAP : Contract.ISOAP
	{
		public Ping Ping()
		{
			return UDI.VTOD.Controller.QueryController.Ping();
		}
	}
}
