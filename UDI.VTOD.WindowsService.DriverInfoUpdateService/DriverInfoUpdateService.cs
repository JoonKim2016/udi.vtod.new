﻿using System;
using System.ServiceProcess;
using System.Timers;
using log4net;
using UDI.VTOD.Controller;

namespace UDI.VTOD.WindowsService.DriverInfoUpdateService
{
    public partial class DriverInfoUpdateService : ServiceBase
    {
        #region Fields
		private Timer timer = new Timer();
		static ILog log;
		static bool isRunning;
		//static int test = 0;
		#endregion

		#region Console
		static void Main(string[] args)
		{
			var service = new DriverInfoUpdateService();

			if (Environment.UserInteractive)
			{
				service.OnStart(args);
				Console.WriteLine("Press any key to stop program");
				Console.Read();
				service.OnStop();
			}
			else
			{
				Run(service);
			}

		}
		#endregion

		#region Constructors
        public DriverInfoUpdateService()
		{
			isRunning = false;
			InitializeComponent();
			log4net.Config.XmlConfigurator.Configure();
			log = LogManager.GetLogger(typeof(DriverInfoUpdateService));

			#region EventLog
			if (!System.Diagnostics.EventLog.SourceExists("VTODTStatus"))
			{
				System.Diagnostics.EventLog.CreateEventSource(
					"VTODTStatus", "VTODTStatus");
			}
			eventLog.Source = "VTODTStatus";
			eventLog.Log = "VTODTStatus";
			#endregion
		}
		#endregion

		#region Event
		protected override void OnStart(string[] args)
		{
            eventLog.WriteEntry(DateTime.Now.ToString() + " DriverInfoUpdateService has been started...");
            log.Info(DateTime.Now.ToString() + " DriverInfoUpdateService has been started...");
			timer.AutoReset = true;
			timer.Interval = 5;
			timer.Elapsed += OnElapsedEvent;
			timer.Start();
		}

		protected override void OnStop()
		{
			timer.Stop();
            eventLog.WriteEntry(DateTime.Now.ToString() + " DriverInfoUpdateService has been stopped...");
            log.Info(DateTime.Now.ToString() + " DriverInfoUpdateService has been Stopped...");
		}
		#endregion

		#region Timer function
		static void OnElapsedEvent(object sender, ElapsedEventArgs e)
		{

			#region Set interval from app.config
			if (((System.Timers.Timer)sender).Interval == 5)
			{
				var interval = 10000;

				try
				{
					interval = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["Interval"]);
					((System.Timers.Timer)sender).Interval = interval;
					Console.WriteLine("timer.Interval = " + interval.ToString());
				}
				catch (Exception)
				{
				}
			}
			#endregion

			if (!isRunning)
			{
				isRunning = true;

				var queryController = new QueryController();
                 queryController.ProcessDriverInfo();

				isRunning = false;
			}
		}
		#endregion
    }
}
