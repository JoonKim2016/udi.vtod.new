﻿# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary

This service was created as a workaround for not being allowed to install  
SFTP file transferring tools that would allow us to export .csv files into ECar 
Charger's SFTP server via MSSQLS stored procedures.  This service runs every hour 
and looks for files in a specifc directory. If it finds files, it proceeds 
to upload them into ECar Charger's SFTP server.  If the file is  successfully uploaded,
it is archived locally.  The file directories and SFTP credentials are 
table-based and are in the dbo.ecarCashieringFeedConfiguration table.  
The core structure of the window service was previously implemented and we 
just added the logic to upload to SFTP server. We also want to note that  
the stored procedure dbo.GenerateECarCashieringFeedDataFile is responsible
for populating the local directory with files.  The stored procedure is called
by a job GenerateECarCashieringFeedDataFile which executes said stored procedure
on an hourly basis.

* Version
1.0.0.0

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

Local Setup
1. Clone the project on local machine.
2. Create local file directories directories.
	a) C:\Exports\ECarCharger\Archive
	b) C:\Exports\ECarCharger\TransportFiles
   These are the current values but they are table driven and can be found in 
   the dbo.ecarCashieringFeedConfiguration database table.
3. Check the ECar Charger SFTP server credentials.  They can be found in database table dbo.ecarCashieringFeedConfiguration.
3. You have to be on UDI - Transdev in order to access the vtod database which contains configuration values.
4. Run the GenerateECarCashieringFeedDataFile MSSQLS job in order to generate files.  You can also place dummy
   files in the appropriate pick up directories.  Keep in mind that the ECar Cashiering system
   will pick up files and process.

Production Setup
1. The window service is part of VTOD so you have to install VTOD.
2. Ensure the shared file directories are setup.
3. Ensure that the GenerateECarCashieringFeedDataFile MSSQLS job is enabled.

* Summary of set up

1. Install VTOD solution which contains window service.
2. Ensure the dbo.GenerateECarCashieringFeedDataFile MSSQLS job is enabled.


* Configuration

You should just be able to build solution successfully out of the box.  If not, please update the Nuget packages to download libraries.

* Dependencies

We are using a few Nuget libraries
1. Log4Net
2. SSH.NET
3. The dbo.ecarCashieringFeedConfiguration database table records.  We are mentioning this here because without valid data i.e.
   credentials and file directory the system will fail.


* Database configuration
1. The dbo.ecarCashieringFeedConfiguration database table records must contain valid credentials and accessible file directories.
2. You have to be connected to UDI - Transdev network.
3. The database is vtod.


* How to run tests

Ideally you would want to setup a test SFTP server.  We tested against ECar Charger's SFTP server before they turned on their
system that picks up the files.  Any future tests against this SFTP server may cause issues.  Also, the table dbo.ecarCashierFeedHistory
is used for keeping track of processed records.  This way we never create files with duplicate data.  We would want to rim out this
part of the stored procedure dbo.GenerateECarCashieringFeedDataFile so as to not disrupt production.

* Deployment instructions

Build vtod solution and publish to desired solution.  

### Contribution guidelines ###

* Writing tests

1. Setup the window service (ECarPushCashiering)
2. Enable the MSSQLS job GenerateECarCashieringFeedDataFile.  Modify the job to run every 5 minutes as oppose to every 60 minutes.
   In production it will run every 60 minutes.
3. Modify the stored procedure by dbo.GenerateECarCashieringFeedDataFile such that the insert into the dbo.ecarCashierFeedHistory
   query only inserts a single record.  This table keeps track of the records we have loaded.  If set it to only insert a single
   record the system will generate better sample files over an hour period. 
4. Book trips using the ZTrip app and complete them using the ecar's trip retrieval app.  This will create records for our system
   to process. Or simply use the existing records in the test database.  Or you can book trips with ZTrip app and then manually
   update the database records to 'Completed'.
5. Go to the Archive directory (currently C:\Exports\ECarCashiering\Archive) but check the dbo.ecarCashieringFeedConfiguration 
   table first.  
6. Make sure there are files in the Archive directory.
7. Go to the SFTP server and make sure there are files there.
8. Lastly make sure that the records in files are not duplicated.

* Test Cases
We created the following trips.  
1. Cash trips (Should show up.) - Done (as directed/with destination)
2. Credit card trips (Should show up.) - Done
3. Cancelled trips (Should not show up.) - Done
4. In-progress trips (Should not show up.) - Done
5. Later trips not completed. (Should not show up.) - Done
6. Trips with final fare less than or equal to 0.  (Should show up.) - Done
7. zTrip Credits (Full) - Done 
8. zTrip Credits (zTrip Credits + CC) - Done

* Code review

TODO

* Other guidelines

### Who do I talk to? ###

* Repo owner or admin

API Team

* Other community or team contact