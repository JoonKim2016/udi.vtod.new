﻿@ECHO OFF
 
echo Installing UDI.VTOD.WindowsService.EcarAccountingService ...
echo ---------------------------------------------------
set Path=C:\Windows\Microsoft.NET\Framework64\v4.0.30319
cd C:\Program Files\UDI\VTOD.EcarAccountingService
installutil /u /username=ssdmzvtodapi\UDIAdmin /password=DigiKey12 /unattended UDI.VTOD.WindowsService.EcarAccountingService.exe

echo ---------------------------------------------------
echo Done.
pause