﻿@ECHO OFF
 
echo Installing UDI.VTOD.WindowsService.EcarNotificationService ...
echo ---------------------------------------------------
set Path=C:\Windows\Microsoft.NET\Framework64\v4.0.30319
cd C:\Program Files\UDI\VTOD.EcarNotificationService
installutil /i /username=ssdmzvtodapi\UDIAdmin /password=DigiKey12 /unattended UDI.VTOD.WindowsService.EcarNotificationService.exe

echo ---------------------------------------------------
echo Done.
pause