﻿@ECHO OFF
 
echo Installing UDI.VTOD.WindowsService.EcarStatusService ...
echo ---------------------------------------------------
set Path=C:\Windows\Microsoft.NET\Framework64\v4.0.30319
cd C:\Program Files\UDI\VTOD.EcarStatusService
installutil /u /username=ssdmzvtodapi\UDIAdmin /password=DigiKey12 /unattended UDI.VTOD.WindowsService.EcarStatusService.exe

echo ---------------------------------------------------
echo Done.
pause