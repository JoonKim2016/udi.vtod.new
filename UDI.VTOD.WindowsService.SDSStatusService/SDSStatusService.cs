﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using UDI.VTOD;
using UDI.VTOD.Common;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Common.Track;
using UDI.VTOD.Controller;

namespace UDI.VTOD.WindowsService.SDSStatusService
{
	partial class SDSStatusService : ServiceBase
	{
		#region Fields
		private System.Timers.Timer timer = new System.Timers.Timer();
		static ILog log;
		static bool isRunning;
		//static int test = 0;
		#endregion

		#region Console
		static void Main(string[] args)
		{
			SDSStatusService service = new SDSStatusService();

			if (Environment.UserInteractive)
			{
				service.OnStart(args);
				Console.WriteLine("Press any key to stop program");
				Console.Read();
				service.OnStop();
			}
			else
			{
				ServiceBase.Run(service);
			}

		}
		#endregion

		#region Constructors
		public SDSStatusService()
		{
			isRunning = false;
			InitializeComponent();
			log4net.Config.XmlConfigurator.Configure();
			log = LogManager.GetLogger(typeof(SDSStatusService));

			#region EventLog
			if (!System.Diagnostics.EventLog.SourceExists("VTODSStatus"))
			{
				System.Diagnostics.EventLog.CreateEventSource(
					"VTODSStatus", "VTODSStatus");
			}
			eventLog.Source = "VTODSStatus";
			eventLog.Log = "VTODSStatus";
			#endregion
		}
		#endregion

		#region Event
		protected override void OnStart(string[] args)
		{
			eventLog.WriteEntry(DateTime.Now.ToString() + " SDSStatusService has been started...");
			//(DateTime.Now.ToString() + " SDSStatusService has been started...").WriteToConsole();
			log.Info(DateTime.Now.ToString() + " SDSStatusService has been started...");
			timer.AutoReset = true;
			timer.Interval = 5;
			timer.Elapsed += OnElapsedEvent;
			timer.Start();
		}

		protected override void OnStop()
		{
			timer.Stop();
			eventLog.WriteEntry(DateTime.Now.ToString() + " SDSStatusService has been stopped...");
			//(DateTime.Now.ToString() + " SDSStatusService has been stopped...").WriteToConsole();
			log.Info(DateTime.Now.ToString() + " SDSStatusService has been Stopped...");
		}
		#endregion

		#region Timer function
		static void OnElapsedEvent(object sender, ElapsedEventArgs e)
		{

			#region Set interval from app.config
			if (((System.Timers.Timer)sender).Interval == 5)
			{
				var interval = 10000;

				try
				{
					interval = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["Interval"]);
					((System.Timers.Timer)sender).Interval = interval;
					Console.WriteLine("timer.Interval = " + interval.ToString());
				}
				catch (Exception)
				{
				}
			}
			#endregion

			if (!isRunning)
			{
				isRunning = true;

				var queryController = new QueryController();
				queryController.PullSDSStatus();

				#region Old
				/*
				#region Intit
				isRunning = true;
				var trackTime = new TrackTime(LogOwnerType.VTOD, Method.WindowsService_GetStatus);
				var vtodDomain = new UDI.VTOD.Domain.VTOD.VTODDomain();
				#endregion

				try
				{
					var sdsDomain = new UDI.VTOD.Domain.SDS.SDSDomain(); sdsDomain.TrackTime = trackTime;

					#region Read from Config
					var clientType = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SDS_TokenClientType"]);
					var clientIPAddress = System.Configuration.ConfigurationManager.AppSettings["SDS_TokenClientIPAddress"];
					var username = System.Configuration.ConfigurationManager.AppSettings["SDS_Username"];
					var password = System.Configuration.ConfigurationManager.AppSettings["SDS_Password"];
					var fleetType = "SuperShuttle_ExecuCar";
					#endregion

					#region Making TokenRQ
					var tokenRQ = new UDI.VTOD.Common.DTO.OTA.TokenRQ();
					tokenRQ.ClientType = clientType;
					tokenRQ.ClientIPAddress = clientIPAddress;
					tokenRQ.Username = username;
					tokenRQ.Password = password;
					tokenRQ.FleetType = fleetType;
					#endregion

					#region Get TokenRS
					var queryController = new UDI.VTOD.Controller.QueryController();
					var token = queryController.GetToken(tokenRQ);

					//Modify TokenRS
					token.ClientType = clientType;
					token.ClientIPAddress = clientIPAddress;
					#endregion

					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_WindowsService_SDSStatusService, "Get TokenRS");

					#region Get RezSource
					List<string> rezSources = sdsDomain.GetAllUserRezSources();

					#endregion

					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_WindowsService_SDSStatusService, "Get RezSource");

					#region Get Status
					foreach (var rezSource in rezSources)
					{
						sdsDomain.GetStatus(token, rezSource);
					}
					#endregion

					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_WindowsService_SDSStatusService, "Get Status");
				}
				catch (Exception ex)
				{
					//ex.Message.WriteToConsole();
					//ex.InnerException.WriteToConsole();

					log.Error(DateTime.Now.ToString() + " Error: ", ex);
				}
				finally
				{
					#region Log Reponse
					try
					{
						#region Track
						trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
						#endregion

						#region SaveTrackTime
						vtodDomain.InsertTrack(trackTime);
						trackTime.Dispose();
						#endregion
					}
					catch (Exception ex)
					{
						log.Error(ex);
					}
					#endregion
				} 
				*/
				#endregion

				isRunning = false;
			}
		}
		#endregion
	}
}
