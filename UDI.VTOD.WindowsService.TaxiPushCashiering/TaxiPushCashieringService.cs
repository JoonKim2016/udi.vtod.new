﻿using System;
using System.ServiceProcess;
using System.Timers;
using log4net;
using UDI.VTOD.Controller;

namespace UDI.VTOD.WindowsService.TaxiPushCashieringService
{

    //========================================================================================
    // This service was created as a workaround for not being allowed to install  
    // SFTP file transferring tools that would allow us to export .csv files into Taxi 
    // Charger's SFTP server.  This service runs every hour and looks for files in a specifc
    // directory based on table-based values.  If it finds files, it proceeds to upload into
    // Taxi Charger's SFTP server.  The core structure of the window service was previously
    // implemented and we just added the logic to upload to SFTP server.
    //========================================================================================

    public partial class TaxiPushCashieringService : ServiceBase
    {
        #region Fields
		private Timer timer = new Timer();
		static ILog log;
		static bool isRunning;
		//static int test = 0;
		#endregion

		#region Console
		static void Main(string[] args)
		{
			var service = new TaxiPushCashieringService();

			if (Environment.UserInteractive)
			{
				service.OnStart(args);
				Console.WriteLine("Press any key to stop program");
				Console.Read();
				service.OnStop();
			}
			else
			{
				Run(service);
			}

		}
		#endregion

		#region Constructors
        public TaxiPushCashieringService()
		{
			isRunning = false;
			InitializeComponent();
			log4net.Config.XmlConfigurator.Configure();
			log = LogManager.GetLogger(typeof(TaxiPushCashieringService));

			#region EventLog
			if (!System.Diagnostics.EventLog.SourceExists("VTODTStatus"))
			{
				System.Diagnostics.EventLog.CreateEventSource(
					"VTODTStatus", "VTODTStatus");
			}
			eventLog.Source = "VTODTStatus";
			eventLog.Log = "VTODTStatus";
			#endregion
		}
		#endregion

		#region Event
		protected override void OnStart(string[] args)
		{
            eventLog.WriteEntry(DateTime.Now.ToString() + " TaxiPushCashieringService has been started...");
            log.Info(DateTime.Now.ToString() + " TaxiPushCashieringService has been started...");
			timer.AutoReset = true;
			timer.Interval = 5;
			timer.Elapsed += OnElapsedEvent;
			timer.Start();
		}

		protected override void OnStop()
		{
			timer.Stop();
            eventLog.WriteEntry(DateTime.Now.ToString() + " TaxiPushCashieringService has been stopped...");
            log.Info(DateTime.Now.ToString() + " TaxiPushCashieringService has been Stopped...");
		}
		#endregion

		#region Timer function
		static void OnElapsedEvent(object sender, ElapsedEventArgs e)
		{

			#region Set interval from app.config
			if (((System.Timers.Timer)sender).Interval == 5)
			{
				var interval = 10000;

				try
				{
					interval = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["Interval"]);
					((System.Timers.Timer)sender).Interval = interval;
					Console.WriteLine("timer.Interval = " + interval.ToString());
				}
				catch (Exception)
				{
				}
			}
			#endregion

			if (!isRunning)
			{
				isRunning = true;

				var queryController = new QueryController();
                 queryController.TaxiPushCashiering();

				isRunning = false;
			}
		}
        #endregion

        private void eventLog_EntryWritten(object sender, System.Diagnostics.EntryWrittenEventArgs e)
        {

        }
    }
}
