﻿Taxi Reservation Confirmation {ProviderTripID}##<style type="text/css">
	body, tr
	{
		font-family: Arial;
		font-size: 12px;
		color: #565252;
	}

	.titleHead
	{
		font-size: 13px;
		color: #063b7f;
	}

	.smallFont
	{
		font-size: 11px;
		color: #565252;
	}

	.smallFont_white
	{
		font-size: 11px;
		color: #FFFFFF;
	}

	.auto-style1
	{
		height: 70px;
	}
</style>
<table width="70%" border="0" cellspacing="0px" cellpadding="0px" align="center" style="border: 1px solid #e0e0e1">
	<tr>
		<td valign="top">
			<table width="100%" border="0" cellspacing="0px" cellpadding="3px">
				<tr>
					<td valign="top" class="auto-style1">
						<!--Header table start here -->
						<table align="center" width="90%" border="0" cellspacing="0px" cellpadding="0px">
							<tr>
								<td>
									<img src="http://api.vtod.com/utilities/Images/Logo_zTrip.png" alt="Logo_zTrip" /></td>
							</tr>
						</table>
						<!--Header table end here -->
					</td>
				</tr>
				<tr>
					<td valign="top" align="center">
						<!--Midddle table start here -->
						<table align="center" width="95%" border="0" cellspacing="0px" cellpadding="0px">
							<tr>
								<td valign="top">
									<table align="center" width="100%" border="0" cellspacing="0px" cellpadding="2px">
										<tr>
											<td align="left"><strong>Dear</strong> <strong style="color: #0128a2">{FirstName} {LastName}</strong>,<br>
												The following information summarizes your confirmed service with zTrip.<br>
											</td>
										</tr>
										<tr height="5px">
											<td></td>
										</tr>
										<tr bgcolor="#ececec">
											<td align="left" style="border-bottom: solid 1px #dcdee5"><strong class="titleHead">Itinerary:</strong></td>
										</tr>
										<tr>
											<td valign="top">
												<!--details table start here -->
												<table width="100%" border="0" cellspacing="0px" cellpadding="0px">
													<tr>
														<td align="left" width="23%"><strong>Confirmation Number:</strong></td>
														<td align="left"><strong style="color: #0128a2">{ProviderTripID}</strong></td>
													</tr>
													<tr>
														<td align="left"><strong>Pickup Date/Time:</strong></td>
														<td align="left"><strong style="color: #0128a2">{PUDateTime}</strong></td>
													</tr>
													<tr>
														<td align="left"><strong>Pickup Location:</strong></td>
														<td align="left"><strong style="color: #0128a2">{PUAddress}</strong></td>
													</tr>
													<tr>
														<td align="left"><strong>Phone:</strong></td>
														<td align="left"><strong><span style="color: #0128a2">{MemberPhone}</strong></span></td>
													</tr>
													<tr>
														<td align="left"><strong>Dropoff Location:</strong></td>
														<td align="left"><strong style="color: #0128a2">{DOAddress}</strong></td>
													</tr>
													<tr>
														<td align="left"><strong>Service Type:</strong></td>
														<td align="left"><strong style="color: #0128a2">Taxi</strong></td>
													</tr>
													<tr>
														<td align="left"><strong>Fleet Name:</strong></td>
														<td align="left"><strong style="color: #0128a2">{FleetName}</strong></td>
													</tr>
													<tr>
														<td colspan="2" valign="top">
															<!--Fares Table start here -->
															<table width="80%" border="0" cellspacing="0px" cellpadding="1px" style="border: 1px solid #565656">
																<tr>
																	<td align="left">Fare</td>
																	<td align="left">{Fare}</td>
																</tr>
																<tr bgcolor="#f3f3f3">
																	<td align="left">Tip</td>
																	<td align="left">{Tip}</td>
																</tr>
																<tr bgcolor="#565656">
																	<td align="left" style="color: #ffffff"><strong>Total</strong></td>
																	<td align="left" style="color: #ffffff"><strong>{TotalFare}</strong></td>
																</tr>
															</table>
															<!--Fares Table end here -->
														</td>
													</tr>
													<tr>
														<td align="left"><strong>Payment Method:</strong></td>
														<td align="left"><strong style="color: #0128a2">PREPAID CREDIT CARD</strong></td>
													</tr>
													<tr>
														<td align="left"><strong>Card Type</strong></td>
														<td align="left"><strong style="color: #0128a2">{CardType}</strong></td>
													</tr>
													<tr>
														<td align="left"><strong>Card Number</strong></td>
														<td align="left"><strong style="color: #0128a2">{CardNumber}</strong></td>
													</tr>
												</table>
												<!--details table end here -->
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--Middle table end here -->
					</td>
				</tr>
				<tr height="60px">
					<td valign="top">
						<!--Footer table start here -->
						<table width="100%" border="0" cellspacing="0px" cellpadding="0px">
							<tr height="2px" bgcolor="#fdc502">
								<td></td>
							</tr>
							<tr height="1px">
								<td height="1px"></td>
							</tr>
							<tr height="57px" bgcolor="#555555">
								<td align="center" style="color: #FFFFFF" class="smallFont_white"><span style="color: rgb(255, 255, 255); font-family: Arial; font-size: 11px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: -webkit-center; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(85, 85, 85); display: inline !important; float: none;">To change or cancel this reservation, please call {FleetPhone}</span><br />
									To view our TERMS OF USE, click <u><a href="http://www.ztrip.com/Terms.aspx" style="color: #FFFFFF">here</a></u><br>
									<strong>Thank you for choosing zTrip!</strong><br>
									<u><a href="http://www.ztrip.com" class="smallFont_white">http://www.ztrip.com</a></u><br>
								</td>
							</tr>
						</table>
						<!--Footer table end here -->
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
