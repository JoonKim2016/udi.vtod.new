﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using log4net;
//using UDI.Utility.Helper;
using UDI.VTOD;
using UDI.VTOD.Common;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Common.Track;
using UDI.VTOD.Controller;

namespace UDI.VTOD.WindowsService.VanStatusService
{
	partial class VanStatusService : ServiceBase
	{
		#region Fields
		private System.Timers.Timer timer = new System.Timers.Timer();
		static ILog log;
		static bool isRunning;
		//static int test = 0;
		#endregion

		#region Console
		static void Main(string[] args)
		{
			VanStatusService service = new VanStatusService();

			if (Environment.UserInteractive)
			{
				service.OnStart(args);
				Console.WriteLine("Press any key to stop program");
				Console.Read();
				service.OnStop();
			}
			else
			{
				ServiceBase.Run(service);
			}

		}
		#endregion

		#region Constructors
		public VanStatusService()
		{
			isRunning = false;
			InitializeComponent();
			log4net.Config.XmlConfigurator.Configure();
			log = LogManager.GetLogger(typeof(VanStatusService));

			#region EventLog
			if (!System.Diagnostics.EventLog.SourceExists("VTODTStatus"))
			{
				System.Diagnostics.EventLog.CreateEventSource(
					"VTODTStatus", "VTODTStatus");
			}
			eventLog.Source = "VTODTStatus";
			eventLog.Log = "VTODTStatus";
			#endregion
		}
		#endregion

		#region Event
		protected override void OnStart(string[] args)
		{
            eventLog.WriteEntry(DateTime.Now.ToString() + " VanStatusService has been started...");
            log.Info(DateTime.Now.ToString() + " VanStatusService has been started...");
			timer.AutoReset = true;
			timer.Interval = 5;
			timer.Elapsed += OnElapsedEvent;
			timer.Start();
		}

		protected override void OnStop()
		{
			timer.Stop();
            eventLog.WriteEntry(DateTime.Now.ToString() + " VanStatusService has been stopped...");
            log.Info(DateTime.Now.ToString() + " VanStatusService has been Stopped...");
		}
		#endregion

		#region Timer function
		static void OnElapsedEvent(object sender, ElapsedEventArgs e)
		{

			#region Set interval from app.config
			if (((System.Timers.Timer)sender).Interval == 5)
			{
				var interval = 10000;

				try
				{
					interval = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["Interval"]);
					((System.Timers.Timer)sender).Interval = interval;
					Console.WriteLine("timer.Interval = " + interval.ToString());
				}
				catch (Exception)
				{
				}
			}
			#endregion

			if (!isRunning)
			{
				isRunning = true;

				var queryController = new QueryController();
                queryController.PullVanStatus();

				isRunning = false;
			}
		}
		#endregion
	}
}
