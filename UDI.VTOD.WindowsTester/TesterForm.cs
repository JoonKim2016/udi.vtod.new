﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

using Newtonsoft.Json.Linq;
using UDI.Utility.Helper;

namespace UDI.VTOD.Tester
{
	public partial class TesterForm : Form
	{
		#region Constructors
		public TesterForm()
		{
			InitializeComponent();
			cmbLoadRequest.SelectedIndex = 0;
		}
		#endregion

		private bool gotURLs = false;

		private void btnBrowse_Click(object sender, EventArgs e)
		{
			//bool isLoadTemplateFromURL = false;
			//if (bool.TryParse(ConfigurationManager.AppSettings["LoadTemplateFromURL"], out isLoadTemplateFromURL))
			//{
			//if (isLoadTemplateFromURL)
			//{
			LoadTempaltesFromURL();
			//}
			//else
			//{
			//LoadTemplateFromFile();
			//}
			//}
		}

		private void btnRequest_Click(object sender, EventArgs e)
		{
			lblTime.Text = string.Empty;
			Stopwatch sp = new Stopwatch();
			sp.Start();

			txtResponse.Clear();
			var headers = new Dictionary<string, string>();
			var status = string.Empty;
			var contentType = string.Empty;
			var method = string.Empty;
			var response = string.Empty;

			if (rbtnXML.Checked)
				contentType = "application/xml; charset=utf-8";
			if (rbtnJSON.Checked)
				contentType = "application/json; charset=utf-8";

			if (rbtnGet.Checked)
				method = "GET";
			else if (rbtnPost.Checked)
				method = "POST";

			try
			{
				if (!string.IsNullOrWhiteSpace(txtRequestHeader.Text))
				{
					var header = txtRequestHeader.Text.EndsWith(";") ? txtRequestHeader.Text.Substring(0, txtRequestHeader.Text.Length - 1) : txtRequestHeader.Text;
					var headerStrings = header.Split(';');
					foreach (var item in headerStrings)
					{
						headers.Add(item.Split(':')[0], item.Split(':')[1]);
					}
				}

				response = UDI.Utility.Helper.WebRequestHelper.ProcessWebRequest(
						txtUrlRequest.Text,
						txtRequest.Text,
						contentType,
						method,
						headers,
						out status);
			}
			catch (Exception ex)
			{
				response = ex.Message;
			}

			#region Formatting
			try
			{
				if (rbtnXML.Checked)
				{
					response = XDocument.Parse(response).CleanNamespace().CleanComment().ToString();
				}
				else if (rbtnJSON.Checked)
				{
					JObject json = JObject.Parse(response);
					response = json.ToString();
				}
			}
			catch { }
			#endregion

			#region Colorizing
			AddColouredText(txtResponse, response);
			#endregion

			sp.Stop();
			lblTime.Text = string.Format("{0} milisecond", sp.ElapsedMilliseconds);
		}

		private void cmbLoadRequest_SelectedIndexChanged(object sender, EventArgs e)
		{
			GetURLs();
		}

		#region Private
		private void AddColouredText(RichTextBox textBox, string strTextToAdd)
		{
			//Use the RichTextBox to create the initial RTF code
			textBox.Clear();
			textBox.AppendText(strTextToAdd);
			string strRTF = textBox.Rtf;
			textBox.Clear();

			/* 
			 * ADD COLOUR TABLE TO THE HEADER FIRST 
			 * */

			// Search for colour table info, if it exists (which it shouldn't)
			// remove it and replace with our one
			int iCTableStart = strRTF.IndexOf("colortbl;");

			if (iCTableStart != -1) //then colortbl exists
			{
				//find end of colortbl tab by searching
				//forward from the colortbl tab itself
				int iCTableEnd = strRTF.IndexOf('}', iCTableStart);
				strRTF = strRTF.Remove(iCTableStart, iCTableEnd - iCTableStart);

				//now insert new colour table at index of old colortbl tag
				strRTF = strRTF.Insert(iCTableStart,
					// CHANGE THIS STRING TO ALTER COLOUR TABLE
					"colortbl ;\\red255\\green0\\blue0;\\red0\\green128\\blue0;\\red0\\green0\\blue255;}");
			}

			//colour table doesn't exist yet, so let's make one
			else
			{
				// find index of start of header
				int iRTFLoc = strRTF.IndexOf("\\rtf");
				// get index of where we'll insert the colour table
				// try finding opening bracket of first property of header first                
				int iInsertLoc = strRTF.IndexOf('{', iRTFLoc);

				// if there is no property, we'll insert colour table
				// just before the end bracket of the header
				if (iInsertLoc == -1) iInsertLoc = strRTF.IndexOf('}', iRTFLoc) - 1;

				// insert the colour table at our chosen location                
				strRTF = strRTF.Insert(iInsertLoc,
					// CHANGE THIS STRING TO ALTER COLOUR TABLE
					"{\\colortbl ;\\red128\\green0\\blue0;\\red0\\green128\\blue0;\\red0\\green0\\blue255;}");
			}

			/*
			 * NOW PARSE THROUGH RTF DATA, ADDING RTF COLOUR TAGS WHERE WE WANT THEM
			 * In our colour table we defined:
			 * cf1 = red  
			 * cf2 = green
			 * cf3 = blue             
			 * */

			for (int i = 0; i < strRTF.Length; i++)
			{
				if (strRTF[i] == '<')
				{
					//add RTF tags after symbol 
					//Check for comments tags 
					if (strRTF[i + 1] == '!')
						strRTF = strRTF.Insert(i + 4, "\\cf2 ");
					else
						strRTF = strRTF.Insert(i + 1, "\\cf1 ");
					//add RTF before symbol
					strRTF = strRTF.Insert(i, "\\cf3 ");

					//skip forward past the characters we've just added
					//to avoid getting trapped in the loop
					i += 6;
				}
				else if (strRTF[i] == '>')
				{
					//add RTF tags after character
					strRTF = strRTF.Insert(i + 1, "\\cf0 ");
					//Check for comments tags
					if (strRTF[i - 1] == '-')
					{
						strRTF = strRTF.Insert(i - 2, "\\cf3 ");
						//skip forward past the 6 characters we've just added
						i += 8;
					}
					else
					{
						strRTF = strRTF.Insert(i, "\\cf3 ");
						//skip forward past the 6 characters we've just added
						i += 6;
					}
				}
			}
			textBox.Rtf = strRTF;
		}

		private void GetURLs()
		{
			//bool loadTemplateFromURL = false;

			//if (bool.TryParse(ConfigurationManager.AppSettings["LoadTemplateFromURL"], out loadTemplateFromURL))
			//{
			try
			{
				if (!gotURLs)
				{
					//if (loadTemplateFromURL)
					//{
					//get list
					string url = ConfigurationManager.AppSettings["SampleDirectory"];

					using (WebClient client = new WebClient())
					{
						string page = client.DownloadString(url);
						Regex re = new Regex(@"<A HREF=""([/\.\w\d\s]+\.[xmljson]+)"">", RegexOptions.IgnoreCase);
						MatchCollection mCol = re.Matches(page);
						if (mCol.Count > 0)
						{
							cmbLoadRequest.Items.Clear();
							foreach (Match m in mCol)
							{
								if (m.Groups[1].Value.Contains("RQ.xml") || m.Groups[1].Value.Contains("RQ.json"))
								{
									cmbLoadRequest.Items.Add(m.Groups[1].Value);
								}
							}
							gotURLs = true;
						}
					}
					//}
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}

			//}
		}

		private void LoadTempaltesFromURL()
		{
			string REST_URL = System.Configuration.ConfigurationManager.AppSettings["URL"];
			string SampleDirectory = System.Configuration.ConfigurationManager.AppSettings["SampleDirectory"];
			using (WebClient client = new WebClient())
			{
				#region URL
				#region Parameter
				string parameter = "";
				if (cmbLoadRequest.Text.Contains(".xml"))
				{
					parameter = "?media=xml";
					rbtnXML.Checked = true;
				}
				else
				{
					parameter = "";
					rbtnJSON.Checked = true;
				}
				#endregion

				#region Action
				string action = "";
				if (cmbLoadRequest.Text.Contains("Token"))
				{
					action = "GetToken";
				}
				else if (cmbLoadRequest.Text.Contains("GroundAvail_VehicleInfo"))
				{
					action = "GroundAvail_VehicleInfo";
				}
				else if (cmbLoadRequest.Text.Contains("GroundAvail"))
				{
					action = "GroundAvail";
				}
				else if (cmbLoadRequest.Text.Contains("GroundBook"))
				{
					action = "GroundBook";
				}
				else if (cmbLoadRequest.Text.Contains("GroundResRetrieve"))
				{
					action = "GroundResRetrieve";
				}
				else if (cmbLoadRequest.Text.Contains("GroundCancel"))
				{
					action = "GroundCancel";
				}
				#endregion

				txtUrlRequest.Text = REST_URL + action + parameter;
				#endregion

				#region Header
				string header = "";
				if (!cmbLoadRequest.Text.Contains("Taxi_"))
				{
					if (cmbLoadRequest.Text.Contains("Token"))
					{
						header = "";
					}
					else
					{
						header = "Username:##;SecurityKey:##";
					}
				}
				else
				{
					header = "Username:Taxi;SecurityKey:P@ssw0rd";
				}
				AddColouredText(txtRequestHeader, header);

				#endregion

				#region Shows request content
				var fileName = cmbLoadRequest.Text;
				var fileNames = fileName.Split('/');
				if (fileNames.Count() > 1)
				{
					fileName = fileNames.Last();
				}
				string request = client.DownloadString(SampleDirectory + fileName);
				AddColouredText(txtRequest, request);
				#endregion
			}
		}

		//private void LoadTemplateFromFile()
		//{
		//	var sampleDirectory = System.Configuration.ConfigurationManager.AppSettings["SampleDirectory"];
		//	var baseUrl = System.Configuration.ConfigurationManager.AppSettings["URL"];
		//	var fileName = string.Empty;
		//	txtUrlRequest.Text = string.Empty;
		//	txtRequest.Clear();
		//	txtResponse.Clear();
		//	txtRequestHeader.Clear();
		//	var request = string.Empty;
		//	var needHeader = true;

		//	try
		//	{
		//		switch (cmbLoadRequest.SelectedIndex)
		//		{
		//			case 0:
		//				//Load SDS Token request - XML
		//				fileName = sampleDirectory + "TokenRQ.xml";
		//				txtUrlRequest.Text = baseUrl + "GetToken?media=xml";
		//				rbtnXML.Checked = true;
		//				needHeader = false;

		//				break;
		//			case 1:
		//				//Load SDS Token request - JSON
		//				fileName = sampleDirectory + "TokenRQ.json";
		//				txtUrlRequest.Text = baseUrl + "GetToken";
		//				rbtnJSON.Checked = true;
		//				needHeader = false;
		//				break;
		//			case 3:
		//				//Load GroundAvail request - XML
		//				fileName = sampleDirectory + "OTA_GroundAvailRQ.xml";
		//				txtUrlRequest.Text = baseUrl + "GroundAvail?media=xml";
		//				rbtnXML.Checked = true;
		//				break;
		//			case 4:
		//				//Load GroundAvail request - JSON
		//				fileName = sampleDirectory + "OTA_GroundAvailRQ.json";
		//				txtUrlRequest.Text = baseUrl + "GroundAvail";
		//				rbtnJSON.Checked = true;
		//				break;
		//			case 6:
		//				//Load GroundBook request - XML
		//				fileName = sampleDirectory + "OTA_GroundBookRQ.xml";
		//				txtUrlRequest.Text = baseUrl + "GroundBook?media=xml";
		//				rbtnXML.Checked = true;
		//				break;
		//			case 7:
		//				//Load GroundBook request - JSON
		//				fileName = sampleDirectory + "OTA_GroundBookRQ.json";
		//				txtUrlRequest.Text = baseUrl + "GroundBook";
		//				rbtnJSON.Checked = true;
		//				break;
		//			case 9:
		//				//Load GroundResRetrieve request - XML
		//				fileName = sampleDirectory + "OTA_GroundResRetrieveRQ.xml";
		//				txtUrlRequest.Text = baseUrl + "GroundResRetrieve?media=xml";
		//				rbtnXML.Checked = true;
		//				break;
		//			case 10:
		//				//Load GroundResRetrieve request - JSON
		//				fileName = sampleDirectory + "OTA_GroundResRetrieveRQ.json";
		//				txtUrlRequest.Text = baseUrl + "GroundResRetrieve";
		//				rbtnJSON.Checked = true;
		//				break;
		//			case 12:
		//				//Load Cancel request - XML
		//				fileName = sampleDirectory + "OTA_GroundCancelRQ.xml";
		//				txtUrlRequest.Text = baseUrl + "GroundCancel?media=xml";
		//				rbtnXML.Checked = true;
		//				break;
		//			case 13:
		//				//Load Cancel request - JSON
		//				fileName = sampleDirectory + "OTA_GroundCancelRQ.json";
		//				txtUrlRequest.Text = baseUrl + "GroundCancel";
		//				rbtnJSON.Checked = true;
		//				break;
		//		}

		//		if (!string.IsNullOrWhiteSpace(fileName))
		//		{
		//			if (rbtnXML.Checked)
		//			{
		//				request = XDocument.Load(fileName).ToString();
		//			}
		//			else if (rbtnJSON.Enabled)
		//			{
		//				var wc = new WebClient();

		//				using (var sourceStream = wc.OpenRead(fileName))
		//				{
		//					using (StreamReader sr = new StreamReader(sourceStream))
		//					{
		//						request = sr.ReadToEnd();
		//					}
		//				}
		//			}

		//			AddColouredText(txtRequest, request);

		//			if (needHeader)
		//			{
		//				AddColouredText(txtRequestHeader, "ClientType:##;ClientIPAddress:##;SecurityKey:##;Username:##;");
		//			}
		//		}

		//	}
		//	catch (Exception ex)
		//	{
		//		MessageBox.Show(ex.Message);
		//	}
		//}

		#endregion
	}

}
