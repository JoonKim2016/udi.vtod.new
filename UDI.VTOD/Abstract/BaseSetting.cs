﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

using UDI.Utility.Helper;

namespace UDI.VTOD.Abstract
{
	public abstract class BaseSetting
	{
		#region Fields
		public static ILog logger;
		public bool isTest = false;
		public bool isDebug = false;
		public bool isDummy = false;
		public bool isLogInDB = false;
		#endregion

		#region Constructors
		public BaseSetting()
		{
			var type = this.GetType();
			logger = LogManager.GetLogger(type);

			log4net.Config.XmlConfigurator.Configure();

			#region IsTest
			try
			{
				isTest = System.Configuration.ConfigurationManager.AppSettings["IsTest"].ToBool();
			}
			catch { }
			#endregion

			#region IsDummy
			try
			{
				isDummy = System.Configuration.ConfigurationManager.AppSettings["isDummy"].ToBool();
			}
			catch { }
			#endregion

			#region IsDebug
			try
			{
				isDebug = System.Configuration.ConfigurationManager.AppSettings["IsDebug"].ToBool();
			}
			catch { }
			#endregion

			#region IsLogInDB
			try
			{
				isLogInDB = System.Configuration.ConfigurationManager.AppSettings["IsLogInDB"].ToBool();
			}
			catch { }
			#endregion
		}
		#endregion
	}
}
