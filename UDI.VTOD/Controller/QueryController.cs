﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.ServiceModel.Web;
using UDI.Utility.DTO;
using UDI.VTOD.Common.DTO;
using UDI.VTOD.Common.DTO.OTA;
using UDI.Utility.Serialization;
using UDI.Utility.Helper;
using UDI.VTOD.Domain;
using UDI.VTOD.Common.Helper;
using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Domain.Membership;
using UDI.VTOD.Domain.Accounting;
using UDI.VTOD.Domain.VTOD;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Domain.SDS;
using UDI.VTOD.Domain.Taxi;
using UDI.VTOD.Domain.Van;
using UDI.VTOD.Common.Track;
using UDI.VTOD.Domain.Utility;
using System.Diagnostics;
using UDI.VTOD.Domain.Taxi.Const;
using UDI.VTOD.Domain.Notification;
using UDI.VTOD.Domain.ECar;
using UDI.VTOD.Helper;
using UDI.VTOD.Domain.VTODUtility;
using System.Data;
using UDI.Map;
using UDI.VTOD.DataAccess.VTOD;
using UDI.VTOD.Domain.Payment;
using System.Configuration;
using System.Data.Entity.Core.Objects;
using UDI.VTOD.Utility.Common.Cost;

namespace UDI.VTOD.Controller
{
	/// <summary>
	/// This is a class which is having all the methods of the VTOD API.
	/// </summary>
	public class QueryController : BaseSetting
	{
		#region Examples
		public static Ping Ping()
		{
			System.Threading.Thread.Sleep(2000);
			return new Common.DTO.Ping { Time = System.DateTime.UtcNow, Version = "test", Internal = "Internal" };
		}

		public static void Error(WebContentFormat format)
		{
			var exception = new UDI.VTOD.Common.DTO.OTA.OTA_GroundAvailRS();
			var error = new UDI.VTOD.Common.DTO.OTA.ErrorList();
			error.Errors = new List<UDI.VTOD.Common.DTO.OTA.Error>();
			error.Errors.Add(new UDI.VTOD.Common.DTO.OTA.Error { Type = "type1", Value = "error1" });
			error.Errors.Add(new UDI.VTOD.Common.DTO.OTA.Error { Type = "type2", Value = "error2" });
			exception.Errors = error;
			exception.Format = format;			

			var faultException = new FaultException<UDI.VTOD.Common.DTO.OTA.OTA_GroundAvailRS>(exception);
			throw faultException;
		}
        #endregion

        #region Ground
        
        /// <summary>
        /// This is the method which retrieves the availability of a Trip/Request.
        /// </summary>
        /// <param name="input">OTA_GroundAvail Request Object contains Passenger details,Vehicle Info and Pick Up/Drop off details.</param>
        /// <returns name="OTA_GroundAvailRS">OTA_GroundAvail Response Object contains Success/Errors.</returns>
        public OTA_GroundAvailRS GroundAvail(OTA_GroundAvailRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.GroundAvail;
			OTA_GroundAvailRS result = new OTA_GroundAvailRS();
			var mapService = new UDI.Map.MapService();
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var sdsDomain = new SDSDomain(trackTime);			 
			var taxiDomain = new TaxiDomain();
            var vanDomain = new VanDomain();
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();			
			bool isAuthenricate = false;			
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);				

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();               
                if (isDebug)
				{
					logger.InfoFormat("Request{0}", rawRequest);
				}
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_OTA_GroundAvailRQ(input);
				#endregion

				#region Get FleetType & Recognizing Fleet

				#region Fleet Type
				var fleetType = input.RateQualifiers.First().RateQualifierValue.ToFleetType();
				#endregion

				#region Recognizing Fleet
				var fleetRecognized = false;
				if (input.RateQualifiers.First().SpecialInputs != null && input.RateQualifiers.First().SpecialInputs.Any())
				{
					var dispachCodeObj = input.RateQualifiers.First().SpecialInputs.Where(s => s.Name.ToLowerInvariant() == "dispatchcode").FirstOrDefault();
					if (dispachCodeObj != null)
					{
						fleetRecognized = dispachCodeObj.Value.ToFleetRecognized();
					}
				}
				#endregion

				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				//if (fleetType == Common.DTO.Enum.FleetType.Taxi)
				//	isAuthenricate = membershipDomain.IsAuthenticate(token.Username, token.SecurityKey);
				//else
				//	isAuthenricate = true;
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
                #endregion

                #region Reverse GeoCode if AddressValidationRequired = true
                //Pick up address
                if (input.Service.Pickup != null && input.Service.Pickup.Address != null && input.Service.Pickup.Address.TPA_Extensions != null && input.Service.Pickup.Address.TPA_Extensions.AddressValidationRequired.HasValue && input.Service.Pickup.Address.TPA_Extensions.AddressValidationRequired.Value)
				{
					var latStr = input.Service.Pickup.Address.Latitude;
					var lonStr = input.Service.Pickup.Address.Longitude;

					if (!string.IsNullOrWhiteSpace(latStr) && !string.IsNullOrWhiteSpace(lonStr))
					{
						var lat = latStr.ToDecimal();
						var lon = lonStr.ToDecimal();
						if (lat != 0 && lon != 0)
						{
							var error = string.Empty;
							var addresses = mapService.GetAddressesFromGeoLocationWithProximity(new Map.DTO.Geolocation { Latitude = lat, Longitude = lon }, out error);
							if (string.IsNullOrWhiteSpace(error))
							{
								if (addresses != null && addresses.Any())
								{
									input.Service.Pickup.Address = addresses.First().ToAddress(input.Service.Pickup.Address.Latitude.ToDecimal(), input.Service.Pickup.Address.Longitude.ToDecimal());
								}
							}
						}
					}
				}
				//Drop off address
				if (input.Service.Dropoff != null && input.Service.Dropoff.Address != null && input.Service.Dropoff.Address.TPA_Extensions != null && input.Service.Dropoff.Address.TPA_Extensions.AddressValidationRequired.HasValue && input.Service.Dropoff.Address.TPA_Extensions.AddressValidationRequired.Value)
				{
					var latStr = input.Service.Dropoff.Address.Latitude;
					var lonStr = input.Service.Dropoff.Address.Longitude;

					if (!string.IsNullOrWhiteSpace(latStr) && !string.IsNullOrWhiteSpace(lonStr))
					{
						var lat = latStr.ToDecimal();
						var lon = lonStr.ToDecimal();
						if (lat != 0 && lon != 0)
						{
							var error = string.Empty;
							var addresses = mapService.GetAddressesFromGeoLocationWithProximity(new Map.DTO.Geolocation { Latitude = lat, Longitude = lon }, out error);
							if (string.IsNullOrWhiteSpace(error))
							{
								if (addresses != null && addresses.Any())
								{
									input.Service.Dropoff.Address = addresses.First().ToAddress(input.Service.Pickup.Address.Latitude.ToDecimal(), input.Service.Pickup.Address.Longitude.ToDecimal());
								}
							}
						}
					}
				}
				#endregion

				#region Call Domian Method
				if (fleetType == Common.DTO.Enum.FleetType.ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttle_ExecuCar)
				{
                    ECarDomain ecarDomain = new ECarDomain();

                    if (fleetRecognized)
					{
						result = ecarDomain.GetEstimation(token, input);
					}
					else
					{
						OTA_GroundAvailRS sdsResult = null;
						OTA_GroundAvailRS ecarResult = null;
						//var errorListForSuccess = new ErrorList();
						//errorListForSuccess.Errors = new List<Common.DTO.OTA.Error>();

						#region TPL Make
						var tasks = new List<Task>();

						#region SDS Task
						var taskSDS = new Task(
							() =>
							{
								try
								{
									sdsResult = sdsDomain.GroundAvail(token, input);
								}
								catch (Exception ex)
								{
									#region Add errors to OTA
									//errorListForSuccess.Errors.Add(new Common.DTO.OTA.Error { Type = "SDS", Value = ex.Message });
									#endregion

									logger.Error(ex);
								}
							}
							,
							TaskCreationOptions.AttachedToParent);
						tasks.Add(taskSDS);
						#endregion

						#region Ecar Task
						var taskGTC = new Task(
										() =>
										{
											try
											{
												ecarResult = ecarDomain.GetEstimation(token, input);
											}
											catch (Exception ex)
											{
												#region Add errors to OTA
												//errorListForSuccess.Errors.Add(new Common.DTO.OTA.Error { Type = "ECar", Value = ex.Message });
												#endregion

												logger.Error(ex);
											}
										}
										,
										TaskCreationOptions.AttachedToParent);
						tasks.Add(taskGTC);
						#endregion
						#endregion

						#region TPL Run
						if (tasks.Count > 0)
						{
							foreach (var item in tasks)
							{
								item.Start();
							}
						}
						#endregion

						#region TPL WaitAll
						Task.WaitAll(tasks.ToArray());
						#endregion

						#region Making Result
						//result = new OTA_GroundAvailRS();
						result.Success = new Success();
						result.RateQualifiers = input.RateQualifiers;
						result.GroundServices = new GroundServiceList();
						result.GroundServices.GroundServices = new List<GroundService>();
						result.Version = input.Version;

						try
						{
							if (ecarResult != null)
							{
								if (ecarResult.GroundServices != null && ecarResult.GroundServices.GroundServices != null && ecarResult.GroundServices.GroundServices.Any())
								{
									result.GroundServices.GroundServices.AddRange(ecarResult.GroundServices.GroundServices);
								}
								if (ecarResult.TPA_Extensions != null && ecarResult.TPA_Extensions.References != null && ecarResult.TPA_Extensions.References.LinkedReferences != null && ecarResult.TPA_Extensions.References.LinkedReferences.Any())
								{
									#region Initiate LinkedReference
									if (result.TPA_Extensions == null)
										result.TPA_Extensions = new TPA_Extensions();
									if (result.TPA_Extensions.References == null)
										result.TPA_Extensions.References = new ReferenceList();
									if (result.TPA_Extensions.References.LinkedReferences == null)
										result.TPA_Extensions.References.LinkedReferences = new List<LinkedReference>();
									#endregion

									foreach (var linkedReferences in ecarResult.TPA_Extensions.References.LinkedReferences)
									{
										result.TPA_Extensions.References.LinkedReferences.Add(linkedReferences);
									}
								}
							}
						}
						catch (Exception ex)
						{
							logger.Error(ex);
						}

						//SDS as more priority so we add it's list at the end
						try
						{
							if (sdsResult != null)
							{
								if (sdsResult.GroundServices != null && sdsResult.GroundServices.GroundServices != null && sdsResult.GroundServices.GroundServices.Any())
								{
									result.GroundServices.GroundServices.AddRange(sdsResult.GroundServices.GroundServices);
								}
								if (sdsResult.TPA_Extensions != null && sdsResult.TPA_Extensions.References != null && sdsResult.TPA_Extensions.References.LinkedReferences != null && sdsResult.TPA_Extensions.References.LinkedReferences.Any())
								{
									#region Initiate LinkedReference
									if (result.TPA_Extensions == null)
										result.TPA_Extensions = new TPA_Extensions();
									if (result.TPA_Extensions.References == null)
										result.TPA_Extensions.References = new ReferenceList();
									if (result.TPA_Extensions.References.LinkedReferences == null)
										result.TPA_Extensions.References.LinkedReferences = new List<LinkedReference>();
									#endregion

									foreach (var linkedReferences in sdsResult.TPA_Extensions.References.LinkedReferences)
									{
										result.TPA_Extensions.References.LinkedReferences.Add(linkedReferences);
									}
								}
							}

						}
						catch (Exception ex)
						{
							logger.Error(ex);
						}

						//Throw exception if there is no result
						if (result.GroundServices == null || result.GroundServices.GroundServices == null || !result.GroundServices.GroundServices.Any())
						{
							throw VTOD.Common.DTO.VtodException.CreateException(ExceptionType.General, 1003);// new Exception(Messages.General_NoMatchingRate);
						}

						//gtcResult != null && gtcResult.TPA_Extensions != null && gtcResult.TPA_Extensions.Vehicles != null && gtcResult.TPA_Extensions.Vehicles.Items != null && gtcResult.TPA_Extensions.Vehicles.Items.Any()

						#endregion
					}

				}
				else if (fleetType == Common.DTO.Enum.FleetType.SuperShuttle || fleetType == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly)
				{
					result = sdsDomain.GroundAvail(token, input);
				}
				else if (fleetType == Common.DTO.Enum.FleetType.Taxi)
				{
					result = taxiDomain.GetEstimation(token, input);
				}
                else if (fleetType == Common.DTO.Enum.FleetType.Van)
                {
                    result = vanDomain.GetEstimation(token, input);
                }
				if (result == null)
				{
					throw VtodException.CreateException(ExceptionType.General, 1);
				}
				#endregion

				#region Make Rate Qualifier
				result.RateQualifiers = input.RateQualifiers;
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}			
			catch (Exception ex)
			{
				logger.Error("GroundAvail", ex);

				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }					
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// This is the method which retrieves the Vehicle Information.
		/// </summary>
		/// <param name="input">OTA_GroundAvail Request Object contains the Pick Up details like latitude/longitude,city,state etc.It contains passenger details and also vehicle details.</param>
		/// <returns name="OTA_GroundAvailRS">OTA_GroundAvail response object contains list of Vehicle Information driver id,driver name,vehicle type,vehicle number and vehicle status.</returns>

		public OTA_GroundAvailRS GroundVehicleInfo(OTA_GroundAvailRQ input)
		{
			#region Init
			OTA_GroundAvailRS result = null;
			OTA_GroundAvailRS newResult = null;
			var methodName = Common.DTO.Enum.Method.GroundVehicleInfo;			
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var sdsDomain = new SDSDomain(trackTime); 			
			var taxiDomain = new TaxiDomain(); taxiDomain.TrackTime = trackTime;
            var vanDomain = new VanDomain(); vanDomain.TrackTime = trackTime;
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();
			bool isAuthenricate = false;			
			int vehicleMovementAllowed = System.Configuration.ConfigurationManager.AppSettings["VehicleMovementAllowed"].ToInt32();
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_OTA_GroundAvailRQ(input);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Get FleetType & Recognizing Fleet

				#region Fleet Type
				var fleetType = input.RateQualifiers.First().RateQualifierValue.ToFleetType();
				#endregion

				#region Recognizing Fleet
				var fleetRecognized = false;
				if (input.RateQualifiers.First().SpecialInputs != null && input.RateQualifiers.First().SpecialInputs.Any())
				{
					var dispachCodeObj = input.RateQualifiers.First().SpecialInputs.Where(s => s.Name.ToLowerInvariant() == "dispatchcode").FirstOrDefault();
					if (dispachCodeObj != null)
					{
						fleetRecognized = dispachCodeObj.Value.ToFleetRecognized();
					}
				}
				#endregion

				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Authenticate
				//if (fleetType == Common.DTO.Enum.FleetType.Taxi)
				//	isAuthenricate = membershipDomain.IsAuthenticate(token.Username, token.SecurityKey);
				//else
				//	isAuthenricate = true;
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion
                
				#region Call Domian Method
				if (fleetType == Common.DTO.Enum.FleetType.ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttle_ExecuCar)
				{
                    ECarDomain ecarDomain = new ECarDomain();

                    if (fleetRecognized)
					{
						result = ecarDomain.GetVehicleInfo(token, input);
					}
					else
					{
						//result = ecarDomain.GetVehicleInfo(token, input, transactionID);
						OTA_GroundAvailRS sdsResult = null;
						OTA_GroundAvailRS ecarResult = null;
						//var errorListForSuccess = new ErrorList();
						//errorListForSuccess.Errors = new List<Common.DTO.OTA.Error>();

						#region TPL Make
						#region SDS Task
						var tasks = new List<Task>();

						var taskSDS = new Task(
							() =>
							{
								try
								{
									sdsResult = sdsDomain.GetVehicleInfo(token, input);
								}
								catch (Exception ex)
								{
									//errorListForSuccess.Errors.Add(new Common.DTO.OTA.Error { Type = "SDS", Value = ex.Message });
									logger.Error(ex);
								}
							}
							,
							TaskCreationOptions.AttachedToParent);
						tasks.Add(taskSDS);
						#endregion

						#region Ecar Task
						var taskGTC = new Task(
										() =>
										{
											try
											{
												ecarResult = ecarDomain.GetVehicleInfo(token, input);
											}
											catch (Exception ex)
											{
												//errorListForSuccess.Errors.Add(new Common.DTO.OTA.Error { Type = "ECar", Value = ex.Message });
												logger.Error(ex);
											}
										}
										,
										TaskCreationOptions.AttachedToParent);
						tasks.Add(taskGTC);
						#endregion
						#endregion

						#region TPL Run
						if (tasks.Count > 0)
						{
							foreach (var item in tasks)
							{
								item.Start();
							}
						}
						#endregion

						#region TPL WaitAll
						Task.WaitAll(tasks.ToArray());
						#endregion

						#region Making Result
						result = new OTA_GroundAvailRS();
						result.Success = new Success();
						result.TPA_Extensions = new TPA_Extensions();
						result.TPA_Extensions.Vehicles = new Vehicles();
						result.TPA_Extensions.Vehicles.Items = new List<Vehicle>();
						result.TPA_Extensions.Vehicles.NumberOfVehicles = "0";
						result.TPA_Extensions.Vehicles.NearestVehicleETA = "0";
						result.Version = input.Version;

						//result.Errors = errorListForSuccess;

						try
						{
							if (sdsResult != null && sdsResult.TPA_Extensions != null && sdsResult.TPA_Extensions.Vehicles != null && sdsResult.TPA_Extensions.Vehicles.Items != null && sdsResult.TPA_Extensions.Vehicles.Items.Any())
							{
								result.TPA_Extensions.Vehicles.Items.AddRange(sdsResult.TPA_Extensions.Vehicles.Items);
								result.TPA_Extensions.Vehicles.NumberOfVehicles = (result.TPA_Extensions.Vehicles.NumberOfVehicles.ToInt32() + sdsResult.TPA_Extensions.Vehicles.NumberOfVehicles.ToInt32()).ToString();
								if (!string.IsNullOrWhiteSpace(sdsResult.TPA_Extensions.Vehicles.NearestVehicleETA))
								{
									result.TPA_Extensions.Vehicles.NearestVehicleETA =
															(result.TPA_Extensions.Vehicles.NearestVehicleETA.ToInt32() == 0) || (result.TPA_Extensions.Vehicles.NearestVehicleETA.ToInt32() > sdsResult.TPA_Extensions.Vehicles.NearestVehicleETA.ToInt32())
															?
															sdsResult.TPA_Extensions.Vehicles.NearestVehicleETA
															:
															result.TPA_Extensions.Vehicles.NearestVehicleETA;
								}
							}
						}
						catch (Exception ex)
						{
							logger.Error(ex);
						}

						try
						{
							if (ecarResult != null && ecarResult.TPA_Extensions != null && ecarResult.TPA_Extensions.Vehicles != null && ecarResult.TPA_Extensions.Vehicles.Items != null && ecarResult.TPA_Extensions.Vehicles.Items.Any())
							{
								result.TPA_Extensions.Vehicles.Items.AddRange(ecarResult.TPA_Extensions.Vehicles.Items);
								result.TPA_Extensions.Vehicles.NumberOfVehicles = (result.TPA_Extensions.Vehicles.NumberOfVehicles.ToInt32() + ecarResult.TPA_Extensions.Vehicles.NumberOfVehicles.ToInt32()).ToString();
								if (!string.IsNullOrWhiteSpace(ecarResult.TPA_Extensions.Vehicles.NearestVehicleETA))
								{
									result.TPA_Extensions.Vehicles.NearestVehicleETA =
															(result.TPA_Extensions.Vehicles.NearestVehicleETA.ToInt32() == 0) || (result.TPA_Extensions.Vehicles.NearestVehicleETA.ToInt32() > ecarResult.TPA_Extensions.Vehicles.NearestVehicleETA.ToInt32())
															?
															ecarResult.TPA_Extensions.Vehicles.NearestVehicleETA
															:
															result.TPA_Extensions.Vehicles.NearestVehicleETA;
								}
							}
						}
						catch (Exception ex)
						{
							logger.Error(ex);
						}
                        
						#endregion						
					}
				}
				else if (fleetType == Common.DTO.Enum.FleetType.Taxi)
				{
					result = taxiDomain.GetVehicleInfo(token, input);
				}
                else if (fleetType == Common.DTO.Enum.FleetType.Van)
                {
                    result = vanDomain.GetVehicleInfo(token, input);
                }
				else if (fleetType == Common.DTO.Enum.FleetType.SuperShuttle || fleetType == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly)
				{
					result = sdsDomain.GetVehicleInfo(token, input);
				}
				else
				{
					throw Common.DTO.VtodException.CreateFieldRequiredValidationException("RateQualifierValue");
				}

				if (result == null)
				{
					throw VtodException.CreateException(ExceptionType.General, 1);
				}
				#endregion

				#region Adjust number of vehicles (from web.config)
				var numberOfReturnedVehicle = 0;
				try
				{
					numberOfReturnedVehicle = System.Configuration.ConfigurationManager.AppSettings["NumberOfReturnedVehicle"].ToInt32();
				}
				catch
				{ }

				if (numberOfReturnedVehicle > 0)
				{
					if (result.TPA_Extensions.Vehicles != null && result.TPA_Extensions.Vehicles.Items != null && result.TPA_Extensions.Vehicles.Items.Any())
					{
						var vehicleCount = result.TPA_Extensions.Vehicles.Items.Count();

						if (vehicleCount > numberOfReturnedVehicle)
						{
							var removeNumber = vehicleCount - numberOfReturnedVehicle;
							//var x1 = result.TPA_Extensions.Vehicles.Items.XmlSerialize();
							result.TPA_Extensions.Vehicles.Items.RemoveRange(numberOfReturnedVehicle, removeNumber);
							//var x2 = result.TPA_Extensions.Vehicles.Items.XmlSerialize();
						}
					}
				}
				#endregion

				#region Calcualte Vehicle Movement
				try
				{
					if (vehicleMovementAllowed == 1)
					{
						newResult = vtodDomain.CalculateVehicleMovement(token, input, result);
						if (newResult != null)
							result = newResult;
					}
				}
				catch (Exception ex)
				{
					logger.Error("Calculate Vehicle Movement Error :", ex);
				}

				#endregion

				#region Add Rate Qualifier
				result.RateQualifiers = input.RateQualifiers;
				#endregion


                #region Average ETA
                try
                {
                    newResult = vtodDomain.CalculateAverageETA(token, input, result);
                    if (newResult != null)
                     result = newResult;
                    
                }
                catch (Exception ex)
                {
                    logger.Error("Calculate Average ETA Error :", ex);
                }
                #endregion

                rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("GroundAvail_VehicleInfo", ex);

				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }					
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}
			}

			return result;
		}

        public OTA_GroundBookRS GroundBook(OTA_GroundBookRQ input)
        {
            #region Init
            var methodName = Common.DTO.Enum.Method.GroundBook;
            OTA_GroundBookRS result = null;
            var mapService = new UDI.Map.MapService();            
            var rawRequest = string.Empty;
            var rawResponse = string.Empty;
            var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
            var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
            var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime; 
            var taxiDomain = new TaxiDomain();
            var vanDomain = new VanDomain();
            var validationDomain = new ValidationDomain();
            var vtodUtility = new VTODUtilityDomain(); vtodUtility.TrackTime = trackTime;
            var token = new TokenRS();
            bool isAuthenricate = false;            
            int phoneVerificationEnabled = System.Configuration.ConfigurationManager.AppSettings["EnablePhoneVerification"].ToInt32();
            int referralEnabled = System.Configuration.ConfigurationManager.AppSettings["EnableReferral"].ToInt32();
            int pushNotificationEnabled = System.Configuration.ConfigurationManager.AppSettings["EnablePushNotification"].ToInt32();
            List<Tuple<string, int, long>> disptach_Rez_VTOD = null;
            int fleetTripCode = FleetTripCode.Unkonwn;
            string ImageURL = string.Empty;
            #endregion

            try
            {
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
                #endregion

                #region Log Request
                rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey().CleanCardNumber().CleanSeriesCode();
                if (isDebug)
                {
                    logger.InfoFormat("Request{0}", rawRequest);
                }                
                #endregion

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
                #endregion

                #region Modify Token
                Modification.ModifyTokenRS(ref token);
                #endregion

                #region Validate request object
                validationDomain.Validate_OTA_GroundBookRQ(token, input);
                #endregion

                #region Get FleetType & DispatchType
                #region Fleet Type
                var fleetType = input.GroundReservations.First().RateQualifiers.First().RateQualifierValue.ToFleetType();
                #endregion

                #region DispatchType
                var dispatchType = Common.DTO.Enum.DispatchType.Unkown;// input.GroundReservations.First().RateQualifiers.First().RateQualifierValue.ToFleetType();

                if (fleetType == FleetType.Taxi)
                {
                    dispatchType = DispatchType.Taxi;
                }
                else if(fleetType == FleetType.Van)
                {
                    dispatchType = DispatchType.Van;
                }
                else if (fleetType == Common.DTO.Enum.FleetType.SuperShuttle || fleetType == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly)
                {
                    dispatchType = DispatchType.SDS;
                }
                else if (fleetType == Common.DTO.Enum.FleetType.ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttle_ExecuCar)
                {
                    if (input.GroundReservations.First().RateQualifiers.First().SpecialInputs != null && input.GroundReservations.First().RateQualifiers.First().SpecialInputs.Any())
                    {
                        var dispachCodeObj = input.GroundReservations.First().RateQualifiers.First().SpecialInputs.Where(s => s.Name.ToLowerInvariant() == "dispatchcode").FirstOrDefault();
                        if (dispachCodeObj != null)
                        {
                            dispatchType = dispachCodeObj.Value.ToDispatchType();
                        }
                    }
                    if (dispatchType == Common.DTO.Enum.DispatchType.Unkown)
                    {
                        if (input.References != null && input.References.Any())
                        {
                            var reference = input.References.First();
                            if (reference.ID.IsNumeric())
                                dispatchType = DispatchType.SDS;
                            else
                                dispatchType = DispatchType.ECar;
                        }
                        else
                        {
                            Reference item = new Reference();
                            item.ID = string.Empty;
                            input.References.Add(item);
                            dispatchType = DispatchType.ECar;
                        }
                    }
                }
                #endregion
                #endregion

                #region Validate Token
                //if (fleetType == Common.DTO.Enum.FleetType.ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttle || fleetType == Common.DTO.Enum.FleetType.SuperShuttle_ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly)
                //{
                validationDomain.Validate_SDS_Token(token);
                //}
                //else if (fleetType == Common.DTO.Enum.FleetType.Taxi)
                //{
                //	validationResult = validationDomain.Validate_Taxi_Token(token, out validationError);
                //	if (!validationResult)
                //		throw new ValidationException(validationError);
                //}
                #endregion

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
                #endregion

                #region Authenticate
                //if (fleetType == Common.DTO.Enum.FleetType.Taxi)
                //	isAuthenricate = membershipDomain.IsAuthenticate(token.Username, token.SecurityKey);
                //else
                //	isAuthenricate = true;
                isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);

                if (!isAuthenricate)
                    throw VtodException.CreateException(ExceptionType.Auth, 101);
                #endregion
                
                #region Reverse GeoCode if AddressValidationRequired = true
                //Pick up address
                if (input.GroundReservations.First().Service.Location.Pickup != null && input.GroundReservations.First().Service.Location.Pickup.Address != null && input.GroundReservations.First().Service.Location.Pickup.Address.TPA_Extensions != null && input.GroundReservations.First().Service.Location.Pickup.Address.TPA_Extensions.AddressValidationRequired.HasValue && input.GroundReservations.First().Service.Location.Pickup.Address.TPA_Extensions.AddressValidationRequired.Value)
                {
                    var latStr = input.GroundReservations.First().Service.Location.Pickup.Address.Latitude;
                    var lonStr = input.GroundReservations.First().Service.Location.Pickup.Address.Longitude;

                    if (!string.IsNullOrWhiteSpace(latStr) && !string.IsNullOrWhiteSpace(lonStr))
                    {
                        var lat = latStr.ToDecimal();
                        var lon = lonStr.ToDecimal();
                        if (lat != 0 && lon != 0)
                        {
                            var error = string.Empty;
                            var addresses = mapService.GetAddressesFromGeoLocationWithProximity(new Map.DTO.Geolocation { Latitude = lat, Longitude = lon }, out error);
                            if (string.IsNullOrWhiteSpace(error))
                            {
                                if (addresses != null && addresses.Any())
                                {
                                    input.GroundReservations.First().Service.Location.Pickup.Address = addresses.First().ToAddress(input.GroundReservations.First().Service.Location.Pickup.Address.Latitude.ToDecimal(), input.GroundReservations.First().Service.Location.Pickup.Address.Longitude.ToDecimal());
                                }
                            }
                        }
                    }
                }
                //Drop off address
                if (input.GroundReservations.First().Service.Location.Dropoff != null && input.GroundReservations.First().Service.Location.Dropoff.Address != null && input.GroundReservations.First().Service.Location.Dropoff.Address.TPA_Extensions != null && input.GroundReservations.First().Service.Location.Dropoff.Address.TPA_Extensions.AddressValidationRequired.HasValue && input.GroundReservations.First().Service.Location.Dropoff.Address.TPA_Extensions.AddressValidationRequired.Value)
                {
                    var latStr = input.GroundReservations.First().Service.Location.Dropoff.Address.Latitude;
                    var lonStr = input.GroundReservations.First().Service.Location.Dropoff.Address.Longitude;

                    if (!string.IsNullOrWhiteSpace(latStr) && !string.IsNullOrWhiteSpace(lonStr))
                    {
                        var lat = latStr.ToDecimal();
                        var lon = lonStr.ToDecimal();
                        if (lat != 0 && lon != 0)
                        {
                            var error = string.Empty;
                            var addresses = mapService.GetAddressesFromGeoLocationWithProximity(new Map.DTO.Geolocation { Latitude = lat, Longitude = lon }, out error);
                            if (string.IsNullOrWhiteSpace(error))
                            {
                                if (addresses != null && addresses.Any())
                                {
                                    input.GroundReservations.First().Service.Location.Dropoff.Address = addresses.First().ToAddress(input.GroundReservations.First().Service.Location.Pickup.Address.Latitude.ToDecimal(), input.GroundReservations.First().Service.Location.Pickup.Address.Longitude.ToDecimal());
                                }
                            }
                        }
                    }
                }
                #endregion

				#region Populagte Geolocation
				//Pickup
				if (input.GroundReservations.First().Service.Location.Pickup != null && input.GroundReservations.First().Service.Location.Pickup.Address != null && input.GroundReservations.First().Service.Location.Pickup.Address.TPA_Extensions != null && input.GroundReservations.First().Service.Location.Pickup.Address.TPA_Extensions.PopulateGeolocation.HasValue && input.GroundReservations.First().Service.Location.Pickup.Address.TPA_Extensions.PopulateGeolocation.Value)
				{

					#region Get Geolocation from MapQuest
					MapService ms = new MapService();
					string errorMessage = "";
					var address = input.GroundReservations.First().Service.Location.Pickup.Address;
					var vtodAddress = string.Format("{0} {1} {2} {3}", address.StreetNmbr, address.StreetName, address.CityName, address.StateProv.StateCode);
                    var addresses = ms.GetAddresses(vtodAddress, out errorMessage);
					if (addresses != null && addresses.Any())
					{
						input.GroundReservations[0].Service.Location.Pickup.Address.Latitude = addresses.FirstOrDefault().Geolocation.Latitude.ToString();
						input.GroundReservations[0].Service.Location.Pickup.Address.Longitude = addresses.FirstOrDefault().Geolocation.Longitude.ToString();

					}
					#endregion
				}

				//Drop off address
				if (input.GroundReservations.First().Service.Location.Dropoff != null && input.GroundReservations.First().Service.Location.Dropoff.Address != null && input.GroundReservations.First().Service.Location.Dropoff.Address.TPA_Extensions != null && input.GroundReservations.First().Service.Location.Dropoff.Address.TPA_Extensions.PopulateGeolocation.HasValue && input.GroundReservations.First().Service.Location.Dropoff.Address.TPA_Extensions.PopulateGeolocation.Value)
				{
					#region Get Geolocation from MapQuest
					MapService ms = new MapService();
					string errorMessage = "";
					var address = input.GroundReservations.First().Service.Location.Dropoff.Address;
					var addresses = ms.GetAddresses(string.Format("{0} {1} {2} {3}", address.StreetNmbr, address.StreetName, address.CityName, address.StateProv.StateCode), out errorMessage);
					if (addresses != null && addresses.Any())
					{
						input.GroundReservations[0].Service.Location.Dropoff.Address.Latitude = addresses.FirstOrDefault().Geolocation.Latitude.ToString();
						input.GroundReservations[0].Service.Location.Dropoff.Address.Longitude = addresses.FirstOrDefault().Geolocation.Longitude.ToString();

					}
					#endregion
				}
				#endregion

                #region PhoneVerificationRequired
                if (phoneVerificationEnabled == 1)
                {
                    logger.InfoFormat("Start---Verify Phone");

                    var isPhoneVerified = false;
                    if (input.TPA_Extensions != null && input.TPA_Extensions.PhoneVerificationRequired && !string.IsNullOrWhiteSpace(input.TPA_Extensions.MemberID))
                    {
                        int memberID = 0;
                        string phoneNumber = string.Empty;

                        #region Call VTOD Utility web service
                        if (input.TPA_Extensions.PhoneVerificationRequired)
                        {

                            #region VerifyPhone
                            #region PhoneNumbers from Request Object
                            if (input.GroundReservations.First().Passenger.Primary.Telephones.First() != null && !string.IsNullOrEmpty((input.GroundReservations.First().Passenger.Primary.Telephones.First().PhoneNumber)))
                            {
                                phoneNumber = string.Format("{0}{1}{2}", input.GroundReservations.First().Passenger.Primary.Telephones.First().CountryAccessCode, input.GroundReservations.First().Passenger.Primary.Telephones.First().AreaCityCode, input.GroundReservations.First().Passenger.Primary.Telephones.First().PhoneNumber);
                                phoneNumber = phoneNumber.Replace("-", "");
                                logger.InfoFormat("Phone Number:{0}:", phoneNumber);
                            }
                            #endregion
                            #region MemberID from Request Object
                            if (input.TPA_Extensions.MemberID.ToInt32() > 0)
                            {
                                memberID = input.TPA_Extensions.MemberID.ToInt32();
                                logger.InfoFormat("Member ID:{0}:", memberID);
                            }
                            #endregion
                            #region Call VTOD Utility Domian
                            if (memberID > 0 || !string.IsNullOrEmpty(phoneNumber))
                            {
                                logger.InfoFormat("PhoneNumber{0},MemberID{1}:", phoneNumber, memberID);
                                logger.InfoFormat("Start Web Service Method:IsPhoneVerify");
                                isPhoneVerified = vtodUtility.IsPhoneVerified(token, phoneNumber, memberID);
                                logger.InfoFormat("End Web Service Method:IsPhoneVerify");
                            }
                            else
                            {
                                #region Validations
                                if (memberID <= 0)
                                {
                                    logger.InfoFormat("MemberID is required to validate");
                                    throw VTOD.Common.DTO.VtodException.CreateException(ExceptionType.Validation, 13007);
                                }
                                else
                                {
                                    logger.InfoFormat("Phone Number is required to validate");
                                    throw VTOD.Common.DTO.VtodException.CreateException(ExceptionType.Validation, 13008);
                                }
                                #endregion
                            }
                            #endregion
                            if (!isPhoneVerified)
                            {
                                logger.InfoFormat("Your phone is not verified");
                                throw VTOD.Common.DTO.VtodException.CreateException(ExceptionType.Utility, 9008);

                            }

                            #endregion
                        }
                        #endregion


                    }

                    logger.InfoFormat("End---Verify Phone");
                }
                #endregion

                #region Call Domian Method                
                if (dispatchType == DispatchType.ECar)
                {
                    ECarDomain ecarDomain = new ECarDomain();
                    result = ecarDomain.Book(token, input, out disptach_Rez_VTOD, out fleetTripCode);
                }
                else if (dispatchType == DispatchType.SDS)
                {
                    SDSDomain sdsDomain = new SDSDomain(trackTime);
                    result = sdsDomain.GroundBook(token, input, out disptach_Rez_VTOD, null, out fleetTripCode);
                }                
                else if (dispatchType == DispatchType.Taxi)
                {
                    result = taxiDomain.Book(token, input, out disptach_Rez_VTOD, out fleetTripCode);
                }
                else if (dispatchType == DispatchType.Van)
                {
                    result = vanDomain.Book(token, input, out disptach_Rez_VTOD, out fleetTripCode);
                }
                else
                {
                    throw VtodException.CreateException(ExceptionType.General, 2002); // new Exception(Messages.General_DispatchTypeError);
                }
                if (result == null)
                {
                    throw VtodException.CreateException(ExceptionType.General, 1);
                }
                #endregion                               

                #region Link disptach_VTOD_TripIDs
                try
                {
                    var memberID = 0;

                    //Get member ID
                    if (input.TPA_Extensions.IsBOBO.ToBool())
                    {
                        memberID = input.TPA_Extensions.BOBOMemberID.ToInt32();
                    }
                    else if (input.TPA_Extensions != null && !string.IsNullOrWhiteSpace(input.TPA_Extensions.MemberID))
                    {
                        try
                        {
                            memberID = input.TPA_Extensions.MemberID.ToInt32();
                        }
                        catch
                        { }
                    }

                    if (memberID > 0)
                    {                       
                        foreach (var item in disptach_Rez_VTOD)
                        {
                            membershipDomain.LinkReservation(token, item.Item3, item.Item2, memberID);
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }
                #endregion

                #region Insert Notification Record in DB
                if (token != null && token.Username.ToLower().ToString() == "ZTRIP".ToLower().ToString())
                {
                    if (fleetType == Common.DTO.Enum.FleetType.Taxi)
                    {

                        if (disptach_Rez_VTOD != null && disptach_Rez_VTOD.Any())
                        {
                            foreach (var item in disptach_Rez_VTOD)
                            {
                                vtodDomain.InsertTripNotification(item.Item3);                                
                            }
                        }
                    }
                    if (fleetType == Common.DTO.Enum.FleetType.Van)
                    {

                        if (disptach_Rez_VTOD != null && disptach_Rez_VTOD.Any())
                        {
                            foreach (var item in disptach_Rez_VTOD)
                            {
                                vtodDomain.InsertTripNotification(item.Item3);
                            }
                        }
                    }
                    if (fleetType == Common.DTO.Enum.FleetType.ExecuCar)
                    {

                        if (disptach_Rez_VTOD != null && disptach_Rez_VTOD.Any())
                        {
                            foreach (var item in disptach_Rez_VTOD)
                            {
                                var trip = vtodDomain.GetEcarTrip(item.Item3);
                                if (trip != null && trip.ServiceAPIId == FleetTripCode.Ecar_Texas)
                                {
                                    vtodDomain.InsertTripNotification(item.Item3);
                                }
                            }
                        }
                    }
                }
                #endregion

                #region Check Trip Status Record in DB
                if (token != null && token.Username.ToLower().ToString() == "ZTRIP".ToLower().ToString())
                {
                    vtodDomain.InsertCheckTripStatus(result.Reservations.FirstOrDefault().Confirmation.ID.ToInt64());

                }
                #endregion

                #region Insert Push Notification in DB                
                if (pushNotificationEnabled == 1)
                {
                    string firstName = string.Empty;
                    if (result.Reservations != null)
                    {
                        try
                        {
                            firstName = result.Reservations.FirstOrDefault().Passenger.Primary.PersonName.GivenName;
                        }
                        catch
                        {

                        }
                    }
                    DateTime? datetimeUTC = vtodDomain.GetVTODUTCTime(result.Reservations.FirstOrDefault().Confirmation.ID.ToInt64()).PickupDateTimeUTC.ToDateTime();
                    if (datetimeUTC != null)
                        if (token != null && token.Username.ToLower().ToString() == "ZTRIP".ToLower().ToString())
                        {
                            #region TripReminder for PMUL
                            if (input.TPA_Extensions.PickMeUpNow != null && input.TPA_Extensions.PickMeUpNow == false)
                            {
                                if (!string.IsNullOrWhiteSpace(input.TPA_Extensions.MemberID))
                                {
                                    var memberID = input.TPA_Extensions.MemberID.ToInt32();
                                    var membershipController = new UDI.SDS.MembershipController(token, TrackTime);
                                    if (memberID > 0)
                                    {
                                        var memberDetails = membershipController.GetProfileCommunicationSettings(memberID);
                                        vtodDomain.InsertPushNotification(input.TPA_Extensions.MemberID.ToInt64(), null, result.Reservations.FirstOrDefault().Confirmation.ID.ToInt64(), datetimeUTC, UDI.VTOD.Utility.Common.Const.PushTypes.Later.ToString(), fleetTripCode, Utility.Common.Const.TripStatus.Booked, firstName, memberDetails.AllowSms, token.Username, Utility.Common.Const.PushWorkFlow.PushWorkFlow_Inserted_TripReminder);
                                    }
                                }
                            }
                            #endregion
                            
                        }
                }
                #endregion



                #region check if it's a referral trip
                //BOBO Trip is not eligible for referral credit
                if (referralEnabled == 1 && !input.TPA_Extensions.IsBOBO.ToBool())
                {
                    int sdsMemberId = 0;
                    if(!string.IsNullOrEmpty(input.TPA_Extensions.MemberID))
                        sdsMemberId=Convert.ToInt32(input.TPA_Extensions.MemberID);
                    
                    if (token != null && token.Username.ToLower().ToString() == "ZTRIP".ToLower().ToString())
                    {
                        Reservation bookedReservation = result.Reservations.FirstOrDefault();

                        //Need to filter with paymentMethod.PaymentCard.( This is a Biz Rule for Referral)                             
                        if (bookedReservation != null && input.Payments.Payments.Any(x => x.PaymentCard != null))
                        {
                            using (var db = new DataAccess.VTOD.VTODEntities())
                            {
                                int? referrerSDSMemberID = db.vtod_member_info.Where(x => x.SDSMemberId.Equals(sdsMemberId)).Select(x => x.ReferrerSDSMemberID).SingleOrDefault();
                                if (referrerSDSMemberID.HasValue)
                                {

                                    bool anyChargedTrip = db.vtod_trip.Any(x => (x.ChargeStatus.Equals("Charged", StringComparison.OrdinalIgnoreCase)
                                                                          || x.FinalStatus.Equals("Completed", StringComparison.OrdinalIgnoreCase))
                                                                          && x.UserID.Equals(11)
                                                                          && x.MemberID == sdsMemberId.ToString());
                                    if (!anyChargedTrip)
                                    {
                                        logger.InfoFormat("Referral Inesrtion -- Started");

                                        ReferralTrip referralTrip = new ReferralTrip();
                                        referralTrip.VtodTripID = Convert.ToInt64(bookedReservation.Confirmation.ID);
                                        referralTrip.SDSMemberID = sdsMemberId;
                                        referralTrip.ReferrerSDSMemberID = referrerSDSMemberID.Value;
                                        referralTrip.CreatedOn = DateTime.Now;
                                        referralTrip.ReferralCreditFlow = ReferrerCreditFlow.Credit_Inserted;
                                        //ReferralCreditAmount  will be determin when user finished the trip

                                        db.ReferralTrips.Add(referralTrip);
                                        db.SaveChanges();

                                        logger.InfoFormat("Referral Inertion -- End");
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion

                #region check if it's first trip
                
                if (token != null && token.Username.ToLower().ToString() == "ZTRIP".ToLower().ToString() 
                                                            && (   fleetTripCode == FleetTripCode.SDS_Shuttle_Airport 
                                                                || fleetTripCode == FleetTripCode.SDS_Shuttle_Charter
                                                                || fleetTripCode == FleetTripCode.SDS_Shuttle_Hourly))
                {
                    using (var db = new DataAccess.VTOD.VTODEntities())
                    {
                        var memberInfo = db.vtod_member_info.Where(x => x.SDSMemberId.ToString() == input.TPA_Extensions.MemberID.ToString()).FirstOrDefault();

                        if (memberInfo != null && !memberInfo.HasBeenBooked)
                        {
                            memberInfo.HasBeenBooked = true;
                            db.SaveChanges();
                        }
                    }
                }

                #endregion

                rawResponse = result.XmlSerialize().ToString();

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
                #endregion
            }
          
            catch (Exception ex)
            {
                logger.Error("GroundBook", ex);
                
                var exception = getOtaException(ex);
                rawResponse = exception.Errors.XmlSerialize().ToString();
                throw exception;
            }
            finally
            {
                try
                {
                    #region Log Reponse
                    if (isDebug)
                    {
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }                    
                    #endregion

                    #region Track
                    trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
                    #endregion

                    #region SaveTrackTime
                    vtodDomain.InsertTrack(trackTime);
                    trackTime.Dispose();
                    #endregion
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }
            }

            return result;
        }
		/// <summary>
		/// This is the method which is used to do a Book a trip.
		/// </summary>
		/// <param name="input">OTA_GroundBook request object contains ground reservation(e.g. name,phone number,email address of the passengers),location details,passenger count and the vehicle information like taxi/ecar etc.</param>
		/// <returns name="OTA_GroundBookRS">OTA_GroundBook Response Object contains the Trip ID of the booking and the location details.
		/// Upon success it returns success object otherwise error object is returned.</returns>

		public OTA_GroundBookRS GroundModifyBook(OTA_GroundBookRQ input)
		{
			#region Init
            var methodName = Common.DTO.Enum.Method.GroundModifyBook;
			OTA_GroundBookRS result = null;
			var mapService = new UDI.Map.MapService();			
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;						
			var taxiDomain = new TaxiDomain();
			var validationDomain = new ValidationDomain();
			var vtodUtility = new VTODUtilityDomain(); vtodUtility.TrackTime = trackTime;
			var token = new TokenRS();
			bool isAuthenricate = false;			
			int pushNotificationEnabled = System.Configuration.ConfigurationManager.AppSettings["EnablePushNotification"].ToInt32();
			List<Tuple<string, int, long>> disptach_Rez_VTOD = null;
			int fleetTripCode = FleetTripCode.Unkonwn;
			string ImageURL = string.Empty;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey().CleanCardNumber().CleanSeriesCode();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }				
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
                validationDomain.Validate_OTA_GroundModifyBookRQ(token, input);
				#endregion

				#region Get FleetType & DispatchType
				#region Fleet Type
				var fleetType = input.GroundReservations.First().RateQualifiers.First().RateQualifierValue.ToFleetType();
				#endregion

				#region DispatchType
				var dispatchType = Common.DTO.Enum.DispatchType.Unkown;

				if (fleetType == FleetType.Taxi)
				{
					dispatchType = DispatchType.Taxi;
				}
				else if (fleetType == Common.DTO.Enum.FleetType.SuperShuttle || fleetType == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly || fleetType == Common.DTO.Enum.FleetType.SuperShuttle_ExecuCar)
				{
					dispatchType = DispatchType.SDS;
				}
				else if (fleetType == Common.DTO.Enum.FleetType.ExecuCar)
				{
					if (input.GroundReservations.First().RateQualifiers.First().SpecialInputs != null && input.GroundReservations.First().RateQualifiers.First().SpecialInputs.Any())
					{
						var dispachCodeObj = input.GroundReservations.First().RateQualifiers.First().SpecialInputs.Where(s => s.Name.ToLowerInvariant() == "dispatchcode").FirstOrDefault();
						if (dispachCodeObj != null)
						{
							dispatchType = dispachCodeObj.Value.ToDispatchType();
						}
					}
					if (dispatchType == Common.DTO.Enum.DispatchType.Unkown)
					{
                        if (input.References != null && input.References.Any())
                        {
                            if (input.References.FirstOrDefault().Type.Trim().ToLower() == ConfirmationType.AgencyConfirmation.ToString().Trim().ToLower())
                            {
                                dispatchType = DispatchType.SDS;
                            }
                            else if (input.References.FirstOrDefault().Type.Trim().ToLower() == ConfirmationType.DispatchConfirmation.ToString().Trim().ToLower())
                            {
                                dispatchType = vtodDomain.GetDispatchTypeWithDispatchConfirmation(input.References.First().ID);
                            }
                            else if (input.References.FirstOrDefault().Type.Trim().ToLower() == ConfirmationType.Confirmation.ToString().Trim().ToLower())
                            {
                                dispatchType = vtodDomain.GetDispatchType(input.References.First().ID.ToInt64());
                            }
                            else
                            {
                                //Intentionally blank
                                logger.Error("Unknown case");
                            }
                        }
                    }
				}
				#endregion
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);

				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion
                

				#region Reverse GeoCode if AddressValidationRequired = true
				//Pick up address
				if (input.GroundReservations.First().Service.Location.Pickup != null && input.GroundReservations.First().Service.Location.Pickup.Address != null && input.GroundReservations.First().Service.Location.Pickup.Address.TPA_Extensions != null && input.GroundReservations.First().Service.Location.Pickup.Address.TPA_Extensions.AddressValidationRequired.HasValue && input.GroundReservations.First().Service.Location.Pickup.Address.TPA_Extensions.AddressValidationRequired.Value)
				{
					var latStr = input.GroundReservations.First().Service.Location.Pickup.Address.Latitude;
					var lonStr = input.GroundReservations.First().Service.Location.Pickup.Address.Longitude;

					if (!string.IsNullOrWhiteSpace(latStr) && !string.IsNullOrWhiteSpace(lonStr))
					{
						var lat = latStr.ToDecimal();
						var lon = lonStr.ToDecimal();
						if (lat != 0 && lon != 0)
						{
							var error = string.Empty;
							var addresses = mapService.GetAddressesFromGeoLocationWithProximity(new Map.DTO.Geolocation { Latitude = lat, Longitude = lon }, out error);
							if (string.IsNullOrWhiteSpace(error))
							{
								if (addresses != null && addresses.Any())
								{
									input.GroundReservations.First().Service.Location.Pickup.Address = addresses.First().ToAddress(input.GroundReservations.First().Service.Location.Pickup.Address.Latitude.ToDecimal(), input.GroundReservations.First().Service.Location.Pickup.Address.Longitude.ToDecimal());
								}
							}
						}
					}
				}
				#endregion
				#region Call Domian Method
				if (dispatchType == DispatchType.ECar)
				{
                    ECarDomain ecarDomain = new ECarDomain();
                    result = ecarDomain.ModifyBook(token, input, out disptach_Rez_VTOD, out fleetTripCode);
				}
				else if (dispatchType == DispatchType.SDS)
				{
                    SDSDomain sdsDomain = new SDSDomain(trackTime);                    
                    result = sdsDomain.ModifyBook(token, input, out disptach_Rez_VTOD, null, out fleetTripCode);
                }
				else if (dispatchType == DispatchType.Taxi)
				{
                    result = taxiDomain.ModifyBook(token, input, out disptach_Rez_VTOD, out fleetTripCode);
                }
				else
				{
					throw VtodException.CreateException(ExceptionType.General, 2002); 
				}
				if (result == null)
				{
					throw VtodException.CreateException(ExceptionType.General, 1);
				}
				#endregion

				#region Insert Push Notification in DB
				#region TripReminder for PMUL
				if (pushNotificationEnabled == 1)
				{
					string firstName = string.Empty;
					if (result.Reservations != null)
					{
						try
						{
							firstName = result.Reservations.FirstOrDefault().Passenger.Primary.PersonName.GivenName;
						}
						catch
						{

						}
					}
					DateTime? datetimeUTC = vtodDomain.GetVTODUTCTime(result.Reservations.FirstOrDefault().Confirmation.ID.ToInt64()).PickupDateTimeUTC.ToDateTime();
					if (datetimeUTC != null)
						if (token != null && token.Username.ToLower().ToString() == "ZTRIP".ToLower().ToString())
						{
							if (input.TPA_Extensions.PickMeUpNow != null && input.TPA_Extensions.PickMeUpNow == false)
							{
								if (!string.IsNullOrWhiteSpace(input.TPA_Extensions.MemberID))
								{
									var memberID = input.TPA_Extensions.MemberID.ToInt32();
									var membershipController = new UDI.SDS.MembershipController(token, TrackTime);
									if (memberID > 0)
									{
										var memberDetails = membershipController.GetProfileCommunicationSettings(memberID);
                                        vtodDomain.InsertPushNotification(input.TPA_Extensions.MemberID.ToInt64(), null, result.Reservations.FirstOrDefault().Confirmation.ID.ToInt64(), datetimeUTC, UDI.VTOD.Utility.Common.Const.PushTypes.Later.ToString(), fleetTripCode, UDI.VTOD.Utility.Common.Const.TripStatus.Booked, firstName, memberDetails.AllowSms, token.Username, UDI.VTOD.Utility.Common.Const.PushWorkFlow.PushWorkFlow_Inserted_TripReminder);
									}
								}
							}
						}
				}
				#endregion
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("GroundModifyBook", ex);
				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }					
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}
			}

			return result;
		}
		/// <summary>
		/// It cancels the trip.
		/// </summary>
		/// <param name="input">OTA_GroundCancel request object contains trip id to be cancelled.It also contains the passenger name,telephone number who raised the request.</param>
		/// <returns name="OTA_GroundCancelRS">OTA_GroundCancel response object contains Success/Error</returns>
		public OTA_GroundCancelRS GroundCancel(OTA_GroundCancelRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.GroundCancel;
			OTA_GroundCancelRS result = null;			
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;			
            var vanDomain = new VanDomain();
			var taxiDomain = new TaxiDomain();
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var sdsDomain = new SDSDomain(trackTime);
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();			
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_OTA_GroundCancelRQ(input);
				#endregion

				#region Validate Trip+User
				if (input.Reservation.UniqueID != null && input.Reservation.UniqueID.Where(s => s.Type.ToLower() == Common.DTO.Enum.ConfirmationType.Confirmation.ToString().ToLower()).Any())
				{
					var tripID = input.Reservation.UniqueID.Where(s => s.Type.ToLower() == Common.DTO.Enum.ConfirmationType.Confirmation.ToString().ToLower()).First().ID.ToInt64();
					if (!vtodDomain.ValidateTripUser(tripID, token.Username))
					{
						throw Common.DTO.VtodException.CreateException(ExceptionType.Validation, 13005);// ("token.Username"); //throw new ValidationException(Messages.Validation_Trip_UserName);					}
					}
				}
				#endregion

				#region Get FleetType & DispatchType
				#region FleetType
				var fleetType = input.TPA_Extensions.RateQualifiers.First().RateQualifierValue.ToFleetType();
				#endregion

				#region DispatchType
				var dispatchType = Common.DTO.Enum.DispatchType.Unkown;// input.GroundReservations.First().RateQualifiers.First().RateQualifierValue.ToFleetType();

				if (fleetType == FleetType.Taxi)
				{
					dispatchType = DispatchType.Taxi;
				}
                else if (fleetType == FleetType.Van)
                {
                    dispatchType = DispatchType.Van;
                }
				else if (fleetType == Common.DTO.Enum.FleetType.SuperShuttle || fleetType == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly)
				{
					dispatchType = DispatchType.SDS;
				}
				else if (fleetType == Common.DTO.Enum.FleetType.ExecuCar || fleetType == FleetType.SuperShuttle_ExecuCar)
				{
					if (input.TPA_Extensions.RateQualifiers.First().SpecialInputs != null && input.TPA_Extensions.RateQualifiers.First().SpecialInputs.Any())
					{
						#region For ECar like GreenTomatro .. (not for SDS)
						var dispachCodeObj = input.TPA_Extensions.RateQualifiers.First().SpecialInputs.Where(s => s.Name.ToLower() == "dispatchcode").FirstOrDefault();
						if (dispachCodeObj != null)
						{
							dispatchType = dispachCodeObj.Value.ToDispatchType();
						}
						#endregion
					}
					if (dispatchType == DispatchType.Unkown)
					{
                        #region Confirmation

                        if (input.Reservation.UniqueID != null && input.Reservation.UniqueID.Any())
                        {
                            if (input.Reservation.UniqueID.First().Type.Trim().ToLower() == ConfirmationType.AgencyConfirmation.ToString().Trim().ToLower())
                            {
                                dispatchType = DispatchType.SDS;
                            }
                            else if (input.Reservation.UniqueID.First().Type.Trim().ToLower() == ConfirmationType.DispatchConfirmation.ToString().Trim().ToLower())
                            {
                                dispatchType = vtodDomain.GetDispatchTypeWithDispatchConfirmation(input.Reservation.UniqueID.First().ID);
                            }
                            else if (input.Reservation.UniqueID.First().Type.Trim().ToLower() == ConfirmationType.Confirmation.ToString().Trim().ToLower())
                            {
                                dispatchType = vtodDomain.GetDispatchType(input.Reservation.UniqueID.First().ID.ToInt64());
                            }
                            else
                            {
                                //Intentionally blank
                                logger.Error("Unknown case");
                            }
                        }
                        #endregion                       
					}
				}
				#endregion
				#endregion

				#region Validate Token
				//if (fleetType == Common.DTO.Enum.FleetType.ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttle || fleetType == Common.DTO.Enum.FleetType.SuperShuttle_ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly)
				//{
				//validationDomain.Validate_SDS_Token(token);
				//}
				//else if (fleetType == Common.DTO.Enum.FleetType.Taxi)
				//{
				//	validationResult = validationDomain.Validate_Taxi_Token(token, out validationError);
				//	if (!validationResult)
				//		throw new ValidationException(validationError);
				//}
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				//if (fleetType == Common.DTO.Enum.FleetType.Taxi)
				//	isAuthenricate = membershipDomain.IsAuthenticate(token.Username, token.SecurityKey);
				//else
				//	isAuthenricate = true;
				//isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				//if (!isAuthenricate)
				//    throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion
                
				#region Call Domian Method
				//if (fleetType == Common.DTO.Enum.FleetType.ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttle || fleetType == Common.DTO.Enum.FleetType.SuperShuttle_ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly)
				//{
				if (dispatchType == DispatchType.ECar)
				{
                    ECarDomain ecarDomain = new ECarDomain();
                    result = ecarDomain.Cancel(token, input);
				}
				else if (dispatchType == DispatchType.SDS)
				{
					result = sdsDomain.GroundCancel(token, input);
				}
				//}
				else if (dispatchType == DispatchType.Taxi)
				{
					result = taxiDomain.Cancel(token, input);
				}
                else if (dispatchType == DispatchType.Van)
                {
                    result = vanDomain.Cancel(token, input);
                }
				else
				{
					throw VtodException.CreateException(ExceptionType.General, 5002);// new Exception(Messages.General_DispatchTypeError);
				}
				if (result == null)
				{
					throw VtodException.CreateException(ExceptionType.General, 1);
				}
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("GroundCancel", ex);

				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					long? tripID = null;
					try
					{
						tripID = input.Reservation.UniqueID.First().ID.ToInt64();
					}
					catch
					{ }

					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }					
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}
			}

			return result;
		}

		/// <summary>
		/// This method is used to retrieve the status of the request.
		/// </summary>
		/// <param name="input">OTA_GroundResRetrieve request object contains the trip id  of the request.</param>
		/// <returns name="OTA_GroundResRetrieveRS">OTA_GroundResRetrieve response object contains the booking driver details,vehicle information and the contact details </returns>
		public OTA_GroundResRetrieveRS GroundResRetrieve(OTA_GroundResRetrieveRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.GroundResRetrieve;
			OTA_GroundResRetrieveRS result = null;			
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain();
			var taxiDomain = new TaxiDomain();
            var vanDomain = new VanDomain();
			var sdsDomain = new SDSDomain(trackTime);            
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }				
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_OTA_GroundResRetrieveRQ(input);
				#endregion
				
				#region Encrypeted confirmation
				if (input.Reference != null && input.Reference.Any() && input.Reference.FirstOrDefault().ID_Context != null)
				{
					if (input.Reference.FirstOrDefault().ID_Context == "Encrypt")
					{
						input.Reference.FirstOrDefault().ID = UDI.Utility.Helper.Cryptography.ConvertHexToString(input.Reference.FirstOrDefault().ID);
					}
				} 
				#endregion

				#region Validate Trip+User
				if (input.Reference != null && input.Reference.Where(s => s.Type.ToLower() == Common.DTO.Enum.ConfirmationType.Confirmation.ToString().ToLower()).Any())
				{
					var tripID = input.Reference.Where(s => s.Type.ToLower() == Common.DTO.Enum.ConfirmationType.Confirmation.ToString().ToLower()).First().ID.ToInt64();
					if (!vtodDomain.ValidateTripUser(tripID, token.Username))
					{
						throw Common.DTO.VtodException.CreateException(ExceptionType.Validation, 13005);// ("token.Username"); //throw new ValidationException(Messages.Validation_Trip_UserName);
					}
				}
				#endregion

				#region Get FleetType & DispatchType
				#region FleetType
				var fleetType = input.TPA_Extensions.RateQualifiers.First().RateQualifierValue.ToFleetType();
				#endregion

              
				#region DispatchType
				var dispatchType = DispatchType.Unkown;

				if (fleetType == FleetType.Taxi)
				{
					dispatchType = DispatchType.Taxi;
				}

                else if (fleetType == FleetType.Van)
                {
                    dispatchType = DispatchType.Van;
                }
				else if (fleetType == Common.DTO.Enum.FleetType.SuperShuttle || fleetType == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly)
				{
					dispatchType = DispatchType.SDS;
				}
				else if (fleetType == Common.DTO.Enum.FleetType.ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttle_ExecuCar)
				{

					if (input.TPA_Extensions.RateQualifiers.First().SpecialInputs != null && input.TPA_Extensions.RateQualifiers.First().SpecialInputs.Any())
					{
						#region For ECar like GreenTomatro .. (not for SDS)
						var dispachCodeObj = input.TPA_Extensions.RateQualifiers.First().SpecialInputs.Where(s => s.Name.ToLower() == "dispatchcode").FirstOrDefault();
						if (dispachCodeObj != null)
						{
							dispatchType = dispachCodeObj.Value.ToDispatchType();
						}
						#endregion
					}
					if (dispatchType == DispatchType.Unkown)
					{                           
                        if (input.Reference != null && input.Reference.Any())
                        {
                            if (input.Reference.FirstOrDefault().Type.Trim().ToLower() == ConfirmationType.AgencyConfirmation.ToString().Trim().ToLower())
                            {
                                dispatchType = DispatchType.SDS;
                            }
                            else if (input.Reference.FirstOrDefault().Type.Trim().ToLower() == ConfirmationType.DispatchConfirmation.ToString().Trim().ToLower())
                            {
                                dispatchType = vtodDomain.GetDispatchTypeWithDispatchConfirmation(input.Reference.First().ID);
                            }
                            else if (input.Reference.FirstOrDefault().Type.Trim().ToLower() == ConfirmationType.Confirmation.ToString().Trim().ToLower())
                            {
                                dispatchType = vtodDomain.GetDispatchType(input.Reference.First().ID.ToInt64());
                            }
                            else
                            {
                                //Intentionally blank
                                logger.Error("Unknown case");
                            }
                        }                        
                    }					
				}
				#endregion
				#endregion

				#region Validate Token
				//if (fleetType == Common.DTO.Enum.FleetType.ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttle || fleetType == Common.DTO.Enum.FleetType.SuperShuttle_ExecuCar)
				//{
				validationDomain.Validate_SDS_Token(token);
				//}
				//else if (fleetType == Common.DTO.Enum.FleetType.Taxi)
				//{
				//	validationResult = validationDomain.Validate_Taxi_Token(token, out validationError);
				//	if (!validationResult)
				//		throw new ValidationException(validationError);
				//}
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				//if (fleetType == Common.DTO.Enum.FleetType.Taxi)
				//	isAuthenricate = membershipDomain.IsAuthenticate(token.Username, token.SecurityKey);
				//else
				//	isAuthenricate = true;
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion
                
				#region Call Domian Method
				//if (fleetType == Common.DTO.Enum.FleetType.ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttle || fleetType == Common.DTO.Enum.FleetType.SuperShuttle_ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly)
				//{

				if (dispatchType == DispatchType.ECar)
				{
                    ECarDomain ecarDomain = new ECarDomain();
                    result = ecarDomain.Status(token, input);
				}
				else if (dispatchType == DispatchType.SDS)
				{
					#region Do not delete it. It's logic for mapping SDS and ECar.
					//var mappedEcarID = vtodDomain.GetMappedEcarVtodID(input.Reference.First().ID.ToInt64());
					//if (mappedEcarID.ToString() == input.Reference.First().ID.ToString())
					//{
					//result = sdsDomain.GroundResRetrieve(token, input, transactionID);
					//}
					//else
					//{
					//	input.Reference.First().ID = mappedEcarID.ToString();
					//	result = ecarDomain.Status(token, input, transactionID);
					//}

					//result = sdsDomain.GroundResRetrieve(token, input, transactionID); 
					#endregion

					result = sdsDomain.GroundResRetrieve(token, input);

				}
				//}
				//if (fleetType == Common.DTO.Enum.FleetType.ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttle || fleetType == Common.DTO.Enum.FleetType.SuperShuttle_ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly)
				//{
				//	result = sdsDomain.GroundResRetrieve(token, input, transactionID);
				//}
				else if (dispatchType == DispatchType.Taxi)
				{
					result = taxiDomain.Status(token, input);
				}

                else if (dispatchType == DispatchType.Van)
                {
                    result = vanDomain.Status(token, input);
                }
				else
				{
					throw VtodException.CreateException(ExceptionType.General, 3002); ; // new Exception(Messages.General_DispatchTypeError);
				}

				if (result == null)
				{
					throw VtodException.CreateException(ExceptionType.General, 1);
				}
				#endregion

				#region Modifying Response for zTrip ## it's hardcode but should be implemented in a better way !!!!
				if (token.Username.ToLower().Trim() == "ztrip")
				{
					if (result != null && result.TPA_Extensions != null && result.TPA_Extensions.Statuses != null && result.TPA_Extensions.Statuses.Status != null && result.TPA_Extensions.Statuses.Status.Any())
					{
						var status = result.TPA_Extensions.Statuses.Status.FirstOrDefault();
						if (result.TPA_Extensions.Vehicles != null && result.TPA_Extensions.Vehicles.Items != null && result.TPA_Extensions.Vehicles.Items.Any())
						{
							if (status != null && !string.IsNullOrWhiteSpace(status.Value) && status.Value.Trim().ToLower() == "locating driver")
							{
								result.TPA_Extensions.Vehicles.Items.RemoveAll(s => true);
							}
						}
					}
				}
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("GroundResRetrieve", ex);

				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }					
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}
			}

			return result;
		}

		public OTA_GroundResRetrieveRS GroundOriginalResRetrieve(OTA_GroundResRetrieveRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.GroundOriginalResRetrieve;
			OTA_GroundResRetrieveRS result = null;			
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain();
			var taxiDomain = new TaxiDomain();
			var sdsDomain = new SDSDomain(trackTime);			
            var validationDomain = new ValidationDomain();
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }				
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_OTA_GroundOriginalResRetrieve(input);
				#endregion

				#region Validate Trip+User
				if (input.Reference != null && input.Reference.Where(s => s.Type.ToLower() == Common.DTO.Enum.ConfirmationType.Confirmation.ToString().ToLower()).Any())
				{
					var tripID = input.Reference.Where(s => s.Type.ToLower() == Common.DTO.Enum.ConfirmationType.Confirmation.ToString().ToLower()).First().ID.ToInt64();
					if (!vtodDomain.ValidateTripUser(tripID, token.Username))
					{
						throw Common.DTO.VtodException.CreateException(ExceptionType.Validation, 13005);// ("token.Username"); //throw new ValidationException(Messages.Validation_Trip_UserName);
					}
				}
				#endregion

				#region Get FleetType & DispatchType
				#region FleetType
				var fleetType = input.TPA_Extensions.RateQualifiers.First().RateQualifierValue.ToFleetType();
				#endregion

				#region DispatchType
				var dispatchType = DispatchType.Unkown;

				if (fleetType == FleetType.Taxi)
				{
					dispatchType = DispatchType.Taxi;
				}
				else if (fleetType == Common.DTO.Enum.FleetType.SuperShuttle || fleetType == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly)
				{
					dispatchType = DispatchType.SDS;
				}
				else if (fleetType == Common.DTO.Enum.FleetType.ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttle_ExecuCar)
				{
					if (
						(input.Reference != null && input.Reference.Any() && input.Reference.First().Type.Trim().ToLower() == Common.DTO.Enum.ConfirmationType.AgencyConfirmation.ToString().Trim().ToLower())
						||
						(input.Reference != null && input.Reference.Any() && input.Reference.First().Type.Trim().ToLower() == Common.DTO.Enum.ConfirmationType.DispatchConfirmation.ToString().Trim().ToLower())
						)
					{
						dispatchType = DispatchType.SDS;
					}
					else
					{
						dispatchType = vtodDomain.GetDispatchType(input.Reference.First().ID.ToInt64());
					}
				}
				#endregion
				#endregion

				#region Validate Token
				//if (fleetType == Common.DTO.Enum.FleetType.ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttle || fleetType == Common.DTO.Enum.FleetType.SuperShuttle_ExecuCar)
				//{
				validationDomain.Validate_SDS_Token(token);
				//}
				//else if (fleetType == Common.DTO.Enum.FleetType.Taxi)
				//{
				//	validationResult = validationDomain.Validate_Taxi_Token(token, out validationError);
				//	if (!validationResult)
				//		throw new ValidationException(validationError);
				//}
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				//if (fleetType == Common.DTO.Enum.FleetType.Taxi)
				//	isAuthenricate = membershipDomain.IsAuthenticate(token.Username, token.SecurityKey);
				//else
				//	isAuthenricate = true;
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion
                
				#region Call Domian Method
				//if (fleetType == Common.DTO.Enum.FleetType.ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttle || fleetType == Common.DTO.Enum.FleetType.SuperShuttle_ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly)
				//{

				if (dispatchType == DispatchType.ECar)
				{
                    ECarDomain ecarDomain = new ECarDomain();
                    result = ecarDomain.Status(token, input);
				}
				else if (dispatchType == DispatchType.SDS)
				{
					#region Do not delete it. It's logic for mapping SDS and ECar.
					//var mappedEcarID = vtodDomain.GetMappedEcarVtodID(input.Reference.First().ID.ToInt64());
					//if (mappedEcarID.ToString() == input.Reference.First().ID.ToString())
					//{
					//result = sdsDomain.GroundResRetrieve(token, input, transactionID);
					//}
					//else
					//{
					//	input.Reference.First().ID = mappedEcarID.ToString();
					//	result = ecarDomain.Status(token, input, transactionID);
					//}

					//result = sdsDomain.GroundResRetrieve(token, input, transactionID); 
					#endregion

					result = sdsDomain.GroundOriginalResRetrieve(token, input);

				}
				//}
				//if (fleetType == Common.DTO.Enum.FleetType.ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttle || fleetType == Common.DTO.Enum.FleetType.SuperShuttle_ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly)
				//{
				//	result = sdsDomain.GroundResRetrieve(token, input, transactionID);
				//}
				else if (dispatchType == DispatchType.Taxi)
				{
					result = taxiDomain.Status(token, input);
				}
				else
				{
					throw VtodException.CreateException(ExceptionType.General, 3002);
				}

				if (result == null)
				{
					throw VtodException.CreateException(ExceptionType.General, 1);
				}
				#endregion

				#region Modifying Response for zTrip ## it's hardcode but should be implemented in a better way !!!!
				if (token.Username.ToLower().Trim() == "ztrip")
				{
					if (result != null && result.TPA_Extensions != null && result.TPA_Extensions.Statuses != null && result.TPA_Extensions.Statuses.Status != null && result.TPA_Extensions.Statuses.Status.Any())
					{
						var status = result.TPA_Extensions.Statuses.Status.FirstOrDefault();
						if (result.TPA_Extensions.Vehicles != null && result.TPA_Extensions.Vehicles.Items != null && result.TPA_Extensions.Vehicles.Items.Any())
						{
							if (status != null && !string.IsNullOrWhiteSpace(status.Value) && status.Value.Trim().ToLower() == "locating driver")
							{
								result.TPA_Extensions.Vehicles.Items.RemoveAll(s => true);
							}
						}
					}
				}
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("GroundOriginalResRetrieve", ex);

				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }					
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}
			}

			return result;
		}

		/// <summary>
		/// This method is to used to calculate the cancellation fee of a booking request.
		/// </summary>
		/// <param name="input">OTA_GroundResRetrieve request object contains the trip id and reservation criteria  of the request.</param>
		/// <returns name="OTA_GroundResRetrieveRS">OTA_GroundResRetrieve response object contains cancellation amount,currency code of the cancelled trip.</returns>

		public OTA_GroundResRetrieveRS GroundCancelFee(OTA_GroundResRetrieveRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.GroundResRetrieve;
			OTA_GroundResRetrieveRS result = null;			
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain();
			var taxiDomain = new TaxiDomain();
			var sdsDomain = new SDSDomain(trackTime);			
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();			
			bool isAuthenricate = false;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }				
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_GroundCancelFeeRQ(input);
				#endregion

				#region Validate Trip+User
				if (input.Reference != null && input.Reference.Where(s => s.Type.ToLower() == Common.DTO.Enum.ConfirmationType.Confirmation.ToString().ToLower()).Any())
				{
					var tripID = input.Reference.Where(s => s.Type.ToLower() == Common.DTO.Enum.ConfirmationType.Confirmation.ToString().ToLower()).First().ID.ToInt64();
					if (!vtodDomain.ValidateTripUser(tripID, token.Username))
					{
						throw Common.DTO.VtodException.CreateException(ExceptionType.Validation, 13005);// ("token.Username"); //throw new ValidationException(Messages.Validation_Trip_UserName);					}
					}
				}
				#endregion

				#region Get FleetType & DispatchType
				#region FleetType
				var fleetType = input.TPA_Extensions.RateQualifiers.First().RateQualifierValue.ToFleetType();
				#endregion

				#region DispatchType
				var dispatchType = DispatchType.Unkown;

				if (fleetType == FleetType.Taxi)
				{
					dispatchType = DispatchType.Taxi;
				}
				else if (fleetType == Common.DTO.Enum.FleetType.SuperShuttle || fleetType == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly)
				{
					dispatchType = DispatchType.SDS;
				}
				else if (fleetType == Common.DTO.Enum.FleetType.ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttle_ExecuCar)
				{
					if (input.Reference != null && input.Reference.Any() && input.Reference.First().Type.Trim().ToLower() == Common.DTO.Enum.ConfirmationType.AgencyConfirmation.ToString().Trim().ToLower())
					{
						dispatchType = DispatchType.SDS;
					}
					else if (input.Reference != null && input.Reference.Any() && input.Reference.First().Type.Trim().ToLower() == Common.DTO.Enum.ConfirmationType.DispatchConfirmation.ToString().Trim().ToLower())
					{
						//In future if we need to have this for SDS also, we need to check DB by checking FleetTripCode. As of now (8/19/2015) it's just for Aleph reservation.
						dispatchType = DispatchType.ECar; //added by tanushri
					}
					else
					{
						dispatchType = vtodDomain.GetDispatchType(input.Reference.First().ID.ToInt64());
					}
				}
				#endregion
				#endregion

				#region Validate Token
				//if (fleetType == Common.DTO.Enum.FleetType.ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttle || fleetType == Common.DTO.Enum.FleetType.SuperShuttle_ExecuCar)
				//{
				validationDomain.Validate_SDS_Token(token);
				//}
				//else if (fleetType == Common.DTO.Enum.FleetType.Taxi)
				//{
				//	validationResult = validationDomain.Validate_Taxi_Token(token, out validationError);
				//	if (!validationResult)
				//		throw new ValidationException(validationError);
				//}
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				//if (fleetType == Common.DTO.Enum.FleetType.Taxi)
				//	isAuthenricate = membershipDomain.IsAuthenticate(token.Username, token.SecurityKey);
				//else
				//	isAuthenricate = true;
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion
                
				#region Call Domian Method				
				if (dispatchType == DispatchType.SDS)
				{
					result = sdsDomain.GroundCancelFee(token, input);

				}				
				else if (dispatchType == DispatchType.ECar)
				{
                    ECarDomain ecarDomain = new ECarDomain();
                    result = ecarDomain.GroundCancelFee(token, input);
				}

				else
				{
					throw VtodException.CreateException(ExceptionType.General, 5002); ;// new Exception(Messages.General_DispatchTypeError);
				}
				if (result == null)
				{
					throw VtodException.CreateException(ExceptionType.General, 1);
				}
				#endregion

				#region Modifying Response for zTrip ## it's hardcode but should be implemented in a better way !!!!
				if (token.Username.ToLower().Trim() == "ztrip")
				{
					if (result != null && result.TPA_Extensions != null && result.TPA_Extensions.Statuses != null && result.TPA_Extensions.Statuses.Status != null && result.TPA_Extensions.Statuses.Status.Any())
					{
						var status = result.TPA_Extensions.Statuses.Status.FirstOrDefault();
						if (result.TPA_Extensions.Vehicles != null && result.TPA_Extensions.Vehicles.Items != null && result.TPA_Extensions.Vehicles.Items.Any())
						{
							if (status != null && !string.IsNullOrWhiteSpace(status.Value) && status.Value.Trim().ToLower() == "locating driver")
							{
								result.TPA_Extensions.Vehicles.Items.RemoveAll(s => true);
							}
						}
					}
				}
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("GroundCancelFee", ex);

				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }					
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}
			}

			return result;
		}
		/// <summary>
		/// This method returns the pick up time of a booking request.
		/// </summary>
		/// <param name="input">GroundGetPickupTime request object contains the fare id,trip direction etc.</param>
		/// <returns name="GroundGetPickupTimeRS">GroundGetPickupTime response object contain the pick up start time and the end time.</returns>

		public GroundGetPickupTimeRS GroundGetPickupTime(GroundGetPickupTimeRQ input)
		{

			#region Init
			var methodName = Common.DTO.Enum.Method.GroundGetPickupTime;
			GroundGetPickupTimeRS result = null;			
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var sdsDomain = new SDSDomain(trackTime);
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();				
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_GroundGetPickupTime(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion
                				
				#region Call Domian Method
				result = sdsDomain.GetPickupTime(token, input);
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("GroundGetPickupTime", ex);

				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }					
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// This method calculates the Fees,Service charges and total charges of a trip.
		/// </summary>
		/// <param name="input">GroundGetFareScheduleWithFees request object contains the Inbound Fare ID, Outbound Fare ID and travel dates</param>
		/// <returns name="GroundGetFareScheduleWithFeesRS">GroundGetFareScheduleWithFees response object contains fees,service charges and the total charges of a trip.</returns>

		public GroundGetFareScheduleWithFeesRS GroundGetFareScheduleWithFees(GroundGetFareScheduleWithFeesRQ input)
		{

			#region Init
			var methodName = Common.DTO.Enum.Method.GroundGetFareScheduleWithFees;
			GroundGetFareScheduleWithFeesRS result = null;			
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var sdsDomain = new SDSDomain(trackTime);
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();				
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_GroundGetFareScheduleWithFees(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion
                				
				#region Call Domian Method
				result = sdsDomain.GroundGetFareScheduleWithFees(token, input);
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("GroundGetFareScheduleWithFees", ex);

				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }					
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}

        /// <summary>
        /// This method is used to retrieve final route details
        /// </summary>
        /// <param name="input">GetFinalRoute Request Object contains Trip ID whose geolocations needs to be retrieved.</param>
        /// <returns name="GetFinalRouteRS">GetFareDetail Response Object contains all the geolocations until the trip got completed.
        /// </returns>
        public OTA_GroundGetFinalRouteRS GroundGetFinalRoute(OTA_GroundGetFinalRouteRQ input)
        {
            #region Init
            OTA_GroundGetFinalRouteRS result = null;
            var methodName = Common.DTO.Enum.Method.GroundGetFinalRoute;            
            var rawRequest = string.Empty;
            var rawResponse = string.Empty;
            var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
            var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
            var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
            var sdsDomain = new SDSDomain(trackTime);
            var taxiDomain = new TaxiDomain(); taxiDomain.TrackTime = trackTime;
            var ecarDomain = new ECarDomain(); ecarDomain.TrackTime = trackTime;
            var vanDomain = new VanDomain(); vanDomain.TrackTime = trackTime;
            var validationDomain = new ValidationDomain();
            var token = new TokenRS();
            bool isAuthenricate = false;
            #endregion

            try
            {
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
                #endregion

                #region Log Request
                rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();                
                if (isDebug)
                {
                    logger.InfoFormat("Request{0}", rawRequest);
                }
                #endregion

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
                #endregion

                #region Modify Token
                Modification.ModifyTokenRS(ref token);
                #endregion

                #region Validate request object
                validationDomain.Validate_OTA_GroundGetFinalRouteRQ(input);
                #endregion

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
                #endregion

                #region Get FleetType & Recognizing Fleet

                #region Fleet Type
                var fleetType =input.TPA_Extensions.RateQualifiers.First().RateQualifierValue.ToFleetType();
                #endregion
                #endregion

                #region Validate Token
                validationDomain.Validate_SDS_Token(token);
                #endregion

                #region Authenticate
                isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
                if (!isAuthenricate)
                    throw VtodException.CreateException(ExceptionType.Auth, 101);
                #endregion
                                
                #region Call Domian Method
                if (fleetType == Common.DTO.Enum.FleetType.ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttle_ExecuCar)
                {
                    result = ecarDomain.GetFinalRoute(token, input);
                }
                else if (fleetType == Common.DTO.Enum.FleetType.Taxi)
                {
                    result = taxiDomain.GetFinalRoute(token, input);
                }
                else if (fleetType == Common.DTO.Enum.FleetType.Van)
                {
                    throw new NotImplementedException();
                }
                else if (fleetType == Common.DTO.Enum.FleetType.SuperShuttle || fleetType == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly)
                {
                    throw new NotImplementedException();
                }
                else
                {
                    throw Common.DTO.VtodException.CreateFieldRequiredValidationException("RateQualifierValue");
                }
                #endregion
                rawResponse = result.XmlSerialize().ToString();

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
                #endregion
            }
            catch (Exception ex)
            {
                logger.Error("GroundGetFinalRoute", ex);
               
                var exception = getOtaException(ex);
                rawResponse = exception.Errors.XmlSerialize().ToString();
                throw exception;
            }
            finally
            {
                try
                {
                    #region Log Reponse
                    if (isDebug)
                    {
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
                    #endregion

                    #region Track
                    trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
                    #endregion

                    #region SaveTrackTime
                    vtodDomain.InsertTrack(trackTime);
                    trackTime.Dispose();
                    #endregion
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }
            }

            return result;
        }
        #endregion
        #region Ground ASAP
        /// <summary>
        /// This is used to book a Trip ASAP.
        /// This can only be done in SDS.
        /// </summary>
        /// <param name="input">OTA_GroundAvail Request Object is used to specify the Pick Up/Drop Off details and also the Dispatch details like Taxi/Ecar etc.</param>
        /// <returns name="OTA_GroundAvailRS">OTA_GroundAvail Response Object contains the Trip ID  of the booking.</returns>
        public OTA_GroundAvailRS GroundCreateAsapRequest(OTA_GroundAvailRQ input)
		{
			#region Init
			OTA_GroundAvailRS result = null;
			var methodName = Common.DTO.Enum.Method.GroundCreateAsapRequest;			
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var sdsDomain = new SDSDomain(trackTime);
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();				
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_GroundCreateAsapRequestRQ(input);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region GetFleetType
				var fleetType = input.RateQualifiers.First().RateQualifierValue.ToFleetType();
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion
                
				#region Call Domian Method
				if (fleetType == Common.DTO.Enum.FleetType.ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttle || fleetType == Common.DTO.Enum.FleetType.SuperShuttle_ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly)
				{
					result = sdsDomain.GroundCreateAsapRequest(token, input);
				}
				else if (fleetType == Common.DTO.Enum.FleetType.Taxi)
				{
					throw new NotImplementedException();
				}
				if (result == null)
				{
					throw VtodException.CreateException(ExceptionType.General, 1);
				}
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}			
			catch (Exception ex)
			{
				logger.Error("GroundCreateAsapRequest", ex);				

				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }			
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}
			}

			return result;
		}
        /// <summary>
        /// This method is used to retrieve the status of a ASAP Request.
        /// </summary>
        /// <param name="input">GroundGetASAPRequestStatus Request Object contains the ASAP ID.</param>
        /// <returns name="GroundGetASAPRequestStatusRS">OTA_GroundAvail Response Object contains ApprovedPickupTime,Status like  Unknown,PickupDenied,OriginalPickupTimeApproved and New Pickup Time.</returns>
        public GroundGetASAPRequestStatusRS GroundGetASAPRequestStatus(GroundGetASAPRequestStatusRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.GroundGetASAPRequestStatus;
			GroundGetASAPRequestStatusRS result = null;			
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var sdsDomain = new SDSDomain(trackTime);
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_GroundGetASAPRequestStatus(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion
                
				#region Call Domian Method
				result = sdsDomain.GroundGetASAPRequestStatus(token, input);
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("GroundGetASAPRequestStatusRS", ex);
                
				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }					
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		///  This method is used to cancel a ASAP Request.
		/// </summary>
		/// <param name="input">GroundAbandonASAPRequest Request Object contains the ASAP ID</param>
		/// <returns name="GroundAbandonASAPRequestRS">GroundAbandonASAPRequest Response Object contains Success/Error.</returns>
		public GroundAbandonASAPRequestRS GroundAbandonASAPRequest(GroundAbandonASAPRequestRQ input)
		{

			#region Init
			var methodName = Common.DTO.Enum.Method.GroundAbandonASAPRequest;
			GroundAbandonASAPRequestRS result = null;			
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var sdsDomain = new SDSDomain(trackTime);
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();				
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_GroundAbandonASAPRequest(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion
                
				#region Call Domian Method
				result = sdsDomain.GroundAbandonASAPRequest(token, input);
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}		
			catch (Exception ex)
			{
				logger.Error("GroundAbandonASAPRequest", ex);
                
				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }					
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		#endregion

		#region Accounting
		/// <summary>
		/// Add a Credit Card to a Member
		/// </summary>
		/// <param name="input">AccountingAddMemberPaymentCard Request Object contains Member ID and Payment Card Details of a Member.</param>
		/// <returns name="AccountingAddMemberPaymentCardRS">AccountingAddMemberPaymentCard Response Object returns Success/Errors</returns>
		public AccountingAddMemberPaymentCardRS AccountingAddPaymentCard(AccountingAddMemberPaymentCardRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.CreateCreditCardAccount;
			AccountingAddMemberPaymentCardRS result = null;			
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var validationDomain = new ValidationDomain();
			var accountingDomain = new AccountingDomain(); accountingDomain.TrackTime = trackTime;
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion
			try
			{
                GetHeaderAndMakeToken(token);

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				//Not save credit card information
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n", token.XmlSerialize().ToString()).CleanSecurityKey();				
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_CreateCreditCardAccountRQ(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				//if (fleetType == Common.DTO.Enum.FleetType.Taxi)
				//	isAuthenricate = membershipDomain.IsAuthenticate(token.Username, token.SecurityKey);
				//else
				//	isAuthenricate = true;
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion
                
				#region Call Domian Method

				result = accountingDomain.AccountingAddPaymentCard(token, input);
				if (result == null)
				{
					throw VtodException.CreateException(ExceptionType.General, 1);
				}
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}			
			catch (Exception ex)
			{
				logger.Error("CreateCreditCardAccount", ex);
                
				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }					
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}

		/// <summary>
		/// Edit a Credit Card to a Member
		/// </summary>
		/// <param name="input">AccountingEditMemberPaymentCardRQ Request Object contains Member ID and Payment Card Details of a Member.</param>
		/// <returns name="AccountingEditMemberPaymentCardRS">AccountingEditMemberPaymentCard Response Object returns Success/Errors</returns>
		public AccountingEditMemberPaymentCardRS AccountingEditPaymentCard(AccountingEditMemberPaymentCardRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.EditCreateCreditCardAccount;
			AccountingEditMemberPaymentCardRS result = null;			
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var validationDomain = new ValidationDomain();
			var accountingDomain = new AccountingDomain(); accountingDomain.TrackTime = trackTime;
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion
			try
			{
                GetHeaderAndMakeToken(token);

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n", token.XmlSerialize().ToString()).CleanSecurityKey();				
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_EditCreateCreditCardAccountRQ(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion
                				
				#region Call Domian Method

				result = accountingDomain.AccountingEditPaymentCard(token, input);
				if (result == null)
				{
					throw VtodException.CreateException(ExceptionType.General, 1);
				}
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("AccountingEditPaymentCard", ex);
				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }					
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// Retrieves the list of Payment Card details for a Member
		/// </summary>
		/// <param name="input">AccountingGetMemberPaymentCards Request Object contains Member ID</param>
		/// <returns name="AccountingGetMemberPaymentCardsRS">AccountingGetMemberPaymentCards Response Object contains the card details like Card Number,Card Series Type,Address etc.</returns>
		public AccountingGetMemberPaymentCardsRS AccountingGetMemberPaymentCards(AccountingGetMemberPaymentCardsRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.GetCreditCardAccounts;
			AccountingGetMemberPaymentCardsRS result = null;			
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;			
			var validationDomain = new ValidationDomain();
			var accountingDomain = new AccountingDomain(); accountingDomain.TrackTime = trackTime;
			var token = new TokenRS();			
			bool isAuthenricate = false;
			#endregion
			try
			{
                GetHeaderAndMakeToken(token);

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();				
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_GetCreditCardAccountsRQ(input);
				#endregion

				#region Validate Token
				//if (fleetType == Common.DTO.Enum.FleetType.ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttle || fleetType == Common.DTO.Enum.FleetType.SuperShuttle_ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly)
				//{
				validationDomain.Validate_SDS_Token(token);
				//}
				//else if (fleetType == Common.DTO.Enum.FleetType.Taxi)
				//{
				//	validationResult = validationDomain.Validate_Taxi_Token(token, out validationError);
				//	if (!validationResult)
				//		throw new ValidationException(validationError);
				//}
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				//if (fleetType == Common.DTO.Enum.FleetType.Taxi)
				//	isAuthenricate = membershipDomain.IsAuthenticate(token.Username, token.SecurityKey);
				//else
				//	isAuthenricate = true;
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion
                				
				#region Call Domian Method
				result = accountingDomain.AccountingGetMemberPaymentCards(token, input);
				if (result == null)
				{
					throw VtodException.CreateException(ExceptionType.General, 1);
				}
				#endregion

				rawResponse = "Credit card information will not be save.";

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}			
			catch (Exception ex)
			{
				logger.Error("GetCreditCardAccounts", ex);
                				
				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }					
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// Set up a Payment Card as a default card to a Member.
		/// </summary>
		/// <param name="input">AccountingSetMemberDefaultPaymentCard Request Object contains Member ID and Account ID</param>
		/// <returns name="AccountingSetMemberDefaultPaymentCardRS">AccountingSetMemberDefaultPaymentCard Response Object returns Success/Errors</returns>
		public AccountingSetMemberDefaultPaymentCardRS AccountingSetMemberDefaultPaymentCard(AccountingSetMemberDefaultPaymentCardRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.SetDefaultCreditCardAccount;
			AccountingSetMemberDefaultPaymentCardRS result = null;			
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;			
			var validationDomain = new ValidationDomain();
			var accountingDomain = new AccountingDomain(); accountingDomain.TrackTime = trackTime;
			var token = new TokenRS();			
			bool isAuthenricate = false;
			#endregion
			try
			{
                GetHeaderAndMakeToken(token);

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();				
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_SetDefaultCreditCardAccountRQ(input);
				#endregion

				#region Validate Token
				//if (fleetType == Common.DTO.Enum.FleetType.ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttle || fleetType == Common.DTO.Enum.FleetType.SuperShuttle_ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly)
				//{
				validationDomain.Validate_SDS_Token(token);
				//}
				//else if (fleetType == Common.DTO.Enum.FleetType.Taxi)
				//{
				//	validationResult = validationDomain.Validate_Taxi_Token(token, out validationError);
				//	if (!validationResult)
				//		throw new ValidationException(validationError);
				//}
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				//if (fleetType == Common.DTO.Enum.FleetType.Taxi)
				//	isAuthenricate = membershipDomain.IsAuthenticate(token.Username, token.SecurityKey);
				//else
				//	isAuthenricate = true;
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion
                				
				#region Call Domian Method

				result = accountingDomain.AccountingSetMemberDefaultPaymentCard(token, input);
				if (result == null)
				{
					throw VtodException.CreateException(ExceptionType.General, 1);
				}
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}			
			catch (Exception ex)
			{
				logger.Error("SetDefaultCreditCardAccount", ex);
                
				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }					
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// This method is used to delete a payment card from a Member.
		/// </summary>
		/// <param name="input">AccountingDeleteMemberPaymentCard Request Object contains Member ID and Account ID.</param>
		/// <returns name="AccountingDeleteMemberPaymentCardRS">AccountingDeleteMemberPaymentCard Response Object returns Success/Errors</returns>
		public AccountingDeleteMemberPaymentCardRS AccountingDeleteMemberPaymentCard(AccountingDeleteMemberPaymentCardRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.DeleteCreditCardAccount;
			AccountingDeleteMemberPaymentCardRS result = null;			
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var sdsDomain = new SDSDomain(trackTime);
			var taxiDomain = new TaxiDomain();
			var validationDomain = new ValidationDomain();
			var accountingDomain = new AccountingDomain(); accountingDomain.TrackTime = trackTime;
			var token = new TokenRS();			
			bool isAuthenricate = false;
			#endregion
			try
			{
                GetHeaderAndMakeToken(token);

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();				
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_DeleteCreditCardAccountRQ(input);
                #endregion

                #region Validate Token
                validationDomain.Validate_SDS_Token(token);
                #endregion

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate				
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion
                				
				#region Call Domian Method
				result = accountingDomain.AccountingDeleteMemberPaymentCard(token, input);
				if (result == null)
				{
					throw VtodException.CreateException(ExceptionType.General, 1);
				}
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}			
			catch (Exception ex)
			{
				logger.Error("DeleteCreditCardAccount", ex);
                
				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }					
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// This method is used to add a Bill to a Member.
		/// </summary>
		/// <param name="input">AccountingAddMemberDirectBill Request Object contains Member ID and Billing Number.</param>
		/// <returns name="AccountingAddMemberDirectBillRS">AccountingAddMemberDirectBill Response Object contains Success/Error</returns>
		public AccountingAddMemberDirectBillRS AccountingAddMemberDirectBill(AccountingAddMemberDirectBillRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.AddDirectBillAccountToCustomerProfile;
			AccountingAddMemberDirectBillRS result = null;			
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;			
			var validationDomain = new ValidationDomain();
			var accountingDomain = new AccountingDomain(); accountingDomain.TrackTime = trackTime;
			var token = new TokenRS();			
			bool isAuthenricate = false;
			#endregion
			try
			{
                GetHeaderAndMakeToken(token);

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				//Not save credit card information
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n", token.XmlSerialize().ToString()).CleanSecurityKey();				
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_AddDirectBillAccountToCustomerProfileRQ(input);
				#endregion

				#region Validate Token
				//if (fleetType == Common.DTO.Enum.FleetType.ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttle || fleetType == Common.DTO.Enum.FleetType.SuperShuttle_ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly)
				//{
				validationDomain.Validate_SDS_Token(token);
				//}
				//else if (fleetType == Common.DTO.Enum.FleetType.Taxi)
				//{
				//	validationResult = validationDomain.Validate_Taxi_Token(token, out validationError);
				//	if (!validationResult)
				//		throw new ValidationException(validationError);
				//}
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				//if (fleetType == Common.DTO.Enum.FleetType.Taxi)
				//	isAuthenricate = membershipDomain.IsAuthenticate(token.Username, token.SecurityKey);
				//else
				//	isAuthenricate = true;
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion
                				
				#region Call Domian Method
				result = accountingDomain.AccountingAddMemberDirectBill(token, input);
				if (result == null)
				{
					throw VtodException.CreateException(ExceptionType.General, 1);
				}
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}			
			catch (Exception ex)
			{
				logger.Error("AddDirectBillAccountToCustomerProfile", ex);
                				
				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }					
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// This method is used to retrieve the Bill Details of a Member.
		/// </summary>
		/// <param name="input">AccountingGetMemberDirectBills Request Object contains the Member ID</param>
		/// <returns name="AccountingGetMemberDirectBillsRS">AccountingGetMemberDirectBills Response Object contains Billing Details like Billing Number,Company Name etc.</returns>
		public AccountingGetMemberDirectBillsRS AccountingGetMemberDirectBills(AccountingGetMemberDirectBillsRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.GetDirectBillAccounts;
			AccountingGetMemberDirectBillsRS result = null;			
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;			
			var validationDomain = new ValidationDomain();
			var accountingDomain = new AccountingDomain(); accountingDomain.TrackTime = trackTime;
			var token = new TokenRS();		
			bool isAuthenricate = false;
			#endregion
			try
			{
                GetHeaderAndMakeToken(token);

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_GetDirectBillAccountsRQ(input);
				#endregion

				#region Validate Token
				//if (fleetType == Common.DTO.Enum.FleetType.ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttle || fleetType == Common.DTO.Enum.FleetType.SuperShuttle_ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly)
				//{
				validationDomain.Validate_SDS_Token(token);
				//}
				//else if (fleetType == Common.DTO.Enum.FleetType.Taxi)
				//{
				//	validationResult = validationDomain.Validate_Taxi_Token(token, out validationError);
				//	if (!validationResult)
				//		throw new ValidationException(validationError);
				//}
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				//if (fleetType == Common.DTO.Enum.FleetType.Taxi)
				//	isAuthenricate = membershipDomain.IsAuthenticate(token.Username, token.SecurityKey);
				//else
				//	isAuthenricate = true;
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion
                				
				#region Call Domian Method
				result = accountingDomain.AccountingGetMemberDirectBills(token, input);
				if (result == null)
				{
					throw VtodException.CreateException(ExceptionType.General, 1);
				}
				#endregion

				rawResponse = "Direct bill information will not be save.";

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}			
			catch (Exception ex)
			{
				logger.Error("GetDirectBillAccounts", ex);
                
				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }					
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// This method is used to set up a Direct Bill as a Default to a Member
		/// </summary>
		/// <param name="input">AccountingSetMemberDefaultDirectBill Request Object contains Member ID and Account ID</param>
		/// <returns name="AccountingSetMemberDefaultDirectBillRS">AccountingSetMemberDefaultDirectBill Response Object returns Success/Errors</returns>
		public AccountingSetMemberDefaultDirectBillRS AccountingSetMemberDefaultDirectBill(AccountingSetMemberDefaultDirectBillRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.SetDefaultDirectBillAccount;
			AccountingSetMemberDefaultDirectBillRS result = null;			
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;			
			var validationDomain = new ValidationDomain();
			var accountingDomain = new AccountingDomain(); accountingDomain.TrackTime = trackTime;
			var token = new TokenRS();			
			bool isAuthenricate = false;			
			#endregion
			try
			{
                GetHeaderAndMakeToken(token);

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_SetDefaultDirectBillAccountRQ(input);
				#endregion

				#region Validate Token
				//if (fleetType == Common.DTO.Enum.FleetType.ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttle || fleetType == Common.DTO.Enum.FleetType.SuperShuttle_ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly)
				//{
				validationDomain.Validate_SDS_Token(token);
				//}
				//else if (fleetType == Common.DTO.Enum.FleetType.Taxi)
				//{
				//	validationResult = validationDomain.Validate_Taxi_Token(token, out validationError);
				//	if (!validationResult)
				//		throw new ValidationException(validationError);
				//}
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				//if (fleetType == Common.DTO.Enum.FleetType.Taxi)
				//	isAuthenricate = membershipDomain.IsAuthenticate(token.Username, token.SecurityKey);
				//else
				//	isAuthenricate = true;
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion
                
				#region Call Domian Method
				result = accountingDomain.AccountingSetMemberDefaultDirectBill(token, input);
				if (result == null)
				{
					throw VtodException.CreateException(ExceptionType.General, 1);
				}
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}			
			catch (Exception ex)
			{
				logger.Error("SetDefaultDirectBillAccount", ex);
                				
				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// This method is used to delete a Direct Bill from a  Member
		/// </summary>
		/// <param name="input">AccountingDeleteMemberDirectBill Request Object contains Member ID and Account ID which needs to be deleted.</param>
		/// <returns name="AccountingDeleteMemberDirectBillRS">AccountingDeleteMemberDirectBill Response Object returns Success/Errors</returns>
		public AccountingDeleteMemberDirectBillRS AccountingDeleteMemberDirectBill(AccountingDeleteMemberDirectBillRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.DeleteDirectBillAccount;
			AccountingDeleteMemberDirectBillRS result = null;			
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;			
			var validationDomain = new ValidationDomain();
			var accountingDomain = new AccountingDomain(); accountingDomain.TrackTime = trackTime;
			var token = new TokenRS();		
			bool isAuthenricate = false;			
			#endregion
			try
			{
                GetHeaderAndMakeToken(token);

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();				
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_DeleteDirectBillAccountRQ(input);
				#endregion

				#region Validate Token
				//if (fleetType == Common.DTO.Enum.FleetType.ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttle || fleetType == Common.DTO.Enum.FleetType.SuperShuttle_ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly)
				//{
				validationDomain.Validate_SDS_Token(token);
				//}
				//else if (fleetType == Common.DTO.Enum.FleetType.Taxi)
				//{
				//	validationResult = validationDomain.Validate_Taxi_Token(token, out validationError);
				//	if (!validationResult)
				//		throw new ValidationException(validationError);
				//}
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				//if (fleetType == Common.DTO.Enum.FleetType.Taxi)
				//	isAuthenricate = membershipDomain.IsAuthenticate(token.Username, token.SecurityKey);
				//else
				//	isAuthenricate = true;
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion
                				
				#region Call Domian Method
				result = accountingDomain.AccountingDeleteMemberDirectBill(token, input);
				if (result == null)
				{
					throw VtodException.CreateException(ExceptionType.General, 1);
				}
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}			
			catch (Exception ex)
			{
				logger.Error("DeleteDirectBillAccount", ex);
                
				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }					
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// This method is used to retrieve fare details
		/// </summary>
		/// <param name="input">GetFareDetail Request Object contains status of a Trip and the Trip ID whose fare details needs to be retrieved.</param>
		/// <returns name="GetFareDetailRS">GetFareDetail Response Object contains Trip Details like DropOffLocation/Pick Up Location.
		/// Fare Items like Gratuity,Fare,Fuel Surcharge,Discount Amount etc.</returns>
		public GetFareDetailRS GetFareDetail(GetFareDetailRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.GetFareDetail;
			GetFareDetailRS result = null;			
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;			
			var taxiDomain = new TaxiDomain();
            var vanDomain = new VanDomain();
			var validationDomain = new ValidationDomain();
			var accountingDomain = new AccountingDomain(); accountingDomain.TrackTime = trackTime;
			var token = new TokenRS();			
			bool isAuthenricate = false;
			#endregion
			try
			{
                GetHeaderAndMakeToken(token);

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();				
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_GetFareDetailRQ(input);
				#endregion

				#region GetFleetType
				var fleetType = input.TPA_Extensions.RateQualifiers.First().RateQualifierValue.ToFleetType();
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion
                				
				#region Call Domian Method
				if (fleetType == Common.DTO.Enum.FleetType.ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttle || fleetType == Common.DTO.Enum.FleetType.SuperShuttle_ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly)
				{
					result = accountingDomain.GetFareDetail(token, input);
				}
				else if (fleetType == Common.DTO.Enum.FleetType.Taxi)
				{
					result = taxiDomain.GetFareDetail(token, input);
				}
                else if (fleetType == Common.DTO.Enum.FleetType.Van)
                {
                    result = vanDomain.GetFareDetail(token, input);
                }
				if (result == null)
				{
					throw VtodException.CreateException(ExceptionType.General, 1);
				}
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}			
			catch (Exception ex)
			{
				logger.Error("GetFareDetail", ex);

				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }					
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}

		#endregion

		#region zTrip Credit
		/// <summary>
		/// This method activates the credit card of a Member.
		/// </summary>
		/// <param name="input">ZTripCreditActivateMemberCredit request object contains member ID and the activation code of the member</param>
		/// <returns name="ZTripCreditActivateMemberCreditRS">ZTripCreditActivateMemberCredit response object contains Activation message and Success/Error Object</returns>
		public ZTripCreditActivateMemberCreditRS ZTripCreditActivateMemberCredit(ZTripCreditActivateMemberCreditRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.ActivateCredits;
			ZTripCreditActivateMemberCreditRS result = null;			
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;			
			var validationDomain = new ValidationDomain();
			var accountingDomain = new AccountingDomain(); accountingDomain.TrackTime = trackTime;
			var token = new TokenRS();			
			bool isAuthenricate = false;
			#endregion
			try
			{
                GetHeaderAndMakeToken(token);

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();				
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_ActivateCreditsRQ(input);
				#endregion

				#region Validate Token
				//if (fleetType == Common.DTO.Enum.FleetType.ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttle || fleetType == Common.DTO.Enum.FleetType.SuperShuttle_ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly)
				//{
				validationDomain.Validate_SDS_Token(token);
				//}
				//else if (fleetType == Common.DTO.Enum.FleetType.Taxi)
				//{
				//	validationResult = validationDomain.Validate_Taxi_Token(token, out validationError);
				//	if (!validationResult)
				//		throw new ValidationException(validationError);
				//}
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				//if (fleetType == Common.DTO.Enum.FleetType.Taxi)
				//	isAuthenricate = membershipDomain.IsAuthenticate(token.Username, token.SecurityKey);
				//else
				//	isAuthenricate = true;
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
                #endregion
                                
                #region Call Domian Method after checking ztrip credit restriction if the option is enabled.
                string configured = System.Configuration.ConfigurationManager.AppSettings["EnableZtripRestriction"];
                bool isZtripRestrictionEnabled;
                if (!bool.TryParse(configured, out isZtripRestrictionEnabled))
                {
                    isZtripRestrictionEnabled = false;
                }

                if (isZtripRestrictionEnabled)
                {
                    using (var db = new VTODEntities())
                    {
                        bool hasBeenBooked = db.vtod_member_info.Any(x => x.SDSMemberId.Equals(input.MemberId) && x.HasBeenBooked);
                        bool hasCompletedTrip = db.vtod_trip.Any(x => x.MemberID.Equals(input.MemberId.ToString()) && x.FinalStatus.Equals("Completed", StringComparison.OrdinalIgnoreCase));
                        if (hasBeenBooked || hasCompletedTrip)
                        {
                            throw VtodException.CreateException(ExceptionType.Accounting, 8013);
                        }
                    }
                }
                

                result = accountingDomain.ZTripCreditActivateMemberCredit(token, input);
				if (result == null)
				{
					throw VtodException.CreateException(ExceptionType.General, 1);
				}
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}			
			catch (Exception ex)
			{
				logger.Error("ActivateCredits", ex);

				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }					
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// It returns the member's Credit Balance
		/// </summary>
		/// <param name="input">ZTripCreditGetMemberBalance request object contains the Member ID to retrieve the Credit balance</param>
		/// <returns name="ZTripCreditGetMemberBalanceRS">ZTripCreditGetMemberBalance response object contains the Balance and Success/Error Object</returns>
		public ZTripCreditGetMemberBalanceRS ZTripCreditGetMemberBalance(ZTripCreditGetMemberBalanceRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.GetCreditsBalance;
			ZTripCreditGetMemberBalanceRS result = null;			
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;			
			var validationDomain = new ValidationDomain();
			var accountingDomain = new AccountingDomain(); accountingDomain.TrackTime = trackTime;
			var token = new TokenRS();			
			bool isAuthenricate = false;
			#endregion
			try
			{
                GetHeaderAndMakeToken(token);

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_GetCreditsBalanceRQ(input);
				#endregion

				#region Validate Token
				//if (fleetType == Common.DTO.Enum.FleetType.ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttle || fleetType == Common.DTO.Enum.FleetType.SuperShuttle_ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly)
				//{
				validationDomain.Validate_SDS_Token(token);
				//}
				//else if (fleetType == Common.DTO.Enum.FleetType.Taxi)
				//{
				//	validationResult = validationDomain.Validate_Taxi_Token(token, out validationError);
				//	if (!validationResult)
				//		throw new ValidationException(validationError);
				//}
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				//if (fleetType == Common.DTO.Enum.FleetType.Taxi)
				//	isAuthenricate = membershipDomain.IsAuthenticate(token.Username, token.SecurityKey);
				//else
				//	isAuthenricate = true;
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion
                				
				#region Call Domian Method
				result = accountingDomain.ZTripCreditGetMemberBalance(token, input);
				if (result == null)
				{
					throw VtodException.CreateException(ExceptionType.General, 1);
				}
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}			
			catch (Exception ex)
			{
				logger.Error("GetCreditsBalance", ex);

				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		#endregion

		#region Reward
		/// <summary>
		/// This method retrieves the list of Reward Program of All Airlines
		/// </summary>
		/// <param name="input">RewardGetAirlinePrograms Request Object sends a request with a Token.</param>
		/// <returns name="RewardGetAirlineProgramsRS">RewardGetAirlinePrograms Response Object retrieves a list of Reward ID and Program Name</returns>
		public RewardGetAirlineProgramsRS GetAirlineRewardsPrograms(RewardGetAirlineProgramsRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.GetAirlineRewardsPrograms;
			RewardGetAirlineProgramsRS result = null;			
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;			
			var validationDomain = new ValidationDomain();
			var accountingDomain = new AccountingDomain(); accountingDomain.TrackTime = trackTime;
			var token = new TokenRS();			
			bool isAuthenricate = false;
			#endregion
			try
			{
                GetHeaderAndMakeToken(token);

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();				
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_GetAirlineRewardsProgramsRQ(input);
				#endregion

				#region Validate Token
				//if (fleetType == Common.DTO.Enum.FleetType.ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttle || fleetType == Common.DTO.Enum.FleetType.SuperShuttle_ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly)
				//{
				validationDomain.Validate_SDS_Token(token);
				//}
				//else if (fleetType == Common.DTO.Enum.FleetType.Taxi)
				//{
				//	validationResult = validationDomain.Validate_Taxi_Token(token, out validationError);
				//	if (!validationResult)
				//		throw new ValidationException(validationError);
				//}
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				//if (fleetType == Common.DTO.Enum.FleetType.Taxi)
				//	isAuthenricate = membershipDomain.IsAuthenticate(token.Username, token.SecurityKey);
				//else
				//	isAuthenricate = true;
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion
                				
				#region Call Domian Method
				result = accountingDomain.GetAirlineRewardsPrograms(token, input);
				if (result == null)
				{
					throw VtodException.CreateException(ExceptionType.General, 1);
				}
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}			
			catch (Exception ex)
			{
				logger.Error("GetAirlineRewardsPrograms", ex);

				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// This method is used to Create a Member in the Reward Program
		/// </summary>
		/// <param name="input">RewardCreateMemberAccount Request Object contains the Member ID,Reward ID and the account number to create a member</param>
		/// <returns name="RewardCreateMemberAccountRS">RewardCreateMemberAccount Response Object returns Success/Error</returns>
		public RewardCreateMemberAccountRS RewardCreateMemberAccount(RewardCreateMemberAccountRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.CreateMembershipMileageAccount;
			RewardCreateMemberAccountRS result = null;			
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;			
			var validationDomain = new ValidationDomain();
			var accountingDomain = new AccountingDomain(); accountingDomain.TrackTime = trackTime;
			var token = new TokenRS();			
			bool isAuthenricate = false;
			#endregion
			try
			{
                GetHeaderAndMakeToken(token);

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_CreateMembershipMileageAccountRQ(input);
				#endregion

				#region Validate Token
				//if (fleetType == Common.DTO.Enum.FleetType.ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttle || fleetType == Common.DTO.Enum.FleetType.SuperShuttle_ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly)
				//{
				validationDomain.Validate_SDS_Token(token);
				//}
				//else if (fleetType == Common.DTO.Enum.FleetType.Taxi)
				//{
				//	validationResult = validationDomain.Validate_Taxi_Token(token, out validationError);
				//	if (!validationResult)
				//		throw new ValidationException(validationError);
				//}
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				//if (fleetType == Common.DTO.Enum.FleetType.Taxi)
				//	isAuthenricate = membershipDomain.IsAuthenticate(token.Username, token.SecurityKey);
				//else
				//	isAuthenricate = true;
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion
                				

				#region Call Domian Method
				result = accountingDomain.CreateMembershipMileageAccount(token, input);
				if (result == null)
				{
					throw VtodException.CreateException(ExceptionType.General, 1);
				}
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}			
			catch (Exception ex)
			{
				logger.Error("CreateMembershipMileageAccount", ex);

				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// This method is used to Delete a Member in the Reward Program
		/// </summary>
		/// <param name="input">RewardDeleteMemberAccount Request Object contains the Member ID and the account id to delete a member</param>
		/// <returns name="RewardDeleteMemberAccountRS">RewardDeleteMemberAccountRS Response Object returns Success or Error.</returns>
		public RewardDeleteMemberAccountRS RewardDeleteMemberAccount(RewardDeleteMemberAccountRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.DeleteMembershipMileageAccount;
			RewardDeleteMemberAccountRS result = null;			
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;			
			var validationDomain = new ValidationDomain();
			var accountingDomain = new AccountingDomain(); accountingDomain.TrackTime = trackTime;
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion
			try
			{
                GetHeaderAndMakeToken(token);

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_DeleteMembershipMileageAccountRQ(input);
				#endregion

				#region Validate Token
				//if (fleetType == Common.DTO.Enum.FleetType.ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttle || fleetType == Common.DTO.Enum.FleetType.SuperShuttle_ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly)
				//{
				validationDomain.Validate_SDS_Token(token);
				//}
				//else if (fleetType == Common.DTO.Enum.FleetType.Taxi)
				//{
				//	validationResult = validationDomain.Validate_Taxi_Token(token, out validationError);
				//	if (!validationResult)
				//		throw new ValidationException(validationError);
				//}
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				//if (fleetType == Common.DTO.Enum.FleetType.Taxi)
				//	isAuthenricate = membershipDomain.IsAuthenticate(token.Username, token.SecurityKey);
				//else
				//	isAuthenricate = true;
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion
                
				#region Call Domian Method
				result = accountingDomain.RewardDeleteMemberAccount(token, input);
				if (result == null)
				{
					throw VtodException.CreateException(ExceptionType.General, 1);
				}
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}			
			catch (Exception ex)
			{
				logger.Error("DeleteMembershipMileageAccount", ex);
                				
				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// This method is used to retrieve the details of the Reward Account.
		/// </summary>
		/// <param name="input">RewardGetMemberAccounts Request Object contains the Member ID to fetch the Reward Details.</param>
		/// <returns name="RewardGetMemberAccountsRS">RewardGetMemberAccounts Response Object contains the list of the Airlines Rewards for that member.</returns>
		public RewardGetMemberAccountsRS RewardGetMemberAccounts(RewardGetMemberAccountsRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.GetMembershipAirlineMileageAccounts;
			RewardGetMemberAccountsRS result = null;			
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;			
			var validationDomain = new ValidationDomain();
			var accountingDomain = new AccountingDomain(); accountingDomain.TrackTime = trackTime;
			var token = new TokenRS();			
			bool isAuthenricate = false;
			#endregion
			try
			{
                GetHeaderAndMakeToken(token);

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_GetMembershipAirlineMileageAccountsRQ(input);
				#endregion

				#region Validate Token
				//if (fleetType == Common.DTO.Enum.FleetType.ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttle || fleetType == Common.DTO.Enum.FleetType.SuperShuttle_ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly)
				//{
				validationDomain.Validate_SDS_Token(token);
				//}
				//else if (fleetType == Common.DTO.Enum.FleetType.Taxi)
				//{
				//	validationResult = validationDomain.Validate_Taxi_Token(token, out validationError);
				//	if (!validationResult)
				//		throw new ValidationException(validationError);
				//}
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				//if (fleetType == Common.DTO.Enum.FleetType.Taxi)
				//	isAuthenricate = membershipDomain.IsAuthenticate(token.Username, token.SecurityKey);
				//else
				//	isAuthenricate = true;
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion
                
				#region Call Domian Method
				result = accountingDomain.RewardGetMemberAccounts(token, input);
				if (result == null)
				{
					throw VtodException.CreateException(ExceptionType.General, 1);
				}
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}			
			catch (Exception ex)
			{
				logger.Error("GetMembershipAirlineMileageAccounts", ex);
                
				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// This method is used to set up a default account for a member.
		/// </summary>
		/// <param name="input">RewardSetDefaultAccount Request Object contains Member ID and Account ID to set up an account.</param>
		/// <returns name="RewardSetDefaultAccountRS">RewardSetDefaultAccount Response Object contains Success or Error.</returns>
		public RewardSetDefaultAccountRS RewardSetDefaultAccount(RewardSetDefaultAccountRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.RewardSetDefaultAccount;
			RewardSetDefaultAccountRS result = null;			
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;			
			var validationDomain = new ValidationDomain();
			var accountingDomain = new AccountingDomain(); accountingDomain.TrackTime = trackTime;
			var token = new TokenRS();			
			bool isAuthenricate = false;
			#endregion
			try
			{
                GetHeaderAndMakeToken(token);

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_RewardSetDefaultAccountRQ(input);
				#endregion

				#region Validate Token
				//if (fleetType == Common.DTO.Enum.FleetType.ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttle || fleetType == Common.DTO.Enum.FleetType.SuperShuttle_ExecuCar || fleetType == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly)
				//{
				validationDomain.Validate_SDS_Token(token);
				//}
				//else if (fleetType == Common.DTO.Enum.FleetType.Taxi)
				//{
				//	validationResult = validationDomain.Validate_Taxi_Token(token, out validationError);
				//	if (!validationResult)
				//		throw new ValidationException(validationError);
				//}
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				//if (fleetType == Common.DTO.Enum.FleetType.Taxi)
				//	isAuthenricate = membershipDomain.IsAuthenticate(token.Username, token.SecurityKey);
				//else
				//	isAuthenricate = true;
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

				#region Call Domian Method
				result = accountingDomain.RewardSetDefaultAccount(token, input);
				if (result == null)
				{
					throw VtodException.CreateException(ExceptionType.General, 1);
				}
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}			
			catch (Exception ex)
			{
				logger.Error("RewardSetDefaultAccount", ex);
                				
				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}


		/// <summary>
		/// This method is used to modify the airline rewards
		/// </summary>
		/// <param name="input">RewardModifyMemberAccount Request Object contains Member ID and Account ID to set up an account.</param>
		/// <returns name="RewardSetDefaultAccountRS">RewardSetDefaultAccount Response Object contains Success or Error.</returns>
		public RewardModifyMemberAccountRS RewardModifyMemberAccount(RewardModifyMemberAccountRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.ModifyMembershipMileageAccount;
			RewardModifyMemberAccountRS result = null;			
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var validationDomain = new ValidationDomain();
			var accountingDomain = new AccountingDomain(); accountingDomain.TrackTime = trackTime;
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion
			try
			{
                GetHeaderAndMakeToken(token);

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();				
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_ModifyMembershipMileageAccountRQ(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

				#region Call Domian Method
				result = accountingDomain.RewardModifyMemberAccount(token, input);
				if (result == null)
				{
					throw VtodException.CreateException(ExceptionType.General, 1);
				}
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("RewardModifyMemberAccount", ex);
				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		#endregion

		#region Membership
		/// <summary>
		/// Returns The TokenResponse Object.It takes the Username and Password and returns a token object.Token Object is valid for 15 mins if it is not used and 1 hour if it is used.
		/// </summary>
		/// <param name="token">This is a Token Request Object where User sends the IP Address,Username and the Password</param>
		/// <returns name="TokenRS">Returns a Token Response Object with success/error</returns>

		public TokenRS GetToken(TokenRQ token)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.GetToken;
			TokenRS result = null;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain();
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var sdsDomain = new SDSDomain(trackTime);
			var validationDomain = new ValidationDomain();			
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
            
			var contextErrorCode = string.Empty;
			#endregion

			try
			{
				#region Log Request
				if (System.ServiceModel.OperationContext.Current != null)
				{
					rawRequest = System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString().CleanPassword();					
				}
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify token - assign ip and client type
				Modification.ModifyTokenRQ(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_GetToken(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion
				
				#region Call Domain Method
				result = sdsDomain.GetToken(token);
				if (result == null)
				{
					throw VtodException.CreateException(ExceptionType.General, 1);
				}
				#endregion

				rawResponse = result.XmlSerialize().ToString().CleanSecurityKey();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}			
			catch (Exception ex)
			{
				logger.Error("GetToken", ex);

				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				#region Log Reponse
				try
				{
					#region Log
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }					
					#endregion

					#region Modify token - remove ip and client type
					if (result != null)
					{
						Modification.ModifyTokenRSForConsumer(ref result);
					}
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}
				#endregion
			}

			return result;

		}

		/// <summary>
		/// Create a Member.It uses member information like Name,Home Airport,Birthdate,Email ID to create a member.
		/// </summary>
		/// <param name="input">MemberCreate RequestObject</param>
		/// <returns  name="MemberCreateRS">Returns a MemberCreate ResponseObject with a MemberID and status as Success/Error</returns>
		public MemberCreateRS MemberCreate(MemberCreateRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.MemberCreate;
			MemberCreateRS result = null;			
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;			
			var vtodUtility = new VTODUtilityDomain(); vtodUtility.TrackTime = trackTime;			
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();
			bool isAuthenricate = false;			
			string phoneNumber = string.Empty;
			int phoneVerificationEnabled = System.Configuration.ConfigurationManager.AppSettings["EnablePhoneVerification"].ToInt32();
			int memberID = 0;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();				
				if (isDebug)
				{
					logger.InfoFormat("Request{0}", rawRequest);
				}
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_CreateMembership(token, input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

                #region Remove the leading zeros from the phone number
                if (input.MembershipRecord.Telephone != null && input.MembershipRecord.Telephone.CountryAccessCode != null)
                    input.MembershipRecord.Telephone.CountryAccessCode = input.MembershipRecord.Telephone.CountryAccessCode.TrimStart(new Char[] { '0' });
                #endregion

				#region Call Domian Method
				#region Corporates init
				var corporateUserID = input.MembershipRecord.EmailAddress;
				string corporateKey = null;
				List<CorporateName> corporateNames = null;
				CorporateName corporateName = CorporateName.Unknown;
				bool isRegisterInCorporate = false;
				#endregion

				#region Get Corporates for users
				//This should be a list of corporate things to push to SDS, but as SDS method accepts just one corporate, we get the first in the list
				corporateNames = membershipDomain.GetCorporateNames(input.MembershipRecord.EmailAddress);
				if (corporateNames != null && corporateNames.Any())
				{
					corporateName = corporateNames.First();
				}

				#endregion

               
                #region Register in SDS membership
                #region Add corporate to the request
                if (corporateName != CorporateName.Unknown)
				{
					corporateKey = Guid.NewGuid().ToString();
					input.CorporateAccount = new CorporateAccount { Corporation = corporateName.ToString(), UserKey = corporateKey, UserID = corporateUserID };
				}
				#endregion
                
				#region Validating Duplicate Phone Number
				bool? isphoneTaken;
				if (phoneVerificationEnabled == 1)
				{
					if (input.TPA_Extensions != null && input.TPA_Extensions.PhoneVerificationRequired)
					{
						#region PhoneNumbers from Request Object
						if (input.MembershipRecord.Telephone != null && !string.IsNullOrEmpty(input.MembershipRecord.Telephone.PhoneNumber))
						{
							phoneNumber = string.Format("{0}{1}{2}", input.MembershipRecord.Telephone.CountryAccessCode, input.MembershipRecord.Telephone.AreaCityCode, input.MembershipRecord.Telephone.PhoneNumber);
							logger.InfoFormat("Phone Number:{0}:", phoneNumber);
						}
						isphoneTaken = vtodUtility.RegistrationPhoneVerificationStatus(token, phoneNumber);
						if (isphoneTaken == true)
						{
							logger.InfoFormat("Exception Code : 9010");
							throw VtodException.CreateException(VTOD.Common.DTO.Enum.ExceptionType.Utility, 9010);
						}
						#endregion
					}
				}
				#endregion
				result = membershipDomain.MemberCreate(token, input);
                #endregion

                #region Register in Corporate
                //This should be a list of corporateName and doing foreach, but as I mentioned above, SDS accepts just one corporate
                if (corporateName != CorporateName.Unknown)
				{
                    membershipDomain.DeleteCorporateUser(corporateName, corporateUserID, string.Empty);
					isRegisterInCorporate = membershipDomain.RegisterCorporateUser(corporateName, corporateUserID, corporateKey);
				}
				#endregion

				#region GetCorporateProfile
				//This should be a list of corporateName and doing foreach, but as I mentioned above, SDS accepts just one corporate
				if (isRegisterInCorporate)
				{
					if (corporateName != CorporateName.Unknown)
					{
						result.Corporates = new Corporates();
						result.Corporates.CorporateList = new List<Corporate>();

						var corporateResult = membershipDomain.GetCorporate(corporateName, corporateUserID, corporateKey);
						if (corporateResult != null)
						{
							result.Corporates.CorporateList.Add(corporateResult);
						}
					}
				}
				#endregion

				
				#endregion

				#region VerifyPhone
				if (phoneVerificationEnabled == 1)
				{
					logger.InfoFormat("Start-Verify Phone");

					var isPhoneVerified = false;

					if (input.TPA_Extensions != null && input.TPA_Extensions.PhoneVerificationRequired)
					{
						#region PhoneNumbers from Request Object
						if (input.MembershipRecord.Telephone != null && !string.IsNullOrEmpty(input.MembershipRecord.Telephone.PhoneNumber))
						{
							logger.InfoFormat("Phone Number:{0}:", phoneNumber);
							phoneNumber = string.Format("{0}{1}{2}", input.MembershipRecord.Telephone.CountryAccessCode, input.MembershipRecord.Telephone.AreaCityCode, input.MembershipRecord.Telephone.PhoneNumber);
						}
						#endregion

						#region MemberID from Response Object
						if (result != null)
						{
							if (result.MemberID > 0)
							{
								logger.InfoFormat("Member ID:{0}:", memberID);
								memberID = result.MemberID;
							}
							#endregion

							#region Call Domain Method
							logger.InfoFormat("Phone NUmber:{0},MemberID:{1}:", phoneNumber, memberID);
							isPhoneVerified = vtodUtility.IsPhoneVerified(token, phoneNumber, memberID);
							logger.InfoFormat("Status of IsphoneVerified:{0}:", isPhoneVerified);
							logger.InfoFormat("Start Web Service Method Verify Phone");
							if (isPhoneVerified == false)
							{
								logger.InfoFormat("Phone Number:{0},MemberID:{1}:", phoneNumber, memberID);
                                try
                                {
                                    if (input.TPA_Extensions.IsPhoneVerifyByCode == true)
                                    {
                                        vtodUtility.VerifyPhone(token, phoneNumber, memberID, "code");
                                    }
                                    else
                                    {
                                        vtodUtility.VerifyPhone(token, phoneNumber, memberID,null);
                                    }
                                }
                                catch
                                {
                                    //vtodUtility.VerifyPhone(token, phoneNumber, memberID, null);
                                }
							}
							else
							{
								logger.InfoFormat("Phone number is already verified");
								//throw VtodException.CreateException(ExceptionType.Auth, 104);
							}
							#endregion
						}
						else
						{
							logger.InfoFormat("MemberID does not exists");
						}
					}
					logger.InfoFormat("End Web Service Method Verify Phone");
				}
				#endregion

                #region Sync zTrip phone and Device to Corporate
                try
                {
					if (result != null)
                   {
                       bool isRegisterCorporateDevice = false;
						if (result.Corporates != null && result.Corporates.CorporateList != null && result.Corporates.CorporateList.Any())
                       {
                           if (input.MembershipRecord.Telephone != null && !string.IsNullOrEmpty(input.MembershipRecord.Telephone.PhoneNumber))
                           {
                               phoneNumber = string.Format("{0}{1}{2}", input.MembershipRecord.Telephone.CountryAccessCode, input.MembershipRecord.Telephone.AreaCityCode, input.MembershipRecord.Telephone.PhoneNumber);
                           }
                           isRegisterCorporateDevice = membershipDomain.RegisterCorporateDeviceName(token, corporateName, corporateUserID, corporateKey, input.TPA_Extensions.DeviceToken, input.TPA_Extensions.Device, input.TPA_Extensions.DeviceVersion, phoneNumber, result.MemberID);
                       }
                        logger.ErrorFormat("Device registered:{0}:", isRegisterCorporateDevice);
                   }
                }
                catch (Exception ex)
                {
                    logger.ErrorFormat("SyncCorporate failed: {0}", ex);
                }
                #endregion
				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}			
			catch (Exception ex)
			{
				logger.Error("CreateMembership", ex);
                				
				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }                  	
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}

		/// <summary>
		/// Add a location to a Member.Based on the Address and Member ID supplied it will add a location to the member.
		/// </summary>
		/// <param name="input">MemberAddLocation Request Object</param>
		/// <returns name="MemberAddLocationRS">Returns a MemberAddLocation Response Object with Success/Error</returns>
		public MemberAddLocationRS MemberAddLocation(MemberAddLocationRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.MemberAddLocation;
			MemberAddLocationRS result = null;			
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;			
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();			
			bool isAuthenricate = false;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_MemberAddLocation(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

				#region Call Domian Method
				result = membershipDomain.MemberAddLocation(token, input);
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}			
			catch (Exception ex)
			{
				logger.Error("MemberAddLocation", ex);
                				
				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}

		/// <summary>
		///  Member can update the Email Address registered in the App with the new Email Address and Member ID.
		/// </summary>
		/// <param name="input">MemberChangeEmailAddress Request Object</param>
		/// <returns name="MemberChangeEmailAddressRS">Returns a MemberChangeEmailAddress Response Object with success/error</returns>
		public MemberChangeEmailAddressRS MemberChangeEmailAddress(MemberChangeEmailAddressRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.MemberChangeEmailAddress;
			MemberChangeEmailAddressRS result = null;			
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;			
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();			
			bool isAuthenricate = false;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_MemberChangeEmailAddress(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

				#region Call Domian Method
				#region Corporate things
				#region Corporates init
				var newCorporateUserID = input.NewEmailAddress;
				var oldCorporateUserID = input.OldEmailAddress;
				string oldCorporateKey = null;
				string newCorporateKey = null;
				List<CorporateName> newCorporateNames = null;
				CorporateName newCorporateName = CorporateName.Unknown;
				List<CorporateName> oldCorporateNames = null;
				CorporateName oldCorporateName = CorporateName.Unknown;
				bool updateEmail = true;
				#endregion

				#region Get Corporates for users for Old Email
				oldCorporateNames = membershipDomain.GetCorporateNames(oldCorporateUserID);
				if (oldCorporateNames != null && oldCorporateNames.Any())
				{
					oldCorporateName = oldCorporateNames.First();
				}
				#endregion

				#region Get Corporates for users for New Email
				newCorporateNames = membershipDomain.GetCorporateNames(newCorporateUserID);
				if (newCorporateNames != null && newCorporateNames.Any())
				{
					newCorporateName = newCorporateNames.First();
					newCorporateKey = Guid.NewGuid().ToString();
				}
				#endregion

				if (oldCorporateName != CorporateName.Unknown || newCorporateName != CorporateName.Unknown)
				{
					#region Call SDS for validation of Email and Password and also to Get oldCorporateKey
					var memberGetCorporateProfileOldRQ = new MemberGetCorporateProfileRQ();
					memberGetCorporateProfileOldRQ.CorporateID = input.OldEmailAddress;
					memberGetCorporateProfileOldRQ.MemberID = input.MemberId;


					//var loginOldRS = new MemberLoginRQ();
					//loginOldRS.EmailAddress = input.OldEmailAddress;
					//loginOldRS.Password = input.Password;
					Common.DTO.OTA.MemberGetCorporateProfileRS oldProfile = null;

					try
					{
						oldProfile = membershipDomain.MemberGetCorporateProfile(token, memberGetCorporateProfileOldRQ);
					}
					catch { }

					if (oldProfile != null && oldProfile.Corporates != null && oldProfile.Corporates.CorporateList != null && oldProfile.Corporates.CorporateList.Any())
					{
						var sdsCorporate = oldProfile.Corporates.CorporateList.First();
						if (sdsCorporate.CorporateAccount != null)
						{
							oldCorporateKey = sdsCorporate.CorporateAccount.UserKey;
						}
					}
					#endregion


					if (oldCorporateName != CorporateName.Unknown)
					{
						#region Remove Corporate Profile
						updateEmail = membershipDomain.DeleteCorporateUser(oldCorporateName, oldCorporateUserID, oldCorporateKey);
						#endregion
					}

					if (newCorporateName != CorporateName.Unknown)
					{
						#region Register in Corporate
						updateEmail = membershipDomain.RegisterCorporateUser(newCorporateName, newCorporateUserID, newCorporateKey);

						if (!updateEmail)
						{
							membershipDomain.DeleteCorporateUser(newCorporateName, newCorporateUserID, newCorporateKey);
							updateEmail = membershipDomain.RegisterCorporateUser(newCorporateName, newCorporateUserID, newCorporateKey);

						}
						#endregion
					}
				}


				if (updateEmail)
				{
					#region Update SDS email
					result = membershipDomain.MemberChangeEmailAddress(token, input);
					#endregion

					if (oldCorporateName != CorporateName.Unknown || newCorporateName != CorporateName.Unknown)
					{
						#region Update SDS Profile
						#region Call SDS to Get user profile

						var memberGetCorporateProfileNewRQ = new MemberGetCorporateProfileRQ();
						memberGetCorporateProfileNewRQ.CorporateID = input.NewEmailAddress;
						memberGetCorporateProfileNewRQ.MemberID = input.MemberId;


						var loginNewRS = new MemberLoginRQ();
						loginNewRS.EmailAddress = input.NewEmailAddress;
						Common.DTO.OTA.MemberGetCorporateProfileRS newProfile = null;
						try
						{
							newProfile = membershipDomain.MemberGetCorporateProfile(token, memberGetCorporateProfileNewRQ);
						}
						catch { }


						#endregion

						#region Update SDS
						if (newProfile != null)
						{
							var memberUpdateProfileRQ = new MemberUpdateProfileRQ();
							memberUpdateProfileRQ.MembershipRecord = newProfile.MembershipRecord;

							if (oldCorporateName != CorporateName.Unknown)
							{
								memberUpdateProfileRQ.CorporateAccount = new CorporateAccount { Corporation = string.Empty, UserID = string.Empty, UserKey = string.Empty };
							}
							if (newCorporateName != CorporateName.Unknown)
							{
								memberUpdateProfileRQ.CorporateAccount = new CorporateAccount { Corporation = newCorporateName.ToString(), UserID = newCorporateUserID, UserKey = newCorporateKey };

								#region GetCorporateProfile
								//This should be a list of corporateName and doing foreach, but as I mentioned above, SDS accepts just one corporate
								//if (isRegisterInCorporate)
								//{
								if (newCorporateName != CorporateName.Unknown)
								{
									result.Corporates = new Corporates();
									result.Corporates.CorporateList = new List<Corporate>();

									var corporateResult = membershipDomain.GetCorporate(newCorporateName, newCorporateUserID, newCorporateKey);
									if (corporateResult != null)
									{
										result.Corporates.CorporateList.Add(corporateResult);
									}
								}
								//}
								#endregion
							}

							membershipDomain.MemberUpdateProfile(token, memberUpdateProfileRQ);
						}
						#endregion
						#endregion
					}
				}


				#endregion
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}			
			catch (Exception ex)
			{
				logger.Error("MemberChangeEmailAddress", ex);
                
				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// Member can change the application password which is used to login.
		/// </summary>
		/// <param name="input">MemberChangePassword Request Object</param>
		/// <returns name="MemberChangePasswordRS">Returns a MemberChangePassword Response Object with Success/Error</returns>
		public MemberChangePasswordRS MemberChangePassword(MemberChangePasswordRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.MemberChangePassword;
			MemberChangePasswordRS result = null;			
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;			
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();			
			bool isAuthenricate = false;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_MemberChangePassword(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

				#region Call Domian Method
				#region Call SDS
				result = membershipDomain.MemberChangePassword(token, input);
				#endregion

				if (result != null && result.Success != null)
				{
					#region Corporates init
					var corporateUserID = input.EmailAddress;
					string newCorporateKey = null;
					string oldCorporateKey = null;
					List<CorporateName> corporateNames = null;
					CorporateName corporateName = CorporateName.Unknown;
					bool isCorporatePasswordChanged = false;
					#endregion

					#region Get Corporates for users
					//This should be a list of corporate things to push to SDS, but as SDS method accepts just one corporate, we get the first in the list
					corporateNames = membershipDomain.GetCorporateNames(input.EmailAddress);
					if (corporateNames != null && corporateNames.Any())
					{
						corporateName = corporateNames.First();
					}
					#endregion

					if (corporateName != CorporateName.Unknown)
					{
						#region Create CorporateKey
						newCorporateKey = Guid.NewGuid().ToString();
						#endregion

						#region UpdateSDS
						#region Call SDS to Get user profile
						var loginRS = new MemberLoginRQ();
						loginRS.EmailAddress = input.EmailAddress;
						loginRS.Password = input.NewPassword;
						var logingResult = membershipDomain.MemberLogin(token, loginRS);
						if (logingResult.Corporates != null && logingResult.Corporates.CorporateList != null && logingResult.Corporates.CorporateList.Any())
						{
							var sdsCorporate = logingResult.Corporates.CorporateList.First();
							if (sdsCorporate != null && sdsCorporate.CorporateAccount != null)
							{
								oldCorporateKey = sdsCorporate.CorporateAccount.UserKey;
							}
						}
						#endregion

						#region Update Corporate
						#region Register in Corporate
						//This should be a list of corporateName and doing foreach, but as I mentioned above, SDS accepts just one corporate
						isCorporatePasswordChanged = membershipDomain.ChangePassword(corporateName, corporateUserID, newCorporateKey, oldCorporateKey);

						if (!isCorporatePasswordChanged)
						{
							membershipDomain.DeleteCorporateUser(corporateName, corporateUserID, oldCorporateKey);
							isCorporatePasswordChanged = membershipDomain.RegisterCorporateUser(corporateName, corporateUserID, newCorporateKey);

						}
						#endregion

						#region Update SDS Profile
						if (isCorporatePasswordChanged)
						{
							if (logingResult.Corporates != null && logingResult.Corporates.CorporateList != null && logingResult.Corporates.CorporateList.Any())
							{
								var corporate = logingResult.Corporates.CorporateList.First();

								var memberUpdateProfileRQ = new MemberUpdateProfileRQ();
								memberUpdateProfileRQ.MembershipRecord = logingResult.MembershipRecord;
								memberUpdateProfileRQ.CorporateAccount = corporate.CorporateAccount;
								memberUpdateProfileRQ.CorporateAccount.UserKey = newCorporateKey;
								membershipDomain.MemberUpdateProfile(token, memberUpdateProfileRQ);

								#region GetCorporateProfile
								//This should be a list of corporateName and doing foreach, but as I mentioned above, SDS accepts just one corporate
								if (isCorporatePasswordChanged)
								{
									if (corporateName != CorporateName.Unknown)
									{
										result.Corporates = new Corporates();
										result.Corporates.CorporateList = new List<Corporate>();

										var corporateResult = membershipDomain.GetCorporate(corporateName, corporateUserID, newCorporateKey);
										if (corporateResult != null)
										{
											result.Corporates.CorporateList.Add(corporateResult);
										}
									}
								}
								#endregion
							}
						}
						#endregion
						#endregion


						#endregion

					}
				}


				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}			
			catch (Exception ex)
			{
				logger.Error("MemberChangePassword", ex);
                
				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// Deletes the location of the Member.
		/// </summary>
		/// <param name="input">MemberDeleteLocation Request Object</param>
		/// <returns name="MemberDeleteLocationRS">Retunrs a MemberDeleteLocation Response Object with Success/Error</returns>
		public MemberDeleteLocationRS MemberDeleteLocation(MemberDeleteLocationRQ input)
		{

			#region Init
			var methodName = Common.DTO.Enum.Method.MemberDeleteLocation;
			MemberDeleteLocationRS result = null;			
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;			
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();			
			bool isAuthenricate = false;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_MemberDeleteLocation(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

				#region Call Domian Method
				result = membershipDomain.MemberDeleteLocation(token, input);
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("MemberDeleteLocation", ex);
                
				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// Member notifies SDS about the Forgot Password.Upon success of this method User receives an email to change the password.
		/// </summary>
		/// <param name="input">MemberForgotPassword Request Object</param>
		/// <returns name="MemberForgotPasswordRS">Returns a MemberForgotPassword Response Object with Success/Error.</returns>
		public MemberForgotPasswordRS MemberForgotPassword(MemberForgotPasswordRQ input)
		{

			#region Init
			var methodName = Common.DTO.Enum.Method.MemberForgotPassword;
			MemberForgotPasswordRS result = null;			
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;			
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_MemberForgotPassword(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

				#region Call Domian Method
				result = membershipDomain.MemberForgotPassword(token, input);
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("MemberForgotPassword", ex);
                
				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// This method Validates the Password Request.
		/// </summary>
		/// <param name="input">MemberGetPassword Request Object  contains the Request Guid</param>
		/// <returns name="MemberGetPasswordRequestRS">Returns a MemberGetPasswordRequest Response Object with the details of the member like EmailAddress/Name/Member ID etc.</returns>
		public MemberGetPasswordRequestRS MemberGetPasswordRequest(MemberGetPasswordRequestRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.MemberGetPasswordRequest;
			MemberGetPasswordRequestRS result = null;			
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token); 

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if(isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_MemberGetPasswordRequest(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

				#region Call Domian Method
				result = membershipDomain.MemberGetPasswordRequest(token, input);
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("MemberGetPasswordRequest", ex);
                
				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// Member resets the password by using Member ID and the password.
		/// </summary>
		/// <param name="input">MemberResetPassword Request Object</param>
		/// <returns name="MemberResetPasswordRS">MemberResetPassword Response Object</returns>
		public MemberResetPasswordRS MemberResetPassword(MemberResetPasswordRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.MemberResetPassword;
			MemberResetPasswordRS result = null;
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_MemberResetPassword(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

				#region Call Domian Method
				result = membershipDomain.MemberResetPassword(token, input);
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("MemberResetPassword", ex);

				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }					
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// Retrieves the List of Address of the Member eg. home address,office address,school address etc.
		/// </summary>
		/// <param name="input">MemberGetLocations Request Object</param>
		/// <returns name="MemberGetLocationsRS">Returns MemberGetLocations Response Object with the List of Address</returns>
		public MemberGetLocationsRS MemberGetLocations(MemberGetLocationsRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.MemberGetLocations;
			MemberGetLocationsRS result = null;
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_MemberGetLocations(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

				#region Call Domian Method
				result = membershipDomain.MemberGetLocations(token, input);
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("MemberGetLocations", ex);
                
				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// Retrieves the List of the Address where user has travelled recently.
		/// </summary>
		/// <param name="input">MemberGetLocations Request Object</param>
		/// <returns name="MemberGetLocationsRS">Returns a  MemberGetLocations Response Object with the Address</returns>
		public MemberGetLocationsRS MemberGetRecentLocations(MemberGetLocationsRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.MemberGetRecentLocations;
			MemberGetLocationsRS result = null;
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;			
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();			
			bool isAuthenricate = false;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_MemberGetLocations(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

				#region Call Domian Method
				result = membershipDomain.MemberGetRecentLocations(token, input);
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("MemberGetRecentLocations", ex);
                
				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// Retrieves the List of the Trips where user has done the booking.
		/// </summary>
		/// <param name="input">MemberGetLocations Request Object</param>
		/// <returns name="MemberGetReservationRS">Returns a MemberGetLocations Response Object</returns>
		public MemberGetReservationRS MemberGetReservation(MemberGetReservationRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.MemberGetReservation;
			MemberGetReservationRS result = null;
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);
                
                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_MemberGetReservation(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
                #endregion
                
                #region BOBO
                if (input.IsBOBO.ToBool() && input.BOBOMemberID.HasValue)
                {
                    input.MemberId = input.BOBOMemberID.Value;
                }
                #endregion

                #region Call Domian Method
                bool isCorporate = false;
				bool isMemberAllReservationForAleph = false;
				try
				{
					isMemberAllReservationForAleph = System.Configuration.ConfigurationManager.AppSettings["isMemberAllReservationForAleph"].ToBool();
				}
				catch (Exception ex)
				{
					logger.ErrorFormat(ex.Message);
				}
				#region Check Corporate User
				if (isMemberAllReservationForAleph == true)
				{
					if (input.Corporate != null && input.Corporate.CorporateAccount != null && !string.IsNullOrWhiteSpace(input.Corporate.CorporateAccount.UserID))
					{
						isCorporate = true;
					}
				}
				#endregion

				if (isCorporate)
				{
					#region Get Reservation For Corporate
					if (input.TripReminder == null || input.TripReminder == false)
					{
						result = membershipDomain.MemberGetReservationForCorporate(token, input);
					}
					else if (input.TripReminder == true)
					{
						#region Get Reservation
						result = membershipDomain.MemberGetReservationTripReminder(token, input);
						#endregion
					}
					#endregion
				}
				else
				{
					#region Get Reservation
					if (input.TripReminder == null || input.TripReminder == false)
					{
						result = membershipDomain.MemberGetReservation(token, input);
					}
					else if (input.TripReminder == true)
					{
						#region Get Reservation
						result = membershipDomain.MemberGetReservationTripReminder(token, input);
						#endregion
					}
					#endregion
				}

				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("MemberGetReservation", ex);
                
				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// This method links VTOD Api to SDS.Through this VTOP API Trip ID gets tagged with the SDS RezID.
		/// </summary>
		/// <param name="input">MemberLinkReservation Request Object</param>
		/// <returns name="MemberLinkReservationRS">Returns a MemberLinkReservation Response Object</returns>
		public MemberLinkReservationRS MemberLinkReservation(MemberLinkReservationRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.MemberLinkReservation;
			MemberLinkReservationRS result = null;
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_MemberLinkReservation(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

				#region Call Domian Method
				#region Input
				var vtodTripID = 0;
				var rezID = input.RezID;
				var memberID = input.MemberId;
				#endregion

				membershipDomain.LinkReservation(token, vtodTripID, rezID, memberID);
				result = new MemberLinkReservationRS { Success = new Success() };
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("MemberLinkReservation", ex);
                
				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse); 
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// This method authenticates the User Details with the SuperShuttle.User receives Success/Error as an output.
		/// </summary>
		/// <param name="input">MemberLogin  Request Object</param>
		/// <returns name="MemberLoginRS">Returns a MemberLogin Response Object</returns>
		public MemberLoginRS MemberLogin(MemberLoginRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.MemberLogin;
			MemberLoginRS result = null;
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_MemberLogin(token, input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

				#region Call Domian Method
				#region Corporates init
				var corporateUserID = input.EmailAddress;
				string corporateKey = null;
				List<CorporateName> corporateNames = null;
				CorporateName corporateName = CorporateName.Unknown;
				bool isRegisterInCorporate = false;
				#endregion

				#region Call SDS
				result = membershipDomain.MemberLogin(token, input);
				#endregion

				if (result != null && result.Success != null)
				{
					#region Get Corporate (Name)
					if (result.Corporates != null && result.Corporates.CorporateList != null && result.Corporates.CorporateList.Any())
					{
						var corporate = result.Corporates.CorporateList.First();
						if (corporate.CorporateAccount != null && !string.IsNullOrWhiteSpace(corporate.CorporateAccount.Corporation))
						{
							corporateName = corporate.CorporateAccount.Corporation.ToCoporateName();
							corporateKey = corporate.CorporateAccount.UserKey;
						}
					}
					#endregion

					if (corporateName != CorporateName.Unknown)
					{
						#region Get Corporates for users
						//This should be a list of corporate things to push to SDS, but as SDS method accepts just one corporate, we get the first in the list
						corporateNames = membershipDomain.GetCorporateNames(input.EmailAddress);
						if (corporateNames != null && corporateNames.Any())
						{
							corporateName = corporateNames.First();
						}
						#endregion

						if (corporateName == CorporateName.Unknown)
						{
							#region Remove User's corporate account from SDS
							var memberUpdateProfileRQ = new MemberUpdateProfileRQ();
							result.Corporates = null;
							memberUpdateProfileRQ.MembershipRecord = result.MembershipRecord;
							memberUpdateProfileRQ.CorporateAccount = new CorporateAccount { Corporation = string.Empty, UserID = string.Empty, UserKey = string.Empty };
							membershipDomain.MemberUpdateProfile(token, memberUpdateProfileRQ);
							#endregion
						}
						else
						{
							#region Get Corporate Profile and add it to the RS
							#region GetCorporateProfile
							//This should be a list of corporateName and doing foreach, but as I mentioned above, SDS accepts just one corporate
							result.Corporates = new Corporates();
							result.Corporates.CorporateList = new List<Corporate>();

							var corporateResult = membershipDomain.GetCorporate(corporateName, corporateUserID, corporateKey);
							if (corporateResult != null && corporateResult.CorporateAccount != null)
							{
								result.Corporates.CorporateList.Add(corporateResult);
								isRegisterInCorporate = true;
							}
							#endregion

							#endregion
						}

					}

					if (!isRegisterInCorporate)
					{
						#region Get Corporates for users
						//This should be a list of corporate things to push to SDS, but as SDS method accepts just one corporate, we get the first in the list
						corporateNames = membershipDomain.GetCorporateNames(input.EmailAddress);
						if (corporateNames != null && corporateNames.Any())
						{
							corporateName = corporateNames.First();
						}
						#endregion

						#region Create CorporateKey
						corporateKey = Guid.NewGuid().ToString();
						#endregion

						#region Register in Corporate
						//This should be a list of corporateName and doing foreach, but as I mentioned above, SDS accepts just one corporate
						if (corporateName != CorporateName.Unknown)
						{
							membershipDomain.DeleteCorporateUser(corporateName, corporateUserID, string.Empty);
							isRegisterInCorporate = membershipDomain.RegisterCorporateUser(corporateName, corporateUserID, corporateKey);
						}
						#endregion

						if (isRegisterInCorporate)
						{
							if (corporateName != CorporateName.Unknown)
							{
								#region GetCorporateProfile
								//This should be a list of corporateName and doing foreach, but as I mentioned above, SDS accepts just one corporate

								result.Corporates = new Corporates();
								result.Corporates.CorporateList = new List<Corporate>();

								var corporateResult = membershipDomain.GetCorporate(corporateName, corporateUserID, corporateKey);
								if (corporateResult != null)
								{
									result.Corporates.CorporateList.Add(corporateResult);
								}
								#endregion

								#region Remove User's corporate account from SDS
								var memberUpdateProfileRQ = new MemberUpdateProfileRQ();
								//result.Corporates = null;
								memberUpdateProfileRQ.MembershipRecord = result.MembershipRecord;
								memberUpdateProfileRQ.CorporateAccount = new CorporateAccount { Corporation = corporateName.ToString(), UserID = corporateUserID, UserKey = corporateKey };
								membershipDomain.MemberUpdateProfile(token, memberUpdateProfileRQ);
								#endregion

							}


						}

					}
				}
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("MemberLogin", ex);

				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}




        /// <summary>
        /// Member Get Referral Link
        /// </summary>
        /// <param name="input">Using MemberLogin Request Object</param>
        /// <returns name="MemberUpdateLocationRS">Returns a MemberGetReferralLink Response Object with Success/Error as an output.</returns>
        public MemberGetReferralLinkRS MemberGetReferralLink(MemberGetReferralLinkRQ input)
        {
            #region Init
            var methodName = Method.MemberGetReferralLink;
            MemberGetReferralLinkRS result = null;            
            var rawRequest = string.Empty;
            var rawResponse = string.Empty;
            var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
            var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;            
            var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
            var validationDomain = new ValidationDomain();
            var token = new TokenRS();
            bool isAuthenricate = false;            

            #endregion

            try
            {
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
                #endregion

                #region Log Request
                rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();              
                if (isDebug)
                {
                    logger.InfoFormat("Request{0}",  rawRequest);
                }
                #endregion

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
                #endregion

                #region Modify Token
                Modification.ModifyTokenRS(ref token);
                #endregion
                
                #region Validate Token
                validationDomain.Validate_SDS_Token(token);
                #endregion
                
                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
                #endregion

                #region Authenticate
                isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
                if (!isAuthenricate)
                    throw VtodException.CreateException(ExceptionType.Auth, 101);
                #endregion

                #region Call Domain Method
                result = membershipDomain.MemberGetReferralLink(token, input);
                #endregion

                result.EchoToken = input.EchoToken;
                result.Target = input.Target;
                result.Version = input.Version;
                result.PrimaryLangID = input.PrimaryLangID;

                rawResponse = result.XmlSerialize().ToString();

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
                #endregion

            }
            catch (Exception ex)
            {
                logger.Error(methodName.ToString(), ex);

                var exception = getOtaException(ex);
                rawResponse = exception.Errors.XmlSerialize().ToString();
                throw exception;
            }
            finally
            {
                try
                {
                    #region Log Reponse
                    if (isDebug)
                    {
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }                   
                    #endregion

                    #region Track
                    trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
                    #endregion

                    #region SaveTrackTime
                    vtodDomain.InsertTrack(trackTime);
                    trackTime.Dispose();
                    #endregion
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }

            }

            return result;
        }




        /// <summary>
        /// Member Location details get updated.
        /// </summary>
        /// <param name="input">MemberUpdateLocation Request Object</param>
        /// <returns name="MemberUpdateLocationRS">Returns a MemberUpdateLocation Response Object with Success/Error as an output.</returns>
        public MemberUpdateLocationRS MemberUpdateLocation(MemberUpdateLocationRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.MemberUpdateLocation;
			MemberUpdateLocationRS result = null;
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;			
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_MemberUpdateLocation(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

				#region Call Domian Method
				result = membershipDomain.MemberUpdateLocation(token, input);
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("MemberUpdateLocation", ex);
                
				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// Member Profile gets updated like name,phone number,email address etc.
		/// </summary>
		/// <param name="input">MemberUpdateProfile Request Object</param>
		/// <returns name="MemberUpdateProfileRS">Returns a MemberUpdateProfile Response Object</returns>
		public MemberUpdateProfileRS MemberUpdateProfile(MemberUpdateProfileRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.MemberUpdateProfile;
			MemberUpdateProfileRS result = null;			
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var validationDomain = new ValidationDomain();
			var vtodUtility = new VTODUtilityDomain(); vtodUtility.TrackTime = trackTime;
			var token = new TokenRS();
			bool isAuthenricate = false;
			int memberID = 0;
			string phoneNumber = string.Empty;
			int phoneVerificationEnabled = System.Configuration.ConfigurationManager.AppSettings["EnablePhoneVerification"].ToInt32();
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_MemberUpdateProfile(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

                #region Remove the leading zeros from the phone number
                if (input.MembershipRecord.Telephone != null && input.MembershipRecord.Telephone.CountryAccessCode != null)
                    input.MembershipRecord.Telephone.CountryAccessCode = input.MembershipRecord.Telephone.CountryAccessCode.TrimStart(new Char[] { '0' });
                #endregion

				#region VerifyPhone
				if (phoneVerificationEnabled == 1)
				{
					logger.InfoFormat("Start-Verify Phone");

					var isPhoneVerified = false;
					if (input.TPA_Extensions != null && input.TPA_Extensions.PhoneVerificationRequired)
					{
						#region PhoneNumbers from Request Object
						if (input.MembershipRecord.Telephone != null && !string.IsNullOrEmpty(input.MembershipRecord.Telephone.PhoneNumber))
						{
							phoneNumber = string.Format("{0}{1}{2}", input.MembershipRecord.Telephone.CountryAccessCode, input.MembershipRecord.Telephone.AreaCityCode, input.MembershipRecord.Telephone.PhoneNumber);
							logger.InfoFormat("Phone Number:{0}:", phoneNumber);
						}
						#endregion

						#region MemberID from Response Object
						if (input.MembershipRecord.MemberID > 0)
						{
							memberID = input.MembershipRecord.MemberID;
							logger.InfoFormat("Member ID:{0}:", memberID);
						}
						#endregion

						#region Call Domain Method

						if (memberID > 0 || !string.IsNullOrEmpty(phoneNumber))
						{

							isPhoneVerified = vtodUtility.IsPhoneVerified(token, phoneNumber, memberID);
							logger.InfoFormat("Status of IsphoneVerified:{0}:", isPhoneVerified);
							logger.InfoFormat("Start WebService Method : Verify Phone");
							if (isPhoneVerified == false)
							{
								logger.InfoFormat("Start WebService Method : Verify Phone");
								logger.InfoFormat("Phone Number:{0},MemberID:{1}:", phoneNumber, memberID);
                                try
                                {
                                    if (input.TPA_Extensions.IsPhoneVerifyByCode == true)
                                    {
                                        vtodUtility.VerifyPhone(token, phoneNumber, memberID, "code");
                                    }
                                    else
                                    {
                                        vtodUtility.VerifyPhone(token, phoneNumber, memberID,null);
                                    }
                                }
                                catch
                                {
                                    //vtodUtility.VerifyPhone(token, phoneNumber, memberID, null);
                                }
                                logger.InfoFormat("End WebService Method : Verify Phone");
							}
							else
							{
								logger.InfoFormat("Phone number is already verified");
								//throw VtodException.CreateException(ExceptionType.Auth, 104);
							}
						}


						#endregion
					}
					logger.InfoFormat("End-Verify Phone");
				}
				#endregion

				#region Call Domian Method
				result = membershipDomain.MemberUpdateProfile(token, input);
				#endregion

				#region Sync zTrip phone and Device to Corporate
                try
                {
                    if (result != null)
                    {
                        bool isRegisterCorporateDevice = false;
                        var memberhshipController = new UDI.SDS.MembershipController(token, TrackTime);
                        var sdsResult = memberhshipController.GetCorporateAccountKey(CorporateName.Aleph.ToString(), input.MembershipRecord.EmailAddress);
                        if (input.MembershipRecord.Telephone != null && !string.IsNullOrEmpty(input.MembershipRecord.Telephone.PhoneNumber))
                        {
                            phoneNumber = string.Format("{0}{1}{2}", input.MembershipRecord.Telephone.CountryAccessCode, input.MembershipRecord.Telephone.AreaCityCode, input.MembershipRecord.Telephone.PhoneNumber);
                        }
                        if (sdsResult != null)
                        {
                            isRegisterCorporateDevice = membershipDomain.RegisterCorporateDeviceName(token, CorporateName.Aleph, input.MembershipRecord.EmailAddress, sdsResult.UserKey.ToString(), input.TPA_Extensions.DeviceToken, input.TPA_Extensions.Device, input.TPA_Extensions.DeviceVersion, phoneNumber,input.MembershipRecord.MemberID);
                        }
                        logger.ErrorFormat("Device registered:{0}:", isRegisterCorporateDevice);
                    }
                }
                catch (Exception ex)
                {
                    logger.ErrorFormat("SyncCorporate failed: {0}", ex);
                }
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("MemberUpdateProfile", ex);
                
				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// These is 2 factor authentication where user confirms his email.
		/// </summary>
		/// <param name="input">MemberRequestEmailConfirmation Request Object</param>
		/// <returns name="MemberRequestEmailConfirmationRS">Returns a MemberRequestEmailConfirmationRS Object</returns>
		public MemberRequestEmailConfirmationRS MemberRequestEmailConfirmation(MemberRequestEmailConfirmationRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.MemberRequestEmailConfirmation;
			MemberRequestEmailConfirmationRS result = null;			
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_MemberRequestEmailConfirmation(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

				#region Call Domian Method
				result = membershipDomain.MemberRequestEmailConfirmation(token, input);
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("MemberRequestEmailConfirmation", ex);

				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
				    #endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// ztrip Customer  rate their trip from 1 to 5.
		/// App call this method to get the user's latest trip that has not been rated yet.
		/// If user has already rated all his trips, it return nothing
		/// </summary>
		/// <param name="input">MemberGetLatestUnRatedTrip Request Object</param>
		/// <returns name="MemberGetLatestUnRatedTripRS">Returns a MemberGetLatestUnRatedTrip Response Object</returns>
        /// 
        public MemberRequestEmailCancellationRS MemberRequestEmailCancellation(MemberRequestEmailCancellationRQ input)
        {

            #region Init
            var methodName = Common.DTO.Enum.Method.MemberRequestEmailCancellation;
            MemberRequestEmailCancellationRS result = null;            
            var rawRequest = string.Empty;
            var rawResponse = string.Empty;
            var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
            var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
            var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
            var validationDomain = new ValidationDomain();
            var token = new TokenRS();
            bool isAuthenricate = false;
            #endregion

            try
            {
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
                #endregion

                #region Log Request
                rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
                if (isDebug)
                {
                    logger.InfoFormat("Request{0}", rawRequest);
                }
                #endregion

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
                #endregion

                #region Modify Token
                Modification.ModifyTokenRS(ref token);
                #endregion

                #region Validate request object
                validationDomain.Validate_MemberRequestEmailCancellation(input);
                #endregion

                #region Validate Token
                validationDomain.Validate_SDS_Token(token);
                #endregion

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
                #endregion

                #region Authenticate
                isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
                if (!isAuthenricate)
                    throw VtodException.CreateException(ExceptionType.Auth, 101);
                #endregion
                
                #region Call Domian Method
                result = membershipDomain.MemberRequestEmailCancellation(token, input);
                #endregion

                rawResponse = result.XmlSerialize().ToString();

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
                #endregion
            }
            catch (Exception ex)
            {
                logger.Error("MemberRequestEmailCancellation", ex);
                var exception = getOtaException(ex);
                rawResponse = exception.Errors.XmlSerialize().ToString();
                throw exception;
            }
            finally
            {
                try
                {
                    #region Log Reponse
                    if (isDebug)
                    {
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
                    #endregion

                    #region Track
                    trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
                    #endregion

                    #region SaveTrackTime
                    vtodDomain.InsertTrack(trackTime);
                    trackTime.Dispose();
                    #endregion
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }

            }

            return result;
        }
		public MemberGetLatestUnRatedTripRS MemberGetLatestUnRatedTrip(MemberGetLatestUnRatedTripRQ input)
		{

			#region Init
			var methodName = Common.DTO.Enum.Method.MemberGetLatestUnRatedTrip;
			MemberGetLatestUnRatedTripRS result = null;			
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion

			try
			{
				#region Get Header
				var requestMessages = OperationContext.Current.IncomingMessageProperties[HttpRequestMessageProperty.Name] as HttpRequestMessageProperty;

				try
				{
					if (requestMessages.Headers["Username"] != null)
						token.Username = requestMessages.Headers["Username"];
				}
				catch { }
				try
				{
					if (requestMessages.Headers["SecurityKey"] != null)
						token.SecurityKey = requestMessages.Headers["SecurityKey"];
				}
				catch { }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_MemberGetLatestUnRatedTrip(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion
                
				#region Call Domian Method
				result = membershipDomain.MemberGetLatestUnRatedTrip(token, input);
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("MemberGetLatestUnRatedTrip", ex);
                
				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// Retrieves the Corporate Details like CompanyName,Payment Method etc. with respect to a MemberID 
		/// </summary>
		/// <param name="input">MemberGetCorporateProfile Request Object</param>
		/// <returns name="MemberGetCorporateProfileRS">Returns a MemberGetCorporateProfile Response Object</returns>
		public MemberGetCorporateProfileRS MemberGetCorporateProfile(MemberGetCorporateProfileRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.MemberGetCorporateProfile;
			MemberGetCorporateProfileRS result = null;			
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_MemberGetCorporateProfile(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

				#region Call Domian Method
				result = membershipDomain.MemberGetCorporateProfile(token, input);
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("MemberGetCorporateProfile", ex);

				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}

        /// <summary>
        /// Retrieves the Corporate Details like Corporate Name,CompanyName,Payment Method etc. with respect to a MemberID 
        /// </summary>
        /// <param name="input">MemberGetCorporateProfile Request Object</param>
        /// <returns name="MemberGetCorporateProfileRS">Returns a MemberGetCorporateProfile Response Object</returns>
        public MemberGetCorporateProfile2RS MemberGetCorporateProfile2(MemberGetCorporateProfile2RQ input)
        {

            #region Init
            var methodName = Common.DTO.Enum.Method.MemberGetCorporateProfile2;
            MemberGetCorporateProfile2RS result = null;
            var rawRequest = string.Empty;
            var rawResponse = string.Empty;
            var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
            var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
            var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
            var validationDomain = new ValidationDomain();
            var token = new TokenRS();
            bool isAuthenricate = false;
            #endregion

            try
            {
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
                #endregion

                #region Log Request
                rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
                if (isDebug)
                {
                    logger.InfoFormat("Request{0}", rawRequest);
                }
                #endregion

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
                #endregion

                #region Modify Token
                Modification.ModifyTokenRS(ref token);
                #endregion

                #region Validate request object
                validationDomain.Validate_MemberGetCorporateProfile2(input);
                #endregion

                #region Validate Token
                validationDomain.Validate_SDS_Token(token);
                #endregion

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
                #endregion

                #region Authenticate
                isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
                if (!isAuthenricate)
                    throw VtodException.CreateException(ExceptionType.Auth, 101);
                #endregion

                #region Call Domian Method
                result = membershipDomain.MemberGetCorporateProfile2(token, input);
                #endregion

                rawResponse = result.XmlSerialize().ToString();

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
                #endregion
            }
            catch (Exception ex)
            {
                logger.Error("MemberGetCorporateProfile2", ex);
                var exception = getOtaException(ex);
                rawResponse = exception.Errors.XmlSerialize().ToString();
                throw exception;
            }
            finally
            {
                try
                {
                    #region Log Reponse
                    if (isDebug)
                    {
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
                    #endregion

                    #region Track
                    trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
                    #endregion

                    #region SaveTrackTime
                    vtodDomain.InsertTrack(trackTime);
                    trackTime.Dispose();
                    #endregion
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }

            }

            return result;
        }
		/// <summary>
		/// Deletes a Member from VTOD
		/// </summary>
		/// <param name="input">MemberDelete Request Object</param>
		/// <returns name="MemberDeleteRS">Returns a MemberDelete Request Object with status as Success and Failure</returns>
		public MemberDeleteRS MemberDelete(MemberDeleteRQ input)
		{

			#region Init
			var methodName = Common.DTO.Enum.Method.MemberDelete;
			MemberDeleteRS result = null;
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_MemberDelete(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

                #region Sync zTrip phone and Device to Corporate
                try
                {
                    bool isRegisterCorporateDevice = false;
                    var memberhshipController = new UDI.SDS.MembershipController(token, TrackTime);
                    var sdsResult = memberhshipController.GetCorporateAccountKey(CorporateName.Aleph.ToString(), input.EmailAddress);
                    int memberId = 0;
                    if (!string.IsNullOrEmpty(input.MemberID))
                    {
                        memberId = input.MemberID.ToInt32();
                    }
                    if (sdsResult != null)
                    {
                        isRegisterCorporateDevice = membershipDomain.DeleteCorporateDeviceName(token, CorporateName.Aleph, input.EmailAddress, sdsResult.UserKey.ToString(), input.DeviceToken, memberId);
                    }
                    logger.ErrorFormat("Device registered:{0}:", isRegisterCorporateDevice);

                }
                catch (Exception ex)
                {
                    logger.ErrorFormat("SyncCorporate failed: {0}", ex);
                }
				#region Call SDS
				result = membershipDomain.MemberDelete(token, input);
				#endregion
			
				#endregion
				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("MemberDelete", ex);
				
				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}


        public MemberGetLatestTripRS MemberGetLatestTrip(MemberGetLatestTripRQ input)
        {
            #region Init
            var methodName = Common.DTO.Enum.Method.MemberGetLatestTrip;
            MemberGetLatestTripRS result = null;
            var rawRequest = string.Empty;
            var rawResponse = string.Empty;
            var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
            var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
            var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
            var validationDomain = new ValidationDomain();
            var token = new TokenRS();
            bool isAuthenricate = false;
            #endregion

            try
            {
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
                #endregion

                #region Log Request
                rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
                if (isDebug)
                {
                    logger.InfoFormat("Request{0}", rawRequest);
                }
                #endregion

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
                #endregion

                #region Modify Token
                Modification.ModifyTokenRS(ref token);
                #endregion

                #region Validate request object
                validationDomain.Validate_MemberGetLatestTrip(input);
                #endregion

                #region Validate Token
                validationDomain.Validate_SDS_Token(token);
                #endregion

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
                #endregion

                #region Authenticate
                isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
                if (!isAuthenricate)
                    throw VtodException.CreateException(ExceptionType.Auth, 101);
                #endregion
                                
                #region Call Domian Method
                result = membershipDomain.MemberGetLatestTrip(token, input);
                #endregion

                rawResponse = result.XmlSerialize().ToString();

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
                #endregion
            }
            catch (Exception ex)
            {
                logger.Error("MemberGetLatestTrip", ex);
             
                var exception = getOtaException(ex);
                rawResponse = exception.Errors.XmlSerialize().ToString();
                throw exception;
            }
            finally
            {
                try
                {
                    #region Log Reponse
                    if (isDebug)
                    {
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
                    #endregion

                    #region Track
                    trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
                    #endregion

                    #region SaveTrackTime
                    vtodDomain.InsertTrack(trackTime);
                    trackTime.Dispose();
                    #endregion
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }

            }

            return result;
        }
		#endregion

		#region Utilities
		/// <summary>
		/// Retrieves the list of the Destinations available for a trip.
		/// </summary>
		/// <param name="input">UtilityGetDestinations Request Object</param>
		/// <returns name="UtilityGetDestinationsRS">Returns a UtilityGetDestinations Response Object with following Details:AirportCode,AirportName,City,Country,CountryCode,Latitude,Longitude,State,StateName,Zip</returns>
		public UtilityGetDestinationsRS UtilityGetDestinations(UtilityGetDestinationsRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.UtilityGetDestinations;
			UtilityGetDestinationsRS result = null;
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var utilityDomain = new UtilityDomain(); utilityDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_UtilityGetDestinations(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

				#region Call Domian Method
				result = utilityDomain.GetDestinations(token, input);
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("UtilityGetDestinations", ex);


				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// Finds the Nearest Airport Locations available for a Latitude/Longitude and Radius
		/// </summary>
		/// <param name="input">UtilityGetServicedAirports Request Object</param>
		/// <returns name="UtilityGetServicedAirportsRS">Returns a UtilityGetServicedAirports Response Object</returns>
		public UtilityGetServicedAirportsRS GetServicedAirports(UtilityGetServicedAirportsRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.UtilityGetWebServicedAirports;
			UtilityGetServicedAirportsRS result = null;
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var utilityDomain = new UtilityDomain(); utilityDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_GetServicedAirports(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

				#region Call Domian Method
				result = utilityDomain.GetServicedAirports(token, input);
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("GetServicedAirports", ex);

				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        if (rawResponse.Contains("Success"))
                        {
                            int pFrom = rawResponse.IndexOf("<Airports>") + "<Airports>".Length;

                            int pTo = rawResponse.LastIndexOf("</Airports>");

                            string airports = rawResponse.Substring(pFrom, pTo - pFrom);

                            rawResponse = rawResponse.Replace(airports, "Doug doesn't want to see airports on the log");
                        }

                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// Retrieves the list of the Locations.
		/// </summary>
		/// <param name="input">UtilityGetLocationList Request Object</param>
		/// <returns name="UtilityGetLocationListRS">Returns a UtilityGetLocationList Response Object with the set of all the locations.</returns>
		public UtilityGetLocationListRS GetCharterLocationsByType(UtilityGetLocationListRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.UtilityGetCharterLocationsByType;
			UtilityGetLocationListRS result = null;
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var utilityDomain = new UtilityDomain(); utilityDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_GetCharterLocationsByType(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

				#region Call Domian Method
				result = utilityDomain.GetLocationList(token, input);
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("GetCharterLocationsByType", ex);
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// This method returns a Fleet Type
		/// </summary>
		/// <param name="input">UtilityGetDefaultFleetTypeForPoint Request Object sends the Latitude and the Longitude with the Request Object</param>
		/// <returns name="UtilityGetDefaultFleetTypeForPointRS">Returns a UtilityGetDefaultFleetTypeForPoint Response Object with the Fleet Type</returns>
		public UtilityGetDefaultFleetTypeForPointRS GetDefaultVehicleTypeForPoint(UtilityGetDefaultFleetTypeForPointRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.UtilityVehicleGetDefaultVehicleTypeForPoint;
			UtilityGetDefaultFleetTypeForPointRS result = null;
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var utilityDomain = new UtilityDomain(); utilityDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_GetDefaultVehicleTypeForPoint(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

				#region Call Domian Method
				result = utilityDomain.GetDefaultFleetTypeForPoint(token, input);
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("GetDefaultVehicleTypeForPoint", ex);

				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// Returns the list of the locations
		/// </summary>
		/// <param name="input">UtilitySearchLocation Request Object with the LocationID</param>
		/// <returns name="UtilitySearchLocationRS">Returns a UtilitySearchLocation Response Object based on the Location ID</returns>
		public UtilitySearchLocationRS CharterSearchLandmark(UtilitySearchLocationRQ input)
		{

			#region Init
			var methodName = Common.DTO.Enum.Method.UtilityLandmarkCharterSearchLandmark;
			UtilitySearchLocationRS result = null;
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var utilityDomain = new UtilityDomain(); utilityDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_CharterSearchLandmark(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

				#region Call Domian Method
				result = utilityDomain.SearchLocation(token, input);
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("CharterSearchLandmark", ex);
                
				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// Returns the Airport Name
		/// </summary>
		/// <param name="input">UtilityGetAirportName Request Object with Latitude/Longitude</param>
		/// <returns name="UtilityGetAirportNameRS">Returns a UtilityGetAirportName Response Object with the Airport Name</returns>
		public UtilityGetAirportNameRS GetAirportName(UtilityGetAirportNameRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.UtilityGetAirportName;
			UtilityGetAirportNameRS result = null;
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var utilityDomain = new UtilityDomain(); utilityDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_GetAirportName(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

				#region Call Domian Method
				result = utilityDomain.GetAirportName(token, input);
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("GetAirportName", ex);

				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// Returns the Available Fleet for an Address
		/// </summary>
		/// <param name="input">UtilityAvailableFleets Request Object with CityName,Postal Code,State,Country,Latitude and Longitude</param>
		/// <returns name="UtilityAvailableFleetsRS">Returns UtilityAvailableFleets Response Object with the set of Fleet Types Available.</returns>
		public UtilityAvailableFleetsRS UtilityAvailableFleets(UtilityAvailableFleetsRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.UtilityAvailableFleet;
			UtilityAvailableFleetsRS result = null;
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var utilityDomain = new UtilityDomain(); utilityDomain.TrackTime = trackTime;
			var taxiDomain = new TaxiDomain();
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;			
            var vanDomain = new VanDomain();  vanDomain.TrackTime = trackTime;
            var ecarDomain = new ECarDomain(); ecarDomain.TrackTime = trackTime;
            var validationDomain = new ValidationDomain();
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_UtilityAvailableFleet(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

				#region Call Domian Method
				List<Fleet> sdsResult = null;
				Fleet taxiResult = null;
				Fleet ecarResult = null;
                Fleet vanResult = null;
                Fleet ecarTexasResult = null;
                #region Regular
                if (input.Corporate == null || input.Corporate.CorporateAccount == null)
				{
					#region TPL Make
					var tasks = new List<Task>();

					#region SDS Task

					var taskSDS = new Task(
						() =>
						{
							try
							{
								sdsResult = utilityDomain.GetAvailableFleets(token, input, FleetType.SuperShuttle_ExecuCar);
							}
							catch (Exception ex)
							{								
								logger.Error(ex);
							}
						}
						,
						TaskCreationOptions.AttachedToParent);
					tasks.Add(taskSDS);
					#endregion

					#region Taxi Task
					var taskGTC = new Task(
									() =>
									{
										try
										{
											taxiResult = taxiDomain.GetAvailableFleets(token, input);
										}
										catch (Exception ex)
										{
											#region Add errors to OTA
											//errorListForSuccess.Errors.Add(new Common.DTO.OTA.Error { Type = "ECar", Value = ex.Message });
											#endregion

											logger.Error(ex);
										}
									}
									,
									TaskCreationOptions.AttachedToParent);
					tasks.Add(taskGTC);
                    #endregion
                    #region Ecar Task
                    var taskEcar = new Task(
                                    () =>
                                    {
                                        try
                                        {
                                            ecarTexasResult = ecarDomain.GetAvailableFleets(token, input);
                                        }
                                        catch (Exception ex)
                                        {
                                            #region Add errors to OTA
                                            //errorListForSuccess.Errors.Add(new Common.DTO.OTA.Error { Type = "ECar", Value = ex.Message });
                                            #endregion

                                            logger.Error(ex);
                                        }
                                    }
                                    ,
                                    TaskCreationOptions.AttachedToParent);
                    tasks.Add(taskEcar);
                    #endregion
                    #region Van Task
                    var taskVan = new Task(
                                    () =>
                                    {
                                        try
                                        {
                                            vanResult = vanDomain.GetAvailableFleets(token, input);
                                        }
                                        catch (Exception ex)
                                        {
                                            #region Add errors to OTA
                                            //errorListForSuccess.Errors.Add(new Common.DTO.OTA.Error { Type = "ECar", Value = ex.Message });
                                            #endregion

                                            logger.Error(ex);
                                        }
                                    }
                                    ,
                                    TaskCreationOptions.AttachedToParent);
                    tasks.Add(taskVan);
                    #endregion
					#endregion

					#region TPL Run
					if (tasks.Count > 0)
					{
						foreach (var item in tasks)
						{
							item.Start();
						}
					}
					#endregion

					#region TPL WaitAll
					Task.WaitAll(tasks.ToArray());
					#endregion
				}
				#endregion

				#region Corporate
				if (input.Corporate != null && input.Corporate.CorporateAccount != null)
				{
                    ecarResult = ecarDomain.GetAvailableFleets(token, input);
				}
				#endregion

				#region Make response
				result = new UtilityAvailableFleetsRS();
				result.Success = new Success();
				result.FleetTypes = new Fleets();
				result.FleetTypes.Items = new List<Common.DTO.OTA.Fleet>();
				if (sdsResult != null && sdsResult.Any())
				{    
                    result.FleetTypes.Items.AddRange(sdsResult);
				}
				if (taxiResult != null)
				{
					result.FleetTypes.Items.Add(taxiResult);
				}
                if (ecarResult != null)
                {
                    result.FleetTypes.Items.Add(ecarResult);
                }
                if (vanResult != null)
                {
                    result.FleetTypes.Items.Add(vanResult);
                }
                if (ecarTexasResult != null)
                { 
                    if (result.FleetTypes != null && result.FleetTypes.Items != null && result.FleetTypes.Items.Any())
                    {
                        if (result.FleetTypes.Items.Where(p => p.Type == Common.DTO.Enum.FleetType.ExecuCar.ToString()).Any())
                        {
                            if (ecarTexasResult.Now.ToLower()=="true")
                            {
                                result.FleetTypes.Items.Where(p => p.Type == Common.DTO.Enum.FleetType.ExecuCar.ToString()).FirstOrDefault().Now = "true";
                            }
                            if (ecarTexasResult.Later.ToLower() == "true")
                            {
                                result.FleetTypes.Items.Where(p => p.Type == Common.DTO.Enum.FleetType.ExecuCar.ToString()).FirstOrDefault().Later = "true";
                            }
                            if (ecarTexasResult.DisabilityVehicleInd == true)
                            {
                                result.FleetTypes.Items.Where(p => p.Type == Common.DTO.Enum.FleetType.ExecuCar.ToString()).FirstOrDefault().DisabilityVehicleInd=true;
                            }
                        }
                        else
                        {
                            result.FleetTypes.Items.Add(ecarTexasResult);
                        }
                    }
                    else
                    {
                        result.FleetTypes.Items.Add(ecarTexasResult);
                    }
                   
                }

                #endregion

            #endregion


            rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("UtilityAvailableFleet", ex);

				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// Returns the list of Airlines available for an Airport.
		/// </summary>
		/// <param name="input">UtilityGetAirlinesByAirport Request Object with the Airport Code</param>
		/// <returns name="UtilityGetAirlinesByAirportRS">Returns a UtilityGetAirlinesByAirport Response Object with the list of all the Airlines</returns>
		public UtilityGetAirlinesByAirportRS UtilityGetAirlinesByAirport(UtilityGetAirlinesByAirportRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.UtilityGetAirlines;
			UtilityGetAirlinesByAirportRS result = null;
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var utilityDomain = new UtilityDomain(); utilityDomain.TrackTime = trackTime;
			var taxiDomain = new TaxiDomain();
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_UtilityGetAirlines(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

				#region Call Domian Method
				result = utilityDomain.GetAirlinesByAirport(token, input);
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("UtilityGetAirlines", ex);

				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// Returns the list of the Trips based on a Status like Completed,Booked,Cancelled etc.
		/// </summary>
		/// <param name="input">UtilityGetTripList Request Object with type of vehicle like Taxi/Ecar  and the Status</param>
		/// <returns name="UtilityGetTripListRS">Returns a UtilityGetTripList Response Object with the list of All the trips available.</returns>
		public UtilityGetTripListRS UtilityGetTripList(UtilityGetTripListRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.UtilityGetTripList;
			UtilityGetTripListRS result = null;
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var utilityDomain = new UtilityDomain(); utilityDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_UtilityGetTripList(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

				#region Call Domian Method
				result = utilityDomain.GetTripList(token, input);
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("UtilityGetTripList", ex);

				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="input"></param>
		/// <returns name="UtilityGetAirportInstructionsRS"></returns>
		public UtilityGetAirportInstructionsRS UtilityGetAirportInstructions(UtilityGetAirportInstructionsRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.UtilityGetAirportInstructions;
			UtilityGetAirportInstructionsRS result = null;
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var utilityDomain = new UtilityDomain(); utilityDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_UtilityGetAirportInstructions(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

				#region Call Domian Method
				result = utilityDomain.GetAirportInstructions(token, input);
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("UtilityGetAirportInstructions", ex);
                
				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// Returns a list of supported Honorifics in our system like :  Mr, M, Ms. Miss, Mrs
		/// </summary>
		/// <param name="input">UtilityGetHonorifics Request Object with the Token Details</param>
		/// <returns name="UtilityGetHonorificsRS">Returns a list of UtilityGetHonorifics Response Object with the list of Honorifics</returns>
		public UtilityGetHonorificsRS UtilityGetHonorifics(UtilityGetHonorificsRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.UtilityGetHonorifics;
			UtilityGetHonorificsRS result = null;
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var utilityDomain = new UtilityDomain(); utilityDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_UtilityGetHonorifics(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

				#region Call Domian Method
				result = utilityDomain.GetHonorifics(token, input);
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("UtilityGetHonorifics", ex);

				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// Returns a List of Booking Criteria based on a Corporate
		/// </summary>
		/// <param name="input">UtilityGetBookingCriteria Request Object</param>
		/// <returns name="UtilityGetBookingCriteriaRS">Returns a UtilityGetBookingCriteria Response Object</returns>
		public UtilityGetBookingCriteriaRS UtilityGetBookingCriteria(UtilityGetBookingCriteriaRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.UtilityGetBookingCriteria;
			UtilityGetBookingCriteriaRS result = null;
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var utilityDomain = new UtilityDomain(); utilityDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_UtilityGetBookingCriteria(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

				#region Call Domian Method
				result = utilityDomain.UtilityGetBookingCriteria(token, input);
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("UtilityGetBookingCriteria", ex);

				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}

		/// <summary>
		/// Returns a list of requirements based on a corporation
		/// Created: 6/4/2015
		/// </summary>
		/// <param name="input">UtilityGetCorporateRequirementsRQ</param>
		/// <returns name="UtilityGetCorporateRequirementsRS">UtilityGetCorporateRequirementsRS</returns>
		public UtilityGetCorporateRequirementsRS UtilityGetCorporateRequirements(UtilityGetCorporateRequirementsRQ input)
		{
			#region Init
			const Method methodName = Method.UtilityGetCorporateRequirements;
			UtilityGetCorporateRequirementsRS result = null;
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain { TrackTime = trackTime };
			var utilityDomain = new UtilityDomain { TrackTime = trackTime };
			var vtodDomain = new VTODDomain { TrackTime = trackTime };
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize(),
					OperationContext.Current.RequestContext.RequestMessage).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_UtilityGetCorporateRequirements(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

				#region Call Domian Method
				result = utilityDomain.UtilityGetCorporateRequirements(token, input);
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("UtilityGetCorporateRequirements", ex);

				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// Get corporate requirements typeahead
		/// </summary>
		/// <param name="username"></param>
		/// <param name="corporate"></param>
		/// <param name="corporateCompanyName"></param>
		/// <param name="accountName"></param>
		/// <param name="requirementName"></param>
		/// <param name="letters"></param>
		/// <returns></returns>
		public List<string> UtilityTypeAhead(string username, string corporate, string corporateCompanyName, string accountName, string requirementName, string letters)
		{
			#region Init
			const Method methodName = Method.UtilityTypeAhead;
			List<string> result = null;
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain { TrackTime = trackTime };
			var utilityDomain = new UtilityDomain { TrackTime = trackTime };
			var vtodDomain = new VTODDomain { TrackTime = trackTime };
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize(),
					OperationContext.Current.RequestContext.RequestMessage).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				if (string.IsNullOrWhiteSpace(username))
				{
					throw VtodException.CreateFieldRequiredValidationException("username");
				}

				if (string.IsNullOrWhiteSpace(corporate))
				{
					throw VtodException.CreateFieldRequiredValidationException("corporate");
				}
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

				#region Call Domian Method
				result = utilityDomain.UtilityGetCorporateRequirementsTypeAhead(token, username, corporate,
					corporateCompanyName, accountName, requirementName, letters);
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("UtilityTypeAhead", ex);

				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}

		/// <summary>
		/// Returns a list of payment methods based on a corporation
		/// Created: 6/5/2015
		/// </summary>
		/// <param name="input">UtilityGetCorporateRequirementsRQ</param>
		/// <returns name="UtilityGetCorporateRequirementsRS">UtilityGetCorporateRequirementsRS</returns>
		public UtilityGetCorporateRequirementsRS UtilityGetCorporatePaymentMethod(UtilityGetCorporateRequirementsRQ input)
		{
			#region Init
			const Method methodName = Method.UtilityGetCorporatePaymentMethod;
			UtilityGetCorporateRequirementsRS result = null;
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain { TrackTime = trackTime };
			var utilityDomain = new UtilityDomain { TrackTime = trackTime };
			var vtodDomain = new VTODDomain { TrackTime = trackTime };
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize(),
					OperationContext.Current.RequestContext.RequestMessage).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_UtilityGetCorporateRequirements(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

				#region Call Domian Method
				result = utilityDomain.UtilityGetCorporatePaymentMethod(token, input);
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("UtilityGetCorporatePaymentMethod", ex);

				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// Returns a list of Airport PickUp Options
		/// </summary>
		/// <param name="input">UtilityGetAirportPickupOptions Request Object</param>
		/// <returns name="UtilityGetAirportPickupOptionsRS">UtilityGetAirportPickupOptions Response Object</returns>
		public UtilityGetAirportPickupOptionsRS UtilityGetAirportPickupOptions(UtilityGetAirportPickupOptionsRQ input)
		{

			#region Init
			var methodName = Common.DTO.Enum.Method.UtilityGetAirportPickupOptions;
			UtilityGetAirportPickupOptionsRS result = null;
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var utilityDomain = new UtilityDomain(); utilityDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();
			bool isAuthenricate = false;			
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_UtilityGetAirportPickupOptions(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

				#region Call Domian Method
				result = utilityDomain.UtilityGetAirportPickupOptions(token, input);
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("UtilityGetAirportPickupOptions", ex);


				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}

        public UtilityGetLeadTimeRS UtilityGetLeadTime(UtilityGetLeadTimeRQ input)
        {

            #region Init
            var methodName = Common.DTO.Enum.Method.UtilityGetLeadTime;
            UtilityGetLeadTimeRS result = null;            
            var rawRequest = string.Empty;
            var rawResponse = string.Empty;
            var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
            var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
            var utilityDomain = new UtilityDomain(); utilityDomain.TrackTime = trackTime;
            var taxiDomain = new TaxiDomain();
            var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
            
            var validationDomain = new ValidationDomain();
            var token = new TokenRS();
            bool isAuthenricate = false;            
            #endregion
            try
            {
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
                #endregion

                #region Log Request
                rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
                if (isDebug)
                {
                    logger.InfoFormat("Request{0}", rawRequest);
                }
                #endregion

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
                #endregion

                #region Modify Token
                Modification.ModifyTokenRS(ref token);
                #endregion

                #region Validate request object
                validationDomain.Validate_UtilityGetLeadTime(input);
                #endregion

                #region Validate Token
                validationDomain.Validate_SDS_Token(token);
                #endregion

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
                #endregion

                #region Authenticate
                isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
                if (!isAuthenricate)
                    throw VtodException.CreateException(ExceptionType.Auth, 101);
                #endregion

                #region Call Domian Method
                Fleet ecarResult = null;

                #region Corporate
                if (input.Corporate != null && input.Corporate.CorporateAccount != null)
                {
                    ECarDomain ecarDomain = new ECarDomain();
                    ecarResult = ecarDomain.GetCorporateLeadTime(token, input);
                }
                #endregion

                #region Make response
                result = new UtilityGetLeadTimeRS();
                result.Success = new Success();
                result.FleetTypes = new Fleets();
                result.FleetTypes.Items = new List<Common.DTO.OTA.Fleet>();
                if (ecarResult != null)
                {
                    result.FleetTypes.Items.Add(ecarResult);
                }
                #endregion
                #endregion

                rawResponse = result.XmlSerialize().ToString();

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
                #endregion
            }
            catch (Exception ex)
            {
                logger.Error("UtilityGetLeadTime", ex);
                var exception = getOtaException(ex);
                rawResponse = exception.Errors.XmlSerialize().ToString();
                throw exception;
            }
            finally
            {
                try
                {
                    #region Log Reponse
                    if (isDebug)
                    {
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
                    #endregion

                    #region Track
                    trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
                    #endregion

                    #region SaveTrackTime
                    vtodDomain.InsertTrack(trackTime);
                    trackTime.Dispose();
                    #endregion
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }

            }

            return result;
        }


        public UtilityGetClientTokenRS UtilityGetClientToken(UtilityGetClientTokenRQ input)
        {

            #region Init
            var methodName = Common.DTO.Enum.Method.UtilityGetClientToken;
            UtilityGetClientTokenRS result = null;
            var rawRequest = string.Empty;
            var rawResponse = string.Empty;
            var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
            var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
            var utilityDomain = new UtilityDomain(); utilityDomain.TrackTime = trackTime;
            var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;

            var validationDomain = new ValidationDomain();
            var token = new TokenRS();
            bool isAuthenricate = false;
            #endregion
            try
            {
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
                #endregion

                #region Log Request
                rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
                if (isDebug)
                {
                    logger.InfoFormat("Request{0}", rawRequest);
                }
                #endregion

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
                #endregion

                #region Modify Token
                Modification.ModifyTokenRS(ref token);
                #endregion

                #region Validate request object
                validationDomain.Validate_UtilityGetClientToken(input);
                #endregion

                #region Validate Token
                validationDomain.Validate_SDS_Token(token);
                #endregion

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
                #endregion

                #region Authenticate
                isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
                if (!isAuthenricate)
                    throw VtodException.CreateException(ExceptionType.Auth, 101);
                #endregion

                #region Call Domian Method
                #region Corporate
                if (input.Corporate != null && input.Corporate.CorporateAccount != null)
                {
                    ECarDomain ecarDomain = new ECarDomain();
                    result = ecarDomain.GetCorporateClientToken(token, input);
                }
                #endregion

                #region Make response
                result.Success = new Success();
                #endregion
                #endregion

                rawResponse = result.XmlSerialize().ToString();

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
                #endregion
            }
            catch (Exception ex)
            {
                logger.Error("UtilityGetClientToken", ex);
                var exception = getOtaException(ex);
                rawResponse = exception.Errors.XmlSerialize().ToString();
                throw exception;
            }
            finally
            {
                try
                {
                    #region Log Reponse
                    if (isDebug)
                    {
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
                    #endregion

                    #region Track
                    trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
                    #endregion

                    #region SaveTrackTime
                    vtodDomain.InsertTrack(trackTime);
                    trackTime.Dispose();
                    #endregion
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }

            }

            return result;
        }



        #endregion

        #region Rate
        /// <summary>
        /// Sets a Rate of a Trip
        /// </summary>
        /// <param name="input">RateSet Request Object which contains Member ID,Trip ID,Rate etc.</param>
        /// <returns name="RateSetRS">Returns a RateSet Response Object with Success/Failure status.</returns>
        public RateSetRS RateSet(RateSetRQ input)
		{
			#region Init
			RateSetRS result = null;
			var methodName = Common.DTO.Enum.Method.RateSet;
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var sdsDomain = new SDSDomain(trackTime);
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();
			bool isAuthenricate = false;			
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_RateSetRQ(input);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

				#region Call Domian Method
				result = vtodDomain.RateSet(input);
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("RateSet", ex);
                
				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }					
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}
			}

			return result;
		}
		/// <summary>
		/// It returns the Rate of a Trip
		/// </summary>
		/// <param name="input">RateGet Request Object which contains Member ID,Trip ID etc</param>
		/// <returns name="RateGetRS">Returns a RateGet Response Object with Success/Failure status and Rate.</returns>
		public RateGetRS RateGet(RateGetRQ input)
		{
			#region Init
			RateGetRS result = null;
			var methodName = Common.DTO.Enum.Method.RateGet;
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var sdsDomain = new SDSDomain(trackTime);
			var validationDomain = new ValidationDomain();
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion

			try
			{
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_RateGetRQ(input);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

				#region Call Domian Method
				result = vtodDomain.RateGet(input);
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("RateGet", ex);
                
				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}
			}

			return result;
		}

		#endregion

		#region Group
		/// <summary>
		/// Validates a Discount Code
		/// </summary>
		/// <param name="input">GroupValidateDiscountCode Request Object with a Discount Code</param>
		/// <returns name="GroupValidateDiscountCodeRS">Returns a GroupValidateDiscountCode Response Object with IsValid True or False</returns>
		public GroupValidateDiscountCodeRS GroupValidateDiscountCode(GroupValidateDiscountCodeRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.GroupValidateDiscountCode;
			GroupValidateDiscountCodeRS result = null;
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var validationDomain = new ValidationDomain();
			var groupDomain = new GroupDomain(); groupDomain.TrackTime = trackTime;
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion
			try
			{
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_GroupValidateDiscountCodeRQ(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

				#region Call Domian Method

				result = groupDomain.ValidateDiscountCode(token, input);
				if (result == null)
				{
					throw VtodException.CreateException(ExceptionType.General, 1);
				}
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("GroupValidateDiscountCode", ex);

				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// Based on the Discount ID it returns the list Of Airport which qualifies for it.
		/// </summary>
		/// <param name="input">GroupGetDiscount Request Object with a Discount ID</param>
		/// <returns name="GroupGetDiscountRS">Returns a GroupValidateDiscountCode Response Object with the AiportCodes/Discount Applies(Yes/No)</returns>
		public GroupGetDiscountRS GroupGetDiscount(GroupGetDiscountRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.GroupGetDiscount;
			GroupGetDiscountRS result = null;
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var validationDomain = new ValidationDomain();
			var groupDomain = new GroupDomain(); groupDomain.TrackTime = trackTime;
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion
			try
			{
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_GroupGetDiscountRQ(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

				#region Call Domian Method
				result = groupDomain.GetDiscount(token, input);
				if (result == null)
				{
					throw VtodException.CreateException(ExceptionType.General, 1);
				}
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("GroupGetDiscount", ex);

				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// It gives us the Promotion Type
		/// </summary>
		/// <param name="input">GroupGetPromotionType Request Object with the Promotion Code</param>
		/// <returns name="GroupGetPromotionTypeRS">Returns a GroupGetPromotionType Response Object with the Promotion Type</returns>
		public GroupGetPromotionTypeRS GroupGetPromotionType(GroupGetPromotionTypeRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.GroupGetPromotionType;
			GroupGetPromotionTypeRS result = null;
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var validationDomain = new ValidationDomain();
			var groupDomain = new GroupDomain(); groupDomain.TrackTime = trackTime;
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion
			try
			{
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_GroupGetPromotionTypeRQ(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

				#region Call Domian Method
				result = groupDomain.GetPromotionCodeType(token, input);
				if (result == null)
				{
					throw VtodException.CreateException(ExceptionType.General, 1);
				}
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("GroupGetPromotionType", ex);

				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// Retrieves the Defaults of a Discount like whether it allows Inbound OutBound Trip,Outbound Inbound Trip etc.
		/// </summary>
		/// <param name="input">GroupGetDefaults Request Object with the Discount ID.</param>
		/// <returns name="GroupGetDefaultsRS">Returns a GroupGetDefaults Response Object with the defaults.</returns>
		public GroupGetDefaultsRS GroupGetDefaults(GroupGetDefaultsRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.GroupGetDefaults;
			GroupGetDefaultsRS result = null;
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var validationDomain = new ValidationDomain();
			var groupDomain = new GroupDomain(); groupDomain.TrackTime = trackTime;
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion
			try
			{
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_GroupGetDefaultsRQ(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

				#region Call Domian Method
				result = groupDomain.GetDefaults(token, input);
				if (result == null)
				{
					throw VtodException.CreateException(ExceptionType.General, 1);
				}
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("GroupGetDefaults", ex);

				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// Retrieves the list of the Landmarks based on the Group ID
		/// </summary>
		/// <param name="input">GroupGetLandmarks Request Object with the Group ID</param>
		/// <returns name="GroupGetLandmarksRS">Returns a GroupGetDefaults Response Object with contains the list of Landmarks</returns>
		public GroupGetLandmarksRS GroupGetLandmarks(GroupGetLandmarksRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.GroupGetLandmarks;
			GroupGetLandmarksRS result = null;
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var validationDomain = new ValidationDomain();
			var groupDomain = new GroupDomain(); groupDomain.TrackTime = trackTime;
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion
			try
			{
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_GroupGetLandmarksRQ(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

				#region Call Domian Method
				result = groupDomain.GetLandmarks(token, input);
				if (result == null)
				{
					throw VtodException.CreateException(ExceptionType.General, 1);
				}
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("GroupGetLandmarks", ex);

				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}
		/// <summary>
		/// Returns the list of the Airports
		/// </summary>
		/// <param name="input">GroupGetLocalizedServicedAirports Request Object with the Group ID</param>
		/// <returns name="GroupGetLocalizedServicedAirportsRS">Returns a GroupGetLocalizedServicedAirports Response Object with the list of Airports.</returns>
		public GroupGetLocalizedServicedAirportsRS GroupGetLocalizedServicedAirports(GroupGetLocalizedServicedAirportsRQ input)
		{
			#region Init
			var methodName = Common.DTO.Enum.Method.GroupGetLocalizedServicedAirports;
			GroupGetLocalizedServicedAirportsRS result = null;
			var rawRequest = string.Empty;
			var rawResponse = string.Empty;
			var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
			var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
			var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
			var validationDomain = new ValidationDomain();
			var groupDomain = new GroupDomain(); groupDomain.TrackTime = trackTime;
			var token = new TokenRS();
			bool isAuthenricate = false;
			#endregion
			try
			{
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
				#endregion

				#region Log Request
				rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
				if (isDebug)
				{
                    logger.InfoFormat("Request{0}", rawRequest);
                }
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
				#endregion

				#region Modify Token
				Modification.ModifyTokenRS(ref token);
				#endregion

				#region Validate request object
				validationDomain.Validate_GroupGetLocalizedServicedAirportsRQ(input);
				#endregion

				#region Validate Token
				validationDomain.Validate_SDS_Token(token);
				#endregion

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
				#endregion

				#region Authenticate
				isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
				if (!isAuthenricate)
					throw VtodException.CreateException(ExceptionType.Auth, 101);
				#endregion

				#region Call Domian Method
				result = groupDomain.GetLocalizedServicedAirports(token, input);
				if (result == null)
				{
					throw VtodException.CreateException(ExceptionType.General, 1);
				}
				#endregion

				rawResponse = result.XmlSerialize().ToString();

				#region Track
				trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error("GroupGetLocalizedServicedAirports", ex);

				var exception = getOtaException(ex);
				rawResponse = exception.Errors.XmlSerialize().ToString();
				throw exception;
			}
			finally
			{
				try
				{
					#region Log Reponse
					if (isDebug)
					{
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
					#endregion

					#region Track
					trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
					#endregion

					#region SaveTrackTime
					vtodDomain.InsertTrack(trackTime);
					trackTime.Dispose();
					#endregion
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

			}

			return result;
		}

        #endregion

        #region Split Payment
        public SplitPaymentInviteRS SplitPaymentInvite(SplitPaymentInviteRQ input)
        {
            #region Init
            var methodName = Method.SplitPaymentInvite;
            SplitPaymentInviteRS result = null;
            var rawRequest = string.Empty;
            var rawResponse = string.Empty;
            var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
            var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
            var splitPaymentDomain = new SplitPaymentDomain(); splitPaymentDomain.TrackTime = trackTime;
            var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
            var validationDomain = new ValidationDomain();
            var token = new TokenRS();
            bool isAuthenricate = false;
            #endregion

            try
            {
                #region Get Header
                var requestMessages = OperationContext.Current.IncomingMessageProperties[HttpRequestMessageProperty.Name] as HttpRequestMessageProperty;

                try
                {
                    if (requestMessages.Headers["Username"] != null)
                        token.Username = requestMessages.Headers["Username"];
                }
                catch { }
                try
                {
                    if (requestMessages.Headers["SecurityKey"] != null)
                        token.SecurityKey = requestMessages.Headers["SecurityKey"];
                }
                catch { }
                #endregion

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
                #endregion

                #region Log Request
                rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
                if (isDebug)
                {
                    logger.InfoFormat("Request{0}", rawRequest);
                }
                #endregion

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
                #endregion

                #region Modify Token
                Modification.ModifyTokenRS(ref token);
                #endregion

                #region Validate request object
                validationDomain.Validate_SplitPaymentInvite(input);
                #endregion

                #region Validate Token
                validationDomain.Validate_SDS_Token(token);
                #endregion

                #region Validate Data

                long memberBID = 0;
                using (var db = new VTODEntities())
                {
                    #region Check if user b is verified
                    var phoneNumber = string.Format("{0}{1}{2}", input.MemberB.TelephoneInfo.CountryAccessCode, input.MemberB.TelephoneInfo.AreaCityCode, input.MemberB.TelephoneInfo.PhoneNumber).CleanPhone();

                    ObjectParameter output = new ObjectParameter("MemberID", typeof(long));
                    db.SP_CheckVerification(phoneNumber, output);
                    var outputMemberID = (long)output.Value;

                    //TODO: Change it back later
                    //if (outputMemberID == -2)  // phone number doesn't exist
                    //    throw VtodException.CreateException(ExceptionType.Taxi, 2023);
                    //else if (outputMemberID == -1)  // phone number not verified
                    //    throw VtodException.CreateException(ExceptionType.Taxi, 9008);
                    if (outputMemberID < 0)
                        throw VtodException.CreateException(ExceptionType.Taxi, 2023);

                    memberBID = outputMemberID;
                    #endregion

                    var vtodTrip = db.vtod_trip.FirstOrDefault(m => m.Id == input.TripID);
                    var taxiTrip = db.taxi_trip.FirstOrDefault(m => m.Id == input.TripID);
                    if (vtodTrip == null || taxiTrip == null)
                        throw VtodException.CreateException(ExceptionType.Taxi, 2022);

                    // check if it is MTData taxi trip
                    if (vtodTrip.FleetType != FleetType.Taxi.ToString())
                        throw VtodException.CreateException(ExceptionType.Taxi, 2022);

                    // check if split payment is enabled for this fleet
                    if (!taxiTrip.taxi_fleet.SplitPayementEnabled.HasValue
                        || !taxiTrip.taxi_fleet.SplitPayementEnabled.Value)
                        throw VtodException.CreateException(ExceptionType.Taxi, 2022);

                    // check payment type
                    if (vtodTrip.PaymentType != PaymentType.PaymentCard.ToString())
                        throw VtodException.CreateException(ExceptionType.Taxi, 2001);

                    // check trip status
                    var validStatus = ConfigurationManager.AppSettings["SplitPaymentValidStatus"].Split(',');
                    if (!validStatus.Any(m => m == vtodTrip.FinalStatus))
                        throw VtodException.CreateException(ExceptionType.Taxi, 2024);

                    // check split payment status
                    if (vtodTrip.IsSplitPayment != null && vtodTrip.IsSplitPayment.Value)
                    {
                        if (vtodTrip.SplitPaymentStatus == (int)SplitPaymentStatus.Accepted)
                            throw VtodException.CreateException(ExceptionType.Taxi, 2026);
                        else if (vtodTrip.SplitPaymentStatus == (int)SplitPaymentStatus.Offered)
                            throw VtodException.CreateException(ExceptionType.Taxi, 2027);
                        else if (vtodTrip.SplitPaymentStatus == (int)SplitPaymentStatus.Completed)
                            throw VtodException.CreateException(ExceptionType.Taxi, 2028);
                    }
                }
                #endregion

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
                #endregion

                #region Authenticate
                isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
                if (!isAuthenricate)
                    throw VtodException.CreateException(ExceptionType.Auth, 101);
                #endregion

                #region Call Domain Method
                result = splitPaymentDomain.Invite(token, input.TripID, input.MemberA.MemberID, memberBID.ToString());
                #endregion

                result.EchoToken = input.EchoToken;
                result.Target = input.Target;
                result.Version = input.Version;
                result.PrimaryLangID = input.PrimaryLangID;

                rawResponse = result.XmlSerialize().ToString();

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
                #endregion

            }
            catch (Exception ex)
            {
                logger.Error(methodName.ToString(), ex);

                var exception = getOtaException(ex);
                rawResponse = exception.Errors.XmlSerialize().ToString();
                throw exception;
            }
            finally
            {
                try
                {
                    #region Log Reponse
                    if (isDebug)
                    {
                        logger.InfoFormat("Request{0}", rawRequest);
                    }
                    #endregion

                    #region Track
                    trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
                    #endregion

                    #region SaveTrackTime
                    vtodDomain.InsertTrack(trackTime);
                    trackTime.Dispose();
                    #endregion
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }

            }

            return result;
        }

        public SplitPaymentUpdateStatusRS SplitPaymentUpdateStatus(SplitPaymentUpdateStatusRQ input)
        {
            #region Init
            var methodName = Method.SplitPaymentUpdateStatus;
            SplitPaymentUpdateStatusRS result = null;
            var rawRequest = string.Empty;
            var rawResponse = string.Empty;
            var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
            var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
            var splitPaymentDomain = new SplitPaymentDomain(); splitPaymentDomain.TrackTime = trackTime;
            var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
            var validationDomain = new ValidationDomain();
            var token = new TokenRS();
            bool isAuthenricate = false;
            #endregion

            try
            {
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
                #endregion

                #region Log Request
                rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
                if (isDebug)
                {
                    logger.InfoFormat("Request{0}", rawRequest);
                }
                #endregion

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
                #endregion

                #region Modify Token
                Modification.ModifyTokenRS(ref token);
                #endregion

                #region Validate request object
                validationDomain.Validate_SplitPaymentUpdateStatus(input);
                #endregion

                #region Validate Token
                validationDomain.Validate_SDS_Token(token);
                #endregion

                #region Validate Data 
                // check if this trip exists and status is valid
                int? prevSplitPaymentStatus;
                using (var db = new VTODEntities())
                {
                    #region Check if this trip exists 
                    var vtodTrip = db.vtod_trip.FirstOrDefault(m => m.Id == input.TripID);
                    if (vtodTrip == null)
                        throw VtodException.CreateException(ExceptionType.Taxi, 2022);
                    #endregion

                    #region Check trip status
                    var validStatus = ConfigurationManager.AppSettings["SplitPaymentValidStatus"].Split(',');
                    if (!validStatus.Any(m => m == vtodTrip.FinalStatus))
                        throw VtodException.CreateException(ExceptionType.Taxi, 2024);
                    #endregion

                    #region Check split status
                    if (vtodTrip.SplitPaymentStatus == (int)SplitPaymentStatus.Completed)
                        throw VtodException.CreateException(ExceptionType.Taxi, 2001);
                    if (!string.IsNullOrWhiteSpace(input.MemberBID) &&
                        (vtodTrip.SplitPaymentMemberB != input.MemberBID || vtodTrip.SplitPaymentStatus == (int)SplitPaymentStatus.Canceled))
                        throw VtodException.CreateException(ExceptionType.Taxi, 2025);
                    #endregion

                    prevSplitPaymentStatus = vtodTrip.SplitPaymentStatus;
                }
                #endregion

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
                #endregion

                #region Authenticate
                isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
                if (!isAuthenricate)
                    throw VtodException.CreateException(ExceptionType.Auth, 101);
                #endregion

                #region Call Domain Method
                result = splitPaymentDomain.UpdateStatus(token, input.TripID, input.MemberAID, input.MemberBID, input.Status);

                #region Link or delink member reservation
                if (result.Success != null)
                {
                    if (!string.IsNullOrWhiteSpace(input.MemberBID) && input.Status)
                    {
                        // Link this reservation with member b
                        try
                        {
                            membershipDomain.LinkReservation(token, input.TripID, 0, int.Parse(input.MemberBID));
                        }
                        catch (Exception ex)
                        {
                            logger.Error(methodName.ToString()+"_LinkReservation", ex);
                        }
                    }
                    else if (!input.Status)  // member a cancels or member b declines
                    {
                        if (prevSplitPaymentStatus == (int)SplitPaymentStatus.Accepted)
                        {
                            // remove this reservation from member b's list
                            try
                            {
                                membershipDomain.DelinkReservation(token, input.TripID, 0, int.Parse(input.MemberBID));
                            }
                            catch (Exception ex)
                            {
                                logger.Error(methodName.ToString()+"DelinkReservation", ex);
                            }
                        }
                    }
                }
                #endregion

                #endregion

                result.EchoToken = input.EchoToken;
                result.Target = input.Target;
                result.Version = input.Version;
                result.PrimaryLangID = input.PrimaryLangID;

                rawResponse = result.XmlSerialize().ToString();

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
                #endregion

            }
            catch (Exception ex)
            {
                logger.Error(methodName.ToString(), ex);

                var exception = getOtaException(ex);
                rawResponse = exception.Errors.XmlSerialize().ToString();
                throw exception;
            }
            finally
            {
                try
                {
                    #region Log Reponse
                    if (isDebug)
                    {
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
                    #endregion

                    #region Track
                    trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
                    #endregion

                    #region SaveTrackTime
                    vtodDomain.InsertTrack(trackTime);
                    trackTime.Dispose();
                    #endregion
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }

            }

            return result;
        }

        public SplitPaymentGetInvitationsRS SplitPaymentGetInvitations(SplitPaymentGetInvitationsRQ input)
        {
            #region Init
            var methodName = Method.SplitPaymentGetInvitations;
            SplitPaymentGetInvitationsRS result = null;
            var rawRequest = string.Empty;
            var rawResponse = string.Empty;
            var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
            var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
            var splitPaymentDomain = new SplitPaymentDomain(); splitPaymentDomain.TrackTime = trackTime;
            var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
            var validationDomain = new ValidationDomain();
            var token = new TokenRS();
            bool isAuthenricate = false;
            #endregion

            try
            {
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
                #endregion

                #region Log Request
                rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
                if (isDebug)
                {
                    logger.InfoFormat("Request{0}", rawRequest);
                }
                #endregion

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
                #endregion

                #region Modify Token
                Modification.ModifyTokenRS(ref token);
                #endregion

                #region Validate request object
                validationDomain.Validate_SplitPaymentGetInvitations(input);
                #endregion

                #region Validate Token
                validationDomain.Validate_SDS_Token(token);
                #endregion

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
                #endregion

                #region Authenticate
                isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
                if (!isAuthenricate)
                    throw VtodException.CreateException(ExceptionType.Auth, 101);
                #endregion

                #region Call Domain Method
                result = splitPaymentDomain.GetInvitations(token, input.MemberBID, input.TripID);
                #endregion

                result.EchoToken = input.EchoToken;
                result.Target = input.Target;
                result.Version = input.Version;
                result.PrimaryLangID = input.PrimaryLangID;

                rawResponse = result.XmlSerialize().ToString();

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
                #endregion

            }
            catch (Exception ex)
            {
                logger.Error(methodName.ToString(), ex);

                var exception = getOtaException(ex);
                rawResponse = exception.Errors.XmlSerialize().ToString();
                throw exception;
            }
            finally
            {
                try
                {
                    #region Log Reponse
                    if (isDebug)
                    {
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
                    #endregion

                    #region Track
                    trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
                    #endregion

                    #region SaveTrackTime
                    vtodDomain.InsertTrack(trackTime);
                    trackTime.Dispose();
                    #endregion
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }

            }

            return result;
        }

        public SplitPaymentCheckIfEnabledRS SplitPaymentCheckIfEnabled(SplitPaymentCheckIfEnabledRQ input)
        {
            #region Init
            var methodName = Method.SplitPaymentCheckIfEnabled;
            SplitPaymentCheckIfEnabledRS result = null;
            var rawRequest = string.Empty;
            var rawResponse = string.Empty;
            var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
            var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
            var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
            var validationDomain = new ValidationDomain();
            var token = new TokenRS();
            bool isAuthenricate = false;
            #endregion

            try
            {
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
                #endregion

                #region Log Request
                rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
                if (isDebug)
                {
                    logger.InfoFormat("Request{0}", rawRequest);
                }
                #endregion

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
                #endregion

                #region Modify Token
                Modification.ModifyTokenRS(ref token);
                #endregion

                #region Validate request object
                validationDomain.Validate_SplitPaymentCheckIfEnabled(input);
                #endregion

                #region Validate Token
                validationDomain.Validate_SDS_Token(token);
                #endregion

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
                #endregion

                #region Authenticate
                isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
                if (!isAuthenricate)
                    throw VtodException.CreateException(ExceptionType.Auth, 101);
                #endregion

                #region Check if it is enabled or not
                bool isSplitPaymentEnabled = false;

                using (var db = new VTODEntities())
                {
                    var vtodTrip = db.vtod_trip.FirstOrDefault(m => m.Id == input.TripID);
                    var taxiTrip = db.taxi_trip.FirstOrDefault(m => m.Id == input.TripID);
                    if (vtodTrip == null || taxiTrip == null)
                        throw VtodException.CreateException(ExceptionType.Taxi, 2022);

                    var validStatus = ConfigurationManager.AppSettings["SplitPaymentValidStatus"].Split(',');

                    if (vtodTrip.FleetType != FleetType.Taxi.ToString()
                        || !taxiTrip.taxi_fleet.SplitPayementEnabled.HasValue
                        || !taxiTrip.taxi_fleet.SplitPayementEnabled.Value
                        || vtodTrip.PaymentType != PaymentType.PaymentCard.ToString()
                        || !validStatus.Any(m => m == vtodTrip.FinalStatus)
                        || (vtodTrip.IsSplitPayment.HasValue && vtodTrip.IsSplitPayment.Value 
                            && vtodTrip.SplitPaymentStatus.HasValue 
                            && (vtodTrip.SplitPaymentStatus.Value == (int)SplitPaymentStatus.Accepted
                                || vtodTrip.SplitPaymentStatus.Value == (int)SplitPaymentStatus.Completed
                                || vtodTrip.SplitPaymentStatus.Value == (int)SplitPaymentStatus.Offered)))
                    {
                        isSplitPaymentEnabled = false;
                    }
                    else
                    {
                        isSplitPaymentEnabled = true;
                    }
                }

                #endregion

                result = new SplitPaymentCheckIfEnabledRS();
                result.Success = new Success();
                result.IsEnabled = isSplitPaymentEnabled;

                rawResponse = result.XmlSerialize().ToString();

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
                #endregion

            }
            catch (Exception ex)
            {
                logger.Error(methodName.ToString(), ex);

                var exception = getOtaException(ex);
                rawResponse = exception.Errors.XmlSerialize().ToString();
                throw exception;
            }
            finally
            {
                try
                {
                    #region Log Reponse
                    if (isDebug)
                    {
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }                    
                    #endregion

                    #region Track
                    trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
                    #endregion

                    #region SaveTrackTime
                    vtodDomain.InsertTrack(trackTime);
                    trackTime.Dispose();
                    #endregion
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }

            }

            return result;
        }

        #endregion
        
        #region IVR Methods

        public UtilityCustomerInfoRS UtilityGetCustomerInfo(UtilityCustomerInfoRQ input)
        {
            #region Init
            var methodName = Common.DTO.Enum.Method.UtilityGetCustomerInfo;
            UtilityCustomerInfoRS result = null;
            var rawRequest = string.Empty;
            var rawResponse = string.Empty;
            var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
            var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
            var sdsDomain = new SDSDomain(trackTime);            
            var taxiDomain = new TaxiDomain();
            var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
            var vtodUtility = new VTODUtilityDomain(); vtodUtility.TrackTime = trackTime;
            var validationDomain = new ValidationDomain();
            var token = new TokenRS();
            bool isAuthenricate = false;
            #endregion

            try
            {
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
                #endregion

                #region Log Request
                rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
                if (isDebug)
                {
                    logger.InfoFormat("Request{0}", rawRequest);
                }
                #endregion

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
                #endregion

                #region Modify Token
                Modification.ModifyTokenRS(ref token);
                #endregion

                #region Validate Token
                validationDomain.Validate_SDS_Token(token);
                #endregion

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
                #endregion

                #region Authenticate
                isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
                if (!isAuthenricate)
                    throw VtodException.CreateException(ExceptionType.Auth, 101);
                #endregion

                #region Get FleetType & DispatchType
                #region Fleet Type
                var fleetType = input.RateQualifiers.First().RateQualifierValue.ToFleetType();
                #endregion

                #region DispatchType
                var dispatchType = Common.DTO.Enum.DispatchType.Unkown;// input.GroundReservations.First().RateQualifiers.First().RateQualifierValue.ToFleetType();

                if (fleetType == FleetType.Taxi)
                {
                    dispatchType = DispatchType.Taxi;
                }
                else if (fleetType == FleetType.ExecuCar)
                {
                    dispatchType = DispatchType.ECar;
                }
                else if (fleetType == Common.DTO.Enum.FleetType.SuperShuttle || fleetType == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly)
                {
                    dispatchType = DispatchType.SDS;
                }
				else
				{
                    dispatchType = DispatchType.Unkown;
                }
               
                #endregion

                #region Call Domian Method
                if (dispatchType == DispatchType.Taxi)
                {
                    result = taxiDomain.GetCustomerInfo(token, input);
                }
                else
                {
                    throw VtodException.CreateException(ExceptionType.General, 2002); // new Exception(Messages.General_DispatchTypeError);
                }
                if (result == null)
                {
                    throw VtodException.CreateException(ExceptionType.General, 1);
                }
                #endregion
                #endregion

                rawResponse = result.XmlSerialize().ToString();

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
                #endregion
            }
            catch (Exception ex)
            {
                logger.Error("UtilityCustomerInfo", ex);
                var exception = getOtaException(ex);
                rawResponse = exception.Errors.XmlSerialize().ToString();
                throw exception;
            }
            finally
            {
                try
                {
                    #region Log Reponse
                    if (isDebug)
                    {
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
                    #endregion

                    #region Track
                    trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
                    #endregion

                    #region SaveTrackTime
                    trackTime.Dispose();
                    #endregion
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }

            }

            return result;
        }

        public UtilityGetPickupAddressRS UtilityGetPickUpAddress(UtilityGetPickupAddressRQ input)
        {

            #region Init
            var methodName = Common.DTO.Enum.Method.UtilityGetPickUpAddress;
            UtilityGetPickupAddressRS result = null;
            var rawRequest = string.Empty;
            var rawResponse = string.Empty;
            var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
            var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
            var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;

            var sdsDomain = new SDSDomain(trackTime);            
            var taxiDomain = new TaxiDomain();
            
            var vtodUtility = new VTODUtilityDomain(); vtodUtility.TrackTime = trackTime;
            var validationDomain = new ValidationDomain();
            var token = new TokenRS();
            bool isAuthenricate = false;
            #endregion

            try
            {
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
                #endregion

                #region Log Request
                rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
                if (isDebug)
                {
                    logger.InfoFormat("Request{0}", rawRequest);
                }
                #endregion

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
                #endregion

                #region Modify Token
                Modification.ModifyTokenRS(ref token);
                #endregion

                #region Validate Token
                validationDomain.Validate_SDS_Token(token);
                #endregion

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
                #endregion

                #region Authenticate
                isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
                if (!isAuthenricate)
                    throw VtodException.CreateException(ExceptionType.Auth, 101);
                #endregion

                #region Get FleetType & DispatchType
                #region Fleet Type
                var fleetType = input.RateQualifiers.First().RateQualifierValue.ToFleetType();
                #endregion

                #region DispatchType
                var dispatchType = Common.DTO.Enum.DispatchType.Unkown;// input.GroundReservations.First().RateQualifiers.First().RateQualifierValue.ToFleetType();

                if (fleetType == FleetType.Taxi)
                {
                    dispatchType = DispatchType.Taxi;
                }
                else if (fleetType == FleetType.ExecuCar)
                {
                    dispatchType = DispatchType.ECar;
                }
                else if (fleetType == Common.DTO.Enum.FleetType.SuperShuttle || fleetType == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly)
                {
                    dispatchType = DispatchType.SDS;
                }
                else
                {
                    dispatchType = DispatchType.Unkown;
                }

                #endregion

                #region Call Domian Method
                if (dispatchType == DispatchType.Taxi)
                {
                    result = taxiDomain.GetPickupAddress(token, input);
                }
                else
                {
                    throw VtodException.CreateException(ExceptionType.General, 2002); // new Exception(Messages.General_DispatchTypeError);
                }
                if (result == null)
                {
                    throw VtodException.CreateException(ExceptionType.General, 1);
                }
                #endregion
                #endregion
                rawResponse = result.XmlSerialize().ToString();

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
                #endregion
            }
            catch (Exception ex)
            {
                logger.Error("UtilityGetPickUpAddress", ex);
                var exception = getOtaException(ex);
                rawResponse = exception.Errors.XmlSerialize().ToString();
                throw exception;
            }
            finally
            {
                try
                {
                    #region Log Reponse
                    if (isDebug)
                    {
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
                    #endregion

                    #region Track
                    trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
                    #endregion

                    #region SaveTrackTime
                    trackTime.Dispose();
                    #endregion
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }

            }

            return result;
        }

        public UtilityGetLastTripRS UtilityGetLastTrip(UtilityGetLastTripRQ input)
        {

            #region Init
            var methodName = Common.DTO.Enum.Method.UtilityGetPickUpAddress;
            UtilityGetLastTripRS result = null;
            var rawRequest = string.Empty;
            var rawResponse = string.Empty;
            var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
            var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
            var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
            var sdsDomain = new SDSDomain(trackTime);            
            var taxiDomain = new TaxiDomain();
            var vtodUtility = new VTODUtilityDomain(); vtodUtility.TrackTime = trackTime;
            var validationDomain = new ValidationDomain();
            var token = new TokenRS();
            bool isAuthenricate = false;
            #endregion

            try
            {
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
                #endregion

                #region Log Request
                rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
                if (isDebug)
                {
                    logger.InfoFormat("Request{0}", rawRequest);
                }
                #endregion

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
                #endregion

                #region Modify Token
                Modification.ModifyTokenRS(ref token);
                #endregion

                #region Validate Token
                validationDomain.Validate_SDS_Token(token);
                #endregion

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
                #endregion

                #region Authenticate
                isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
                if (!isAuthenricate)
                    throw VtodException.CreateException(ExceptionType.Auth, 101);
                #endregion

                #region Get FleetType & DispatchType
                #region Fleet Type
                var fleetType = input.RateQualifiers.First().RateQualifierValue.ToFleetType();
                #endregion

                #region DispatchType
                var dispatchType = Common.DTO.Enum.DispatchType.Unkown;// input.GroundReservations.First().RateQualifiers.First().RateQualifierValue.ToFleetType();

                if (fleetType == FleetType.Taxi)
                {
                    dispatchType = DispatchType.Taxi;
                }
                else if (fleetType == FleetType.ExecuCar)
                {
                    dispatchType = DispatchType.ECar;
                }
                else if (fleetType == Common.DTO.Enum.FleetType.SuperShuttle || fleetType == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly)
                {
                    dispatchType = DispatchType.SDS;
                }
                else
                {
                    dispatchType = DispatchType.Unkown;
                }

                #endregion

                #region Call Domian Method
                if (dispatchType == DispatchType.Taxi)
                {
                    result = taxiDomain.GetLastTrip(token, input);
                }
                else
                {
                    throw VtodException.CreateException(ExceptionType.General, 2002); // new Exception(Messages.General_DispatchTypeError);
                }
                if (result == null)
                {
                    throw VtodException.CreateException(ExceptionType.General, 1);
                }
                #endregion
                #endregion

                rawResponse = result.XmlSerialize().ToString();

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
                #endregion
            }
            catch (Exception ex)
            {
                logger.Error("UtilityGetLastTrip", ex);
                var exception = getOtaException(ex);
                rawResponse = exception.Errors.XmlSerialize().ToString();
                throw exception;
            }
            finally
            {
                try
                {
                    #region Log Reponse
                    if (isDebug)
                    {
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
                    #endregion

                    #region Track
                    trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
                    #endregion

                    #region SaveTrackTime
                    trackTime.Dispose();
                    #endregion
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }

            }

            return result;
        }

        public UtilityGetBlackoutPeriodRS UtilityGetBlackoutPeriod(UtilityGetBlackoutPeriodRQ input)
        {

            #region Init
            var methodName = Common.DTO.Enum.Method.UtilityGetCustomerInfo;
            UtilityGetBlackoutPeriodRS result = null;
            var rawRequest = string.Empty;
            var rawResponse = string.Empty;
            var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
            var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
            var sdsDomain = new SDSDomain(trackTime);            
            var taxiDomain = new TaxiDomain();
            var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
            var vtodUtility = new VTODUtilityDomain(); vtodUtility.TrackTime = trackTime;
            var validationDomain = new ValidationDomain();
            var token = new TokenRS();
            bool isAuthenricate = false;
            #endregion

            try
            {
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
                #endregion

                #region Log Request
                rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
                if (isDebug)
                {
                    logger.InfoFormat("Request{0}", rawRequest);
                }
                #endregion

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
                #endregion

                #region Modify Token
                Modification.ModifyTokenRS(ref token);
                #endregion

                #region Validate Token
                validationDomain.Validate_SDS_Token(token);
                #endregion

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
                #endregion

                #region Authenticate
                isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
                if (!isAuthenricate)
                    throw VtodException.CreateException(ExceptionType.Auth, 101);
                #endregion

                #region Get FleetType & DispatchType
                #region Fleet Type
                var fleetType = input.RateQualifiers.First().RateQualifierValue.ToFleetType();
                #endregion

                #region DispatchType
                var dispatchType = Common.DTO.Enum.DispatchType.Unkown;// input.GroundReservations.First().RateQualifiers.First().RateQualifierValue.ToFleetType();

                if (fleetType == FleetType.Taxi)
                {
                    dispatchType = DispatchType.Taxi;
                }
                else if (fleetType == FleetType.ExecuCar)
                {
                    dispatchType = DispatchType.ECar;
                }
                else if (fleetType == Common.DTO.Enum.FleetType.SuperShuttle || fleetType == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly)
                {
                    dispatchType = DispatchType.SDS;
                }
                else
                {
                    dispatchType = DispatchType.Unkown;
                }

                #endregion

                #region Call Domian Method
                if (dispatchType == DispatchType.Taxi)
                {
                    result = taxiDomain.GetBlackoutPeriod(token, input);
                }
                else
                {
                    throw VtodException.CreateException(ExceptionType.General, 2002); // new Exception(Messages.General_DispatchTypeError);
                }
                if (result == null)
                {
                    throw VtodException.CreateException(ExceptionType.General, 1);
                }
                #endregion
                #endregion

                rawResponse = result.XmlSerialize().ToString();
                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
                #endregion
            }
            catch (Exception ex)
            {
                logger.Error("UtilityGetBlackoutPeriod", ex);
                var exception = getOtaException(ex);
                rawResponse = exception.Errors.XmlSerialize().ToString();
                throw exception;
            }
            finally
            {
                try
                {
                    #region Log Reponse
                    if (isDebug)
                    {
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
                    #endregion

                    #region Track
                    trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
                    #endregion

                    #region SaveTrackTime
                    trackTime.Dispose();
                    #endregion
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }

            }

            return result;
        }

        public UtilityGetNotificationRS UtilityGetStaleNotification(UtilityGetNotificationRQ input)
        {

            #region Init
            var methodName = Common.DTO.Enum.Method.UtilityGetCustomerInfo;
            UtilityGetNotificationRS result = null;
            var rawRequest = string.Empty;
            var rawResponse = string.Empty;
            var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
            var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
            var sdsDomain = new SDSDomain(trackTime);            
            var taxiDomain = new TaxiDomain();
            var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
            var vtodUtility = new VTODUtilityDomain(); vtodUtility.TrackTime = trackTime;
            var validationDomain = new ValidationDomain();
            var token = new TokenRS();
            bool isAuthenricate = false;
            #endregion

            try
            {
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
                #endregion

                #region Log Request
                rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
                if (isDebug)
                {
                    logger.InfoFormat("Request{0}", rawRequest);
                }
                #endregion

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
                #endregion

                #region Modify Token
                Modification.ModifyTokenRS(ref token);
                #endregion

                #region Validate Token
                validationDomain.Validate_SDS_Token(token);
                #endregion

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
                #endregion

                #region Authenticate
                isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
                if (!isAuthenricate)
                    throw VtodException.CreateException(ExceptionType.Auth, 101);
                #endregion

                #region Get FleetType & DispatchType
                #region Fleet Type
                var fleetType = input.RateQualifiers.First().RateQualifierValue.ToFleetType();
                #endregion

                #region DispatchType
                var dispatchType = Common.DTO.Enum.DispatchType.Unkown;// input.GroundReservations.First().RateQualifiers.First().RateQualifierValue.ToFleetType();

                if (fleetType == FleetType.Taxi)
                {
                    dispatchType = DispatchType.Taxi;
                }
                else if (fleetType == FleetType.ExecuCar)
                {
                    dispatchType = DispatchType.ECar;
                }
                else if (fleetType == Common.DTO.Enum.FleetType.SuperShuttle || fleetType == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly)
                {
                    dispatchType = DispatchType.SDS;
                }
                else
                {
                    dispatchType = DispatchType.Unkown;
                }

                #endregion

                #region Call Domian Method
                if (dispatchType == DispatchType.Taxi)
                {
                    result = taxiDomain.GetStaleNotification(token, input);
                }
                else
                {
                    throw VtodException.CreateException(ExceptionType.General, 2002); // new Exception(Messages.General_DispatchTypeError);
                }
                if (result == null)
                {
                    throw VtodException.CreateException(ExceptionType.General, 1);
                }
                #endregion
                #endregion

                rawResponse = result.XmlSerialize().ToString();
                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
                #endregion
            }
            catch (Exception ex)
            {
                logger.Error("UtilityGetStaleNotification", ex);
                var exception = getOtaException(ex);
                rawResponse = exception.Errors.XmlSerialize().ToString();
                throw exception;
            }
            finally
            {
                try
                {
                    #region Log Reponse
                    if (isDebug)
                    {
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
                    #endregion

                    #region Track
                    trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
                    #endregion

                    #region SaveTrackTime
                    trackTime.Dispose();
                    #endregion
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }

            }

            return result;
        }

        public UtilityGetNotificationRS UtilityGetArrivalNotification(UtilityGetNotificationRQ input)
        {

            #region Init
            var methodName = Common.DTO.Enum.Method.UtilityGetCustomerInfo;
            UtilityGetNotificationRS result = null;
            var rawRequest = string.Empty;
            var rawResponse = string.Empty;
            var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
            var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
            var sdsDomain = new SDSDomain(trackTime);            
            var taxiDomain = new TaxiDomain();
            var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
            var vtodUtility = new VTODUtilityDomain(); vtodUtility.TrackTime = trackTime;
            var validationDomain = new ValidationDomain();
            var token = new TokenRS();
            bool isAuthenricate = false;
            #endregion

            try
            {
                GetHeaderAndMakeToken(token);

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
                #endregion

                #region Log Request
                rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
                if (isDebug)
                {
                    logger.InfoFormat("Request{0}", rawRequest);
                }
                #endregion

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
                #endregion

                #region Modify Token
                Modification.ModifyTokenRS(ref token);
                #endregion

                #region Validate Token
                validationDomain.Validate_SDS_Token(token);
                #endregion

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
                #endregion

                #region Authenticate
                isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
                if (!isAuthenricate)
                    throw VtodException.CreateException(ExceptionType.Auth, 101);
                #endregion

                #region Get FleetType & DispatchType
                #region Fleet Type
                var fleetType = input.RateQualifiers.First().RateQualifierValue.ToFleetType();
                #endregion

                #region DispatchType
                var dispatchType = Common.DTO.Enum.DispatchType.Unkown;// input.GroundReservations.First().RateQualifiers.First().RateQualifierValue.ToFleetType();

                if (fleetType == FleetType.Taxi)
                {
                    dispatchType = DispatchType.Taxi;
                }
                else if (fleetType == FleetType.ExecuCar)
                {
                    dispatchType = DispatchType.ECar;
                }
                else if (fleetType == Common.DTO.Enum.FleetType.SuperShuttle || fleetType == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly)
                {
                    dispatchType = DispatchType.SDS;
                }
                else
                {
                    dispatchType = DispatchType.Unkown;
                }

                #endregion

                #region Call Domian Method
                if (dispatchType == DispatchType.Taxi)
                {
                    result = taxiDomain.GetArrivalNotification(token, input);
                }
                else
                {
                    throw VtodException.CreateException(ExceptionType.General, 2002); // new Exception(Messages.General_DispatchTypeError);
                }
                if (result == null)
                {
                    throw VtodException.CreateException(ExceptionType.General, 1);
                }
                #endregion
                #endregion

                rawResponse = result.XmlSerialize().ToString();
                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
                #endregion
            }
            catch (Exception ex)
            {
                logger.Error("UtilityGetArrivalNotification", ex);
                var exception = getOtaException(ex);
                rawResponse = exception.Errors.XmlSerialize().ToString();
                throw exception;
            }
            finally
            {
                try
                {
                    #region Log Reponse
                    if (isDebug)
                    {
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
                    #endregion

                    #region Track
                    trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
                    #endregion

                    #region SaveTrackTime
                    trackTime.Dispose();
                    #endregion
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }

            }

            return result;
        }

        public UtilityGetNotificationRS UtilityGetNightBeforeReminder(UtilityGetNotificationRQ input)
        {

            #region Init
            var methodName = Common.DTO.Enum.Method.UtilityGetCustomerInfo;
            UtilityGetNotificationRS result = null;
            var rawRequest = string.Empty;
            var rawResponse = string.Empty;
            var trackTime = new TrackTime(LogOwnerType.VTOD, methodName);
            var membershipDomain = new MembershipDomain(); membershipDomain.TrackTime = trackTime;
            var sdsDomain = new SDSDomain(trackTime);            
            var taxiDomain = new TaxiDomain();
            var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
            var vtodUtility = new VTODUtilityDomain(); vtodUtility.TrackTime = trackTime;
            var validationDomain = new ValidationDomain();
            var token = new TokenRS();
            bool isAuthenricate = false;
            #endregion

            try
            {
                #region Get Header
                var requestMessages = OperationContext.Current.IncomingMessageProperties[HttpRequestMessageProperty.Name] as HttpRequestMessageProperty;

                try
                {
                    if (requestMessages.Headers["Username"] != null)
                        token.Username = requestMessages.Headers["Username"];
                }
                catch { }
                try
                {
                    if (requestMessages.Headers["SecurityKey"] != null)
                        token.SecurityKey = requestMessages.Headers["SecurityKey"];
                }
                catch { }
                #endregion

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Process Header");
                #endregion

                #region Log Request
                rawRequest = string.Format("Header: \r\n {0} \r\n Body: \r\n {1}", token.XmlSerialize().ToString(), System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.ToString()).CleanSecurityKey();
                if (isDebug)
                {
                    logger.InfoFormat("Request{0}", rawRequest);
                }
                #endregion

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
                #endregion

                #region Modify Token
                Modification.ModifyTokenRS(ref token);
                #endregion

                #region Validate Token
                validationDomain.Validate_SDS_Token(token);
                #endregion

                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Validate");
                #endregion

                #region Authenticate
                isAuthenricate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
                if (!isAuthenricate)
                    throw VtodException.CreateException(ExceptionType.Auth, 101);
                #endregion

                #region Get FleetType & DispatchType
                #region Fleet Type
                var fleetType = input.RateQualifiers.First().RateQualifierValue.ToFleetType();
                #endregion

                #region DispatchType
                var dispatchType = Common.DTO.Enum.DispatchType.Unkown;// input.GroundReservations.First().RateQualifiers.First().RateQualifierValue.ToFleetType();

                if (fleetType == FleetType.Taxi)
                {
                    dispatchType = DispatchType.Taxi;
                }
                else if (fleetType == FleetType.ExecuCar)
                {
                    dispatchType = DispatchType.ECar;
                }
                else if (fleetType == Common.DTO.Enum.FleetType.SuperShuttle || fleetType == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly)
                {
                    dispatchType = DispatchType.SDS;
                }
                else
                {
                    dispatchType = DispatchType.Unkown;
                }

                #endregion

                #region Call Domian Method
                if (dispatchType == DispatchType.Taxi)
                {
                    result = taxiDomain.GetNightBeforeReminder(token, input);
                }
                else
                {
                    throw VtodException.CreateException(ExceptionType.General, 2002); // new Exception(Messages.General_DispatchTypeError);
                }
                if (result == null)
                {
                    throw VtodException.CreateException(ExceptionType.General, 1);
                }
                #endregion
                #endregion

                rawResponse = result.XmlSerialize().ToString();
                #region Track
                trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Response Ready");
                #endregion
            }
            catch (Exception ex)
            {
                logger.Error("UtilityGetNightBeforeReminder", ex);
                var exception = getOtaException(ex);
                rawResponse = exception.Errors.XmlSerialize().ToString();
                throw exception;
            }
            finally
            {
                try
                {
                    #region Log Reponse
                    if (isDebug)
                    {
                        logger.InfoFormat("Response:{0}", rawResponse);
                    }
                    #endregion

                    #region Track
                    trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
                    #endregion

                    #region SaveTrackTime
                    trackTime.Dispose();
                    #endregion
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }

            }

            return result;
        }

        #endregion

		#region WindowsService
		#region Do not delete
		//public void PullSDSStatus()
		//{
		//	#region Intit
		//	var trackTime = new TrackTime(LogOwnerType.VTOD, Method.WindowsService_PullSDSStatus);
		//	var vtodDomain = new UDI.VTOD.Domain.VTOD.VTODDomain();
		//	#endregion

		//	try
		//	{
		//		var sdsDomain = new UDI.VTOD.Domain.SDS.SDSDomain(); sdsDomain.TrackTime = trackTime;

		//		#region Token
		//		#region Read from Config
		//		var username = System.Configuration.ConfigurationManager.AppSettings["SDS_Username"];
		//		var password = System.Configuration.ConfigurationManager.AppSettings["SDS_Password"];
		//		var fleetType = "SuperShuttle_ExecuCar";
		//		var version = System.Configuration.ConfigurationManager.AppSettings["Version"];
		//		#endregion

		//		#region Making TokenRQ
		//		var tokenRQ = new UDI.VTOD.Common.DTO.OTA.TokenRQ();
		//		tokenRQ.Username = username;
		//		tokenRQ.Password = password;
		//		tokenRQ.FleetType = fleetType;
		//		tokenRQ.Version = version;
		//		#endregion

		//		#region Get TokenRS
		//		var queryController = new UDI.VTOD.Controller.QueryController();
		//		var token = queryController.GetToken(tokenRQ);
		//		#endregion

		//		#region Modify Token
		//		token.ClientIPAddress = System.Configuration.ConfigurationManager.AppSettings["SDS_TokenClientIPAddress"];
		//		token.ClientType = System.Configuration.ConfigurationManager.AppSettings["SDS_TokenClientType"].ToInt32();
		//		#endregion
		//		#endregion

		//		trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_WindowsService_SDSStatusService, "Get TokenRS");

		//		#region Get RezSource
		//		List<string> rezSources = sdsDomain.GetAllUserRezSources();

		//		#endregion

		//		trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_WindowsService_SDSStatusService, "Get RezSource");

		//		#region Get Status
		//		foreach (var rezSource in rezSources)
		//		{
		//			sdsDomain.GetStatus(token, rezSource);
		//		}
		//		#endregion

		//		trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_WindowsService_SDSStatusService, "Get Status");
		//	}
		//	catch (Exception ex)
		//	{
		//		string.Format(DateTime.Now.ToString() + " Error: {0}", ex).WriteToConsole();

		//		logger.Error(DateTime.Now.ToString() + " Error: ", ex);
		//	}
		//	finally
		//	{
		//		#region Log Reponse
		//		try
		//		{
		//			#region Track
		//			trackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Controller, "Log");
		//			#endregion

		//			#region SaveTrackTime
		//			vtodDomain.InsertTrack(trackTime);
		//			trackTime.Dispose();
		//			#endregion
		//		}
		//		catch (Exception ex)
		//		{
		//			logger.Error(ex);
		//		}
		//		#endregion
		//	}

		//} 
		#endregion

        public void ProcessDriverInfo()
        {
            #region Intit

            var trackTime = new TrackTime(LogOwnerType.VTOD, Method.WindowsService_ProcessDriverInfo);
            var vtodDomain = new UDI.VTOD.Domain.VTOD.VTODDomain();
            var vanDomain = new UDI.VTOD.Domain.Van.VanDomain();
            const char fieldSeparator = ',';
            string filePath = System.Configuration.ConfigurationManager.AppSettings["DriverDetailsFilePath"];
            var driverInfo = new DriverInfo(logger);
            vtod_driver_info returnedDriverInfo = null;
            #endregion

            try
            {
                #region Token
                TokenRS token = null;
                if (UDI.VTOD.Common.DTO.Static.Token.TokenRS == null || UDI.VTOD.Common.DTO.Static.Token.DateTime.AddMinutes(10) <= DateTime.Now)
                {
                    #region Read from Config
                    var username = System.Configuration.ConfigurationManager.AppSettings["SDS_Username"];
                    var password = System.Configuration.ConfigurationManager.AppSettings["SDS_Password"];
                    var fleetType = "Taxi";
                    var version = System.Configuration.ConfigurationManager.AppSettings["Version"];
                    #endregion

                    #region Making TokenRQ
                    var tokenRQ = new UDI.VTOD.Common.DTO.OTA.TokenRQ();
                    tokenRQ.Username = username;
                    tokenRQ.Password = password;
                    tokenRQ.FleetType = fleetType;
                    tokenRQ.Version = version;
                    #endregion

                    #region Get TokenRS
                    var queryController = new UDI.VTOD.Controller.QueryController();
                    token = queryController.GetToken(tokenRQ);
                    #endregion

                    #region Modify Token
                    token.ClientIPAddress = System.Configuration.ConfigurationManager.AppSettings["SDS_TokenClientIPAddress"];
                    token.ClientType = System.Configuration.ConfigurationManager.AppSettings["SDS_TokenClientType"].ToInt32();
                    #endregion

                    UDI.VTOD.Common.DTO.Static.Token.TokenRS = token;
                    UDI.VTOD.Common.DTO.Static.Token.DateTime = DateTime.Now;
                }
                else
                {
                    token = UDI.VTOD.Common.DTO.Static.Token.TokenRS;
                }
                #endregion

                #region Reading the CSV File
                logger.Info("File Reading Started");
                using (StreamReader SR = new StreamReader(@filePath))
                {
                    bool passedFirstline = false; //to skip the first line
                    while (!SR.EndOfStream) // while not end of file do 
                    {
                        var CSValues = SR.ReadLine().Split(fieldSeparator);
                        if (passedFirstline)
                        {
                            //read a line of our file and split it into its separate values
                            #region Add Drive Details
                            if (CSValues != null)
                            {
                                string ImageUrl = System.Configuration.ConfigurationManager.AppSettings["DriverDetailsImageURL"];
                                var newDriver = new vtod_driver_info();
                                byte[] driverImage = null;
                                newDriver.DriverID = CSValues[2].ToString();
                                newDriver.VehicleID = string.Empty;
                                if (!string.IsNullOrWhiteSpace(CSValues[0].ToString()))
                                {
                                    //string name = CSValues[0].ToString().Replace(",", "");
                                   // newDriver.FirstName = name.Substring(0, name.LastIndexOf(" "));
                                    //newDriver.LastName = name.Substring(name.LastIndexOf(" ") + 1);
                                    newDriver.FirstName =CSValues[0].ToString();
                                    newDriver.LastName = CSValues[1].ToString();
                                }
                                else
                                {
                                    newDriver.FirstName = string.Empty;
                                    newDriver.LastName = string.Empty;
                                }
                                newDriver.CellPhone = CSValues[4].ToString();
                                newDriver.HomePhone = string.Empty;
                                newDriver.AlternatePhone = string.Empty;
                                newDriver.QualificationDate = null;
                                newDriver.zTripQualified = false;
                                newDriver.StartDate = null;
                                newDriver.HrDays = string.Empty;
                                newDriver.LeaseDate = null;
                                newDriver.FleetType = CSValues[5].ToString();
                                newDriver.FleetID = CSValues[6].ToInt32();
                                newDriver.AppendDate = System.DateTime.Now;
                                ImageUrl = ImageUrl + CSValues[2].ToString() + ".jpg";

                                #region Image
                                try
                                {
                                    driverImage = new System.Net.WebClient().DownloadData(@ImageUrl);
                                }
                                catch (Exception ex)
                                {
                                    logger.Error(ex);
                                }
                                #endregion
                                returnedDriverInfo = driverInfo.UpsertDriverDetails(driverInfo: newDriver, driverImage: driverImage);
                            #endregion
                            }
                        }
                        else
                        {
                            passedFirstline = true;
                        }
                    }
                }
                logger.Info("File Reading Completed");
                #endregion
            }
            catch (Exception ex)
            {
                string.Format(DateTime.Now.ToString() + " Error: {0}", ex).WriteToConsole();
                logger.Error(DateTime.Now.ToString() + " Error: ", ex);
                UDI.VTOD.Common.DTO.Static.Token.TokenRS = null;
            }
        }
		public void PullTaxiStatus()
		{
			#region Intit
			var trackTime = new TrackTime(LogOwnerType.VTOD, Method.WindowsService_PullTaxiStatus);
			var vtodDomain = new UDI.VTOD.Domain.VTOD.VTODDomain();
			var taxiDomain = new UDI.VTOD.Domain.Taxi.TaxiDomain();
			#endregion

			try
			{
				#region Token
				TokenRS token = null;
				if (UDI.VTOD.Common.DTO.Static.Token.TokenRS == null || UDI.VTOD.Common.DTO.Static.Token.DateTime.AddMinutes(10) <= DateTime.Now)
				{
					#region Read from Config
					var username = System.Configuration.ConfigurationManager.AppSettings["SDS_Username"];
					var password = System.Configuration.ConfigurationManager.AppSettings["SDS_Password"];
					var fleetType = "SuperShuttle_ExecuCar";
					var version = System.Configuration.ConfigurationManager.AppSettings["Version"];
					#endregion

					#region Making TokenRQ
					var tokenRQ = new UDI.VTOD.Common.DTO.OTA.TokenRQ();
					tokenRQ.Username = username;
					tokenRQ.Password = password;
					tokenRQ.FleetType = fleetType;
					tokenRQ.Version = version;
					#endregion

					#region Get TokenRS
					var queryController = new UDI.VTOD.Controller.QueryController();
					token = queryController.GetToken(tokenRQ);
					#endregion

					#region Modify Token
					token.ClientIPAddress = System.Configuration.ConfigurationManager.AppSettings["SDS_TokenClientIPAddress"];
					token.ClientType = System.Configuration.ConfigurationManager.AppSettings["SDS_TokenClientType"].ToInt32();
					#endregion

					UDI.VTOD.Common.DTO.Static.Token.TokenRS = token;
					UDI.VTOD.Common.DTO.Static.Token.DateTime = DateTime.Now;
				}
				else
				{
					token = UDI.VTOD.Common.DTO.Static.Token.TokenRS;
				}
				#endregion

				#region Get UnComplete Trips
				using (var db = new DataAccess.VTOD.VTODEntities())
				{

					var trips = db.SP_Taxi_GetUnCompletedTrips().ToList();
					if (trips != null && trips.Any())
					{
						#region Pull Status
						//ToDo: Pull Statuses can be MultiThread
						foreach (var trip in trips)
						{
							try
							{
								#region Pull
								var statusRQ = new OTA_GroundResRetrieveRQ();
								var reference = new Reference { Type = Common.DTO.Enum.ConfirmationType.Confirmation.ToString() };
								reference.ID = trip.Id.ToString();
								statusRQ.Reference = new List<Reference>();
								statusRQ.Reference.Add(reference);

								#region Log Request
								logger.Info(statusRQ.XmlSerialize());
								#endregion

								var status = taxiDomain.Status(token, statusRQ);

                                //status.TPA_Extensions.SharedLink

                                #region Log Response
                                logger.Info(status.XmlSerialize());
								#endregion
								#endregion

								#region Notify Driver for Pickup Status
                                try
                                {
                                    if (status != null && status.TPA_Extensions != null && status.TPA_Extensions.Statuses != null && status.TPA_Extensions.Statuses.Status != null)
                                    {
                                        if (status.TPA_Extensions.Statuses.Status.Any() && status.TPA_Extensions.Statuses.Status.FirstOrDefault().Value!=null)
                                        { 
                                        if (status.TPA_Extensions.Statuses.Status.FirstOrDefault().Value.ToLower().ToString() == "Boarded".ToLower().ToString() ||
                                            status.TPA_Extensions.Statuses.Status.FirstOrDefault().Value.ToLower().ToString() == "Pickup".ToLower().ToString())
                                        {
                                            try
                                            {
                                                var notifyDriverRQ = new UtilityNotifyDriverRQ();
                                                notifyDriverRQ.TripID = trip.Id;
                                                var state = vtodDomain.Get_WS_State("vtod_trip", trip.Id.ToInt64(), "NotifyPickup");
                                                if (state == null)
                                                {
                                                    var taxiTrip = vtodDomain.GetTaxiTrip(trip.Id);
                                                    if (taxiTrip != null)
                                                    {
                                                        var firstName = taxiTrip.FirstName;
                                                        var lastName = taxiTrip.LastName;

                                                        //notifyDriverRQ.Message = string.Format(Messages.Notification_NotifyDriver, firstName, lastName, fare.Value + gratuity.Value);
                                                        taxiDomain.NotifyPickupDriver(token, notifyDriverRQ);
                                                        #region save Taxi Pickup Info in ws state
                                                        vtodDomain.Set_WS_State("vtod_trip", trip.Id.ToInt64(), "NotifyPickup", "1");
                                                        #endregion
                                                    }
                                                    else
                                                    {
                                                        throw new Exception(string.Format(Messages.Notification_NotifyDriver, trip.Id));
                                                    }
                                                }
                                                else
                                                {
                                                    logger.InfoFormat("This trip is already has passed the pickup:{0}", trip.Id);
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                logger.Error(ex);
                                            }
                                        }
                                    }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    logger.Error(ex);
                                }
								#endregion
							}
							catch (Exception ex)
							{
								logger.Error("PullTaxiStatus", ex);
                            }
						}
						#endregion
					}
				}
				#endregion
			}
			catch (Exception ex)
			{
				string.Format(DateTime.Now.ToString() + " Error: {0}", ex).WriteToConsole();
				logger.Error(DateTime.Now.ToString() + " Error: ", ex);
                logger.Error("hii");
				UDI.VTOD.Common.DTO.Static.Token.TokenRS = null;
			}
		}
        public void PullVanStatus()
        {
            #region Intit
            var trackTime = new TrackTime(LogOwnerType.VTOD, Method.WindowsService_PullVanStatus);
            var vtodDomain = new UDI.VTOD.Domain.VTOD.VTODDomain();
            var VanDomain = new UDI.VTOD.Domain.Van.VanDomain();
            #endregion

            try
            {
                #region Token
                TokenRS token = null;
                if (UDI.VTOD.Common.DTO.Static.Token.TokenRS == null || UDI.VTOD.Common.DTO.Static.Token.DateTime.AddMinutes(10) <= DateTime.Now)
                {
                    #region Read from Config
                    var username = System.Configuration.ConfigurationManager.AppSettings["SDS_Username"];
                    var password = System.Configuration.ConfigurationManager.AppSettings["SDS_Password"];
                    var fleetType = "Van";
                    var version = System.Configuration.ConfigurationManager.AppSettings["Version"];
                    #endregion

                    #region Making TokenRQ
                    var tokenRQ = new UDI.VTOD.Common.DTO.OTA.TokenRQ();
                    tokenRQ.Username = username;
                    tokenRQ.Password = password;
                    tokenRQ.FleetType = fleetType;
                    tokenRQ.Version = version;
                    #endregion

                    #region Get TokenRS
                    var queryController = new UDI.VTOD.Controller.QueryController();
                    token = queryController.GetToken(tokenRQ);
                    #endregion

                    #region Modify Token
                    token.ClientIPAddress = System.Configuration.ConfigurationManager.AppSettings["SDS_TokenClientIPAddress"];
                    token.ClientType = System.Configuration.ConfigurationManager.AppSettings["SDS_TokenClientType"].ToInt32();
                    #endregion

                    UDI.VTOD.Common.DTO.Static.Token.TokenRS = token;
                    UDI.VTOD.Common.DTO.Static.Token.DateTime = DateTime.Now;
                }
                else
                {
                    token = UDI.VTOD.Common.DTO.Static.Token.TokenRS;
                }
                #endregion

                #region Get UnComplete Trips
                using (var db = new DataAccess.VTOD.VTODEntities())
                {

                    var trips = db.SP_Van_GetUnCompletedTrips().ToList();
                    if (trips != null && trips.Any())
                    {
                        #region Pull Status
                        //ToDo: Pull Statuses can be MultiThread
                        foreach (var trip in trips)
                        {
                            try
                            {
                                #region Pull
                                var statusRQ = new OTA_GroundResRetrieveRQ();
                                var reference = new Reference { Type = Common.DTO.Enum.ConfirmationType.Confirmation.ToString() };
                                reference.ID = trip.Id.ToString();
                                statusRQ.Reference = new List<Reference>();
                                statusRQ.Reference.Add(reference);

                                #region Log Request
                                logger.Info(statusRQ.XmlSerialize());
                                #endregion

                                var status = VanDomain.Status(token, statusRQ);

                                #region Log Response
                                logger.Info(status.XmlSerialize());
                                #endregion
                                #endregion

                                #region Notify Driver for Pickup Status
                                try
                                {
                                    if (status != null && status.TPA_Extensions != null && status.TPA_Extensions.Statuses != null && status.TPA_Extensions.Statuses.Status != null)
                                    {
                                        if (status.TPA_Extensions.Statuses.Status.FirstOrDefault().Value.ToLower().ToString() == "Boarded".ToLower().ToString() ||
                                            status.TPA_Extensions.Statuses.Status.FirstOrDefault().Value.ToLower().ToString() == "Pickup".ToLower().ToString())
                                        {
                                            try
                                            {
                                                var notifyDriverRQ = new UtilityNotifyDriverRQ();
                                                notifyDriverRQ.TripID = trip.Id;
                                                var state = vtodDomain.Get_WS_State("vtod_trip", trip.Id.ToInt64(), "NotifyPickup");
                                                if (state == null)
                                                {
                                                    var VanTrip = vtodDomain.GetVanTrip(trip.Id);
                                                    if (VanTrip != null)
                                                    {
                                                        var firstName = VanTrip.FirstName;
                                                        var lastName = VanTrip.LastName;

                                                        //notifyDriverRQ.Message = string.Format(Messages.Notification_NotifyDriver, firstName, lastName, fare.Value + gratuity.Value);
                                                        VanDomain.NotifyPickupDriver(token, notifyDriverRQ);
                                                        #region save Van Pickup Info in ws state
                                                        vtodDomain.Set_WS_State("vtod_trip", trip.Id.ToInt64(), "NotifyPickup", "1");
                                                        #endregion
                                                    }
                                                    else
                                                    {
                                                        throw new Exception(string.Format(Messages.Notification_NotifyDriver, trip.Id));
                                                    }
                                                }
                                                else
                                                {
                                                    logger.InfoFormat("This trip is already has passed the pickup:{0}", trip.Id);
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                logger.Error(ex);
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    logger.Error(ex);
                                }
                                #endregion
                            }
                            catch (Exception ex)
                            {
                                logger.Error("PullVanStatus", ex);
                            }
                        }
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                string.Format(DateTime.Now.ToString() + " Error: {0}", ex).WriteToConsole();
                logger.Error(DateTime.Now.ToString() + " Error: ", ex);
                UDI.VTOD.Common.DTO.Static.Token.TokenRS = null;
            }
        }
        public void PullEcarStatus()
        {
            #region Intit
            var trackTime = new TrackTime(LogOwnerType.VTOD, Method.WindowsService_PullEcarStatus);
            var vtodDomain = new UDI.VTOD.Domain.VTOD.VTODDomain();
            var ecarDomain = new UDI.VTOD.Domain.ECar.ECarDomain();
            #endregion

            try
            {
                #region Token
                TokenRS token = null;
                if (UDI.VTOD.Common.DTO.Static.Token.TokenRS == null || UDI.VTOD.Common.DTO.Static.Token.DateTime.AddMinutes(10) <= DateTime.Now)
                {
                    #region Read from Config
                    var username = System.Configuration.ConfigurationManager.AppSettings["SDS_Username"];
                    var password = System.Configuration.ConfigurationManager.AppSettings["SDS_Password"];
                    var fleetType = "ExecuCar";
                    var version = System.Configuration.ConfigurationManager.AppSettings["Version"];
                    #endregion

                    #region Making TokenRQ
                    var tokenRQ = new UDI.VTOD.Common.DTO.OTA.TokenRQ();
                    tokenRQ.Username = username;
                    tokenRQ.Password = password;
                    tokenRQ.FleetType = fleetType;
                    tokenRQ.Version = version;
                    #endregion

                    #region Get TokenRS
                    var queryController = new UDI.VTOD.Controller.QueryController();
                    token = queryController.GetToken(tokenRQ);
                    #endregion

                    #region Modify Token
                    token.ClientIPAddress = System.Configuration.ConfigurationManager.AppSettings["SDS_TokenClientIPAddress"];
                    token.ClientType = System.Configuration.ConfigurationManager.AppSettings["SDS_TokenClientType"].ToInt32();
                    #endregion

                    UDI.VTOD.Common.DTO.Static.Token.TokenRS = token;
                    UDI.VTOD.Common.DTO.Static.Token.DateTime = DateTime.Now;
                }
                else
                {
                    token = UDI.VTOD.Common.DTO.Static.Token.TokenRS;
                }
                #endregion

                #region Get UnComplete Trips
                using (var db = new DataAccess.VTOD.VTODEntities())
                {

                    var trips = db.SP_Ecar_GetUnCompletedTrips().ToList();
                    if (trips != null && trips.Any())
                    {
                        #region Pull Status
                        foreach (var trip in trips)
                        {
                            try
                            {
                                #region Pull
                                var statusRQ = new OTA_GroundResRetrieveRQ();
                                var reference = new Reference { Type = Common.DTO.Enum.ConfirmationType.Confirmation.ToString() };
                                reference.ID = trip.Id.ToString();
                                statusRQ.Reference = new List<Reference>();
                                statusRQ.Reference.Add(reference);

                                #region Log Request
                                logger.Info(statusRQ.XmlSerialize());
                                #endregion

                                var status = ecarDomain.Status(token, statusRQ);

                                #region Log Response
                                logger.Info(status.XmlSerialize());
                                #endregion
                                #endregion

                                #region Notify Driver for Pickup Status
                                try
                                {
                                    if (status != null && status.TPA_Extensions != null && status.TPA_Extensions.Statuses != null && status.TPA_Extensions.Statuses.Status != null)
                                    {
                                        if (status.TPA_Extensions.Statuses.Status.Any() && status.TPA_Extensions.Statuses.Status.FirstOrDefault().Value != null)
                                        {
                                            if (status.TPA_Extensions.Statuses.Status.FirstOrDefault().Value.ToLower().ToString() == "Boarded".ToLower().ToString() ||
                                                status.TPA_Extensions.Statuses.Status.FirstOrDefault().Value.ToLower().ToString() == "Pickup".ToLower().ToString())
                                            {
                                                try
                                                {
                                                    var notifyDriverRQ = new UtilityNotifyDriverRQ();
                                                    notifyDriverRQ.TripID = trip.Id;
                                                    var state = vtodDomain.Get_WS_State("vtod_trip", trip.Id.ToInt64(), "NotifyPickup");
                                                    if (state == null)
                                                    {
                                                        var ecarTrip = vtodDomain.GetEcarTrip(trip.Id);
                                                        if (ecarTrip != null)
                                                        {
                                                            var firstName = ecarTrip.FirstName;
                                                            var lastName = ecarTrip.LastName;
                                                            ecarDomain.NotifyPickupDriver(token, notifyDriverRQ);
                                                            #region save Ecar Pickup Info in ws state
                                                            vtodDomain.Set_WS_State("vtod_trip", trip.Id.ToInt64(), "NotifyPickup", "1");
                                                            #endregion
                                                        }
                                                        else
                                                        {
                                                            throw new Exception(string.Format(Messages.Notification_NotifyDriver, trip.Id));
                                                        }
                                                    }
                                                    else
                                                    {
                                                        logger.InfoFormat("This trip is already has passed the pickup:{0}", trip.Id);
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    logger.Error(ex);
                                                }
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    logger.Error(ex);
                                }
                                #endregion
                            }
                            catch (Exception ex)
                            {
                                logger.Error("PullEcarStatus", ex);
                            }
                        }
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                string.Format(DateTime.Now.ToString() + " Error: {0}", ex).WriteToConsole();
                logger.Error(DateTime.Now.ToString() + " Error: ", ex);
                UDI.VTOD.Common.DTO.Static.Token.TokenRS = null;
            }
        }
        public void NotifyTaxiStatus()
		{
			#region Intit
			var trackTime = new TrackTime(LogOwnerType.VTOD, Method.WindowsService_PullTaxiStatus);
			var notificationDomain = new UDI.VTOD.Domain.Notification.NotificationDomain(); notificationDomain.TrackTime = trackTime;
			#endregion

			try
			{
				#region Token
				TokenRS token = null;
				if (UDI.VTOD.Common.DTO.Static.Token.TokenRS == null || UDI.VTOD.Common.DTO.Static.Token.DateTime.AddMinutes(10) <= DateTime.Now)
				{
					#region Read from Config
					var username = System.Configuration.ConfigurationManager.AppSettings["SDS_Username"];
					var password = System.Configuration.ConfigurationManager.AppSettings["SDS_Password"];
					var fleetType = "SuperShuttle_ExecuCar";
					var version = System.Configuration.ConfigurationManager.AppSettings["Version"];
					#endregion

					#region Making TokenRQ
					var tokenRQ = new UDI.VTOD.Common.DTO.OTA.TokenRQ();
					tokenRQ.Username = username;
					tokenRQ.Password = password;
					tokenRQ.FleetType = fleetType;
					tokenRQ.Version = version;
					#endregion

					#region Get TokenRS
					var queryController = new UDI.VTOD.Controller.QueryController();
					token = queryController.GetToken(tokenRQ);
					#endregion

					#region Modify Token
					token.ClientIPAddress = System.Configuration.ConfigurationManager.AppSettings["SDS_TokenClientIPAddress"];
					token.ClientType = System.Configuration.ConfigurationManager.AppSettings["SDS_TokenClientType"].ToInt32();
					#endregion

					UDI.VTOD.Common.DTO.Static.Token.TokenRS = token;
					UDI.VTOD.Common.DTO.Static.Token.DateTime = DateTime.Now;
				}
				else
				{
					token = UDI.VTOD.Common.DTO.Static.Token.TokenRS;
				}
				#endregion

				#region Make TPL List
				List<Task> tasks = new List<Task>();
				tasks = new List<Task>();

				#region old
				//var taskAcceptedStatus = new Task(new Action<object>(notificationDomain.notifyAcceptedStatus), token, TaskCreationOptions.AttachedToParent);
				//tasks.Add(taskAcceptedStatus);

				//var taskCompletedStatus = new Task(new Action<object>(notificationDomain.notifyCompletedStatus), token, TaskCreationOptions.AttachedToParent);
				//tasks.Add(taskCompletedStatus);
				#endregion

				var taskBookedStatus = new Task(new Action<object>(notificationDomain.notifyBookedStatus), token, TaskCreationOptions.AttachedToParent);
				tasks.Add(taskBookedStatus);

				var taskCancledStatus = new Task(new Action<object>(notificationDomain.notifyCanceledStatus), token, TaskCreationOptions.AttachedToParent);
				tasks.Add(taskCancledStatus);

				var taskCompletedStatus = new Task(new Action<object>(notificationDomain.notifyCompletedStatus), token, TaskCreationOptions.AttachedToParent);
				tasks.Add(taskCompletedStatus);

				var taskAssignedStatus = new Task(new Action<object>(notificationDomain.notifyAssignedStatus), token, TaskCreationOptions.AttachedToParent);
				tasks.Add(taskAssignedStatus);

				#endregion

				#region Run TPL
				if (tasks.Count > 0)
				{
					foreach (var item in tasks)
					{
						item.Start();
					}
				}
				#endregion

				#region Finalize
				#endregion
			}
			catch (Exception ex)
			{
				string.Format(DateTime.Now.ToString() + " Error: {0}", ex).WriteToConsole();
				logger.Error(DateTime.Now.ToString() + " Error: ", ex);
				UDI.VTOD.Common.DTO.Static.Token.TokenRS = null;
			}
		}
        public void NotifyVanStatus()
        {
            #region Intit
            var trackTime = new TrackTime(LogOwnerType.VTOD, Method.WindowsService_PullVanStatus);
            var notificationDomain = new UDI.VTOD.Domain.Notification.NotificationDomain(); notificationDomain.TrackTime = trackTime;
            #endregion

            try
            {
                #region Token
                TokenRS token = null;
                if (UDI.VTOD.Common.DTO.Static.Token.TokenRS == null || UDI.VTOD.Common.DTO.Static.Token.DateTime.AddMinutes(10) <= DateTime.Now)
                {
                    #region Read from Config
                    var username = System.Configuration.ConfigurationManager.AppSettings["SDS_Username"];
                    var password = System.Configuration.ConfigurationManager.AppSettings["SDS_Password"];
                    var fleetType = "Van";
                    var version = System.Configuration.ConfigurationManager.AppSettings["Version"];
                    #endregion

                    #region Making TokenRQ
                    var tokenRQ = new UDI.VTOD.Common.DTO.OTA.TokenRQ();
                    tokenRQ.Username = username;
                    tokenRQ.Password = password;
                    tokenRQ.FleetType = fleetType;
                    tokenRQ.Version = version;
                    #endregion

                    #region Get TokenRS
                    var queryController = new UDI.VTOD.Controller.QueryController();
                    token = queryController.GetToken(tokenRQ);
                    #endregion

                    #region Modify Token
                    token.ClientIPAddress = System.Configuration.ConfigurationManager.AppSettings["SDS_TokenClientIPAddress"];
                    token.ClientType = System.Configuration.ConfigurationManager.AppSettings["SDS_TokenClientType"].ToInt32();
                    #endregion

                    UDI.VTOD.Common.DTO.Static.Token.TokenRS = token;
                    UDI.VTOD.Common.DTO.Static.Token.DateTime = DateTime.Now;
                }
                else
                {
                    token = UDI.VTOD.Common.DTO.Static.Token.TokenRS;
                }
                #endregion

                #region Make TPL List
                List<Task> tasks = new List<Task>();
                tasks = new List<Task>();

                #region old
                //var taskAcceptedStatus = new Task(new Action<object>(notificationDomain.notifyAcceptedStatus), token, TaskCreationOptions.AttachedToParent);
                //tasks.Add(taskAcceptedStatus);

                //var taskCompletedStatus = new Task(new Action<object>(notificationDomain.notifyCompletedStatus), token, TaskCreationOptions.AttachedToParent);
                //tasks.Add(taskCompletedStatus);
                #endregion

                var taskBookedStatus = new Task(new Action<object>(notificationDomain.notifyVanBookedStatus), token, TaskCreationOptions.AttachedToParent);
                tasks.Add(taskBookedStatus);

                var taskCancledStatus = new Task(new Action<object>(notificationDomain.notifyVanCanceledStatus), token, TaskCreationOptions.AttachedToParent);
                tasks.Add(taskCancledStatus);

                var taskCompletedStatus = new Task(new Action<object>(notificationDomain.notifyVanCompletedStatus), token, TaskCreationOptions.AttachedToParent);
                tasks.Add(taskCompletedStatus);

                var taskAssignedStatus = new Task(new Action<object>(notificationDomain.notifyVanAssignedStatus), token, TaskCreationOptions.AttachedToParent);
                tasks.Add(taskAssignedStatus);

                #endregion

                #region Run TPL
                if (tasks.Count > 0)
                {
                    foreach (var item in tasks)
                    {
                        item.Start();
                    }
                }
                #endregion

                #region Finalize
                #endregion
            }
            catch (Exception ex)
            {
                string.Format(DateTime.Now.ToString() + " Error: {0}", ex).WriteToConsole();
                logger.Error(DateTime.Now.ToString() + " Error: ", ex);
                UDI.VTOD.Common.DTO.Static.Token.TokenRS = null;
            }
        }
        public void NotifyEcarStatus()
        {
            #region Intit
            var trackTime = new TrackTime(LogOwnerType.VTOD, Method.WindowsService_PullEcarStatus);
            var notificationDomain = new UDI.VTOD.Domain.Notification.NotificationDomain(); notificationDomain.TrackTime = trackTime;
            #endregion

            try
            {
                #region Token
                TokenRS token = null;
                if (UDI.VTOD.Common.DTO.Static.Token.TokenRS == null || UDI.VTOD.Common.DTO.Static.Token.DateTime.AddMinutes(10) <= DateTime.Now)
                {
                    #region Read from Config
                    var username = System.Configuration.ConfigurationManager.AppSettings["SDS_Username"];
                    var password = System.Configuration.ConfigurationManager.AppSettings["SDS_Password"];
                    var fleetType = "ExecuCar";
                    var version = System.Configuration.ConfigurationManager.AppSettings["Version"];
                    #endregion

                    #region Making TokenRQ
                    var tokenRQ = new UDI.VTOD.Common.DTO.OTA.TokenRQ();
                    tokenRQ.Username = username;
                    tokenRQ.Password = password;
                    tokenRQ.FleetType = fleetType;
                    tokenRQ.Version = version;
                    #endregion

                    #region Get TokenRS
                    var queryController = new UDI.VTOD.Controller.QueryController();
                    token = queryController.GetToken(tokenRQ);
                    #endregion

                    #region Modify Token
                    token.ClientIPAddress = System.Configuration.ConfigurationManager.AppSettings["SDS_TokenClientIPAddress"];
                    token.ClientType = System.Configuration.ConfigurationManager.AppSettings["SDS_TokenClientType"].ToInt32();
                    #endregion

                    UDI.VTOD.Common.DTO.Static.Token.TokenRS = token;
                    UDI.VTOD.Common.DTO.Static.Token.DateTime = DateTime.Now;
                }
                else
                {
                    token = UDI.VTOD.Common.DTO.Static.Token.TokenRS;
                }
                #endregion

                #region Make TPL List
                List<Task> tasks = new List<Task>();
                tasks = new List<Task>();

                var taskBookedStatus = new Task(new Action<object>(notificationDomain.notifyEcarBookedStatus), token, TaskCreationOptions.AttachedToParent);
                tasks.Add(taskBookedStatus);

                var taskCancledStatus = new Task(new Action<object>(notificationDomain.notifyEcarCanceledStatus), token, TaskCreationOptions.AttachedToParent);
                tasks.Add(taskCancledStatus);

                var taskCompletedStatus = new Task(new Action<object>(notificationDomain.notifyEcarCompletedStatus), token, TaskCreationOptions.AttachedToParent);
                tasks.Add(taskCompletedStatus);

                var taskAssignedStatus = new Task(new Action<object>(notificationDomain.notifyEcarAssignedStatus), token, TaskCreationOptions.AttachedToParent);
                tasks.Add(taskAssignedStatus);

                #endregion

                #region Run TPL
                if (tasks.Count > 0)
                {
                    foreach (var item in tasks)
                    {
                        item.Start();
                    }
                }
                #endregion

                #region Finalize
                #endregion
            }
            catch (Exception ex)
            {
                string.Format(DateTime.Now.ToString() + " Error: {0}", ex).WriteToConsole();
                logger.Error(DateTime.Now.ToString() + " Error: ", ex);
                UDI.VTOD.Common.DTO.Static.Token.TokenRS = null;
            }
        }
        public void ChargeTaxiTrips()
        {
            #region Intit
            var trackTime = new TrackTime(LogOwnerType.VTOD, Method.WindowsService_PullTaxiStatus);
            var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
            var taxiDomain = new TaxiDomain();
            var accountingDomain = new AccountingDomain(); accountingDomain.TrackTime = trackTime;
            #endregion

            try
            {
                #region Token
                TokenRS token = null;
                if (UDI.VTOD.Common.DTO.Static.Token.TokenRS == null || UDI.VTOD.Common.DTO.Static.Token.DateTime.AddMinutes(10) <= DateTime.Now)
                {
                    #region Read from Config
                    var username = System.Configuration.ConfigurationManager.AppSettings["SDS_Username"];
                    var password = System.Configuration.ConfigurationManager.AppSettings["SDS_Password"];
                    var fleetType = "SuperShuttle_ExecuCar";
                    var version = System.Configuration.ConfigurationManager.AppSettings["Version"];
                    #endregion

                    #region Making TokenRQ
                    var tokenRQ = new UDI.VTOD.Common.DTO.OTA.TokenRQ();
                    tokenRQ.Username = username;
                    tokenRQ.Password = password;
                    tokenRQ.FleetType = fleetType;
                    tokenRQ.Version = version;
                    #endregion

                    #region Get TokenRS
                    var queryController = new UDI.VTOD.Controller.QueryController();
                    token = queryController.GetToken(tokenRQ);
                    #endregion

                    #region Modify Token
                    token.ClientIPAddress = System.Configuration.ConfigurationManager.AppSettings["SDS_TokenClientIPAddress"];
                    token.ClientType = System.Configuration.ConfigurationManager.AppSettings["SDS_TokenClientType"].ToInt32();
                    #endregion

                    UDI.VTOD.Common.DTO.Static.Token.TokenRS = token;
                    UDI.VTOD.Common.DTO.Static.Token.DateTime = DateTime.Now;
                }
                else
                {
                    token = UDI.VTOD.Common.DTO.Static.Token.TokenRS;
                }
                #endregion

                #region Get GetTripsToBeCharged
                using (var db = new DataAccess.VTOD.VTODEntities())
                {
                    //var x = (DataAccess.VTOD.taxi_trip) db.vtod_trip.Where(s => s.Id == 545);

                    var trips = db.SP_Taxi_GetTripsToBeCharged().ToList();
                    if (trips != null && trips.Any())
                    {
                        foreach (var trip in trips)
                        {
                            long? sdsFleetID = null;
                            decimal? totalFare = null;
                            decimal? fare = null;
                            decimal? gratuity = null;
                            string memberID = null;
                            string creditCardID = null;
                            string transactionID = null;
                            decimal? amountCharged = null;
                            decimal? creditsApplied = null;
                            string error = null;
                            bool chargeUserB = false;
                            string transactionIDUserB = null;
                            decimal? amountChargedUserB = null;
                            decimal? creditsAppliedUserB = null;
                            string errorUserB = null;
                            int chargeTry = trip.ChargeTry;
                            string chargeStatus = null;
                            //string chargeError = null;

                            try
                            {
                                #region Getting sdsfleetID

                                var sdsFleetIDs = db.SP_Taxi_GetSDSFleetIdFromTripID(trip.Id).ToList();
                                if (sdsFleetIDs != null && sdsFleetIDs.Any())
                                {
                                    sdsFleetID = sdsFleetIDs.First();
                                }
                                #endregion

                                #region Chareging
                                if (sdsFleetID.HasValue)
                                {
                                    if (trip.FleetTripCode.HasValue && trip.FleetTripCode.Value == FleetTripCode.Taxi_MTDATA)
                                    {
                                        totalFare = trip.TotalFareAmount;
                                        memberID = trip.MemberID;
                                        creditCardID = trip.CreditCardID;
                                        gratuity = trip.Gratuity;

                                        //TODO: For MTData taxi, we calculate gratuity if there is gratuity percentage but no gratuity. Might need to do it for all taxi fleets, but need to test.
                                        if (!gratuity.HasValue || gratuity.Value <= 0)
                                        {
                                            if (trip.GratuityRate.HasValue && trip.GratuityRate.Value > 0 && totalFare.HasValue)
                                            {
                                                var gratuityRate = trip.GratuityRate.Value * (decimal)0.01;
                                                gratuity = decimal.Round(totalFare.Value * gratuityRate / (1 + gratuityRate), 2);
                                            }
                                            else
                                            {
                                                gratuity = 0;
                                            }
                                        }
                                        fare = totalFare - gratuity;

                                        if (trip.IsSplitPayment.HasValue && trip.IsSplitPayment.Value
                                            && trip.SplitPaymentStatus.HasValue && trip.SplitPaymentStatus.Value == (int)SplitPaymentStatus.Accepted)  // split payment
                                        {
                                            #region charging for split payment

                                            // Try charging user b first in half. If it is successful, then charge user a in half. Or charge user a in full
                                            if (!string.IsNullOrWhiteSpace(trip.MemberBCreditCardID))
                                            {
                                                var bChargeUserBSuccess = false;
                                                chargeUserB = true;

                                                // try charging user b in half
                                                try
                                                {
                                                    var result = accountingDomain.ChargeVtodTrip(
                                                        token, trip.Id, sdsFleetID.Value, trip.SplitPaymentMemberB.ToInt32(), trip.MemberBCreditCardID.ToInt32(),
                                                        decimal.Round(fare.Value / 2, 2), decimal.Round(gratuity.Value / 2, 2));
                                                    transactionIDUserB = result.transactionID;
                                                    amountChargedUserB = result.AmountCharged;
                                                    creditsAppliedUserB = result.CreditsApplied;
                                                    if (string.IsNullOrWhiteSpace(transactionIDUserB) && !creditsAppliedUserB.HasValue && creditsAppliedUserB.Value <= 0)
                                                    {
                                                        throw VtodException.CreateException(ExceptionType.Accounting, 8010);
                                                    }

                                                    bChargeUserBSuccess = true;
                                                }
                                                catch (Exception ex)
                                                {
                                                    string.Format(DateTime.Now.ToString() + " Error: {0}", ex).WriteToConsole();
                                                    logger.Error(DateTime.Now.ToString() + " Error: ", ex);
                                                }

                                                if (bChargeUserBSuccess)
                                                {
                                                    // charge user a in half
                                                    var result = accountingDomain.ChargeVtodTrip(
                                                        token, trip.Id, sdsFleetID.Value, memberID.ToInt32(), creditCardID.ToInt32(), decimal.Round(fare.Value / 2, 2), decimal.Round(gratuity.Value / 2, 2));
                                                    transactionID = result.transactionID;
                                                    amountCharged = result.AmountCharged;
                                                    creditsApplied = result.CreditsApplied;
                                                    if (string.IsNullOrWhiteSpace(transactionID) && !creditsApplied.HasValue && creditsApplied.Value <= 0)
                                                    {
                                                        throw VtodException.CreateException(ExceptionType.Accounting, 8010);
                                                    }
                                                }
                                                else
                                                {
                                                    // charge user a in full
                                                    var result = accountingDomain.ChargeVtodTrip(
                                                       token, trip.Id, sdsFleetID.Value, memberID.ToInt32(), creditCardID.ToInt32(), fare.Value, gratuity.Value);
                                                    transactionID = result.transactionID;
                                                    amountCharged = result.AmountCharged;
                                                    creditsApplied = result.CreditsApplied;
                                                    if (string.IsNullOrWhiteSpace(transactionID) && !creditsApplied.HasValue && creditsApplied.Value <= 0)
                                                    {
                                                        throw VtodException.CreateException(ExceptionType.Accounting, 8010);
                                                    }
                                                }

                                                chargeStatus = ChargeStatus.Charged.ToString();
                                            }
                                            else
                                            {
                                                // charge user a in full
                                                var result = accountingDomain.ChargeVtodTrip(token, trip.Id, sdsFleetID.Value, memberID.ToInt32(), creditCardID.ToInt32(), fare.Value, gratuity.Value);
                                                transactionID = result.transactionID;
                                                amountCharged = result.AmountCharged;
                                                creditsApplied = result.CreditsApplied;
                                                //trip.FinalStatus = TaxiTripStatusType.Charged;
                                                //trip.ChargeStatus = Common.DTO.Enum.ChargeStatus.Charged.ToString();// TaxiTripStatusType.Charged;
                                                if (string.IsNullOrWhiteSpace(transactionID) && !creditsApplied.HasValue && creditsApplied.Value <= 0)
                                                {
                                                    throw VtodException.CreateException(ExceptionType.Accounting, 8010);
                                                }

                                                chargeStatus = ChargeStatus.Charged.ToString();// TaxiTripStatusType.Charged;
                                            }
                                            #endregion
                                        }
                                        else
                                        {
                                            var result = accountingDomain.ChargeVtodTrip(token, trip.Id, sdsFleetID.Value, memberID.ToInt32(), creditCardID.ToInt32(), fare.Value, gratuity.Value);
                                            transactionID = result.transactionID;
                                            amountCharged = result.AmountCharged;
                                            creditsApplied = result.CreditsApplied;
                                            //trip.FinalStatus = TaxiTripStatusType.Charged;
                                            //trip.ChargeStatus = Common.DTO.Enum.ChargeStatus.Charged.ToString();// TaxiTripStatusType.Charged;
                                            if (string.IsNullOrWhiteSpace(transactionID) && !creditsApplied.HasValue && creditsApplied.Value <= 0)
                                            {
                                                throw VtodException.CreateException(ExceptionType.Accounting, 8010);
                                            }

                                            chargeStatus = ChargeStatus.Charged.ToString();// TaxiTripStatusType.Charged;
                                        }
                                    }
                                    else
                                    {
                                        totalFare = trip.TotalFareAmount;
                                        memberID = trip.MemberID;
                                        creditCardID = trip.CreditCardID;
                                        gratuity = trip.Gratuity;
                                        if (!gratuity.HasValue || gratuity.Value <= 0)
                                        {
                                            gratuity = 0;
                                        }
                                        fare = totalFare - gratuity;

                                        var result = accountingDomain.ChargeVtodTrip(token, trip.Id, sdsFleetID.Value, memberID.ToInt32(), creditCardID.ToInt32(), fare.Value, gratuity.Value);
                                        transactionID = result.transactionID;
                                        amountCharged = result.AmountCharged;
                                        creditsApplied = result.CreditsApplied;
                                        //trip.FinalStatus = TaxiTripStatusType.Charged;
                                        //trip.ChargeStatus = Common.DTO.Enum.ChargeStatus.Charged.ToString();// TaxiTripStatusType.Charged;
                                        if (string.IsNullOrWhiteSpace(transactionID) && !creditsApplied.HasValue && creditsApplied.Value <= 0)
                                        {
                                            throw VtodException.CreateException(ExceptionType.Accounting, 8010);//throw new Exception(Messages.Accounting_ReturnedNullTransactionID);
                                        }

                                        chargeStatus = Common.DTO.Enum.ChargeStatus.Charged.ToString();// TaxiTripStatusType.Charged;
                                    }
                                }
                                else
                                {
                                    //trip.FinalStatus = TaxiTripStatusType.UnCharged;
                                    //trip.ChargeStatus = Common.DTO.Enum.ChargeStatus.UnCharged.ToString();// TaxiTripStatusType.Charged;
                                    chargeStatus = Common.DTO.Enum.ChargeStatus.UnCharged.ToString();// TaxiTripStatusType.Charged;

                                    error = "No SDS_FleetMerchantID found";
                                    //trip.Error = error;

                                    //chargeTry = -1;
                                }
                                #endregion

                                #region Notifying Driver
                                //if (trip.FinalStatus == TaxiTripStatusType.Charged)
                                //if (trip.ChargeStatus == Common.DTO.Enum.ChargeStatus.Charged.ToString()) // TaxiTripStatusType.Charged)
                                if (chargeStatus == Common.DTO.Enum.ChargeStatus.Charged.ToString()) // TaxiTripStatusType.Charged)
                                {
                                    try
                                    {
                                        var notifyDriverRQ = new UtilityNotifyDriverRQ();
                                        notifyDriverRQ.TripID = trip.Id;

                                        //var dispatchTripId = ((DataAccess.VTOD.taxi_trip)trip).DispatchTripId;
                                        //var xxx = ((DataAccess.VTOD.taxi_trip)trip);
                                        //var xx = db.vtod_trip.Where(s => s.Id == trip.Id).ToList();
                                        var taxiTrip = vtodDomain.GetTaxiTrip(trip.Id);
                                        //var taxiTrip = (DataAccess.VTOD.taxi_trip)db.vtod_trip.Where(s => s.Id == trip.Id).FirstOrDefault();

                                        if (taxiTrip != null)
                                        {
                                            var firstName = taxiTrip.FirstName;
                                            var lastName = taxiTrip.LastName;

                                            notifyDriverRQ.Message = string.Format(Messages.Notification_NotifyDriver, firstName, lastName, fare.Value + gratuity.Value);
                                            //notifyDriverRQ.Message = string.Format(Messages.Notification_NotifyDriver, firstName, lastName,firstName);
                                            taxiDomain.NotifyDriver(token, notifyDriverRQ);
                                        }
                                        else
                                        {
                                            throw new Exception(string.Format(Messages.Notification_NotifyDriver, trip.Id));
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        logger.Error(ex);
                                    }
                                }
                                #endregion
                            }
                            catch (Exception ex)
                            {                             
                                chargeStatus = Common.DTO.Enum.ChargeStatus.Error.ToString();
                                error = ex.Message;
                            }
                            finally
                            {                               
                                chargeTry++;

                                db.SP_vtod_InsertTripAccountingTransaction(trip.Id, transactionID, amountCharged, creditsApplied, error, DateTime.Now, memberID);
                                if (chargeUserB)  // split payment, user b is charged
                                  db.SP_vtod_InsertTripAccountingTransaction(trip.Id, transactionIDUserB, amountChargedUserB, creditsAppliedUserB, errorUserB, DateTime.Now, trip.SplitPaymentMemberB);

                                db.SP_vtod_Update_TripChargeStatus(trip.Id, chargeStatus, chargeTry, error);                                                                
                            }
                        }
                    }                 
                }
                #endregion
            }
            catch (Exception ex)
            {
                string.Format(DateTime.Now.ToString() + " Error: {0}", ex).WriteToConsole();
                logger.Error(DateTime.Now.ToString() + " Error: ", ex);
                UDI.VTOD.Common.DTO.Static.Token.TokenRS = null;
            }
        }
        public void ChargeVanTrips()
        {
            #region Intit
            var trackTime = new TrackTime(LogOwnerType.VTOD, Method.WindowsService_PullVanStatus);
            var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
            var vanDomain = new VanDomain();
            var accountingDomain = new AccountingDomain(); accountingDomain.TrackTime = trackTime;
            #endregion

            try
            {
                #region Token
                TokenRS token = null;
                if (UDI.VTOD.Common.DTO.Static.Token.TokenRS == null || UDI.VTOD.Common.DTO.Static.Token.DateTime.AddMinutes(10) <= DateTime.Now)
                {
                    #region Read from Config
                    var username = System.Configuration.ConfigurationManager.AppSettings["SDS_Username"];
                    var password = System.Configuration.ConfigurationManager.AppSettings["SDS_Password"];
                    var fleetType = "Van";
                    var version = System.Configuration.ConfigurationManager.AppSettings["Version"];
                    #endregion

                    #region Making TokenRQ
                    var tokenRQ = new UDI.VTOD.Common.DTO.OTA.TokenRQ();
                    tokenRQ.Username = username;
                    tokenRQ.Password = password;
                    tokenRQ.FleetType = fleetType;
                    tokenRQ.Version = version;
                    #endregion

                    #region Get TokenRS
                    var queryController = new UDI.VTOD.Controller.QueryController();
                    token = queryController.GetToken(tokenRQ);
                    #endregion

                    #region Modify Token
                    token.ClientIPAddress = System.Configuration.ConfigurationManager.AppSettings["SDS_TokenClientIPAddress"];
                    token.ClientType = System.Configuration.ConfigurationManager.AppSettings["SDS_TokenClientType"].ToInt32();
                    #endregion

                    UDI.VTOD.Common.DTO.Static.Token.TokenRS = token;
                    UDI.VTOD.Common.DTO.Static.Token.DateTime = DateTime.Now;
                }
                else
                {
                    token = UDI.VTOD.Common.DTO.Static.Token.TokenRS;
                }
                #endregion

                #region Get GetTripsToBeCharged
                using (var db = new DataAccess.VTOD.VTODEntities())
                {
                    var trips = db.SP_Van_GetTripsToBeCharged().ToList();
                    if (trips != null && trips.Any())
                    {
                        foreach (var trip in trips)
                        {
                            long? sdsFleetID = null;
                            decimal? totalFare = null;
                            decimal? fare = null;
                            decimal? gratuity = null;
                            string memberID = null;
                            string creditCardID = null;
                            string transactionID = null;
                            decimal? amountCharged = null;
                            decimal? creditsApplied = null;
                            string error = null;
                            int chargeTry = trip.ChargeTry;
                            string chargeStatus = null;
                            //string chargeError = null;

                            try
                            {
                                #region Getting sdsfleetID

                                var sdsFleetIDs = db.SP_van_GetSDSFleetIdFromTripID(trip.Id).ToList();
                                if (sdsFleetIDs != null && sdsFleetIDs.Any())
                                {
                                    sdsFleetID = sdsFleetIDs.First();
                                }
                                #endregion

                                #region Chareging
                                if (sdsFleetID.HasValue)
                                {
                                    totalFare = trip.TotalFareAmount;
                                    memberID = trip.MemberID;
                                    creditCardID = trip.CreditCardID;
                                    gratuity = trip.Gratuity;
                                    if (!gratuity.HasValue || gratuity.Value <= 0)
                                    {
                                        gratuity = 0;
                                    }
                                    fare = totalFare - gratuity;

                                    var result = accountingDomain.ChargeVtodTrip(token, trip.Id, sdsFleetID.Value, memberID.ToInt32(), creditCardID.ToInt32(), fare.Value, gratuity.Value);
                                    transactionID = result.transactionID;
                                    amountCharged = result.AmountCharged;
                                    creditsApplied = result.CreditsApplied;
                                    if (string.IsNullOrWhiteSpace(transactionID) && !creditsApplied.HasValue && creditsApplied.Value <= 0)
                                    {
                                        throw VtodException.CreateException(ExceptionType.Accounting, 8010);//throw new Exception(Messages.Accounting_ReturnedNullTransactionID);
                                    }

                                    chargeStatus = Common.DTO.Enum.ChargeStatus.Charged.ToString();
                                }
                                else
                                {
                                   
                                    chargeStatus = Common.DTO.Enum.ChargeStatus.UnCharged.ToString();
                                    error = "No SDS_FleetMerchantID found";
                                }
                                #endregion

                                #region Notifying Drive
                                if (chargeStatus == Common.DTO.Enum.ChargeStatus.Charged.ToString())
                                {
                                    try
                                    {
                                        var notifyDriverRQ = new UtilityNotifyDriverRQ();
                                        notifyDriverRQ.TripID = trip.Id;
                                        var vanTrip = vtodDomain.GetVanTrip(trip.Id);
                                        if (vanTrip != null)
                                        {
                                            var firstName = vanTrip.FirstName;
                                            var lastName = vanTrip.LastName;

                                            notifyDriverRQ.Message = string.Format(Messages.Notification_NotifyDriver, firstName, lastName, fare.Value + gratuity.Value);
                                            vanDomain.NotifyDriver(token, notifyDriverRQ);
                                        }
                                        else
                                        {
                                            throw new Exception(string.Format(Messages.Notification_NotifyDriver, trip.Id));
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        logger.Error(ex);
                                    }
                                }
                                #endregion
                            }
                            catch (Exception ex)
                            {
                                chargeStatus = Common.DTO.Enum.ChargeStatus.Error.ToString();
                                error = ex.Message;
                            }
                            finally
                            {
                                
                                chargeTry++;
                                db.SP_vtod_InsertTripAccountingTransaction(trip.Id, transactionID, amountCharged, creditsApplied, error, DateTime.Now, null);
                                db.SP_vtod_Update_TripChargeStatus(trip.Id, chargeStatus, chargeTry, error);
                            }
                        }
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                string.Format(DateTime.Now.ToString() + " Error: {0}", ex).WriteToConsole();
                logger.Error(DateTime.Now.ToString() + " Error: ", ex);
                UDI.VTOD.Common.DTO.Static.Token.TokenRS = null;
            }
        }
        public void ChargeEcarTrips()
        {
            #region Intit
            var trackTime = new TrackTime(LogOwnerType.VTOD, Method.WindowsService_PullEcarStatus);
            var vtodDomain = new VTODDomain(); vtodDomain.TrackTime = trackTime;
            var ecarDomain = new ECarDomain();
            var accountingDomain = new AccountingDomain(); accountingDomain.TrackTime = trackTime;
            #endregion

            try
            {
                #region Token
                TokenRS token = null;
                if (UDI.VTOD.Common.DTO.Static.Token.TokenRS == null || UDI.VTOD.Common.DTO.Static.Token.DateTime.AddMinutes(10) <= DateTime.Now)
                {
                    #region Read from Config
                    var username = System.Configuration.ConfigurationManager.AppSettings["SDS_Username"];
                    var password = System.Configuration.ConfigurationManager.AppSettings["SDS_Password"];
                    var fleetType = "SuperShuttle_ExecuCar";
                    var version = System.Configuration.ConfigurationManager.AppSettings["Version"];
                    #endregion

                    #region Making TokenRQ
                    var tokenRQ = new UDI.VTOD.Common.DTO.OTA.TokenRQ();
                    tokenRQ.Username = username;
                    tokenRQ.Password = password;
                    tokenRQ.FleetType = fleetType;
                    tokenRQ.Version = version;
                    #endregion

                    #region Get TokenRS
                    var queryController = new UDI.VTOD.Controller.QueryController();
                    token = queryController.GetToken(tokenRQ);
                    #endregion

                    #region Modify Token
                    token.ClientIPAddress = System.Configuration.ConfigurationManager.AppSettings["SDS_TokenClientIPAddress"];
                    token.ClientType = System.Configuration.ConfigurationManager.AppSettings["SDS_TokenClientType"].ToInt32();
                    #endregion

                    UDI.VTOD.Common.DTO.Static.Token.TokenRS = token;
                    UDI.VTOD.Common.DTO.Static.Token.DateTime = DateTime.Now;
                }
                else
                {
                    token = UDI.VTOD.Common.DTO.Static.Token.TokenRS;
                }
                #endregion

                #region Get GetTripsToBeCharged
                using (var db = new DataAccess.VTOD.VTODEntities())
                {
                    var trips = db.SP_Ecar_GetTripsToBeCharged().ToList();
                    if (trips != null && trips.Any())
                    {
                        foreach (var trip in trips)
                        {
                            long? sdsFleetID = null;
                            decimal? totalFare = null;
                            decimal? fare = null;
                            decimal? gratuity = null;
                            string memberID = null;
                            string creditCardID = null;
                            string transactionID = null;
                            decimal? amountCharged = null;
                            decimal? creditsApplied = null;
                            string error = null;
                            bool chargeUserB = false;
                            string transactionIDUserB = null;
                            decimal? amountChargedUserB = null;
                            decimal? creditsAppliedUserB = null;
                            string errorUserB = null;
                            int chargeTry = trip.ChargeTry;
                            string chargeStatus = null;
                            try
                            {
                                #region Getting sdsfleetID

                                var sdsFleetIDs = db.SP_Ecar_GetSDSFleetIdFromTripID(trip.Id).ToList();
                                if (sdsFleetIDs != null && sdsFleetIDs.Any())
                                {
                                    sdsFleetID = sdsFleetIDs.First();
                                }
                                #endregion

                                #region Chareging
                                if (sdsFleetID.HasValue)
                                {
                                    if (trip.FleetTripCode.HasValue && trip.FleetTripCode.Value == FleetTripCode.Ecar_Texas)
                                    {
                                        totalFare = trip.TotalFareAmount;
                                        memberID = trip.MemberID;
                                        creditCardID = trip.CreditCardID;
                                        gratuity = trip.Gratuity;
                                        if (!gratuity.HasValue || gratuity.Value <= 0)
                                        {
                                            if (trip.GratuityRate.HasValue && trip.GratuityRate.Value > 0 && totalFare.HasValue)
                                            {
                                                var gratuityRate = trip.GratuityRate.Value * (decimal)0.01;
                                                gratuity = decimal.Round(totalFare.Value * gratuityRate / (1 + gratuityRate), 2);
                                            }
                                            else
                                            {
                                                gratuity = 0;
                                            }
                                        }
                                        fare = totalFare - gratuity;

                                        if (trip.IsSplitPayment.HasValue && trip.IsSplitPayment.Value
                                            && trip.SplitPaymentStatus.HasValue && trip.SplitPaymentStatus.Value == (int)SplitPaymentStatus.Accepted)  // split payment
                                        {
                                            #region charging for split payment

                                            // Try charging user b first in half. If it is successful, then charge user a in half. Or charge user a in full
                                            if (!string.IsNullOrWhiteSpace(trip.MemberBCreditCardID))
                                            {
                                                var bChargeUserBSuccess = false;
                                                chargeUserB = true;

                                                // try charging user b in half
                                                try
                                                {
                                                    var result = accountingDomain.ChargeVtodTrip(
                                                        token, trip.Id, sdsFleetID.Value, trip.SplitPaymentMemberB.ToInt32(), trip.MemberBCreditCardID.ToInt32(),
                                                        decimal.Round(fare.Value / 2, 2), decimal.Round(gratuity.Value / 2, 2));
                                                    transactionIDUserB = result.transactionID;
                                                    amountChargedUserB = result.AmountCharged;
                                                    creditsAppliedUserB = result.CreditsApplied;
                                                    if (string.IsNullOrWhiteSpace(transactionIDUserB) && !creditsAppliedUserB.HasValue && creditsAppliedUserB.Value <= 0)
                                                    {
                                                        throw VtodException.CreateException(ExceptionType.Accounting, 8010);
                                                    }

                                                    bChargeUserBSuccess = true;
                                                }
                                                catch (Exception ex)
                                                {
                                                    string.Format(DateTime.Now.ToString() + " Error: {0}", ex).WriteToConsole();
                                                    logger.Error(DateTime.Now.ToString() + " Error: ", ex);
                                                }

                                                if (bChargeUserBSuccess)
                                                {
                                                    // charge user a in half
                                                    var result = accountingDomain.ChargeVtodTrip(
                                                        token, trip.Id, sdsFleetID.Value, memberID.ToInt32(), creditCardID.ToInt32(), decimal.Round(fare.Value / 2, 2), decimal.Round(gratuity.Value / 2, 2));
                                                    transactionID = result.transactionID;
                                                    amountCharged = result.AmountCharged;
                                                    creditsApplied = result.CreditsApplied;
                                                    if (string.IsNullOrWhiteSpace(transactionID) && !creditsApplied.HasValue && creditsApplied.Value <= 0)
                                                    {
                                                        throw VtodException.CreateException(ExceptionType.Accounting, 8010);
                                                    }
                                                }
                                                else
                                                {
                                                    // charge user a in full
                                                    var result = accountingDomain.ChargeVtodTrip(
                                                       token, trip.Id, sdsFleetID.Value, memberID.ToInt32(), creditCardID.ToInt32(), fare.Value, gratuity.Value);
                                                    transactionID = result.transactionID;
                                                    amountCharged = result.AmountCharged;
                                                    creditsApplied = result.CreditsApplied;
                                                    if (string.IsNullOrWhiteSpace(transactionID) && !creditsApplied.HasValue && creditsApplied.Value <= 0)
                                                    {
                                                        throw VtodException.CreateException(ExceptionType.Accounting, 8010);
                                                    }
                                                }

                                                chargeStatus = ChargeStatus.Charged.ToString();
                                            }
                                            else
                                            {
                                                // charge user a in full
                                                var result = accountingDomain.ChargeVtodTrip(token, trip.Id, sdsFleetID.Value, memberID.ToInt32(), creditCardID.ToInt32(), fare.Value, gratuity.Value);
                                                transactionID = result.transactionID;
                                                amountCharged = result.AmountCharged;
                                                creditsApplied = result.CreditsApplied;
                                                if (string.IsNullOrWhiteSpace(transactionID) && !creditsApplied.HasValue && creditsApplied.Value <= 0)
                                                {
                                                    throw VtodException.CreateException(ExceptionType.Accounting, 8010);
                                                }

                                                chargeStatus = ChargeStatus.Charged.ToString();// EcarTripStatusType.Charged;
                                            }
                                            #endregion
                                        }
                                        else
                                        {
                                            var result = accountingDomain.ChargeVtodTrip(token, trip.Id, sdsFleetID.Value, memberID.ToInt32(), creditCardID.ToInt32(), fare.Value, gratuity.Value);
                                            transactionID = result.transactionID;
                                            amountCharged = result.AmountCharged;
                                            creditsApplied = result.CreditsApplied;
                                            if (string.IsNullOrWhiteSpace(transactionID) && !creditsApplied.HasValue && creditsApplied.Value <= 0)
                                            {
                                                throw VtodException.CreateException(ExceptionType.Accounting, 8010);
                                            }

                                            chargeStatus = ChargeStatus.Charged.ToString();// EcarTripStatusType.Charged;
                                        }
                                    }
                                    else
                                    {
                                        totalFare = trip.TotalFareAmount;
                                        memberID = trip.MemberID;
                                        creditCardID = trip.CreditCardID;
                                        gratuity = trip.Gratuity;
                                        if (!gratuity.HasValue || gratuity.Value <= 0)
                                        {
                                            gratuity = 0;
                                        }
                                        fare = totalFare - gratuity;

                                        var result = accountingDomain.ChargeVtodTrip(token, trip.Id, sdsFleetID.Value, memberID.ToInt32(), creditCardID.ToInt32(), fare.Value, gratuity.Value);
                                        transactionID = result.transactionID;
                                        amountCharged = result.AmountCharged;
                                        creditsApplied = result.CreditsApplied;
                                        if (string.IsNullOrWhiteSpace(transactionID) && !creditsApplied.HasValue && creditsApplied.Value <= 0)
                                        {
                                            throw VtodException.CreateException(ExceptionType.Accounting, 8010);//throw new Exception(Messages.Accounting_ReturnedNullTransactionID);
                                        }
                                        chargeStatus = Common.DTO.Enum.ChargeStatus.Charged.ToString();// EcarTripStatusType.Charged;
                                    }
                                }
                                else
                                {
                                    chargeStatus = Common.DTO.Enum.ChargeStatus.UnCharged.ToString();// EcarTripStatusType.Charged;
                                    error = "No SDS_FleetMerchantID found";

                                }
                                #endregion

                                #region Notifying Driver
                                if (chargeStatus == Common.DTO.Enum.ChargeStatus.Charged.ToString()) // EcarTripStatusType.Charged)
                                {
                                    try
                                    {
                                        var notifyDriverRQ = new UtilityNotifyDriverRQ();
                                        notifyDriverRQ.TripID = trip.Id;
                                        var ecarTrip = vtodDomain.GetEcarTrip(trip.Id);
                                        if (ecarTrip != null)
                                        {
                                            var firstName = ecarTrip.FirstName;
                                            var lastName = ecarTrip.LastName;

                                            notifyDriverRQ.Message = string.Format(Messages.Notification_NotifyDriver, firstName, lastName, fare.Value + gratuity.Value);
                                            ecarDomain.NotifyDriver(token, notifyDriverRQ);
                                        }
                                        else
                                        {
                                            throw new Exception(string.Format(Messages.Notification_NotifyDriver, trip.Id));
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        logger.Error(ex);
                                    }
                                }
                                #endregion
                            }
                            catch (Exception ex)
                            {
                                chargeStatus = Common.DTO.Enum.ChargeStatus.Error.ToString();
                                error = ex.Message;
                            }
                            finally
                            {
                                chargeTry++;

                                db.SP_vtod_InsertTripAccountingTransaction(trip.Id, transactionID, amountCharged, creditsApplied, error, DateTime.Now, memberID);
                                if (chargeUserB)  // split payment, user b is charged
                                    db.SP_vtod_InsertTripAccountingTransaction(trip.Id, transactionIDUserB, amountChargedUserB, creditsAppliedUserB, errorUserB, DateTime.Now, trip.SplitPaymentMemberB);
                                db.SP_vtod_Update_TripChargeStatus(trip.Id, chargeStatus, chargeTry, error);
                            }
                        }
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                string.Format(DateTime.Now.ToString() + " Error: {0}", ex).WriteToConsole();
                logger.Error(DateTime.Now.ToString() + " Error: ", ex);
                UDI.VTOD.Common.DTO.Static.Token.TokenRS = null;
            }
        }
        public void ModifyTaxiDriverInfo()
		{
			#region Intit
			var trackTime = new TrackTime(LogOwnerType.VTOD, Method.WindowsService_PullTaxiStatus);
			var sdsDomain = new SDSDomain(trackTime);
			//var taxiDomain = new TaxiDomain();
			//var accountingDomain = new AccountingDomain(); accountingDomain.TrackTime = trackTime;
			#endregion

			try
			{
				#region Get Trips for getting Driver Info
				List<long?> tripIDs = null;
				using (var db = new DataAccess.VTOD.VTODEntities())
				{
					tripIDs = db.SP_Taxi_GetTripIDsForUpdatingDriverInfo().ToList();
				}
				if (tripIDs != null && tripIDs.Any())
				{
					foreach (var id in tripIDs)
					{
						try
						{
							using (var db = new DataAccess.VTOD.VTODEntities())
							{
								var taxi = db.vtod_trip.Include("taxi_trip").Where(s => s.Id == id).FirstOrDefault();
								if (taxi.taxi_trip.FleetId > 0 && !string.IsNullOrWhiteSpace(taxi.taxi_trip.DispatchTripId))
								{
									UDI.SDS.DTO.driver_info driverInfo = null;

									#region Work around for fleet iD
									//ToDO: this should be handled in a better way. For now I hardcode it.
									//the problem is there is no way to distinguish between taxi fleet 16 and 17. Both belong to KC and both use the same DDS api.
									//So I do the query for all KC trips including 16 and 17 twice to get the driver info.
									if (taxi.taxi_trip.FleetId == 16 || taxi.taxi_trip.FleetId == 17)
									{
										driverInfo = sdsDomain.GetTaxiDriverInfo("17", taxi.taxi_trip.DispatchTripId);

										if (driverInfo == null || !string.IsNullOrWhiteSpace(driverInfo.error))
										{
											driverInfo = sdsDomain.GetTaxiDriverInfo("16", taxi.taxi_trip.DispatchTripId);
										}
									}
									else
									{
										driverInfo = sdsDomain.GetTaxiDriverInfo(taxi.taxi_trip.FleetId.ToString(), taxi.taxi_trip.DispatchTripId);
									}

									#endregion

									if (!string.IsNullOrWhiteSpace(driverInfo.error))
									{
										throw new Exception(string.Format("Error:{0}, tripID:{1}, DispatchTripID:{2} fleetID:{3}, FinalStatus:{4}, PU time: {5}", driverInfo.error, taxi.Id, taxi.taxi_trip.DispatchTripId, taxi.taxi_trip.FleetId, taxi.FinalStatus, taxi.PickupDateTime));
									}
									else
									{
										taxi.DriverName = driverInfo.name;
										taxi.DriverID = driverInfo.id;
										db.SaveChanges();
									}
								}
							}
						}
						catch (Exception ex)
						{
							string.Format(DateTime.Now.ToString() + " Error: {0}", ex).WriteToConsole();
							logger.InfoFormat(DateTime.Now.ToString() + " Cannot get driver info for : Trip {0}", id);
							logger.Error(DateTime.Now.ToString() + " Error: ", ex);
						}

					}
				}
				#endregion
			}
			catch (Exception ex)
			{
				string.Format(DateTime.Now.ToString() + " Error: {0}", ex).WriteToConsole();
				logger.Error(DateTime.Now.ToString() + " Error: ", ex);
			}

		}
		#endregion

		#region Error Methods
		public string GetContextErrorCode(string userName, string contextCode, string languageCode)
		{
			var vtodDomain = new VTODDomain();
			var result = vtodDomain.GetContextErrorCode(userName, contextCode, languageCode);
			return result;
		}
		#endregion

		#region Private
		private OTA_Exception getOtaException(Exception ex)
		{
			UDI.VTOD.Common.DTO.OTA.OTA_Exception exception;
			VtodException vtodEx;
			if (ex.GetType() == typeof(VtodException))
			{
				vtodEx = (VtodException)ex;

				if (vtodEx.ExceptionType == ExceptionType.Auth)
				{
					exception = new OTA_Exception_Unauthorized();
				}
				else
				{
					exception = new OTA_Exception();
				}
			}
			else
			{
				vtodEx = VtodException.CreateException(ExceptionType.General, 1);
				exception = new OTA_Exception();
			}
			exception.Errors = new List<Common.DTO.OTA.Error>();
			exception.Errors.Add(new UDI.VTOD.Common.DTO.OTA.Error { Code = vtodEx.ExceptionMessage.Code, Type = vtodEx.ExceptionType.ToString(), Value = vtodEx.ExceptionMessage.Message });

			return exception;
		}

        private void GetHeaderAndMakeToken(TokenRS token)
        {
            var requestMessages = OperationContext.Current.IncomingMessageProperties[HttpRequestMessageProperty.Name] as HttpRequestMessageProperty;

            try
            {
                if (requestMessages.Headers["Username"] != null)
                {
                    token.Username = requestMessages.Headers["Username"];
                    logger.InfoFormat("UserName: {0}", requestMessages.Headers["Username"]);
                }
                   
                if (requestMessages.Headers["SecurityKey"] != null)
                {
                    token.SecurityKey = requestMessages.Headers["SecurityKey"];
                    logger.InfoFormat("UserName: {0}", requestMessages.Headers["SecurityKey"]);
                }
                    
            }
            catch { }
        }

        #endregion

        #region Texas Taxi
        public void TaxiPushCashiering()
        {
            #region Intit
            var vtodDomain = new UDI.VTOD.Domain.VTOD.VTODDomain();
            #endregion

            try
            {
                vtodDomain.TaxiUploadToSFTP();
            }
            catch (Exception ex)
            {
                string.Format(DateTime.Now.ToString() + " Error: {0}", ex).WriteToConsole();
                logger.Error(DateTime.Now.ToString() + " Error: ", ex);
            }
        }
        public void ECarPushCashiering()
        {
            #region Intit
            var vtodDomain = new UDI.VTOD.Domain.VTOD.VTODDomain();
            #endregion

            try
            {
                vtodDomain.ECarUploadToSFTP();
            }
            catch (Exception ex)
            {
                string.Format(DateTime.Now.ToString() + " Error: {0}", ex).WriteToConsole();
                logger.Error(DateTime.Now.ToString() + " Error: ", ex);
            }
        }
        #endregion

        #region Properties
        public TrackTime TrackTime { get; set; }
		#endregion
	}
}
