﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.DataAccess
{
	public interface IEntity
	{
		long Id { get; }
		DateTime AppendTime { get; set; }
	}
}
