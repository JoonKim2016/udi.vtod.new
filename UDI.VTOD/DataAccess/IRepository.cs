﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.DataAccess
{
	public interface IRepository<T>
	{
		void Add(T entity);
		void Delete(T entity);
		IQueryable<T> Find(Expression<Func<T, bool>> predicate);
		IQueryable<T> GetAll();
		T GetByID(long id);
		void Save();
	}
}
