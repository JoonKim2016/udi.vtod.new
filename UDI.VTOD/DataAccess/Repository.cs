﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.DataAccess
{
	public class Repository<T> : IRepository<T> where T : class, IEntity
	{
		#region Fields
		protected IDbSet<T> _dbSet;
		protected DbContext _dataContext;
		#endregion

		public Repository(DbContext dataContext)
		{
			_dataContext = dataContext;
			_dbSet = _dataContext.Set<T>();
		}

		#region IRepository<T> Members

		public void Add(T entity)
		{
			if (entity.AppendTime == null || entity.AppendTime == DateTime.MinValue)
			{
				entity.AppendTime = System.DateTime.UtcNow;
			}
			_dbSet.Add(entity);
		}

		public void Update(T entity)
		{
			//DataTable.(entity);
		}

		public void Delete(T entity)
		{
			_dbSet.Remove(entity);
		}

		public IQueryable<T> Find(Expression<Func<T, bool>> predicate)
		{
			return _dbSet.Where(predicate);
		}

		public IQueryable<T> GetAll()
		{
			return _dbSet;
		}

		public T GetByID(long id)
		{
			// Sidenote: the == operator throws NotSupported Exception!
			// 'The Mapping of Interface Member is not supported'
			// Use .Equals() instead
			return _dbSet.Single(e => e.Id.Equals(id));
		}

		public void Save()
		{
			_dataContext.SaveChanges();
		}

		#endregion
	}
}
