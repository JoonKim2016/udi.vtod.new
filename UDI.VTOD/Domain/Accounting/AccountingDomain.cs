﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO.OTA;
using UDI.SDS;
using UDI.VTOD.Common.Helper;
using UDI.VTOD.Common.DTO;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.DataAccess.VTOD;
using UDI.SDS.Helper;
using UDI.Utility.Helper;
using UDI.Utility.Serialization;
using UDI.SDS.MembershipService;
using UDI.VTOD.Common.Track;
using UDI.SDS.DTO;
using UDI.VTOD.Domain.ECar.Aleph;
using UDI.VTOD.Helper;

namespace UDI.VTOD.Domain.Accounting
{
    /// <summary>
    /// These is used to do all sort of calculations ; like Credit Card Calculation,Direct Bill Calculations,Airline Reward Calculation,Ztrip Credits Calculation etc.
    /// </summary>
	public class AccountingDomain : BaseSetting
	{
		private VTOD.VTODDomain vtod_domain = new VTOD.VTODDomain();

		#region Methods

		internal TokenRS GetToken(TokenRQ authRequest, long? vtodTXId)
		{
			try
			{
				#region Init
				TokenRS result = null;
				var securityController = new SecurityController(null, TrackTime);

				UserInfo userInfo;
				#endregion

				result = securityController.GetToken(authRequest, out userInfo);


				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("AccountingDomain.GetToken:: {0}", ex.ToString() + ex.StackTrace);

				#region Throw MembershipException
				if (ex.Message.Contains("::"))//Remove method name
					throw new MembershipException(ex.Message.Substring(ex.Message.IndexOf("::") + 2));
				else
					throw new MembershipException(ex.Message);
				#endregion
			}
		}

		#region Credit Card
		internal AccountingAddMemberPaymentCardRS AccountingAddPaymentCard(TokenRS tokenVTOD, AccountingAddMemberPaymentCardRQ request)
		{
			try
			{
                #region Init
                #region Init Respose
                AccountingAddMemberPaymentCardRS response = new AccountingAddMemberPaymentCardRS();
                #endregion
                if (request.PaymentCard != null && request.PaymentCard.TPA_Extensions.IsTandemRQ())
                {
                    var corporateInputs = request.PaymentCard.TPA_Extensions;
                    AlephPaymentCard alephPaymentCard;
                    if (corporateInputs.SpecialInputs != null)
                    {
                        alephPaymentCard = corporateInputs.SpecialInputs.ToAlephPaymentCardInfo();

                        if (string.IsNullOrEmpty(alephPaymentCard.CorporateEmail)
                            || string.IsNullOrEmpty(alephPaymentCard.CorporateProfileID)
                            || string.IsNullOrEmpty(alephPaymentCard.CorporatePaymentNonce))
                        {
                            logger.Warn(Messages.ECar_Common_InvalidPaymentCard);
                            throw new ValidationException(Messages.ECar_Common_InvalidPaymentCard);
                        }

                        alephPaymentCard.IsPrimary = (bool)corporateInputs.IsDefault;

                        #region Get Corporate Key
                        string corporateKey = string.Empty;
                        try
                        {
                            corporateKey = AlephSDSCall.GetCorporateAccountKey(tokenVTOD, TrackTime, alephPaymentCard.CorporateEmail);
                        }
                        catch (Exception ex1)
                        {
                            throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(ex1.Message));
                        }

                        #endregion Get Corporate Key

                        #region Call Aleph API

                        try
                        {
                            var alephAPICall = new AlephAPICall();
                            var addPaymentCardResponse = alephAPICall.AddCorporatePaymentCard(alephPaymentCard, corporateKey, request.Nickname);
                            if (addPaymentCardResponse == null)
                                response.Success = new Success();
                        }
                        catch (Exception ex)
                        {
                            string msg = string.Format("Unable to add a credit card. errormessage={0}", ex.InnerException.Message);
                            logger.Warn(msg);

                            throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(ex.Message));
                        }

                        #endregion Call Aleph API




                    }
                }
                else
                {
                    #region CardType
                    var cardtype = CreditCardTypesEnumeration.Unknown;

                    if (request.PaymentCard.CardType != null && !string.IsNullOrEmpty(request.PaymentCard.CardType.Value))
                    {
                        cardtype = request.PaymentCard.CardType.Value.ToMembershipServiceCreditCardType();
                    }
                    else
                    {
                        cardtype = request.PaymentCard.CardNumber.PlainText.ToPaymentCardType().ToMembershipServiceCreditCardType();
                    }
                    #endregion

                    if (cardtype == CreditCardTypesEnumeration.Unknown)
                        throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_InvalidCardType);

                    var controller = new UDI.SDS.AccoutingController(tokenVTOD, TrackTime);
                    UDI.SDS.MembershipService.MembershipCreditCardAccount accountDetails = new MembershipCreditCardAccount();
                    if (request.PaymentCard.Address != null)
                    {
                        accountDetails.BillingAddress = request.PaymentCard.Address.AddressLine;
                        accountDetails.BillingCity = request.PaymentCard.Address.CityName;
                        accountDetails.BillingState = request.PaymentCard.Address.StateProv.StateCode;
                        accountDetails.BillingZipCode = request.PaymentCard.Address.PostalCode;
                    }
                    else
                    {
                        //var userInfo = vtod_domain.GetUserInfo(tokenVTOD.Username);
                        //if (userInfo != null)
                        //{
                        //    accountDetails.BillingAddress = string.Format("{0} {1}", userInfo.vtod_users_info.StreetNumber, userInfo.vtod_users_info.StreetName);
                        //    accountDetails.BillingCity = userInfo.vtod_users_info.City;
                        //    accountDetails.BillingState = userInfo.vtod_users_info.State;
                        //    accountDetails.BillingZipCode = userInfo.vtod_users_info.PostalCode;
                        //}
                    }
                    accountDetails.BillingName = request.PaymentCard.CardHolderName;
                    accountDetails.CardType = cardtype;
                    accountDetails.CreditCardNumber = request.PaymentCard.CardNumber.PlainText;
                    accountDetails.ExpirationDate = request.PaymentCard.ExpireDate.ToDateTime().Value;
                    accountDetails.Nickname = request.Nickname;
                    if (request.PaymentCard.TPA_Extensions != null && request.PaymentCard.TPA_Extensions.IsDefault.HasValue)
                    {
                        accountDetails.IsDefault = request.PaymentCard.TPA_Extensions.IsDefault.Value;
                    }
                    #endregion


               #region Create credit card account
                    string cvv = string.Empty;
                    if (request.PaymentCard.SeriesCode != null && !string.IsNullOrWhiteSpace(request.PaymentCard.SeriesCode.PlainText))
                    {
                        cvv = request.PaymentCard.SeriesCode.PlainText;
                    }
                    
                    response.AccountId = controller.CreateCreditCardAccount(request.MemberId.ToInt32(), accountDetails, cvv);

                    if (response.AccountId > 0)
                        response.Success = new Success();
                    #endregion
            }
                
                #region Common
                response.EchoToken = request.EchoToken;
                response.PrimaryLangID = request.PrimaryLangID;
                response.Target = request.Target;
                response.Version = request.Version;
                #endregion
                return response;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("AccountingDomain.CreateCreditCardAccount:: {0}", ex.ToString() + ex.StackTrace);
                #region Throw AccountingException || Validation Exception
                throw ex;
                #endregion                
			}
		}

		internal AccountingGetMemberPaymentCardsRS AccountingGetMemberPaymentCards(TokenRS tokenVTOD, AccountingGetMemberPaymentCardsRQ request)
		{
			try
			{
                #region Init Respose
                AccountingGetMemberPaymentCardsRS response = new AccountingGetMemberPaymentCardsRS();
                response.PaymentCards = new List<Common.DTO.OTA.PaymentCard>();
                #endregion

                AlephPaymentCard alephPaymentCard;
                if (request.TPA_Extensions.IsTandemRQ())
                {
                    var corporateInputs = request.TPA_Extensions.SpecialInputs;
                    alephPaymentCard = corporateInputs.ToAlephPaymentCardInfo();
                    if (corporateInputs != null)
                    {
                        if (string.IsNullOrEmpty(alephPaymentCard.CorporateName)
                            || string.IsNullOrEmpty(alephPaymentCard.Account)
                            || string.IsNullOrEmpty(alephPaymentCard.CorporateProfileID)
                            || string.IsNullOrEmpty(alephPaymentCard.CorporateEmail))
                        {
                            logger.Warn(Messages.ECar_Common_InvalidPaymentCard);
                            throw new ValidationException(Messages.Aleph_Invalid_CorporateInputs);
                        }
                    }

                    #region Get Corporate Key
                    string corporateKey = string.Empty;
                    try
                    {
                        corporateKey = AlephSDSCall.GetCorporateAccountKey(tokenVTOD, TrackTime, alephPaymentCard.CorporateEmail);
                    }
                    catch (Exception ex1)
                    {
                        throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(ex1.Message));
                    }

                    #endregion Get Corporate Key

                    #region Call Aleph API

                    try
                    {
                        var alephAPICall = new AlephAPICall();                        

                        var alephGetPaymentMethodRS = alephAPICall.GetCorporatePaymentMethods(alephPaymentCard.CorporateName, alephPaymentCard.Account, alephPaymentCard.CorporateEmail, corporateKey);
                        
                        response.CorporateCompanyRequirement = alephGetPaymentMethodRS.ToCorporateCompanyRequirements().First();

                        if (response.CorporateCompanyRequirement.CorporateCompanyAccounts.First().IsCreditCardEligible != null && response.CorporateCompanyRequirement.CorporateCompanyAccounts.First().IsCreditCardEligible == true)
                        {
                            var AlephCoporateCreditCards = alephAPICall.GetCorporatePaymentCards(alephPaymentCard.CorporateProfileID, alephPaymentCard.CorporateEmail, corporateKey);
                            if (AlephCoporateCreditCards != null && AlephCoporateCreditCards.Any())
                            {
                                foreach (var creditCardInfo in AlephCoporateCreditCards)
                                {
                                    response.PaymentCards.Add(creditCardInfo.ToPaymentCard());
                                }
                            }
                        }
                                                
                        response.Success = new Success();
                    }
                    catch (Exception ex)
                    {
                        string msg = string.Format("Unable to add a credit card. errormessage={0}", ex.InnerException.Message);
                        logger.Warn(msg);

                        throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(ex.Message));
                    }

                    #endregion Call Aleph API

                }
                else
                {
                    #region Init
                    var controller = new UDI.SDS.AccoutingController(tokenVTOD, TrackTime);
                    #endregion

                    #region Get Credit Card Accounts
                    var results = controller.GetCreditCardAccounts(request.MemberId);

                    if (results != null && results.Count > 0)
                    {
                        foreach (var result in results)
                        {
                            var account = new Common.DTO.OTA.PaymentCard();
                            account.Address = new Address();
                            account.Address.AddressLine = result.BillingAddress;
                            account.Address.CityName = result.BillingCity;
                            account.CardHolderName = result.BillingName;
                            account.Address.StateProv = new StateProv();
                            account.Address.StateProv.StateCode = result.BillingState;
                            account.Address.PostalCode = result.BillingZipCode;
                            account.CardType = new CardType();
                            account.CardType.Value = result.CardType.ToString();
                            account.CardType.Description = result.CardTypeDescription;
                            //account.SeriesCode = new CardNumber_SeriesCode();
                            account.TPA_Extensions = new TPA_Extensions();
                            account.TPA_Extensions.IsVerified = result.CardVerified;
                            account.TPA_Extensions.IsDefault = result.IsDefault;
                            account.CardNumber = new CardNumber_SeriesCode();
                            //account.CardNumber.PlainText = result.CreditCardNumber;
                            account.CardNumber.EncryptedValue = result.CreditCardNumber;
                            account.CardNumber.LastFourDigit = result.Last4;
                            account.ExpireDate = result.ExpirationDate.ToString();
                            account.CardNumber.ID = result.AccountID.ToString();
                            account.Nickname = result.Nickname;
                            response.PaymentCards.Add(account);
                        }

                        response.Success = new Success();
                    }
                }
                
				#region Common
				response.EchoToken = request.EchoToken;
				response.PrimaryLangID = request.PrimaryLangID;
				response.Target = request.Target;
				response.Version = request.Version;
				#endregion

				#endregion

				return response;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("AccountingDomain.GetCreditCardAccounts:: {0}", ex.ToString() + ex.StackTrace);
                
				#region Throw AccountingException || Validation Exception

				if (ex.Message.Contains("::"))//Remove method name
					throw new AccountingException(ex.Message.Substring(ex.Message.IndexOf("::") + 2));
				else
					throw new AccountingException(ex.Message);

				#endregion
			}
		}

		internal AccountingSetMemberDefaultPaymentCardRS AccountingSetMemberDefaultPaymentCard(TokenRS tokenVTOD, AccountingSetMemberDefaultPaymentCardRQ request)
		{
			try
			{
				#region Init
				var controller = new UDI.SDS.AccoutingController(tokenVTOD, TrackTime);
				#endregion

				#region Init Respose
				AccountingSetMemberDefaultPaymentCardRS response = new AccountingSetMemberDefaultPaymentCardRS();
				#endregion

				#region Set Default Credit Card Account
				var result = controller.SetDefaultCreditCardAccount(request.MemberId, request.AccountId, request.IsDefault);

				if (!result)
					throw VtodException.CreateException(ExceptionType.Accounting, 8003);// new AccountingException(Messages.Accounting_SetDefaultCreditCardAccountFailed);
				else
					response.Success = new Success();

				#region Common
				response.EchoToken = request.EchoToken;
				response.PrimaryLangID = request.PrimaryLangID;
				response.Target = request.Target;
				response.Version = request.Version;
				#endregion

				#endregion
                
				return response;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("AccountingDomain.SetDefaultCreditCardAccount:: {0}", ex.ToString() + ex.StackTrace);

				#region Throw AccountingException || Validation Exception

				if (ex.Message.Contains("::"))//Remove method name
					throw new AccountingException(ex.Message.Substring(ex.Message.IndexOf("::") + 2));
				else
					throw new AccountingException(ex.Message);

				#endregion
			}
		}

		internal AccountingDeleteMemberPaymentCardRS AccountingDeleteMemberPaymentCard(TokenRS tokenVTOD, AccountingDeleteMemberPaymentCardRQ request)
		{
			try
			{
                var response = new AccountingDeleteMemberPaymentCardRS();

                if (request.TPA_Extensions.IsTandemRQ())
                {
                    var corporateInputs = request.TPA_Extensions.SpecialInputs;
                    AlephPaymentCard alephPaymentCardInfo;
                    if (corporateInputs != null)
                    {
                        alephPaymentCardInfo = corporateInputs.ToAlephPaymentCardInfo();
                        if (corporateInputs != null)
                        {
                            if (string.IsNullOrEmpty(alephPaymentCardInfo.CorporatePaymentMethodToken)                                
                                || string.IsNullOrEmpty(alephPaymentCardInfo.CorporateProfileID)
                                || string.IsNullOrEmpty(alephPaymentCardInfo.CorporateEmail))
                            {
                                logger.Warn(Messages.ECar_Common_InvalidPaymentCard);
                                throw new ValidationException(Messages.ECar_Common_InvalidPaymentCard);
                            }
                        }

                        #region Get Corporate Key
                        string corporateKey = string.Empty;
                        try
                        {
                            corporateKey = AlephSDSCall.GetCorporateAccountKey(tokenVTOD, TrackTime, alephPaymentCardInfo.CorporateEmail);
                        }
                        catch (Exception ex1)
                        {
                            throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(ex1.Message));
                        }

                        #endregion Get Corporate Key

                        #region Call Aleph API

                        try
                        {
                            var alephAPICall = new AlephAPICall();
                            var deletepaymentCardResponse = alephAPICall.DeleteCorporatePaymentCard(alephPaymentCardInfo, corporateKey);
                            if (deletepaymentCardResponse == null)
                                response.Success = new Success();
                        }
                        catch (Exception ex)
                        {
                            string msg = string.Format("Unable to delete a credit card. errormessage={0}", ex.InnerException.Message);
                            logger.Warn(msg);

                            throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(ex.Message));
                        }

                        #endregion Call Aleph API
                    }
                }
                else
                {
                    #region Init
                    var controller = new UDI.SDS.AccoutingController(tokenVTOD, TrackTime);
                    #endregion

                    #region Init Respose
                   
                    #endregion

                    #region Delete Credit Card Account
                    var result = controller.DeleteCreditCardAccount(request.AccountId);

                    if (!result)
                        throw VtodException.CreateException(ExceptionType.Accounting, 8003);
                    else
                        response.Success = new Success();

                    #endregion
                }

                #region Common
                response.EchoToken = request.EchoToken;
                response.PrimaryLangID = request.PrimaryLangID;
                response.Target = request.Target;
                response.Version = request.Version;
                #endregion

                return response;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("AccountingDomain.DeleteCreditCardAccount:: {0}", ex.ToString() + ex.StackTrace);			
				#region Throw AccountingException || Validation Exception

				if (ex.Message.Contains("::"))//Remove method name
					throw new AccountingException(ex.Message.Substring(ex.Message.IndexOf("::") + 2));
				else
					throw new AccountingException(ex.Message);

				#endregion
			}
		}

        internal AccountingEditMemberPaymentCardRS AccountingEditPaymentCard(TokenRS tokenVTOD, AccountingEditMemberPaymentCardRQ request)
        {
            try
            {
                #region Init
                var controller = new UDI.SDS.MembershipController(tokenVTOD, TrackTime);
                UDI.SDS.MembershipService.MembershipCreditCardAccount accountDetails = new MembershipCreditCardAccount();
                PatchCreditCardRequest patchRequest = new PatchCreditCardRequest();
                patchRequest.AccountId = request.AccountId;
                patchRequest.CardHolderName = request.CardHolderName;
                patchRequest.Cvv = request.CVV;
                patchRequest.ExpirationDate = request.ExpirationDate.ToDateTime().Value;
                patchRequest.MemberId = request.MemberId;
                patchRequest.PostalCode = request.PostalCode;
                #endregion

                #region Init Respose
                AccountingEditMemberPaymentCardRS response = new AccountingEditMemberPaymentCardRS();
                #endregion

                #region Create credit card account
                MembershipCreditCardAccount editCreditCardResponse = controller.PatchCredit(patchRequest);
                response.AccountId = editCreditCardResponse.AccountID;
                if (editCreditCardResponse.AccountID>0)
                    response.Success = new Success();

                #region Common
                response.EchoToken = request.EchoToken;
                response.PrimaryLangID = request.PrimaryLangID;
                response.Target = request.Target;
                response.Version = request.Version;
                #endregion
                #endregion
                return response;
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("AccountingDomain.CreateCreditCardAccount:: {0}", ex.ToString() + ex.StackTrace);
                #region Throw AccountingException || Validation Exception
                throw ex;
                #endregion
            }
        }
		#endregion

		#region Direct Bill
		internal AccountingAddMemberDirectBillRS AccountingAddMemberDirectBill(TokenRS tokenVTOD, AccountingAddMemberDirectBillRQ request)
		{
			try
			{
				#region Init
				var controller = new UDI.SDS.AccoutingController(tokenVTOD, TrackTime);
				#endregion

				#region Init Respose
				var response = new AccountingAddMemberDirectBillRS();
				#endregion

				#region Add Direct Bill Account To Customer Profile
				var directBillAccountNumber = request.DirectBill.BillingNumber;
				var memberID = request.MemberId;
				var isDefault = false;
				if (request.DirectBill.TPA_Extensions != null && request.DirectBill.TPA_Extensions.IsDefault.HasValue)
				{
					isDefault = request.DirectBill.TPA_Extensions.IsDefault.Value;
				}


				var result = controller.AddDirectBillAccountToCustomerProfile(memberID.ToInt32(), directBillAccountNumber, isDefault);

				if (!result)
					throw VtodException.CreateException(ExceptionType.Accounting, 8006);//throw new AccountingException(Messages.Accounting_AddDirectBillAccountToCustomerProfileFailed);
				else
					response.Success = new Success();

				#region Common
				response.EchoToken = request.EchoToken;
				response.PrimaryLangID = request.PrimaryLangID;
				response.Target = request.Target;
				response.Version = request.Version;
				#endregion

				#endregion

				//#region Save to log Database

				//using (var db = new VTODEntities())
				//{
				//	db.SP_Accounting_InsertLog(vtodTXId, Method.AddDirectBillAccountToCustomerProfile.ToString(),
				//								request.XmlSerialize().ToString(), response.XmlSerialize().ToString());
				//}

				//#endregion

				return response;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("AccountingDomain.AddDirectBillAccountToCustomerProfile:: {0}", ex.ToString() + ex.StackTrace);

				//#region Save to Database

				//using (var db = new VTODEntities())
				//{
				//	var vtod_logId = vtod_domain.LogInDB(LogOwnerType.Accounting, ex.ToString() + ex.StackTrace,
				//										UDI.VTOD.Common.DTO.Enum.LogType.Error,
				//										Method.AddDirectBillAccountToCustomerProfile.ToString(), DateTime.Now);
				//}

				//#endregion

				#region Throw AccountingException || Validation Exception

				throw;

				//if (ex.Message.Contains("::"))//Remove method name
				//	throw new AccountingException(ex.Message.Substring(ex.Message.IndexOf("::") + 2));
				//else
				//	throw new AccountingException(ex.Message);

				#endregion
			}
		}

		internal AccountingGetMemberDirectBillsRS AccountingGetMemberDirectBills(TokenRS tokenVTOD, AccountingGetMemberDirectBillsRQ request)
		{
			try
			{
				#region Init
				var controller = new UDI.SDS.AccoutingController(tokenVTOD, TrackTime);
				#endregion

				#region Init Respose
				var response = new AccountingGetMemberDirectBillsRS();
				response.DirectBills = new List<Common.DTO.OTA.DirectBill>();
				#endregion

				#region Get Direct Bill Accounts
				var results = controller.GetDirectBillAccounts(request.MemberId);

				if (results != null && results.Count > 0)
				{
					foreach (var result in results)
					{
						var account = new Common.DTO.OTA.DirectBill();
						account.ID = result.AccountID.ToString();
						//account.BillingNumber = result.AccountName;
						account.BillingNumber = result.ExecuCarAccountNo;
						//account.SuperShuttleAccountNo = result.SuperShuttleAccountNo;
						account.TPA_Extensions = new TPA_Extensions();
						account.TPA_Extensions.IsDefault = result.IsDefault;
						account.CompanyName = new CompanyName();
						account.CompanyName.CompanyShortName = result.AccountName;

						response.DirectBills.Add(account);
					}

					response.Success = new Success();
				}

				#region Common
				response.EchoToken = request.EchoToken;
				response.PrimaryLangID = request.PrimaryLangID;
				response.Target = request.Target;
				response.Version = request.Version;
				#endregion

				#endregion

				//#region Save to log Database

				//using (var db = new VTODEntities())
				//{
				//	db.SP_Accounting_InsertLog(vtodTXId, Method.GetDirectBillAccounts.ToString(),
				//								request.XmlSerialize().ToString(), string.Format("account count = {0}.", response.DirectBills.Count));
				//}

				//#endregion

				return response;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("AccountingDomain.GetDirectBillAccounts:: {0}", ex.ToString() + ex.StackTrace);

				//#region Save to Database

				//using (var db = new VTODEntities())
				//{
				//	var vtod_logId = vtod_domain.LogInDB(LogOwnerType.Accounting, ex.ToString() + ex.StackTrace,
				//									UDI.VTOD.Common.DTO.Enum.LogType.Error, Method.GetDirectBillAccounts.ToString(), DateTime.Now);
				//}

				//#endregion

				#region Throw AccountingException || Validation Exception

				if (ex.Message.Contains("::"))//Remove method name
					throw new AccountingException(ex.Message.Substring(ex.Message.IndexOf("::") + 2));
				else
					throw new AccountingException(ex.Message);

				#endregion
			}
		}

		internal AccountingSetMemberDefaultDirectBillRS AccountingSetMemberDefaultDirectBill(TokenRS tokenVTOD, AccountingSetMemberDefaultDirectBillRQ request)
		{
			try
			{
				#region Init
				var controller = new UDI.SDS.AccoutingController(tokenVTOD, TrackTime);
				#endregion

				#region Init Respose
				var response = new AccountingSetMemberDefaultDirectBillRS();
				#endregion

				#region Set Default Direct Bill Account
				var result = controller.SetDefaultDirectBillAccount(request.MemberId, request.AccountId, request.IsDefault);

				if (!result)
					throw VtodException.CreateException(ExceptionType.Accounting, 8007);//throw new AccountingException(Messages.Accounting_SetDefaultDirectBillAccountFailed);
				else
					response.Success = new Success();

				#region Common
				response.EchoToken = request.EchoToken;
				response.PrimaryLangID = request.PrimaryLangID;
				response.Target = request.Target;
				response.Version = request.Version;
				#endregion
				#endregion

				//#region Save to log Database

				//using (var db = new VTODEntities())
				//{
				//	db.SP_Accounting_InsertLog(vtodTXId, Method.SetDefaultDirectBillAccount.ToString(),
				//								request.XmlSerialize().ToString(),
				//								response.XmlSerialize().ToString());
				//}

				//#endregion

				return response;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("AccountingDomain.SetDefaultDirectBillAccount:: {0}", ex.ToString() + ex.StackTrace);

				//#region Save to Database

				//using (var db = new VTODEntities())
				//{
				//	var vtod_logId = vtod_domain.LogInDB(LogOwnerType.Accounting, ex.ToString() + ex.StackTrace,
				//										UDI.VTOD.Common.DTO.Enum.LogType.Error,
				//										Method.SetDefaultDirectBillAccount.ToString(), DateTime.Now);
				//}

				//#endregion

				#region Throw AccountingException || Validation Exception

				if (ex.Message.Contains("::"))//Remove method name
					throw new AccountingException(ex.Message.Substring(ex.Message.IndexOf("::") + 2));
				else
					throw new AccountingException(ex.Message);

				#endregion
			}
		}

		internal AccountingDeleteMemberDirectBillRS AccountingDeleteMemberDirectBill(TokenRS tokenVTOD, AccountingDeleteMemberDirectBillRQ request)
		{
			try
			{
				#region Init
				var controller = new UDI.SDS.AccoutingController(tokenVTOD, TrackTime);
				#endregion

				#region Init Respose
				var response = new AccountingDeleteMemberDirectBillRS();
				#endregion

				#region Delete Credit Card Account
				var result = controller.DeleteDirectBillAccount(request.MemberId.ToInt32(), request.AccountId);

				if (!result)
					throw VtodException.CreateException(ExceptionType.Accounting, 8008);//throw new AccountingException(Messages.Accounting_DeleteDirectBillAccountFailed);
				else
					response.Success = new Success();

				#region Common
				response.EchoToken = request.EchoToken;
				response.PrimaryLangID = request.PrimaryLangID;
				response.Target = request.Target;
				response.Version = request.Version;
				#endregion
				#endregion

				//#region Save to log Database

				//using (var db = new VTODEntities())
				//{
				//	db.SP_Accounting_InsertLog(vtodTXId, Method.DeleteDirectBillAccount.ToString(),
				//								request.XmlSerialize().ToString(),
				//								response.XmlSerialize().ToString());
				//}

				//#endregion

				return response;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("AccountingDomain.DeleteDirectBillAccount:: {0}", ex.ToString() + ex.StackTrace);

				//#region Save to Database

				//using (var db = new VTODEntities())
				//{
				//	var vtod_logId = vtod_domain.LogInDB(LogOwnerType.Accounting, ex.ToString() + ex.StackTrace,
				//										UDI.VTOD.Common.DTO.Enum.LogType.Error,
				//										Method.DeleteDirectBillAccount.ToString(), DateTime.Now);
				//}

				//#endregion

				#region Throw AccountingException || Validation Exception

				if (ex.Message.Contains("::"))//Remove method name
					throw new AccountingException(ex.Message.Substring(ex.Message.IndexOf("::") + 2));
				else
					throw new AccountingException(ex.Message);

				#endregion
			}
		}

		#endregion

		#region Airline Rewards

		internal RewardGetAirlineProgramsRS GetAirlineRewardsPrograms(TokenRS tokenVTOD, RewardGetAirlineProgramsRQ request)
		{
			try
			{
				#region Init
				var controller = new UDI.SDS.AccoutingController(tokenVTOD, TrackTime);
				#endregion

				#region Init Respose
				var response = new RewardGetAirlineProgramsRS();
				response.AirlineRewards = new List<Common.DTO.OTA.AirlineReward>();
				#endregion

				#region Get Airline Rewards Programs
				var results = controller.GetAirlineRewardsPrograms();

				if (results != null && results.Count > 0)
				{
					foreach (var result in results)
					{
						var account = new Common.DTO.OTA.AirlineReward();
						account.RewardID = result.ProgramID;
						account.ProgramName = result.ProgramName;

						response.AirlineRewards.Add(account);
					}

					response.Success = new Success();

				}

				#endregion

				//#region Save to log Database

				//using (var db = new VTODEntities())
				//{
				//	db.SP_Accounting_InsertLog(vtodTXId, Method.GetAirlineRewardsPrograms.ToString(),
				//								request.XmlSerialize().ToString(), response.XmlSerialize().ToString());
				//}

				//#endregion

				return response;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("AccountingDomain.GetAirlineRewardsPrograms:: {0}", ex.ToString() + ex.StackTrace);

				//#region Save to Database

				//using (var db = new VTODEntities())
				//{
				//	var vtod_logId = vtod_domain.LogInDB(LogOwnerType.Accounting, ex.ToString() + ex.StackTrace,
				//									UDI.VTOD.Common.DTO.Enum.LogType.Error, Method.GetAirlineRewardsPrograms.ToString(), DateTime.Now);
				//}

				//#endregion

				#region Throw AccountingException || Validation Exception

				if (ex.Message.Contains("::"))//Remove method name
					throw new AccountingException(ex.Message.Substring(ex.Message.IndexOf("::") + 2));
				else
					throw new AccountingException(ex.Message);

				#endregion
			}
		}

		internal RewardCreateMemberAccountRS CreateMembershipMileageAccount(TokenRS tokenVTOD, RewardCreateMemberAccountRQ request)
		{
			try
			{
				#region Init
				var controller = new UDI.SDS.AccoutingController(tokenVTOD, TrackTime);
				UDI.SDS.MembershipService.AirlineMileageAccounts accountDetails = new AirlineMileageAccounts();
				accountDetails.AccountNumber = request.AccountNumber;
				accountDetails.RewardsID = request.RewardID;
				#endregion

				#region Init Respose
				RewardCreateMemberAccountRS response = new RewardCreateMemberAccountRS();
				#endregion

				#region Create Membership Mileage Account
				response.Result = controller.CreateMembershipMileageAccount(request.MemberID, accountDetails, request.IsDefault);
				response.Success = new Success();
				#endregion

				//#region Save to log Database

				//using (var db = new VTODEntities())
				//{
				//	db.SP_Accounting_InsertLog(vtodTXId, Method.CreateMembershipMileageAccount.ToString(), request.XmlSerialize().ToString(), response.XmlSerialize().ToString());
				//}

				//#endregion

				return response;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("AccountingDomain.CreateMembershipMileageAccount:: {0}", ex.ToString() + ex.StackTrace);

				//#region Save to Database

				//using (var db = new VTODEntities())
				//{
				//	var vtod_logId = vtod_domain.LogInDB(LogOwnerType.Accounting, ex.ToString() + ex.StackTrace, UDI.VTOD.Common.DTO.Enum.LogType.Error, Method.CreateMembershipMileageAccount.ToString(), DateTime.Now);
				//}

				//#endregion

				#region Throw AccountingException || Validation Exception

				if (ex.Message.Contains("::"))//Remove method name
					throw new AccountingException(ex.Message.Substring(ex.Message.IndexOf("::") + 2));
				else
					throw new AccountingException(ex.Message);

				#endregion
			}
		}

		internal RewardDeleteMemberAccountRS RewardDeleteMemberAccount(TokenRS tokenVTOD, RewardDeleteMemberAccountRQ request)
		{
			try
			{
				#region Init
				var controller = new UDI.SDS.AccoutingController(tokenVTOD, TrackTime);
				#endregion

				#region Init Respose
				var response = new RewardDeleteMemberAccountRS();
				#endregion

				#region Delete Credit Card Account
				var result = controller.DeleteMembershipMileageAccount(request.AccountID);

				if (!result)
					throw VtodException.CreateException(ExceptionType.Accounting, 8009);//throw new AccountingException(Messages.Accounting_DeleteMembershipMileageAccountFailed);
				else
					response.Success = new Success();
				#endregion				

				return response;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("AccountingDomain.DeleteMembershipMileageAccount:: {0}", ex.ToString() + ex.StackTrace);
                
				#region Throw AccountingException || Validation Exception

				if (ex.Message.Contains("::"))//Remove method name
					throw new AccountingException(ex.Message.Substring(ex.Message.IndexOf("::") + 2));
				else
					throw new AccountingException(ex.Message);

				#endregion
			}
		}

        internal RewardModifyMemberAccountRS RewardModifyMemberAccount(TokenRS tokenVTOD, RewardModifyMemberAccountRQ request)
        {
            try
            {
                #region Init
                var controller = new UDI.SDS.AccoutingController(tokenVTOD, TrackTime);
                UDI.SDS.MembershipService.AirlineMileageAccounts accountDetails = new AirlineMileageAccounts();
                accountDetails.AccountNumber = request.AccountNumber;
                accountDetails.RewardsID = request.RewardID;
                #endregion

                #region Init Respose
                var response = new RewardModifyMemberAccountRS();
                #endregion

                #region Delete Credit Card Account
                var result = controller.DeleteMembershipMileageAccount(request.AccountID);

                if (!result)
                    throw VtodException.CreateException(ExceptionType.Accounting, 8009);
                #endregion

                #region Create Membership Mileage Account

                response.Result = controller.CreateMembershipMileageAccount(request.MemberID, accountDetails, request.IsDefault);
                response.Success = new Success();

                #endregion

                return response;
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("AccountingDomain.RewardModifyMemberAccount:: {0}", ex.ToString() + ex.StackTrace);
                #region Throw AccountingException || Validation Exception

                if (ex.Message.Contains("::"))
                    throw new AccountingException(ex.Message.Substring(ex.Message.IndexOf("::") + 2));
                else
                    throw new AccountingException(ex.Message);

                #endregion
            }
        }
		internal RewardGetMemberAccountsRS RewardGetMemberAccounts(TokenRS tokenVTOD, RewardGetMemberAccountsRQ request)
		{
			try
			{
				#region Init
				var controller = new UDI.SDS.AccoutingController(tokenVTOD, TrackTime);
				#endregion

				#region Init Respose
				var response = new RewardGetMemberAccountsRS();
				response.AirlineRewards = new List<Common.DTO.OTA.AirlineReward>();
				#endregion

				#region Get Airline Rewards Programs
				var results = controller.GetMembershipAirlineMileageAccounts(request.MemberID);

				if (results != null && results.Count > 0)
				{
					foreach (var result in results)
					{
						var account = new Common.DTO.OTA.AirlineReward();
						account.AccountID = result.RewardsID; //IMPORTANT this is the problem in SDS api. Do not change it
						account.ProgramName = result.ProgramName;
						account.RewardID = result.AccountID; //IMPORTANT this is the problem in SDS api. Do not change it
						account.IsDefault = result.IsDefault.ToString();
						account.AccountNumber = result.AccountNumber;

						response.AirlineRewards.Add(account);
					}

					response.Success = new Success();
				}

				#endregion
                
				return response;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("AccountingDomain.GetMembershipAirlineMileageAccounts:: {0}", ex.ToString() + ex.StackTrace);

				#region Throw AccountingException || Validation Exception

				if (ex.Message.Contains("::"))//Remove method name
					throw new AccountingException(ex.Message.Substring(ex.Message.IndexOf("::") + 2));
				else
					throw new AccountingException(ex.Message);

				#endregion
			}
		}

		internal RewardSetDefaultAccountRS RewardSetDefaultAccount(TokenRS tokenVTOD, RewardSetDefaultAccountRQ request)
		{
			try
			{
				#region Init
				var controller = new UDI.SDS.AccoutingController(tokenVTOD, TrackTime);
				#endregion

				#region Init Respose
				var response = new RewardSetDefaultAccountRS();
				#endregion

				#region Get Airline Rewards Programs
				var result = controller.SetDefaultMileageAccount(request.MemberId, request.AccountId, request.IsDefault);

				if (result)
				{
					response.Success = new Success();
				}
				else
				{
					throw VtodException.CreateException(ExceptionType.Accounting, 8002);// new Exception(Messages.UDI_SDS_DefaultAirlineRewardError);
				}
				#endregion

				return response;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("AccountingDomain.GetMembershipAirlineMileageAccounts:: {0}", ex.ToString() + ex.StackTrace);

				throw ex;
			}
		}
		#endregion

		#region zTrip Credits

		internal ZTripCreditActivateMemberCreditRS ZTripCreditActivateMemberCredit(TokenRS tokenVTOD, ZTripCreditActivateMemberCreditRQ request)
		{
			try
			{
				#region Init
				var controller = new UDI.SDS.AccoutingController(tokenVTOD, TrackTime);
				#endregion

				#region Init Respose
				var response = new ZTripCreditActivateMemberCreditRS();
				#endregion

				#region Get Airline Rewards Programs
				var results = controller.ActivateCredits(request.ActivationCode, request.MemberId);

				if (results != null)
				{
					response.ActivationMessage = results.ActivationMessage;
					response.CreditsBalance = results.CreditsBalance;
					response.Success = new Success();
				}

				#endregion
                
				return response;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("AccountingDomain.ActivateCredits:: {0}", ex.ToString() + ex.StackTrace);
                
				#region Throw AccountingException || Validation Exception

				if (ex.Message.Contains("::"))//Remove method name
					throw new AccountingException(ex.Message.Substring(ex.Message.IndexOf("::") + 2));
				else
					throw new AccountingException(ex.Message);

				#endregion
			}
		}

		internal ZTripCreditGetMemberBalanceRS ZTripCreditGetMemberBalance(TokenRS tokenVTOD, ZTripCreditGetMemberBalanceRQ request)
		{
			try
			{
				#region Init
				var controller = new UDI.SDS.AccoutingController(tokenVTOD, TrackTime);
				#endregion

				#region Init Respose
				var response = new ZTripCreditGetMemberBalanceRS();
				#endregion

				#region Get Airline Rewards Programs
				response.Balance = controller.GetCreditsBalance(request.MemberId).ToString();
				response.Success = new Success();

				#endregion

				return response;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("AccountingDomain.GetCreditsBalance:: {0}", ex.ToString() + ex.StackTrace);
                
				#region Throw AccountingException || Validation Exception

				if (ex.Message.Contains("::"))//Remove method name
					throw new AccountingException(ex.Message.Substring(ex.Message.IndexOf("::") + 2));
				else
					throw new AccountingException(ex.Message);

				#endregion
			}
		}

		#endregion

		#region FareDetail
		internal GetFareDetailRS GetFareDetail(TokenRS tokenVTOD, GetFareDetailRQ request)
		{
			try
			{
                string alephVendorTripID=null;
                AlephBookingRS alephBookingResponse = null;
				GetFareDetailRS result = null;
				var rateQualifier = string.Empty;
                AlephStatusParameter asp = new AlephStatusParameter();
                AlephECarUtility utility = new AlephECarUtility(logger);
				var reservationsController = new UDI.SDS.ReservationsController(tokenVTOD, TrackTime);
				var vtodDomain = new VTOD.VTODDomain(); vtodDomain.TrackTime = TrackTime;
				var confirmationObj = request.Reference.Where(s => s.Type == Common.DTO.Enum.ConfirmationType.Confirmation.ToString()).FirstOrDefault();
				long confirmationID = 0;
				if (confirmationObj !=null)
				{
					confirmationID = confirmationObj.ID.ToInt64();
				}
				string dispatchConfirmationID = null;
				var dispatchConfirmationObj = request.Reference.Where(s => s.Type == Common.DTO.Enum.ConfirmationType.DispatchConfirmation.ToString()).FirstOrDefault();

				if (dispatchConfirmationObj != null)
				{
					dispatchConfirmationID = dispatchConfirmationObj.ID;
				}

				try
				{
					rateQualifier = request.TPA_Extensions.RateQualifiers.First().RateQualifierValue;
				}
				catch
				{ }
                if (!string.IsNullOrWhiteSpace(dispatchConfirmationID))
                {
                if (dispatchConfirmationID.Contains("##"))
                {
                    if (dispatchConfirmationID.IndexOf("##") != -1)
                    {
                        alephVendorTripID = dispatchConfirmationID.Substring(dispatchConfirmationID.IndexOf("##") + 2);
                    }
                }
                }
				#region DispatchConfirmationID
				//long? rezID = null;
				vtod_trip trip = null;

				using (var db = new DataAccess.VTOD.VTODEntities())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Accounting_Domain, "Call");
                    #endregion

                    if (confirmationID > 0)
                    {
                        if (rateQualifier.Trim().ToLower() == "taxi")
                        {
                            trip = db.vtod_trip.Include("taxi_trip").Where(s => s.Id == confirmationID).FirstOrDefault();
                        }
                        else
                        {
                            trip = db.vtod_trip.Include("sds_trip").Include("ecar_trip").Where(s => s.Id == confirmationID).FirstOrDefault();
                        }
                    }
                    else if (dispatchConfirmationID != null)
                    {
                        trip = db.ecar_trip.Include("vtod_trip").Where(x => x.DispatchTripId == dispatchConfirmationID).FirstOrDefault().vtod_trip;
                    }



                    if (!string.IsNullOrWhiteSpace(dispatchConfirmationID) && !dispatchConfirmationID.Contains("##"))
                    {
                        if (rateQualifier.Trim().ToLower() == "execucar")
                        {
                            ecar_trip ecarTrip = new ecar_trip();
                            ecarTrip = db.ecar_trip.Where(s => s.DispatchTripId == dispatchConfirmationID).FirstOrDefault();
                            if (ecarTrip != null)
                            {
                                trip = db.vtod_trip.Include("sds_trip").Where(s => s.Id == ecarTrip.Id).FirstOrDefault();
                                trip.ecar_trip = ecarTrip;
                            }
                        }
                    }
                    #region Track
                    if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_DB, "db.vtod_trip.Where(s => s.Id == confirmationID).ToList()");
					#endregion
				}
                #region "Output conversion"
                result = new GetFareDetailRS();
                result.FareItems = new FareItems();
                result.FareItems.Items = new List<NameValue>();
                #endregion
                if (!string.IsNullOrWhiteSpace(dispatchConfirmationID) || confirmationID>0)
                {
                    //Need to get WebServiceState, if the RQ does not have it
                    string wsState = string.Empty;

                    if (string.IsNullOrWhiteSpace(request.TPA_Extensions.WebServiceState))
                    {
                        wsState = vtodDomain.Get_WS_State("vtod_trip", trip.Id, "CorporateIDs");
                        request.TPA_Extensions.WebServiceState = wsState;
                    }

                    if (request.TPA_Extensions.RateQualifiers.First().SpecialInputs.Count == 0 && !string.IsNullOrEmpty(wsState))
                    {
                        request.TPA_Extensions.RateQualifiers.First().SpecialInputs = wsState.JsonDeserialize<List<NameValue>>();
                    }

                    if (trip!=null && trip.FleetTripCode==FleetTripCode.Ecar_Texas)
                    {

                        #region Black Sedan Texas Taxi
                        string paymentType = null;// string.Empty;
                        if (trip != null)
                        {
                            #region Make result
                            result.Success = new Success();
                            result.TripInfo = new Common.DTO.OTA.TripInfo();
                            result.PaymentInfo = new PaymentInfo { Type = paymentType };
                            decimal? creditsApplied = null;
                            decimal? charged = null;
                            string transactionNumber = null;
                            DateTime? transactionTime = null;
                            string transactionError = null;
                            using (var db = new DataAccess.VTOD.VTODEntities())
                            {
                                var ecar_trip_status = db.ecar_trip_status.Where(s => s.TripID == trip.Id && s.VehicleLatitude != null && s.VehicleLongitude != null).OrderBy(p => p.Id).ToList();
                                if (ecar_trip_status != null)
                                {
                                    if (ecar_trip_status.Any())
                                    {
                                        result.DropOffLatitude = ecar_trip_status.FirstOrDefault().VehicleLatitude.ToString();
                                        result.DropOffLongitude = ecar_trip_status.FirstOrDefault().VehicleLongitude.ToString();
                                    }
                                }
                            }

                            if (trip.IsSplitPayment.HasValue && trip.IsSplitPayment.Value
                                && trip.SplitPaymentStatus.HasValue
                                && (trip.SplitPaymentStatus.Value == (int)SplitPaymentStatus.Accepted || trip.SplitPaymentStatus.Value == (int)SplitPaymentStatus.Completed))  // split payment
                            {
                                result.SplitMemberInfo = new SplitPaymentInfo();
                                result.SplitMemberInfo.MemberA = new SplitPaymentUserInfo
                                {
                                    MemberID = trip.MemberID,
                                    FirstName = trip.taxi_trip.FirstName,
                                    LastName = trip.taxi_trip.LastName
                                };
                                result.SplitMemberInfo.MemberB = new SplitPaymentUserInfo
                                {
                                    MemberID = trip.SplitPaymentMemberB,
                                    FirstName = trip.taxi_trip.MemberBFirstName,
                                    LastName = trip.taxi_trip.MemberBLastName
                                };

                                result.SplitRideInfo = new List<SplitPaymentFare>();
                                result.SplitRideInfo.Add(new SplitPaymentFare
                                {
                                    FareItems = new FareItems
                                    {
                                        Items = new List<NameValue>()
                                    }
                                });

                                #region Gratuity Rate
                                if (trip.GratuityRate.HasValue)
                                {
                                    var gratuityRate = new NameValue();
                                    gratuityRate.Name = "GratuityRate";
                                    gratuityRate.Value = trip.GratuityRate.Value.ToString();
                                    result.FareItems.Items.Add(gratuityRate);

                                    result.SplitRideInfo[0].FareItems.Items.Add(gratuityRate);
                                }
                                #endregion

                                #region Gratuity
                                if (trip.Gratuity.HasValue)
                                {
                                    var gratuity = new NameValue();
                                    gratuity.Name = "Gratuity";
                                    gratuity.Value = decimal.Round(trip.Gratuity.Value / 2, 2).ToString();
                                    result.FareItems.Items.Add(gratuity);

                                    result.SplitRideInfo[0].FareItems.Items.Add(gratuity);
                                }
                                #endregion

                                #region Fare
                                if (trip.TotalFareAmount.HasValue)
                                {
                                    var total = new NameValue();
                                    total.Name = "Fare";
                                    if (trip.Gratuity.HasValue)
                                        total.Value = decimal.Round((trip.TotalFareAmount.Value - trip.Gratuity.Value) / 2, 2).ToString();
                                    else
                                        total.Value = decimal.Round(trip.TotalFareAmount.Value / 2, 2).ToString();
                                    result.FareItems.Items.Add(total);

                                    result.SplitRideInfo[0].FareItems.Items.Add(total);
                                }
                                #endregion

                                if (!string.IsNullOrWhiteSpace(trip.MemberBCreditCardID))
                                {
                                    var rq = new AccountingGetMemberPaymentCardsRQ { MemberId = trip.SplitPaymentMemberB.ToInt32() };
                                    var memberPaymentCards = AccountingGetMemberPaymentCards(tokenVTOD, rq);
                                    if (memberPaymentCards != null && memberPaymentCards.PaymentCards != null && memberPaymentCards.PaymentCards.Any())
                                    {
                                        foreach (var item in memberPaymentCards.PaymentCards)
                                        {
                                            if (item.CardNumber.ID == trip.MemberBCreditCardID)
                                            {
                                                result.SplitRideInfo[0].PaymentInfo = new PaymentInfo
                                                {
                                                    PaymentCard = item,
                                                    Type = paymentType
                                                };

                                                break;
                                            }
                                        }
                                    }
                                }

                                #region zTrip Credit, transactionNumber, error, transactionTime
                                using (var db = new DataAccess.VTOD.VTODEntities())
                                {
                                    try
                                    {
                                        var tripAccountTransactionForMemberA = db.vtod_trip_account_transaction.Where(s => s.TripID == trip.Id && s.MemberID == trip.MemberID).FirstOrDefault();
                                        var tripDriverInfo = db.vtod_driver_info.Where(s => s.Id == trip.DriverInfoID).OrderByDescending(s => s.Id).FirstOrDefault();

                                        if (tripAccountTransactionForMemberA != null && tripAccountTransactionForMemberA.CreditsApplied.HasValue && tripAccountTransactionForMemberA.CreditsApplied.Value != 0)
                                        {
                                            creditsApplied = -System.Math.Abs(tripAccountTransactionForMemberA.CreditsApplied.Value);
                                        }

                                        decimal? creditsAppliedForMemberB = null;
                                        var tripAccountTransactionForMemberB = db.vtod_trip_account_transaction.Where(s => s.TripID == trip.Id && s.MemberID == trip.SplitPaymentMemberB).FirstOrDefault();
                                        if (tripAccountTransactionForMemberB.CreditsApplied.HasValue && tripAccountTransactionForMemberB.CreditsApplied.Value != 0)
                                        {
                                            creditsAppliedForMemberB = -System.Math.Abs(tripAccountTransactionForMemberB.CreditsApplied.Value);
                                        }

                                        //This section is for TCS.
                                        if (request.Reference.First().Type == ConfirmationType.DispatchConfirmation.ToString())
                                        {
                                            #region Account Transaction
                                            if (tripAccountTransactionForMemberA != null)
                                            {
                                                transactionTime = tripAccountTransactionForMemberA.AppendTime;
                                            }

                                            if (tripAccountTransactionForMemberA != null && tripAccountTransactionForMemberA.AmountCharged.HasValue && tripAccountTransactionForMemberA.AmountCharged.Value != 0)
                                            {
                                                charged = -System.Math.Abs(tripAccountTransactionForMemberA.AmountCharged.Value);
                                            }
                                            if (tripAccountTransactionForMemberA != null && !string.IsNullOrWhiteSpace(tripAccountTransactionForMemberA.TransactionNumber))
                                            {
                                                transactionNumber = tripAccountTransactionForMemberA.TransactionNumber;
                                            }
                                            if (tripAccountTransactionForMemberA != null && !string.IsNullOrWhiteSpace(tripAccountTransactionForMemberA.Error))
                                            {
                                                transactionError = tripAccountTransactionForMemberA.Error;
                                            }


                                            if (creditsApplied.HasValue && creditsApplied != 0)
                                            {
                                                var zTripCreditKeyValue = new NameValue();
                                                zTripCreditKeyValue.Name = "zTrip Credit";
                                                zTripCreditKeyValue.Value = creditsApplied.ToString();
                                                result.FareItems.Items.Add(zTripCreditKeyValue);
                                            }

                                            if (!string.IsNullOrWhiteSpace(transactionNumber))
                                            {
                                                var transactionNumberKeyValue = new NameValue();
                                                transactionNumberKeyValue.Name = "TransactionNumber";
                                                transactionNumberKeyValue.Value = transactionNumber;
                                                result.FareItems.Items.Add(transactionNumberKeyValue);
                                            }
                                            if (transactionTime.HasValue)
                                            {
                                                var transactionTimeKeyValue = new NameValue();
                                                transactionTimeKeyValue.Name = "TransactionTime";
                                                transactionTimeKeyValue.Value = transactionTime.ToString();
                                                result.FareItems.Items.Add(transactionTimeKeyValue);
                                            }
                                            if (charged.HasValue)
                                            {
                                                var chargedTimeKeyValue = new NameValue();
                                                chargedTimeKeyValue.Name = "AmountCharged";
                                                chargedTimeKeyValue.Value = charged.ToString();
                                                result.FareItems.Items.Add(chargedTimeKeyValue);
                                            }
                                            if (!string.IsNullOrEmpty(transactionError))
                                            {
                                                var errorKeyValue = new NameValue();
                                                errorKeyValue.Name = "TransactionError";
                                                errorKeyValue.Value = transactionError.ToString();
                                                result.FareItems.Items.Add(errorKeyValue);
                                            }
                                            //if (!string.IsNullOrEmpty(transactionError))
                                            //{
                                            var confirmationeyValue = new NameValue();
                                            confirmationeyValue.Name = "VtodTripId";
                                            confirmationeyValue.Value = trip.Id.ToString();
                                            result.FareItems.Items.Add(confirmationeyValue);
                                            //}
                                            if (!string.IsNullOrWhiteSpace(trip.MemberID))
                                            {
                                                var memberIDValue = new NameValue();
                                                memberIDValue.Name = "MemberId";
                                                memberIDValue.Value = trip.MemberID;
                                                result.FareItems.Items.Add(memberIDValue);
                                            }
                                            if (!string.IsNullOrWhiteSpace(trip.CreditCardID))
                                            {
                                                var creditCardIDValue = new NameValue();
                                                creditCardIDValue.Name = "CreditCardId";
                                                creditCardIDValue.Value = trip.CreditCardID;
                                                result.FareItems.Items.Add(creditCardIDValue);
                                            }
                                            if (!string.IsNullOrWhiteSpace(trip.PaymentType))
                                            {
                                                var paymentTypeValue = new NameValue();
                                                paymentTypeValue.Name = "PaymentType";
                                                paymentTypeValue.Value = trip.PaymentType;
                                                result.FareItems.Items.Add(paymentTypeValue);
                                            }
                                            #endregion

                                            #region DriverInfo
                                            if (tripDriverInfo != null)
                                            {
                                                if (!string.IsNullOrWhiteSpace(tripDriverInfo.VehicleID))
                                                {
                                                    var chargedTimeKeyValue = new NameValue();
                                                    chargedTimeKeyValue.Name = "VehicleID";
                                                    chargedTimeKeyValue.Value = tripDriverInfo.VehicleID;
                                                    result.FareItems.Items.Add(chargedTimeKeyValue);
                                                }

                                                if (!string.IsNullOrWhiteSpace(tripDriverInfo.DriverID))
                                                {
                                                    var chargedTimeKeyValue = new NameValue();
                                                    chargedTimeKeyValue.Name = "DriverID";
                                                    chargedTimeKeyValue.Value = tripDriverInfo.DriverID;
                                                    result.FareItems.Items.Add(chargedTimeKeyValue);
                                                }

                                                if (!string.IsNullOrWhiteSpace(tripDriverInfo.FirstName))
                                                {
                                                    var chargedTimeKeyValue = new NameValue();
                                                    chargedTimeKeyValue.Name = "FirstName";
                                                    chargedTimeKeyValue.Value = tripDriverInfo.FirstName;
                                                    result.FareItems.Items.Add(chargedTimeKeyValue);
                                                }

                                                if (!string.IsNullOrWhiteSpace(tripDriverInfo.LastName))
                                                {
                                                    var chargedTimeKeyValue = new NameValue();
                                                    chargedTimeKeyValue.Name = "LastName";
                                                    chargedTimeKeyValue.Value = tripDriverInfo.LastName;
                                                    result.FareItems.Items.Add(chargedTimeKeyValue);
                                                }



                                            }
                                            #endregion

                                            #region Source and Device
                                            if (!string.IsNullOrWhiteSpace(trip.ConsumerSource))
                                            {
                                                var keyValue = new NameValue();
                                                keyValue.Name = "Source";
                                                keyValue.Value = trip.ConsumerSource;
                                                result.FareItems.Items.Add(keyValue);
                                            }

                                            if (!string.IsNullOrWhiteSpace(trip.ConsumerDevice))
                                            {
                                                var keyValue = new NameValue();
                                                keyValue.Name = "Device";
                                                keyValue.Value = trip.ConsumerDevice;
                                                result.FareItems.Items.Add(keyValue);
                                            }


                                            #endregion

                                            #region Account Transaction for Member B
                                            if (tripAccountTransactionForMemberB != null)
                                            {
                                                decimal? chargedForMemberB = null;
                                                string transactionNumberForMemberB = null;
                                                DateTime? transactionTimeForMemberB = null;
                                                string transactionErrorForMemberB = null;

                                                transactionTimeForMemberB = tripAccountTransactionForMemberB.AppendTime;

                                                if (tripAccountTransactionForMemberB.AmountCharged.HasValue && tripAccountTransactionForMemberB.AmountCharged.Value != 0)
                                                {
                                                    chargedForMemberB = -System.Math.Abs(tripAccountTransactionForMemberB.AmountCharged.Value);
                                                }
                                                if (!string.IsNullOrWhiteSpace(tripAccountTransactionForMemberB.TransactionNumber))
                                                {
                                                    transactionNumberForMemberB = tripAccountTransactionForMemberB.TransactionNumber;
                                                }
                                                if (!string.IsNullOrWhiteSpace(tripAccountTransactionForMemberB.Error))
                                                {
                                                    transactionErrorForMemberB = tripAccountTransactionForMemberB.Error;
                                                }


                                                if (creditsAppliedForMemberB.HasValue && creditsAppliedForMemberB != 0)
                                                {
                                                    var zTripCreditKeyValue = new NameValue();
                                                    zTripCreditKeyValue.Name = "zTrip Credit";
                                                    zTripCreditKeyValue.Value = creditsAppliedForMemberB.ToString();
                                                    result.SplitRideInfo[0].FareItems.Items.Add(zTripCreditKeyValue);
                                                }

                                                if (!string.IsNullOrWhiteSpace(transactionNumberForMemberB))
                                                {
                                                    var transactionNumberKeyValue = new NameValue();
                                                    transactionNumberKeyValue.Name = "TransactionNumber";
                                                    transactionNumberKeyValue.Value = transactionNumberForMemberB;
                                                    result.SplitRideInfo[0].FareItems.Items.Add(transactionNumberKeyValue);
                                                }
                                                if (transactionTimeForMemberB.HasValue)
                                                {
                                                    var transactionTimeKeyValue = new NameValue();
                                                    transactionTimeKeyValue.Name = "TransactionTime";
                                                    transactionTimeKeyValue.Value = transactionTimeForMemberB.ToString();
                                                    result.SplitRideInfo[0].FareItems.Items.Add(transactionTimeKeyValue);
                                                }
                                                if (chargedForMemberB.HasValue)
                                                {
                                                    var chargedTimeKeyValue = new NameValue();
                                                    chargedTimeKeyValue.Name = "AmountCharged";
                                                    chargedTimeKeyValue.Value = chargedForMemberB.ToString();
                                                    result.SplitRideInfo[0].FareItems.Items.Add(chargedTimeKeyValue);
                                                }
                                                if (!string.IsNullOrEmpty(transactionErrorForMemberB))
                                                {
                                                    var errorKeyValue = new NameValue();
                                                    errorKeyValue.Name = "TransactionError";
                                                    errorKeyValue.Value = transactionErrorForMemberB.ToString();
                                                    result.SplitRideInfo[0].FareItems.Items.Add(errorKeyValue);
                                                }

                                                confirmationeyValue = new NameValue();
                                                confirmationeyValue.Name = "VtodTripId";
                                                confirmationeyValue.Value = trip.Id.ToString();
                                                result.SplitRideInfo[0].FareItems.Items.Add(confirmationeyValue);

                                                if (!string.IsNullOrWhiteSpace(trip.SplitPaymentMemberB))
                                                {
                                                    var memberIDValue = new NameValue();
                                                    memberIDValue.Name = "MemberId";
                                                    memberIDValue.Value = trip.SplitPaymentMemberB;
                                                    result.SplitRideInfo[0].FareItems.Items.Add(memberIDValue);
                                                }
                                                if (!string.IsNullOrWhiteSpace(trip.MemberBCreditCardID))
                                                {
                                                    var creditCardIDValue = new NameValue();
                                                    creditCardIDValue.Name = "CreditCardId";
                                                    creditCardIDValue.Value = trip.MemberBCreditCardID;
                                                    result.SplitRideInfo[0].FareItems.Items.Add(creditCardIDValue);
                                                }
                                                if (!string.IsNullOrWhiteSpace(trip.PaymentType))
                                                {
                                                    var paymentTypeValue = new NameValue();
                                                    paymentTypeValue.Name = "PaymentType";
                                                    paymentTypeValue.Value = trip.PaymentType;
                                                    result.SplitRideInfo[0].FareItems.Items.Add(paymentTypeValue);
                                                }
                                            }
                                            #endregion
                                        }
                                        else
                                        {
                                            if (creditsApplied.HasValue && creditsApplied != 0)
                                            {
                                                var zTripCreditKeyValue = new NameValue();
                                                zTripCreditKeyValue.Name = "zTrip Credit";
                                                zTripCreditKeyValue.Value = creditsApplied.ToString();
                                                result.FareItems.Items.Add(zTripCreditKeyValue);
                                            }

                                            if (creditsAppliedForMemberB.HasValue && creditsAppliedForMemberB != 0)
                                            {
                                                var zTripCreditKeyValue = new NameValue();
                                                zTripCreditKeyValue.Name = "zTrip Credit";
                                                zTripCreditKeyValue.Value = creditsAppliedForMemberB.ToString();
                                                result.SplitRideInfo[0].FareItems.Items.Add(zTripCreditKeyValue);
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {

                                        logger.Error(ex);
                                    }

                                }
                                #endregion

                            }
                            else
                            {
                                #region Gratuity Rate
                                if (trip.GratuityRate.HasValue)
                                {
                                    var gratuityRate = new NameValue();
                                    gratuityRate.Name = "GratuityRate";
                                    gratuityRate.Value = trip.GratuityRate.Value.ToString();
                                    result.FareItems.Items.Add(gratuityRate);
                                }
                                #endregion

                                #region Gratuity
                                if (trip.Gratuity.HasValue)
                                {
                                    var gratuity = new NameValue();
                                    gratuity.Name = "Gratuity";
                                    gratuity.Value = trip.Gratuity.Value.ToString();
                                    result.FareItems.Items.Add(gratuity);
                                }
                                #endregion

                                #region Fare
                                if (trip.TotalFareAmount.HasValue)
                                {
                                    var total = new NameValue();
                                    total.Name = "Fare";
                                    if (trip.Gratuity.HasValue)
                                        total.Value = (trip.TotalFareAmount.Value - trip.Gratuity.Value).ToString();
                                    else
                                        total.Value = trip.TotalFareAmount.Value.ToString();
                                    result.FareItems.Items.Add(total);
                                }
                                #endregion

                                #region zTrip Credit, transactionNumber, error, transactionTime
                                using (var db = new DataAccess.VTOD.VTODEntities())
                                {

                                    try
                                    {
                                        var tripAccountTransactions = db.vtod_trip_account_transaction.Where(s => s.TripID == trip.Id).FirstOrDefault();
                                        var tripDriverInfo = db.vtod_driver_info.Where(s => s.Id == trip.DriverInfoID).OrderByDescending(s => s.Id).FirstOrDefault();

                                        if (tripAccountTransactions != null && tripAccountTransactions.CreditsApplied.HasValue && tripAccountTransactions.CreditsApplied.Value != 0)
                                        {
                                            creditsApplied = -System.Math.Abs(tripAccountTransactions.CreditsApplied.Value);
                                        }

                                        //This section is for TCS.
                                        if (request.Reference.First().Type == ConfirmationType.DispatchConfirmation.ToString())
                                        {


                                            #region Account Transaction
                                            if (tripAccountTransactions != null)
                                            {
                                                transactionTime = tripAccountTransactions.AppendTime;
                                            }

                                            if (tripAccountTransactions != null && tripAccountTransactions.AmountCharged.HasValue && tripAccountTransactions.AmountCharged.Value != 0)
                                            {
                                                charged = -System.Math.Abs(tripAccountTransactions.AmountCharged.Value);
                                            }
                                            if (tripAccountTransactions != null && !string.IsNullOrWhiteSpace(tripAccountTransactions.TransactionNumber))
                                            {
                                                transactionNumber = tripAccountTransactions.TransactionNumber;
                                            }
                                            if (tripAccountTransactions != null && !string.IsNullOrWhiteSpace(tripAccountTransactions.Error))
                                            {
                                                transactionError = tripAccountTransactions.Error;
                                            }


                                            if (creditsApplied.HasValue && creditsApplied != 0)
                                            {
                                                var zTripCreditKeyValue = new NameValue();
                                                zTripCreditKeyValue.Name = "zTrip Credit";
                                                zTripCreditKeyValue.Value = creditsApplied.ToString();
                                                result.FareItems.Items.Add(zTripCreditKeyValue);
                                            }

                                            if (!string.IsNullOrWhiteSpace(transactionNumber))
                                            {
                                                var transactionNumberKeyValue = new NameValue();
                                                transactionNumberKeyValue.Name = "TransactionNumber";
                                                transactionNumberKeyValue.Value = transactionNumber;
                                                result.FareItems.Items.Add(transactionNumberKeyValue);
                                            }
                                            if (transactionTime.HasValue)
                                            {
                                                var transactionTimeKeyValue = new NameValue();
                                                transactionTimeKeyValue.Name = "TransactionTime";
                                                transactionTimeKeyValue.Value = transactionTime.ToString();
                                                result.FareItems.Items.Add(transactionTimeKeyValue);
                                            }
                                            if (charged.HasValue)
                                            {
                                                var chargedTimeKeyValue = new NameValue();
                                                chargedTimeKeyValue.Name = "AmountCharged";
                                                chargedTimeKeyValue.Value = charged.ToString();
                                                result.FareItems.Items.Add(chargedTimeKeyValue);
                                            }
                                            if (!string.IsNullOrEmpty(transactionError))
                                            {
                                                var errorKeyValue = new NameValue();
                                                errorKeyValue.Name = "TransactionError";
                                                errorKeyValue.Value = transactionError.ToString();
                                                result.FareItems.Items.Add(errorKeyValue);
                                            }
                                            //if (!string.IsNullOrEmpty(transactionError))
                                            //{
                                            var confirmationeyValue = new NameValue();
                                            confirmationeyValue.Name = "VtodTripId";
                                            confirmationeyValue.Value = trip.Id.ToString();
                                            result.FareItems.Items.Add(confirmationeyValue);
                                            //}
                                            if (!string.IsNullOrWhiteSpace(trip.MemberID))
                                            {
                                                var memberIDValue = new NameValue();
                                                memberIDValue.Name = "MemberId";
                                                memberIDValue.Value = trip.MemberID;
                                                result.FareItems.Items.Add(memberIDValue);
                                            }
                                            if (!string.IsNullOrWhiteSpace(trip.CreditCardID))
                                            {
                                                var creditCardIDValue = new NameValue();
                                                creditCardIDValue.Name = "CreditCardId";
                                                creditCardIDValue.Value = trip.CreditCardID;
                                                result.FareItems.Items.Add(creditCardIDValue);
                                            }
                                            if (!string.IsNullOrWhiteSpace(trip.PaymentType))
                                            {
                                                var paymentTypeValue = new NameValue();
                                                paymentTypeValue.Name = "PaymentType";
                                                paymentTypeValue.Value = trip.PaymentType;
                                                result.FareItems.Items.Add(paymentTypeValue);
                                            }
                                            #endregion

                                            #region DriverInfo
                                            if (tripDriverInfo != null)
                                            {
                                                if (!string.IsNullOrWhiteSpace(tripDriverInfo.VehicleID))
                                                {
                                                    var chargedTimeKeyValue = new NameValue();
                                                    chargedTimeKeyValue.Name = "VehicleID";
                                                    chargedTimeKeyValue.Value = tripDriverInfo.VehicleID;
                                                    result.FareItems.Items.Add(chargedTimeKeyValue);
                                                }

                                                if (!string.IsNullOrWhiteSpace(tripDriverInfo.DriverID))
                                                {
                                                    var chargedTimeKeyValue = new NameValue();
                                                    chargedTimeKeyValue.Name = "DriverID";
                                                    chargedTimeKeyValue.Value = tripDriverInfo.DriverID;
                                                    result.FareItems.Items.Add(chargedTimeKeyValue);
                                                }

                                                if (!string.IsNullOrWhiteSpace(tripDriverInfo.FirstName))
                                                {
                                                    var chargedTimeKeyValue = new NameValue();
                                                    chargedTimeKeyValue.Name = "FirstName";
                                                    chargedTimeKeyValue.Value = tripDriverInfo.FirstName;
                                                    result.FareItems.Items.Add(chargedTimeKeyValue);
                                                }

                                                if (!string.IsNullOrWhiteSpace(tripDriverInfo.LastName))
                                                {
                                                    var chargedTimeKeyValue = new NameValue();
                                                    chargedTimeKeyValue.Name = "LastName";
                                                    chargedTimeKeyValue.Value = tripDriverInfo.LastName;
                                                    result.FareItems.Items.Add(chargedTimeKeyValue);
                                                }



                                            }
                                            #endregion

                                            #region Source and Device
                                            if (!string.IsNullOrWhiteSpace(trip.ConsumerSource))
                                            {
                                                var keyValue = new NameValue();
                                                keyValue.Name = "Source";
                                                keyValue.Value = trip.ConsumerSource;
                                                result.FareItems.Items.Add(keyValue);
                                            }

                                            if (!string.IsNullOrWhiteSpace(trip.ConsumerDevice))
                                            {
                                                var keyValue = new NameValue();
                                                keyValue.Name = "Device";
                                                keyValue.Value = trip.ConsumerDevice;
                                                result.FareItems.Items.Add(keyValue);
                                            }


                                            #endregion
                                        }
                                        else
                                        {
                                            if (creditsApplied.HasValue && creditsApplied != 0)
                                            {
                                                var zTripCreditKeyValue = new NameValue();
                                                zTripCreditKeyValue.Name = "zTrip Credit";
                                                zTripCreditKeyValue.Value = creditsApplied.ToString();
                                                result.FareItems.Items.Add(zTripCreditKeyValue);
                                            }

                                        }
                                    }
                                    catch (Exception ex)
                                    {

                                        logger.Error(ex);
                                    }

                                }
                                #endregion
                            }

                            if (trip.TripStartTime.HasValue && trip.TripEndTime.HasValue)
                            {
                                
                                TimeSpan ts = trip.TripEndTime.Value.Subtract(trip.TripStartTime.Value).Duration();
                                result.TripInfo.Duration = string.Format("{0:00}:{1:00}:{2:00}", ts.Hours, ts.Minutes, ts.Seconds);
                            }
                            #endregion
                        }
                        else
                        {
                            throw VtodException.CreateException(ExceptionType.ECar, 1012);
                        } 
                        #endregion
                    }
                    else if (trip != null && trip.FleetTripCode == FleetTripCode.Ecar_Aleph && !utility.ValidateFareDetailsInput(tokenVTOD, request, ref asp))
                    {
                        throw VtodException.CreateException(ExceptionType.ECar, 3003);
                    }
                    else
                    {
                        if (trip != null && trip.FleetTripCode == FleetTripCode.Ecar_Aleph)
                        {
                            #region Get Corporate Key
                            string corporateKey = string.Empty;
                            try
                            {
                                corporateKey = AlephSDSCall.GetCorporateAccountKey(tokenVTOD, TrackTime, asp.CorporateEmail);
                            }
                            catch (Exception ex1)
                            {
                                throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(ex1.Message));
                            }

                            #endregion Get Corporate Key

                            #region Call Aleph API


                            try
                            {
                                var alephAPICall = new AlephAPICall();
                                alephBookingResponse = alephAPICall.GetReservation(asp.CorporateProviderID, asp.CorporateCompanyName, asp.CorporateProviderAccountID, asp.CorporateProfileID, asp.CorporateEmail, corporateKey, alephVendorTripID);
                            }
                            catch (Exception ex)
                            {
                                string msg = string.Format("Unable to retrieve status for this trip. tripID={0}, errormessage={1}", asp.Trip.Id, ex.InnerException.Message);
                                logger.Warn(msg);

                                throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(ex.Message));
                            }

                            #endregion Call Aleph API
                        }
                    }
                }
				#endregion

				//if (true) 
				//{
				UDI.SDS.ReservationsService.GetReceiptResponse sdsResult = null;
				if (rateQualifier.Trim().ToLower() == "taxi")
				{
					sdsResult = reservationsController.GetSDSFareDetail(trip.taxi_trip.RezId.Value.ToInt32());
				}
				else
				{
                    if (trip != null)
                    {
                        if (alephVendorTripID == null && trip.sds_trip != null)
                        {
                            sdsResult = reservationsController.GetSDSFareDetail(trip.sds_trip.RezID.Value.ToInt32());
                        }
                        else if (trip.ecar_trip != null)
                        {
                            //This is mostly for Aleph, but we cannot get final fare from Aleph
                        }
                    }
				}

				//}
				#region Output Conversion
                if (rateQualifier.Trim().ToLower() == "taxi")
                {
                    using (var db = new DataAccess.VTOD.VTODEntities())
                    {
                        var taxi_trip_status = db.taxi_trip_status.Where(s => s.TripID == trip.Id && s.VehicleLatitude != null && s.VehicleLongitude != null).OrderBy(p => p.Id).ToList();
                        if (taxi_trip_status != null)
                        {
                            if (taxi_trip_status.Any())
                            {
                                result.DropOffLatitude = taxi_trip_status.FirstOrDefault().VehicleLatitude.ToString();
                                result.DropOffLongitude = taxi_trip_status.FirstOrDefault().VehicleLongitude.ToString();
                            }
                        }
                    }
                }
                if (trip != null)
                {
                    if (alephVendorTripID == null && trip.sds_trip != null)
                    {
                        if (trip.sds_trip.DropoffLocationLatitude != null)
                            result.DropOffLatitude = trip.sds_trip.DropoffLocationLatitude.ToString();
                        if (trip.sds_trip.DropoffLocationLongitude != null)
                            result.DropOffLongitude = trip.sds_trip.DropoffLocationLongitude.ToString();
                        if (!string.IsNullOrWhiteSpace(trip.sds_trip.DropoffLocation))
                            result.DropOffLocation = trip.sds_trip.DropoffLocation.ToString();

                    }
                }
				
				PaymentInfo payementInfo = null;
                string status_Time=null;
             

                if (alephBookingResponse != null)
                {
                    Contact item=new Contact();
                    string status = "";
					string originalStatus = "";

                    payementInfo = new PaymentInfo { Type = alephBookingResponse.payment_method };
                    if (alephBookingResponse.reservation_payment != null)
                    {
                        payementInfo.PaymentCard = alephBookingResponse.reservation_payment.payment_info.ToPaymentCard();
                    }
                    

                    try
                    {
                        if (alephBookingResponse.dropoff != null)
                        {
                            result.DropOffLatitude = string.Format("{0}", alephBookingResponse.dropoff.Latitude);
                            result.DropOffLongitude = string.Format("{0}", alephBookingResponse.dropoff.Longitude);
                            result.DropOffLocation = string.Format("{0} {1} {2} {3} {4} {5}", alephBookingResponse.dropoff.street_no, alephBookingResponse.dropoff.street_name, alephBookingResponse.dropoff.city, alephBookingResponse.dropoff.zip_code, alephBookingResponse.dropoff.state, alephBookingResponse.dropoff.country);
                        }

                        if (alephBookingResponse.pickup != null)
                        {
                            result.PickupLatitude = string.Format("{0}", alephBookingResponse.pickup.Latitude);
                            result.PickupLongitude = string.Format("{0}", alephBookingResponse.pickup.Longitude);
                            result.PickupLocation = string.Format("{0} {1} {2} {3} {4} {5}", alephBookingResponse.pickup.street_no, alephBookingResponse.pickup.street_name, alephBookingResponse.pickup.city, alephBookingResponse.pickup.zip_code, alephBookingResponse.pickup.state, alephBookingResponse.pickup.country);
                        }

                        if (alephBookingResponse.requested_datetime != null)
                        {
                            result.PickupTime = alephBookingResponse.requested_datetime.ToString();
                        }


                        result.Contacts = new Contacts();
                        result.Contacts.Items = new List<Contact>();
                        var contact = new Contact { Type = Common.DTO.Enum.ContactType.Dispatch.ToString() };
                        if (!string.IsNullOrWhiteSpace(alephBookingResponse.company_name))
                        {
                            contact.Name = alephBookingResponse.company_name;
                        }
                        if (!string.IsNullOrWhiteSpace(alephBookingResponse.company_phone_number))
                        {
                            contact.Telephone = new Telephone { PhoneNumber = alephBookingResponse.company_phone_number };
                        }
                        result.Contacts.Items.Add(contact);
                        


                        if (alephBookingResponse.assigned_vehicle != null)
                        {
                            var driverContact = new Contact { Type = Common.DTO.Enum.ContactType.Driver.ToString() };

                            if (!string.IsNullOrWhiteSpace(alephBookingResponse.assigned_vehicle.driver_first_name) && !string.IsNullOrWhiteSpace(alephBookingResponse.assigned_vehicle.driver_last_name))
                            {
                                driverContact.Name = alephBookingResponse.assigned_vehicle.driver_first_name + ' ' + alephBookingResponse.assigned_vehicle.driver_last_name;
                            }
                            if (!string.IsNullOrWhiteSpace(alephBookingResponse.assigned_vehicle.driver_phone_number))
                            {
                                driverContact.Telephone = new Telephone { PhoneNumber = alephBookingResponse.assigned_vehicle.driver_phone_number };
                            }
                            
                            result.Contacts.Items.Add(driverContact);
                        }
                        

                        bool hasTime = alephBookingResponse.status_messages.Where(o => !string.IsNullOrWhiteSpace(o.message_datetime)).Any();
                        if (alephBookingResponse.status_messages.Any())
                        {
                            if (hasTime)
                            {
                                originalStatus = alephBookingResponse.status_messages.Where(o => !string.IsNullOrWhiteSpace(o.message_datetime)).OrderByDescending(f => f.message_datetime.ToDateTime().Value).FirstOrDefault().status_message_type;
                                status_Time = alephBookingResponse.status_messages.Where(o => !string.IsNullOrWhiteSpace(o.message_datetime)).OrderByDescending(f => f.message_datetime.ToDateTime().Value).FirstOrDefault().message_datetime;
                            }
                            else
                            {
                                originalStatus = alephBookingResponse.status_messages.LastOrDefault().status_message_type;
                                status_Time = null;
                            }
                            string device = null;
                            string version = null;
                            if (request.TPA_Extensions != null)
                            {
                                if (!string.IsNullOrEmpty(request.TPA_Extensions.Device))
                                {
                                    device = request.TPA_Extensions.Device;
                                }
                                if (!string.IsNullOrEmpty(request.TPA_Extensions.Source))
                                {
                                    version = request.TPA_Extensions.Source;
                                }
                            }
                            status = utility.ConvertTripStatus(originalStatus, device, version);
                        }

                        if (status.ToLower().ToString() == "Completed".ToLower().ToString())
                        {
                            result.TripInfo = new TripInfo();
                            if (string.IsNullOrWhiteSpace(status_Time))
                            {
                                result.TripInfo.Duration = "00:00:00";
                            }

                            else
                            {
                                TimeSpan duration = DateTime.Parse(status_Time).Subtract(alephBookingResponse.requested_datetime);
                                result.TripInfo.Duration = string.Format("{0:00}:{1:00}:{2:00}", duration.Hours, duration.Minutes, duration.Seconds);
                            }
                        }
                        else
                        {
                            result.TripInfo = new TripInfo();
                            result.TripInfo.Duration = "00:00:00";
                        }



                        // FareItems
                        if (alephBookingResponse.price_components != null)
                        {
                            foreach (AlephPriceComponent priceComponent in alephBookingResponse.price_components)
                            {
                                var newItem = new NameValue();
                                newItem.Name = priceComponent.name;
                                newItem.Value = priceComponent.amount.ToString();

                                result.FareItems.Items.Add(newItem);
                            }
                        }
                        

                        if (alephBookingResponse.final_price.HasValue)
                        {
                            var newItem = new NameValue();
                            newItem.Name = "FinalPrice";
                            newItem.Value = alephBookingResponse.final_price.Value.ToString();
                            result.FareItems.Items.Add(newItem);
                        }


                        if (!string.IsNullOrWhiteSpace( alephBookingResponse.final_price_message))
                        {
                            var newItem = new NameValue();
                            newItem.Name = "Message";
                            newItem.Value = alephBookingResponse.final_price_message;
                            result.FareItems.Items.Add(newItem);
                        }

                        if (!string.IsNullOrWhiteSpace(alephBookingResponse.final_price_currency))
                        {
                            var newItem = new NameValue();
                            newItem.Name = "Currency";
                            newItem.Value = alephBookingResponse.final_price_currency;
                            result.FareItems.Items.Add(newItem);
                        }


                       

                    }
                    catch
                    {

                    }                  
                  
                   
                }
               
			 /*Commented as because Ali's Bug*/
				//if (payementInfo == null)
				//{
                //   if (trip != null)
    //                {
    //                    var ws_state = vtodDomain.Get_WS_State("vtod_trip", trip.Id, "CorporateIDs");
    //                    if (!string.IsNullOrWhiteSpace(ws_state))
    //                    {
    //                        try
    //                        {
    //                            List<NameValue> specialInputs = null;

    //                            specialInputs = ws_state.JsonDeserialize<List<NameValue>>();

    //                            if (specialInputs != null)
    //                            {
    //                                var payementInfoPair = specialInputs.Where(s => s.Name.ToLower() == "CorporatePaymentMethod".ToLowerInvariant()).FirstOrDefault();
    //                                if (payementInfoPair != null)
    //                                {
    //                                    payementInfo = new PaymentInfo { Type = payementInfoPair.Value };
    //                                }
    //                            }
    //                        }
    //                        catch
    //                        { }
    //                    }

    //                }
    //                payementInfo = new PaymentInfo { Type = trip.PaymentType };
                  
				//}

				result.PaymentInfo = payementInfo;
				

				try
				{
					CompleteFareDetail(tokenVTOD, result, request, trip);
				}
				catch
				{ }

				result.Success = new Success();

				#region Common
				//result.EchoToken = input.EchoToken;
				//result.PrimaryLangID = input.PrimaryLangID;
				//result.Target = input.Target;
				//result.Version = input.Version;
				#endregion
				#endregion

				if (sdsResult != null && sdsResult.ReceiptItems.Any())
				{
					foreach (var item in sdsResult.ReceiptItems)
					{
						var newItem = new NameValue();
						newItem.Name = item.Description;
						if (item.Description.ToLower().Contains("discount"))
						{
							newItem.Value = (-System.Math.Abs(item.Cost)).ToString();
						}
						else
						{
							newItem.Value = item.Cost.ToString();
						}
						result.FareItems.Items.Add(newItem);
						try
						{
							result.TripInfo = new TripInfo();
							result.TripInfo.Duration = string.Format("{0:00}:{1:00}:{2:00}", sdsResult.TripDuration.Hours, sdsResult.TripDuration.Minutes, sdsResult.TripDuration.Seconds);
						}
						catch
						{ }
					}
				}

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("AccountingDomain.GetFareDetail:: {0}", ex.ToString() + ex.StackTrace);

				//throw new MembershipException(Messages.Membership_CreateMembership);
				throw new AccountingException(ex.Message);
			}
		}

		internal void CompleteFareDetail(TokenRS tokenVTOD, GetFareDetailRS fareDetail, GetFareDetailRQ originalRequest, vtod_trip trip)
		{
			#region Init
			var sdsDomain = new SDS.SDSDomain(TrackTime);			
			var taxiDomain = new Taxi.TaxiDomain(); taxiDomain.TrackTime = TrackTime;
            var vanDomain = new Van.VanDomain(); taxiDomain.TrackTime = TrackTime;
			#endregion

			fareDetail.PickMeUpNow = trip.PickMeUpNow.HasValue ? trip.PickMeUpNow.Value : false;
			fareDetail.PickupTime = trip.PickupDateTime.ToString();
			#region Get Trip Location, lat, long, ..
			var otaStatus = new OTA_GroundResRetrieveRQ();
			otaStatus.Reference = new List<Reference>();
			otaStatus.Reference.Add(originalRequest.Reference.First());
			otaStatus.TPA_Extensions = new TPA_Extensions();
			otaStatus.TPA_Extensions.RateQualifiers = new List<RateQualifier>();
			//var rateQualifier = new RateQualifier { RateQualifierValue = latestUnratedTrip.FleetType };
			otaStatus.TPA_Extensions.RateQualifiers.Add(originalRequest.TPA_Extensions.RateQualifiers.First());

			if (trip.FleetType.ToLower() == Common.DTO.Enum.FleetType.Taxi.ToString().ToLower())
			{

				fareDetail.PickupLatitude = trip.taxi_trip.PickupLatitude.ToString();
				fareDetail.PickupLongitude = trip.taxi_trip.PickupLongitude.ToString();
				fareDetail.PickupLocation = trip.taxi_trip.PickupFullAddress;
				if (!string.IsNullOrWhiteSpace(trip.taxi_trip.DropOffFullAddress))
				{
					fareDetail.DropOffLocation = trip.taxi_trip.DropOffFullAddress;
				}
				if (trip.taxi_trip.DropOffLatitude.HasValue && trip.taxi_trip.DropOffLongitude.HasValue)
				{
					fareDetail.DropOffLatitude = trip.taxi_trip.DropOffLatitude.ToString();
					fareDetail.DropOffLongitude = trip.taxi_trip.DropOffLongitude.ToString();
				}

				#region Get Driver and Dispatch info by calling GetResRetrive
				try
				{
					var newOTAStatusRQ = new OTA_GroundResRetrieveRQ();
					newOTAStatusRQ.TPA_Extensions = new TPA_Extensions();
					newOTAStatusRQ.TPA_Extensions.RateQualifiers = new List<RateQualifier>();
					var newRateQualifier = new RateQualifier { RateQualifierValue = "Taxi" };

					newOTAStatusRQ.Reference = new List<Reference>();
					var newRef = new Reference { ID = trip.Id.ToString(), Type = Common.DTO.Enum.ConfirmationType.Confirmation.ToString() };
					newOTAStatusRQ.Reference.Add(newRef);

					var taxiStatus = taxiDomain.Status(tokenVTOD, newOTAStatusRQ);
					fareDetail.Contacts = taxiStatus.TPA_Extensions.Contacts;
				}
				catch (Exception ex)
				{
					logger.ErrorFormat("There is no status for this trip# {0}", trip.Id);
					logger.Error(ex);
				}
				#endregion

			}
            else if (trip.FleetType.ToLower() == Common.DTO.Enum.FleetType.Van.ToString().ToLower())
            {

                fareDetail.PickupLatitude = trip.van_trip.PickupLatitude.ToString();
                fareDetail.PickupLongitude = trip.van_trip.PickupLongitude.ToString();
                fareDetail.PickupLocation = trip.van_trip.PickupFullAddress;
                if (!string.IsNullOrWhiteSpace(trip.van_trip.DropOffFullAddress))
                {
                    fareDetail.DropOffLocation = trip.van_trip.DropOffFullAddress;
                }
                if (trip.van_trip.DropOffLatitude.HasValue && trip.van_trip.DropOffLongitude.HasValue)
                {
                    fareDetail.DropOffLatitude = trip.van_trip.DropOffLatitude.ToString();
                    fareDetail.DropOffLongitude = trip.van_trip.DropOffLongitude.ToString();
                }

                #region Get Driver and Dispatch info by calling GetResRetrive
                try
                {
                    var newOTAStatusRQ = new OTA_GroundResRetrieveRQ();
                    newOTAStatusRQ.TPA_Extensions = new TPA_Extensions();
                    newOTAStatusRQ.TPA_Extensions.RateQualifiers = new List<RateQualifier>();
                    var newRateQualifier = new RateQualifier { RateQualifierValue = "Van" };

                    newOTAStatusRQ.Reference = new List<Reference>();
                    var newRef = new Reference { ID = trip.Id.ToString(), Type = Common.DTO.Enum.ConfirmationType.Confirmation.ToString() };
                    newOTAStatusRQ.Reference.Add(newRef);

                    var vanStatus = vanDomain.Status(tokenVTOD, newOTAStatusRQ);
                    fareDetail.Contacts = vanStatus.TPA_Extensions.Contacts;
                }
                catch (Exception ex)
                {
                    logger.ErrorFormat("There is no status for this trip# {0}", trip.Id);
                    logger.Error(ex);
                }
                #endregion

            }
			else if (
				trip.FleetType.ToLower() == Common.DTO.Enum.FleetType.ExecuCar.ToString().ToLower()
				||
				trip.FleetType.ToLower() == Common.DTO.Enum.FleetType.SuperShuttle.ToString().ToLower()
				||
				trip.FleetType.ToLower() == Common.DTO.Enum.FleetType.SuperShuttle_ExecuCar.ToString().ToLower()
				||
				trip.FleetType.ToLower() == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly.ToString().ToLower()
				)
			{
				if (trip.sds_trip != null)
				{
					fareDetail.PickupLatitude = trip.sds_trip.PickupLocationLatitude.ToString();
					fareDetail.PickupLongitude = trip.sds_trip.PickupLocationLongitude.ToString();
					fareDetail.PickupLocation = trip.sds_trip.PickupLocation;
					if (!string.IsNullOrWhiteSpace(trip.sds_trip.DropoffLocation))
					{
						fareDetail.DropOffLocation = trip.sds_trip.DropoffLocation;
					}
					if (trip.sds_trip.DropoffLocationLatitude.HasValue && trip.sds_trip.DropoffLocationLongitude.HasValue)
					{
						fareDetail.DropOffLatitude = trip.sds_trip.DropoffLocationLatitude.ToString();
						fareDetail.DropOffLongitude = trip.sds_trip.DropoffLocationLongitude.ToString();
					}

					#region Get Driver and Dispatch info by calling GetResRetrive
					try
					{
						var sdsStatus = sdsDomain.GroundResRetrieve(tokenVTOD, otaStatus);
						fareDetail.Contacts = sdsStatus.TPA_Extensions.Contacts;
					}
					catch (Exception ex)
					{
						logger.ErrorFormat("There is no status for this trip# {0}", trip.Id);
						logger.Error(ex);
					}
					#endregion
				}
				else if (trip.ecar_trip != null)
				{
					fareDetail.PickupLatitude = trip.ecar_trip.PickupLatitude.ToString();
					fareDetail.PickupLongitude = trip.ecar_trip.PickupLongitude.ToString();
					fareDetail.PickupLocation = trip.ecar_trip.PickupFullAddress;
					if (!string.IsNullOrWhiteSpace(trip.ecar_trip.DropOffFullAddress))
					{
						fareDetail.DropOffLocation = trip.ecar_trip.DropOffFullAddress;
					}
					if (trip.ecar_trip.DropOffLatitude.HasValue && trip.ecar_trip.DropOffLongitude.HasValue)
					{
						fareDetail.DropOffLatitude = trip.ecar_trip.DropOffLatitude.ToString();
						fareDetail.DropOffLongitude = trip.ecar_trip.DropOffLongitude.ToString();
					}

					#region Get Driver and Dispatch info by calling GetResRetrive
					try
					{
                        var ecarDomain = new ECar.ECarDomain();
                        var ecarStatus = ecarDomain.Status(tokenVTOD, otaStatus);
						fareDetail.Contacts = ecarStatus.TPA_Extensions.Contacts;
						fareDetail.TripInfo = ecarStatus.TPA_Extensions.TripInfo;
					}
					catch (Exception ex)
					{
						logger.ErrorFormat("There is no status for this trip# {0}", trip.Id);
						logger.Error(ex);
					}
					#endregion
				}


			}
			#endregion

			#region Get CreditCard, DirectBill info
			try
			{
				if (trip.PaymentType == Common.DTO.Enum.PaymentType.PaymentCard.ToString())
				{
					var rq = new AccountingGetMemberPaymentCardsRQ { MemberId = trip.MemberID.ToInt32() };
					var memberPaymentCards = AccountingGetMemberPaymentCards(tokenVTOD, rq);
					if (memberPaymentCards != null && memberPaymentCards.PaymentCards != null && memberPaymentCards.PaymentCards.Any())
					{
						foreach (var item in memberPaymentCards.PaymentCards)
						{
							if (item.CardNumber.ID == trip.CreditCardID)
							{
								fareDetail.PaymentInfo.PaymentCard = item;
							}
						}
					}
				}
				if (trip.PaymentType == Common.DTO.Enum.PaymentType.DirectBill.ToString())
				{
					var rq = new AccountingGetMemberDirectBillsRQ { MemberId = trip.MemberID.ToInt32() };
					var memberDirectbills = AccountingGetMemberDirectBills(tokenVTOD, rq);
					if (memberDirectbills != null && memberDirectbills.DirectBills != null && memberDirectbills.DirectBills.Any())
					{
						foreach (var item in memberDirectbills.DirectBills)
						{
							if (item.ID == trip.DirectBillAccountID)
							{
								fareDetail.PaymentInfo.DirectBill = item;
							}
						}
					}

				}
			}
			catch (Exception ex)
			{
				logger.Error(ex);
			}

			#endregion
		}
		#endregion

		#region ChargeVtodTrip
		internal UDI.SDS.ReservationsServiceSecure.ChargeVtodTripResponse ChargeVtodTrip(TokenRS token, long tripID, long SdsFleetMerchantID, int memberID, int creditCardID, decimal fare, decimal gratuity)
		{
			var reservationController = new ReservationsController(token, TrackTime);
			var request = new UDI.SDS.ReservationsServiceSecure.ChargeVtodTripRequest();
			request.fare = fare;
			request.fleetID = SdsFleetMerchantID;
			request.gratuity = gratuity;
			request.memberId = memberID;
			request.vtodTripId = tripID;
			request.creditCardAccountId = creditCardID;
			var result = reservationController.ChargeVtodTrip(request);
			return result;
		}

		#endregion

      
		#endregion

		#region Properties
		public TrackTime TrackTime { get; set; }
		#endregion

	}
}
