﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.ECar.Aleph
{
	public class AirportPickup
	{
		public string airport_pickup_value { get; set; }
		public string airport_pickup_description { get; set; }
	}
}
