﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.IO;
using System.Diagnostics;
using System.Configuration;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Runtime.CompilerServices;
using UDI.Utility.Helper;
using UDI.Utility.Serialization;
using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.DTO;

namespace UDI.VTOD.Domain.ECar.Aleph
{
	public class AlephAPICall : BaseSetting
	{
		public AlephAPICall()
		{
			OwnerType = LogOwnerType.Aleph;
		}

		#region Reservation

		public AlephBookingRS BookReservation(AlephBookingParameter parameter,string password, string content)
		{
			Dictionary<string, string> requestHeader = new Dictionary<string, string>();
			Dictionary<string, string> responseHeader = new Dictionary<string, string>();
			string methodName = GetMethodName();
			string status = "";

			int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["AlephAPI_Https_Timeout"]);

			string queryUrl = ConfigurationManager.AppSettings["AlephAPI_Domain"] + "/reservation/" + parameter.CorporateProviderID + "/" 
                               + parameter.CorporateCompanyName + "/" + parameter.CorporateProviderAccountID + "/" + parameter.CorporateProfileID;

			#region Add Headers
			requestHeader.Add("Accept", "application/json");
			requestHeader.Add("Api-Key", ConfigurationManager.AppSettings["AlephAPI_Key"]);
			requestHeader.Add("Content-Type", "application/json");
			requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(parameter.CorporateEmail, password));
			requestHeader.Add("Content-Length", content.Length.ToString());
			#endregion Add Headers


			string bookingResult = "";
			AlephBookingRS alephBookingResponse = null;
			try
			{
				LogRequest(methodName, content);
				bookingResult = ProcessWebRequestEx(queryUrl, content, "application/json", "POST", string.Empty, string.Empty, timeout, requestHeader, out status, out responseHeader);
				alephBookingResponse = bookingResult.JsonDeserialize<AlephBookingRS>();
				LogResponse(methodName, bookingResult);
			}
			catch (Exception ex)
			{
				int vtodErrorCode = 0;
				 string errorMessage = "Street number is invalid";
                if (ex.Message.Contains(errorMessage))
                {
                    vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
                    LogException(methodName, vtodErrorCode.ToString(), ex);
                    throw VtodException.CreateException(ExceptionType.ECar, 20040);
                }
                else
				{
				vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
				LogException(methodName, vtodErrorCode.ToString(), ex);
				throw new Exception(vtodErrorCode.ToString(), ex);
				}
			}


			return alephBookingResponse;
		}
		public AlephBookingRS ModifyBookReservation(string providerId, string companyName, string providerAccountId, string profileId, string userName, string password, string content, string vendor_confirmation_no)
		{
			Dictionary<string, string> requestHeader = new Dictionary<string, string>();
			Dictionary<string, string> responseHeader = new Dictionary<string, string>();
			string methodName = GetMethodName();
			string status = "";

			int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["AlephAPI_Https_Timeout"]);

			string queryUrl = ConfigurationManager.AppSettings["AlephAPI_Domain"] + "/reservation/" + providerId + "/" + companyName + "/" + providerAccountId + "/" + profileId + "/" + vendor_confirmation_no;

			#region Add Headers
			requestHeader.Add("Accept", "application/json");
			requestHeader.Add("Api-Key", ConfigurationManager.AppSettings["AlephAPI_Key"]);
			requestHeader.Add("Content-Type", "application/json");
			requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, password));
			requestHeader.Add("Content-Length", content.Length.ToString());
			#endregion Add Headers


			string bookingResult = "";
			AlephBookingRS alephBookingResponse = null;
			try
			{
				LogRequest(methodName, content);
				bookingResult = ProcessWebRequestEx(queryUrl, content, "application/json", "PUT", string.Empty, string.Empty, timeout, requestHeader, out status, out responseHeader);
				alephBookingResponse = bookingResult.JsonDeserialize<AlephBookingRS>();
				LogResponse(methodName, bookingResult);
			}
			catch (Exception ex)
			{
				int vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
				LogException(methodName, vtodErrorCode.ToString(), ex);
				throw new Exception(vtodErrorCode.ToString(), ex);
			}


			return alephBookingResponse;
		}
		public AlephBookingRS ModifyBookReservationNow(string providerId, string companyName, string providerAccountId, string profileId, string userName, string password, string content, string vendor_confirmation_no)
		{
			Dictionary<string, string> requestHeader = new Dictionary<string, string>();
			Dictionary<string, string> responseHeader = new Dictionary<string, string>();
			string methodName = GetMethodName();
			string status = "";

			int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["AlephAPI_Https_Timeout"]);

			string queryUrl = ConfigurationManager.AppSettings["AlephAPI_Domain"] + "/reservation/" + providerId + "/" + companyName + "/" + providerAccountId + "/" + profileId + "/" + vendor_confirmation_no;

			#region Add Headers
			requestHeader.Add("Accept", "application/json");
			requestHeader.Add("Api-Key", ConfigurationManager.AppSettings["AlephAPI_Key"]);
			requestHeader.Add("Content-Type", "application/json");
			requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, password));
			requestHeader.Add("Content-Length", content.Length.ToString());
			#endregion Add Headers


			string bookingResult = "";
			AlephBookingRS alephBookingResponse = null;
			try
			{
				LogRequest(methodName, content);
				bookingResult = ProcessWebRequestEx(queryUrl, content, "application/json", "PUT", string.Empty, string.Empty, timeout, requestHeader, out status, out responseHeader);
				alephBookingResponse = bookingResult.JsonDeserialize<AlephBookingRS>();
				LogResponse(methodName, bookingResult);
			}
			catch (Exception ex)
			{
				int vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
				LogException(methodName, vtodErrorCode.ToString(), ex);
				throw new Exception(vtodErrorCode.ToString(), ex);
			}


			return alephBookingResponse;
		}
		public AlephBookingRS BookReservationNow(AlephBookingParameter parameter, string password, string content, string corporateAccountName, string representedUserName, string strategy)
		{
			Dictionary<string, string> requestHeader = new Dictionary<string, string>();
			Dictionary<string, string> responseHeader = new Dictionary<string, string>();
			string methodName = GetMethodName();
			string status = "";

			int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["AlephAPI_Https_Timeout"]);

            string queryUrl = ConfigurationManager.AppSettings["AlephAPI_Domain"] + "/corporate/reservation/now/"
                            + parameter.CorporatePickupLatitude.ToString() + "/" + parameter.CorporatePickupLongitude.ToString();

            if (!string.IsNullOrWhiteSpace(corporateAccountName))
			{
				queryUrl = queryUrl + "/" + corporateAccountName;
			}

			if (!string.IsNullOrWhiteSpace(representedUserName))
			{
				queryUrl = queryUrl + "/" + representedUserName;
			}

			if (!string.IsNullOrWhiteSpace(strategy))
			{
				queryUrl = queryUrl + "/" + strategy;
			}

			//Add corporateName to the request if it exists
			if (!string.IsNullOrWhiteSpace(parameter.CorporateName))
			{
				queryUrl = queryUrl + "/" + parameter.CorporateName;

				//Add accountName to the request if it exists
				if (!string.IsNullOrWhiteSpace(parameter.CorporateAccountName))
				{
					queryUrl = queryUrl + "/" + parameter.CorporateAccountName;
				}
			}
			#region Add Headers
			requestHeader.Add("Accept", "application/json");
			requestHeader.Add("Api-Key", ConfigurationManager.AppSettings["AlephAPI_Key"]);
			requestHeader.Add("Content-Type", "application/json");
			requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(parameter.CorporateEmail, password));
			requestHeader.Add("Content-Length", content.Length.ToString());
			#endregion Add Headers


			string bookingResult = "";
			AlephBookingRS alephBookingResponse = null;
			try
			{
				LogRequest(methodName, content);
				bookingResult = ProcessWebRequestEx(queryUrl, content, "application/json", "POST", string.Empty, string.Empty, timeout, requestHeader, out status, out responseHeader);
				alephBookingResponse = bookingResult.JsonDeserialize<AlephBookingRS>();
				LogResponse(methodName, bookingResult);
			}
			catch (Exception ex)
			{
				int vtodErrorCode = 0;
                string errorMessage = "Street number is invalid";
                if (ex.Message.Contains(errorMessage))
                {
                    vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
                    LogException(methodName, vtodErrorCode.ToString(), ex);
                    throw VtodException.CreateException(ExceptionType.ECar, 20040);
                }
                else
				{
				vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
				LogException(methodName, vtodErrorCode.ToString(), ex);
				throw new Exception(vtodErrorCode.ToString(), ex);
				}
			}


			return alephBookingResponse;
		}


		public AlephBookingRS GetReservation(string providerId, string companyName, string providerAccountId, string profileId, string userName, string password, string reservationId)
		{
			Dictionary<string, string> requestHeader = new Dictionary<string, string>();
			Dictionary<string, string> responseHeader = new Dictionary<string, string>();
			string methodName = GetMethodName();
			string status = "";

			int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["AlephAPI_Https_Timeout"]);

			string queryUrl = ConfigurationManager.AppSettings["AlephAPI_Domain"] + "/reservation/" + providerId + "/" + companyName + "/" + providerAccountId + "/" + profileId + "/" + reservationId;

			#region Add Headers
			requestHeader.Add("Accept", "application/json");
			requestHeader.Add("Api-Key", ConfigurationManager.AppSettings["AlephAPI_Key"]);
			requestHeader.Add("Content-Type", "application/json");
			requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, password));
			#endregion Add Headers


			string bookingResult = "";
			AlephBookingRS alephBookingResponse = null;

			try
			{
				bookingResult = ProcessWebRequestEx(queryUrl, "", "application/json", "GET", string.Empty, string.Empty, timeout, requestHeader, out status, out responseHeader);
				alephBookingResponse = bookingResult.JsonDeserialize<AlephBookingRS>();
				LogResponse(methodName, bookingResult);
			}
			catch (Exception ex)
			{
				int vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
				LogException(methodName, vtodErrorCode.ToString(), ex);
				throw new Exception(vtodErrorCode.ToString(), ex);
			}


			return alephBookingResponse;
		}

		public List<AlephReservationRS> GetAllReservation(string userName, string alephPassword, int numberoftrips)
		{
			logger.InfoFormat("userName: {0}", userName);
			logger.InfoFormat("alephPassword: {0}", alephPassword);
			logger.InfoFormat("numberoftrips: {0}", numberoftrips);

			#region Init

			Dictionary<string, string> requestHeader = new Dictionary<string, string>();
			Dictionary<string, string> responseHeader = new Dictionary<string, string>();
			string HttpStatus = "";
			string content = "";
			string reservationResult = "";
			string methodName = GetMethodName();
			#endregion
			#region Add Headers
			int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["AlephAPI_Https_Timeout"]);
			logger.InfoFormat("timeout: {0}", timeout);
			logger.InfoFormat("AlephAPI_Domain: {0}", ConfigurationManager.AppSettings["AlephAPI_Domain"]);

			string queryUrl = ConfigurationManager.AppSettings["AlephAPI_Domain"] + "/corporate/reservation?count=" + numberoftrips;
			logger.InfoFormat("queryUrl: {0}", queryUrl);

			requestHeader.Add("Accept", "application/json");
			requestHeader.Add("Api-Key", ConfigurationManager.AppSettings["AlephAPI_Key"]);
			logger.InfoFormat("AlephAPI_Key: {0}", ConfigurationManager.AppSettings["AlephAPI_Key"]);

			requestHeader.Add("Content-Type", "application/json");
			requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, alephPassword));
			logger.InfoFormat("Base64EncodeForBasicAuthentication: {0}", Base64EncodeForBasicAuthentication(userName, alephPassword));

			#endregion
			try
			{
				#region Aleph Call
				LogRequest(methodName, content);
				logger.InfoFormat("Method: {0}, URL: {1]", methodName, queryUrl);
				reservationResult = ProcessWebRequestEx(queryUrl, content, "application/json", "GET", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out responseHeader);
				List<UDI.VTOD.Domain.ECar.Aleph.AlephReservationRS> alephBookingResponse = reservationResult.JsonDeserialize<List<UDI.VTOD.Domain.ECar.Aleph.AlephReservationRS>>();
				LogResponse(methodName, reservationResult);
				return alephBookingResponse;
				#endregion
			}
			catch (Exception ex)
			{
				#region Exception
				int vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
				LogException(methodName, vtodErrorCode.ToString(), ex);
				throw new Exception(vtodErrorCode.ToString(), ex);
				#endregion
			}


		}

		public AlephBookingRS ChangeReservation(string providerId, string companyName, string providerAccountId, string profileId, string userName, string password, string content, string reservationId)
		{
			Dictionary<string, string> requestHeader = new Dictionary<string, string>();
			Dictionary<string, string> responseHeader = new Dictionary<string, string>();
			string methodName = GetMethodName();
			string status = "";

			int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["AlephAPI_Https_Timeout"]);

			string queryUrl = ConfigurationManager.AppSettings["AlephAPI_Domain"] + "/reservation/" + providerId + "/" + companyName + "/" + providerAccountId + "/" + profileId + "/" + reservationId;

			#region Add Headers
			requestHeader.Add("Accept", "application/json");
			requestHeader.Add("Api-Key", ConfigurationManager.AppSettings["AlephAPI_Key"]);
			requestHeader.Add("Content-Type", "application/json");
			requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, password));
			requestHeader.Add("Content-Length", content.Length.ToString());
			#endregion Add Headers


			string bookingResult = "";
			AlephBookingRS alephBookingResponse = null;
			try
			{
				LogRequest(methodName, content);
				bookingResult = ProcessWebRequestEx(queryUrl, content, "application/json", "PUT", string.Empty, string.Empty, timeout, requestHeader, out status, out responseHeader);
				alephBookingResponse = bookingResult.JsonDeserialize<AlephBookingRS>();
				LogResponse(methodName, bookingResult);
			}
			catch (Exception ex)
			{
				int vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
				LogException(methodName, vtodErrorCode.ToString(), ex);
				throw new Exception(vtodErrorCode.ToString(), ex);
			}


			return alephBookingResponse;
		}

       

        public AlephBookingRS CancelReservation(string providerId, string companyName, string providerAccountId, string profileId, string userName, string password, string reservationId)
		{
			Dictionary<string, string> requestHeader = new Dictionary<string, string>();
			Dictionary<string, string> responseHeader = new Dictionary<string, string>();
			string methodName = GetMethodName();
			string status = "";

			int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["AlephAPI_Https_Timeout"]);

			string queryUrl = ConfigurationManager.AppSettings["AlephAPI_Domain"] + "/reservation/" + providerId + "/" + companyName + "/" + providerAccountId + "/" + profileId + "/" + reservationId;


			#region Add Headers
			requestHeader.Add("Accept", "application/json");
			requestHeader.Add("Api-Key", ConfigurationManager.AppSettings["AlephAPI_Key"]);
			requestHeader.Add("Content-Type", "application/json");
			requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, password));
			#endregion Add Headers


			string bookingResult = "";
			AlephBookingRS alephBookingResponse = null;
			try
			{
				bookingResult = ProcessWebRequestEx(queryUrl, "", "application/json", "DELETE", string.Empty, string.Empty, timeout, requestHeader, out status, out responseHeader);
				alephBookingResponse = bookingResult.JsonDeserialize<AlephBookingRS>();
				LogResponse(methodName, bookingResult);
			}
			catch (Exception ex)
			{
				int vtodErrorCode = 0;
				if (ex.Message.Contains("dispatched or assigned"))
				{
					vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
					LogException(methodName, vtodErrorCode.ToString(), ex);
					throw VtodException.CreateException(ExceptionType.ECar, 20037);
				}
				else
				{
					vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
					LogException(methodName, vtodErrorCode.ToString(), ex);
					throw new Exception(vtodErrorCode.ToString(), ex);
				}

			}


			return alephBookingResponse;
		}


		public AlephGetBookingCriteriaRS GetBookingCriteria(string providerId, string companyName, string providerAccountId, string profileId, string userName, string password)
		{
			Dictionary<string, string> requestHeader = new Dictionary<string, string>();
			Dictionary<string, string> responseHeader = new Dictionary<string, string>();
			string methodName = GetMethodName();
			string status = "";

			int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["AlephAPI_Https_Timeout"]);

			string queryUrl = ConfigurationManager.AppSettings["AlephAPI_Domain"] + "/accountuserbookingcriterion/" + providerId + "/" + companyName + "/" + providerAccountId + "/" + profileId;


			#region Add Headers
			requestHeader.Add("Accept", "application/json");
			requestHeader.Add("Api-Key", ConfigurationManager.AppSettings["AlephAPI_Key"]);
			requestHeader.Add("Content-Type", "application/json");
			requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, password));
			#endregion Add Headers


			string getBookingCriteriaResult = "";
			AlephGetBookingCriteriaRS getBookingCriteriaResponse = new AlephGetBookingCriteriaRS();

			try
			{
				getBookingCriteriaResult = ProcessWebRequestEx(queryUrl, "", "application/json", "GET", string.Empty, string.Empty, timeout, requestHeader, out status, out responseHeader);
				getBookingCriteriaResponse.booking_criteria = getBookingCriteriaResult.JsonDeserialize<List<AlephBookingCriteria>>();
				LogResponse(methodName, getBookingCriteriaResult);
			}
			catch (Exception ex)
			{
				int vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
				LogException(methodName, vtodErrorCode.ToString(), ex);
				throw new Exception(vtodErrorCode.ToString(), ex);
			}


			return getBookingCriteriaResponse;
		}


		public AlephGetBookingCriteriaRS GetBookingCriteriaNow(string corporateName, string accountName, string userName, string password)
		{
			Dictionary<string, string> requestHeader = new Dictionary<string, string>();
			Dictionary<string, string> responseHeader = new Dictionary<string, string>();
			string methodName = GetMethodName();
			string status = "";

			int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["AlephAPI_Https_Timeout"]);

			string queryUrl = ConfigurationManager.AppSettings["AlephAPI_Domain"] + "/corporate/bookingcriteria";

			if (!string.IsNullOrWhiteSpace(corporateName) && !string.IsNullOrWhiteSpace(accountName))
			{
				queryUrl = queryUrl + "/" + corporateName + "/" + accountName;
			}

			#region Add Headers
			requestHeader.Add("Accept", "application/json");
			requestHeader.Add("Api-Key", ConfigurationManager.AppSettings["AlephAPI_Key"]);
			requestHeader.Add("Content-Type", "application/json");
			requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, password));
			#endregion Add Headers


			string getBookingCriteriaResult = "";
			AlephGetBookingCriteriaRS getBookingCriteriaResponse = new AlephGetBookingCriteriaRS();

			try
			{
				getBookingCriteriaResult = ProcessWebRequestEx(queryUrl, "", "application/json", "GET", string.Empty, string.Empty, timeout, requestHeader, out status, out responseHeader);
				getBookingCriteriaResponse.booking_criteria = getBookingCriteriaResult.JsonDeserialize<List<AlephBookingCriteria>>();
				LogResponse(methodName, getBookingCriteriaResult);
			}
			catch (Exception ex)
			{
				int vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
				LogException(methodName, vtodErrorCode.ToString(), ex);
				throw new Exception(vtodErrorCode.ToString(), ex);
			}


			return getBookingCriteriaResponse;
		}


		public AlephGetCorporateRequirementsRS GetCorporateRequirements(string corporateName, string accountName, string requirementName, string userName, string password)
		{
			var requestHeader = new Dictionary<string, string>();
			var methodName = GetMethodName();

			var timeout = Convert.ToInt32(ConfigurationManager.AppSettings["AlephAPI_Https_Timeout"]);

			var queryUrl = string.Format("{0}/corporate/requirements", ConfigurationManager.AppSettings["AlephAPI_Domain"]);

			//Add corporateName to the request if it exists
			if (!string.IsNullOrWhiteSpace(corporateName))
			{
				queryUrl = string.Format("{0}/{1}", queryUrl, corporateName);

				//Add accountName to the request if it exists
				if (!string.IsNullOrWhiteSpace(accountName))
				{
					queryUrl = string.Format("{0}/{1}", queryUrl, accountName);

					//Add criteriaName to the request if accountName exists and criteriaName exists
					if (!string.IsNullOrWhiteSpace(requirementName))
					{
						queryUrl = string.Format("{0}/{1}", queryUrl, requirementName);
					}
				}
			}

			#region Add Headers
			requestHeader.Add("Accept", "application/json");
			requestHeader.Add("Api-Key", ConfigurationManager.AppSettings["AlephAPI_Key"]);
			requestHeader.Add("Content-Type", "application/json");
			requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, password));
			#endregion Add Headers

			List<AlephCorporationRequirements> getCorporateRequirementsResponse;

			try
			{
				string status;
				Dictionary<string, string> responseHeader;
				var getCorporateRequirementsResult = ProcessWebRequestEx(queryUrl, "", "application/json", "GET", string.Empty, string.Empty, timeout, requestHeader, out status, out responseHeader);
				getCorporateRequirementsResponse = getCorporateRequirementsResult.JsonDeserialize<List<AlephCorporationRequirements>>();
				LogResponse(methodName, getCorporateRequirementsResult);
			}
			catch (Exception ex)
			{
				int vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
				LogException(methodName, vtodErrorCode.ToString(), ex);
				throw new Exception(vtodErrorCode.ToString(), ex);
			}

			return new AlephGetCorporateRequirementsRS
			{
				CorporationRequirements = getCorporateRequirementsResponse
			};
		}
		public List<string> GetCorporateRequirementsTypeAhead(string corporateCompanyName, string accountName, string requirementName, string letters, string userName, string password)
		{
			var requestHeader = new Dictionary<string, string>();
			var methodName = GetMethodName();

			var rows = ConfigurationManager.AppSettings["AlephAPI_TypeAhead_Rows"];
			var queryUrl = string.Format("{0}/corporate/requirements/typeahead/{1}/{2}/{3}/{4}/{5}",
				ConfigurationManager.AppSettings["AlephAPI_Domain"], corporateCompanyName, accountName,
				requirementName, rows, letters);

			#region Add Headers
			requestHeader.Add("Accept", "application/json");
			requestHeader.Add("Api-Key", ConfigurationManager.AppSettings["AlephAPI_Key"]);
			requestHeader.Add("Content-Type", "application/json");
			requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, password));
			#endregion Add Headers

			List<string> getTypeAheadResponse;

			try
			{
				string status;
				Dictionary<string, string> responseHeader;

				var timeout = Convert.ToInt32(ConfigurationManager.AppSettings["AlephAPI_Https_Timeout"]);

				var getTypeAheadResult = ProcessWebRequestEx(queryUrl, "", "application/json", "GET", string.Empty, string.Empty, timeout, requestHeader, out status, out responseHeader);
				getTypeAheadResponse = getTypeAheadResult.JsonDeserialize<List<string>>();
				LogResponse(methodName, getTypeAheadResult);
			}
			catch (Exception ex)
			{
				int vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
				LogException(methodName, vtodErrorCode.ToString(), ex);
				throw new Exception(vtodErrorCode.ToString(), ex);
			}

			return getTypeAheadResponse;
		}

		public AlephGetPaymentMethodRS GetCorporatePaymentMethods(string corporateCompanyName, string accountName, string userName, string password)
		{
			var requestHeader = new Dictionary<string, string>();
			var methodName = GetMethodName();

			var queryUrl = string.Format("{0}/corporate/paymentmethod", ConfigurationManager.AppSettings["AlephAPI_Domain"]);

			//Add corporateName to the request if it exists
			if (!string.IsNullOrWhiteSpace(corporateCompanyName))
			{
				queryUrl = string.Format("{0}/{1}", queryUrl, corporateCompanyName);

				//Add accountName to the request if it exists
				if (!string.IsNullOrWhiteSpace(accountName))
				{
					queryUrl = string.Format("{0}/{1}", queryUrl, accountName);
				}
			}

			#region Add Headers
			requestHeader.Add("Accept", "application/json");
			requestHeader.Add("Api-Key", ConfigurationManager.AppSettings["AlephAPI_Key"]);
			requestHeader.Add("Content-Type", "application/json");
			requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, password));
			#endregion Add Headers

			List<AlephCorporationPaymentMethods> getCorporatePaymentMethodsResponse;

			try
			{
				string status;
				Dictionary<string, string> responseHeader;

				var timeout = Convert.ToInt32(ConfigurationManager.AppSettings["AlephAPI_Https_Timeout"]);

				var getCorporatePaymentMethodsResult = ProcessWebRequestEx(queryUrl, "", "application/json", "GET", string.Empty, string.Empty, timeout, requestHeader, out status, out responseHeader);
				getCorporatePaymentMethodsResponse = getCorporatePaymentMethodsResult.JsonDeserialize<List<AlephCorporationPaymentMethods>>();
				LogResponse(methodName, getCorporatePaymentMethodsResult);
			}
			catch (Exception ex)
			{
				int vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
				LogException(methodName, vtodErrorCode.ToString(), ex);
				throw new Exception(vtodErrorCode.ToString(), ex);
			}

			return new AlephGetPaymentMethodRS
			{
				CorporationPaymentMethods = getCorporatePaymentMethodsResponse
			};
		}

        internal List<AlephCoporateCreditCardInfo> GetCorporatePaymentCards(string corporateProfileID, string corporateEmail, string corporateKey)
        {
            var requestHeader = new Dictionary<string, string>();
            var methodName = GetMethodName();

            var queryUrl = string.Format("{0}/userpaymentinfo", ConfigurationManager.AppSettings["AlephAPI_Domain"]);
            AlephCorporatePaymentCardRQ alephCorporatePaymentCardRequest = new AlephCorporatePaymentCardRQ();
            //Add Profile Id to the request if it exists
            if (!string.IsNullOrWhiteSpace(corporateProfileID))
            {
                queryUrl = string.Format("{0}/{1}", queryUrl, corporateProfileID);

                //Add UserName to the request if it exists
                if (!string.IsNullOrWhiteSpace(corporateEmail))
                {
                    queryUrl = string.Format("{0}/{1}", queryUrl, corporateEmail);
                }
            }

            #region Add Headers
            requestHeader.Add("Accept", "application/json");
            requestHeader.Add("Api-Key", ConfigurationManager.AppSettings["AlephAPI_Key"]);
            requestHeader.Add("Content-Type", "application/json");
            requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(corporateEmail, corporateKey));
            #endregion Add Headers

            List<AlephCoporateCreditCardInfo> alephCoporateCreditCardInfo;

            try
            {
                string status;
                Dictionary<string, string> responseHeader;

                var timeout = Convert.ToInt32(ConfigurationManager.AppSettings["AlephAPI_Https_Timeout"]);

                var getCorporatePaymentCardsResult = ProcessWebRequestEx(queryUrl, "", "application/json", "GET", string.Empty, string.Empty, timeout, requestHeader, out status, out responseHeader);
                alephCoporateCreditCardInfo = getCorporatePaymentCardsResult.JsonDeserialize<List<AlephCoporateCreditCardInfo>>();
                LogResponse(methodName, getCorporatePaymentCardsResult);
            }
            catch (Exception ex)
            {
                int vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
                LogException(methodName, vtodErrorCode.ToString(), ex);
                throw new Exception(vtodErrorCode.ToString(), ex);
            }
            return alephCoporateCreditCardInfo;
        }

        public AccountingAddMemberPaymentCardRS AddCorporatePaymentCard(AlephPaymentCard alephPaymentCard, string password,string nickName)
        {
            var requestHeader = new Dictionary<string, string>();
            var methodName = GetMethodName();
        
            var queryUrl = string.Format("{0}/userpaymentinfo", ConfigurationManager.AppSettings["AlephAPI_Domain"]);
            AlephCorporatePaymentCardRQ alephCorporatePaymentCardRequest = new AlephCorporatePaymentCardRQ();
            //Add Profile Id to the request if it exists
            if (!string.IsNullOrWhiteSpace(alephPaymentCard.CorporateProfileID))
            {
                queryUrl = string.Format("{0}/{1}", queryUrl, alephPaymentCard.CorporateProfileID);

                //Add UserName(email) to the request if it exists
                if (!string.IsNullOrWhiteSpace(alephPaymentCard.CorporateEmail))
                {
                    queryUrl = string.Format("{0}/{1}", queryUrl, alephPaymentCard.CorporateEmail);
                }
            }
            #region Add Content
            alephCorporatePaymentCardRequest.is_primary = alephPaymentCard.IsPrimary;
            alephCorporatePaymentCardRequest.payment_method_nonce = alephPaymentCard.CorporatePaymentNonce;
            alephCorporatePaymentCardRequest.nick_name = nickName;
            #endregion

            //Serialize content class
            string content = alephCorporatePaymentCardRequest.JsonSerialize();
            #region Add Headers
            requestHeader.Add("Accept", "application/json");
            requestHeader.Add("Api-Key", ConfigurationManager.AppSettings["AlephAPI_Key"]);
            requestHeader.Add("Content-Type", "application/json");
            requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(alephPaymentCard.CorporateEmail, password));
            #endregion Add Headers

            AccountingAddMemberPaymentCardRS addCorporatePaymentCardResponse;

            try
            {
                string status;
                Dictionary<string, string> responseHeader;

                var timeout = Convert.ToInt32(ConfigurationManager.AppSettings["AlephAPI_Https_Timeout"]);

                var addCorporatePaymentCardResult = ProcessWebRequestEx(queryUrl, content, "application/json", "POST", string.Empty, string.Empty, timeout, requestHeader, out status, out responseHeader);
                addCorporatePaymentCardResponse = addCorporatePaymentCardResult.JsonDeserialize<AccountingAddMemberPaymentCardRS>();
                LogResponse(methodName, addCorporatePaymentCardResult);
            }
            catch (Exception ex)
            {
                int vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
                LogException(methodName, vtodErrorCode.ToString(), ex);
                throw new Exception(vtodErrorCode.ToString(), ex);
            }
            return addCorporatePaymentCardResponse;
        }


        public AccountingDeleteMemberPaymentCardRS DeleteCorporatePaymentCard(AlephPaymentCard alephPaymentCard,string password)
        {
            var requestHeader = new Dictionary<string, string>();
            var methodName = GetMethodName();

            var queryUrl = string.Format("{0}/userpaymentinfo", ConfigurationManager.AppSettings["AlephAPI_Domain"]);
            AlephCorporatePaymentCardRQ alephCorporatePaymentCardRequest = new AlephCorporatePaymentCardRQ();
            //Add Profile Id to the request if it exists
            if (!string.IsNullOrWhiteSpace(alephPaymentCard.CorporateProfileID))
            {
                queryUrl = string.Format("{0}/{1}", queryUrl, alephPaymentCard.CorporateProfileID);
                                
                if (!string.IsNullOrWhiteSpace(alephPaymentCard.CorporateEmail))
                {
                    queryUrl = string.Format("{0}/{1}/{2}", queryUrl, alephPaymentCard.CorporateEmail, alephPaymentCard.CorporatePaymentMethodToken);
                }
            }
         
            #region Add Headers
            requestHeader.Add("Accept", "application/json");
            requestHeader.Add("Api-Key", ConfigurationManager.AppSettings["AlephAPI_Key"]);
            requestHeader.Add("Content-Type", "application/json");
            requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(alephPaymentCard.CorporateEmail, password));
            #endregion Add Headers

            AccountingDeleteMemberPaymentCardRS deleteCorporatePaymentCardResponse;

            try
            {
                string status;
                Dictionary<string, string> responseHeader;

                var timeout = Convert.ToInt32(ConfigurationManager.AppSettings["AlephAPI_Https_Timeout"]);

                var deleteCorporatePaymentCardResult = ProcessWebRequestEx(queryUrl, "", "application/json", "DELETE", string.Empty, string.Empty, timeout, requestHeader, out status, out responseHeader);
                deleteCorporatePaymentCardResponse = deleteCorporatePaymentCardResult.JsonDeserialize<AccountingDeleteMemberPaymentCardRS>();
                LogResponse(methodName, deleteCorporatePaymentCardResult);
            }
            catch (Exception ex)
            {
                int vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
                LogException(methodName, vtodErrorCode.ToString(), ex);
                throw new Exception(vtodErrorCode.ToString(), ex);
            }
            return deleteCorporatePaymentCardResponse;
        }

        public List<AirportPickUp> GetAirportPickUpOptions(string userName, string password)
		{
			var requestHeader = new Dictionary<string, string>();
			var methodName = GetMethodName();

			int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["AlephAPI_Https_Timeout"]);

			string queryUrl = ConfigurationManager.AppSettings["AlephAPI_Domain"] + "/airport/pickuppoint";

			#region Add Headers
			requestHeader.Add("Accept", "application/json");
			requestHeader.Add("Api-Key", ConfigurationManager.AppSettings["AlephAPI_Key"]);
			requestHeader.Add("Content-Type", "application/json");
			requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, password));
			#endregion Add Headers


			List<AirportPickUp> getAirportPickUpResponse;
			try
			{
				string status;
				Dictionary<string, string> responseHeader;

				var getAirportPickUpOptionsResult = ProcessWebRequestEx(queryUrl, "", "application/json", "GET", string.Empty, string.Empty, timeout, requestHeader, out status, out responseHeader);
				getAirportPickUpResponse = getAirportPickUpOptionsResult.JsonDeserialize<List<AirportPickUp>>();
				LogResponse(methodName, getAirportPickUpOptionsResult);
			}
			catch (Exception ex)
			{
				int vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
				LogException(methodName, vtodErrorCode.ToString(), ex);
				throw new Exception(vtodErrorCode.ToString(), ex);
			}

			return getAirportPickUpResponse;
		}

		#endregion Reservation


		#region Vehicle

		public AlephVehicleLocationRS GetVehicleLocation(string providerId, string companyName, string providerAccountId, string profileId, string userName, string password, string reservationId)
		{
			Dictionary<string, string> requestHeader = new Dictionary<string, string>();
			Dictionary<string, string> responseHeader = new Dictionary<string, string>();
			string methodName = GetMethodName();
			string status = "";

			int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["AlephAPI_Https_Timeout"]);

			string queryUrl = ConfigurationManager.AppSettings["AlephAPI_Domain"] + "/gps/" + providerId + "/" + companyName + "/" + providerAccountId + "/" + profileId + "/" + reservationId;

			#region Add Headers
			requestHeader.Add("Accept", "application/json");
			requestHeader.Add("Api-Key", ConfigurationManager.AppSettings["AlephAPI_Key"]);
			requestHeader.Add("Content-Type", "application/json");
			requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, password));
			#endregion Add Headers


			string locationResult = "";
			AlephVehicleLocationRS alephVehicleLocationResponse = new AlephVehicleLocationRS();

			try
			{
				locationResult = ProcessWebRequestEx(queryUrl, "", "application/json", "GET", string.Empty, string.Empty, timeout, requestHeader, out status, out responseHeader);
				alephVehicleLocationResponse = locationResult.JsonDeserialize<AlephVehicleLocationRS>();
				LogResponse(methodName, locationResult);
			}
			catch (Exception ex)
			{
				int vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
				LogException(methodName, vtodErrorCode.ToString(), ex);
				throw new Exception(vtodErrorCode.ToString(), ex);
			}


			return alephVehicleLocationResponse;
		}


		public AlephGetAvailableVehiclesRS GetAvailableVehicles(string providerId, decimal latitude, decimal longitude, decimal radius, int maxCarsToReturn, string dispatchStrategy, string userName, string password)
		{
			Dictionary<string, string> requestHeader = new Dictionary<string, string>();
			Dictionary<string, string> responseHeader = new Dictionary<string, string>();
			string methodName = GetMethodName();
			string status = "";
			string queryUrl = "";

			int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["AlephAPI_Https_Timeout"]);


			if (string.IsNullOrWhiteSpace(providerId) || string.IsNullOrWhiteSpace(dispatchStrategy))
			{
				queryUrl = ConfigurationManager.AppSettings["AlephAPI_Domain"] + "/vehicle/available/" + latitude.ToString() + "/" + longitude.ToString() + "/" + radius.ToString() + "/" + maxCarsToReturn.ToString();
			}
			else
			{
				queryUrl = ConfigurationManager.AppSettings["AlephAPI_Domain"] + "/vehicle/available/" + latitude.ToString() + "/" + longitude.ToString() + "/" + radius.ToString() + "/" + maxCarsToReturn.ToString() + "/" + providerId.ToString() + "/" + dispatchStrategy.ToString();
			}


			#region Add Headers
			requestHeader.Add("Accept", "application/json");
			requestHeader.Add("Api-Key", ConfigurationManager.AppSettings["AlephAPI_Key"]);
			requestHeader.Add("Content-Type", "application/json");
			requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, password));
			#endregion Add Headers


			string vehiclesResult = "";
			AlephGetAvailableVehiclesRS alephAvailableVehiclesResponse = new AlephGetAvailableVehiclesRS();

			try
			{
				vehiclesResult = ProcessWebRequestEx(queryUrl, "", "application/json", "GET", string.Empty, string.Empty, timeout, requestHeader, out status, out responseHeader);
				alephAvailableVehiclesResponse.vehicles = vehiclesResult.JsonDeserialize<List<AlephAvailableVehicle>>();
				LogResponse(methodName, vehiclesResult);
			}
			catch (Exception ex)
			{
				alephAvailableVehiclesResponse.vehicles = new List<AlephAvailableVehicle>();


				//If the exception is not a 404 - Not Found, then re-throw the exception;
				if (!ex.InnerException.Message.Contains("404"))
				{
					int vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
					LogException(methodName, vtodErrorCode.ToString(), ex);
					throw new Exception(vtodErrorCode.ToString(), ex);
				}
			}


			return alephAvailableVehiclesResponse;
		}

		#endregion Vehicle


		#region User

		public AlephGetUserProfilesRS GetUserProfiles(string userName, string password)
		{
			Dictionary<string, string> requestHeader = new Dictionary<string, string>();
			Dictionary<string, string> responseHeader = new Dictionary<string, string>();
			string methodName = GetMethodName();
			string status = "";

			int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["AlephAPI_Https_Timeout"]);

			string queryUrl = ConfigurationManager.AppSettings["AlephAPI_Domain"] + "/user/profile";


			#region Add Headers
			requestHeader.Add("Accept", "application/json");
			requestHeader.Add("Api-Key", ConfigurationManager.AppSettings["AlephAPI_Key"]);
			requestHeader.Add("Content-Type", "application/json");
			requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, password));
			#endregion Add Headers


			string profilesResult = "";
			AlephGetUserProfilesRS alephUserProfilesResponse = new AlephGetUserProfilesRS();

			try
			{
				profilesResult = ProcessWebRequestEx(queryUrl, "", "application/json", "GET", string.Empty, string.Empty, timeout, requestHeader, out status, out responseHeader);
				alephUserProfilesResponse.profiles = profilesResult.JsonDeserialize<List<AlephUserProfile>>();
				LogResponse(methodName, profilesResult);

				//Remove profiles that are not for this specific user
				if ((alephUserProfilesResponse.profiles != null) && (alephUserProfilesResponse.profiles.Count > 0))
				{
					var filteredProfileList = new List<AlephUserProfile>();

					foreach (AlephUserProfile profile in alephUserProfilesResponse.profiles)
					{
						if (profile.username.Trim().ToLower().Equals(userName.Trim().ToLower()))
						{
							filteredProfileList.Add(profile);
						}
					}

					alephUserProfilesResponse.profiles = filteredProfileList;
				}
			}
			catch (Exception ex)
			{
				int vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
				LogException(methodName, vtodErrorCode.ToString(), ex);
				throw new Exception(vtodErrorCode.ToString(), ex);
			}


			return alephUserProfilesResponse;
		}


		public List<AlephCorporateRS> GetUserProfiles2(string userName, string password, string latitude, string longitude)
		{
			Dictionary<string, string> requestHeader = new Dictionary<string, string>();
			Dictionary<string, string> responseHeader = new Dictionary<string, string>();
			string methodName = GetMethodName();
			string status = "";

			int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["AlephAPI_Https_Timeout"]);

			string queryUrl = ConfigurationManager.AppSettings["AlephAPI_Domain"] + "/corporate/user/profile";

			queryUrl = queryUrl + "/" + latitude + "/" + longitude;
			#region Add Headers
			requestHeader.Add("Accept", "application/json");
			requestHeader.Add("Api-Key", ConfigurationManager.AppSettings["AlephAPI_Key"]);
			requestHeader.Add("Content-Type", "application/json");
			requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, password));
			#endregion Add Headers


			string profilesResult = "";
			List<AlephCorporateRS> alephUserProfilesResponse = new List<AlephCorporateRS>();

			try
			{
				profilesResult = ProcessWebRequestEx(queryUrl, "", "application/json", "GET", string.Empty, string.Empty, timeout, requestHeader, out status, out responseHeader);
				alephUserProfilesResponse = profilesResult.JsonDeserialize<List<AlephCorporateRS>>();
				LogResponse(methodName, profilesResult);
			}
			catch (Exception ex)
			{
				int vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
				LogException(methodName, vtodErrorCode.ToString(), ex);
				throw new Exception(vtodErrorCode.ToString(), ex);
			}


			return alephUserProfilesResponse;
		}

		public AlephVerifyUserRS VerifyUser(string userName)
		{
			Dictionary<string, string> requestHeader = new Dictionary<string, string>();
			Dictionary<string, string> responseHeader = new Dictionary<string, string>();
			AlephVerifyUserRQ alephVerifyUserRequest = new AlephVerifyUserRQ();
			string methodName = GetMethodName();
			string status = "";

			int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["AlephAPI_Https_Timeout"]);

			string queryUrl = ConfigurationManager.AppSettings["AlephAPI_Domain"] + "/user/verification";


			#region Add Content
			alephVerifyUserRequest.username = userName;

			//Serialize content class
			string content = alephVerifyUserRequest.JsonSerialize();
			#endregion Add Content


			#region Add Headers
			requestHeader.Add("Accept", "application/json");
			requestHeader.Add("Api-Key", ConfigurationManager.AppSettings["AlephAPI_Key"]);
			requestHeader.Add("Content-Type", "application/json");
			#endregion Add Headers


			string verifyResult = "";
			AlephVerifyUserRS alephVerifyUserResponse = new AlephVerifyUserRS();

			try
			{
				LogRequest(methodName, content);
				verifyResult = ProcessWebRequestEx(queryUrl, content, "application/json", "POST", string.Empty, string.Empty, timeout, requestHeader, out status, out responseHeader);
				alephVerifyUserResponse.verified = true;
				LogResponse(methodName, verifyResult);
			}
			catch (Exception ex)
			{
				alephVerifyUserResponse.verified = false;

				//If the exception is not a 404 - Not Found, then re-throw the exception;
				if (!ex.InnerException.Message.Contains("404"))
				{
					int vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
					LogException(methodName, vtodErrorCode.ToString(), ex);
					throw new Exception(vtodErrorCode.ToString(), ex);
				}
			}


			return alephVerifyUserResponse;
		}


		public AlephRegisterUserRS RegisterUser(string userName, string password)
		{
			Dictionary<string, string> requestHeader = new Dictionary<string, string>();
			Dictionary<string, string> responseHeader = new Dictionary<string, string>();
			AlephRegisterUserRQ alephRegisterUserRequest = new AlephRegisterUserRQ();
			string methodName = GetMethodName();
			string status = "";

			int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["AlephAPI_Https_Timeout"]);

			string queryUrl = ConfigurationManager.AppSettings["AlephAPI_Domain"] + "/user/ztrip/registration";


			#region Add Content
			alephRegisterUserRequest.username = userName;
			alephRegisterUserRequest.password = password;


			//Serialize content class
			string content = alephRegisterUserRequest.JsonSerialize();
			#endregion Add Content


			#region Add Headers
			requestHeader.Add("Accept", "application/json");
			requestHeader.Add("Api-Key", ConfigurationManager.AppSettings["AlephAPI_Key"]);
			requestHeader.Add("Content-Type", "application/json");
			#endregion Add Headers


			string registerResult = "";
			AlephRegisterUserRS alephRegisterUserResponse = new AlephRegisterUserRS();

			try
			{
				LogRequest(methodName, content);
				registerResult = ProcessWebRequestEx(queryUrl, content, "application/json", "POST", string.Empty, string.Empty, timeout, requestHeader, out status, out responseHeader);
				alephRegisterUserResponse.registered = true;
				LogResponse(methodName, registerResult);
			}
			catch (Exception ex)
			{
				alephRegisterUserResponse.registered = false;

				int vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
				LogException(methodName, vtodErrorCode.ToString(), ex);
				throw new Exception(vtodErrorCode.ToString(), ex);
			}


			return alephRegisterUserResponse;
		}


		public AlephChangeUserPasswordRS ChangeUserPassword(string userName, string oldpassword, string password)
		{
			Dictionary<string, string> requestHeader = new Dictionary<string, string>();
			Dictionary<string, string> responseHeader = new Dictionary<string, string>();
			AlephChangeUserPasswordRQ alephChangeUserPasswordRequest = new AlephChangeUserPasswordRQ();
			string methodName = GetMethodName();
			string status = "";

			int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["AlephAPI_Https_Timeout"]);

			string queryUrl = ConfigurationManager.AppSettings["AlephAPI_Domain"] + "/user/ztrip/registration";


			#region Add Content
			alephChangeUserPasswordRequest.username = userName;
			alephChangeUserPasswordRequest.password = oldpassword;
			alephChangeUserPasswordRequest.new_password = password;

			//Serialize content class
			string content = alephChangeUserPasswordRequest.JsonSerialize();
			#endregion Add Content


			#region Add Headers
			requestHeader.Add("Accept", "application/json");
			requestHeader.Add("Api-Key", ConfigurationManager.AppSettings["AlephAPI_Key"]);
			requestHeader.Add("Content-Type", "application/json");
			requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, oldpassword));
			#endregion Add Headers


			string changeUserResult = "";
			AlephChangeUserPasswordRS alephChangeUserPasswordResponse = new AlephChangeUserPasswordRS();

			try
			{
				LogRequest(methodName, content);
				changeUserResult = ProcessWebRequestEx(queryUrl, content, "application/json", "PUT", string.Empty, string.Empty, timeout, requestHeader, out status, out responseHeader);
				alephChangeUserPasswordResponse.changed = true;
				LogResponse(methodName, changeUserResult);
			}
			catch (Exception ex)
			{
				alephChangeUserPasswordResponse.changed = false;

				//If the exception is not a 404 - Not Found, then re-throw the exception;
				if (!ex.InnerException.Message.Contains("404"))
				{
					int vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
					LogException(methodName, vtodErrorCode.ToString(), ex);
					throw new Exception(vtodErrorCode.ToString(), ex);
				}
			}


			return alephChangeUserPasswordResponse;
		}


		public AlephDeleteUserRS DeleteUser(string userName, string password)
		{
			Dictionary<string, string> requestHeader = new Dictionary<string, string>();
			Dictionary<string, string> responseHeader = new Dictionary<string, string>();
			AlephDeleteUserRQ deleteUserRequest = new AlephDeleteUserRQ();
			string methodName = GetMethodName();
			string status = "";

			int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["AlephAPI_Https_Timeout"]);

			string queryUrl = ConfigurationManager.AppSettings["AlephAPI_Domain"] + "/user/ztrip/registration";


			#region Add Content
			deleteUserRequest.username = userName;

			//Serialize content class
			string content = deleteUserRequest.JsonSerialize();
			#endregion Add Content


			#region Add Headers
			requestHeader.Add("Accept", "application/json");
			requestHeader.Add("Api-Key", ConfigurationManager.AppSettings["AlephAPI_Key"]);
			requestHeader.Add("Content-Type", "application/json");
			requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, password));
			#endregion Add Headers


			string deleteResult = "";
			AlephDeleteUserRS alephDeleteUserResponse = new AlephDeleteUserRS();

			try
			{
				LogRequest(methodName, content);
				deleteResult = ProcessWebRequestEx(queryUrl, content, "application/json", "DELETE", string.Empty, string.Empty, timeout, requestHeader, out status, out responseHeader);
				alephDeleteUserResponse.deleted = true;
				LogResponse(methodName, deleteResult);
			}
			catch (Exception ex)
			{
				alephDeleteUserResponse.deleted = false;

				//If the exception is not a 404 - Not Found, then re-throw the exception;
				if (!ex.InnerException.Message.Contains("404"))
				{
					int vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
					LogException(methodName, vtodErrorCode.ToString(), ex);
					throw new Exception(vtodErrorCode.ToString(), ex);
				}
			}


			return alephDeleteUserResponse;
		}
		public AlephRegisterDeviceRS DeleteDevice(string userName, string password, string deviceToken, int memberID)
		{
			Dictionary<string, string> requestHeader = new Dictionary<string, string>();
			Dictionary<string, string> responseHeader = new Dictionary<string, string>();
			AlephRegisterDeviceRQ alephRegisterDeviceRequest = new AlephRegisterDeviceRQ();
			string methodName = GetMethodName();
			string status = "";
			string content = "";
			if (string.IsNullOrEmpty(deviceToken) && string.IsNullOrWhiteSpace(deviceToken))
			{
				if (memberID != null)
				{
					deviceToken = memberID.ToString();
				}
			}
			int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["AlephAPI_Https_Timeout"]);

			string queryUrl = ConfigurationManager.AppSettings["AlephAPI_Domain"] + "/user/device/";
			queryUrl = queryUrl + deviceToken;

			#region Add Headers
			requestHeader.Add("Accept", "application/json");
			requestHeader.Add("Api-Key", ConfigurationManager.AppSettings["AlephAPI_Key"]);
			requestHeader.Add("Content-Type", "application/json");
			requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, password));
			#endregion Add Headers


			string registerResult = "";
			AlephRegisterDeviceRS alephRegisterDeviceResponse = new AlephRegisterDeviceRS();

			try
			{
				LogRequest(methodName, content);
				registerResult = ProcessWebRequestEx(queryUrl, "", "application/json", "GET", string.Empty, string.Empty, timeout, requestHeader, out status, out responseHeader);
				LogResponse(methodName, registerResult);
				if (!string.IsNullOrEmpty(registerResult))
				{
					content = registerResult;
					LogRequest(methodName, content);
					registerResult = ProcessWebRequestEx(queryUrl, content, "application/json", "DELETE", string.Empty, string.Empty, timeout, requestHeader, out status, out responseHeader);
					LogResponse(methodName, registerResult);
					alephRegisterDeviceResponse.registered = true;
				}
			}
			catch (Exception ex)
			{
				int vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
				LogException(methodName, vtodErrorCode.ToString(), ex);
				throw new Exception(vtodErrorCode.ToString(), ex);
			}
			return alephRegisterDeviceResponse;
		}
		public AlephRegisterDeviceRS RegisterDevice(string deviceToken, string platform_name, string platform_version, string phone_number, string userName, string password, int memberID)
		{
			Dictionary<string, string> requestHeader = new Dictionary<string, string>();
			Dictionary<string, string> responseHeader = new Dictionary<string, string>();
			AlephRegisterDeviceRQ alephRegisterDeviceRequest = new AlephRegisterDeviceRQ();
			string methodName = GetMethodName();
			string status = "";

			int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["AlephAPI_Https_Timeout"]);


			#region Add Content
			if (!string.IsNullOrEmpty(deviceToken) && !string.IsNullOrWhiteSpace(deviceToken))
			{
				alephRegisterDeviceRequest.device_token = deviceToken;
			}
			else
			{
				if (memberID != null)
				{
					alephRegisterDeviceRequest.device_token = memberID.ToString();
				}
			}
			alephRegisterDeviceRequest.platform_name = platform_name;
			alephRegisterDeviceRequest.platform_version = platform_version;
			if (!string.IsNullOrWhiteSpace(phone_number))
			{
				if (phone_number.Length > 10)
				{
					if (phone_number.Substring(0, 1) == "1")
					{
						phone_number = phone_number.Substring(1);
						phone_number = phone_number.Substring(0, 10);
					}
				}
			}
			alephRegisterDeviceRequest.phone_number = phone_number;
			alephRegisterDeviceRequest.push_notification_token = null;
			alephRegisterDeviceRequest.push_notifications = true;
			alephRegisterDeviceRequest.sms_notifications = true;
			alephRegisterDeviceRequest.is_primary_device = true;
			string content = alephRegisterDeviceRequest.JsonSerialize();
			#endregion Add Content


			#region Add Headers
			requestHeader.Add("Accept", "application/json");
			requestHeader.Add("Api-Key", ConfigurationManager.AppSettings["AlephAPI_Key"]);
			requestHeader.Add("Content-Type", "application/json");
			requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, password));
			#endregion Add Headers

			string queryUrl = ConfigurationManager.AppSettings["AlephAPI_Domain"] + "/user/device/";
			queryUrl = queryUrl + alephRegisterDeviceRequest.device_token + "/";

			string registerResult = "";
			AlephRegisterDeviceRS alephRegisterDeviceResponse = new AlephRegisterDeviceRS();

			try
			{
				try
				{
					LogRequest(methodName, content);
					registerResult = ProcessWebRequestEx(queryUrl, "", "application/json", "GET", string.Empty, string.Empty, timeout, requestHeader, out status, out responseHeader);
					LogResponse(methodName, registerResult);
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}
				if (string.IsNullOrEmpty(registerResult) || registerResult == "[]")
				{
					LogRequest(methodName, content);
					registerResult = ProcessWebRequestEx(queryUrl, content, "application/json", "POST", string.Empty, string.Empty, timeout, requestHeader, out status, out responseHeader);
					LogResponse(methodName, registerResult);
					alephRegisterDeviceResponse.registered = true;
				}
				else
				{
					LogRequest(methodName, content);
					registerResult = ProcessWebRequestEx(queryUrl, content, "application/json", "PUT", string.Empty, string.Empty, timeout, requestHeader, out status, out responseHeader);
					LogResponse(methodName, registerResult);
					alephRegisterDeviceResponse.registered = true;
				}
			}
			catch (Exception ex)
			{
				int vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
				LogException(methodName, vtodErrorCode.ToString(), ex);
				throw new Exception(vtodErrorCode.ToString(), ex);
			}
			return alephRegisterDeviceResponse;
		}


		#endregion User


		#region Fleets

		public AlephGetAvailableFleetsRS GetAvailableFleets(decimal latitude, decimal longitude, string userName, string password, string representedUserName)
		{
			Dictionary<string, string> requestHeader = new Dictionary<string, string>();
			Dictionary<string, string> responseHeader = new Dictionary<string, string>();
			string methodName = GetMethodName();
			string status = "";
			string queryUrl = "";

			int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["AlephAPI_Https_Timeout"]);


			queryUrl = ConfigurationManager.AppSettings["AlephAPI_Domain"] + "/corporate/bookingoption/" + latitude.ToString() + "/" + longitude.ToString();

			if (!string.IsNullOrWhiteSpace(representedUserName))
			{
				queryUrl = queryUrl + "/" + representedUserName;
			}


			#region Add Headers
			requestHeader.Add("Accept", "application/json");
			requestHeader.Add("Api-Key", ConfigurationManager.AppSettings["AlephAPI_Key"]);
			requestHeader.Add("Content-Type", "application/json");
			requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, password));
			#endregion Add Headers


			string getAvailableFleetsResult = "";
			AlephGetAvailableFleetsRS getAvailableFleetsResponse = new AlephGetAvailableFleetsRS();

			try
			{
				getAvailableFleetsResult = ProcessWebRequestEx(queryUrl, "", "application/json", "GET", string.Empty, string.Empty, timeout, requestHeader, out status, out responseHeader);
				getAvailableFleetsResponse = getAvailableFleetsResult.JsonDeserialize<AlephGetAvailableFleetsRS>();
				LogResponse(methodName, getAvailableFleetsResult);
			}
			catch (Exception ex)
			{
				int vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
				LogException(methodName, vtodErrorCode.ToString(), ex);
				throw new Exception(vtodErrorCode.ToString(), ex);
			}


			return getAvailableFleetsResponse;
		}

		#endregion Fleets

		#region GetLeadTime
		public AlephGetLeadTimeRS GetLeadTime(decimal latitude, decimal longitude, string userName, string password, string provider_name, string company_name)
		{
			Dictionary<string, string> requestHeader = new Dictionary<string, string>();
			Dictionary<string, string> responseHeader = new Dictionary<string, string>();
			string methodName = GetMethodName();
			string status = "";
			string queryUrl = "";

			int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["AlephAPI_Https_Timeout"]);


			queryUrl = ConfigurationManager.AppSettings["AlephAPI_Domain"] + "/corporate/location/config/" + latitude.ToString() + "/" + longitude.ToString();

			if (!string.IsNullOrWhiteSpace(provider_name))
			{
				queryUrl = queryUrl + "/" + provider_name;
			}

			if (!string.IsNullOrWhiteSpace(company_name))
			{
				queryUrl = queryUrl + "/" + company_name + "/";
			}

			#region Add Headers
			requestHeader.Add("Accept", "application/json");
			requestHeader.Add("Api-Key", ConfigurationManager.AppSettings["AlephAPI_Key"]);
			requestHeader.Add("Content-Type", "application/json");
			requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, password));
			#endregion Add Headers


			string getLeadTimeResult = "";
			AlephGetLeadTimeRS getLeadTimeResponse = new AlephGetLeadTimeRS();

			try
			{
				getLeadTimeResult = ProcessWebRequestEx(queryUrl, "", "application/json", "GET", string.Empty, string.Empty, timeout, requestHeader, out status, out responseHeader);
				getLeadTimeResponse = getLeadTimeResult.JsonDeserialize<AlephGetLeadTimeRS>();
				LogResponse(methodName, getLeadTimeResult);
			}
			catch (Exception ex)
			{
				int vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
				LogException(methodName, vtodErrorCode.ToString(), ex);
				throw new Exception(vtodErrorCode.ToString(), ex);
			}


			return getLeadTimeResponse;
		}
        #endregion

        #region GetClientToken
        public AlephGetClientTokenRS GetClientToken(string userName, string password, string profile_id)
        {
            Dictionary<string, string> requestHeader = new Dictionary<string, string>();
            Dictionary<string, string> responseHeader = new Dictionary<string, string>();
            string methodName = GetMethodName();
            string status = "";
            string queryUrl = "";

            int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["AlephAPI_Https_Timeout"]);


            queryUrl = ConfigurationManager.AppSettings["AlephAPI_Domain"] + "/gatewayclienttoken";

            if (!string.IsNullOrWhiteSpace(profile_id))
            {
                queryUrl = queryUrl + "/" + profile_id;
            }

            if (!string.IsNullOrWhiteSpace(userName))
            {
                queryUrl = queryUrl + "/" + userName + "/";
            }

            #region Add Headers
            requestHeader.Add("Accept", "application/json");
            requestHeader.Add("Api-Key", ConfigurationManager.AppSettings["AlephAPI_Key"]);
            requestHeader.Add("Content-Type", "application/json");
            requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, password));
            #endregion Add Headers


            string getClientTokenResult = "";
            AlephGetClientTokenRS getClientTokenResponse = new AlephGetClientTokenRS();

            try
            {
                getClientTokenResult = ProcessWebRequestEx(queryUrl, "", "application/json", "GET", string.Empty, string.Empty, timeout, requestHeader, out status, out responseHeader);
                getClientTokenResponse = getClientTokenResult.JsonDeserialize<AlephGetClientTokenRS>();
                LogResponse(methodName, getClientTokenResult);
            }
            catch (Exception ex)
            {
                int vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
                LogException(methodName, vtodErrorCode.ToString(), ex);
                throw new Exception(vtodErrorCode.ToString(), ex);
            }


            return getClientTokenResponse;
        }
        #endregion

        #region Utilities

        //Make HttpWebRequest
        private static string ProcessWebRequestEx(string url, string content, string contentType, string method, string userName, string password, int timeout, Dictionary<string, string> headers, out string status, out Dictionary<string, string> responseHeader)
		{
			var result = string.Empty;
			status = string.Empty;
			responseHeader = new Dictionary<string, string>();


			try
			{
				ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

				HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
				if (headers != null && headers.Any())
				{
					foreach (var item in headers)
					{
						//If the Header type specified is one of the existing 'restricted headers', just assign the new value. 
						//Otherwise add the new header and value.
						switch (item.Key.ToLower())
						{
							case "accept":
								request.Accept = item.Value;
								break;
							case "connection":
								request.Connection = item.Value;
								break;
							case "content-length":
								request.ContentLength = Convert.ToInt64(item.Value);
								break;
							case "content-type":
								request.ContentType = item.Value;
								break;
							case "date":
								request.Date = Convert.ToDateTime(item.Value);
								break;
							case "expect":
								request.Expect = item.Value;
								break;
							case "host":
								request.Host = item.Value;
								break;
							case "if-modified-since":
								request.IfModifiedSince = Convert.ToDateTime(item.Value);
								break;
							case "referer":
								request.Referer = item.Value;
								break;
							case "transfer-encoding":
								request.TransferEncoding = item.Value;
								break;
							case "user-agent":
								request.UserAgent = item.Value;
								break;
							default:
								request.Headers.Add(item.Key + ":" + item.Value);
								break;
						}
					}
				}

				if (!string.IsNullOrWhiteSpace(userName))
				{
					NetworkCredential networkCredential = new NetworkCredential(userName, password);
					request.Credentials = networkCredential;
				}
				//NetworkCredential networkCredential = new NetworkCredential("DenverYellowCab", "denveryellowcab@12345");
				//request.Credentials = networkCredential;
				request.ProtocolVersion = HttpVersion.Version11;
				request.Method = method;
				request.Timeout = timeout;
				if (request.Accept == null) request.Accept = "*/*";
				request.UserAgent = "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; Trident/6.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; InfoPath.3; .NET4.0C; .NET4.0E)";


				Stream dataStream;
				if (!string.IsNullOrWhiteSpace(content))
				{
					byte[] byteArray = Encoding.UTF8.GetBytes(content);
					//byte[] byteArray = Encoding.ASCII.GetBytes(content);
					request.ContentType = contentType;
					request.ContentLength = byteArray.Length;
					dataStream = request.GetRequestStream();
					dataStream.Write(byteArray, 0, byteArray.Length);
					dataStream.Close();
				}
				//else
				//{
				//	dataStream = request.GetRequestStream();
				//}
				WebResponse response = request.GetResponse();

				if (response.Headers != null && response.Headers.Count > 0)
				{
					for (int i = 0; i < response.Headers.Count; ++i)
					{
						responseHeader.Add(response.Headers.Keys[i], response.Headers[i]);
					}
				}


				status = ((HttpWebResponse)response).StatusDescription;

				dataStream = response.GetResponseStream();
				StreamReader reader = new StreamReader(dataStream);
				result = reader.ReadToEnd();

				reader.Close();
				dataStream.Close();
				response.Close();

			}
			catch (WebException ex)
			{
				using (var reader = new StreamReader(ex.Response.GetResponseStream()))
				{
					result = reader.ReadToEnd();
				}
				throw new Exception(result, ex);
			}

			return result;

		}

		//Encode username and password as base64
		private static string Base64EncodeForBasicAuthentication(string user, string pass)
		{
			var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(user.Trim() + ":" + pass.Trim());
			return System.Convert.ToBase64String(plainTextBytes);
		}

		//Returns the name of the method that calls this method. 
		private static string GetMethodName([CallerMemberName] string memberName = "")
		{
			return memberName;
		}

		//Logs Request content
		private void LogRequest(string method, string request)
		{
			const string className = "AlephAPICall";

			logger.InfoFormat("Method:{0}.{1}, Request Content:{2}", className, method, request);
		}

		//Logs Response from Aleph
		private void LogResponse(string method, string response)
		{
			const string className = "AlephAPICall";

			logger.InfoFormat("Method:{0}.{1}, Aleph Response:{2}", className, method, response);
		}

		//Logs Exceptions from Aleph
		private void LogException(string method, string message, Exception e)
		{
			const string className = "AlephAPICall";

			logger.Error(string.Format("Method:{0}.{1}, Message:{2}", className, method, message), e);
		}

		#endregion

		#region Member Request Email Confirmation
		public MemberRequestEmailConfirmationRS MemberRequestEmailConfirmation(string corporateName, string accountName, string confirmationNumber, string userName, string password)
		{
			Dictionary<string, string> requestHeader = new Dictionary<string, string>();
			Dictionary<string, string> responseHeader = new Dictionary<string, string>();
			string methodName = GetMethodName();
			string status = "";
			string queryUrl = "";

			int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["AlephAPI_Https_Timeout"]);


			if (!string.IsNullOrWhiteSpace(corporateName) || !string.IsNullOrWhiteSpace(accountName))
			{
				queryUrl = ConfigurationManager.AppSettings["AlephAPI_Domain"] + "/corporate/reservation/confirmationemail/" + corporateName.ToString() + "/" + accountName.ToString() + "/" + confirmationNumber.ToString() + "/";
			}


			#region Add Headers
			requestHeader.Add("Accept", "application/json");
			requestHeader.Add("Api-Key", ConfigurationManager.AppSettings["AlephAPI_Key"]);
			requestHeader.Add("Content-Type", "application/json");
			requestHeader.Add("Authorization", "Basic " + Base64EncodeForBasicAuthentication(userName, password));
			#endregion Add Headers


			string emailConfirmationResult = "";
			MemberRequestEmailConfirmationRS alephRequestEmailConfirmationResponse = new MemberRequestEmailConfirmationRS();

			try
			{
				string username = @"{'username':'" + userName + "'}";
				emailConfirmationResult = ProcessWebRequestEx(queryUrl, username, "application/json", "POST", string.Empty, string.Empty, timeout, requestHeader, out status, out responseHeader);
				alephRequestEmailConfirmationResponse = emailConfirmationResult.JsonDeserialize<MemberRequestEmailConfirmationRS>();

				LogResponse(methodName, emailConfirmationResult);
			}
			catch (Exception ex)
			{

				if (!ex.InnerException.Message.Contains("404"))
				{
					int vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
					LogException(methodName, vtodErrorCode.ToString(), ex);
					throw new Exception(vtodErrorCode.ToString(), ex);
				}
				else
				{
					int vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
					LogException(methodName, vtodErrorCode.ToString(), ex);
					throw new Exception(vtodErrorCode.ToString(), ex);

				}
			}


			return alephRequestEmailConfirmationResponse;
		}
		#endregion

	}
}
