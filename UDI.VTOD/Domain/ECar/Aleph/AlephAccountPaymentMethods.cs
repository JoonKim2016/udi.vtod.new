﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.ECar.Aleph
{
    public class AlephAccountPaymentMethods
    {
        public string account_name { get; set; }
        public bool? is_credit_card_eligible { get; set; }
        public List<string> payment_methods { get; set; }
       
    }
}
