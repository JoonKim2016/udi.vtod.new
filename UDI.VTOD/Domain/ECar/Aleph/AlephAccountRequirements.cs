﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.ECar.Aleph
{
    public class AlephAccountRequirements
    {
        public string account_name { get; set; }
        public List<AlephRequirement> corporation_requirements { get; set; }
    }
}
