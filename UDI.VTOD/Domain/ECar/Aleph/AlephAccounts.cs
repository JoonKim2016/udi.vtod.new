﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.ECar.Aleph
{
   public class AlephAccounts
    {
       public List<string> Profile { get; set; }
       public int provider_id { get; set; }
       public string provider_account_id { get; set; }
       public string company_name { get; set; }
       public string profile_id { get; set; }
       public string default_payment_method { get; set; }
       public string username { get; set; }
       public string preferred_company { get; set; }

    }
}
