﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.ECar.Aleph
{
    public class AlephPaymentCard 
    {
        public string CorporateProviderID { set; get; }        
        public string CorporateProfileID { set; get; }
        public string CorporateEmail { set; get; }        
        public string CorporateName { set; get; }                
        public string CorporatePaymentNonce { set; get; }
        public string Account { set; get; }
        public bool IsPrimary { set; get; }
        public string CorporatePaymentMethodToken { set; get; }
    }
}
