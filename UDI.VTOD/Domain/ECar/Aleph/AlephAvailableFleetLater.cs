﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.ECar.Aleph
{
    public class AlephAvailableFleetLater
    {
        public bool CanBook { get; set; }
        public int MinimumMinutesFromNowBookingIsAllowed { get; set; }
    }
}
