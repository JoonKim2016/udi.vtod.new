﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.ECar.Aleph
{
    public class AlephBookNowRQ
    {
        public int provider_id { get; set; }
        public string provider_account_id { get; set; }
        public string company_name { get; set; }
        public string profile_id { get; set; }
        public string originating_system_reference_number { get; set; }
		public bool is_dropoff_as_directed { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public string username { get; set; }
        public string strategy { get; set; }
        public string CorporateName { set; get; }
        public string CorporateAccountName { set; get; }
        public DateTime requested_datetime { get; set; }
        public AlephAddress pickup { get; set; }
        public List<AlephAddress> stops { get; set; }
        public AlephAddress dropoff { get; set; }
        public string payment_method { get; set; }
        public string special_instructions { get; set; }
        public List<AlephBookingCriteria> booking_criteria { get; set; }
		public bool? is_asap { get; set; }
        public string vendor_confirmation_no { get; set; }
        public string agent_ride_no { get; set; }
        public List<AlephPassenger> passengers { get; set; }
        public string payment_method_token { get; set; }
    }
}
