﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.ECar.Aleph
{
    public class AlephBookingCriteria
    {
        public string booking_criterion_name { get; set; }
        public string booking_criterion_value { get; set; }
    }
}
