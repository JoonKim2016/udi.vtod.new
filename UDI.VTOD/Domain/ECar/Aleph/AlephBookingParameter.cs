﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Domain.ECar.Class;

namespace UDI.VTOD.Domain.ECar.Aleph
{
    public class AlephBookingParameter : ECarBookingParameter
    {
        public AlephBookingParameter(): base ()
		{			
		}
                
        public string CorporateProviderID { set; get; }
        public string CorporateCompanyName { get; set; }
        public string CorporateProviderAccountID { set; get; }
        public string CorporateProfileID { set; get; }
        public string CorporateEmail { set; get; }
        public string CorporatePaymentMethod { set; get; }
        public string CorporateSpecialInstructions { set; get; }
        public string CorporateName { set; get; }
        public string CorporateAccountName { set; get; }
        public string WebServiceState { set; get; }
        public List<AlephBookingCriteria> BookingCriteria { set; get; }
        public decimal CorporatePickupLatitude { set; get; }
        public decimal CorporatePickupLongitude { set; get; }
        public decimal CorporateDropoffLatitude { set; get; }
        public decimal CorporateDropoffLongitude { set; get; }
        public string VendorConfirmationNumber { set; get; }
        public List<AlephPassenger> passengers { get; set; }

        public string PaymentMethodToken { set; get; }

    }
}
