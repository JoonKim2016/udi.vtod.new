﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.ECar.Aleph
{
    public class AlephBookingRQ
    {
        public int provider_id { get; set; }
        public string provider_account_id { get; set; }
        public string company_name { get; set; }
        public string profile_id { get; set; }
        public string originating_system_reference_number { get; set; }
        public DateTime requested_datetime { get; set; }
        public AlephAddress pickup { get; set; }
        public AlephAddress dropoff { get; set; }
        public string payment_method { get; set; }
        public string CorporateName { get; set; }
        public string CorporateAccountName { get; set; }
        public string special_instructions { get; set; }
        public List<AlephBookingCriteria> booking_criteria { get; set; }
        public List<AlephPassenger> passengers { get; set; }
        public List<AlephGatewayTrackingNumber> gateway_tracking_numbers { get; set; }
		public bool? is_asap { get; set; }
        public List<AlephAddress> stops { get; set; }
        public string vendor_confirmation_no { get; set; }
        public string agent_ride_no { get; set; }
        public string payment_method_token { get; set; }
    }
}
