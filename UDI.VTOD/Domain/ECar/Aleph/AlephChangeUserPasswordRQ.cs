﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.ECar.Aleph
{
    class AlephChangeUserPasswordRQ
    {
        public string username { get; set; }
        public string password { get; set; }
        public string new_password { get; set; }
    }
}
