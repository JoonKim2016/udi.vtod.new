﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Common.DTO.OTA;

namespace UDI.VTOD.Domain.ECar.Aleph
{
   public class AlephCompany
    {
       public string company_name;
       public List<NameValue> config_options;
    }
}
