﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.ECar.Aleph
{
	public class AlephCompanyInfo
	{
		public string company_name { get; set; }
		public string company_phone_number { get; set; }
	}
}
