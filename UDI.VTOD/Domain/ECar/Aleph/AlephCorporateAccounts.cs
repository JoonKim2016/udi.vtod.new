﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.ECar.Aleph
{
   public class AlephCorporateAccounts
    {
       public string account_name { get; set; }
       public string preferred_company { get; set; }
       public List<AlephAccounts> account_providers { get; set; }
    }
}
