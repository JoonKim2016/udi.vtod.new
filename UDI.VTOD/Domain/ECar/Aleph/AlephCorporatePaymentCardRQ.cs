﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.ECar.Aleph
{
    public class AlephCorporatePaymentCardRQ
    {
        public string payment_method_nonce { get; set; }
        public string nick_name { get; set; }
        public bool is_primary { get; set; }
    }
}
