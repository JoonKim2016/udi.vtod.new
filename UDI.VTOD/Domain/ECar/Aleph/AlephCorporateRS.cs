﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.ECar.Aleph
{
    public class AlephCorporateRS
    {
        public List<AlephCorporateAccounts> corporation_accounts { get; set; }
        public string corporation_name { get; set; }
    }
}
