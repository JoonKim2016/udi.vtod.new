﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.ECar.Aleph
{
    public class AlephCorporationPaymentMethods
    {
        public string corporation_name { get; set; }
        public List<AlephAccountPaymentMethods> corporation_accounts { get; set; }
    }
}
