﻿using log4net;
using System;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Diagnostics;
using System.Configuration;
using System.Globalization;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Runtime.CompilerServices;

using UDI.Map;
using UDI.Utility.Helper;
using UDI.Utility.Serialization;
using UDI.Notification.Service.Class;

using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.Track;
using UDI.VTOD.Common.Helper;
using UDI.VTOD.Common.DTO;

using UDI.VTOD.Domain.ECar;
using UDI.VTOD.Domain.ECar.Class;
using UDI.VTOD.Domain.ECar.Const;
using UDI.VTOD.DataAccess.VTOD;
namespace UDI.VTOD.Domain.ECar.Aleph
{
	public class AlephECarService : IECarService
	{
		private ILog logger;

		private ECarUtility utility;
		//private TokenRS tokenVTOD;

		public AlephECarService(ECarUtility util, ILog log)
		{
			this.logger = log;
			this.utility = util;
		}

		#region Public Methods
		public Common.DTO.OTA.OTA_GroundBookRS Book(Common.DTO.OTA.TokenRS tokenVTOD, Common.DTO.OTA.OTA_GroundBookRQ request, out List<Tuple<string, int, long>> disptach_Rez_VTOD, ECarBookingParameter ecbp)
		{
			Stopwatch sw = new Stopwatch();
			sw.Start();
			OTA_GroundBookRS response = null;
			disptach_Rez_VTOD = new List<Tuple<string, int, long>>();
			AlephECarUtility utility = new AlephECarUtility(logger);
			string postContent = "";
			string errMsg = "";

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
			#endregion

			#region [ECar 1.1] Validate required fields
			AlephBookingParameter parameter = new AlephBookingParameter { Fleet = ecbp.Fleet, serviceAPIID = ecbp.serviceAPIID };
			if (!utility.ValidateBookingRequest(tokenVTOD, request, ref parameter))
			{
				throw new ValidationException(Messages.ECar_Common_InvalidBookingRequest);
			}
			#endregion

			#region [ECar 1.2] Book the trip
			DateTime? requestedLocalTime = null;
			if (parameter.Fleet != null && parameter.Fleet.ServerUTCOffset != null)
			{
				requestedLocalTime = DateTime.UtcNow.FromUtcNullAble(parameter.Fleet.ServerUTCOffset);
			}

			try
			{
				Dictionary<string, string> requestHeader = new Dictionary<string, string>();
				Dictionary<string, string> responseHeader = new Dictionary<string, string>();


				//Change user first name and last name into Test for Test system
				if (ConfigurationManager.AppSettings["IsTest"].ToBool())
				{
					parameter.FirstName = "Test";
					parameter.LastName = "Test";
				}


				#region Book

				#region Generate Request Content
				if (parameter.PickupNow)
				{
					postContent = BuildReservationBookNow(parameter);
				}
				else
				{
					postContent = BuildReservation(parameter);
				}
				#endregion

				#region Send HttpRequest and set response info object

				#region Get Corporate Key

				string corporateKey = string.Empty;

				try
				{
					corporateKey = AlephSDSCall.GetCorporateAccountKey(tokenVTOD, TrackTime, parameter.CorporateEmail);
				}
				catch (Exception ex1)
				{
					throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(ex1.Message));
				}

				#endregion Get Corporate Key


				//string corporateKey = "cfd45da8-27c9-41b5-b4cf-15938eb792a0";


				#region Call Aleph API

				AlephBookingRS alephBookingResponse = null;

				try
				{
					var alephAPICall = new AlephAPICall();

					if (parameter.PickupNow)
					{
                        try
                        {
						    alephBookingResponse = alephAPICall.BookReservationNow(parameter, corporateKey, postContent, "", "", "");
					    }
                        catch (VtodException ex)
                        {
                            if (ex.ExceptionMessage != null)
                            {
                                int code = ex.ExceptionMessage.Code;
                                throw VtodException.CreateException(ExceptionType.ECar, code);
                            }
                            else
                                throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(ex.Message));
                        }
                    }
					else
					{
                        try
                        {
                            alephBookingResponse = alephAPICall.BookReservation(parameter, corporateKey, postContent);

                        }
                        catch (VtodException ex)
                        {
                            if (ex.ExceptionMessage != null)
                            {
                                int code = ex.ExceptionMessage.Code;
                                throw VtodException.CreateException(ExceptionType.ECar, code);
                            }
                            else
                                throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(ex.Message));
                        }
                    }


					#region Update the Corporate Special Instructons and save to DB

					//Deserialize the old Corporate Special Instructions JSON
					List<NameValue> specialInputs = null;
					if (!string.IsNullOrWhiteSpace(parameter.WebServiceState)) specialInputs = parameter.WebServiceState.JsonDeserialize<List<NameValue>>();

					//Update Corporate Special Instructions
					var newSpecialInputs = utility.InsertorUpdateCorporateSpecialInstructions(specialInputs, alephBookingResponse);

					//Save updated Corporate Special Instructions to DB
					utility.SaveCorporateSpecialInstructions(newSpecialInputs.JsonSerialize(), parameter.Trip.Id);

					#endregion

					#region save Aleph Company Info
					var companyInfo = new AlephCompanyInfo
					{
						company_name = alephBookingResponse.company_name,
						company_phone_number = alephBookingResponse.company_phone_number
					};
					utility.SaveAlephCompanyInfo(parameter.Trip.Id, companyInfo.JsonSerialize());
					#endregion
				}
                catch (VtodException exc)
                {
                    if (exc.ExceptionMessage != null)
                    {
                        int code = exc.ExceptionMessage.Code;
                        throw VtodException.CreateException(ExceptionType.ECar, code);
                    }
                    else
                        throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(exc.Message));
                }
				catch (Exception exc)
				{
					errMsg = string.Format("Unable to book this trip. tripID={0}, errormessage={1}", parameter.Trip.Id, exc.InnerException.Message);
					logger.Warn(errMsg);

					throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(exc.Message));
				}

				#endregion Call Aleph API


				//success
				logger.Info("Booking successful");

				parameter.Trip = utility.UpdateECarTrip(parameter.Trip, alephBookingResponse.confirmation_no + "##" + alephBookingResponse.vendor_confirmation_no,ecbp.Fleet_TripCode);

				if (!string.IsNullOrWhiteSpace(alephBookingResponse.vendor_confirmation_no))
				{
					if (parameter.EnableResponseAndLog)
					{
						GroundReservation reservation = parameter.request.GroundReservations.FirstOrDefault();
						response = new OTA_GroundBookRS();
						response.EchoToken = parameter.request.EchoToken;
						response.Target = parameter.request.Target;
						response.Version = parameter.request.Version;
						response.PrimaryLangID = parameter.request.PrimaryLangID;

						response.Success = new Success();

						//Reservation
						response.Reservations = new List<Reservation>();
						Reservation r = new Reservation();
						r.Confirmation = new Confirmation();
						//r.Confirmation.Type = "???";
						r.Confirmation.ID = parameter.Trip.Id.ToString();
						r.Confirmation.Type = ConfirmationType.Confirmation.ToString();

						r.Passenger = reservation.Passenger;
						r.Service = reservation.Service;
						r.Confirmation.TPA_Extensions = new TPA_Extensions();
						//r.Confirmation.TPA_Extensions.DispatchConfirmation = new Confirmation();
						//r.Confirmation.TPA_Extensions.DispatchConfirmation.ID = tbp.Trip.DispatchTripId;
						r.Confirmation.TPA_Extensions.Confirmations = new List<Confirmation>();
						r.Confirmation.TPA_Extensions.Confirmations.Add(new Confirmation { ID = parameter.Trip.ecar_trip.DispatchTripId, Type = ConfirmationType.DispatchConfirmation.ToString() });// = tbp.Trip.DispatchTripId;
						//r.Confirmation.TPA_Extensions.Statuses = new Statuses();
						//r.Confirmation.TPA_Extensions.Statuses.Status = new List<Common.DTO.OTA.Status>();

						#region FleetInfo
						if (!string.IsNullOrWhiteSpace(alephBookingResponse.company_name) || !string.IsNullOrWhiteSpace(alephBookingResponse.company_phone_number))
						{
							r.TPA_Extensions = new TPA_Extensions();
							r.TPA_Extensions.Contacts = new Contacts();
							r.TPA_Extensions.Contacts.Items = new List<Contact>();
							var contact = new Contact { Type = Common.DTO.Enum.ContactType.Dispatch.ToString() };
							if (!string.IsNullOrWhiteSpace(alephBookingResponse.company_name))
							{
								contact.Name = alephBookingResponse.company_name;
							}
							if (!string.IsNullOrWhiteSpace(alephBookingResponse.company_phone_number))
							{
								contact.Telephone = new Telephone { PhoneNumber = alephBookingResponse.company_phone_number };
							}
							r.TPA_Extensions.Contacts.Items.Add(contact);
						}
						#endregion

						response.Reservations.Add(r);

						#region Put status into ecar_trip_status
						utility.SaveTripStatus(parameter.Trip.Id, ECarTripStatusType.Booked, requestedLocalTime, null, null, null, null,null, null,null, null, string.Empty, string.Empty, null);
						#endregion
					}
					else
					{
						logger.Info("Disable response and log for booking");
					}
				}
				else
				{
					string error = string.Format("Cannot get DispatchTripId. Or dispatchTripId is white space.");
					logger.Error(error);
					utility.MarkThisTripAsError(parameter.Trip.Id, error);
					throw new ECarException(error);
				}

				#endregion

				#endregion


			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);

				//change the status of trip
				utility.UpdateFinalStatus(parameter.Trip.Id, ECarTripStatusType.Error, null, null, null);

				if (ex.Message != "Cannot get DispatchTripId. Or dispatchTripId is white space.") utility.MarkThisTripAsError(parameter.Trip.Id, errMsg);

				throw ex;
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#endregion

			#region [ECar 1.3] Link  disptach_VTOD_TripIDs (for supershuttle system)
			if (response.Reservations != null && response.Reservations.Any())
			{
				var rez = response.Reservations.First();
				//disptach_VTOD_TripIDs.Add(rez.Confirmation.TPA_Extensions.DispatchConfirmation.ID, rez.Confirmation.ID.ToInt64());
				var DispatchConfirmationID = rez.Confirmation.TPA_Extensions.Confirmations.Where(s => s.Type.Trim().ToLower() == ConfirmationType.DispatchConfirmation.ToString().ToLower()).FirstOrDefault().ID;

				disptach_Rez_VTOD.Add(new Tuple<string, int, long>(DispatchConfirmationID, 0, rez.Confirmation.ID.ToInt64()));
			}
			#endregion

			response.Success = new Success();

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Aleph, "Book");
			#endregion

			return response;
		}
        public OTA_GroundGetFinalRouteRS GetFinalRoute(TokenRS tokenVTOD, OTA_GroundGetFinalRouteRQ request)
        {
            throw new NotImplementedException();
        }
        public bool GetDisabilityInd(TokenRS tokenVTOD,ecar_fleet f, out int condition, string authenticateEndpoint, string bookingEndpoint)
        {
            throw new NotImplementedException();
        }
        public Common.DTO.OTA.OTA_GroundResRetrieveRS Status(Common.DTO.OTA.TokenRS tokenVTOD, Common.DTO.OTA.OTA_GroundResRetrieveRQ request, ECarStatusParameter ecsp)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			OTA_GroundResRetrieveRS response = null;

			string dispatchConfirmation = null;
			try
			{
				#region Get dispatchConfirmation
				dispatchConfirmation = request.Reference.FirstOrDefault().Type;
				#endregion

				if (!string.IsNullOrWhiteSpace(dispatchConfirmation))
				{
					if (dispatchConfirmation.ToLower() == "dispatchconfirmation")
					{
						response = StatusWithDispatchConfirmation(tokenVTOD, request, ecsp);
					}
					else
					{
						response = StatusWithConfirmation(tokenVTOD, request, ecsp);
					}
				}


			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw;
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_GreenTomato, "Status");
			#endregion

			return response;
		}

		public Common.DTO.OTA.OTA_GroundResRetrieveRS StatusWithConfirmation(Common.DTO.OTA.TokenRS tokenVTOD, OTA_GroundResRetrieveRQ request, ECarStatusParameter ecsp)
		{
			Stopwatch sw = new Stopwatch();
			sw.Start();
            OTA_GroundResRetrieveRS response = null;

            AlephECarUtility utility = new AlephECarUtility(logger);

			#region [ECar 2.0] Validate required fields
			AlephStatusParameter asp = new AlephStatusParameter { Fleet = ecsp.Fleet, serviceAPIID = ecsp.serviceAPIID };
			string alephVendorTripID;
			if (!utility.ValidateStatusRequest(tokenVTOD, request, ref asp))
			{
				throw VtodException.CreateException(ExceptionType.ECar, 3003);
			}
			#endregion

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
			#endregion
			#region Parsing Of TripID
			string TripID = asp.Trip.ecar_trip.DispatchTripId;
			if (TripID.Contains("##"))
			{
				if (TripID.IndexOf("##") != -1)
				{
					alephVendorTripID = TripID.Substring(TripID.IndexOf("##") + 2);
				}
				else
				{
					alephVendorTripID = asp.Trip.ecar_trip.DispatchTripId;
				}
			}
			else
			{
				alephVendorTripID = asp.Trip.ecar_trip.DispatchTripId;
			}
			#endregion

			try
			{

				#region Get Corporate Key

				string corporateKey = string.Empty;

				try
				{
					corporateKey = AlephSDSCall.GetCorporateAccountKey(tokenVTOD, TrackTime, asp.CorporateEmail);
				}
				catch (Exception ex1)
				{
					throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(ex1.Message));
				}

				#endregion Get Corporate Key


				#region Call Aleph API

				AlephBookingRS alephBookingResponse = null;

				try
				{
					var alephAPICall = new AlephAPICall();
					alephBookingResponse = alephAPICall.GetReservation(asp.CorporateProviderID, asp.CorporateCompanyName, asp.CorporateProviderAccountID, asp.CorporateProfileID, asp.CorporateEmail, corporateKey, alephVendorTripID);
				}
				catch (Exception ex)
				{
					string msg = string.Format("Unable to retrieve status for this trip. tripID={0}, errormessage={1}", asp.Trip.Id, ex.InnerException.Message);
					logger.Warn(msg);

					throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(ex.Message));
				}

				#endregion Call Aleph API


				//success
				if ((alephBookingResponse != null) && (alephBookingResponse.vendor_confirmation_no != null) && (alephBookingResponse.vendor_confirmation_no.Trim().Length > 0))
				{

					#region Retrieve Current Status


					string status = "";
					string originalStatus = "";


					//If Booking Details have a valid 'cancelled_timestamp' value then the trip has been cancelled
					if (alephBookingResponse.cancelled_timestamp.HasValue)
					{
						originalStatus = "Canceled";
					}
					else if ((alephBookingResponse.status_messages != null) && (alephBookingResponse.status_messages.Count > 0))
					{
						//Retrieve the most recently generated status from the list of status returned by Aleph
						//originalStatus = alephBookingResponse.status_messages[alephBookingResponse.status_messages.Count - 1].status_message_type;
						bool hasTime = alephBookingResponse.status_messages.Where(o => !string.IsNullOrWhiteSpace(o.message_datetime)).Any();


						if (hasTime)
						{
							originalStatus = alephBookingResponse.status_messages.Where(o => !string.IsNullOrWhiteSpace(o.message_datetime)).OrderByDescending(f => f.message_datetime.ToDateTime().Value).FirstOrDefault().status_message_type;
						}
						else
						{
							originalStatus = alephBookingResponse.status_messages.LastOrDefault().status_message_type;
						}
					}

                    string device = null;
                    string version = null;
                    if (request.TPA_Extensions != null)
                    {
                        if (!string.IsNullOrEmpty(request.TPA_Extensions.Device))
                        {
                            device = request.TPA_Extensions.Device;
                        }
                        if (!string.IsNullOrEmpty(request.TPA_Extensions.Source))
                        {
                            version = request.TPA_Extensions.Source;
                        }
                    }
                    status = utility.ConvertTripStatus(originalStatus, device, version);
                    #region Logic For Modify Reservation
                    DateTime tripTime = asp.Trip.PickupDateTimeUTC.Value; //TODO : Double check if PickupDateTimeUTC is always not null
                    DateTime currentTime = DateTime.UtcNow;
                    double timeDiff = tripTime.Subtract(currentTime).TotalMinutes;

                    TPA_Extensions extension = new TPA_Extensions();

                    if (timeDiff > 60)
                    {
                        if (status == UDI.VTOD.Domain.ECar.Const.ECarTripStatusType.Completed || status == UDI.VTOD.Domain.ECar.Const.ECarTripStatusType.Canceled || status == UDI.VTOD.Domain.ECar.Const.ECarTripStatusType.NoShow)
                        {
                            extension.IsModify = false;
                        }
                        else
                        {
                            extension.IsModify = true;
                        }

                    }
                    else
                    {
                        extension.IsModify = false;
                    }

                    #endregion
                    #endregion Retrieve Current Status


                    #region Get List of Status for which no Driver or Vehicle info is to be retrieved from the Web.Config

                    string noVehicleInfoStatus = string.Empty;
					List<string> noVehicleInfoList = new List<string>();
					bool loadNoVehicleInfo = false;

					//Get list from Web.Config
					try
					{
						noVehicleInfoStatus = ConfigurationManager.AppSettings["AlephAPI_No_Vehicle_Info_Status"];
					}
					catch { }

					//Convert string of status into List<string> 
					if (!string.IsNullOrWhiteSpace(noVehicleInfoStatus))
					{
						try
						{
							noVehicleInfoList = noVehicleInfoStatus.ToLower().Split(',').ToList();
						}
						catch { }
					}

					//Check if current status is in the list of status for which no driver or vehicle info is to be retrieved
					if ((noVehicleInfoList != null) && (noVehicleInfoList.Count > 0))
						loadNoVehicleInfo = noVehicleInfoList.Contains(originalStatus.ToLower());

                    #endregion Get List of Status for which no Driver or Vehicle info is to be retrieved from the Web.Config



                    //Generate OTA response object
                    response = new OTA_GroundResRetrieveRS();
					response.Success = new Success();
					response.EchoToken = asp.request.EchoToken;
					response.Target = asp.request.Target;
					response.Version = asp.request.Version;
					response.PrimaryLangID = asp.request.PrimaryLangID;
                    response.TPA_Extensions = extension;
                    response.TPA_Extensions.Driver = (alephBookingResponse.assigned_vehicle == null) ? null : utility.ExtractDriverName(alephBookingResponse.assigned_vehicle);
					response.TPA_Extensions.Vehicles = new Vehicles();
					response.TPA_Extensions.Vehicles.Items = new List<Vehicle>();

                    #region Confirmations
                    try
                    {
                        response.TPA_Extensions.Confirmation = new Confirmation();
                        response.TPA_Extensions.Confirmation.ID = request.Reference.FirstOrDefault().ID.ToString();
                        response.TPA_Extensions.Confirmation.Type = ConfirmationType.Confirmation.ToString();
                        response.TPA_Extensions.Confirmation.TPA_Extensions = new TPA_Extensions();
                        response.TPA_Extensions.Confirmation.TPA_Extensions.Confirmations = new List<Confirmation>();
                        response.TPA_Extensions.Confirmation.TPA_Extensions.Confirmations.Add(new Confirmation { ID = asp.Trip.ecar_trip.DispatchTripId, Type = ConfirmationType.DispatchConfirmation.ToString() });
                    }
                    catch(Exception ex)
                    {
                        logger.Error("Error in retriving the Confirmations");
                    }
                    #endregion

					//calculate ETA
					int? minutesAway = null;

					//Vehicle
					var vehicle = new Vehicle();

					vehicle.Geolocation = new Geolocation();

					AlephVehicleLocationRS vInfo = null;

					if ((alephBookingResponse.assigned_vehicle != null) && (!loadNoVehicleInfo))
					{
						vehicle.ID = alephBookingResponse.assigned_vehicle.car_no;


						#region Get Vehicle Location from Aleph
						try
						{
							var alephAPICall = new AlephAPICall();
							vInfo = alephAPICall.GetVehicleLocation(asp.CorporateProviderID, asp.CorporateCompanyName, asp.CorporateProviderAccountID, asp.CorporateProfileID, asp.CorporateEmail, corporateKey, alephVendorTripID);
						}
						catch (Exception ex)
						{
							//failure 
							logger.ErrorFormat("Message:{0}", ex.Message);
							logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
							logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
							//response = null;
						}
						#endregion Get Vehicle Location from Aleph


						if ((vInfo != null) && (vInfo.latitude != null) && (vInfo.longitude != null))
						{
							vehicle.Geolocation.Lat = Convert.ToDecimal(vInfo.latitude);
							vehicle.Geolocation.Long = Convert.ToDecimal(vInfo.longitude);
						}
					}
					else
					{
						logger.Info("No vehicle information from response");
					}


					#region Calculate ETA



					#region Others

					//fare
					decimal? totalFare = null;

					if (status.Trim().ToLower() == ECarTripStatusType.Completed.Trim().ToLower() || status.Trim().ToLower() == ECarTripStatusType.Fare.Trim().ToLower())
					{
						totalFare = asp.Trip.TotalFareAmount;
					}


					//Convert status to front-end status
					string frontEndStatus = utility.ConvertTripStatusMessageForFrontEndDevice(asp.Fleet_User.UserId, status);
					//Status s = new Common.DTO.OTA.Status { Value = status, Fare = succ.TripInfo[0].FareAmount.ToDecimal() };
					Status s = new Common.DTO.OTA.Status { Value = frontEndStatus };
					if (totalFare.HasValue)
					{
						s.Fare = totalFare.Value;
					}
					response.TPA_Extensions.Statuses = new Statuses();
					response.TPA_Extensions.Statuses.Status = new List<Status>();
					response.TPA_Extensions.Statuses.Status.Add(s);
					response.TPA_Extensions.Contacts = new Contacts();
					response.TPA_Extensions.Contacts.Items = new List<Contact>();

					if ((alephBookingResponse != null) && (alephBookingResponse.assigned_vehicle != null))
					{
						var fleetContact = new Contact { Type = Common.DTO.Enum.ContactType.Dispatch.ToString(), Name = alephBookingResponse.assigned_vehicle.company_name, Telephone = new Telephone { PhoneNumber = alephBookingResponse.assigned_vehicle.company_phone_number } };
						response.TPA_Extensions.Contacts.Items.Add(fleetContact);
						var driverContact = new Contact { Type = Common.DTO.Enum.ContactType.Driver.ToString(), Name = alephBookingResponse.assigned_vehicle.driver_first_name + " " + alephBookingResponse.assigned_vehicle.driver_last_name, Telephone = new Telephone { PhoneNumber = alephBookingResponse.assigned_vehicle.driver_phone_number } };
						response.TPA_Extensions.Contacts.Items.Add(driverContact);
					}

					if (asp.Trip != null && asp.Trip.ecar_trip.PickupLatitude.HasValue && asp.Trip.ecar_trip.PickupLatitude.Value != 0 && asp.Trip.ecar_trip.PickupLongitude.HasValue && asp.Trip.ecar_trip.PickupLongitude.Value != 0)
					{
						response.TPA_Extensions.PickupLocation = new Pickup_Dropoff_Stop();
						response.TPA_Extensions.PickupLocation.Address = new Common.DTO.OTA.Address();
						response.TPA_Extensions.PickupLocation.Address.Latitude = asp.Trip.ecar_trip.PickupLatitude.Value.ToString();
						response.TPA_Extensions.PickupLocation.Address.Longitude = asp.Trip.ecar_trip.PickupLongitude.Value.ToString();
					}

                    if (asp.Trip != null && asp.Trip.ecar_trip.DropOffLatitude.HasValue && asp.Trip.ecar_trip.DropOffLatitude.Value != 0 && asp.Trip.ecar_trip.DropOffLongitude.HasValue && asp.Trip.ecar_trip.DropOffLongitude.Value != 0)
                    {
                        response.TPA_Extensions.DropoffLocation = new Pickup_Dropoff_Stop();
                        response.TPA_Extensions.DropoffLocation.Address = new Common.DTO.OTA.Address();
                        response.TPA_Extensions.DropoffLocation.Address.Latitude = asp.Trip.ecar_trip.DropOffLatitude.Value.ToString();
                        response.TPA_Extensions.DropoffLocation.Address.Longitude = asp.Trip.ecar_trip.DropOffLongitude.Value.ToString();
                    }
					#endregion

					if ((vehicle.Geolocation.Lat != 0 && vehicle.Geolocation.Long != 0)
						&&
						(asp.Trip.ecar_trip.PickupLatitude.HasValue && asp.Trip.ecar_trip.PickupLongitude.HasValue)
					)
					{
						MapService ms = new MapService();
						double d = ms.GetDirectDistanceInMeter(asp.Trip.ecar_trip.PickupLatitude.Value, asp.Trip.ecar_trip.PickupLongitude.Value, Convert.ToDouble(vehicle.Geolocation.Lat), Convert.ToDouble(vehicle.Geolocation.Long));
						var rc = new UDI.SDS.RoutingController(asp.tokenVTOD, null);
						UDI.SDS.RoutingService.Point start = new UDI.SDS.RoutingService.Point { Latitude = asp.Trip.ecar_trip.PickupLatitude.Value, Longitude = asp.Trip.ecar_trip.PickupLongitude.Value };
						UDI.SDS.RoutingService.Point end = new UDI.SDS.RoutingService.Point { Latitude = Convert.ToDouble(vehicle.Geolocation.Lat), Longitude = Convert.ToDouble(vehicle.Geolocation.Long) };
						var result = rc.GetRouteMetrics(start, end);
						vehicle.MinutesAway = result.TotalMinutes;
						if (vehicle.MinutesAway != null) minutesAway = Convert.ToInt32(vehicle.MinutesAway);
					}

					//Add vehicle info only if it is a valid status type
					if (!loadNoVehicleInfo)
					{
						response.TPA_Extensions.Vehicles.Items.Add(vehicle);
					}

					#endregion

					//Others

					//update ecar trip
					string driverName = String.Empty;
					string carNo = String.Empty;
					string driverId = String.Empty;

					if ((alephBookingResponse != null) && (alephBookingResponse.assigned_vehicle != null) && (!loadNoVehicleInfo))
					{
						driverName = alephBookingResponse.assigned_vehicle.driver_first_name + " " + alephBookingResponse.assigned_vehicle.driver_last_name;
						carNo = alephBookingResponse.assigned_vehicle.car_no;
						driverId = alephBookingResponse.assigned_vehicle.driver_call_number;
					}

					utility.UpdateECarTrip(asp.Trip.Id, carNo, minutesAway, asp.Trip.ecar_trip.DispatchTripId, driverName);


					DateTime? statusTime = null;
					DateTime? tripStartTime = null;
					DateTime? tripEndTime = null;

					//Get the status time of the current status from the Aleph response
					try
					{
						if ((alephBookingResponse.status_messages != null) && (alephBookingResponse.status_messages.Count > 0))
						{
							statusTime = alephBookingResponse.status_messages.OrderByDescending(f => f.message_datetime.ToDateTime().Value).First().message_datetime.ToDateTime().Value;
						}
					}
					catch { }

					if (!statusTime.HasValue || statusTime.Value <= DateTime.MinValue)
					{
						if (asp.Fleet != null && asp.Fleet.ServerUTCOffset != null)
						{
							statusTime = DateTime.UtcNow.FromUtcNullAble(asp.Fleet.ServerUTCOffset);
						}
					}


					//Walk through the status message list looking for a Trip start(Load) or Trip end(Unload) status message
					if ((alephBookingResponse.status_messages != null) && (alephBookingResponse.status_messages.Count > 0))
					{
						//Initialize to current values in the DB
						tripStartTime = asp.Trip.TripStartTime;
						tripEndTime = asp.Trip.TripEndTime;

						foreach (AlephStatusMessage msg in alephBookingResponse.status_messages)
						{
							//If a 'load' message is found and the Trip Start value in the DB is currently 'null'
							if ((msg.status_message_type.ToLower().Equals("load")) && (asp.Trip.TripStartTime == null))
							{
								try
								{
									tripStartTime = Convert.ToDateTime(msg.message_datetime);
								}
								catch { }
							}
							//If an 'unload' message is found and the Trip End value in the DB is currently 'null'
							else if ((msg.status_message_type.ToLower().Equals("unload")) && (asp.Trip.TripEndTime == null))
							{
								try
								{
									tripEndTime = Convert.ToDateTime(msg.message_datetime);
								}
								catch { }
							}
						}
					}

					//Below is the old way of figuring out the trip start and trip end times. Delete this block of coe if the new way above works properly
					/*
					if ((status.Trim().ToLower() == ECarTripStatusType.InService.Trim().ToLower() || status.Trim().ToLower() == ECarTripStatusType.MeterON.Trim().ToLower()) && (asp.Trip.TripStartTime == null))
					{
						tripStartTime = statusTime;
					}
					else
					{
						tripStartTime = asp.Trip.TripStartTime;
					}

					if ((status.Trim().ToLower() == ECarTripStatusType.Completed.Trim().ToLower() || status.Trim().ToLower() == ECarTripStatusType.MeterOff.Trim().ToLower()) && (asp.Trip.TripEndTime == null))
					{
						tripEndTime = statusTime;
					}
					else
					{
						tripEndTime = asp.Trip.TripEndTime;
					}
					*/


					if (status.ToUpperInvariant() == ECarTripStatusType.Canceled.ToUpperInvariant())
					{
						utility.SaveTripStatus(asp.Trip.Id, ECarTripStatusType.Canceled, statusTime, null, null, null, null,null, null,null, null, string.Empty, originalStatus, null);
						utility.UpdateFinalStatus(asp.Trip.Id, ECarTripStatusType.Canceled, null, null, null);
					}
					else
					{
						string vehId = string.Empty;
						decimal? vehLat = null;
						decimal? vehLon = null;
						int? vehETA = null;
                        int? vehETAWithTraffic = null;
                        try
						{
							vehId = response.TPA_Extensions.Vehicles.Items.First().ID;
							vehETA = response.TPA_Extensions.Vehicles.Items.First().MinutesAway.ToInt32();
                            vehETAWithTraffic = response.TPA_Extensions.Vehicles.Items.First().MinutesAwayWithTraffic.ToInt32();
                            vehLat = response.TPA_Extensions.Vehicles.Items.First().Geolocation.Lat;
							vehLon = response.TPA_Extensions.Vehicles.Items.First().Geolocation.Long;
						}
						catch { }

						//Save trip status
						utility.SaveTripStatus(asp.Trip.Id, status, statusTime, vehId, vehLat, vehLon, totalFare, totalFare, vehETA, vehETAWithTraffic, driverName, string.Empty, originalStatus, driverId);

						//Save final status
						utility.UpdateFinalStatus(asp.Trip.Id, status, totalFare, tripStartTime, tripEndTime);

						#region Trip Duration
						try
						{
							if (tripEndTime != null && tripStartTime != null)
							{
								//string.Format("{0:00}:{1:00}:{2:00}", sdsResult.TripDuration.Hours, sdsResult.TripDuration.Minutes, sdsResult.TripDuration.Seconds);
								var duration = (tripEndTime.Value - tripStartTime.Value);
								response.TPA_Extensions.TripInfo = new TripInfo { Duration = string.Format("{0:00}:{1:00}:{2:00}", duration.Hours, duration.Minutes, duration.Seconds) };
							}
						}
						catch (Exception)
						{

							throw;
						}
						#endregion


					}
				}
				else
				{
					//unable to do status
					var vtodException = VtodException.CreateException(ExceptionType.ECar, 303);
					logger.WarnFormat("Unable to retrieve Aleph booking status");
					throw vtodException; // new ECarException(Messages.ECar_StatusFailure);
				}


			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);

				throw new ECarException(ex.Message);
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);



			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_GreenTomato, "Status");
			#endregion

			return response;
		}

		public Common.DTO.OTA.OTA_GroundResRetrieveRS StatusWithDispatchConfirmation(Common.DTO.OTA.TokenRS tokenVTOD, Common.DTO.OTA.OTA_GroundResRetrieveRQ request, ECarStatusParameter ecsp)
		{
			Stopwatch sw = new Stopwatch();
			sw.Start();
			OTA_GroundResRetrieveRS response = null;

			AlephECarUtility utility = new AlephECarUtility(logger);
			string dispatchConfirmation = null;
			string dispatchTripID = null;

			#region Get dispatchConfirmation
			dispatchConfirmation = request.Reference.FirstOrDefault().Type;
			#endregion
			AlephStatusParameter asp = new AlephStatusParameter { };
			string alephVendorTripID = string.Empty;
			if (!string.IsNullOrWhiteSpace(dispatchConfirmation))
			{
				if (dispatchConfirmation.ToLower().ToString() == "dispatchconfirmation")
					dispatchTripID = request.Reference.FirstOrDefault().ID;

				if (dispatchTripID.Contains("##"))
				{
					if (dispatchTripID.IndexOf("##") != -1)
					{
						alephVendorTripID = dispatchTripID.Substring(dispatchTripID.IndexOf("##") + 2);
					}
					else
					{
						alephVendorTripID = dispatchTripID;
					}
				}
				else
				{
					alephVendorTripID = dispatchTripID;
				}
			}

			#region [ECar 2.0] Validate required fields

			if (!utility.ValidateStatusDispatchConfirmationRequest(tokenVTOD, request, ref asp))
			{
				throw VtodException.CreateException(ExceptionType.ECar, 3003);
			}
			#endregion

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
			#endregion


			try
			{

				#region Get Corporate Key
				string corporateKey = string.Empty;
				try
				{
					corporateKey = AlephSDSCall.GetCorporateAccountKey(tokenVTOD, TrackTime, asp.CorporateEmail);
				}
				catch (Exception ex1)
				{
					throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(ex1.Message));
				}

				#endregion Get Corporate Key

				#region Call Aleph API

				AlephBookingRS alephBookingResponse = null;
				try
				{
					var alephAPICall = new AlephAPICall();
					alephBookingResponse = alephAPICall.GetReservation(asp.CorporateProviderID, asp.CorporateCompanyName, asp.CorporateProviderAccountID, asp.CorporateProfileID, asp.CorporateEmail, corporateKey, alephVendorTripID);
				}
				catch (Exception ex)
				{
					string msg = string.Format("Unable to retrieve status for this trip. tripID={0}, errormessage={1}", asp.Trip.Id, ex.InnerException.Message);
					logger.Warn(msg);

					throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(ex.Message));
				}

				#endregion Call Aleph API


				//success
				if ((alephBookingResponse != null) && (alephBookingResponse.vendor_confirmation_no != null) && (alephBookingResponse.vendor_confirmation_no.Trim().Length > 0))
				{

					#region Retrieve Current Status
					string status = "";
					string originalStatus = "";


					//If Booking Details have a valid 'cancelled_timestamp' value then the trip has been cancelled
					if (alephBookingResponse.cancelled_timestamp.HasValue)
					{
						originalStatus = "Canceled";
					}
					else if ((alephBookingResponse.status_messages != null) && (alephBookingResponse.status_messages.Count > 0))
					{


						bool hasTime = alephBookingResponse.status_messages.Where(o => !string.IsNullOrWhiteSpace(o.message_datetime)).Any();


						if (hasTime)
						{
							originalStatus = alephBookingResponse.status_messages.Where(o => !string.IsNullOrWhiteSpace(o.message_datetime)).OrderByDescending(f => f.message_datetime.ToDateTime().Value).FirstOrDefault().status_message_type;
						}
						else
						{
							originalStatus = alephBookingResponse.status_messages.LastOrDefault().status_message_type;
						}

						//string statusMessageTime = alephBookingResponse.status_messages.Where(o => o.message_datetime == null).FirstOrDefault().message_datetime;
						//                  if (statusMessageTime != null)
						//                  {
						//                      originalStatus = alephBookingResponse.status_messages.OrderByDescending(f => f.message_datetime.ToDateTime().Value).FirstOrDefault().status_message_type;
						//                  }
						//                  else
						//                  {
						//                      originalStatus = alephBookingResponse.status_messages.LastOrDefault().status_message_type;
						//                  }
					}
					//status = originalStatus;
					//Map Aleph Status to Vtod Status
                    string device=null;
                    string version=null;
                    if (request.TPA_Extensions!=null)
                    {
                        if (!string.IsNullOrEmpty(request.TPA_Extensions.Device))
                        {
                            device = request.TPA_Extensions.Device;
                        }
                        if (!string.IsNullOrEmpty(request.TPA_Extensions.Source))
                        {
                            version = request.TPA_Extensions.Source;
                        }
                    }
                    status = utility.ConvertTripStatus(originalStatus, device, version);
					#endregion Retrieve Current Status


					#region Get List of Status for which no Driver or Vehicle info is to be retrieved from the Web.Config

					string noVehicleInfoStatus = string.Empty;
					List<string> noVehicleInfoList = new List<string>();
					bool loadNoVehicleInfo = false;

					//Get list from Web.Config
					try
					{
						noVehicleInfoStatus = ConfigurationManager.AppSettings["AlephAPI_No_Vehicle_Info_Status"];
					}
					catch { }

					//Convert string of status into List<string> 
					if (!string.IsNullOrWhiteSpace(noVehicleInfoStatus))
					{
						try
						{
							noVehicleInfoList = noVehicleInfoStatus.ToLower().Split(',').ToList();
						}
						catch { }
					}

					//Check if current status is in the list of status for which no driver or vehicle info is to be retrieved
					if ((noVehicleInfoList != null) && (noVehicleInfoList.Count > 0))
						loadNoVehicleInfo = noVehicleInfoList.Contains(originalStatus.ToLower());

					#endregion Get List of Status for which no Driver or Vehicle info is to be retrieved from the Web.Config



					//Generate OTA response object
					response = new OTA_GroundResRetrieveRS();
					response.Success = new Success();
					response.EchoToken = asp.request.EchoToken;
					response.Target = asp.request.Target;
					response.Version = asp.request.Version;
					response.PrimaryLangID = asp.request.PrimaryLangID;

					response.TPA_Extensions = new TPA_Extensions();
					response.TPA_Extensions.Driver = (alephBookingResponse.assigned_vehicle == null) ? null : utility.ExtractDriverName(alephBookingResponse.assigned_vehicle);
					response.TPA_Extensions.Vehicles = new Vehicles();
					response.TPA_Extensions.Vehicles.Items = new List<Vehicle>();
                    
                    #region Confirmations
                    try
                    {
                        Int64 tripid = 0;
                        using (var db = new DataAccess.VTOD.VTODEntities())
                        {
                            string dispatchtripid = request.Reference.FirstOrDefault().ID;
                            var aleph_trip = db.ecar_trip.Where(l => l.DispatchTripId == dispatchtripid);
                            if (aleph_trip != null)
                            {
                                if (aleph_trip.Any())
                                {
                                    tripid = aleph_trip.FirstOrDefault().Id;
                                    if(asp!=null)
                                    {
                                        if(asp.Trip!=null)
                                        {
                                            if (asp.Trip.Id == 0)
                                                asp.Trip.Id = tripid;
                                        }
                                    }
                                }
                            }
                        }
                        response.TPA_Extensions.Confirmation = new Confirmation();
                        response.TPA_Extensions.Confirmation.ID = tripid.ToString();
                        response.TPA_Extensions.Confirmation.Type = ConfirmationType.Confirmation.ToString();
                        response.TPA_Extensions.Confirmation.TPA_Extensions = new TPA_Extensions();
                        response.TPA_Extensions.Confirmation.TPA_Extensions.Confirmations = new List<Confirmation>();
                        response.TPA_Extensions.Confirmation.TPA_Extensions.Confirmations.Add(new Confirmation { ID = request.Reference.FirstOrDefault().ID, Type = ConfirmationType.DispatchConfirmation.ToString() });

                    }
                    catch (Exception ex)
                    {
                        logger.Error("Error in retriving the Confirmations");
                    }
                    #endregion
                    
					//calculate ETA
					int? minutesAway = null;

					//Vehicle
					var vehicle = new Vehicle();

					vehicle.Geolocation = new Geolocation();

					AlephVehicleLocationRS vInfo = null;

					if ((alephBookingResponse.assigned_vehicle != null) && (!loadNoVehicleInfo))
					{
						vehicle.ID = alephBookingResponse.assigned_vehicle.car_no;


						#region Get Vehicle Location from Aleph
						try
						{
							var alephAPICall = new AlephAPICall();
							vInfo = alephAPICall.GetVehicleLocation(asp.CorporateProviderID, asp.CorporateCompanyName, asp.CorporateProviderAccountID, asp.CorporateProfileID, asp.CorporateEmail, corporateKey, alephVendorTripID);
						}
						catch (Exception ex)
						{
							//failure 
							logger.ErrorFormat("Message:{0}", ex.Message);
							logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
							logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
							//response = null;
						}
						#endregion Get Vehicle Location from Aleph


						if ((vInfo != null) && (vInfo.latitude != null) && (vInfo.longitude != null))
						{
							vehicle.Geolocation.Lat = Convert.ToDecimal(vInfo.latitude);
							vehicle.Geolocation.Long = Convert.ToDecimal(vInfo.longitude);
						}
					}
					else
					{
						logger.Info("No vehicle information from response");
					}


					#region Calculate ETA



					#region Others

					//fare
					decimal? totalFare = null;

					if (status.Trim().ToLower() == ECarTripStatusType.Completed.Trim().ToLower() || status.Trim().ToLower() == ECarTripStatusType.Fare.Trim().ToLower())
					{
						totalFare = asp.Trip.TotalFareAmount;
					}

					//Convert status to front-end status
					string frontEndStatus = string.Empty;
					int UserID = utility.GetUserDetails(tokenVTOD.Username);
					if (UserID != 0)
					{
						frontEndStatus = utility.ConvertTripStatusMessageForFrontEndDevice(UserID, status);
					}
					Status s = null;
					//Status s = new Common.DTO.OTA.Status { Value = status, Fare = succ.TripInfo[0].FareAmount.ToDecimal() };
					if (!string.IsNullOrWhiteSpace(frontEndStatus) && !string.IsNullOrEmpty(frontEndStatus))
						s = new Common.DTO.OTA.Status { Value = frontEndStatus };
					else
						s = new Common.DTO.OTA.Status { Value = status };
					///Status s = new Common.DTO.OTA.Status { Value = status };
					if (totalFare.HasValue)
					{
						s.Fare = totalFare.Value;
					}
					response.TPA_Extensions.Statuses = new Statuses();
					response.TPA_Extensions.Statuses.Status = new List<Status>();
					response.TPA_Extensions.Statuses.Status.Add(s);
					response.TPA_Extensions.Contacts = new Contacts();
					response.TPA_Extensions.Contacts.Items = new List<Contact>();

					if ((alephBookingResponse != null) && (alephBookingResponse.assigned_vehicle != null))
					{
						var fleetContact = new Contact { Type = Common.DTO.Enum.ContactType.Dispatch.ToString(), Name = alephBookingResponse.assigned_vehicle.company_name, Telephone = new Telephone { PhoneNumber = alephBookingResponse.assigned_vehicle.company_phone_number } };
						response.TPA_Extensions.Contacts.Items.Add(fleetContact);
						var driverContact = new Contact { Type = Common.DTO.Enum.ContactType.Driver.ToString(), Name = alephBookingResponse.assigned_vehicle.driver_first_name + " " + alephBookingResponse.assigned_vehicle.driver_last_name, Telephone = new Telephone { PhoneNumber = alephBookingResponse.assigned_vehicle.driver_phone_number } };
						response.TPA_Extensions.Contacts.Items.Add(driverContact);
					}

					if (asp.Trip != null && alephBookingResponse.pickup != null && alephBookingResponse.pickup.Longitude.Value != 0 && alephBookingResponse.pickup.Latitude.Value != 0)
					{
						response.TPA_Extensions.PickupLocation = new Pickup_Dropoff_Stop();
						response.TPA_Extensions.PickupLocation.Address = new Common.DTO.OTA.Address();
						response.TPA_Extensions.PickupLocation.Address.Latitude = alephBookingResponse.pickup.Latitude.Value.ToString();
						response.TPA_Extensions.PickupLocation.Address.Longitude = alephBookingResponse.pickup.Longitude.Value.ToString();
					}
					#endregion

					if ((vehicle.Geolocation.Lat != 0 && vehicle.Geolocation.Long != 0)
						&&
						(alephBookingResponse.pickup.Latitude.HasValue && alephBookingResponse.pickup.Longitude.HasValue)
					)
					{
						MapService ms = new MapService();
						double d = ms.GetDirectDistanceInMeter(alephBookingResponse.pickup.Latitude.Value.ToInt64(), alephBookingResponse.pickup.Longitude.Value.ToInt64(), Convert.ToDouble(vehicle.Geolocation.Lat), Convert.ToDouble(vehicle.Geolocation.Long));
						var rc = new UDI.SDS.RoutingController(asp.tokenVTOD, null);
						UDI.SDS.RoutingService.Point start = new UDI.SDS.RoutingService.Point { Latitude = alephBookingResponse.pickup.Latitude.Value.ToDouble(), Longitude = alephBookingResponse.pickup.Longitude.Value.ToDouble() };
						UDI.SDS.RoutingService.Point end = new UDI.SDS.RoutingService.Point { Latitude = Convert.ToDouble(vehicle.Geolocation.Lat), Longitude = Convert.ToDouble(vehicle.Geolocation.Long) };
						var result = rc.GetRouteMetrics(start, end);
						vehicle.MinutesAway = result.TotalMinutes;
						if (vehicle.MinutesAway != null) minutesAway = Convert.ToInt32(vehicle.MinutesAway);
					}

					//Add vehicle info only if it is a valid status type
					if (!loadNoVehicleInfo)
					{
						response.TPA_Extensions.Vehicles.Items.Add(vehicle);
					}

					#endregion

					//Others

					//update ecar trip
					string driverName = String.Empty;
					string carNo = String.Empty;
					string driverId = String.Empty;

					if ((alephBookingResponse != null) && (alephBookingResponse.assigned_vehicle != null) && (!loadNoVehicleInfo))
					{
						driverName = alephBookingResponse.assigned_vehicle.driver_first_name + " " + alephBookingResponse.assigned_vehicle.driver_last_name;
						carNo = alephBookingResponse.assigned_vehicle.car_no;
						driverId = alephBookingResponse.assigned_vehicle.driver_call_number;
					}

					//utility.UpdateECarTrip(asp.Trip.Id, carNo, minutesAway, asp.Trip.ecar_trip.DispatchTripId, driverName);


					DateTime? statusTime = null;
					DateTime? tripStartTime = null;
					DateTime? tripEndTime = null;

					//Get the status time of the current status from the Aleph response
					try
					{
						if ((alephBookingResponse.status_messages != null) && (alephBookingResponse.status_messages.Count > 0))
						{
							statusTime = alephBookingResponse.status_messages.OrderByDescending(f => f.message_datetime.ToDateTime().Value).First().message_datetime.ToDateTime().Value;
						}
					}
					catch { }

					if (!statusTime.HasValue || statusTime.Value <= DateTime.MinValue)
					{
						//if (asp.Fleet != null && asp.Fleet.ServerUTCOffset != null)
						//{
						//    statusTime = DateTime.UtcNow.FromUtcNullAble(asp.Fleet.ServerUTCOffset);
						//}
					}


					//Walk through the status message list looking for a Trip start(Load) or Trip end(Unload) status message
					if ((alephBookingResponse.status_messages != null) && (alephBookingResponse.status_messages.Count > 0))
					{
						////Initialize to current values in the DB
						//tripStartTime = alephBookingResponse.;
						//tripEndTime = asp.Trip.TripEndTime;

						//foreach (AlephStatusMessage msg in alephBookingResponse.status_messages)
						//{
						//    //If a 'load' message is found and the Trip Start value in the DB is currently 'null'
						//    if ((msg.status_message_type.ToLower().Equals("load")) && (asp.Trip.TripStartTime == null))
						//    {
						//        try
						//        {
						//            //tripStartTime = Convert.ToDateTime(msg.message_datetime);
						//        }
						//        catch { }
						//    }
						//    //If an 'unload' message is found and the Trip End value in the DB is currently 'null'
						//    else if ((msg.status_message_type.ToLower().Equals("unload")) && (asp.Trip.TripEndTime == null))
						//    {
						//        try
						//        {
						//            //tripEndTime = Convert.ToDateTime(msg.message_datetime);
						//        }
						//        catch { }
						//    }
						//}
					}

					if (status.ToUpperInvariant() == ECarTripStatusType.Canceled.ToUpperInvariant())
					{

					}
					else
					{
						string vehId = string.Empty;
						decimal? vehLat = null;
						decimal? vehLon = null;
						int? vehETA = null;

						try
						{
							vehId = response.TPA_Extensions.Vehicles.Items.First().ID;
							vehETA = response.TPA_Extensions.Vehicles.Items.First().MinutesAway.ToInt32();
							vehLat = response.TPA_Extensions.Vehicles.Items.First().Geolocation.Lat;
							vehLon = response.TPA_Extensions.Vehicles.Items.First().Geolocation.Long;
						}
						catch { }

                        //Save trip status
                        //utility.SaveTripStatus(asp.Trip.Id, status, statusTime, vehId, vehLat, vehLon, totalFare, vehETA, driverName, string.Empty, originalStatus, driverId);

                        //Save final status
                        if (status.ToUpperInvariant() == ECarTripStatusType.Assigned.ToUpperInvariant() || status.ToUpperInvariant() == ECarTripStatusType.Accepted.ToUpperInvariant())
                            utility.UpdateFinalStatus(asp.Trip.Id, status, totalFare,asp.Trip.Gratuity, tripStartTime, tripEndTime, vehETA);
                        else
                            if (asp!= null && asp.Trip!=null)
                            {
                                if (asp.Trip.Id>0)
                                {
                                    utility.UpdateFinalStatus(asp.Trip.Id, status, totalFare, asp.Trip.Gratuity, tripStartTime, tripEndTime, null);
                                }
                            }

                        #region Trip Duration
                        try
						{
							if (tripEndTime != null && tripStartTime != null)
							{
								//string.Format("{0:00}:{1:00}:{2:00}", sdsResult.TripDuration.Hours, sdsResult.TripDuration.Minutes, sdsResult.TripDuration.Seconds);
								var duration = (tripEndTime.Value - tripStartTime.Value);
								response.TPA_Extensions.TripInfo = new TripInfo { Duration = string.Format("{0:00}:{1:00}:{2:00}", duration.Hours, duration.Minutes, duration.Seconds) };
							}
						}
						catch (Exception)
						{

							throw;
						}
						#endregion
					}
                    #region Logic For Modify Reservation
                    DateTime tripTime = asp.Trip.PickupDateTimeUTC.HasValue ? asp.Trip.PickupDateTimeUTC.Value : DateTime.UtcNow.AddMinutes(-100);
                    DateTime currentTime = DateTime.UtcNow;
                    double timeDiff = tripTime.Subtract(currentTime).TotalMinutes;
                    if (timeDiff > 60)
                    {
                        if (status == UDI.VTOD.Domain.ECar.Const.ECarTripStatusType.Completed || status== UDI.VTOD.Domain.ECar.Const.ECarTripStatusType.Canceled || status == UDI.VTOD.Domain.ECar.Const.ECarTripStatusType.NoShow)
                        {
                            response.TPA_Extensions.IsModify = false;
                        }
                        else
                        {
                            response.TPA_Extensions.IsModify = true;
                        }

                    }
                    else
                    {
                        response.TPA_Extensions.IsModify = false;
                    }

                    #endregion

                }
                else
				{
					//unable to do status
					var vtodException = VtodException.CreateException(ExceptionType.ECar, 303);
					logger.WarnFormat("Unable to retrieve Aleph booking status");
					throw vtodException; // new ECarException(Messages.ECar_StatusFailure);
				}
               


            }
            catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);

				throw new ECarException(ex.Message);
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);



			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_GreenTomato, "Status");
			#endregion

			return response;
		}

		public Common.DTO.OTA.OTA_GroundCancelRS Cancel(Common.DTO.OTA.TokenRS tokenVTOD, Common.DTO.OTA.OTA_GroundCancelRQ request, ECarCancelParameter eccp)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			OTA_GroundCancelRS response = null;
			//AlephECarUtility utility = new AlephECarUtility(logger);

			string dispatchConfirmation = null;
			try
			{
				#region Get dispatchConfirmation
				dispatchConfirmation = request.Reservation.UniqueID.FirstOrDefault().Type;
				#endregion

				if (!string.IsNullOrWhiteSpace(dispatchConfirmation))
				{
					if (dispatchConfirmation.ToLower() == "dispatchconfirmation")
					{
						response = CancelWithDispatchConfirmation(tokenVTOD, request, eccp);
					}
					else
					{
						response = CancelWithConfirmation(tokenVTOD, request, eccp);
					}
				}


				//#region [ECar 3.0] Validate required fields
				//AlephCancelParameter acp = new AlephCancelParameter { Fleet = eccp.Fleet, serviceAPIID = eccp.serviceAPIID };
				//if (!utility.ValidateCancelRequest(tokenVTOD, request, ref acp))
				//{
				//    throw VtodException.CreateException(ExceptionType.ECar, 5005);
				//}
				//#endregion

				//#region Track
				//if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
				//#endregion

				//try
				//{
				//    DateTime? requestedLocalTime = null;
				//    if (acp.Fleet != null && acp.Fleet.ServerUTCOffset != null)
				//    {
				//        requestedLocalTime = DateTime.UtcNow.FromUtcNullAble(acp.Fleet.ServerUTCOffset);
				//    }
				//    try
				//    {

				//        if (utility.IsAbleToCancelThisTrip(acp.Trip.FinalStatus))
				//        {


				//            #region Get Corporate Key

				//            string corporateKey = string.Empty;

				//            try
				//            {
				//                corporateKey = AlephSDSCall.GetCorporateAccountKey(tokenVTOD, TrackTime, acp.CorporateEmail);
				//            }
				//            catch (Exception ex1)
				//            {
				//                throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(ex1.Message));
				//            }

				//            #endregion Get Corporate Key

				//            //string corporateKey = "cfd45da8-27c9-41b5-b4cf-15938eb792a0";

				//            #region Call Aleph API

				//            AlephBookingRS alephCancelResponse = null;

				//            try
				//            {
				//                var alephAPICall = new AlephAPICall();
				//                alephCancelResponse = alephAPICall.CancelReservation(acp.CorporateProviderID, acp.CorporateCompanyName, acp.CorporateProviderAccountID, acp.CorporateProfileID, acp.CorporateEmail, corporateKey, acp.Trip.ecar_trip.DispatchTripId);
				//            }
				//            catch (Exception ex)
				//            {
				//                string msg = string.Format("Unable to cancel this trip. tripID={0}, errormessage={1}", acp.Trip.Id, ex.InnerException.Message);
				//                logger.Warn(msg);

				//                throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(ex.Message));
				//            }

				//            #endregion Call Aleph API




				//            if (alephCancelResponse != null)
				//            {
				//                response = new OTA_GroundCancelRS();
				//                response.Success = new Success();
				//                response.EchoToken = acp.request.EchoToken;
				//                response.Target = acp.request.Target;
				//                response.Version = acp.request.Version;
				//                response.PrimaryLangID = acp.request.PrimaryLangID;

				//                response.Reservation = new Reservation();
				//                response.Reservation.CancelConfirmation = new CancelConfirmation();
				//                response.Reservation.CancelConfirmation.UniqueID = new UniqueID { ID = acp.Trip.Id.ToString() };

				//                #region Set final status
				//                utility.UpdateFinalStatus(acp.Trip.Id, ECarTripStatusType.Canceled, null, null, null);
				//                #endregion

				//                #region Put status into ecar_trip_status
				//                utility.SaveTripStatus(acp.Trip.Id, ECarTripStatusType.Canceled, requestedLocalTime, null, null, null, null, null, null, string.Empty, string.Empty, null);
				//                #endregion
				//            }
				//            else
				//            {
				//                logger.WarnFormat("Unable to cancel this trip. Trip Id ={0}, dispatchId={1}", acp.Trip.Id, acp.Trip.ecar_trip.DispatchTripId);
				//                throw VtodException.CreateException(ExceptionType.ECar, 5001);// new ECarException(Messages.ECar_UnableToCancelGreenTomato);
				//            }

				//        }
				//        else
				//        {
				//            throw new ECarException(Messages.ECar_CancelFailure);
				//        }
				//}
				//catch (Exception ex)
				//{

				//    logger.ErrorFormat("Message:{0}", ex.Message);
				//    logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				//    logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				//    throw;
				//}

				//sw.Stop();
				//logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw;
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);


			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_GreenTomato, "Cancel");
			#endregion

			return response;
		}

		public Common.DTO.OTA.OTA_GroundAvailRS GetVehicleInfo(Common.DTO.OTA.TokenRS tokenVTOD, Common.DTO.OTA.OTA_GroundAvailRQ request, ECarGetVehicleInfoParameter egvip)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			var response = new OTA_GroundAvailRS();
			response.Success = new Success();
			response.TPA_Extensions = new TPA_Extensions();
			response.TPA_Extensions.Vehicles = new Vehicles();
			response.TPA_Extensions.Vehicles.Items = new List<Vehicle>();



			AlephECarUtility utility = new AlephECarUtility(logger);

			#region [ECar 4.0] Validate required fields
			AlephGetVehicleInfoParameter agvip = new AlephGetVehicleInfoParameter { Fleet = egvip.Fleet, serviceAPIID = egvip.serviceAPIID };
			if (!utility.ValidateGetVehicleInfoRequest(tokenVTOD, request, ref agvip))
			{
				string error = string.Format("This GetVehicleInfo request is invalid.");
				throw new ValidationException(error);
			}
			#endregion


			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
			#endregion

			try
			{
				//foreach (var fleet in agvip.fleetList)
				//{
				var fleet = agvip.Fleet;
				uint NearestVehicleETA = 60;
				List<Vehicle> tempVehicles = new List<Vehicle>();


				//get vehicles for this fleet
				DateTime? requestedLocalTime = null;
				if (fleet != null && fleet.ServerUTCOffset != null)
				{
					requestedLocalTime = DateTime.UtcNow.FromUtcNullAble(fleet.ServerUTCOffset);
				}
				try
				{
					//Set up default value for MaxVehiclesToReturn
					const int MaxVehiclesToReturnDefault = 20;
					int vehiclesToReturn = MaxVehiclesToReturnDefault;


					//Set vehiclesToReturn value
					try
					{
						vehiclesToReturn = Convert.ToInt32(ConfigurationManager.AppSettings["NumberOfReturnedVehicle"]);
					}
					catch
					{
						vehiclesToReturn = MaxVehiclesToReturnDefault;
					}


					#region Get Corporate Key

					string corporateKey = string.Empty;

					try
					{
						corporateKey = AlephSDSCall.GetCorporateAccountKey(tokenVTOD, TrackTime, agvip.CorporateEmail);
					}
					catch (Exception ex1)
					{
						throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(ex1.Message));
					}

					#endregion Get Corporate Key

					//string corporateKey = "cfd45da8-27c9-41b5-b4cf-15938eb792a0";

					#region Call Aleph API

					AlephGetAvailableVehiclesRS alephGetAvailableVehiclesResponse = null;

					try
					{
						var alephAPICall = new AlephAPICall();
						alephGetAvailableVehiclesResponse = alephAPICall.GetAvailableVehicles(agvip.CorporateProviderID, Convert.ToDecimal(agvip.Latitude), Convert.ToDecimal(agvip.Longtitude), Convert.ToDecimal(agvip.Radius), vehiclesToReturn, "", agvip.CorporateEmail, corporateKey);
					}
					catch (Exception ex)
					{
						string msg = string.Format("Unable to retrieve available vehicles");
						logger.Warn(msg);

						throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(ex.Message));
					}

					#endregion Call Aleph API



					if (alephGetAvailableVehiclesResponse != null)
					{
						if ((alephGetAvailableVehiclesResponse.vehicles != null) && (alephGetAvailableVehiclesResponse.vehicles.Count > 0))
						{

							foreach (var item in alephGetAvailableVehiclesResponse.vehicles.OrderBy(s => s.distance))
							{
								var vehicle = new Vehicle();
								vehicle.Geolocation = new Geolocation();
								vehicle.Geolocation.Long = decimal.Parse(item.longitude.ToString());
								vehicle.Geolocation.Lat = decimal.Parse(item.latitude.ToString());

								vehicle.FleetID = fleet.Id.ToString();
								vehicle.ID = item.car_no;
								vehicle.VehicleStatus = "Available";
								vehicle.VehicleType = item.vehicle_type;
								vehicle.Distance = item.distance;
								vehicle.MinutesAway = item.ETA;

								if (item.ETA < NearestVehicleETA) NearestVehicleETA = Convert.ToUInt32(System.Math.Ceiling(item.ETA));

								tempVehicles.Add(vehicle);
							}
						}
						else
						{
							logger.Info("Found no vehicles");
							//response.TPA_Extensions.Vehicles.NumberOfVehicles = "0";
						}

					}
					else
					{
						var vtodExcaption = VtodException.CreateException(ExceptionType.ECar, 4001);
						logger.Warn(vtodExcaption.ExceptionMessage.Message);
						throw vtodExcaption; // throw new ECarException(Messages.ECar_GetVehicleInfoFailure);
					}

				}
				catch (Exception ex)
				{

					logger.ErrorFormat("Message:{0}", ex.Message);
					logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
					logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
					response = null;
				}


				//add them in to the response
				if (Convert.ToUInt32(NearestVehicleETA) < Convert.ToUInt32(response.TPA_Extensions.Vehicles.NearestVehicleETA))
				{
					response.TPA_Extensions.Vehicles.NearestVehicleETA = NearestVehicleETA.ToString();
				}


				response.TPA_Extensions.Vehicles.Items = response.TPA_Extensions.Vehicles.Items.Concat(tempVehicles.AsEnumerable()).ToList();

				response.TPA_Extensions.Vehicles.NumberOfVehicles = tempVehicles.Count().ToString();
				//}
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw new ECarException(ex.Message);
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_GreenTomato, "GetVehicleInfo");
			#endregion

			return response;
		}
     
		public Common.DTO.OTA.OTA_GroundAvailRS GetEstimation(Common.DTO.OTA.TokenRS tokenVTOD, Common.DTO.OTA.OTA_GroundAvailRQ request, ECarGetEstimationParameter ecgep)
		{
			throw new NotImplementedException();
		}

		public Common.DTO.OTA.Fleet GetAvailableFleets(Common.DTO.OTA.TokenRS token, Common.DTO.OTA.UtilityAvailableFleetsRQ input)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			var response = new Fleet();

			try
			{
				var corporateEmail = input.Corporate.CorporateAccount.UserID;

				#region Get Corporate Key

				string corporateKey = string.Empty;

				try
				{
					corporateKey = AlephSDSCall.GetCorporateAccountKey(token, TrackTime, corporateEmail);
				}
				catch (Exception ex1)
				{
					throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(ex1.Message));
				}

				#endregion Get Corporate Key

				var lat = input.Address.Latitude.ToDecimal();
				var lon = input.Address.Longitude.ToDecimal();
				var alephAPICall = new AlephAPICall();
				var alephResponse = alephAPICall.GetAvailableFleets(lat, lon, corporateEmail, corporateKey, string.Empty);//  //(agvip.CorporateProviderID, Convert.ToDecimal(agvip.Latitude), Convert.ToDecimal(agvip.Longtitude), Convert.ToDecimal(agvip.Radius), vehiclesToReturn, "", agvip.CorporateEmail, corporateKey);

				if (alephResponse != null)
				{
					response.Type = FleetType.ExecuCar.ToString();
					response.Now = alephResponse.Corporate.Now.CanBook.ToString();
					response.Later = alephResponse.Corporate.Later.CanBook.ToString();
					response.RestrictNowBookingMins = alephResponse.Corporate.Later.MinimumMinutesFromNowBookingIsAllowed;
					response.DefaultRadius = System.Configuration.ConfigurationManager.AppSettings["DefaultRadiusForCorporate"].ToInt32();
				}
			}
			catch (Exception ex)
			{
				string msg = string.Format("Unable to retrieve available fleets");
				logger.Warn(msg);

				throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(ex.Message));
			}

			return response;

		}

        public Common.DTO.OTA.Fleet GetLeadTime(Common.DTO.OTA.TokenRS token, Common.DTO.OTA.UtilityGetLeadTimeRQ input)
        {
            logger.Info("Start");
            Stopwatch sw = new Stopwatch();
            sw.Start();
            var response = new Fleet();

            try
            {
                var corporateEmail = input.Corporate.CorporateAccount.UserID;

                #region Get Corporate Key

                string corporateKey = string.Empty;

                try
                {
                    corporateKey = AlephSDSCall.GetCorporateAccountKey(token, TrackTime, corporateEmail);
                }
                catch (Exception ex1)
                {
                    throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(ex1.Message));
                }

                #endregion Get Corporate Key

                var lat = input.Address.Latitude.ToDecimal();
                var lon = input.Address.Longitude.ToDecimal();
                var alephAPICall = new AlephAPICall();
                string providerID = string.Empty;
                string companyName = string.Empty;
                if (input.Corporate != null && input.Corporate.CorporateProfiles != null)
                {
                    if (input.Corporate.CorporateProfiles.CorporateProfileList != null && input.Corporate.CorporateProfiles.CorporateProfileList.Any())
                    {
                        if (input.Corporate.CorporateProfiles.CorporateProfileList.FirstOrDefault().ProviderID != null)
                        {
                            providerID = input.Corporate.CorporateProfiles.CorporateProfileList.FirstOrDefault().ProviderID.ToString();
                        }
                        if (input.Corporate.CorporateProfiles.CorporateProfileList.FirstOrDefault().CompanyName != null)
                        {
                            companyName = input.Corporate.CorporateProfiles.CorporateProfileList.FirstOrDefault().CompanyName;
                        }
                    }
                }
                var alephResponse = alephAPICall.GetLeadTime(lat, lon, corporateEmail, corporateKey, providerID, companyName);
                if (alephResponse != null)
                {
                    response.Type = FleetType.ExecuCar.ToString();
                    if (alephResponse.providers != null && alephResponse.providers.Any())
                    {
                        if (alephResponse.providers.FirstOrDefault().companies != null && alephResponse.providers.FirstOrDefault().companies.Any())
                        {
                            if (alephResponse.providers.FirstOrDefault().companies.FirstOrDefault().config_options != null && alephResponse.providers.FirstOrDefault().companies.FirstOrDefault().config_options.Any())
                            {
                                if (alephResponse.providers.FirstOrDefault().companies.FirstOrDefault().config_options[0].Name == "book_later_lead_time_minutes")
                                response.RestrictNowBookingMins = alephResponse.providers.FirstOrDefault().companies.FirstOrDefault().config_options[0].Value.ToInt32();
                            }
                           
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string msg = string.Format("Unable to retrieve lead time");
                logger.Warn(msg);

                throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(ex.Message));
            }

            return response;

        }
        public Common.DTO.OTA.UtilityGetClientTokenRS GetCorporateClientToken(Common.DTO.OTA.TokenRS token, Common.DTO.OTA.UtilityGetClientTokenRQ input)
        {
            logger.Info("Start");
            Stopwatch sw = new Stopwatch();
            sw.Start();
            var response = new UtilityGetClientTokenRS();

            try
            {
                var corporateEmail = input.Corporate.CorporateAccount.UserID;

                #region Get Corporate Key

                string corporateKey = string.Empty;

                try
                {
                    corporateKey = AlephSDSCall.GetCorporateAccountKey(token, TrackTime, corporateEmail);
                }
                catch (Exception ex1)
                {
                    throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(ex1.Message));
                }

                #endregion Get Corporate Key
                var alephAPICall = new AlephAPICall();
                string profileID = string.Empty;
                if (input.Corporate != null && input.Corporate.CorporateProfiles != null)
                {
                    if (input.Corporate.CorporateProfiles.CorporateProfileList != null && input.Corporate.CorporateProfiles.CorporateProfileList.Any())
                    {
                        if (input.Corporate.CorporateProfiles.CorporateProfileList.FirstOrDefault().ProfileID != null)
                        {
                            profileID = input.Corporate.CorporateProfiles.CorporateProfileList.FirstOrDefault().ProfileID;
                        }
                    }
                }
                var alephResponse = alephAPICall.GetClientToken(corporateEmail, corporateKey, profileID);
                if (alephResponse != null)
                {
                    if (alephResponse != null && alephResponse.client_token != null)
                    {
                        response.ClientToken = alephResponse.client_token;
                    }
                }
            }
            catch (Exception ex)
            {
                string msg = string.Format("Unable to retrieve client token");
                logger.Warn(msg);

                throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(ex.Message));
            }

            return response;

        }
        public Common.DTO.OTA.OTA_GroundBookRS ModifyBook(Common.DTO.OTA.TokenRS tokenVTOD, Common.DTO.OTA.OTA_GroundBookRQ request, out List<Tuple<string, int, long>> disptach_Rez_VTOD, ECarBookingParameter ecbp)
		{

			AlephBookingRS alephBookingResponse = null;
			Stopwatch sw = new Stopwatch();
			sw.Start();
			OTA_GroundBookRS response = null;
			disptach_Rez_VTOD = new List<Tuple<string, int, long>>();
			AlephECarUtility utility = new AlephECarUtility(logger);
			string postContent = "";
			string errMsg = "";
			string alephVendorTripID = string.Empty;
			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
			string tripid = string.Empty;
			#endregion



			#region Validate Aleph Inputs
			AlephBookingParameter ecb = new AlephBookingParameter();
			try
			{
				var rateQualifier = request.GroundReservations.First().RateQualifiers.First();
				if (rateQualifier.SpecialInputs != null)
				{
					var CorporateEmail = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateEmail".ToLowerInvariant()).FirstOrDefault();
					if (CorporateEmail != null)
					{
						ecb.CorporateEmail = CorporateEmail.Value;
					}
					else
					{
						ecb.CorporateEmail = null;
						logger.Warn(Messages.ECar_Common_InvalidReservationService);
						throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
					}
					//CorporateProviderID
					var CorporateProviderID = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateProviderID".ToLowerInvariant()).FirstOrDefault();
					if (CorporateProviderID != null)
					{
						ecb.CorporateProviderID = CorporateProviderID.Value;
					}
					else
					{
						if (!ecbp.PickupNow)
						{
							ecb.CorporateProviderID = null;
							logger.Warn(Messages.ECar_Common_InvalidReservationService);
							throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
						}
					}
					//CorporateCompanyName
					var CorporateCompanyName = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateCompanyName".ToLowerInvariant()).FirstOrDefault();
					if (CorporateCompanyName != null)
					{
						ecb.CorporateCompanyName = CorporateCompanyName.Value;
					}
					else
					{
						if (!ecbp.PickupNow)
						{
							ecb.CorporateCompanyName = null;
							logger.Warn(Messages.ECar_Common_InvalidReservationService);

							throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
						}
					}
					//CorporateProviderAccountID
					var CorporateProviderAccountID = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateProviderAccountID".ToLowerInvariant()).FirstOrDefault();
					if (CorporateProviderAccountID != null)
					{
						ecb.CorporateProviderAccountID = CorporateProviderAccountID.Value;
					}
					else
					{
						if (!ecbp.PickupNow)
						{
							ecb.CorporateProviderAccountID = null;
							logger.Warn(Messages.ECar_Common_InvalidReservationService);
							throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
						}
					}
					//CorporateProfileID
					var CorporateProfileID = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateProfileID".ToLowerInvariant()).FirstOrDefault();
					if (CorporateProfileID != null)
					{
						ecb.CorporateProfileID = CorporateProfileID.Value;
					}
					else
					{
						if (!ecbp.PickupNow)
						{
							ecb.CorporateProfileID = null;
							logger.Warn(Messages.ECar_Common_InvalidReservationService);
							throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
						}
					}
					//CorporatePaymentMethod
					var CorporatePaymentMethod = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporatePaymentMethod".ToLowerInvariant()).FirstOrDefault();
					if (CorporatePaymentMethod != null)
					{
						ecb.CorporatePaymentMethod = CorporatePaymentMethod.Value;
					}
					else
					{
						if (!ecbp.PickupNow)
						{
							ecb.CorporatePaymentMethod = null;
							logger.Warn(Messages.ECar_Common_InvalidReservationService);
							throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
						}
						else
						{
							ecb.CorporatePaymentMethod = "Voucher";
						}
					}
					//CorporateSpecialInstructions
					var CorporateSpecialInstructions = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateSpecialInstructions".ToLowerInvariant()).FirstOrDefault();
					if (CorporateSpecialInstructions != null)
					{
						ecb.CorporateSpecialInstructions = CorporateSpecialInstructions.Value;
					}
					else
					{
						ecb.CorporateSpecialInstructions = null;
						logger.Warn(Messages.ECar_Common_InvalidReservationService);
						throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
					}
				}
				else
				{
					throw VtodException.CreateException(ExceptionType.ECar, 20022);
				}


			}
			catch (Exception ex)
			{
				logger.Error("Aleph: Error While Validating Aleph Profile Info", ex);
				logger.Warn("Error while validating Aleph profile inputs.");
				throw VtodException.CreateException(ExceptionType.ECar, 20023);
			}
			#endregion

			try
			{
				#region Get Corporate Key

				string corporateKey = string.Empty;

				try
				{
					corporateKey = AlephSDSCall.GetCorporateAccountKey(tokenVTOD, TrackTime, ecb.CorporateEmail);
				}
				catch (Exception ex1)
				{
					throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(ex1.Message));
				}

				#endregion Get Corporate Key

				#region Get dispatchConfirmation
				string dispatchConfirmation = request.References.FirstOrDefault().Type;
				string dispatchTripID = null;
				#endregion

				AlephStatusParameter asp = new AlephStatusParameter { };

				if (request.References != null && request.References.Any())
				{
					if (request.References.FirstOrDefault().Type.Trim().ToLower() == Common.DTO.Enum.ConfirmationType.DispatchConfirmation.ToString().Trim().ToLower())
					{
						dispatchTripID = request.References.FirstOrDefault().ID;
						if (dispatchTripID.Contains("##"))
						{
							if (dispatchTripID.IndexOf("##") != -1)
							{
								alephVendorTripID = dispatchTripID.Substring(dispatchTripID.IndexOf("##") + 2);
							}
							else
							{
								alephVendorTripID = dispatchTripID;
							}
						}
						else
						{
							alephVendorTripID = dispatchTripID;
						}
					}
					else
					{
						vtod_trip t = utility.GetTripDetails(request.References.FirstOrDefault().ID.ToInt64());
						if (t != null)
							alephVendorTripID = t.ecar_trip.DispatchTripId;
						if (alephVendorTripID.Contains("##"))
						{
							if (alephVendorTripID.IndexOf("##") != -1)
							{
								alephVendorTripID = alephVendorTripID.Substring(alephVendorTripID.IndexOf("##") + 2);
							}
							else
							{
								alephVendorTripID = t.ecar_trip.DispatchTripId;
							}
						}
						else
						{
							alephVendorTripID = t.ecar_trip.DispatchTripId;
						}
					}
				}
				var alephAPICall = new AlephAPICall();
				alephBookingResponse = alephAPICall.GetReservation(ecb.CorporateProviderID, ecb.CorporateCompanyName, ecb.CorporateProviderAccountID, ecb.CorporateProfileID, ecb.CorporateEmail, corporateKey, alephVendorTripID);
			}
			catch (Exception ex)
			{
				//string msg = string.Format("Unable to retrieve status for this trip. tripID={0}, errormessage={1}", asp.Trip.Id, ex.InnerException.Message);
				//logger.Warn(msg);

				throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(ex.Message));
			}
			#region [ECar 1.1] Validate required fields
			AlephBookingParameter abp = new AlephBookingParameter { Fleet = ecbp.Fleet, serviceAPIID = ecbp.serviceAPIID };
			if (!utility.ValidateModifyBookingRequest(tokenVTOD, request, ref abp, alephBookingResponse, alephVendorTripID, request.References.FirstOrDefault().Type.Trim().ToLower()))
			{
				throw new ValidationException(Messages.ECar_Common_InvalidBookingRequest);
			}
			#endregion

			#region [ECar 1.2] Book the trip
			DateTime? requestedLocalTime = null;
			if (abp.Fleet != null && abp.Fleet.ServerUTCOffset != null)
			{
				requestedLocalTime = DateTime.UtcNow.FromUtcNullAble(abp.Fleet.ServerUTCOffset);
			}

			try
			{
				Dictionary<string, string> requestHeader = new Dictionary<string, string>();
				Dictionary<string, string> responseHeader = new Dictionary<string, string>();


				//Change user first name and last name into Test for Test system
				if (ConfigurationManager.AppSettings["IsTest"].ToBool())
				{
					abp.FirstName = "Test";
					abp.LastName = "Test";
				}


				#region Book

				#region Generate Request Content
				if (abp.PickupNow)
				{
					postContent = ModifyReservationBookNow(abp);
				}
				else
				{
					postContent = ModifyReservation(abp);
				}
				#endregion

				#region Send HttpRequest and set response info object

				#region Get Corporate Key

				string corporateKey = string.Empty;

				try
				{
					corporateKey = AlephSDSCall.GetCorporateAccountKey(tokenVTOD, TrackTime, abp.CorporateEmail);
				}
				catch (Exception ex1)
				{
					throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(ex1.Message));
				}

				#endregion Get Corporate Key
				#region Call Aleph API

				try
				{
					var alephAPICall = new AlephAPICall();

					if (abp.PickupNow)
					{
						alephBookingResponse = alephAPICall.ModifyBookReservationNow(abp.CorporateProviderID, abp.CorporateCompanyName, abp.CorporateProviderAccountID, abp.CorporateProfileID, abp.CorporateEmail, corporateKey, postContent, alephBookingResponse.vendor_confirmation_no);
					}
					else
					{
						alephBookingResponse = alephAPICall.ModifyBookReservation(abp.CorporateProviderID, abp.CorporateCompanyName, abp.CorporateProviderAccountID, abp.CorporateProfileID, abp.CorporateEmail, corporateKey, postContent, alephBookingResponse.vendor_confirmation_no);
					}


					#region Update the Corporate Special Instructons and save to DB
					List<NameValue> specialInputs = null;
					if (!string.IsNullOrWhiteSpace(abp.WebServiceState)) specialInputs = abp.WebServiceState.JsonDeserialize<List<NameValue>>();
					//Update Corporate Special Instructions
					var newSpecialInputs = utility.InsertorUpdateCorporateSpecialInstructions(specialInputs, alephBookingResponse);
					//Save updated Corporate Special Instructions to DB
					utility.UpdateCorporateSpecialInstructions(newSpecialInputs.JsonSerialize(), abp.Trip.Id);
					#endregion

					#region save Aleph Company Info
					var companyInfo = new AlephCompanyInfo
					{
						company_name = alephBookingResponse.company_name,
						company_phone_number = alephBookingResponse.company_phone_number
					};
					utility.UpdateAlephCompanyInfo(abp.Trip.Id, companyInfo.JsonSerialize());
					#endregion
				}
				catch (Exception exc)
				{
					errMsg = string.Format("Unable to book this trip. tripID={0}, errormessage={1}", abp.Trip.Id, exc.InnerException.Message);
					logger.Warn(errMsg);

					throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(exc.Message));
				}

				#endregion Call Aleph API
				//success
				logger.Info("Booking successful");

				abp.Trip = utility.UpdateECarTrip(abp.Trip, alephBookingResponse.confirmation_no + "##" + alephBookingResponse.vendor_confirmation_no, ecbp.Fleet_TripCode);

				if (!string.IsNullOrWhiteSpace(alephBookingResponse.vendor_confirmation_no))
				{
					if (abp.EnableResponseAndLog)
					{
						GroundReservation reservation = abp.request.GroundReservations.FirstOrDefault();
						response = new OTA_GroundBookRS();
						response.EchoToken = abp.request.EchoToken;
						response.Target = abp.request.Target;
						response.Version = abp.request.Version;
						response.PrimaryLangID = abp.request.PrimaryLangID;

						response.Success = new Success();

						//Reservation
						response.Reservations = new List<Reservation>();
						Reservation r = new Reservation();
						r.Confirmation = new Confirmation();
						r.Confirmation.ID = abp.Trip.Id.ToString();
						r.Confirmation.Type = ConfirmationType.Confirmation.ToString();
						r.Passenger = reservation.Passenger;
						r.Service = reservation.Service;
						r.Confirmation.TPA_Extensions = new TPA_Extensions();
						r.Confirmation.TPA_Extensions.Confirmations = new List<Confirmation>();
						r.Confirmation.TPA_Extensions.Confirmations.Add(new Confirmation { ID = abp.Trip.ecar_trip.DispatchTripId, Type = ConfirmationType.DispatchConfirmation.ToString() });// = tbp.Trip.DispatchTripId;
						#region FleetInfo
						if (!string.IsNullOrWhiteSpace(alephBookingResponse.company_name) || !string.IsNullOrWhiteSpace(alephBookingResponse.company_phone_number))
						{
							r.TPA_Extensions = new TPA_Extensions();
							r.TPA_Extensions.Contacts = new Contacts();
							r.TPA_Extensions.Contacts.Items = new List<Contact>();
							var contact = new Contact { Type = Common.DTO.Enum.ContactType.Dispatch.ToString() };
							if (!string.IsNullOrWhiteSpace(alephBookingResponse.company_name))
							{
								contact.Name = alephBookingResponse.company_name;
							}
							if (!string.IsNullOrWhiteSpace(alephBookingResponse.company_phone_number))
							{
								contact.Telephone = new Telephone { PhoneNumber = alephBookingResponse.company_phone_number };
							}
							r.TPA_Extensions.Contacts.Items.Add(contact);
						}
						#endregion

						response.Reservations.Add(r);

						#region Put status into ecar_trip_status
						utility.SaveTripStatus(abp.Trip.Id, ECarTripStatusType.Booked, requestedLocalTime, null, null, null, null,null,null, null, null, string.Empty, string.Empty, null);
						#endregion
					}
					else
					{
						logger.Info("Disable response and log for booking");
					}
				}
				else
				{
					string error = string.Format("Cannot get DispatchTripId. Or dispatchTripId is white space.");
					logger.Error(error);
					utility.MarkThisTripAsError(abp.Trip.Id, error);
					throw new ECarException(error);
				}

				#endregion

				#endregion


			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);

				//change the status of trip
				utility.UpdateFinalStatus(abp.Trip.Id, ECarTripStatusType.Error, null, null, null);

				if (ex.Message != "Cannot get DispatchTripId. Or dispatchTripId is white space.") utility.MarkThisTripAsError(abp.Trip.Id, errMsg);

				throw ex;
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#endregion

			#region [ECar 1.3] Link  disptach_VTOD_TripIDs (for supershuttle system)
			if (response.Reservations != null && response.Reservations.Any())
			{
				var rez = response.Reservations.First();
				var DispatchConfirmationID = rez.Confirmation.TPA_Extensions.Confirmations.Where(s => s.Type.Trim().ToLower() == ConfirmationType.DispatchConfirmation.ToString().ToLower()).FirstOrDefault().ID;
				disptach_Rez_VTOD.Add(new Tuple<string, int, long>(DispatchConfirmationID, 0, rez.Confirmation.ID.ToInt64()));
			}
			#endregion

			response.Success = new Success();

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Aleph, "Book");
			#endregion

			return response;
		}
        public bool NotifyPickupDriver(TokenRS token, UtilityNotifyDriverRQ input)
        {
            throw new NotImplementedException();
        }
        public bool NotifyDriver(TokenRS token, UtilityNotifyDriverRQ input)
        {
            throw new NotImplementedException();
        }
        #endregion Public Methods

        #region Properties
        public TrackTime TrackTime { get; set; }
		#endregion

		#region Private Methods

		//Encode string as base64
		private static string Base64Encode(string plainText)
		{
			var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
			return System.Convert.ToBase64String(plainTextBytes);
		}

		//Encode username and password as base64
		private static string Base64EncodeForBasicAuthentication(string user, string pass)
		{
			var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(user.Trim() + ":" + pass.Trim());
			return System.Convert.ToBase64String(plainTextBytes);
		}


		private string BuildReservation(AlephBookingParameter ecbp)
		{
			AlephBookingRQ addReservationRequest = new AlephBookingRQ();

			AlephAddress pickupAddress = new AlephAddress();
			AlephAddress dropoffAddress = null;

            List<AlephAddress> stoplist = new List<AlephAddress>();

			addReservationRequest.is_asap = false;

			#region PickupAddress

			if (ecbp.PickupAddressType == AddressType.Address)
			{
				pickupAddress.street_no = ecbp.PickupAddress.StreetNo;
				pickupAddress.street_name = ecbp.PickupAddress.Street.Trim();
				pickupAddress.city = ecbp.PickupAddress.City;
				pickupAddress.state = ecbp.PickupAddress.State;
				pickupAddress.zip_code = ecbp.PickupAddress.ZipCode;
				pickupAddress.country = ecbp.PickupAddress.Country;
				pickupAddress.auto_complete = ecbp.PickupAddress.FullAddress;
				pickupAddress.address_type = "Street Address";
				pickupAddress.airport_pickup_point = ecbp.AirportPickupPoint;
				if (ecbp.Trip != null && ecbp.Trip.ecar_trip != null)
				{
					pickupAddress.pickup_point = ecbp.Trip.ecar_trip.RemarkForPickup;
				}

				if ((ecbp.PickupAddress.Geolocation.Latitude == 0) && (ecbp.PickupAddress.Geolocation.Longitude == 0))
				{
					pickupAddress.Latitude = null;
					pickupAddress.Longitude = null;
				}
				else
				{
					pickupAddress.Latitude = ecbp.PickupAddress.Geolocation.Latitude;
					pickupAddress.Longitude = ecbp.PickupAddress.Geolocation.Longitude;
				}

			}
			else if (ecbp.PickupAddressType == AddressType.Airport)
			{
				pickupAddress.address_type = "Airport";
				pickupAddress.airport_code = ecbp.PickupAirport;
				pickupAddress.airline = ecbp.PickupAirlineCode;
				pickupAddress.flight_number = ecbp.PickupAirlineFlightNumber;
				pickupAddress.airport_pickup_point = ecbp.AirportPickupPoint;

				if (ecbp.Trip != null && ecbp.Trip.ecar_trip != null)
				{
					pickupAddress.pickup_point = ecbp.Trip.ecar_trip.RemarkForPickup;
				}

				//pickupAddress.airport_pickup_point = "";
				//pickupAddress.city_of_origin = "";              
			}
			else
			{
				var ex = new ValidationException("The specified pickup address type is not supported.");
				string methodName = GetMethodName();
				int vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
				LogException(methodName, vtodErrorCode.ToString(), ex);
				throw new Exception(vtodErrorCode.ToString(), ex);
			}

			#endregion PickupAddress
           
			#region DropOffAddress

			if ((ecbp.DropOffAddressType != null) && (ecbp.DropOffAddress != null || !string.IsNullOrWhiteSpace(ecbp.DropoffAirport)))
			{
				dropoffAddress = new AlephAddress();

				if (ecbp.DropOffAddressType == AddressType.Address)
				{
					dropoffAddress.street_no = ecbp.DropOffAddress.StreetNo;
					dropoffAddress.street_name = ecbp.DropOffAddress.Street.Trim();
					dropoffAddress.city = ecbp.DropOffAddress.City;
					dropoffAddress.state = ecbp.DropOffAddress.State;
					dropoffAddress.zip_code = ecbp.DropOffAddress.ZipCode;
					dropoffAddress.country = ecbp.DropOffAddress.Country;
					dropoffAddress.auto_complete = ecbp.DropOffAddress.FullAddress;
					dropoffAddress.address_type = "Street Address";


					if ((ecbp.DropOffAddress.Geolocation.Latitude == 0) && (ecbp.DropOffAddress.Geolocation.Longitude == 0))
					{
						dropoffAddress.Latitude = null;
						dropoffAddress.Longitude = null;
					}
					else
					{
						dropoffAddress.Latitude = ecbp.DropOffAddress.Geolocation.Latitude;
						dropoffAddress.Longitude = ecbp.DropOffAddress.Geolocation.Longitude;
					}

				}
				else if (ecbp.DropOffAddressType == AddressType.Airport)
				{
					dropoffAddress.address_type = "Airport";
					dropoffAddress.airport_code = ecbp.DropoffAirport;
					dropoffAddress.airline = ecbp.DropOffAirlineCode;
					dropoffAddress.flight_number = ecbp.DropOffAirlineFlightNumber;
				}
			}

			#endregion DropOffAddress

            #region Stops

            if (ecbp.Stops != null)
            {
                foreach (var item in ecbp.Stops)
                {
                    if (!string.IsNullOrWhiteSpace(item.FullAddress))
                    {
                        AlephAddress stopAddress = new AlephAddress();
                        stopAddress.street_no = item.StreetNo;
                        stopAddress.street_name = item.Street.Trim();
                        stopAddress.city = item.City;
                        stopAddress.state = item.State;
                        stopAddress.zip_code = item.ZipCode;
                        stopAddress.country = item.Country;
                        stopAddress.auto_complete = item.FullAddress;
                        stopAddress.address_type = "Street Address";
                        stopAddress.pickup_point = item.pickup_point;
                        if ((item.Geolocation.Latitude == 0) && (item.Geolocation.Longitude == 0))
				        {
					        stopAddress.Latitude = null;
					        stopAddress.Longitude = null;
				        }
				        else
				        {
                            stopAddress.Latitude = item.Geolocation.Latitude;
                            stopAddress.Longitude = item.Geolocation.Longitude;
				        }
                        stoplist.Add(stopAddress);

                    }
                    else if (!string.IsNullOrWhiteSpace(item.AirportCode))
                    {
                        AlephAddress stopAddress = new AlephAddress();
                        stopAddress.address_type = "Airport";
                        stopAddress.airport_pickup_point = item.airport_pickup_point;
                        stopAddress.airport_code = item.AirportCode;
                        stopAddress.airline = item.Airline;
                        stopAddress.flight_number = item.AirlineFlightNumber;
                        stopAddress.flightDateTime = item.FlightDateTime;
                        stoplist.Add(stopAddress);
                    }
                }

            }
            if (stoplist != null)
            {
                if (stoplist.Any())
                    addReservationRequest.stops = stoplist;
            }
            #endregion

			#region Booking


			#region Required Parameters

			try
			{
				addReservationRequest.provider_id = Convert.ToInt32(ecbp.CorporateProviderID);

				addReservationRequest.provider_account_id = ecbp.CorporateProviderAccountID;

				addReservationRequest.company_name = ecbp.CorporateCompanyName;

				addReservationRequest.profile_id = ecbp.CorporateProfileID;

				addReservationRequest.payment_method = ecbp.CorporatePaymentMethod;

				addReservationRequest.requested_datetime = ecbp.PickupDateTime;

				addReservationRequest.pickup = pickupAddress;

                addReservationRequest.payment_method_token = ecbp.PaymentMethodToken;

            }
			catch (Exception exc)
			{
				var ex = new ValidationException("Unable to specify a required booking parameter. " + exc.Message);
				string methodName = GetMethodName();
				int vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
				LogException(methodName, vtodErrorCode.ToString(), ex);
				throw new Exception(vtodErrorCode.ToString(), ex);
			}

			#endregion Required Parameters


			#region Optional Parameters

			try
			{
				if (dropoffAddress != null)
					addReservationRequest.dropoff = dropoffAddress;
			}
			catch (Exception e1)
			{
				string methodName = GetMethodName();
				LogWarning(methodName, "Exception thrown generated while assigning dropoff address. Error Message - " + e1.Message);
			}


			try
			{
				addReservationRequest.booking_criteria = new List<AlephBookingCriteria>();
				if ((ecbp.BookingCriteria != null) && (ecbp.BookingCriteria.Count > 0))
				{
					addReservationRequest.booking_criteria = ecbp.BookingCriteria;
				}
			}
			catch (Exception e2)
			{
				string methodName = GetMethodName();
				LogWarning(methodName, "Exception thrown while assigning booking criteria. Error Message - " + e2.Message);
			}


			try
			{
				addReservationRequest.special_instructions = ecbp.CorporateSpecialInstructions;
			}
			catch (Exception e3)
			{
				string methodName = GetMethodName();
				LogWarning(methodName, "Exception thrown while assigning corporate parameters. Error Message - " + e3.Message);
			}


			#endregion Optional Parameters


			#endregion Booking
			return addReservationRequest.JsonSerialize();
		}

        private string ModifyReservation(AlephBookingParameter ecbp)
        {
            AlephBookingRQ addReservationRequest = new AlephBookingRQ();

            AlephAddress pickupAddress = new AlephAddress();
            AlephAddress dropoffAddress = null;

            List<AlephAddress> stoplist = new List<AlephAddress>();

            addReservationRequest.is_asap = false;

            #region Passengers
            if (ecbp.passengers != null)
            {
                if (ecbp.passengers.Any())
                {
                    addReservationRequest.passengers = ecbp.passengers;
                }
            }
            else
                addReservationRequest.passengers = null; 
            #endregion
            #region PickupAddress
          
            if (ecbp.PickupAddressType == AddressType.Address)
            {
                pickupAddress.street_no = ecbp.PickupAddress.StreetNo;
                pickupAddress.street_name = ecbp.PickupAddress.Street.Trim();
                pickupAddress.city = ecbp.PickupAddress.City;
                pickupAddress.state = ecbp.PickupAddress.State;
                pickupAddress.zip_code = ecbp.PickupAddress.ZipCode;
                pickupAddress.country = ecbp.PickupAddress.Country;
                pickupAddress.auto_complete = ecbp.PickupAddress.FullAddress;
                pickupAddress.address_type = "Street Address";
                if (ecbp.Trip != null && ecbp.Trip.ecar_trip != null)
                {
                    pickupAddress.pickup_point = ecbp.Trip.ecar_trip.RemarkForPickup;
                }

                if ((ecbp.PickupAddress.Geolocation.Latitude == 0) && (ecbp.PickupAddress.Geolocation.Longitude == 0))
                {
                    pickupAddress.Latitude = null;
                    pickupAddress.Longitude = null;
                }
                else
                {
                    pickupAddress.Latitude = ecbp.PickupAddress.Geolocation.Latitude;
                    pickupAddress.Longitude = ecbp.PickupAddress.Geolocation.Longitude;
                }

            }
            else if (ecbp.PickupAddressType == AddressType.Airport)
            {
                pickupAddress.address_type = "Airport";
                pickupAddress.airport_code = ecbp.PickupAirport;
                pickupAddress.airline = ecbp.PickupAirlineCode;
                pickupAddress.flight_number = ecbp.PickupAirlineFlightNumber;
                pickupAddress.airport_pickup_point = ecbp.AirportPickupPoint;
                pickupAddress.flightDateTime = ecbp.PickupAirlineFlightDateTime;
            }
            else
            {
                var ex = new ValidationException("The specified pickup address type is not supported.");
                string methodName = GetMethodName();
                int vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
                LogException(methodName, vtodErrorCode.ToString(), ex);
                throw new Exception(vtodErrorCode.ToString(), ex);
            }

            #endregion PickupAddress

            #region DropOffAddress

            if ((ecbp.DropOffAddressType != null) && (ecbp.DropOffAddress != null || !string.IsNullOrWhiteSpace(ecbp.DropoffAirport)))
            {
                dropoffAddress = new AlephAddress();

                if (ecbp.DropOffAddressType == AddressType.Address)
                {
                    dropoffAddress.street_no = ecbp.DropOffAddress.StreetNo;
                    dropoffAddress.street_name = ecbp.DropOffAddress.Street.Trim();
                    dropoffAddress.city = ecbp.DropOffAddress.City;
                    dropoffAddress.state = ecbp.DropOffAddress.State;
                    dropoffAddress.zip_code = ecbp.DropOffAddress.ZipCode;
                    dropoffAddress.country = ecbp.DropOffAddress.Country;
                    dropoffAddress.auto_complete = ecbp.DropOffAddress.FullAddress;
                    dropoffAddress.address_type = "Street Address";


                    if ((ecbp.DropOffAddress.Geolocation.Latitude == 0) && (ecbp.DropOffAddress.Geolocation.Longitude == 0))
                    {
                        dropoffAddress.Latitude = null;
                        dropoffAddress.Longitude = null;
                    }
                    else
                    {
                        dropoffAddress.Latitude = ecbp.DropOffAddress.Geolocation.Latitude;
                        dropoffAddress.Longitude = ecbp.DropOffAddress.Geolocation.Longitude;
                    }

                }
                else if (ecbp.DropOffAddressType == AddressType.Airport)
                {
                    dropoffAddress.address_type = "Airport";
                    dropoffAddress.airport_code = ecbp.DropoffAirport;
                    dropoffAddress.airline = ecbp.DropOffAirlineCode;
                    dropoffAddress.flight_number = ecbp.DropOffAirlineFlightNumber;
                    dropoffAddress.flightDateTime = ecbp.DropOffAirlineFlightDateTime;
                }
            }

            #endregion DropOffAddress

            #region Stops

            if (ecbp.Stops != null)
            {
                foreach (var item in ecbp.Stops)
                {
                    if (!string.IsNullOrWhiteSpace(item.FullAddress))
                    {
                        AlephAddress stopAddress = new AlephAddress();
                        stopAddress.street_no = item.StreetNo;
                        stopAddress.street_name = item.Street.Trim();
                        stopAddress.city = item.City;
                        stopAddress.state = item.State;
                        stopAddress.zip_code = item.ZipCode;
                        stopAddress.country = item.Country;
                        stopAddress.auto_complete = item.FullAddress;
                        stopAddress.address_type = "Street Address";
                        stopAddress.pickup_point = item.pickup_point;
                        if ((item.Geolocation.Latitude == 0) && (item.Geolocation.Longitude == 0))
                        {
                            stopAddress.Latitude = null;
                            stopAddress.Longitude = null;
                        }
                        else
                        {
                            stopAddress.Latitude = item.Geolocation.Latitude;
                            stopAddress.Longitude = item.Geolocation.Longitude;
                        }
                        stoplist.Add(stopAddress);

                    }
                    else if (!string.IsNullOrWhiteSpace(item.AirportCode))
                    {
                        AlephAddress stopAddress = new AlephAddress();
                        stopAddress.address_type = "Airport";
                        stopAddress.airport_pickup_point = item.airport_pickup_point;
                        stopAddress.airport_code = item.AirportCode;
                        stopAddress.airline = item.Airline;
                        stopAddress.flight_number = item.AirlineFlightNumber;
                        stopAddress.flightDateTime = item.FlightDateTime;
                        stoplist.Add(stopAddress);
                    }
                }

            }
            if (stoplist != null)
            {
                if (stoplist.Any())
                    addReservationRequest.stops = stoplist;
            }
            else
            {
                addReservationRequest.stops = null;
            }
            #endregion

            #region  Passengers
         
            #endregion

            #region Booking


            #region Required Parameters

            try
            {
                addReservationRequest.provider_id = Convert.ToInt32(ecbp.CorporateProviderID);

                addReservationRequest.provider_account_id = ecbp.CorporateProviderAccountID;

                addReservationRequest.company_name = ecbp.CorporateCompanyName;

                addReservationRequest.profile_id = ecbp.CorporateProfileID;

                addReservationRequest.payment_method = ecbp.CorporatePaymentMethod;

                addReservationRequest.requested_datetime = ecbp.PickupDateTime;

                addReservationRequest.pickup = pickupAddress;
                addReservationRequest.payment_method_token = ecbp.PaymentMethodToken;
            }
            catch (Exception exc)
            {
                var ex = new ValidationException("Unable to specify a required booking parameter. " + exc.Message);
                string methodName = GetMethodName();
                int vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
                LogException(methodName, vtodErrorCode.ToString(), ex);
                throw new Exception(vtodErrorCode.ToString(), ex);
            }

            #endregion Required Parameters


            #region Optional Parameters

            try
            {
                if (dropoffAddress != null)
                    addReservationRequest.dropoff = dropoffAddress;
            }
            catch (Exception e1)
            {
                string methodName = GetMethodName();
                LogWarning(methodName, "Exception thrown generated while assigning dropoff address. Error Message - " + e1.Message);
            }


            try
            {
                addReservationRequest.booking_criteria = new List<AlephBookingCriteria>();
                if ((ecbp.BookingCriteria != null) && (ecbp.BookingCriteria.Count > 0))
                {
                    addReservationRequest.booking_criteria = ecbp.BookingCriteria;
                }
            }
            catch (Exception e2)
            {
                string methodName = GetMethodName();
                LogWarning(methodName, "Exception thrown while assigning booking criteria. Error Message - " + e2.Message);
            }


            try
            {
                addReservationRequest.special_instructions = ecbp.CorporateSpecialInstructions;
            }
            catch (Exception e3)
            {
                string methodName = GetMethodName();
                LogWarning(methodName, "Exception thrown while assigning corporate parameters. Error Message - " + e3.Message);
            }


            #endregion Optional Parameters


            #endregion Booking

            addReservationRequest.agent_ride_no = ecbp.ConsumerConfirmationID;
            addReservationRequest.vendor_confirmation_no = ecbp.VendorConfirmationNumber;
            return addReservationRequest.JsonSerialize();
        }
		private string BuildReservationBookNow(AlephBookingParameter ecbp)
		{
			AlephBookNowRQ addReservationRequest = new AlephBookNowRQ();

			AlephAddress pickupAddress = new AlephAddress();
			AlephAddress dropoffAddress = null;
            List<AlephAddress> stoplist = new List<AlephAddress>();

			#region PickupAddress
			pickupAddress.Latitude = ecbp.CorporatePickupLatitude;
			pickupAddress.Longitude = ecbp.CorporatePickupLongitude;

			if (ecbp.PickupAddressType == AddressType.Address)
			{
				pickupAddress.street_no = ecbp.PickupAddress.StreetNo;
				pickupAddress.street_name = ecbp.PickupAddress.Street.Trim();
				pickupAddress.city = ecbp.PickupAddress.City;
				pickupAddress.state = ecbp.PickupAddress.State;
				pickupAddress.zip_code = ecbp.PickupAddress.ZipCode;
				pickupAddress.country = ecbp.PickupAddress.Country;
				pickupAddress.auto_complete = ecbp.PickupAddress.FullAddress;
				pickupAddress.address_type = "Street Address";
				pickupAddress.airport_pickup_point = ecbp.AirportPickupPoint;
				if (ecbp.Trip != null && ecbp.Trip.ecar_trip != null)
				{
					pickupAddress.pickup_point = ecbp.Trip.ecar_trip.RemarkForPickup;
				}
			}
			else if (ecbp.PickupAddressType == AddressType.Airport)
			{
				pickupAddress.address_type = "Airport";
				pickupAddress.airport_code = ecbp.PickupAirport;
				pickupAddress.airline = ecbp.PickupAirlineCode;
				pickupAddress.flight_number = ecbp.PickupAirlineFlightNumber;
				pickupAddress.airport_pickup_point = ecbp.AirportPickupPoint;

				if (ecbp.Trip != null && ecbp.Trip.ecar_trip != null)
				{
					pickupAddress.pickup_point = ecbp.Trip.ecar_trip.RemarkForPickup;
				}  
			}
			else
			{
				var ex = new ValidationException("The specified pickup address type is not supported.");
				string methodName = GetMethodName();
				int vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
				LogException(methodName, vtodErrorCode.ToString(), ex);
				throw new Exception(vtodErrorCode.ToString(), ex);
			}            
			#endregion PickupAddress


			#region DropOffAddress
			if ((ecbp.DropOffAddressType != null) && (ecbp.DropOffAddress != null || !string.IsNullOrWhiteSpace(ecbp.DropoffAirport)))
			{
				addReservationRequest.is_dropoff_as_directed = false;

				dropoffAddress = new AlephAddress();

				if ((ecbp.CorporateDropoffLatitude == 0) && (ecbp.CorporateDropoffLongitude == 0))
				{
					dropoffAddress.Latitude = null;
					dropoffAddress.Longitude = null;
				}
				else
				{
					dropoffAddress.Latitude = ecbp.CorporateDropoffLatitude;
					dropoffAddress.Longitude = ecbp.CorporateDropoffLongitude;
				}


				if (ecbp.DropOffAddressType == AddressType.Address)
				{
					dropoffAddress.street_no = ecbp.DropOffAddress.StreetNo;
					dropoffAddress.street_name = ecbp.DropOffAddress.Street.Trim();
					dropoffAddress.city = ecbp.DropOffAddress.City;
					dropoffAddress.state = ecbp.DropOffAddress.State;
					dropoffAddress.zip_code = ecbp.DropOffAddress.ZipCode;
					dropoffAddress.country = ecbp.DropOffAddress.Country;
					dropoffAddress.auto_complete = ecbp.DropOffAddress.FullAddress;
					dropoffAddress.address_type = "Street Address";
				}
				else if (ecbp.DropOffAddressType == AddressType.Airport)
				{
					dropoffAddress.address_type = "Airport";
					dropoffAddress.airport_code = ecbp.DropoffAirport;
					dropoffAddress.airline = ecbp.DropOffAirlineCode;
					dropoffAddress.flight_number = ecbp.DropOffAirlineFlightNumber;
				}
			}
			else
			{
				addReservationRequest.is_dropoff_as_directed = true;
			}

			#endregion DropOffAddress

            #region Stops

            if (ecbp.Stops != null)
            {
                foreach (var item in ecbp.Stops)
                {
                    if (!string.IsNullOrWhiteSpace(item.FullAddress))
                    {
                        AlephAddress stopAddress = new AlephAddress();
                        stopAddress.street_no = item.StreetNo;
                        stopAddress.street_name = item.Street.Trim();
                        stopAddress.city = item.City;
                        stopAddress.state = item.State;
                        stopAddress.zip_code = item.ZipCode;
                        stopAddress.country = item.Country;
                        stopAddress.auto_complete = item.FullAddress;
                        stopAddress.address_type = "Street Address";
                        stopAddress.pickup_point = item.pickup_point;
                        if ((item.Geolocation.Latitude == 0) && (item.Geolocation.Longitude == 0))
                        {
                            stopAddress.Latitude = null;
                            stopAddress.Longitude = null;
                        }
                        else
                        {
                            stopAddress.Latitude = item.Geolocation.Latitude;
                            stopAddress.Longitude = item.Geolocation.Longitude;
                        }
                        stoplist.Add(stopAddress);

                    }
                    else if (!string.IsNullOrWhiteSpace(item.AirportCode))
                    {
                        AlephAddress stopAddress = new AlephAddress();
                        stopAddress.address_type = "Airport";
                        stopAddress.airport_pickup_point = item.airport_pickup_point;
                        stopAddress.airport_code = item.AirportCode;
                        stopAddress.airline = item.Airline;
                        stopAddress.flight_number = item.AirlineFlightNumber;
                        stopAddress.flightDateTime = item.FlightDateTime;
                        stoplist.Add(stopAddress);
                    }
                }

            }
            if (stoplist != null)
            {
                if (stoplist.Any())
                    addReservationRequest.stops = stoplist;
            }
            #endregion

            #region Booking


            #region Required Parameters

            try
			{
				addReservationRequest.strategy = "Haversine";//This value is currently hard-coded per Tom Antola conversation on Skype 'Aleph Integration' thread. 04/06/2015. 2:31pm;

				addReservationRequest.username = ecbp.CorporateEmail;

				addReservationRequest.latitude = ecbp.CorporatePickupLatitude.ToString();

				addReservationRequest.longitude = ecbp.CorporatePickupLongitude.ToString();

				addReservationRequest.payment_method = ecbp.CorporatePaymentMethod;

				addReservationRequest.requested_datetime = ecbp.PickupDateTime;

				addReservationRequest.pickup = pickupAddress;

                addReservationRequest.payment_method_token = ecbp.PaymentMethodToken;

            }
			catch (Exception exc)
			{
				var ex = new ValidationException("Unable to specify a required booking parameter. " + exc.Message);
				string methodName = GetMethodName();
				int vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
				LogException(methodName, vtodErrorCode.ToString(), ex);
				throw new Exception(vtodErrorCode.ToString(), ex);
			}

			#endregion Required Parameters


			#region Optional Parameters

			try
			{
				if (dropoffAddress != null)
					addReservationRequest.dropoff = dropoffAddress;
			}
			catch (Exception e1)
			{
				string methodName = GetMethodName();
				LogWarning(methodName, "Exception thrown while assigning dropoff address. Error Message - " + e1.Message);
			}


			try
			{
				addReservationRequest.booking_criteria = new List<AlephBookingCriteria>();
				if ((ecbp.BookingCriteria != null) && (ecbp.BookingCriteria.Count > 0))
				{
					addReservationRequest.booking_criteria = ecbp.BookingCriteria;
				}
			}
			catch (Exception e2)
			{
				string methodName = GetMethodName();
				LogWarning(methodName, "Exception thrown while assigning booking criteria. Error Message - " + e2.Message);
			}


			try
			{
				addReservationRequest.special_instructions = ecbp.CorporateSpecialInstructions;
			}
			catch (Exception e3)
			{
				string methodName = GetMethodName();
				LogWarning(methodName, "Exception thrown while assigning corporate parameters. Error Message - " + e3.Message);
			}


			#endregion Optional Parameters


			#endregion Booking

			addReservationRequest.is_asap = true;
			return addReservationRequest.JsonSerialize();
		}

        private string ModifyReservationBookNow(AlephBookingParameter ecbp)
        {
            AlephBookNowRQ addReservationRequest = new AlephBookNowRQ();

            AlephAddress pickupAddress = new AlephAddress();
            AlephAddress dropoffAddress = null;
            List<AlephAddress> stoplist = new List<AlephAddress>();
            #region Passengers
            if (ecbp.passengers != null)
            {
                if (ecbp.passengers.Any())
                {
                    addReservationRequest.passengers = ecbp.passengers;
                }
            }
            else
                addReservationRequest.passengers = null;
            #endregion
            #region PickupAddress

            pickupAddress.Latitude = ecbp.CorporatePickupLatitude;
            pickupAddress.Longitude = ecbp.CorporatePickupLongitude;

            if (ecbp.PickupAddressType == AddressType.Address)
            {
                pickupAddress.street_no = ecbp.PickupAddress.StreetNo;
                pickupAddress.street_name = ecbp.PickupAddress.Street.Trim();
                pickupAddress.city = ecbp.PickupAddress.City;
                pickupAddress.state = ecbp.PickupAddress.State;
                pickupAddress.zip_code = ecbp.PickupAddress.ZipCode;
                pickupAddress.country = ecbp.PickupAddress.Country;
                pickupAddress.auto_complete = ecbp.PickupAddress.FullAddress;
                pickupAddress.address_type = "Street Address";
                pickupAddress.airport_pickup_point = ecbp.AirportPickupPoint;
                if (ecbp.Trip != null && ecbp.Trip.ecar_trip != null)
                {
                    pickupAddress.pickup_point = ecbp.Trip.ecar_trip.RemarkForPickup;
                }
            }
            else if (ecbp.PickupAddressType == AddressType.Airport)
            {
                pickupAddress.address_type = "Airport";
                pickupAddress.airport_code = ecbp.PickupAirport;
                pickupAddress.airline = ecbp.PickupAirlineCode;
                pickupAddress.flight_number = ecbp.PickupAirlineFlightNumber;
                pickupAddress.airport_pickup_point = ecbp.AirportPickupPoint;
                pickupAddress.flightDateTime = ecbp.PickupAirlineFlightDateTime;
                if (ecbp.Trip != null && ecbp.Trip.ecar_trip != null)
                {
                    pickupAddress.pickup_point = ecbp.Trip.ecar_trip.RemarkForPickup;
                }
            }
            else
            {
                var ex = new ValidationException("The specified pickup address type is not supported.");
                string methodName = GetMethodName();
                int vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
                LogException(methodName, vtodErrorCode.ToString(), ex);
                throw new Exception(vtodErrorCode.ToString(), ex);
            }

            #endregion PickupAddress


            #region DropOffAddress


            if ((ecbp.DropOffAddressType != null) && (ecbp.DropOffAddress != null || !string.IsNullOrWhiteSpace(ecbp.DropoffAirport)))
            {
                addReservationRequest.is_dropoff_as_directed = false;

                dropoffAddress = new AlephAddress();

                if ((ecbp.CorporateDropoffLatitude == 0) && (ecbp.CorporateDropoffLongitude == 0))
                {
                    dropoffAddress.Latitude = null;
                    dropoffAddress.Longitude = null;
                }
                else
                {
                    dropoffAddress.Latitude = ecbp.CorporateDropoffLatitude;
                    dropoffAddress.Longitude = ecbp.CorporateDropoffLongitude;
                }


                if (ecbp.DropOffAddressType == AddressType.Address)
                {
                    dropoffAddress.street_no = ecbp.DropOffAddress.StreetNo;
                    dropoffAddress.street_name = ecbp.DropOffAddress.Street.Trim();
                    dropoffAddress.city = ecbp.DropOffAddress.City;
                    dropoffAddress.state = ecbp.DropOffAddress.State;
                    dropoffAddress.zip_code = ecbp.DropOffAddress.ZipCode;
                    dropoffAddress.country = ecbp.DropOffAddress.Country;
                    dropoffAddress.auto_complete = ecbp.DropOffAddress.FullAddress;
                    dropoffAddress.address_type = "Street Address";
                }
                else if (ecbp.DropOffAddressType == AddressType.Airport)
                {
                    dropoffAddress.address_type = "Airport";
                    dropoffAddress.airport_code = ecbp.DropoffAirport;
                    dropoffAddress.airline = ecbp.DropOffAirlineCode;
                    dropoffAddress.flight_number = ecbp.DropOffAirlineFlightNumber;
                    dropoffAddress.flightDateTime = ecbp.DropOffAirlineFlightDateTime;
                  
                }
            }
            else
            {
                addReservationRequest.is_dropoff_as_directed = true;
            }

            #endregion DropOffAddress

            #region Stops

            if (ecbp.Stops != null)
            {
                foreach (var item in ecbp.Stops)
                {
                    if (!string.IsNullOrWhiteSpace(item.FullAddress))
                    {
                        AlephAddress stopAddress = new AlephAddress();
                        stopAddress.street_no = item.StreetNo;
                        stopAddress.street_name = item.Street.Trim();
                        stopAddress.city = item.City;
                        stopAddress.state = item.State;
                        stopAddress.zip_code = item.ZipCode;
                        stopAddress.country = item.Country;
                        stopAddress.auto_complete = item.FullAddress;
                        stopAddress.address_type = "Street Address";
                        stopAddress.pickup_point = item.pickup_point;
                        if ((item.Geolocation.Latitude == 0) && (item.Geolocation.Longitude == 0))
                        {
                            stopAddress.Latitude = null;
                            stopAddress.Longitude = null;
                        }
                        else
                        {
                            stopAddress.Latitude = item.Geolocation.Latitude;
                            stopAddress.Longitude = item.Geolocation.Longitude;
                        }
                        stoplist.Add(stopAddress);

                    }
                    else if (!string.IsNullOrWhiteSpace(item.AirportCode))
                    {
                        AlephAddress stopAddress = new AlephAddress();
                        stopAddress.address_type = "Airport";
                        stopAddress.airport_pickup_point = item.airport_pickup_point;
                        stopAddress.airport_code = item.AirportCode;
                        stopAddress.airline = item.Airline;
                        stopAddress.flight_number = item.AirlineFlightNumber;
                        stopAddress.flightDateTime = item.FlightDateTime;
                        stoplist.Add(stopAddress);
                    }
                }

            }
            if (stoplist != null)
            {
                if (stoplist.Any())
                    addReservationRequest.stops = stoplist;
            }
            else
            {
                addReservationRequest.stops = null;
            }
            #endregion

            #region Booking


            #region Required Parameters

            try
            {
                //addReservationRequest.strategy = "Haversine";//This value is currently hard-coded per Tom Antola conversation on Skype 'Aleph Integration' thread. 04/06/2015. 2:31pm;

                //addReservationRequest.username = ecbp.CorporateEmail;

                //addReservationRequest.latitude = ecbp.CorporatePickupLatitude.ToString();

                //addReservationRequest.longitude = ecbp.CorporatePickupLongitude.ToString();

                //addReservationRequest.payment_method = ecbp.CorporatePaymentMethod;

                //addReservationRequest.requested_datetime = ecbp.PickupDateTime;

                //addReservationRequest.pickup = pickupAddress;

                addReservationRequest.provider_id = Convert.ToInt32(ecbp.CorporateProviderID);

                addReservationRequest.provider_account_id = ecbp.CorporateProviderAccountID;

                addReservationRequest.company_name = ecbp.CorporateCompanyName;

                addReservationRequest.profile_id = ecbp.CorporateProfileID;

                addReservationRequest.payment_method = ecbp.CorporatePaymentMethod;

                addReservationRequest.requested_datetime = ecbp.PickupDateTime;

                addReservationRequest.pickup = pickupAddress;

                addReservationRequest.payment_method_token = ecbp.PaymentMethodToken;

            }
            catch (Exception exc)
            {
                var ex = new ValidationException("Unable to specify a required booking parameter. " + exc.Message);
                string methodName = GetMethodName();
                int vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(methodName, ex);
                LogException(methodName, vtodErrorCode.ToString(), ex);
                throw new Exception(vtodErrorCode.ToString(), ex);
            }

            #endregion Required Parameters


            #region Optional Parameters

            try
            {
                if (dropoffAddress != null)
                    addReservationRequest.dropoff = dropoffAddress;
            }
            catch (Exception e1)
            {
                string methodName = GetMethodName();
                LogWarning(methodName, "Exception thrown while assigning dropoff address. Error Message - " + e1.Message);
            }


            try
            {
                addReservationRequest.booking_criteria = new List<AlephBookingCriteria>();
                if ((ecbp.BookingCriteria != null) && (ecbp.BookingCriteria.Count > 0))
                {
                    addReservationRequest.booking_criteria = ecbp.BookingCriteria;
                }
            }
            catch (Exception e2)
            {
                string methodName = GetMethodName();
                LogWarning(methodName, "Exception thrown while assigning booking criteria. Error Message - " + e2.Message);
            }


            try
            {
                addReservationRequest.special_instructions = ecbp.CorporateSpecialInstructions;
            }
            catch (Exception e3)
            {
                string methodName = GetMethodName();
                LogWarning(methodName, "Exception thrown while assigning corporate parameters. Error Message - " + e3.Message);
            }


            #endregion Optional Parameters


            #endregion Booking

            addReservationRequest.is_asap = true;
            addReservationRequest.agent_ride_no = ecbp.ConsumerConfirmationID;
            addReservationRequest.vendor_confirmation_no = ecbp.VendorConfirmationNumber;
            return addReservationRequest.JsonSerialize();
        }
		//Make Http web request
		public static string ProcessWebRequestEx(string url, string content, string contentType, string method, string userName, string password, int timeout, Dictionary<string, string> headers, out string status, out Dictionary<string, string> responseHeader)
		{
			var result = string.Empty;
			status = string.Empty;
			responseHeader = new Dictionary<string, string>();


			try
			{
				ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

				HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
				if (headers != null && headers.Any())
				{
					foreach (var item in headers)
					{
						//If the Header type specified is one of the existing 'restricted headers', just assign the new value. 
						//Otherwise add the new header and value.
						switch (item.Key.ToLower())
						{
							case "accept":
								request.Accept = item.Value;
								break;
							case "connection":
								request.Connection = item.Value;
								break;
							case "content-length":
								request.ContentLength = Convert.ToInt64(item.Value);
								break;
							case "content-type":
								request.ContentType = item.Value;
								break;
							case "date":
								request.Date = Convert.ToDateTime(item.Value);
								break;
							case "expect":
								request.Expect = item.Value;
								break;
							case "host":
								request.Host = item.Value;
								break;
							case "if-modified-since":
								request.IfModifiedSince = Convert.ToDateTime(item.Value);
								break;
							case "referer":
								request.Referer = item.Value;
								break;
							case "transfer-encoding":
								request.TransferEncoding = item.Value;
								break;
							case "user-agent":
								request.UserAgent = item.Value;
								break;
							default:
								request.Headers.Add(item.Key + ":" + item.Value);
								break;
						}
					}
				}

				if (!string.IsNullOrWhiteSpace(userName))
				{
					NetworkCredential networkCredential = new NetworkCredential(userName, password);
					request.Credentials = networkCredential;
				}
				//NetworkCredential networkCredential = new NetworkCredential("DenverYellowCab", "denveryellowcab@12345");
				//request.Credentials = networkCredential;
				request.ProtocolVersion = HttpVersion.Version11;
				request.Method = method;
				request.Timeout = timeout;
				if (request.Accept == null) request.Accept = "*/*";
				request.UserAgent = "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; Trident/6.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; InfoPath.3; .NET4.0C; .NET4.0E)";


				Stream dataStream;
				if (!string.IsNullOrWhiteSpace(content))
				{
					byte[] byteArray = Encoding.UTF8.GetBytes(content);
					//byte[] byteArray = Encoding.ASCII.GetBytes(content);
					request.ContentType = contentType;
					request.ContentLength = byteArray.Length;
					dataStream = request.GetRequestStream();
					dataStream.Write(byteArray, 0, byteArray.Length);
					dataStream.Close();
				}
				//else
				//{
				//	dataStream = request.GetRequestStream();
				//}
				WebResponse response = request.GetResponse();

				if (response.Headers != null && response.Headers.Count > 0)
				{
					for (int i = 0; i < response.Headers.Count; ++i)
					{
						responseHeader.Add(response.Headers.Keys[i], response.Headers[i]);
					}
				}


				status = ((HttpWebResponse)response).StatusDescription;

				dataStream = response.GetResponseStream();
				StreamReader reader = new StreamReader(dataStream);
				result = reader.ReadToEnd();

				reader.Close();
				dataStream.Close();
				response.Close();

			}
			catch (WebException ex)
			{
				using (var reader = new StreamReader(ex.Response.GetResponseStream()))
				{
					result = reader.ReadToEnd();
				}
				throw new Exception(result);
			}

			return result;

		}


		//Returns the name of the method that calls this method. 
		private static string GetMethodName([CallerMemberName] string memberName = "")
		{
			return memberName;
		}

		//Logs Warnings
		private void LogWarning(string method, string message)
		{
			const string className = "AlephECarService";

			logger.WarnFormat("Method:{0}.{1}, Warning:{2}", className, method, message);
		}

		//Logs Exceptions
		private void LogException(string method, string message, Exception e)
		{
			const string className = "AlephECarService";

			logger.Error(string.Format("Method:{0}.{1}, Message:{2}", className, method, message), e);
		}
		private Common.DTO.OTA.OTA_GroundCancelRS CancelWithConfirmation(Common.DTO.OTA.TokenRS tokenVTOD, Common.DTO.OTA.OTA_GroundCancelRQ request, ECarCancelParameter eccp)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			OTA_GroundCancelRS response = null;
			AlephECarUtility utility = new AlephECarUtility(logger);
			string alephTripID = string.Empty;

			#region [ECar 3.0] Validate required fields
			AlephCancelParameter acp = new AlephCancelParameter { Fleet = eccp.Fleet, serviceAPIID = eccp.serviceAPIID };
			if (!utility.ValidateCancelRequest(tokenVTOD, request, ref acp))
			{
				throw VtodException.CreateException(ExceptionType.ECar, 5005);
			}
			#region Parsing Of a TripID
			string TripID = acp.Trip.ecar_trip.DispatchTripId;
			if (TripID.Contains("##"))
			{
				if (TripID.IndexOf("##") != -1)
				{
					alephTripID = TripID.Substring(TripID.IndexOf("##") + 2);
				}
				else
				{
					alephTripID = TripID;
				}
			}
			else
			{
				alephTripID = TripID;
			}
			#endregion
			#endregion

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
			#endregion

			try
			{
				DateTime? requestedLocalTime = null;
				if (acp.Fleet != null && acp.Fleet.ServerUTCOffset != null)
				{
					requestedLocalTime = DateTime.UtcNow.FromUtcNullAble(acp.Fleet.ServerUTCOffset);
				}
				try
				{

					if (utility.IsAbleToCancelThisTrip(acp.Trip.FinalStatus))
					{


						#region Get Corporate Key

						string corporateKey = string.Empty;

						try
						{
							corporateKey = AlephSDSCall.GetCorporateAccountKey(tokenVTOD, TrackTime, acp.CorporateEmail);
						}
						catch (Exception ex1)
						{
							throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(ex1.Message));
						}

						#endregion Get Corporate Key

						//string corporateKey = "cfd45da8-27c9-41b5-b4cf-15938eb792a0";

						#region Call Aleph API

						AlephBookingRS alephCancelResponse = null;

						try
						{
							var alephAPICall = new AlephAPICall();
							alephCancelResponse = alephAPICall.CancelReservation(acp.CorporateProviderID, acp.CorporateCompanyName, acp.CorporateProviderAccountID, acp.CorporateProfileID, acp.CorporateEmail, corporateKey, alephTripID);
						}
                        catch (VtodException ex)
                        {
                            if (ex.ExceptionMessage != null)
                            {
                                int code = ex.ExceptionMessage.Code;
                                throw VtodException.CreateException(ExceptionType.ECar, code);
                            }
                            else
                                throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(ex.Message));
                        }
						catch (Exception ex)
						{
							string msg = string.Format("Unable to cancel this trip. tripID={0}, errormessage={1}", acp.Trip.Id, ex.InnerException.Message);
							//logger.Warn(msg);

							throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(ex.Message));
						}
                       
						#endregion Call Aleph API




						if (alephCancelResponse != null)
						{
							response = new OTA_GroundCancelRS();
							response.Success = new Success();
							response.EchoToken = acp.request.EchoToken;
							response.Target = acp.request.Target;
							response.Version = acp.request.Version;
							response.PrimaryLangID = acp.request.PrimaryLangID;

							response.Reservation = new Reservation();
							response.Reservation.CancelConfirmation = new CancelConfirmation();
							response.Reservation.CancelConfirmation.UniqueID = new UniqueID { ID = acp.Trip.Id.ToString() };

							#region Set final status
							utility.UpdateFinalStatus(acp.Trip.Id, ECarTripStatusType.Canceled, null, null, null);
							#endregion

							#region Put status into ecar_trip_status
							utility.SaveTripStatus(acp.Trip.Id, ECarTripStatusType.Canceled, requestedLocalTime, null, null, null, null,null, null,null, null, string.Empty, string.Empty, null);
							#endregion
						}
						else
						{
							logger.WarnFormat("Unable to cancel this trip. Trip Id ={0}, dispatchId={1}", acp.Trip.Id, acp.Trip.ecar_trip.DispatchTripId);
							throw VtodException.CreateException(ExceptionType.ECar, 5001);// new ECarException(Messages.ECar_UnableToCancelGreenTomato);
						}

					}
					else
					{
						throw new ECarException(Messages.ECar_CancelFailure);
					}
				}
				catch (Exception ex)
				{

					logger.ErrorFormat("Message:{0}", ex.Message);
					logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
					logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
					throw;
				}

				sw.Stop();
				logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw;
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);


			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_GreenTomato, "Cancel");
			#endregion

			return response;
		}

		private Common.DTO.OTA.OTA_GroundCancelRS CancelWithDispatchConfirmation(Common.DTO.OTA.TokenRS tokenVTOD, Common.DTO.OTA.OTA_GroundCancelRQ request, ECarCancelParameter eccp)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			OTA_GroundCancelRS response = null;
			AlephECarUtility utility = new AlephECarUtility(logger);

			string dispatchConfirmation = null;
			string dispatchTripID = null;

			#region Get dispatchConfirmation
			dispatchConfirmation = request.Reservation.UniqueID.FirstOrDefault().Type;
			#endregion
			string alephTripID = string.Empty;
			if (!string.IsNullOrWhiteSpace(dispatchConfirmation))
			{
				if (dispatchConfirmation.ToLower() == "dispatchconfirmation")
					dispatchTripID = request.Reservation.UniqueID.FirstOrDefault().ID;
				if (dispatchTripID.Contains("##"))
				{
					if (dispatchTripID.IndexOf("##") != -1)
					{
						alephTripID = dispatchTripID.Substring(dispatchTripID.IndexOf("##") + 2);
					}
					else
					{
						alephTripID = dispatchTripID;
					}
				}
				else
				{
					alephTripID = dispatchTripID;
				}
			}


			#region [ECar 3.0] Validate required fields
			AlephCancelParameter acp = new AlephCancelParameter();
			if (!utility.ValidateCancelRequestForDispatchTripID(tokenVTOD, request, ref acp))
			{
				throw VtodException.CreateException(ExceptionType.ECar, 5005);
			}
			#endregion

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
			#endregion

			try
			{
				//DateTime? requestedLocalTime = null;
				//if (acp.Fleet != null && acp.Fleet.ServerUTCOffset != null)
				//{
				//    requestedLocalTime = DateTime.UtcNow.FromUtcNullAble(acp.Fleet.ServerUTCOffset);
				//}
				try
				{

					//if (utility.IsAbleToCancelThisTrip(acp.Trip.FinalStatus))
					//{


					#region Get Corporate Key

					string corporateKey = string.Empty;

					try
					{
						corporateKey = AlephSDSCall.GetCorporateAccountKey(tokenVTOD, TrackTime, acp.CorporateEmail);
					}
					catch (Exception ex1)
					{
						throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(ex1.Message));
					}

					#endregion Get Corporate Key

					#region Call Aleph API

					AlephBookingRS alephCancelResponse = null;

					try
					{
						var alephAPICall = new AlephAPICall();
						alephCancelResponse = alephAPICall.CancelReservation(acp.CorporateProviderID, acp.CorporateCompanyName, acp.CorporateProviderAccountID, acp.CorporateProfileID, acp.CorporateEmail, corporateKey, alephTripID.ToString());
					}
                    catch (VtodException ex)
                    {
                        if (ex.ExceptionMessage != null)
                        {
                            int code = ex.ExceptionMessage.Code;
                            throw VtodException.CreateException(ExceptionType.ECar, code);
                        }
                        else
                            throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(ex.Message));
                    }
                    catch (Exception ex)
                    {
                        string msg = string.Format("Unable to cancel this trip. tripID={0}, errormessage={1}", acp.Trip.Id, ex.InnerException.Message);
                        //logger.Warn(msg);

                        throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(ex.Message));
                    }

					#endregion Call Aleph API




					if (alephCancelResponse != null)
					{
						response = new OTA_GroundCancelRS();
						response.Success = new Success();
						response.EchoToken = acp.request.EchoToken;
						response.Target = acp.request.Target;
						response.Version = acp.request.Version;
						response.PrimaryLangID = acp.request.PrimaryLangID;

						response.Reservation = new Reservation();
						response.Reservation.CancelConfirmation = new CancelConfirmation();
						response.Reservation.CancelConfirmation.UniqueID = new UniqueID { ID = dispatchTripID.ToString() };

						#region Set final status
						//utility.UpdateFinalStatus(cancelID, ECarTripStatusType.Canceled, null, null, null);
						#endregion

						#region Put status into ecar_trip_status
						//utility.SaveTripStatus(cancelID, ECarTripStatusType.Canceled, requestedLocalTime, null, null, null, null, null, null, string.Empty, string.Empty, null);
						#endregion
					}
					else
					{
						logger.WarnFormat("Unable to cancel this trip. Trip Id ={0}", dispatchTripID);
						throw VtodException.CreateException(ExceptionType.ECar, 5001);
					}

					//}//
					//else
					//{
					//throw new ECarException(Messages.ECar_CancelFailure);
					//}
				}
				catch (Exception ex)
				{

					logger.ErrorFormat("Message:{0}", ex.Message);
					logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
					logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
					throw new ECarException(Messages.ECar_CancelFailure);
				}

				sw.Stop();
				logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw;
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);


			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Aleph, "Cancel");
			#endregion

			return response;
		}

		#endregion Private Methods
	}
}
