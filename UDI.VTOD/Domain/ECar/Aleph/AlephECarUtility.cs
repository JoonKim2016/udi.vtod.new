﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Security.Cryptography;
using System.Globalization;
using System.Diagnostics;
using UDI.Map;
using UDI.VTOD.Common.DTO;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.Helper;
using UDI.VTOD.Common.Track;
using UDI.VTOD.DataAccess.VTOD;
using UDI.VTOD.Domain.ECar.Class;
using UDI.VTOD.Domain.ECar.Const;
using UDI.Utility.Helper;
using UDI.VTOD.Domain.Utility;
using log4net;
using UDI.Utility.Serialization;
using UDI.VTOD.Domain.ECar;

namespace UDI.VTOD.Domain.ECar.Aleph
{
	public class AlephECarUtility : ECarUtility
	{
		public AlephECarUtility(ILog log)
			: base(log)
		{

		}
		public bool ValidateModifyBookingRequest(TokenRS tokenVTOD, OTA_GroundBookRQ request, ref AlephBookingParameter ecbp, AlephBookingRS asp, string alephVendorTripID, string type)
		{
			Map.DTO.Address dropoffAddress = new Map.DTO.Address();

			bool result = true;
			Stopwatch sw = new Stopwatch();
			sw.Start();

			Stopwatch swEachPart = new Stopwatch();
			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
			#endregion

			#region Validate Reservation
			if (request.GroundReservations.Any())
			{
				GroundReservation reservation = request.GroundReservations.FirstOrDefault();

				#region Set token, request
				ecbp.tokenVTOD = tokenVTOD;
				ecbp.request = request;
				#endregion

				#region Set Source and Device and ref
				if (request.TPA_Extensions != null && !string.IsNullOrWhiteSpace(request.TPA_Extensions.Source))
				{
					ecbp.Source = request.TPA_Extensions.Source;
				}

				if (request.TPA_Extensions != null && !string.IsNullOrWhiteSpace(request.TPA_Extensions.Device))
				{
					ecbp.Device = request.TPA_Extensions.Device;
				}

				if (!string.IsNullOrWhiteSpace(request.EchoToken))
				{
					ecbp.Ref = request.EchoToken;
				}
				#endregion
				ecbp.VendorConfirmationNumber = alephVendorTripID;
				ecbp.ConsumerConfirmationID = asp.confirmation_no;
				swEachPart.Restart();
				#region Validate Customer
				if (request.GroundReservations.FirstOrDefault().Passenger != null)
				{
					List<AlephPassenger> passengers = new List<AlephPassenger>();
					AlephPassenger item = new AlephPassenger();
					if (request.GroundReservations.FirstOrDefault().Passenger.Primary != null)
					{
						if (request.GroundReservations.FirstOrDefault().Passenger.Primary.PersonName != null)
						{
							if (string.IsNullOrWhiteSpace(request.GroundReservations.FirstOrDefault().Passenger.Primary.PersonName.GivenName) || string.IsNullOrWhiteSpace(request.GroundReservations.FirstOrDefault().Passenger.Primary.PersonName.Surname))
							{
								result = false;
								logger.Warn(Messages.ECar_Common_InvalidReservationPassengerPrimaryPersonNameDetail);
								throw new ValidationException(Messages.ECar_Common_InvalidReservationPassengerPrimaryPersonNameDetail);
							}
							else
							{
								ecbp.FirstName = request.GroundReservations.FirstOrDefault().Passenger.Primary.PersonName.GivenName.Trim();
								ecbp.LastName = request.GroundReservations.FirstOrDefault().Passenger.Primary.PersonName.Surname.Trim();
								item.first_name = request.GroundReservations.FirstOrDefault().Passenger.Primary.PersonName.GivenName;
								item.last_name = request.GroundReservations.FirstOrDefault().Passenger.Primary.PersonName.Surname;
								logger.Info(Messages.ECar_Common_ValidReservationPassengerPrimaryPersonNameDetail);
							}
						}
						if (request.GroundReservations.FirstOrDefault().Passenger.Primary.Telephones != null && request.GroundReservations.FirstOrDefault().Passenger.Primary.Telephones.Any())
						{
							if (!string.IsNullOrWhiteSpace(request.GroundReservations.FirstOrDefault().Passenger.Primary.Telephones.FirstOrDefault().CountryAccessCode) || !string.IsNullOrWhiteSpace(request.GroundReservations.FirstOrDefault().Passenger.Primary.Telephones.FirstOrDefault().AreaCityCode) || !string.IsNullOrWhiteSpace(request.GroundReservations.FirstOrDefault().Passenger.Primary.Telephones.FirstOrDefault().PhoneNumber))
							{
								item.phone_number = request.GroundReservations.FirstOrDefault().Passenger.Primary.Telephones.FirstOrDefault().CountryAccessCode + request.GroundReservations.FirstOrDefault().Passenger.Primary.Telephones.FirstOrDefault().AreaCityCode + request.GroundReservations.FirstOrDefault().Passenger.Primary.Telephones.FirstOrDefault().PhoneNumber;
								ecbp.PhoneNumber = item.phone_number;
								logger.Info(Messages.ECar_Common_ValidReservationPassengerPrimaryTelephones);
							}
							else
							{
								result = false;
								logger.Warn(Messages.ECar_Common_InvalidReservationPassengerPrimaryPhoneNumber);
								throw new ValidationException(Messages.ECar_Common_InvalidReservationPassengerPrimaryPhoneNumber);
							}

						}
						if (request.GroundReservations.FirstOrDefault().Passenger.Primary.Emails != null && request.GroundReservations.FirstOrDefault().Passenger.Primary.Emails.Any())
						{
							if (!string.IsNullOrWhiteSpace(request.GroundReservations.FirstOrDefault().Passenger.Primary.Emails.FirstOrDefault().Value))
							{
								Email e = new Email();
								e.Value = request.GroundReservations.FirstOrDefault().Passenger.Primary.Emails.FirstOrDefault().Value;


								string emailPattern = @"^(?("")("".+?(?<!\\)""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
													 @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]*\.)+[a-zA-Z0-9][\-a-zA-Z0-9]{0,22}[a-zA-Z0-9]))$";

								if (!string.IsNullOrWhiteSpace(e.Value) && Regex.IsMatch(e.Value, emailPattern, RegexOptions.IgnorePatternWhitespace))
								{
									item.email_address = e.Value;
									ecbp.emailAddress = e.Value;
									logger.Info(Messages.ECar_Common_ValidReservationPassengerPrimaryTelephones);
								}
								else
								{
									result = false;
									logger.Warn(Messages.ECar_Common_InvalidReservationPassengerPrimaryEmails);
									throw new ValidationException(Messages.ECar_Common_InvalidReservationPassengerPrimaryEmails);
								}
							}


						}
						passengers.Add(item);
						ecbp.passengers = passengers;
					}
				}
				else if (asp.passengers != null)
				{

					//Name
					if (asp.passengers.Any() != null)
					{

						ecbp.passengers = asp.passengers;
						if (string.IsNullOrWhiteSpace(asp.passengers.FirstOrDefault().first_name) || string.IsNullOrWhiteSpace(asp.passengers.FirstOrDefault().last_name))
						{
							result = false;
							logger.Warn(Messages.ECar_Common_InvalidReservationPassengerPrimaryPersonNameDetail);
							throw new ValidationException(Messages.ECar_Common_InvalidReservationPassengerPrimaryPersonNameDetail);
						}
						else
						{
							ecbp.FirstName = asp.passengers.FirstOrDefault().first_name.Trim();
							ecbp.LastName = asp.passengers.FirstOrDefault().last_name.Trim();
							logger.Info(Messages.ECar_Common_ValidReservationPassengerPrimaryPersonNameDetail);
						}

						//Telephone
						if (!string.IsNullOrWhiteSpace(asp.passengers.FirstOrDefault().phone_number))
						{
							ecbp.PhoneNumber = asp.passengers.FirstOrDefault().phone_number;
							logger.Info(Messages.ECar_Common_ValidReservationPassengerPrimaryTelephones);
						}
						else
						{
							result = false;
							logger.Warn(Messages.ECar_Common_InvalidReservationPassengerPrimaryPhoneNumber);
							throw new ValidationException(Messages.ECar_Common_InvalidReservationPassengerPrimaryPhoneNumber);
						}

						//email
						if (!string.IsNullOrWhiteSpace(asp.passengers.FirstOrDefault().email_address))
						{
							Email e = new Email();
							e.Value = asp.passengers.FirstOrDefault().email_address;


							string emailPattern = @"^(?("")("".+?(?<!\\)""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
												 @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]*\.)+[a-zA-Z0-9][\-a-zA-Z0-9]{0,22}[a-zA-Z0-9]))$";

							if (!string.IsNullOrWhiteSpace(e.Value) && Regex.IsMatch(e.Value, emailPattern, RegexOptions.IgnorePatternWhitespace))
							{
								ecbp.emailAddress = e.Value;
								logger.Info(Messages.ECar_Common_ValidReservationPassengerPrimaryTelephones);
							}
							else
							{
								result = false;
								logger.Warn(Messages.ECar_Common_InvalidReservationPassengerPrimaryEmails);
								throw new ValidationException(Messages.ECar_Common_InvalidReservationPassengerPrimaryEmails);
							}
						}

						if (result)
						{
							logger.InfoFormat("Customer firstname={0}, lastname={1}, phonenumber={2}", ecbp.FirstName, ecbp.LastName, ecbp.PhoneNumber);
						}
						else
						{
							result = false;
							logger.Warn(Messages.ECar_Common_UnableToCreateOrFindECarCustomer);
							throw new ValidationException(Messages.ECar_Common_UnableToCreateOrFindECarCustomer);
						}
					}
					else
					{
						result = false;
						logger.Warn(Messages.ECar_Common_InvalidReservationPassengerPrimary);
						throw new ValidationException(Messages.ECar_Common_InvalidReservationPassengerPrimary);
					}
				}
				else
				{
					result = false;
					logger.Warn(Messages.ECar_Common_InvalidReservationPassenger);
					throw new ValidationException(Messages.ECar_Common_InvalidReservationPassenger);
				}

				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidateCustomer");
				swEachPart.Stop();
				logger.DebugFormat("Validate Customer Duration:{0} ms.", swEachPart.ElapsedMilliseconds);


				swEachPart.Restart();
				#region Validate PickupDateTime
				if (result)
				{
					if (asp.is_asap)
					{
						ecbp.PickupNow = true;
					}
					else
					{
						ecbp.PickupNow = false;
					}

					DateTime pickupDateTime;
					if (DateTime.TryParse(reservation.Service.Location.Pickup.DateTime, out pickupDateTime))
					{
						ecbp.PickupDateTime = pickupDateTime;
						logger.Info("PickupDateTime is correct");
					}
					else if (ecbp.PickupNow && !DateTime.TryParse(reservation.Service.Location.Pickup.DateTime, out pickupDateTime))
					{
						ecbp.PickupDateTime = DateTime.Now;
						logger.InfoFormat("There is no Pickup datetime for this request. But it has pickup now value. So the pickup time will set as {0:yyyy/MM/dd HH:mm:ss}.", ecbp.PickupDateTime);
					}
					else
					{
						result = false;
						logger.Warn(Messages.ECar_Common_InvalidReservationServiceLocationPickupDateTime(reservation.Service.Location.Pickup.DateTime));
						throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocationPickupDateTime(reservation.Service.Location.Pickup.DateTime));
					}
				}
				else
				{
					result = false;
					logger.Warn(Messages.ECar_Common_UnableProcessByPreviousInvalidRequest);
					throw new ValidationException(Messages.ECar_Common_UnableProcessByPreviousInvalidRequest);
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidatePickupDateTime");
				swEachPart.Stop();
				logger.DebugFormat("Validate PickupTime Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Airport Pick Up Point
				if (!string.IsNullOrWhiteSpace(request.TPA_Extensions.AirportPickupPoint))
				{
					ecbp.AirportPickupPoint = request.TPA_Extensions.AirportPickupPoint;
				}
				else
				{
					ecbp.AirportPickupPoint = asp.pickup.airport_pickup_point;
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidateUser");
				swEachPart.Stop();
				logger.DebugFormat("Validate user Duration:{0} ms.", swEachPart.ElapsedMilliseconds);
				swEachPart.Restart();
				#region Validate Pickup Address

				if (reservation.Service != null)
				{
					if (reservation.Service.Location != null)
					{
						if (reservation.Service.Location.Pickup != null)
						{

							if (((reservation.Service.Location.Pickup.Address != null) && (reservation.Service.Location.Pickup.AirportInfo != null)) || ((reservation.Service.Location.Pickup.Address == null) && (reservation.Service.Location.Pickup.AirportInfo == null)))
							{
								result = false;
								logger.Warn(Messages.ECar_Common_InvalidReservationServiceLocationPickupContainsBothPickupAddressAndAirportInfo);
								throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocationPickupContainsBothPickupAddressAndAirportInfo);
							}
							else if ((reservation.Service.Location.Pickup.Address != null) && (reservation.Service.Location.Pickup.AirportInfo == null))
							{

								if ((string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude) || (string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CountryName.Code)))
								{
									result = false;
									logger.Warn(Messages.ECar_Common_InvalidReservationServiceLocationPickupDetail);
									throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocationPickupDetail);
								}
								else if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.AddressLine) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CityName) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.PostalCode) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CountryName.Code))
								{
									//Address
									Map.DTO.Address pickupAddress = null;
									logger.Info(Messages.ECar_Common_SkipReservationServiceLocationPickupDetailByMAPAPI);
									pickupAddress = ConvertAddress(reservation.Service.Location.Pickup);

									ecbp.PickupAddressType = UDI.VTOD.Domain.ECar.Const.AddressType.Address;
									ecbp.PickupAddress = pickupAddress;


									logger.Info("Pickup AddressType: Address");
								}
								else if ((!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude) && (!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CountryName.Code)))
								{
									result = false;
									logger.Warn(Messages.ECar_Common_InvalidReservationServiceLocationPickupDetail);
									throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocationPickupDetail);
								}
								else
								{
									result = false;
									logger.Warn(Messages.ECar_Common_UnknownError);
									throw new ValidationException(Messages.ECar_Common_UnknownError);
								}

							}
							else if ((reservation.Service.Location.Pickup.Address == null) && (reservation.Service.Location.Pickup.AirportInfo != null))
							{
								//airport
								if (((reservation.Service.Location.Pickup.AirportInfo.Arrival != null) && (reservation.Service.Location.Pickup.AirportInfo.Departure != null)) || ((reservation.Service.Location.Pickup.AirportInfo.Arrival == null) && (reservation.Service.Location.Pickup.AirportInfo.Departure == null)))
								{
									result = false;
									logger.Warn(Messages.ECar_Common_InvalidReservationServiceLocationPickupAirportInfoContainsBothArrivalAndDepature);
									throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocationPickupAirportInfoContainsBothArrivalAndDepature);
								}
								else if (reservation.Service.Location.Pickup.AirportInfo.Arrival != null && reservation.Service.Location.Pickup.AirportInfo.Departure == null)
								{
									//Arrival
									ecbp.PickupAddressType = AddressType.Airport;
									ecbp.PickupAirport = reservation.Service.Location.Pickup.AirportInfo.Arrival.LocationCode;
									logger.Info("Pickup AddressType: AirportInfo.Arrival");
								}
								else if (reservation.Service.Location.Pickup.AirportInfo.Arrival == null && reservation.Service.Location.Pickup.AirportInfo.Departure != null)
								{
									//Depature

									ecbp.PickupAddressType = AddressType.Airport;
									ecbp.PickupAirport = reservation.Service.Location.Pickup.AirportInfo.Departure.LocationCode;
									logger.Info("Pickup AddressType: AirportInfo.Depature");

								}
								//longitude and latitude, and flight information
								if (result)
								{
									double? longitude = null;
									double? latitude = null;
									if (GetLongitudeAndLatitudeForAirport(ecbp.PickupAirport, out longitude, out latitude))
									{
										ecbp.LongitudeForPickupAirport = longitude.Value;
										ecbp.LatitudeForPickupAirport = latitude.Value;
									}
									//else
									//{
									//    throw new ValidationException(Messages.ECar_Common_UnableToFindAirportLongitudeAndLatitude);
									//}

									if (reservation.Service.Location.Pickup.Airline != null)
									{
										if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Airline.FlightNumber))
										{
											ecbp.PickupAirlineFlightNumber = reservation.Service.Location.Pickup.Airline.FlightNumber;
										}

										if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Airline.Code))
										{
											ecbp.PickupAirlineCode = reservation.Service.Location.Pickup.Airline.Code;
										}

										if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Airline.FlightDateTime))
										{
											ecbp.PickupAirlineFlightDateTime = reservation.Service.Location.Pickup.Airline.FlightDateTime;
										}
										if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Airline.FlightDateTime))
										{
											ecbp.PickupAirlineFlightDateTime = reservation.Service.Location.Pickup.Airline.FlightDateTime;
										}
									}

								}


							}

							//For Pick me up now, make sure a Lat and Lon value are included in the Pickup request info. Change the Error message thrown to a more specific one.
							if (ecbp.PickupNow)
							{
								if ((reservation.Service.Location.Pickup.Address != null) && (!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Latitude) && (!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude))))
								{
									ecbp.CorporatePickupLatitude = Convert.ToDecimal(reservation.Service.Location.Pickup.Address.Latitude);
									ecbp.CorporatePickupLongitude = Convert.ToDecimal(reservation.Service.Location.Pickup.Address.Longitude);
								}
								else if ((ecbp.LongitudeForPickupAirport != null) && (ecbp.LatitudeForPickupAirport != null) && ((ecbp.LongitudeForPickupAirport != 0) && (ecbp.LatitudeForPickupAirport != 0)))
								{
									ecbp.CorporatePickupLatitude = (decimal)ecbp.LatitudeForPickupAirport;
									ecbp.CorporatePickupLongitude = (decimal)ecbp.LongitudeForPickupAirport;
								}
								else
								{
									result = false;
									logger.Warn(Messages.ECar_Common_InvalidReservationServiceLocationPickupDetail);
									throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocationPickupDetail);
								}
							}

						}
						else if (asp.pickup != null)
						{
							if (asp.pickup.address_type == "Street Address")
							{

								Map.DTO.Address pickupAddress = null;
								logger.Info(Messages.ECar_Common_SkipReservationServiceLocationPickupDetailByMAPAPI);
								pickupAddress.address_type = asp.pickup.address_type;
								pickupAddress.Street = asp.pickup.street_name.Trim();
								pickupAddress.StreetNo = asp.pickup.street_no;
								pickupAddress.FullStreet = asp.pickup.auto_complete;
								pickupAddress.State = asp.pickup.state;
								pickupAddress.City = asp.pickup.city;
								pickupAddress.ZipCode = asp.pickup.zip_code;
								pickupAddress.Latitude = asp.pickup.Latitude.ToString();
								pickupAddress.Longitude = asp.pickup.Longitude.ToString();
								pickupAddress.Country = asp.pickup.country;
								ecbp.PickupAddressType = UDI.VTOD.Domain.ECar.Const.AddressType.Address;
								ecbp.PickupAddress = pickupAddress;
							}
							if (asp.pickup.address_type == "Airport")
							{

								Map.DTO.Address pickupAddress = null;
								logger.Info(Messages.ECar_Common_SkipReservationServiceLocationPickupDetailByMAPAPI);
								pickupAddress.AirportCode = asp.pickup.airport_code;
								pickupAddress.Airline = asp.pickup.airline;
								pickupAddress.AirlineFlightNumber = asp.pickup.flight_number;
								pickupAddress.pickup_point = asp.pickup.pickup_point;
								pickupAddress.airport_pickup_point = asp.pickup.airport_pickup_point;
								pickupAddress.City = asp.pickup.city_of_origin;
								pickupAddress.Latitude = asp.pickup.Latitude.ToString();
								pickupAddress.Longitude = asp.pickup.Longitude.ToString();
								pickupAddress.FlightDateTime = asp.pickup.flightDateTime;
								ecbp.PickupAddressType = UDI.VTOD.Domain.ECar.Const.AddressType.Airport;
								ecbp.PickupAddress = pickupAddress;
							}
						}
						else
						{
							result = false;
							logger.Warn(Messages.ECar_Common_InvalidReservationServiceLocationPickup);
							throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocationPickup);
						}




					}
					else
					{
						result = false;
						logger.Warn(Messages.ECar_Common_InvalidReservationServiceLocation);
						throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocation);
					}
				}
				else
				{
					result = false;
					logger.Warn(Messages.ECar_Common_InvalidReservationService);
					throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidatePickupAddress");
				swEachPart.Stop();
				logger.DebugFormat("Validate Pickup address Duration:{0} ms.", swEachPart.ElapsedMilliseconds);
                swEachPart.Restart();

                #region Validate DropOff Address

                if (reservation.Service != null)
                {
                    if (reservation.Service.Location != null)
                    {
                        if (reservation.Service.Location.Dropoff != null)
                        {

                            if (((reservation.Service.Location.Dropoff.Address != null) && (reservation.Service.Location.Dropoff.AirportInfo != null)) || ((reservation.Service.Location.Dropoff.Address == null) && (reservation.Service.Location.Dropoff.AirportInfo == null)))
                            {
                                result = false;
                                logger.Warn(Messages.ECar_Common_InvalidReservationServiceLocationDropoffContainsBothDropoffAddressAndAirportInfo);
                                throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocationDropoffContainsBothDropoffAddressAndAirportInfo);
                            }
                            else if ((reservation.Service.Location.Dropoff.Address != null) && (reservation.Service.Location.Dropoff.AirportInfo == null))
                            {

                                if ((string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.Longitude) || (string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CountryName.Code)))
                                {
                                    result = false;
                                    logger.Warn(Messages.ECar_Common_InvalidReservationServiceLocationDropoffDetail);
                                    throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocationDropoffDetail);
                                }
                                else if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.AddressLine) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CityName) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.PostalCode) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CountryName.Code))
                                {
                                    //Address
                                    Map.DTO.Address DropoffAddress = null;
                                    logger.Info(Messages.ECar_Common_SkipReservationServiceLocationDropoffDetailByMAPAPI);
                                    DropoffAddress = ConvertAddress(reservation.Service.Location.Dropoff);

                                    ecbp.DropOffAddressType = UDI.VTOD.Domain.ECar.Const.AddressType.Address;
                                    ecbp.DropOffAddress = DropoffAddress;


                                    logger.Info("Dropoff AddressType: Address");
                                }
                                else if ((!string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.Longitude) && (!string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CountryName.Code)))
                                {
                                    result = false;
                                    logger.Warn(Messages.ECar_Common_InvalidReservationServiceLocationDropoffDetail);
                                    throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocationDropoffDetail);
                                }
                                else
                                {
                                    result = false;
                                    logger.Warn(Messages.ECar_Common_UnknownError);
                                    throw new ValidationException(Messages.ECar_Common_UnknownError);
                                }

                            }
                            else if ((reservation.Service.Location.Dropoff.Address == null) && (reservation.Service.Location.Dropoff.AirportInfo != null))
                            {
                                //airport
                                if (((reservation.Service.Location.Dropoff.AirportInfo.Arrival != null) && (reservation.Service.Location.Dropoff.AirportInfo.Departure != null)) || ((reservation.Service.Location.Dropoff.AirportInfo.Arrival == null) && (reservation.Service.Location.Dropoff.AirportInfo.Departure == null)))
                                {
                                    result = false;
                                    logger.Warn(Messages.ECar_Common_InvalidReservationServiceLocationDropoffAirportInfoContainsBothArrivalAndDepature);
                                    throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocationDropoffAirportInfoContainsBothArrivalAndDepature);
                                }
                                else if (reservation.Service.Location.Dropoff.AirportInfo.Arrival != null && reservation.Service.Location.Dropoff.AirportInfo.Departure == null)
                                {
                                    //Arrival
                                    ecbp.DropOffAddressType = AddressType.Airport;
                                    ecbp.DropoffAirport = reservation.Service.Location.Dropoff.AirportInfo.Arrival.LocationCode;
                                    logger.Info("Dropoff AddressType: AirportInfo.Arrival");
                                }
                                else if (reservation.Service.Location.Dropoff.AirportInfo.Arrival == null && reservation.Service.Location.Dropoff.AirportInfo.Departure != null)
                                {
                                    //Depature

                                    ecbp.DropOffAddressType = AddressType.Airport;
                                    ecbp.DropoffAirport = reservation.Service.Location.Dropoff.AirportInfo.Departure.LocationCode;
                                    logger.Info("Dropoff AddressType: AirportInfo.Depature");

                                }
                                //longitude and latitude, and flight information
                                if (result)
                                {
                                    double? longitude = null;
                                    double? latitude = null;
                                    if (GetLongitudeAndLatitudeForAirport(ecbp.DropoffAirport, out longitude, out latitude))
                                    {
                                        ecbp.LongitudeForDropoffAirport = longitude.Value;
                                        ecbp.LatitudeForDropoffAirport = latitude.Value;
                                    }
                                    //else
                                    //{
                                    //    throw new ValidationException(Messages.ECar_Common_UnableToFindAirportLongitudeAndLatitude);
                                    //}

                                    if (reservation.Service.Location.Dropoff.Airline != null)
                                    {
                                        if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Airline.FlightNumber))
                                        {
                                            ecbp.DropOffAirlineFlightNumber = reservation.Service.Location.Dropoff.Airline.FlightNumber;
                                        }

                                        if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Airline.Code))
                                        {
                                            ecbp.DropOffAirlineCode = reservation.Service.Location.Dropoff.Airline.Code;
                                        }

                                        if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Airline.FlightDateTime))
                                        {
                                            ecbp.DropOffAirlineFlightDateTime = reservation.Service.Location.Dropoff.Airline.FlightDateTime;
                                        }
                                        if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Airline.FlightDateTime))
                                        {
                                            ecbp.DropOffAirlineFlightDateTime = reservation.Service.Location.Dropoff.Airline.FlightDateTime;
                                        }
                                    }

                                }


                            }

                            if ((reservation.Service.Location.Dropoff.Address != null) && (!string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.Latitude) && (!string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.Longitude))))
                                {
                                    ecbp.CorporateDropoffLatitude = Convert.ToDecimal(reservation.Service.Location.Dropoff.Address.Latitude);
                                    ecbp.CorporateDropoffLongitude = Convert.ToDecimal(reservation.Service.Location.Dropoff.Address.Longitude);
                                }
                                else if ((ecbp.LongitudeForDropoffAirport != null) && (ecbp.LatitudeForDropoffAirport != null) && ((ecbp.LongitudeForDropoffAirport != 0) && (ecbp.LatitudeForDropoffAirport != 0)))
                                {
                                    ecbp.CorporateDropoffLatitude = (decimal)ecbp.LatitudeForDropoffAirport;
                                    ecbp.CorporateDropoffLongitude = (decimal)ecbp.LongitudeForDropoffAirport;
                                }
                            

                        }
                        else if (asp.dropoff != null)
                        {
                            if (asp.dropoff.address_type == "Street Address")
                            {

                                Map.DTO.Address DropoffAddress = null;
                                logger.Info(Messages.ECar_Common_SkipReservationServiceLocationDropoffDetailByMAPAPI);
                                DropoffAddress.address_type = asp.dropoff.address_type;
                                DropoffAddress.Street = asp.dropoff.street_name.Trim();
                                DropoffAddress.StreetNo = asp.dropoff.street_no;
                                DropoffAddress.FullStreet = asp.dropoff.auto_complete;
                                DropoffAddress.State = asp.dropoff.state;
                                DropoffAddress.City = asp.dropoff.city;
                                DropoffAddress.ZipCode = asp.dropoff.zip_code;
                                DropoffAddress.Latitude = asp.dropoff.Latitude.ToString();
                                DropoffAddress.Longitude = asp.dropoff.Longitude.ToString();
                                DropoffAddress.Country = asp.dropoff.country;
                                ecbp.DropOffAddressType = UDI.VTOD.Domain.ECar.Const.AddressType.Address;
                                ecbp.DropOffAddress = DropoffAddress;
                            }
                            if (asp.dropoff.address_type == "Airport")
                            {

                                Map.DTO.Address DropoffAddress = null;
                                logger.Info(Messages.ECar_Common_SkipReservationServiceLocationDropoffDetailByMAPAPI);
                                DropoffAddress.AirportCode = asp.dropoff.airport_code;
                                DropoffAddress.Airline = asp.dropoff.airline;
                                DropoffAddress.AirlineFlightNumber = asp.dropoff.flight_number;
                                DropoffAddress.City = asp.dropoff.city_of_origin;
                                DropoffAddress.Latitude = asp.dropoff.Latitude.ToString();
                                DropoffAddress.Longitude = asp.dropoff.Longitude.ToString();
                                DropoffAddress.FlightDateTime = asp.dropoff.flightDateTime;
                                ecbp.DropOffAddressType = UDI.VTOD.Domain.ECar.Const.AddressType.Airport;
                                ecbp.DropOffAddress = DropoffAddress;
                            }
                        }
                        else
                        {
                            result = false;
                            logger.Warn(Messages.ECar_Common_InvalidReservationServiceLocationDropoffDetail);
                            throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocationDropoffDetail);
                        }




                    }
                    else
                    {
                        result = false;
                        logger.Warn(Messages.ECar_Common_InvalidReservationServiceLocation);
                        throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocation);
                    }
                }
                else
                {
                    result = false;
                    logger.Warn(Messages.ECar_Common_InvalidReservationService);
                    throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
                }
                #endregion
                if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidateDropOffAddress");
                swEachPart.Stop();
                logger.DebugFormat("Validate Pickup address Duration:{0} ms.", swEachPart.ElapsedMilliseconds);
                swEachPart.Restart();

                #region Validate Aleph Inputs

                if (result)
				{
					try
					{
						var rateQualifier = request.GroundReservations.First().RateQualifiers.First();
						if (rateQualifier.SpecialInputs != null)
						{
							//CorporateEmail 
							var CorporateEmail = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateEmail".ToLowerInvariant()).FirstOrDefault();
							if (CorporateEmail != null)
							{
								ecbp.CorporateEmail = CorporateEmail.Value;
							}
							else
							{
								ecbp.CorporateEmail = null;
								result = false;
								logger.Warn(Messages.ECar_Common_InvalidReservationService);
								throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
							}


							//if (!ecbp.PickupNow)
							//{
							//CorporateProviderID
							var CorporateProviderID = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateProviderID".ToLowerInvariant()).FirstOrDefault();
							if (CorporateProviderID != null)
							{
								ecbp.CorporateProviderID = CorporateProviderID.Value;
							}
							else
							{
								if (!ecbp.PickupNow)
								{
									ecbp.CorporateProviderID = null;
									result = false;
									logger.Warn(Messages.ECar_Common_InvalidReservationService);
									throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
								}
							}


							//CorporateCompanyName
							var CorporateCompanyName = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateCompanyName".ToLowerInvariant()).FirstOrDefault();
							if (CorporateCompanyName != null)
							{
								ecbp.CorporateCompanyName = CorporateCompanyName.Value;
							}
							else
							{
								if (!ecbp.PickupNow)
								{
									ecbp.CorporateCompanyName = null;
									result = false;
									logger.Warn(Messages.ECar_Common_InvalidReservationService);

									throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
								}
							}


							//CorporateProviderAccountID
							var CorporateProviderAccountID = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateProviderAccountID".ToLowerInvariant()).FirstOrDefault();
							if (CorporateProviderAccountID != null)
							{
								ecbp.CorporateProviderAccountID = CorporateProviderAccountID.Value;
							}
							else
							{
								if (!ecbp.PickupNow)
								{
									ecbp.CorporateProviderAccountID = null;
									result = false;
									logger.Warn(Messages.ECar_Common_InvalidReservationService);
									throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
								}
							}


							//CorporateProfileID
							var CorporateProfileID = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateProfileID".ToLowerInvariant()).FirstOrDefault();
							if (CorporateProfileID != null)
							{
								ecbp.CorporateProfileID = CorporateProfileID.Value;
							}
							else
							{
								if (!ecbp.PickupNow)
								{
									ecbp.CorporateProfileID = null;
									result = false;
									logger.Warn(Messages.ECar_Common_InvalidReservationService);
									throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
								}
							}


							//CorporatePaymentMethod
							var CorporatePaymentMethod = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporatePaymentMethod".ToLowerInvariant()).FirstOrDefault();
							if (CorporatePaymentMethod != null)
							{
								ecbp.CorporatePaymentMethod = CorporatePaymentMethod.Value;
                                    if ("Credit Card".Equals(CorporatePaymentMethod.Value, StringComparison.OrdinalIgnoreCase))
                                    {
                                        var payment = request.Payments.Payments.FirstOrDefault();
                                        if (payment != null && payment.PaymentCard != null)
                                        {
                                            ecbp.PaymentMethodToken = payment.PaymentCard.CardNumber.EncryptedValue;
                                        }
                                        else
                                        {
                                        if(asp!=null && asp.reservation_payment!=null && asp.reservation_payment.payment_info!=null && !string.IsNullOrEmpty(asp.reservation_payment.payment_info.payment_method_token))
                                            ecbp.PaymentMethodToken = asp.reservation_payment.payment_info.payment_method_token;
                                        }
                                }
                            }
							else
							{
								if (!ecbp.PickupNow)
								{
									ecbp.CorporatePaymentMethod = null;
									result = false;
									logger.Warn(Messages.ECar_Common_InvalidReservationService);
									throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
								}
								else
								{
									ecbp.CorporatePaymentMethod = "Voucher";
								}
							}
							//}


							//CorporateSpecialInstructions
							var CorporateSpecialInstructions = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateSpecialInstructions".ToLowerInvariant()).FirstOrDefault();
							if (CorporateSpecialInstructions != null)
							{
								ecbp.CorporateSpecialInstructions = CorporateSpecialInstructions.Value;
							}
							else
							{
								ecbp.CorporateSpecialInstructions = null;
								result = false;
								logger.Warn(Messages.ECar_Common_InvalidReservationService);
								throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
							}


							//Booking Criteria                           
							try
							{
								if (request.TPA_Extensions.Criteria != null && request.TPA_Extensions.Criteria.SpecialInputs != null && request.TPA_Extensions.Criteria.SpecialInputs.Any())
								{
									AlephBookingCriteria bookingCriteria = null;

									ecbp.BookingCriteria = new List<AlephBookingCriteria>();

									foreach (NameValue criteria in request.TPA_Extensions.Criteria.SpecialInputs)
									{
										bookingCriteria = new AlephBookingCriteria();

										bookingCriteria.booking_criterion_name = criteria.Name;
										bookingCriteria.booking_criterion_value = criteria.Value;

										ecbp.BookingCriteria.Add(bookingCriteria);
									}
								}
								else
								{
									ecbp.BookingCriteria = null;
								}
							}
							catch (Exception ex)
							{
								ecbp.BookingCriteria = null;
								logger.Error("Aleph: Booking Criteria", ex);
							}


							ecbp.WebServiceState = rateQualifier.SpecialInputs.JsonSerialize();
						}
						else
						{
							throw VtodException.CreateException(ExceptionType.ECar, 20022);
						}


					}
					catch (Exception ex)
					{
						logger.Error("Aleph: Error While Validating Aleph Profile Info", ex);
						result = false;
						logger.Warn("Error while validating Aleph profile inputs.");
						throw VtodException.CreateException(ExceptionType.ECar, 20023);
					}
				}
				else
				{
					result = false;
					logger.Warn(Messages.ECar_Common_InvalidReservationService);
					throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidateAlephInputs");
				swEachPart.Stop();
				logger.DebugFormat("Validate Aleph Inputs Duration:{0} ms.", swEachPart.ElapsedMilliseconds);


				swEachPart.Restart();
				#region Validate User
				if (result)
				{
					ecar_fleet_user user = null;
					if (GetECarFleetUser(ecbp.Fleet.Id, tokenVTOD.Username, out user))
					{
						ecbp.User = user;
						logger.Info("This user has sufficient privilege.");
					}
					else
					{
						result = false;
						logger.Warn(Messages.ECar_Common_InsufficientPrivilege(tokenVTOD.Username, ecbp.Fleet.Id));
						throw new ValidationException(Messages.ECar_Common_InsufficientPrivilege(tokenVTOD.Username, ecbp.Fleet.Id));
					}
				}
				else
				{
					result = false;
					logger.Warn(Messages.ECar_Common_UnableProcessByPreviousInvalidRequest);
					throw new ValidationException(Messages.ECar_Common_UnableProcessByPreviousInvalidRequest);
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidateUser");
				swEachPart.Stop();
				logger.DebugFormat("Validate user Duration:{0} ms.", swEachPart.ElapsedMilliseconds);



				swEachPart.Restart();
				#region Validate duplicated booking trip
				if (result)
				{
					if (IsAbleToBookTrip(ecbp))
					{
						logger.Info("This trip can be booked.");
					}
					else
					{
						result = false;
						var vtodException = VtodException.CreateException(ExceptionType.ECar, 2006);
						logger.Warn(vtodException.ExceptionMessage.Message);
						throw vtodException; // new ValidationException(Messages.ECar_Common_DuplicatedTrips);
					}
				}
				else
				{
					result = false;
					logger.Warn(Messages.ECar_Common_UnableProcessByPreviousInvalidRequest);
					throw new ValidationException(Messages.ECar_Common_UnableProcessByPreviousInvalidRequest);
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidateDuplicatedBookingTrip");
				swEachPart.Stop();
				logger.DebugFormat("Validate duplicated trip Duration:{0} ms.", swEachPart.ElapsedMilliseconds);


				//swEachPart.Restart();
				//#region Validate Dropoff Address
				//if (asp.dropoff != null)
				//{
				//	using (VTODEntities context = new VTODEntities())
				//	{
				//		if (asp.dropoff.address_type == "Street Address")
				//		{

				//			dropoffAddress.StreetNo = asp.dropoff.street_no;
				//			dropoffAddress.Street = asp.dropoff.street_name.Trim();
				//			dropoffAddress.City = asp.dropoff.city;
				//			dropoffAddress.State = asp.dropoff.state;
				//			dropoffAddress.ZipCode = asp.dropoff.zip_code;
				//			dropoffAddress.Country = asp.dropoff.country;
				//			dropoffAddress.FullAddress = asp.dropoff.auto_complete;
				//			dropoffAddress.address_type = "Street Address";
				//			dropoffAddress.Geolocation = new UDI.Map.DTO.Geolocation { Latitude = asp.dropoff.Latitude.Value, Longitude = asp.dropoff.Longitude.Value };
				//			ecbp.DropOffAddress = dropoffAddress;
				//			ecbp.DropOffAddressType = UDI.VTOD.Domain.ECar.Const.AddressType.Address;
				//			logger.Info("Dropoff AddressType=Address");
				//		}
				//		else if (asp.dropoff.address_type == "Airport")
				//		{
				//			dropoffAddress.address_type = AddressType.Airport;
				//			dropoffAddress.airport_pickup_point = asp.dropoff.airport_pickup_point;
				//			dropoffAddress.AirportCode = asp.dropoff.airport_code;
				//			dropoffAddress.Airline = asp.dropoff.airline;
				//			dropoffAddress.AirlineFlightNumber = asp.dropoff.flight_number;
				//			dropoffAddress.FlightDateTime = asp.dropoff.flightDateTime;
				//			logger.Info("Dropoff AddressType: AirportInfo.Arrival");
				//			ecbp.DropOffAddress = dropoffAddress;
				//			ecbp.DropOffAddressType = UDI.VTOD.Domain.ECar.Const.AddressType.Airport;
				//			if (result)
				//			{
				//				double? longitude = null;
				//				double? latitude = null;
				//				if (GetLongitudeAndLatitudeForAirport(ecbp.DropoffAirport, out longitude, out latitude))
				//				{
				//					ecbp.LongitudeForDropoffAirport = longitude.Value;
				//					ecbp.LatitudeForDropoffAirport = latitude.Value;
				//				}
				//				if (reservation.Service.Location.Dropoff.Airline != null)
				//				{
				//					if (!string.IsNullOrWhiteSpace(asp.dropoff.flight_number))
				//					{
				//						ecbp.DropOffAirlineFlightNumber = asp.dropoff.flight_number;
				//					}

				//					if (!string.IsNullOrWhiteSpace(asp.dropoff.airline))
				//					{
				//						ecbp.DropOffAirlineCode = asp.dropoff.airline;
				//					}

				//					if (!string.IsNullOrWhiteSpace(asp.dropoff.airport_code))
				//					{
				//						ecbp.DropOffAirlineCodeContext = asp.dropoff.airport_code;
				//					}

				//					if (!string.IsNullOrWhiteSpace(asp.dropoff.flightDateTime))
				//					{
				//						ecbp.DropOffAirlineFlightDateTime = asp.dropoff.flightDateTime;
				//					}

				//				}
				//			}
				//		}
				//	}


				//	//For Pick me up now, make sure a Lat and Lon value are included in the Pickup request info. Change the Error message thrown to a more specific one.
				//	if (ecbp.PickupNow)
				//	{
				//		if (reservation.Service.Location.Dropoff != null)
				//		{
				//			if ((reservation.Service.Location.Dropoff.Address != null) && (!string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.Latitude) && (!string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.Longitude))))
				//			{
				//				ecbp.CorporateDropoffLatitude = Convert.ToDecimal(reservation.Service.Location.Dropoff.Address.Latitude);
				//				ecbp.CorporateDropoffLongitude = Convert.ToDecimal(reservation.Service.Location.Dropoff.Address.Longitude);
				//			}
				//			else if ((ecbp.LongitudeForDropoffAirport != null) && (ecbp.LatitudeForDropoffAirport != null) && ((ecbp.LongitudeForDropoffAirport != 0) && (ecbp.LatitudeForDropoffAirport != 0)))
				//			{
				//				ecbp.CorporateDropoffLatitude = (decimal)ecbp.LatitudeForDropoffAirport;
				//				ecbp.CorporateDropoffLongitude = (decimal)ecbp.LongitudeForDropoffAirport;
				//			}
				//		}
				//		else if (asp.dropoff != null)
				//		{
				//			if ((asp.dropoff.Latitude != null) && (asp.dropoff.Longitude != null))
				//			{
				//				ecbp.CorporateDropoffLatitude = Convert.ToDecimal((asp.dropoff.Latitude));
				//				ecbp.CorporateDropoffLongitude = Convert.ToDecimal(asp.dropoff.Longitude);
				//			}
				//			else
				//			{
				//				ecbp.CorporateDropoffLatitude = 0;
				//				ecbp.CorporateDropoffLongitude = 0;
				//			}
				//		}
				//	}


				//}
				//#endregion
				//if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidateDropOffAddress");
				//swEachPart.Stop();
				//logger.DebugFormat("Validate dropoff address Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidateDropoffDateTime");
				swEachPart.Stop();
				logger.DebugFormat("Validate dropoff time Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Validate Available address type
				if (result)
				{
					string pickupAddressType = ecbp.PickupAddressType;
					string dropoffAddressType = ecbp.DropOffAddressType;

					//verify the address type of pickup address and drop off address
					UtilityDomain ud = new UtilityDomain();
					if (pickupAddressType == AddressType.Address.ToString())
					{
						UtilityGetAirportNameRQ uganRQ = new UtilityGetAirportNameRQ();
						uganRQ.Address = new Address();
						uganRQ.Address.Latitude = ecbp.PickupAddress.Geolocation.Latitude.ToString();
						uganRQ.Address.Longitude = ecbp.PickupAddress.Geolocation.Longitude.ToString();

						UtilityGetAirportNameRS uganRS = ud.GetAirportName(tokenVTOD, uganRQ);
						if (uganRS.Success != null)
						{
							if (!string.IsNullOrWhiteSpace(uganRS.AirportName))
							{
								pickupAddressType = AddressType.Airport;
							}
						}

					}

					if (dropoffAddressType == AddressType.Address.ToString())
					{
						UtilityGetAirportNameRQ uganRQ = new UtilityGetAirportNameRQ();
						uganRQ.Address = new Address();
						uganRQ.Address.Latitude = ecbp.DropOffAddress.Geolocation.Latitude.ToString();
						uganRQ.Address.Longitude = ecbp.DropOffAddress.Geolocation.Longitude.ToString();

						UtilityGetAirportNameRS uganRS = ud.GetAirportName(tokenVTOD, uganRQ);
						if (uganRS.Success != null)
						{
							if (!string.IsNullOrWhiteSpace(uganRS.AirportName))
							{
								dropoffAddressType = AddressType.Airport;
							}
						}
					}

					//check database setting
					//1. get ecar_fleet_availableaddresstype
					using (VTODEntities context = new VTODEntities())
					{
						long fleetId = ecbp.Fleet.Id;
						if (context.ecar_fleet_availableaddresstype.Where(x => fleetId == x.FleetId).Any())
						{
							ecar_fleet_availableaddresstype ecfa = context.ecar_fleet_availableaddresstype.Where(x => fleetId == x.FleetId).Select(x => x).FirstOrDefault();
							//2. check if the type is available or not
							//ECar_Common_InvalidAddressTypeForFleet
							if (AddressType.Address.ToString() == pickupAddressType && AddressType.Address.ToString() == dropoffAddressType)
							{
								//address to address
								if (ecfa.AddressToAddress)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.ECar, 1013);// (Messages.Validation_InvalidAddressType);
								}
							}
							else if (AddressType.Address.ToString() == pickupAddressType && AddressType.Airport.ToString() == dropoffAddressType)
							{
								//address to airport
								if (ecfa.AddressToAirport)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.ECar, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
							else if (AddressType.Address.ToString() == pickupAddressType && null == dropoffAddressType)
							{
								//address to null
								if (ecfa.AddressToNull)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.ECar, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
							else if (AddressType.Airport.ToString() == pickupAddressType && AddressType.Address.ToString() == dropoffAddressType)
							{
								//airport to address
								if (ecfa.AirportToAddress)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.ECar, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
							else if (AddressType.Airport.ToString() == pickupAddressType && AddressType.Airport.ToString() == dropoffAddressType)
							{
								//airport to airport
								if (ecfa.AirportToAirport)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.ECar, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
							else if (AddressType.Airport.ToString() == pickupAddressType && null == dropoffAddressType)
							{
								//airport to null
								if (ecfa.AirportToNull)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.ECar, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
						}
						else
						{
							result = false;
							throw VtodException.CreateException(ExceptionType.ECar, 1011); //ValidationException(Messages.ECar_Common_UnableToGetAvailableFleets);
						}
					}


				}
				else
				{
					result = false;
					var vtodException = Common.DTO.VtodException.CreateFieldRequiredValidationException("Payment");
					logger.Warn(vtodException.ExceptionMessage.Message);
					throw vtodException; // new ValidationException(Messages.Validation_NullPayment);
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidateAvailableAddressType");
				swEachPart.Stop();
				logger.DebugFormat("Validate available address type Duration:{0} ms.", swEachPart.ElapsedMilliseconds);


				swEachPart.Restart();
				#region Validate PaymentInfo

				#region Authorize
				var membershipDomain = new UDI.VTOD.Domain.Membership.MembershipDomain();
				ecbp.PaymentType = Common.DTO.Enum.PaymentType.CorporateBilling;
				#endregion

				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidatePaymentInfo");
				swEachPart.Stop();
				logger.DebugFormat("Validate payment Id Duration:{0} ms.", swEachPart.ElapsedMilliseconds);



				swEachPart.Restart();
				#region Validate Member Info
				//As of this version, all requester should pass memberID.
				//We need to modify this based on some others who do not pass memberID

				if (request.TPA_Extensions == null && string.IsNullOrWhiteSpace(request.TPA_Extensions.MemberID))
				{
					result = false;
					var vtodException = Common.DTO.VtodException.CreateFieldRequiredValidationException("MemberID");
					logger.Warn(vtodException.ExceptionMessage.Message);
					throw vtodException; // new ValidationException(Messages.Validation_NullPayment);
				}
				else
				{
					ecbp.MemberID = request.TPA_Extensions.MemberID.ToInt32();
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidateMemberInfo");
				swEachPart.Stop();
				logger.DebugFormat("Validate member Info Duration:{0} ms.", swEachPart.ElapsedMilliseconds);



				swEachPart.Restart();
				#region Get Gratuity
				try
				{
					var rateQuialifier = request.GroundReservations.First().RateQualifiers.First();
					if (rateQuialifier.SpecialInputs != null)
					{
						//rate
						var gratuities = rateQuialifier.SpecialInputs.Where(s => s.Name.ToLower() == "gratuity".ToLower()).ToList();

						if (gratuities != null && gratuities.Any())
						{
							decimal f;
							if (decimal.TryParse(gratuities.First().Value.Replace("%", "").ToString(), out f))
								ecbp.Gratuity = gratuities.First().Value;
							else
								throw new ValidationException("Invalid Gratuity");
						}
					}
				}
				catch (Exception ex)
				{
					logger.Error("ECar:Gratuity", ex);
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ GetGratuity");
				swEachPart.Stop();
				logger.DebugFormat("Get Gratuity Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Get Rate
				try
				{
					var rateQuialifier = request.GroundReservations.First().RateQualifiers.First();
					if (rateQuialifier.SpecialInputs != null)
					{
						//rate
						var rates = rateQuialifier.SpecialInputs.Where(s => s.Name.ToLower() == "rate".ToLower()).ToList();


						if (rates != null && rates.Any())
						{
							decimal f;
							if (decimal.TryParse(rates.First().Value, out f))
								ecbp.Rate = rates.First().Value;
							else
								throw new ValidationException("Invalid fare rate");
						}
					}
				}
				catch (Exception ex)
				{
					logger.Error("ECar:Rate", ex);
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ GetRate");
				swEachPart.Stop();
				logger.DebugFormat("Get Rate Duration:{0} ms.", swEachPart.ElapsedMilliseconds);


				swEachPart.Restart();
				#region Get Commission
				try
				{
					var rateQuialifier = request.GroundReservations.First().RateQualifiers.First();
					if (rateQuialifier.SpecialInputs != null)
					{
						//Commission
						var commissions = rateQuialifier.SpecialInputs.Where(s => s.Name.ToLower() == "commission".ToLower()).ToList();


						if (commissions != null && commissions.Any())
						{
							decimal f;
							if (decimal.TryParse(commissions.First().Value, out f))
								ecbp.commission = commissions.First().Value;
							else
								throw new ValidationException("Invalid commission");
						}
					}
				}
				catch (Exception ex)
				{
					logger.Error("ECar:Commission", ex);
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ GetCommission");
				swEachPart.Stop();
				logger.DebugFormat("Get Commission Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Get number of passenger
				try
				{
					if (request.TPA_Extensions.Passengers != null && request.TPA_Extensions.Passengers.Any())
					{
						ecbp.numberOfPassenger = request.TPA_Extensions.Passengers.Sum(x => x.Quantity);
					}
					else
					{
						ecbp.numberOfPassenger = 1;
					}
				}
				catch (Exception ex)
				{
					logger.Error("ECar: Number of Passenger", ex);
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ GetNumberOfPassenger");
				swEachPart.Stop();
				logger.DebugFormat("Get Number of Passenger Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Get controller notes
				try
				{
					var rateQuialifier = request.GroundReservations.First().RateQualifiers.First();
					if (rateQuialifier.SpecialInputs != null)
					{
						//rate
						var controllerNotes = rateQuialifier.SpecialInputs.Where(s => s.Name.ToLower() == "ControllerNotes".ToLower()).FirstOrDefault();

						if (controllerNotes != null && !string.IsNullOrWhiteSpace(controllerNotes.Value))
						{
							ecbp.controllerNotes = controllerNotes.Value;
						}
					}
				}
				catch (Exception ex)
				{
					logger.Error("ECar: Get controller notes", ex);
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ GeControllerNotes");
				swEachPart.Stop();
				logger.DebugFormat("Get controller notes Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Stops
				//List<Map.DTO.Address> Stops = new List<Map.DTO.Address>();
				//if ((reservation.Service.Location != null) && (reservation.Service.Location.Stops != null) && (reservation.Service.Location.Stops.Stops != null))
				//{
				//    if (reservation.Service.Location.Stops.Stops.Any())
				//    {
				//        foreach (var item in reservation.Service.Location.Stops.Stops)
				//        {
				//            if (item.AirportInfo != null)
				//            {
				//                Map.DTO.Address stop = new Map.DTO.Address();
				//                if (item.Airline != null)
				//                {
				//                    if (!string.IsNullOrWhiteSpace(item.Airline.FlightNumber))
				//                    {
				//                        stop.AirlineFlightNumber = item.Airline.FlightNumber;
				//                    }

				//                    if (!string.IsNullOrWhiteSpace(item.Airline.Code))
				//                    {
				//                        stop.Airline = item.Airline.Code;
				//                    }

				//                    if (!string.IsNullOrWhiteSpace(item.Airline.FlightDateTime))
				//                    {
				//                        stop.FlightDateTime = item.Airline.FlightDateTime;
				//                    }

				//                    if (!string.IsNullOrWhiteSpace(item.AirportInfo.Arrival.LocationCode))
				//                    {
				//                        stop.AirportCode = item.AirportInfo.Arrival.LocationCode;
				//                    }
				//                    Stops.Add(stop);
				//                }
				//            }
				//            if (item.Address != null)
				//            {

				//                Map.DTO.Address stop = new Map.DTO.Address();
				//                stop = ConvertAddress(item);
				//                stop.address_type = UDI.VTOD.Domain.ECar.Const.AddressType.Address;
				//                //string AddressStr = GetAddressString(item.Address);
				//                //stop.StreetNo = item.Address.StreetNmbr;
				//                //stop.Street = item.Address.AddressLine;
				//                //stop.FullAddress = item.Address.AddressLine;
				//                //stop.City = item.Address.CityName;
				//                //stop.State = item.Address.StateProv.StateCode;
				//                //stop.Country = item.Address.CountryName.Code;
				//                //stop.ZipCode = item.Address.PostalCode;
				//                //stop.Latitude = item.Address.Latitude;
				//                //stop.Longitude = item.Address.Longitude;
				//                Stops.Add(stop);

				//            }
				//        }
				//    }

				//    if (result)
				//    {
				//        ecbp.Stops = Stops;
				//    }
				//}
				#endregion

				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ GetStops");
				swEachPart.Stop();
				logger.DebugFormat("Get controller notes Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Create this trip into database
				if (result)
				{
					vtod_trip trip = UpdateTrip(ecbp, tokenVTOD.Username, alephVendorTripID, type);
					ecbp.Trip = trip;
				}
				else
				{
					result = false;
					var vtodException = VtodException.CreateException(ExceptionType.ECar, 2001);
					logger.Warn(vtodException.ExceptionMessage.Message);
					throw vtodException;
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ GreateTripInDatabase");
				swEachPart.Stop();
				logger.DebugFormat("UpdateTrip in db  Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

			}
			else
			{
				result = false;
				var vtodException = VtodException.CreateFieldRequiredValidationException("GroundReservation");
				logger.Warn(vtodException.ExceptionMessage.Message);
				throw vtodException;
			}
			#endregion

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);


			#region Create ECar log
			if (result)
			{
				if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
				{
					WriteECarLog(ecbp.Trip.Id, ecbp.serviceAPIID, ECarServiceMethodType.Book, sw.ElapsedMilliseconds, string.Empty, string.Empty, null, null);
				}
			}
			else
			{
				result = false;
				logger.Warn(Messages.ECar_Common_UnableProcessByPreviousInvalidRequest);
				throw new ValidationException(Messages.ECar_Common_UnableProcessByPreviousInvalidRequest);
			}
			#endregion

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest");
			#endregion


			return result;

		}

		public bool ValidateBookingRequest(TokenRS tokenVTOD, OTA_GroundBookRQ request, ref AlephBookingParameter ecbp)
		{
			bool result = true;
			Stopwatch sw = new Stopwatch();
			sw.Start();

			Stopwatch swEachPart = new Stopwatch();



			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
			#endregion

			#region Validate Reservation
			if (request.GroundReservations.Any())
			{
				GroundReservation reservation = request.GroundReservations.FirstOrDefault();

				#region Set token, request
				ecbp.tokenVTOD = tokenVTOD;
				ecbp.request = request;
				#endregion

				#region Set Source and Device and ref
				if (request.TPA_Extensions != null && !string.IsNullOrWhiteSpace(request.TPA_Extensions.Source))
				{
					ecbp.Source = request.TPA_Extensions.Source;
				}

				if (request.TPA_Extensions != null && !string.IsNullOrWhiteSpace(request.TPA_Extensions.Device))
				{
					ecbp.Device = request.TPA_Extensions.Device;
				}

				if (!string.IsNullOrWhiteSpace(request.EchoToken))
				{
					ecbp.Ref = request.EchoToken;
				}
				#endregion

				swEachPart.Restart();
				#region Validate Customer
				if (reservation.Passenger != null)
				{
					//Name
					if (reservation.Passenger.Primary != null)
					{
						if (reservation.Passenger.Primary.PersonName != null)
						{
							if (string.IsNullOrWhiteSpace(reservation.Passenger.Primary.PersonName.GivenName) || string.IsNullOrWhiteSpace(reservation.Passenger.Primary.PersonName.Surname))
							{

								result = false;
								logger.Warn(Messages.ECar_Common_InvalidReservationPassengerPrimaryPersonNameDetail);
								throw new ValidationException(Messages.ECar_Common_InvalidReservationPassengerPrimaryPersonNameDetail);
							}
							else
							{
								ecbp.FirstName = reservation.Passenger.Primary.PersonName.GivenName.Trim();
								ecbp.LastName = reservation.Passenger.Primary.PersonName.Surname.Trim();
								logger.Info(Messages.ECar_Common_ValidReservationPassengerPrimaryPersonNameDetail);
							}
						}
						else
						{
							result = false;
							logger.Warn(Messages.ECar_Common_InvalidReservationPassengerPrimaryPersonName);
							throw new ValidationException(Messages.ECar_Common_InvalidReservationPassengerPrimaryPersonName);
						}

						//Telephone
						if (reservation.Passenger.Primary.Telephones.Any())
						{
							Telephone p = reservation.Passenger.Primary.Telephones.FirstOrDefault();
							string countrycode = ExtractDigitsWithPlus(p.CountryAccessCode);
							string areacode = ExtractDigits(p.AreaCityCode);
							string phonenumber = ExtractDigits(p.PhoneNumber);
							//if (areacode.Length.Equals(3) && phonenumber.Length.Equals(7))
							//{
							ecbp.PhoneNumber = string.Format("{0}{1}{2}", countrycode, areacode, phonenumber);
							logger.Info(Messages.ECar_Common_ValidReservationPassengerPrimaryTelephones);
							//}
							//else
							//{
							//	result = false;
							//	logger.Warn(Messages.ECar_Common_InvalidReservationPassengerPrimaryTelephones);
							//	throw new ValidationException(Messages.ECar_Common_InvalidReservationPassengerPrimaryTelephones);
							//}
						}
						else
						{
							result = false;
							logger.Warn(Messages.ECar_Common_InvalidReservationPassengerPrimaryPhoneNumber);
							throw new ValidationException(Messages.ECar_Common_InvalidReservationPassengerPrimaryPhoneNumber);
						}

						//email
						if (reservation.Passenger.Primary.Emails.Any())
						{
							Email e = reservation.Passenger.Primary.Emails.FirstOrDefault();


							string emailPattern = @"^(?("")("".+?(?<!\\)""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
												 @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]*\.)+[a-zA-Z0-9][\-a-zA-Z0-9]{0,22}[a-zA-Z0-9]))$";

							if (!string.IsNullOrWhiteSpace(e.Value) && Regex.IsMatch(e.Value, emailPattern, RegexOptions.IgnorePatternWhitespace))
							{
								ecbp.emailAddress = e.Value;
								logger.Info(Messages.ECar_Common_ValidReservationPassengerPrimaryTelephones);
							}
							else
							{
								result = false;
								logger.Warn(Messages.ECar_Common_InvalidReservationPassengerPrimaryEmails);
								throw new ValidationException(Messages.ECar_Common_InvalidReservationPassengerPrimaryEmails);
							}
						}

						if (result)
						{
							logger.InfoFormat("Customer firstname={0}, lastname={1}, phonenumber={2}", ecbp.FirstName, ecbp.LastName, ecbp.PhoneNumber);
						}
						else
						{
							result = false;
							logger.Warn(Messages.ECar_Common_UnableToCreateOrFindECarCustomer);
							throw new ValidationException(Messages.ECar_Common_UnableToCreateOrFindECarCustomer);
						}
					}
					else
					{
						result = false;
						logger.Warn(Messages.ECar_Common_InvalidReservationPassengerPrimary);
						throw new ValidationException(Messages.ECar_Common_InvalidReservationPassengerPrimary);
					}
				}
				else
				{
					result = false;
					logger.Warn(Messages.ECar_Common_InvalidReservationPassenger);
					throw new ValidationException(Messages.ECar_Common_InvalidReservationPassenger);
				}

				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidateCustomer");
				swEachPart.Stop();
				logger.DebugFormat("Validate Customer Duration:{0} ms.", swEachPart.ElapsedMilliseconds);


				swEachPart.Restart();
				#region Validate PickupDateTime
				if (result)
				{
					//pickiup now
					//bool pickupNow=false;
					if (request.TPA_Extensions != null && request.TPA_Extensions.PickMeUpNow.HasValue)
					{
						if (request.TPA_Extensions.PickMeUpNow.Value)
						{
							ecbp.PickupNow = true;
						}
						else
						{
							ecbp.PickupNow = false;
						}
					}
					else
					{
						ecbp.PickupNow = false;
					}


					DateTime pickupDateTime;
					//if (DateTime.TryParseExact(reservation.Service.Location.Pickup.DateTime, ConfigurationManager.AppSettings["DateTimeFormatForPickupAndDropoff"], null, DateTimeStyles.None, out pickupDateTime))
					if (DateTime.TryParse(reservation.Service.Location.Pickup.DateTime, out pickupDateTime))
					{
						ecbp.PickupDateTime = pickupDateTime;
						logger.Info("PickupDateTime is correct");
					}
					else if (ecbp.PickupNow && !DateTime.TryParse(reservation.Service.Location.Pickup.DateTime, out pickupDateTime))
					{
						ecbp.PickupDateTime = DateTime.Now;
						logger.InfoFormat("There is no Pickup datetime for this request. But it has pickup now value. So the pickup time will set as {0:yyyy/MM/dd HH:mm:ss}.", ecbp.PickupDateTime);
					}
					else
					{
						result = false;
						logger.Warn(Messages.ECar_Common_InvalidReservationServiceLocationPickupDateTime(reservation.Service.Location.Pickup.DateTime));
						throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocationPickupDateTime(reservation.Service.Location.Pickup.DateTime));
					}
				}
				else
				{
					result = false;
					logger.Warn(Messages.ECar_Common_UnableProcessByPreviousInvalidRequest);
					throw new ValidationException(Messages.ECar_Common_UnableProcessByPreviousInvalidRequest);
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidatePickupDateTime");
				swEachPart.Stop();
				logger.DebugFormat("Validate PickupTime Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Airport Pick Up Point
				if (!string.IsNullOrWhiteSpace(request.TPA_Extensions.AirportPickupPoint))
				{
					ecbp.AirportPickupPoint = request.TPA_Extensions.AirportPickupPoint;
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidateUser");
				swEachPart.Stop();
				logger.DebugFormat("Validate user Duration:{0} ms.", swEachPart.ElapsedMilliseconds);
				swEachPart.Restart();
				#region Validate Pickup Address

				if (reservation.Service != null)
				{
					if (reservation.Service.Location != null)
					{
						if (reservation.Service.Location.Pickup != null)
						{

							if (((reservation.Service.Location.Pickup.Address != null) && (reservation.Service.Location.Pickup.AirportInfo != null)) || ((reservation.Service.Location.Pickup.Address == null) && (reservation.Service.Location.Pickup.AirportInfo == null)))
							{
								result = false;
								logger.Warn(Messages.ECar_Common_InvalidReservationServiceLocationPickupContainsBothPickupAddressAndAirportInfo);
								throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocationPickupContainsBothPickupAddressAndAirportInfo);
							}
							else if ((reservation.Service.Location.Pickup.Address != null) && (reservation.Service.Location.Pickup.AirportInfo == null))
							{

								if ((string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude) || (string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CountryName.Code)))
								{
									result = false;
									logger.Warn(Messages.ECar_Common_InvalidReservationServiceLocationPickupDetail);
									throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocationPickupDetail);
								}
								else if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.AddressLine) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CityName) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.PostalCode) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CountryName.Code))
								{
									//Address
									Map.DTO.Address pickupAddress = null;
									logger.Info(Messages.ECar_Common_SkipReservationServiceLocationPickupDetailByMAPAPI);
									pickupAddress = ConvertAddress(reservation.Service.Location.Pickup);

									ecbp.PickupAddressType = UDI.VTOD.Domain.ECar.Const.AddressType.Address;
									ecbp.PickupAddress = pickupAddress;


									logger.Info("Pickup AddressType: Address");
								}
								else if ((!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude) && (!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CountryName.Code)))
								{
									result = false;
									logger.Warn(Messages.ECar_Common_InvalidReservationServiceLocationPickupDetail);
									throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocationPickupDetail);
								}
								else
								{
									result = false;
									logger.Warn(Messages.ECar_Common_UnknownError);
									throw new ValidationException(Messages.ECar_Common_UnknownError);
								}

							}
							else if ((reservation.Service.Location.Pickup.Address == null) && (reservation.Service.Location.Pickup.AirportInfo != null))
							{
								//airport
								if (((reservation.Service.Location.Pickup.AirportInfo.Arrival != null) && (reservation.Service.Location.Pickup.AirportInfo.Departure != null)) || ((reservation.Service.Location.Pickup.AirportInfo.Arrival == null) && (reservation.Service.Location.Pickup.AirportInfo.Departure == null)))
								{
									result = false;
									logger.Warn(Messages.ECar_Common_InvalidReservationServiceLocationPickupAirportInfoContainsBothArrivalAndDepature);
									throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocationPickupAirportInfoContainsBothArrivalAndDepature);
								}
								else if (reservation.Service.Location.Pickup.AirportInfo.Arrival != null && reservation.Service.Location.Pickup.AirportInfo.Departure == null)
								{
									//Arrival
									ecbp.PickupAddressType = AddressType.Airport;
									ecbp.PickupAirport = reservation.Service.Location.Pickup.AirportInfo.Arrival.LocationCode;
									logger.Info("Pickup AddressType: AirportInfo.Arrival");
								}
								else if (reservation.Service.Location.Pickup.AirportInfo.Arrival == null && reservation.Service.Location.Pickup.AirportInfo.Departure != null)
								{
									//Depature

									ecbp.PickupAddressType = AddressType.Airport;
									ecbp.PickupAirport = reservation.Service.Location.Pickup.AirportInfo.Departure.LocationCode;
									logger.Info("Pickup AddressType: AirportInfo.Depature");

								}
								//longitude and latitude, and flight information
								if (result)
								{
									double? longitude = null;
									double? latitude = null;
									if (GetLongitudeAndLatitudeForAirport(ecbp.PickupAirport, out longitude, out latitude))
									{
										ecbp.LongitudeForPickupAirport = longitude.Value;
										ecbp.LatitudeForPickupAirport = latitude.Value;
									}
									//else
									//{
									//    throw new ValidationException(Messages.ECar_Common_UnableToFindAirportLongitudeAndLatitude);
									//}

									if (reservation.Service.Location.Pickup.Airline != null)
									{
										if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Airline.FlightNumber))
										{
											ecbp.PickupAirlineFlightNumber = reservation.Service.Location.Pickup.Airline.FlightNumber;
										}

										if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Airline.Code))
										{
											ecbp.PickupAirlineCode = reservation.Service.Location.Pickup.Airline.Code;
										}

										if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Airline.FlightDateTime))
										{
											ecbp.PickupAirlineFlightDateTime = reservation.Service.Location.Pickup.Airline.FlightDateTime;
										}
										if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Airline.FlightDateTime))
										{
											ecbp.PickupAirlineFlightDateTime = reservation.Service.Location.Pickup.Airline.FlightDateTime;
										}
									}

								}


							}

							//For Pick me up now, make sure a Lat and Lon value are included in the Pickup request info. Change the Error message thrown to a more specific one.
							if (ecbp.PickupNow)
							{
								if ((reservation.Service.Location.Pickup.Address != null) && (!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Latitude) && (!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude))))
								{
									ecbp.CorporatePickupLatitude = Convert.ToDecimal(reservation.Service.Location.Pickup.Address.Latitude);
									ecbp.CorporatePickupLongitude = Convert.ToDecimal(reservation.Service.Location.Pickup.Address.Longitude);
								}
								else if ((ecbp.LongitudeForPickupAirport != 0) && (ecbp.LatitudeForPickupAirport != 0))
								{
									ecbp.CorporatePickupLatitude = (decimal)ecbp.LatitudeForPickupAirport;
									ecbp.CorporatePickupLongitude = (decimal)ecbp.LongitudeForPickupAirport;
								}
								else
								{
									result = false;
									logger.Warn(Messages.ECar_Common_InvalidReservationServiceLocationPickupDetail);
									throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocationPickupDetail);
								}
							}

						}
						else
						{
							result = false;
							logger.Warn(Messages.ECar_Common_InvalidReservationServiceLocationPickup);
							throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocationPickup);
						}




					}
					else
					{
						result = false;
						logger.Warn(Messages.ECar_Common_InvalidReservationServiceLocation);
						throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocation);
					}
				}
				else
				{
					result = false;
					logger.Warn(Messages.ECar_Common_InvalidReservationService);
					throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidatePickupAddress");
				swEachPart.Stop();
				logger.DebugFormat("Validate Pickup address Duration:{0} ms.", swEachPart.ElapsedMilliseconds);


				swEachPart.Restart();
				#region Validate Aleph Inputs

				if (result)
				{
					try
					{
						var rateQualifier = request.GroundReservations.First().RateQualifiers.First();
						if (rateQualifier.SpecialInputs != null)
						{
							//CorporateEmail 
							var CorporateEmail = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateEmail".ToLowerInvariant()).FirstOrDefault();
							if (CorporateEmail != null)
							{
								ecbp.CorporateEmail = CorporateEmail.Value;
							}
							else
							{
								ecbp.CorporateEmail = null;
								result = false;
								logger.Warn(Messages.ECar_Common_InvalidReservationService);
								throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
							}


							//if (!ecbp.PickupNow)
							//{
							//CorporateProviderID
							var CorporateProviderID = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateProviderID".ToLowerInvariant()).FirstOrDefault();
							if (CorporateProviderID != null)
							{
								ecbp.CorporateProviderID = CorporateProviderID.Value;
							}
							else
							{
								if (!ecbp.PickupNow)
								{
									ecbp.CorporateProviderID = null;
									result = false;
									logger.Warn(Messages.ECar_Common_InvalidReservationService);
									throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
								}
							}


							//CorporateCompanyName
							var CorporateCompanyName = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateCompanyName".ToLowerInvariant()).FirstOrDefault();
							if (CorporateCompanyName != null)
							{
								ecbp.CorporateCompanyName = CorporateCompanyName.Value;
							}
							else
							{
								if (!ecbp.PickupNow)
								{
									ecbp.CorporateCompanyName = null;
									result = false;
									logger.Warn(Messages.ECar_Common_InvalidReservationService);

									throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
								}
							}


							//CorporateProviderAccountID
							var CorporateProviderAccountID = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateProviderAccountID".ToLowerInvariant()).FirstOrDefault();
							if (CorporateProviderAccountID != null)
							{
								ecbp.CorporateProviderAccountID = CorporateProviderAccountID.Value;
							}
							else
							{
								if (!ecbp.PickupNow)
								{
									ecbp.CorporateProviderAccountID = null;
									result = false;
									logger.Warn(Messages.ECar_Common_InvalidReservationService);
									throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
								}
							}


							//CorporateProfileID
							var CorporateProfileID = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateProfileID".ToLowerInvariant()).FirstOrDefault();
							if (CorporateProfileID != null)
							{
								ecbp.CorporateProfileID = CorporateProfileID.Value;
							}
							else
							{
								if (!ecbp.PickupNow)
								{
									ecbp.CorporateProfileID = null;
									result = false;
									logger.Warn(Messages.ECar_Common_InvalidReservationService);
									throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
								}
							}


							//CorporatePaymentMethod
							var CorporatePaymentMethod = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporatePaymentMethod".ToLowerInvariant()).FirstOrDefault();
							if (CorporatePaymentMethod != null)
							{
								ecbp.CorporatePaymentMethod = CorporatePaymentMethod.Value;
                                if ("Credit Card".Equals(CorporatePaymentMethod.Value,StringComparison.OrdinalIgnoreCase))
                                {
                                    var payment = request.Payments.Payments.FirstOrDefault();
                                    if (payment != null && payment.PaymentCard != null)
                                    {
                                        ecbp.PaymentMethodToken = payment.PaymentCard.CardNumber.EncryptedValue;
                                    }
                                    
                                }
							}
							else
							{
								if (!ecbp.PickupNow)
								{
									ecbp.CorporatePaymentMethod = null;
									result = false;
									logger.Warn(Messages.ECar_Common_InvalidReservationService);
									throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
								}
								else
								{
									ecbp.CorporatePaymentMethod = "Voucher";
								}
							}
							//}


							//CorporateSpecialInstructions
							var CorporateSpecialInstructions = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateSpecialInstructions".ToLowerInvariant()).FirstOrDefault();
							if (CorporateSpecialInstructions != null)
							{
								ecbp.CorporateSpecialInstructions = CorporateSpecialInstructions.Value;
							}
							else
							{
								ecbp.CorporateSpecialInstructions = null;
								result = false;
								logger.Warn(Messages.ECar_Common_InvalidReservationService);
								throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
							}

							//CorporateName
							var CorporateName = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateName".ToLowerInvariant()).FirstOrDefault();
							if (CorporateName != null)
							{
								ecbp.CorporateName = CorporateName.Value;
							}
							else
							{
								ecbp.CorporateName = null;
								result = true;
								logger.Warn(Messages.ECar_Common_InvalidReservationService);
								//throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
							}
							//CorporateAccountName
							var CorporateAccountName = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "Account".ToLowerInvariant()).FirstOrDefault();
							if (CorporateAccountName != null)
							{
								ecbp.CorporateAccountName = CorporateAccountName.Value;
							}
							else
							{
								ecbp.CorporateAccountName = null;
								result = true;
								logger.Warn(Messages.ECar_Common_InvalidReservationService);
								//throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
							}

							//Booking Criteria                           
							try
							{
								if (request.TPA_Extensions.Criteria != null && request.TPA_Extensions.Criteria.SpecialInputs != null && request.TPA_Extensions.Criteria.SpecialInputs.Any())
								{
									AlephBookingCriteria bookingCriteria = null;

									ecbp.BookingCriteria = new List<AlephBookingCriteria>();

									foreach (NameValue criteria in request.TPA_Extensions.Criteria.SpecialInputs)
									{
										bookingCriteria = new AlephBookingCriteria();

										bookingCriteria.booking_criterion_name = criteria.Name;
										bookingCriteria.booking_criterion_value = criteria.Value;

										ecbp.BookingCriteria.Add(bookingCriteria);
									}
								}
								else
								{
									ecbp.BookingCriteria = null;
								}
							}
							catch (Exception ex)
							{
								ecbp.BookingCriteria = null;
								logger.Error("Aleph: Booking Criteria", ex);
							}


							ecbp.WebServiceState = rateQualifier.SpecialInputs.JsonSerialize();
						}
						else
						{
							throw VtodException.CreateException(ExceptionType.ECar, 20022);
						}


					}
					catch (Exception ex)
					{
						logger.Error("Aleph: Error While Validating Aleph Profile Info", ex);
						result = false;
						logger.Warn("Error while validating Aleph profile inputs.");
						throw VtodException.CreateException(ExceptionType.ECar, 20023);
					}
				}
				else
				{
					result = false;
					logger.Warn(Messages.ECar_Common_InvalidReservationService);
					throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidateAlephInputs");
				swEachPart.Stop();
				logger.DebugFormat("Validate Aleph Inputs Duration:{0} ms.", swEachPart.ElapsedMilliseconds);


				swEachPart.Restart();
				#region Validate User
				if (result)
				{
					ecar_fleet_user user = null;
					if (GetECarFleetUser(ecbp.Fleet.Id, tokenVTOD.Username, out user))
					{
						ecbp.User = user;
						logger.Info("This user has sufficient privilege.");
					}
					else
					{
						result = false;
						logger.Warn(Messages.ECar_Common_InsufficientPrivilege(tokenVTOD.Username, ecbp.Fleet.Id));
						throw new ValidationException(Messages.ECar_Common_InsufficientPrivilege(tokenVTOD.Username, ecbp.Fleet.Id));
					}
				}
				else
				{
					result = false;
					logger.Warn(Messages.ECar_Common_UnableProcessByPreviousInvalidRequest);
					throw new ValidationException(Messages.ECar_Common_UnableProcessByPreviousInvalidRequest);
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidateUser");
				swEachPart.Stop();
				logger.DebugFormat("Validate user Duration:{0} ms.", swEachPart.ElapsedMilliseconds);



				swEachPart.Restart();
				#region Validate duplicated booking trip
				if (result)
				{
					if (IsAbleToBookTrip(ecbp))
					{
						logger.Info("This trip can be booked.");
					}
					else
					{
						result = false;
						var vtodException = VtodException.CreateException(ExceptionType.ECar, 2006);
						logger.Warn(vtodException.ExceptionMessage.Message);
						throw vtodException; // new ValidationException(Messages.ECar_Common_DuplicatedTrips);
					}
				}
				else
				{
					result = false;
					logger.Warn(Messages.ECar_Common_UnableProcessByPreviousInvalidRequest);
					throw new ValidationException(Messages.ECar_Common_UnableProcessByPreviousInvalidRequest);
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidateDuplicatedBookingTrip");
				swEachPart.Stop();
				logger.DebugFormat("Validate duplicated trip Duration:{0} ms.", swEachPart.ElapsedMilliseconds);


				swEachPart.Restart();
				#region Validate Dropoff Address
				//DropOff is optional: By_Pouyan
				if ((reservation.Service.Location != null) && (reservation.Service.Location.Dropoff != null) && (((reservation.Service.Location.Dropoff.Address != null) && ((!string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.AddressLine)))) || (reservation.Service.Location.Dropoff.AirportInfo != null)))
					if (result)
					{
						if (reservation.Service != null)
						{
							if (reservation.Service.Location != null)
							{
								if (reservation.Service.Location.Dropoff != null)
								{
									using (VTODEntities context = new VTODEntities())
									{
										if (((reservation.Service.Location.Dropoff.Address != null) && (reservation.Service.Location.Dropoff.AirportInfo != null)) || ((reservation.Service.Location.Dropoff.Address == null) && (reservation.Service.Location.Dropoff.AirportInfo == null)))
										{
											result = false;
											var vtodException = VtodException.CreateValidationException(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
											logger.Warn(vtodException.ExceptionMessage.Message);
											throw vtodException; // new ValidationException(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
										}
										else if ((reservation.Service.Location.Dropoff.Address != null) && (reservation.Service.Location.Dropoff.AirportInfo == null))
										{
											//Address
											if ((string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.Longitude) || (string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CountryName.Code)))
											//if (string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CountryName.Code))
											{
												result = false;
												logger.Warn(Messages.Validation_LocationDetail);
												throw VtodException.CreateValidationException(Messages.Validation_LocationDetail);
											}
											else if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.AddressLine) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CityName) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.PostalCode) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CountryName.Code))
											{
												Map.DTO.Address dropoffAddress = null;
												string dropoffAddressStr = GetAddressString(reservation.Service.Location.Dropoff.Address);
												//if (reservation.Service.Location.Dropoff.Address.TPA_Extensions != null)
												//{
												//	if (reservation.Service.Location.Dropoff.Address.TPA_Extensions.AddressValidationRequired.HasValue ? reservation.Service.Location.Dropoff.Address.TPA_Extensions.AddressValidationRequired.Value : false)
												//	{
												//		if (!LookupAddress(reservation.Service.Location.Dropoff, dropoffAddressStr, out dropoffAddress))
												//		{
												//			result = false;
												//			logger.WarnFormat(Messages.Validation_LocationDetailByMapApi);
												//			throw VtodException.CreateValidationException(Messages.Validation_LocationDetailByMapApi);
												//		}
												//	}
												//	else
												//	{
												//		if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.Longitude) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.Latitude))
												//		{
												//			logger.Info("Skip MAP API Validation.");
												//			dropoffAddress = ConvertAddress(reservation.Service.Location.Dropoff);
												//		}
												//		else
												//		{
												//			result = false;
												//			logger.WarnFormat(Messages.ECar_Common_InvalidReservationServiceLocationNoMAPAPIWithEmptyLongAndLat(dropoffAddressStr));
												//			throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocationNoMAPAPIWithEmptyLongAndLat(dropoffAddressStr));
												//		}
												//	}
												//}
												//else
												//{
												logger.Info("Skip MAP API Validation.");
												dropoffAddress = ConvertAddress(reservation.Service.Location.Dropoff);
												//}

												ecbp.DropOffAddress = dropoffAddress;
												ecbp.DropOffAddressType = UDI.VTOD.Domain.ECar.Const.AddressType.Address;
												logger.Info("Dropoff AddressType=Address");
											}
											else if ((!string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.Longitude) && (!string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CountryName.Code)))
											{
												result = false;
												logger.Warn(Messages.ECar_Common_InvalidReservationServiceLocationPickupDetail);
												throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocationPickupDetail);
											}
											else
											{
												result = false;
												logger.Warn(Messages.ECar_Common_UnknownError);
												throw new ValidationException(Messages.ECar_Common_UnknownError);
											}
										}
										else if ((reservation.Service.Location.Dropoff.Address == null) && (reservation.Service.Location.Dropoff.AirportInfo != null))
										{

											//airport
											if (((reservation.Service.Location.Dropoff.AirportInfo.Arrival != null) && (reservation.Service.Location.Dropoff.AirportInfo.Departure != null)) || ((reservation.Service.Location.Dropoff.AirportInfo.Arrival == null) && (reservation.Service.Location.Dropoff.AirportInfo.Departure == null)))
											{
												result = false;
												var vtodException = VtodException.CreateValidationException(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
												logger.Warn(vtodException.ExceptionMessage.Message);
												throw vtodException; // new ValidationException(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
											}
											else if (reservation.Service.Location.Dropoff.AirportInfo.Arrival != null && reservation.Service.Location.Dropoff.AirportInfo.Departure == null)
											{
												//Arrival
												ecbp.DropOffAddressType = AddressType.Airport;
												ecbp.DropoffAirport = reservation.Service.Location.Dropoff.AirportInfo.Arrival.LocationCode;
												logger.Info("Dropoff AddressType: AirportInfo.Arrival");
											}
											else if (reservation.Service.Location.Dropoff.AirportInfo.Arrival == null && reservation.Service.Location.Dropoff.AirportInfo.Departure != null)
											{
												//Depature
												ecbp.DropOffAddressType = AddressType.Airport;
												ecbp.DropoffAirport = reservation.Service.Location.Dropoff.AirportInfo.Departure.LocationCode;
												logger.Info("Dropoff AddressType: AirportInfo.Depature");
											}

											//longitude and latitude
											if (result)
											{
												double? longitude = null;
												double? latitude = null;
												if (GetLongitudeAndLatitudeForAirport(ecbp.DropoffAirport, out longitude, out latitude))
												{
													ecbp.LongitudeForDropoffAirport = longitude.Value;
													ecbp.LatitudeForDropoffAirport = latitude.Value;
												}
												//else
												//{
												//    throw new ValidationException(Messages.ECar_Common_UnableToFindAirportLongitudeAndLatitude);
												//}


												if (reservation.Service.Location.Dropoff.Airline != null)
												{
													if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Airline.FlightNumber))
													{
														ecbp.DropOffAirlineFlightNumber = reservation.Service.Location.Dropoff.Airline.FlightNumber;
													}

													if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Airline.Code))
													{
														ecbp.DropOffAirlineCode = reservation.Service.Location.Dropoff.Airline.Code;
													}

													if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Airline.CodeContext))
													{
														ecbp.DropOffAirlineCodeContext = reservation.Service.Location.Dropoff.Airline.CodeContext;
													}

													if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Airline.FlightDateTime))
													{
														ecbp.DropOffAirlineFlightDateTime = reservation.Service.Location.Dropoff.Airline.FlightDateTime;
													}

												}
											}
										}
									}


									//For Pick me up now, make sure a Lat and Lon value are included in the Pickup request info. Change the Error message thrown to a more specific one.
									if (ecbp.PickupNow)
									{
										if ((reservation.Service.Location.Dropoff.Address != null) && (!string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.Latitude) && (!string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.Longitude))))
										{
											ecbp.CorporateDropoffLatitude = Convert.ToDecimal(reservation.Service.Location.Dropoff.Address.Latitude);
											ecbp.CorporateDropoffLongitude = Convert.ToDecimal(reservation.Service.Location.Dropoff.Address.Longitude);
										}
										else if ((ecbp.LongitudeForDropoffAirport != null) && (ecbp.LatitudeForDropoffAirport != null) && ((ecbp.LongitudeForDropoffAirport != 0) && (ecbp.LatitudeForDropoffAirport != 0)))
										{
											ecbp.CorporateDropoffLatitude = (decimal)ecbp.LatitudeForDropoffAirport;
											ecbp.CorporateDropoffLongitude = (decimal)ecbp.LongitudeForDropoffAirport;
										}
									}


								}

							}
							else
							{
								result = false;
								logger.Warn(Messages.ECar_Common_InvalidReservationServiceLocation);
								throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocation);
							}
						}
						else
						{
							result = false;
							logger.Warn(Messages.ECar_Common_InvalidReservationService);
							throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
						}
					}
					else
					{
						result = false;
						logger.Warn(Messages.ECar_Common_UnableProcessByPreviousInvalidRequest);
						throw new ValidationException(Messages.ECar_Common_UnableProcessByPreviousInvalidRequest);
					}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidateDropOffAddress");
				swEachPart.Stop();
				logger.DebugFormat("Validate dropoff address Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Validate DropOffDateTime
				//if (result)
				//{
				//	DateTime dropOffDateTime;
				//	if (reservation.Service.Location.Dropoff != null)
				//	{
				//		if (DateTime.TryParse(reservation.Service.Location.Dropoff.DateTime, out dropOffDateTime))
				//		{
				//			tbp.DropOffDateTime = dropOffDateTime;
				//		}
				//		else
				//		{
				//			tbp.DropOffDateTime = null;
				//		}
				//	}
				//	else
				//	{
				//		tbp.DropOffDateTime = null;
				//	}
				//}
				//else
				//{
				//	logger.Info("No drop off datetime");
				//}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidateDropoffDateTime");
				swEachPart.Stop();
				logger.DebugFormat("Validate dropoff time Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Validate Available address type
				if (result)
				{
					string pickupAddressType = ecbp.PickupAddressType;
					string dropoffAddressType = ecbp.DropOffAddressType;

					//verify the address type of pickup address and drop off address
					UtilityDomain ud = new UtilityDomain();
					if (pickupAddressType == AddressType.Address.ToString())
					{
						UtilityGetAirportNameRQ uganRQ = new UtilityGetAirportNameRQ();
						uganRQ.Address = new Address();
						uganRQ.Address.Latitude = ecbp.PickupAddress.Geolocation.Latitude.ToString();
						uganRQ.Address.Longitude = ecbp.PickupAddress.Geolocation.Longitude.ToString();

						UtilityGetAirportNameRS uganRS = ud.GetAirportName(tokenVTOD, uganRQ);
						if (uganRS.Success != null)
						{
							if (!string.IsNullOrWhiteSpace(uganRS.AirportName))
							{
								pickupAddressType = AddressType.Airport;
							}
						}

					}

					if (dropoffAddressType == AddressType.Address.ToString())
					{
						UtilityGetAirportNameRQ uganRQ = new UtilityGetAirportNameRQ();
						uganRQ.Address = new Address();
						uganRQ.Address.Latitude = ecbp.DropOffAddress.Geolocation.Latitude.ToString();
						uganRQ.Address.Longitude = ecbp.DropOffAddress.Geolocation.Longitude.ToString();

						UtilityGetAirportNameRS uganRS = ud.GetAirportName(tokenVTOD, uganRQ);
						if (uganRS.Success != null)
						{
							if (!string.IsNullOrWhiteSpace(uganRS.AirportName))
							{
								dropoffAddressType = AddressType.Airport;
							}
						}
					}

					//check database setting
					//1. get ecar_fleet_availableaddresstype
					using (VTODEntities context = new VTODEntities())
					{
						long fleetId = ecbp.Fleet.Id;
						if (context.ecar_fleet_availableaddresstype.Where(x => fleetId == x.FleetId).Any())
						{
							ecar_fleet_availableaddresstype ecfa = context.ecar_fleet_availableaddresstype.Where(x => fleetId == x.FleetId).Select(x => x).FirstOrDefault();
							//2. check if the type is available or not
							//ECar_Common_InvalidAddressTypeForFleet
							if (AddressType.Address.ToString() == pickupAddressType && AddressType.Address.ToString() == dropoffAddressType)
							{
								//address to address
								if (ecfa.AddressToAddress)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.ECar, 1013);// (Messages.Validation_InvalidAddressType);
								}
							}
							else if (AddressType.Address.ToString() == pickupAddressType && AddressType.Airport.ToString() == dropoffAddressType)
							{
								//address to airport
								if (ecfa.AddressToAirport)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.ECar, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
							else if (AddressType.Address.ToString() == pickupAddressType && null == dropoffAddressType)
							{
								//address to null
								if (ecfa.AddressToNull)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.ECar, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
							else if (AddressType.Airport.ToString() == pickupAddressType && AddressType.Address.ToString() == dropoffAddressType)
							{
								//airport to address
								if (ecfa.AirportToAddress)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.ECar, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
							else if (AddressType.Airport.ToString() == pickupAddressType && AddressType.Airport.ToString() == dropoffAddressType)
							{
								//airport to airport
								if (ecfa.AirportToAirport)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.ECar, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
							else if (AddressType.Airport.ToString() == pickupAddressType && null == dropoffAddressType)
							{
								//airport to null
								if (ecfa.AirportToNull)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.ECar, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
						}
						else
						{
							result = false;
							throw VtodException.CreateException(ExceptionType.ECar, 1011); //ValidationException(Messages.ECar_Common_UnableToGetAvailableFleets);
						}
					}


				}
				else
				{
					result = false;
					var vtodException = Common.DTO.VtodException.CreateFieldRequiredValidationException("Payment");
					logger.Warn(vtodException.ExceptionMessage.Message);
					throw vtodException; // new ValidationException(Messages.Validation_NullPayment);
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidateAvailableAddressType");
				swEachPart.Stop();
				logger.DebugFormat("Validate available address type Duration:{0} ms.", swEachPart.ElapsedMilliseconds);


				swEachPart.Restart();
				#region Validate PaymentInfo

				#region Authorize
				//var membershipController = new UDI.SDS.MembershipController(tokenVTOD, TrackTime);
				var membershipDomain = new UDI.VTOD.Domain.Membership.MembershipDomain();
				ecbp.PaymentType = Common.DTO.Enum.PaymentType.CorporateBilling;
				//if (membershipDomain.IsUserInPaymentRole(tokenVTOD.Username, Common.DTO.Enum.PaymentType.ChargedByAleph.ToString()))
				//{
				//	ecbp.PaymentType = Common.DTO.Enum.PaymentType.ChargedByAleph;
				//}
				//else
				//{
				//	result = false;
				//	logger.Warn(Messages.Validation_InvalidPaymentType);
				//	throw VtodException.CreateValidationException(Messages.Validation_InvalidPaymentType); // ValidationException(Messages.General_InvalidPaymentType);
				//}
				#endregion

				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidatePaymentInfo");
				swEachPart.Stop();
				logger.DebugFormat("Validate payment Id Duration:{0} ms.", swEachPart.ElapsedMilliseconds);



				swEachPart.Restart();
				#region Validate Member Info
				//As of this version, all requester should pass memberID.
				//We need to modify this based on some others who do not pass memberID

				if (request.TPA_Extensions == null && string.IsNullOrWhiteSpace(request.TPA_Extensions.MemberID))
				{
					result = false;
					var vtodException = Common.DTO.VtodException.CreateFieldRequiredValidationException("MemberID");
					logger.Warn(vtodException.ExceptionMessage.Message);
					throw vtodException; // new ValidationException(Messages.Validation_NullPayment);
				}
				else
				{
					ecbp.MemberID = request.TPA_Extensions.MemberID.ToInt32();
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidateMemberInfo");
				swEachPart.Stop();
				logger.DebugFormat("Validate member Info Duration:{0} ms.", swEachPart.ElapsedMilliseconds);



				swEachPart.Restart();
				#region Get Gratuity
				try
				{
					var rateQuialifier = request.GroundReservations.First().RateQualifiers.First();
					if (rateQuialifier.SpecialInputs != null)
					{
						//rate
						var gratuities = rateQuialifier.SpecialInputs.Where(s => s.Name.ToLower() == "gratuity".ToLower()).ToList();

						if (gratuities != null && gratuities.Any())
						{
							decimal f;
							if (decimal.TryParse(gratuities.First().Value.Replace("%", "").ToString(), out f))
								ecbp.Gratuity = gratuities.First().Value;
							else
								throw new ValidationException("Invalid Gratuity");
						}
					}
				}
				catch (Exception ex)
				{
					logger.Error("ECar:Gratuity", ex);
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ GetGratuity");
				swEachPart.Stop();
				logger.DebugFormat("Get Gratuity Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Get Rate
				try
				{
					var rateQuialifier = request.GroundReservations.First().RateQualifiers.First();
					if (rateQuialifier.SpecialInputs != null)
					{
						//rate
						var rates = rateQuialifier.SpecialInputs.Where(s => s.Name.ToLower() == "rate".ToLower()).ToList();


						if (rates != null && rates.Any())
						{
							decimal f;
							if (decimal.TryParse(rates.First().Value, out f))
								ecbp.Rate = rates.First().Value;
							else
								throw new ValidationException("Invalid fare rate");
						}
					}
				}
				catch (Exception ex)
				{
					logger.Error("ECar:Rate", ex);
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ GetRate");
				swEachPart.Stop();
				logger.DebugFormat("Get Rate Duration:{0} ms.", swEachPart.ElapsedMilliseconds);


				swEachPart.Restart();
				#region Get Commission
				try
				{
					var rateQuialifier = request.GroundReservations.First().RateQualifiers.First();
					if (rateQuialifier.SpecialInputs != null)
					{
						//Commission
						var commissions = rateQuialifier.SpecialInputs.Where(s => s.Name.ToLower() == "commission".ToLower()).ToList();


						if (commissions != null && commissions.Any())
						{
							decimal f;
							if (decimal.TryParse(commissions.First().Value, out f))
								ecbp.commission = commissions.First().Value;
							else
								throw new ValidationException("Invalid commission");
						}
					}
				}
				catch (Exception ex)
				{
					logger.Error("ECar:Commission", ex);
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ GetCommission");
				swEachPart.Stop();
				logger.DebugFormat("Get Commission Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Get number of passenger
				try
				{
					if (request.TPA_Extensions.Passengers != null && request.TPA_Extensions.Passengers.Any())
					{
						ecbp.numberOfPassenger = request.TPA_Extensions.Passengers.Sum(x => x.Quantity);
					}
					else
					{
						ecbp.numberOfPassenger = 1;
					}
				}
				catch (Exception ex)
				{
					logger.Error("ECar: Number of Passenger", ex);
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ GetNumberOfPassenger");
				swEachPart.Stop();
				logger.DebugFormat("Get Number of Passenger Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Get controller notes
				try
				{
					var rateQuialifier = request.GroundReservations.First().RateQualifiers.First();
					if (rateQuialifier.SpecialInputs != null)
					{
						//rate
						var controllerNotes = rateQuialifier.SpecialInputs.Where(s => s.Name.ToLower() == "ControllerNotes".ToLower()).FirstOrDefault();

						if (controllerNotes != null && !string.IsNullOrWhiteSpace(controllerNotes.Value))
						{
							ecbp.controllerNotes = controllerNotes.Value;
						}
					}
				}
				catch (Exception ex)
				{
					logger.Error("ECar: Get controller notes", ex);
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ GeControllerNotes");
				swEachPart.Stop();
				logger.DebugFormat("Get controller notes Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Stops
				List<Map.DTO.Address> Stops = new List<Map.DTO.Address>();
				if ((reservation.Service.Location != null) && (reservation.Service.Location.Stops != null) && (reservation.Service.Location.Stops.Stops != null))
				{
					if (reservation.Service.Location.Stops.Stops.Any())
					{
						foreach (var item in reservation.Service.Location.Stops.Stops)
						{
							if (item.AirportInfo != null)
							{
								Map.DTO.Address stop = new Map.DTO.Address();
								if (item.Airline != null)
								{
									if (!string.IsNullOrWhiteSpace(item.Airline.FlightNumber))
									{
										stop.AirlineFlightNumber = item.Airline.FlightNumber;
									}

									if (!string.IsNullOrWhiteSpace(item.Airline.Code))
									{
										stop.Airline = item.Airline.Code;
									}

									if (!string.IsNullOrWhiteSpace(item.Airline.FlightDateTime))
									{
										stop.FlightDateTime = item.Airline.FlightDateTime;
									}

									if (!string.IsNullOrWhiteSpace(item.AirportInfo.Arrival.LocationCode))
									{
										stop.AirportCode = item.AirportInfo.Arrival.LocationCode;
									}
									Stops.Add(stop);
								}
							}
							if (item.Address != null)
							{

								Map.DTO.Address stop = new Map.DTO.Address();
								stop = ConvertAddress(item);
								stop.address_type = UDI.VTOD.Domain.ECar.Const.AddressType.Address;
								//string AddressStr = GetAddressString(item.Address);
								//stop.StreetNo = item.Address.StreetNmbr;
								//stop.Street = item.Address.AddressLine;
								//stop.FullAddress = item.Address.AddressLine;
								//stop.City = item.Address.CityName;
								//stop.State = item.Address.StateProv.StateCode;
								//stop.Country = item.Address.CountryName.Code;
								//stop.ZipCode = item.Address.PostalCode;
								//stop.Latitude = item.Address.Latitude;
								//stop.Longitude = item.Address.Longitude;
								Stops.Add(stop);

							}
						}
					}

					if (result)
					{
						ecbp.Stops = Stops;
					}
				}
				#endregion

				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ GetStops");
				swEachPart.Stop();
				logger.DebugFormat("Get controller notes Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				#region PromisedETA

				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ GetPromisedETA");
				swEachPart.Restart();
				if (request.TPA_Extensions.PickMeUpNow.HasValue && request.TPA_Extensions.PickMeUpNow == true)
				{
					var rateQuialifier = request.GroundReservations.First().RateQualifiers.First();
					if (rateQuialifier.SpecialInputs != null)
					{
						var promisedetas = rateQuialifier.SpecialInputs.Where(s => s.Name.ToLower() == "promisedeta").ToList();

						if (promisedetas != null && promisedetas.Any())
						{
							ecbp.PromisedETA = promisedetas.First().Value.ToInt32();
						}
					}
				}
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ GetPromisedETA");
				swEachPart.Stop();
				#endregion

				swEachPart.Restart();
				#region Create this trip into database
				if (result)
				{
					vtod_trip trip = CreateNewTrip(ecbp, tokenVTOD.Username);
					ecbp.Trip = trip;
				}
				else
				{
					result = false;
					var vtodException = VtodException.CreateException(ExceptionType.ECar, 2001);
					logger.Warn(vtodException.ExceptionMessage.Message);
					throw vtodException;
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ GreateTripInDatabase");
				swEachPart.Stop();
				logger.DebugFormat("CreateNewTrip in db  Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

			}
			else
			{
				result = false;
				var vtodException = VtodException.CreateFieldRequiredValidationException("GroundReservation");
				logger.Warn(vtodException.ExceptionMessage.Message);
				throw vtodException;
			}
			#endregion

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);


			#region Create ECar log
			if (result)
			{
				if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
				{
					WriteECarLog(ecbp.Trip.Id, ecbp.serviceAPIID, ECarServiceMethodType.Book, sw.ElapsedMilliseconds, string.Empty, string.Empty, null, null);
				}
			}
			else
			{
				result = false;
				logger.Warn(Messages.ECar_Common_UnableProcessByPreviousInvalidRequest);
				throw new ValidationException(Messages.ECar_Common_UnableProcessByPreviousInvalidRequest);
			}
			#endregion

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest");
			#endregion


			return result;

		}


		public bool ValidateStatusRequest(TokenRS tokenVTOD, OTA_GroundResRetrieveRQ request, ref AlephStatusParameter ecsp)
		{
			bool result = true;
			Stopwatch sw = new Stopwatch();
			sw.Start();

			var vtodDomain = new VTOD.VTODDomain(); vtodDomain.TrackTime = TrackTime;

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
			#endregion

			if (request.Reference != null)
			{
				#region Set token, request
				ecsp.tokenVTOD = tokenVTOD;
				ecsp.request = request;
				#endregion

				#region Validate Reference Id
				if (request.Reference == null || !request.Reference.Any() || string.IsNullOrWhiteSpace(request.Reference.First().ID))
				{
					result = false;
					logger.Warn("Reference.ID is empty.");
				}
				else
				{
					logger.Info("Request is valid.");
				}
				#endregion

				#region Validate Trip
				if (result)
				{

					vtod_trip t = GetECarTrip(request.Reference.First().ID);
					if (t != null)
					{
						ecsp.Trip = t;
						logger.InfoFormat("Trip found. tripID={0}", request.Reference.First().ID);
					}
					else
					{
						result = false;
						logger.WarnFormat("Cannot find this trip. tripID={0}", request.Reference.First().ID);
					}
				}
				else
				{
					result = false;
					logger.Warn("Cannot find trip by invalid referenceId");
				}
				#endregion

				#region Validate Fleet
				if (result)
				{
					ecar_fleet fleet = null;
					if (GetFleet(ecsp.Trip.ecar_trip.FleetId, out fleet))
					{
						ecsp.Fleet = fleet;
						logger.InfoFormat("Fleet found. ID={0}", fleet.Id);

						if (fleet != null)
						{
							ecsp.serviceAPIID = GetECarFleetServiceAPIPreference(fleet.Id, ECarServiceAPIPreferenceConst.Status).ServiceAPIId;
						}
					}
					else
					{
						result = false;
						logger.Warn("Unable to find fleet by this fleetID");
					}
				}
				else
				{
					result = false;
					logger.Warn("Unable to find fleet by this fleetID");
				}
				#endregion

				#region Validate Fleet User
				if (result)
				{
					ecar_fleet_user fleet_user = null;
					if (GetECarFleetUser(ecsp.Trip.ecar_trip.FleetId, tokenVTOD.Username, out fleet_user))
					{
						ecsp.Fleet_User = fleet_user;
						logger.Info("This user has sufficient privilege.");

						ecsp.User = GetUser(fleet_user.UserId);
					}
					else
					{
						result = false;
						logger.WarnFormat("Insufficient privilege. User={0}, FleetId={1}", tokenVTOD.Username, ecsp.Fleet.Id);
					}
				}
				else
				{
					result = false;
					logger.Warn("Cannot validate this user by invalid fleet ");
				}
				#endregion

				#region Validate Aleph Inputs

				if (result)
				{
					try
					{
						var ws_state = vtodDomain.Get_WS_State("vtod_trip", ecsp.Trip.Id, "CorporateIDs");
						List<NameValue> specialInputs = null;

						if (!string.IsNullOrWhiteSpace(ws_state))
						{
							specialInputs = ws_state.JsonDeserialize<List<NameValue>>();
						}

						//var specialInputs = ecsp.Trip.WebServiceState.JsonDeserialize<List<NameValue>>();
						//var rateQualifier = request.TPA_Extensions.RateQualifiers.First();
						//if (rateQualifier.SpecialInputs != null)
						if (specialInputs != null)
						{
							//CorporateEmail 
							//var CorporateEmail = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateEmail".ToLowerInvariant()).FirstOrDefault();
							var CorporateEmail = specialInputs.Where(s => s.Name.ToLower() == "CorporateEmail".ToLowerInvariant()).FirstOrDefault();
							if (CorporateEmail != null)
							{
								ecsp.CorporateEmail = CorporateEmail.Value;
							}
							else
							{
								ecsp.CorporateEmail = null;
								result = false;
								logger.Warn(Messages.ECar_Common_InvalidReservationService);
								throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
							}


							//CorporateProviderID
							//var CorporateProviderID = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateProviderID".ToLowerInvariant()).FirstOrDefault();
							var CorporateProviderID = specialInputs.Where(s => s.Name.ToLower() == "CorporateProviderID".ToLowerInvariant()).FirstOrDefault();
							if (CorporateProviderID != null)
							{
								ecsp.CorporateProviderID = CorporateProviderID.Value;
							}
							else
							{
								if (!ecsp.Trip.PickMeUpNow.HasValue || !ecsp.Trip.PickMeUpNow.Value)
								{
									ecsp.CorporateProviderID = null;
									result = false;
									logger.Warn(Messages.ECar_Common_InvalidReservationService);
									throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
								}
							}


							//CorporateCompanyName
							//var CorporateCompanyName = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateCompanyName".ToLowerInvariant()).FirstOrDefault();
							var CorporateCompanyName = specialInputs.Where(s => s.Name.ToLower() == "CorporateCompanyName".ToLowerInvariant()).FirstOrDefault();
							if (CorporateCompanyName != null)
							{
								ecsp.CorporateCompanyName = CorporateCompanyName.Value;
							}
							else
							{
								if (!ecsp.Trip.PickMeUpNow.HasValue || !ecsp.Trip.PickMeUpNow.Value)
								{
									ecsp.CorporateCompanyName = null;
									result = false;
									logger.Warn(Messages.ECar_Common_InvalidReservationService);
									throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
								}
							}


							//CorporateProviderAccountID
							//var CorporateProviderAccountID = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateProviderAccountID".ToLowerInvariant()).FirstOrDefault();
							var CorporateProviderAccountID = specialInputs.Where(s => s.Name.ToLower() == "CorporateProviderAccountID".ToLowerInvariant()).FirstOrDefault();
							if (CorporateProviderAccountID != null)
							{
								ecsp.CorporateProviderAccountID = CorporateProviderAccountID.Value;
							}
							else
							{
								if (!ecsp.Trip.PickMeUpNow.HasValue || !ecsp.Trip.PickMeUpNow.Value)
								{
									ecsp.CorporateProviderAccountID = null;
									result = false;
									logger.Warn(Messages.ECar_Common_InvalidReservationService);
									throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
								}
							}


							//CorporateProfileID
							//var CorporateProfileID = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateProfileID".ToLowerInvariant()).FirstOrDefault();
							var CorporateProfileID = specialInputs.Where(s => s.Name.ToLower() == "CorporateProfileID".ToLowerInvariant()).FirstOrDefault();
							if (CorporateProfileID != null)
							{
								ecsp.CorporateProfileID = CorporateProfileID.Value;
							}
							else
							{
								if (!ecsp.Trip.PickMeUpNow.HasValue || !ecsp.Trip.PickMeUpNow.Value)
								{
									ecsp.CorporateProfileID = null;
									result = false;
									logger.Warn(Messages.ECar_Common_InvalidReservationService);
									throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
								}
							}

						}
						else
						{
							throw VtodException.CreateException(ExceptionType.ECar, 20022);
						}
					}
					catch (Exception ex)
					{
						logger.Error("Aleph: Error While Validating Aleph Profile Info", ex);
						result = false;
						logger.Warn("Error while validating Aleph profile inputs.");
						throw VtodException.CreateException(ExceptionType.ECar, 20023);
					}
				}
				else
				{
					result = false;
					logger.Warn(Messages.ECar_Common_InvalidReservationService);
					throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
				}
				#endregion

				#region GetAllTripStatusHistory
				if (result)
				{
					using (VTODEntities context = new VTODEntities())
					{
						long tripId = ecsp.Trip.Id;
						ecsp.lastTripStatus = context.ecar_trip_status.Where(x => x.TripID == tripId).Select(x => x).OrderByDescending(x => x.Id).FirstOrDefault();
					}
				}
				#endregion
			}
			else
			{
				result = false;
				logger.Info("Reference is null.");
			}

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Create ecar log
			if (result)
			{
				if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
				{
					WriteECarLog(ecsp.Trip.Id, ecsp.serviceAPIID, ECarServiceMethodType.Status, sw.ElapsedMilliseconds, string.Empty, string.Empty, null, null);
				}
			}
			else
			{
				result = false;
				logger.Warn("Cannot create or get this ecar log by invalid trip");
			}
			#endregion

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateStatusRequest");
			#endregion

			return result;
		}

		public bool ValidateStatusDispatchConfirmationRequest(TokenRS tokenVTOD, OTA_GroundResRetrieveRQ request, ref AlephStatusParameter ecsp)
		{
			bool result = true;
			Stopwatch sw = new Stopwatch();
			sw.Start();

			var vtodDomain = new VTOD.VTODDomain(); vtodDomain.TrackTime = TrackTime;

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
			#endregion

			if (request.Reference != null)
			{
				#region Set token, request
				ecsp.tokenVTOD = tokenVTOD;
				ecsp.request = request;
				#endregion

				#region Validate Reference Id
				if (request.Reference == null || !request.Reference.Any() || string.IsNullOrWhiteSpace(request.Reference.First().ID))
				{
					result = false;
					logger.Warn("Reference.ID is empty.");
				}
				else
				{
					logger.Info("Request is valid.");
				}
                #endregion

                #region Validate Trip
                if (request.Reference.FirstOrDefault().Type == "dispatchconfirmation")
                {
                    if ((request.Reference.FirstOrDefault().ID.Contains("##")))
                    {

                        ecsp.Trip = GetVtodTripWithReferenceID(request.Reference.FirstOrDefault().ID, false);
                    }
                    
                }

                
                //if (result)
                //{

                //    vtod_trip t = GetECarTrip(request.Reference.First().ID);
                //    if (t != null)
                //    {
                //        ecsp.Trip = t;
                //        logger.InfoFormat("Trip found. tripID={0}", request.Reference.First().ID);
                //    }
                //    else
                //    {
                //        result = false;
                //        logger.WarnFormat("Cannot find this trip. tripID={0}", request.Reference.First().ID);
                //    }
                //}
                //else
                //{
                //    result = false;
                //    logger.Warn("Cannot find trip by invalid referenceId");
                //}
                #endregion

                #region Validate Fleet
                //if (result)
                //{
                //    ecar_fleet fleet = null;
                //    if (GetFleet(ecsp.Trip.ecar_trip.FleetId, out fleet))
                //    {
                //        ecsp.Fleet = fleet;
                //        logger.InfoFormat("Fleet found. ID={0}", fleet.Id);

                //        if (fleet != null)
                //        {
                //            ecsp.serviceAPIID = GetECarFleetServiceAPIPreference(fleet.Id, ECarServiceAPIPreferenceConst.Status).ServiceAPIId;
                //        }
                //    }
                //    else
                //    {
                //        result = false;
                //        logger.Warn("Unable to find fleet by this fleetID");
                //    }
                //}
                //else
                //{
                //    result = false;
                //    logger.Warn("Unable to find fleet by this fleetID");
                //}
                #endregion

                #region Validate Fleet User
                //if (result)
                //{
                //    ecar_fleet_user fleet_user = null;
                //    if (GetECarFleetUser(ecsp.Trip.ecar_trip.FleetId, tokenVTOD.Username, out fleet_user))
                //    {
                //        ecsp.Fleet_User = fleet_user;
                //        logger.Info("This user has sufficient privilege.");

                //        ecsp.User = GetUser(fleet_user.UserId);
                //    }
                //    else
                //    {
                //        result = false;
                //        logger.WarnFormat("Insufficient privilege. User={0}, FleetId={1}", tokenVTOD.Username, ecsp.Fleet.Id);
                //    }
                //}
                //else
                //{
                //    result = false;
                //    logger.Warn("Cannot validate this user by invalid fleet ");
                //}
                #endregion

                #region Get Email
                try
				{
					var CorporateEmail = request.TPA_Extensions.RateQualifiers.FirstOrDefault().SpecialInputs.Where(s => s.Name.ToLower().Trim() == "corporateemail").FirstOrDefault().Value;  //specialInputs.Where(s => s.Name.ToLower() == "CorporateEmail".ToLowerInvariant()).FirstOrDefault();
					if (!string.IsNullOrEmpty(CorporateEmail))
					{
						ecsp.CorporateEmail = CorporateEmail;
					}
					else
					{
						ecsp.CorporateEmail = null;
						result = false;
						logger.Warn(Messages.ECar_Common_InvalidReservationService);
						throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
					}

				}
				catch
				{
					ecsp.CorporateEmail = null;
					result = false;
					logger.Warn(Messages.ECar_Common_InvalidReservationService);
					throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
				}

				#endregion
				#region Validate Aleph Inputs

				if (result)
				{
					try
					{
						var specialInputRequest = request.TPA_Extensions.WebServiceState;
						List<NameValue> specialInputs = null;

						if (!string.IsNullOrWhiteSpace(specialInputRequest))
						{
							specialInputs = specialInputRequest.JsonDeserialize<List<NameValue>>();
						}

						//var specialInputs = ecsp.Trip.WebServiceState.JsonDeserialize<List<NameValue>>();
						//var rateQualifier = request.TPA_Extensions.RateQualifiers.First();
						//if (rateQualifier.SpecialInputs != null)
						if (specialInputs != null && specialInputs.Any())
						{
							var CorporateProviderID = specialInputs.Where(s => s.Name.ToLower() == "CorporateProviderID".ToLowerInvariant()).FirstOrDefault();
							if (CorporateProviderID != null)
							{
								ecsp.CorporateProviderID = CorporateProviderID.Value;
							}
							else
							{
								if (!ecsp.Trip.PickMeUpNow.HasValue || !ecsp.Trip.PickMeUpNow.Value)
								{
									ecsp.CorporateProviderID = null;
									result = false;
									logger.Warn(Messages.ECar_Common_InvalidReservationService);
									throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
								}
							}


							//CorporateCompanyName
							//var CorporateCompanyName = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateCompanyName".ToLowerInvariant()).FirstOrDefault();
							var CorporateCompanyName = specialInputs.Where(s => s.Name.ToLower() == "CorporateCompanyName".ToLowerInvariant()).FirstOrDefault();
							if (CorporateCompanyName != null)
							{
								ecsp.CorporateCompanyName = CorporateCompanyName.Value;
							}
							else
							{
								if (!ecsp.Trip.PickMeUpNow.HasValue || !ecsp.Trip.PickMeUpNow.Value)
								{
									ecsp.CorporateCompanyName = null;
									result = false;
									logger.Warn(Messages.ECar_Common_InvalidReservationService);
									throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
								}
							}


							//CorporateProviderAccountID
							//var CorporateProviderAccountID = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateProviderAccountID".ToLowerInvariant()).FirstOrDefault();
							var CorporateProviderAccountID = specialInputs.Where(s => s.Name.ToLower() == "CorporateProviderAccountID".ToLowerInvariant()).FirstOrDefault();
							if (CorporateProviderAccountID != null)
							{
								ecsp.CorporateProviderAccountID = CorporateProviderAccountID.Value;
							}
							else
							{
								if (!ecsp.Trip.PickMeUpNow.HasValue || !ecsp.Trip.PickMeUpNow.Value)
								{
									ecsp.CorporateProviderAccountID = null;
									result = false;
									logger.Warn(Messages.ECar_Common_InvalidReservationService);
									throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
								}
							}


							//CorporateProfileID
							//var CorporateProfileID = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateProfileID".ToLowerInvariant()).FirstOrDefault();
							var CorporateProfileID = specialInputs.Where(s => s.Name.ToLower() == "CorporateProfileID".ToLowerInvariant()).FirstOrDefault();
							if (CorporateProfileID != null)
							{
								ecsp.CorporateProfileID = CorporateProfileID.Value;
							}
							else
							{
								if (!ecsp.Trip.PickMeUpNow.HasValue || !ecsp.Trip.PickMeUpNow.Value)
								{
									ecsp.CorporateProfileID = null;
									result = false;
									logger.Warn(Messages.ECar_Common_InvalidReservationService);
									throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
								}
							}

							//Dispatch Code
							var DispatchCode = specialInputs.Where(s => s.Name.ToLower() == "DispatchCode".ToLowerInvariant()).FirstOrDefault();
							if (DispatchCode != null)
							{
								ecsp.DispatchCode = DispatchCode.Value;
							}
							else
							{
								if (!ecsp.Trip.PickMeUpNow.HasValue || !ecsp.Trip.PickMeUpNow.Value)
								{
									ecsp.DispatchCode = null;
									result = false;
									logger.Warn(Messages.ECar_Common_InvalidReservationService);
									throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
								}
							}

							//Gratuity
							var Gratuity = specialInputs.Where(s => s.Name.ToLower() == "Gratuity".ToLowerInvariant()).FirstOrDefault();
							if (Gratuity != null)
							{
								ecsp.Gratuity = Gratuity.Value;
							}
							else
							{
								if (!ecsp.Trip.PickMeUpNow.HasValue || !ecsp.Trip.PickMeUpNow.Value)
								{
									ecsp.Gratuity = null;
									result = false;
									logger.Warn(Messages.ECar_Common_InvalidReservationService);
									throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
								}
							}

							//CorporateSpecialInstructions
							var CorporateSpecialInstructions = specialInputs.Where(s => s.Name.ToLower() == "CorporateSpecialInstructions".ToLowerInvariant()).FirstOrDefault();
							if (CorporateSpecialInstructions != null)
							{
								ecsp.CorporateSpecialInstructions = CorporateSpecialInstructions.Value;
							}
							else
							{
								if (!ecsp.Trip.PickMeUpNow.HasValue || !ecsp.Trip.PickMeUpNow.Value)
								{
									ecsp.CorporateSpecialInstructions = null;
									result = false;
									logger.Warn(Messages.ECar_Common_InvalidReservationService);
									throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
								}
							}

							//Corporate Pyament 
							var CorporatePaymentMethod = specialInputs.Where(s => s.Name.ToLower() == "CorporatePaymentMethod".ToLowerInvariant()).FirstOrDefault();
							if (CorporatePaymentMethod != null)
							{
								ecsp.CorporatePaymentMethod = CorporatePaymentMethod.Value;
							}
							else
							{
								if (!ecsp.Trip.PickMeUpNow.HasValue || !ecsp.Trip.PickMeUpNow.Value)
								{
									ecsp.CorporatePaymentMethod = null;
									result = false;
									logger.Warn(Messages.ECar_Common_InvalidReservationService);
									throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
								}
							}
						}
						else
						{
							throw VtodException.CreateException(ExceptionType.ECar, 20022);
						}
					}
					catch (Exception ex)
					{
						logger.Error("Aleph: Error While Validating Aleph Profile Info", ex);
						result = false;
						logger.Warn("Error while validating Aleph profile inputs.");
						throw VtodException.CreateException(ExceptionType.ECar, 20023);
					}
				}
				else
				{
					result = false;
					logger.Warn(Messages.ECar_Common_InvalidReservationService);
					throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
				}
				#endregion

				#region GetAllTripStatusHistory
				//if (result)
				//{
				//    using (VTODEntities context = new VTODEntities())
				//    {
				//        long tripId = ecsp.Trip.Id;
				//        ecsp.lastTripStatus = context.ecar_trip_status.Where(x => x.TripID == tripId).Select(x => x).OrderByDescending(x => x.Id).FirstOrDefault();
				//    }
				//}
				#endregion
			}
			else
			{
				result = false;
				logger.Info("Reference is null.");
			}

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Create ecar log
			if (result)
			{
				if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
				{
					WriteECarLog(ecsp.Trip.Id, ecsp.serviceAPIID, ECarServiceMethodType.Status, sw.ElapsedMilliseconds, string.Empty, string.Empty, null, null);
				}
			}
			else
			{
				result = false;
				logger.Warn("Cannot create or get this ecar log by invalid trip");
			}
			#endregion

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateStatusRequest");
			#endregion

			return result;
		}


		public bool ValidateCancelRequest(TokenRS tokenVTOD, OTA_GroundCancelRQ request, ref AlephCancelParameter eccp)
		{
			bool result = true;
			Stopwatch sw = new Stopwatch();
			sw.Start();
			var vtodDomain = new VTOD.VTODDomain(); vtodDomain.TrackTime = TrackTime;

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
			#endregion

			#region Set token, request
			eccp.tokenVTOD = tokenVTOD;
			eccp.request = request;
			#endregion

			#region Validate Unique ID
			string uid = request.Reservation.UniqueID.FirstOrDefault().ID;
			if (request.Reservation.UniqueID.Any())
			{
				if (!string.IsNullOrWhiteSpace(uid))
				{
					logger.Info("Request is valid.");
				}
				else
				{
					result = false;
					logger.Info("request.Reservation.UniqueID.Frist().ID is empty.");
				}
			}
			else
			{
				result = false;
				logger.Info("Reservation.UniqueID is empty.");
			}
			#endregion

			#region Validate Trip
			if (result)
			{
				vtod_trip t = GetECarTrip(uid);
				if (t != null)
				{
					eccp.Trip = t;
					logger.InfoFormat("Trip found. tripID={0}", uid);
				}
				else
				{
					result = false;
					logger.WarnFormat("Cannot find this trip. tripID={0}", uid);
				}
			}
			else
			{
				result = false;
				logger.Warn("Cannot find trip by invalid referenceId");
			}
			#endregion

			#region Validate Fleet
			if (result)
			{
				ecar_fleet fleet = null;
				if (GetFleet(eccp.Trip.ecar_trip.FleetId, out fleet))
				{
					eccp.Fleet = fleet;
					logger.InfoFormat("Fleet found. ID={0}", fleet.Id);

					if (fleet != null)
					{
						eccp.serviceAPIID = GetECarFleetServiceAPIPreference(fleet.Id, ECarServiceAPIPreferenceConst.Cancel).ServiceAPIId;
					}
				}
				else
				{
					result = false;
					logger.Warn("Unable to find fleet by this fleetID");
				}
			}
			else
			{
				result = false;
				logger.Warn("Unable to find fleet by this fleetID");
			}
			#endregion

			#region Validate Fleet User
			if (result)
			{
				ecar_fleet_user user = null;
				if (GetECarFleetUser(eccp.Trip.ecar_trip.FleetId, tokenVTOD.Username, out user))
				{
					eccp.User = user;
					logger.Info("This user has sufficient privilege.");
				}
				else
				{
					result = false;
					logger.WarnFormat("Insufficient privilege. User={0}, FleetId={1}", tokenVTOD.Username, eccp.Fleet.Id);
				}
			}
			else
			{
				result = false;
				logger.Warn("Cannot validate this user by invalid fleet ");
			}
			#endregion

			#region Validate Aleph Inputs

			if (result)
			{
				try
				{
					//var rateQualifier = request.TPA_Extensions.RateQualifiers.First();
					var ws_state = vtodDomain.Get_WS_State("vtod_trip", eccp.Trip.Id, "CorporateIDs");
					List<NameValue> specialInputs = null;

					if (!string.IsNullOrWhiteSpace(ws_state))
					{
						specialInputs = ws_state.JsonDeserialize<List<NameValue>>();
					}

					//var specialInputs = eccp.Trip.WebServiceState.JsonDeserialize<List<NameValue>>();
					//if (rateQualifier.SpecialInputs != null)
					if (specialInputs != null)
					{
						//CorporateEmail 
						//var CorporateEmail = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateEmail".ToLowerInvariant()).FirstOrDefault();
						var CorporateEmail = specialInputs.Where(s => s.Name.ToLower() == "CorporateEmail".ToLowerInvariant()).FirstOrDefault();
						if (CorporateEmail != null)
						{
							eccp.CorporateEmail = CorporateEmail.Value;
						}
						else
						{
							eccp.CorporateEmail = null;
							result = false;
							logger.Warn(Messages.ECar_Common_InvalidReservationService);
							throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
						}



						//CorporateProviderID
						//var CorporateProviderID = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateProviderID".ToLowerInvariant()).FirstOrDefault();
						var CorporateProviderID = specialInputs.Where(s => s.Name.ToLower() == "CorporateProviderID".ToLowerInvariant()).FirstOrDefault();
						if (CorporateProviderID != null)
						{
							eccp.CorporateProviderID = CorporateProviderID.Value;
						}
						else
						{
							if (!eccp.Trip.PickMeUpNow.HasValue || !eccp.Trip.PickMeUpNow.Value)
							{
								eccp.CorporateProviderID = null;
								result = false;
								logger.Warn(Messages.ECar_Common_InvalidReservationService);
								throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
							}
						}


						//CorporateCompanyName
						//var CorporateCompanyName = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateCompanyName".ToLowerInvariant()).FirstOrDefault();
						var CorporateCompanyName = specialInputs.Where(s => s.Name.ToLower() == "CorporateCompanyName".ToLowerInvariant()).FirstOrDefault();
						if (CorporateCompanyName != null)
						{
							eccp.CorporateCompanyName = CorporateCompanyName.Value;
						}
						else
						{
							if (!eccp.Trip.PickMeUpNow.HasValue || !eccp.Trip.PickMeUpNow.Value)
							{
								eccp.CorporateCompanyName = null;
								result = false;
								logger.Warn(Messages.ECar_Common_InvalidReservationService);
								throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
							}
						}


						//CorporateProviderAccountID
						//var CorporateProviderAccountID = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateProviderAccountID".ToLowerInvariant()).FirstOrDefault();
						var CorporateProviderAccountID = specialInputs.Where(s => s.Name.ToLower() == "CorporateProviderAccountID".ToLowerInvariant()).FirstOrDefault();
						if (CorporateProviderAccountID != null)
						{
							eccp.CorporateProviderAccountID = CorporateProviderAccountID.Value;
						}
						else
						{
							if (!eccp.Trip.PickMeUpNow.HasValue || !eccp.Trip.PickMeUpNow.Value)
							{
								eccp.CorporateProviderAccountID = null;
								result = false;
								logger.Warn(Messages.ECar_Common_InvalidReservationService);
								throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
							}
						}


						//CorporateProfileID
						//var CorporateProfileID = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateProfileID".ToLowerInvariant()).FirstOrDefault();
						var CorporateProfileID = specialInputs.Where(s => s.Name.ToLower() == "CorporateProfileID".ToLowerInvariant()).FirstOrDefault();
						if (CorporateProfileID != null)
						{
							eccp.CorporateProfileID = CorporateProfileID.Value;
						}
						else
						{
							if (!eccp.Trip.PickMeUpNow.HasValue || !eccp.Trip.PickMeUpNow.Value)
							{
								eccp.CorporateProfileID = null;
								result = false;
								logger.Warn(Messages.ECar_Common_InvalidReservationService);
								throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
							}
						}



					}
					else
					{
						throw VtodException.CreateException(ExceptionType.ECar, 20022);
					}
				}
				catch (Exception ex)
				{
					logger.Error("Aleph: Error While Validating Aleph Profile Info", ex);
					result = false;
					logger.Warn("Error while validating Aleph profile inputs.");
					throw VtodException.CreateException(ExceptionType.ECar, 20023);
				}
			}
			else
			{
				result = false;
				logger.Warn(Messages.ECar_Common_InvalidReservationService);
				throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
			}
			#endregion


			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Create ecar log
			if (result)
			{
				if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
				{
					WriteECarLog(eccp.Trip.Id, eccp.serviceAPIID, ECarServiceMethodType.Cancel, sw.ElapsedMilliseconds, string.Empty, string.Empty, null, null);
				}
			}
			else
			{
				result = false;
				logger.Warn("Cannot create or get this ecar log by invalid trip");
			}
			#endregion

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateCancelRequest");
			#endregion

			return result;
		}

		public bool ValidateFareDetailsInput(TokenRS tokenVTOD, GetFareDetailRQ request, ref AlephStatusParameter ecsp)
		{
			bool result = true;
			Stopwatch sw = new Stopwatch();
			sw.Start();
			var vtodDomain = new VTOD.VTODDomain(); vtodDomain.TrackTime = TrackTime;
			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
			#endregion

			#region Get Email
			try
			{
				var CorporateEmail = request.TPA_Extensions.RateQualifiers.FirstOrDefault().SpecialInputs.Where(s => s.Name.ToLower().Trim() == "corporateemail").FirstOrDefault().Value;  //specialInputs.Where(s => s.Name.ToLower() == "CorporateEmail".ToLowerInvariant()).FirstOrDefault();
				if (!string.IsNullOrEmpty(CorporateEmail))
				{
					ecsp.CorporateEmail = CorporateEmail;
				}
				else
				{
					ecsp.CorporateEmail = null;
					result = false;
					logger.Warn(Messages.ECar_Common_InvalidReservationService);
					throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
				}

			}
			catch
			{
				ecsp.CorporateEmail = null;
				result = false;
				logger.Warn(Messages.ECar_Common_InvalidReservationService);
				throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
			}

			#endregion
			#region Validate Aleph Inputs

			if (result)
			{
				try
				{
					var specialInputRequest = request.TPA_Extensions.WebServiceState;
					List<NameValue> specialInputs = null;

					if (!string.IsNullOrWhiteSpace(specialInputRequest))
					{
						specialInputs = specialInputRequest.JsonDeserialize<List<NameValue>>();
					}

					//var specialInputs = ecsp.Trip.WebServiceState.JsonDeserialize<List<NameValue>>();
					//var rateQualifier = request.TPA_Extensions.RateQualifiers.First();
					//if (rateQualifier.SpecialInputs != null)
					if (specialInputs != null && specialInputs.Any())
					{
						var CorporateProviderID = specialInputs.Where(s => s.Name.ToLower() == "CorporateProviderID".ToLowerInvariant()).FirstOrDefault();
						if (CorporateProviderID != null)
						{
							ecsp.CorporateProviderID = CorporateProviderID.Value;
						}
						else
						{
							if (!ecsp.Trip.PickMeUpNow.HasValue || !ecsp.Trip.PickMeUpNow.Value)
							{
								ecsp.CorporateProviderID = null;
								result = false;
								logger.Warn(Messages.ECar_Common_InvalidReservationService);
								throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
							}
						}


						//CorporateCompanyName
						//var CorporateCompanyName = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateCompanyName".ToLowerInvariant()).FirstOrDefault();
						var CorporateCompanyName = specialInputs.Where(s => s.Name.ToLower() == "CorporateCompanyName".ToLowerInvariant()).FirstOrDefault();
						if (CorporateCompanyName != null)
						{
							ecsp.CorporateCompanyName = CorporateCompanyName.Value;
						}
						else
						{
							if (!ecsp.Trip.PickMeUpNow.HasValue || !ecsp.Trip.PickMeUpNow.Value)
							{
								ecsp.CorporateCompanyName = null;
								result = false;
								logger.Warn(Messages.ECar_Common_InvalidReservationService);
								throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
							}
						}


						//CorporateProviderAccountID
						//var CorporateProviderAccountID = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateProviderAccountID".ToLowerInvariant()).FirstOrDefault();
						var CorporateProviderAccountID = specialInputs.Where(s => s.Name.ToLower() == "CorporateProviderAccountID".ToLowerInvariant()).FirstOrDefault();
						if (CorporateProviderAccountID != null)
						{
							ecsp.CorporateProviderAccountID = CorporateProviderAccountID.Value;
						}
						else
						{
							if (!ecsp.Trip.PickMeUpNow.HasValue || !ecsp.Trip.PickMeUpNow.Value)
							{
								ecsp.CorporateProviderAccountID = null;
								result = false;
								logger.Warn(Messages.ECar_Common_InvalidReservationService);
								throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
							}
						}


						//CorporateProfileID
						//var CorporateProfileID = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateProfileID".ToLowerInvariant()).FirstOrDefault();
						var CorporateProfileID = specialInputs.Where(s => s.Name.ToLower() == "CorporateProfileID".ToLowerInvariant()).FirstOrDefault();
						if (CorporateProfileID != null)
						{
							ecsp.CorporateProfileID = CorporateProfileID.Value;
						}
						else
						{
							if (!ecsp.Trip.PickMeUpNow.HasValue || !ecsp.Trip.PickMeUpNow.Value)
							{
								ecsp.CorporateProfileID = null;
								result = false;
								logger.Warn(Messages.ECar_Common_InvalidReservationService);
								throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
							}
						}

						//Dispatch Code
						var DispatchCode = specialInputs.Where(s => s.Name.ToLower() == "DispatchCode".ToLowerInvariant()).FirstOrDefault();
						if (DispatchCode != null)
						{
							ecsp.DispatchCode = DispatchCode.Value;
						}
						else
						{
							if (!ecsp.Trip.PickMeUpNow.HasValue || !ecsp.Trip.PickMeUpNow.Value)
							{
								ecsp.DispatchCode = null;
								result = false;
								logger.Warn(Messages.ECar_Common_InvalidReservationService);
								throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
							}
						}

						//Gratuity
						var Gratuity = specialInputs.Where(s => s.Name.ToLower() == "Gratuity".ToLowerInvariant()).FirstOrDefault();
						if (Gratuity != null)
						{
							ecsp.Gratuity = Gratuity.Value;
						}
						else
						{
							if (!ecsp.Trip.PickMeUpNow.HasValue || !ecsp.Trip.PickMeUpNow.Value)
							{
								ecsp.Gratuity = null;
								result = false;
								logger.Warn(Messages.ECar_Common_InvalidReservationService);
								throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
							}
						}

						//CorporateSpecialInstructions
						var CorporateSpecialInstructions = specialInputs.Where(s => s.Name.ToLower() == "CorporateSpecialInstructions".ToLowerInvariant()).FirstOrDefault();
						if (CorporateSpecialInstructions != null)
						{
							ecsp.CorporateSpecialInstructions = CorporateSpecialInstructions.Value;
						}
						else
						{
							if (!ecsp.Trip.PickMeUpNow.HasValue || !ecsp.Trip.PickMeUpNow.Value)
							{
								ecsp.CorporateSpecialInstructions = null;
								result = false;
								logger.Warn(Messages.ECar_Common_InvalidReservationService);
								throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
							}
						}

						//Corporate Pyament 
						var CorporatePaymentMethod = specialInputs.Where(s => s.Name.ToLower() == "CorporatePaymentMethod".ToLowerInvariant()).FirstOrDefault();
						if (CorporatePaymentMethod != null)
						{
							ecsp.CorporatePaymentMethod = CorporatePaymentMethod.Value;
						}
						else
						{
							if (!ecsp.Trip.PickMeUpNow.HasValue || !ecsp.Trip.PickMeUpNow.Value)
							{
								ecsp.CorporatePaymentMethod = null;
								result = false;
								logger.Warn(Messages.ECar_Common_InvalidReservationService);
								throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
							}
						}
					}
					else
					{
						throw VtodException.CreateException(ExceptionType.ECar, 20022);
					}
				}
				catch (Exception ex)
				{
					logger.Error("Aleph: Error While Validating Aleph Profile Info", ex);
					result = false;
					logger.Warn("Error while validating Aleph profile inputs.");
					throw VtodException.CreateException(ExceptionType.ECar, 20023);
				}
			}
			else
			{
				result = false;
				logger.Warn(Messages.ECar_Common_InvalidReservationService);
				throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
			}
			#endregion

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateCancelRequest");
			#endregion
			return result;
		}

		public bool ValidateCancelRequestForDispatchTripID(TokenRS tokenVTOD, OTA_GroundCancelRQ request, ref AlephCancelParameter eccp)
		{
			bool result = true;
			Stopwatch sw = new Stopwatch();
			sw.Start();
			var vtodDomain = new VTOD.VTODDomain(); vtodDomain.TrackTime = TrackTime;

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
			#endregion

			#region Set token, request
			eccp.tokenVTOD = tokenVTOD;
			eccp.request = request;
			#endregion

			#region Validate Unique ID
			string uid = request.Reservation.UniqueID.FirstOrDefault().ID;
			if (request.Reservation.UniqueID.Any())
			{
				if (!string.IsNullOrWhiteSpace(uid))
				{
					logger.Info("Request is valid.");
				}
				else
				{
					result = false;
					logger.Info("request.Reservation.UniqueID.First().ID is empty.");
				}
			}
			else
			{
				result = false;
				logger.Info("Reservation.UniqueID is empty.");
			}
			#endregion

			#region Deleted
			#region Validate Trip
			//if (result)
			//{
			//    vtod_trip t = GetECarTrip(uid);
			//    if (t != null)
			//    {
			//        eccp.Trip = t;
			//        logger.InfoFormat("Trip found. tripID={0}", uid);
			//    }
			//    else
			//    {
			//        result = false;
			//        logger.WarnFormat("Cannot find this trip. tripID={0}", uid);
			//    }
			//}
			//else
			//{
			//    result = false;
			//    logger.Warn("Cannot find trip by invalid referenceId");
			//}
			#endregion

			#region Validate Fleet
			//if (result)
			//{
			//    ecar_fleet fleet = null;
			//    if (GetFleet(eccp.Trip.ecar_trip.FleetId, out fleet))
			//    {
			//        eccp.Fleet = fleet;
			//        logger.InfoFormat("Fleet found. ID={0}", fleet.Id);

			//        if (fleet != null)
			//        {
			//            eccp.serviceAPIID = GetECarFleetServiceAPIPreference(fleet.Id, ECarServiceAPIPreferenceConst.Cancel).ServiceAPIId;
			//        }
			//    }
			//    else
			//    {
			//        result = false;
			//        logger.Warn("Unable to find fleet by this fleetID");
			//    }
			//}
			//else
			//{
			//    result = false;
			//    logger.Warn("Unable to find fleet by this fleetID");
			//}
			#endregion

			#region Validate Fleet User
			//if (result)
			//{
			//    ecar_fleet_user user = null;
			//    if (GetECarFleetUser(eccp.Trip.ecar_trip.FleetId, tokenVTOD.Username, out user))
			//    {
			//        eccp.User = user;
			//        logger.Info("This user has sufficient privilege.");
			//    }
			//    else
			//    {
			//        result = false;
			//        logger.WarnFormat("Insufficient privilege. User={0}, FleetId={1}", tokenVTOD.Username, eccp.Fleet.Id);
			//    }
			//}
			//else
			//{
			//    result = false;
			//    logger.Warn("Cannot validate this user by invalid fleet ");
			//}
			#endregion
			#endregion

			#region Get Email
			try
			{
				var CorporateEmail = request.TPA_Extensions.RateQualifiers.FirstOrDefault().SpecialInputs.Where(s => s.Name.ToLower().Trim() == "corporateemail").FirstOrDefault().Value;  //specialInputs.Where(s => s.Name.ToLower() == "CorporateEmail".ToLowerInvariant()).FirstOrDefault();
				if (!string.IsNullOrEmpty(CorporateEmail))
				{
					eccp.CorporateEmail = CorporateEmail;
				}
				else
				{
					eccp.CorporateEmail = null;
					result = false;
					logger.Warn(Messages.ECar_Common_InvalidReservationService);
					throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
				}

			}
			catch
			{
				eccp.CorporateEmail = null;
				result = false;
				logger.Warn(Messages.ECar_Common_InvalidReservationService);
				throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
			}

			#endregion

			#region Validate Aleph Inputs

			if (result)
			{
				try
				{
					var specialInputRequest = request.TPA_Extensions.WebServiceState;
					List<NameValue> specialInputs = null;

					if (!string.IsNullOrWhiteSpace(specialInputRequest))
					{
						specialInputs = specialInputRequest.JsonDeserialize<List<NameValue>>();
					}


					if (specialInputs != null && specialInputs.Any())
					{
						////CorporateEmail 
						////var CorporateEmail = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateEmail".ToLowerInvariant()).FirstOrDefault();
						//var CorporateEmail = specialInputs.Where(s => s.Name.ToLower() == "CorporateEmail".ToLowerInvariant()).FirstOrDefault();
						//if (CorporateEmail != null)
						//{
						//	eccp.CorporateEmail = CorporateEmail.Value;
						//}
						//else
						//{
						//	eccp.CorporateEmail = null;
						//	result = false;
						//	logger.Warn(Messages.ECar_Common_InvalidReservationService);
						//	throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
						//}



						//CorporateProviderID
						//var CorporateProviderID = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateProviderID".ToLowerInvariant()).FirstOrDefault();
						var CorporateProviderID = specialInputs.Where(s => s.Name.ToLower() == "CorporateProviderID".ToLowerInvariant()).FirstOrDefault();
						if (CorporateProviderID != null)
						{
							eccp.CorporateProviderID = CorporateProviderID.Value;
						}
						else
						{
							if (!eccp.Trip.PickMeUpNow.HasValue || !eccp.Trip.PickMeUpNow.Value)
							{
								eccp.CorporateProviderID = null;
								result = false;
								logger.Warn(Messages.ECar_Common_InvalidReservationService);
								throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
							}
						}


						//CorporateCompanyName
						//var CorporateCompanyName = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateCompanyName".ToLowerInvariant()).FirstOrDefault();
						var CorporateCompanyName = specialInputs.Where(s => s.Name.ToLower() == "CorporateCompanyName".ToLowerInvariant()).FirstOrDefault();
						if (CorporateCompanyName != null)
						{
							eccp.CorporateCompanyName = CorporateCompanyName.Value;
						}
						else
						{
							if (!eccp.Trip.PickMeUpNow.HasValue || !eccp.Trip.PickMeUpNow.Value)
							{
								eccp.CorporateCompanyName = null;
								result = false;
								logger.Warn(Messages.ECar_Common_InvalidReservationService);
								throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
							}
						}


						//CorporateProviderAccountID
						//var CorporateProviderAccountID = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateProviderAccountID".ToLowerInvariant()).FirstOrDefault();
						var CorporateProviderAccountID = specialInputs.Where(s => s.Name.ToLower() == "CorporateProviderAccountID".ToLowerInvariant()).FirstOrDefault();
						if (CorporateProviderAccountID != null)
						{
							eccp.CorporateProviderAccountID = CorporateProviderAccountID.Value;
						}
						else
						{
							if (!eccp.Trip.PickMeUpNow.HasValue || !eccp.Trip.PickMeUpNow.Value)
							{
								eccp.CorporateProviderAccountID = null;
								result = false;
								logger.Warn(Messages.ECar_Common_InvalidReservationService);
								throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
							}
						}


						//CorporateProfileID
						//var CorporateProfileID = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateProfileID".ToLowerInvariant()).FirstOrDefault();
						var CorporateProfileID = specialInputs.Where(s => s.Name.ToLower() == "CorporateProfileID".ToLowerInvariant()).FirstOrDefault();
						if (CorporateProfileID != null)
						{
							eccp.CorporateProfileID = CorporateProfileID.Value;
						}
						else
						{
							if (!eccp.Trip.PickMeUpNow.HasValue || !eccp.Trip.PickMeUpNow.Value)
							{
								eccp.CorporateProfileID = null;
								result = false;
								logger.Warn(Messages.ECar_Common_InvalidReservationService);
								throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
							}
						}

						//Dispatch Code
						var DispatchCode = specialInputs.Where(s => s.Name.ToLower() == "DispatchCode".ToLowerInvariant()).FirstOrDefault();
						if (DispatchCode != null)
						{
							eccp.DispatchCode = DispatchCode.Value;
						}
						else
						{
							if (!eccp.Trip.PickMeUpNow.HasValue || !eccp.Trip.PickMeUpNow.Value)
							{
								eccp.DispatchCode = null;
								result = false;
								logger.Warn(Messages.ECar_Common_InvalidReservationService);
								throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
							}
						}

						//Gratuity
						var Gratuity = specialInputs.Where(s => s.Name.ToLower() == "Gratuity".ToLowerInvariant()).FirstOrDefault();
						if (Gratuity != null)
						{
							eccp.Gratuity = Gratuity.Value;
						}
						else
						{
							if (!eccp.Trip.PickMeUpNow.HasValue || !eccp.Trip.PickMeUpNow.Value)
							{
								eccp.Gratuity = null;
								result = false;
								logger.Warn(Messages.ECar_Common_InvalidReservationService);
								throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
							}
						}

						//CorporateSpecialInstructions
						var CorporateSpecialInstructions = specialInputs.Where(s => s.Name.ToLower() == "CorporateSpecialInstructions".ToLowerInvariant()).FirstOrDefault();
						if (CorporateSpecialInstructions != null)
						{
							eccp.CorporateSpecialInstructions = CorporateSpecialInstructions.Value;
						}
						else
						{
							if (!eccp.Trip.PickMeUpNow.HasValue || !eccp.Trip.PickMeUpNow.Value)
							{
								eccp.CorporateSpecialInstructions = null;
								result = false;
								logger.Warn(Messages.ECar_Common_InvalidReservationService);
								throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
							}
						}

						//Corporate Pyament 
						var CorporatePaymentMethod = specialInputs.Where(s => s.Name.ToLower() == "CorporatePaymentMethod".ToLowerInvariant()).FirstOrDefault();
						if (CorporatePaymentMethod != null)
						{
							eccp.CorporatePaymentMethod = CorporatePaymentMethod.Value;
						}
						else
						{
							if (!eccp.Trip.PickMeUpNow.HasValue || !eccp.Trip.PickMeUpNow.Value)
							{
								eccp.CorporatePaymentMethod = null;
								result = false;
								logger.Warn(Messages.ECar_Common_InvalidReservationService);
								throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
							}
						}

					}
					else
					{
						throw VtodException.CreateException(ExceptionType.ECar, 20022);
					}
				}
				catch (Exception ex)
				{
					logger.Error("Aleph: Error While Validating Aleph Profile Info", ex);
					result = false;
					logger.Warn("Error while validating Aleph profile inputs.");
					throw VtodException.CreateException(ExceptionType.ECar, 20023);
				}
			}
			else
			{
				result = false;
				logger.Warn(Messages.ECar_Common_InvalidReservationService);
				throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
			}
			#endregion


			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Create ecar log
			if (result)
			{
				if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
				{
					WriteECarLog(eccp.request.Reservation.UniqueID.FirstOrDefault().ID.ToInt64(), eccp.serviceAPIID, ECarServiceMethodType.Cancel, sw.ElapsedMilliseconds, string.Empty, string.Empty, null, null);
				}
			}
			else
			{
				result = false;
				logger.Warn("Cannot create or get this ecar log by invalid trip");
			}
			#endregion

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateCancelRequest");
			#endregion

			return result;
		}

		public bool ValidateGetVehicleInfoRequest(TokenRS tokenVTOD, OTA_GroundAvailRQ request, ref AlephGetVehicleInfoParameter ecgvip)
		{
			bool result = true;
			Stopwatch sw = new Stopwatch();
			sw.Start();


			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
			#endregion

			#region Set token, request
			ecgvip.tokenVTOD = tokenVTOD;
			ecgvip.request = request;
			#endregion


			if (request.Service != null)
			{
				if (request.Service.Pickup != null)
				{
					#region Validate longtitude, latitude
					if (request.Service.Pickup.Address != null)
					{
						if (!string.IsNullOrWhiteSpace(request.Service.Pickup.Address.Latitude) && !string.IsNullOrWhiteSpace(request.Service.Pickup.Address.Longitude))
						{
							double longtitue = 0;
							double latitude = 0;
							if (double.TryParse(request.Service.Pickup.Address.Longitude, out longtitue) && double.TryParse(request.Service.Pickup.Address.Latitude, out latitude))
							{
								ecgvip.Longtitude = longtitue;
								ecgvip.Latitude = latitude;
							}
							else
							{
								result = false;
								logger.Warn("Invalid longitude and latittude");
							}
						}
						else
						{
							result = false;
							logger.Warn("Invalid longitude and latittude");
						}
					}
					else
					{
						result = false;
						logger.Warn("Cannot find any longitude or latitude from address");
					}
					#endregion

					#region Validate Pickup Address
					if (result)
					{
						Pickup_Dropoff_Stop stop = request.Service.Pickup;

						if ((stop.Address != null) && (stop.AirportInfo == null))
						{

							if (stop.Address.CountryName != null && !string.IsNullOrWhiteSpace(stop.Address.CountryName.Code) && !string.IsNullOrWhiteSpace(stop.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(stop.Address.CityName) && !string.IsNullOrWhiteSpace(stop.Address.PostalCode))
							{
								Map.DTO.Address pickupAddress = new Map.DTO.Address { Country = stop.Address.CountryName.Code, State = stop.Address.StateProv.StateCode, City = stop.Address.CityName, ZipCode = stop.Address.PostalCode, Geolocation = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(stop.Address.Latitude), Longitude = Convert.ToDecimal(stop.Address.Longitude) } };
								ecgvip.PickupAddressType = UDI.VTOD.Domain.ECar.Const.AddressType.Address;
								ecgvip.PickupAddress = pickupAddress;
							}
							else if (stop.Address.CountryName != null && !string.IsNullOrWhiteSpace(stop.Address.CountryName.Code) && !string.IsNullOrWhiteSpace(stop.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(stop.Address.CityName))
							{
								Map.DTO.Address pickupAddress = new Map.DTO.Address { Country = stop.Address.CountryName.Code, State = stop.Address.StateProv.StateCode, City = stop.Address.CityName, Geolocation = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(stop.Address.Latitude), Longitude = Convert.ToDecimal(stop.Address.Longitude) } };
								ecgvip.PickupAddressType = UDI.VTOD.Domain.ECar.Const.AddressType.Address;
								ecgvip.PickupAddress = pickupAddress;

							}
							else if (!string.IsNullOrWhiteSpace(stop.Address.LocationName))
							{
								result = false;
								logger.Warn("Cannot use locationName from address.");
							}
							else
							{
								result = false;
								logger.Warn("Cannot find any address/locationName from address.");
							}
						}
						else if ((stop.Address == null) && (stop.AirportInfo != null))
						{
							if ((stop.AirportInfo.Arrival != null) && (stop.AirportInfo.Departure == null))
							{
								result = false;
								logger.Warn("Cannot use aiportinfo for address");
							}
							else if ((stop.AirportInfo.Arrival == null) && (stop.AirportInfo.Departure != null))
							{
								result = false;
								logger.Warn("Cannot use aiportinfo for address");
							}
							else if ((stop.AirportInfo.Arrival != null) && (stop.AirportInfo.Departure != null))
							{
								result = false;
								logger.Warn("This OTA request contains both AirportInfo.Arrival and AirportInfo.Departure");
							}
							else
							{
								result = false;
								logger.Warn("Cannot find any arrival or departure from AirportInfo");
							}
						}
						else
						{
							//find address by longitude and latitude
							Map.DTO.Address pickupAddress = null;
							if (!LookupAddress(ecgvip.Longtitude, ecgvip.Latitude, out pickupAddress))
							{
								logger.WarnFormat("Cannot find longtitude={0} and latitude={1}", ecgvip.Longtitude, ecgvip.Latitude);
								result = false;
							}

							ecgvip.PickupAddressType = UDI.VTOD.Domain.ECar.Const.AddressType.Address;
							ecgvip.PickupAddress = pickupAddress;
						}
					}
					else
					{
						result = false;
					}
					#endregion

					#region Validate Fleet
					if (result)
					{
						List<ecar_fleet> fleetList = null;
						if (GetFleet(ecgvip.PickupAddressType, ecgvip.PickupAddress, ecgvip.PickupLandmark, out fleetList))
						{
							///egvip.Fleet = fleet;
							ecgvip.fleetList = fleetList.ToList();
							logger.InfoFormat("Fleet found. count={0}", fleetList.Count());

							if (fleetList.Any())
							{
								foreach (var f in fleetList)
								{
									//egvip.serviceAPIID = GetECarFleetServiceAPIPreference(fleet.Id, ECarServiceAPIPreferenceConst.GetVehicleInfo).ServiceAPIId;
									long serviceAPIID = GetECarFleetServiceAPIPreference(f.Id, ECarServiceAPIPreferenceConst.GetVehicleInfo).ServiceAPIId;
									if (!ecgvip.serviceAPIIDList.Where(x => x == serviceAPIID).Any())
									{
										ecgvip.serviceAPIIDList.Add(serviceAPIID);
									}


									//log
									if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
									{
										//WriteECarLog(null, egvip.serviceAPIID, ECarServiceMethodType.GetVehicleInfo, sw.ElapsedMilliseconds, string.Empty, string.Empty, null, null);
										//unknown service method type
										WriteECarLog(null, serviceAPIID, ECarServiceMethodType.GetVehicleInfo, sw.ElapsedMilliseconds, string.Empty, string.Empty, null, null);
									}
								}

							}
						}
						else
						{
							result = false;
							logger.Warn("Unable to find fleet by this address or landmark.");
						}
					}
					else
					{
						result = false;
						logger.Warn("Unable to find fleet by invalid address or landmark.");
					}
					#endregion

					#region Validate Zone
					if (result)
					{
						//Default: Circle
						if (request.Service.Pickup.Address.TPA_Extensions != null)
						{
							#region Validate Polygon Type
							if (request.Service.Pickup.Address.TPA_Extensions.Zone != null)
							{
								if (request.Service.Pickup.Address.TPA_Extensions.Zone.Circle != null)
								{
									ecgvip.Radius = double.Parse(request.Service.Pickup.Address.TPA_Extensions.Zone.Circle.Radius.ToString());
									ecgvip.PolygonType = PolygonTypeConst.Circle;
								}

								if (string.IsNullOrWhiteSpace(ecgvip.PolygonType))
								{
									result = false;
									logger.Warn("Unable to find the polygon area for GetVehicleInfo");
								}
							}
							else
							{
								result = false;
								logger.Warn("request.TPA_Extensions.Zone");
							}
							#endregion

							#region Validate Vehicle status
							if (request.Service.Pickup.Address.TPA_Extensions.Vehicles != null)
							{
								if (request.Service.Pickup.Address.TPA_Extensions.Vehicles.Items.Any())
								{
									//tgvip.VehicleStausList = request.Service.Pickup.Address.TPA_Extensions.Vehicles.Items;
									if (!"all".Equals(request.Service.Pickup.Address.TPA_Extensions.Vehicles.Items.FirstOrDefault().VehicleStatus.ToLower()))
									{
										Vehicle v = new Vehicle { VehicleStatus = request.Service.Pickup.Address.TPA_Extensions.Vehicles.Items.FirstOrDefault().VehicleStatus.ToString() };
										ecgvip.VehicleStausList.Add(v);
									}
									else
									{
										logger.Info("Vehicle status = ALL");
									}
								}
								else
								{
									result = false;
									logger.Warn("request.Service.Pickup.Address.TPA_Extensions.Vehicles.Items is empty");
								}

							}
							else
							{
								result = false;
								logger.Info("Unable to find request.TPA_Extensions.Vehicles");
							}

							#endregion
						}
						else
						{
							result = false;
							logger.Warn("Unable to find request.TPA_Extensions");
						}
					}
					else
					{
						result = false;
						logger.Warn("Cannot get polygon type by invalid request");
					}
					#endregion

					#region Validate Aleph Inputs

					if (result)
					{
						try
						{

							var rateQualifier = request.RateQualifiers.First();
							if (rateQualifier.SpecialInputs != null)
							{
								//CorporateEmail 
								var CorporateEmail = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateEmail".ToLowerInvariant()).FirstOrDefault();
								if (CorporateEmail != null)
								{
									ecgvip.CorporateEmail = CorporateEmail.Value;
								}
								else
								{
									ecgvip.CorporateEmail = null;
									result = false;
									logger.Warn(Messages.ECar_Common_InvalidReservationService);
									throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
								}



								//CorporateProviderID
								//var CorporateProviderID = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateProviderID".ToLowerInvariant()).FirstOrDefault();
								//if (CorporateProviderID != null)
								//{
								//	ecgvip.CorporateProviderID = CorporateProviderID.Value;
								//}
								//else
								//{
								//	ecgvip.CorporateProviderID = null;
								//	result = false;
								//	logger.Warn(Messages.ECar_Common_InvalidReservationService);
								//	throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
								//}


								//CorporateCompanyName
								//var CorporateCompanyName = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateCompanyName".ToLowerInvariant()).FirstOrDefault();
								//if (CorporateCompanyName != null)
								//{
								//	ecgvip.CorporateCompanyName = CorporateCompanyName.Value;
								//}
								//else
								//{
								//	ecgvip.CorporateCompanyName = null;
								//	result = false;
								//	logger.Warn(Messages.ECar_Common_InvalidReservationService);
								//	throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
								//}


								//CorporateProviderAccountID
								//var CorporateProviderAccountID = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateProviderAccountID".ToLowerInvariant()).FirstOrDefault();
								//if (CorporateProviderAccountID != null)
								//{
								//	ecgvip.CorporateProviderAccountID = CorporateProviderAccountID.Value;
								//}
								//else
								//{
								//	ecgvip.CorporateProviderAccountID = null;
								//	result = false;
								//	logger.Warn(Messages.ECar_Common_InvalidReservationService);
								//	throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
								//}


								//CorporateProfileID
								//var CorporateProfileID = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "CorporateProfileID".ToLowerInvariant()).FirstOrDefault();
								//if (CorporateProfileID != null)
								//{
								//	ecgvip.CorporateProfileID = CorporateProfileID.Value;
								//}
								//else
								//{
								//	ecgvip.CorporateProfileID = null;
								//	result = false;
								//	logger.Warn(Messages.ECar_Common_InvalidReservationService);
								//	throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
								//}
							}
							else
							{
								throw VtodException.CreateException(ExceptionType.ECar, 20022);
							}


						}
						catch (Exception ex)
						{
							logger.Error("Aleph: Error While Validating Aleph Profile Info", ex);
							result = false;
							logger.Warn("Error while validating Aleph profile inputs.");
							throw VtodException.CreateException(ExceptionType.ECar, 20023);
						}
					}
					else
					{
						result = false;
						logger.Warn(Messages.ECar_Common_InvalidReservationService);
						throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
					}
					#endregion
				}
				else
				{
					result = false;
					logger.Warn("request.Service.Pickup is null");
				}
			}
			else
			{
				result = false;
				logger.Warn("request.Service is null");
			}

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);




			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateGetVehicleInfoRequest");
			#endregion

			return result;
		}


		public Person ExtractDriverName(AlephVehicle vehicle)
		{
			Person p = null;
			Telephone t = null;
			Tuple<string, string, string> splitPhone = null;
			try
			{
				if (vehicle != null)
				{
					//Name
					p = new Person();
					p.PersonName = new PersonName();
					p.PersonName.GivenName = vehicle.driver_first_name;
					p.PersonName.Surname = vehicle.driver_last_name;


					//Phone
					p.Telephones = new List<Telephone>();
					t = new Telephone();
					splitPhone = Phone.SplitPhone(vehicle.driver_phone_number);

					if (splitPhone == null)
					{
						t.PhoneNumber = Phone.CleanPhone(vehicle.driver_phone_number);
					}
					else
					{
						t.CountryAccessCode = splitPhone.Item1;
						t.AreaCityCode = splitPhone.Item2;
						t.PhoneNumber = splitPhone.Item3;
					}

					p.Telephones.Add(t);
				}

			}
			catch { }

			return p;
		}

		public vtod_trip GetTripDetails(long Id)
		{
			vtod_trip t = null;
			using (VTODEntities context = new VTODEntities())
			{
				t = context.vtod_trip.Include("ecar_trip").Where(x => x.Id == Id).Select(x => x).FirstOrDefault();

			}
			return t;
		}
		public string ConvertTripStatus(string source, string device, string version)
		{
			string target = source;
			using (VTODEntities context = new VTODEntities())
			{
				var t = context.ecar_status_alephwrapper.Where(x => x.Source == source).Select(x => x.Target).FirstOrDefault();
				if (!string.IsNullOrWhiteSpace(t))
				{
					target = t;
					logger.InfoFormat("Convert status from {0} to {1}", source, target);
				}
			}
			#region "Handling the Status=Accept,Diaptch,Assign"
			if (string.IsNullOrEmpty(device) && string.IsNullOrEmpty(version))
			{
				if (source.ToUpper() == "Assign".ToUpper() || source.ToUpper() == "Accept".ToUpper() || source.ToUpper() == "Dispatch".ToUpper())
				{
					target = source;
				}
			}
			#endregion
			return target;
		}

		public int GetUserDetails(string userName)
		{
			#region GetUser
			using (VTODEntities context = new VTODEntities())
			{
				var user = context.my_aspnet_users.Where(s => s.name.ToLower() == userName.ToLower()).ToList().FirstOrDefault();
				return user.id;
			}
			#endregion
		}
		public vtod_trip CreateNewTrip(AlephBookingParameter ecbp, string userName)
		{
			Stopwatch sw = new Stopwatch();
			sw.Start();
			Stopwatch swEachPart = new Stopwatch();
			//ecar_trip ec = new ecar_trip();
			vtod_trip trip = new vtod_trip();
			trip.ecar_trip = new ecar_trip();
			GroundReservation reservation = ecbp.request.GroundReservations.FirstOrDefault();
			TimeHelper utcTimeHelper = new TimeHelper();
			using (VTODEntities context = new VTODEntities())
			{

				swEachPart.Restart();
				#region Consumer confirmation Id
				if (!string.IsNullOrWhiteSpace(ecbp.ConsumerConfirmationID))
				{
					trip.ecar_trip.ConsumerConfirmationId = ecbp.ConsumerConfirmationID;
				}
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set consumer confirmation ID:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region fleetId and DNI
				trip.ecar_trip.FleetId = ecbp.Fleet.Id;

				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set fletId and DNI Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Counsumer Source, Device, Ref
				trip.ConsumerDevice = ecbp.Device;
				trip.ConsumerSource = ecbp.Source;
				trip.ConsumerRef = ecbp.Ref;
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set Consumer Device, Source, Ref:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Expire
				double timeToExpire = 0;
				if (!double.TryParse(ConfigurationManager.AppSettings["ECar_TimeToExpire"], out timeToExpire))
				{
					timeToExpire = 10;
				}
				trip.ecar_trip.Expires = DateTime.Parse(reservation.Service.Location.Pickup.DateTime).AddMinutes(timeToExpire);
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set expire time Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region PickupTime and DropOffTime
				trip.PickupDateTime = ecbp.PickupDateTime;
				//trip.PickupDateTimeUTC = ecbp.PickupDateTime.ToUtc(ecbp.Fleet.ServerUTCOffset);

				//DateTime dropOffDateTime;
				//if (tbp.DropOffDateTime.HasValue)
				//{
				//	t.DropOffDateTime = tbp.DropOffDateTime.Value;
				//	t.PickupDateTimeUTC = tbp.DropOffDateTime.Value.ToUtc(tbp.Fleet.ServerUTCOffset);
				//}
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set pickuptime and dropioffTime Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Get Account number
				//ToDo: it should not be hardcoded. It should read it from DB based on FleetID
				using (var db = new DataAccess.VTOD.VTODEntities())
				{
					var accountNumber = db.SP_ECar_GetFleet_AccountNumber(trip.ecar_trip.FleetId, ecbp.tokenVTOD.Username).ToList().FirstOrDefault();
					if (!string.IsNullOrWhiteSpace(accountNumber))
					{
						trip.ecar_trip.AccountNumber = accountNumber;
					}
				}
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set expire time Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region pickup address
				if (ecbp.PickupAddressType == AddressType.Address)
				{
					trip.ecar_trip.PickupAddressType = ecbp.PickupAddressType;
					double pickupLat = 0;
					double pickupLongt = 0;
					if (Double.TryParse(ecbp.PickupAddress.Geolocation.Latitude.ToString(), out pickupLat))
					{
						trip.ecar_trip.PickupLatitude = pickupLat;
					}

					if (Double.TryParse(ecbp.PickupAddress.Geolocation.Longitude.ToString(), out pickupLongt))
					{
						trip.ecar_trip.PickupLongitude = pickupLongt;
					}

					trip.ecar_trip.PickupStreetNo = ecbp.PickupAddress.StreetNo;
					trip.ecar_trip.PickupStreetName = ecbp.PickupAddress.Street;
					trip.ecar_trip.PickupStreetType = ExtractStreetType(ecbp.PickupAddress.Street);
					trip.ecar_trip.PickupAptNo = ecbp.PickupAddress.ApartmentNo;
					trip.ecar_trip.PickupCity = ecbp.PickupAddress.City;
					trip.ecar_trip.PickupStateCode = ecbp.PickupAddress.State;
					trip.ecar_trip.PickupZipCode = ecbp.PickupAddress.ZipCode;
					trip.ecar_trip.PickupCountryCode = ecbp.PickupAddress.Country;
					trip.ecar_trip.PickupFullAddress = ecbp.PickupAddress.FullAddress;
				}
				else if (ecbp.PickupAddressType == AddressType.Airport)
				{
					trip.ecar_trip.PickupAddressType = ecbp.PickupAddressType;
					trip.ecar_trip.PickupAirport = ecbp.PickupAirport;
					trip.ecar_trip.PickupFullAddress = ecbp.PickupAirport;
				}
				else
				{
					logger.Warn("No pickup location");
					trip.ecar_trip.PickupAddressType = AddressType.Empty;
				}
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set pickup address Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region PickUp UTC Time
				try
				{
					if (trip.ecar_trip.PickupLatitude != null && trip.ecar_trip.PickupLongitude != null)
					{
						trip.PickupDateTimeUTC = utcTimeHelper.GetUTCFromLocalTime(trip.ecar_trip.PickupLatitude, trip.ecar_trip.PickupLongitude, ecbp.PickupDateTime);
					}
					else
					{
						if (ecbp.PickupAddressType == AddressType.Airport)
						{
							double? longitude;
							double? latitude;
							if (GetLongitudeAndLatitudeForAirport(ecbp.PickupAirport, out longitude, out latitude))
							{
								trip.PickupDateTimeUTC = utcTimeHelper.GetUTCFromLocalTime(latitude, longitude, ecbp.PickupDateTime);
							}
						}
						else
						{
							trip.PickupDateTimeUTC = ecbp.PickupDateTime.ToUtc(ecbp.Fleet.ServerUTCOffset);
						}
					}
				}
				catch (Exception ex)
				{
					logger.InfoFormat("Exception in PickUpTime UTC:{0}", ex.Message);
					trip.PickupDateTimeUTC = ecbp.PickupDateTime.ToUtc(ecbp.Fleet.ServerUTCOffset);
				}
				#endregion

				swEachPart.Stop();
				logger.DebugFormat("Set PickUp UTC Time Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region dropoff address
				//Dropoff is optioanl : By_Pouyan
				if (reservation.Service.Location.Dropoff != null)
				{
					double dropoffLat = 0;
					double dropoffLongt = 0;
					if (ecbp.DropOffAddressType == AddressType.Address)
					{
						trip.ecar_trip.DropOffAddressType = ecbp.DropOffAddressType;
						if (ecbp.DropOffAddress.Geolocation != null)
						{
							if (Double.TryParse(ecbp.DropOffAddress.Geolocation.Latitude.ToString(), out dropoffLat))
							{
								trip.ecar_trip.DropOffLatitude = dropoffLat;
							}
							if (Double.TryParse(ecbp.DropOffAddress.Geolocation.Longitude.ToString(), out dropoffLongt))
							{
								trip.ecar_trip.DropOffLongitude = dropoffLongt;
							}
						}

						trip.ecar_trip.DropOffStreetNo = ecbp.DropOffAddress.StreetNo;
						trip.ecar_trip.DropOffStreetName = ecbp.DropOffAddress.Street;
						trip.ecar_trip.DropOffStreetType = ExtractStreetType(ecbp.DropOffAddress.Street);
						trip.ecar_trip.DropOffAptNo = ecbp.DropOffAddress.ApartmentNo;
						trip.ecar_trip.DropOffCity = ecbp.DropOffAddress.City;
						trip.ecar_trip.DropOffStateCode = ecbp.DropOffAddress.State;
						trip.ecar_trip.DropOffZipCode = ecbp.DropOffAddress.ZipCode;
						trip.ecar_trip.DropOffCountryCode = ecbp.DropOffAddress.Country;
						trip.ecar_trip.DropOffFullAddress = ecbp.DropOffAddress.FullAddress;

					}
					else if (ecbp.DropOffAddressType == AddressType.Airport)
					{
						trip.ecar_trip.DropOffAddressType = ecbp.DropOffAddressType;
						trip.ecar_trip.DropOffAirport = ecbp.DropoffAirport;
						trip.ecar_trip.DropOffFullAddress = ecbp.DropoffAirport;
					}
					else if (ecbp.DropOffAddressType == AddressType.Empty)
					{
						trip.ecar_trip.DropOffAddressType = AddressType.Empty;
						logger.Info("No drop off location");
					}
				}
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set dropoff address Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region common fields
				trip.ecar_trip.ServiceAPIId = ecbp.serviceAPIID;
				//t.CustomerId = tbp.Customer.Id;
				trip.ecar_trip.FirstName = ecbp.FirstName;
				trip.ecar_trip.LastName = ecbp.LastName;
				trip.ecar_trip.PhoneNumber = ecbp.PhoneNumber;
				//t.CabNumber = "";
				trip.ecar_trip.MinutesAway = null;
				trip.ecar_trip.NumberOfPassenger = ecbp.request.TPA_Extensions.Passengers.Sum(x => x.Quantity);
				//t.DriverNotes = null;
				trip.ecar_trip.RefernceNumber = ecbp.request.EchoToken;
				//if (ecbp.PickupAddressType == AddressType.Address)
				if (reservation.Service.Location.Pickup != null && !string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Remark))
				{
					trip.ecar_trip.RemarkForPickup = reservation.Service.Location.Pickup.Remark;
				}
				//if (ecbp.DropOffAddressType == AddressType.Address)
				if (reservation.Service.Location.Dropoff != null && !string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Remark))
				{
					trip.ecar_trip.RemarkForDropOff = reservation.Service.Location.Dropoff.Remark;
				}
				//t.wheelchairAccessible = reservation.Service.DisabilityVehicleInd;

				trip.AppendTime = DateTime.Now;
				trip.FleetType = Common.DTO.Enum.FleetType.ExecuCar.ToString();
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set other common fields Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Payment
				if (ecbp.PaymentType == Common.DTO.Enum.PaymentType.PaymentCard)
				{
					trip.PaymentType = ecbp.PaymentType.ToString();
					trip.CreditCardID = ecbp.CreditCardID.HasValue ? ecbp.CreditCardID.Value.ToString() : null;
				}
				else
				{
					trip.PaymentType = ecbp.PaymentType.ToString();
				}
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set payment type Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region MemberID
				trip.MemberID = ecbp.MemberID.HasValue ? ecbp.MemberID.Value.ToString() : null;
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set member Id:{0} ms.", swEachPart.ElapsedMilliseconds);

				if (!string.IsNullOrEmpty(ecbp.emailAddress))
				{
					trip.EmailAddress = ecbp.emailAddress;
				}

				swEachPart.Restart();
				#region PromisedETA

				swEachPart.Stop();
				logger.DebugFormat("Set promised eta:{0} ms.", swEachPart.ElapsedMilliseconds);
				try
				{

					trip.PromisedETA = ecbp.PromisedETA;
				}
				catch
				{
					trip.PromisedETA = 0;
				}


				swEachPart.Restart();
				#endregion
				#region Check Fare and Gratuity
				#region Check if it's percentage or amount

				if (!string.IsNullOrWhiteSpace(ecbp.Gratuity) && !string.IsNullOrWhiteSpace(ecbp.Rate))
				{

					if (ecbp.Gratuity.Contains("%"))
					{
						//calculate by Percentage;
						trip.GratuityRate = ecbp.Gratuity.Replace("%", "").ToString().ToDecimal();
						trip.Gratuity = ecbp.Rate.ToDecimal() * trip.GratuityRate * new decimal(0.01);
					}
					else
					{
						//calculate by gratuity;
						trip.Gratuity = ecbp.Gratuity.ToDecimal();
					}

					// fare + Gratuity
					trip.TotalFareAmount = CalculateTotalPrice(ecbp.Gratuity, ecbp.Rate).ToDecimal();

				}
				else if (string.IsNullOrWhiteSpace(ecbp.Gratuity) && !string.IsNullOrWhiteSpace(ecbp.Rate))
				{
					//only fare, no gratuity
					trip.TotalFareAmount = ecbp.Rate.ToDecimal();

				}



				#endregion

				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set Gratuity Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region PickMeUpNow
				if (trip.PickupDateTimeUTC <= DateTime.UtcNow.AddMinutes(5))
				{
					trip.PickMeUpNow = true;
				}
				else
				{
					trip.PickMeUpNow = false;
				}
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set Pickup now Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Server Booking time
				trip.BookingServerDateTime = System.DateTime.Now;
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("BookingServerDateTime:{0} ms.", swEachPart.ElapsedMilliseconds);

				//swEachPart.Restart();
				//#region Fare
				//if(ecbp.)


				//#endregion
				//swEachPart.Stop();
				//logger.DebugFormat("Set User Id Duration:{0} ms.", swEachPart.ElapsedMilliseconds);


				swEachPart.Restart();
				#region User
				#region GetUser
				var user = context.my_aspnet_users.Where(s => s.name.ToLower() == userName.ToLower()).ToList().FirstOrDefault();
				trip.UserID = user.id;
				if (string.IsNullOrEmpty(trip.ConsumerDevice))
				{
					if (user != null)
					{
						trip.ConsumerDevice = user.name;
					}
				}
				#endregion

				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set User Id Duration:{0} ms.", swEachPart.ElapsedMilliseconds);


				swEachPart.Restart();
				#region WebSeriveState
				//ToDo:WebServiceState
				//trip.WebServiceState = ecbp.WebServiceState;

				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set User Id Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				logger.Info("before we create a ecar trip");
				swEachPart.Restart();
				#region Save
				context.vtod_trip.Add(trip);
				context.SaveChanges();
				#endregion

				swEachPart.Restart();
				#region Add relation between SDS Trip and Ecar trip
				MapSDSTripToECarTrip(ecbp.ConsumerConfirmationID, trip.Id);
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Map sds trip to ecar trip. Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Stop();
				logger.DebugFormat("Save this trip Duration:{0} ms.", swEachPart.ElapsedMilliseconds);
				logger.Info("after we create a ecar trip");
			}

			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return trip;
		}

		public vtod_trip ModifyTrip(AlephBookingParameter ecbp, string userName)
		{
			Stopwatch sw = new Stopwatch();
			sw.Start();
			Stopwatch swEachPart = new Stopwatch();
			//ecar_trip ec = new ecar_trip();
			vtod_trip trip = new vtod_trip();
			trip.ecar_trip = new ecar_trip();
			GroundReservation reservation = ecbp.request.GroundReservations.FirstOrDefault();
			TimeHelper utcTimeHelper = new TimeHelper();
			using (VTODEntities context = new VTODEntities())
			{

				//var v = context.vtod_trip.Where(x => x.Id == t.Id).Select(x => x).FirstOrDefault();
				//var e = context.ecar_trip.Where(x => x.Id == t.Id).Select(x => x).FirstOrDefault();
				swEachPart.Restart();
				#region Consumer confirmation Id
				if (!string.IsNullOrWhiteSpace(ecbp.ConsumerConfirmationID))
				{
					trip.ecar_trip.ConsumerConfirmationId = ecbp.ConsumerConfirmationID;
				}
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set consumer confirmation ID:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region fleetId and DNI
				trip.ecar_trip.FleetId = ecbp.Fleet.Id;

				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set fletId and DNI Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Counsumer Source, Device, Ref
				trip.ConsumerDevice = ecbp.Device;
				trip.ConsumerSource = ecbp.Source;
				trip.ConsumerRef = ecbp.Ref;
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set Consumer Device, Source, Ref:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Expire
				double timeToExpire = 0;
				if (!double.TryParse(ConfigurationManager.AppSettings["ECar_TimeToExpire"], out timeToExpire))
				{
					timeToExpire = 10;
				}
				trip.ecar_trip.Expires = DateTime.Parse(reservation.Service.Location.Pickup.DateTime).AddMinutes(timeToExpire);
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set expire time Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region PickupTime and DropOffTime
				trip.PickupDateTime = ecbp.PickupDateTime;
				//trip.PickupDateTimeUTC = ecbp.PickupDateTime.ToUtc(ecbp.Fleet.ServerUTCOffset);

				//DateTime dropOffDateTime;
				//if (tbp.DropOffDateTime.HasValue)
				//{
				//	t.DropOffDateTime = tbp.DropOffDateTime.Value;
				//	t.PickupDateTimeUTC = tbp.DropOffDateTime.Value.ToUtc(tbp.Fleet.ServerUTCOffset);
				//}
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set pickuptime and dropioffTime Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Get Account number
				//ToDo: it should not be hardcoded. It should read it from DB based on FleetID
				using (var db = new DataAccess.VTOD.VTODEntities())
				{
					var accountNumber = db.SP_ECar_GetFleet_AccountNumber(trip.ecar_trip.FleetId, ecbp.tokenVTOD.Username).ToList().FirstOrDefault();
					if (!string.IsNullOrWhiteSpace(accountNumber))
					{
						trip.ecar_trip.AccountNumber = accountNumber;
					}
				}
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set expire time Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region pickup address
				if (ecbp.PickupAddressType == AddressType.Address)
				{
					trip.ecar_trip.PickupAddressType = ecbp.PickupAddressType;
					double pickupLat = 0;
					double pickupLongt = 0;
					if (Double.TryParse(ecbp.PickupAddress.Geolocation.Latitude.ToString(), out pickupLat))
					{
						trip.ecar_trip.PickupLatitude = pickupLat;
					}

					if (Double.TryParse(ecbp.PickupAddress.Geolocation.Longitude.ToString(), out pickupLongt))
					{
						trip.ecar_trip.PickupLongitude = pickupLongt;
					}

					trip.ecar_trip.PickupStreetNo = ecbp.PickupAddress.StreetNo;
					trip.ecar_trip.PickupStreetName = ecbp.PickupAddress.Street;
					trip.ecar_trip.PickupStreetType = ExtractStreetType(ecbp.PickupAddress.Street);
					trip.ecar_trip.PickupAptNo = ecbp.PickupAddress.ApartmentNo;
					trip.ecar_trip.PickupCity = ecbp.PickupAddress.City;
					trip.ecar_trip.PickupStateCode = ecbp.PickupAddress.State;
					trip.ecar_trip.PickupZipCode = ecbp.PickupAddress.ZipCode;
					trip.ecar_trip.PickupCountryCode = ecbp.PickupAddress.Country;
					trip.ecar_trip.PickupFullAddress = ecbp.PickupAddress.FullAddress;
				}
				else if (ecbp.PickupAddressType == AddressType.Airport)
				{
					trip.ecar_trip.PickupAddressType = ecbp.PickupAddressType;
					trip.ecar_trip.PickupAirport = ecbp.PickupAirport;
					trip.ecar_trip.PickupFullAddress = ecbp.PickupAirport;
				}
				else
				{
					logger.Warn("No pickup location");
					trip.ecar_trip.PickupAddressType = AddressType.Empty;
				}
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set pickup address Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region PickUp UTC Time
				try
				{
					if (trip.ecar_trip.PickupLatitude != null && trip.ecar_trip.PickupLongitude != null)
					{
						trip.PickupDateTimeUTC = utcTimeHelper.GetUTCFromLocalTime(trip.ecar_trip.PickupLatitude, trip.ecar_trip.PickupLongitude, ecbp.PickupDateTime);
					}
					else
					{
						if (ecbp.PickupAddressType == AddressType.Airport)
						{
							double? longitude;
							double? latitude;
							if (GetLongitudeAndLatitudeForAirport(ecbp.PickupAirport, out longitude, out latitude))
							{
								trip.PickupDateTimeUTC = utcTimeHelper.GetUTCFromLocalTime(latitude, longitude, ecbp.PickupDateTime);
							}
						}
						else
						{
							trip.PickupDateTimeUTC = ecbp.PickupDateTime.ToUtc(ecbp.Fleet.ServerUTCOffset);
						}
					}
				}
				catch (Exception ex)
				{
					logger.InfoFormat("Exception in PickUpTime UTC:{0}", ex.Message);
					trip.PickupDateTimeUTC = ecbp.PickupDateTime.ToUtc(ecbp.Fleet.ServerUTCOffset);
				}
				#endregion

				swEachPart.Stop();
				logger.DebugFormat("Set PickUp UTC Time Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region dropoff address
				//Dropoff is optioanl : By_Pouyan
				if (reservation.Service.Location.Dropoff != null)
				{
					double dropoffLat = 0;
					double dropoffLongt = 0;
					if (ecbp.DropOffAddressType == AddressType.Address)
					{
						trip.ecar_trip.DropOffAddressType = ecbp.DropOffAddressType;
						if (ecbp.DropOffAddress.Geolocation != null)
						{
							if (Double.TryParse(ecbp.DropOffAddress.Geolocation.Latitude.ToString(), out dropoffLat))
							{
								trip.ecar_trip.DropOffLatitude = dropoffLat;
							}
							if (Double.TryParse(ecbp.DropOffAddress.Geolocation.Longitude.ToString(), out dropoffLongt))
							{
								trip.ecar_trip.DropOffLongitude = dropoffLongt;
							}
						}

						trip.ecar_trip.DropOffStreetNo = ecbp.DropOffAddress.StreetNo;
						trip.ecar_trip.DropOffStreetName = ecbp.DropOffAddress.Street;
						trip.ecar_trip.DropOffStreetType = ExtractStreetType(ecbp.DropOffAddress.Street);
						trip.ecar_trip.DropOffAptNo = ecbp.DropOffAddress.ApartmentNo;
						trip.ecar_trip.DropOffCity = ecbp.DropOffAddress.City;
						trip.ecar_trip.DropOffStateCode = ecbp.DropOffAddress.State;
						trip.ecar_trip.DropOffZipCode = ecbp.DropOffAddress.ZipCode;
						trip.ecar_trip.DropOffCountryCode = ecbp.DropOffAddress.Country;
						trip.ecar_trip.DropOffFullAddress = ecbp.DropOffAddress.FullAddress;

					}
					else if (ecbp.DropOffAddressType == AddressType.Airport)
					{
						trip.ecar_trip.DropOffAddressType = ecbp.DropOffAddressType;
						trip.ecar_trip.DropOffAirport = ecbp.DropoffAirport;
						trip.ecar_trip.DropOffFullAddress = ecbp.DropoffAirport;
					}
					else if (ecbp.DropOffAddressType == AddressType.Empty)
					{
						trip.ecar_trip.DropOffAddressType = AddressType.Empty;
						logger.Info("No drop off location");
					}
				}
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set dropoff address Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region common fields
				trip.ecar_trip.ServiceAPIId = ecbp.serviceAPIID;
				//t.CustomerId = tbp.Customer.Id;
				trip.ecar_trip.FirstName = ecbp.FirstName;
				trip.ecar_trip.LastName = ecbp.LastName;
				trip.ecar_trip.PhoneNumber = ecbp.PhoneNumber;
				//t.CabNumber = "";
				trip.ecar_trip.MinutesAway = null;
				trip.ecar_trip.NumberOfPassenger = ecbp.request.TPA_Extensions.Passengers.Sum(x => x.Quantity);
				//t.DriverNotes = null;
				trip.ecar_trip.RefernceNumber = ecbp.request.EchoToken;
				//if (ecbp.PickupAddressType == AddressType.Address)
				if (reservation.Service.Location.Pickup != null && !string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Remark))
				{
					trip.ecar_trip.RemarkForPickup = reservation.Service.Location.Pickup.Remark;
				}
				//if (ecbp.DropOffAddressType == AddressType.Address)
				if (reservation.Service.Location.Dropoff != null && !string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Remark))
				{
					trip.ecar_trip.RemarkForDropOff = reservation.Service.Location.Dropoff.Remark;
				}
				//t.wheelchairAccessible = reservation.Service.DisabilityVehicleInd;

				trip.AppendTime = DateTime.Now;
				trip.FleetType = Common.DTO.Enum.FleetType.ExecuCar.ToString();
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set other common fields Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Payment
				if (ecbp.PaymentType == Common.DTO.Enum.PaymentType.PaymentCard)
				{
					trip.PaymentType = ecbp.PaymentType.ToString();
					trip.CreditCardID = ecbp.CreditCardID.HasValue ? ecbp.CreditCardID.Value.ToString() : null;
				}
				else
				{
					trip.PaymentType = ecbp.PaymentType.ToString();
				}
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set payment type Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region MemberID
				trip.MemberID = ecbp.MemberID.HasValue ? ecbp.MemberID.Value.ToString() : null;
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set member Id:{0} ms.", swEachPart.ElapsedMilliseconds);

				if (!string.IsNullOrEmpty(ecbp.emailAddress))
				{
					trip.EmailAddress = ecbp.emailAddress;
				}

				swEachPart.Restart();
				#region Check Fare and Gratuity
				#region Check if it's percentage or amount

				if (!string.IsNullOrWhiteSpace(ecbp.Gratuity) && !string.IsNullOrWhiteSpace(ecbp.Rate))
				{

					if (ecbp.Gratuity.Contains("%"))
					{
						//calculate by Percentage;
						trip.GratuityRate = ecbp.Gratuity.Replace("%", "").ToString().ToDecimal();
						trip.Gratuity = ecbp.Rate.ToDecimal() * trip.GratuityRate * new decimal(0.01);
					}
					else
					{
						//calculate by gratuity;
						trip.Gratuity = ecbp.Gratuity.ToDecimal();
					}

					// fare + Gratuity
					trip.TotalFareAmount = CalculateTotalPrice(ecbp.Gratuity, ecbp.Rate).ToDecimal();

				}
				else if (string.IsNullOrWhiteSpace(ecbp.Gratuity) && !string.IsNullOrWhiteSpace(ecbp.Rate))
				{
					//only fare, no gratuity
					trip.TotalFareAmount = ecbp.Rate.ToDecimal();

				}



				#endregion

				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set Gratuity Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region PickMeUpNow
				if (trip.PickupDateTimeUTC <= DateTime.UtcNow.AddMinutes(5))
				{
					trip.PickMeUpNow = true;
				}
				else
				{
					trip.PickMeUpNow = false;
				}
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set Pickup now Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Server Booking time
				trip.BookingServerDateTime = System.DateTime.Now;
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("BookingServerDateTime:{0} ms.", swEachPart.ElapsedMilliseconds);

				//swEachPart.Restart();
				//#region Fare
				//if(ecbp.)


				//#endregion
				//swEachPart.Stop();
				//logger.DebugFormat("Set User Id Duration:{0} ms.", swEachPart.ElapsedMilliseconds);


				swEachPart.Restart();
				#region User
				#region GetUser
				var user = context.my_aspnet_users.Where(s => s.name.ToLower() == userName.ToLower()).ToList().FirstOrDefault();
				trip.UserID = user.id;
				if (string.IsNullOrEmpty(trip.ConsumerDevice))
				{
					if (user != null)
					{
						trip.ConsumerDevice = user.name;
					}
				}
				#endregion

				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set User Id Duration:{0} ms.", swEachPart.ElapsedMilliseconds);


				swEachPart.Restart();
				#region WebSeriveState
				//ToDo:WebServiceState
				//trip.WebServiceState = ecbp.WebServiceState;

				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set User Id Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				logger.Info("before we create a ecar trip");
				swEachPart.Restart();
				#region Save
				context.vtod_trip.Add(trip);
				context.SaveChanges();
				#endregion

				swEachPart.Restart();
				#region Add relation between SDS Trip and Ecar trip
				MapSDSTripToECarTrip(ecbp.ConsumerConfirmationID, trip.Id);
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Map sds trip to ecar trip. Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Stop();
				logger.DebugFormat("Save this trip Duration:{0} ms.", swEachPart.ElapsedMilliseconds);
				logger.Info("after we create a ecar trip");
			}

			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return trip;
		}
		public vtod_trip UpdateTrip(AlephBookingParameter ecbp, string userName, string alephVendorTripID, string type)
		{
			Stopwatch sw = new Stopwatch();
			sw.Start();
			Stopwatch swEachPart = new Stopwatch();
			//ecar_trip ec = new ecar_trip();
			vtod_trip trip = new vtod_trip();
			trip.ecar_trip = new ecar_trip();
			GroundReservation reservation = ecbp.request.GroundReservations.FirstOrDefault();
			TimeHelper utcTimeHelper = new TimeHelper();
			using (VTODEntities context = new VTODEntities())
			{
				ecar_trip e;
				if (type.ToLower() == Common.DTO.Enum.ConfirmationType.DispatchConfirmation.ToString().Trim().ToLower())
				{
					e = context.ecar_trip.Where(p => p.DispatchTripId.Contains(alephVendorTripID)).FirstOrDefault();
					trip = context.vtod_trip.Where(p => p.Id == e.Id).FirstOrDefault();
				}
				else
				{
					e = context.ecar_trip.Where(p => p.Id == alephVendorTripID.ToInt32()).FirstOrDefault();
					trip = context.vtod_trip.Where(p => p.Id == e.Id).FirstOrDefault();
				}

				swEachPart.Restart();
				#region Consumer confirmation Id
				if (!string.IsNullOrWhiteSpace(ecbp.ConsumerConfirmationID))
				{
					trip.ecar_trip.ConsumerConfirmationId = ecbp.ConsumerConfirmationID;
				}
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set consumer confirmation ID:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region fleetId and DNI
				trip.ecar_trip.FleetId = ecbp.Fleet.Id;

				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set fletId and DNI Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Counsumer Source, Device, Ref
				trip.ConsumerDevice = ecbp.Device;
				trip.ConsumerSource = ecbp.Source;
				trip.ConsumerRef = ecbp.Ref;
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set Consumer Device, Source, Ref:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Expire
				double timeToExpire = 0;
				if (!double.TryParse(ConfigurationManager.AppSettings["ECar_TimeToExpire"], out timeToExpire))
				{
					timeToExpire = 10;
				}
				trip.ecar_trip.Expires = DateTime.Parse(reservation.Service.Location.Pickup.DateTime).AddMinutes(timeToExpire);
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set expire time Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region PickupTime and DropOffTime
				trip.PickupDateTime = ecbp.PickupDateTime;
				//trip.PickupDateTimeUTC = ecbp.PickupDateTime.ToUtc(ecbp.Fleet.ServerUTCOffset);

				//DateTime dropOffDateTime;
				//if (tbp.DropOffDateTime.HasValue)
				//{
				//	t.DropOffDateTime = tbp.DropOffDateTime.Value;
				//	t.PickupDateTimeUTC = tbp.DropOffDateTime.Value.ToUtc(tbp.Fleet.ServerUTCOffset);
				//}
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set pickuptime and dropioffTime Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Get Account number
				//ToDo: it should not be hardcoded. It should read it from DB based on FleetID
				using (var db = new DataAccess.VTOD.VTODEntities())
				{
					var accountNumber = db.SP_ECar_GetFleet_AccountNumber(trip.ecar_trip.FleetId, ecbp.tokenVTOD.Username).ToList().FirstOrDefault();
					if (!string.IsNullOrWhiteSpace(accountNumber))
					{
						trip.ecar_trip.AccountNumber = accountNumber;
					}
				}
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set expire time Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region pickup address
				if (ecbp.PickupAddressType == AddressType.Address)
				{
					trip.ecar_trip.PickupAddressType = ecbp.PickupAddressType;
					double pickupLat = 0;
					double pickupLongt = 0;
					if (Double.TryParse(ecbp.PickupAddress.Geolocation.Latitude.ToString(), out pickupLat))
					{
						trip.ecar_trip.PickupLatitude = pickupLat;
					}

					if (Double.TryParse(ecbp.PickupAddress.Geolocation.Longitude.ToString(), out pickupLongt))
					{
						trip.ecar_trip.PickupLongitude = pickupLongt;
					}

					trip.ecar_trip.PickupStreetNo = ecbp.PickupAddress.StreetNo;
					trip.ecar_trip.PickupStreetName = ecbp.PickupAddress.Street;
					trip.ecar_trip.PickupStreetType = ExtractStreetType(ecbp.PickupAddress.Street);
					trip.ecar_trip.PickupAptNo = ecbp.PickupAddress.ApartmentNo;
					trip.ecar_trip.PickupCity = ecbp.PickupAddress.City;
					trip.ecar_trip.PickupStateCode = ecbp.PickupAddress.State;
					trip.ecar_trip.PickupZipCode = ecbp.PickupAddress.ZipCode;
					trip.ecar_trip.PickupCountryCode = ecbp.PickupAddress.Country;
					trip.ecar_trip.PickupFullAddress = ecbp.PickupAddress.FullAddress;
				}
				else if (ecbp.PickupAddressType == AddressType.Airport)
				{
					trip.ecar_trip.PickupAddressType = ecbp.PickupAddressType;
					trip.ecar_trip.PickupAirport = ecbp.PickupAirport;
					trip.ecar_trip.PickupFullAddress = ecbp.PickupAirport;
				}
				else
				{
					logger.Warn("No pickup location");
					trip.ecar_trip.PickupAddressType = AddressType.Empty;
				}
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set pickup address Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region PickUp UTC Time
				try
				{
					if (trip.ecar_trip.PickupLatitude != null && trip.ecar_trip.PickupLongitude != null)
					{
						trip.PickupDateTimeUTC = utcTimeHelper.GetUTCFromLocalTime(trip.ecar_trip.PickupLatitude, trip.ecar_trip.PickupLongitude, ecbp.PickupDateTime);
					}
					else
					{
						if (ecbp.PickupAddressType == AddressType.Airport)
						{
							double? longitude;
							double? latitude;
							if (GetLongitudeAndLatitudeForAirport(ecbp.PickupAirport, out longitude, out latitude))
							{
								trip.PickupDateTimeUTC = utcTimeHelper.GetUTCFromLocalTime(latitude, longitude, ecbp.PickupDateTime);
							}
						}
						else
						{
							trip.PickupDateTimeUTC = ecbp.PickupDateTime.ToUtc(ecbp.Fleet.ServerUTCOffset);
						}
					}
				}
				catch (Exception ex)
				{
					logger.InfoFormat("Exception in PickUpTime UTC:{0}", ex.Message);
					trip.PickupDateTimeUTC = ecbp.PickupDateTime.ToUtc(ecbp.Fleet.ServerUTCOffset);
				}
				#endregion

				swEachPart.Stop();
				logger.DebugFormat("Set PickUp UTC Time Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region dropoff address
				//Dropoff is optioanl : By_Pouyan
				if (reservation.Service.Location.Dropoff != null)
				{
					double dropoffLat = 0;
					double dropoffLongt = 0;
					if (ecbp.DropOffAddressType == AddressType.Address)
					{
						trip.ecar_trip.DropOffAddressType = ecbp.DropOffAddressType;
						if (ecbp.DropOffAddress.Geolocation != null)
						{
							if (Double.TryParse(ecbp.DropOffAddress.Geolocation.Latitude.ToString(), out dropoffLat))
							{
								trip.ecar_trip.DropOffLatitude = dropoffLat;
							}
							if (Double.TryParse(ecbp.DropOffAddress.Geolocation.Longitude.ToString(), out dropoffLongt))
							{
								trip.ecar_trip.DropOffLongitude = dropoffLongt;
							}
						}

						trip.ecar_trip.DropOffStreetNo = ecbp.DropOffAddress.StreetNo;
						trip.ecar_trip.DropOffStreetName = ecbp.DropOffAddress.Street;
						trip.ecar_trip.DropOffStreetType = ExtractStreetType(ecbp.DropOffAddress.Street);
						trip.ecar_trip.DropOffAptNo = ecbp.DropOffAddress.ApartmentNo;
						trip.ecar_trip.DropOffCity = ecbp.DropOffAddress.City;
						trip.ecar_trip.DropOffStateCode = ecbp.DropOffAddress.State;
						trip.ecar_trip.DropOffZipCode = ecbp.DropOffAddress.ZipCode;
						trip.ecar_trip.DropOffCountryCode = ecbp.DropOffAddress.Country;
						trip.ecar_trip.DropOffFullAddress = ecbp.DropOffAddress.FullAddress;

					}
					else if (ecbp.DropOffAddressType == AddressType.Airport)
					{
						trip.ecar_trip.DropOffAddressType = ecbp.DropOffAddressType;
						trip.ecar_trip.DropOffAirport = ecbp.DropoffAirport;
						trip.ecar_trip.DropOffFullAddress = ecbp.DropoffAirport;
					}
					else if (ecbp.DropOffAddressType == AddressType.Empty)
					{
						trip.ecar_trip.DropOffAddressType = AddressType.Empty;
						logger.Info("No drop off location");
					}
				}
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set dropoff address Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region common fields
				trip.ecar_trip.ServiceAPIId = ecbp.serviceAPIID;
				//t.CustomerId = tbp.Customer.Id;
				trip.ecar_trip.FirstName = ecbp.FirstName;
				trip.ecar_trip.LastName = ecbp.LastName;
				trip.ecar_trip.PhoneNumber = ecbp.PhoneNumber;
				//t.CabNumber = "";
				trip.ecar_trip.MinutesAway = null;
				trip.ecar_trip.NumberOfPassenger = ecbp.request.TPA_Extensions.Passengers.Sum(x => x.Quantity);
				//t.DriverNotes = null;
				trip.ecar_trip.RefernceNumber = ecbp.request.EchoToken;
				//if (ecbp.PickupAddressType == AddressType.Address)
				if (reservation.Service.Location.Pickup != null && !string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Remark))
				{
					trip.ecar_trip.RemarkForPickup = reservation.Service.Location.Pickup.Remark;
				}
				//if (ecbp.DropOffAddressType == AddressType.Address)
				if (reservation.Service.Location.Dropoff != null && !string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Remark))
				{
					trip.ecar_trip.RemarkForDropOff = reservation.Service.Location.Dropoff.Remark;
				}
				//t.wheelchairAccessible = reservation.Service.DisabilityVehicleInd;

				trip.AppendTime = DateTime.Now;
				trip.FleetType = Common.DTO.Enum.FleetType.ExecuCar.ToString();
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set other common fields Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Payment
				if (ecbp.PaymentType == Common.DTO.Enum.PaymentType.PaymentCard)
				{
					trip.PaymentType = ecbp.PaymentType.ToString();
					trip.CreditCardID = ecbp.CreditCardID.HasValue ? ecbp.CreditCardID.Value.ToString() : null;
				}
				else
				{
					trip.PaymentType = ecbp.PaymentType.ToString();
				}
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set payment type Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region MemberID
				trip.MemberID = ecbp.MemberID.HasValue ? ecbp.MemberID.Value.ToString() : null;
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set member Id:{0} ms.", swEachPart.ElapsedMilliseconds);

				if (!string.IsNullOrEmpty(ecbp.emailAddress))
				{
					trip.EmailAddress = ecbp.emailAddress;
				}

				swEachPart.Restart();
				#region Check Fare and Gratuity
				#region Check if it's percentage or amount

				if (!string.IsNullOrWhiteSpace(ecbp.Gratuity) && !string.IsNullOrWhiteSpace(ecbp.Rate))
				{

					if (ecbp.Gratuity.Contains("%"))
					{
						//calculate by Percentage;
						trip.GratuityRate = ecbp.Gratuity.Replace("%", "").ToString().ToDecimal();
						trip.Gratuity = ecbp.Rate.ToDecimal() * trip.GratuityRate * new decimal(0.01);
					}
					else
					{
						//calculate by gratuity;
						trip.Gratuity = ecbp.Gratuity.ToDecimal();
					}

					// fare + Gratuity
					trip.TotalFareAmount = CalculateTotalPrice(ecbp.Gratuity, ecbp.Rate).ToDecimal();

				}
				else if (string.IsNullOrWhiteSpace(ecbp.Gratuity) && !string.IsNullOrWhiteSpace(ecbp.Rate))
				{
					//only fare, no gratuity
					trip.TotalFareAmount = ecbp.Rate.ToDecimal();

				}



				#endregion

				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set Gratuity Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region PickMeUpNow
				if (trip.PickupDateTimeUTC <= DateTime.UtcNow.AddMinutes(5))
				{
					trip.PickMeUpNow = true;
				}
				else
				{
					trip.PickMeUpNow = false;
				}
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set Pickup now Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Server Booking time
				trip.BookingServerDateTime = System.DateTime.Now;
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("BookingServerDateTime:{0} ms.", swEachPart.ElapsedMilliseconds);

				//swEachPart.Restart();
				//#region Fare
				//if(ecbp.)


				//#endregion
				//swEachPart.Stop();
				//logger.DebugFormat("Set User Id Duration:{0} ms.", swEachPart.ElapsedMilliseconds);


				swEachPart.Restart();
				#region User
				#region GetUser
				var user = context.my_aspnet_users.Where(s => s.name.ToLower() == userName.ToLower()).ToList().FirstOrDefault();
				trip.UserID = user.id;
				if (string.IsNullOrEmpty(trip.ConsumerDevice))
				{
					if (user != null)
					{
						trip.ConsumerDevice = user.name;
					}
				}
				#endregion

				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set User Id Duration:{0} ms.", swEachPart.ElapsedMilliseconds);


				swEachPart.Restart();
				#region WebSeriveState
				//ToDo:WebServiceState
				//trip.WebServiceState = ecbp.WebServiceState;

				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set User Id Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				logger.Info("before we create a ecar trip");
				swEachPart.Restart();
				#region Save
				//context.vtod_trip.Add(trip);
				context.SaveChanges();
				#endregion

				swEachPart.Restart();
				#region Add relation between SDS Trip and Ecar trip
				//MapSDSTripToECarTrip(ecbp.ConsumerConfirmationID, trip.Id);
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Map sds trip to ecar trip. Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Stop();
				logger.DebugFormat("Save this trip Duration:{0} ms.", swEachPart.ElapsedMilliseconds);
				logger.Info("after we create a ecar trip");
			}

			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return trip;
		}
		public List<NameValue> InsertorUpdateCorporateSpecialInstructions(List<NameValue> input, AlephBookingRS bookingInfo)
		{
			List<NameValue> output = new List<NameValue>();
			NameValue pair = null;

			//Create the new namevalue pairs and add them to the new list

			//CorporateProviderID
			pair = new NameValue();
			pair.Name = "CorporateProviderID";
			pair.Value = bookingInfo.provider_id.ToString();
			output.Add(pair);

			//CorporateCompanyName
			pair = new NameValue();
			pair.Name = "CorporateCompanyName";
			pair.Value = bookingInfo.company_name;
			output.Add(pair);

			//CorporateProviderAccountID
			pair = new NameValue();
			pair.Name = "CorporateProviderAccountID";
			pair.Value = bookingInfo.provider_account_id;
			output.Add(pair);

			//CorporateProfileID
			pair = new NameValue();
			pair.Name = "CorporateProfileID";
			pair.Value = bookingInfo.profile_id;
			output.Add(pair);

			//CorporatePaymentMethod
			pair = new NameValue();
			pair.Name = "CorporatePaymentMethod";
			pair.Value = bookingInfo.payment_method;
			output.Add(pair);


			//Walk through the old list of namevalue pairs and add any others to the new list 
			if ((input != null) && (input.Count > 0))
			{
				foreach (NameValue oldPair in input)
				{
					if (
						(oldPair.Name.Trim().ToLower() != "corporateproviderid") &&
						(oldPair.Name.Trim().ToLower() != "corporatecompanyname") &&
						(oldPair.Name.Trim().ToLower() != "corporateprovideraccountid") &&
						(oldPair.Name.Trim().ToLower() != "corporateprofileid") &&
						(oldPair.Name.Trim().ToLower() != "corporatepaymentmethod")
					  )
					{
						output.Add(oldPair);
					}
				}
			}

			return output;
		}
		public List<NameValue> InsertorUpdateCorporateSpecialInstructionsForCorporate(List<NameValue> input, AlephReservationRS reservationResponse)
		{
			List<NameValue> output = new List<NameValue>();
			NameValue pair = null;

			//Create the new namevalue pairs and add them to the new list

			//CorporateProviderID
			pair = new NameValue();
			pair.Name = "CorporateProviderID";
			pair.Value = reservationResponse.provider_id.ToString();
			output.Add(pair);

			//CorporateCompanyName
			pair = new NameValue();
			pair.Name = "CorporateCompanyName";
			pair.Value = reservationResponse.company_name;
			output.Add(pair);

			//CorporateProviderAccountID
			pair = new NameValue();
			pair.Name = "CorporateProviderAccountID";
			pair.Value = reservationResponse.provider_account_id;
			output.Add(pair);

			//CorporateProfileID
			pair = new NameValue();
			pair.Name = "CorporateProfileID";
			pair.Value = reservationResponse.profile_id;
			output.Add(pair);

			//CorporatePaymentMethod
			pair = new NameValue();
			pair.Name = "CorporatePaymentMethod";
			pair.Value = reservationResponse.payment_method;
			output.Add(pair);

			pair = new NameValue();
			pair.Name = "CorporateName";
			pair.Value = reservationResponse.corporation_name;
			output.Add(pair);

			pair = new NameValue();
			pair.Name = "Account";
			pair.Value = reservationResponse.corporation_account_name;
			output.Add(pair);

			pair = new NameValue();
			pair.Name = "DispatchCode";
			pair.Value = "Aleph";
			output.Add(pair);

			#region CorporateEmail
			try
			{
				if (!string.IsNullOrEmpty(reservationResponse.corporateemail))
				{
					pair = new NameValue();
					pair.Name = "CorporateEmail";
					pair.Value = reservationResponse.corporateemail;
					output.Add(pair);
				}
				else if (reservationResponse.passengers != null && reservationResponse.passengers.Any())
				{
					var passenger = reservationResponse.passengers.First();
					if (!string.IsNullOrWhiteSpace(passenger.email_address))
					{
						pair = new NameValue();
						pair.Name = "CorporateEmail";
						pair.Value = passenger.email_address;
						output.Add(pair);
					}
				}
			}
			catch (Exception ex)
			{
				logger.Error(ex);
			}
			#endregion

			pair = new NameValue();
			pair.Name = "CorporateSpecialInstructions";
			pair.Value = reservationResponse.special_instructions == null ? "" : reservationResponse.special_instructions;
			output.Add(pair);



			pair = new NameValue();
			pair.Name = "Gratuity";
			pair.Value = "";
			output.Add(pair);

			pair = new NameValue();
			pair.Name = "PromisedETA";
			pair.Value = reservationResponse.ETA == null ? "" : reservationResponse.ETA;
			output.Add(pair);

			//Walk through the old list of namevalue pairs and add any others to the new list 
			if ((input != null) && (input.Count > 0))
			{
				foreach (NameValue oldPair in input)
				{
					if (
						(oldPair.Name.Trim().ToLower() != "corporateproviderid") &&
						(oldPair.Name.Trim().ToLower() != "corporatecompanyname") &&
						(oldPair.Name.Trim().ToLower() != "corporateprovideraccountid") &&
						(oldPair.Name.Trim().ToLower() != "corporateprofileid") &&
						(oldPair.Name.Trim().ToLower() != "corporatepaymentmethod") &&
						(oldPair.Name.Trim().ToLower() != "dispatchcode") &&
						(oldPair.Name.Trim().ToLower() != "corporatespecialinstructions") &&
						(oldPair.Name.Trim().ToLower() != "gratuity") &&
						 (oldPair.Name.Trim().ToLower() != "promisedeta")
					  )
					{
						output.Add(oldPair);
					}
				}
			}

			return output;
		}
		public bool SaveCorporateSpecialInstructions(string special, long tripId)
		{
			const string tableName = "vtod_trip";
			bool ret = true;
			vtod_WS_State state = new vtod_WS_State();

			try
			{
				using (VTODEntities context = new VTODEntities())
				{
					state.ReferenceId = tripId;
					state.TableName = tableName;
					state.State = special;
					state.Key = "CorporateIDs";
					state.AppendTime = DateTime.Now;

					#region Save
					context.vtod_WS_State.Add(state);
					context.SaveChanges();
					#endregion
				}
			}
			catch (Exception ex)
			{
				ret = false;
				logger.Error("Aleph: Error while saving Corporate Special Instructions to database.", ex);
			}

			return ret;
		}
		public bool UpdateCorporateSpecialInstructions(string special, long tripId)
		{
			const string tableName = "vtod_trip";
			bool ret = true;
			vtod_WS_State state = new vtod_WS_State();

			try
			{

				using (VTODEntities context = new VTODEntities())
				{
					state = context.vtod_WS_State.Where(p => p.Id == tripId && p.Key == "CorporateIDs").FirstOrDefault();
					state.ReferenceId = tripId;
					state.TableName = tableName;
					state.State = special;
					state.Key = "CorporateIDs";
					state.AppendTime = DateTime.Now;

					#region Save
					//context.vtod_WS_State.Add(state);
					context.SaveChanges();
					#endregion
				}
			}
			catch (Exception ex)
			{
				ret = false;
				logger.Error("Aleph: Error while updating Corporate Special Instructions to database.", ex);
			}

			return ret;
		}

		public bool SaveAlephCompanyInfo(long tripId, string jsonCompanyInfo)
		{
			const string tableName = "vtod_trip";
			bool ret = true;
			vtod_WS_State state = new vtod_WS_State();

			try
			{
				using (VTODEntities context = new VTODEntities())
				{
					state.ReferenceId = tripId;
					state.TableName = tableName;
					state.State = jsonCompanyInfo;
					state.Key = "CompanyInfo";
					state.AppendTime = DateTime.Now;

					#region Save
					context.vtod_WS_State.Add(state);
					context.SaveChanges();
					#endregion
				}
			}
			catch (Exception ex)
			{
				ret = false;
				logger.Error("Aleph: Error while saving Company Info to database.", ex);
			}

			return ret;
		}
		public bool UpdateAlephCompanyInfo(long tripId, string jsonCompanyInfo)
		{
			const string tableName = "vtod_trip";
			bool ret = true;
			vtod_WS_State state = new vtod_WS_State();

			try
			{
				using (VTODEntities context = new VTODEntities())
				{
					state = context.vtod_WS_State.Where(p => p.Id == tripId && p.Key == "CompanyInfo").FirstOrDefault();
					state.ReferenceId = tripId;
					state.TableName = tableName;
					state.State = jsonCompanyInfo;
					state.Key = "CompanyInfo";
					state.AppendTime = DateTime.Now;

					#region Save
					//context.vtod_WS_State.Add(state);
					context.SaveChanges();
					#endregion
				}
			}
			catch (Exception ex)
			{
				ret = false;
				logger.Error("Aleph: Error while updating Company Info to database.", ex);
			}

			return ret;
		}

		private bool GetLongitudeAndLatitudeForAirport(string airportCode, out double? longitude, out double? latitude)
		{
			bool result = false;


			//#region Track
			//if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
			//#endregion

			using (VTODEntities context = new VTODEntities())
			{
				vtod_polygon zone = context.vtod_polygon.Where(x => x.Name == airportCode && x.Type == AddressType.Airport).Select(x => x).FirstOrDefault();
				if (zone != null)
				{

					double lon;
					double lat;
					if (zone.CenterLongitude.HasValue && double.TryParse(zone.CenterLongitude.Value.ToString(), out lon))
					{
						longitude = lon;
						result = true;
					}
					else
					{
						logger.Error("Unable to convert longitude");
						result = false;
						longitude = null;
					}

					if (zone.CenterLatitude.HasValue && double.TryParse(zone.CenterLatitude.Value.ToString(), out lat))
					{
						latitude = lat;
						result = true;
					}
					else
					{
						logger.Error("Unable to convert latitude");
						result = false;
						latitude = null;
					}
				}
				else
				{
					result = false;
					longitude = null;
					latitude = null;
				}

			}

			//#region Track
			//if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "GetLongitudeAndLatitudeForAirport");
			//#endregion

			return result;
		}

	}
}
