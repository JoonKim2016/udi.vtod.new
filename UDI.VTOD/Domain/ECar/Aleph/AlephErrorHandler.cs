﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.ECar.Aleph
{
    public class AlephErrorHandler
    {
        public string message { set; get; }
        public string description { set; get; }
        public string error_code { set; get; }
    }
}
