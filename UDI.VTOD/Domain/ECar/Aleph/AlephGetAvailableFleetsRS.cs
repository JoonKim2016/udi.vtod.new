﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.ECar.Aleph
{
    public class AlephGetAvailableFleetsRS
    {
        public AlephFleet Corporate { get; set; }
        public AlephFleet Retail { get; set; }
    }
}
