﻿using System.Collections.Generic;

namespace UDI.VTOD.Domain.ECar.Aleph
{
    public class AlephGetCorporateRequirementsRS
    {
        public List<AlephCorporationRequirements> CorporationRequirements { get; set; }
    }
}
