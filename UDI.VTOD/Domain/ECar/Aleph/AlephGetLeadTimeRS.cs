﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.ECar.Aleph
{
   public class AlephGetLeadTimeRS
    {
       public string username { get; set; }
       public decimal longitude { get; set; }
       public string latitude { get; set; }
       public List<AlephProvider> providers { get; set; }
    }
}
