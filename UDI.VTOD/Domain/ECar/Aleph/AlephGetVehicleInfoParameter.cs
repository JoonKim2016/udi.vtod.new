﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Domain.ECar.Class;

namespace UDI.VTOD.Domain.ECar.Aleph
{
    public class AlephGetVehicleInfoParameter : ECarGetVehicleInfoParameter
    {
        public AlephGetVehicleInfoParameter()
            : base()
        {
        }
               
        public string CorporateProviderID { set; get; }
        public string CorporateCompanyName { get; set; }
        public string CorporateProviderAccountID { set; get; }
        public string CorporateProfileID { set; get; }
        public string CorporateEmail { set; get; }
    }
}
