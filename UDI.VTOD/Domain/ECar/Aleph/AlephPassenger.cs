﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.ECar.Aleph
{
    public class AlephPassenger
    {
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string phone_number { get; set; }
        public string email_address { get; set; }
        public bool is_primary_contact { get; set; }
        public bool is_system_user { get; set; }
        public string profile_id { get; set; }
    }
}
