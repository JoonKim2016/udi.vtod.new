﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.ECar.Aleph
{
    public class AlephPriceComponent
    {
        public string name { get; set; }
        public decimal amount { get; set; }
    }
}
