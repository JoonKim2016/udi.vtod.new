﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.ECar.Aleph
{
    public class AlephProvider
    {
        public string provider_name;
        public List<AlephCompany> companies;
    }
}
