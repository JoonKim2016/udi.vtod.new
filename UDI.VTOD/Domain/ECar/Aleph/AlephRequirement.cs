﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.ECar.Aleph
{
    public class AlephRequirement
    {
        public string requirement_name { get; set; }
        public bool should_use_typeahead_uri { get; set; }
        public bool is_required { get; set; }
        public bool provide_hints { get; set; }
        public string typeahead_base_uri { get; set; }
    }
}
