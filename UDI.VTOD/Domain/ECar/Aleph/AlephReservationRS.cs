﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
namespace UDI.VTOD.Domain.ECar.Aleph
{
   public class AlephReservationRS
    {
     
            public int provider_id { get; set; }
            public string provider_account_id { get; set; }
            public string corporateemail { get; set; }
            public string company_name { get; set; }
            public string company_phone_number { get; set; }
            public string profile_id { get; set; }
            public string originating_system_reference_number { get; set; }
            public string vendor_confirmation_no { get; set; }
            public string confirmation_no { get; set; }
            public DateTimeOffset requested_datetime{ get; set; }
            public DateTime? cancelled_timestamp { get; set; }
            public AlephAddress pickup { get; set; }
            public AlephAddress dropoff { get; set; }
            public string payment_method { get; set; }
            public string special_instructions { get; set; }
            public string provider_vehicle_id { get; set; }
            public string corporation_name { get; set; }
            public string corporation_account_name { get; set; }
            public bool is_asap { get; set; }
            public bool is_dropoff_as_directed { get; set; }
            public string provider_subaccount_id { get; set; }
            public List<AlephBookingCriteria> booking_criteria { get; set; }
            public List<AlephStatusMessage> status_messages { get; set; }
            public List<AlephPassenger> passengers { get; set; }
            public List<AlephGatewayTrackingNumber> gateway_tracking_numbers { get; set; }
            public AlephVehicle assigned_vehicle { get; set; }
            public AlephReservationPayment reservation_payment { get; set; }
            public string ETA { get; set; }
    }
}
