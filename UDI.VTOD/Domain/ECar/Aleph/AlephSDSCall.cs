﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;

namespace UDI.VTOD.Domain.ECar.Aleph
{
	public static class AlephSDSCall
	{

		public static string GetCorporateAccountKey(Common.DTO.OTA.TokenRS tokenVTOD, Common.Track.TrackTime trackTime, string corporateId)
		{
			string key = String.Empty;

			try
			{
				var membershipController = new UDI.SDS.MembershipController(tokenVTOD, trackTime);

				var result = membershipController.GetCorporateAccountKey(Common.DTO.Enum.CorporateName.Aleph.ToString(), corporateId);

                //Check if the key returned by SDS is blank and react accordingly
                if(!String.IsNullOrWhiteSpace(result.UserKey))
                {
                    key = result.UserKey.Trim();
                }
                else
                {
                    //This exception gets thrown if the SDS method returns a blank/empty string as the corporate key.
                    throw new Exception("20021", new Exception());
                }				
			}
			catch (Exception ex)
			{
				int vtodErrorCode = AlephVtodErrorCodeFactory.GetVtodErrorCode(GetMethodName(), ex);
				throw new Exception(vtodErrorCode.ToString(), ex);
			}

			return key;
		}
        

		//Returns the name of the method that calls this method. 
		private static string GetMethodName([CallerMemberName] string memberName = "")
		{
			return memberName;
		}
	}
}
