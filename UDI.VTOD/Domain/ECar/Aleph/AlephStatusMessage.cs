﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.ECar.Aleph
{
    public class AlephStatusMessage
    {
        public string status_message_type { get; set; }
        public string message_datetime { get; set; }
        public string message { get; set; }
        public string car_no { get; set; }
        public string driver_name { get; set; }
        public string driver_mobile_number { get; set; }
    }
}
