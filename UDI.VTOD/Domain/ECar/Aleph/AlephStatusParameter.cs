﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Domain.ECar.Class;

namespace UDI.VTOD.Domain.ECar.Aleph
{
    public class AlephStatusParameter : ECarStatusParameter
    {
        public AlephStatusParameter() : base()
		{			
		}

      
        public string CorporateProviderID { set; get; }
        public string CorporateCompanyName { get; set; }
        public string CorporateProviderAccountID { set; get; }
        public string CorporateProfileID { set; get; }
        public string CorporateEmail { set; get; }
        public string DispatchCode { set; get; }
        public string Gratuity { set; get; }
        public string CorporatePaymentMethod { set; get; }
        public string CorporateSpecialInstructions { set; get; }

    }
}
