﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Common.DTO.OTA;

namespace UDI.VTOD.Domain.ECar.Aleph
{
    public class AlephVehicleLocationRS
    {      
        public int provider_id { get; set; }
        public string provider_vehicle_id { get; set; }
        public string provider_driver_id { get; set; }
        public string driver_first_name { get; set; }
        public string driver_last_name { get; set; }
        public DateTime local_ts { get; set; }
        public decimal? latitude { get; set; }
        public decimal? longitude { get; set; }        
    }
}
