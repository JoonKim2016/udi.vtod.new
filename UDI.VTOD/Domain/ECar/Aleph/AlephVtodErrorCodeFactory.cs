﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.Utility.Serialization;

namespace UDI.VTOD.Domain.ECar.Aleph
{
    public static class AlephVtodErrorCodeFactory
    {

        public static int GetVtodErrorCode(string callingMethodName, Exception ex)
        {
            int ret = 0;

                        
            #region Call the factory method that is specific to the calling method

            switch (callingMethodName)
            {
                case "BookReservation":
                    ret = BookReservationErrorCode(ex);
                    break;

                case "BookReservationNow":
                    ret = BookReservationNowErrorCode(ex);
                    break;

                case "GetReservation":
                    ret = GetReservationErrorCode(ex);
                    break;

                case "ChangeReservation":
                    ret = ChangeReservationErrorCode(ex);
                    break;

                case "CancelReservation":
                    ret = CancelReservationErrorCode(ex);
                    break;

                case "GetBookingCriteria":
                    ret = GetBookingCriteriaErrorCode(ex);
                    break;

                case "GetBookingCriteriaNow":
                    ret = GetBookingCriteriaNowErrorCode(ex);
                    break;

                case "GetCriteriaValidation":
                    ret = GetCriteriaValidationErrorCode(ex);
                    break;

                case "GetVehicleLocation":
                    ret = GetVehicleLocationErrorCode(ex);
                    break;

                case "GetUserProfiles":
                    ret = GetUserProfilesErrorCode(ex);
                    break;

                case "VerifyUser":
                    ret = VerifyUserErrorCode(ex);
                    break;

                case "RegisterUser":
                    ret = RegisterUserErrorCode(ex);
                    break;

                case "ChangeUserPassword":
                    ret = ChangeUserPasswordErrorCode(ex);
                    break;

                case "DeleteUser":
                    ret = DeleteUserErrorCode(ex);
                    break;

                case "GetAvailableVehicles":
                    ret = GetAvailableVehiclesErrorCode(ex);
                    break;

                case "GetCorporateAccountKey":
                    ret = GetAvailableVehiclesErrorCode(ex);
                    break;

                case "GetAvailableFleets":
                    ret = GetAvailableFleetsErrorCode(ex);
                    break;

                case "BuildReservation":
                    ret = BuildReservationErrorCode(ex);
                    break;

				case "BuildReservationBookNow":
					ret = BuildReservationBookNowErrorCode(ex);
					break;

				case "ModifyBookReservation":
					ret = BuildModifyBookReservationErrorCode(ex);
					break;
                case "AddCorporatePaymentCard":
                case "DeleteCorporatePaymentCard":
                    ret = BuildCorporatePaymentCardErrorCode(ex);
                    break;
                default:
                    ret = 20004;
                    break;
            }

            #endregion


            return ret;
        }


        private static int BookReservationErrorCode(Exception ex)
        {
            int code = 0;


            if (ex.InnerException.Message.Contains("(400)"))
            {
                if (ex.Message.ToLower().Contains("your username is required"))
                {
                    code = 20011;
                }
				else if (ex.Message.ToLower().Contains("not all required booking criteria is present or has a value."))
				{
					code = 20025;
				}
				else if (ex.Message.ToLower().Contains("street number is required"))
				{
					code = 20038;
				}
				else if (ex.Message.ToLower().Contains("the street name is invalid"))
				{
					code = 20040;
				}
				else code = 20005;
            }
            else if (ex.InnerException.Message.Contains("(401)"))
            {
                code = 20006;
            }
            else if (ex.InnerException.Message.Contains("(403)"))
            {
                if (ex.Message.ToLower().Contains("you do not have access to this profile."))
                {
                    code = 20024;
                }
                else code = 20007;
            }
            else if (ex.InnerException.Message.Contains("(404)"))
            {
                if (ex.Message.ToLower().Contains("the resource you are looking for might have been removed"))
                {
                    code = 20030;
                }
                else if (ex.Message.ToLower().Contains("no vehicles were found at the pickup point for user's contracted provider"))
                {
					code = 20036;
                }
                else code = 20008;
            }
            else if (ex.InnerException.Message.Contains("(405)"))
            {
                if (ex.Message.ToLower().Contains("the requested resource does not support http method 'put'"))
                {
                    code = 20026;
                }
				else code = 20029;
            }
            else if (ex.InnerException.Message.Contains("(409)"))
            {
                if (ex.Message.ToLower().Contains("you cannot make this reservation because the pickup time is earlier than the current time"))
                {
                    code = 20027;
                }
                else if (ex.Message.ToLower().Contains("your requested url does not match your reservation request content"))
                {
                    code = 20027;
                }
				else code = 20009;
            }
            else if (ex.InnerException.Message.Contains("(500)"))
            {
                code = 20010;
            }
            else
            {
                code = 20004;
            }

            return code;
        }


        private static int BookReservationNowErrorCode(Exception ex)
        {
            int code = 0;


            if (ex.InnerException.Message.Contains("(400)"))
            {
                if (ex.Message.ToLower().Contains("your username is required"))
                {
                    code = 20011;
                }
				else if (ex.Message.ToLower().Contains("not all required booking criteria is present or has a value."))
				{
					code = 20025;
				}
				else if (ex.Message.ToLower().Contains("the street name is invalid"))
				{
					code = 20040;
				}
				else if (ex.Message.ToLower().Contains("street number is required"))
				{
					code = 20038;
				}
				else code = 20005;
            }
            else if (ex.InnerException.Message.Contains("(401)"))
            {
                code = 20006;
            }
            else if (ex.InnerException.Message.Contains("(403)"))
            {
                if (ex.Message.ToLower().Contains("you do not have access to this profile."))
                {
                    code = 20024;
                }
                else code = 20007;
            }
            else if (ex.InnerException.Message.Contains("(404)"))
            {
                if (ex.Message.ToLower().Contains("the resource you are looking for might have been removed"))
                {
                    code = 20030;
                }
                else if (ex.Message.ToLower().Contains("no vehicles were found at the pickup point for user's contracted provider"))
                {
					code = 20035;
                }
                else code = 20008;
            }
            else if (ex.InnerException.Message.Contains("(405)"))
            {
                if (ex.Message.ToLower().Contains("the requested resource does not support http method 'put'"))
                {
                    code = 20026;
                }
				else code = 20029;
            }
            else if (ex.InnerException.Message.Contains("(409)"))
            {
                if (ex.Message.ToLower().Contains("you cannot make this reservation because the pickup time is earlier than the current time"))
                {
                    code = 20027;
                }
                else if (ex.Message.ToLower().Contains("your requested url does not match your reservation request content"))
                {
                    code = 20027;
                }
				else code = 20009;
            }
            else if (ex.InnerException.Message.Contains("(500)"))
            {
                code = 20010;
            }
            else
            {
                code = 20004;
            }

            return code;
        }


        private static int GetReservationErrorCode(Exception ex)
        {
            int code = 0;

            if (ex.InnerException.Message.Contains("(400)"))
            {
                if (ex.Message.ToLower().Contains("your username is required"))
                {
                    code = 20011;
                }
                else code = 20005;
            }
            else if (ex.InnerException.Message.Contains("(401)"))
            {
                code = 20006;
            }
            else if (ex.InnerException.Message.Contains("(403)"))
            {
                if (ex.Message.ToLower().Contains("you do not have access to this profile."))
                {
                    code = 20024;
                }
                else code = 20007;
            }
            else if (ex.InnerException.Message.Contains("(404)"))
            {
                if (ex.Message.ToLower().Contains("the resource you are looking for might have been removed"))
                {
                    code = 20030;
                }
                else code = 20008;
            }
            else if (ex.InnerException.Message.Contains("(405)"))
            {
                code = 20029;
            }
            else if (ex.InnerException.Message.Contains("(409)"))
            {
                code = 20009;
            }
            else if (ex.InnerException.Message.Contains("(500)"))
            {
                code = 20010;
            }
            else
            {
                code = 20004;
            }

            return code;
        }


        private static int ChangeReservationErrorCode(Exception ex)
        {
            int code = 0;


            if (ex.InnerException.Message.Contains("(400)"))
            {
                if (ex.Message.ToLower().Contains("your username is required"))
                {
                    code = 20011;
                }
                else if (ex.Message.ToLower().Contains("not all required booking criteria is present or has a value."))
                {
                    code = 20025;
                }
                else code = 20005;
            }
            else if (ex.InnerException.Message.Contains("(401)"))
            {
                code = 20006;
            }
            else if (ex.InnerException.Message.Contains("(403)"))
            {
                if (ex.Message.ToLower().Contains("you do not have access to this profile."))
                {
                    code = 20024;
                }
                else code = 20007;
            }
            else if (ex.InnerException.Message.Contains("(404)"))
            {
                if (ex.Message.ToLower().Contains("the resource you are looking for might have been removed"))
                {
                    code = 20030;
                }
                else code = 20008;
            }
            else if (ex.InnerException.Message.Contains("(405)"))
            {                
                code = 20029;
            }
            else if (ex.InnerException.Message.Contains("(409)"))
            {
                if (ex.Message.ToLower().Contains("you cannot make this reservation because the pickup time is earlier than the current time"))
                {
                    code = 20027;
                }
                else if (ex.Message.ToLower().Contains("your requested url does not match your reservation request content"))
                {
                    code = 20027;
                }
                code = 20009;
            }
            else if (ex.InnerException.Message.Contains("(500)"))
            {
                code = 20010;
            }
            else
            {
                code = 20004;
            }

            return code;
        }


        private static int CancelReservationErrorCode(Exception ex)
        {
            int code = 0;

            if (ex.InnerException.Message.Contains("(400)"))
            {
                if (ex.Message.ToLower().Contains("your username is required"))
                {
                    code = 20011;
                }
                else code = 20005;
            }
            else if (ex.InnerException.Message.Contains("(401)"))
            {
                code = 20006;
            }
            else if (ex.InnerException.Message.Contains("(403)"))
            {
                if (ex.Message.ToLower().Contains("you do not have access to this profile"))
                {
                    code = 20024;
                }
                else if (ex.Message.ToLower().Contains("the request to cancel this reservation has been denied because the driver has already either been dispatched or assigned"))
                {
                    code = 20034;
                }
                else code = 20007;
            }
            else if (ex.InnerException.Message.Contains("(404)"))
            {
                if (ex.Message.ToLower().Contains("the resource you are looking for might have been removed"))
                {
                    code = 20030;
                }
                else code = 20008;
            }
            else if (ex.InnerException.Message.Contains("(405)"))
            {
                code = 20029;
            }
            else if (ex.InnerException.Message.Contains("(409)"))
            {
                code = 20009;
            }
            else if (ex.InnerException.Message.Contains("(500)"))
            {
                code = 20010;
            }
            else
            {
                code = 20004;
            }

            return code;
        }


        private static int GetBookingCriteriaErrorCode(Exception ex)
        {
            int code = 0;

            if (ex.InnerException.Message.Contains("(400)"))
            {
                if (ex.Message.ToLower().Contains("your username is required"))
                {
                    code = 20011;
                }
                else code = 20005;
            }
            else if (ex.InnerException.Message.Contains("(401)"))
            {
                code = 20006;
            }
            else if (ex.InnerException.Message.Contains("(403)"))
            {
                if (ex.Message.ToLower().Contains("you do not have access to this profile."))
                {
                    code = 20024;
                }
                else code = 20007;
            }
            else if (ex.InnerException.Message.Contains("(404)"))
            {
                if (ex.Message.ToLower().Contains("the resource you are looking for might have been removed"))
                {
                    code = 20030;
                }
                else code = 20008;
            }
            else if (ex.InnerException.Message.Contains("(405)"))
            {
                code = 20029;
            }
            else if (ex.InnerException.Message.Contains("(409)"))
            {
                code = 20009;
            }
            else if (ex.InnerException.Message.Contains("(500)"))
            {
                code = 20010;
            }
            else
            {
                code = 20004;
            }

            return code;
        }


        private static int GetBookingCriteriaNowErrorCode(Exception ex)
        {
            int code = 0;

            if (ex.InnerException.Message.Contains("(400)"))
            {
                if (ex.Message.ToLower().Contains("your username is required"))
                {
                    code = 20011;
                }
                else code = 20005;
            }
            else if (ex.InnerException.Message.Contains("(401)"))
            {
                code = 20006;
            }
            else if (ex.InnerException.Message.Contains("(403)"))
            {
                if (ex.Message.ToLower().Contains("you do not have access to this profile."))
                {
                    code = 20024;
                }
                else code = 20007;
            }
            else if (ex.InnerException.Message.Contains("(404)"))
            {
                if (ex.Message.ToLower().Contains("the resource you are looking for might have been removed"))
                {
                    code = 20030;
                }
                else code = 20008;
            }
            else if (ex.InnerException.Message.Contains("(405)"))
            {
                code = 20029;
            }
            else if (ex.InnerException.Message.Contains("(409)"))
            {
                code = 20009;
            }
            else if (ex.InnerException.Message.Contains("(500)"))
            {
                code = 20010;
            }
            else
            {
                code = 20004;
            }

            return code;
        }


        private static int GetCriteriaValidationErrorCode(Exception ex)
        {
            int code = 0;

            if (ex.InnerException.Message.Contains("(400)"))
            {
                if (ex.Message.ToLower().Contains("your username is required"))
                {
                    code = 20011;
                }
                else code = 20005;
            }
            else if (ex.InnerException.Message.Contains("(401)"))
            {
                code = 20006;
            }
            else if (ex.InnerException.Message.Contains("(403)"))
            {
                if (ex.Message.ToLower().Contains("you do not have access to this profile."))
                {
                    code = 20024;
                }
                else code = 20007;
            }
            else if (ex.InnerException.Message.Contains("(404)"))
            {
                if (ex.Message.ToLower().Contains("the resource you are looking for might have been removed"))
                {
                    code = 20030;
                }
                else code = 20008;
            }
            else if (ex.InnerException.Message.Contains("(405)"))
            {
                code = 20029;
            }
            else if (ex.InnerException.Message.Contains("(409)"))
            {
                code = 20009;
            }
            else if (ex.InnerException.Message.Contains("(500)"))
            {
                code = 20010;
            }
            else
            {
                code = 20004;
            }

            return code;
        }


        private static int GetVehicleLocationErrorCode(Exception ex)
        {
            int code = 0;

            if (ex.InnerException.Message.Contains("(400)"))
            {
                if (ex.Message.ToLower().Contains("your username is required"))
                {
                    code = 20011;
                }
                else code = 20005;
            }
            else if (ex.InnerException.Message.Contains("(401)"))
            {
                code = 20006;
            }
            else if (ex.InnerException.Message.Contains("(403)"))
            {
                if (ex.Message.ToLower().Contains("you do not have access to this profile."))
                {
                    code = 20024;
                }
                else code = 20007;
            }
            else if (ex.InnerException.Message.Contains("(404)"))
            {
                if (ex.Message.ToLower().Contains("the resource you are looking for might have been removed"))
                {
                    code = 20030;
                }
                else code = 20008;
            }
            else if (ex.InnerException.Message.Contains("(405)"))
            {
                code = 20029;
            }
            else if (ex.InnerException.Message.Contains("(409)"))
            {
                code = 20009;
            }
            else if (ex.InnerException.Message.Contains("(500)"))
            {
                code = 20010;
            }
            else
            {
                code = 20004;
            }

            return code;
        }


        private static int GetAvailableVehiclesErrorCode(Exception ex)
        {
            int code = 0;

            if (ex.InnerException.Message.Contains("(400)"))
            {
                code = 20005;
            }
            else if (ex.InnerException.Message.Contains("(401)"))
            {
                code = 20006;
            }
            else if (ex.InnerException.Message.Contains("(403)"))
            {
                if (ex.Message.ToLower().Contains("you do not have access to this profile."))
                {
                    code = 20024;
                }
                else code = 20007;
            }
            else if (ex.InnerException.Message.Contains("(404)"))
            {
                if (ex.Message.ToLower().Contains("the resource you are looking for might have been removed"))
                {
                    code = 20030;
                }
                else code = 20017;                
            }
            else if (ex.InnerException.Message.Contains("(405)"))
            {
                code = 20029;
            }
            else if (ex.InnerException.Message.Contains("(409)"))
            {
                code = 20009;
            }
            else if (ex.InnerException.Message.Contains("(500)"))
            {
                code = 20010;
            }
            else
            {
                code = 20004;
            }

            return code;
        }


        private static int GetUserProfilesErrorCode(Exception ex)
        {
            int code = 0;

            if (ex.InnerException.Message.Contains("(400)"))
            {
                code = 20005;
            }
            else if (ex.InnerException.Message.Contains("(401)"))
            {
                code = 20006;
            }
            else if (ex.InnerException.Message.Contains("(403)"))
            {
                if (ex.Message.ToLower().Contains("you do not have access to this profile."))
                {
                    code = 20024;
                }
                else code = 20007;
            }
            else if (ex.InnerException.Message.Contains("(404)"))
            {
                if (ex.Message.ToLower().Contains("the resource you are looking for might have been removed"))
                {
                    code = 20030;
                }
                else code = 20008;
            }
            else if (ex.InnerException.Message.Contains("(405)"))
            {
                code = 20029;
            }
            else if (ex.InnerException.Message.Contains("(409)"))
            {
                code = 20009;
            }
            else if (ex.InnerException.Message.Contains("(500)"))
            {
                code = 20010;
            }
            else
            {
                code = 20004;
            }

            return code;
        }


        private static int VerifyUserErrorCode(Exception ex)
        {
            int code = 0;


            if (ex.InnerException.Message.Contains("(400)"))
            {
                if (ex.Message.ToLower().Contains("your username is required"))
                {
                    code = 20011;
                }
                else if (ex.Message.ToLower().Contains("your password is required"))
                {
                    code = 20012;
                }
                else if (ex.Message.ToLower().Contains("the username cannot exceed 64 characters"))
                {
                    code = 20013;
                }
                else code = 20005;
            }
            else if (ex.InnerException.Message.Contains("(401)"))
            {
                code = 20006;
            }
            else if (ex.InnerException.Message.Contains("(403)"))
            {
                if (ex.Message.ToLower().Contains("you do not have access to this profile."))
                {
                    code = 20024;
                }
                else code = 20007;
            }
            else if (ex.InnerException.Message.Contains("(404)"))
            {
                if (ex.Message.ToLower().Contains("the resource you are looking for might have been removed"))
                {
                    code = 20030;
                }
                else code = 20008;
            }
            else if (ex.InnerException.Message.Contains("(405)"))
            {
                code = 20029;
            }
            else if (ex.InnerException.Message.Contains("(409)"))
            {
                code = 20009;
            }
            else if (ex.InnerException.Message.Contains("(500)"))
            {
                code = 20010;
            }
            else
            {
                code = 20004;
            }

            return code;
        }


        private static int RegisterUserErrorCode(Exception ex)
        {
            int code = 0;

            if (ex.InnerException.Message.Contains("(400)"))
            {
                if (ex.Message.ToLower().Contains("your username is required"))
                {
                    code = 20011;
                }
                else if (ex.Message.ToLower().Contains("your password is required"))
                {
                    code = 20012;
                }
                else if (ex.Message.ToLower().Contains("the username cannot exceed 64 characters"))
                {
                    code = 20013;
                }
                else if (ex.Message.ToLower().Contains("the password cannot exceed 32 characters"))
                {
                    code = 20014;
                }
                else code = 20005;
            }
            else if (ex.InnerException.Message.Contains("(401)"))
            {
                code = 20006;
            }
            else if (ex.InnerException.Message.Contains("(403)"))
            {
                if (ex.Message.ToLower().Contains("you do not have access to this profile."))
                {
                    code = 20024;
                }
                else code = 20007;
            }
            else if (ex.InnerException.Message.Contains("(404)"))
            {
                if (ex.Message.ToLower().Contains("the resource you are looking for might have been removed"))
                {
                    code = 20030;
                }
                else code = 20008;
            }
            else if (ex.InnerException.Message.Contains("(405)"))
            {
                code = 20029;
            }
            else if (ex.InnerException.Message.Contains("(409)"))
            {
                code = 20018;
            }
            else if (ex.InnerException.Message.Contains("(500)"))
            {
                code = 20010;
            }
            else
            {
                code = 20004;
            }

            return code;
        }


        private static int ChangeUserPasswordErrorCode(Exception ex)
        {
            int code = 0;

            if (ex.InnerException.Message.Contains("(400)"))
            {
                if (ex.Message.ToLower().Contains("your username is required"))
                {
                    code = 20011;
                }
                else if (ex.Message.ToLower().Contains("your password is required"))
                {
                    code = 20012;
                }
                else if (ex.Message.ToLower().Contains("the username cannot exceed 64 characters"))
                {
                    code = 20013;
                }
                else if (ex.Message.ToLower().Contains("the password cannot exceed 32 characters"))
                {
                    code = 20014;
                }
                else if (ex.Message.ToLower().Contains("your new password is required."))
                {
                    code = 20016;
                }
                else code = 20005;
            }
            else if (ex.InnerException.Message.Contains("(401)"))
            {
                code = 20006;
            }
            else if (ex.InnerException.Message.Contains("(403)"))
            {
                if (ex.Message.ToLower().Contains("you do not have access to this profile."))
                {
                    code = 20024;
                }
                else code = 20007;
            }
            else if (ex.InnerException.Message.Contains("(404)"))
            {
                if (ex.Message.ToLower().Contains("the resource you are looking for might have been removed"))
                {
                    code = 20030;
                }
                else code = 20008;
            }
            else if (ex.InnerException.Message.Contains("(405)"))
            {
                code = 20029;
            }
            else if (ex.InnerException.Message.Contains("(409)"))
            {
                code = 20019;
            }
            else if (ex.InnerException.Message.Contains("(500)"))
            {
                code = 20010;
            }
            else
            {
                code = 20004;
            }

            return code;
        }


        private static int DeleteUserErrorCode(Exception ex)
        {
            int code = 0;

            if (ex.InnerException.Message.Contains("(400)"))
            {
                if (ex.Message.ToLower().Contains("your username is required"))
                {
                    code = 20011;
                }                
                else if (ex.Message.ToLower().Contains("the username cannot exceed 64 characters"))
                {
                    code = 20013;
                }
                else code = 20005;
            }
            else if (ex.InnerException.Message.Contains("(401)"))
            {
                code = 20006;
            }
            else if (ex.InnerException.Message.Contains("(403)"))
            {
                if (ex.Message.ToLower().Contains("you do not have access to this profile."))
                {
                    code = 20024;
                }
                else code = 20007;
            }
            else if (ex.InnerException.Message.Contains("(404)"))
            {
                if (ex.Message.ToLower().Contains("the resource you are looking for might have been removed"))
                {
                    code = 20030;
                }
                else code = 20008;
            }
            else if (ex.InnerException.Message.Contains("(405)"))
            {
                code = 20029;
            }
            else if (ex.InnerException.Message.Contains("(409)"))
            {
                code = 20009;
            }
            else if (ex.InnerException.Message.Contains("(500)"))
            {
                code = 20010;
            }
            else
            {
                code = 20004;
            }


            return code;
        }

        
        private static int GetCorporateAccountKeyErrorCode(Exception ex)
        {
            //This is a general error code for all SDS exceptions. The error code returned can be refined further after discussions with the SDS team.
            return 20020;
        }


        private static int GetAvailableFleetsErrorCode(Exception ex)
        {
            int code = 0;

            if (ex.InnerException.Message.Contains("(400)"))
            {
                code = 20005;
            }
            else if (ex.InnerException.Message.Contains("(401)"))
            {
                code = 20006;
            }
            else if (ex.InnerException.Message.Contains("(403)"))
            {
                if (ex.Message.ToLower().Contains("you do not have access to this profile."))
                {
                    code = 20024;
                }
                else code = 20007;
            }
            else if (ex.InnerException.Message.Contains("(404)"))
            {
                if (ex.Message.ToLower().Contains("the resource you are looking for might have been removed"))
                {
                    code = 20030;
                }
                else code = 20008;
            }
            else if (ex.InnerException.Message.Contains("(405)"))
            {
                code = 20029;
            }
            else if (ex.InnerException.Message.Contains("(409)"))
            {
                code = 20009;
            }
            else if (ex.InnerException.Message.Contains("(500)"))
            {
                code = 20010;
            }
            else
            {
                code = 20004;
            }

            return code;
        }


        private static int BuildReservationErrorCode(Exception ex)
        {
            int code = 0;

            if (ex.Message.Contains("The specified pickup address type is not supported."))
            {
                code = 20031;
            }
            else if (ex.Message.Contains("Unable to specify a required booking parameter."))
            {
                code = 20033;
            }
            else
            {
                code = 20004;
            }

            return code;
        }


		private static int BuildReservationBookNowErrorCode(Exception ex)
		{
			int code = 0;

			if (ex.Message.Contains("The specified pickup address type is not supported."))
			{
				code = 20031;
			}
			else if (ex.Message.Contains("Unable to specify a required booking parameter."))
			{
				code = 20033;
			}
			else
			{
				code = 20004;
			}

			return code;
		}

		private static int BuildModifyBookReservationErrorCode(Exception ex)
		{
			int code = 0;

			if (ex.Message.Contains("Not all required booking criteria is present or has a value"))
			{
				code = 20039;
			}
			else
			{
				code = 20004;
			}

			return code;
		}
        private static int BuildCorporatePaymentCardErrorCode(Exception ex)
        {
            int code = 1;

            if (!string.IsNullOrEmpty(ex.Message))
            {
                var exceptionObjects = ex.Message.JsonDeserialize<List<AlephErrorHandler>>();
                if (exceptionObjects != null && exceptionObjects.Count > 0)
                {
                    var exception = exceptionObjects.FirstOrDefault();
                    if (exception != null)
                    {
                        switch (exception.error_code)
                        {
                            case "81724":
                            case "7":
                                code = 20041;
                                break;
                            case "2005":
                            case "2008":
                            case "2051":
                            case "81714":
                            case "81715":
                            case "81716":
                            case "81717":
                                code = 20042;
                                break;
                            case "2010":
                            case "81706":
                            case "81707":
                            case "81736":
                                code = 20043;
                                break;
                            case "2004":
                            case "2006":
                            case "81709":
                            case "81710":
                            case "81711":
                            case "81712":
                            case "81713":
                                code = 20044;
                                break;
                            case "81509":
                            case "2024":
                            case "81703":
                            case "91726":
                            case "91734":
                                code = 20045;
                                break;
                            case "2":
                                code = 20047;
                                break;
                            case "4":
                            case "6":
                                code = 20048;
                                break;
                            case "10":
                                code = 20049;
                                break;
                            default:
                                code = 20046;
                                break;
                        }
                    }
                }
            }

            return code;
        }


    }
}
