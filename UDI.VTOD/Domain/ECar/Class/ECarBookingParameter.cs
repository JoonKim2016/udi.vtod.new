﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.DataAccess.VTOD;
using UDI.VTOD.Domain.Taxi.Const;

namespace UDI.VTOD.Domain.ECar.Class
{
	public class ECarBookingParameter
	{
        public ECarBookingParameter()
		{
			this.EnableResponseAndLog = true;
            this.PickupAddressOnlyContainsLatAndLong = false;
            this.DropOffAddressOnlyContainsLatAndLong = false;
		}

		public TokenRS tokenVTOD { set; get; }
		public OTA_GroundBookRQ request { set; get; }

		public ecar_fleet Fleet { set; get; }

		public long serviceAPIID { set; get; }

        public int Fleet_TripCode { set; get; }

        public double FixedPrice { set; get; }
        public string FirstName { set; get; }
		public string LastName { set; get; }
		public string PhoneNumber { set; get; }
        public string emailAddress { get; set; }

		public ecar_fleet_user User { get; set; }
        public vtod_trip Trip = new vtod_trip();

		public DateTime PickupDateTime { set; get; }
		public DateTime? DropOffDateTime { set; get; }

        public bool PickupAddressOnlyContainsLatAndLong { set; get; }
        public bool DropOffAddressOnlyContainsLatAndLong { set; get; }
		public string PickupAddressType { set; get; }
		public Map.DTO.Address PickupAddress { set; get; }
        public List<Map.DTO.Address> Stops { get; set; }
		public string PickupAirport { set; get; }
		public double LongitudeForPickupAirport { set; get; }
		public double LatitudeForPickupAirport { set; get; }

		public string DropOffAddressType { set; get; }
		public Map.DTO.Address DropOffAddress { set; get; }
		public string DropoffAirport { set; get; }
		public double LongitudeForDropoffAirport { set; get; }
		public double LatitudeForDropoffAirport { set; get; }
        public string EmailAddress { get; set; }
        public bool EnableResponseAndLog { set; get; }

		public int? CreditCardID { get; set; }

		public int? MemberID { get; set; }

		public int? DirectBillAccountID { get; set; }

		public string Gratuity { get; set; }
		public string Rate { get; set; }

		public Common.DTO.Enum.PaymentType PaymentType { get; set; }

        public bool IsFixedPrice { set; get; }
        public bool PickupNow { set; get; }
        public string AirportPickupPoint { get; set; }

        public string DispatchCode {set;get;}
        public string ConsumerCode { set; get; }
        public string ConsumerConfirmationID { set; get; }

        public ecar_fleet_servicearea_relation Relation { set; get; }

		public string Source { get; set; }
		public string Device { get; set; }
		public string Ref { get; set; }


        public string PickupAirlineFlightNumber { get; set; }
        public string PickupAirlineCode { get; set; }
        public string PickupAirlineCodeContext { get; set; }
        public string PickupAirlineFlightDateTime { get; set; }


        public string DropOffAirlineFlightNumber { get; set; }
        public string DropOffAirlineCode { get; set; }
        public string DropOffAirlineCodeContext { get; set; }
        public string DropOffAirlineFlightDateTime { get; set; }

        public int numberOfPassenger { set; get; }

        public string controllerNotes { set; get; }


        public string commission { set; get; }
      
        public int PromisedETA { set; get; }

        public string BookingWebServiceEndpointName { set; get; }
        public string AuthenticationServiceEndpointName { set; get; }
        public string AddressWebServiceEndpointName { set; get; }
        public string OsiWebServiceEndpointName { set; get; }

        public string RemarkForPickup { set; get; }

        public string RemarkForDropOff { set; get; }

        public int NumberOfPassenger { set; get; }
        public string GratuityType { get; set; }
    }
}
