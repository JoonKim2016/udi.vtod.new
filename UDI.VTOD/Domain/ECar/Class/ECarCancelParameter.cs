﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.DataAccess.VTOD;

namespace UDI.VTOD.Domain.ECar.Class
{
    public class ECarCancelParameter
    {
		public ECarCancelParameter()
		{
			this.EnableResponseAndLog = true;
		}

        public TokenRS tokenVTOD { set; get; }
        public OTA_GroundCancelRQ request { set; get; }

        public ecar_fleet Fleet { set; get; }

		public long serviceAPIID { set; get; }

        //public taxi_customer Customer { get; set; }
        //public string FirstName { set; get; }
        //public string LastName { set; get; }
        //public string PhoneNumber { set; get; }



        public ecar_fleet_user User { get; set; }
        public vtod_trip Trip = new vtod_trip();

		public bool EnableResponseAndLog { set; get; }

        public string BookingWebServiceEndpointName { set; get; }
        public string AuthenticationServiceEndpointName { set; get; }
        public string AddressWebServiceEndpointName { set; get; }
        public string OsiWebServiceEndpointName { set; get; }
    }
}
