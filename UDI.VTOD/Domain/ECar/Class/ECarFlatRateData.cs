﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.DataAccess.VTOD;

namespace UDI.VTOD.Domain.ECar.Class
{
	public class ECarFlatRateData: ecar_fleet_zone_flatrate
	{
	    public string PickupZoneName { set; get; }
        public string DropOffZoneName { set; get; }
    }
}
