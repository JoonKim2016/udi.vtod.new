﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.DataAccess.VTOD;
using UDI.VTOD.Domain.Taxi.Const;

namespace UDI.VTOD.Domain.ECar.Class
{
    public class ECarGetEstimationParameter : ECarGroundAvailParameter
    {

		public ECarGetEstimationParameter()
		{
			EnableResponseAndLog = true;
		}

        //common
		public string EstimationType { set; get; }
        public bool EnableResponseAndLog { set; get; }

        //public OTA_GroundAvailRQ request { set; get; }
        //public TokenRS tokenVTOD { set; get; }
        //public ecar_fleet Fleet { set; get; }
        //public long serviceAPIID { set; get; }
        //public string PickupAddressType { set; get; }
        //public Map.DTO.Address PickupAddress { set; get; }
        //public string PickupAirport { set; get; }
		

        //Address or airportInfo
		public bool PickupAddressOnlyContainsLatAndLong { set; get; }
		public bool DropOffAddressOnlyContainsLatAndLong { set; get; }

        public Map.DTO.Address DropOffAddress { set; get; }

        public double LongitudeForPickupAirport { set; get; }
		public double LatitudeForPickupAirport { set; get; }


        public DateTime PickupDateTime { set; get; }

		public string DropOffAddressType { set; get; }
		
		public string DropoffAirport { set; get; }
	
		public double LongitudeForDropoffAirport { set; get; }
		public double LatitudeForDropoffAirport { set; get; }

        //Zone
        //public ecar_fleet_zone PickupZone { set; get; }
        public ecar_fleet_zone DropoffZone { set; get; }

       
        //response
        public ecar_fleet_rate Info_Rate { set; get; }
        public List<ECarFlatRateData> Info_FlatRates { set; get; }
        public RateData RateEstimation { set; get; }
        public FlatRateData FlatRateEstimation { set; get; }

        //distance
        public decimal Distance_Mile { set; get; }
        public decimal Distance_Kilometer { set; get; }



		public string Gratuity { get; set; }

        //user
        public ecar_fleet_user User { get; set; }
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string PhoneNumber { set; get; }
        public string Email { set; get; }
        public string BookingWebServiceEndpointName { set; get; }
        public string AuthenticationServiceEndpointName { set; get; }
        public string AddressWebServiceEndpointName { set; get; }
        public string OsiWebServiceEndpointName { set; get; }
        public ecar_fleet_zone PickupZone { set; get; }
    }
}
