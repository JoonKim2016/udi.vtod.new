﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.DataAccess.VTOD;

namespace UDI.VTOD.Domain.ECar.Class
{
    public class ECarGetVehicleInfoParameter : ECarGroundAvailParameter
    {
		public ECarGetVehicleInfoParameter()
		{
			this.EnableResponseAndLog = true;
		}

        public bool EnableResponseAndLog { set; get; }
        
        public string PickupLandmark { set; get; }

		public List<Vehicle> VehicleStausList = new List<Vehicle>();
        public double Longtitude { set; get; }
        public double Latitude { set; get; }
        public string PolygonType { set; get; } //Circle|Retengle

        //Circle
        public double Radius { set; get; }


        //public List<ecar_fleet> fleetList = new List<ecar_fleet>();
        //public List<long> serviceAPIIDList = new List<long>();

        public string APIInformation { set; get; }


        /// <summary>
        /// It stores the the Disability Info of the user.</summary>
      
        
        public string VehicleStatus { set; get; }

        public string AuthenticationServiceEndpointName { set; get; }
        public string BookingWebServiceEndpointName { set; get; }
        public string OsiWebServiceEndpointName { set; get; }
        public string AddressWebServiceEndpointName { set; get; }

        public bool DisabilityVehicleInd { set; get; }

    }

    
}
