﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.DataAccess.VTOD;

namespace UDI.VTOD.Domain.ECar.Class
{
    public abstract class ECarGroundAvailParameter
    {
        public OTA_GroundAvailRQ request { set; get; }
        public TokenRS tokenVTOD { set; get; }
        public ecar_fleet Fleet { set; get; }
        public long serviceAPIID { set; get; }
        public long? extendedServiceAPIID { set; get; }
        public string PickupAddressType { set; get; }
        public Map.DTO.Address PickupAddress { set; get; }
        public string PickupAirport { set; get; }
        
        public List<ecar_fleet> fleetList = new List<ecar_fleet>();
        public List<long> serviceAPIIDList = new List<long>();
    }
}
