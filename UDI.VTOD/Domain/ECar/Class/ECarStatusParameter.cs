﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.DataAccess.VTOD;

namespace UDI.VTOD.Domain.ECar.Class
{
    public class ECarStatusParameter
    {
		public ECarStatusParameter()
		{
			this.EnableResponseAndLog = true;
		}

        public TokenRS tokenVTOD { set; get; }
        public OTA_GroundResRetrieveRQ request { set; get; }

        public ecar_fleet Fleet { set; get; }

		public long serviceAPIID { set; get; }

        public ecar_fleet_user Fleet_User { get; set; }
        public vtod_trip Trip = new vtod_trip();
        public ecar_trip_status lastTripStatus {set;get;}
		public bool EnableResponseAndLog { set; get; }

		public my_aspnet_users User { set; get; }

        public string BookingWebServiceEndpointName { set; get; }
        public string AuthenticationServiceEndpointName { set; get; }
        public string AddressWebServiceEndpointName { set; get; }
        public string OsiWebServiceEndpointName { set; get; }
    }
}
