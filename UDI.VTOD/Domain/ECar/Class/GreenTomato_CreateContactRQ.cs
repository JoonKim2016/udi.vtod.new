﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.ECar.Class
{
	public class GreenTomato_CreateContactRQ
	{
        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
		public string id { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Include)]
		public string firstName { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Include)]
		public string surname { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Include)]
		public string customerAccountNumber { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Include)]
		public string email { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Include)]
		public string mobilePhone { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Include)]
		public string landlinePhone { get; set; }
	}
}
