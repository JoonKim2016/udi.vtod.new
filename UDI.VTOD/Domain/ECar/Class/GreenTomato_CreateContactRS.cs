﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.ECar.Class
{
	public class GreenTomato_CreateContactRS
	{
        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
		public string success { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Include)]
		public List<string> errors { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Include)]
		public List<GreenTomato_contact> rows { get; set; }
	}
}
