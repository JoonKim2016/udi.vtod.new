﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.ECar.Class
{
	public class GreenTomato_ServiceListRQ
	{
        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
		public string startIndex { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Include)]
		public string count { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Include)]
		public string customerAccountNumber { get; set; }
	}
}
