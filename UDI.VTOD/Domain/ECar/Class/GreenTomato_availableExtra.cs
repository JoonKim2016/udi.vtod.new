﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.ECar.Class
{
	public class GreenTomato_availableExtra
	{
        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
		public string id { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Include)]
		public string name { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Include)]
		public string value { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string enable { get; set; }
	}
}
