﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.ECar.Class
{
	public class GreenTomato_error
	{
        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
		public string message { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Include)]
		public string type { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Include)]
		public string stack { get; set; }
	}
}
