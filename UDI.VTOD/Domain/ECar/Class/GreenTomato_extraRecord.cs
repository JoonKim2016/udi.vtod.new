﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.ECar.Class
{
    public class GreenTomato_extraRecord
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public long id { set; get; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string name{ set; get; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string value{ set; get; }
        
    }
}
