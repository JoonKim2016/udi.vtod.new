﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UDI.VTOD.Domain.ECar.Class
{
    public class GreenTomato_passengerRecord
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string id { set; get; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string fullName { set; get; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string email { set; get; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string mobilePhone { set; get; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string landlinePhone { set; get; }
    }
}
