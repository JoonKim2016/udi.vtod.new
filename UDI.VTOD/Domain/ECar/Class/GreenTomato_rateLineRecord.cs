﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.ECar.Class
{
	public class GreenTomato_rateLineRecord
	{
		[JsonProperty(NullValueHandling = NullValueHandling.Include)]
		public string templateId { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Include)]
		public string rate { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Include)]
		public string units { get; set; }
	}
}
