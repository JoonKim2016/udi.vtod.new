﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UDI.VTOD.Domain.ECar.Class
{
    public class GreenTomato_referenceRecord
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string type { set; get; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string value { set; get; }
    }
}
