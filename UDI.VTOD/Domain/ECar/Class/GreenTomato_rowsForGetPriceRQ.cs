﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.ECar.Class
{
    public class GreenTomato_rowsForGetPriceRQ
	{
        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
		public string totalPrice { get; set; }
	}
}
