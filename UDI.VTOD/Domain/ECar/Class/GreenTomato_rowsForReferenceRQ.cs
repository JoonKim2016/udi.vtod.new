﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.ECar.Class
{
    public class GreenTomato_rowsForReferenceRQ
	{
        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
		public string name { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string description { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string mandatory { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string templateFrom { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string templateTo { get; set; }
	}
}
