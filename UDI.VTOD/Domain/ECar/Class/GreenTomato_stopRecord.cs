﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.ECar.Class
{
	public class GreenTomato_stopRecord
	{
        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
		public string address { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
		public string type { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string postcode { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string latitude { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string longitude { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string specialPlaceId { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string specialPlaceType { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string specialPlacePointId { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string flightNumber { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string scheduledLandingDate { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string holdOffTimeInMinutes { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string airportMeetingType { get; set; }
	}
}
