﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.ECar.Class
{
    public class RateData
    {
        public const string Code = "Rate";
        public string DistanceUnit { set; get; }
        public double Distance { set; get; }
        public string CurrencyMode { set; get; }
        public double EstimationTotalAmount { set; get; }
    }
}
