﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.ECar.Class
{
    public class ecar_fleet_rate
    {
        public long FleetId { set; get; }
        public double InitialFreeDistance { set; get; }
        public double InitialAmount { set; get; }
        public double PerDistanceAmount { set; get; }
        public string DistanceUnit { set; get; }
        public string AmountUnit { set; get; }
        public DateTime AppendTime { set; get; }
    }
}
