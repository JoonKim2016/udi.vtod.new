﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.ECar.Const
{
    public class ECarServiceAPIConst
    {
        public const long None = 0;
        public const long GreenTomato = 1;
        public const long Aleph = 2;
        public const long Texas = 3;
    }
}
