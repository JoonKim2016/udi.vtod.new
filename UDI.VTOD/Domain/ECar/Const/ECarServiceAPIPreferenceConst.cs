﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.ECar.Const
{
	public class ECarServiceAPIPreferenceConst
	{
		public const string Book = "Book";
        public const string Cancel = "Cancel";
        public const string Status = "Status";
		public const string GetVehicleInfo = "GetVehicleInfo";
		public const string GetEstimation = "GetEstimation";
        public const string NotifyDriver = "NotifyDriver";
        public const string GetAvailableFleets = "GetAvailableFleets";
        public const string GetFinalRoute = "GetFinalRoute";
    }
}
