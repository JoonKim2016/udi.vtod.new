﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.ECar.Const
{
    public class EstimationResultType
    {
        public const string Info_FlatRate = "Info_FlatRate";
        public const string Info_Rate = "Info_Rate";
        public const string Info = "Info";
    }
}
