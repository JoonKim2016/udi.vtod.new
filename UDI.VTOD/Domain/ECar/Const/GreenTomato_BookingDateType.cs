﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.ECar.Const
{
	public class GreenTomato_BookingDateType
	{
		public const string ASAP = "ASAP";
		public const string PICKUP = "PICKUP";
	}
}
