﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.ECar.Const
{
    public class PickMeUpNowOptionConst
    {
        public const int Anytime = 1;
        public const int DontPickMeUp = 0;
        public const int OnlyNow = 2;
        public const int OnlyFuture = 3;
    }
}
