﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.ECar.Const
{
	public class ECarTripStatusType
	{
        //And we will use the status from UDI33
		public const string Booked = "Booked";
		public const string FastMeter = "FastMeter";
		public const string DispatchPending = "DispatchPending";
		public const string Canceled = "Canceled";
		public const string Accepted = "Accepted";
		public const string Assigned = "Assigned";
		public const string InTransit = "InTransit";
		public const string InService = "InService";
		public const string MeterON = "MeterOn";
		public const string NoShow = "NoShow";
		public const string Matched = "Matched";
		public const string UnMatched = "UnMatched";
		public const string Completed = "Completed";
		public const string Fare = "Fare";
		//public const string Charged = "Charged";
		//public const string UnCharged = "UnCharged";
		public const string Error = "Error";
		public const string MeterOff= "MeterOff";
        public const string Arrived = "Arrived";
        public const string Offered = "Offered";
        public const string PickUp = "PickUp";
		public const string DropOff = "DropOff";
		public const string TripNotFound = "TripNotFound";
        public const string ModifyBooked = "ModifyBooked";
        //CCSi
        //AcceptedByDriver ---> Accepted
        //OnSite-->Arrived
        //AssignedByProvider--> Assigned
        //maybe GPS exists in transit
        public const string Closed = "Closed";
        public const string Updated = "Updated";
        public const string GPS = "GPS";

        //Defined
        public const string Unknown = "Unknown";

		
	}
}
