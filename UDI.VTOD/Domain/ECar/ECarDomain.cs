﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.Helper;
using UDI.VTOD.Common.Track;
using UDI.VTOD.Domain.ECar.Class;
using UDI.VTOD.Domain.ECar.Const;
using UDI.VTOD.Domain.ECar.GreenTomato;
using UDI.Utility.Helper;
using UDI.VTOD.Domain.ECar.Aleph;
using UDI.VTOD.Domain.ECar.MTData_Derived;
using UDI.VTOD.DataAccess.VTOD;
namespace UDI.VTOD.Domain.ECar
{
	public class ECarDomain : BaseSetting
	{
        #region Constructors
        internal ECarDomain()
		{
			OwnerType = LogOwnerType.ECar;            
        }
        #endregion

        #region private
        private IECarService GetServiceAPI(long serviceAPIID, TokenRS tokenVTOD,out int FleetTripCode)
        {
            IECarService service = null;
            ECarUtility utility = new ECarUtility(this.logger);
            if (serviceAPIID == ECarServiceAPIConst.GreenTomato)
            {
                service = new GreenTomatoECarService(utility, logger);
                FleetTripCode = UDI.VTOD.Common.DTO.Enum.FleetTripCode.Ecar_GreenTomato;
            }
            else if (serviceAPIID == ECarServiceAPIConst.Aleph)
            {
                service = new AlephECarService(utility, logger);
                FleetTripCode = UDI.VTOD.Common.DTO.Enum.FleetTripCode.Ecar_Aleph;
            }
        
            else if (serviceAPIID == ECarServiceAPIConst.Texas)
            {
                service = new TexasEcarService(utility, logger);
                FleetTripCode = UDI.VTOD.Common.DTO.Enum.FleetTripCode.Ecar_Texas;
            }
            else
            {
                string error = Messages.ECar_Common_NoServiceAPIPreference;
                logger.Error(error);
                throw new Exception(error);
            }
            return service;
        }
        #endregion


        #region public

        public OTA_GroundAvailRS GetVehicleInfo(TokenRS tokenVTOD, OTA_GroundAvailRQ request)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			OTA_GroundAvailRS response = null;
            int fleetTripCode = 0;

            try
			{			
				ECarUtility utility = new ECarUtility(logger);
				ECarGetVehicleInfoParameter ecgvip = (ECarGetVehicleInfoParameter)utility.InitiateECarParameter(request, ECarServiceAPIPreferenceConst.GetVehicleInfo);
				IECarService service = this.GetServiceAPI(ecgvip.serviceAPIID, tokenVTOD, out fleetTripCode);
				response = service.GetVehicleInfo(tokenVTOD, request, ecgvip);			
			}
			catch (Exception ex)
			{				
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw;
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return response;
		}
		public OTA_GroundResRetrieveRS Status(TokenRS tokenVTOD, OTA_GroundResRetrieveRQ request)
		{
			//NotifyDriver(tokenVTOD, new UtilityNotifyDriverRQ { Message = "Hello world", TripID = 311 }, null);
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
            int fleettripcode=0;
            sw.Start();
			OTA_GroundResRetrieveRS response = null;
			try
			{
                ECarUtility utility = new ECarUtility(logger);
                                           
                var eCarStatusParameter = utility.InitiateECarParameter(request, tokenVTOD);
                var service = this.GetServiceAPI(eCarStatusParameter.serviceAPIID, tokenVTOD, out fleettripcode);
                response = service.Status(tokenVTOD, request, eCarStatusParameter);
            }
			catch (Exception ex)
			{				
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw;
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return response;
		}

		public OTA_GroundBookRS Book(TokenRS tokenVTOD, OTA_GroundBookRQ request, out List<Tuple<string, int, long>> disptach_Rez_VTOD,out int fleetTripCode)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			OTA_GroundBookRS response = null;
            disptach_Rez_VTOD = new List<Tuple<string, int, long>>();
            fleetTripCode = 0;
            try
			{   
				ECarUtility utility = new ECarUtility(logger);
				ECarBookingParameter ecbp = utility.InitiateECarParameter(request);
                IECarService service = this.GetServiceAPI(ecbp.serviceAPIID, tokenVTOD, out fleetTripCode);
                ecbp.Fleet_TripCode = fleetTripCode; 
                if (fleetTripCode == FleetTripCode.Ecar_Texas && request.Payments != null && request.Payments.Payments != null && request.Payments.Payments.Any() && request.Payments.Payments.First().Cash != null)
                {
                    var vtodException = VtodException.CreateException(ExceptionType.ECar, 2029);
                    throw vtodException;
                }
                else
                {
                    response = service.Book(tokenVTOD, request, out disptach_Rez_VTOD, ecbp);
                }
			
				#region [Ecar 1.2] Link  disptach_VTOD_TripIDs (for supershuttel system)
				if (response.Reservations != null && response.Reservations.Any())
				{
					if (!disptach_Rez_VTOD.Any())
					{
						var rez = response.Reservations.First();
						//disptach_VTOD_TripIDs.Add(rez.Confirmation.TPA_Extensions.DispatchConfirmation.ID, rez.Confirmation.ID.ToInt64());
						var dispatchConfirmationID = rez.Confirmation.TPA_Extensions.Confirmations.Where(s => s.Type.Trim().ToLower() == ConfirmationType.DispatchConfirmation.ToString().ToLower()).FirstOrDefault().ID;
						//var DispatchConfirmationID = rez.Confirmation.TPA_Extensions.Confirmations.Where(s => s.Type.Trim().ToLower() == ConfirmationType.DispatchConfirmation.ToString().ToLower()).FirstOrDefault().ID;

						disptach_Rez_VTOD.Add(new Tuple<string, int, long>(dispatchConfirmationID, 0, rez.Confirmation.ID.ToInt64()));
						//disptach_VTOD_TripIDs.Add(dispatchConfirmationID, rez.Confirmation.ID.ToInt64());
					}
				}
				#endregion

			}
			catch (System.Data.Entity.Validation.DbEntityValidationException ex)
			{
				foreach (var eve in ex.EntityValidationErrors)
				{
					logger.ErrorFormat("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
						eve.Entry.Entity.GetType().Name, eve.Entry.State);
					foreach (var ve in eve.ValidationErrors)
					{
						logger.ErrorFormat("- Property: \"{0}\", Error: \"{1}\"",
							ve.PropertyName, ve.ErrorMessage);
					}
				}
				var vtodException = VtodException.CreateException(ExceptionType.ECar, 1001);
				logger.Warn(vtodException.ExceptionMessage.Message);
				throw vtodException;
			}
			catch (Exception ex)
			{				
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
                				
				if (ex.GetType().Equals(typeof(VtodException)))
				{
					throw;
				}
				else
				{
					throw VtodException.CreateException(ExceptionType.ECar, 2001);// new TaxiException(Messages.Taxi_BookingFailure);
				}

			}

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return response;
		}



        public OTA_GroundBookRS ModifyBook(TokenRS tokenVTOD, OTA_GroundBookRQ request, out List<Tuple<string, int, long>> disptach_Rez_VTOD, out int fleetTripCode)
        {
            logger.Info("Start");
            Stopwatch sw = new Stopwatch();
            sw.Start();
            OTA_GroundBookRS response = null;
            disptach_Rez_VTOD = new List<Tuple<string, int, long>>();
            fleetTripCode = 0;
            try
            {
                ECarUtility utility = new ECarUtility(logger);
                ECarBookingParameter ecbp = utility.InitiateECarParameter(request);
                IECarService service = this.GetServiceAPI(ecbp.serviceAPIID, tokenVTOD, out fleetTripCode);                
                ecbp.Fleet_TripCode = fleetTripCode;

                // Cash Trip is not allowed.
                if (fleetTripCode == FleetTripCode.Ecar_Texas && request.Payments != null && request.Payments.Payments != null 
                    && request.Payments.Payments.Any() && request.Payments.Payments.First().Cash != null)
                {
                    throw  VtodException.CreateException(ExceptionType.ECar, 2029);
                }

                response = service.ModifyBook(tokenVTOD, request, out disptach_Rez_VTOD, ecbp);

            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                foreach (var eve in ex.EntityValidationErrors)
                {
                    logger.ErrorFormat("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        logger.ErrorFormat("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                var vtodException = VtodException.CreateException(ExceptionType.ECar, 1001);
                logger.Warn(vtodException.ExceptionMessage.Message);
                throw vtodException;
            }
            catch (Exception ex)
            {                
                logger.ErrorFormat("Message:{0}", ex.Message);
                logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
                logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
                if (ex.GetType().Equals(typeof(VtodException)))
                {
                    throw;
                }
                else
                {
                    throw VtodException.CreateException(ExceptionType.ECar, 2001);
                }

            }

            sw.Stop();
            logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
            return response;
        }
		public OTA_GroundCancelRS Cancel(TokenRS tokenVTOD, OTA_GroundCancelRQ request)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			OTA_GroundCancelRS response = null;
            int fleetTripCode = 0;
            try
			{
                ECarUtility utility = new ECarUtility(logger);

                var eCarCancelParameter = utility.InitiateECarParameter(request);
                var service = this.GetServiceAPI(eCarCancelParameter.serviceAPIID, tokenVTOD, out fleetTripCode);
                response = service.Cancel(tokenVTOD, request, eCarCancelParameter);
            }
			catch (Exception ex)
			{				
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);

				throw;
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return response;
		}

		public OTA_GroundAvailRS GetEstimation(TokenRS tokenVTOD, OTA_GroundAvailRQ request)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			OTA_GroundAvailRS response = new OTA_GroundAvailRS();
            OTA_GroundAvailRS result = new OTA_GroundAvailRS();
            int fleetTripCode = 0;
            try
			{
				#region new way                
				ECarUtility utility = new ECarUtility(logger);
				ECarGetEstimationParameter ecgep = (ECarGetEstimationParameter)utility.InitiateECarParameter(request, ECarServiceAPIPreferenceConst.GetEstimation);
				IECarService service = this.GetServiceAPI(ecgep.serviceAPIID, tokenVTOD, out fleetTripCode);
				response = service.GetEstimation(tokenVTOD, request, ecgep);
                #region Add Estimation Rates
                if (fleetTripCode == FleetTripCode.Ecar_Texas)
                {
                    logger.InfoFormat("Start- Calling Method AddEstimationRates");
                    this.AddEstimationRates(response, ecgep.Fleet, out result);
                    logger.InfoFormat("End- Calling Method AddEstimationRates");
                } 
                #endregion
                #endregion
            }
			catch (Exception ex)
			{				
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw;
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
            response = result;
            return response;
		}
        public void AddEstimationRates(OTA_GroundAvailRS response, ecar_fleet fleet, out OTA_GroundAvailRS result)
        {
            try
            {
                logger.InfoFormat("Start -- AddEstimatesRates Method");
                decimal? MaxEstimatedRate = 0;
                decimal? MinEstimatedRate = 0;
                decimal? totalAmount = 0;
                logger.InfoFormat("Fleet IsEstimated Property:{0}:", fleet.IsEstimated);
                if (fleet.IsEstimated == true)
                {
                    if (response.GroundServices.GroundServices != null)
                    {
                        foreach (GroundService item in response.GroundServices.GroundServices)
                        {
                            logger.InfoFormat("Total Charge :{0}:", item.TotalCharge);
                            if (item.TotalCharge != null)
                            {
                                logger.InfoFormat("EstimatedTotalAmount :{0}:", item.TotalCharge.EstimatedTotalAmount);
                                if (item.TotalCharge.EstimatedTotalAmount != 0)
                                {
                                    totalAmount = item.TotalCharge.EstimatedTotalAmount;

                                    if (fleet.MaxEstimatedRate != null)
                                    {
                                        MaxEstimatedRate = totalAmount + (totalAmount * (fleet.MaxEstimatedRate / 100));
                                    }
                                    logger.InfoFormat("Max Estimated Amount :{0}:", MaxEstimatedRate);
                                    if (fleet.MinEstimatedRate != null)
                                    {
                                        MinEstimatedRate = totalAmount - (totalAmount * fleet.MinEstimatedRate / 100);
                                    }
                                    logger.InfoFormat("Min Estimated Amount :{0}:", MinEstimatedRate);
                                    item.TotalCharge.MaxEstimatedRate = System.Math.Round(MaxEstimatedRate.ToDecimal(), 0);
                                    item.TotalCharge.MinEstimatedRate = System.Math.Round(MinEstimatedRate.ToDecimal(), 0);
                                }

                            }
                            logger.InfoFormat("GroundService Count:{0}:", response.GroundServices.GroundServices.Count());
                        }

                    }
                }

                logger.InfoFormat("End -- AddEstimatesRates Method");
                result = response;
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Message:{0}", ex.Message);
                logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
                logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
                throw;
            }
        }
        public Common.DTO.OTA.Fleet GetCorporateLeadTime(TokenRS token, UtilityGetLeadTimeRQ input)
		{
			
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			Common.DTO.OTA.Fleet result = null;
            int fleetTripCode = 0;
            try
			{
				#region new way                
				ECarUtility utility = new ECarUtility(logger);
				IECarService service = this.GetServiceAPI(ECarServiceAPIConst.Aleph, token, out fleetTripCode);
                result = service.GetLeadTime(token, input);
				#endregion
			}
			catch (Exception ex)
			{

				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw;
			}


			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return result;
		}

        public UtilityGetClientTokenRS GetCorporateClientToken(TokenRS token, UtilityGetClientTokenRQ input)
        {

            logger.Info("Start");
            Stopwatch sw = new Stopwatch();
            sw.Start();
            UtilityGetClientTokenRS result = null;
            int fleetTripCode = 0;
            try
            {
                #region new way                
                ECarUtility utility = new ECarUtility(logger);
                IECarService service = this.GetServiceAPI(ECarServiceAPIConst.Aleph, token, out fleetTripCode);
                result = service.GetCorporateClientToken(token, input);
                #region Common
                result.EchoToken = input.EchoToken;
                result.PrimaryLangID = input.PrimaryLangID;
                result.Target = input.Target;
                result.Version = input.Version;
                #endregion
                #endregion
            }
            catch (Exception ex)
            {

                logger.ErrorFormat("Message:{0}", ex.Message);
                logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
                logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
                throw;
            }


            sw.Stop();
            logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
            return result;
        }
        public Common.DTO.OTA.Fleet GetAvailableFleets(TokenRS token, UtilityAvailableFleetsRQ input)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			Common.DTO.OTA.Fleet result = null;
            int fleetTripCode = 0;
            try
			{

                #region new way
                ecar_fleet f;
                int condition;
                IECarService service = null;
                ECarUtility utility = new ECarUtility(logger);
                long serviceAPIID = 0;
                if (input.Corporate != null && input.Corporate.CorporateAccount != null)
                {
                    service = this.GetServiceAPI(ECarServiceAPIConst.Aleph, token, out fleetTripCode);
                }
                else
                {
                    serviceAPIID = utility.GetServiceAPIIdByRequest(input);
                    service = this.GetServiceAPI(serviceAPIID, token, out fleetTripCode);
                }
                result = service.GetAvailableFleets(token, input);
                #region
                result.MaxPassengers = System.Configuration.ConfigurationManager.AppSettings["MaxPassengersForBlackCar"].ToInt32();
                if (fleetTripCode == ECarServiceAPIConst.Texas)
                {
                    if (utility.ValidateFleets(token, input, out f))
                    {
                        #region Fetching Endpoint Names
                        List<string> endpointNames = utility.MtdataEndpointConfiguration(serviceAPIID, f.Id);
                        #endregion
                        bool disabilityInd = service.GetDisabilityInd(token, f, out condition, endpointNames[0], endpointNames[1]);
                        result.DisabilityVehicleInd = disabilityInd;
                        #region DefaultRadius and  RestrictNowBookingMins
                        result.RestrictNowBookingMins = System.Configuration.ConfigurationManager.AppSettings["RestrictNowBookingMins"].ToInt32();
                        result.DefaultRadius = System.Configuration.ConfigurationManager.AppSettings["DefaultRadiusForRegular"].ToInt32();
                        #endregion
                    }
                }
                else
                    result.DisabilityVehicleInd = false;
                #endregion

                #endregion
            }
            catch (Exception ex)
			{

				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw;
			}


			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return result;
		}
      
		internal OTA_GroundResRetrieveRS GroundCancelFee(TokenRS tokenVTOD, OTA_GroundResRetrieveRQ input)
		{
			try
			{
				OTA_GroundResRetrieveRS result = null;

				#region Output Conversion
				result = new OTA_GroundResRetrieveRS();
				result.Success = new Success();
				result.TPA_Extensions = new TPA_Extensions();

				result.TPA_Extensions.ServiceCharges = new ServiceCharges { Description = "Are you sure you want to cancel this trip?", Amount = "0", CurrencyCode = "USD" };
				var chargePurpose = new ChargePurpose();
				chargePurpose.Code = Common.DTO.Enum.ChargePurposeValue.CancelFee.ToString();
				chargePurpose.Value = Common.DTO.Enum.ChargePurposeValue.Other_.ToString();
				result.TPA_Extensions.ServiceCharges.ChargePurpose = chargePurpose;


				#region Common
				result.EchoToken = input.EchoToken;
				result.PrimaryLangID = input.PrimaryLangID;
				result.Target = input.Target;
				result.Version = input.Version;
				#endregion
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("GroundCancelFee:: {0}", ex.ToString() + ex.StackTrace);
				throw;

				#region Throw UtilityException
				//throw new UtilityException(Messages.Utility_VehicleGetDefaultVehicleTypeForPoint);
				//throw new SDSException(ex.Message);
				#endregion
			}
		}
        public OTA_GroundGetFinalRouteRS GetFinalRoute(TokenRS tokenVTOD, OTA_GroundGetFinalRouteRQ request)
        {
            logger.Info("Start");
            Stopwatch sw = new Stopwatch();
            sw.Start();
            int fleetTripCode = 0;
            OTA_GroundGetFinalRouteRS response = null;
            try
            {
                #region new way
                ECarUtility utility = new ECarUtility(logger);
                long serviceAPIID = utility.GetServiceAPIIdByRequest(request);
                IECarService service = this.GetServiceAPI(serviceAPIID, tokenVTOD, out fleetTripCode);
                if(fleetTripCode == FleetTripCode.Ecar_Texas)
                 response = service.GetFinalRoute(tokenVTOD, request);
                #endregion
                response.Success = new Success();
                #region Common
                response.EchoToken = request.EchoToken;
                response.PrimaryLangID = request.PrimaryLangID;
                response.Target = request.Target;
                response.Version = request.Version;
                #endregion

            }
            catch (Exception ex)
            {
            }
            sw.Stop();
            logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
            return response;
        }
        public UtilityNotifyDriverRS NotifyDriver(TokenRS token, UtilityNotifyDriverRQ input)
        {
            logger.Info("Start");
            Stopwatch sw = new Stopwatch();
            sw.Start();
            UtilityNotifyDriverRS rs = new UtilityNotifyDriverRS();
            int fleetTripCode = 0;
            try
            {
                ECarUtility utility = new ECarUtility(logger);
                long serviceAPIID = utility.GetServiceAPIIdByRequest(input);
                IECarService service = this.GetServiceAPI(serviceAPIID, token, out fleetTripCode);
                service.NotifyDriver(token, input);
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Message:{0}", ex.Message);
                logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
                logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);

                throw;
            }

            sw.Stop();
            logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
            return rs;
        }
        public UtilityNotifyDriverRS NotifyPickupDriver(TokenRS token, UtilityNotifyDriverRQ input)
        {
            logger.Info("Start");
            Stopwatch sw = new Stopwatch();
            sw.Start();
            UtilityNotifyDriverRS rs = new UtilityNotifyDriverRS();
            int fleetTripCode = 0;
            try
            {                               
                ECarUtility utility = new ECarUtility(logger);
                long serviceAPIID = utility.GetServiceAPIIdByRequest(input);
                IECarService service = this.GetServiceAPI(serviceAPIID, token, out fleetTripCode);
                service.NotifyPickupDriver(token, input);                
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Message:{0}", ex.Message);
                logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
                logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);

                throw;
            }

            sw.Stop();
            logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
            return rs;
        }

        #endregion

        #region Properties
        public TrackTime TrackTime { get; set; }
        #endregion



    }


}
