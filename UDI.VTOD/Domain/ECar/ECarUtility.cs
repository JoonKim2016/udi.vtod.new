﻿using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UDI.Map;
using UDI.VTOD.Common.DTO;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.Helper;
using UDI.VTOD.Common.Track;
using UDI.VTOD.DataAccess.VTOD;
using UDI.VTOD.Domain.ECar.Class;
using UDI.VTOD.Domain.ECar.Const;
using UDI.Utility.Helper;
using UDI.VTOD.Domain.Utility;
using System.Configuration;
using System.Security.Cryptography;
using System.Globalization;
using UDI.SDS.PaymentsService;
using System.Data;


namespace UDI.VTOD.Domain.ECar
{
	public class ECarUtility
	{
		protected ILog logger;
		private string VTODAPIVersion = string.Empty;

		public ECarUtility(ILog log)
		{
			this.logger = log;
			VTODAPIVersion = ConfigurationManager.AppSettings["Version"];
		}

        public long GetServiceAPIIdByRequest(OTA_GroundGetFinalRouteRQ request)
        {
            long serviceAPIID = -1;
            ecar_trip trip = null;
            if (request.Reference != null && request.Reference.Any())
            {
                if (request.Reference.Where(x => x.Type.Equals("Confirmation")).Any())
                {
                    using (var context = new VTODEntities())
                    {
                        var tripID = Convert.ToInt64(request.Reference.Where(x => x.Type.Equals("Confirmation")).First().ID);
                        trip = context.ecar_trip.Where(x => x.Id == tripID).FirstOrDefault();
                    }
                }
                else if (request.Reference.Where(x => x.Type.Equals("DispatchConfirmation")).Any())
                {

                    using (var context = new VTODEntities())
                    {
                        var tripID = Convert.ToInt64(request.Reference.Where(x => x.Type.Equals("DispatchConfirmation")).First().ID);

                        trip = context.ecar_trip.Where(
                                x =>
                                    x.DispatchTripId == tripID.ToString()
                            ).FirstOrDefault();
                    }
                }
                serviceAPIID = GetServiceAPIId(trip.FleetId, ECarServiceAPIPreferenceConst.GetFinalRoute);
            }
            if (serviceAPIID == -1)
            {
                var vtodException = VtodException.CreateException(ExceptionType.ECar, 1011);
                logger.Warn(vtodException.ExceptionMessage.Message);
                throw vtodException;
            }

            return serviceAPIID;
        }
        public long GetServiceAPIIdByRequest(UtilityAvailableFleetsRQ request)
        {
            long serviceAPIID = -1;
            bool result = true;
            Map.DTO.Address address = new Map.DTO.Address();

            #region Validate Pickup Address
            if (result)
            {
                //Pickup_Dropoff_Stop stop = request.Service.Pickup;

                if (request.Address != null)
                {
                    if (!string.IsNullOrWhiteSpace(request.Address.CountryName.Code) && !string.IsNullOrWhiteSpace(request.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(request.Address.CityName) && !string.IsNullOrWhiteSpace(request.Address.PostalCode))
                    {
                        address = new Map.DTO.Address { Country = request.Address.CountryName.Code, State = request.Address.StateProv.StateCode, City = request.Address.CityName, ZipCode = request.Address.PostalCode, Geolocation = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(request.Address.Latitude), Longitude = Convert.ToDecimal(request.Address.Longitude) } };
                    }
                    else
                    {
                        result = false;
                        logger.Warn("Cannot find any address/locationName from address.");
                    }
                }
                else
                {
                    result = false;
                    logger.Warn("Cannot find any address/locationName from address.");
                }
            }
            else
            {
                result = false;
            }
            #endregion

            #region Validate Fleet
            ecar_fleet fleet = null;
            if (result)
            {
                if (GetFleet(AddressType.Address, address, string.Empty, out fleet))
                {
                    logger.InfoFormat("Fleet found. ID={0}", fleet.Id);
                }
                else
                {
                    result = false;
                    logger.Warn("Unable to find fleet by this address or landmark.");
                }
            }
            else
            {
                result = false;
                logger.Warn("Unable to find fleet by invalid address or landmark.");
            }
            #endregion

            if (result)
            {
                serviceAPIID = GetServiceAPIId(fleet.Id, ECarServiceAPIPreferenceConst.GetAvailableFleets);
                if (serviceAPIID == -1)
                {
                    var vtodException = VtodException.CreateException(ExceptionType.ECar, 1011);
                    logger.Warn(vtodException.ExceptionMessage.Message);
                    throw vtodException;
                }
            }

            return serviceAPIID;
        }

        public bool GetLongitudeAndLatitudeForAirport(string airportCode, out double? longitude, out double? latitude)
		{
			bool result = false;


			//#region Track
			//if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
			//#endregion

			using (VTODEntities context = new VTODEntities())
			{
				ecar_fleet_zone zone = context.ecar_fleet_zone.Where(x => x.Name == airportCode && x.Type == AddressType.Airport).Select(x => x).FirstOrDefault();
				if (zone != null)
				{

					double lon;
					double lat;
					if (zone.CenterLongitude.HasValue && double.TryParse(zone.CenterLongitude.Value.ToString(), out lon))
					{
						longitude = lon;
						result = true;
					}
					else
					{
						logger.Error("Unable to convert longitude");
						result = false;
						longitude = null;
					}

					if (zone.CenterLatitude.HasValue && double.TryParse(zone.CenterLatitude.Value.ToString(), out lat))
					{
						latitude = lat;
						result = true;
					}
					else
					{
						logger.Error("Unable to convert latitude");
						result = false;
						latitude = null;
					}
				}
				else
				{
					result = false;
					longitude = null;
					latitude = null;
				}

			}

			//#region Track
			//if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "GetLongitudeAndLatitudeForAirport");
			//#endregion

			return result;
		}

		public string ExtractDigitsWithPlus(string input)
		{
			return Regex.Replace(input, @"[^\d\+]+", "");
			//return String.Join("", input.ToCharArray().Where(c => c >= '0' && c <= '9').Select(c => c.ToString()).ToArray());
		}

		public string ExtractDigits(string input)
		{
			return Regex.Replace(input, @"[^\d]+", "");
			//return String.Join("", input.ToCharArray().Where(c => c >= '0' && c <= '9').Select(c => c.ToString()).ToArray());
		}

		public bool IsAbleToCancelThisTrip(string status)
		{
			bool isAbleToCancel = true;
			using (VTODEntities context = new VTODEntities())
			{
				if (context.ecar_forbiddencancelstatus.Where(x => x.Status == status).Any())
				{
					isAbleToCancel = false;
				}
				else
				{
					isAbleToCancel = true;
				}

			}
			return isAbleToCancel;
		}


		public void MarkThisTripAsError(long Id, string errorMessage)
		{
			using (VTODEntities context = new VTODEntities())
			{
				vtod_trip t = context.vtod_trip.Where(x => x.Id == Id).Select(x => x).FirstOrDefault();
				t.FinalStatus = ECarTripStatusType.Error;
				t.Error = errorMessage;
				context.SaveChanges();
			}
		}

		public void SaveTripStatus(long tripID, string status, DateTime? statusTime, string vehicleNumber, decimal? vehicleLatitude, decimal? vehicleLongitude, decimal? fare, decimal? dispatchFare, int? eta, int? etawithtraffic, string driverName, string comment, string originalStatus, string driverID)
		{
			using (VTODEntities context = new VTODEntities())
			{
				logger.InfoFormat("Trip ID={0}, status={1}", tripID, status);
				context.SP_ECar_InsertTripStatus(tripID, status, statusTime, vehicleNumber, vehicleLatitude, vehicleLongitude, fare, dispatchFare, eta, etawithtraffic, driverName, driverID, comment, originalStatus);
                                                
			}
		}

		public void UpdateFinalStatus(long Id, string status, decimal? fare, DateTime? tripStartTime, DateTime? tripEndTime)
		{
			using (VTODEntities context = new VTODEntities())
			{
				vtod_trip t = context.vtod_trip.Where(x => x.Id == Id).Select(x => x).FirstOrDefault();
				decimal? gratuity = null;
				decimal? totalFareAmount = fare;
				DateTime? newTripStartTime = null;
				DateTime? newTripEndTime = null;

				#region Update Fare
				#region Calculate Gratuity
				if (t.TotalFareAmount == null || t.TotalFareAmount.Value <= 0)
				{
					if (fare.HasValue && fare > 0)
					{
						if (!t.Gratuity.HasValue || t.Gratuity <= 0)
						{
							if (t.GratuityRate.HasValue && t.GratuityRate > 0)
							{
								gratuity = fare.Value * t.GratuityRate.Value / 100;
								//t.Gratuity = gratuity;
							}
						}
						else
						{
							gratuity = t.Gratuity;
						}
					}
				}
				else
				{
					gratuity = t.Gratuity;
				}
				#endregion

				#region Calculate TotalFare
				if (t.TotalFareAmount == null || t.TotalFareAmount.Value <= 0)
				{
					if (fare.HasValue && fare > 0 && gratuity.HasValue && gratuity.Value > 0)
					{
						totalFareAmount = fare + gratuity;
					}
					else if (fare.HasValue && fare > 0)
					{
						totalFareAmount = fare;
					}
				}
				else
				{
					totalFareAmount = t.TotalFareAmount;
				}
				#endregion
				#endregion

				#region Update Trip Time

				if (t.TripStartTime == null)
					newTripStartTime = tripStartTime;
				else
					newTripStartTime = t.TripStartTime;

				if (t.TripEndTime == null)
					newTripEndTime = tripEndTime;
				else
					newTripEndTime = t.TripEndTime;
				#endregion


				context.SP_vtod_Update_FinalStatus(Id, status, totalFareAmount, gratuity, newTripStartTime, newTripEndTime, null);
			}
		}
        private void CheckBlackOut(ecar_fleet fleet, OTA_GroundBookRQ request)
        {
            //Filter has been removed 
            string requestedPickupTimeString = request.GroundReservations.FirstOrDefault().Service.Location.Pickup.DateTime;

            DateTime requestedPickupTime;
            if (DateTime.TryParse(requestedPickupTimeString, out requestedPickupTime))
            {
                using (VTODEntities context = new VTODEntities())
                {
                    ecar_fleet_blackout blackoutInfo = context.ecar_fleet_blackout.Where(p => p.FleetId == fleet.Id).SingleOrDefault();

                    if (blackoutInfo != null)
                    {
                        if (blackoutInfo.BlackoutStartFleetLocalTime.HasValue && blackoutInfo.BlackoutEndFleetLocalTime.HasValue)
                        {
                            if (requestedPickupTime >= blackoutInfo.BlackoutStartFleetLocalTime && requestedPickupTime <= blackoutInfo.BlackoutEndFleetLocalTime)
                            {
                                throw VtodException.CreateException(ExceptionType.ECar, 2013);
                            }
                        }
                    }
                }
            }
        }
        public void UpdateFinalStatus(long Id, string status, decimal? fare, decimal? gratuity, DateTime? tripStartTime, DateTime? tripEndTime, int? driverAcceptedEta)
        {
            using (VTODEntities context = new VTODEntities())
            {
                vtod_trip t = context.vtod_trip.Where(x => x.Id == Id).Select(x => x).FirstOrDefault();
                //t.FinalStatus = status;
                //t.TotalFareAmount = fare;
                //decimal? gratuity = null;
                decimal? totalFareAmount = fare;
                DateTime? newTripStartTime = null;
                DateTime? newTripEndTime = null;
                int? newDriverAcceptedEta = null;

                #region Update Fare
                #region Calculate Gratuity
                if (!gratuity.HasValue || gratuity.Value <= 0)
                {
                    if (t.TotalFareAmount == null || t.TotalFareAmount.Value <= 0)
                    {
                        if (fare.HasValue && fare > 0)
                        {
                            if (!t.Gratuity.HasValue || t.Gratuity <= 0)
                            {
                                if (t.GratuityRate.HasValue && t.GratuityRate > 0)
                                {
                                    gratuity = fare.Value * t.GratuityRate.Value / 100;
                                    //t.Gratuity = gratuity;
                                }
                            }
                            else
                            {
                                gratuity = t.Gratuity;
                            }
                        }
                    }
                    else
                    {
                        gratuity = t.Gratuity;
                    }
                }
                #endregion

                #region Calculate TotalFare
                if (t.TotalFareAmount == null || t.TotalFareAmount.Value <= 0)
                {
                    if (fare.HasValue && fare > 0 && gratuity.HasValue && gratuity.Value > 0)
                    {
                        //t.TotalFareAmount = fare + gratuity;
                        totalFareAmount = fare + gratuity;
                    }
                    else if (fare.HasValue && fare > 0)
                    {
                        totalFareAmount = fare;
                    }
                }
                else
                {
                    totalFareAmount = t.TotalFareAmount;
                }
                #endregion
                #endregion

                #region Update Trip Time
                if (t.TripStartTime == null)
                    newTripStartTime = tripStartTime;
                else
                    newTripStartTime = t.TripStartTime;

                if (t.TripEndTime == null)
                    newTripEndTime = tripEndTime;
                else
                    newTripEndTime = t.TripEndTime;
                #endregion

                #region DriverAcceptedETA
                if (!t.DriverAcceptedETA.HasValue)
                {
                    newDriverAcceptedEta = driverAcceptedEta;
                }
                else
                {
                    newDriverAcceptedEta = t.DriverAcceptedETA.Value;
                }
                #endregion

                //context.SaveChanges();

                context.SP_vtod_Update_FinalStatus(Id, status, totalFareAmount, gratuity, newTripStartTime, newTripEndTime, newDriverAcceptedEta);
            }
        }

        public bool IsAllowedPullingDriverInfo(string userName)
        {
            int ecarUserNameForPulling_Count = 0;

            try
            {
                var ecarUserNameForPulling = System.Configuration.ConfigurationManager.AppSettings["Taxi_UserName_For_Pulling_DriverInfo"];
                if (!string.IsNullOrWhiteSpace(ecarUserNameForPulling))
                {
                    //TODO : Need to dobule check to replace count with contain
                    if (ecarUserNameForPulling.Contains(","))
                        ecarUserNameForPulling_Count =
                            ecarUserNameForPulling.Split(',')
                                .Where(s => !string.IsNullOrEmpty(s) && s.Trim().ToLower() == userName.Trim().ToLower())
                                .Count();
                    else
                        ecarUserNameForPulling_Count = 1;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                logger.Error(ex.InnerException);
                logger.Error(ex.StackTrace);
            }

            return ecarUserNameForPulling_Count > 0 ? true : false;
        }

        public bool HasDriver(string status)
        {
            if (!string.IsNullOrEmpty(status))
            {
                string loweredAndTrimmedStatus = status.ToLower().Trim();

                if (loweredAndTrimmedStatus == ECarTripStatusType.Accepted.ToLower().Trim()
                   || loweredAndTrimmedStatus == ECarTripStatusType.InTransit.ToLower().Trim()
                   || loweredAndTrimmedStatus == ECarTripStatusType.PickUp.ToLower().Trim()
                   || loweredAndTrimmedStatus == ECarTripStatusType.MeterON.ToLower().Trim()
                   || loweredAndTrimmedStatus == ECarTripStatusType.InService.ToLower().Trim()
                   || loweredAndTrimmedStatus == ECarTripStatusType.Completed.ToLower().Trim()
                   || loweredAndTrimmedStatus == ECarTripStatusType.MeterOff.ToLower().Trim()
                   || loweredAndTrimmedStatus == ECarTripStatusType.Assigned.ToLower().Trim())
                {
                    return true;
                }
            }

            return false;
        }

        public vtod_driver_info GetDriverInfoByDriverId(string userName, vtod_trip trip, string status, string driverId)
        {
            vtod_driver_info result = null;

            try
            {
                if (!string.IsNullOrWhiteSpace(userName))
                {
                    if (ConfigHelper.IsTest())
                    {
                        status = ECarTripStatusType.Accepted;
                        trip.DriverInfoID = 0;
                    }

                    if (IsAllowedPullingDriverInfo(userName))
                    {
                        if (!trip.DriverInfoID.HasValue || trip.DriverInfoID.Value == 0)
                        {
                            if (HasDriver(status))
                            {
                                result = GetDriverInfoFromSDSAndUpdate(driverId, "", trip, true);
                            }
                        }
                        else
                        {
                            if (trip.DriverInfoID.HasValue && trip.DriverInfoID.Value > 0)
                            {
                                using (var db = new DataAccess.VTOD.VTODEntities())
                                {
                                    result = db.vtod_driver_info.Where(s => s.Id == trip.DriverInfoID.Value).FirstOrDefault();
                                }
                            }
                        }
                    }
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                foreach (var eve in ex.EntityValidationErrors)
                {
                    logger.ErrorFormat("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        logger.ErrorFormat("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("GetDriverInfo: {0}", ex.ToString() + ex.StackTrace);
            }

            return result;
        }

        public bool UpdateModifyEcarTrip(ECarBookingParameter ecbp)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            Stopwatch swEachPart = new Stopwatch();
            vtod_trip vtodTrip;
            ecar_trip ecarTrip;
            var history = new ecar_trip_History();
            TimeHelper utcTimeHelper = new TimeHelper();
            using (VTODEntities context = new VTODEntities())
            {
                vtodTrip = context.vtod_trip.Where(x => x.Id == ecbp.Trip.Id).Select(x => x).FirstOrDefault();
                ecarTrip = context.ecar_trip.Where(x => x.Id == ecbp.Trip.Id).Select(x => x).FirstOrDefault();
                history.PickupDateTime = vtodTrip.PickupDateTime;
                history.PickUpFullAddress = ecarTrip.PickupFullAddress;
                history.DropOffAddress = ecarTrip.DropOffFullAddress;
                history.DispatchTripId = ecarTrip.DispatchTripId;
                history.VtodTripId = ecarTrip.Id.ToInt32();
                history.PaymentType = vtodTrip.PaymentType;
                history.CreditCardID = vtodTrip.CreditCardID;
                history.DirectBillAccountID = vtodTrip.DirectBillAccountID;
                history.AppendTime = DateTime.Now;
                history.MemberID = vtodTrip.MemberID;
                context.ecar_trip_History.Add(history);
                vtodTrip.PickupDateTime = ecbp.PickupDateTime;
                //Pickup
                if (ecbp.PickupAddressType == AddressType.Address)
                {
                    var zone = GetFleetZone(ecbp.Fleet.Id, ecbp.PickupAddress);
                    if (zone != null)
                    {
                        ecarTrip.PickupFlatRateZone = zone.Name;
                        logger.InfoFormat("Pickup flat rate zone={0}", ecarTrip.PickupFlatRateZone);
                    }
                }
                else if (ecbp.PickupAddressType == AddressType.Airport)
                {
                    var zone = GetFleetZoneByAirport(ecbp.Fleet.Id, ecbp.PickupAirport);
                    if (zone != null)
                    {
                        ecarTrip.PickupFlatRateZone = zone.Name;

                        try
                        {
                            ecarTrip.PickupLatitude = System.Convert.ToDouble(zone.CenterLatitude);
                            ecarTrip.PickupLongitude = System.Convert.ToDouble(zone.CenterLongitude);
                        }
                        catch { }

                        logger.InfoFormat("Pickup flat rate zone={0}", ecarTrip.PickupFlatRateZone);
                    }

                }
                //Dropoff
                if (ecbp.DropOffAddressType == AddressType.Address)
                {
                    var zone = GetFleetZone(ecbp.Fleet.Id, ecbp.DropOffAddress);
                    if (zone != null)
                    {
                        ecarTrip.DropOffFlatRateZone = zone.Name;
                        logger.InfoFormat("Dropoff flat rate zone={0}", ecarTrip.DropOffFlatRateZone);
                    }
                }
                else if (ecbp.DropOffAddressType == AddressType.Airport)
                {
                    var zone = GetFleetZoneByAirport(ecbp.Fleet.Id, ecbp.DropoffAirport);
                    if (zone != null)
                    {
                        ecarTrip.DropOffFlatRateZone = zone.Name;

                        try
                        {
                            ecarTrip.DropOffLatitude = System.Convert.ToDouble(zone.CenterLatitude);
                            ecarTrip.DropOffLongitude = System.Convert.ToDouble(zone.CenterLongitude);
                        }
                        catch { }

                        logger.InfoFormat("Dropoff flat rate zone={0}", ecarTrip.DropOffFlatRateZone);
                    }

                }
                var accountNumber = context.SP_ECar_GetFleet_AccountNumber(ecbp.Fleet.Id, ecbp.tokenVTOD.Username).ToList().FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(accountNumber))
                {
                    ecarTrip.AccountNumber = accountNumber;
                }
                swEachPart.Restart();
                #region pickup address
                if (ecbp.PickupAddressType == AddressType.Address)
                {
                    ecarTrip.PickupAddressType = ecbp.PickupAddressType;
                    double pickupLat = 0;
                    double pickupLongt = 0;
                    if (Double.TryParse(ecbp.PickupAddress.Geolocation.Latitude.ToString(), out pickupLat))
                    {
                        ecarTrip.PickupLatitude = pickupLat;
                    }

                    if (Double.TryParse(ecbp.PickupAddress.Geolocation.Longitude.ToString(), out pickupLongt))
                    {
                        ecarTrip.PickupLongitude = pickupLongt;
                    }

                    ecarTrip.PickupStreetNo = ecbp.PickupAddress.StreetNo;
                    ecarTrip.PickupStreetName = ecbp.PickupAddress.Street;
                    ecarTrip.PickupStreetType = ExtractStreetType(ecbp.PickupAddress.Street);
                    ecarTrip.PickupAptNo = ecbp.PickupAddress.ApartmentNo;
                    ecarTrip.PickupCity = ecbp.PickupAddress.City;
                    ecarTrip.PickupStateCode = ecbp.PickupAddress.State;
                    ecarTrip.PickupZipCode = ecbp.PickupAddress.ZipCode;
                    ecarTrip.PickupCountryCode = ecbp.PickupAddress.Country;
                    ecarTrip.PickupFullAddress = ecbp.PickupAddress.FullAddress;
                }
                else if (ecbp.PickupAddressType == AddressType.Airport)
                {
                    ecarTrip.PickupAddressType = ecbp.PickupAddressType;
                    ecarTrip.PickupAirport = ecbp.PickupAirport;
                    ecarTrip.PickupFullAddress = ecbp.PickupAirport;
                }
                else
                {
                    logger.Warn("No pickup location");
                    ecarTrip.PickupAddressType = AddressType.Empty;
                }
                #endregion
                swEachPart.Stop();
                logger.DebugFormat("Set pickup address Duration:{0} ms.", swEachPart.ElapsedMilliseconds);
                swEachPart.Restart();
                #region PickUp UTC Time
                try
                {
                    if (ecarTrip.PickupLatitude != null && ecarTrip.PickupLongitude != null)
                    {
                        vtodTrip.PickupDateTimeUTC = utcTimeHelper.GetUTCFromLocalTime(ecarTrip.PickupLatitude, ecarTrip.PickupLongitude, ecbp.PickupDateTime);
                    }
                    else
                    {
                        if (ecbp.PickupAddressType == AddressType.Airport)
                        {
                            double? longitude;
                            double? latitude;
                            if (GetLongitudeAndLatitudeForAirport(ecarTrip.PickupAirport, out longitude, out latitude))
                            {
                                vtodTrip.PickupDateTimeUTC = utcTimeHelper.GetUTCFromLocalTime(ecarTrip.PickupLatitude, ecarTrip.PickupLongitude, ecbp.PickupDateTime);
                            }
                        }
                        else
                        {
                            vtodTrip.PickupDateTimeUTC = ecbp.PickupDateTime.ToUtc(ecbp.Fleet.ServerUTCOffset);
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.InfoFormat("Exception in PickUpTime UTC:{0}", ex.Message);
                    vtodTrip.PickupDateTimeUTC = ecbp.PickupDateTime.ToUtc(ecbp.Fleet.ServerUTCOffset);
                }
                #endregion
                swEachPart.Stop();
                logger.DebugFormat("Set pick time utc Duration:{0} ms.", swEachPart.ElapsedMilliseconds);
                #region dropoff address
                double dropoffLat = 0;
                double dropoffLongt = 0;
                if (ecbp.DropOffAddressType == AddressType.Address)
                {
                    ecarTrip.DropOffAddressType = ecbp.DropOffAddressType;
                    if (ecbp.DropOffAddress.Geolocation != null)
                    {
                        if (Double.TryParse(ecbp.DropOffAddress.Geolocation.Latitude.ToString(), out dropoffLat))
                        {
                            ecarTrip.DropOffLatitude = dropoffLat;
                        }
                        if (Double.TryParse(ecbp.DropOffAddress.Geolocation.Longitude.ToString(), out dropoffLongt))
                        {
                            ecarTrip.DropOffLongitude = dropoffLongt;
                        }
                    }

                    ecarTrip.DropOffStreetNo = ecbp.DropOffAddress.StreetNo;
                    ecarTrip.DropOffStreetName = ecbp.DropOffAddress.Street;
                    ecarTrip.DropOffStreetType = ExtractStreetType(ecbp.DropOffAddress.Street);
                    ecarTrip.DropOffAptNo = ecbp.DropOffAddress.ApartmentNo;
                    ecarTrip.DropOffCity = ecbp.DropOffAddress.City;
                    ecarTrip.DropOffStateCode = ecbp.DropOffAddress.State;
                    ecarTrip.DropOffZipCode = ecbp.DropOffAddress.ZipCode;
                    ecarTrip.DropOffCountryCode = ecbp.DropOffAddress.Country;
                    ecarTrip.DropOffFullAddress = ecbp.DropOffAddress.FullAddress;

                }
                else if (ecbp.DropOffAddressType == AddressType.Airport)
                {
                    ecarTrip.DropOffAddressType = ecbp.DropOffAddressType;
                    ecarTrip.DropOffAirport = ecbp.DropoffAirport;
                    ecarTrip.DropOffFullAddress = ecbp.DropoffAirport;
                }
                else if (ecbp.DropOffAddressType == AddressType.Empty)
                {
                    ecarTrip.DropOffAddressType = AddressType.Empty;
                    logger.Info("No drop off location");
                }
                #endregion
                swEachPart.Stop();
                logger.DebugFormat("Set dropoff address Duration:{0} ms.", swEachPart.ElapsedMilliseconds);
                ecarTrip.RemarkForPickup = ecbp.RemarkForPickup;
                ecarTrip.RemarkForDropOff = ecbp.RemarkForDropOff;
                #region Payment
                swEachPart.Restart();
                if (ecbp.PaymentType == Common.DTO.Enum.PaymentType.PaymentCard)
                {
                    vtodTrip.PaymentType = ecbp.PaymentType.ToString();
                    vtodTrip.CreditCardID = ecbp.CreditCardID.HasValue ? ecbp.CreditCardID.Value.ToString() : null;
                }
                else
                {
                    vtodTrip.PaymentType = ecbp.PaymentType.ToString();
                }

                swEachPart.Stop();
                logger.DebugFormat("Set payment type Duration:{0} ms.", swEachPart.ElapsedMilliseconds);
                #endregion
                vtodTrip.PickMeUpNow = ecbp.PickupNow;
                context.SaveChanges();
                swEachPart.Stop();
                logger.DebugFormat("Modify this trip Duration:{0} ms.", swEachPart.ElapsedMilliseconds);
                logger.Info("After we modify a trip");
            }
            return true;
        }

        public vtod_trip UpdateECarTrip(vtod_trip t, string confirmationNo,int fleetTripCode)
		{
			using (VTODEntities context = new VTODEntities())
			{

				var v = context.vtod_trip.Where(x => x.Id == t.Id).Select(x => x).FirstOrDefault();
				var e = context.ecar_trip.Where(x => x.Id == t.Id).Select(x => x).FirstOrDefault();
				e.DispatchTripId = confirmationNo;
				v.AppendTime = DateTime.Now;
                v.FleetTripCode = fleetTripCode;
				v.FinalStatus = ECarTripStatusType.Booked;
				context.SaveChanges();

				t.ecar_trip.DispatchTripId = e.DispatchTripId;
				t.AppendTime = v.AppendTime;
				t.FinalStatus = v.FinalStatus;
			}
			return t;
		}

        public string Data_Hex_Asc(string Data)
        {
            string hex = "";
            foreach (char c in Data)
            {
                int tmp = c;
                hex += String.Format("{0:x2}", (uint)System.Convert.ToUInt32(tmp.ToString()));
            }
            return hex;
        }
        public ecar_mtdata_configuration GetConfigurationNames(long fleetID)
        {
            ecar_mtdata_configuration configurations = new ecar_mtdata_configuration();
            using (VTODEntities context = new VTODEntities())
            {
                configurations = context.ecar_mtdata_configuration.Where(x => x.FleetId == fleetID).Select(x => x).FirstOrDefault();
            }
            return configurations;
        }
        public List<string> MtdataEndpointConfiguration(long serviceAPIID, long Id)
        {
            List<string> endpointNames = new List<string>();
            if (serviceAPIID != 0)
            {
                if (serviceAPIID == ECarServiceAPIConst.Texas)
                {
                    ecar_mtdata_configuration config = GetConfigurationNames(Id);
                    if (config != null)
                    {
                        endpointNames.Add(config.AuthenticateServiceEndpointName);
                        endpointNames.Add(config.BookingServiceEndpointName);
                        endpointNames.Add(config.OsiServiceEndpointName);
                        endpointNames.Add(config.AddressServiceEndpointName);
                    }
                    else
                    {
                        endpointNames.Add("MTDataAuthenticationWebServiceSoap");
                        endpointNames.Add("MTDataBookingWebServiceSoap");
                        endpointNames.Add("MTDataOSIWebServiceSoap");
                        endpointNames.Add("MTDataAddressWebServiceSoap");
                    }
                }
            }
            return endpointNames;
        }
        public long GetServiceAPIIdByRequest(UtilityNotifyDriverRQ request)
        {
            long serviceAPIID = -1;

            using (VTODEntities context = new VTODEntities())
            {
                var ecarTrip = context.ecar_trip.FirstOrDefault(m => m.Id == request.TripID);
                if (ecarTrip != null)
                {
                    try
                    {
                        serviceAPIID = GetServiceAPIId(ecarTrip.FleetId, ECarServiceAPIPreferenceConst.NotifyDriver);
                    }
                    catch (Exception ex)
                    {
                        var vtodException = VtodException.CreateException(ExceptionType.ECar, 1011);
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException;
                    }
                }

                if (serviceAPIID == -1)
                { 
                    serviceAPIID = ECarServiceAPIConst.Texas;
                }
            }

            if (serviceAPIID == -1)
            {
                var vtodException = VtodException.CreateException(ExceptionType.ECar, 1011);
                logger.Warn(vtodException.ExceptionMessage.Message);
                throw vtodException;
            }

            return serviceAPIID;
        }
        private ecar_fleet_zone GetFleetZone(long fleetId, Map.DTO.Address address)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            ecar_fleet_zone result = null;
            using (VTODEntities context = new VTODEntities())
            {
                try
                {
                    //find fleet zone by geolocation
                    if (address != null && address.Geolocation != null && address.Geolocation.Latitude != 0 && address.Geolocation.Longitude != 0)
                    {
                        result = context.SP_Ecar_GetFleetZone(fleetId, address.Geolocation.Latitude, address.Geolocation.Longitude).FirstOrDefault();
                    }

                    //find flett zone by zip
                    if (result == null)
                    {
                        result = context.SP_Ecar_GetFlletZoneByZip(fleetId, address.ZipCode.ToString()).FirstOrDefault();
                    }

                    //find flett zone by city
                    if (result == null)
                    {
                        result = context.SP_Ecar_GetFlletZoneByCity(fleetId, address.City.ToString()).FirstOrDefault();
                    }
                }
                catch { }
            }
            sw.Stop();
            logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
            return result;
        }
        
        private ecar_fleet_zone GetFleetZoneByAirport(long fleetId, string airportName)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            ecar_fleet_zone result = null;
            using (VTODEntities context = new VTODEntities())
            {
                try
                {
                    if (fleetId > 0 && !string.IsNullOrWhiteSpace(airportName))
                    {
                        result = context.SP_Ecar_GetFleetZoneByAirport(fleetId, airportName).FirstOrDefault();
                    }
                }
                catch { }
            }
            sw.Stop();
            logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
            return result;
        }




        #region Validation
        public bool ValidateTexasModifyBookingRequest(TokenRS tokenVTOD, OTA_GroundBookRQ request, ECarBookingParameter ecbp, MTData.BookingWebService.GetBookingFullResult bookingResult)
        {
            bool result = true;
            Stopwatch sw = new Stopwatch();
            sw.Start();           
            Stopwatch swEachPart = new Stopwatch();

            #region Track

            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");

            #endregion

            #region Validate Reservation

            if (request.GroundReservations.Any())
            {
                GroundReservation reservation = request.GroundReservations.FirstOrDefault();

                #region Set token, request

                ecbp.tokenVTOD = tokenVTOD;
                ecbp.request = request;

                #endregion
                if (request == null || request.TPA_Extensions == null || request.TPA_Extensions.MemberID == null)
                {
                    var vtodException = VtodException.CreateFieldRequiredValidationException("MemberId");
                    logger.Warn(vtodException.ExceptionMessage.Message);
                    throw vtodException;
                }
                else
                {
                    ecbp.MemberID = request.TPA_Extensions.MemberID.ToInt32();
                }               

                swEachPart.Restart();

                #region Validate Customer

                if (bookingResult != null && bookingResult.Settings != null)
                {
                    if (bookingResult.Settings.Booking != null)
                    {
                        if (!string.IsNullOrWhiteSpace(bookingResult.Settings.Booking.ContactName))
                        {
                            ecbp.FirstName = bookingResult.Settings.Booking.ContactName.Substring(0, bookingResult.Settings.Booking.ContactName.IndexOf(" "));
                            ecbp.LastName = bookingResult.Settings.Booking.ContactName.Substring(bookingResult.Settings.Booking.ContactName.IndexOf(" ") + 1);
                            logger.Info("First name and last name are valid.");
                        }
                        else
                        {
                            result = false;
                            var vtodException =
                                VtodException.CreateFieldRequiredValidationException(
                                    "PassengerGivenName|PassengerSurname");
                            logger.Warn(vtodException.ExceptionMessage.Message);
                            throw vtodException;

                        }
                    }
                    else
                    {
                        result = false;
                        var vtodException = VtodException.CreateFieldRequiredValidationException("PassengerName");
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException;
                    }

                    //Telephone
                    if (!string.IsNullOrWhiteSpace(bookingResult.Settings.Booking.ContactPhoneNumber))
                    {
                        ecbp.PhoneNumber = bookingResult.Settings.Booking.ContactPhoneNumber;
                        logger.Info("Phone number(s) is/are valid.");
                    }
                    else
                    {
                        result = false;
                        var vtodException = VtodException.CreateFieldRequiredValidationException("Telephones");
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException;
                    }
                    if (!string.IsNullOrWhiteSpace(bookingResult.Settings.Booking.ContactEmail))
                    {
                        ecbp.EmailAddress = bookingResult.Settings.Booking.ContactEmail;
                        logger.Info(Messages.ECar_Common_ValidReservationPassengerPrimaryTelephones);
                    }
                    if (result)
                    {
                        logger.InfoFormat("Customer firstname={0}, lastname={1}, phonenumber={2}", ecbp.FirstName,
                            ecbp.LastName, ecbp.PhoneNumber);
                    }
                    else
                    {
                        result = false;
                        var vtodException = VtodException.CreateException(ExceptionType.ECar, 2008);
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException;
                    }
                }
                else
                {
                    result = false;
                    var vtodException = VtodException.CreateFieldRequiredValidationException("Passenger");
                    logger.Warn(vtodException.ExceptionMessage.Message);
                    throw vtodException; // new ValidationException(Messages.Taxi_Common_InvalidReservation);
                }

                #endregion

                if (TrackTime != null)
                    TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidateCustomer");
                swEachPart.Stop();
                logger.DebugFormat("Validate Customer Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                

                #region determine Pick me up now               
                ecbp.PickupNow = request.TPA_Extensions.PickMeUpNow.ToBool();
                #endregion

                swEachPart.Restart();

                #region Validate Pickup Address

                if (reservation.Service != null && ecbp.PickupNow == false)
                {
                    if (reservation.Service.Location != null)
                    {
                        if (reservation.Service.Location.Pickup != null)
                        {
                            if (((reservation.Service.Location.Pickup.Address != null) &&
                                 (reservation.Service.Location.Pickup.AirportInfo != null)) ||
                                ((reservation.Service.Location.Pickup.Address == null) &&
                                 (reservation.Service.Location.Pickup.AirportInfo == null)))
                            {
                                result = false;
                                logger.Warn(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
                                throw new ValidationException(
                                    Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
                            }
                            else if ((reservation.Service.Location.Pickup.Address != null) &&
                                     (reservation.Service.Location.Pickup.AirportInfo == null))
                            {

                                if (
                                    (string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude) ||
                                     (string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude))) &&
                                    (string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.StreetNmbr) ||
                                     string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.AddressLine) ||
                                     string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CityName) ||
                                     string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.PostalCode) ||
                                     string.IsNullOrWhiteSpace(
                                         reservation.Service.Location.Pickup.Address.StateProv.StateCode) ||
                                     string.IsNullOrWhiteSpace(
                                         reservation.Service.Location.Pickup.Address.CountryName.Code)))
                                {
                                    result = false;
                                    logger.Warn(Messages.Validation_LocationDetail);
                                    throw new ValidationException(Messages.Validation_LocationDetail);
                                }
                                else if (
                                (!string.IsNullOrWhiteSpace(
                                    reservation.Service.Location.Pickup.Address.Longitude) &&
                                 (!string.IsNullOrWhiteSpace(
                                     reservation.Service.Location.Pickup.Address.Longitude))) &&
                                (string.IsNullOrWhiteSpace(
                                    reservation.Service.Location.Pickup.Address.StreetNmbr) ||
                                 string.IsNullOrWhiteSpace(
                                     reservation.Service.Location.Pickup.Address.AddressLine) ||
                                 string.IsNullOrWhiteSpace(
                                     reservation.Service.Location.Pickup.Address.CityName) ||
                                 string.IsNullOrWhiteSpace(
                                     reservation.Service.Location.Pickup.Address.PostalCode) ||
                                 string.IsNullOrWhiteSpace(
                                     reservation.Service.Location.Pickup.Address.StateProv.StateCode) ||
                                 string.IsNullOrWhiteSpace(
                                     reservation.Service.Location.Pickup.Address.CountryName.Code)))
                                {
                                    ecbp.PickupAddressOnlyContainsLatAndLong = true;
                                    ecbp.PickupAddressType = AddressType.Address;
                                    ecbp.PickupAddress = ConvertAddress(reservation.Service.Location.Pickup);
                                }
                                else if (
                                    !string.IsNullOrWhiteSpace(
                                        reservation.Service.Location.Pickup.Address.AddressLine) &&
                                    !string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CityName) &&
                                    !string.IsNullOrWhiteSpace(
                                        reservation.Service.Location.Pickup.Address.PostalCode) &&
                                    !string.IsNullOrWhiteSpace(
                                        reservation.Service.Location.Pickup.Address.StateProv.StateCode) &&
                                    !string.IsNullOrWhiteSpace(
                                        reservation.Service.Location.Pickup.Address.CountryName.Code))
                                {
                                    //Address
                                    Map.DTO.Address pickupAddress = null;
                                    //string pickupAddressStr = GetAddressString(reservation.Service.Location.Pickup.Address);
                                    logger.Info("Skip MAP API Validation.");
                                    pickupAddress = ConvertAddress(reservation.Service.Location.Pickup);
                                    ecbp.PickupAddressType = AddressType.Address;
                                    ecbp.PickupAddress = pickupAddress;
                                    logger.Info("Pickup AddressType: Address");
                                }
                                else
                                {
                                    result = false;
                                    var vtodException = VtodException.CreateException(ExceptionType.ECar, 1);
                                    logger.Warn(vtodException.ExceptionMessage.Message);                                    
                                    throw vtodException;
                                }
                            }
                            else if ((reservation.Service.Location.Pickup.Address == null) &&
                                     (reservation.Service.Location.Pickup.AirportInfo != null))
                            {
                                //airport
                                if (((reservation.Service.Location.Pickup.AirportInfo.Arrival != null) &&
                                     (reservation.Service.Location.Pickup.AirportInfo.Departure != null)) ||
                                    ((reservation.Service.Location.Pickup.AirportInfo.Arrival == null) &&
                                     (reservation.Service.Location.Pickup.AirportInfo.Departure == null)))
                                {
                                    result = false;
                                    logger.Warn(Messages.Validation_ArrivalAndDepartureAirportInfo);
                                    throw VtodException.CreateValidationException(
                                        Messages.Validation_ArrivalAndDepartureAirportInfo);
                                }
                                else if (reservation.Service.Location.Pickup.AirportInfo.Arrival != null &&
                                         reservation.Service.Location.Pickup.AirportInfo.Departure == null)
                                {
                                    //Arrival
                                    ecbp.PickupAddressType = AddressType.Airport;
                                    ecbp.PickupAirport =
                                        reservation.Service.Location.Pickup.AirportInfo.Arrival.LocationCode;
                                    logger.Info("Pickup AddressType: AirportInfo.Arrival");
                                }
                                else if (reservation.Service.Location.Pickup.AirportInfo.Arrival == null &&
                                         reservation.Service.Location.Pickup.AirportInfo.Departure != null)
                                {
                                    //Depature

                                    ecbp.PickupAddressType = AddressType.Airport;
                                    ecbp.PickupAirport =
                                        reservation.Service.Location.Pickup.AirportInfo.Departure
                                            .LocationCode;
                                    logger.Info("Pickup AddressType: AirportInfo.Depature");
                                }

                                //longitude and latitude
                                if (result)
                                {
                                    double? longitude = null;
                                    double? latitude = null;
                                    if (GetLongitudeAndLatitudeForAirport(ecbp.PickupAirport, out longitude,
                                        out latitude))
                                    {
                                        ecbp.LongitudeForPickupAirport = longitude.Value;
                                        ecbp.LatitudeForPickupAirport = latitude.Value;
                                    }
                                    else
                                    {
                                        throw VtodException.CreateException(ExceptionType.ECar, 1010);
                                    }
                                }


                            }

                        }

                    }

                }
                else
                {
                    if (bookingResult.Settings != null && bookingResult.Settings.Booking != null)
                    {
                        if (bookingResult.Settings.Booking.Locations != null && bookingResult.Settings.Booking.Locations.Any())
                        {
                            var pickupLocation = bookingResult.Settings.Booking.Locations.Where(p => p.LocationType == UDI.VTOD.MTData.BookingWebService.LocationType.PickUp).FirstOrDefault();
                            if (pickupLocation != null)
                            {
                                if (pickupLocation.AddressType.ToString() == AddressType.Airport)
                                {
                                    ecbp.PickupAddressType = pickupLocation.AddressType.ToString();
                                    ecbp.LatitudeForPickupAirport = pickupLocation.Address.Latitude.ToDouble();
                                    ecbp.LongitudeForDropoffAirport = pickupLocation.Address.ToDouble();
                                }
                                else
                                {

                                    Map.DTO.Address pickupAddress = new Map.DTO.Address();
                                    if (pickupLocation.Address.Suburb != null)
                                        pickupAddress.City = pickupLocation.Address.Suburb.Name;
                                    pickupAddress.Geolocation = new UDI.Map.DTO.Geolocation();
                                    pickupAddress.Geolocation.Latitude = pickupLocation.Address.Latitude.ToDecimal();
                                    pickupAddress.Geolocation.Longitude = pickupLocation.Address.Longitude.ToDecimal();
                                    pickupAddress.StreetNo = pickupLocation.Address.Unit;
                                    if (pickupLocation.Address.Street != null)
                                        pickupAddress.Street = pickupLocation.Address.Street.Name;
                                    if (pickupLocation.Address.Street.Postcode != null)
                                        pickupAddress.ZipCode = pickupLocation.Address.Street.Postcode;
                                    if (pickupLocation.Address.Suburb.Postcode != null)
                                        pickupAddress.ZipCode = pickupLocation.Address.Suburb.Postcode;
                                    ecbp.PickupAddress = new Map.DTO.Address();
                                    ecbp.PickupAddress = pickupAddress;
                                }

                            }
                        }
                    }
                }
                if (bookingResult.Settings != null && bookingResult.Settings.Booking != null)
                {
                    if (bookingResult.Settings.Booking.Locations != null && bookingResult.Settings.Booking.Locations.Any())
                    {
                        var pickupLocation = bookingResult.Settings.Booking.Locations.Where(p => p.LocationType == UDI.VTOD.MTData.BookingWebService.LocationType.PickUp).FirstOrDefault();
                        if (pickupLocation != null)
                        {
                            ecbp.RemarkForPickup = pickupLocation.Remark;
                            ecbp.NumberOfPassenger = pickupLocation.Pax;
                        }
                    }
                }

                #endregion

                if (TrackTime != null)
                    TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility,
                        "ValidateBookingRequest_ValidatePickupAddress");
                swEachPart.Stop();
                logger.DebugFormat("Validate Pickup address Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                swEachPart.Restart();


                if (result)
                {   
                    if (ecbp.Fleet != null)
                    {                        
                        logger.InfoFormat("Fleet found. ID={0}", ecbp.Fleet.Id);

                        if (ecbp.Fleet != null)
                        {
                            ecbp.serviceAPIID = GetECarFleetServiceAPIPreference(ecbp.Fleet.Id, ECarServiceAPIPreferenceConst.Book).ServiceAPIId;
                            
                            #region Fetching Endpoint Names
                            List<string> endpointNames = MtdataEndpointConfiguration(ecbp.serviceAPIID, ecbp.Fleet.Id);
                            if (endpointNames != null && endpointNames.Any())
                            {
                                ecbp.AuthenticationServiceEndpointName = endpointNames[0];
                                ecbp.BookingWebServiceEndpointName = endpointNames[1];
                                ecbp.OsiWebServiceEndpointName = endpointNames[2];
                                ecbp.AddressWebServiceEndpointName = endpointNames[3];
                            }
                            #endregion
                        }
                    }
                    else
                    {
                        result = false;
                        var vtodException = VtodException.CreateException(ExceptionType.ECar, 1011);
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException;                        
                    }
                }
                else
                {
                    result = false;
                    var vtodException = VtodException.CreateException(ExceptionType.ECar, 2001);
                    logger.Warn(vtodException.ExceptionMessage.Message);
                    throw vtodException;                    
                }

                #endregion

                if (TrackTime != null)
                    TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidateFleet");
                swEachPart.Stop();
                logger.DebugFormat("Validate fleet Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                swEachPart.Restart();

                #region Validate User

                if (result)
                {
                    ecar_fleet_user user = null;
                    if (GetECarFleetUser(ecbp.Fleet.Id, tokenVTOD.Username, out user))
                    {
                        ecbp.User = user;
                        logger.Info("This user has sufficient privilege.");
                    }
                    else
                    {
                        result = false;
                        var vtodException = VtodException.CreateException(ExceptionType.ECar, 102);
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException;                        
                    }
                }

                #endregion

                if (TrackTime != null)
                    TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidateUser");
                swEachPart.Stop();
                logger.DebugFormat("Validate user Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                swEachPart.Restart();

                #region Validate PickupDateTime

                if (result)
                {
                    #region determine pick me up now Time

                    DateTime pickupDateTime;
                    if (ecbp.PickupNow == false)
                    {
                        if (bookingResult.Settings != null && bookingResult.Settings.Booking != null)
                        {
                            if (bookingResult.Settings.Booking.Locations != null && bookingResult.Settings.Booking.Locations.Any())
                            {
                                var pickupLocation = bookingResult.Settings.Booking.Locations.Where(p => p.LocationType == UDI.VTOD.MTData.BookingWebService.LocationType.PickUp).FirstOrDefault();
                                if (pickupLocation != null)
                                {
                                    ecbp.PickupDateTime = pickupLocation.Time;
                                }
                            }
                        }

                    }
                    else
                    {
                        if (DateTime.TryParse(reservation.Service.Location.Pickup.DateTime, out pickupDateTime))
                        {
                            ecbp.PickupDateTime = pickupDateTime;
                            logger.Info("PickupDateTime is correct");
                        }
                        else if (bookingResult.Settings != null && bookingResult.Settings.Booking != null)
                        {
                            if (bookingResult.Settings.Booking.Locations != null && bookingResult.Settings.Booking.Locations.Any())
                            {
                                var pickupLocation = bookingResult.Settings.Booking.Locations.Where(p => p.LocationType == UDI.VTOD.MTData.BookingWebService.LocationType.PickUp).FirstOrDefault();
                                if (pickupLocation != null)
                                {
                                    ecbp.PickupDateTime = pickupLocation.Time;
                                }
                            }
                        }
                        else
                        {
                            result = false;
                            var vtodException = VtodException.CreateFieldFormatValidationException("PickupDateTime",
                                "DateTime");
                            logger.Warn(vtodException.ExceptionMessage.Message);
                            throw vtodException;
                        }
                    }

                    #endregion
                }
                else
                {
                    result = false;
                    var vtodException = VtodException.CreateException(ExceptionType.ECar, 2001);
                    logger.Warn(vtodException.ExceptionMessage.Message);
                    throw vtodException;                    
                }

                #endregion

                if (TrackTime != null)
                    TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility,"ValidateBookingRequest_ValidatePickupDateTime");
                swEachPart.Stop();
                logger.DebugFormat("Validate PickupTime Duration:{0} ms.", swEachPart.ElapsedMilliseconds);
                swEachPart.Restart();

                #region Validate Dropoff Address

                //DropOff is optional: By_Pouyan
                if (reservation.Service.Location != null && reservation.Service.Location.Dropoff != null)
                    if (result)
                    {
                        if (reservation.Service != null)
                        {
                            if (reservation.Service.Location != null)
                            {
                                if (reservation.Service.Location.Dropoff != null)
                                {
                                    using (VTODEntities context = new VTODEntities())
                                    {
                                        if (((reservation.Service.Location.Dropoff.Address != null) &&
                                             (reservation.Service.Location.Dropoff.AirportInfo != null)) ||
                                            ((reservation.Service.Location.Dropoff.Address == null) &&
                                             (reservation.Service.Location.Dropoff.AirportInfo == null)))
                                        {
                                            result = false;
                                            logger.Warn(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
                                            throw VtodException.CreateValidationException(
                                                Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
                                        }
                                        else if ((reservation.Service.Location.Dropoff.Address != null) &&
                                                 (reservation.Service.Location.Dropoff.AirportInfo == null))
                                        {
                                            //Address
                                            if (
                                                (string.IsNullOrWhiteSpace(
                                                    reservation.Service.Location.Dropoff.Address.Longitude) ||
                                                 (string.IsNullOrWhiteSpace(
                                                     reservation.Service.Location.Dropoff.Address.Longitude))) &&
                                                (string.IsNullOrWhiteSpace(
                                                    reservation.Service.Location.Dropoff.Address.StreetNmbr) ||
                                                 string.IsNullOrWhiteSpace(
                                                     reservation.Service.Location.Dropoff.Address.AddressLine) ||
                                                 string.IsNullOrWhiteSpace(
                                                     reservation.Service.Location.Dropoff.Address.CityName) ||
                                                 string.IsNullOrWhiteSpace(
                                                     reservation.Service.Location.Dropoff.Address.PostalCode) ||
                                                 string.IsNullOrWhiteSpace(
                                                     reservation.Service.Location.Dropoff.Address.StateProv.StateCode) ||
                                                 string.IsNullOrWhiteSpace(
                                                     reservation.Service.Location.Dropoff.Address.CountryName.Code)))
                                            //if (string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CountryName.Code))
                                            {
                                                result = false;
                                                logger.Warn(Messages.Validation_LocationDetail);
                                                throw VtodException.CreateValidationException(
                                                    Messages.Validation_LocationDetail);
                                            }
                                            else if (
                                                (!string.IsNullOrWhiteSpace(
                                                    reservation.Service.Location.Dropoff.Address.Longitude) &&
                                                 (!string.IsNullOrWhiteSpace(
                                                     reservation.Service.Location.Dropoff.Address.Longitude))) &&
                                                (string.IsNullOrWhiteSpace(
                                                    reservation.Service.Location.Dropoff.Address.StreetNmbr) ||
                                                 string.IsNullOrWhiteSpace(
                                                     reservation.Service.Location.Dropoff.Address.AddressLine) ||
                                                 string.IsNullOrWhiteSpace(
                                                     reservation.Service.Location.Dropoff.Address.CityName) ||
                                                 string.IsNullOrWhiteSpace(
                                                     reservation.Service.Location.Dropoff.Address.PostalCode) ||
                                                 string.IsNullOrWhiteSpace(
                                                     reservation.Service.Location.Dropoff.Address.StateProv
                                                         .StateCode) ||
                                                 string.IsNullOrWhiteSpace(
                                                     reservation.Service.Location.Dropoff.Address.CountryName
                                                         .Code)))
                                            {
                                                ecbp.DropOffAddressOnlyContainsLatAndLong = true;
                                                ecbp.DropOffAddressType = AddressType.Address;
                                                ecbp.DropOffAddress =
                                                    ConvertAddress(reservation.Service.Location.Dropoff);
                                            }
                                            else if (
                                                !string.IsNullOrWhiteSpace(
                                                    reservation.Service.Location.Dropoff.Address.AddressLine) &&
                                                !string.IsNullOrWhiteSpace(
                                                    reservation.Service.Location.Dropoff.Address.CityName) &&
                                                !string.IsNullOrWhiteSpace(
                                                    reservation.Service.Location.Dropoff.Address.PostalCode) &&
                                                !string.IsNullOrWhiteSpace(
                                                    reservation.Service.Location.Dropoff.Address.StateProv
                                                        .StateCode) &&
                                                !string.IsNullOrWhiteSpace(
                                                    reservation.Service.Location.Dropoff.Address.CountryName.Code))
                                            {
                                                Map.DTO.Address dropoffAddress = null;
                                                string dropoffAddressStr =
                                                    GetAddressString(reservation.Service.Location.Dropoff.Address);
                                                logger.Info("Skip MAP API Validation.");
                                                dropoffAddress =
                                                    ConvertAddress(reservation.Service.Location.Dropoff);
                                                ecbp.DropOffAddress = dropoffAddress;
                                                ecbp.DropOffAddressType =
                                                    UDI.VTOD.Domain.ECar.Const.AddressType.Address;
                                                logger.Info("Dropoff AddressType=Address");
                                            }
                                            else
                                            {
                                                result = false;
                                                var vtodException =
                                                    VtodException.CreateException(ExceptionType.ECar, 1);
                                                logger.Warn(vtodException.ExceptionMessage.Message);
                                                throw vtodException;
                                            }
                                        }
                                        else if ((reservation.Service.Location.Dropoff.Address == null) &&
                                                 (reservation.Service.Location.Dropoff.AirportInfo != null))
                                        {

                                            //airport
                                            if (((reservation.Service.Location.Dropoff.AirportInfo.Arrival != null) &&
                                                 (reservation.Service.Location.Dropoff.AirportInfo.Departure !=
                                                  null)) ||
                                                ((reservation.Service.Location.Dropoff.AirportInfo.Arrival == null) &&
                                                 (reservation.Service.Location.Dropoff.AirportInfo.Departure ==
                                                  null)))
                                            {
                                                result = false;
                                                logger.Warn(Messages.Validation_ArrivalAndDepartureAirportInfo);
                                                throw VtodException.CreateValidationException(
                                                    Messages.Validation_ArrivalAndDepartureAirportInfo);
                                            }
                                            else if (reservation.Service.Location.Dropoff.AirportInfo.Arrival !=
                                                     null &&
                                                     reservation.Service.Location.Dropoff.AirportInfo.Departure ==
                                                     null)
                                            {
                                                //Arrival
                                                ecbp.DropOffAddressType = AddressType.Airport;
                                                ecbp.DropoffAirport =
                                                    reservation.Service.Location.Dropoff.AirportInfo.Arrival
                                                        .LocationCode;
                                                logger.Info("Dropoff AddressType: AirportInfo.Arrival");
                                            }
                                            else if (reservation.Service.Location.Dropoff.AirportInfo.Arrival ==
                                                     null &&
                                                     reservation.Service.Location.Dropoff.AirportInfo.Departure !=
                                                     null)
                                            {
                                                //Depature
                                                ecbp.DropOffAddressType = AddressType.Airport;
                                                ecbp.DropoffAirport =
                                                    reservation.Service.Location.Dropoff.AirportInfo
                                                        .Departure.LocationCode;
                                                logger.Info("Dropoff AddressType: AirportInfo.Depature");
                                            }

                                            //longitude and latitude
                                            if (result)
                                            {
                                                double? longitude = null;
                                                double? latitude = null;
                                                if (GetLongitudeAndLatitudeForAirport(ecbp.DropoffAirport,
                                                    out longitude, out latitude))
                                                {
                                                    ecbp.LongitudeForDropoffAirport = longitude.Value;
                                                    ecbp.LatitudeForDropoffAirport = latitude.Value;
                                                }
                                                else
                                                {
                                                    throw VtodException.CreateException(ExceptionType.ECar, 1010);                                                    
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (bookingResult.Settings != null && bookingResult.Settings.Booking != null)
                                    {
                                        if (bookingResult.Settings.Booking.Locations != null && bookingResult.Settings.Booking.Locations.Any())
                                        {
                                            var droppffLocation = bookingResult.Settings.Booking.Locations.Where(p => p.LocationType == UDI.VTOD.MTData.BookingWebService.LocationType.DropOff).FirstOrDefault();
                                            if (droppffLocation != null)
                                            {
                                                if (droppffLocation.AddressType.ToString() == AddressType.Airport)
                                                {
                                                    ecbp.DropOffAddressType = droppffLocation.AddressType.ToString();
                                                    ecbp.LatitudeForDropoffAirport = droppffLocation.Address.Latitude.ToDouble();
                                                    ecbp.LongitudeForDropoffAirport = droppffLocation.Address.ToDouble();
                                                }
                                                else
                                                {

                                                    Map.DTO.Address DropOffAddress = new Map.DTO.Address();
                                                    if (droppffLocation.Address.Suburb != null)
                                                        DropOffAddress.City = droppffLocation.Address.Suburb.Name;
                                                    DropOffAddress.Latitude = droppffLocation.Address.Latitude.ToString();
                                                    DropOffAddress.Longitude = droppffLocation.Address.Longitude.ToString();
                                                    if (droppffLocation.Address.Street != null)
                                                        DropOffAddress.Street = droppffLocation.Address.Street.Name;
                                                    DropOffAddress.StreetNo = droppffLocation.Address.Unit;
                                                    if (droppffLocation.Address.Street.Postcode != null)
                                                        DropOffAddress.ZipCode = droppffLocation.Address.Street.Postcode;
                                                    if (droppffLocation.Address.Suburb.Postcode != null)
                                                        DropOffAddress.ZipCode = droppffLocation.Address.Suburb.Postcode;
                                                    DropOffAddress.Geolocation = new UDI.Map.DTO.Geolocation();
                                                    DropOffAddress.Geolocation.Latitude = droppffLocation.Address.Latitude.ToDecimal();
                                                    DropOffAddress.Geolocation.Longitude = droppffLocation.Address.Longitude.ToDecimal();
                                                    ecbp.DropOffAddress = new Map.DTO.Address();
                                                    ecbp.DropOffAddress = DropOffAddress;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (bookingResult.Settings != null && bookingResult.Settings.Booking != null)
                            {
                                if (bookingResult.Settings.Booking.Locations != null && bookingResult.Settings.Booking.Locations.Any())
                                {
                                    var dropoffLocation = bookingResult.Settings.Booking.Locations.Where(p => p.LocationType == UDI.VTOD.MTData.BookingWebService.LocationType.DropOff).FirstOrDefault();
                                    if (dropoffLocation != null)
                                    {
                                        if (dropoffLocation.AddressType.ToString() == AddressType.Airport)
                                        {
                                            ecbp.DropOffAddressType = dropoffLocation.AddressType.ToString();
                                            ecbp.LatitudeForDropoffAirport = dropoffLocation.Address.Latitude.ToDouble();
                                            ecbp.LongitudeForDropoffAirport = dropoffLocation.Address.ToDouble();
                                        }
                                        else
                                        {
                                            Map.DTO.Address DropOffAddress = new Map.DTO.Address();
                                            if (dropoffLocation.Address.Suburb != null)
                                                DropOffAddress.City = dropoffLocation.Address.Suburb.Name;
                                            DropOffAddress.Latitude = dropoffLocation.Address.Latitude.ToString();
                                            DropOffAddress.Longitude = dropoffLocation.Address.Longitude.ToString();
                                            if (dropoffLocation.Address.Street != null)
                                                DropOffAddress.Street = dropoffLocation.Address.Street.Name;
                                            DropOffAddress.StreetNo = dropoffLocation.Address.Unit;
                                            if (dropoffLocation.Address.Street.Postcode != null)
                                                DropOffAddress.ZipCode = dropoffLocation.Address.Street.Postcode;
                                            if (dropoffLocation.Address.Suburb.Postcode != null)
                                                DropOffAddress.ZipCode = dropoffLocation.Address.Suburb.Postcode;
                                            DropOffAddress.Geolocation = new UDI.Map.DTO.Geolocation();
                                            DropOffAddress.Geolocation.Latitude = dropoffLocation.Address.Latitude.ToDecimal();
                                            DropOffAddress.Geolocation.Longitude = dropoffLocation.Address.Longitude.ToDecimal();
                                            ecbp.DropOffAddress = new Map.DTO.Address();
                                            ecbp.DropOffAddress = DropOffAddress;
                                        }
                                    }
                                }
                            }
                        }
                    }

                #endregion


                if (bookingResult.Settings != null && bookingResult.Settings.Booking != null)
                {
                    if (bookingResult.Settings.Booking.Locations != null && bookingResult.Settings.Booking.Locations.Any())
                    {
                        var dropoffLocation = bookingResult.Settings.Booking.Locations.Where(p => p.LocationType == UDI.VTOD.MTData.BookingWebService.LocationType.DropOff).FirstOrDefault();
                        if (dropoffLocation != null)
                            ecbp.RemarkForDropOff = dropoffLocation.Remark;
                    }
                }
                if (TrackTime != null)
                    TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidateDropOffAddress");
                swEachPart.Stop();
                logger.DebugFormat("Validate dropoff address Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                swEachPart.Restart();
                if (TrackTime != null)
                    TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidateDropoffDateTime");
                swEachPart.Stop();
                logger.DebugFormat("Validate dropoff time Duration:{0} ms.", swEachPart.ElapsedMilliseconds);


                swEachPart.Restart();

                #region Validate Available address type

                if (result)
                {

                    UtilityDomain ud = new UtilityDomain();
                    if (ecbp.PickupAddressType == AddressType.Address)
                    {
                        if (ecbp.PickupAddress.Geolocation == null)
                        {
                            result = false;
                            var vtodException =
                                VtodException.CreateFieldRequiredValidationException("PickupAddress.Geolocation");
                            logger.Warn(vtodException.ExceptionMessage.Message);
                            throw vtodException;
                        }

                        UtilityGetAirportNameRQ uganRQ = new UtilityGetAirportNameRQ();
                        uganRQ.Address = new Address();
                        uganRQ.Address.Latitude = ecbp.PickupAddress.Geolocation.Latitude.ToString();
                        uganRQ.Address.Longitude = ecbp.PickupAddress.Geolocation.Longitude.ToString();

                        UtilityGetAirportNameRS uganRS = ud.GetAirportName(tokenVTOD, uganRQ);
                        if (uganRS.Success != null)
                        {
                            if (!string.IsNullOrWhiteSpace(uganRS.AirportName))
                            {
                                //pickupAddressType = AddressType.Airport;
                                ecbp.PickupAddressType = AddressType.Airport;
                                ecbp.PickupAirport = uganRS.AirportName;
                                ecbp.LongitudeForPickupAirport = uganRQ.Address.Longitude.ToDouble();
                                ecbp.LatitudeForPickupAirport = uganRQ.Address.Longitude.ToDouble();
                            }
                        }

                    }

                    if (ecbp.DropOffAddressType == AddressType.Address)
                    {
                        if (ecbp.DropOffAddress.Geolocation == null)
                        {
                            result = false;
                            var vtodException =
                                VtodException.CreateFieldRequiredValidationException("DropOffAddress.Geolocation");
                            logger.Warn(vtodException.ExceptionMessage.Message);
                            throw vtodException;
                        }

                        UtilityGetAirportNameRQ uganRQ = new UtilityGetAirportNameRQ();
                        uganRQ.Address = new Address();
                        uganRQ.Address.Latitude = ecbp.DropOffAddress.Geolocation.Latitude.ToString();
                        uganRQ.Address.Longitude = ecbp.DropOffAddress.Geolocation.Longitude.ToString();

                        UtilityGetAirportNameRS uganRS = ud.GetAirportName(tokenVTOD, uganRQ);
                        if (uganRS.Success != null)
                        {
                            if (!string.IsNullOrWhiteSpace(uganRS.AirportName))
                            {
                                //dropoffAddressType = AddressType.Airport;
                                ecbp.DropOffAddressType = AddressType.Airport;
                                ecbp.DropoffAirport = uganRS.AirportName;
                                ecbp.LongitudeForDropoffAirport = uganRQ.Address.Longitude.ToDouble();
                                ecbp.LatitudeForDropoffAirport = uganRQ.Address.Longitude.ToDouble();
                            }
                        }
                    }

                    //check database setting                    
                    using (VTODEntities context = new VTODEntities())
                    {
                        long fleetId = ecbp.Fleet.Id;
                        if (context.ecar_fleet_availableaddresstype.Where(x => fleetId == x.FleetId).Any())
                        {
                            var ecarFleetAvailableAddressType =  context.ecar_fleet_availableaddresstype.Where(x => fleetId == x.FleetId)
                                                                                                        .Select(x => x)
                                                                                                        .FirstOrDefault();
                            //2. check if the type is available or not
                            //Ecar_Common_InvalidAddressTypeForFleet
                            if (AddressType.Address.ToString() == ecbp.PickupAddressType &&
                                AddressType.Address.ToString() == ecbp.DropOffAddressType)
                            {
                                //address to address
                                if (ecarFleetAvailableAddressType.AddressToAddress)
                                {
                                    result = true;
                                }
                                else
                                {
                                    result = false;
                                    throw VtodException.CreateException(ExceptionType.ECar, 1013);                                    
                                }
                            }
                            else if (AddressType.Address.ToString() == ecbp.PickupAddressType &&
                                     AddressType.Airport.ToString() == ecbp.DropOffAddressType)
                            {
                                //address to airport
                                if (ecarFleetAvailableAddressType.AddressToAirport)
                                {
                                    result = true;
                                }
                                else
                                {
                                    result = false;
                                    throw VtodException.CreateException(ExceptionType.ECar, 2020);
                                    //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                }
                            }
                            else if (AddressType.Address.ToString() == ecbp.PickupAddressType &&
                                     null == ecbp.DropOffAddressType)
                            {
                                //address to null
                                if (ecarFleetAvailableAddressType.AddressToNull)
                                {
                                    result = true;
                                }
                                else
                                {
                                    result = false;
                                    throw VtodException.CreateException(ExceptionType.ECar, 1013);
                                    //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                }
                            }
                            else if (AddressType.Airport.ToString() == ecbp.PickupAddressType &&
                                     AddressType.Address.ToString() == ecbp.DropOffAddressType)
                            {
                                //airport to address
                                if (ecarFleetAvailableAddressType.AirportToAddress)
                                {
                                    result = true;
                                }
                                else
                                {
                                    result = false;
                                    throw VtodException.CreateException(ExceptionType.ECar, 2019);
                                    //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                }
                            }
                            else if (AddressType.Airport.ToString() == ecbp.PickupAddressType &&
                                     AddressType.Airport.ToString() == ecbp.DropOffAddressType)
                            {
                                //airport to airport
                                if (ecarFleetAvailableAddressType.AirportToAirport)
                                {
                                    result = true;
                                }
                                else
                                {
                                    result = false;
                                    throw VtodException.CreateException(ExceptionType.ECar, 1013);
                                    //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                }
                            }
                            else if (AddressType.Airport.ToString() == ecbp.PickupAddressType &&
                                     null == ecbp.DropOffAddressType)
                            {
                                //airport to null
                                if (ecarFleetAvailableAddressType.AirportToNull)
                                {
                                    result = true;
                                }
                                else
                                {
                                    result = false;
                                    throw VtodException.CreateException(ExceptionType.ECar, 1013);                                    
                                }
                            }
                        }
                        else
                        {
                            result = false;
                            throw VtodException.CreateException(ExceptionType.ECar, 1011);                            
                        }
                    }


                }
                else
                {
                    result = false;
                    var vtodException = VtodException.CreateFieldRequiredValidationException("Paymnet");                    
                    logger.Warn(vtodException.ExceptionMessage.Message);
                    throw vtodException;                    
                }

                #endregion

                if (TrackTime != null)
                    TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility,"ValidateBookingRequest_ValidateAvailableAddressType");
                swEachPart.Stop();
                logger.DebugFormat("Validate available address type Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                swEachPart.Restart();

                #region Validate PaymentInfo

                if (request.Payments == null || request.Payments.Payments == null || !request.Payments.Payments.Any())
                {
                    result = true;
                    if (bookingResult.Settings.Booking.PaymentMethod != null)
                    {
                        if (bookingResult.Settings.Booking.PaymentMethod.ToString().ToLower() == "Cash".ToLower())
                        {
                            ecbp.PaymentType = Common.DTO.Enum.PaymentType.Cash;
                        }
                        else
                        {
                            ecbp.PaymentType = Common.DTO.Enum.PaymentType.PaymentCard;
                            ecbp.CreditCardID = bookingResult.Settings.Booking.PaymentMethod.ID.ToInt32();
                        }
                    }

                }
                else
                {
                    if (request.Payments.Payments.First().PaymentCard != null &&
                        request.Payments.Payments.First().Cash == null)
                    {
                        if (request.Payments.Payments.First().PaymentCard.CardNumber == null ||
                            string.IsNullOrWhiteSpace(request.Payments.Payments.First().PaymentCard.CardNumber.ID))
                        {
                            result = false;
                            var vtodException =
                                Common.DTO.VtodException.CreateFieldRequiredValidationException(
                                    "CardNumber and CardNumberID");
                            logger.Warn(vtodException.ExceptionMessage.Message);
                            throw vtodException; // new ValidationException(Messages.Validation_NullPayment);
                        }
                        else
                        {
                            ecbp.CreditCardID = request.Payments.Payments.First().PaymentCard.CardNumber.ID.ToInt32();
                            ecbp.PaymentType = Common.DTO.Enum.PaymentType.PaymentCard;


                            #region Pre-Authorze creadit card

                            //1. Get pre auth information
                            //ecbp.User.PreAuthCreditCardAmount
                            //ecbp.User.PreAuthCreditCardOption
                            //ecbp.MemberID



                            //2. Do pre authorization
                            if (ecbp.User.PreAuthCreditCardOption)
                            {
                                PaymentDomain pd = new PaymentDomain();
                                PreSaleVerificationResponse preAuthResult;
                                try
                                {
                                    preAuthResult = pd.PreSaleCardVerification(
                                        tokenVTOD,
                                        ecbp.CreditCardID.Value,
                                        ecbp.User.PreAuthCreditCardAmount,
                                        ecbp.MemberID.Value,
                                        Convert.ToInt32(ecbp.Fleet.SDS_FleetMerchantID.Value.ToString())
                                        );

                                }
                                catch (Exception ex)
                                {
                                    logger.Error("Unable to pre auth credit card information");
                                    logger.ErrorFormat("ex={0}", ex.Message);
                                    throw;
                                }
                                if (preAuthResult != null)
                                {
                                    if (preAuthResult.IsPrepaidCard == true)
                                    {
                                        throw VtodException.CreateException(ExceptionType.ECar, 2021);
                                    }
                                    if (preAuthResult.TransactionApproved == false)
                                    {
                                        throw VtodException.CreateException(ExceptionType.ECar, 2010);
                                    }

                                }
                                else
                                    throw VtodException.CreateException(ExceptionType.ECar, 2010);
                            }

                            #endregion


                        }
                    }
                    else if (request.Payments.Payments.First().Cash != null &&
                             request.Payments.Payments.First().PaymentCard == null &&
                             request.Payments.Payments.First().Cash.CashIndicator == true)
                    {
                        ecbp.PaymentType = Common.DTO.Enum.PaymentType.Cash;
                    }
                    else if (request.Payments.Payments.First().MiscChargeOrder != null &&
                             !string.IsNullOrWhiteSpace(
                                 request.Payments.Payments.First().MiscChargeOrder.OriginalIssuePlace) &&
                             request.Payments.Payments.First().MiscChargeOrder.OriginalIssuePlace.ToUpper() == "SDS")
                    {
                        #region Authorize                        
                        var membershipDomain = new UDI.VTOD.Domain.Membership.MembershipDomain();
                        #endregion

                    }
                    else if (request.Payments.Payments.First().MiscChargeOrder != null &&
                             !string.IsNullOrWhiteSpace(
                                 request.Payments.Payments.First().MiscChargeOrder.OriginalIssuePlace) &&
                             request.Payments.Payments.First().MiscChargeOrder.OriginalIssuePlace.ToUpper() ==
                             "CONSUMER")
                    {
                        #region Authorize
                        var membershipDomain = new UDI.VTOD.Domain.Membership.MembershipDomain();
                        #endregion

                    }
                    else
                    {
                        result = false;
                        logger.Warn(Messages.Validation_CreditCard);
                        throw VtodException.CreateValidationException(Messages.Validation_CreditCard);
                        // new ValidationException(Messages.General_BadCreditCard);
                    }
                }

                #endregion

                if (TrackTime != null)
                    TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility,
                        "ValidateBookingRequest_ValidatePaymentInfo");
                swEachPart.Stop();
                logger.DebugFormat("Validate payment Id Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                swEachPart.Restart();

                #region Get Gratuity

                try
                {
                    var tipInfo = bookingResult.Settings.Booking.TipInfo;
                    if (tipInfo != null)
                    {
                        ecbp.GratuityType = tipInfo.TipType.ToString();
                        var tipvalue = tipInfo.Tip;
                        ecbp.Gratuity = tipvalue.ToString();
                    }
                }
                catch (Exception ex)
                {
                    logger.Error("Ecar:Gratuity", ex);
                }

                #endregion

                if (TrackTime != null)
                    TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ GetGratuity");
                swEachPart.Stop();
                logger.DebugFormat("Get Gratuity Duration:{0} ms.", swEachPart.ElapsedMilliseconds);


                swEachPart.Restart();

                #region Check if it's fixed price zone

                if (result && (bool)ecbp.Fleet.OverWriteFixedPrice)
                {
                    var flatRates = new List<ECarFlatRateData>(); ;
                    ecar_fleet_zone pickupZone = null;
                    ecar_fleet_zone dropoffZone = null;

                    #region Get FlatRate
                    using (VTODEntities context = new VTODEntities())
                    {
                        long pickupfleetID = ecbp.Fleet.Id;
                        var tfzfList = context.ecar_fleet_zone_flatrate.Where(x => x.PickupZone.FleetId == pickupfleetID).Select(x => x).ToList();
                        if (tfzfList.Any())
                        {
                            foreach (var x in tfzfList)
                            {
                                var tfrd = new ECarFlatRateData { Amount = x.Amount, AmountUnit = x.AmountUnit, AppendTime = x.AppendTime, DropoffZoneId = x.DropoffZoneId, Id = x.Id, PickupZoneId = x.PickupZoneId };
                                tfrd.PickupZoneName = context.ecar_fleet_zone.Where(xx => xx.Id == tfrd.PickupZoneId).Select(xxx => xxx.Name).FirstOrDefault();
                                tfrd.DropOffZoneName = context.ecar_fleet_zone.Where(xx => xx.Id == tfrd.DropoffZoneId).Select(xxx => xxx.Name).FirstOrDefault();
                                flatRates.Add(tfrd);
                            }
                        }
                    }
                    #endregion

                    #region Pickup zone and dropoff zone
                    if (result)
                    {
                        //pickup zone
                        if ((ecbp.PickupAddressType == AddressType.Address && ecbp.PickupAddress.Geolocation != null && ecbp.PickupAddress.Geolocation.Latitude != 0 && ecbp.PickupAddress.Geolocation.Longitude != 0)
                            ||
                            (ecbp.PickupAddressType == AddressType.Airport))
                        {
                            ecar_fleet_zone zone = null;

                            if (ecbp.PickupAddressType == AddressType.Address)
                            {
                                zone = GetFleetZone(ecbp.Fleet.Id, ecbp.PickupAddress);
                            }
                            else if (ecbp.PickupAddressType == AddressType.Airport)
                            {
                                zone = GetFleetZoneByAirport(ecbp.Fleet.Id, ecbp.PickupAirport);
                            }

                            if (zone != null)
                            {
                                pickupZone = zone;
                            }
                        }
                    }
                    else
                    {
                        result = false;
                        logger.Warn("Cannot process pickup zone by previous invalid request");
                    }

                    if (result)
                    {
                        if (reservation.Service.Location.Dropoff != null)
                        {
                            //drop off zone
                            if ((ecbp.DropOffAddressType == AddressType.Address && ecbp.DropOffAddress.Geolocation != null && ecbp.DropOffAddress.Geolocation.Latitude != 0 && ecbp.DropOffAddress.Geolocation.Longitude != 0)
                                ||
                                (ecbp.DropOffAddressType == AddressType.Airport))
                            {
                                ecar_fleet_zone zone = null;

                                if (ecbp.DropOffAddressType == AddressType.Address)
                                {
                                    zone = GetFleetZone(ecbp.Fleet.Id, ecbp.DropOffAddress);
                                }
                                else if (ecbp.DropOffAddressType == AddressType.Airport)
                                {
                                    zone = GetFleetZoneByAirport(ecbp.Fleet.Id, ecbp.DropoffAirport);
                                }

                                if (zone != null)
                                {
                                    dropoffZone = zone;
                                }
                            }
                            else
                            {
                                result = false;
                                logger.Warn("Cannot process drop off zone by previous invalid request");
                            }
                        }
                    }
                    else
                    {
                        result = false;
                        logger.Warn("Cannot process drop off address by previous invalid request");
                    }
                    #endregion

                    #region Determine FlatRate or Rate
                    if (reservation.Service.Location.Dropoff != null)
                    {
                        long? pickupZoneId = null;
                        long? dropoffZoneId = null;

                        if ((pickupZone != null) && (dropoffZone != null))
                        {
                            pickupZoneId = pickupZone.Id;
                            dropoffZoneId = dropoffZone.Id;
                        }

                        if ((result) && pickupZoneId.HasValue && dropoffZoneId.HasValue && (flatRates.Any())
                            && flatRates.Any(x => x.PickupZoneId == pickupZoneId.Value && x.DropoffZoneId == dropoffZoneId.Value))
                        {
                            //flatrate
                            ecbp.IsFixedPrice = true;

                            #region Do calculation for flatrate

                            //caculate flatrate
                            var tfzf = flatRates.Where(x => x.PickupZoneId == pickupZoneId && x.DropoffZoneId == dropoffZoneId).Select(x => x).FirstOrDefault();

                            ecbp.FixedPrice = tfzf.Amount;

                            logger.Info("Flat rate found");

                            #endregion
                        }
                        else
                        {
                            ecbp.IsFixedPrice = false;
                        }
                    }
                    #endregion
                }
                else
                {
                    ecbp.IsFixedPrice = false;
                }

                #endregion

                if (TrackTime != null)
                    TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility,
                        "ValidateBookingRequest_CheckFixedPriceZone");
                swEachPart.Stop();
                logger.DebugFormat("Check fixed price zone  Duration:{0} ms.", swEachPart.ElapsedMilliseconds);
            }
            else
            {
                var vtodException = VtodException.CreateFieldRequiredValidationException("GroundReservation");
                logger.Warn(vtodException.ExceptionMessage.Message);
                throw vtodException;
            }

            #endregion

            sw.Stop();
            logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);


            #region Create eCar log

            if (result)
            {
                if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
                {
                    WriteECarLog(request.References.FirstOrDefault().ID.ToInt64(), ecbp.serviceAPIID, ECarServiceMethodType.ModifyBook, sw.ElapsedMilliseconds,
                        string.Empty, string.Empty, null, null);
                }
            }
            else
            {
                result = false;
                var vtodException = Common.DTO.VtodException.CreateException(ExceptionType.ECar, 2001);
                logger.Warn(vtodException.ExceptionMessage.Message);
                throw vtodException;
            }

            #endregion

            #region Track

            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateModifyBookingRequest");

            #endregion

            return result;
        }
        public bool ValidateTexasBookingRequest(TokenRS tokenVTOD, OTA_GroundBookRQ request, out ECarBookingParameter tbp, ecar_fleet fleet, int fleetTripCode)
        {
            bool result = true;
            Stopwatch sw = new Stopwatch();
            sw.Start();
            tbp = new ECarBookingParameter();
            tbp.Fleet = fleet;
            Stopwatch swEachPart = new Stopwatch();
            tbp.Fleet_TripCode = fleetTripCode;
            #region Track

            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");

            #endregion

            #region Validate Reservation

            if (request.GroundReservations.Any())
            {
               
                GroundReservation reservation = request.GroundReservations.FirstOrDefault();

                #region Set token, request

                tbp.tokenVTOD = tokenVTOD;
                tbp.request = request;

                #endregion

                #region Set Source and Device and ref

                if (request.TPA_Extensions != null && !string.IsNullOrWhiteSpace(request.TPA_Extensions.Source))
                {
                    tbp.Source = request.TPA_Extensions.Source;
                }

                if (request.TPA_Extensions != null && !string.IsNullOrWhiteSpace(request.TPA_Extensions.Device))
                {
                    tbp.Device = request.TPA_Extensions.Device;
                }

                if (!string.IsNullOrWhiteSpace(request.EchoToken))
                {
                    tbp.Ref = request.EchoToken;
                }

                #endregion

                swEachPart.Restart();

                #region Validate Member Info

                //As of this version, all requester should pass memberID.
                //We need to modify this based on some others who do not pass memberID
                if (request.TPA_Extensions == null && string.IsNullOrWhiteSpace(request.TPA_Extensions.MemberID))
                {
                    result = false;
                    var vtodException = VtodException.CreateFieldRequiredValidationException("MemberID");
                    //ExceptionType.Ecar, 2001);
                    logger.Warn(vtodException.ExceptionMessage.Message);
                    throw vtodException;
                    // new ValidationException(Messages.ecar_Common_UnableToFindFleetByAddressOrAirportInfo);
                }
                else
                {
                    tbp.MemberID = request.TPA_Extensions.MemberID.ToInt32();
                }

                #endregion

                if (TrackTime != null)
                    TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility,
                        "ValidateBookingRequest_ValidateMemberInfo");
                swEachPart.Stop();
                logger.DebugFormat("Validate member Info Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                swEachPart.Restart();

                #region Validate Customer

                if (reservation.Passenger != null)
                {
                    //Name
                    if (reservation.Passenger.Primary != null)
                    {
                        if (reservation.Passenger.Primary.PersonName != null)
                        {
                            if (string.IsNullOrWhiteSpace(reservation.Passenger.Primary.PersonName.GivenName) ||
                                string.IsNullOrWhiteSpace(reservation.Passenger.Primary.PersonName.Surname))
                            {
                                result = false;
                                var vtodException =
                                    VtodException.CreateFieldRequiredValidationException(
                                        "PassengerGivenName|PassengerSurname");
                                logger.Warn(vtodException.ExceptionMessage.Message);
                                throw vtodException; 
                            }
                            else
                            {
                                tbp.FirstName = reservation.Passenger.Primary.PersonName.GivenName.Trim();
                                tbp.LastName = reservation.Passenger.Primary.PersonName.Surname.Trim();
                                logger.Info("First name and last name are valid.");
                            }
                        }
                        else
                        {
                            result = false;
                            var vtodException = VtodException.CreateFieldRequiredValidationException("PassengerName");
                            logger.Warn(vtodException.ExceptionMessage.Message);
                            throw vtodException; 
                        }

                        //Telephone
                        if (reservation.Passenger.Primary.Telephones.Any())
                        {
                            Telephone p = reservation.Passenger.Primary.Telephones.FirstOrDefault();
                            string areacode = ExtractDigits(p.AreaCityCode);
                            string phonenumber = ExtractDigits(p.PhoneNumber);
                            if (areacode.Length.Equals(3) && phonenumber.Length.Equals(7))
                            {
                                tbp.PhoneNumber = string.Format("{0}{1}", areacode, phonenumber);
                                logger.Info("Phone number(s) is/are valid.");
                            }
                            else
                            {
                                result = false;
                                var vtodException = VtodException.CreateFieldFormatValidationException("Telephone",
                                    "10 digits");
                                logger.Warn(vtodException.ExceptionMessage.Message);
                                throw vtodException; 
                            }
                        }
                        else
                        {
                            result = false;
                            var vtodException = VtodException.CreateFieldRequiredValidationException("Telephones");
                            logger.Warn(vtodException.ExceptionMessage.Message);
                            throw vtodException;
                        }
                        if (reservation.Passenger.Primary.Emails.Any())
                        {
                            Email e = reservation.Passenger.Primary.Emails.FirstOrDefault();


                            string emailPattern = @"^(?("")("".+?(?<!\\)""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
                                                 @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]*\.)+[a-zA-Z0-9][\-a-zA-Z0-9]{0,22}[a-zA-Z0-9]))$";

                            if (!string.IsNullOrWhiteSpace(e.Value) && Regex.IsMatch(e.Value, emailPattern, RegexOptions.IgnorePatternWhitespace))
                            {
                                tbp.EmailAddress = e.Value;
                                logger.Info(Messages.ECar_Common_ValidReservationPassengerPrimaryTelephones);
                            }
                        }
                        if (result)
                        {
                            logger.InfoFormat("Customer firstname={0}, lastname={1}, phonenumber={2}", tbp.FirstName,
                                tbp.LastName, tbp.PhoneNumber);
                        }
                        else
                        {
                            result = false;
                            var vtodException = VtodException.CreateException(ExceptionType.ECar, 2008);
                            logger.Warn(vtodException.ExceptionMessage.Message);
                            throw vtodException; 
                        }
                    }
                    else
                    {
                        result = false;
                        var vtodException = VtodException.CreateFieldRequiredValidationException("PassengerPrimary");
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException; 
                    }
                }
                else
                {
                    result = false;
                    var vtodException = VtodException.CreateFieldRequiredValidationException("Passenger");
                    logger.Warn(vtodException.ExceptionMessage.Message);
                    throw vtodException; 
                }

                #endregion

                if (TrackTime != null)
                    TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidateCustomer");
                swEachPart.Stop();
                logger.DebugFormat("Validate Customer Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                swEachPart.Restart();

                #region Validate Pickup Address

                if (reservation.Service != null)
                {
                    if (reservation.Service.Location != null)
                    {
                        if (reservation.Service.Location.Pickup != null)
                        {
                            if (((reservation.Service.Location.Pickup.Address != null) &&
                                 (reservation.Service.Location.Pickup.AirportInfo != null)) ||
                                ((reservation.Service.Location.Pickup.Address == null) &&
                                 (reservation.Service.Location.Pickup.AirportInfo == null)))
                            {
                                result = false;
                                logger.Warn(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
                                throw new ValidationException(
                                    Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
                            }
                            else if ((reservation.Service.Location.Pickup.Address != null) &&
                                     (reservation.Service.Location.Pickup.AirportInfo == null))
                            {

                                if (
                                    (string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude) ||
                                     (string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude))) &&
                                    (string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.StreetNmbr) ||
                                     string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.AddressLine) ||
                                     string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CityName) ||
                                     string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.PostalCode) ||
                                     string.IsNullOrWhiteSpace(
                                         reservation.Service.Location.Pickup.Address.StateProv.StateCode) ||
                                     string.IsNullOrWhiteSpace(
                                         reservation.Service.Location.Pickup.Address.CountryName.Code)))
                                {
                                    result = false;
                                    logger.Warn(Messages.Validation_LocationDetail);
                                    throw new ValidationException(Messages.Validation_LocationDetail);
                                }
                                else if (
                                (!string.IsNullOrWhiteSpace(
                                    reservation.Service.Location.Pickup.Address.Longitude) &&
                                 (!string.IsNullOrWhiteSpace(
                                     reservation.Service.Location.Pickup.Address.Longitude))) &&
                                (string.IsNullOrWhiteSpace(
                                    reservation.Service.Location.Pickup.Address.StreetNmbr) ||
                                 string.IsNullOrWhiteSpace(
                                     reservation.Service.Location.Pickup.Address.AddressLine) ||
                                 string.IsNullOrWhiteSpace(
                                     reservation.Service.Location.Pickup.Address.CityName) ||
                                 string.IsNullOrWhiteSpace(
                                     reservation.Service.Location.Pickup.Address.PostalCode) ||
                                 string.IsNullOrWhiteSpace(
                                     reservation.Service.Location.Pickup.Address.StateProv.StateCode) ||
                                 string.IsNullOrWhiteSpace(
                                     reservation.Service.Location.Pickup.Address.CountryName.Code)))
                                {
                                    tbp.PickupAddressOnlyContainsLatAndLong = true;
                                    tbp.PickupAddressType = AddressType.Address;
                                    tbp.PickupAddress = ConvertAddress(reservation.Service.Location.Pickup);
                                }
                                else if (
                                    !string.IsNullOrWhiteSpace(
                                        reservation.Service.Location.Pickup.Address.AddressLine) &&
                                    !string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CityName) &&
                                    !string.IsNullOrWhiteSpace(
                                        reservation.Service.Location.Pickup.Address.PostalCode) &&
                                    !string.IsNullOrWhiteSpace(
                                        reservation.Service.Location.Pickup.Address.StateProv.StateCode) &&
                                    !string.IsNullOrWhiteSpace(
                                        reservation.Service.Location.Pickup.Address.CountryName.Code))
                                {
                                    //Address
                                    Map.DTO.Address pickupAddress = null;
                                    //string pickupAddressStr = GetAddressString(reservation.Service.Location.Pickup.Address);
                                    logger.Info("Skip MAP API Validation.");
                                    pickupAddress = ConvertAddress(reservation.Service.Location.Pickup);
                                    tbp.PickupAddressType = AddressType.Address;
                                    tbp.PickupAddress = pickupAddress;
                                    logger.Info("Pickup AddressType: Address");
                                }
                                else
                                {
                                    result = false;
                                    var vtodException = VtodException.CreateException(ExceptionType.ECar, 1);
                                    logger.Warn(vtodException.ExceptionMessage.Message);
                                    throw vtodException;
                                }
                            }
                            else if ((reservation.Service.Location.Pickup.Address == null) &&
                                     (reservation.Service.Location.Pickup.AirportInfo != null))
                            {
                                //airport
                                if (((reservation.Service.Location.Pickup.AirportInfo.Arrival != null) &&
                                     (reservation.Service.Location.Pickup.AirportInfo.Departure != null)) ||
                                    ((reservation.Service.Location.Pickup.AirportInfo.Arrival == null) &&
                                     (reservation.Service.Location.Pickup.AirportInfo.Departure == null)))
                                {
                                    result = false;
                                    logger.Warn(Messages.Validation_ArrivalAndDepartureAirportInfo);
                                    throw VtodException.CreateValidationException(
                                        Messages.Validation_ArrivalAndDepartureAirportInfo);
                                }
                                else if (reservation.Service.Location.Pickup.AirportInfo.Arrival != null &&
                                         reservation.Service.Location.Pickup.AirportInfo.Departure == null)
                                {
                                    //Arrival
                                    tbp.PickupAddressType = AddressType.Airport;
                                    tbp.PickupAirport =
                                        reservation.Service.Location.Pickup.AirportInfo.Arrival.LocationCode;
                                    logger.Info("Pickup AddressType: AirportInfo.Arrival");
                                }
                                else if (reservation.Service.Location.Pickup.AirportInfo.Arrival == null &&
                                         reservation.Service.Location.Pickup.AirportInfo.Departure != null)
                                {
                                    //Depature

                                    tbp.PickupAddressType = AddressType.Airport;
                                    tbp.PickupAirport =
                                        reservation.Service.Location.Pickup.AirportInfo.Departure
                                            .LocationCode;
                                    logger.Info("Pickup AddressType: AirportInfo.Depature");
                                }

                                //longitude and latitude
                                if (result)
                                {
                                    double? longitude = null;
                                    double? latitude = null;
                                    if (GetLongitudeAndLatitudeForAirport(tbp.PickupAirport, out longitude,
                                        out latitude))
                                    {
                                        tbp.LongitudeForPickupAirport = longitude.Value;
                                        tbp.LatitudeForPickupAirport = latitude.Value;
                                    }
                                    else
                                    {
                                        throw VtodException.CreateException(ExceptionType.ECar, 1010);
                                     
                                    }
                                }
                            }
                        }
                        else
                        {
                            result = false;
                            var vtodException = VtodException.CreateFieldRequiredValidationException("Pickup");
                            logger.Warn(vtodException.ExceptionMessage.Message);
                            throw vtodException; 
                        }
                    }
                    else
                    {
                        result = false;
                        var vtodException =
                            VtodException.CreateFieldRequiredValidationException("Reservation.Service.Location");
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException; 
                    }
                }
                else
                {
                    result = false;
                    var vtodException = VtodException.CreateFieldRequiredValidationException("GroundReservation.Service");
                    logger.Warn(vtodException.ExceptionMessage.Message);
                    throw vtodException;
                }

                #endregion

                if (TrackTime != null)
                    TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility,
                        "ValidateBookingRequest_ValidatePickupAddress");
                swEachPart.Stop();
                logger.DebugFormat("Validate Pickup address Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                swEachPart.Restart();

                #region Validate Fleet

                if (result)
                {
                  
                    if (tbp.Fleet != null)//GetFleet(tbp.PickupAddressType, tbp.PickupAddress, tbp.PickupAirport, out fleet))
                    {
                        //tbp.Fleet = fleet;
                        logger.InfoFormat("Fleet found. ID={0}", tbp.Fleet.Id);

                        if (tbp.Fleet != null)
                        {
                            tbp.serviceAPIID =
                        GetECarFleetServiceAPIPreference(tbp.Fleet.Id, ECarServiceAPIPreferenceConst.Book).ServiceAPIId;
                            #region Fetching Endpoint Names
                            List<string> endpointNames = MtdataEndpointConfiguration(tbp.serviceAPIID, tbp.Fleet.Id);
                            if (endpointNames != null && endpointNames.Any())
                            {
                                tbp.AuthenticationServiceEndpointName = endpointNames[0];
                                tbp.BookingWebServiceEndpointName = endpointNames[1];
                                tbp.OsiWebServiceEndpointName = endpointNames[2];
                                tbp.AddressWebServiceEndpointName = endpointNames[3];
                            }
                            #endregion
                        }
                    }
                    else
                    {
                        result = false;
                        var vtodException = VtodException.CreateException(ExceptionType.ECar, 1011);
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException;
                       
                    }
                }
                else
                {
                    result = false;
                    var vtodException = VtodException.CreateException(ExceptionType.ECar, 2001);
                    logger.Warn(vtodException.ExceptionMessage.Message);
                    throw vtodException;
                 
                }

                #endregion

                if (TrackTime != null)
                    TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidateFleet");
                swEachPart.Stop();
                logger.DebugFormat("Validate fleet Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                swEachPart.Restart();

                #region Validate User

                if (result)
                {
                    ecar_fleet_user user = null;
                    if (GetECarFleetUser(tbp.Fleet.Id, tokenVTOD.Username, out user))
                    {
                        tbp.User = user;
                        logger.Info("This user has sufficient privilege.");
                    }
                    else
                    {
                        result = false;
                        var vtodException = VtodException.CreateException(ExceptionType.ECar, 102);
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException;
                    }
                }

                #endregion

                if (TrackTime != null)
                    TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidateUser");
                swEachPart.Stop();
                logger.DebugFormat("Validate user Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                swEachPart.Restart();

                #region Validate PickupDateTime

                if (result)
                {
                    //pickiup now

                    #region determine Pick me up now

                    if (request.TPA_Extensions != null && request.TPA_Extensions.PickMeUpNow.HasValue)
                    {
                        tbp.PickupNow = request.TPA_Extensions.PickMeUpNow.Value;
                    }
                    else
                    {
                        try
                        {
                            tbp.PickupNow =
                                reservation.Service.Location.Pickup.DateTime.ToDateTime()
                                    .Value.ToUtc(tbp.Fleet.ServerUTCOffset) <= DateTime.UtcNow.AddMinutes(5);
                        }
                        catch
                        {
                        }
                    }

                    #endregion

                    #region Validate Pick me up now against DB fleet

                    bool pickMeUpNow = true;
                    bool pickMeUpLater = true;
                    GetPickMeupOption(tbp.Fleet, out pickMeUpNow, out pickMeUpLater);
                    #region Check BlackOut
                    CheckBlackOut(tbp.Fleet, request);


                    #endregion

                    #endregion


                    #region determine pick me up now Time

                    DateTime pickupDateTime;
                    //if (DateTime.TryParseExact(reservation.Service.Location.Pickup.DateTime, ConfigurationManager.AppSettings["DateTimeFormatForPickupAndDropoff"], null, DateTimeStyles.None, out pickupDateTime))
                    if (DateTime.TryParse(reservation.Service.Location.Pickup.DateTime, out pickupDateTime))
                    {
                        tbp.PickupDateTime = pickupDateTime;
                        logger.Info("PickupDateTime is correct");
                    }
                    else if (tbp.PickupNow &&
                             !DateTime.TryParse(reservation.Service.Location.Pickup.DateTime, out pickupDateTime))
                    {
                        tbp.PickupDateTime = DateTime.UtcNow.FromUtc(tbp.Fleet.ServerUTCOffset);
                        logger.InfoFormat(
                            "There is no Pickup datetime for this request. But it has pickup now value. So the pickup time will set as {0:yyyy/MM/dd HH:mm:ss}.",
                            tbp.PickupDateTime);
                    }
                    else
                    {
                        result = false;
                        var vtodException = VtodException.CreateFieldFormatValidationException("PickupDateTime",
                            "DateTime");
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException;
                    }

                    #endregion
                }
                else
                {
                    result = false;
                    var vtodException = VtodException.CreateException(ExceptionType.ECar, 2001);
                    logger.Warn(vtodException.ExceptionMessage.Message);
                    throw vtodException;
                }

                #endregion

                if (TrackTime != null)
                    TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility,
                        "ValidateBookingRequest_ValidatePickupDateTime");
                swEachPart.Stop();
                logger.DebugFormat("Validate PickupTime Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                swEachPart.Restart();

                #region Validate duplicated booking trip

                if (result)
                {
                    if (IsAbleToBookTrip(tbp))
                    {
                        logger.Info("This trip can be booked.");
                    }
                    else
                    {
                        result = false;
                        var vtodException = VtodException.CreateException(ExceptionType.ECar, 2006);
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException;
                    }
                }
                else
                {
                    result = false;
                    var vtodException = VtodException.CreateException(ExceptionType.ECar, 2001);
                    logger.Warn(vtodException.ExceptionMessage.Message);
                    throw vtodException;
                }

                #endregion

                if (TrackTime != null)
                    TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility,
                        "ValidateBookingRequest_ValidateDuplicatedBookingTrip");
                swEachPart.Stop();
                logger.DebugFormat("Validate duplicated trip Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                swEachPart.Restart();

                #region Validate Dropoff Address

                //DropOff is optional: By_Pouyan
                if (reservation.Service.Location != null && reservation.Service.Location.Dropoff != null)
                    if (result)
                    {
                        if (reservation.Service != null)
                        {
                            if (reservation.Service.Location != null)
                            {
                                if (reservation.Service.Location.Dropoff != null)
                                {
                                    using (VTODEntities context = new VTODEntities())
                                    {
                                        if (((reservation.Service.Location.Dropoff.Address != null) &&
                                             (reservation.Service.Location.Dropoff.AirportInfo != null)) ||
                                            ((reservation.Service.Location.Dropoff.Address == null) &&
                                             (reservation.Service.Location.Dropoff.AirportInfo == null)))
                                        {
                                            result = false;
                                            logger.Warn(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
                                            throw VtodException.CreateValidationException(
                                                Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
                                        }
                                        else if ((reservation.Service.Location.Dropoff.Address != null) &&
                                                 (reservation.Service.Location.Dropoff.AirportInfo == null))
                                        {
                                            //Address
                                            if (
                                                (string.IsNullOrWhiteSpace(
                                                    reservation.Service.Location.Dropoff.Address.Longitude) ||
                                                 (string.IsNullOrWhiteSpace(
                                                     reservation.Service.Location.Dropoff.Address.Longitude))) &&
                                                (string.IsNullOrWhiteSpace(
                                                    reservation.Service.Location.Dropoff.Address.StreetNmbr) ||
                                                 string.IsNullOrWhiteSpace(
                                                     reservation.Service.Location.Dropoff.Address.AddressLine) ||
                                                 string.IsNullOrWhiteSpace(
                                                     reservation.Service.Location.Dropoff.Address.CityName) ||
                                                 string.IsNullOrWhiteSpace(
                                                     reservation.Service.Location.Dropoff.Address.PostalCode) ||
                                                 string.IsNullOrWhiteSpace(
                                                     reservation.Service.Location.Dropoff.Address.StateProv.StateCode) ||
                                                 string.IsNullOrWhiteSpace(
                                                     reservation.Service.Location.Dropoff.Address.CountryName.Code)))
                                            //if (string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CountryName.Code))
                                            {
                                                result = false;
                                                logger.Warn(Messages.Validation_LocationDetail);
                                                throw VtodException.CreateValidationException(
                                                    Messages.Validation_LocationDetail);
                                            }
                                            else if (
                                                (!string.IsNullOrWhiteSpace(
                                                    reservation.Service.Location.Dropoff.Address.Longitude) &&
                                                 (!string.IsNullOrWhiteSpace(
                                                     reservation.Service.Location.Dropoff.Address.Longitude))) &&
                                                (string.IsNullOrWhiteSpace(
                                                    reservation.Service.Location.Dropoff.Address.StreetNmbr) ||
                                                 string.IsNullOrWhiteSpace(
                                                     reservation.Service.Location.Dropoff.Address.AddressLine) ||
                                                 string.IsNullOrWhiteSpace(
                                                     reservation.Service.Location.Dropoff.Address.CityName) ||
                                                 string.IsNullOrWhiteSpace(
                                                     reservation.Service.Location.Dropoff.Address.PostalCode) ||
                                                 string.IsNullOrWhiteSpace(
                                                     reservation.Service.Location.Dropoff.Address.StateProv
                                                         .StateCode) ||
                                                 string.IsNullOrWhiteSpace(
                                                     reservation.Service.Location.Dropoff.Address.CountryName
                                                         .Code)))
                                            {
                                                tbp.DropOffAddressOnlyContainsLatAndLong = true;
                                                tbp.DropOffAddressType = AddressType.Address;
                                                tbp.DropOffAddress =
                                                    ConvertAddress(reservation.Service.Location.Dropoff);
                                            }
                                            else if (
                                                !string.IsNullOrWhiteSpace(
                                                    reservation.Service.Location.Dropoff.Address.AddressLine) &&
                                                !string.IsNullOrWhiteSpace(
                                                    reservation.Service.Location.Dropoff.Address.CityName) &&
                                                !string.IsNullOrWhiteSpace(
                                                    reservation.Service.Location.Dropoff.Address.PostalCode) &&
                                                !string.IsNullOrWhiteSpace(
                                                    reservation.Service.Location.Dropoff.Address.StateProv
                                                        .StateCode) &&
                                                !string.IsNullOrWhiteSpace(
                                                    reservation.Service.Location.Dropoff.Address.CountryName.Code))
                                            {
                                                Map.DTO.Address dropoffAddress = null;
                                                string dropoffAddressStr =
                                                    GetAddressString(reservation.Service.Location.Dropoff.Address);
                                                logger.Info("Skip MAP API Validation.");
                                                dropoffAddress =
                                                    ConvertAddress(reservation.Service.Location.Dropoff);
                                                tbp.DropOffAddress = dropoffAddress;
                                                tbp.DropOffAddressType =
                                                    UDI.VTOD.Domain.ECar.Const.AddressType.Address;
                                                logger.Info("Dropoff AddressType=Address");
                                            }
                                            else
                                            {
                                                result = false;
                                                var vtodException =
                                                    VtodException.CreateException(ExceptionType.ECar, 1);
                                                logger.Warn(vtodException.ExceptionMessage.Message);
                                                throw vtodException;
                                            }
                                        }
                                        else if ((reservation.Service.Location.Dropoff.Address == null) &&
                                                 (reservation.Service.Location.Dropoff.AirportInfo != null))
                                        {

                                            //airport
                                            if (((reservation.Service.Location.Dropoff.AirportInfo.Arrival != null) &&
                                                 (reservation.Service.Location.Dropoff.AirportInfo.Departure !=
                                                  null)) ||
                                                ((reservation.Service.Location.Dropoff.AirportInfo.Arrival == null) &&
                                                 (reservation.Service.Location.Dropoff.AirportInfo.Departure ==
                                                  null)))
                                            {
                                                result = false;
                                                logger.Warn(Messages.Validation_ArrivalAndDepartureAirportInfo);
                                                throw VtodException.CreateValidationException(
                                                    Messages.Validation_ArrivalAndDepartureAirportInfo);
                                            }
                                            else if (reservation.Service.Location.Dropoff.AirportInfo.Arrival !=
                                                     null &&
                                                     reservation.Service.Location.Dropoff.AirportInfo.Departure ==
                                                     null)
                                            {
                                                //Arrival
                                                tbp.DropOffAddressType = AddressType.Airport;
                                                tbp.DropoffAirport =
                                                    reservation.Service.Location.Dropoff.AirportInfo.Arrival
                                                        .LocationCode;
                                                logger.Info("Dropoff AddressType: AirportInfo.Arrival");
                                            }
                                            else if (reservation.Service.Location.Dropoff.AirportInfo.Arrival ==
                                                     null &&
                                                     reservation.Service.Location.Dropoff.AirportInfo.Departure !=
                                                     null)
                                            {
                                                //Depature
                                                tbp.DropOffAddressType = AddressType.Airport;
                                                tbp.DropoffAirport =
                                                    reservation.Service.Location.Dropoff.AirportInfo
                                                        .Departure.LocationCode;
                                                logger.Info("Dropoff AddressType: AirportInfo.Depature");
                                            }

                                            //longitude and latitude
                                            if (result)
                                            {
                                                double? longitude = null;
                                                double? latitude = null;
                                                if (GetLongitudeAndLatitudeForAirport(tbp.DropoffAirport,
                                                    out longitude, out latitude))
                                                {
                                                    tbp.LongitudeForDropoffAirport = longitude.Value;
                                                    tbp.LatitudeForDropoffAirport = latitude.Value;
                                                }
                                                else
                                                {
                                                    throw VtodException.CreateException(ExceptionType.ECar, 1010);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                result = false;
                                var vtodException =
                                    VtodException.CreateFieldRequiredValidationException("Reservation.Service.Location");
                                logger.Warn(vtodException.ExceptionMessage.Message);
                                throw vtodException;
                            }
                        }
                        else
                        {
                            result = false;
                            var vtodException =
                                VtodException.CreateFieldRequiredValidationException("GroundReservation.Service");
                            logger.Warn(vtodException.ExceptionMessage.Message);
                            throw vtodException; 
                        }
                    }
                    else
                    {
                        result = false;
                        var vtodException = VtodException.CreateException(ExceptionType.ECar, 2001);
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException;
                      
                    }

                #endregion

                if (TrackTime != null)
                    TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility,
                        "ValidateBookingRequest_ValidateDropOffAddress");
                swEachPart.Stop();
                logger.DebugFormat("Validate dropoff address Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                swEachPart.Restart();
                if (TrackTime != null)
                    TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility,
                        "ValidateBookingRequest_ValidateDropoffDateTime");
                swEachPart.Stop();
                logger.DebugFormat("Validate dropoff time Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                swEachPart.Restart();

                #region Validate Available address type

                if (result)
                {
                    UtilityDomain ud = new UtilityDomain();
                    if (tbp.PickupAddressType == AddressType.Address)
                    {
                        if (tbp.PickupAddress.Geolocation == null)
                        {
                            result = false;
                            var vtodException =
                                VtodException.CreateFieldRequiredValidationException("PickupAddress.Geolocation");
                            logger.Warn(vtodException.ExceptionMessage.Message);
                            throw vtodException;
                        }

                        UtilityGetAirportNameRQ uganRQ = new UtilityGetAirportNameRQ();
                        uganRQ.Address = new Address();
                        uganRQ.Address.Latitude = tbp.PickupAddress.Geolocation.Latitude.ToString();
                        uganRQ.Address.Longitude = tbp.PickupAddress.Geolocation.Longitude.ToString();

                        UtilityGetAirportNameRS uganRS = ud.GetAirportName(tokenVTOD, uganRQ);
                        if (uganRS.Success != null)
                        {
                            if (!string.IsNullOrWhiteSpace(uganRS.AirportName))
                            {
                                tbp.PickupAddressType = AddressType.Airport;
                                tbp.PickupAirport = uganRS.AirportName;
                                tbp.LongitudeForPickupAirport = uganRQ.Address.Longitude.ToDouble();
                                tbp.LatitudeForPickupAirport = uganRQ.Address.Longitude.ToDouble();
                            }
                        }

                    }

                    if (tbp.DropOffAddressType == AddressType.Address)
                    {
                        if (tbp.DropOffAddress.Geolocation == null)
                        {
                            result = false;
                            var vtodException =
                                VtodException.CreateFieldRequiredValidationException("DropOffAddress.Geolocation");
                            logger.Warn(vtodException.ExceptionMessage.Message);
                            throw vtodException;
                        }

                        UtilityGetAirportNameRQ uganRQ = new UtilityGetAirportNameRQ();
                        uganRQ.Address = new Address();
                        uganRQ.Address.Latitude = tbp.DropOffAddress.Geolocation.Latitude.ToString();
                        uganRQ.Address.Longitude = tbp.DropOffAddress.Geolocation.Longitude.ToString();

                        UtilityGetAirportNameRS uganRS = ud.GetAirportName(tokenVTOD, uganRQ);
                        if (uganRS.Success != null)
                        {
                            if (!string.IsNullOrWhiteSpace(uganRS.AirportName))
                            {
                                tbp.DropOffAddressType = AddressType.Airport;
                                tbp.DropoffAirport = uganRS.AirportName;
                                tbp.LongitudeForDropoffAirport = uganRQ.Address.Longitude.ToDouble();
                                tbp.LatitudeForDropoffAirport = uganRQ.Address.Longitude.ToDouble();
                            }
                        }
                    }

                   
                    using (VTODEntities context = new VTODEntities())
                    {
                        long fleetId = tbp.Fleet.Id;
                        if (context.ecar_fleet_availableaddresstype.Where(x => fleetId == x.FleetId).Any())
                        {
                            ecar_fleet_availableaddresstype tfa =
                                context.ecar_fleet_availableaddresstype.Where(x => fleetId == x.FleetId)
                                    .Select(x => x)
                                    .FirstOrDefault();
                            if (AddressType.Address.ToString() == tbp.PickupAddressType &&
                                AddressType.Address.ToString() == tbp.DropOffAddressType)
                            {
                                //address to address
                                if (tfa.AddressToAddress)
                                {
                                    result = true;
                                }
                                else
                                {
                                    result = false;
                                    throw VtodException.CreateException(ExceptionType.ECar, 1013);
                                }
                            }
                            else if (AddressType.Address.ToString() == tbp.PickupAddressType &&
                                     AddressType.Airport.ToString() == tbp.DropOffAddressType)
                            {
                                //address to airport
                                if (tfa.AddressToAirport)
                                {
                                    result = true;
                                }
                                else
                                {
                                    result = false;
                                    throw VtodException.CreateException(ExceptionType.ECar, 2020);
                                }
                            }
                            else if (AddressType.Address.ToString() == tbp.PickupAddressType &&
                                     null == tbp.DropOffAddressType)
                            {
                                //address to null
                                if (tfa.AddressToNull)
                                {
                                    result = true;
                                }
                                else
                                {
                                    result = false;
                                    throw VtodException.CreateException(ExceptionType.ECar, 1013);
                                    //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                }
                            }
                            else if (AddressType.Airport.ToString() == tbp.PickupAddressType &&
                                     AddressType.Address.ToString() == tbp.DropOffAddressType)
                            {
                                //airport to address
                                if (tfa.AirportToAddress)
                                {
                                    result = true;
                                }
                                else
                                {
                                    result = false;
                                    throw VtodException.CreateException(ExceptionType.ECar, 2019);
                                    //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                }
                            }
                            else if (AddressType.Airport.ToString() == tbp.PickupAddressType &&
                                     AddressType.Airport.ToString() == tbp.DropOffAddressType)
                            {
                                //airport to airport
                                if (tfa.AirportToAirport)
                                {
                                    result = true;
                                }
                                else
                                {
                                    result = false;
                                    throw VtodException.CreateException(ExceptionType.ECar, 1013);
                                    //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                }
                            }
                            else if (AddressType.Airport.ToString() == tbp.PickupAddressType &&
                                     null == tbp.DropOffAddressType)
                            {
                                //airport to null
                                if (tfa.AirportToNull)
                                {
                                    result = true;
                                }
                                else
                                {
                                    result = false;
                                    throw VtodException.CreateException(ExceptionType.ECar, 1013);
                                    //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                }
                            }
                        }
                        else
                        {
                            result = false;
                            throw VtodException.CreateException(ExceptionType.ECar, 1011);
                           
                        }
                    }


                }
                else
                {
                    result = false;
                    var vtodException = VtodException.CreateFieldRequiredValidationException("Paymnet");
               
                    logger.Warn(vtodException.ExceptionMessage.Message);
                    throw vtodException;
                  
                }

                #endregion

                if (TrackTime != null)
                    TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility,
                        "ValidateBookingRequest_ValidateAvailableAddressType");
                swEachPart.Stop();
                logger.DebugFormat("Validate available address type Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                swEachPart.Restart();

                #region Validate PaymentInfo

                //As of this version, all requester should pass payment Info.
                //We need to modify this based on some others who do not pass payementInfo
                if (request.Payments == null || request.Payments.Payments == null || !request.Payments.Payments.Any())
                {
                    result = false;
                    var vtodException = Common.DTO.VtodException.CreateFieldRequiredValidationException("Payment");
                    logger.Warn(vtodException.ExceptionMessage.Message);
                    throw vtodException; // new ValidationException(Messages.Validation_NullPayment);
                }
                else
                {
                    if (request.Payments.Payments.First().PaymentCard != null &&
                        request.Payments.Payments.First().Cash == null)
                    {
                        if (request.Payments.Payments.First().PaymentCard.CardNumber == null ||
                            string.IsNullOrWhiteSpace(request.Payments.Payments.First().PaymentCard.CardNumber.ID))
                        {
                            result = false;
                            var vtodException =
                                Common.DTO.VtodException.CreateFieldRequiredValidationException(
                                    "CardNumber and CardNumberID");
                            logger.Warn(vtodException.ExceptionMessage.Message);
                            throw vtodException; // new ValidationException(Messages.Validation_NullPayment);
                        }
                        else
                        {
                            tbp.CreditCardID = request.Payments.Payments.First().PaymentCard.CardNumber.ID.ToInt32();
                            tbp.PaymentType = Common.DTO.Enum.PaymentType.PaymentCard;


                            #region Pre-Authorze creadit card

                            //1. Get pre auth information
                            //tbp.User.PreAuthCreditCardAmount
                            //tbp.User.PreAuthCreditCardOption
                            //tbp.MemberID



                            //2. Do pre authorization
                            if (tbp.User.PreAuthCreditCardOption)
                            {
                                PaymentDomain pd = new PaymentDomain();
                                PreSaleVerificationResponse preAuthResult;
                                try
                                {
                                    preAuthResult = pd.PreSaleCardVerification(
                                        tokenVTOD,
                                        tbp.CreditCardID.Value,
                                        tbp.User.PreAuthCreditCardAmount,
                                        tbp.MemberID.Value,
                                        Convert.ToInt32(tbp.Fleet.SDS_FleetMerchantID.Value.ToString())
                                        );

                                }
                                catch (Exception ex)
                                {
                                    logger.Error("Unable to pre auth credit card information");
                                    logger.ErrorFormat("ex={0}", ex.Message);
                                    throw;
                                }
                                if (preAuthResult != null)
                                {
                                    if (preAuthResult.IsPrepaidCard == true)
                                    {
                                        throw VtodException.CreateException(ExceptionType.ECar, 2021);
                                    }
                                    if (preAuthResult.TransactionApproved == false)
                                    {
                                        throw VtodException.CreateException(ExceptionType.ECar, 2010);
                                    }

                                }
                                else
                                    throw VtodException.CreateException(ExceptionType.ECar, 2010);
                            }

                            #endregion


                        }
                    }
                    else if (request.Payments.Payments.First().Cash != null &&
                             request.Payments.Payments.First().PaymentCard == null &&
                             request.Payments.Payments.First().Cash.CashIndicator == true)
                    {
                        tbp.PaymentType = Common.DTO.Enum.PaymentType.Cash;
                    }
                    else if (request.Payments.Payments.First().MiscChargeOrder != null &&
                             !string.IsNullOrWhiteSpace(
                                 request.Payments.Payments.First().MiscChargeOrder.OriginalIssuePlace) &&
                             request.Payments.Payments.First().MiscChargeOrder.OriginalIssuePlace.ToUpper() == "SDS")
                    {
                        #region Authorize

                        //var membershipController = new UDI.SDS.MembershipController(tokenVTOD, TrackTime);
                        var membershipDomain = new UDI.VTOD.Domain.Membership.MembershipDomain();
                        //if (membershipDomain.IsUserInPaymentRole(tokenVTOD.Username, Common.DTO.Enum.PaymentType.ChargedBySDS.ToString()))
                        //{
                        //	tbp.PaymentType = Common.DTO.Enum.PaymentType.ChargedBySDS;
                        //}
                        //else
                        //{
                        //	result = false;
                        //	logger.Warn(Messages.Validation_InvalidPaymentType);
                        //	throw VtodException.CreateValidationException(Messages.Validation_InvalidPaymentType);// new ValidationException(Messages.General_InvalidPaymentType);
                        //}

                        #endregion

                    }
                    else if (request.Payments.Payments.First().MiscChargeOrder != null &&
                             !string.IsNullOrWhiteSpace(
                                 request.Payments.Payments.First().MiscChargeOrder.OriginalIssuePlace) &&
                             request.Payments.Payments.First().MiscChargeOrder.OriginalIssuePlace.ToUpper() ==
                             "CONSUMER")
                    {
                        #region Authorize

                        //var membershipController = new UDI.SDS.MembershipController(tokenVTOD, TrackTime);
                        var membershipDomain = new UDI.VTOD.Domain.Membership.MembershipDomain();
                        //if (membershipDomain.IsUserInPaymentRole(tokenVTOD.Username, Common.DTO.Enum.PaymentType.ChargedByConsumer.ToString()))
                        //{
                        //	tbp.PaymentType = Common.DTO.Enum.PaymentType.ChargedByConsumer;
                        //}
                        //else
                        //{
                        //	result = false;
                        //	logger.Warn(Messages.Validation_InvalidPaymentType);
                        //	throw VtodException.CreateValidationException(Messages.Validation_InvalidPaymentType);// ValidationException(Messages.General_InvalidPaymentType);
                        //}

                        #endregion

                    }
                    else
                    {
                        result = false;
                        logger.Warn(Messages.Validation_CreditCard);
                        throw VtodException.CreateValidationException(Messages.Validation_CreditCard);
                        // new ValidationException(Messages.General_BadCreditCard);
                    }
                }

                #endregion

                if (TrackTime != null)
                    TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility,
                        "ValidateBookingRequest_ValidatePaymentInfo");
                swEachPart.Stop();
                logger.DebugFormat("Validate payment Id Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                swEachPart.Restart();

                #region Get Gratuity

                try
                {
                    var rateQuialifier = request.GroundReservations.First().RateQualifiers.First();
                    if (rateQuialifier.SpecialInputs != null)
                    {
                        var gratuities = rateQuialifier.SpecialInputs.Where(s => s.Name.ToLower() == "gratuity").ToList();

                        if (gratuities != null && gratuities.Any())
                        {
                            tbp.Gratuity = gratuities.First().Value;
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.Error("ECar:Gratuity", ex);
                }

                #endregion

                if (TrackTime != null)
                    TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ GetGratuity");
                swEachPart.Stop();
                logger.DebugFormat("Get Gratuity Duration:{0} ms.", swEachPart.ElapsedMilliseconds);


                swEachPart.Restart();

                #region Get PromisedETA

                try
                {
                    var rateQuialifier = request.GroundReservations.First().RateQualifiers.First();
                    if (request.TPA_Extensions.PickMeUpNow.HasValue && request.TPA_Extensions.PickMeUpNow == true)
                    {
                        if (rateQuialifier.SpecialInputs != null)
                        {
                            var promisedetas =
                                rateQuialifier.SpecialInputs.Where(s => s.Name.ToLower() == "promisedeta").ToList();

                            if (promisedetas != null && promisedetas.Any())
                            {
                                tbp.PromisedETA = promisedetas.First().Value.ToInt32();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.Error("ECar:PromisedETA", ex);
                }

                #endregion

                if (TrackTime != null)
                    TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ PromisedETA");
                swEachPart.Stop();
                logger.DebugFormat("Get PromisedETA Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                swEachPart.Restart();
                #region Adding Tip to MTData
                //***********************************************************************************************************
                //**************** we are adding tip just for MTData in its domain calss: MTDataECarService *****************
                //**************** so if you add Tip to all ECar in ECarDomain calss please comment this block **************
                //***********************************************************************************************************

                var specialInputs = request.GroundReservations.First().RateQualifiers.First().SpecialInputs;
                if (specialInputs != null && specialInputs.Any())
                {
                    var gratuity = specialInputs.Where(s => s.Name.ToLower().Trim() == "gratuity").FirstOrDefault();
                    if (gratuity != null)
                    {
                        var addingText = string.Format(" Tip is {0}", gratuity.Value);
                        request.GroundReservations.First().Service.Location.Pickup.Remark += addingText;
                    }
                }
                #endregion
                if (TrackTime != null)
                    TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility,
                        "Adding Tip To Remarks");
                swEachPart.Stop();
                logger.DebugFormat("Adding Tip To Remarks:{0} ms.", swEachPart.ElapsedMilliseconds);

                swEachPart.Restart();

                #region Check if it's fixed price zone

                if (result && (bool)tbp.Fleet.OverWriteFixedPrice)
                {
                    var flatRates = new List<ECarFlatRateData>(); ;
                    ecar_fleet_zone pickupZone = null;
                    ecar_fleet_zone dropoffZone = null;

                    #region Get FlatRate
                    using (VTODEntities context = new VTODEntities())
                    {
                        long pickupfleetID = tbp.Fleet.Id;
                        var tfzfList = context.ecar_fleet_zone_flatrate.Where(x => x.PickupZone.FleetId == pickupfleetID).Select(x => x).ToList();
                        if (tfzfList.Any())
                        {
                            foreach (var x in tfzfList)
                            {
                                var tfrd = new ECarFlatRateData { Amount = x.Amount, AmountUnit = x.AmountUnit, AppendTime = x.AppendTime, DropoffZoneId = x.DropoffZoneId, Id = x.Id, PickupZoneId = x.PickupZoneId };
                                tfrd.PickupZoneName = context.ecar_fleet_zone.Where(xx => xx.Id == tfrd.PickupZoneId).Select(xxx => xxx.Name).FirstOrDefault();
                                tfrd.DropOffZoneName = context.ecar_fleet_zone.Where(xx => xx.Id == tfrd.DropoffZoneId).Select(xxx => xxx.Name).FirstOrDefault();
                                flatRates.Add(tfrd);
                            }
                        }
                    }
                    #endregion

                    #region Pickup zone and dropoff zone
                    if (result)
                    {
                        //pickup zone
                        if ((tbp.PickupAddressType == AddressType.Address && tbp.PickupAddress.Geolocation != null && tbp.PickupAddress.Geolocation.Latitude != 0 && tbp.PickupAddress.Geolocation.Longitude != 0)
                            ||
                            (tbp.PickupAddressType == AddressType.Airport))
                        {
                            ecar_fleet_zone zone = null;

                            if (tbp.PickupAddressType == AddressType.Address)
                            {
                                zone = GetFleetZone(tbp.Fleet.Id, tbp.PickupAddress);
                            }
                            else if (tbp.PickupAddressType == AddressType.Airport)
                            {
                                zone = GetFleetZoneByAirport(tbp.Fleet.Id, tbp.PickupAirport);
                            }

                            if (zone != null)
                            {
                                pickupZone = zone;
                            }
                        }
                    }
                    else
                    {
                        result = false;
                        logger.Warn("Cannot process pickup zone by previous invalid request");
                    }

                    if (result)
                    {
                        if (reservation.Service.Location.Dropoff != null)
                        {
                            //drop off zone
                            if ((tbp.DropOffAddressType == AddressType.Address && tbp.DropOffAddress.Geolocation != null && tbp.DropOffAddress.Geolocation.Latitude != 0 && tbp.DropOffAddress.Geolocation.Longitude != 0)
                                ||
                                (tbp.DropOffAddressType == AddressType.Airport))
                            {
                                ecar_fleet_zone zone = null;

                                if (tbp.DropOffAddressType == AddressType.Address)
                                {
                                    zone = GetFleetZone(tbp.Fleet.Id, tbp.DropOffAddress);
                                }
                                else if (tbp.DropOffAddressType == AddressType.Airport)
                                {
                                    zone = GetFleetZoneByAirport(tbp.Fleet.Id, tbp.DropoffAirport);
                                }

                                if (zone != null)
                                {
                                    dropoffZone = zone;
                                }
                            }
                            else
                            {
                                result = false;
                                logger.Warn("Cannot process drop off zone by previous invalid request");
                            }
                        }
                    }
                    else
                    {
                        result = false;
                        logger.Warn("Cannot process drop off address by previous invalid request");
                    }
                    #endregion

                    #region Determine FlatRate or Rate
                    if (reservation.Service.Location.Dropoff != null)
                    {
                        long? pickupZoneId = null;
                        long? dropoffZoneId = null;

                        if ((pickupZone != null) && (dropoffZone != null))
                        {
                            pickupZoneId = pickupZone.Id;
                            dropoffZoneId = dropoffZone.Id;
                        }

                        if ((result) && pickupZoneId.HasValue && dropoffZoneId.HasValue && (flatRates.Any())
                            && flatRates.Any(x => x.PickupZoneId == pickupZoneId.Value && x.DropoffZoneId == dropoffZoneId.Value))
                        {
                            //flatrate
                            tbp.IsFixedPrice = true;

                            #region Do calculation for flatrate

                            //caculate flatrate
                            var tfzf = flatRates.Where(x => x.PickupZoneId == pickupZoneId && x.DropoffZoneId == dropoffZoneId).Select(x => x).FirstOrDefault();

                            tbp.FixedPrice = tfzf.Amount;

                            logger.Info("Flat rate found");

                            #endregion
                        }
                        else
                        {
                            tbp.IsFixedPrice = false;
                        }
                    }
                    #endregion
                }
                else
                {
                    tbp.IsFixedPrice = false;
                }

                #endregion

                if (TrackTime != null)
                    TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility,
                        "ValidateBookingRequest_CheckFixedPriceZone");
                swEachPart.Stop();
                logger.DebugFormat("Check fixed price zone  Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                swEachPart.Restart();

                #region Create this trip into database

                if (result)
                {
                    tbp.Trip = CreateNewTrip(tbp, tokenVTOD.Username);
                }
                else
                {
                    result = false;
                    var vtodException = VtodException.CreateException(ExceptionType.ECar, 2001);
                    logger.Warn(vtodException.ExceptionMessage.Message);
                    throw vtodException; 
                }

                #endregion

                if (TrackTime != null)
                    TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility,
                        "ValidateBookingRequest_ GreateTripInDatabase");
                swEachPart.Stop();
                logger.DebugFormat("CreateNewTrip in db  Duration:{0} ms.", swEachPart.ElapsedMilliseconds);


            }
            else
            {
                var vtodException = VtodException.CreateFieldRequiredValidationException("GroundReservation");
                logger.Warn(vtodException.ExceptionMessage.Message);
            }

            #endregion

            sw.Stop();
            logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);


            #region Create ECar log

            if (result)
            {
                if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
                {
                    WriteECarLog(tbp.Trip.Id, tbp.serviceAPIID, ECarServiceMethodType.Book, sw.ElapsedMilliseconds,
                        string.Empty, string.Empty, null, null);
                }
            }
            else
            {
                result = false;
                var vtodException = Common.DTO.VtodException.CreateException(ExceptionType.ECar, 2001);
                // ("CardNumber and CardNumberID");
                logger.Warn(vtodException.ExceptionMessage.Message);
                throw vtodException; // new ValidationException(Messages.Validation_NullPayment);
            }

            #endregion

            #region Track

            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest");

            #endregion


            return result;
        }

        public List<string> GetRemarks(string userName, long fleetID)
        {
            List<string> remarksList = new List<string>();
            Stopwatch sw = new Stopwatch();
            sw.Start();
            SP_ECar_GetRemarksForPickUp_DropOff_Result remarks = null;
            using (VTODEntities context = new VTODEntities())
            {
                remarks = context.SP_ECar_GetRemarksForPickUp_DropOff(userName, fleetID).FirstOrDefault();
            }
            sw.Stop();
            logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
            if (remarks != null)
            {
                remarksList.Add(remarks.RemarkForPickUpCash);
                remarksList.Add(remarks.RemarkForDropOffCash);
                remarksList.Add(remarks.RemarkForPickUpCreditCard);
                remarksList.Add(remarks.RemarkForDropOffCreditCard);
            }
            return remarksList.ToList<string>();
        }

        public bool ValidateTexasGetEstimationRequest(TokenRS tokenVTOD, OTA_GroundAvailRQ reservation, out ECarGetEstimationParameter tgep)
        {
            bool result = true;
            Stopwatch sw = new Stopwatch();
            sw.Start();
            tgep = new ECarGetEstimationParameter();

            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
            #endregion

            if (reservation.Service != null)
            {
                #region Token
                tgep.tokenVTOD = tokenVTOD;
                #endregion

                #region Validate Pickup Datetime
                if (reservation.Service.Pickup.AirportInfo != null)
                {
                    if (reservation.Service.Pickup != null && reservation.Service.Pickup.Airline != null && string.IsNullOrWhiteSpace(reservation.Service.Pickup.Airline.FlightDateTime))
                    {
                        var vtodException = VtodException.CreateFieldRequiredValidationException("Pickup datetime");
                        throw vtodException;
                    }
                    else
                    {
                        tgep.PickupDateTime = DateTime.Parse(reservation.Service.Pickup.Airline.FlightDateTime);
                    }
                }
                else if (string.IsNullOrWhiteSpace(reservation.Service.Pickup.DateTime))
                {
                    var vtodException = VtodException.CreateFieldRequiredValidationException("Pickup datetime");
                    throw vtodException;
                }
                else if(!string.IsNullOrWhiteSpace(reservation.Service.Pickup.DateTime))
                {
                    tgep.PickupDateTime = DateTime.Parse(reservation.Service.Pickup.DateTime);
                }
                #endregion

                #region Validate Pickup address
                if (reservation.Service.Pickup != null)
                {
                    if (((reservation.Service.Pickup.Address != null) && (reservation.Service.Pickup.AirportInfo != null)) || ((reservation.Service.Pickup.Address == null) && (reservation.Service.Pickup.AirportInfo == null)))
                    {
                        result = false;
                        logger.Warn(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
                        throw new ValidationException(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
                    }
                    else if (reservation.Service.Pickup.Address != null && reservation.Service.Pickup.AirportInfo == null)
                    {
                        if ((string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.Longitude) || (string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.CountryName.Code)))
                        {
                            result = false;
                            logger.Warn(Messages.Validation_LocationDetail);
                            throw new ValidationException(Messages.Validation_LocationDetail);
                        }
                        else if ((!string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.Longitude) && (!string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.CountryName.Code)))
                        {
                            tgep.PickupAddressOnlyContainsLatAndLong = true;
                            tgep.PickupAddressType = AddressType.Address;
                            tgep.PickupAddress = ConvertAddress(reservation.Service.Pickup);
                        }
                        else if (!string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.AddressLine) && !string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.CityName) && !string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.PostalCode) && !string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.CountryName.Code))
                        {
                            //Address
                            Map.DTO.Address pickupAddress = null;
                            string pickupAddressStr = GetAddressString(reservation.Service.Pickup.Address);
                            logger.Info("Skip MAP API Validation.");
                            pickupAddress = ConvertAddress(reservation.Service.Pickup);
                            tgep.PickupAddressType = AddressType.Address;
                            tgep.PickupAddress = pickupAddress;
                            logger.Info("Pickup AddressType: Address");
                        }
                        else
                        {
                            result = false;
                            var vtodException = VtodException.CreateException(ExceptionType.ECar, 1);
                            logger.Warn(vtodException.ExceptionMessage.Message);
                            throw vtodException;
                        }
                    }
                    else if ((reservation.Service.Pickup.Address == null) && (reservation.Service.Pickup.AirportInfo != null))
                    {
                        //airport
                        if (((reservation.Service.Pickup.AirportInfo.Arrival != null) && (reservation.Service.Pickup.AirportInfo.Departure != null)) || ((reservation.Service.Pickup.AirportInfo.Arrival == null) && (reservation.Service.Pickup.AirportInfo.Departure == null)))
                        {
                            result = false;
                            logger.Warn(Messages.Validation_ArrivalAndDepartureAirportInfo);
                            throw VtodException.CreateValidationException(Messages.Validation_ArrivalAndDepartureAirportInfo);
                        }
                        else if (reservation.Service.Pickup.AirportInfo.Arrival != null && reservation.Service.Pickup.AirportInfo.Departure == null)
                        {
                            //Arrival
                            tgep.PickupAddressType = AddressType.Airport;
                            tgep.PickupAirport = reservation.Service.Pickup.AirportInfo.Arrival.LocationCode;
                            tgep.PickupAddress = new Map.DTO.Address();
                            decimal longtitude = 0;
                            decimal latitude = 0;
                            if (decimal.TryParse(reservation.Service.Pickup.AirportInfo.Arrival.Latitude, out latitude) && decimal.TryParse(reservation.Service.Pickup.AirportInfo.Arrival.Longitude, out longtitude))
                            {
                                tgep.PickupAddress.Geolocation = new Map.DTO.Geolocation { Latitude = latitude, Longitude = longtitude };
                            }
                            logger.Info("Pickup AddressType: AirportInfo.Arrival");
                        }
                        else if (reservation.Service.Pickup.AirportInfo.Arrival == null && reservation.Service.Pickup.AirportInfo.Departure != null)
                        {
                            //Depature

                            tgep.PickupAddressType = AddressType.Airport;
                            tgep.PickupAirport = reservation.Service.Pickup.AirportInfo.Departure.LocationCode;
                            tgep.PickupAddress = new Map.DTO.Address();
                            decimal longtitude = 0;
                            decimal latitude = 0;
                            if (decimal.TryParse(reservation.Service.Pickup.AirportInfo.Departure.Latitude, out latitude) && decimal.TryParse(reservation.Service.Pickup.AirportInfo.Departure.Longitude, out longtitude))
                            {
                                tgep.PickupAddress.Geolocation = new Map.DTO.Geolocation { Latitude = latitude, Longitude = longtitude };
                            }
                            logger.Info("Pickup AddressType: AirportInfo.Depature");
                        }

                        //longitude and latitude
                        if (result)
                        {
                            double? longitude = null;
                            double? latitude = null;
                            if (GetLongitudeAndLatitudeForAirport(tgep.PickupAirport, out longitude, out latitude))
                            {
                                tgep.LongitudeForPickupAirport = longitude.Value;
                                tgep.LatitudeForPickupAirport = latitude.Value;
                            }
                            else
                            {
                                throw VtodException.CreateException(ExceptionType.ECar, 1010);
                            }
                        }

                    }
                }
                else
                {
                    result = false;
                    logger.Warn("request.Service.Pickup is null");
                }

                #endregion

                #region Validate Pickup Fleet
                if (result)
                {
                    ecar_fleet fleet = null;
                    if (GetFleet(tgep.PickupAddressType, tgep.PickupAddress, tgep.PickupAirport, out fleet))
                    {
                        tgep.Fleet = fleet;
                        logger.InfoFormat("Fleet found. ID={0}", fleet.Id);
                        #region Fetcing the Endpoints
                        if (tgep.Fleet != null)
                        {
                            tgep.serviceAPIID =
                                GetECarFleetServiceAPIPreference(tgep.Fleet.Id, ECarServiceAPIPreferenceConst.GetEstimation).ServiceAPIId;
                            #region Fetching Endpoint Names
                            List<string> endpointNames = MtdataEndpointConfiguration(tgep.serviceAPIID, tgep.Fleet.Id);
                            if (endpointNames != null && endpointNames.Any())
                            {
                                tgep.AuthenticationServiceEndpointName = endpointNames[0];
                                tgep.BookingWebServiceEndpointName = endpointNames[1];
                                tgep.OsiWebServiceEndpointName = endpointNames[2];
                                tgep.AddressWebServiceEndpointName = endpointNames[3];
                            }
                            #endregion
                        }
                        #endregion
                    }
                    else
                    {
                        result = false;
                        logger.Warn("Unable to find fleet by this address or landmark.");
                    }
                }
                else
                {
                    result = false;
                    logger.Warn("Unable to find fleet by invalid address or landmark.");
                }
                #endregion

                #region Validate Drop off address
                if (result)
                {
                    //drop off address can be address or airportInfo
                    if (reservation.Service.Dropoff != null)
                    {
                        using (VTODEntities context = new VTODEntities())
                        {
                            if (((reservation.Service.Dropoff.Address != null) && (reservation.Service.Dropoff.AirportInfo != null)) || ((reservation.Service.Dropoff.Address == null) && (reservation.Service.Dropoff.AirportInfo == null)))
                            {
                                result = false;
                                logger.Warn(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
                                throw VtodException.CreateValidationException(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
                            }
                            else if ((reservation.Service.Dropoff.Address != null) && (reservation.Service.Dropoff.AirportInfo == null))
                            {
                                //Address
                                if ((string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.Longitude) || (string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.CountryName.Code)))
                                {
                                    result = false;
                                    logger.Warn(Messages.Validation_LocationDetail);
                                    throw VtodException.CreateValidationException(Messages.Validation_LocationDetail);
                                }
                                else if ((!string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.Longitude) && (!string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.CountryName.Code)))
                                {
                                    tgep.DropOffAddressOnlyContainsLatAndLong = true;
                                    tgep.DropOffAddressType = AddressType.Address;
                                    tgep.DropOffAddress = ConvertAddress(reservation.Service.Dropoff);
                                }
                                else if (!string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.AddressLine) && !string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.CityName) && !string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.PostalCode) && !string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.CountryName.Code))
                                {
                                    Map.DTO.Address dropoffAddress = null;
                                    string dropoffAddressStr = GetAddressString(reservation.Service.Dropoff.Address);
                                    logger.Info("Skip MAP API Validation.");
                                    dropoffAddress = ConvertAddress(reservation.Service.Dropoff);

                                    tgep.DropOffAddress = dropoffAddress;
                                    tgep.DropOffAddressType = AddressType.Address;
                                    logger.Info("Dropoff AddressType=Address");
                                }
                                else
                                {
                                    result = false;
                                    var vtodException = VtodException.CreateException(ExceptionType.ECar, 1);
                                    logger.Warn(vtodException.ExceptionMessage.Message);
                                    throw vtodException;
                                }
                            }
                            else if ((reservation.Service.Dropoff.Address == null) && (reservation.Service.Dropoff.AirportInfo != null))
                            {

                                //airport
                                if (((reservation.Service.Dropoff.AirportInfo.Arrival != null) && (reservation.Service.Dropoff.AirportInfo.Departure != null)) || ((reservation.Service.Dropoff.AirportInfo.Arrival == null) && (reservation.Service.Dropoff.AirportInfo.Departure == null)))
                                {
                                    result = false;
                                    logger.Warn(Messages.Validation_ArrivalAndDepartureAirportInfo);
                                    throw VtodException.CreateValidationException(Messages.Validation_ArrivalAndDepartureAirportInfo);
                                }
                                else if (reservation.Service.Dropoff.AirportInfo.Arrival != null && reservation.Service.Dropoff.AirportInfo.Departure == null)
                                {
                                    //Arrival
                                    tgep.DropOffAddressType = AddressType.Airport;
                                    tgep.DropoffAirport = reservation.Service.Dropoff.AirportInfo.Arrival.LocationCode;
                                    tgep.DropOffAddress = new Map.DTO.Address();
                                    decimal longtitude = 0;
                                    decimal latitude = 0;
                                    if (decimal.TryParse(reservation.Service.Dropoff.AirportInfo.Arrival.Latitude, out latitude) && decimal.TryParse(reservation.Service.Dropoff.AirportInfo.Arrival.Longitude, out longtitude))
                                    {
                                        tgep.DropOffAddress.Geolocation = new Map.DTO.Geolocation { Latitude = latitude, Longitude = longtitude };
                                    }
                                    logger.Info("Dropoff AddressType: AirportInfo.Arrival");
                                }
                                else if (reservation.Service.Dropoff.AirportInfo.Arrival == null && reservation.Service.Dropoff.AirportInfo.Departure != null)
                                {
                                    //Depature
                                    tgep.DropOffAddressType = AddressType.Airport;
                                    tgep.DropoffAirport = reservation.Service.Dropoff.AirportInfo.Departure.LocationCode;
                                    tgep.DropOffAddress = new Map.DTO.Address();
                                    decimal longtitude = 0;
                                    decimal latitude = 0;
                                    if (decimal.TryParse(reservation.Service.Dropoff.AirportInfo.Departure.Latitude, out latitude) && decimal.TryParse(reservation.Service.Dropoff.AirportInfo.Departure.Longitude, out longtitude))
                                    {
                                        tgep.DropOffAddress.Geolocation = new Map.DTO.Geolocation { Latitude = latitude, Longitude = longtitude };
                                    }
                                    logger.Info("Dropoff AddressType: AirportInfo.Depature");
                                }

                                //longitude and latitude
                                if (result)
                                {
                                    double? longitude = null;
                                    double? latitude = null;
                                    if (GetLongitudeAndLatitudeForAirport(tgep.DropoffAirport, out longitude, out latitude))
                                    {
                                        tgep.LongitudeForDropoffAirport = longitude.Value;
                                        tgep.LatitudeForDropoffAirport = latitude.Value;
                                    }
                                    else
                                    {
                                        throw VtodException.CreateException(ExceptionType.ECar, 1010);
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    result = false;
                    logger.Warn("Cannot get estimation by previous invalid request ");
                }
                #endregion

                #region Validate Available address type
                if (result)
                {
                    //string pickupAddressType = tgep.PickupAddressType;
                    //string dropoffAddressType = tgep.DropOffAddressType;

                    //verify the address type of pickup address and drop off address
                    UtilityDomain ud = new UtilityDomain();
                    if (tgep.PickupAddressType == AddressType.Address.ToString())
                    {
                        UtilityGetAirportNameRQ uganRQ = new UtilityGetAirportNameRQ();
                        uganRQ.Address = new Address();
                        //tgep.PickupAddress.Geolocation = new Map.DTO.Geolocation();
                        uganRQ.Address.Latitude = tgep.PickupAddress.Geolocation.Latitude.ToString();
                        uganRQ.Address.Longitude = tgep.PickupAddress.Geolocation.Longitude.ToString();

                        UtilityGetAirportNameRS uganRS = ud.GetAirportName(tokenVTOD, uganRQ);
                        if (uganRS.Success != null)
                        {
                            if (!string.IsNullOrWhiteSpace(uganRS.AirportName))
                            {
                                //pickupAddressType = AddressType.Airport;

                                tgep.PickupAddressType = AddressType.Airport;
                                tgep.PickupAirport = uganRS.AirportName;
                                tgep.LongitudeForPickupAirport = uganRQ.Address.Longitude.ToDouble();
                                tgep.LatitudeForPickupAirport = uganRQ.Address.Longitude.ToDouble();
                            }
                        }

                    }

                    if (tgep.DropOffAddressType == AddressType.Address.ToString())
                    {
                        UtilityGetAirportNameRQ uganRQ = new UtilityGetAirportNameRQ();
                        uganRQ.Address = new Address();
                        //tgep.DropoffAddress.Geolocation = new Map.DTO.Geolocation();
                        uganRQ.Address.Latitude = tgep.DropOffAddress.Geolocation.Latitude.ToString();
                        uganRQ.Address.Longitude = tgep.DropOffAddress.Geolocation.Longitude.ToString();

                        UtilityGetAirportNameRS uganRS = ud.GetAirportName(tokenVTOD, uganRQ);
                        if (uganRS.Success != null)
                        {
                            if (!string.IsNullOrWhiteSpace(uganRS.AirportName))
                            {
                                //dropoffAddressType = AddressType.Airport;
                                tgep.DropOffAddressType = AddressType.Airport;
                                tgep.DropoffAirport = uganRS.AirportName;
                                tgep.LongitudeForDropoffAirport = uganRQ.Address.Longitude.ToDouble();
                                tgep.LatitudeForDropoffAirport = uganRQ.Address.Longitude.ToDouble();
                            }
                        }
                    }

                    //check database setting
                    //1. get ecar_fleet_availableaddresstype
                    using (VTODEntities context = new VTODEntities())
                    {
                        long fleetId = tgep.Fleet.Id;
                        if (context.ecar_fleet_availableaddresstype.Where(x => fleetId == x.FleetId).Any())
                        {
                            ecar_fleet_availableaddresstype tfa = context.ecar_fleet_availableaddresstype.Where(x => fleetId == x.FleetId).Select(x => x).FirstOrDefault();
                            //2. check if the type is available or not
                            //ecar_Common_InvalidAddressTypeForFleet
                            if (AddressType.Address.ToString() == tgep.PickupAddressType && AddressType.Address.ToString() == tgep.DropOffAddressType)
                            {
                                //address to address
                                if (tfa.AddressToAddress)
                                {
                                    result = true;
                                }
                                else
                                {
                                    result = false;
                                    throw VtodException.CreateException(ExceptionType.ECar, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                }
                            }
                            else if (AddressType.Address.ToString() == tgep.PickupAddressType && AddressType.Airport.ToString() == tgep.DropOffAddressType)
                            {
                                //address to airport
                                if (tfa.AddressToAirport)
                                {
                                    result = true;
                                }
                                else
                                {
                                    result = false;
                                    throw VtodException.CreateException(ExceptionType.ECar, 2020); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                }
                            }
                            else if (AddressType.Address.ToString() == tgep.PickupAddressType && null == tgep.DropOffAddressType)
                            {
                                //address to null
                                if (tfa.AddressToNull)
                                {
                                    result = true;
                                }
                                else
                                {
                                    result = false;
                                    throw VtodException.CreateException(ExceptionType.ECar, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                }
                            }
                            else if (AddressType.Airport.ToString() == tgep.PickupAddressType && AddressType.Address.ToString() == tgep.DropOffAddressType)
                            {
                                //airport to address
                                if (tfa.AirportToAddress)
                                {
                                    result = true;
                                }
                                else
                                {
                                    result = false;
                                    throw VtodException.CreateException(ExceptionType.ECar, 2019); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                }
                            }
                            else if (AddressType.Airport.ToString() == tgep.PickupAddressType && AddressType.Airport.ToString() == tgep.DropOffAddressType)
                            {
                                //airport to airport
                                if (tfa.AirportToAirport)
                                {
                                    result = true;
                                }
                                else
                                {
                                    result = false;
                                    throw VtodException.CreateException(ExceptionType.ECar, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                }
                            }
                            else if (AddressType.Airport.ToString() == tgep.PickupAddressType && null == tgep.DropOffAddressType)
                            {
                                //airport to null
                                if (tfa.AirportToNull)
                                {
                                    result = true;
                                }
                                else
                                {
                                    result = false;
                                    throw VtodException.CreateException(ExceptionType.ECar, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                }
                            }
                        }
                        else
                        {
                            result = false;
                            throw VtodException.CreateException(ExceptionType.ECar, 1011);// VtodException.CreateException(ExceptionType.ecar, 1011);// new ValidationException(Messages.ecar_Common_UnableToGetAvailableFleets);
                        }
                    }


                }
                else
                {
                    result = false;
                    var vtodException = Common.DTO.VtodException.CreateFieldRequiredValidationException("Payment");
                    logger.Warn(vtodException.ExceptionMessage.Message);
                    throw vtodException; // new ValidationException(Messages.Validation_NullPayment);
                }
                #endregion

                if (result && (bool)tgep.Fleet.OverWriteFixedPrice)
                {
                    #region Get FlatRate
                    using (VTODEntities context = new VTODEntities())
                    {
                        long pickupfleetID = tgep.Fleet.Id;
                        List<ecar_fleet_zone_flatrate> tfzfList = context.ecar_fleet_zone_flatrate.Where(x => x.PickupZone.FleetId == pickupfleetID).Select(x => x).ToList();
                        if (tfzfList.Any())
                        {
                            tgep.Info_FlatRates = new List<ECarFlatRateData>();
                            foreach (var x in tfzfList)
                            {
                                var tfrd = new ECarFlatRateData { Amount = x.Amount, AmountUnit = x.AmountUnit, AppendTime = x.AppendTime, DropoffZoneId = x.DropoffZoneId, Id = x.Id, PickupZoneId = x.PickupZoneId };
                                tfrd.PickupZoneName = context.ecar_fleet_zone.Where(xx => xx.Id == tfrd.PickupZoneId).Select(xxx => xxx.Name).FirstOrDefault();
                                tfrd.DropOffZoneName = context.ecar_fleet_zone.Where(xx => xx.Id == tfrd.DropoffZoneId).Select(xxx => xxx.Name).FirstOrDefault();
                                tgep.Info_FlatRates.Add(tfrd);
                            }
                        }
                    }
                    #endregion

                    #region Pickup zone and dropoff zone
                    if (result)
                    {
                        //pickup zone
                        if ((tgep.PickupAddressType == AddressType.Address && tgep.PickupAddress.Geolocation != null && tgep.PickupAddress.Geolocation.Latitude != 0 && tgep.PickupAddress.Geolocation.Longitude != 0)
                            ||
                            (tgep.PickupAddressType == AddressType.Airport))
                        {
                            ecar_fleet_zone zone = null;

                            if (tgep.PickupAddressType == AddressType.Address)
                            {
                                zone = GetFleetZone(tgep.Fleet.Id, tgep.PickupAddress);
                            }
                            else if (tgep.PickupAddressType == AddressType.Airport)
                            {
                                zone = GetFleetZoneByAirport(tgep.Fleet.Id, tgep.PickupAirport);
                            }

                            if (zone != null)
                            {
                                tgep.PickupZone = zone;
                            }
                        }
                    }
                    else
                    {
                        result = false;
                        logger.Warn("Cannot process pickup zone by previous invalid request");
                    }

                    if (result)
                    {
                        if (reservation.Service.Dropoff != null)
                        {
                            //drop off zone
                            if ((tgep.DropOffAddressType == AddressType.Address && tgep.DropOffAddress.Geolocation != null && tgep.DropOffAddress.Geolocation.Latitude != 0 && tgep.DropOffAddress.Geolocation.Longitude != 0)
                                ||
                                (tgep.DropOffAddressType == AddressType.Airport))
                            {
                                ecar_fleet_zone zone = null;

                                if (tgep.DropOffAddressType == AddressType.Address)
                                {
                                    zone = GetFleetZone(tgep.Fleet.Id, tgep.DropOffAddress);
                                }
                                else if (tgep.DropOffAddressType == AddressType.Airport)
                                {
                                    zone = GetFleetZoneByAirport(tgep.Fleet.Id, tgep.DropoffAirport);
                                }

                                if (zone != null)
                                {
                                    tgep.DropoffZone = zone;
                                }
                            }
                            else
                            {
                                result = false;
                                logger.Warn("Cannot process drop off zone by previous invalid request");
                            }
                        }
                    }
                    else
                    {
                        result = false;
                        logger.Warn("Cannot process drop off address by previous invalid request");
                    }
                    #endregion

                    #region Determine FlatRate or Rate
                    if (reservation.Service.Dropoff != null)
                    {
                        long? pickupZoneId = null;
                        long? dropoffZoneId = null;

                        if ((tgep.PickupZone != null) && (tgep.DropoffZone != null))
                        {
                            pickupZoneId = tgep.PickupZone.Id;
                            dropoffZoneId = tgep.DropoffZone.Id;
                        }

                        if ((result) && pickupZoneId.HasValue && dropoffZoneId.HasValue && tgep.Info_FlatRates != null && tgep.Info_FlatRates.Any()
                            && tgep.Info_FlatRates.Any(x => x.PickupZoneId == pickupZoneId.Value && x.DropoffZoneId == dropoffZoneId.Value))
                        {
                            //flatrate
                            tgep.EstimationType = EstimationResultType.Info_FlatRate;

                            #region Do calculation for flatrate

                            //caculate flatrate
                            ecar_fleet_zone_flatrate tfzf = tgep.Info_FlatRates.Where(x => x.PickupZoneId == pickupZoneId && x.DropoffZoneId == dropoffZoneId).Select(x => x).FirstOrDefault();

                            tgep.FlatRateEstimation = new FlatRateData
                            {
                                CurrencyMode = tfzf.AmountUnit,
                                PickupZone = tgep.PickupZone.Name,
                                DropoffZone = tgep.DropoffZone.Name,
                                EstimationTotalAmount = tfzf.Amount
                            };
                            logger.Info("Flat rate found");

                            #endregion
                        }
                        else
                        {
                            //rate
                            tgep.EstimationType = EstimationResultType.Info_Rate;
                        }
                    }
                    #endregion
                }

                #region Add Accessibility
                if (reservation != null & reservation.DisabilityInfo != null && reservation.DisabilityInfo.RequiredInd == true)
                {


                }
                #endregion

                sw.Stop();
                logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
            }

            #region Create ecar log
            if (result)
            {
                if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
                {
                    WriteECarLog(null, ECarServiceAPIConst.None, ECarServiceMethodType.Estimation, sw.ElapsedMilliseconds, string.Empty, string.Empty, null, null);
                }
            }
            else
            {
                result = false;
                logger.Warn("Cannot create or get this ecar log by invalid trip");
            }
            #endregion

            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateGetEstimationRequest");
            #endregion

            return result;
        }
        public bool ValidateStatusRequestForGreenTomato(TokenRS tokenVTOD, OTA_GroundResRetrieveRQ request,ECarStatusParameter inECsp ,  out ECarStatusParameter outECsp)
		{
			bool result = true;
			Stopwatch sw = new Stopwatch();
			sw.Start();
			outECsp = new ECarStatusParameter();
            outECsp = inECsp;
			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
			#endregion

			if (request.Reference != null)
			{
				#region Set token, request
				outECsp.tokenVTOD = tokenVTOD;
				outECsp.request = request;
				#endregion

				#region Validate Reference Id
				if (request.Reference == null || !request.Reference.Any() || string.IsNullOrWhiteSpace(request.Reference.First().ID))
				{
					result = false;
					logger.Warn("Reference.ID is empty.");
				}
				else
				{
					logger.Info("Request is valid.");
				}
				#endregion

				#region Validate Trip
				if (result)
				{

					vtod_trip t = GetECarTrip(request.Reference.First().ID);
					if (t != null)
					{
						outECsp.Trip = t;
						logger.InfoFormat("Trip found. tripID={0}", request.Reference.First().ID);
					}
					else
					{
						result = false;
						logger.WarnFormat("Cannot find this trip. tripID={0}", request.Reference.First().ID);
					}
				}
				else
				{
					result = false;
					logger.Warn("Cannot find trip by invalid referenceId");
				}
				#endregion

				#region Validate Fleet
				if (result)
				{
					ecar_fleet fleet = null;
					if (GetFleet(outECsp.Trip.ecar_trip.FleetId, out fleet))
					{
						outECsp.Fleet = fleet;
						logger.InfoFormat("Fleet found. ID={0}", fleet.Id);

						if (fleet != null)
						{
							outECsp.serviceAPIID = GetECarFleetServiceAPIPreference(fleet.Id, ECarServiceAPIPreferenceConst.Status).ServiceAPIId;
						}
					}
					else
					{
						result = false;
						logger.Warn("Unable to find fleet by this fleetID");
					}
				}
				else
				{
					result = false;
					logger.Warn("Unable to find fleet by this fleetID");
				}
				#endregion

				#region Validate Fleet User
				if (result)
				{
					ecar_fleet_user fleet_user = null;
					if (GetECarFleetUser(outECsp.Trip.ecar_trip.FleetId, tokenVTOD.Username, out fleet_user))
					{
						outECsp.Fleet_User = fleet_user;
						logger.Info("This user has sufficient privilege.");

						outECsp.User = GetUser(fleet_user.UserId);
					}
					else
					{
						result = false;
						logger.WarnFormat("Insufficient privilege. User={0}, FleetId={1}", tokenVTOD.Username, outECsp.Fleet.Id);
					}
				}
				else
				{
					result = false;
					logger.Warn("Cannot validate this user by invalid fleet ");
				}
				#endregion

				#region GetAllTripStatusHistory
				if (result)
				{
					using (VTODEntities context = new VTODEntities())
					{
						long tripId = outECsp.Trip.Id;
						outECsp.lastTripStatus = context.ecar_trip_status.Where(x => x.TripID == tripId).Select(x => x).OrderByDescending(x => x.Id).FirstOrDefault();
					}
				}
				#endregion
			}
			else
			{
				result = false;
				logger.Info("Reference is null.");
			}

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Create ecar log
			if (result)
			{
				if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
				{
					WriteECarLog(outECsp.Trip.Id, outECsp.serviceAPIID, ECarServiceMethodType.Status, sw.ElapsedMilliseconds, string.Empty, string.Empty, null, null);
				}
			}
			else
			{
				result = false;
				logger.Warn("Cannot create or get this ecar log by invalid trip");
			}
			#endregion

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateStatusRequest");
			#endregion

			return result;
		}

		public bool ValidateCancelRequestForGreenTomato(TokenRS tokenVTOD, OTA_GroundCancelRQ request, ECarCancelParameter inECcp ,out ECarCancelParameter outECcp)
		{
			bool result = true;
			Stopwatch sw = new Stopwatch();
			sw.Start();
			outECcp = new ECarCancelParameter();
            outECcp = inECcp;

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
			#endregion

			#region Set token, request
			outECcp.tokenVTOD = tokenVTOD;
			outECcp.request = request;
			#endregion

			#region Validate Unique ID
			string uid = request.Reservation.UniqueID.FirstOrDefault().ID;
			if (request.Reservation.UniqueID.Any())
			{
				if (!string.IsNullOrWhiteSpace(uid))
				{
					logger.Info("Request is valid.");
				}
				else
				{
					result = false;
					logger.Info("request.Reservation.UniqueID.Frist().ID is empty.");
				}
			}
			else
			{
				result = false;
				logger.Info("Reservation.UniqueID is empty.");
			}
			#endregion

			#region Validate Trip
			if (result)
			{
				vtod_trip t = GetECarTrip(uid);
				if (t != null)
				{
					outECcp.Trip = t;
					logger.InfoFormat("Trip found. tripID={0}", uid);
				}
				else
				{
					result = false;
					logger.WarnFormat("Cannot find this trip. tripID={0}", uid);
				}
			}
			else
			{
				result = false;
				logger.Warn("Cannot find trip by invalid referenceId");
			}
			#endregion

			#region Validate Fleet
			if (result)
			{
				ecar_fleet fleet = null;
				if (GetFleet(outECcp.Trip.ecar_trip.FleetId, out fleet))
				{
					outECcp.Fleet = fleet;
					logger.InfoFormat("Fleet found. ID={0}", fleet.Id);

					if (fleet != null)
					{
						outECcp.serviceAPIID = GetECarFleetServiceAPIPreference(fleet.Id, ECarServiceAPIPreferenceConst.Cancel).ServiceAPIId;
					}
				}
				else
				{
					result = false;
					logger.Warn("Unable to find fleet by this fleetID");
				}
			}
			else
			{
				result = false;
				logger.Warn("Unable to find fleet by this fleetID");
			}
			#endregion

			#region Validate User
			if (result)
			{
				ecar_fleet_user user = null;
				if (GetECarFleetUser(outECcp.Trip.ecar_trip.FleetId, tokenVTOD.Username, out user))
				{
					outECcp.User = user;
					logger.Info("This user has sufficient privilege.");
				}
				else
				{
					result = false;
					logger.WarnFormat("Insufficient privilege. User={0}, FleetId={1}", tokenVTOD.Username, outECcp.Fleet.Id);
				}
			}
			else
			{
				result = false;
				logger.Warn("Cannot validate this user by invalid fleet ");
			}
			#endregion

			#region Validate customer
			if (result)
			{

			}
			else
			{
				result = false;
				logger.Warn("Cannot find customer by Invalid trip");
			}
			#endregion

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Create ecar log
			if (result)
			{
				if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
				{
					WriteECarLog(outECcp.Trip.Id, outECcp.serviceAPIID, ECarServiceMethodType.Cancel, sw.ElapsedMilliseconds, string.Empty, string.Empty, null, null);
				}
			}
			else
			{
				result = false;
				logger.Warn("Cannot create or get this ecar log by invalid trip");
			}
			#endregion

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateCancelRequest");
			#endregion

			return result;
		}

		public bool ValidateGetVehicleInfoRequestForGreenTomato(TokenRS tokenVTOD, OTA_GroundAvailRQ request,ECarGetVehicleInfoParameter inEVgvip ,out ECarGetVehicleInfoParameter outECgvip)
		{
			bool result = true;
			Stopwatch sw = new Stopwatch();
			sw.Start();
			outECgvip = new ECarGetVehicleInfoParameter();
            outECgvip = inEVgvip;

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
			#endregion

			#region Set token, request
			outECgvip.tokenVTOD = tokenVTOD;
			outECgvip.request = request;
			#endregion


			if (request.Service != null)
			{
				if (request.Service.Pickup != null)
				{
					#region Validate longtitude, latitude
					if (request.Service.Pickup.Address != null)
					{
						if (!string.IsNullOrWhiteSpace(request.Service.Pickup.Address.Latitude) && !string.IsNullOrWhiteSpace(request.Service.Pickup.Address.Longitude))
						{
							double longtitue = 0;
							double latitude = 0;
							if (double.TryParse(request.Service.Pickup.Address.Longitude, out longtitue) && double.TryParse(request.Service.Pickup.Address.Latitude, out latitude))
							{
								outECgvip.Longtitude = longtitue;
								outECgvip.Latitude = latitude;
							}
							else
							{
								result = false;
								logger.Warn("Invalid longtitude and latittude");
							}
						}
						else
						{
							result = false;
							logger.Warn("Invalid longtitude and latittude");
						}
					}
					else
					{
						result = false;
						logger.Warn("Cannot find any longtitude or latitude from address");
					}
					#endregion

					#region Validate Pickup Address
					if (result)
					{
						Pickup_Dropoff_Stop stop = request.Service.Pickup;

						if ((stop.Address != null) && (stop.AirportInfo == null))
						{

							if (stop.Address.CountryName != null && !string.IsNullOrWhiteSpace(stop.Address.CountryName.Code) && !string.IsNullOrWhiteSpace(stop.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(stop.Address.CityName) && !string.IsNullOrWhiteSpace(stop.Address.PostalCode))
							{
								Map.DTO.Address pickupAddress = new Map.DTO.Address { Country = stop.Address.CountryName.Code, State = stop.Address.StateProv.StateCode, City = stop.Address.CityName, ZipCode = stop.Address.PostalCode, Geolocation = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(stop.Address.Latitude), Longitude = Convert.ToDecimal(stop.Address.Longitude) } };
								outECgvip.PickupAddressType = UDI.VTOD.Domain.ECar.Const.AddressType.Address;
								outECgvip.PickupAddress = pickupAddress;
							}
							else if (stop.Address.CountryName != null && !string.IsNullOrWhiteSpace(stop.Address.CountryName.Code) && !string.IsNullOrWhiteSpace(stop.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(stop.Address.CityName))
							{
								Map.DTO.Address pickupAddress = new Map.DTO.Address { Country = stop.Address.CountryName.Code, State = stop.Address.StateProv.StateCode, City = stop.Address.CityName, Geolocation = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(stop.Address.Latitude), Longitude = Convert.ToDecimal(stop.Address.Longitude) } };
								outECgvip.PickupAddressType = UDI.VTOD.Domain.ECar.Const.AddressType.Address;
								outECgvip.PickupAddress = pickupAddress;

							}
							else if (!string.IsNullOrWhiteSpace(stop.Address.LocationName))
							{
								result = false;
								logger.Warn("Cannot use locationName from address.");
							}
							else
							{
								result = false;
								logger.Warn("Cannot find any address/locationName from address.");
							}
						}
						else if ((stop.Address == null) && (stop.AirportInfo != null))
						{
							if ((stop.AirportInfo.Arrival != null) && (stop.AirportInfo.Departure == null))
							{
								result = false;
								logger.Warn("Cannot use aiportinfo for address");
							}
							else if ((stop.AirportInfo.Arrival == null) && (stop.AirportInfo.Departure != null))
							{
								result = false;
								logger.Warn("Cannot use aiportinfo for address");
							}
							else if ((stop.AirportInfo.Arrival != null) && (stop.AirportInfo.Departure != null))
							{
								result = false;
								logger.Warn("This OTA request contains both AirportInfo.Arrival and AirportInfo.Departure");
							}
							else
							{
								result = false;
								logger.Warn("Cannot find any arrival aor depature from AirportInfo");
							}
						}
						else
						{
							//find address by longtitude and latitude
							Map.DTO.Address pickupAddress = null;
							if (!LookupAddress(outECgvip.Longtitude, outECgvip.Latitude, out pickupAddress))
							{
								logger.WarnFormat("Cannot find longtitude={0} and latitude={1}", outECgvip.Longtitude, outECgvip.Latitude);
								result = false;
							}

							outECgvip.PickupAddressType = UDI.VTOD.Domain.ECar.Const.AddressType.Address;
							outECgvip.PickupAddress = pickupAddress;
						}
					}
					else
					{
						result = false;
					}
					#endregion

					#region Validate Fleet
					if (result)
					{
						List<ecar_fleet> fleetList = null;
						if (GetFleet(outECgvip.PickupAddressType, outECgvip.PickupAddress, outECgvip.PickupLandmark, out fleetList))
						{
							///egvip.Fleet = fleet;
							outECgvip.fleetList = fleetList.ToList();
							logger.InfoFormat("Fleet found. count={0}", fleetList.Count());

							if (fleetList.Any())
							{
								foreach (var f in fleetList)
								{
									//egvip.serviceAPIID = GetECarFleetServiceAPIPreference(fleet.Id, ECarServiceAPIPreferenceConst.GetVehicleInfo).ServiceAPIId;
									long serviceAPIID = GetECarFleetServiceAPIPreference(f.Id, ECarServiceAPIPreferenceConst.GetVehicleInfo).ServiceAPIId;
									if (!outECgvip.serviceAPIIDList.Where(x => x == serviceAPIID).Any())
									{
										outECgvip.serviceAPIIDList.Add(serviceAPIID);
									}


									//log
									if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
									{
										//WriteECarLog(null, egvip.serviceAPIID, ECarServiceMethodType.GetVehicleInfo, sw.ElapsedMilliseconds, string.Empty, string.Empty, null, null);
										//unknown service method type
										WriteECarLog(null, serviceAPIID, ECarServiceMethodType.GetVehicleInfo, sw.ElapsedMilliseconds, string.Empty, string.Empty, null, null);
									}
								}

							}
						}
						else
						{
							result = false;
							logger.Warn("Unable to find fleet by this address or landmark.");
						}
					}
					else
					{
						result = false;
						logger.Warn("Unable to find fleet by invalid address or landmark.");
					}
					#endregion

					#region Validate Zone
					if (result)
					{
						//Default: Circle
						if (request.Service.Pickup.Address.TPA_Extensions != null)
						{
							#region Validate Polygon Type
							if (request.Service.Pickup.Address.TPA_Extensions.Zone != null)
							{
								if (request.Service.Pickup.Address.TPA_Extensions.Zone.Circle != null)
								{
									outECgvip.Radius = double.Parse(request.Service.Pickup.Address.TPA_Extensions.Zone.Circle.Radius.ToString());
									outECgvip.PolygonType = PolygonTypeConst.Circle;
								}

								if (string.IsNullOrWhiteSpace(outECgvip.PolygonType))
								{
									result = false;
									logger.Warn("Unable to find the polygon area for GetVehicleInfo");
								}
							}
							else
							{
								result = false;
								logger.Warn("request.TPA_Extensions.Zone");
							}
							#endregion

							#region Validate Vehicle status
							if (request.Service.Pickup.Address.TPA_Extensions.Vehicles != null)
							{
								if (request.Service.Pickup.Address.TPA_Extensions.Vehicles.Items.Any())
								{
									//tgvip.VehicleStausList = request.Service.Pickup.Address.TPA_Extensions.Vehicles.Items;
									if (!"all".Equals(request.Service.Pickup.Address.TPA_Extensions.Vehicles.Items.FirstOrDefault().VehicleStatus.ToLower()))
									{
										Vehicle v = new Vehicle { VehicleStatus = request.Service.Pickup.Address.TPA_Extensions.Vehicles.Items.FirstOrDefault().VehicleStatus.ToString() };
										outECgvip.VehicleStausList.Add(v);
									}
									else
									{
										logger.Info("Vehicle status = ALL");
									}
								}
								else
								{
									result = false;
									logger.Warn("request.Service.Pickup.Address.TPA_Extensions.Vehicles.Items is empty");
								}

							}
							else
							{
								result = false;
								logger.Info("Unable to find request.TPA_Extensions.Vehicles");
							}

							#endregion
						}
						else
						{
							result = false;
							logger.Warn("Unable to find request.TPA_Extensions");
						}
					}
					else
					{
						result = false;
						logger.Warn("Cannot get polygon type by invalid request");
					}
					#endregion

				}
				else
				{
					result = false;
					logger.Warn("request.Service.Pickup is null");
				}
			}
			else
			{
				result = false;
				logger.Warn("request.Service is null");
			}

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateGetVehicleInfoRequest");
			#endregion

			return result;
		}

        public ECarBookingParameter InitiateECarParameter(OTA_GroundBookRQ request) 
        {
            ECarBookingParameter ecbp = new ECarBookingParameter();
            ecbp.PickupAddress = new Map.DTO.Address();
            bool result = true;
            string airportCode = string.Empty;
            if (request.GroundReservations.Any())
            {
                GroundReservation reservation = request.GroundReservations.FirstOrDefault();

                #region Determine fleet by dispatch code                
                string dispatchCode = GetDispatchCode(reservation.RateQualifiers);
                if (!string.IsNullOrWhiteSpace(dispatchCode))
                {
                    var ecarFleet = new ecar_fleet();
                    if (GetFleet(dispatchCode, out ecarFleet))
                    {
                        ecbp.Fleet = ecarFleet;
                    }
                    else
                    {
                        var vtodException = VtodException.CreateException(ExceptionType.ECar, 1011);
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException;
                    }
                }
                else
                {
                    if (reservation.RateQualifiers.Where(x => x.RateQualifierValue == "SuperShuttle_ExecuCar").Any())
                    {
                        var vtodException = VtodException.CreateException(ExceptionType.ECar, 1011);
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException;
                    }
                    else if (reservation.RateQualifiers.Where(x => x.RateQualifierValue == "ExecuCar").Any())
                    {
                        string PickupAddressType = "";
                        Map.DTO.Address PickupAddress = new Map.DTO.Address();

                        #region Validate Pickup Address
                        if (result)
                        {
                            Pickup_Dropoff_Stop stop = reservation.Service.Location.Pickup;

                            if ((stop.Address != null) && (stop.AirportInfo == null))
                            {
                                if (!string.IsNullOrWhiteSpace(stop.Address.CountryName.Code) && !string.IsNullOrWhiteSpace(stop.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(stop.Address.CityName) && !string.IsNullOrWhiteSpace(stop.Address.PostalCode))
                                {
                                    Map.DTO.Address pickupAddress = new Map.DTO.Address { Country = stop.Address.CountryName.Code, State = stop.Address.StateProv.StateCode, City = stop.Address.CityName, ZipCode = stop.Address.PostalCode, Geolocation = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(stop.Address.Latitude), Longitude = Convert.ToDecimal(stop.Address.Longitude) } };
                                    PickupAddressType = UDI.VTOD.Domain.ECar.Const.AddressType.Address;
                                    PickupAddress = pickupAddress;
                                }
                                else if (!string.IsNullOrWhiteSpace(stop.Address.CountryName.Code) && !string.IsNullOrWhiteSpace(stop.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(stop.Address.CityName))
                                {
                                    Map.DTO.Address pickupAddress = new Map.DTO.Address { Country = stop.Address.CountryName.Code, State = stop.Address.StateProv.StateCode, City = stop.Address.CityName, Geolocation = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(stop.Address.Latitude), Longitude = Convert.ToDecimal(stop.Address.Longitude) } };
                                    PickupAddressType = UDI.VTOD.Domain.ECar.Const.AddressType.Address;
                                    PickupAddress = pickupAddress;

                                }
                                else if (!string.IsNullOrWhiteSpace(stop.Address.LocationName))
                                {
                                    result = false;
                                    logger.Warn("Cannot use locationName from address.");
                                }
                                else
                                {
                                    result = false;
                                    logger.Warn("Cannot find any address/locationName from address.");
                                }
                            }
                            else if ((stop.Address == null) && (stop.AirportInfo != null))
                            {
                                if ((stop.AirportInfo.Arrival != null) && (stop.AirportInfo.Departure == null))
                                {
                                    result = true;
                                    PickupAddressType = UDI.VTOD.Domain.ECar.Const.AddressType.Airport;
                                    PickupAddress.AirportCode = reservation.Service.Location.Pickup.AirportInfo.Arrival.LocationCode;
                                    airportCode = PickupAddress.AirportCode;
                                }
                                else if ((stop.AirportInfo.Arrival == null) && (stop.AirportInfo.Departure != null))
                                {
                                    result = false;
                                    logger.Warn("This OTA request contains both AirportInfo.Arrival and AirportInfo.Departure");
                                }
                                else if ((stop.AirportInfo.Arrival != null) && (stop.AirportInfo.Departure != null))
                                {
                                    result = false;
                                    logger.Warn("This OTA request contains both AirportInfo.Arrival and AirportInfo.Departure");
                                }
                                else
                                {
                                    result = false;
                                    logger.Warn("Cannot find any arrival aor depature from AirportInfo");
                                }
                            }
                            else
                            {
                                double Longtitude = 0;
                                double Latitude = 0;
                                Map.DTO.Address pickupAddress2 = null;

                                if (!LookupAddress(Longtitude, Latitude, out pickupAddress2))
                                {
                                    logger.WarnFormat("Cannot find longtitude={0} and latitude={1}", Longtitude, Latitude);
                                    result = false;
                                }
                                PickupAddressType = UDI.VTOD.Domain.ECar.Const.AddressType.Address;
                                PickupAddress = pickupAddress2;
                            }

                        }
                        else
                        {
                            result = false;
                        }
                        #endregion
                        #region Validate Fleet
                        ecar_fleet fleet = null;
                        if (result)
                        {
                            if (GetFleet(PickupAddressType, PickupAddress, airportCode, out fleet))
                            {
                                ecbp.Fleet = fleet;
                                logger.InfoFormat("Fleet found. ID={0}", fleet.Id);
                            }
                            else
                            {
                                result = false;
                                logger.Warn("Unable to find fleet by this address or landmark.");
                            }
                        }
                        else
                        {
                            result = false;
                            logger.Warn("Unable to find fleet by invalid address or landmark.");
                        }
                        #endregion
                    }
                    else
                    {
                        var vtodException = VtodException.CreateException(ExceptionType.ECar, 1011);
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException;
                    }
                }
                #endregion

                #region Get Service API ID
                if (result)
                {
                    ecbp.serviceAPIID = GetServiceAPIId(ecbp.Fleet.Id, ECarServiceAPIPreferenceConst.Book);      
                }
                #endregion
            }
            else {
                var vtodException = VtodException.CreateException(ExceptionType.ECar, 1001);
                logger.Warn(vtodException.ExceptionMessage.Message);
                throw vtodException;
            }

            return ecbp;
        }

        public ECarStatusParameter InitiateECarParameter(OTA_GroundResRetrieveRQ request, TokenRS tokenVTOD)
        {
            ECarStatusParameter ecsp = new ECarStatusParameter();
            bool result = true;

            #region Validate Reference Id
            if (request.Reference == null || !request.Reference.Any() || string.IsNullOrWhiteSpace(request.Reference.First().ID))
            {
                result = false;
                logger.Warn("Reference.ID is empty.");
                var vtodException = VtodException.CreateException(ExceptionType.ECar, 1015);
                logger.Warn(vtodException.ExceptionMessage.Message);
                throw vtodException;
            }
            else
            {
                logger.Info("Request is valid.");
            }
            #endregion

            #region Validate Trip            
            if (result)
            {
                var reference = request.Reference.First();
                var vtodTrip = GetVtodTripWithReferenceID(reference.ID, reference.Type.Equals(ConfirmationType.Confirmation.ToString(), StringComparison.OrdinalIgnoreCase));
                if (vtodTrip != null)
                {
                    ecsp.Trip = vtodTrip;
                    logger.InfoFormat("Trip found. tripID={0}", request.Reference.First().ID);
                    
                    long fleetId = vtodTrip.ecar_trip.FleetId;
                    ecar_fleet fleet=new ecar_fleet();
                    if (GetFleet(fleetId, out fleet))
                    {
                        ecsp.Fleet = fleet;
                        
                        if (fleet != null)
                        {
                            logger.InfoFormat("Fleet found. ID={0}", fleet.Id);

                            ecsp.serviceAPIID = GetServiceAPIId(ecsp.Fleet.Id, ECarServiceAPIPreferenceConst.Status);
                                                        
                            List<string> endpointNames = null;
                            endpointNames = MtdataEndpointConfiguration(ecsp.serviceAPIID, ecsp.Fleet.Id);
                            if (endpointNames != null && endpointNames.Any())
                            {
                                ecsp.AuthenticationServiceEndpointName = endpointNames[0];
                                ecsp.BookingWebServiceEndpointName = endpointNames[1];
                                ecsp.OsiWebServiceEndpointName = endpointNames[2];
                                ecsp.AddressWebServiceEndpointName = endpointNames[3];
                            }

                            ecar_fleet_user fleet_user = null;
                            if (GetECarFleetUser(ecsp.Trip.ecar_trip.FleetId, tokenVTOD.Username, out fleet_user))
                            {
                                ecsp.Fleet_User = fleet_user;
                                logger.Info("This user has sufficient privilege.");

                                ecsp.User = GetUser(fleet_user.UserId);
                            }
                            else
                            {
                                result = false;
                                logger.WarnFormat("Insufficient privilege. User={0}, FleetId={1}", tokenVTOD.Username, ecsp.Fleet.Id);
                            }

                        }
                    }
                    else
                    {
                        result = false;
                        var vtodException = VtodException.CreateException(ExceptionType.ECar, 1011);
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException;
                    }
                }
                else
                {
                    result = false;
                    logger.WarnFormat("Cannot find this trip. tripID={0}", request.Reference.First().ID);
                    var vtodException = VtodException.CreateException(ExceptionType.ECar, 1012);
                    logger.Warn(vtodException.ExceptionMessage.Message);
                    throw vtodException;
                }
            }
            #endregion                      

            return ecsp;
        }

        public ECarCancelParameter InitiateECarParameter(OTA_GroundCancelRQ request)
        {
            ECarCancelParameter eccp = new ECarCancelParameter();
            bool result = true;

            #region Validate Unique ID
            var uid = request.Reservation.UniqueID.FirstOrDefault();
            if (request.Reservation.UniqueID.Any())
            {
                if (!string.IsNullOrWhiteSpace(uid.ID))
                {
                    logger.Info("Request is valid.");
                }
                else
                {
                    result = false;
                    logger.Info("request.Reservation.UniqueID.Frist().ID is empty.");
                    var vtodException = VtodException.CreateException(ExceptionType.ECar, 1015);
                    logger.Warn(vtodException.ExceptionMessage.Message);
                    throw vtodException;
                }
            }
            else
            {
                result = false;
                logger.Info("Reservation.UniqueID is empty.");
                var vtodException = VtodException.CreateException(ExceptionType.ECar, 1015);
                logger.Warn(vtodException.ExceptionMessage.Message);
                throw vtodException;
            }
            #endregion

            #region Validate Trip
            
            if (result)
            {
                var vtodTrip = GetVtodTripWithReferenceID(uid.ID, uid.Type.Equals(ConfirmationType.Confirmation.ToString(), StringComparison.OrdinalIgnoreCase));
                if (vtodTrip != null)
                {
                    eccp.Trip= vtodTrip;
                    logger.InfoFormat("Trip found. tripID={0}", uid.ID);
                    ecar_fleet fleeet= new ecar_fleet();
                    if(GetFleet(vtodTrip.ecar_trip.FleetId,out fleeet))
                    {
                        eccp.Fleet=fleeet;
                    }
                    else
                    {
                        result = false;
                        var vtodException = VtodException.CreateException(ExceptionType.ECar, 1011);
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException;
                    }
                }
                else
                {
                    result = false;
                    logger.WarnFormat("Cannot find this trip. tripID={0}", uid);
                    var vtodException = VtodException.CreateException(ExceptionType.ECar, 1012);
                    logger.Warn(vtodException.ExceptionMessage.Message);
                    throw vtodException;
                }
            }
            else
            {
                result = false;
                logger.Warn("Cannot find trip by invalid referenceId");
                var vtodException = VtodException.CreateException(ExceptionType.ECar, 1012);
                logger.Warn(vtodException.ExceptionMessage.Message);
                throw vtodException;
            }
            #endregion

            #region Get Service API ID
            if (result)
            {
                eccp.serviceAPIID = GetServiceAPIId(eccp.Fleet.Id, ECarServiceAPIPreferenceConst.Cancel);
            }
            #endregion

            return eccp;
        }

        public ECarGroundAvailParameter InitiateECarParameter(OTA_GroundAvailRQ request, string action)
        {
            ECarGroundAvailParameter gap = null;
            bool result = true;
            string airportCode = string.Empty;
            if (ECarServiceAPIPreferenceConst.GetEstimation.Equals(action)) {
                gap = new ECarGetEstimationParameter();
            }
            else if (ECarServiceAPIPreferenceConst.GetVehicleInfo.Equals(action))
            {
                gap = new ECarGetVehicleInfoParameter();
            }
            else {
                result = false;
                var vtodException = VtodException.CreateException(ExceptionType.ECar, 1009);
                logger.Warn(vtodException.ExceptionMessage.Message);
                throw vtodException;
            }

            string dispatchCode = string.Empty;
            try
            {
                dispatchCode = GetDispatchCode(request.RateQualifiers);
            }
            catch(Exception e)
            {
                logger.Warn(e.Message);
            }
            if (!string.IsNullOrWhiteSpace(dispatchCode))
            {
                ecar_fleet f = new ecar_fleet();
                if (GetFleet(dispatchCode, out f))
                {
                    gap.Fleet = f;
                }
                else
                {
                    var vtodException = VtodException.CreateException(ExceptionType.ECar, 1011);
                    logger.Warn(vtodException.ExceptionMessage.Message);
                    throw vtodException;
                }
            }
            else
            {
                //ByPass if RateQualifierValue has SuperShuttle_ExecuCar
                if (request.RateQualifiers.Where(x => x.RateQualifierValue == "SuperShuttle_ExecuCar").Any())
                {
                    var vtodException = VtodException.CreateException(ExceptionType.ECar, 1014);
                    logger.Warn(vtodException.ExceptionMessage.Message);
                    throw vtodException;
                }
                else if (request.RateQualifiers.Where(x => x.RateQualifierValue == "ExecuCar").Any())
                {
                    string PickupAddressType = "";
                    Map.DTO.Address PickupAddress = new Map.DTO.Address();

                    #region Validate Pickup Address
                    if (result)
                    {
                        Pickup_Dropoff_Stop stop = request.Service.Pickup;

                        if ((stop.Address != null) && (stop.AirportInfo == null))
                        {
                            if (!string.IsNullOrWhiteSpace(stop.Address.CountryName.Code) && !string.IsNullOrWhiteSpace(stop.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(stop.Address.CityName) && !string.IsNullOrWhiteSpace(stop.Address.PostalCode))
                            {
                                Map.DTO.Address pickupAddress = new Map.DTO.Address { Country = stop.Address.CountryName.Code, State = stop.Address.StateProv.StateCode, City = stop.Address.CityName, ZipCode = stop.Address.PostalCode, Geolocation = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(stop.Address.Latitude), Longitude = Convert.ToDecimal(stop.Address.Longitude) } };
                                PickupAddressType = UDI.VTOD.Domain.ECar.Const.AddressType.Address;
                                PickupAddress = pickupAddress;
                            }
                            else if (!string.IsNullOrWhiteSpace(stop.Address.CountryName.Code) && !string.IsNullOrWhiteSpace(stop.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(stop.Address.CityName))
                            {
                                Map.DTO.Address pickupAddress = new Map.DTO.Address { Country = stop.Address.CountryName.Code, State = stop.Address.StateProv.StateCode, City = stop.Address.CityName, Geolocation = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(stop.Address.Latitude), Longitude = Convert.ToDecimal(stop.Address.Longitude) } };
                                PickupAddressType = UDI.VTOD.Domain.ECar.Const.AddressType.Address;
                                PickupAddress = pickupAddress;

                            }
                            else if (!string.IsNullOrWhiteSpace(stop.Address.LocationName))
                            {
                                #region Not support now
                                //tgvip.PickupAddressType = UDI.VTOD.Domain.ECar.Const.AddressType.LocationName;
                                //tgvip.PickupLandmark = stop.Address.LocationName;
                                #endregion

                                result = false;
                                logger.Warn("Cannot use locationName from address.");
                            }
                            else
                            {
                                result = false;
                                logger.Warn("Cannot find any address/locationName from address.");
                            }
                        }
                        else if ((stop.Address == null) && (stop.AirportInfo != null))
                        {
                           
                            //if ((stop.AirportInfo.Arrival != null) && (stop.AirportInfo.Departure == null))
                            //{
                            //    #region Not support now
                            //    tgvip.PickupAddressType = UDI.VTOD.Domain.ECar.Const.AddressType.AirportInfo;
                            //    tgvip.PickupLandmark = stop.AirportInfo.Arrival.LocationCode;
                            //    #endregion

                            //    result = false;
                            //    logger.Warn("Cannot use aiportinfo for address");
                            //}
                            //else if ((stop.AirportInfo.Arrival == null) && (stop.AirportInfo.Departure != null))
                            //{
                            //    #region Not support now
                            //    //tgvip.PickupAddressType = UDI.VTOD.Domain.ECar.Const.AddressType.AirportInfo;
                            //    //tgvip.PickupLandmark = stop.AirportInfo.Departure.LocationCode;
                            //    #endregion

                            //    result = false;
                            //    logger.Warn("Cannot use aiportinfo for address");
                            //}
                            if ((stop.AirportInfo.Arrival != null) && (stop.AirportInfo.Departure == null))
                            {
                                result = true;
                                Map.DTO.Address pickupAddress = new Map.DTO.Address { Geolocation = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(stop.AirportInfo.Arrival.Latitude), Longitude = Convert.ToDecimal(stop.AirportInfo.Arrival.Longitude) } };
                                PickupAddressType = UDI.VTOD.Domain.ECar.Const.AddressType.Airport;
                                pickupAddress.AirportCode = stop.AirportInfo.Arrival.LocationCode;
                                airportCode = pickupAddress.AirportCode;
                                PickupAddress = pickupAddress;
                            }
                            else if ((stop.AirportInfo.Arrival == null) && (stop.AirportInfo.Departure != null))
                            {
                                result = true;
                                Map.DTO.Address pickupAddress = new Map.DTO.Address { Geolocation = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(stop.AirportInfo.Departure.Latitude), Longitude = Convert.ToDecimal(stop.AirportInfo.Departure.Longitude) } };
                                pickupAddress.AirportCode = stop.AirportInfo.Departure.LocationCode;
                                PickupAddressType = UDI.VTOD.Domain.ECar.Const.AddressType.Airport;
                                airportCode = pickupAddress.AirportCode;
                                PickupAddress = pickupAddress;
                            }
                            else
                            {
                                result = false;
                                logger.Warn("Cannot find any arrival aor depature from AirportInfo");
                            }
                        }
                        else
                        {
                            //find address by longtitude and latitude
                            double Longtitude = 0;
                            double Latitude = 0;
                            Map.DTO.Address pickupAddress2 = null;

                            if (!LookupAddress(Longtitude, Latitude, out pickupAddress2))
                            {
                                logger.WarnFormat("Cannot find longtitude={0} and latitude={1}", Longtitude, Latitude);
                                result = false;
                            }
                            PickupAddressType = UDI.VTOD.Domain.ECar.Const.AddressType.Address;
                            PickupAddress = pickupAddress2;
                        }

                    }
                    else
                    {
                        result = false;
                    }
                    #endregion
                    #region Validate Fleet
                    ecar_fleet fleet = null;
                    if (result)
                    {
                        if (GetFleet(PickupAddressType, PickupAddress, airportCode, out fleet))
                        {
                            gap.Fleet = fleet;
                            logger.InfoFormat("Fleet found. ID={0}", fleet.Id);
                        }
                        else
                        {
                            result = false;
                            logger.Warn("Unable to find fleet by this address or landmark.");
                        }
                    }
                    else
                    {
                        result = false;
                        logger.Warn("Unable to find fleet by invalid address or landmark.");
                    }
                    #endregion
                }              
            }
            #region Get Service API ID
            if (result)
            {
                gap.serviceAPIID = GetServiceAPIId(gap.Fleet.Id, action);
            }
            #endregion
            return gap;
        }
    
        public bool ValidateBookingRequestForGreenTomato(TokenRS tokenVTOD, OTA_GroundBookRQ request, ECarBookingParameter inputECbp, out ECarBookingParameter outECbp)
		{
			bool result = true;
			Stopwatch sw = new Stopwatch();
			sw.Start();
            outECbp = new ECarBookingParameter();
            outECbp = inputECbp;
            

			Stopwatch swEachPart = new Stopwatch();



			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
			#endregion

			#region Validate Reservation
			if (request.GroundReservations.Any())
			{
				GroundReservation reservation = request.GroundReservations.FirstOrDefault();

				#region Set token, request
				outECbp.tokenVTOD = tokenVTOD;
				outECbp.request = request;
				#endregion

				#region Set Source and Device and ref
				if (request.TPA_Extensions != null && !string.IsNullOrWhiteSpace(request.TPA_Extensions.Source))
				{
					outECbp.Source = request.TPA_Extensions.Source;
				}

				if (request.TPA_Extensions != null && !string.IsNullOrWhiteSpace(request.TPA_Extensions.Device))
				{
					outECbp.Device = request.TPA_Extensions.Device;
				}

				if (!string.IsNullOrWhiteSpace(request.EchoToken))
				{
					outECbp.Ref = request.EchoToken;
				}
				#endregion

				swEachPart.Restart();
				#region Validate Customer
				if (reservation.Passenger != null)
				{
					//Name
					if (reservation.Passenger.Primary != null)
					{
						if (reservation.Passenger.Primary.PersonName != null)
						{
							if (string.IsNullOrWhiteSpace(reservation.Passenger.Primary.PersonName.GivenName) || string.IsNullOrWhiteSpace(reservation.Passenger.Primary.PersonName.Surname))
							{

								result = false;
								logger.Warn(Messages.ECar_Common_InvalidReservationPassengerPrimaryPersonNameDetail);
								throw new ValidationException(Messages.ECar_Common_InvalidReservationPassengerPrimaryPersonNameDetail);
							}
							else
							{
								outECbp.FirstName = reservation.Passenger.Primary.PersonName.GivenName.Trim();
								outECbp.LastName = reservation.Passenger.Primary.PersonName.Surname.Trim();
								logger.Info(Messages.ECar_Common_ValidReservationPassengerPrimaryPersonNameDetail);
							}
						}
						else
						{
							result = false;
							logger.Warn(Messages.ECar_Common_InvalidReservationPassengerPrimaryPersonName);
							throw new ValidationException(Messages.ECar_Common_InvalidReservationPassengerPrimaryPersonName);
						}

						//Telephone
						if (reservation.Passenger.Primary.Telephones.Any())
						{
							Telephone p = reservation.Passenger.Primary.Telephones.FirstOrDefault();
							string countrycode = ExtractDigitsWithPlus(p.CountryAccessCode);
							string areacode = ExtractDigits(p.AreaCityCode);
							string phonenumber = ExtractDigits(p.PhoneNumber);
							//if (areacode.Length.Equals(3) && phonenumber.Length.Equals(7))
							//{
							outECbp.PhoneNumber = string.Format("{0}{1}{2}", countrycode, areacode, phonenumber);
							logger.Info(Messages.ECar_Common_ValidReservationPassengerPrimaryTelephones);
							//}
							//else
							//{
							//	result = false;
							//	logger.Warn(Messages.ECar_Common_InvalidReservationPassengerPrimaryTelephones);
							//	throw new ValidationException(Messages.ECar_Common_InvalidReservationPassengerPrimaryTelephones);
							//}
						}
						else
						{
							result = false;
							logger.Warn(Messages.ECar_Common_InvalidReservationPassengerPrimaryPhoneNumber);
							throw new ValidationException(Messages.ECar_Common_InvalidReservationPassengerPrimaryPhoneNumber);
						}

						//email
						if (reservation.Passenger.Primary.Emails.Any())
						{
							Email e = reservation.Passenger.Primary.Emails.FirstOrDefault();


							string emailPattern = @"^(?("")("".+?(?<!\\)""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
												 @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]*\.)+[a-zA-Z0-9][\-a-zA-Z0-9]{0,22}[a-zA-Z0-9]))$";

							if (!string.IsNullOrWhiteSpace(e.Value) && Regex.IsMatch(e.Value, emailPattern, RegexOptions.IgnorePatternWhitespace))
							{
								outECbp.emailAddress = e.Value;
								logger.Info(Messages.ECar_Common_ValidReservationPassengerPrimaryTelephones);
							}
							else
							{
								result = false;
								logger.Warn(Messages.ECar_Common_InvalidReservationPassengerPrimaryEmails);
								throw new ValidationException(Messages.ECar_Common_InvalidReservationPassengerPrimaryEmails);
							}
						}

						if (result)
						{
							logger.InfoFormat("Customer firstname={0}, lastname={1}, phonenumber={2}", outECbp.FirstName, outECbp.LastName, outECbp.PhoneNumber);
						}
						else
						{
							result = false;
							logger.Warn(Messages.ECar_Common_UnableToCreateOrFindECarCustomer);
							throw new ValidationException(Messages.ECar_Common_UnableToCreateOrFindECarCustomer);
						}
					}
					else
					{
						result = false;
						logger.Warn(Messages.ECar_Common_InvalidReservationPassengerPrimary);
						throw new ValidationException(Messages.ECar_Common_InvalidReservationPassengerPrimary);
					}
				}
				else
				{
					result = false;
					logger.Warn(Messages.ECar_Common_InvalidReservationPassenger);
					throw new ValidationException(Messages.ECar_Common_InvalidReservationPassenger);
				}

				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidateCustomer");
				swEachPart.Stop();
				logger.DebugFormat("Validate Customer Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Validate Pickup Address

				if (reservation.Service != null)
				{
					if (reservation.Service.Location != null)
					{
						if (reservation.Service.Location.Pickup != null)
						{
							if (((reservation.Service.Location.Pickup.Address != null) && (reservation.Service.Location.Pickup.AirportInfo != null)) || ((reservation.Service.Location.Pickup.Address == null) && (reservation.Service.Location.Pickup.AirportInfo == null)))
							{
								result = false;
								logger.Warn(Messages.ECar_Common_InvalidReservationServiceLocationPickupContainsBothPickupAddressAndAirportInfo);
								throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocationPickupContainsBothPickupAddressAndAirportInfo);
							}
							else if ((reservation.Service.Location.Pickup.Address != null) && (reservation.Service.Location.Pickup.AirportInfo == null))
							{

								if ((string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude) || (string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CountryName.Code)))
								{
									result = false;
									logger.Warn(Messages.ECar_Common_InvalidReservationServiceLocationPickupDetail);
									throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocationPickupDetail);
								}
								else if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.AddressLine) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CityName) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.PostalCode) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CountryName.Code))
								{
									//Address
									Map.DTO.Address pickupAddress = null;
									string pickupAddressStr = GetAddressString(reservation.Service.Location.Pickup.Address);
									//if (reservation.Service.Location.Pickup.Address.TPA_Extensions != null)
									//{
									//	if (reservation.Service.Location.Pickup.Address.TPA_Extensions.AddressValidationRequired.HasValue ? reservation.Service.Location.Pickup.Address.TPA_Extensions.AddressValidationRequired.Value : false)
									//	{
									//		if (!LookupAddress(reservation.Service.Location.Pickup, pickupAddressStr, out pickupAddress))
									//		{
									//			result = false;
									//			logger.WarnFormat(Messages.ECar_Common_InvalidReservationServiceLocationPickupDetailByMAPAPI(pickupAddressStr));
									//			throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocationPickupDetailByMAPAPI(pickupAddressStr));
									//		}
									//	}
									//	else
									//	{
									//		if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Latitude))
									//		{
									//			logger.Info(Messages.ECar_Common_SkipReservationServiceLocationPickupDetailByMAPAPI);
									//			pickupAddress = ConvertAddress(reservation.Service.Location.Pickup);
									//		}
									//		else
									//		{
									//			result = false;
									//			logger.WarnFormat(Messages.ECar_Common_InvalidReservationServiceLocationNoMAPAPIWithEmptyLongAndLat(pickupAddressStr));
									//			throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocationNoMAPAPIWithEmptyLongAndLat(pickupAddressStr));
									//		}
									//	}
									//}
									//else
									//{
									logger.Info(Messages.ECar_Common_SkipReservationServiceLocationPickupDetailByMAPAPI);
									pickupAddress = ConvertAddress(reservation.Service.Location.Pickup);
									//}
									outECbp.PickupAddressType = UDI.VTOD.Domain.ECar.Const.AddressType.Address;
									outECbp.PickupAddress = pickupAddress;


									logger.Info("Pickup AddressType: Address");
								}
								else if ((!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude) && (!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CountryName.Code)))
								{
									outECbp.PickupAddressOnlyContainsLatAndLong = true;
									MapService ms = new MapService();
									Map.DTO.Geolocation g = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(reservation.Service.Location.Pickup.Address.Latitude), Longitude = Convert.ToDecimal(reservation.Service.Location.Pickup.Address.Longitude) };
									string error = "";
									var addressList = ms.GetAddressesFromGeoLocationWithProximity(g, out error);
									if (string.IsNullOrWhiteSpace(error) && addressList.Any())
									{
										var add = addressList.FirstOrDefault();
										reservation.Service.Location.Pickup.Address.StreetNmbr = add.StreetNo;
										reservation.Service.Location.Pickup.Address.AddressLine = add.Street;
										reservation.Service.Location.Pickup.Address.BldgRoom = add.ApartmentNo;
										reservation.Service.Location.Pickup.Address.CityName = add.City;
										reservation.Service.Location.Pickup.Address.PostalCode = add.ZipCode;
										reservation.Service.Location.Pickup.Address.StateProv = new StateProv { StateCode = add.State };
										reservation.Service.Location.Pickup.Address.CountryName = new CountryName { Code = add.Country };
										reservation.Service.Location.Pickup.Address.StreetNmbr = add.StreetNo;

										outECbp.PickupAddressType = UDI.VTOD.Domain.ECar.Const.AddressType.Address;
										outECbp.PickupAddress = ConvertAddress(reservation.Service.Location.Pickup);
										logger.Info("Pickup AddressType: Address");

									}
									else
									{
										result = false;
										logger.Warn(Messages.ECar_Common_InvalidReservationServiceLocationPickupDetail);
										throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocationPickupDetail);
									}
								}
								else
								{
									result = false;
									logger.Warn(Messages.ECar_Common_UnknownError);
									throw new ValidationException(Messages.ECar_Common_UnknownError);
								}
							}
							else if ((reservation.Service.Location.Pickup.Address == null) && (reservation.Service.Location.Pickup.AirportInfo != null))
							{
								//airport
								if (((reservation.Service.Location.Pickup.AirportInfo.Arrival != null) && (reservation.Service.Location.Pickup.AirportInfo.Departure != null)) || ((reservation.Service.Location.Pickup.AirportInfo.Arrival == null) && (reservation.Service.Location.Pickup.AirportInfo.Departure == null)))
								{
									result = false;
									logger.Warn(Messages.ECar_Common_InvalidReservationServiceLocationPickupAirportInfoContainsBothArrivalAndDepature);
									throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocationPickupAirportInfoContainsBothArrivalAndDepature);
								}
								else if (reservation.Service.Location.Pickup.AirportInfo.Arrival != null && reservation.Service.Location.Pickup.AirportInfo.Departure == null)
								{
									//Arrival
									outECbp.PickupAddressType = AddressType.Airport;
									outECbp.PickupAirport = reservation.Service.Location.Pickup.AirportInfo.Arrival.LocationCode;
									logger.Info("Pickup AddressType: AirportInfo.Arrival");
								}
								else if (reservation.Service.Location.Pickup.AirportInfo.Arrival == null && reservation.Service.Location.Pickup.AirportInfo.Departure != null)
								{
									//Depature

									outECbp.PickupAddressType = AddressType.Airport;
									outECbp.PickupAirport = reservation.Service.Location.Pickup.AirportInfo.Departure.LocationCode;
									logger.Info("Pickup AddressType: AirportInfo.Depature");

								}

								//longitude and latitude, and flight information
								if (result)
								{
									double? longitude = null;
									double? latitude = null;
									if (GetLongitudeAndLatitudeForAirport(outECbp.PickupAirport, out longitude, out latitude))
									{
										outECbp.LongitudeForPickupAirport = longitude.Value;
										outECbp.LatitudeForPickupAirport = latitude.Value;
									}
									//else
									//{
									//    throw new ValidationException(Messages.ECar_Common_UnableToFindAirportLongitudeAndLatitude);
									//}

									if (reservation.Service.Location.Pickup.Airline != null)
									{
										if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Airline.FlightNumber))
										{
											outECbp.PickupAirlineFlightNumber = reservation.Service.Location.Pickup.Airline.FlightNumber;
										}

										if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Airline.Code))
										{
											outECbp.PickupAirlineCode = reservation.Service.Location.Pickup.Airline.Code;
										}

										if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Airline.CodeContext))
										{
											outECbp.PickupAirlineCodeContext = reservation.Service.Location.Pickup.Airline.CodeContext;
										}

										if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Airline.FlightDateTime))
										{
											outECbp.PickupAirlineFlightDateTime = reservation.Service.Location.Pickup.Airline.FlightDateTime;
										}

									}

								}


							}
						}
						else
						{
							result = false;
							logger.Warn(Messages.ECar_Common_InvalidReservationServiceLocationPickup);
							throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocationPickup);
						}
					}
					else
					{
						result = false;
						logger.Warn(Messages.ECar_Common_InvalidReservationServiceLocation);
						throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocation);
					}
				}
				else
				{
					result = false;
					logger.Warn(Messages.ECar_Common_InvalidReservationService);
					throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidatePickupAddress");
				swEachPart.Stop();
				logger.DebugFormat("Validate Pickup address Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                #region fleet
				swEachPart.Restart();
				#region Validate Fleet
				if (result)
				{
					//find fleet
					List<ecar_fleet> fleetList = new List<ecar_fleet>();
					//ecar_fleet fleet = null;
					if (GetFleet(outECbp.PickupAddressType, outECbp.PickupAddress, outECbp.PickupAirport, out fleetList))
					{
						//find relation
						if (fleetList.Any())
						{
							try
							{

								var rateQuialifier = request.GroundReservations.First().RateQualifiers.First();
								if (rateQuialifier.SpecialInputs != null)
								{
									//Dispatch code, counsumer code, 
									var DispatchCode = rateQuialifier.SpecialInputs.Where(s => s.Name.ToLower() == "DispatchCode".ToLowerInvariant()).FirstOrDefault();
									var ConsumerCode = rateQuialifier.SpecialInputs.Where(s => s.Name.ToLower() == "ConsumerCode".ToLowerInvariant()).FirstOrDefault();
									if ((DispatchCode != null) && (ConsumerCode != null))
									{
										outECbp.DispatchCode = DispatchCode.Value;
										outECbp.ConsumerCode = ConsumerCode.Value;

										//find fleet by Dispatch code, counsumer code
										ecar_fleet_servicearea_relation relation = null;
										foreach (ecar_fleet fleet in fleetList)
										{
											if (GetFleetRelation(fleet.Id, outECbp.DispatchCode, outECbp.ConsumerCode, out relation))
											{
												if ((outECbp.Relation != null) && (relation != null))
												{
													//duplicate relation setting
													string error = Messages.ECar_Common_DuplicatedFleetRelationSetting;
													throw new ValidationException(error);
												}
												else
												{
													//find one relation and put it into object
													outECbp.serviceAPIID = GetECarFleetServiceAPIPreference(fleet.Id, ECarServiceAPIPreferenceConst.Book).ServiceAPIId;
													outECbp.Fleet = fleet;
													outECbp.Relation = relation;
													logger.InfoFormat("Fleet found. ID={0}", fleet.Id);
												}
											}
										}

										//unable to fine relation setting
										if (outECbp.Relation == null)
										{
											throw new ValidationException(Messages.ECar_Common_UnableToCorrectDispatchCodeAndConsumerCode);
										}

									}
									else if (request.References.Any() && (!string.IsNullOrWhiteSpace(request.References[0].ID)))
									{
										logger.Info("use reference Id");


										if (request.References.Any() && !string.IsNullOrWhiteSpace(request.References[0].ID))
										{
											outECbp.Relation = new ecar_fleet_servicearea_relation();
											outECbp.Relation.DispatchServiceId = UDI.Utility.Helper.Cryptography.DecryptDES(request.References[0].ID, ConfigurationManager.AppSettings["Encryption_Key"], ConfigurationManager.AppSettings["Encryption_IV"]);

											//check expired
											if (IsExpiredForBooking(outECbp.Relation.DispatchServiceId))
											{
												string error = string.Format("Expired for booking this trip. {0}", outECbp.Relation.DispatchServiceId);
												throw new ValidationException(error);
											}
											else
											{
												outECbp.Fleet = fleetList.FirstOrDefault();
												outECbp.serviceAPIID = GetECarFleetServiceAPIPreference(outECbp.Fleet.Id, ECarServiceAPIPreferenceConst.Book).ServiceAPIId;
											}
										}
										else
										{
											//invalid
											throw new ValidationException("Invalid Reference Id");
										}

									}
									else
									{
										throw new ValidationException(Messages.ECar_Common_UnableToCorrectDispatchCodeAndConsumerCode);
									}

									//consumer confiramtion ID
									var ConsumerConfirmationID = rateQuialifier.SpecialInputs.Where(s => s.Name.ToLower() == "ConsumerConfirmationID".ToLowerInvariant()).FirstOrDefault();
									if (ConsumerConfirmationID != null)
									{
										outECbp.ConsumerConfirmationID = ConsumerConfirmationID.Value;
									}
									else
									{
										outECbp.ConsumerConfirmationID = null;
									}

								}
								else
								{
									throw new ValidationException(Messages.ECar_Common_UnableToCorrectDispatchCodeAndConsumerCode);
								}


							}
							catch (Exception ex)
							{
								logger.Error("ECar:Dispatch code and consumer code", ex);
								result = false;
								logger.Warn(Messages.ECar_Common_UnableToFindFleetByAddressOrAirportInfoOrDispatchCodeAndConsumerCode);
								throw new ValidationException(Messages.ECar_Common_UnableToFindFleetByAddressOrAirportInfoOrDispatchCodeAndConsumerCode);
							}
						}
						else
						{
							string error = Messages.ECar_Common_UnableToFindFleetByPickupAddress;
							throw new ValidationException(error);
						}
					}
					else
					{
						string error = Messages.ECar_Common_UnableToFindFleetByPickupAddress;
						throw new ValidationException(error);
					}




				}

				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidateFleet");
				swEachPart.Stop();
				logger.DebugFormat("Validate fleet Duration:{0} ms.", swEachPart.ElapsedMilliseconds);
                #endregion 

                swEachPart.Restart();
				#region Validate User
				if (result)
				{
					ecar_fleet_user user = null;
					if (GetECarFleetUser(outECbp.Fleet.Id, tokenVTOD.Username, out user))
					{
						outECbp.User = user;
						logger.Info("This user has sufficient privilege.");
					}
					else
					{
						result = false;
						logger.Warn(Messages.ECar_Common_InsufficientPrivilege(tokenVTOD.Username, outECbp.Fleet.Id));
						throw new ValidationException(Messages.ECar_Common_InsufficientPrivilege(tokenVTOD.Username, outECbp.Fleet.Id));
					}
				}
				else
				{
					result = false;
					logger.Warn(Messages.ECar_Common_UnableProcessByPreviousInvalidRequest);
					throw new ValidationException(Messages.ECar_Common_UnableProcessByPreviousInvalidRequest);
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidateUser");
				swEachPart.Stop();
				logger.DebugFormat("Validate user Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Validate PickupDateTime
				if (result)
				{
					//pickiup now
					//bool pickupNow=false;
					if (request.TPA_Extensions != null && request.TPA_Extensions.PickMeUpNow.HasValue)
					{
						if (request.TPA_Extensions.PickMeUpNow.Value)
						{
							outECbp.PickupNow = true;
						}
						else
						{
							outECbp.PickupNow = false;
						}
					}
					else
					{
						outECbp.PickupNow = false;
					}


					DateTime pickupDateTime;
					//if (DateTime.TryParseExact(reservation.Service.Location.Pickup.DateTime, ConfigurationManager.AppSettings["DateTimeFormatForPickupAndDropoff"], null, DateTimeStyles.None, out pickupDateTime))
					if (DateTime.TryParse(reservation.Service.Location.Pickup.DateTime, out pickupDateTime))
					{
						outECbp.PickupDateTime = pickupDateTime;
						logger.Info("PickupDateTime is correct");
					}
					else if (outECbp.PickupNow && !DateTime.TryParse(reservation.Service.Location.Pickup.DateTime, out pickupDateTime))
					{
						outECbp.PickupDateTime = DateTime.Now;
						logger.InfoFormat("There is no Pickup datetime for this request. But it has pickup now value. So the pickup time will set as {0:yyyy/MM/dd HH:mm:ss}.", outECbp.PickupDateTime);
					}
					else
					{
						result = false;
						logger.Warn(Messages.ECar_Common_InvalidReservationServiceLocationPickupDateTime(reservation.Service.Location.Pickup.DateTime));
						throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocationPickupDateTime(reservation.Service.Location.Pickup.DateTime));
					}
				}
				else
				{
					result = false;
					logger.Warn(Messages.ECar_Common_UnableProcessByPreviousInvalidRequest);
					throw new ValidationException(Messages.ECar_Common_UnableProcessByPreviousInvalidRequest);
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidatePickupDateTime");
				swEachPart.Stop();
				logger.DebugFormat("Validate PickupTime Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Validate duplicated booking trip
				if (result)
				{
					if (IsAbleToBookTrip(outECbp))
					{
						logger.Info("This trip can be booked.");
					}
					else
					{
						result = false;
						var vtodException = VtodException.CreateException(ExceptionType.ECar, 2006);
						logger.Warn(vtodException.ExceptionMessage.Message);
						throw vtodException; // new ValidationException(Messages.ECar_Common_DuplicatedTrips);
					}
				}
				else
				{
					result = false;
					logger.Warn(Messages.ECar_Common_UnableProcessByPreviousInvalidRequest);
					throw new ValidationException(Messages.ECar_Common_UnableProcessByPreviousInvalidRequest);
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidateDuplicatedBookingTrip");
				swEachPart.Stop();
				logger.DebugFormat("Validate duplicated trip Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Validate Dropoff Address
				//DropOff is optional: By_Pouyan
				if (reservation.Service.Location != null && reservation.Service.Location.Dropoff != null)
					if (result)
					{
						if (reservation.Service != null)
						{
							if (reservation.Service.Location != null)
							{
								if (reservation.Service.Location.Dropoff != null)
								{
									using (VTODEntities context = new VTODEntities())
									{
										if (((reservation.Service.Location.Dropoff.Address != null) && (reservation.Service.Location.Dropoff.AirportInfo != null)) || ((reservation.Service.Location.Dropoff.Address == null) && (reservation.Service.Location.Dropoff.AirportInfo == null)))
										{
											result = false;
											var vtodException = VtodException.CreateValidationException(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
											logger.Warn(vtodException.ExceptionMessage.Message);
											throw vtodException; // new ValidationException(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
										}
										else if ((reservation.Service.Location.Dropoff.Address != null) && (reservation.Service.Location.Dropoff.AirportInfo == null))
										{
											//Address
											if ((string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.Longitude) || (string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CountryName.Code)))
											//if (string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CountryName.Code))
											{
												result = false;
												logger.Warn(Messages.Validation_LocationDetail);
												throw VtodException.CreateValidationException(Messages.Validation_LocationDetail);
											}
											else if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.AddressLine) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CityName) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.PostalCode) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CountryName.Code))
											{
												Map.DTO.Address dropoffAddress = null;
												string dropoffAddressStr = GetAddressString(reservation.Service.Location.Dropoff.Address);
												//if (reservation.Service.Location.Dropoff.Address.TPA_Extensions != null)
												//{
												//	if (reservation.Service.Location.Dropoff.Address.TPA_Extensions.AddressValidationRequired.HasValue ? reservation.Service.Location.Dropoff.Address.TPA_Extensions.AddressValidationRequired.Value : false)
												//	{
												//		if (!LookupAddress(reservation.Service.Location.Dropoff, dropoffAddressStr, out dropoffAddress))
												//		{
												//			result = false;
												//			logger.WarnFormat(Messages.Validation_LocationDetailByMapApi);
												//			throw VtodException.CreateValidationException(Messages.Validation_LocationDetailByMapApi);
												//		}
												//	}
												//	else
												//	{
												//		if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.Longitude) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.Latitude))
												//		{
												//			logger.Info("Skip MAP API Validation.");
												//			dropoffAddress = ConvertAddress(reservation.Service.Location.Dropoff);
												//		}
												//		else
												//		{
												//			result = false;
												//			logger.WarnFormat(Messages.ECar_Common_InvalidReservationServiceLocationNoMAPAPIWithEmptyLongAndLat(dropoffAddressStr));
												//			throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocationNoMAPAPIWithEmptyLongAndLat(dropoffAddressStr));
												//		}
												//	}
												//}
												//else
												//{
												logger.Info("Skip MAP API Validation.");
												dropoffAddress = ConvertAddress(reservation.Service.Location.Dropoff);
												//}

												outECbp.DropOffAddress = dropoffAddress;
												outECbp.DropOffAddressType = UDI.VTOD.Domain.ECar.Const.AddressType.Address;
												logger.Info("Dropoff AddressType=Address");
											}
											else if ((!string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.Longitude) && (!string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CountryName.Code)))
											{
												outECbp.DropOffAddressOnlyContainsLatAndLong = true;
												MapService ms = new MapService();
												Map.DTO.Geolocation g = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(reservation.Service.Location.Dropoff.Address.Latitude), Longitude = Convert.ToDecimal(reservation.Service.Location.Dropoff.Address.Longitude) };
												string error = "";
												var addressList = ms.GetAddressesFromGeoLocationWithProximity(g, out error);
												if (string.IsNullOrWhiteSpace(error) && addressList.Any())
												{
													var add = addressList.FirstOrDefault();
													reservation.Service.Location.Dropoff.Address.StreetNmbr = add.StreetNo;
													reservation.Service.Location.Dropoff.Address.AddressLine = add.Street;
													reservation.Service.Location.Dropoff.Address.BldgRoom = add.ApartmentNo;
													reservation.Service.Location.Dropoff.Address.CityName = add.City;
													reservation.Service.Location.Dropoff.Address.PostalCode = add.ZipCode;
													reservation.Service.Location.Dropoff.Address.StateProv = new StateProv { StateCode = add.State };
													reservation.Service.Location.Dropoff.Address.CountryName = new CountryName { Code = add.Country };
													reservation.Service.Location.Dropoff.Address.StreetNmbr = add.StreetNo;
													reservation.Service.Location.Dropoff.Address.StreetNmbr = add.StreetNo;

													outECbp.DropOffAddressType = UDI.VTOD.Domain.ECar.Const.AddressType.Address;
													outECbp.DropOffAddress = ConvertAddress(reservation.Service.Location.Dropoff);
													logger.Info("Dropoff AddressType: Address");

												}
												else
												{
													result = false;
													logger.Warn(Messages.ECar_Common_InvalidReservationServiceLocationPickupDetail);
													throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocationPickupDetail);
												}
											}
											else
											{
												result = false;
												logger.Warn(Messages.ECar_Common_UnknownError);
												throw new ValidationException(Messages.ECar_Common_UnknownError);
											}
										}
										else if ((reservation.Service.Location.Dropoff.Address == null) && (reservation.Service.Location.Dropoff.AirportInfo != null))
										{

											//airport
											if (((reservation.Service.Location.Dropoff.AirportInfo.Arrival != null) && (reservation.Service.Location.Dropoff.AirportInfo.Departure != null)) || ((reservation.Service.Location.Dropoff.AirportInfo.Arrival == null) && (reservation.Service.Location.Dropoff.AirportInfo.Departure == null)))
											{
												result = false;
												var vtodException = VtodException.CreateValidationException(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
												logger.Warn(vtodException.ExceptionMessage.Message);
												throw vtodException; // new ValidationException(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
											}
											else if (reservation.Service.Location.Dropoff.AirportInfo.Arrival != null && reservation.Service.Location.Dropoff.AirportInfo.Departure == null)
											{
												//Arrival
												outECbp.DropOffAddressType = AddressType.Airport;
												outECbp.DropoffAirport = reservation.Service.Location.Dropoff.AirportInfo.Arrival.LocationCode;
												logger.Info("Dropoff AddressType: AirportInfo.Arrival");
											}
											else if (reservation.Service.Location.Dropoff.AirportInfo.Arrival == null && reservation.Service.Location.Dropoff.AirportInfo.Departure != null)
											{
												//Depature
												outECbp.DropOffAddressType = AddressType.Airport;
												outECbp.DropoffAirport = reservation.Service.Location.Dropoff.AirportInfo.Departure.LocationCode;
												logger.Info("Dropoff AddressType: AirportInfo.Depature");
											}

											//longitude and latitude
											if (result)
											{
												double? longitude = null;
												double? latitude = null;
												if (GetLongitudeAndLatitudeForAirport(outECbp.DropoffAirport, out longitude, out latitude))
												{
													outECbp.LongitudeForDropoffAirport = longitude.Value;
													outECbp.LatitudeForDropoffAirport = latitude.Value;
												}
												//else
												//{
												//    throw new ValidationException(Messages.ECar_Common_UnableToFindAirportLongitudeAndLatitude);
												//}


												if (reservation.Service.Location.Dropoff.Airline != null)
												{
													if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Airline.FlightNumber))
													{
														outECbp.DropOffAirlineFlightNumber = reservation.Service.Location.Dropoff.Airline.FlightNumber;
													}

													if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Airline.Code))
													{
														outECbp.DropOffAirlineCode = reservation.Service.Location.Dropoff.Airline.Code;
													}

													if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Airline.CodeContext))
													{
														outECbp.DropOffAirlineCodeContext = reservation.Service.Location.Dropoff.Airline.CodeContext;
													}

													if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Airline.FlightDateTime))
													{
														outECbp.DropOffAirlineFlightDateTime = reservation.Service.Location.Dropoff.Airline.FlightDateTime;
													}

												}
											}
										}
									}
								}

							}
							else
							{
								result = false;
								logger.Warn(Messages.ECar_Common_InvalidReservationServiceLocation);
								throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocation);
							}
						}
						else
						{
							result = false;
							logger.Warn(Messages.ECar_Common_InvalidReservationService);
							throw new ValidationException(Messages.ECar_Common_InvalidReservationService);
						}
					}
					else
					{
						result = false;
						logger.Warn(Messages.ECar_Common_UnableProcessByPreviousInvalidRequest);
						throw new ValidationException(Messages.ECar_Common_UnableProcessByPreviousInvalidRequest);
					}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidateDropOffAddress");
				swEachPart.Stop();
				logger.DebugFormat("Validate dropoff address Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Validate DropOffDateTime
				//if (result)
				//{
				//	DateTime dropOffDateTime;
				//	if (reservation.Service.Location.Dropoff != null)
				//	{
				//		if (DateTime.TryParse(reservation.Service.Location.Dropoff.DateTime, out dropOffDateTime))
				//		{
				//			tbp.DropOffDateTime = dropOffDateTime;
				//		}
				//		else
				//		{
				//			tbp.DropOffDateTime = null;
				//		}
				//	}
				//	else
				//	{
				//		tbp.DropOffDateTime = null;
				//	}
				//}
				//else
				//{
				//	logger.Info("No drop off datetime");
				//}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidateDropoffDateTime");
				swEachPart.Stop();
				logger.DebugFormat("Validate dropoff time Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Validate Available address type
				if (result)
				{
					string pickupAddressType = outECbp.PickupAddressType;
					string dropoffAddressType = outECbp.DropOffAddressType;

					//verify the address type of pickup address and drop off address
					UtilityDomain ud = new UtilityDomain();
					if (pickupAddressType == AddressType.Address.ToString())
					{
						UtilityGetAirportNameRQ uganRQ = new UtilityGetAirportNameRQ();
						uganRQ.Address = new Address();
						uganRQ.Address.Latitude = outECbp.PickupAddress.Geolocation.Latitude.ToString();
						uganRQ.Address.Longitude = outECbp.PickupAddress.Geolocation.Longitude.ToString();

						UtilityGetAirportNameRS uganRS = ud.GetAirportName(tokenVTOD, uganRQ);
						if (uganRS.Success != null)
						{
							if (!string.IsNullOrWhiteSpace(uganRS.AirportName))
							{
								pickupAddressType = AddressType.Airport;
							}
						}

					}

					if (dropoffAddressType == AddressType.Address.ToString())
					{
						UtilityGetAirportNameRQ uganRQ = new UtilityGetAirportNameRQ();
						uganRQ.Address = new Address();
						uganRQ.Address.Latitude = outECbp.DropOffAddress.Geolocation.Latitude.ToString();
						uganRQ.Address.Longitude = outECbp.DropOffAddress.Geolocation.Longitude.ToString();

						UtilityGetAirportNameRS uganRS = ud.GetAirportName(tokenVTOD, uganRQ);
						if (uganRS.Success != null)
						{
							if (!string.IsNullOrWhiteSpace(uganRS.AirportName))
							{
								dropoffAddressType = AddressType.Airport;
							}
						}
					}

					//check database setting
					//1. get ecar_fleet_availableaddresstype
					using (VTODEntities context = new VTODEntities())
					{
						long fleetId = outECbp.Fleet.Id;
						if (context.ecar_fleet_availableaddresstype.Where(x => fleetId == x.FleetId).Any())
						{
							ecar_fleet_availableaddresstype ecfa = context.ecar_fleet_availableaddresstype.Where(x => fleetId == x.FleetId).Select(x => x).FirstOrDefault();
							//2. check if the type is available or not
							//ECar_Common_InvalidAddressTypeForFleet
							if (AddressType.Address.ToString() == pickupAddressType && AddressType.Address.ToString() == dropoffAddressType)
							{
								//address to address
								if (ecfa.AddressToAddress)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.ECar, 1013);// (Messages.Validation_InvalidAddressType);
								}
							}
							else if (AddressType.Address.ToString() == pickupAddressType && AddressType.Airport.ToString() == dropoffAddressType)
							{
								//address to airport
								if (ecfa.AddressToAirport)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.ECar, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
							else if (AddressType.Address.ToString() == pickupAddressType && null == dropoffAddressType)
							{
								//address to null
								if (ecfa.AddressToNull)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.ECar, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
							else if (AddressType.Airport.ToString() == pickupAddressType && AddressType.Address.ToString() == dropoffAddressType)
							{
								//airport to address
								if (ecfa.AirportToAddress)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.ECar, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
							else if (AddressType.Airport.ToString() == pickupAddressType && AddressType.Airport.ToString() == dropoffAddressType)
							{
								//airport to airport
								if (ecfa.AirportToAirport)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.ECar, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
							else if (AddressType.Airport.ToString() == pickupAddressType && null == dropoffAddressType)
							{
								//airport to null
								if (ecfa.AirportToNull)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.ECar, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
						}
						else
						{
							result = false;
							throw VtodException.CreateException(ExceptionType.ECar, 1011); //ValidationException(Messages.ECar_Common_UnableToGetAvailableFleets);
						}
					}


				}
				else
				{
					result = false;
					var vtodException = Common.DTO.VtodException.CreateFieldRequiredValidationException("Payment");
					logger.Warn(vtodException.ExceptionMessage.Message);
					throw vtodException; // new ValidationException(Messages.Validation_NullPayment);
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidateAvailableAddressType");
				swEachPart.Stop();
				logger.DebugFormat("Validate available address type Duration:{0} ms.", swEachPart.ElapsedMilliseconds);


				swEachPart.Restart();
				#region Validate PaymentInfo
				//As of this version, all requester should pass payment Info.
				//We need to modify this based on some others who do not pass payementInfo
				if (request.Payments == null || request.Payments.Payments == null || !request.Payments.Payments.Any())
				{
					result = false;
					var vtodException = Common.DTO.VtodException.CreateFieldRequiredValidationException("Payment");
					logger.Warn(vtodException.ExceptionMessage.Message);
					throw vtodException; // new ValidationException(Messages.Validation_NullPayment);
				}
				else
				{
					if (request.Payments.Payments.First().PaymentCard != null && request.Payments.Payments.First().DirectBill == null)
					{
						if (request.Payments.Payments.First().PaymentCard.CardNumber == null || string.IsNullOrWhiteSpace(request.Payments.Payments.First().PaymentCard.CardNumber.ID))
						{
							result = false;
							var vtodException = Common.DTO.VtodException.CreateFieldRequiredValidationException("CardNumber and CardNumberID");
							logger.Warn(vtodException.ExceptionMessage.Message);
							throw vtodException; // new ValidationException(Messages.Validation_NullPayment);
						}
						else
						{
							outECbp.CreditCardID = request.Payments.Payments.First().PaymentCard.CardNumber.ID.ToInt32();
							outECbp.PaymentType = Common.DTO.Enum.PaymentType.PaymentCard;
						}
					}
					else if (request.Payments.Payments.First().DirectBill != null && request.Payments.Payments.First().PaymentCard == null)
					{
						if (string.IsNullOrWhiteSpace(request.Payments.Payments.First().DirectBill.ID))
						{
							result = false;
							var vtodException = Common.DTO.VtodException.CreateFieldRequiredValidationException("DirectBillID");
							logger.Warn(vtodException.ExceptionMessage.Message);
							throw vtodException; // new ValidationException(Messages.Validation_NullPayment);
						}
						else
						{
							outECbp.DirectBillAccountID = request.Payments.Payments.First().DirectBill.ID.ToInt32();
							outECbp.PaymentType = Common.DTO.Enum.PaymentType.DirectBill;
						}
					}
					else if (request.Payments.Payments.First().MiscChargeOrder != null && !string.IsNullOrWhiteSpace(request.Payments.Payments.First().MiscChargeOrder.OriginalIssuePlace) && request.Payments.Payments.First().MiscChargeOrder.OriginalIssuePlace.ToUpper() == "SDS")
					{
						#region Authorize
						//var membershipController = new UDI.SDS.MembershipController(tokenVTOD, TrackTime);
						var membershipDomain = new UDI.VTOD.Domain.Membership.MembershipDomain();
						//I removed this to handle error for our role manager
						//if (membershipDomain.IsUserInPaymentRole(tokenVTOD.Username, Common.DTO.Enum.PaymentType.ChargedBySDS.ToString()))
						//{
						//	outECbp.PaymentType = Common.DTO.Enum.PaymentType.ChargedBySDS;
						//}
						//else
						//{
						//	result = false;
						//	logger.Warn(Messages.Validation_InvalidPaymentType);
						//	throw VtodException.CreateValidationException(Messages.Validation_InvalidPaymentType); // ValidationException(Messages.General_InvalidPaymentType);
						//}
						#endregion

					}
					else
					{
						result = false;
						logger.Warn(Messages.Validation_PaymentInfo);
						throw VtodException.CreateValidationException(Messages.Validation_PaymentInfo);
					}
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidatePaymentInfo");
				swEachPart.Stop();
				logger.DebugFormat("Validate payment Id Duration:{0} ms.", swEachPart.ElapsedMilliseconds);



				swEachPart.Restart();
				#region Validate Member Info
				//As of this version, all requester should pass memberID.
				//We need to modify this based on some others who do not pass memberID
				if (request.TPA_Extensions == null && string.IsNullOrWhiteSpace(request.TPA_Extensions.MemberID))
				{
					result = false;
					var vtodException = Common.DTO.VtodException.CreateFieldRequiredValidationException("MemberID");
					logger.Warn(vtodException.ExceptionMessage.Message);
					throw vtodException; // new ValidationException(Messages.Validation_NullPayment);
				}
				else
				{
					outECbp.MemberID = request.TPA_Extensions.MemberID.ToInt32();
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ValidateMemberInfo");
				swEachPart.Stop();
				logger.DebugFormat("Validate member Info Duration:{0} ms.", swEachPart.ElapsedMilliseconds);



				swEachPart.Restart();
				#region Get Gratuity
				try
				{
					var rateQuialifier = request.GroundReservations.First().RateQualifiers.First();
					if (rateQuialifier.SpecialInputs != null)
					{
						//rate
						var gratuities = rateQuialifier.SpecialInputs.Where(s => s.Name.ToLower() == "gratuity".ToLower()).ToList();

						if (gratuities != null && gratuities.Any())
						{
							decimal f;
							if (decimal.TryParse(gratuities.First().Value.Replace("%", "").ToString(), out f))
								outECbp.Gratuity = gratuities.First().Value;
							else
								throw new ValidationException("Invalid Gratuity");
						}
					}
				}
				catch (Exception ex)
				{
					logger.Error("ECar:Gratuity", ex);
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ GetGratuity");
				swEachPart.Stop();
				logger.DebugFormat("Get Gratuity Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Get Rate
				try
				{
					var rateQuialifier = request.GroundReservations.First().RateQualifiers.First();
					if (rateQuialifier.SpecialInputs != null)
					{
						//rate
						var rates = rateQuialifier.SpecialInputs.Where(s => s.Name.ToLower() == "rate".ToLower()).ToList();


						if (rates != null && rates.Any())
						{
							decimal f;
							if (decimal.TryParse(rates.First().Value, out f))
								outECbp.Rate = rates.First().Value;
							else
								throw new ValidationException("Invalid fare rate");
						}
					}
				}
				catch (Exception ex)
				{
					logger.Error("ECar:Rate", ex);
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ GetRate");
				swEachPart.Stop();
				logger.DebugFormat("Get Rate Duration:{0} ms.", swEachPart.ElapsedMilliseconds);


				swEachPart.Restart();
				#region Get Commission
				try
				{
					var rateQuialifier = request.GroundReservations.First().RateQualifiers.First();
					if (rateQuialifier.SpecialInputs != null)
					{
						//Commission
						var commissions = rateQuialifier.SpecialInputs.Where(s => s.Name.ToLower() == "commission".ToLower()).ToList();


						if (commissions != null && commissions.Any())
						{
							decimal f;
							if (decimal.TryParse(commissions.First().Value, out f))
								outECbp.commission = commissions.First().Value;
							else
								throw new ValidationException("Invalid commission");
						}
					}
				}
				catch (Exception ex)
				{
					logger.Error("ECar:Commission", ex);
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ GetCommission");
				swEachPart.Stop();
				logger.DebugFormat("Get Commission Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Get number of passenger
				try
				{
					if (request.TPA_Extensions.Passengers != null && request.TPA_Extensions.Passengers.Any())
					{
						outECbp.numberOfPassenger = request.TPA_Extensions.Passengers.Sum(x => x.Quantity);
					}
					else
					{
						outECbp.numberOfPassenger = 1;
					}
				}
				catch (Exception ex)
				{
					logger.Error("ECar: Number of Passenger", ex);
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ GetNumberOfPassenger");
				swEachPart.Stop();
				logger.DebugFormat("Get Number of Passenger Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Get controller notes
				try
				{
					var rateQuialifier = request.GroundReservations.First().RateQualifiers.First();
					if (rateQuialifier.SpecialInputs != null)
					{
						//rate
						var controllerNotes = rateQuialifier.SpecialInputs.Where(s => s.Name.ToLower() == "ControllerNotes".ToLower()).FirstOrDefault();

						if (controllerNotes != null && !string.IsNullOrWhiteSpace(controllerNotes.Value))
						{
							outECbp.controllerNotes = controllerNotes.Value;
						}
					}
				}
				catch (Exception ex)
				{
					logger.Error("ECar: Get controller notes", ex);
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ GeControllerNotes");
				swEachPart.Stop();
				logger.DebugFormat("Get controller notes Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                outECbp.Fleet_TripCode = inputECbp.Fleet_TripCode;
                logger.InfoFormat("Fleet Trip Code:{0} ", outECbp.Fleet_TripCode);
               
				swEachPart.Restart();
				#region Create this trip into database
				if (result)
				{
					vtod_trip trip = CreateNewTrip(outECbp, tokenVTOD.Username);
					outECbp.Trip = trip;
				}
				else
				{
					result = false;
					var vtodException = VtodException.CreateException(ExceptionType.ECar, 2001);
					logger.Warn(vtodException.ExceptionMessage.Message);
					throw vtodException;
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest_ GreateTripInDatabase");
				swEachPart.Stop();
				logger.DebugFormat("CreateNewTrip in db  Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

			}
			else
			{
				result = false;
				var vtodException = VtodException.CreateFieldRequiredValidationException("GroundReservation");
				logger.Warn(vtodException.ExceptionMessage.Message);
				throw vtodException; 
			}
			#endregion

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);


			#region Create ECar log
			if (result)
			{
				if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
				{
					WriteECarLog(outECbp.Trip.Id, outECbp.serviceAPIID, ECarServiceMethodType.Book, sw.ElapsedMilliseconds, string.Empty, string.Empty, null, null);
				}
			}
			else
			{
				result = false;
				logger.Warn(Messages.ECar_Common_UnableProcessByPreviousInvalidRequest);
				throw new ValidationException(Messages.ECar_Common_UnableProcessByPreviousInvalidRequest);
			}
			#endregion

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateBookingRequest");
			#endregion


			return result;

		}
        
		protected bool IsExpiredForBooking(string dispatchServiceId)
		{
			bool result = true;

			if (dispatchServiceId.ToLowerInvariant().Contains("expired"))
			{
				string[] sep = { "," };
				List<string> parameters = dispatchServiceId.Split(sep, StringSplitOptions.None).ToList();

				foreach (string para in parameters)
				{
					if (Regex.IsMatch(para, "Expired=(.*)", RegexOptions.IgnoreCase))
					{
						string[] format = { "MM/dd/yyyy HH:mm:ss" };
						string tempResult = Regex.Match(dispatchServiceId, "Expired=(.*)", RegexOptions.IgnoreCase).Groups[1].Value.Trim();
						DateTime dt = new DateTime();
						if (DateTime.TryParseExact(tempResult, format, new CultureInfo("en-US"), DateTimeStyles.None, out dt))
						{
							if (DateTime.Compare(dt, DateTime.Now) >= 0)
							{
								result = false;
							}
							else
							{
								result = true;
								throw new Exception("Unable to extract expired date from dispatchServiceId. Invalid date string.");
							}
						}
						else
						{
							result = true;
							throw new Exception("Unable to extract expired date from dispatchServiceId. Invalid date string.");
						}
					}
				}
			}
			else
			{
				result = true;
				throw new Exception("Wrong dispatch service ID confiruration. check ServiceID part");
			}

			return result;

		}
        public string ConvertToLocalTime(ecar_fleet fleet, DateTime dt)
        {
            return dt.FromUtc(fleet.ServerUTCOffset).ToString("yyyy-MM-ddTHH:mm:ss");
        }


        public bool ValidateGetEstimationRequestForGreenTomato(TokenRS tokenVTOD, OTA_GroundAvailRQ reservation, ECarGetEstimationParameter inECgep,  out ECarGetEstimationParameter outECgep)
		{
			bool result = true;
			Stopwatch sw = new Stopwatch();
			sw.Start();
			outECgep = new ECarGetEstimationParameter();
            outECgep = inECgep;

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
			#endregion


			if (reservation.Service != null)
			{

				#region Token
				outECgep.tokenVTOD = tokenVTOD;
				#endregion

				#region Validate Pickup address
				if (reservation.Service.Pickup != null)
				{
					if (((reservation.Service.Pickup.Address != null) && (reservation.Service.Pickup.AirportInfo != null)) || ((reservation.Service.Pickup.Address == null) && (reservation.Service.Pickup.AirportInfo == null)))
					{
						result = false;
						logger.Warn(Messages.ECar_Common_InvalidReservationServiceLocationPickupContainsBothPickupAddressAndAirportInfo);
						throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocationPickupContainsBothPickupAddressAndAirportInfo);
					}
					else if (reservation.Service.Pickup.Address != null && reservation.Service.Pickup.AirportInfo == null)
					{
						if ((string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.Longitude) || (string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.CountryName.Code)))
						{
							result = false;
							logger.Warn(Messages.ECar_Common_InvalidReservationServiceLocationPickupDetail);
							throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocationPickupDetail);
						}

						else if (!string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.AddressLine) && !string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.CityName) && !string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.PostalCode) && !string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.CountryName.Code))
						{
							//Address
							Map.DTO.Address pickupAddress = null;
							string pickupAddressStr = GetAddressString(reservation.Service.Pickup.Address);
							//if (reservation.Service.Pickup.Address.TPA_Extensions != null)
							//{
							//	if (reservation.Service.Pickup.Address.TPA_Extensions.AddressValidationRequired.HasValue ? reservation.Service.Pickup.Address.TPA_Extensions.AddressValidationRequired.Value : false)
							//	{
							//		if (!LookupAddress(reservation.Service.Pickup, pickupAddressStr, out pickupAddress))
							//		{
							//			result = false;
							//			logger.WarnFormat(Messages.ECar_Common_InvalidReservationServiceLocationPickupDetailByMAPAPI(pickupAddressStr));
							//			throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocationPickupDetailByMAPAPI(pickupAddressStr));
							//		}
							//	}
							//	else
							//	{
							//		if (!string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.Longitude) && !string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.Latitude))
							//		{
							//			logger.Info(Messages.ECar_Common_SkipReservationServiceLocationPickupDetailByMAPAPI);
							//			pickupAddress = ConvertAddress(reservation.Service.Pickup);
							//		}
							//		else
							//		{
							//			result = false;
							//			logger.WarnFormat(Messages.ECar_Common_InvalidReservationServiceLocationNoMAPAPIWithEmptyLongAndLat(pickupAddressStr));
							//			throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocationNoMAPAPIWithEmptyLongAndLat(pickupAddressStr));
							//		}
							//	}
							//}
							//else
							//{
							logger.Info(Messages.ECar_Common_SkipReservationServiceLocationPickupDetailByMAPAPI);
							pickupAddress = ConvertAddress(reservation.Service.Pickup);
							//}
							outECgep.PickupAddressType = UDI.VTOD.Domain.ECar.Const.AddressType.Address;
							outECgep.PickupAddress = pickupAddress;
							logger.Info("Pickup AddressType: Address");
						}

						else if ((!string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.Longitude) && (!string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.CountryName.Code)))
						{
							outECgep.PickupAddressOnlyContainsLatAndLong = true;
							MapService ms = new MapService();
							Map.DTO.Geolocation g = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(reservation.Service.Pickup.Address.Latitude), Longitude = Convert.ToDecimal(reservation.Service.Pickup.Address.Longitude) };
							string error = "";
							var addressList = ms.GetAddressesFromGeoLocationWithProximity(g, out error);

							if (string.IsNullOrWhiteSpace(error) && addressList.Any())
							{
								var add = addressList.FirstOrDefault();

								reservation.Service.Pickup.Address.StreetNmbr = add.StreetNo;
								reservation.Service.Pickup.Address.AddressLine = add.Street;
								reservation.Service.Pickup.Address.BldgRoom = add.ApartmentNo;
								reservation.Service.Pickup.Address.CityName = add.City;
								reservation.Service.Pickup.Address.PostalCode = add.ZipCode;
								reservation.Service.Pickup.Address.StateProv = new StateProv { StateCode = add.State };
								reservation.Service.Pickup.Address.CountryName = new CountryName { Code = add.Country };
								reservation.Service.Pickup.Address.StreetNmbr = add.StreetNo;
								reservation.Service.Pickup.Address.StreetNmbr = add.StreetNo;

								outECgep.PickupAddressType = UDI.VTOD.Domain.ECar.Const.AddressType.Address;
								outECgep.PickupAddress = ConvertAddress(reservation.Service.Pickup);
								logger.Info("Pickup AddressType: Address");

							}
							else
							{
								result = false;
								logger.Warn(Messages.ECar_Common_InvalidReservationServiceLocationPickupDetail);
								throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocationPickupDetail);
							}



						}
						else
						{
							result = false;
							logger.Warn(Messages.ECar_Common_UnknownError);
							throw new ValidationException(Messages.ECar_Common_UnknownError);
						}
					}
					else if ((reservation.Service.Pickup.Address == null) && (reservation.Service.Pickup.AirportInfo != null))
					{
						//airport
						if (((reservation.Service.Pickup.AirportInfo.Arrival != null) && (reservation.Service.Pickup.AirportInfo.Departure != null)) || ((reservation.Service.Pickup.AirportInfo.Arrival == null) && (reservation.Service.Pickup.AirportInfo.Departure == null)))
						{
							result = false;
							logger.Warn(Messages.ECar_Common_InvalidReservationServiceLocationPickupAirportInfoContainsBothArrivalAndDepature);
							throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocationPickupAirportInfoContainsBothArrivalAndDepature);
						}
						else if (reservation.Service.Pickup.AirportInfo.Arrival != null && reservation.Service.Pickup.AirportInfo.Departure == null)
						{
							//Arrival
							outECgep.PickupAddressType = AddressType.Airport;
							outECgep.PickupAirport = reservation.Service.Pickup.AirportInfo.Arrival.LocationCode;
							logger.Info("Pickup AddressType: AirportInfo.Arrival");
						}
						else if (reservation.Service.Pickup.AirportInfo.Arrival == null && reservation.Service.Pickup.AirportInfo.Departure != null)
						{
							//Depature

							outECgep.PickupAddressType = AddressType.Airport;
							outECgep.PickupAirport = reservation.Service.Pickup.AirportInfo.Departure.LocationCode;
							logger.Info("Pickup AddressType: AirportInfo.Depature");
						}

						//longitude and latitude
						if (result)
						{
							double? longitude = null;
							double? latitude = null;
							if (GetLongitudeAndLatitudeForAirport(outECgep.PickupAirport, out longitude, out latitude))
							{
								outECgep.LongitudeForPickupAirport = longitude.Value;
								outECgep.LatitudeForPickupAirport = latitude.Value;
							}
							else
							{
								throw new ValidationException(Messages.ECar_Common_UnableToFindAirportLongitudeAndLatitude);
							}
						}

						//throw new ValidationException(Messages.ECar_Common_InvalidAirportPickup);
					}


				}
				else
				{
					result = false;
					logger.Warn("request.Service.Pickup is null");
				}

				#endregion

				#region Validate Pickup Fleet
				if (result)
				{
					ecar_fleet fleet = null;
					if (GetFleet(outECgep.PickupAddressType, outECgep.PickupAddress, outECgep.PickupAirport, out fleet))
					{
						outECgep.Fleet = fleet;
						logger.InfoFormat("Fleet found. ID={0}", fleet.Id);

						if (fleet != null)
						{
							outECgep.serviceAPIID = GetECarFleetServiceAPIPreference(fleet.Id, ECarServiceAPIPreferenceConst.GetEstimation).ServiceAPIId;
						}
					}
					else
					{
						result = false;
						logger.Warn("Unable to find fleet by this address or landmark.");
					}
				}
				else
				{
					result = false;
					logger.Warn("Unable to find fleet by invalid address or landmark.");
				}
				#endregion

				#region Validate Drop off address
				if (result)
				{
					//drop off address can be address or airportInfo
					if (reservation.Service.Dropoff != null)
					{
						using (VTODEntities context = new VTODEntities())
						{
							if (((reservation.Service.Dropoff.Address != null) && (reservation.Service.Dropoff.AirportInfo != null)) || ((reservation.Service.Dropoff.Address == null) && (reservation.Service.Dropoff.AirportInfo == null)))
							{
								result = false;
								var vtodException = VtodException.CreateValidationException(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
								logger.Warn(vtodException.ExceptionMessage.Message);
								throw vtodException; // new ValidationException(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
							}
							else if ((reservation.Service.Dropoff.Address != null) && (reservation.Service.Dropoff.AirportInfo == null))
							{
								//Address
								if ((string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.Longitude) || (string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.CountryName.Code)))
								{
									result = false;
									logger.Warn(Messages.Validation_LocationDetail);
									throw VtodException.CreateValidationException(Messages.Validation_LocationDetail);
								}
								else if (!string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.AddressLine) && !string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.CityName) && !string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.PostalCode) && !string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.CountryName.Code))
								{
									Map.DTO.Address dropoffAddress = null;
									string dropoffAddressStr = GetAddressString(reservation.Service.Dropoff.Address);
									//if (reservation.Service.Dropoff.Address.TPA_Extensions != null)
									//{
									//	if (reservation.Service.Dropoff.Address.TPA_Extensions.AddressValidationRequired.HasValue ? reservation.Service.Dropoff.Address.TPA_Extensions.AddressValidationRequired.Value : false)
									//	{
									//		if (!LookupAddress(reservation.Service.Dropoff, dropoffAddressStr, out dropoffAddress))
									//		{
									//			result = false;
									//			logger.WarnFormat(Messages.Validation_LocationDetailByMapApi);
									//			throw VtodException.CreateValidationException(Messages.Validation_LocationDetailByMapApi);
									//		}
									//	}
									//	else
									//	{
									//		if (!string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.Longitude) && !string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.Latitude))
									//		{
									//			logger.Info("Skip MAP API Validation.");
									//			dropoffAddress = ConvertAddress(reservation.Service.Dropoff);
									//		}
									//		else
									//		{
									//			result = false;
									//			logger.WarnFormat(Messages.ECar_Common_InvalidReservationServiceLocationNoMAPAPIWithEmptyLongAndLat(dropoffAddressStr));
									//			throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocationNoMAPAPIWithEmptyLongAndLat(dropoffAddressStr));
									//		}
									//	}
									//}
									//else
									//{
									logger.Info("Skip MAP API Validation.");
									dropoffAddress = ConvertAddress(reservation.Service.Dropoff);
									//}

									outECgep.DropOffAddress = dropoffAddress;
									outECgep.DropOffAddressType = UDI.VTOD.Domain.ECar.Const.AddressType.Address;
									logger.Info("Dropoff AddressType=Address");
								}
								else if ((!string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.Longitude) && (!string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.CountryName.Code)))
								{
									outECgep.DropOffAddressOnlyContainsLatAndLong = true;


									MapService ms = new MapService();
									Map.DTO.Geolocation g = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(reservation.Service.Dropoff.Address.Latitude), Longitude = Convert.ToDecimal(reservation.Service.Dropoff.Address.Longitude) };
									string error = "";
									var addressList = ms.GetAddressesFromGeoLocationWithProximity(g, out error);

									if (string.IsNullOrWhiteSpace(error) && addressList.Any())
									{
										var add = addressList.FirstOrDefault();

										reservation.Service.Dropoff.Address.StreetNmbr = add.StreetNo;
										reservation.Service.Dropoff.Address.AddressLine = add.Street;
										reservation.Service.Dropoff.Address.BldgRoom = add.ApartmentNo;
										reservation.Service.Dropoff.Address.CityName = add.City;
										reservation.Service.Dropoff.Address.PostalCode = add.ZipCode;
										reservation.Service.Dropoff.Address.StateProv = new StateProv { StateCode = add.State };
										reservation.Service.Dropoff.Address.CountryName = new CountryName { Code = add.Country };
										reservation.Service.Dropoff.Address.StreetNmbr = add.StreetNo;
										reservation.Service.Dropoff.Address.StreetNmbr = add.StreetNo;
									}
									else
									{
										result = false;
										logger.Warn(Messages.ECar_Common_InvalidReservationServiceLocationPickupDetail);
										throw new ValidationException(Messages.ECar_Common_InvalidReservationServiceLocationPickupDetail);
									}

								}

								else
								{
									result = false;
									logger.Warn(Messages.ECar_Common_UnknownError);
									throw new ValidationException(Messages.ECar_Common_UnknownError);
								}
							}
							else if ((reservation.Service.Dropoff.Address == null) && (reservation.Service.Dropoff.AirportInfo != null))
							{

								//airport
								if (((reservation.Service.Dropoff.AirportInfo.Arrival != null) && (reservation.Service.Dropoff.AirportInfo.Departure != null)) || ((reservation.Service.Dropoff.AirportInfo.Arrival == null) && (reservation.Service.Dropoff.AirportInfo.Departure == null)))
								{
									result = false;
									var vtodException = VtodException.CreateValidationException(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
									logger.Warn(vtodException.ExceptionMessage.Message);
									throw vtodException; // new ValidationException(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
								}
								else if (reservation.Service.Dropoff.AirportInfo.Arrival != null && reservation.Service.Dropoff.AirportInfo.Departure == null)
								{
									//Arrival
									outECgep.DropOffAddressType = AddressType.Airport;
									outECgep.DropoffAirport = reservation.Service.Dropoff.AirportInfo.Arrival.LocationCode;
									logger.Info("Dropoff AddressType: AirportInfo.Arrival");
								}
								else if (reservation.Service.Dropoff.AirportInfo.Arrival == null && reservation.Service.Dropoff.AirportInfo.Departure != null)
								{
									//Depature
									outECgep.DropOffAddressType = AddressType.Airport;
									outECgep.DropoffAirport = reservation.Service.Dropoff.AirportInfo.Departure.LocationCode;
									logger.Info("Dropoff AddressType: AirportInfo.Depature");
								}

								//longitude and latitude
								if (result)
								{
									double? longitude = null;
									double? latitude = null;
									if (GetLongitudeAndLatitudeForAirport(outECgep.DropoffAirport, out longitude, out latitude))
									{
										outECgep.LongitudeForDropoffAirport = longitude.Value;
										outECgep.LatitudeForDropoffAirport = latitude.Value;
									}
									else
									{
										throw new ValidationException(Messages.ECar_Common_UnableToFindAirportLongitudeAndLatitude);
									}
								}
							}
						}
					}
				}
				else
				{
					result = false;
					logger.Warn("Cannot get estimation by previous invalid request ");
				}
				#endregion

				#region Validate Available address type
				if (result)
				{
					string pickupAddressType = outECgep.PickupAddressType;
					string dropoffAddressType = outECgep.DropOffAddressType;

					//verify the address type of pickup address and drop off address
					UtilityDomain ud = new UtilityDomain();
					if (pickupAddressType == AddressType.Address.ToString())
					{
						UtilityGetAirportNameRQ uganRQ = new UtilityGetAirportNameRQ();
						uganRQ.Address = new Address();
						uganRQ.Address.Latitude = outECgep.PickupAddress.Geolocation.Latitude.ToString();
						uganRQ.Address.Longitude = outECgep.PickupAddress.Geolocation.Longitude.ToString();

						UtilityGetAirportNameRS uganRS = ud.GetAirportName(tokenVTOD, uganRQ);
						if (uganRS.Success != null)
						{
							if (!string.IsNullOrWhiteSpace(uganRS.AirportName))
							{
								pickupAddressType = AddressType.Airport;
							}
						}

					}

					if (dropoffAddressType == AddressType.Address.ToString())
					{
						UtilityGetAirportNameRQ uganRQ = new UtilityGetAirportNameRQ();
						uganRQ.Address = new Address();
						uganRQ.Address.Latitude = outECgep.DropOffAddress.Geolocation.Latitude.ToString();
						uganRQ.Address.Longitude = outECgep.DropOffAddress.Geolocation.Longitude.ToString();

						UtilityGetAirportNameRS uganRS = ud.GetAirportName(tokenVTOD, uganRQ);
						if (uganRS.Success != null)
						{
							if (!string.IsNullOrWhiteSpace(uganRS.AirportName))
							{
								dropoffAddressType = AddressType.Airport;
							}
						}
					}

					//check database setting
					//1. get ecar_fleet_availableaddresstype
					using (VTODEntities context = new VTODEntities())
					{
						long fleetId = outECgep.Fleet.Id;
						if (context.ecar_fleet_availableaddresstype.Where(x => fleetId == x.FleetId).Any())
						{
							ecar_fleet_availableaddresstype tfa = context.ecar_fleet_availableaddresstype.Where(x => fleetId == x.FleetId).Select(x => x).FirstOrDefault();
							//2. check if the type is available or not
							if (AddressType.Address.ToString() == pickupAddressType && AddressType.Address.ToString() == dropoffAddressType)
							{
								//address to address
								if (tfa.AddressToAddress)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.ECar, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
							else if (AddressType.Address.ToString() == pickupAddressType && AddressType.Airport.ToString() == dropoffAddressType)
							{
								//address to airport
								if (tfa.AddressToAirport)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.ECar, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
							else if (AddressType.Address.ToString() == pickupAddressType && null == dropoffAddressType)
							{
								//address to null
								if (tfa.AddressToNull)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.ECar, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
							else if (AddressType.Airport.ToString() == pickupAddressType && AddressType.Address.ToString() == dropoffAddressType)
							{
								//airport to address
								if (tfa.AirportToAddress)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.ECar, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
							else if (AddressType.Airport.ToString() == pickupAddressType && AddressType.Airport.ToString() == dropoffAddressType)
							{
								//airport to airport
								if (tfa.AirportToAirport)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.ECar, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
							else if (AddressType.Airport.ToString() == pickupAddressType && null == dropoffAddressType)
							{
								//airport to null
								if (tfa.AirportToNull)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.ECar, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
						}
						else
						{
							result = false;
							throw VtodException.CreateException(ExceptionType.ECar, 1011); //throw new ValidationException(Messages.ECar_Common_UnableToGetAvailableFleets);
						}
					}


				}
				else
				{
					result = false;
					logger.Warn(Messages.ECar_Common_UnableProcessByPreviousInvalidRequest);
					throw new ValidationException(Messages.ECar_Common_UnableProcessByPreviousInvalidRequest);
				}
				#endregion

				#region Get Fleet User
				if (result)
				{
					ecar_fleet_user user = null;
					if (GetECarFleetUser(outECgep.Fleet.Id, tokenVTOD.Username, out user))
					{
						outECgep.User = user;
						logger.Info("This user has sufficient privilege.");
					}
					else
					{
						result = false;
						logger.Warn(Messages.ECar_Common_InsufficientPrivilege(tokenVTOD.Username, outECgep.Fleet.Id));
						throw new ValidationException(Messages.ECar_Common_InsufficientPrivilege(tokenVTOD.Username, outECgep.Fleet.Id));
					}
				}
				else
				{
					result = false;
					logger.Warn(Messages.ECar_Common_UnableProcessByPreviousInvalidRequest);
					throw new ValidationException(Messages.ECar_Common_UnableProcessByPreviousInvalidRequest);
				}
				#endregion


				#region Get Gratuity
				try
				{

					if (reservation.RateQualifiers.Any()
						&& reservation.RateQualifiers.Where(x => x.RateQualifierValue != null).Any()
						&& reservation.RateQualifiers[0].SpecialInputs.Where(x => x.Name.ToLower() == "gratuity").Any()
						)
					{
						var gratuities = reservation.RateQualifiers[0].SpecialInputs.Where(s => s.Name.ToLower() == "gratuity").ToList();

						if (gratuities != null && gratuities.Any())
						{
							outECgep.Gratuity = gratuities.First().Value;
						}
					}
				}
				catch (Exception ex)
				{
					logger.Error("ECar:Gratuity", ex);
				}
				#endregion


				sw.Stop();
				logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			}

			#region Create ecar log
			if (result)
			{
				if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
				{
					WriteECarLog(null, ECarServiceAPIConst.None, ECarServiceMethodType.Estimation, sw.ElapsedMilliseconds, string.Empty, string.Empty, null, null);
				}
			}
			else
			{
				result = false;
				logger.Warn("Cannot create or get this ecar log by invalid trip");
			}
			#endregion

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateGetEstimationRequest");
			#endregion

			return result;
		}
        public bool ValidateGetVehicleInfoRequest(TokenRS tokenVTOD, OTA_GroundAvailRQ request, out ECarGetVehicleInfoParameter tgvip)
        {
            bool result = true;
            Stopwatch sw = new Stopwatch();
            sw.Start();
            tgvip = new ECarGetVehicleInfoParameter();


            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
            #endregion

            #region Set token, request
            tgvip.tokenVTOD = tokenVTOD;
            tgvip.request = request;
            #endregion


            if (request.Service != null)
            {
                if (request.Service.Pickup != null)
                {
                    #region Validate longtitude, latitude
                    if (request.Service.Pickup.Address != null)
                    {
                        if (!string.IsNullOrWhiteSpace(request.Service.Pickup.Address.Latitude) && !string.IsNullOrWhiteSpace(request.Service.Pickup.Address.Longitude))
                        {
                            double longtitue = 0;
                            double latitude = 0;
                            if (double.TryParse(request.Service.Pickup.Address.Longitude, out longtitue) && double.TryParse(request.Service.Pickup.Address.Latitude, out latitude))
                            {
                                tgvip.Longtitude = longtitue;
                                tgvip.Latitude = latitude;
                            }
                            else
                            {
                                result = false;
                                logger.Warn("Invalid longtitude and latittude");
                            }
                        }
                        else
                        {
                            result = false;
                            logger.Warn("Invalid longtitude and latittude");
                        }
                    }
                    else
                    {
                        result = false;
                        logger.Warn("Cannot find any longtitude or latitude from address");
                    }
                    #endregion

                    #region Validate Pickup Address
                    if (result)
                    {
                        Pickup_Dropoff_Stop stop = request.Service.Pickup;

                        if ((stop.Address != null) && (stop.AirportInfo == null))
                        {
                            if (!string.IsNullOrWhiteSpace(stop.Address.CountryName.Code) && !string.IsNullOrWhiteSpace(stop.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(stop.Address.CityName) && !string.IsNullOrWhiteSpace(stop.Address.PostalCode))
                            {
                                Map.DTO.Address pickupAddress = new Map.DTO.Address { Country = stop.Address.CountryName.Code, State = stop.Address.StateProv.StateCode, City = stop.Address.CityName, ZipCode = stop.Address.PostalCode, Geolocation = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(stop.Address.Latitude), Longitude = Convert.ToDecimal(stop.Address.Longitude) } };
                                tgvip.PickupAddressType = UDI.VTOD.Domain.ECar.Const.AddressType.Address;
                                tgvip.PickupAddress = pickupAddress;
                            }
                            else if (!string.IsNullOrWhiteSpace(stop.Address.CountryName.Code) && !string.IsNullOrWhiteSpace(stop.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(stop.Address.CityName))
                            {
                                Map.DTO.Address pickupAddress = new Map.DTO.Address { Country = stop.Address.CountryName.Code, State = stop.Address.StateProv.StateCode, City = stop.Address.CityName, Geolocation = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(stop.Address.Latitude), Longitude = Convert.ToDecimal(stop.Address.Longitude) } };
                                tgvip.PickupAddressType = UDI.VTOD.Domain.ECar.Const.AddressType.Address;
                                tgvip.PickupAddress = pickupAddress;

                            }
                            else if (!string.IsNullOrWhiteSpace(stop.Address.LocationName))
                            {
                                #region Not support now
                                //tgvip.PickupAddressType = UDI.VTOD.Domain.ECar.Const.AddressType.LocationName;
                                //tgvip.PickupLandmark = stop.Address.LocationName;
                                #endregion

                                result = false;
                                logger.Warn("Cannot use locationName from address.");
                            }
                            else
                            {
                                result = false;
                                logger.Warn("Cannot find any address/locationName from address.");
                            }
                        }
                        else if ((stop.Address == null) && (stop.AirportInfo != null))
                        {
                            if ((stop.AirportInfo.Arrival != null) && (stop.AirportInfo.Departure == null))
                            {
                                #region Not support now
                                //tgvip.PickupAddressType = UDI.VTOD.Domain.ECar.Const.AddressType.AirportInfo;
                                //tgvip.PickupLandmark = stop.AirportInfo.Arrival.LocationCode;
                                #endregion

                                result = false;
                                logger.Warn("Cannot use aiportinfo for address");
                            }
                            else if ((stop.AirportInfo.Arrival == null) && (stop.AirportInfo.Departure != null))
                            {
                                #region Not support now
                                //tgvip.PickupAddressType = UDI.VTOD.Domain.ECar.Const.AddressType.AirportInfo;
                                //tgvip.PickupLandmark = stop.AirportInfo.Departure.LocationCode;
                                #endregion

                                result = false;
                                logger.Warn("Cannot use aiportinfo for address");
                            }
                            else if ((stop.AirportInfo.Arrival != null) && (stop.AirportInfo.Departure != null))
                            {
                                result = false;
                                logger.Warn("This OTA request contains both AirportInfo.Arrival and AirportInfo.Departure");
                            }
                            else
                            {
                                result = false;
                                logger.Warn("Cannot find any arrival aor depature from AirportInfo");
                            }
                        }
                        else
                        {
                            //find address by longtitude and latitude
                            Map.DTO.Address pickupAddress = null;
                            if (!LookupAddress(tgvip.Longtitude, tgvip.Latitude, out pickupAddress))
                            {
                                logger.WarnFormat("Cannot find longtitude={0} and latitude={1}", tgvip.Longtitude, tgvip.Latitude);
                                result = false;
                            }

                            tgvip.PickupAddressType = UDI.VTOD.Domain.ECar.Const.AddressType.Address;
                            tgvip.PickupAddress = pickupAddress;
                        }
                    }
                    else
                    {
                        result = false;
                    }
                    #endregion

                    #region Validate Fleet
                    if (result)
                    {
                        ecar_fleet fleet = null;
                        if (GetFleet(tgvip.PickupAddressType, tgvip.PickupAddress, tgvip.PickupLandmark, out fleet))
                        {
                            tgvip.Fleet = fleet;
                            if (tgvip.Fleet.UDI33DNI != null)
                            {
                                tgvip.APIInformation = tgvip.Fleet.UDI33DNI.ToString();
                            }
                            else if ((tgvip.Fleet.CCSiFleetId != null) && (tgvip.Fleet.CCSiSource != null))
                            {
                                tgvip.APIInformation = string.Format("{0}_{1}", tgvip.Fleet.CCSiSource, tgvip.Fleet.CCSiFleetId);
                            }
                            logger.InfoFormat("Fleet found. ID={0}", fleet.Id);

                            if (fleet != null)
                            {
                                var serviceAPIPreference = GetECarFleetServiceAPIPreference(fleet.Id, ECarServiceAPIPreferenceConst.GetVehicleInfo);
                                tgvip.serviceAPIID = serviceAPIPreference.ServiceAPIId;
                                tgvip.extendedServiceAPIID = serviceAPIPreference.ExtendedServiceAPIId;
                                #region Fetching Endpoint Names
                                List<string> endpointNames = MtdataEndpointConfiguration(tgvip.serviceAPIID, tgvip.Fleet.Id);
                                if (endpointNames != null && endpointNames.Any())
                                {
                                    tgvip.AuthenticationServiceEndpointName = endpointNames[0];
                                    tgvip.BookingWebServiceEndpointName = endpointNames[1];
                                    tgvip.OsiWebServiceEndpointName = endpointNames[2];
                                    tgvip.AddressWebServiceEndpointName = endpointNames[3];
                                }
                                #endregion
                            }
                        }
                        else
                        {
                            result = false;
                            logger.Warn("Unable to find fleet by this address or landmark.");
                        }
                    }
                    else
                    {
                        result = false;
                        logger.Warn("Unable to find fleet by invalid address or landmark.");
                    }
                    #endregion

                    #region Validate Zone
                    if (result)
                    {
                        //Default: Circle
                        if (request.Service.Pickup.Address.TPA_Extensions != null)
                        {
                            #region Validate Polygon Type
                            if (request.Service.Pickup.Address.TPA_Extensions.Zone != null)
                            {
                                if (request.Service.Pickup.Address.TPA_Extensions.Zone.Circle != null)
                                {
                                    tgvip.Radius = double.Parse(request.Service.Pickup.Address.TPA_Extensions.Zone.Circle.Radius.ToString());
                                    tgvip.PolygonType = PolygonTypeConst.Circle;
                                }

                                if (string.IsNullOrWhiteSpace(tgvip.PolygonType))
                                {
                                    result = false;
                                    logger.Warn("Unable to find the polygon area for GetVehicleInfo");
                                }
                            }
                            else
                            {
                                result = false;
                                logger.Warn("request.TPA_Extensions.Zone");
                            }
                            #endregion

                            #region Validate Vehicle status
                            if (request.Service.Pickup.Address.TPA_Extensions.Vehicles != null)
                            {
                                if (request.Service.Pickup.Address.TPA_Extensions.Vehicles.Items.Any())
                                {
                                    //tgvip.VehicleStausList = request.Service.Pickup.Address.TPA_Extensions.Vehicles.Items;
                                    if (!"all".Equals(request.Service.Pickup.Address.TPA_Extensions.Vehicles.Items.FirstOrDefault().VehicleStatus.ToLower()))
                                    {
                                        Vehicle v = new Vehicle { VehicleStatus = request.Service.Pickup.Address.TPA_Extensions.Vehicles.Items.FirstOrDefault().VehicleStatus.ToString() };
                                        tgvip.VehicleStausList.Add(v);
                                    }
                                    else
                                    {
                                        logger.Info("Vehicle status = ALL");
                                    }
                                }
                                else
                                {
                                    result = false;
                                    logger.Warn("request.Service.Pickup.Address.TPA_Extensions.Vehicles.Items is empty");
                                }

                            }
                            else
                            {
                                result = false;
                                logger.Info("Unable to find request.TPA_Extensions.Vehicles");
                            }

                            #endregion
                        }
                        else
                        {
                            result = false;
                            logger.Warn("Unable to find request.TPA_Extensions");
                        }
                    }
                    else
                    {
                        result = false;
                        logger.Warn("Cannot get polygon type by invalid request");
                    }
                    #endregion

                    //#region GetVehicleStatus
                    //if (result)
                    //{
                    //    if (request.Service.Pickup.Address.TPA_Extensions != null)
                    //    {
                    //        if (request.Service.Pickup.Address.TPA_Extensions.Vehicles != null)
                    //        {
                    //            if (request.Service.Pickup.Address.TPA_Extensions.Vehicles.Items.Any())
                    //            {
                    //                tgvip.VehicleStatus=request.Service.Pickup.Address.TPA_Extensions.Vehicles.Items.FirstOrDefault().VehicleStatus;
                    //            }
                    //            else {
                    //                tgvip.VehicleStatus = "";
                    //            }
                    //        }
                    //        else {
                    //            tgvip.VehicleStatus = "";
                    //        }
                    //    }
                    //    else {
                    //        tgvip.VehicleStatus = "";
                    //    }
                    //}
                    //else {
                    //    result = false;
                    //    logger.Warn("Cannot get vehicle status by invalid request");
                    //}
                    //#endregion


                }
                else
                {
                    result = false;
                    logger.Warn("request.Service.Pickup is null");
                }
            }
            else
            {
                result = false;
                logger.Warn("request.Service is null");
            }

            sw.Stop();
            logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);


            #region Create ECar log
            if (result)
            {
                if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
                {
                    WriteECarLog(null, tgvip.serviceAPIID, ECarServiceMethodType.GetVehicleInfo, sw.ElapsedMilliseconds, string.Empty, string.Empty, null, null);
                }
            }
            else
            {
                result = false;
                logger.Warn("Cannot create ECarLog by invalid request");
            }
            #endregion

            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateGetVehicleInfoRequest");
            #endregion

            return result;
        }
        public bool ValidateFleets(TokenRS token, UtilityAvailableFleetsRQ input, out ecar_fleet f)
        {
            bool result = false;
            f = null;
            Stopwatch sw = new Stopwatch();
            sw.Start();

            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
            #endregion

            if (input.Address != null)
            {
                using (VTODEntities context = new VTODEntities())
                {
                 
                    Map.DTO.Address address = new Map.DTO.Address { Country = input.Address.CountryName.Code, City = input.Address.CityName, State = input.Address.StateProv.StateCode, ZipCode = input.Address.PostalCode, Geolocation = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(input.Address.Latitude), Longitude = Convert.ToDecimal(input.Address.Longitude) } };
                    string addressType = AddressType.Address.ToString();
                    try
                    {
                        if (GetFleet(addressType, address, "", out f))
                        {
                            result = true;
                        }
                    }
                    catch
                    {

                    }
                }
            }
            sw.Stop();
            logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateFleets");
            #endregion
            return result;
        }

        public bool ValidateGetAvailableFleets(TokenRS token, UtilityAvailableFleetsRQ input, long? transactionID/*, out bool PickMeUpNow, out bool PickMeUpLater*/)
        {
            bool result = false;
           // PickMeUpNow = true;
            //PickMeUpLater = true;
            Stopwatch sw = new Stopwatch();
            sw.Start();

            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
            #endregion

            if (input.Address != null)
            {
                using (VTODEntities context = new VTODEntities())
                {
                    //convert to MAPQuest address
                    Map.DTO.Address address = new Map.DTO.Address { Country = input.Address.CountryName.Code, City = input.Address.CityName, State = input.Address.StateProv.StateCode, ZipCode = input.Address.PostalCode, Geolocation = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(input.Address.Latitude), Longitude = Convert.ToDecimal(input.Address.Longitude) } };
                    ecar_fleet f = new ecar_fleet();
                    string addressType = AddressType.Address.ToString();
                    try
                    {
                        if (GetFleet(addressType, address, "", out f))
                        {
                            if (f != null)
                            {
                                #region Validate Available address type

                                string pickupAddressType = "";


                                //verify the address type 
                                UtilityDomain ud = new UtilityDomain();

                                UtilityGetAirportNameRQ uganRQ = new UtilityGetAirportNameRQ();
                                uganRQ.Address = new Address();
                                uganRQ.Address.Latitude = input.Address.Latitude.ToString();
                                uganRQ.Address.Longitude = input.Address.Longitude.ToString();

                                UtilityGetAirportNameRS uganRS = ud.GetAirportName(token, uganRQ);
                                if (uganRS.Success != null)
                                {
                                    if (!string.IsNullOrWhiteSpace(uganRS.AirportName))
                                    {
                                        pickupAddressType = AddressType.Airport;
                                    }
                                    else
                                    {
                                        pickupAddressType = AddressType.Address;
                                    }
                                }
                                else
                                {
                                    pickupAddressType = AddressType.Address;
                                }




                                //check database setting
                                //1. get ecar_fleet_availableaddresstype

                                long fleetId = f.Id;
                                if (context.ecar_fleet_availableaddresstype.Where(x => fleetId == x.FleetId).Any())
                                {
                                    ecar_fleet_availableaddresstype tfa = context.ecar_fleet_availableaddresstype.Where(x => fleetId == x.FleetId).Select(x => x).FirstOrDefault();
                                    //2. check if the type is available or not
                                    //Ecar_Common_InvalidAddressTypeForFleet
                                    if (AddressType.Address.ToString() == pickupAddressType)
                                    {
                                        //address to address
                                        if (tfa.AddressToAddress || tfa.AddressToAirport || tfa.AddressToNull)
                                        {
                                            result = true;
                                        }
                                        else
                                        {
                                            result = false;
                                            throw VtodException.CreateException(ExceptionType.ECar, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                        }
                                    }
                                    else if (AddressType.Airport.ToString() == pickupAddressType)
                                    {
                                        //address to airport
                                        if (tfa.AirportToAddress || tfa.AirportToAirport || tfa.AirportToNull)
                                        {
                                            result = true;
                                        }
                                        else
                                        {
                                            result = false;
                                            throw VtodException.CreateException(ExceptionType.ECar, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                        }
                                    }
                                    else
                                    {
                                        throw new ValidationException("Unknown address type setting");

                                    }

                                }
                                else
                                {
                                    result = false;
                                    throw VtodException.CreateException(ExceptionType.ECar, 1011);// new ValidationException(Messages.ECar_Common_UnableToGetAvailableFleets);
                                }




                                #endregion

                                //GetPickMeupOption(f, out PickMeUpNow, out PickMeUpLater);
                                result = true;
                            }
                        }
                    }
                    catch
                    {
                        //throw;
                    }
                }
            }

            sw.Stop();
            logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateGetAvailableFleets");
            #endregion

            return result;
        }
        public bool ValidateGetAvailableFleets(TokenRS token, UtilityAvailableFleetsRQ input, out bool PickMeUpNow, out bool PickMeUpLater)
        {
            bool result = false;
            PickMeUpNow = true;
            PickMeUpLater = true;
            Stopwatch sw = new Stopwatch();
            sw.Start();

            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
            #endregion

            if (input.Address != null)
            {
                using (VTODEntities context = new VTODEntities())
                {
                    //convert to MAPQuest address
                    Map.DTO.Address address = new Map.DTO.Address { Country = input.Address.CountryName.Code, City = input.Address.CityName, State = input.Address.StateProv.StateCode, ZipCode = input.Address.PostalCode, Geolocation = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(input.Address.Latitude), Longitude = Convert.ToDecimal(input.Address.Longitude) } };
                    ecar_fleet f = new ecar_fleet();
                    string addressType = AddressType.Address.ToString();
                    try
                    {
                        if (GetFleet(addressType, address, "", out f))
                        {
                            if (f != null)
                            {
                                #region Validate Available address type

                                string pickupAddressType = "";


                                //verify the address type 
                                UtilityDomain ud = new UtilityDomain();

                                UtilityGetAirportNameRQ uganRQ = new UtilityGetAirportNameRQ();
                                uganRQ.Address = new Address();
                                uganRQ.Address.Latitude = input.Address.Latitude.ToString();
                                uganRQ.Address.Longitude = input.Address.Longitude.ToString();

                                UtilityGetAirportNameRS uganRS = ud.GetAirportName(token, uganRQ);
                                if (uganRS.Success != null)
                                {
                                    if (!string.IsNullOrWhiteSpace(uganRS.AirportName))
                                    {
                                        pickupAddressType = AddressType.Airport;
                                    }
                                    else
                                    {
                                        pickupAddressType = AddressType.Address;
                                    }
                                }
                                else
                                {
                                    pickupAddressType = AddressType.Address;
                                }
                                //check database setting
                                //1. get ecar_fleet_availableaddresstype

                                long fleetId = f.Id;
                                if (context.ecar_fleet_availableaddresstype.Where(x => fleetId == x.FleetId).Any())
                                {
                                    ecar_fleet_availableaddresstype tfa = context.ecar_fleet_availableaddresstype.Where(x => fleetId == x.FleetId).Select(x => x).FirstOrDefault();
                                    //2. check if the type is available or not
                                    //ecar_Common_InvalidAddressTypeForFleet
                                    if (AddressType.Address.ToString() == pickupAddressType)
                                    {
                                        //address to address
                                        if (tfa.AddressToAddress || tfa.AddressToAirport || tfa.AddressToNull)
                                        {
                                            result = true;
                                        }
                                        else
                                        {
                                            result = false;
                                            throw VtodException.CreateException(ExceptionType.ECar, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                        }
                                    }
                                    else if (AddressType.Airport.ToString() == pickupAddressType)
                                    {
                                        //address to airport
                                        if (tfa.AirportToAddress || tfa.AirportToAirport || tfa.AirportToNull)
                                        {
                                            result = true;
                                        }
                                        else
                                        {
                                            result = false;
                                            throw VtodException.CreateException(ExceptionType.ECar, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                        }
                                    }
                                    else
                                    {
                                        throw new ValidationException("Unknown address type setting");

                                    }

                                }
                                else
                                {
                                    result = false;
                                    throw VtodException.CreateException(ExceptionType.ECar, 1011);// new ValidationException(Messages.ECar_Common_UnableToGetAvailableFleets);
                                }




                                #endregion

                                GetPickMeupOption(f, out PickMeUpNow, out PickMeUpLater);
                                result = true;
                            }
                        }
                    }
                    catch
                    {
                        //throw;
                    }
                }
            }

            sw.Stop();
            logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateGetAvailableFleets");
            #endregion

            return result;
        }
        

        public string GetDispatchTripId(long vtodTripId)
        {
            string dispatchTripId = string.Empty;
            using (VTODEntities context = new VTODEntities())
            {
                dispatchTripId = context.ecar_trip.Where(x => x.Id == vtodTripId).FirstOrDefault().DispatchTripId;
            }
            return dispatchTripId;
        }

        public vtod_trip GetVtodTripByDispatchConfirmationId(string dispatchTripId, string firstName, string lastName, string phoneNumber)
        {
            vtod_trip v = null;
            using (VTODEntities context = new VTODEntities())
            {

                var t = context.ecar_trip.Where(x => x.DispatchTripId == dispatchTripId && ((x.FirstName == firstName && x.LastName == lastName) || x.PhoneNumber == phoneNumber)).Select(x => x).FirstOrDefault();
                if (t != null)
                {
                    v = context.vtod_trip.Where(x => x.Id == t.Id).FirstOrDefault();
                }
                else
                {
                    throw new Exception(string.Format("Unable to find the trip by dispatch confirmation ID={0}, firstName={1}, lastName={2}", dispatchTripId, firstName, lastName));
                }

            }
            return v;
        }
        

        public string ConvertTripStatusForMTData(MTData.BookingWebService.BookingStatus source)
        {
            string target = source.ToString();
            using (VTODEntities context = new VTODEntities())
            {
                var t = context.ecar_status_mtdatawrapper.Where(x => x.Source == source.ToString()).Select(x => x.Target).FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(t))
                {
                    target = t;
                    logger.InfoFormat("Convert status from {0} to {1}", source, target);
                }
            }
            return target;
        }


        public bool ValidateCancelRequest(TokenRS tokenVTOD, OTA_GroundCancelRQ request, ECarCancelParameter eccp)
        {
            bool result = true;
            Stopwatch sw = new Stopwatch();
            sw.Start();            

            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
            #endregion
            
            #region Validate Unique ID
            string uid = request.Reservation.UniqueID.FirstOrDefault().ID;
            if (request.Reservation.UniqueID.Any())
            {
                if (!string.IsNullOrWhiteSpace(uid))
                {
                    logger.Info("Request is valid.");
                }
                else
                {
                    result = false;
                    logger.Info("request.Reservation.UniqueID.Frist().ID is empty.");
                }
            }
            else
            {
                result = false;
                logger.Info("Reservation.UniqueID is empty.");
            }
            #endregion

            #region Validate Trip
            if (result)
            {
                vtod_trip vtodTrip = null;
                if (request.Reservation.UniqueID.Where(x => x.Type == "Confirmation").Any())
                {
                    var confirmationId = request.Reservation.UniqueID.Where(x => x.Type == "Confirmation").Select(x=> x.ID).SingleOrDefault();
                    vtodTrip = GetECarTrip(Convert.ToInt64(confirmationId));
                }
                else
                {
                    var dispatchConfirmationId = request.Reservation.UniqueID.Where(x => x.Type == "DispatchConfirmation").Select(x => x.ID).SingleOrDefault();
                    string firstname = string.Empty;
                    string lastname = string.Empty;
                    string phonenumber = string.Empty;
                    if (request.Reservation != null && request.Reservation.Verification != null && request.Reservation.Verification.PersonName != null)
                    {
                        firstname = request.Reservation.Verification.PersonName.GivenName;
                        lastname = request.Reservation.Verification.PersonName.Surname;
                    }
                    if (request.Reservation != null && request.Reservation.Verification != null && request.Reservation.Verification.TelephoneInfo != null)
                    {
                        phonenumber = request.Reservation.Verification.TelephoneInfo.PhoneNumber;
                    }
                    vtodTrip = GetVtodTripByDispatchConfirmationId(dispatchConfirmationId, firstname, lastname, phonenumber.CleanPhone());
                }

                if (vtodTrip != null)
                {   
                    logger.InfoFormat("Trip found. vtod tripID={0}", uid);
                }                
            }
            else
            {
                result = false;
                logger.Warn("Cannot find trip by invalid referenceId");
            }
            #endregion

            #region Validate Fleet
            if (result)
            {
                ecar_fleet fleet = null;
                
                if (GetFleet(eccp.Trip.ecar_trip.FleetId, out fleet))
                //if (GetFleet(eccp.Fleet.Id, out fleet))
                {
                    eccp.Fleet = fleet;
                    logger.InfoFormat("Fleet found. ID={0}", fleet.Id);

                    if (fleet != null)
                    {
                        eccp.serviceAPIID = GetECarFleetServiceAPIPreference(fleet.Id, ECarServiceAPIPreferenceConst.Cancel).ServiceAPIId;
                        #region Fetching Endpoint Names
                        List<string> endpointNames = MtdataEndpointConfiguration(eccp.serviceAPIID, eccp.Fleet.Id);
                        if (endpointNames != null && endpointNames.Any())
                        {
                            eccp.AuthenticationServiceEndpointName = endpointNames[0];
                            eccp.BookingWebServiceEndpointName = endpointNames[1];
                            eccp.OsiWebServiceEndpointName = endpointNames[2];
                            eccp.AddressWebServiceEndpointName = endpointNames[3];
                        }
                        #endregion
                    }
                }
                else
                {
                    result = false;
                    logger.Warn("Unable to find fleet by this fleetID");
                }
            }
            else
            {
                result = false;
                logger.Warn("Unable to find fleet by this fleetID");
            }
            #endregion

            #region Validate User
            if (result)
            {
                ecar_fleet_user user = null;
                if (GetECarFleetUser(eccp.Trip.ecar_trip.FleetId, tokenVTOD.Username, out user))
                {
                    eccp.User = user;
                    logger.Info("This user has sufficient privilege.");
                }
                else
                {
                    result = false;
                    logger.WarnFormat("Insufficient privilege. User={0}, FleetId={1}", tokenVTOD.Username, eccp.Fleet.Id);
                }
            }
            else
            {
                result = false;
                logger.Warn("Cannot validate this user by invalid fleet ");
            }
            #endregion

            #region Validate customer
            if (!result)
            {
                result = false;
                logger.Warn("Cannot find customer by Invalid trip");
            }            
            #endregion

            #region  Validate trip status
            //need to be added (michael)
            //if (result && (IsAbleToCancelThisTrip(tcp.Trip.FinalStatus)))
            //{
            //	logger.InfoFormat("This trip can be canceled. status={0}", tcp.Trip.FinalStatus);
            //}
            //else {
            //	logger.WarnFormat("This trip can not be canceled. status={0}", tcp.Trip.FinalStatus);
            //	result = false;
            //}
            #endregion

            sw.Stop();
            logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

            #region Create eCar log
            if (result && eccp.Trip != null)
            {
                if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
                {
                    WriteECarLog(eccp.Trip.Id, eccp.serviceAPIID, ECarServiceMethodType.Cancel, sw.ElapsedMilliseconds, string.Empty, string.Empty, null, null);
                }
            }
            else
            {
                result = false;
                logger.Warn("Cannot create or get this eCar log by invalid trip");
            }
            #endregion

            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "ValidateCancelRequest");
            #endregion

            return result;
        }
      
        public bool IsVTODEcarTrip(string DispatchTripId, long serviceAPIId)
        {
            bool result = true;
            Stopwatch sw = new Stopwatch();
            sw.Start();

            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
            #endregion

            using (VTODEntities context = new VTODEntities())
            {
                if (context.ecar_trip.Where(x => x.DispatchTripId == DispatchTripId && x.ServiceAPIId == serviceAPIId).Any())
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }

            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "IsVTODEcarTrip");
            #endregion

            return result;
        }
        
        public decimal? DetermineOverwriteFare(ECarStatusParameter ecsp, decimal? longtitude, decimal? latitude, decimal? dispatchFare)
        {
            decimal? finalFareWithoutGratuity = dispatchFare;
            Stopwatch sw = new Stopwatch();
            sw.Start();

            try
            {
                if (!longtitude.HasValue || !latitude.HasValue)
                {
                    //get longtitude and latitude from last trip status
                    using (VTODEntities context = new VTODEntities())
                    {
                        if (context.ecar_trip_status.Where(x => x.TripID == ecsp.Trip.Id && x.VehicleLongitude != null && x.VehicleLatitude != null).Select(x => x).OrderByDescending(x => x.Id).Any())
                        {
                            ecar_trip_status tts = context.ecar_trip_status.Where(x => x.TripID == ecsp.Trip.Id && x.VehicleLongitude != null && x.VehicleLatitude != null).Select(x => x).OrderByDescending(x => x.Id).FirstOrDefault();
                            longtitude = tts.VehicleLongitude;
                            latitude = tts.VehicleLatitude;
                        }
                    }

                    //if there is no trip status from DB, just use dispatch fare
                    if (!longtitude.HasValue || !latitude.HasValue)
                    {
                        finalFareWithoutGratuity = dispatchFare;
                    }

                }

                //Get Flat Rate
                if (longtitude.HasValue && latitude.HasValue && ecsp.Trip.ecar_trip.PickupLongitude.HasValue && ecsp.Trip.ecar_trip.PickupLatitude.HasValue && ecsp.Fleet.OverWriteFixedPrice.HasValue)
                {
                    long pickupfleetID = ecsp.Trip.ecar_trip.FleetId;
                    Map.DTO.Address pickupAddress = new Map.DTO.Address { Geolocation = new Map.DTO.Geolocation { Latitude = ecsp.Trip.ecar_trip.PickupLatitude.ToDecimal(), Longitude = ecsp.Trip.ecar_trip.PickupLongitude.ToDecimal() } };
                    ecar_fleet_zone pickupZone = GetFleetZone(pickupfleetID, pickupAddress);

                    Map.DTO.Address dropOffAddress = new Map.DTO.Address { Geolocation = new Map.DTO.Geolocation { Latitude = latitude.Value, Longitude = longtitude.Value } };
                    ecar_fleet_zone dropOffZone = GetFleetZone(pickupfleetID, dropOffAddress);

                    if (pickupZone != null && dropOffZone != null)
                    {
                        using (VTODEntities context = new VTODEntities())
                        {
                            ecar_fleet_zone_flatrate tfzf = context.ecar_fleet_zone_flatrate.Where(x => x.PickupZoneId == pickupZone.Id && x.DropoffZoneId == dropOffZone.Id).Select(x => x).OrderBy(x => x.Amount).FirstOrDefault();
                            if (tfzf != null)
                            {
                                finalFareWithoutGratuity = tfzf.Amount.ToDecimal();
                                UpdateFlatRate(ecsp.Trip.Id, true);
                            }
                            else
                            {
                                //unable to find flatrate
                                finalFareWithoutGratuity = dispatchFare;
                            }
                        }
                    }
                    else
                    {
                        //unable to find any dropoff zone in the flatratezone
                        finalFareWithoutGratuity = dispatchFare;
                    }

                }
                else
                {
                    //just use dispatchFare
                    finalFareWithoutGratuity = dispatchFare;
                }

            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Message:{0}", ex.Message);
                logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
                logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
                throw new ECarException(ex.Message);
            }

            sw.Stop();
            logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "IsVTODEcarTrip");
            #endregion

            return finalFareWithoutGratuity;
        }


        public bool UpdateFlatRate(Int64 tripId, bool IsFlatRate)
        {
            vtod_trip trip = null;
            using (VTODEntities context = new VTODEntities())
            {
                trip = context.vtod_trip.Where(x => x.Id == tripId).FirstOrDefault();
                trip.IsFlatRate = IsFlatRate;
                context.SaveChanges();
            }
            return false;
        }

        public vtod_driver_info GetDriverInfoByVehicleNumber(string userName, string status, string vehicleNumber, string driverNumber, vtod_trip trip)
        {
			vtod_driver_info result = null;			

			try
			{
				if (!string.IsNullOrWhiteSpace(userName))
				{	                    
                    if (IsAllowedPullingDriverInfo(userName) && trip != null)
					{
						if (!trip.DriverInfoID.HasValue || trip.DriverInfoID.Value == 0)
						{
							if (HasDriver(status))
							{
                                result = GetDriverInfoFromSDSAndUpdate(driverNumber, vehicleNumber, trip);
                            }
						}                        
					}
                    //It is IVR trip if trip is null 
                    //In IVR case, we don't need to save driver info
                    else
                    {
                        if (trip.DriverInfoID.HasValue && trip.DriverInfoID.Value > 0)
                        {
                            using (var db = new DataAccess.VTOD.VTODEntities())
                            {
                                result = db.vtod_driver_info.Where(s => s.Id == trip.DriverInfoID.Value).FirstOrDefault();
                            }
                        }
                    }
                }
			}
			catch (System.Data.Entity.Validation.DbEntityValidationException ex)
			{
				foreach (var eve in ex.EntityValidationErrors)
				{
					logger.ErrorFormat("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
						eve.Entry.Entity.GetType().Name, eve.Entry.State);
					foreach (var ve in eve.ValidationErrors)
					{
						logger.ErrorFormat("- Property: \"{0}\", Error: \"{1}\"",
							ve.PropertyName, ve.ErrorMessage);
					}
				}
				throw;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("GetDriverInfo: {0}", ex.ToString() + ex.StackTrace);
			}

			return result;
		}

        public List<string> GetSynonyms(string word)
        {
            using (var db = new VTODEntities())
            {
                return db.vtod_synonym.Where(m => m.Key == word).Select(m => m.Value.ToLower().Trim()).ToList();
            }
        }
        

        #region Private

        private vtod_driver_info GetDriverInfoFromSDSAndUpdate(string driverNumber, string vehicleNumber, vtod_trip trip, bool isCalledFromGetDriverInfoByDriverId = false)
        {
            vtod_driver_info result = null;

            #region Get Driver Info from SDS
            //use test ds if we're in test mode
            var ECarDrvController = new UDI.SDS.ECarDrvController(TrackTime);
            DataSet drvResult;
            if (isCalledFromGetDriverInfoByDriverId)
            {
                drvResult = !ConfigHelper.IsTest() ? ECarDrvController.GetECarDriverInfo(trip.taxi_trip.FleetId.ToString(), driverNumber, "") : ECarDrvController.GetECarDriverInfo("16", "", "726");
            }
            else
            {
                drvResult = ECarDrvController.GetECarDriverInfo(trip.taxi_trip.FleetId.ToString(), string.IsNullOrWhiteSpace(driverNumber) ? "" : driverNumber, vehicleNumber);
            }
            #endregion


            if (drvResult != null && drvResult.Tables.Count > 0)
            {
                var drvDataTable = drvResult.Tables[0];
                if (drvDataTable != null)
                {
                    if (drvDataTable.Rows != null && drvDataTable.Rows.Count > 0)
                    {
                        var newDriver = new vtod_driver_info();
                        byte[] driverImage = null;
                        foreach (DataRow row in drvDataTable.Rows)
                        {
                            newDriver.DriverID = row["DriverID"].ToString();
                            newDriver.VehicleID = row["VehicleID"].ToString();
                            newDriver.FirstName = row["FirstName"].ToString();
                            newDriver.LastName = row["LastName"].ToString();
                            newDriver.CellPhone = row["CellPhone"].ToString();
                            newDriver.HomePhone = row["HomePhone"].ToString();
                            newDriver.AlternatePhone = row["AlternatePhone"].ToString();
                            newDriver.QualificationDate = row["QualificationDate"].ToDateTime();
                            newDriver.zTripQualified = row["zTripQualified"].ToBool();
                            newDriver.StartDate = row["StartDate"].ToDateTime();
                            newDriver.HrDays = row["HrDays"].ToString();
                            newDriver.LeaseDate = row["LeaseDate"].ToDateTime();
                            newDriver.FleetType = Common.DTO.Enum.FleetType.ExecuCar.ToString();
                            newDriver.AppendDate = System.DateTime.Now;

                            #region Image
                            //newDriver.Image = null;
                            if (row["Image"] != null && row["Image"] != System.DBNull.Value)
                            {
                                driverImage = (byte[])row["Image"];
                            }
                            #endregion

                        }

                        #region Log
                        logger.InfoFormat("Driver Found: ID:{0}, VehicleID:{1}", newDriver.DriverID, newDriver.VehicleID);
                        #endregion

                        using (var db = new DataAccess.VTOD.VTODEntities())
                        {
                            var driverInfo = new VTOD.DriverInfo(logger);

                            result = driverInfo.UpsertDriverDetails(newDriver, driverImage);

                            db.SP_vtod_Update_TripDriverInfoID(trip.Id, result.Id);
                        }

                    }
                }
            }

            return result;
        }
        
        private string GetDispatchCode(List<RateQualifier> rqs)
        {
            string dispatchCode = string.Empty;
            if (rqs.Any() && rqs.Where(x => x.RateQualifierValue == "ExecuCar").Any())
            {
                dispatchCode = rqs.Where(x => x.RateQualifierValue == "ExecuCar").Select(x => x.SpecialInputs).FirstOrDefault().Where(x => x.Name == "DispatchCode").Select(x => x.Value).FirstOrDefault();
            }
            return dispatchCode;
        }

        private long GetServiceAPIId(long fleetId, string action)
        {
            long serviceAPIId = -1;

            try
            {
                serviceAPIId = GetECarFleetServiceAPIPreference(fleetId, action).ServiceAPIId;
            }
            catch
            {
                var vtodException = VtodException.CreateException(ExceptionType.ECar, 1009);
                logger.Warn(vtodException.ExceptionMessage.Message);
                throw vtodException;
            }
            return serviceAPIId;
        }


		public void WriteECarLog(long? TripId, long Type, int Action, long? validationDuration, string request, string response, DateTime? sendRequest, DateTime? getResponse)
		{
			#region This is eliminated for having better performance. Everything should be logged in Log4Net
			//using (VTODEntities context = new VTODEntities())
			//{
			//	if (validationDuration.HasValue)
			//	{
			//		//Save validation duration
			//		context.SP_ECar_InsertLogByValidation(TripId, Type, Action, validationDuration.Value, DateTime.Now);
			//	}
			//	else
			//	{
			//		//Save for request and response
			//		context.SP_ECar_InsertLogByDateTime(TripId, Type, Action, request, response, sendRequest, getResponse, DateTime.Now);
			//	}
			//} 
			#endregion
		}

		public vtod_trip CreateNewTrip(ECarBookingParameter ecbp, string userName)
		{
			Stopwatch sw = new Stopwatch();
			sw.Start();
			Stopwatch swEachPart = new Stopwatch();
			//ecar_trip ec = new ecar_trip();
			vtod_trip trip = new vtod_trip();
			trip.ecar_trip = new ecar_trip();
			GroundReservation reservation = ecbp.request.GroundReservations.FirstOrDefault();
            TimeHelper utcTimeHelper = new TimeHelper();
			using (VTODEntities context = new VTODEntities())
			{

				swEachPart.Restart();
				#region Consumer confirmation Id
				if (!string.IsNullOrWhiteSpace(ecbp.ConsumerConfirmationID))
				{
					trip.ecar_trip.ConsumerConfirmationId = ecbp.ConsumerConfirmationID;
				}
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set consumer confirmation ID:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region fleetId and DNI
				trip.ecar_trip.FleetId = ecbp.Fleet.Id;
				//t.TripType = "Production";
				/*
				if (ecbp.Fleet..UDI33DNI != null)
				{
					t.APIInformation = ecbp.Fleet.UDI33DNI.ToString();
				}
				else if ((ecbp.Fleet.CCSiFleetId != null) && (ecbp.Fleet.CCSiSource != null))
				{
					t.APIInformation = string.Format("{0}_{1}", ecbp.Fleet.CCSiSource, ecbp.Fleet.CCSiFleetId);
				}
				*/

				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set fletId and DNI Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Counsumer Source, Device, Ref
				trip.ConsumerDevice = ecbp.Device;
				trip.ConsumerSource = ecbp.Source;
				trip.ConsumerRef = ecbp.Ref;
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set Consumer Device, Source, Ref:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Expire
				double timeToExpire = 0;
				if (!double.TryParse(ConfigurationManager.AppSettings["ECar_TimeToExpire"], out timeToExpire))
				{
					timeToExpire = 10;
				}
				trip.ecar_trip.Expires = DateTime.Parse(reservation.Service.Location.Pickup.DateTime).AddMinutes(timeToExpire);
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set expire time Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region PickupTime and DropOffTime
				trip.PickupDateTime = ecbp.PickupDateTime;
                //trip.PickupDateTimeUTC = ecbp.PickupDateTime.ToUtc(ecbp.Fleet.ServerUTCOffset);

				//DateTime dropOffDateTime;
				//if (tbp.DropOffDateTime.HasValue)
				//{
				//	t.DropOffDateTime = tbp.DropOffDateTime.Value;
				//	t.PickupDateTimeUTC = tbp.DropOffDateTime.Value.ToUtc(tbp.Fleet.ServerUTCOffset);
				//}
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set pickuptime and dropioffTime Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				//swEachPart.Restart();
				//#region Get zone ID from ecar_fleet_zone, Get Consumer AccountNumber
				////Pickup
				//if (ecbp.PickupAddressType == AddressType.Address)
				//{
				//    var zone = GetFleetZone(ecbp.Fleet.Id, ecbp.PickupAddress);
				//    if (zone != null)
				//    {
				//        t.PickupFlatRateZone = zone.Name;
				//        logger.InfoFormat("Pickup flat rate zone={0}", t.PickupFlatRateZone);
				//    }
				//}
				//else if (ecbp.PickupAddressType == AddressType.Airport)
				//{
				//    var zone = GetFleetZoneByAirport(ecbp.Fleet.Id, ecbp.PickupAirport);
				//    if (zone != null)
				//    {
				//        t.PickupFlatRateZone = zone.Name;

				//        try
				//        {
				//            t.PickupLatitude = System.Convert.ToDouble(zone.CenterLatitude);
				//            t.PickupLongitude = System.Convert.ToDouble(zone.CenterLongitude);
				//        }
				//        catch { }

				//        logger.InfoFormat("Pickup flat rate zone={0}", t.PickupFlatRateZone);
				//    }
				//}

				////Dropoff
				//if (ecbp.DropOffAddressType == AddressType.Address)
				//{
				//    var zone = GetFleetZone(ecbp.Fleet.Id, ecbp.DropOffAddress);
				//    if (zone != null)
				//    {
				//        t.DropOffFlatRateZone = zone.Name;
				//        logger.InfoFormat("Dropoff flat rate zone={0}", t.DropOffFlatRateZone);
				//    }
				//}
				//else if (ecbp.DropOffAddressType == AddressType.Airport)
				//{
				//    var zone = GetFleetZoneByAirport(ecbp.Fleet.Id, ecbp.DropoffAirport);
				//    if (zone != null)
				//    {
				//        t.DropOffFlatRateZone = zone.Name;

				//        try
				//        {
				//            t.DropOffLatitude = System.Convert.ToDouble(zone.CenterLatitude);
				//            t.DropOffLongitude = System.Convert.ToDouble(zone.CenterLongitude);
				//        }
				//        catch { }

				//        logger.InfoFormat("Dropoff flat rate zone={0}", t.DropOffFlatRateZone);
				//    }
				//}

				//#endregion
				//swEachPart.Stop();
				//logger.DebugFormat("Set zone Id for ecar_fleet_zone Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Get Account number
				//ToDo: it should not be hardcoded. It should read it from DB based on FleetID
				using (var db = new DataAccess.VTOD.VTODEntities())
				{
					var accountNumber = db.SP_ECar_GetFleet_AccountNumber(trip.ecar_trip.FleetId, ecbp.tokenVTOD.Username).ToList().FirstOrDefault();
					if (!string.IsNullOrWhiteSpace(accountNumber))
					{
						trip.ecar_trip.AccountNumber = accountNumber;
					}
				}
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set expire time Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region pickup address
				if (ecbp.PickupAddressType == AddressType.Address)
				{
					trip.ecar_trip.PickupAddressType = ecbp.PickupAddressType;
					double pickupLat = 0;
					double pickupLongt = 0;
					if (Double.TryParse(ecbp.PickupAddress.Geolocation.Latitude.ToString(), out pickupLat))
					{
						trip.ecar_trip.PickupLatitude = pickupLat;
					}

					if (Double.TryParse(ecbp.PickupAddress.Geolocation.Longitude.ToString(), out pickupLongt))
					{
						trip.ecar_trip.PickupLongitude = pickupLongt;
					}

					trip.ecar_trip.PickupStreetNo = ecbp.PickupAddress.StreetNo;
					trip.ecar_trip.PickupStreetName = ecbp.PickupAddress.Street;
					trip.ecar_trip.PickupStreetType = ExtractStreetType(ecbp.PickupAddress.Street);
					trip.ecar_trip.PickupAptNo = ecbp.PickupAddress.ApartmentNo;
					trip.ecar_trip.PickupCity = ecbp.PickupAddress.City;
					trip.ecar_trip.PickupStateCode = ecbp.PickupAddress.State;
					trip.ecar_trip.PickupZipCode = ecbp.PickupAddress.ZipCode;
					trip.ecar_trip.PickupCountryCode = ecbp.PickupAddress.Country;
					trip.ecar_trip.PickupFullAddress = ecbp.PickupAddress.FullAddress;
				}
				else if (ecbp.PickupAddressType == AddressType.Airport)
				{
					trip.ecar_trip.PickupAddressType = ecbp.PickupAddressType;
					trip.ecar_trip.PickupAirport = ecbp.PickupAirport;
                    trip.ecar_trip.PickupLatitude = ecbp.LatitudeForPickupAirport;
                    trip.ecar_trip.PickupLongitude = ecbp.LongitudeForPickupAirport;
                    trip.ecar_trip.PickupFullAddress= ecbp.PickupAirport;
                }
				else
				{
					logger.Warn("No pickup location");
					trip.ecar_trip.PickupAddressType = AddressType.Empty;
				}
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set pickup address Duration:{0} ms.", swEachPart.ElapsedMilliseconds);


                swEachPart.Restart();
                #region PickUp UTC Time
                try
                {
                    if (trip.ecar_trip.PickupLatitude != null && trip.ecar_trip.PickupLongitude != null)
                    {
                        trip.PickupDateTimeUTC = utcTimeHelper.GetUTCFromLocalTime(trip.ecar_trip.PickupLatitude, trip.ecar_trip.PickupLongitude,ecbp.PickupDateTime);
                    }
                    else
                    {
                        if (ecbp.PickupAddressType == AddressType.Airport)
                        {
                            double? longitude;
                            double? latitude;
                            if (GetLongitudeAndLatitudeForAirport(trip.ecar_trip.PickupAirport, out longitude, out latitude))
                            {
                                trip.PickupDateTimeUTC = utcTimeHelper.GetUTCFromLocalTime(trip.ecar_trip.PickupLatitude, trip.ecar_trip.PickupLongitude, ecbp.PickupDateTime);
                            }
                         
                        }
                        else
                        {
                            trip.PickupDateTimeUTC = ecbp.PickupDateTime.ToUtc(ecbp.Fleet.ServerUTCOffset);
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.InfoFormat("Exception in PickUpTime UTC:{0}", ex.Message);
                    trip.PickupDateTimeUTC = ecbp.PickupDateTime.ToUtc(ecbp.Fleet.ServerUTCOffset);
                }
                #endregion
                swEachPart.Stop();
                logger.DebugFormat("Set Pickup UTC Time Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region dropoff address
				//Dropoff is optioanl : By_Pouyan
				if (reservation.Service.Location.Dropoff != null)
				{
					double dropoffLat = 0;
					double dropoffLongt = 0;
					if (ecbp.DropOffAddressType == AddressType.Address)
					{
						trip.ecar_trip.DropOffAddressType = ecbp.DropOffAddressType;
						if (ecbp.DropOffAddress.Geolocation != null)
						{
							if (Double.TryParse(ecbp.DropOffAddress.Geolocation.Latitude.ToString(), out dropoffLat))
							{
								trip.ecar_trip.DropOffLatitude = dropoffLat;
							}
							if (Double.TryParse(ecbp.DropOffAddress.Geolocation.Longitude.ToString(), out dropoffLongt))
							{
								trip.ecar_trip.DropOffLongitude = dropoffLongt;
							}
						}

						trip.ecar_trip.DropOffStreetNo = ecbp.DropOffAddress.StreetNo;
						trip.ecar_trip.DropOffStreetName = ecbp.DropOffAddress.Street;
						trip.ecar_trip.DropOffStreetType = ExtractStreetType(ecbp.DropOffAddress.Street);
						trip.ecar_trip.DropOffAptNo = ecbp.DropOffAddress.ApartmentNo;
						trip.ecar_trip.DropOffCity = ecbp.DropOffAddress.City;
						trip.ecar_trip.DropOffStateCode = ecbp.DropOffAddress.State;
						trip.ecar_trip.DropOffZipCode = ecbp.DropOffAddress.ZipCode;
						trip.ecar_trip.DropOffCountryCode = ecbp.DropOffAddress.Country;
						trip.ecar_trip.DropOffFullAddress = ecbp.DropOffAddress.FullAddress;

					}
					else if (ecbp.DropOffAddressType == AddressType.Airport)
					{
						trip.ecar_trip.DropOffAddressType = ecbp.DropOffAddressType;
						trip.ecar_trip.DropOffAirport = ecbp.DropoffAirport;
                        trip.ecar_trip.DropOffLatitude = ecbp.LatitudeForDropoffAirport;
                        trip.ecar_trip.DropOffLongitude = ecbp.LongitudeForDropoffAirport;
                        trip.ecar_trip.DropOffFullAddress = ecbp.DropoffAirport;
                    }
					else if (ecbp.DropOffAddressType == AddressType.Empty)
					{
						trip.ecar_trip.DropOffAddressType = AddressType.Empty;
						logger.Info("No drop off location");
					}
				}
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set dropoff address Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region common fields
				trip.ecar_trip.ServiceAPIId = ecbp.serviceAPIID;
				//t.CustomerId = tbp.Customer.Id;
				trip.ecar_trip.FirstName = ecbp.FirstName;
				trip.ecar_trip.LastName = ecbp.LastName;
				trip.ecar_trip.PhoneNumber = ecbp.PhoneNumber;
				//t.CabNumber = "";
				trip.ecar_trip.MinutesAway = null;
				trip.ecar_trip.NumberOfPassenger = ecbp.request.TPA_Extensions.Passengers.Sum(x => x.Quantity);
				//t.DriverNotes = null;
				trip.ecar_trip.RefernceNumber = ecbp.request.EchoToken;
				if (ecbp.PickupAddressType == AddressType.Address)
				{
					trip.ecar_trip.RemarkForPickup = reservation.Service.Location.Pickup.Remark;
				}
				if (ecbp.DropOffAddressType == AddressType.Address)
				{
					trip.ecar_trip.RemarkForDropOff = reservation.Service.Location.Dropoff.Remark;
				}
				//t.wheelchairAccessible = reservation.Service.DisabilityVehicleInd;

				trip.AppendTime = DateTime.Now;
				trip.FleetType = Common.DTO.Enum.FleetType.ExecuCar.ToString();
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set other common fields Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Payment
				if (ecbp.PaymentType == Common.DTO.Enum.PaymentType.PaymentCard)
				{
					trip.PaymentType = ecbp.PaymentType.ToString();
					trip.CreditCardID = ecbp.CreditCardID.HasValue ? ecbp.CreditCardID.Value.ToString() : null;
				}
				else
				{
					trip.PaymentType = ecbp.PaymentType.ToString();
				}
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set payment type Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region MemberID
				trip.MemberID = ecbp.MemberID.HasValue ? ecbp.MemberID.Value.ToString() : null;
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set member Id:{0} ms.", swEachPart.ElapsedMilliseconds);


				swEachPart.Restart();
                #region Gratuity
                #region Check if it's percentage or amount
                if (!string.IsNullOrWhiteSpace(ecbp.Gratuity))
                {
                    var gratuity = ecbp.Gratuity.GetPercentageAmount();
                    //GratuityType gratuityType = GratuityType.Unknown;
                    if (gratuity > 0)
                    {
                        trip.GratuityRate = gratuity;
                    }
                    else
                    {
                        gratuity = trip.Gratuity.ToDecimal();
                        trip.Gratuity = gratuity;
                    }
                }
                #endregion

                #endregion
                swEachPart.Stop();
				logger.DebugFormat("Set Gratuity Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region PickMeUpNow
				if (trip.PickupDateTimeUTC <= DateTime.UtcNow.AddMinutes(5))
				{
					trip.PickMeUpNow = true;
				}
				else
				{
					trip.PickMeUpNow = false;
				}
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set Pickup now Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Server Booking time
				trip.BookingServerDateTime = System.DateTime.Now;
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("BookingServerDateTime:{0} ms.", swEachPart.ElapsedMilliseconds);

				//swEachPart.Restart();
				//#region Fare
				//if(ecbp.)


				//#endregion
				//swEachPart.Stop();
				//logger.DebugFormat("Set User Id Duration:{0} ms.", swEachPart.ElapsedMilliseconds);


				swEachPart.Restart();
				#region User
				#region GetUser
				var user = context.my_aspnet_users.Where(s => s.name.ToLower() == userName.ToLower()).ToList().FirstOrDefault();
				trip.UserID = user.id;
                if (string.IsNullOrEmpty(trip.ConsumerDevice))
                {
                    if (user != null)
                    {
                        trip.ConsumerDevice = user.name;
                    }
                }
				#endregion

				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set User Id Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				logger.Info("before we create a ecar trip");
				swEachPart.Restart();

                #region Fleet Trip Code
                trip.FleetTripCode = ecbp.Fleet_TripCode; 
                #endregion

                swEachPart.Restart();
                #region EmailAddress
                if (!string.IsNullOrEmpty(ecbp.EmailAddress))
                {
                    trip.EmailAddress = ecbp.EmailAddress;
                }
                #endregion
                swEachPart.Stop();
                logger.DebugFormat("Set Email Address Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				#region Save
				//context.ecar_trip.Add(t);
				context.vtod_trip.Add(trip);
				context.SaveChanges();
				#endregion

				swEachPart.Restart();
				#region Add relation between SDS Trip and Ecar trip
				MapSDSTripToECarTrip(ecbp.ConsumerConfirmationID, trip.Id);
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Map sds trip to ecar trip. Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Stop();
				logger.DebugFormat("Save this trip Duration:{0} ms.", swEachPart.ElapsedMilliseconds);
				logger.Info("after we create a ecar trip");
			}

			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return trip;
		}

		protected void MapSDSTripToECarTrip(string ConsumerConfirmationID, long ecarTripId)
		{
			long rezId = -1;
			if (!string.IsNullOrWhiteSpace(ConsumerConfirmationID))
			{
				if (long.TryParse(ConsumerConfirmationID, out rezId))
				{
					using (VTODEntities context = new VTODEntities())
					{
						vtod_trip s = context.vtod_trip.Include("sds_trip").Where(x => x.sds_trip.RezID == rezId).Select(x => x).FirstOrDefault();
						if (s != null)
						{
							context.vtod_map_sds_ecar_trip.Add(new vtod_map_sds_ecar_trip { SDSTripID = s.Id, ECarTripId = ecarTripId, AppendTime = DateTime.Now });
							context.SaveChanges();
						}
						else
						{
							logger.WarnFormat("Unable to find rez ID for this ecar trip. confumerconfirmationId={0}", rezId);
						}
					}
				}
				else
				{
					logger.Info("Invalid format in rezId. Unable to find rez ID for this ecar trip.");
				}
			}
			else
			{
				logger.Info("Unable to find rez ID for this ecar trip.");
			}
		}

		public Person ExtractDriverName(string driverName)
		{
			Person p = null;
			if (!string.IsNullOrWhiteSpace(driverName))
			{
				p = new Person();
				p.PersonName = new PersonName();
				string[] sep = { " " };
				string[] results = driverName.Split(sep, StringSplitOptions.RemoveEmptyEntries);
				if (results.Length == 1)
				{
					p.PersonName.GivenName = driverName;
				}
				else if (results.Length >= 2)
				{
					p.PersonName.GivenName = string.Join(" ", results.Take(results.Length - 1).ToArray());
					p.PersonName.Surname = results[results.Length - 1];
				}
			}

			return p;
		}


		public string ConvertTripStatusForGreenTomato(string source)
		{
			string target = source;
			using (VTODEntities context = new VTODEntities())
			{
				var t = context.ecar_status_greentomatowrapper.Where(x => x.Source == source).Select(x => x.Target).FirstOrDefault();
				if (!string.IsNullOrWhiteSpace(t))
				{
					target = t;
					logger.InfoFormat("Convert status from {0} to {1}", source, target);
				}
			}
			return target;
		}
        private void GetPickMeupOption(ecar_fleet f, out bool PickMeUpNow, out bool PickMeUpLater)
        {
            PickMeUpNow = true;
            PickMeUpLater = true;

            #region Get Pick me up value
            if (f.PickMeUpNowOption == PickMeUpNowOptionConst.Anytime)
            {
                PickMeUpNow = true;
                PickMeUpLater = true;
            }
            else if (f.PickMeUpNowOption == PickMeUpNowOptionConst.DontPickMeUp)
            {
                PickMeUpNow = false;
                PickMeUpLater = false;
            }
            else if (f.PickMeUpNowOption == PickMeUpNowOptionConst.OnlyNow)
            {
                PickMeUpNow = true;
                PickMeUpLater = false;
            }
            else if (f.PickMeUpNowOption == PickMeUpNowOptionConst.OnlyFuture)
            {
                PickMeUpNow = false;
                PickMeUpLater = true;
            }
            #endregion
        }

        public void UpdateECarTrip(long Id, string cabNumber, int? minutesAway, string dispatchTripId, string driverName)
		{
			using (VTODEntities context = new VTODEntities())
			{
				var trip = GetECarTrip(Id);
				//var trip = (ecar_trip)context.vtod_trip.Where(x => x.Id == Id).Select(x => x).FirstOrDefault();
				if (trip != null)
				{
					//if (!string.IsNullOrWhiteSpace(cabNumber))
					//{
					//    trip.CabNumber = cabNumber;
					//}

					if (minutesAway.HasValue)
					{
						trip.ecar_trip.MinutesAway = minutesAway.Value;
					}

					if (!string.IsNullOrWhiteSpace(dispatchTripId))
					{
						trip.ecar_trip.DispatchTripId = dispatchTripId;
					}

					if (!string.IsNullOrWhiteSpace(driverName))
					{
						Person p = ExtractDriverName(driverName);
						trip.ecar_trip.DriverGivenName = p.PersonName.GivenName;
						trip.ecar_trip.DriverSurName = p.PersonName.Surname;
					}

					trip.AppendTime = DateTime.Now;

					context.SaveChanges();
				}
			}
		}        

		public vtod_trip GetECarTrip(long Id)
		{
			vtod_trip vtodTrip = null;
			using (VTODEntities context = new VTODEntities())
			{
				vtodTrip = context.vtod_trip.Include("ecar_trip.ecar_fleet").Include("ecar_trip_status").Where(x => x.Id == Id).Select(x => x).FirstOrDefault();              
            }
			return vtodTrip;
        }

        public string ConvertTripStatusMessageForFrontEndDevice(int userID, string statusMessage)
		{
			string result = "";
			using (VTODEntities context = new VTODEntities())
			{

				result = context.ecar_status_frontendwrapper.Where(x => x.UserId == userID && x.VTODECarStatus == statusMessage).Select(x => x.FrontEndStatus).FirstOrDefault();

				logger.InfoFormat("Convet trip status for frontend app. userID={0}, vtod_status={1}, frontendStatus={2}", userID, statusMessage, result);
			}
			return result;
		}
        

		protected bool LookupAddress(double longtitude, double latutude, out Map.DTO.Address returnAddress)
		{
			Stopwatch sw = new Stopwatch();
			sw.Start();
			bool result = false;
			returnAddress = null;
			string errorMessage = "";
			MapService ms = new MapService();
			Map.DTO.Geolocation location = new Map.DTO.Geolocation { Longitude = decimal.Parse(longtitude.ToString()), Latitude = decimal.Parse(latutude.ToString()) };
			var addresses = ms.GetAddressesFromGeoLocation(location, out errorMessage);
			if (string.IsNullOrWhiteSpace(errorMessage))
			{
				if (addresses.Count > 1)
				{
					logger.WarnFormat("Multiple addresses found. Source longtitude={0}, latitude={1}", longtitude, latutude);
					logger.WarnFormat("Pickup the first one address= {0}", addresses.FirstOrDefault().FullAddress);
				}
				else
				{
					logger.InfoFormat("Pickup the first one address= {0}", addresses.FirstOrDefault().FullAddress);
				}
				returnAddress = addresses.FirstOrDefault();

				//change Zip Code
				if (returnAddress.Country == "US")
				{
					returnAddress.ZipCode = returnAddress.ZipCode.Substring(0, 5);
				}
				if (returnAddress.Country == "CA")
				{
					returnAddress.ZipCode = returnAddress.ZipCode.Substring(0, 6);
				}
				result = true;
			}
			else
			{
				logger.ErrorFormat("Unable to look up address. Source longtitude={0}, latitude={1}", longtitude, latutude);
				result = false;
			}

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return result;
		}

		private bool LookupAddress(Pickup_Dropoff_Stop stop, string sourceAddress, out Map.DTO.Address returnAddress)
		{
			Stopwatch sw = new Stopwatch();
			sw.Start();
			bool result = false;
			returnAddress = null;
			string errorMessage = "";
			MapService ms = new MapService();
			var addresses = ms.GetAddresses(sourceAddress, out errorMessage);
			if (string.IsNullOrWhiteSpace(errorMessage))
			{
				if (addresses.Count > 1)
				{
					logger.WarnFormat("Multiple addresses found. Source address = {0}", sourceAddress);
					logger.WarnFormat("Pickup the first one address= {0}", addresses.FirstOrDefault().FullAddress);
				}
				else
				{
					logger.InfoFormat("Pickup the first one address= {0}", addresses.FirstOrDefault().FullAddress);
				}
				returnAddress = addresses.FirstOrDefault();

				//change Zip Code
				if (!string.IsNullOrWhiteSpace(returnAddress.ZipCode))
				{
					if (returnAddress.Country == "US")
					{
						returnAddress.ZipCode = returnAddress.ZipCode.Substring(0, 5);
					}
					if (returnAddress.Country == "CA")
					{
						returnAddress.ZipCode = returnAddress.ZipCode.Substring(0, 6);
					}
				}

				returnAddress.ApartmentNo = stop.Address.BldgRoom;

				result = true;
			}
			else
			{
				logger.ErrorFormat("Unable to look up address. Source address = {0}", sourceAddress);
				result = false;
			}

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return result;
		}


		protected string GetAddressString(Address ad)
		{
			string pickupAddressStr = string.Format("{0} {1} {2} {3} {4} {5}", ad.StreetNmbr, ad.AddressLine, ad.CityName, ad.PostalCode, ad.StateProv.StateCode, ad.CountryName.Code);
			return pickupAddressStr;
		}

		protected Map.DTO.Address ConvertAddress(Pickup_Dropoff_Stop stop)
		{
			Map.DTO.Address address = new Map.DTO.Address();
			address.AddressName = "";
			address.ApartmentNo = stop.Address.BldgRoom;
			address.City = stop.Address.CityName;

			//hard code
			//1. USA--> US
			//2. empty --> US
			if ((stop.Address.CountryName.Code.Equals("USA")) || (string.IsNullOrWhiteSpace(stop.Address.CountryName.Code)))
			{
				address.Country = "US";
			}
			else
			{
				address.Country = stop.Address.CountryName.Code;
			}

			//address.County = "";
			address.FullAddress = string.Format("{0} {1} {2} {3} {4} {5}", stop.Address.StreetNmbr, stop.Address.AddressLine, stop.Address.CityName, stop.Address.PostalCode, stop.Address.StateProv.StateCode, stop.Address.CountryName.Code);
			address.FullStreet = stop.Address.AddressLine;
			decimal longtitude = 0;
			decimal latitude = 0;
			if (decimal.TryParse(stop.Address.Latitude, out latitude) && decimal.TryParse(stop.Address.Longitude, out longtitude))
			{
				address.Geolocation = new Map.DTO.Geolocation { Latitude = latitude, Longitude = longtitude };
			}
			//else {
			//	throw new Exception(Messages.ECar_Common_InvalidReservationServiceLocationInLatAndLong);
			//}
			address.SimpleAddress = null;


			address.State = ConvertUSStateCode(stop.Address.StateProv.StateCode);


			if (!string.IsNullOrWhiteSpace(stop.Address.StreetNmbr))
			{
				address.Street = stop.Address.AddressLine;
				address.StreetNo = stop.Address.StreetNmbr;
			}
			else
			{
				Regex regex = new Regex(@"^([0-9-–]+[\s]{0,}[0-9\/]{0,})[\s](.*)", RegexOptions.IgnoreCase);
				var matches = regex.Matches(stop.Address.AddressLine.Trim());
				if (matches.Count > 0 && matches[0].Groups.Count == 3)
				{
					address.StreetNo = matches[0].Groups[1].Value;
					address.Street = matches[0].Groups[2].Value;

				}
				else
				{
					address.Street = stop.Address.AddressLine.Trim();
				}
			}



			address.ZipCode = stop.Address.PostalCode;


			if (!string.IsNullOrWhiteSpace(stop.Address.LocationName))
			{
				address.AddressName = stop.Address.LocationName;
			}

			return address;
		}


		private string ConvertUSStateCode(string name)
		{
			string code;

			using (VTODEntities context = new VTODEntities())
			{
				code = context.vtod_usstatecode.Where(x => x.Name == name).Select(x => x.Code).FirstOrDefault();
				if (string.IsNullOrWhiteSpace(code))
				{
					code = name;
				}
			}
			return code;
		}
        

		protected bool GetFleet(string pickupAddressType, Map.DTO.Address address, string airport, out List<ecar_fleet> fleetList)
		{
			Stopwatch sw = new Stopwatch();
			sw.Start();
			bool result = false;
			fleetList = new List<ecar_fleet>();


			using (VTODEntities context = new VTODEntities())
			{
				if (pickupAddressType == AddressType.Address)
				{


					#region find by Coordinate
					//by Coordinate (Lat/Long)
					if (address != null && address.Geolocation != null && address.Geolocation.Latitude != 0 && address.Geolocation.Longitude != 0)
					{
						fleetList = fleetList.Concat(context.SP_ECar_GetFleetByCoordinate2(address.Geolocation.Latitude, address.Geolocation.Longitude).AsEnumerable()).ToList();

					}
					#endregion

					#region find by zip code
					//by Zip code
					if (!string.IsNullOrWhiteSpace(address.ZipCode) && !string.IsNullOrWhiteSpace(address.Country))
					{
						fleetList = fleetList.Concat(context.SP_ECar_GetFleetByZipCountry2(address.ZipCode, address.Country).Select(x => x).AsEnumerable()).ToList();

					}
					#endregion

					#region  find by city
					//by City name, State, Country
					if (!string.IsNullOrWhiteSpace(address.City) && !string.IsNullOrWhiteSpace(address.State) && !string.IsNullOrWhiteSpace(address.Country))
					{
						fleetList = fleetList.Concat(context.SP_ECar_GetFleetByCityStateCountry2(address.City, address.State, address.Country).Select(x => x).AsEnumerable()).ToList();

					}
					#endregion

				}
				else if (pickupAddressType == AddressType.Airport)
				{
					#region by Airport (for this version, landmark is just Airport)
					//by airport
					fleetList = context.SP_ECar_GetFleetByAirport2(airport).ToList();

					#endregion

				}

				if (!fleetList.Any())
				{
					result = false;
					string error = string.Format("Unable to find correct fleet with this address/airport");
					logger.Warn(error);
					//throw new ValidationException(error);
				}
				else
				{
					//distingish
					List<long> fleetIDList = fleetList.Select(x => x.Id).Distinct().ToList();
					List<ecar_fleet> tempFleetList = new List<ecar_fleet>();
					foreach (var fID in fleetIDList)
					{
						ecar_fleet f = fleetList.Where(x => x.Id == fID).Select(x => x).FirstOrDefault();
						tempFleetList.Add(f);
					}
					fleetList.Clear();
					fleetList = tempFleetList.ToList();

					result = true;
				}
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return result;
		}

		public ecar_fleet_api_reference GetECarFleetServiceAPIPreference(long fleetID, string functionName)
		{
			ecar_fleet_api_reference reference = new ecar_fleet_api_reference();
			using (VTODEntities context = new VTODEntities())
			{
                reference = context.ecar_fleet_api_reference.Where(x => x.FleetId == fleetID && x.Name == functionName).Select(x => x).FirstOrDefault();
			}
			return reference;
		}


		protected bool GetECarFleetUser(long fleetId, string userName, out ecar_fleet_user user)
		{

			Stopwatch sw = new Stopwatch();
			sw.Start();
			bool result = false;
			user = null;
			using (VTODEntities context = new VTODEntities())
			{
				user = context.SP_ECar_GetFleet_User2(fleetId, userName).Select(x => x).FirstOrDefault();
				if (user != null)
				{
					result = true;
				}
				else
				{
					result = false;
				}
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return result;
		}

		protected bool IsAbleToBookTrip(ECarBookingParameter ecbp)
		{
			bool result = false;

			using (VTODEntities context = new VTODEntities())
			{
				#region new logic
				int blockInterval = -10;
				blockInterval = ecbp.Fleet.RestrictBookingMins.HasValue ? ecbp.Fleet.RestrictBookingMins.Value : 10;

				int? duplicateTrips = null;
				if (ecbp.Fleet.RestrictBookingOption == RestrictBookingOption.On)
				{
					if (ecbp.PickupAddressType == AddressType.Address)
					{
						//by addresss
						duplicateTrips = context.SP_ECar_IsDuplicateBookingByAddress(ecbp.FirstName, ecbp.LastName, ecbp.PhoneNumber, ecbp.PickupAddress.FullAddress, ecbp.PickupAddress.Street, ecbp.PickupAddress.City, ecbp.PickupAddress.Country, ecbp.PickupDateTime, blockInterval, ecbp.MemberID.HasValue ? ecbp.MemberID.Value.ToString() : null).FirstOrDefault();
					}
					else if (ecbp.PickupAddressType == AddressType.Airport)
					{
						//by airport
						duplicateTrips = context.SP_ECar_IsDuplicateBookingByAirport(ecbp.FirstName, ecbp.LastName, ecbp.PhoneNumber, ecbp.PickupAirport, ecbp.PickupDateTime, blockInterval, ecbp.MemberID.HasValue ? ecbp.MemberID.Value.ToString() : null).FirstOrDefault();
					}
				}
				else
				{
					//bypass duplication check
					duplicateTrips = 0;
					logger.Warn("Bypass duplication check");
				}

				if (duplicateTrips.HasValue)
				{
					if (duplicateTrips.Value > 0)
					{
						result = false;
						logger.Warn("Found duplicated trips");
					}
					else
					{
						result = true;
						logger.Info("No duplicated trip");
					}
				}
				else
				{
					string error = string.Format("Unable to look up check duplicated trip");
					throw new Exception(error);
				}

				#endregion
			}
			return result;
		}

		protected string ExtractStreetType(string input)
		{
			string[] sep = { " " };
			string result = "";
			if (!string.IsNullOrWhiteSpace(input))
			{
				result = input.Split(sep, StringSplitOptions.RemoveEmptyEntries).LastOrDefault();
			}
			return result;
		}

        //GetECarTripWithReferenceID
        public vtod_trip GetVtodTripWithReferenceID(string Id, bool isConfirmationID)
        { 
            using (VTODEntities context = new VTODEntities())
            {
                var confirmationID = 0m;
                vtod_trip vtodTrip = new vtod_trip();

                if (isConfirmationID)
                {
                    confirmationID = Id.ToInt64();                    
                    vtodTrip = context.vtod_trip.Include("ecar_trip").Where(x => x.Id == confirmationID).SingleOrDefault();
                }
                else
                {
                    var ecarTrip = context.ecar_trip.Where(x => x.DispatchTripId.Equals(Id)).SingleOrDefault();
                    if (ecarTrip != null)
                    {                        
                        vtodTrip = ecarTrip.vtod_trip;
                    }
                }

                return vtodTrip;
            }         
        }


        protected vtod_trip GetECarTrip(string Id)
		{
			vtod_trip t = null;
			using (VTODEntities context = new VTODEntities())
			{
				long ecarTripId = 0;
				if (long.TryParse(Id, out ecarTripId))
				{
					t = GetECarTrip(ecarTripId);
				}
			}
			return t;
		}


        private bool GetFleet(string dispatchCode, out ecar_fleet fleet)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            bool result = false;
            fleet = null;
            using (VTODEntities context = new VTODEntities())
            {
                fleet = context.ecar_fleet.Where(x => x.Name==dispatchCode).Select(x => x).FirstOrDefault();
                if (fleet != null)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            sw.Stop();
            logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
            return result;
        }


		protected bool GetFleet(long fleetId, out ecar_fleet fleet)
		{
			Stopwatch sw = new Stopwatch();
			sw.Start();
			bool result = false;
			
			using (VTODEntities context = new VTODEntities())
			{
				fleet = context.ecar_fleet.Where(x => x.Id == fleetId).Select(x => x).FirstOrDefault();
				if (fleet != null)
				{
					result = true;
				}
				else
				{
					result = false;
				}
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return result;
		}

		private bool GetFleet(string pickupAddressType, Map.DTO.Address address, string airport, out ecar_fleet fleet)
		{
			Stopwatch sw = new Stopwatch();
			sw.Start();
			bool result = false;
			fleet = null;

			using (VTODEntities context = new VTODEntities())
			{
				if (pickupAddressType == AddressType.Address)
				{
					#region Priority #1: find by Coordinate
					//by Coordinate (Lat/Long)
					if (fleet == null)
						if (address != null && address.Geolocation != null && address.Geolocation.Latitude != 0 && address.Geolocation.Longitude != 0)
						{
							fleet = context.SP_ECar_GetFleetByCoordinate2(address.Geolocation.Latitude, address.Geolocation.Longitude).FirstOrDefault();

						}
					#endregion

					#region Priority #2: find by zip code
					//by Zip code
					if (fleet == null)
					{
						fleet = context.SP_ECar_GetFleetByZipCountry2(address.ZipCode, address.Country).Select(x => x).FirstOrDefault();

					}
					#endregion

					#region Priority #3: find by city
					//by City name, State, Country
					if (fleet == null)
					{
						fleet = context.SP_ECar_GetFleetByCityStateCountry2(address.City, address.State, address.Country).Select(x => x).FirstOrDefault();

					}
					#endregion
				}
				else if (pickupAddressType == AddressType.Airport)
				{
					#region Priority #4 by Airport (for this version, landmark is just Airport)
					//by airport
					fleet = context.SP_ECar_GetFleetByAirport2(airport).FirstOrDefault();

					#endregion

				}

				if (fleet == null)
				{
					result = false;
					string error = string.Format("Unable to find correct fleet with this address");
					throw new ValidationException(error);
				}
				else
				{
					result = true;
				}
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return result;
		}

		public string CalculateTotalPrice(string Gratuity, string Rate)
		{
			string totalPrice = "N/A";
			#region calcute gratuity
			if (!string.IsNullOrWhiteSpace(Gratuity))
			{
				var g = Gratuity;

				decimal gratuityOperand = 1;
				string gratuityOperator = g.Contains("%") ? "%" : "+";
				string gratuityOperandString = g.Replace("%", "");
				decimal p = Convert.ToDecimal(0.01);

				if (decimal.TryParse(gratuityOperandString, out gratuityOperand))
				{
					if (gratuityOperator == "%")
					{
						totalPrice = (Rate.ToDecimal() + (Rate.ToDecimal() * p * gratuityOperand)).ToString();
					}
					else
					{
						totalPrice = (Rate.ToDecimal() + gratuityOperand.ToDecimal()).ToString();
					}
				}
				else
				{
					//unable to calcute Gratuity
					logger.WarnFormat("unable to calcute Gratuity. gratuityOperandString={0}", gratuityOperandString);
				}
			}
			#endregion
			return totalPrice;

		}

		protected bool GetFleetRelation(long fleetId, string DispatchCode, string ConsumerCode, out ecar_fleet_servicearea_relation relation)
		{
			Stopwatch sw = new Stopwatch();
			sw.Start();
			bool result = false;
			relation = null;
			using (VTODEntities context = new VTODEntities())
			{
				relation = context.ecar_fleet_servicearea_relation.Where(x => x.FleetId == fleetId && x.DispatchSystemCode == DispatchCode && x.ConsumerCode == ConsumerCode).Select(x => x).FirstOrDefault();

				if (relation != null)
				{
					result = true;
				}
				else
				{
					result = false;
				}
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return result;

		}

		public my_aspnet_users GetUser(long Id)
		{
			Stopwatch sw = new Stopwatch();
			sw.Start();
			my_aspnet_users user = null;
			using (VTODEntities context = new VTODEntities())
			{
				user = context.my_aspnet_users.Where(x => x.id == Id).Select(x => x).FirstOrDefault();
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return user;
		}
        		
		#endregion


		#region Properties
		public TrackTime TrackTime { get; set; }
		#endregion
	}
}
