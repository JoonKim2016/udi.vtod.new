﻿using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.Track;
using UDI.Utility.Helper;
using UDI.Utility.Serialization;
using System.Text.RegularExpressions;
using UDI.VTOD.Common.DTO;
using UDI.Notification.Service.Class;
using UDI.VTOD.Common.Helper;
using UDI.Map;
using System.Configuration;
using UDI.VTOD.Domain.ECar;
using UDI.VTOD.Domain.ECar.Class;
using UDI.VTOD.Domain.ECar.Const;
using System.Globalization;
using UDI.VTOD.DataAccess.VTOD;
namespace UDI.VTOD.Domain.ECar.GreenTomato
{
	public class GreenTomatoECarService : IECarService
	{
		private ILog logger;
		private ECarUtility utility;
		//private TokenRS tokenVTOD;


		public GreenTomatoECarService(ECarUtility util, ILog log)
		{
			this.logger = log;
			this.utility = util;
		}

		#region Public method
		#region old way
        //public OTA_GroundResRetrieveRS Status(ECarStatusParameter esp)
        //{
        //    Stopwatch sw = new Stopwatch();
        //    sw.Start();
        //    OTA_GroundResRetrieveRS response = null;

        //    #region Track
        //    if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
        //    #endregion

        //    DateTime? requestedLocalTime = null;
        //    if (esp.Fleet != null && esp.Fleet.ServerUTCOffset != null)
        //    {
        //        requestedLocalTime = DateTime.UtcNow.FromUtcNullAble(esp.Fleet.ServerUTCOffset);
        //    }

        //    try
        //    {
        //        Dictionary<string, string> requestHeader = new Dictionary<string, string>();
        //        Dictionary<string, string> responseHeader = new Dictionary<string, string>();

        //        if (Login(out requestHeader))
        //        {
        //            string HttpStatus = "";
        //            int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["GreenTomatoAPI_Https_Timeout"]);
        //            //string url = string.Format("https://80.87.31.28/ext-api/api/bookings/getById/{0}", tsp.Trip.DispatchTripId);
        //            string url = string.Format("{0}/{1}", GetHttpURL(ConfigurationManager.AppSettings["GreenTomatoAPI_URL_Status"]), esp.Trip.ecar_trip.DispatchTripId);
        //            string statusResult = WebRequestHelper.ProcessWebRequest(url, string.Empty, "application/json", "GET", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out  responseHeader);
        //            GreenTomato_StatusRS statusResponse = statusResult.JsonDeserialize<GreenTomato_StatusRS>();


        //            if (statusResponse.success.ToLowerInvariant().Equals("true") && statusResponse.rows.Any())
        //            {
        //                response = new OTA_GroundResRetrieveRS();
        //                response.Success = new Success();
        //                response.EchoToken = esp.request.EchoToken;
        //                response.Target = esp.request.Target;
        //                response.Version = esp.request.Version;
        //                response.PrimaryLangID = esp.request.PrimaryLangID;

        //                response.TPA_Extensions = new TPA_Extensions();
        //                response.TPA_Extensions.Driver = (statusResponse.rows[0].driverRecord == null) ? null : utility.ExtractDriverName(statusResponse.rows[0].driverRecord.fullName);
        //                response.TPA_Extensions.Vehicles = new Vehicles();
        //                response.TPA_Extensions.Vehicles.Items = new List<Vehicle>();


        //                //calcute ETA
        //                int? minutesAway = null;

        //                //Vehicle
        //                var vehicle = new Vehicle();
        //                vehicle.Geolocation = new Geolocation();
        //                GreenTomato_GetVehicleInfoForStatusRS vInfo = null;
        //                if (statusResponse.rows[0].vehicleRecord != null && !string.IsNullOrWhiteSpace(statusResponse.rows[0].vehicleRecord.id))
        //                {
        //                    vehicle.ID = statusResponse.rows[0].vehicleRecord.id;
        //                    vInfo = GetVehicleInfoForStautus(vehicle.ID, requestHeader);
        //                    if (vInfo.success.ToLowerInvariant().Equals("true") && vInfo.rows.Any())
        //                    {
        //                        vehicle.Geolocation.Lat = vInfo.rows[0].currentLocationLatitude.ToDecimal();
        //                        vehicle.Geolocation.Long = vInfo.rows[0].currentLocationLongitude.ToDecimal();
        //                    }
        //                }
        //                else
        //                {
        //                    logger.Info("No vehicle information from response");
        //                }
        //                #region Caculate ETA by ourself


        //                //Replace status
        //                string originalStatus = statusResponse.rows[0].status;
        //                string status = utility.ConvertTripStatusForGreenTomato(originalStatus);



        //                #region Others
        //                //fare
        //                decimal? totalFare = null;
        //                if (status.Trim().ToLower() == ECarTripStatusType.Completed.Trim().ToLower() || status.Trim().ToLower() == ECarTripStatusType.Fare.Trim().ToLower())
        //                {
        //                    totalFare = statusResponse.rows[0].totalPrice.ToDecimal();
        //                }
        //                else
        //                {
        //                    totalFare = esp.Trip.TotalFareAmount;
        //                }

        //                //Convert status to front-end status
        //                string frontEndStatus = utility.ConvertTripStatusMessageForFrontEndDevice(esp.Fleet_User.UserId, status);
        //                //Status s = new Common.DTO.OTA.Status { Value = status, Fare = succ.TripInfo[0].FareAmount.ToDecimal() };
        //                Status s = new Common.DTO.OTA.Status { Value = frontEndStatus };
        //                if (totalFare.HasValue)
        //                {
        //                    s.Fare = totalFare.Value;
        //                }
        //                response.TPA_Extensions.Statuses = new Statuses();
        //                response.TPA_Extensions.Statuses.Status = new List<Status>();
        //                response.TPA_Extensions.Statuses.Status.Add(s);
        //                response.TPA_Extensions.Contacts = new Contacts();
        //                response.TPA_Extensions.Contacts.Items = new List<Contact>();
        //                var contact = new Contact { Type = Common.DTO.Enum.ContactType.Dispatch.ToString(), Name = esp.Fleet.Alias, Telephone = new Telephone { PhoneNumber = esp.Fleet.PhoneNumber } };
        //                response.TPA_Extensions.Contacts.Items.Add(contact);

        //                if (esp.Trip != null && esp.Trip.ecar_trip.PickupLatitude.HasValue && esp.Trip.ecar_trip.PickupLatitude.Value != 0 && esp.Trip.ecar_trip.PickupLongitude.HasValue && esp.Trip.ecar_trip.PickupLongitude.Value != 0)
        //                {
        //                    response.TPA_Extensions.PickupLocation = new Pickup_Dropoff_Stop();
        //                    response.TPA_Extensions.PickupLocation.Address = new Common.DTO.OTA.Address();
        //                    response.TPA_Extensions.PickupLocation.Address.Latitude = esp.Trip.ecar_trip.PickupLatitude.Value.ToString();
        //                    response.TPA_Extensions.PickupLocation.Address.Longitude = esp.Trip.ecar_trip.PickupLongitude.Value.ToString();
        //                }
        //                #endregion

        //                if ((vehicle.Geolocation.Lat != 0 && vehicle.Geolocation.Long != 0)
        //                    &&
        //                    (esp.Trip.ecar_trip.PickupLatitude.HasValue && esp.Trip.ecar_trip.PickupLongitude.HasValue)
        //                )
        //                {
        //                    MapService ms = new MapService();
        //                    double d = ms.GetDirectDistanceInMeter(esp.Trip.ecar_trip.PickupLatitude.Value, esp.Trip.ecar_trip.PickupLongitude.Value, Convert.ToDouble(vehicle.Geolocation.Lat), Convert.ToDouble(vehicle.Geolocation.Long));
        //                    var rc = new UDI.SDS.RoutingController(esp.tokenVTOD, null);
        //                    UDI.SDS.RoutingService.Point start = new UDI.SDS.RoutingService.Point { Latitude = esp.Trip.ecar_trip.PickupLatitude.Value, Longitude = esp.Trip.ecar_trip.PickupLongitude.Value };
        //                    UDI.SDS.RoutingService.Point end = new UDI.SDS.RoutingService.Point { Latitude = Convert.ToDouble(vehicle.Geolocation.Lat), Longitude = Convert.ToDouble(vehicle.Geolocation.Long) };
        //                    var result = rc.GetRouteMetrics(start, end);
        //                    vehicle.MinutesAway = result.TotalMinutes;

        //                }
        //                response.TPA_Extensions.Vehicles.Items.Add(vehicle);
        //                #endregion





        //                //Others

        //                //update ecar trip
        //                string driverName = (statusResponse.rows[0].driverRecord == null) ? string.Empty : statusResponse.rows[0].driverRecord.fullName;
        //                utility.UpdateECarTrip(esp.Trip.Id, statusResponse.rows[0].serviceRecord.id, minutesAway, esp.Trip.ecar_trip.DispatchTripId, driverName);


        //                DateTime? statusTime = null;
        //                DateTime? tripStartTime = null;
        //                DateTime? tripEndTime = null;

        //                if (!statusTime.HasValue || statusTime.Value <= DateTime.MinValue)
        //                {
        //                    if (esp.Fleet != null && esp.Fleet.ServerUTCOffset != null)
        //                    {
        //                        statusTime = DateTime.UtcNow.FromUtcNullAble(esp.Fleet.ServerUTCOffset);
        //                    }
        //                }

        //                if ((status.Trim().ToLower() == ECarTripStatusType.InService.Trim().ToLower() || status.Trim().ToLower() == ECarTripStatusType.MeterON.Trim().ToLower()) && (esp.Trip.TripStartTime == null))
        //                {
        //                    tripStartTime = statusTime;
        //                }
        //                else
        //                {
        //                    tripStartTime = esp.Trip.TripStartTime;
        //                }

        //                if ((status.Trim().ToLower() == ECarTripStatusType.Completed.Trim().ToLower() || status.Trim().ToLower() == ECarTripStatusType.MeterOff.Trim().ToLower()) && (esp.Trip.TripEndTime == null))
        //                {
        //                    tripEndTime = statusTime;
        //                }
        //                else
        //                {
        //                    tripEndTime = esp.Trip.TripEndTime;
        //                }

        //                if (status.ToUpperInvariant() == ECarTripStatusType.Canceled.ToUpperInvariant())
        //                {
        //                    utility.SaveTripStatus(esp.Trip.Id, ECarTripStatusType.Canceled, statusTime, null, null, null, null, null, null, string.Empty, originalStatus, null);
        //                    utility.UpdateFinalStatus(esp.Trip.Id, ECarTripStatusType.Canceled, null, null, null);
        //                }
        //                //else if (status.ToUpperInvariant() == ECarTripStatusType.FastMeter.ToUpperInvariant())
        //                //{
        //                //    if (esp.Fleet.FastMeterAction == ECarFleetFastMeterAction.ReDispatch)
        //                //    {
        //                //        utility.SaveTripStatus_ReDispatch(esp.Trip.Id, statusTime, null, null, null, null, null, null, string.Empty, originalStatus);
        //                //        utility.UpdateFinalStatus(esp.Trip.Id, ECarTripStatusType.Booked, null, null, null);
        //                //    }
        //                //    else if (esp.Fleet.FastMeterAction == ECarFleetFastMeterAction.Cancel)
        //                //    {
        //                //        utility.SaveTripStatus(esp.Trip.Id, ECarTripStatusType.FastMeter, statusTime, null, null, null, null, null, null, string.Empty, originalStatus);
        //                //        utility.UpdateFinalStatus(esp.Trip.Id, ECarTripStatusType.Canceled, null, null, null);
        //                //    }
        //                //}
        //                else
        //                {
        //                    //Save trip status
        //                    utility.SaveTripStatus(esp.Trip.Id, status, statusTime, response.TPA_Extensions.Vehicles.Items.First().ID, response.TPA_Extensions.Vehicles.Items.First().Geolocation.Lat, response.TPA_Extensions.Vehicles.Items.First().Geolocation.Long, totalFare, response.TPA_Extensions.Vehicles.Items.First().MinutesAway.ToInt32(), driverName, string.Empty, originalStatus, null);

        //                    //Save final status
        //                    utility.UpdateFinalStatus(esp.Trip.Id, status, totalFare, tripStartTime, tripEndTime);
        //                }
        //            }
        //            else
        //            {
        //                //unable to do status
        //                var vtodException = VtodException.CreateException(ExceptionType.ECar, 303);
        //                logger.WarnFormat("Unable to login green tomato or doesn't get a valid JSESSIONID");
        //                throw vtodException; // new ECarException(Messages.ECar_StatusFailure);
        //            }

        //        }
        //        else
        //        {
        //            //unable to do status
        //            var vtodException = VtodException.CreateException(ExceptionType.ECar, 303);
        //            logger.WarnFormat("Unable to login green tomato or doesn't get a valid JSESSIONID");
        //            throw vtodException; // new ECarException(Messages.ECar_StatusFailure);
        //        }




        //    }
        //    catch (Exception ex)
        //    {
        //        logger.ErrorFormat("Message:{0}", ex.Message);
        //        logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
        //        logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);

        //        throw ex;
        //    }
        //    sw.Stop();
        //    logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

        //    #region Track
        //    if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_GreenTomato, "Book");
        //    #endregion

        //    return response;
        //}

        //public OTA_GroundAvailRS GetVehicleInfo(ECarGetVehicleInfoParameter egvip)
        //{
        //    OTA_GroundAvailRS response = new OTA_GroundAvailRS();
        //    response.Success = new Success();
        //    response.TPA_Extensions = new TPA_Extensions();
        //    response.TPA_Extensions.Vehicles = new Vehicles();
        //    response.TPA_Extensions.Vehicles.Items = new List<Vehicle>();

        //    Stopwatch sw = new Stopwatch();
        //    sw.Start();

        //    #region Track
        //    if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
        //    #endregion

        //    foreach (var fleet in egvip.fleetList)
        //    {
        //        string NearestVehicleETA = "60";
        //        List<Vehicle> tempVechiles = new List<Vehicle>();


        //        //get vehicles for this fleet
        //        DateTime? requestedLocalTime = null;
        //        if (fleet != null && fleet.ServerUTCOffset != null)
        //        {
        //            requestedLocalTime = DateTime.UtcNow.FromUtcNullAble(fleet.ServerUTCOffset);
        //        }
        //        try
        //        {
        //            Dictionary<string, string> requestHeader = new Dictionary<string, string>();
        //            Dictionary<string, string> responseHeader = new Dictionary<string, string>();

        //            if (Login(out requestHeader))
        //            {
        //                GreenTomato_GetContactRQ rq = new GreenTomato_GetContactRQ();
        //                //put long , lat and radius in it
        //                string postContent = @"{""startIndex"":0, ""count"":15}";
        //                string status = "";
        //                int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["GreenTomatoAPI_Https_Timeout"]);
        //                string result = WebRequestHelper.ProcessWebRequest(GetHttpURL(ConfigurationManager.AppSettings["GreenTomatoAPI_URL_GetVehicleInfo"]), postContent, "application/json", "POST", string.Empty, string.Empty, timeout, requestHeader, out status);
        //                var vehicleInfoResult = result.JsonDeserialize<GreenTomato_GetVehicleInfoForStatusRS>();

        //                if (vehicleInfoResult != null && vehicleInfoResult.success.ToLowerInvariant().Equals("true"))
        //                {
        //                    if (vehicleInfoResult.rows.Any())
        //                    {
        //                        MapService ms = new MapService();

        //                        int shortestIndex = -1;
        //                        double? shortestDistance = null;
        //                        for (int index = 0; index < vehicleInfoResult.rows.Count(); index++)
        //                        {
        //                            double d = ms.GetDirectDistanceInMile(egvip.Latitude, egvip.Longtitude, vehicleInfoResult.rows[index].currentLocationLatitude.ToDouble(), vehicleInfoResult.rows[index].currentLocationLongitude.ToDouble());
        //                            vehicleInfoResult.rows[index].Distance = d;

        //                            if (shortestDistance.HasValue)
        //                            {
        //                                if (shortestDistance.Value > d)
        //                                {
        //                                    shortestDistance = d;
        //                                    shortestIndex = index;
        //                                }
        //                            }
        //                            else
        //                            {
        //                                shortestDistance = d;
        //                                shortestIndex = index;
        //                            }
        //                        }

        //                        if (shortestIndex != -1)
        //                        {
        //                            //get ETA
        //                            var rc = new UDI.SDS.RoutingController(egvip.tokenVTOD, null);
        //                            UDI.SDS.RoutingService.Point start = new UDI.SDS.RoutingService.Point { Latitude = egvip.Latitude, Longitude = egvip.Longtitude };
        //                            UDI.SDS.RoutingService.Point end = new UDI.SDS.RoutingService.Point { Latitude = vehicleInfoResult.rows[shortestIndex].currentLocationLatitude.ToDouble(), Longitude = vehicleInfoResult.rows[shortestIndex].currentLocationLongitude.ToDouble() };
        //                            var routeResult = rc.GetRouteMetrics(start, end);
        //                            //response.TPA_Extensions.Vehicles.NearestVehicleETA = routeResult.TotalMinutes.ToString();
        //                            NearestVehicleETA = routeResult.TotalMinutes.ToString();
        //                        }

        //                        //response.TPA_Extensions.Vehicles.NumberOfVehicles = vehicleInfoResult.rows.Count().ToString();


        //                        foreach (var item in vehicleInfoResult.rows.OrderBy(s => s.Distance))
        //                        {
        //                            var vehicle = new Vehicle();
        //                            vehicle.Geolocation = new Geolocation();
        //                            vehicle.Geolocation.Long = decimal.Parse(item.currentLocationLongitude.ToString());
        //                            vehicle.Geolocation.Lat = decimal.Parse(item.currentLocationLatitude.ToString());
        //                            vehicle.Geolocation.PriorLong = decimal.Parse(item.availableLongitude.ToString());
        //                            vehicle.Geolocation.PriorLat = decimal.Parse(item.availableLatitude.ToString());

        //                            vehicle.DriverID = item.driverId;
        //                            vehicle.DriverName = item.driverName;
        //                            vehicle.FleetID = fleet.Id.ToString();
        //                            vehicle.ID = item.id;
        //                            vehicle.VehicleStatus = item.status;
        //                            vehicle.VehicleType = item.type;
        //                            vehicle.Distance = item.Distance.ToDecimal();


        //                            #region ETA
        //                            var rc = new UDI.SDS.RoutingController(egvip.tokenVTOD, null);
        //                            UDI.SDS.RoutingService.Point start = new UDI.SDS.RoutingService.Point { Latitude = egvip.Latitude, Longitude = egvip.Longtitude };
        //                            UDI.SDS.RoutingService.Point end = new UDI.SDS.RoutingService.Point { Latitude = item.currentLocationLatitude.ToDouble(), Longitude = item.currentLocationLongitude.ToDouble() };
        //                            var routeResult = rc.GetRouteMetrics(start, end);
        //                            vehicle.MinutesAway = routeResult.TotalMinutes;
        //                            #endregion

        //                            //response.TPA_Extensions.Vehicles.Items.Add(vehicle);
        //                            tempVechiles.Add(vehicle);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        logger.Info("Found no vehicles");
        //                        //response.TPA_Extensions.Vehicles.NumberOfVehicles = "0";
        //                    }
        //                }
        //                else
        //                {
        //                    var vtodExcaption = VtodException.CreateException(ExceptionType.ECar, 4001);
        //                    logger.Warn(vtodExcaption.ExceptionMessage.Message);
        //                    throw vtodExcaption; // throw new ECarException(Messages.ECar_GetVehicleInfoFailure);
        //                }
        //            }
        //            else
        //            {
        //                //unable to do status
        //                var vtodException = VtodException.CreateException(ExceptionType.ECar, 303);
        //                logger.WarnFormat("Unable to login green tomato or doesn't get a valid JSESSIONID");
        //                throw vtodException; // new ECarException(Messages.ECar_StatusFailure);
        //            }
        //        }
        //        catch (Exception ex)
        //        {

        //            logger.ErrorFormat("Message:{0}", ex.Message);
        //            logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
        //            logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
        //            response = null;

        //        }



        //        //addd them in to the response
        //        if (Convert.ToUInt32(NearestVehicleETA) < Convert.ToUInt32(response.TPA_Extensions.Vehicles.NearestVehicleETA))
        //        {
        //            response.TPA_Extensions.Vehicles.NearestVehicleETA = NearestVehicleETA;
        //        }


        //        response.TPA_Extensions.Vehicles.Items = response.TPA_Extensions.Vehicles.Items.Concat(tempVechiles.AsEnumerable()).ToList();
        //        response.TPA_Extensions.Vehicles.NumberOfVehicles += tempVechiles.Count();

        //    }


        //    sw.Stop();
        //    logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

        //    #region Track
        //    if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_GreenTomato, "GetVehicleInfo");
        //    #endregion

        //    return response;
        //}

        //public OTA_GroundBookRS Book(ECarBookingParameter ecbp)
        //{
        //    Stopwatch sw = new Stopwatch();
        //    sw.Start();
        //    OTA_GroundBookRS response = null;

        //    #region Track
        //    if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
        //    #endregion

        //    #region old way
        //    //DateTime? requestedLocalTime = null;
        //    //if (ecbp.Fleet != null && ecbp.Fleet.ServerUTCOffset != null)
        //    //{
        //    //    requestedLocalTime = DateTime.UtcNow.FromUtcNullAble(ecbp.Fleet.ServerUTCOffset);
        //    //}

        //    //try
        //    //{

        //    //    Dictionary<string, string> requestHeader = new Dictionary<string, string>();
        //    //    Dictionary<string, string> responseHeader = new Dictionary<string, string>();

        //    //    if (Login(out requestHeader))
        //    //    {
        //    //        //Change user first name and last name into Test for Test system
        //    //        if (ConfigurationManager.AppSettings["IsTest"].ToBool())
        //    //        {
        //    //            ecbp.FirstName = "Test";
        //    //            ecbp.LastName = "Test";
        //    //        }

        //    //        //Get contact
        //    //        GreenTomato_contact contact = GetContact(ecbp.FirstName, ecbp.LastName, ecbp.User.AccountNumber, ecbp.PhoneNumber, requestHeader, ecbp.emailAddress);
        //    //        if (contact != null)
        //    //        {
        //    //            //get service Id
        //    //            //GreenTomato_ServiceListRS serviceRS = GetService(ecbp.User.AccountNumber, requestHeader);
        //    //            //if (serviceRS != null && serviceRS.success.ToLowerInvariant() == "true" && serviceRS.rows.Any() && !string.IsNullOrWhiteSpace(serviceRS.rows[0].id))
        //    //            if (!string.IsNullOrWhiteSpace(ecbp.Relation.DispatchServiceId))
        //    //            {

        //    //                //get reference type
        //    //                GreenTomato_ReferenceTypeRS refTypeRS = GetReferenceType(ecbp.User.AccountNumber, requestHeader);
        //    //                if (refTypeRS != null && refTypeRS.success.ToLowerInvariant() == "true" && refTypeRS.rows.Any())
        //    //                {

        //    //                    #region Book
        //    //                    #region Create JSON Message
        //    //                    //string postContent = BuildBookingRequest(ecbp, contact.id, serviceRS.rows[0].id, refTypeRS.rows[0]);
        //    //                    string postContent = BuildBookingRequest(ecbp, contact.id, ecbp.Relation.DispatchServiceId, refTypeRS.rows[0], requestHeader);
        //    //                    #endregion

        //    //                    #region Send HttpRequest and set response into object
        //    //                    string status = "";
        //    //                    int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["GreenTomatoAPI_Https_Timeout"]);
        //    //                    string bookingResult = WebRequestHelper.ProcessWebRequest(GetHttpURL(ConfigurationManager.AppSettings["GreenTomatoAPI_URL_Book"]), postContent, "application/json", "POST", string.Empty, string.Empty, timeout, requestHeader, out status, out  responseHeader);
        //    //                    GreenTomato_BookingRS bookingResponse = bookingResult.JsonDeserialize<GreenTomato_BookingRS>();

        //    //                    if (bookingResponse.success.ToLowerInvariant().Equals("false"))
        //    //                    {
        //    //                        //failure
        //    //                        string errorMessage = "";
        //    //                        string msg = "";
        //    //                        if (bookingResponse.errors.Any())
        //    //                        {
        //    //                            errorMessage = bookingResponse.errors[0].message;
        //    //                        }
        //    //                        else
        //    //                        {
        //    //                            errorMessage = "No error messages from API";
        //    //                        }
        //    //                        msg = string.Format("Unable to book this trip. tripID={0}, errormessage={1}", ecbp.Trip.Id, bookingResponse.errors[0].message);
        //    //                        logger.Warn(msg);
        //    //                        throw new ECarException(msg);
        //    //                    }
        //    //                    else
        //    //                    {
        //    //                        //sucess
        //    //                        logger.Info("Booking sucessful");

        //    //                        ecbp.Trip = utility.UpdateECarTrip(ecbp.Trip, bookingResponse.rows[0].id);
        //    //                        if (!string.IsNullOrWhiteSpace(bookingResponse.rows[0].id))
        //    //                        {
        //    //                            if (ecbp.EnableResponseAndLog)
        //    //                            {
        //    //                                GroundReservation reservation = ecbp.request.GroundReservations.FirstOrDefault();
        //    //                                response = new OTA_GroundBookRS();
        //    //                                response.EchoToken = ecbp.request.EchoToken;
        //    //                                response.Target = ecbp.request.Target;
        //    //                                response.Version = ecbp.request.Version;
        //    //                                response.PrimaryLangID = ecbp.request.PrimaryLangID;

        //    //                                response.Success = new Success();

        //    //                                //Reservation
        //    //                                response.Reservations = new List<Reservation>();
        //    //                                Reservation r = new Reservation();
        //    //                                r.Confirmation = new Confirmation();
        //    //                                //r.Confirmation.Type = "???";
        //    //                                r.Confirmation.ID = ecbp.Trip.Id.ToString();
        //    //                                r.Confirmation.Type = ConfirmationType.Confirmation.ToString();

        //    //                                r.Passenger = reservation.Passenger;
        //    //                                r.Service = reservation.Service;
        //    //                                r.Confirmation.TPA_Extensions = new TPA_Extensions();
        //    //                                //r.Confirmation.TPA_Extensions.DispatchConfirmation = new Confirmation();
        //    //                                //r.Confirmation.TPA_Extensions.DispatchConfirmation.ID = tbp.Trip.DispatchTripId;
        //    //                                r.Confirmation.TPA_Extensions.Confirmations = new List<Confirmation>();
        //    //                                r.Confirmation.TPA_Extensions.Confirmations.Add(new Confirmation { ID = ecbp.Trip.ecar_trip.DispatchTripId, Type = ConfirmationType.DispatchConfirmation.ToString() });// = tbp.Trip.DispatchTripId;
        //    //                                //r.Confirmation.TPA_Extensions.Statuses = new Statuses();
        //    //                                //r.Confirmation.TPA_Extensions.Statuses.Status = new List<Common.DTO.OTA.Status>();
        //    //                                response.Reservations.Add(r);

        //    //                                #region Put status into ecar_trip_status



        //    //                                utility.SaveTripStatus(ecbp.Trip.Id, ECarTripStatusType.Booked, requestedLocalTime, null, null, null, null, null, null, string.Empty, string.Empty, null);
        //    //                                #endregion



        //    //                            }
        //    //                            else
        //    //                            {
        //    //                                logger.Info("Disable response and log for booking");
        //    //                            }
        //    //                        }
        //    //                        else
        //    //                        {
        //    //                            string error = string.Format("Cannot get DispatchTripId. Or dispatchTripId is white space.");
        //    //                            logger.Error(error);
        //    //                            utility.MarkThisTripAsError(ecbp.Trip.Id, error);
        //    //                            throw new ECarException(error);
        //    //                        }
        //    //                    }
        //    //                    #endregion
        //    //                    #endregion

        //    //                }
        //    //                else
        //    //                {
        //    //                    logger.Error("Unable to get reference type in Green Tomato");
        //    //                    throw VtodException.CreateException(ExceptionType.Ecar, 303);// new ECarException(Messages.ECar_UnableToGetReferenceTypeGreenTomato);
        //    //                }


        //    //            }
        //    //            else
        //    //            {
        //    //                logger.Error("Unable to get a service in Green Tomato");
        //    //                throw VtodException.CreateException(ExceptionType.Ecar, 303);// new ECarException(Messages.ECar_UnableToGetServiceGreenTomato);
        //    //            }
        //    //        }
        //    //        else
        //    //        {
        //    //            logger.Error("Unable to get or create a contact in Green Tomato");
        //    //            throw VtodException.CreateException(ExceptionType.Ecar, 303);// new ECarException(Messages.ECar_UnableToGetServiceGreenTomato);
        //    //        }

        //    //    }
        //    //    else
        //    //    {
        //    //        //unable to do status
        //    //        var vtodException = VtodException.CreateException(ExceptionType.Ecar, 303);
        //    //        logger.WarnFormat("Unable to login green tomato or doesn't get a valid JSESSIONID");
        //    //        throw vtodException; // new ECarException(Messages.ECar_StatusFailure);
        //    //    }


        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //    logger.ErrorFormat("Message:{0}", ex.Message);
        //    //    logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
        //    //    logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);

        //    //    //change the status of trip
        //    //    utility.UpdateFinalStatus(ecbp.Trip.Id, ECarTripStatusType.Error, null, null, null);

        //    //    throw ex;
        //    //}
        //    //sw.Stop();
        //    //logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
        //    #endregion


        //    #region new way

        //    DateTime? requestedLocalTime = null;
        //    if (ecbp.Fleet != null && ecbp.Fleet.ServerUTCOffset != null)
        //    {
        //        requestedLocalTime = DateTime.UtcNow.FromUtcNullAble(ecbp.Fleet.ServerUTCOffset);
        //    }

        //    try
        //    {
        //        Dictionary<string, string> requestHeader = new Dictionary<string, string>();
        //        Dictionary<string, string> responseHeader = new Dictionary<string, string>();

        //        if (Login(out requestHeader))
        //        {
        //            //Change user first name and last name into Test for Test system
        //            if (ConfigurationManager.AppSettings["IsTest"].ToBool())
        //            {
        //                ecbp.FirstName = "Test";
        //                ecbp.LastName = "Test";
        //            }

        //            //Get contact
        //            GreenTomato_contact contact = GetContact(ecbp.FirstName, ecbp.LastName, ecbp.User.AccountNumber, ecbp.PhoneNumber, requestHeader, ecbp.emailAddress);
        //            if (contact != null)
        //            {
        //                //get service Id
        //                //GreenTomato_ServiceListRS serviceRS = GetService(ecbp.User.AccountNumber, requestHeader);
        //                //if (serviceRS != null && serviceRS.success.ToLowerInvariant() == "true" && serviceRS.rows.Any() && !string.IsNullOrWhiteSpace(serviceRS.rows[0].id))
        //                if (!string.IsNullOrWhiteSpace(ecbp.Relation.DispatchServiceId))
        //                {

        //                    //get reference type
        //                    GreenTomato_ReferenceTypeRS refTypeRS = GetReferenceType(ecbp.User.AccountNumber, requestHeader);
        //                    if (refTypeRS != null && refTypeRS.success.ToLowerInvariant() == "true" && refTypeRS.rows.Any())
        //                    {

        //                        #region Book
        //                        #region Create JSON Message
        //                        //string postContent = BuildBookingRequest(ecbp, contact.id, serviceRS.rows[0].id, refTypeRS.rows[0]);
        //                        string postContent = BuildBookingRequest(ecbp, contact.id, ecbp.Relation.DispatchServiceId, refTypeRS.rows[0], requestHeader);
        //                        #endregion

        //                        #region Send HttpRequest and set response into object
        //                        string status = "";
        //                        int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["GreenTomatoAPI_Https_Timeout"]);
        //                        string bookingResult = WebRequestHelper.ProcessWebRequest(GetHttpURL(ConfigurationManager.AppSettings["GreenTomatoAPI_URL_Book"]), postContent, "application/json", "POST", string.Empty, string.Empty, timeout, requestHeader, out status, out  responseHeader);
        //                        GreenTomato_BookingRS bookingResponse = bookingResult.JsonDeserialize<GreenTomato_BookingRS>();

        //                        if (bookingResponse.success.ToLowerInvariant().Equals("false"))
        //                        {
        //                            //failure
        //                            string errorMessage = "";
        //                            string msg = "";
        //                            if (bookingResponse.errors.Any())
        //                            {
        //                                errorMessage = bookingResponse.errors[0].message;
        //                            }
        //                            else
        //                            {
        //                                errorMessage = "No error messages from API";
        //                            }
        //                            msg = string.Format("Unable to book this trip. tripID={0}, errormessage={1}", ecbp.Trip.Id, bookingResponse.errors[0].message);
        //                            logger.Warn(msg);
        //                            throw new ECarException(msg);
        //                        }
        //                        else
        //                        {
        //                            //sucess
        //                            logger.Info("Booking sucessful");

        //                            ecbp.Trip = utility.UpdateECarTrip(ecbp.Trip, bookingResponse.rows[0].id);
        //                            if (!string.IsNullOrWhiteSpace(bookingResponse.rows[0].id))
        //                            {
        //                                if (ecbp.EnableResponseAndLog)
        //                                {
        //                                    GroundReservation reservation = ecbp.request.GroundReservations.FirstOrDefault();
        //                                    response = new OTA_GroundBookRS();
        //                                    response.EchoToken = ecbp.request.EchoToken;
        //                                    response.Target = ecbp.request.Target;
        //                                    response.Version = ecbp.request.Version;
        //                                    response.PrimaryLangID = ecbp.request.PrimaryLangID;

        //                                    response.Success = new Success();

        //                                    //Reservation
        //                                    response.Reservations = new List<Reservation>();
        //                                    Reservation r = new Reservation();
        //                                    r.Confirmation = new Confirmation();
        //                                    //r.Confirmation.Type = "???";
        //                                    r.Confirmation.ID = ecbp.Trip.Id.ToString();
        //                                    r.Confirmation.Type = ConfirmationType.Confirmation.ToString();

        //                                    r.Passenger = reservation.Passenger;
        //                                    r.Service = reservation.Service;
        //                                    r.Confirmation.TPA_Extensions = new TPA_Extensions();
        //                                    //r.Confirmation.TPA_Extensions.DispatchConfirmation = new Confirmation();
        //                                    //r.Confirmation.TPA_Extensions.DispatchConfirmation.ID = tbp.Trip.DispatchTripId;
        //                                    r.Confirmation.TPA_Extensions.Confirmations = new List<Confirmation>();
        //                                    r.Confirmation.TPA_Extensions.Confirmations.Add(new Confirmation { ID = ecbp.Trip.ecar_trip.DispatchTripId, Type = ConfirmationType.DispatchConfirmation.ToString() });// = tbp.Trip.DispatchTripId;
        //                                    //r.Confirmation.TPA_Extensions.Statuses = new Statuses();
        //                                    //r.Confirmation.TPA_Extensions.Statuses.Status = new List<Common.DTO.OTA.Status>();
        //                                    response.Reservations.Add(r);

        //                                    #region Put status into ecar_trip_status



        //                                    utility.SaveTripStatus(ecbp.Trip.Id, ECarTripStatusType.Booked, requestedLocalTime, null, null, null, null, null, null, string.Empty, string.Empty, null);
        //                                    #endregion



        //                                }
        //                                else
        //                                {
        //                                    logger.Info("Disable response and log for booking");
        //                                }
        //                            }
        //                            else
        //                            {
        //                                string error = string.Format("Cannot get DispatchTripId. Or dispatchTripId is white space.");
        //                                logger.Error(error);
        //                                utility.MarkThisTripAsError(ecbp.Trip.Id, error);
        //                                throw new ECarException(error);
        //                            }
        //                        }
        //                        #endregion
        //                        #endregion

        //                    }
        //                    else
        //                    {
        //                        logger.Error("Unable to get reference type in Green Tomato");
        //                        throw VtodException.CreateException(ExceptionType.ECar, 303);// new ECarException(Messages.ECar_UnableToGetReferenceTypeGreenTomato);
        //                    }


        //                }
        //                else
        //                {
        //                    logger.Error("Unable to get a service in Green Tomato");
        //                    throw VtodException.CreateException(ExceptionType.ECar, 303);// new ECarException(Messages.ECar_UnableToGetServiceGreenTomato);
        //                }
        //            }
        //            else
        //            {
        //                logger.Error("Unable to get or create a contact in Green Tomato");
        //                throw VtodException.CreateException(ExceptionType.ECar, 303);// new ECarException(Messages.ECar_UnableToGetServiceGreenTomato);
        //            }

        //        }
        //        else
        //        {
        //            //unable to do status
        //            var vtodException = VtodException.CreateException(ExceptionType.ECar, 303);
        //            logger.WarnFormat("Unable to login green tomato or doesn't get a valid JSESSIONID");
        //            throw vtodException; // new ECarException(Messages.ECar_StatusFailure);
        //        }


        //    }
        //    catch (Exception ex)
        //    {
        //        logger.ErrorFormat("Message:{0}", ex.Message);
        //        logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
        //        logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);

        //        //change the status of trip
        //        utility.UpdateFinalStatus(ecbp.Trip.Id, ECarTripStatusType.Error, null, null, null);

        //        throw ex;
        //    }
        //    sw.Stop();
        //    logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

        //    #endregion

        //    #region Track
        //    if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_GreenTomato, "Book");
        //    #endregion

        //    return response;

        //}

        //public OTA_GroundCancelRS Cancel(ECarCancelParameter eccp)
        //{
        //    OTA_GroundCancelRS response = null;
        //    Stopwatch sw = new Stopwatch();
        //    sw.Start();

        //    #region Track
        //    if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
        //    #endregion

        //    DateTime? requestedLocalTime = null;
        //    if (eccp.Fleet != null && eccp.Fleet.ServerUTCOffset != null)
        //    {
        //        requestedLocalTime = DateTime.UtcNow.FromUtcNullAble(eccp.Fleet.ServerUTCOffset);
        //    }
        //    try
        //    {

        //        if (utility.IsAbleToCancelThisTrip(eccp.Trip.FinalStatus))
        //        {

        //            Dictionary<string, string> requestHeader = new Dictionary<string, string>();
        //            Dictionary<string, string> responseHeader = new Dictionary<string, string>();

        //            if (Login(out requestHeader))
        //            {
        //                GreenTomato_CancelRQ rq = new GreenTomato_CancelRQ();
        //                rq.bookingId = eccp.Trip.ecar_trip.DispatchTripId;
        //                string rqString = rq.JsonSerialize();
        //                rqString = "bookingId=" + eccp.Trip.ecar_trip.DispatchTripId;
        //                string status = "";
        //                int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["GreenTomatoAPI_Https_Timeout"]);
        //                //string result = WebRequestHelper.ProcessWebRequest("https://80.87.31.28/ext-api/api/bookings/cancel", rqString, "application/json", "POST", string.Empty, string.Empty, 3000, requestHeader, out status);
        //                //string result = WebRequestHelper.ProcessWebRequest(GetHttpURL(ConfigurationManager.AppSettings["GreenTomatoAPI_URL_Cancel"]), rqString, "application/json", "POST", string.Empty, string.Empty, 3000, requestHeader, out status);
        //                string result = WebRequestHelper.ProcessWebRequest(GetHttpURL(ConfigurationManager.AppSettings["GreenTomatoAPI_URL_Cancel"]), rqString, "application/x-www-form-urlencoded", "POST", string.Empty, string.Empty, timeout, requestHeader, out status);
        //                var cancelRS = result.JsonDeserialize<GreenTomato_CancelRS>();

        //                if (cancelRS != null && cancelRS.success.ToLowerInvariant().Equals("true"))
        //                {
        //                    response = new OTA_GroundCancelRS();
        //                    response.Success = new Success();
        //                    response.EchoToken = eccp.request.EchoToken;
        //                    response.Target = eccp.request.Target;
        //                    response.Version = eccp.request.Version;
        //                    response.PrimaryLangID = eccp.request.PrimaryLangID;

        //                    response.Reservation = new Reservation();
        //                    response.Reservation.CancelConfirmation = new CancelConfirmation();
        //                    response.Reservation.CancelConfirmation.UniqueID = new UniqueID { ID = eccp.Trip.Id.ToString() };

        //                    #region Set final status
        //                    utility.UpdateFinalStatus(eccp.Trip.Id, ECarTripStatusType.Canceled, null, null, null);
        //                    #endregion

        //                    #region Put status into ecar_trip_status
        //                    utility.SaveTripStatus(eccp.Trip.Id, ECarTripStatusType.Canceled, requestedLocalTime, null, null, null, null, null, null, string.Empty, string.Empty, null);
        //                    #endregion

        //                }
        //                else
        //                {
        //                    logger.WarnFormat("Unable to cancel this trip. Trip Id ={0}, dispatchId={1}", eccp.Trip.Id, eccp.Trip.ecar_trip.DispatchTripId);
        //                    throw VtodException.CreateException(ExceptionType.ECar, 5001);// new ECarException(Messages.ECar_UnableToCancelGreenTomato);

        //                }


        //            }
        //            else
        //            {
        //                //unable to do status
        //                var vtodException = VtodException.CreateException(ExceptionType.ECar, 303);
        //                logger.WarnFormat("Unable to login green tomato or doesn't get a valid JSESSIONID");
        //                throw vtodException; // new ECarException(Messages.ECar_StatusFailure);
        //            }
        //        }
        //        else
        //        {
        //            throw new ECarException(Messages.ECar_CancelFailure);
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        logger.ErrorFormat("Message:{0}", ex.Message);
        //        logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
        //        logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
        //        response = null;

        //    }

        //    sw.Stop();
        //    logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

        //    #region Track
        //    if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_GreenTomato, "Cancel");
        //    #endregion

        //    return response;
        //}

        //public OTA_GroundAvailRS GetEstimation(ECarGetEstimationParameter ecgep)
        //{
        //    logger.Info("Start");
        //    Stopwatch sw = new Stopwatch();
        //    sw.Start();
        //    OTA_GroundAvailRS response = new OTA_GroundAvailRS();
        //    try
        //    {
        //        Dictionary<string, string> requestHeader = new Dictionary<string, string>();
        //        Dictionary<string, string> responseHeader = new Dictionary<string, string>();

        //        if (Login(out requestHeader))
        //        {
        //            response = GetPrice(ecgep, requestHeader);
        //        }
        //        else
        //        {
        //            //unable to do status
        //            var vtodException = VtodException.CreateException(ExceptionType.ECar, 303);
        //            logger.WarnFormat("Unable to login green tomato or doesn't get a valid JSESSIONID");
        //            throw vtodException; // new ECarException(Messages.ECar_StatusFailure);
        //        }


        //    }
        //    catch (Exception ex)
        //    {
        //        logger.ErrorFormat("Message:{0}", ex.Message);
        //        logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
        //        logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);

        //        throw ex;
        //    }
        //    sw.Stop();
        //    logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
        //    return response;
        //}
		#endregion

		#region new way
		public OTA_GroundBookRS Book(TokenRS tokenVTOD, OTA_GroundBookRQ request, out List<Tuple<string, int, long>> disptach_Rez_VTOD, ECarBookingParameter ecbp)
		{
			Stopwatch sw = new Stopwatch();
			sw.Start();
			OTA_GroundBookRS response = null;
			disptach_Rez_VTOD = new List<Tuple<string, int, long>>();
			ECarUtility utility = new ECarUtility(logger);

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
			#endregion

			#region [ECar 1.1] Validate required fields
			if (!utility.ValidateBookingRequestForGreenTomato(tokenVTOD, request, ecbp, out ecbp))
			{
				throw new ValidationException(Messages.ECar_Common_InvalidBookingRequest);
			}
			#endregion

			#region [ECar 1.2] Book the trip
			DateTime? requestedLocalTime = null;
			if (ecbp.Fleet != null && ecbp.Fleet.ServerUTCOffset != null)
			{
				requestedLocalTime = DateTime.UtcNow.FromUtcNullAble(ecbp.Fleet.ServerUTCOffset);
			}

			try
			{
				Dictionary<string, string> requestHeader = new Dictionary<string, string>();
				Dictionary<string, string> responseHeader = new Dictionary<string, string>();

				if (Login(out requestHeader))
				{
					//Change user first name and last name into Test for Test system
					if (ConfigurationManager.AppSettings["IsTest"].ToBool())
					{
						ecbp.FirstName = "Test";
						ecbp.LastName = "Test";
					}

					//Get contact
					GreenTomato_contact contact = GetContact(ecbp.FirstName, ecbp.LastName, ecbp.User.AccountNumber, ecbp.PhoneNumber, requestHeader, ecbp.emailAddress);
					if (contact != null)
					{
						//get service Id
						//GreenTomato_ServiceListRS serviceRS = GetService(ecbp.User.AccountNumber, requestHeader);
						//if (serviceRS != null && serviceRS.success.ToLowerInvariant() == "true" && serviceRS.rows.Any() && !string.IsNullOrWhiteSpace(serviceRS.rows[0].id))
						if (!string.IsNullOrWhiteSpace(ecbp.Relation.DispatchServiceId))
						{

							//get reference type
							GreenTomato_ReferenceTypeRS refTypeRS = GetReferenceType(ecbp.User.AccountNumber, requestHeader);
							if (refTypeRS != null && refTypeRS.success.ToLowerInvariant() == "true" && refTypeRS.rows.Any())
							{

								#region Book
								#region Create JSON Message
								//string postContent = BuildBookingRequest(ecbp, contact.id, serviceRS.rows[0].id, refTypeRS.rows[0]);
								string postContent = BuildBookingRequest(ecbp, contact.id, ecbp.Relation.DispatchServiceId, refTypeRS.rows[0], requestHeader);
								#endregion

								#region Send HttpRequest and set response into object
								string status = "";
								int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["GreenTomatoAPI_Https_Timeout"]);
								string bookingResult = WebRequestHelper.ProcessWebRequest(GetHttpURL(ConfigurationManager.AppSettings["GreenTomatoAPI_URL_Book"]), postContent, "application/json", "POST", string.Empty, string.Empty, timeout, requestHeader, out status, out  responseHeader);
								GreenTomato_BookingRS bookingResponse = bookingResult.JsonDeserialize<GreenTomato_BookingRS>();

								if (bookingResponse.success.ToLowerInvariant().Equals("false"))
								{
									//failure
									string errorMessage = "";
									string msg = "";
									if (bookingResponse.errors.Any())
									{
										errorMessage = bookingResponse.errors[0].message;
									}
									else
									{
										errorMessage = "No error messages from API";
									}
									msg = string.Format("Unable to book this trip. tripID={0}, errormessage={1}", ecbp.Trip.Id, bookingResponse.errors[0].message);
									logger.Warn(msg);
									throw new ECarException(msg);
								}
								else
								{
									//sucess
									logger.Info("Booking sucessful");

									ecbp.Trip = utility.UpdateECarTrip(ecbp.Trip, bookingResponse.rows[0].id,ecbp.Fleet_TripCode);
									if (!string.IsNullOrWhiteSpace(bookingResponse.rows[0].id))
									{
										if (ecbp.EnableResponseAndLog)
										{
											GroundReservation reservation = ecbp.request.GroundReservations.FirstOrDefault();
											response = new OTA_GroundBookRS();
											response.EchoToken = ecbp.request.EchoToken;
											response.Target = ecbp.request.Target;
											response.Version = ecbp.request.Version;
											response.PrimaryLangID = ecbp.request.PrimaryLangID;

											response.Success = new Success();

											//Reservation
											response.Reservations = new List<Reservation>();
											Reservation r = new Reservation();
											r.Confirmation = new Confirmation();
											//r.Confirmation.Type = "???";
											r.Confirmation.ID = ecbp.Trip.Id.ToString();
											r.Confirmation.Type = ConfirmationType.Confirmation.ToString();

											r.Passenger = reservation.Passenger;
											r.Service = reservation.Service;
											r.Confirmation.TPA_Extensions = new TPA_Extensions();
											//r.Confirmation.TPA_Extensions.DispatchConfirmation = new Confirmation();
											//r.Confirmation.TPA_Extensions.DispatchConfirmation.ID = tbp.Trip.DispatchTripId;
											r.Confirmation.TPA_Extensions.Confirmations = new List<Confirmation>();
											r.Confirmation.TPA_Extensions.Confirmations.Add(new Confirmation { ID = ecbp.Trip.ecar_trip.DispatchTripId, Type = ConfirmationType.DispatchConfirmation.ToString() });// = tbp.Trip.DispatchTripId;
											//r.Confirmation.TPA_Extensions.Statuses = new Statuses();
											//r.Confirmation.TPA_Extensions.Statuses.Status = new List<Common.DTO.OTA.Status>();
                                            #region FleetInfo
                                            ecar_fleet fleetObj = null;
                                            using (var db = new DataAccess.VTOD.VTODEntities())
                                            {
                                                Int64 fleetId = 0;
                                                ecar_trip tripObj = db.ecar_trip.Where(p => p.Id == ecbp.Trip.Id).FirstOrDefault();
                                                if (tripObj != null)
                                                {
                                                    fleetId = tripObj.FleetId;
                                                }
                                                if(fleetId>0)
                                                {
                                                    fleetObj=db.ecar_fleet.Where(p => p.Id==fleetId).FirstOrDefault();
                                                }

                                            }
                                            if (fleetObj != null)
                                            {
                                                r.TPA_Extensions = new TPA_Extensions();
                                                r.TPA_Extensions.Contacts = new Contacts();
                                                r.TPA_Extensions.Contacts.Items = new List<Contact>();
                                                var contactDetails = new Contact { Type = Common.DTO.Enum.ContactType.Dispatch.ToString() };
                                                if (!string.IsNullOrWhiteSpace(fleetObj.Name))
                                                {
                                                    contactDetails.Name = fleetObj.Name;
                                                }
                                                if (!string.IsNullOrWhiteSpace(fleetObj.PhoneNumber))
                                                {
                                                    contactDetails.Telephone = new Telephone { PhoneNumber = fleetObj.PhoneNumber };
                                                }
                                                r.TPA_Extensions.Contacts.Items.Add(contactDetails);
                                            }
                                            #endregion
											response.Reservations.Add(r);


                                          
                                            #region Put status into ecar_trip_status



                                            utility.SaveTripStatus(ecbp.Trip.Id, ECarTripStatusType.Booked, requestedLocalTime, null, null, null, null,null, null,null, null, string.Empty, string.Empty, null);
											#endregion



										}
										else
										{
											logger.Info("Disable response and log for booking");
										}
									}
									else
									{
										string error = string.Format("Cannot get DispatchTripId. Or dispatchTripId is white space.");
										logger.Error(error);
										utility.MarkThisTripAsError(ecbp.Trip.Id, error);
										throw new ECarException(error);
									}
								}
								#endregion
								#endregion

							}
							else
							{
								logger.Error("Unable to get reference type in Green Tomato");
								throw VtodException.CreateException(ExceptionType.ECar, 303);// new ECarException(Messages.ECar_UnableToGetReferenceTypeGreenTomato);
							}


						}
						else
						{
							logger.Error("Unable to get a service in Green Tomato");
							throw VtodException.CreateException(ExceptionType.ECar, 303);// new ECarException(Messages.ECar_UnableToGetServiceGreenTomato);
						}
					}
					else
					{
						logger.Error("Unable to get or create a contact in Green Tomato");
						throw VtodException.CreateException(ExceptionType.ECar, 303);// new ECarException(Messages.ECar_UnableToGetServiceGreenTomato);
					}

				}
				else
				{
					//unable to do status
					var vtodException = VtodException.CreateException(ExceptionType.ECar, 303);
					logger.WarnFormat("Unable to login green tomato or doesn't get a valid JSESSIONID");
					throw vtodException; // new ECarException(Messages.ECar_StatusFailure);
				}


			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);

				//change the status of trip
				utility.UpdateFinalStatus(ecbp.Trip.Id, ECarTripStatusType.Error, null, null, null);

				throw ex;
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#endregion

			#region [ECar 1.3] Link  disptach_VTOD_TripIDs (for supershuttel system)
			if (response.Reservations != null && response.Reservations.Any())
			{
				var rez = response.Reservations.First();
				//disptach_VTOD_TripIDs.Add(rez.Confirmation.TPA_Extensions.DispatchConfirmation.ID, rez.Confirmation.ID.ToInt64());
				var DispatchConfirmationID = rez.Confirmation.TPA_Extensions.Confirmations.Where(s => s.Type.Trim().ToLower() == ConfirmationType.DispatchConfirmation.ToString().ToLower()).FirstOrDefault().ID;

				disptach_Rez_VTOD.Add(new Tuple<string, int, long>(DispatchConfirmationID, 0, rez.Confirmation.ID.ToInt64()));
			}
			#endregion

			response.Success = new Success();

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_GreenTomato, "Book");
			#endregion

			return response;
		}
        public bool GetDisabilityInd(TokenRS tokenVTOD, ecar_fleet f, out int condition, string authenticateEndpoint, string bookingEndpoint)
        {
            throw new NotImplementedException();
        }
        public OTA_GroundBookRS ModifyBook(TokenRS tokenVTOD, OTA_GroundBookRQ request, out List<Tuple<string, int, long>> disptach_Rez_VTOD, ECarBookingParameter ecbp)
        {
            throw new NotImplementedException();
        }
		public OTA_GroundResRetrieveRS Status(TokenRS tokenVTOD, OTA_GroundResRetrieveRQ request, ECarStatusParameter ecsp)
		{
			Stopwatch sw = new Stopwatch();
			sw.Start();
			OTA_GroundResRetrieveRS response = null;

			ECarUtility utility = new ECarUtility(logger);

			#region [ECar 2.0] Validate required fields
			if (!utility.ValidateStatusRequestForGreenTomato(tokenVTOD, request, ecsp, out ecsp))
			{
				throw VtodException.CreateException(ExceptionType.ECar, 3003);
			}
			#endregion

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
			#endregion

			try
			{
				DateTime? requestedLocalTime = null;
				if (ecsp.Fleet != null && ecsp.Fleet.ServerUTCOffset != null)
				{
					requestedLocalTime = DateTime.UtcNow.FromUtcNullAble(ecsp.Fleet.ServerUTCOffset);
				}

				try
				{
					Dictionary<string, string> requestHeader = new Dictionary<string, string>();
					Dictionary<string, string> responseHeader = new Dictionary<string, string>();

					if (Login(out requestHeader))
					{
						string HttpStatus = "";
						int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["GreenTomatoAPI_Https_Timeout"]);
						//string url = string.Format("https://80.87.31.28/ext-api/api/bookings/getById/{0}", tsp.Trip.DispatchTripId);
						string url = string.Format("{0}/{1}", GetHttpURL(ConfigurationManager.AppSettings["GreenTomatoAPI_URL_Status"]), ecsp.Trip.ecar_trip.DispatchTripId);
						string statusResult = WebRequestHelper.ProcessWebRequest(url, string.Empty, "application/json", "GET", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out  responseHeader);
						GreenTomato_StatusRS statusResponse = statusResult.JsonDeserialize<GreenTomato_StatusRS>();

						#region SharedETA Link
						string sharedLink = null;
						string appSharedLink = null;
						string link = null;

						if (request.TPA_Extensions != null && request.TPA_Extensions.RateQualifiers != null && request.TPA_Extensions.RateQualifiers.Any())
						{
							string uniqueID = null;
							string rateQualifier = null;
							appSharedLink = ConfigurationManager.AppSettings["SharedETALink"];
							if (request.Reference.FirstOrDefault().Type.Contains("Dispatch"))
							{
								link = "?t=d";
							}
							else
							{
								link = "?t=c";
							}
							if (request.TPA_Extensions.RateQualifiers != null && request.TPA_Extensions.RateQualifiers.Any())
							{
								rateQualifier = request.TPA_Extensions.RateQualifiers.FirstOrDefault().RateQualifierValue;
								link = link + "&RQ=" + rateQualifier;
							}
							if (request.Reference != null && request.Reference.Any())
							{
								uniqueID = request.Reference.FirstOrDefault().ID;
							}
							link = link + "&id=" + UDI.Utility.Helper.Cryptography.ConvertStringToHex(uniqueID); 
						}
                       
                        #endregion

						if (statusResponse.success.ToLowerInvariant().Equals("true") && statusResponse.rows.Any())
						{
							response = new OTA_GroundResRetrieveRS();
							response.Success = new Success();
							response.EchoToken = ecsp.request.EchoToken;
							response.Target = ecsp.request.Target;
							response.Version = ecsp.request.Version;
							response.PrimaryLangID = ecsp.request.PrimaryLangID;

							response.TPA_Extensions = new TPA_Extensions();
							response.TPA_Extensions.Driver = (statusResponse.rows[0].driverRecord == null) ? null : utility.ExtractDriverName(statusResponse.rows[0].driverRecord.fullName);
							response.TPA_Extensions.Vehicles = new Vehicles();
							response.TPA_Extensions.Vehicles.Items = new List<Vehicle>();
                            response.TPA_Extensions.SharedLink = appSharedLink + link;

							//calcute ETA
							int? minutesAway = null;

							//Vehicle
							var vehicle = new Vehicle();
							vehicle.Geolocation = new Geolocation();
							GreenTomato_GetVehicleInfoForStatusRS vInfo = null;
							if (statusResponse.rows[0].vehicleRecord != null && !string.IsNullOrWhiteSpace(statusResponse.rows[0].vehicleRecord.id))
							{
								vehicle.ID = statusResponse.rows[0].vehicleRecord.id;
								vInfo = GetVehicleInfoForStautus(vehicle.ID, requestHeader);
								if (vInfo.success.ToLowerInvariant().Equals("true") && vInfo.rows.Any())
								{
									vehicle.Geolocation.Lat = vInfo.rows[0].currentLocationLatitude.ToDecimal();
									vehicle.Geolocation.Long = vInfo.rows[0].currentLocationLongitude.ToDecimal();
								}
							}
							else
							{
								logger.Info("No vehicle information from response");
							}
							#region Caculate ETA by ourself


							//Replace status
							string originalStatus = statusResponse.rows[0].status;
							string status = utility.ConvertTripStatusForGreenTomato(originalStatus);



							#region Others
							//fare
							decimal? totalFare = null;
							if (status.Trim().ToLower() == ECarTripStatusType.Completed.Trim().ToLower() || status.Trim().ToLower() == ECarTripStatusType.Fare.Trim().ToLower())
							{
								totalFare = statusResponse.rows[0].totalPrice.ToDecimal();
							}
							else
							{
								totalFare = ecsp.Trip.TotalFareAmount;
							}

							//Convert status to front-end status
							string frontEndStatus = utility.ConvertTripStatusMessageForFrontEndDevice(ecsp.Fleet_User.UserId, status);
							//Status s = new Common.DTO.OTA.Status { Value = status, Fare = succ.TripInfo[0].FareAmount.ToDecimal() };
							Status s = new Common.DTO.OTA.Status { Value = frontEndStatus };
							if (totalFare.HasValue)
							{
								s.Fare = totalFare.Value;
							}
							response.TPA_Extensions.Statuses = new Statuses();
							response.TPA_Extensions.Statuses.Status = new List<Status>();
							response.TPA_Extensions.Statuses.Status.Add(s);
							response.TPA_Extensions.Contacts = new Contacts();
							response.TPA_Extensions.Contacts.Items = new List<Contact>();
							var contact = new Contact { Type = Common.DTO.Enum.ContactType.Dispatch.ToString(), Name = ecsp.Fleet.Alias, Telephone = new Telephone { PhoneNumber = ecsp.Fleet.PhoneNumber } };
							response.TPA_Extensions.Contacts.Items.Add(contact);

							if (ecsp.Trip != null && ecsp.Trip.ecar_trip.PickupLatitude.HasValue && ecsp.Trip.ecar_trip.PickupLatitude.Value != 0 && ecsp.Trip.ecar_trip.PickupLongitude.HasValue && ecsp.Trip.ecar_trip.PickupLongitude.Value != 0)
							{
								response.TPA_Extensions.PickupLocation = new Pickup_Dropoff_Stop();
								response.TPA_Extensions.PickupLocation.Address = new Common.DTO.OTA.Address();
								response.TPA_Extensions.PickupLocation.Address.Latitude = ecsp.Trip.ecar_trip.PickupLatitude.Value.ToString();
								response.TPA_Extensions.PickupLocation.Address.Longitude = ecsp.Trip.ecar_trip.PickupLongitude.Value.ToString();
							}

                            if (ecsp.Trip != null && ecsp.Trip.ecar_trip.DropOffLatitude.HasValue && ecsp.Trip.ecar_trip.DropOffLatitude.Value != 0 && ecsp.Trip.ecar_trip.DropOffLongitude.HasValue && ecsp.Trip.ecar_trip.DropOffLongitude.Value != 0)
                            {
                                response.TPA_Extensions.DropoffLocation = new Pickup_Dropoff_Stop();
                                response.TPA_Extensions.DropoffLocation.Address = new Common.DTO.OTA.Address();
                                response.TPA_Extensions.DropoffLocation.Address.Latitude = ecsp.Trip.ecar_trip.DropOffLatitude.Value.ToString();
                                response.TPA_Extensions.DropoffLocation.Address.Longitude = ecsp.Trip.ecar_trip.DropOffLongitude.Value.ToString();
                            }
							#endregion

							if ((vehicle.Geolocation.Lat != 0 && vehicle.Geolocation.Long != 0)
								&&
								(ecsp.Trip.ecar_trip.PickupLatitude.HasValue && ecsp.Trip.ecar_trip.PickupLongitude.HasValue)
							)
							{
								MapService ms = new MapService();
								double d = ms.GetDirectDistanceInMeter(ecsp.Trip.ecar_trip.PickupLatitude.Value, ecsp.Trip.ecar_trip.PickupLongitude.Value, Convert.ToDouble(vehicle.Geolocation.Lat), Convert.ToDouble(vehicle.Geolocation.Long));
								var rc = new UDI.SDS.RoutingController(ecsp.tokenVTOD, null);
								UDI.SDS.RoutingService.Point start = new UDI.SDS.RoutingService.Point { Latitude = ecsp.Trip.ecar_trip.PickupLatitude.Value, Longitude = ecsp.Trip.ecar_trip.PickupLongitude.Value };
								UDI.SDS.RoutingService.Point end = new UDI.SDS.RoutingService.Point { Latitude = Convert.ToDouble(vehicle.Geolocation.Lat), Longitude = Convert.ToDouble(vehicle.Geolocation.Long) };
								var result = rc.GetRouteMetrics(start, end);
								vehicle.MinutesAway = result.TotalMinutes;
                                if (vehicle.MinutesAway != null) minutesAway = Convert.ToInt32(vehicle.MinutesAway);
							}
							response.TPA_Extensions.Vehicles.Items.Add(vehicle);
							#endregion





							//Others

							//update ecar trip
							string driverName = (statusResponse.rows[0].driverRecord == null) ? string.Empty : statusResponse.rows[0].driverRecord.fullName;
							utility.UpdateECarTrip(ecsp.Trip.Id, statusResponse.rows[0].serviceRecord.id, minutesAway, ecsp.Trip.ecar_trip.DispatchTripId, driverName);


							DateTime? statusTime = null;
							DateTime? tripStartTime = null;
							DateTime? tripEndTime = null;

							if (!statusTime.HasValue || statusTime.Value <= DateTime.MinValue)
							{
								if (ecsp.Fleet != null && ecsp.Fleet.ServerUTCOffset != null)
								{
									statusTime = DateTime.UtcNow.FromUtcNullAble(ecsp.Fleet.ServerUTCOffset);
								}
							}

							if ((status.Trim().ToLower() == ECarTripStatusType.InService.Trim().ToLower() || status.Trim().ToLower() == ECarTripStatusType.MeterON.Trim().ToLower()) && (ecsp.Trip.TripStartTime == null))
							{
								tripStartTime = statusTime;
							}
							else
							{
								tripStartTime = ecsp.Trip.TripStartTime;
							}

							if ((status.Trim().ToLower() == ECarTripStatusType.Completed.Trim().ToLower() || status.Trim().ToLower() == ECarTripStatusType.MeterOff.Trim().ToLower()) && (ecsp.Trip.TripEndTime == null))
							{
								tripEndTime = statusTime;
							}
							else
							{
								tripEndTime = ecsp.Trip.TripEndTime;
							}

							if (status.ToUpperInvariant() == ECarTripStatusType.Canceled.ToUpperInvariant())
							{
								utility.SaveTripStatus(ecsp.Trip.Id, ECarTripStatusType.Canceled, statusTime, null, null, null, null,null, null,null, null, string.Empty, originalStatus, null);
								utility.UpdateFinalStatus(ecsp.Trip.Id, ECarTripStatusType.Canceled, null, null, null);
							}
							else
							{
								//Save trip status
								utility.SaveTripStatus(ecsp.Trip.Id, status, statusTime, response.TPA_Extensions.Vehicles.Items.First().ID, response.TPA_Extensions.Vehicles.Items.First().Geolocation.Lat, response.TPA_Extensions.Vehicles.Items.First().Geolocation.Long, totalFare, totalFare, response.TPA_Extensions.Vehicles.Items.First().MinutesAway.ToInt32(), response.TPA_Extensions.Vehicles.Items.First().MinutesAwayWithTraffic.ToInt32(), driverName, string.Empty, originalStatus, null);

								//Save final status
								utility.UpdateFinalStatus(ecsp.Trip.Id, status, totalFare, tripStartTime, tripEndTime);
							}
						}
						else
						{
							//unable to do status
							var vtodException = VtodException.CreateException(ExceptionType.ECar, 303);
							logger.WarnFormat("Unable to login green tomato or doesn't get a valid JSESSIONID");
							throw vtodException; // new ECarException(Messages.ECar_StatusFailure);
						}

					}
					else
					{
						//unable to do status
						var vtodException = VtodException.CreateException(ExceptionType.ECar, 303);
						logger.WarnFormat("Unable to login green tomato or doesn't get a valid JSESSIONID");
						throw vtodException; // new ECarException(Messages.ECar_StatusFailure);
					}




				}
				catch (Exception ex)
				{
					logger.ErrorFormat("Message:{0}", ex.Message);
					logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
					logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);

					throw ex;
				}
				sw.Stop();
				logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw new ECarException(ex.Message);
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);


			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_GreenTomato, "Status");
			#endregion

			return response;
		}

		public OTA_GroundCancelRS Cancel(TokenRS tokenVTOD, OTA_GroundCancelRQ request, ECarCancelParameter eccp)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			OTA_GroundCancelRS response = null;

			ECarUtility utility = new ECarUtility(logger);

			#region [ECar 3.0] Validate required fields
			if (!utility.ValidateCancelRequestForGreenTomato(tokenVTOD, request, eccp, out eccp))
			{
				throw VtodException.CreateException(ExceptionType.ECar, 5005);
			}
			#endregion

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
			#endregion

			try
			{
				DateTime? requestedLocalTime = null;
				if (eccp.Fleet != null && eccp.Fleet.ServerUTCOffset != null)
				{
					requestedLocalTime = DateTime.UtcNow.FromUtcNullAble(eccp.Fleet.ServerUTCOffset);
				}
				try
				{

					if (utility.IsAbleToCancelThisTrip(eccp.Trip.FinalStatus))
					{

						Dictionary<string, string> requestHeader = new Dictionary<string, string>();
						Dictionary<string, string> responseHeader = new Dictionary<string, string>();

						if (Login(out requestHeader))
						{
							GreenTomato_CancelRQ rq = new GreenTomato_CancelRQ();
							rq.bookingId = eccp.Trip.ecar_trip.DispatchTripId;
							string rqString = rq.JsonSerialize();
							rqString = "bookingId=" + eccp.Trip.ecar_trip.DispatchTripId;
							string status = "";
							int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["GreenTomatoAPI_Https_Timeout"]);
							//string result = WebRequestHelper.ProcessWebRequest("https://80.87.31.28/ext-api/api/bookings/cancel", rqString, "application/json", "POST", string.Empty, string.Empty, 3000, requestHeader, out status);
							//string result = WebRequestHelper.ProcessWebRequest(GetHttpURL(ConfigurationManager.AppSettings["GreenTomatoAPI_URL_Cancel"]), rqString, "application/json", "POST", string.Empty, string.Empty, 3000, requestHeader, out status);
							string result = WebRequestHelper.ProcessWebRequest(GetHttpURL(ConfigurationManager.AppSettings["GreenTomatoAPI_URL_Cancel"]), rqString, "application/x-www-form-urlencoded", "POST", string.Empty, string.Empty, timeout, requestHeader, out status);
							var cancelRS = result.JsonDeserialize<GreenTomato_CancelRS>();

							if (cancelRS != null && cancelRS.success.ToLowerInvariant().Equals("true"))
							{
								response = new OTA_GroundCancelRS();
								response.Success = new Success();
								response.EchoToken = eccp.request.EchoToken;
								response.Target = eccp.request.Target;
								response.Version = eccp.request.Version;
								response.PrimaryLangID = eccp.request.PrimaryLangID;

								response.Reservation = new Reservation();
								response.Reservation.CancelConfirmation = new CancelConfirmation();
								response.Reservation.CancelConfirmation.UniqueID = new UniqueID { ID = eccp.Trip.Id.ToString() };

								#region Set final status
								utility.UpdateFinalStatus(eccp.Trip.Id, ECarTripStatusType.Canceled, null, null, null);
								#endregion

								#region Put status into ecar_trip_status
								utility.SaveTripStatus(eccp.Trip.Id, ECarTripStatusType.Canceled, requestedLocalTime, null, null, null, null,null, null,null, null, string.Empty, string.Empty, null);
								#endregion

							}
							else
							{
								logger.WarnFormat("Unable to cancel this trip. Trip Id ={0}, dispatchId={1}", eccp.Trip.Id, eccp.Trip.ecar_trip.DispatchTripId);
								throw VtodException.CreateException(ExceptionType.ECar, 5001);// new ECarException(Messages.ECar_UnableToCancelGreenTomato);

							}


						}
						else
						{
							//unable to do status
							var vtodException = VtodException.CreateException(ExceptionType.ECar, 303);
							logger.WarnFormat("Unable to login green tomato or doesn't get a valid JSESSIONID");
							throw vtodException; // new ECarException(Messages.ECar_StatusFailure);
						}
					}
					else
					{
						throw new ECarException(Messages.ECar_CancelFailure);
					}
				}
				catch (Exception ex)
				{

					logger.ErrorFormat("Message:{0}", ex.Message);
					logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
					logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
					response = null;

				}

				sw.Stop();
				logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw ex;
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);


			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_GreenTomato, "Cancel");
			#endregion

			return response;
		}

        public OTA_GroundAvailRS GetVehicleInfo(TokenRS tokenVTOD, OTA_GroundAvailRQ request, ECarGetVehicleInfoParameter ecgvip)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			var response = new OTA_GroundAvailRS();
			response.Success = new Success();
			response.TPA_Extensions = new TPA_Extensions();
			response.TPA_Extensions.Vehicles = new Vehicles();
			response.TPA_Extensions.Vehicles.Items = new List<Vehicle>();

			ECarUtility utility = new ECarUtility(logger);

			#region [ECar 4.0] Validate required fields
			ECarGetVehicleInfoParameter egvip = null;
			if (!utility.ValidateGetVehicleInfoRequestForGreenTomato(tokenVTOD, request,egvip, out egvip))
			{
				string error = string.Format("This GetVehicleInfo request is invalid.");
				throw new ValidationException(error);
			}
			#endregion


			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
			#endregion

			try
			{
				foreach (var fleet in egvip.fleetList)
				{
					string NearestVehicleETA = "60";
					List<Vehicle> tempVechiles = new List<Vehicle>();


					//get vehicles for this fleet
					DateTime? requestedLocalTime = null;
					if (fleet != null && fleet.ServerUTCOffset != null)
					{
						requestedLocalTime = DateTime.UtcNow.FromUtcNullAble(fleet.ServerUTCOffset);
					}
					try
					{
						Dictionary<string, string> requestHeader = new Dictionary<string, string>();
						Dictionary<string, string> responseHeader = new Dictionary<string, string>();

						if (Login(out requestHeader))
						{
							GreenTomato_GetContactRQ rq = new GreenTomato_GetContactRQ();
							//put long , lat and radius in it
							string postContent = @"{""startIndex"":0, ""count"":15}";
							string status = "";
							int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["GreenTomatoAPI_Https_Timeout"]);
							string result = WebRequestHelper.ProcessWebRequest(GetHttpURL(ConfigurationManager.AppSettings["GreenTomatoAPI_URL_GetVehicleInfo"]), postContent, "application/json", "POST", string.Empty, string.Empty, timeout, requestHeader, out status);
							var vehicleInfoResult = result.JsonDeserialize<GreenTomato_GetVehicleInfoForStatusRS>();

							if (vehicleInfoResult != null && vehicleInfoResult.success.ToLowerInvariant().Equals("true"))
							{
								if (vehicleInfoResult.rows.Any())
								{
									MapService ms = new MapService();

									int shortestIndex = -1;
									double? shortestDistance = null;
									for (int index = 0; index < vehicleInfoResult.rows.Count(); index++)
									{
										double d = ms.GetDirectDistanceInMile(egvip.Latitude, egvip.Longtitude, vehicleInfoResult.rows[index].currentLocationLatitude.ToDouble(), vehicleInfoResult.rows[index].currentLocationLongitude.ToDouble());
										vehicleInfoResult.rows[index].Distance = d;

										if (shortestDistance.HasValue)
										{
											if (shortestDistance.Value > d)
											{
												shortestDistance = d;
												shortestIndex = index;
											}
										}
										else
										{
											shortestDistance = d;
											shortestIndex = index;
										}
									}

									if (shortestIndex != -1)
									{
										//get ETA
										var rc = new UDI.SDS.RoutingController(egvip.tokenVTOD, null);
										UDI.SDS.RoutingService.Point start = new UDI.SDS.RoutingService.Point { Latitude = egvip.Latitude, Longitude = egvip.Longtitude };
										UDI.SDS.RoutingService.Point end = new UDI.SDS.RoutingService.Point { Latitude = vehicleInfoResult.rows[shortestIndex].currentLocationLatitude.ToDouble(), Longitude = vehicleInfoResult.rows[shortestIndex].currentLocationLongitude.ToDouble() };
										var routeResult = rc.GetRouteMetrics(start, end);
										//response.TPA_Extensions.Vehicles.NearestVehicleETA = routeResult.TotalMinutes.ToString();
										NearestVehicleETA = routeResult.TotalMinutes.ToString();
									}

									//response.TPA_Extensions.Vehicles.NumberOfVehicles = vehicleInfoResult.rows.Count().ToString();


									foreach (var item in vehicleInfoResult.rows.OrderBy(s => s.Distance))
									{
										var vehicle = new Vehicle();
										vehicle.Geolocation = new Geolocation();
										vehicle.Geolocation.Long = decimal.Parse(item.currentLocationLongitude.ToString());
										vehicle.Geolocation.Lat = decimal.Parse(item.currentLocationLatitude.ToString());
										vehicle.Geolocation.PriorLong = decimal.Parse(item.availableLongitude.ToString());
										vehicle.Geolocation.PriorLat = decimal.Parse(item.availableLatitude.ToString());

										vehicle.DriverID = item.driverId;
										vehicle.DriverName = item.driverName;
										vehicle.FleetID = fleet.Id.ToString();
										vehicle.ID = item.id;
										vehicle.VehicleStatus = item.status;
										vehicle.VehicleType = item.type;
										vehicle.Distance = item.Distance.ToDecimal();


										#region ETA
										var rc = new UDI.SDS.RoutingController(egvip.tokenVTOD, null);
										UDI.SDS.RoutingService.Point start = new UDI.SDS.RoutingService.Point { Latitude = egvip.Latitude, Longitude = egvip.Longtitude };
										UDI.SDS.RoutingService.Point end = new UDI.SDS.RoutingService.Point { Latitude = item.currentLocationLatitude.ToDouble(), Longitude = item.currentLocationLongitude.ToDouble() };
										var routeResult = rc.GetRouteMetrics(start, end);
										vehicle.MinutesAway = routeResult.TotalMinutes;
										#endregion

                                     

                                        //response.TPA_Extensions.Vehicles.Items.Add(vehicle);
										tempVechiles.Add(vehicle);
									}
								}
								else
								{
									logger.Info("Found no vehicles");
									//response.TPA_Extensions.Vehicles.NumberOfVehicles = "0";
								}
							}
							else
							{
								var vtodExcaption = VtodException.CreateException(ExceptionType.ECar, 4001);
								logger.Warn(vtodExcaption.ExceptionMessage.Message);
								throw vtodExcaption; // throw new ECarException(Messages.ECar_GetVehicleInfoFailure);
							}
						}
						else
						{
							//unable to do status
							var vtodException = VtodException.CreateException(ExceptionType.ECar, 303);
							logger.WarnFormat("Unable to login green tomato or doesn't get a valid JSESSIONID");
							throw vtodException; // new ECarException(Messages.ECar_StatusFailure);
						}
					}
					catch (Exception ex)
					{

						logger.ErrorFormat("Message:{0}", ex.Message);
						logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
						logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
						response = null;

					}



					//addd them in to the response
					if (Convert.ToUInt32(NearestVehicleETA) < Convert.ToUInt32(response.TPA_Extensions.Vehicles.NearestVehicleETA))
					{
						response.TPA_Extensions.Vehicles.NearestVehicleETA = NearestVehicleETA;
					}


					response.TPA_Extensions.Vehicles.Items = response.TPA_Extensions.Vehicles.Items.Concat(tempVechiles.AsEnumerable()).ToList();
					response.TPA_Extensions.Vehicles.NumberOfVehicles += tempVechiles.Count();

				}
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw new ECarException(ex.Message);
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_GreenTomato, "GetVehicleInfo");
			#endregion

			return response;
		}
        public OTA_GroundGetFinalRouteRS GetFinalRoute(TokenRS tokenVTOD, OTA_GroundGetFinalRouteRQ request)
        {
            throw new NotImplementedException();
        }

        public OTA_GroundAvailRS GetEstimation(TokenRS tokenVTOD, OTA_GroundAvailRQ request, ECarGetEstimationParameter ecgep)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			var response = new OTA_GroundAvailRS();
			response.Success = new Success();
			response.TPA_Extensions = new TPA_Extensions();
			response.TPA_Extensions.Vehicles = new Vehicles();
			response.TPA_Extensions.Vehicles.Items = new List<Vehicle>();

			ECarUtility utility = new ECarUtility(logger);

			#region [ECar 4.0] Validate required fields
			if (!utility.ValidateGetEstimationRequestForGreenTomato(tokenVTOD, request,ecgep, out ecgep))
			{
				string error = string.Format("This GetVehicleInfo request is invalid.");
				throw new ValidationException(error);
			}
			#endregion


			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
			#endregion

			try
			{
				Dictionary<string, string> requestHeader = new Dictionary<string, string>();
				Dictionary<string, string> responseHeader = new Dictionary<string, string>();

				if (Login(out requestHeader))
				{
					response = GetPrice(ecgep, requestHeader);
				}
				else
				{
					//unable to do status
					var vtodException = VtodException.CreateException(ExceptionType.ECar, 303);
					logger.WarnFormat("Unable to login green tomato or doesn't get a valid JSESSIONID");
					throw vtodException; // new ECarException(Messages.ECar_StatusFailure);
				}
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw new ECarException(ex.Message);
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_GreenTomato, "GetVehicleInfo");
			#endregion

			return response;
		}

		public Fleet GetAvailableFleets(TokenRS token, UtilityAvailableFleetsRQ input)
		{
			throw new NotImplementedException();
		}

        public Fleet GetLeadTime(TokenRS token, UtilityGetLeadTimeRQ input)
        {
            throw new NotImplementedException();
        }
        public Common.DTO.OTA.UtilityGetClientTokenRS GetCorporateClientToken(Common.DTO.OTA.TokenRS token, Common.DTO.OTA.UtilityGetClientTokenRQ input)
        {
            throw new NotImplementedException();
        }
        public bool NotifyPickupDriver(TokenRS token, UtilityNotifyDriverRQ input)
        {
            throw new NotImplementedException();
        }
        public bool NotifyDriver(TokenRS token, UtilityNotifyDriverRQ input)
        {
            throw new NotImplementedException();
        }
        //public bool GetDisabilityInd(TokenRS tokenVTOD, taxi_fleet f, out int condition, string authenticateEndpoint, string bookingEndpoint)
        //{
        //    throw new NotImplementedException();
        //}
        #endregion

        #endregion

        #region Properties
        public TrackTime TrackTime { get; set; }
		#endregion

		#region Private method
		private GreenTomato_GetVehicleInfoForStatusRS GetVehicleInfoForStautus(string vehicleId, Dictionary<string, string> headers)
		{
			GreenTomato_GetVehicleInfoForStatusRS response = null;

			try
			{
				string status = "";
				//string url = string.Format("https://80.87.31.28/ext-api/api/vehicle/getById/{0}",vehicleId);
				string url = string.Format("{0}/{1}", GetHttpURL(ConfigurationManager.AppSettings["GreenTomatoAPI_URL_GetVehicleInfoForStatus"]), vehicleId);
				string result = WebRequestHelper.ProcessWebRequest(url, string.Empty, "application/json", "GET", string.Empty, string.Empty, 3000, headers, out status);
				response = result.JsonDeserialize<GreenTomato_GetVehicleInfoForStatusRS>();
			}
			catch (Exception ex)
			{

				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				response = null;

			}

			return response;
		}

		private bool Login(out Dictionary<string, string> newHeaders)
		{


			Stopwatch sw = new Stopwatch();
			sw.Start();

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
			#endregion



			bool result = false;
			newHeaders = new Dictionary<string, string>();

			try
			{
				string username = ConfigurationManager.AppSettings["GreenTomatoAPI_UserName"];
				string pwd = ConfigurationManager.AppSettings["GreenTomatoAPI_Password"];
				string method = "GET";
				//string prefixURL = "https://80.87.31.28/ext-api/api/auth/login"; //https://80.87.31.28/ext-api/api/auth/login?username=zTrip&password=DaKQN284
				string url = string.Format("{0}?username={1}&password={2}", GetHttpURL(ConfigurationManager.AppSettings["GreenTomatoAPI_URL_Login"]), username, pwd);
				string contenTtype = "application/x-www-form-urlencoded";
				Dictionary<string, string> headers = new Dictionary<string, string>();
				string status = "";
				int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["GreenTomatoAPI_Https_Timeout"]);


				logger.InfoFormat("url: {0}, contentType: {1}, method: {2}", url, contenTtype, method);


				try
				{
					string loginResult = WebRequestHelper.ProcessWebRequest(url, "", contenTtype, method, timeout, null, out status, out headers);
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

				try
				{
					logger.InfoFormat("status: {0}", status);
				}
				catch (Exception ex)
				{

					logger.Error(ex);
				}

				try
				{
					logger.InfoFormat("headers: {0}", string.Join(System.Environment.NewLine, headers.Select(s => string.Format("key: {0}, value: {1}", s.Key, s.Value)).ToArray()));
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

				//extract JSESSIONID
				if (Regex.IsMatch(headers["Set-Cookie"], @"JSESSIONID=(\w+);{1}"))
				{

					newHeaders.Add("Cookie", "JSESSIONID=" + Regex.Match(headers["Set-Cookie"], @"JSESSIONID=(\w+);{1}").Groups[1].Value);
					result = true;
				}
				else
				{
					result = false;
				}
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				result = false;
			}

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_GreenTomato, "Login");
			#endregion

			return result;
		}

		private GreenTomato_contact GetContact(string firstName, string lastName, string customerAccountNumber, string phone, Dictionary<string, string> requestHeader, string email)
		{
			GreenTomato_contact contact = null;

			GreenTomato_CreateContactRS rs = GetContact(customerAccountNumber, phone, requestHeader, email);
			if (rs.rows.Any())
			{
				//contact exist
				contact = rs.rows[0];
				logger.InfoFormat("Get a contact. FirstName={0}, LastName={1}, Phone={2}, customerAccountNumber={3}, Id={4}, Email={5}", contact.firstName, contact.surname, contact.mobilePhone, contact.customerAccountNumber, contact.id, contact.email);
			}
			else
			{
				//create contact
				var result = CreateContact(firstName, lastName, phone, customerAccountNumber, requestHeader, email);
				if (result.rows.Any())
				{
					contact = result.rows[0];
					logger.InfoFormat("Create a contact. FirstName={0}, LastName={1}, Phone={2}, customerAccountNumber={3}, Id={4}, Email={5}", contact.firstName, contact.surname, contact.mobilePhone, contact.customerAccountNumber, contact.id, contact.email);
				}
				else
				{
					contact = null;
					logger.Warn("Unable to get/create this contact");
				}
			}

			return contact;
		}

		private GreenTomato_ServiceListRS GetService(string customerAccountNumber, Dictionary<string, string> headers, string count)
		{
			GreenTomato_ServiceListRS response = null;
			try
			{
				string status = "";
				var getServiceRQ = new GreenTomato_ServiceListRQ();
				getServiceRQ.startIndex = "0";
				getServiceRQ.count = count;
				getServiceRQ.customerAccountNumber = customerAccountNumber;
				var strRQ = getServiceRQ.JsonSerialize();
				int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["GreenTomatoAPI_Https_Timeout"]);
				//string serviceResult = WebRequestHelper.ProcessWebRequest("https://80.87.31.28/ext-api/api/services/list", strRQ, "application/json", "POST", string.Empty, string.Empty, 3000, headers, out status);
				string serviceResult = WebRequestHelper.ProcessWebRequest(GetHttpURL(ConfigurationManager.AppSettings["GreenTomatoAPI_URL_GetService"]), strRQ, "application/json", "POST", string.Empty, string.Empty, timeout, headers, out status);
				response = serviceResult.JsonDeserialize<GreenTomato_ServiceListRS>();
			}
			catch (Exception ex)
			{

				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				response = null;

			}
			return response;
		}

		private GreenTomato_CreateContactRS GetContact(string customerAccountNumber, string phone, Dictionary<string, string> headers, string email)
		{
			GreenTomato_CreateContactRS response = null;

			try
			{
				var getContactRQ = new GreenTomato_GetContactRQ();
				string status = "";
				getContactRQ.startIndex = "0";
				getContactRQ.count = "1";
				getContactRQ.customerAccountNumber = customerAccountNumber;
				getContactRQ.phone = phone;
				getContactRQ.email = email;
				var strCreateContactRQ = getContactRQ.JsonSerialize();
				int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["GreenTomatoAPI_Https_Timeout"]);
				//string contactResult = WebRequestHelper.ProcessWebRequest("https://80.87.31.28/ext-api/api/contacts/list", strCreateContactRQ, "application/json", "POST", string.Empty, string.Empty, 3000, headers, out status);
				string contactResult = WebRequestHelper.ProcessWebRequest(GetHttpURL(ConfigurationManager.AppSettings["GreenTomatoAPI_URL_GetContact"]), strCreateContactRQ, "application/json", "POST", string.Empty, string.Empty, timeout, headers, out status);
				response = contactResult.JsonDeserialize<GreenTomato_CreateContactRS>();
			}
			catch (Exception ex)
			{

				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				response = null;

			}
			return response;

		}

		private GreenTomato_CreateContactRS CreateContact(string firstName, string lastName, string phone, string customerAccountNumber, Dictionary<string, string> headers, string email)
		{
			GreenTomato_CreateContactRS response = null;

			try
			{
				string status = "";
				var createContactRQ = new GreenTomato_CreateContactRQ();
				createContactRQ.firstName = firstName;
				createContactRQ.surname = lastName;
				createContactRQ.customerAccountNumber = customerAccountNumber;
				createContactRQ.mobilePhone = phone;
				createContactRQ.email = email;
				var strCreateContactRQ = createContactRQ.JsonSerialize();
				//string contactResult = WebRequestHelper.ProcessWebRequest("https://80.87.31.28/ext-api/api/contacts/update", strCreateContactRQ, "application/json", "POST", string.Empty, string.Empty, 3000, headers, out status);
				string contactResult = WebRequestHelper.ProcessWebRequest(GetHttpURL(ConfigurationManager.AppSettings["GreenTomatoAPI_URL_CreateContact"]), strCreateContactRQ, "application/json", "POST", string.Empty, string.Empty, 3000, headers, out status);
				response = contactResult.JsonDeserialize<GreenTomato_CreateContactRS>();
			}
			catch (Exception ex)
			{

				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				response = null;

			}
			return response;

		}



		private string GetFlightInfomation(string code, string flightNumber)
		{
			string result = null;
			if (!string.IsNullOrWhiteSpace(code) && !string.IsNullOrWhiteSpace(flightNumber))
			{
				result = string.Format("{0} {1}", code, flightNumber);
			}
			else if (!string.IsNullOrWhiteSpace(code) && string.IsNullOrWhiteSpace(flightNumber))
			{
				result = string.Format("{0}", code);
			}
			else if (string.IsNullOrWhiteSpace(code) && !string.IsNullOrWhiteSpace(flightNumber))
			{
				result = string.Format("{0}", flightNumber);
			}
			else
			{
				result = null;
			}



			return result;
		}


		private string BuildBookingRequest(ECarBookingParameter ecbp, string bookerId, string serviceId, GreenTomato_rowsForReferenceRQ refType, Dictionary<string, string> headers)
		{
			string jsonString = "";

			GreenTomato_BookingRQ bookingRQ = new GreenTomato_BookingRQ();

			//ASAP(now) or PICKUP(future trip)
			if (ecbp.PickupNow)
			{
				//ASAP
				bookingRQ.bookingDateType = GreenTomato_BookingDateType.ASAP;
			}
			else
			{
				//PICKUP
				bookingRQ.bookingDateType = GreenTomato_BookingDateType.PICKUP;
				//bookingRQ.bookingDate = System.DateTime.Now.AddDays(2).ToString("dd/MM/yyyy HH:mm");
				//bookingRQ.bookingDate = System.DateTime.UtcNow.FromUtc(tbp.Fleet.ServerUTCOffset).ToString("dd/MM/yyyy HH:mm");
				bookingRQ.bookingDate = ecbp.PickupDateTime.ToString("dd/MM/yyyy HH:mm");
			}

			//Second Reference (Agent trip ID)
			if (!string.IsNullOrEmpty(ecbp.ConsumerConfirmationID))
			{
				bookingRQ.secondNumber = ecbp.ConsumerConfirmationID;
			}

			//phone 
			bookingRQ.phone = ecbp.PhoneNumber;

			//customer account number
			bookingRQ.customerAccountNumber = ecbp.User.AccountNumber;

			//additionalInstructions
			//ToDo: for zTrip we need to add type of payments
			// ...
			bookingRQ.additionalInstructions = CreateAdditionalInstructions(ecbp.Trip.ecar_trip.RemarkForPickup, ecbp.Trip.ecar_trip.RemarkForDropOff, ecbp.PickupAddress != null ? ecbp.PickupAddress.AddressName : null, ecbp.DropOffAddress != null ? ecbp.DropOffAddress.AddressName : null, ecbp.Gratuity, ecbp.numberOfPassenger.ToString());

			//pickup - address
			if (ecbp.PickupAddressType == AddressType.Address)
			{
				GreenTomato_stopRecord pickup = new GreenTomato_stopRecord();
				pickup.address = ecbp.PickupAddress.FullAddress;
				pickup.type = GreenTomato_StopRecordType.PICKUP;
				pickup.postcode = ecbp.PickupAddress.ZipCode;
				if ((ecbp.PickupAddress.Geolocation.Latitude == 0) && (ecbp.PickupAddress.Geolocation.Longitude == 0))
				{
					pickup.latitude = null;
					pickup.longitude = null;
				}
				else
				{
					pickup.latitude = ecbp.PickupAddress.Geolocation.Latitude.ToString();
					pickup.longitude = ecbp.PickupAddress.Geolocation.Longitude.ToString();
				}
				bookingRQ.stopRecordList.Add(pickup);
			}
			else if (ecbp.PickupAddressType == AddressType.Airport)
			{
				GreenTomato_stopRecord pickup = new GreenTomato_stopRecord();
				GreenTomato_GetAirportInfoRS airportInfoRS = GetAirportInfo(ecbp.PickupAirport, headers);
				if (airportInfoRS != null && airportInfoRS.rows != null && airportInfoRS.rows.Any() && (GreenTomato_rowsForAirportInfoRS)airportInfoRS.rows[0] != null)
				{
					GreenTomato_rowsForAirportInfoRS rowInfo = (GreenTomato_rowsForAirportInfoRS)airportInfoRS.rows[0];
					pickup.address = rowInfo.address;
					pickup.type = GreenTomato_StopRecordType.PICKUP;
					pickup.postcode = null;
					pickup.latitude = null;
					pickup.longitude = null;
					pickup.specialPlaceId = rowInfo.specialPlaceId;
					pickup.specialPlaceType = rowInfo.specialPlaceType;
					pickup.specialPlacePointId = null;
					//pickup.flightNumber = !string.IsNullOrWhiteSpace(ecbp.PickupAirlineFlightNumber)?ecbp.PickupAirlineFlightNumber:null;
					pickup.flightNumber = GetFlightInfomation(ecbp.PickupAirlineCode, ecbp.PickupAirlineFlightNumber);
					pickup.scheduledLandingDate = ecbp.PickupAirlineFlightDateTime;
					pickup.holdOffTimeInMinutes = null;
					pickup.airportMeetingType = GetGreenTomatoAirportMeetingType(ecbp.Relation.DispatchServiceId);
					bookingRQ.stopRecordList.Add(pickup);
					logger.InfoFormat("Pickup airportcode={0}, address={1}", ecbp.PickupAirport, rowInfo.address);
				}
				else
				{
					throw new ValidationException("Unknown address type. Unable to recognize airport code");
				}
			}
			else
			{
				throw new ValidationException("Unknown address type");
			}


			//dropoff - adress
			if (ecbp.DropOffAddress != null)
			{
				GreenTomato_stopRecord dropoff = new GreenTomato_stopRecord();
				dropoff.address = ecbp.DropOffAddress.FullAddress;
				dropoff.type = GreenTomato_StopRecordType.DROP;
				dropoff.postcode = ecbp.DropOffAddress.ZipCode;
				if ((ecbp.DropOffAddress.Geolocation.Latitude == 0) && (ecbp.DropOffAddress.Geolocation.Longitude == 0))
				{
					dropoff.latitude = null;
					dropoff.longitude = null;
				}
				else
				{
					dropoff.latitude = ecbp.DropOffAddress.Geolocation.Latitude.ToString();
					dropoff.longitude = ecbp.DropOffAddress.Geolocation.Longitude.ToString();
				}
				bookingRQ.stopRecordList.Add(dropoff);
			}
			else if (ecbp.DropOffAddressType == AddressType.Airport)
			{
				GreenTomato_stopRecord dropOff = new GreenTomato_stopRecord();
				GreenTomato_GetAirportInfoRS airportInfoRS = GetAirportInfo(ecbp.DropoffAirport, headers);
				if (airportInfoRS != null && airportInfoRS.rows != null && airportInfoRS.rows.Any() && (GreenTomato_rowsForAirportInfoRS)airportInfoRS.rows[0] != null)
				{
					GreenTomato_rowsForAirportInfoRS rowInfo = (GreenTomato_rowsForAirportInfoRS)airportInfoRS.rows[0];
					dropOff.address = rowInfo.address;
					dropOff.type = GreenTomato_StopRecordType.DROP;
					dropOff.postcode = null;
					dropOff.latitude = null;
					dropOff.longitude = null;
					dropOff.specialPlaceId = rowInfo.specialPlaceId;
					dropOff.specialPlaceType = rowInfo.specialPlaceType;
					dropOff.specialPlacePointId = null;
					//dropOff.flightNumber = !string.IsNullOrWhiteSpace(ecbp.DropOffAirlineFlightNumber) ? ecbp.DropOffAirlineFlightNumber : null; ;
					dropOff.flightNumber = GetFlightInfomation(ecbp.DropOffAirlineCode, ecbp.DropOffAirlineFlightNumber);
					dropOff.scheduledLandingDate = ecbp.DropOffAirlineFlightDateTime;
					dropOff.holdOffTimeInMinutes = null;
					dropOff.airportMeetingType = GetGreenTomatoAirportMeetingType(ecbp.Relation.DispatchServiceId); ;
					bookingRQ.stopRecordList.Add(dropOff);
					logger.InfoFormat("Drop off airportcode={0}, address={1}", ecbp.DropoffAirport, rowInfo.address);
				}
				else
				{
					throw new ValidationException("Unknown address type. Unable to recognize airport code");
				}
			}
			//else
			//{
			//    throw new Exception("Unknown address type");
			//}

			//passenger record 
			GreenTomato_passengerRecord passenger = new GreenTomato_passengerRecord();
			passenger.fullName = string.Format("{0} {1}", ecbp.Trip.ecar_trip.FirstName, ecbp.Trip.ecar_trip.LastName);
			passenger.mobilePhone = ecbp.Trip.ecar_trip.PhoneNumber;
			passenger.landlinePhone = ecbp.Trip.ecar_trip.PhoneNumber;
			passenger.email = ecbp.emailAddress;


			bookingRQ.passengerRecordList.Add(passenger);
			bookingRQ.email = ecbp.emailAddress;
			bookingRQ.phone = ecbp.Trip.ecar_trip.PhoneNumber;
			bookingRQ.additionalPassengerCount = ecbp.numberOfPassenger <= 1 ? (0).ToString() : (ecbp.numberOfPassenger - 1).ToString();
			bookingRQ.controllerNotes = ecbp.controllerNotes;

			//bookerId serviceId
			bookingRQ.bookerId = bookerId;
			//bookingRQ.serviceId = serviceId;
			bookingRQ.serviceId = GetGreenTomatoServiceId(ecbp.Relation.DispatchServiceId);

			//reference type
			bookingRQ.referenceRecordList.Add(new GreenTomato_referenceRecord { type = refType.name, value = "VTODAPI" });

			//Rate
			if (!string.IsNullOrWhiteSpace(ecbp.Rate))
			{
				bookingRQ.rateLineRecordList = new List<GreenTomato_rateLineRecord>();


				//var totalPrice = CalculateTotalPrice(ecbp.Gratuity, ecbp.Rate);
				bookingRQ.rateLineRecordList.Add(new GreenTomato_rateLineRecord { rate = ecbp.Rate, templateId = GetGreenTomatoFareTemplateID(ecbp.Relation.DispatchServiceId) });
				if (!string.IsNullOrWhiteSpace(ecbp.Gratuity))
				{
					// bookingRQ.rateLineRecordList.Add(new GreenTomato_rateLineRecord { rate = null, templateId = GetGreenTomatoGratuityTemplateID(ecbp.Relation.DispatchServiceId), units = ecbp.Gratuity.Contains("%") ? ecbp.Gratuity : ecbp.Gratuity.Replace("%", "").ToString() });

					if (!ecbp.Gratuity.Contains("%"))
					{
						//Fixed
						bookingRQ.rateLineRecordList.Add(new GreenTomato_rateLineRecord { rate = ecbp.Gratuity.ToString(), templateId = GetGreenTomatoGratuityTemplateID(ecbp.Relation.DispatchServiceId), units = null });
					}
					else
					{

						//Percent
						bookingRQ.rateLineRecordList.Add(new GreenTomato_rateLineRecord { rate = null, templateId = GetGreenTomatoGratuityTemplateID(ecbp.Relation.DispatchServiceId), units = ecbp.Gratuity.Replace("%", "").ToString() });
					}
				}
			}


			//Commission
			if (!string.IsNullOrWhiteSpace(ecbp.commission))
			{
				if (bookingRQ.rateLineRecordList == null || !bookingRQ.rateLineRecordList.Any())
				{
					bookingRQ.rateLineRecordList = new List<GreenTomato_rateLineRecord>();
				}
				bookingRQ.rateLineRecordList.Add(new GreenTomato_rateLineRecord { rate = ecbp.commission.ToString(), templateId = GetGreenTomatoCommissionTemplateID(ecbp.Relation.DispatchServiceId), units = null });
			}


			jsonString = bookingRQ.JsonSerialize();


			return jsonString;
		}






		private GreenTomato_GetAirportInfoRS GetAirportInfo(string airportCode, Dictionary<string, string> headers)
		{
			GreenTomato_GetAirportInfoRS response = null;

			try
			{
				GreenTomato_GetAirportInfoRQ rq = new GreenTomato_GetAirportInfoRQ();
				rq.count = "30";
				rq.startIndex = "0";
				rq.query = airportCode.ToUpperInvariant();
				rq.specialPlaceType = "AIRPORT";
				string rqString = rq.JsonSerialize();
				string status = "";

				//string result = WebRequestHelper.ProcessWebRequest("https://80.87.31.28/ext-api/api/addresses/list", rqString, "application/json", "POST", string.Empty, string.Empty, 3000, headers, out status);
				string result = WebRequestHelper.ProcessWebRequest(GetHttpURL(ConfigurationManager.AppSettings["GreenTomatoAPI_URL_GetAirportInfo"]), rqString, "application/json", "POST", string.Empty, string.Empty, 3000, headers, out status);
				response = result.JsonDeserialize<GreenTomato_GetAirportInfoRS>();
			}
			catch (Exception ex)
			{

				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				response = null;

			}

			return response;

		}

		private GreenTomato_ReferenceTypeRS GetReferenceType(string customerAccountNumber, Dictionary<string, string> headers)
		{
			GreenTomato_ReferenceTypeRS response = null;

			try
			{
				GreenTomato_ReferenceTypeRQ rq = new GreenTomato_ReferenceTypeRQ();
				rq.count = "30";
				rq.startIndex = "0";
				rq.customerAccountNumber = customerAccountNumber;
				string rqString = rq.JsonSerialize();
				string status = "";

				//string result = WebRequestHelper.ProcessWebRequest("https://80.87.31.28/ext-api/api/references/types", rqString, "application/json", "POST", string.Empty, string.Empty, 3000, headers, out status);
				string result = WebRequestHelper.ProcessWebRequest(GetHttpURL(ConfigurationManager.AppSettings["GreenTomatoAPI_URL_GetReferenceType"]), rqString, "application/json", "POST", string.Empty, string.Empty, 3000, headers, out status);
				response = result.JsonDeserialize<GreenTomato_ReferenceTypeRS>();
			}
			catch (Exception ex)
			{

				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				response = null;

			}

			return response;
		}

		/*private GreenTomato_GetVehicleInfoForStatusRS GetVehicleInfoForStautus(string vehicleId, Dictionary<string, string> headers)
		{
			GreenTomato_GetVehicleInfoForStatusRS response = null;

			try
			{
				string status = "";
				//string url = string.Format("https://80.87.31.28/ext-api/api/vehicle/getById/{0}",vehicleId);
				string url = string.Format("{0}/{1}", GetHttpURL(ConfigurationManager.AppSettings["GreenTomatoAPI_URL_GetVehicleInfoForStatus"]), vehicleId);
				string result = WebRequestHelper.ProcessWebRequest(url, string.Empty, "application/json", "GET", string.Empty, string.Empty, 3000, headers, out status);
				response = result.JsonDeserialize<GreenTomato_GetVehicleInfoForStatusRS>();
			}
			catch (Exception ex)
			{

				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				response = null;

			}

			return response;
		}*/

		private string GetGreenTomatoServiceId(string dispatchServiceId)
		{
			string result = null;

			if (dispatchServiceId.ToLowerInvariant().Contains("serviceid".ToLowerInvariant()))
			{
				string[] sep = { "," };
				List<string> parameters = dispatchServiceId.Split(sep, StringSplitOptions.None).ToList();

				foreach (string para in parameters)
				{
					if (Regex.IsMatch(para, "serviceid=([0-9]+)", RegexOptions.IgnoreCase))
					{
						result = Regex.Match(para, "ServiceID=([0-9]+)", RegexOptions.IgnoreCase).Groups[1].Value.Trim();
					}
				}
			}
			else
			{
				throw new Exception("Wrong dispatch service ID confiruration. check ServiceID part");
			}

			return result;

		}

		private string GetGreenTomatoCommissionTemplateID(string dispatchServiceId)
		{
			string result = null;
			if (dispatchServiceId.ToLowerInvariant().Contains("CommissionTemplateId".ToLowerInvariant()))
			{
				string[] sep = { "," };
				List<string> parameters = dispatchServiceId.Split(sep, StringSplitOptions.None).ToList();

				foreach (string para in parameters)
				{
					if (Regex.IsMatch(para, " CommissionTemplateId=(.*)", RegexOptions.IgnoreCase))
					{
						result = Regex.Match(para, " CommissionTemplateId=(.*)", RegexOptions.IgnoreCase).Groups[1].Value.Trim();
					}
				}
			}
			else
			{
				//throw new Exception("Wrong dispatch service ID confiruration. check airportMeetingType part");
				result = null;
			}

			return result;
		}

		private string GetGreenTomatoGratuityTemplateID(string dispatchServiceId)
		{
			string result = null;
			if (dispatchServiceId.ToLowerInvariant().Contains("GratuityTemplateId".ToLowerInvariant()))
			{
				string[] sep = { "," };
				List<string> parameters = dispatchServiceId.Split(sep, StringSplitOptions.None).ToList();

				foreach (string para in parameters)
				{
					if (Regex.IsMatch(para, " GratuityTemplateId=(.*)", RegexOptions.IgnoreCase))
					{
						result = Regex.Match(para, " GratuityTemplateId=(.*)", RegexOptions.IgnoreCase).Groups[1].Value.Trim();
					}
				}
			}
			else
			{
				//throw new Exception("Wrong dispatch service ID confiruration. check airportMeetingType part");
				result = null;
			}

			return result;
		}

		private string GetGreenTomatoFareTemplateID(string dispatchServiceId)
		{
			string result = null;
			if (dispatchServiceId.ToLowerInvariant().Contains("FareTemplateId".ToLowerInvariant()))
			{
				string[] sep = { "," };
				List<string> parameters = dispatchServiceId.Split(sep, StringSplitOptions.None).ToList();

				foreach (string para in parameters)
				{
					if (Regex.IsMatch(para, " FareTemplateId=(.*)", RegexOptions.IgnoreCase))
					{
						result = Regex.Match(para, " FareTemplateId=(.*)", RegexOptions.IgnoreCase).Groups[1].Value.Trim();
					}
				}
			}
			else
			{
				//throw new Exception("Wrong dispatch service ID confiruration. check airportMeetingType part");
				result = null;
			}

			return result;
		}


		private string GetGreenTomatoAirportMeetingType(string dispatchServiceId)
		{
			string result = null;
			if (dispatchServiceId.ToLowerInvariant().Contains("airportMeetingType".ToLowerInvariant()))
			{
				string[] sep = { "," };
				List<string> parameters = dispatchServiceId.Split(sep, StringSplitOptions.None).ToList();

				foreach (string para in parameters)
				{
					if (Regex.IsMatch(para, "airportMeetingType=(.*)", RegexOptions.IgnoreCase))
					{
						result = Regex.Match(para, "airportMeetingType=(.*)", RegexOptions.IgnoreCase).Groups[1].Value.Trim();
					}
				}
			}
			else
			{
				//throw new Exception("Wrong dispatch service ID confiruration. check airportMeetingType part");
				result = null;
			}

			return result;
		}

		private string GetGreenTomatoExpiredDateTime(string dispatchServiceId)
		{
			string result = null;

			if (dispatchServiceId.ToLowerInvariant().Contains("Expired".ToLowerInvariant()))
			{
				string[] sep = { "," };
				List<string> parameters = dispatchServiceId.Split(sep, StringSplitOptions.None).ToList();

				foreach (string para in parameters)
				{
					if (Regex.IsMatch(para, "expired=(.*)", RegexOptions.IgnoreCase))
					{
						string[] format = { "MM/dd/yyyy HH:mm:ss" };
						string tempResult = Regex.Match(para, "expired=(.*)", RegexOptions.IgnoreCase).Groups[1].Value.Trim();
						DateTime dt = new DateTime();
						if (DateTime.TryParseExact(tempResult, format, new CultureInfo("en-US"), DateTimeStyles.None, out dt))
						{
							result = tempResult;
						}
						else
						{
							throw new Exception("Unable to extract expired date from dispatchServiceId. Invalid date string.");
						}
					}
				}
			}
			else
			{
				throw new Exception("Wrong dispatch service ID confiruration. check ServiceID part");
			}

			return result;

		}


		private string GetHttpURL(string URI)
		{
			return string.Format("https://{0}{1}", ConfigurationManager.AppSettings["GreenTomatoAPI_DomainName"], URI);
		}


		private OTA_GroundAvailRS GetPrice(ECarGetEstimationParameter ecgep, Dictionary<string, string> headers)
		{
			OTA_GroundAvailRS result = null;
			GreenTomato_GetPriceRQ getPriceRQ = new GreenTomato_GetPriceRQ();

			GreenTomato_ReferenceTypeRS refType = GetReferenceType(ecgep.User.AccountNumber, headers);
			GreenTomato_contact contact = GetContact(ecgep.FirstName, ecgep.LastName, ecgep.User.AccountNumber, ecgep.PhoneNumber, headers, ecgep.Email);
			GreenTomato_ServiceListRS services = GetService(ecgep.User.AccountNumber, headers, "30");

			//firstname and lastname
			ecgep.FirstName = "VTOD";
			ecgep.LastName = "VTOD";
			ecgep.PhoneNumber = "6262190800";

			//phone 
			getPriceRQ.phone = "6262190800";


			//Booking Date Type
			getPriceRQ.bookingDateType = GreenTomato_StopRecordType.PICKUP;

			//Booking Date
			getPriceRQ.bookingDate = DateTime.Now.ToString("MM/dd/yyyy HH:mm");

			//customer account number
			getPriceRQ.customerAccountNumber = ecgep.User.AccountNumber;

			//pickup - address
			if (ecgep.PickupAddressType == AddressType.Address)
			{
				GreenTomato_stopRecord pickup = new GreenTomato_stopRecord();
				pickup.address = ecgep.PickupAddress.FullAddress;
				pickup.type = GreenTomato_StopRecordType.PICKUP;
				pickup.postcode = ecgep.PickupAddress.ZipCode;
				pickup.latitude = ecgep.PickupAddress.Geolocation.Latitude.ToString();
				pickup.longitude = ecgep.PickupAddress.Geolocation.Longitude.ToString();
				getPriceRQ.stopRecordList.Add(pickup);
			}
			else if (ecgep.PickupAddressType == AddressType.Airport)
			{
				GreenTomato_stopRecord pickup = new GreenTomato_stopRecord();
				GreenTomato_GetAirportInfoRS airportInfoRS = GetAirportInfo(ecgep.PickupAirport, headers);
				if (airportInfoRS != null && airportInfoRS.rows != null && airportInfoRS.rows.Any() && (GreenTomato_rowsForAirportInfoRS)airportInfoRS.rows[0] != null)
				{
					GreenTomato_rowsForAirportInfoRS rowInfo = (GreenTomato_rowsForAirportInfoRS)airportInfoRS.rows[0];
					pickup.address = rowInfo.address;
					pickup.type = GreenTomato_StopRecordType.PICKUP;
					pickup.postcode = null;
					pickup.latitude = null;
					pickup.longitude = null;
					pickup.specialPlaceId = rowInfo.specialPlaceId;
					pickup.specialPlaceType = rowInfo.specialPlaceType;
					pickup.specialPlacePointId = null;
					pickup.flightNumber = null;
					pickup.scheduledLandingDate = null;
					pickup.holdOffTimeInMinutes = null;

					//string amtType = utility.GetGreenTomatoAirportMeetingType(ecgep.?.DispatchServiceId);
					string amtType = string.Empty;
					if (!string.IsNullOrWhiteSpace(amtType))
					{
						pickup.airportMeetingType = amtType;
					}

					getPriceRQ.stopRecordList.Add(pickup);
					logger.InfoFormat("Pickup airportcode={0}, address={1}", ecgep.PickupAirport, rowInfo.address);
				}
				else
				{
					throw new ValidationException("Unknown address type. Unable to recognize airport code");
				}
			}
			else
			{
				throw new ValidationException("Unknown address type");
			}


			//dropoff - adress
			if (ecgep.DropOffAddress != null)
			{
				GreenTomato_stopRecord dropoff = new GreenTomato_stopRecord();
				dropoff.address = ecgep.DropOffAddress.FullAddress;
				dropoff.type = GreenTomato_StopRecordType.DROP;
				dropoff.postcode = ecgep.DropOffAddress.ZipCode;
				dropoff.latitude = ecgep.DropOffAddress.Geolocation.Latitude.ToString();
				dropoff.longitude = ecgep.DropOffAddress.Geolocation.Longitude.ToString();
				getPriceRQ.stopRecordList.Add(dropoff);
			}
			else if (ecgep.DropOffAddressType == AddressType.Airport)
			{
				GreenTomato_stopRecord dropOff = new GreenTomato_stopRecord();
				GreenTomato_GetAirportInfoRS airportInfoRS = GetAirportInfo(ecgep.DropoffAirport, headers);
				if (airportInfoRS != null && airportInfoRS.rows != null && airportInfoRS.rows.Any() && (GreenTomato_rowsForAirportInfoRS)airportInfoRS.rows[0] != null)
				{
					GreenTomato_rowsForAirportInfoRS rowInfo = (GreenTomato_rowsForAirportInfoRS)airportInfoRS.rows[0];
					dropOff.address = rowInfo.address;
					dropOff.type = GreenTomato_StopRecordType.DROP;
					dropOff.postcode = null;
					dropOff.latitude = null;
					dropOff.longitude = null;
					dropOff.specialPlaceId = rowInfo.specialPlaceId;
					dropOff.specialPlaceType = rowInfo.specialPlaceType;
					dropOff.specialPlacePointId = null;
					dropOff.flightNumber = null;
					dropOff.scheduledLandingDate = null;
					dropOff.holdOffTimeInMinutes = null;

					//dropOff.airportMeetingType =  utility.GetGreenTomatoAirportMeetingType(ecgep.re.DispatchServiceId);
					//string amtType = utility.GetGreenTomatoAirportMeetingType(ecgep.re.DispatchServiceId);
					string amtType = string.Empty;
					if (!string.IsNullOrWhiteSpace(amtType))
					{
						dropOff.airportMeetingType = amtType;
					}


					getPriceRQ.stopRecordList.Add(dropOff);
					logger.InfoFormat("Drop off airportcode={0}, address={1}", ecgep.DropoffAirport, rowInfo.address);
				}
				else
				{
					throw new ValidationException("Unknown address type. Unable to recognize airport code");
				}
			}
			//else
			//{
			//    throw new Exception("Unknown address type");
			//}

			//additional instructions
			getPriceRQ.additionalInstructions = string.Empty;

			//passenger record 
			GreenTomato_passengerRecord passenger = new GreenTomato_passengerRecord();
			passenger.fullName = string.Format("{0} {1}", ecgep.FirstName, ecgep.LastName);
			passenger.mobilePhone = ecgep.PhoneNumber;
			passenger.landlinePhone = ecgep.PhoneNumber;
			getPriceRQ.passengerRecordList.Add(passenger);

			//bookerId serviceId
			//getPriceRQ.bookerId = bookerId;
			getPriceRQ.bookerId = contact.id;

			//reference type

			getPriceRQ.referenceRecordList.Add(new GreenTomato_referenceRecord { type = refType.rows[0].name, value = "VTODAPI" });


			foreach (var s in services.rows)
			{

				//get price
				GreenTomato_GetPriceRS response = null;
				try
				{
					getPriceRQ.serviceId = s.id;
					string rqString = getPriceRQ.JsonSerialize();
					string status = "";

					string rsResult = WebRequestHelper.ProcessWebRequest(GetHttpURL(ConfigurationManager.AppSettings["GreenTomatoAPI_URL_GetPrice"]), rqString, "application/json", "POST", string.Empty, string.Empty, 3000, headers, out status);
					response = rsResult.JsonDeserialize<GreenTomato_GetPriceRS>();

					if (response != null)
					{
						if (result == null)
						{
							result = new OTA_GroundAvailRS();
							result.GroundServices = new GroundServiceList();
							result.GroundServices.GroundServices = new List<GroundService>();
						}

						GroundService gs = new GroundService();


						#region reference ID and pickupTime
						Reference r = new Reference();
						r.TPA_Extensions = new TPA_Extensions();
						r.TPA_Extensions.PickupTimes = new PickupTimeList();
						r.TPA_Extensions.PickupTimes.PickupTimes = new List<PickupTime>();

						//pikcup time
						ecgep.PickupDateTime = DateTime.Now;
						string pkTime = ecgep.PickupDateTime.ToString("MM/dd/yyyy HH:mm:ss");
						PickupTime pt = new PickupTime { Status = "AllowPickup", StartDateTime = pkTime, EndDateTime = pkTime };

						//get expired time for fleet
						int ExpiredBookingHrs = 4;
						if (ecgep.Fleet.ExpiredBookingHrs.HasValue)
						{
							ExpiredBookingHrs = ecgep.Fleet.ExpiredBookingHrs.Value;
						}

						string expired = System.DateTime.Now.AddHours(ExpiredBookingHrs).ToString("MM/dd/yyyy HH:mm:ss");
						string refString = string.Format("serviceID={0}, airportMeetingType={1}, expired={2}", s.id, string.Empty, expired);


						//ref ID, and encrypt
						r.ID = UDI.Utility.Helper.Cryptography.EncryptDES(refString, ConfigurationManager.AppSettings["Encryption_Key"], ConfigurationManager.AppSettings["Encryption_IV"]);
						r.TPA_Extensions.PickupTimes.PickupTimes.Add(pt);
						gs.Reference = r;
						#endregion

						#region Service
						gs.Service = new Service { ServiceLevel = new ServiceLevel { Code = s.id, SourceName = "GTC" }, VehicleType = new UDI.VTOD.Common.DTO.OTA.VehicleType { Code = "ECAR" } };
						#endregion


						#region RateQualifer (Code, description)
						Category c = new Category { Code = "Fare", Description = s.name };
						c.Value = "Other_";
						gs.RateQualifier = new RateQualifier { Category = c };
						#endregion




						#region Total Charge
						TotalCharge tc = new TotalCharge();
						string tpString = response.rows[0].totalPrice;
						decimal tpValue = 0;
						if (decimal.TryParse(tpString, out tpValue))
						{
							#region ServiceCharges
							gs.ServiceCharges = new List<ServiceCharges>();
							ServiceCharges sc = new ServiceCharges { Description = "FirstPassengerFare", ChargePurpose = new ChargePurpose { Value = "BaseRate" } };
							sc.Calculations = new List<Calculation>();
							Calculation cc = new Calculation();
							cc.Quantity = 1;
							cc.MaxQuantity = 1; //this might change in future
							cc.Total = tpValue * cc.Quantity;
							cc.UnitCharge = tpValue;
							sc.Calculations.Add(cc);
							gs.ServiceCharges.Add(sc);
							#endregion

							#region calcute gratuity
							if (!string.IsNullOrWhiteSpace(ecgep.Gratuity))
							{
								//	var g = ecgep.Gratuity;

								//	decimal gratuityOperand = 1;
								//	string gratuityOperator = g.Contains("%") ? "%" : "+";
								//	string gratuityOperandString = g.Replace("%", "");
								//	decimal p = Convert.ToDecimal(0.01);

								//	if (decimal.TryParse(gratuityOperandString, out gratuityOperand))
								//	{
								//		if (gratuityOperator == "%")
								//		{
								//			tpValue = tpValue + (tpValue * p * gratuityOperand);
								//		}
								//		else
								//		{
								//			tpValue = tpValue + gratuityOperand;
								//		}
								//	}
								//	else
								//	{
								//		//unable to calcute Gratuity
								//		logger.WarnFormat("unable to calcute Gratuity. gratuityOperandString={0}", gratuityOperandString);
								//	}
								tpValue = utility.CalculateTotalPrice(ecgep.Gratuity, tpValue.ToString()).ToDecimal();
							}
							#endregion

							tc.RateTotalAmount = tpValue;
							tc.EstimatedTotalAmount = tpValue;
							gs.TotalCharge = tc;

						}
						else
						{
							throw new ECarException(string.Format("totalPrice={0}, we are not able to conver this to decimal.", tpString));
						}
						#endregion




						result.GroundServices.GroundServices.Add(gs);
					}

				}
				catch (Exception ex)
				{

					logger.ErrorFormat("Message:{0}", ex.Message);
					logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
					logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
					response = null;
				}
			}

			return result;
		}




		private string CreateAdditionalInstructions(string puRemark, string doRemark, string puLocation, string doLocation, string gratuity, string numberOfPassenger)
		{
			var result = new StringBuilder();

			result.Append(string.Empty);

			if (!string.IsNullOrWhiteSpace(puRemark))
			{
				result.AppendFormat(" PU Remark: {0},", puRemark);
			}

			if (!string.IsNullOrWhiteSpace(doRemark))
			{
				result.AppendFormat(" DO Remark: {0},", doRemark);
			}

			if (!string.IsNullOrWhiteSpace(puLocation))
			{
				result.AppendFormat(" PU Address Special Info : {0},", puLocation);
			}

			if (!string.IsNullOrWhiteSpace(doLocation))
			{
				result.AppendFormat(" DO Address Special Info : {0},", doLocation);
			}

			if (!string.IsNullOrWhiteSpace(gratuity))
			{
				result.AppendFormat(" Gratuity: {0},", gratuity);
			}

			if (!string.IsNullOrWhiteSpace(numberOfPassenger))
			{
				result.AppendFormat(" Number of Passenger: {0},", numberOfPassenger);
			}

			return result.ToString();

		}
		//public TokenRS TokenVTOD
		//{
		//	set
		//	{
		//		tokenVTOD = value;
		//	}
		//}

		#endregion

	}

}
