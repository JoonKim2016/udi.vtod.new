﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Domain.ECar.Class;
using UDI.VTOD.Domain.Taxi.Class;
using UDI.VTOD.DataAccess.VTOD;
namespace UDI.VTOD.Domain.ECar
{
    interface IECarService
    {
        #region New Service API interface
		OTA_GroundBookRS Book(TokenRS tokenVTOD, OTA_GroundBookRQ request, out List<Tuple<string, int, long>> disptach_Rez_VTOD, ECarBookingParameter ecbp);
 
        OTA_GroundBookRS ModifyBook(TokenRS tokenVTOD, OTA_GroundBookRQ request, out List<Tuple<string, int, long>> disptach_Rez_VTOD, ECarBookingParameter ecbp);

        OTA_GroundResRetrieveRS Status(TokenRS tokenVTOD, OTA_GroundResRetrieveRQ request, ECarStatusParameter ecsp);

        OTA_GroundCancelRS Cancel(TokenRS tokenVTOD, OTA_GroundCancelRQ request, ECarCancelParameter eccp);

        OTA_GroundAvailRS GetVehicleInfo(TokenRS tokenVTOD, OTA_GroundAvailRQ request, ECarGetVehicleInfoParameter ecgvip);
       

        OTA_GroundAvailRS GetEstimation(TokenRS tokenVTOD, OTA_GroundAvailRQ request, ECarGetEstimationParameter ecgep);

		Common.DTO.OTA.Fleet GetAvailableFleets(TokenRS token, UtilityAvailableFleetsRQ input);

        Common.DTO.OTA.Fleet GetLeadTime(TokenRS token, UtilityGetLeadTimeRQ input);
        Common.DTO.OTA.UtilityGetClientTokenRS GetCorporateClientToken(Common.DTO.OTA.TokenRS token, Common.DTO.OTA.UtilityGetClientTokenRQ input);
      
        bool NotifyPickupDriver(TokenRS token, UtilityNotifyDriverRQ input);
        bool NotifyDriver(TokenRS token, UtilityNotifyDriverRQ input);
        bool GetDisabilityInd(TokenRS tokenVTOD, ecar_fleet f, out int condition, string authenticateEndpoint, string bookingEndpoint);
        OTA_GroundGetFinalRouteRS GetFinalRoute(TokenRS tokenVTOD, OTA_GroundGetFinalRouteRQ request);
        #endregion
    }
}
