﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Core.Metadata.Edm;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using log4net;
using UDI.Map;
using UDI.Map.Adapter.MapQuest.DTO;
using UDI.SDS;
using UDI.SDS.RoutingService;
using UDI.Utility.DTO.Enum;
using UDI.Utility.Helper;
using UDI.Utility.Serialization;
using UDI.VTOD.Common.DTO;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.Helper;
using UDI.VTOD.DataAccess.VTOD;
using UDI.VTOD.Domain.ECar.Class;
using UDI.VTOD.Domain.ECar.Const;
using UDI.VTOD.MTData.AuthenticationWebService;
using UDI.VTOD.MTData.BookingWebService;
using Address = UDI.Map.DTO.Address;
using AddressType = UDI.VTOD.Domain.ECar.Const.AddressType;
using AddressWS = UDI.VTOD.MTData.AddressWebService;
using BookingChannelType = UDI.VTOD.MTData.AuthenticationWebService.BookingChannelType;
using Fleet = UDI.VTOD.MTData.BookingWebService.Fleet;
using Location = UDI.VTOD.MTData.BookingWebService.Location;
using LocationType = UDI.VTOD.MTData.BookingWebService.LocationType;
using UDI.VTOD.MTData.OSIWebService;
using System.ServiceModel;
using UDI.SDS.PaymentsService;
using UDI.VTOD.Common.Track;
using UDI.VTOD.Domain.Taxi.Const;

namespace UDI.VTOD.Domain.ECar.MTData_Derived
{

   public class TexasEcarService : IECarService
    {
        protected readonly ILog logger;
        protected readonly ECarUtility utility;
        protected readonly int FleetTripCode;
        private static string Token = string.Empty;
        private static readonly object ObjSync = new object();
        private static readonly string UserName = ConfigurationManager.AppSettings["MTData_Username"];
        private static readonly string Password = ConfigurationManager.AppSettings["MTData_Password"];
        private static readonly string SystemID = ConfigurationManager.AppSettings["Texas_SystemID"];
        private static readonly string IVR_UserName = ConfigurationManager.AppSettings["MTData_IVR_Username"];
        private static readonly string IVR_Password = ConfigurationManager.AppSettings["MTData_IVR_Password"];
        private static readonly string IVR_SystemID = ConfigurationManager.AppSettings["MTData_IVR_SystemID"];
        private static readonly string TexasSystemID = ConfigurationManager.AppSettings["Texas_SystemID"];

        public TexasEcarService(ECarUtility util, ILog log)
        {
            this.logger = log;
            this.utility = util;
        }

        #region Public Methods
        public TexasEcarService(ECarUtility util, ILog log, int fleetTripCode) 
        {
            this.logger = log;
            this.utility = util;
            this.FleetTripCode = fleetTripCode;
        }
        /// <summary>
        /// Retrieving a charge estimate
        /// </summary>
        /// <param name="tokenVTOD"></param>
        /// <param name="request"></param>
        /// <param name="ecgep"></param>
        /// <returns></returns>
        public OTA_GroundAvailRS GetEstimation(TokenRS tokenVTOD, OTA_GroundAvailRQ request,ECarGetEstimationParameter ecgep)
        {
            logger.Info("Start");
            Stopwatch sw = new Stopwatch();
            sw.Start();
            Stopwatch swEachPart = new Stopwatch();
            var response = new OTA_GroundAvailRS();

            #region For as directed, return empty ground service
            if (request.Service.Dropoff == null)
            {
                response.Success = new Success();
                response.GroundServices = new GroundServiceList
                {
                    GroundServices = new List<GroundService> { new GroundService() }

                };
                return response;
            }
            #endregion

            #region Validate required fields
            ECarGetEstimationParameter tgep;
            if (!utility.ValidateTexasGetEstimationRequest(tokenVTOD, request, out tgep))
            {
                string error = string.Format("This GetEstimation request is invalid.");
                throw new ValidationException(error);
            }

            #endregion

            try
            {
                if (!string.IsNullOrWhiteSpace(tgep.EstimationType) &&
                    tgep.EstimationType == Const.EstimationResultType.Info_FlatRate)   // for flat rate, just return the rate from VTOD DB
                {
                    #region Build response
                    response.Success = new Success();
                    response.GroundServices = new GroundServiceList
                    {
                        GroundServices = new List<GroundService>
                            {
                                new GroundService
                                {

                                     RateQualifier = new RateQualifier
                                    {
                                        Category =
                                            new Category { Value = "Other_", Code = FareType.FlatRate.ToString() },
                                        SpecialInputs = new List<NameValue>
                                        {
                                            new NameValue { Name = "Pickup", Value = tgep.FlatRateEstimation.PickupZone },
                                            new NameValue { Name = "DropOff", Value = tgep.FlatRateEstimation.DropoffZone }
                                        }
                                    },
                                    TotalCharge = new TotalCharge
                                    {
                                        EstimatedTotalAmount = (decimal)tgep.FlatRateEstimation.EstimationTotalAmount,
                                        RateTotalAmount = (decimal)tgep.FlatRateEstimation.EstimationTotalAmount
                                    }

                                }
                            }
                    };
                    #endregion
                }
                else  // non flat rate, call MTData API
                {
                    //var fleetID = int.Parse(tgep.Fleet.CCSiFleetId);

                    #region convert address to MTData address

                    var mtdataAddresses = new List<AddressWS.Address>();
                    string mapAddressesErrMsg;

                    #region pickup

                    //if (tgep.PickupAddressType == AddressType.Airport)
                    //{
                    //	// airport code mapping
                    //	var airportCodes = utility.GetSynonyms(tgep.PickupAirport);
                    //	if (airportCodes != null && airportCodes.Any(m => m != tgep.PickupAirport))
                    //	{
                    //		tgep.PickupAirport = airportCodes.FirstOrDefault(m => m != tgep.PickupAirport);
                    //	}

                    //	// get place id of pickup airport
                    //	var quickLocation = DoGetFleetQuickLocations(fleetID);
                    //	if (quickLocation.Succeeded &&
                    //		quickLocation.FleetQuickLocationCollection.Any(
                    //			m => m.PlaceName.ToLower() == tgep.PickupAirport.ToLower()))
                    //	{
                    //		var place =
                    //			quickLocation.FleetQuickLocationCollection.FirstOrDefault(
                    //				m => m.PlaceName.ToLower() == tgep.PickupAirport.ToLower());

                    //		var mtdataAddr = new AddressWS.Address
                    //		{
                    //			AddressType = AddressWS.AddressType.Place,
                    //			Place = place == null ? null : new AddressWS.Place { ID = place.PlaceID }
                    //		};
                    //		mtdataAddresses.Add(mtdataAddr);
                    //	}
                    //	else
                    //	{
                    //		string error = string.Format("Cannot find matching airport. message={0}",
                    //			quickLocation.ErrorMessage);
                    //		logger.Error(error);
                    //		throw new ECarException(error);
                    //	}
                    //}
                    if (tgep.PickupAddressType == AddressType.Airport)
                    {
                        // airport code mapping
                        List<string> airportFullNames = utility.GetSynonyms(tgep.PickupAirport);

                        var quickLocation = DoGetFleetQuickLocations(tgep.Fleet, tgep.AuthenticationServiceEndpointName, tgep.BookingWebServiceEndpointName);
                        if (quickLocation.Succeeded && quickLocation.FleetQuickLocationCollection.Any())
                        {
                            var place = quickLocation.FleetQuickLocationCollection.FirstOrDefault(m => m.PlaceName.ToLower().Trim() == tgep.PickupAirport.ToLower().Trim());

                            if (place == null)
                            {
                                place = quickLocation.FleetQuickLocationCollection.FirstOrDefault(m => airportFullNames.Contains(m.PlaceName.ToLower().Trim()));
                            }

                            if (place != null)
                            {
                                var mtdataAddr = new AddressWS.Address
                                {
                                    AddressType = AddressWS.AddressType.Place,
                                    Place = new AddressWS.Place { ID = place.PlaceID }
                                };

                                #region Adding lat/lon
                                if (tgep.PickupAddress!=null && tgep.PickupAddress.Geolocation != null && tgep.PickupAddress.Geolocation.Latitude != 0 && tgep.PickupAddress.Geolocation.Longitude != 0)
                                {
                                    mtdataAddr.Latitude = (double)tgep.PickupAddress.Geolocation.Latitude;
                                    mtdataAddr.Longitude = (double)tgep.PickupAddress.Geolocation.Longitude;
                                }
                                #endregion

                                mtdataAddresses.Add(mtdataAddr);
                            }
                            else
                            {
                                string error = string.Format("Cannot find matching airport. message={0}",
                                    quickLocation.ErrorMessage);
                                logger.Error(error);
                                throw new ECarException(error);
                            }

                        }
                        else
                        {
                            string error = string.Format("Cannot find matching airport. message={0}",
                                quickLocation.ErrorMessage);
                            logger.Error(error);
                            throw new ECarException(error);
                        }
                    }
                    else
                    {
                        //var mapPickupAddress = ConvertLatLongToAddress(fleetID,
                        //    (double)tgep.PickupAddress.Geolocation.Latitude,
                        //    (double)tgep.PickupAddress.Geolocation.Longitude, out mapAddressesErrMsg);

                        var mapPickupAddress = ConvertLatLongToAddress(tgep.Fleet,
                            (double)tgep.PickupAddress.Geolocation.Latitude,
                            (double)tgep.PickupAddress.Geolocation.Longitude, out mapAddressesErrMsg, tgep.AuthenticationServiceEndpointName, tgep.AddressWebServiceEndpointName);

                        if (mapPickupAddress == null)
                        {
                            string error = string.Format("Cannot find matching address. message={0}", mapAddressesErrMsg);
                            logger.Error(error);
                            throw new ECarException(error);
                        }

                        // Since ConvertLatLongToAddress method might change our street number, especially for address range,
                        // we replace it with original street number
                        if (!string.IsNullOrWhiteSpace(tgep.PickupAddress.StreetNo))
                            mapPickupAddress.Number = tgep.PickupAddress.StreetNo;
                        mapPickupAddress.Latitude = (double)tgep.PickupAddress.Geolocation.Latitude;
                        mapPickupAddress.Longitude = (double)tgep.PickupAddress.Geolocation.Longitude;

                        if (System.Configuration.ConfigurationManager.AppSettings["MTData_Force_ZipCode"].ToBool())
                        {
                            if (mapPickupAddress.Street != null && string.IsNullOrWhiteSpace(mapPickupAddress.Street.Postcode))
                            {
                                mapPickupAddress.Street.Postcode = tgep.PickupAddress.ZipCode;
                            }

                            if (mapPickupAddress.Suburb != null && string.IsNullOrWhiteSpace(mapPickupAddress.Suburb.Postcode))
                            {
                                mapPickupAddress.Suburb.Postcode = tgep.PickupAddress.ZipCode;
                            }
                        }

                        mtdataAddresses.Add(mapPickupAddress);
                    }

                    #endregion

                    #region dropoff

                    if (tgep.DropOffAddressType == AddressType.Airport)
                    {
                        // airport code mapping
                        List<string> airportFullNames = utility.GetSynonyms(tgep.DropoffAirport);

                        // get place id of pickup airport
                        var quickLocation = DoGetFleetQuickLocations(tgep.Fleet, tgep.AuthenticationServiceEndpointName, tgep.BookingWebServiceEndpointName);
                        if (quickLocation.Succeeded && quickLocation.FleetQuickLocationCollection.Any())
                        {
                            var place = quickLocation.FleetQuickLocationCollection.FirstOrDefault(m => m.PlaceName.ToLower().Trim() == tgep.DropoffAirport.ToLower().Trim());

                            if (place == null)
                            {
                                place = quickLocation.FleetQuickLocationCollection.FirstOrDefault(m => airportFullNames.Contains(m.PlaceName.ToLower().Trim()));
                            }

                            if (place != null)
                            {
                                var mtdataAddr = new AddressWS.Address
                                {
                                    AddressType = AddressWS.AddressType.Place,
                                    Place = new AddressWS.Place { ID = place.PlaceID }
                                };

                                #region Adding lat/lon
                                if (tgep.DropOffAddress!=null && tgep.DropOffAddress.Geolocation != null && tgep.DropOffAddress.Geolocation.Latitude != 0 && tgep.DropOffAddress.Geolocation.Longitude != 0)
                                {
                                    mtdataAddr.Latitude = (double)tgep.DropOffAddress.Geolocation.Latitude;
                                    mtdataAddr.Longitude = (double)tgep.DropOffAddress.Geolocation.Longitude;
                                }
                                #endregion

                                mtdataAddresses.Add(mtdataAddr);
                            }
                            else
                            {
                                string error = string.Format("Cannot find matching airport. message={0}",
                                    quickLocation.ErrorMessage);
                                logger.Error(error);
                                throw new ECarException(error);
                            }

                        }
                        else
                        {
                            string error = string.Format("Cannot find matching airport. message={0}",
                                quickLocation.ErrorMessage);
                            logger.Error(error);
                            throw new ECarException(error);
                        }
                    }
                    else if (tgep.DropOffAddress != null)
                    {
                        var mapDropOffAddress = ConvertLatLongToAddress(tgep.Fleet,
                            (double)tgep.DropOffAddress.Geolocation.Latitude,
                            (double)tgep.DropOffAddress.Geolocation.Longitude, out mapAddressesErrMsg, tgep.AuthenticationServiceEndpointName, tgep.AddressWebServiceEndpointName);

                        if (mapDropOffAddress == null)
                        {
                            string error = string.Format("Cannot find matching address. message={0}",
                                mapAddressesErrMsg);
                            logger.Error(error);
                            throw new ECarException(error);
                        }

                        // Since ConvertLatLongToAddress method might change our street number, especially for address range,
                        // we replace it with original street number
                        if (!string.IsNullOrWhiteSpace(tgep.DropOffAddress.StreetNo))
                            mapDropOffAddress.Number = tgep.DropOffAddress.StreetNo;
                        mapDropOffAddress.Latitude = (double)tgep.DropOffAddress.Geolocation.Latitude;
                        mapDropOffAddress.Longitude = (double)tgep.DropOffAddress.Geolocation.Longitude;

                        if (System.Configuration.ConfigurationManager.AppSettings["MTData_Force_ZipCode"].ToBool())
                        {
                            if (mapDropOffAddress.Street != null && string.IsNullOrWhiteSpace(mapDropOffAddress.Street.Postcode))
                            {
                                mapDropOffAddress.Street.Postcode = tgep.DropOffAddress.ZipCode;
                            }

                            if (mapDropOffAddress.Suburb != null && string.IsNullOrWhiteSpace(mapDropOffAddress.Suburb.Postcode))
                            {
                                mapDropOffAddress.Suburb.Postcode = tgep.DropOffAddress.ZipCode;
                            }
                        }

                        mtdataAddresses.Add(mapDropOffAddress);
                    }

                    #endregion

                    #endregion

                    #region setup request

                    var pickupAddress = ConvertAddress(mtdataAddresses[0]);
                    var dropoffAddress = ConvertAddress(mtdataAddresses[1]);

                    var settings = new ChargeEstimateSettings
                    {
                        Fleet = new Fleet { ID = Convert.ToInt32(tgep.Fleet.CCSiFleetId) },
                        Time =tgep.PickupDateTime,
                        PickUpAddressType = pickupAddress.AddressType,
                        PickUpAddress = pickupAddress,
                        DropOffAddressType = dropoffAddress.AddressType,
                        DropOffAddress = dropoffAddress
                    };

                    #endregion

                    swEachPart.Restart();

                    var result = DoGetChargeEstimate(settings, tgep.Fleet, tgep.AuthenticationServiceEndpointName, tgep.BookingWebServiceEndpointName);

                    swEachPart.Stop();
                    logger.DebugFormat("Call MTData API to get estimation. Duration:{0} ms.", sw.ElapsedMilliseconds);

                    if (TrackTime != null)
                        TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Texas, "Book_SendingGetEstimationRequest");

                    #region handle response

                    if (result.Succeeded)
                    {
                        response.Success = new Success();
                        Reference r = new Reference();
                        int ExpiredBookingHrs = 4;
                        if (tgep.Fleet.ExpiredBookingHrs.HasValue)
                        {
                            ExpiredBookingHrs = tgep.Fleet.ExpiredBookingHrs.Value;
                        }

                        string expired = System.DateTime.Now.AddHours(ExpiredBookingHrs).ToString("MM/dd/yyyy HH:mm:ss");
                        string refString = string.Format("serviceID={0}, FleetTripCode={1}, expired={2}", ECarServiceAPIConst.Texas, UDI.VTOD.Common.DTO.Enum.FleetTripCode.Ecar_Texas, expired);
                        //ref ID, and encrypt
                        r.ID = UDI.Utility.Helper.Cryptography.EncryptDES(refString, ConfigurationManager.AppSettings["Encryption_Key"], ConfigurationManager.AppSettings["Encryption_IV"]);
                        response.GroundServices = new GroundServiceList
                        {
                            GroundServices = new List<GroundService>
                            {
                                new GroundService
                                {
                                    RateQualifier = new RateQualifier
                                    {
                                        Category =
                                            new Category {Value = "Other_", Code = FareType.Estimation.ToString(),Description="Black Car Sedan (4 Pax)"}
                                    },
                                    Reference=r,
                                    TotalCharge = new TotalCharge
                                    {
                                        EstimatedTotalAmount = result.ChargeExcludingTolls,
                                        RateTotalAmount = result.ChargeExcludingTolls
                                    }
                                }
                            }
                        };

                        if (TrackTime != null)
                            TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Texas,
                                "Book_SendingGetEstimationRequest");
                    }
                    else
                    {
                        logger.Error(result.ErrorMessage);
                        throw new ECarException(result.ErrorMessage);
                    }

                    #endregion
                }

                #region Write ECar Log
                utility.WriteECarLog(null, ECarServiceAPIConst.Texas, ECarServiceMethodType.Estimation, null, null, null, null, null);
                #endregion
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Message:{0}", ex.Message);
                logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
                logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
                throw;
            }
            sw.Stop();
            logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
            return response;
        }

        public  OTA_GroundGetFinalRouteRS GetFinalRoute(TokenRS tokenVTOD, OTA_GroundGetFinalRouteRQ request)
        {
            DateTime requestDateTime = DateTime.Now;
            ecar_trip trip = null;
            List<ecar_trip_status> ecar_trip_status = new List<ecar_trip_status>();
            logger.Info("Start");
            Stopwatch sw = new Stopwatch();
            string status = ConfigurationManager.AppSettings["MTData_GetFinalRoute_Status"];
            Int64 tripID;
            sw.Start();
            var response = new OTA_GroundGetFinalRouteRS();
            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
            #endregion

            try
            {
                if (request.Reference != null && request.Reference.Any())
                {
                    if (request.Reference.Where(x => x.Type.Equals("Confirmation")).Any())
                    {
                        tripID = Convert.ToInt64(request.Reference.Where(x => x.Type.Equals("Confirmation")).First().ID);
                        #region Retrieve the trip details
                        using (var context = new VTODEntities())
                        {
                            trip = context.ecar_trip.Where(
                                   x =>
                                       x.Id == tripID
                               ).FirstOrDefault();

                        }
                        #endregion
                    }
                    else if (request.Reference.Where(x => x.Type.Equals("DispatchConfirmation")).Any())
                    {
                        Int64 dispatchtripid = Convert.ToInt64(request.Reference.Where(x => x.Type.Equals("DispatchConfirmation")).First().ID);
                        #region Retrieve the trip details
                        using (var context = new VTODEntities())
                        {
                            trip = context.ecar_trip.Where(
                                   x =>
                                       x.DispatchTripId == dispatchtripid.ToString()
                               ).FirstOrDefault();

                        }
                        #endregion
                    }
                }
                #region Retrieve the trip status details
                using (var context = new VTODEntities())
                {
                    ecar_trip_status = context.ecar_trip_status.Where(x => x.TripID == trip.Id && !status.Contains(x.Status)).OrderBy(p => p.Id).ToList();
                }
                #endregion
                if (ecar_trip_status != null && ecar_trip_status.Any())
                {
                    #region Populate the Geolocations
                    response.Geolocations = new List<Geolocation>();
                    foreach (var ecar_status in ecar_trip_status)
                    {
                        Geolocation geo = new Geolocation();
                        geo.Lat = ecar_status.VehicleLatitude.ToDecimal();
                        geo.Long = ecar_status.VehicleLongitude.ToDecimal();
                        response.Geolocations.Add(geo);
                    }
                    #endregion
                }
                DateTime responseDateTime = DateTime.Now;
                #region Write ECar Log
                if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
                {
                    var requestMessage = request.ToString();
                    var responseMessage = response.ToString();

                    utility.WriteECarLog(null, ECarServiceAPIConst.Texas, ECarServiceMethodType.GetFinalRoute, null,
                        requestMessage, responseMessage, requestDateTime, responseDateTime);
                }
                #endregion

            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Message:{0}", ex.Message);
                logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
                logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
                throw new ECarException(ex.Message);
            }
            sw.Stop();
            logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Texas, "GetFinalRoute");
            #endregion

            return response;
        }
        public  bool GetDisabilityInd(TokenRS tokenVTOD, ecar_fleet f, out int condition, string authenticateEndpoint, string bookingEndpoint)
        {
            string bookingCondition = ConfigurationManager.AppSettings["MTData_Booking_Condition"];
            bool result = false;
            condition = 0;
            logger.Info("Start");
            Stopwatch sw = new Stopwatch();
            sw.Start();
            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
            #endregion
            var quickLocation = DoGetFleet(f, authenticateEndpoint, bookingEndpoint);
            if (quickLocation.Succeeded && quickLocation.Fleets.Any())
            {
                var fleets = quickLocation.Fleets.Where(p => p.ID == f.CCSiFleetId.ToInt32()).FirstOrDefault();
                if (fleets != null && fleets.BookingConditions.Any())
                {
                    var bookCondition = fleets.BookingConditions.Where(p => p.Name == bookingCondition).FirstOrDefault();
                    if (bookCondition != null)
                    {
                        condition = bookCondition.ID;
                        result = true;
                    }
                }

            }
            sw.Stop();
            logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Texas, "GetDisabilityInd");
            #endregion
            return result;
        }
        public OTA_GroundBookRS Book(TokenRS tokenVTOD, OTA_GroundBookRQ request, out List<Tuple<string, int, long>> disptach_Rez_VTOD, ECarBookingParameter ecbp)
        {
            logger.Info("Start");
            Stopwatch sw = new Stopwatch();
            sw.Start();
            Stopwatch swEachPart = new Stopwatch();
            OTA_GroundBookRS response = null;
            disptach_Rez_VTOD = new List<Tuple<string, int, long>>();
            DateTime requestDateTime = DateTime.Now;
            bool ind = false;
            int condition = 0;
            #region validation
            ECarBookingParameter tbp;
            int FleetTripCode=UDI.VTOD.Common.DTO.Enum.FleetTripCode.Ecar_Texas;
            if (!utility.ValidateTexasBookingRequest(tokenVTOD, request, out tbp, ecbp.Fleet, FleetTripCode) || tbp.Fleet == null)
            {
                throw VtodException.CreateException(ExceptionType.ECar, 2007);
            }
            #endregion

            #region Accessibility
            if (request != null && request.GroundReservations != null && request.GroundReservations.Any())
            {
                if (request.GroundReservations.FirstOrDefault().Service != null && request.GroundReservations.FirstOrDefault().Service.DisabilityVehicleInd == true)
                {

                    ind = GetDisabilityInd(tokenVTOD, tbp.Fleet, out condition, tbp.AuthenticationServiceEndpointName, tbp.BookingWebServiceEndpointName);
                }
            }
            #endregion
            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
            #endregion

            DateTime? requestedLocalTime = null;

            requestedLocalTime = tbp.PickupDateTime;

            try
            {
                #region format request data
                #region replace the street number in pickup address
                if (tbp.PickupAddress != null && !string.IsNullOrWhiteSpace(tbp.PickupAddress.StreetNo)
                            && tbp.PickupAddress.StreetNo.ToArray().Where(x => x.Equals('–')).Any())
                {
                    logger.InfoFormat("The orginial pickup street No. is {0}", tbp.PickupAddress.StreetNo);
                    string[] sep = { "–" };
                    string[] streetNumbers = tbp.PickupAddress.StreetNo.Split(sep, StringSplitOptions.RemoveEmptyEntries);
                    tbp.PickupAddress.StreetNo = streetNumbers[0];
                    tbp.Trip.ecar_trip.RemarkForPickup += string.Format(" {0} Customer on {1} block of {2}",
                        tbp.Trip.ecar_trip.RemarkForPickup, tbp.PickupAddress.StreetNo, tbp.PickupAddress.Street);
                    logger.InfoFormat("Remove range street number in pickup address. Pickup street No. is {0}",
                        tbp.PickupAddress.StreetNo);
                    logger.InfoFormat("Pickup Comment: {0}", tbp.Trip.ecar_trip.RemarkForPickup);
                    if (TrackTime != null)
                        TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Texas, "Book_ReplacePickupStreetNumberRange");
                }
                #endregion

                #region replace the street number in drop address
                if (tbp.DropOffAddress != null && !string.IsNullOrWhiteSpace(tbp.DropOffAddress.StreetNo) &&
                            tbp.DropOffAddress.StreetNo.ToArray().Where(x => x.Equals('–')).Any())
                {
                    logger.InfoFormat("The orginial dropoff street No. is {0}", tbp.DropOffAddress.StreetNo);
                    string[] sep = { "–" };
                    string[] streetNumbers = tbp.DropOffAddress.StreetNo.Split(sep,
                        StringSplitOptions.RemoveEmptyEntries);
                    tbp.DropOffAddress.StreetNo = streetNumbers[0];
                    logger.InfoFormat("Remove range street number in dropoff address. Dropoff street No. is {0}",
                        tbp.DropOffAddress.StreetNo);
                    if (TrackTime != null)
                        TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Texas, "Book_ReplaceDropoffStreetNumberRange");
                }
                #endregion

                #region replace zipCode with 5 digits zipcode only
                //replace pickup zipCode with 5 digits zipcode only
                if (tbp.PickupAddress != null && Regex.IsMatch(tbp.PickupAddress.ZipCode, @"([\d]+)\-[\d]+"))
                {
                    logger.InfoFormat("Original pickup zipCode: {0}", tbp.PickupAddress.ZipCode);
                    tbp.PickupAddress.ZipCode =
                        Regex.Match(tbp.PickupAddress.ZipCode, @"([\d]+)\-[\d]+", RegexOptions.IgnoreCase).Groups[1]
                            .Value;
                    logger.InfoFormat("Replace: {0}", tbp.PickupAddress.ZipCode);
                    if (TrackTime != null)
                        TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Texas, "Book_ReplacePickupZipCode");
                }

                //replace dropoff zipCode with 5 digits zipcode only
                if (tbp.DropOffAddress != null && Regex.IsMatch(tbp.DropOffAddress.ZipCode, @"([\d]+)\-[\d]+"))
                {
                    logger.InfoFormat("Original dropoff zipCode: {0}", tbp.DropOffAddress.ZipCode);
                    tbp.DropOffAddress.ZipCode =
                        Regex.Match(tbp.DropOffAddress.ZipCode, @"([\d]+)\-[\d]+", RegexOptions.IgnoreCase).Groups[1]
                            .Value;
                    logger.InfoFormat("Replace: {0}", tbp.DropOffAddress.ZipCode);
                    if (TrackTime != null)
                        TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Texas, "Book_ReplaceDropoffZipCode");
                }
                #endregion


                #endregion

                #region convert address to MTData address

                var mtdataAddresses = new List<AddressWS.Address>();

                #region pickup
                if (tbp.PickupAddressType == AddressType.Airport)
                {
                    // airport code mapping
                    var quickLocation = DoGetFleetQuickLocations(tbp.Fleet, tbp.AuthenticationServiceEndpointName, tbp.BookingWebServiceEndpointName);
                    if (quickLocation.Succeeded && quickLocation.FleetQuickLocationCollection.Any())
                    {
                        var place = quickLocation.FleetQuickLocationCollection.FirstOrDefault(m => m.PlaceName.ToLower().Trim() == tbp.PickupAirport.ToLower().Trim());

                        if (place == null)
                        {
                            List<string> airportFullNames = utility.GetSynonyms(tbp.PickupAirport);

                            place = quickLocation.FleetQuickLocationCollection.FirstOrDefault(m => airportFullNames.Contains(m.PlaceName.ToLower().Trim()));
                        }

                        if (place != null)
                        {
                            var mtdataAddr = new AddressWS.Address
                            {
                                AddressType = AddressWS.AddressType.Place,
                                Place = new AddressWS.Place { ID = place.PlaceID }
                            };

                            #region Adding lat/lon
                            if (place != null && place.Latitude != 0 && place.Longitude != 0)
                            {
                                mtdataAddr.Latitude = place.Latitude;
                                mtdataAddr.Longitude = place.Longitude;
                            }
                            else if (tbp.DropOffAddress.Geolocation != null && tbp.DropOffAddress.Geolocation.Latitude != 0 && tbp.DropOffAddress.Geolocation.Longitude != 0)
                            {
                                mtdataAddr.Latitude = (double)tbp.DropOffAddress.Geolocation.Latitude;
                                mtdataAddr.Longitude = (double)tbp.DropOffAddress.Geolocation.Longitude;
                            }
                            #endregion

                            mtdataAddresses.Add(mtdataAddr);
                        }
                        else
                        {
                            string error = string.Format("Cannot find matching airport. message={0}", quickLocation.ErrorMessage);
                            logger.Error(error);
                            utility.MarkThisTripAsError(tbp.Trip.Id, quickLocation.ErrorMessage);
                            throw new ECarException(error);
                        }
                    }
                    else
                    {
                        string error = string.Format("Cannot find matching airport. message={0}", quickLocation.ErrorMessage);
                        logger.Error(error);
                        utility.MarkThisTripAsError(tbp.Trip.Id, quickLocation.ErrorMessage);
                        throw new ECarException(error);
                    }
                }
                else
                {
                    var mapPickupAddress = new AddressWS.Address();
                    mapPickupAddress.Designation = new AddressWS.Designation();
                    mapPickupAddress.Street = new AddressWS.Street { Name = tbp.PickupAddress.Street /*, Postcode = tbp.PickupAddress.ZipCode */};
                    mapPickupAddress.Number = tbp.PickupAddress.StreetNo;
                    mapPickupAddress.Suburb = new AddressWS.Suburb { Name = tbp.PickupAddress.City /*, Postcode = tbp.PickupAddress.ZipCode */};
                    //mapPickupAddress.CityName = tbp.PickupAddress.City;
                    mapPickupAddress.Latitude = (double)tbp.PickupAddress.Geolocation.Latitude;
                    mapPickupAddress.Longitude = (double)tbp.PickupAddress.Geolocation.Longitude;
                    mapPickupAddress.AddressType = AddressWS.AddressType.Street;
                    // }


                    mapPickupAddress.Latitude = (double)tbp.PickupAddress.Geolocation.Latitude;
                    mapPickupAddress.Longitude = (double)tbp.PickupAddress.Geolocation.Longitude;

                    if (System.Configuration.ConfigurationManager.AppSettings["MTData_Force_ZipCode"].ToBool())
                    {
                        if (mapPickupAddress.Street != null && string.IsNullOrWhiteSpace(mapPickupAddress.Street.Postcode))
                        {
                            mapPickupAddress.Street.Postcode = tbp.PickupAddress.ZipCode;
                        }

                        if (mapPickupAddress.Suburb != null && string.IsNullOrWhiteSpace(mapPickupAddress.Suburb.Postcode))
                        {
                            mapPickupAddress.Suburb.Postcode = tbp.PickupAddress.ZipCode;
                        }
                    }

                    mtdataAddresses.Add(mapPickupAddress);
                }
                #endregion

                #region dropoff
                if (tbp.DropOffAddressType == AddressType.Airport)
                {
                    // airport code mapping
                    var quickLocation = DoGetFleetQuickLocations(tbp.Fleet, tbp.AuthenticationServiceEndpointName, tbp.BookingWebServiceEndpointName);
                    if (quickLocation.Succeeded && quickLocation.FleetQuickLocationCollection.Any())
                    {
                        var place = quickLocation.FleetQuickLocationCollection.FirstOrDefault(m => m.PlaceName.ToLower().Trim() == tbp.DropoffAirport.ToLower().Trim());

                        if (place == null)
                        {
                            List<string> airportFullNames = utility.GetSynonyms(tbp.DropoffAirport);
                            place = quickLocation.FleetQuickLocationCollection.FirstOrDefault(m => airportFullNames.Contains(m.PlaceName.ToLower().Trim()));
                        }

                        if (place != null)
                        {
                            var mtdataAddr = new AddressWS.Address
                            {
                                AddressType = AddressWS.AddressType.Place,
                                Place = new AddressWS.Place { ID = place.PlaceID, Name = place.Name }
                            };

                            #region Adding lat/lon
                            if (place != null && place.Latitude != 0 && place.Longitude != 0)
                            {
                                mtdataAddr.Latitude = place.Latitude;
                                mtdataAddr.Longitude = place.Longitude;
                            }
                            else if (tbp.DropOffAddress.Geolocation != null && tbp.DropOffAddress.Geolocation.Latitude != 0 && tbp.DropOffAddress.Geolocation.Longitude != 0)
                            {
                                mtdataAddr.Latitude = (double)tbp.DropOffAddress.Geolocation.Latitude;
                                mtdataAddr.Longitude = (double)tbp.DropOffAddress.Geolocation.Longitude;
                            }
                            #endregion

                            mtdataAddresses.Add(mtdataAddr);
                        }
                        else
                        {
                            string error = string.Format("Cannot find matching airport. message={0}", quickLocation.ErrorMessage);
                            logger.Error(error);
                            utility.MarkThisTripAsError(tbp.Trip.Id, quickLocation.ErrorMessage);
                            throw new ECarException(error);
                        }
                    }
                    else
                    {
                        string error = string.Format("Cannot find matching airport. message={0}", quickLocation.ErrorMessage);
                        logger.Error(error);
                        utility.MarkThisTripAsError(tbp.Trip.Id, quickLocation.ErrorMessage);
                        throw new ECarException(error);
                    }
                }
                else if (tbp.DropOffAddress != null)
                {
                    var mapDropOffAddress = new AddressWS.Address();
                    mapDropOffAddress.Designation = new AddressWS.Designation();
                    mapDropOffAddress.Street = new AddressWS.Street { Name = tbp.DropOffAddress.Street/*, Postcode = tbp.DropOffAddress.ZipCode */};
                    mapDropOffAddress.Number = tbp.DropOffAddress.StreetNo;
                    mapDropOffAddress.Suburb = new AddressWS.Suburb { Name = tbp.DropOffAddress.City/*, Postcode = tbp.DropOffAddress.ZipCode */};
                    //mapDropOffAddress.CityName = tbp.DropOffAddress.City;
                    mapDropOffAddress.Latitude = (double)tbp.DropOffAddress.Geolocation.Latitude;
                    mapDropOffAddress.Longitude = (double)tbp.DropOffAddress.Geolocation.Longitude;
                    mapDropOffAddress.AddressType = AddressWS.AddressType.Street;
                    //}


                    mapDropOffAddress.Latitude = (double)tbp.DropOffAddress.Geolocation.Latitude;
                    mapDropOffAddress.Longitude = (double)tbp.DropOffAddress.Geolocation.Longitude;

                    if (System.Configuration.ConfigurationManager.AppSettings["MTData_Force_ZipCode"].ToBool())
                    {
                        if (mapDropOffAddress.Street != null && string.IsNullOrWhiteSpace(mapDropOffAddress.Street.Postcode))
                        {
                            mapDropOffAddress.Street.Postcode = tbp.DropOffAddress.ZipCode;
                        }

                        if (mapDropOffAddress.Suburb != null && string.IsNullOrWhiteSpace(mapDropOffAddress.Suburb.Postcode))
                        {
                            mapDropOffAddress.Suburb.Postcode = tbp.DropOffAddress.ZipCode;
                        }
                    }


                    mtdataAddresses.Add(mapDropOffAddress);
                }
                #endregion                              

                #endregion


                #region setup request
                string bookingSystemID = SystemID;
                var bookingSettings = new ImportBookingSettings
                {
                    Booking = new Booking
                    {
                        ContactName = string.Format("{0} {1}", tbp.FirstName, tbp.LastName),
                        ContactPhoneNumber = tbp.PhoneNumber,
                        Fleet = new Fleet() { ID = Convert.ToInt32(tbp.Fleet.CCSiFleetId) },
                        Locations = new Location[2],
                        //Conditions = new BookingCondition[] { new BookingCondition { ID = 6 } }
                    },
                    RemoteSystemID = bookingSystemID,
                    RemoteReference = tbp.Trip.Id.ToString()
                };

                bookingSettings.Booking.Locations[0] = new Location
                {
                    Address = ConvertAddress(mtdataAddresses[0]),
                    AddressType = (UDI.VTOD.MTData.BookingWebService.AddressType)mtdataAddresses[0].AddressType,
                    CustomerName = string.Format("{0} {1}", tbp.FirstName,
                        tbp.LastName),
                    CustomerPhoneNumber = tbp.PhoneNumber,
                    CustomerEmail = "",
                    LocationType = LocationType.PickUp,
                    //Time = tbp.PickupNow ? DateTime.MinValue : tbp.PickupDateTime,
                    Time = tbp.PickupDateTime,
                    TimeMode = tbp.PickupNow ? TimeMode.NextAvailable : TimeMode.SpecificTime,
                    #region Pax
                    Pax = tbp.Trip.ecar_trip.NumberOfPassenger.HasValue ? tbp.Trip.ecar_trip.NumberOfPassenger.Value : 1,

                    #endregion

                };

                if ((bool)tbp.Fleet.IVREnable)
                {
                    bookingSettings.RemoteSystemID = IVR_SystemID;
                    bookingSettings.Booking.Locations[0].Notifications = new UDI.VTOD.MTData.BookingWebService.Notification { AllowCustomerCallOut = true, CallOnApproach = true };
                }



                if (tbp.PickupAddressType != AddressType.Airport && string.IsNullOrWhiteSpace(mtdataAddresses[0].Number))
                {
                   bookingSettings.Booking.Locations[0].SendPhoneToDriver = true;
                }
                
                bookingSettings.Booking.Locations[0].Remark = tbp.Trip.ecar_trip.RemarkForPickup;                                                                            
                                                                                                                                                                             
                bookingSettings.Booking.Locations[1] = new Location();
                if (mtdataAddresses.Count > 1)
                {
                    bookingSettings.Booking.Locations[1].Address = ConvertAddress(mtdataAddresses[1]);
                    bookingSettings.Booking.Locations[1].AddressType = (UDI.VTOD.MTData.BookingWebService.AddressType)mtdataAddresses[1].AddressType;
                    bookingSettings.Booking.Locations[1].CustomerName = string.Format("{0} {1}", tbp.FirstName,
                        tbp.LastName);
                    bookingSettings.Booking.Locations[1].CustomerPhoneNumber = tbp.PhoneNumber;
                    bookingSettings.Booking.Locations[1].CustomerEmail = "";
                    bookingSettings.Booking.Locations[1].LocationType = LocationType.DropOff;
                    if (tbp.DropOffDateTime == null)
                    {
                        bookingSettings.Booking.Locations[1].TimeMode = TimeMode.None;
                    }
                    else
                    {
                        bookingSettings.Booking.Locations[1].Time = tbp.DropOffDateTime ?? DateTime.MinValue;
                        bookingSettings.Booking.Locations[1].TimeMode = TimeMode.SpecificTime;
                    }
                    bookingSettings.Booking.Locations[1].Remark = tbp.Trip.ecar_trip.RemarkForDropOff;// string.Format(ConfigurationManager.AppSettings["MTData_Pickup_Remark"], tbp.PickupAddress.Street);

                }
                else
                {
                    bookingSettings.Booking.Locations[1].AddressType = UDI.VTOD.MTData.BookingWebService.AddressType.None;
                    bookingSettings.Booking.Locations[1].LocationType = LocationType.DropOff;
                }

                // set fixed price if needed
                if (tbp.IsFixedPrice)
                {
                    bookingSettings.Booking.Price = (int)(tbp.FixedPrice * 100);
                    bookingSettings.Booking.Options = new[] { BookingOption.FixedPrice, BookingOption.PriceRequired };
                }
                else
                {
                    bookingSettings.Booking.Options = new[] { /*BookingOption.FixedPrice,*/ BookingOption.PriceRequired };
                }

                // payment
                if (tbp.PaymentType == PaymentType.PaymentCard)
                {                    
                    bookingSettings.Booking.PaymentMethod = new PaymentMethod
                    {
                        ID = ECarMTDataNameValue.PaymentMethod_Account_ID,
                    };
                    bookingSettings.Booking.Account = new Account { AccountNumber = tbp.User.AccountNumber }; //, AccountPin = "1234" };
                }
                else if (tbp.PaymentType == PaymentType.Cash)
                {
                    bookingSettings.Booking.PaymentMethod = new PaymentMethod
                    {
                        ID = ECarMTDataNameValue.PaymentMethod_Cash_ID,
                    };
                    if (!string.IsNullOrEmpty(tbp.User.CashAccountNumber))
                    {
                        bookingSettings.Booking.Account = new Account { AccountNumber = tbp.User.CashAccountNumber };
                    }
                }
                else
                {
                    bookingSettings.Booking.PaymentMethod = new PaymentMethod
                    {
                        ID = ECarMTDataNameValue.PaymentMethod_Cash_ID
                    };
                }

                // tip info
                if (!string.IsNullOrWhiteSpace(tbp.Gratuity))
                {
                    bookingSettings.Booking.TipInfo = new TipInfo();

                    //if (tbp.Gratuity.IndexOf("%", StringComparison.Ordinal) >= 0)
                    if (tbp.Gratuity != null && !string.IsNullOrWhiteSpace(tbp.Gratuity))
                    {
                        if (tbp.Gratuity.Contains("%"))
                        {
                            bookingSettings.Booking.TipInfo.TipType = TipType.Percent;
                            bookingSettings.Booking.TipInfo.Tip = decimal.Parse(tbp.Gratuity.Replace("%", ""));
                        }
                        else
                        {
                            bookingSettings.Booking.TipInfo.TipType = TipType.Fixed;
                            bookingSettings.Booking.TipInfo.Tip = decimal.Parse(tbp.Gratuity);
                        }
                    }
                }

                #endregion

                swEachPart.Restart();

                var result = DoImportBooking(bookingSettings, ecbp.Fleet, condition, tbp);
                swEachPart.Stop();
                logger.DebugFormat("Call MTData API to book a trip. Duration:{0} ms.", sw.ElapsedMilliseconds);

                if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Texas, "Book_SendingBookingRequest");

                var responseDateTime = DateTime.Now;

                #region handle response

                if (result.Succeeded)
                {
                    string confirmationNumber = result.Booking.BookingID.ToString();
                    if (!string.IsNullOrWhiteSpace(confirmationNumber))
                    {
                        tbp.Trip = utility.UpdateECarTrip(tbp.Trip, confirmationNumber, ecbp.Fleet_TripCode);

                        if (tbp.EnableResponseAndLog)
                        {
                            GroundReservation reservation = tbp.request.GroundReservations.FirstOrDefault();
                            response = new OTA_GroundBookRS
                            {
                                EchoToken = tbp.request.EchoToken,
                                Target = tbp.request.Target,
                                Version = tbp.request.Version,
                                PrimaryLangID = tbp.request.PrimaryLangID,
                                Success = new Success(),
                                Reservations = new List<Reservation>()
                            };

                            //Reservation
                            var r = new Reservation
                            {
                                Confirmation = new Confirmation
                                {
                                    ID = tbp.Trip.Id.ToString(),
                                    Type = ConfirmationType.Confirmation.ToString()
                                },
                                Passenger = reservation.Passenger,
                                Service = reservation.Service
                            };
                            r.Confirmation.TPA_Extensions = new TPA_Extensions
                            {
                                Confirmations = new List<Confirmation>
                                {
                                    new Confirmation
                                    {
                                        ID = tbp.Trip.ecar_trip.DispatchTripId,
                                        Type = ConfirmationType.DispatchConfirmation.ToString()
                                    }
                                }
                            };
                            #region FleetInfo
                            ecar_fleet fleetObj = null;
                            using (var db = new DataAccess.VTOD.VTODEntities())
                            {
                                Int64 fleetId = 0;
                                ecar_trip tripObj = db.ecar_trip.Where(p => p.Id == tbp.Trip.Id).FirstOrDefault();
                                if (tripObj != null)
                                {
                                    fleetId = tripObj.FleetId;
                                }
                                if (fleetId > 0)
                                {
                                    fleetObj = db.ecar_fleet.Where(p => p.Id == fleetId).FirstOrDefault();
                                }

                            }
                            if (fleetObj != null)
                            {
                                r.TPA_Extensions = new TPA_Extensions();
                                r.TPA_Extensions.Contacts = new Contacts();
                                r.TPA_Extensions.Contacts.Items = new List<Contact>();
                                var contactDetails = new Contact { Type = Common.DTO.Enum.ContactType.Dispatch.ToString() };
                                if (!string.IsNullOrWhiteSpace(fleetObj.Name))
                                {
                                    contactDetails.Name = fleetObj.Name;
                                }
                                if (!string.IsNullOrWhiteSpace(fleetObj.PhoneNumber))
                                {
                                    contactDetails.Telephone = new Telephone { PhoneNumber = fleetObj.PhoneNumber };
                                }
                                r.TPA_Extensions.Contacts.Items.Add(contactDetails);
                            }
                            #endregion

                            response.Reservations.Add(r);

                            #region Update status into ecar_trip_status
                            if (tbp.IsFixedPrice)
                            {
                                utility.UpdateFinalStatus(tbp.Trip.Id, ECarTripStatusType.Booked, tbp.FixedPrice.ToDecimal(), null, null, null, null);
                            }

                            utility.SaveTripStatus(tbp.Trip.Id, ECarTripStatusType.Booked, requestedLocalTime, null, null, null,null, null, null,null, null, null,string.Empty, string.Empty);
                            
                            #endregion

                        }
                        else
                        {
                            logger.Info("Disable response and log for booking");
                        }
                    }
                    else
                    {
                        string error = string.Format("Cannot get DispatchTripId. Or dispatchTripId is white space.");
                        logger.Error(error);
                        utility.MarkThisTripAsError(tbp.Trip.Id, "BookingID is empty");
                        throw new ECarException(error);
                    }

                    if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Texas, "Book_CreateResponseObject");
                }
                else
                {
                    logger.Error(result.ErrorMessage);
                    utility.MarkThisTripAsError(tbp.Trip.Id, result.ErrorMessage);
                    throw new ECarException(result.ErrorMessage);
                }

                #endregion

                #region Write ECar Log
                if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
                {
                    var requestMessage = tbp.XmlSerialize().ToString();
                    var responseMessage = result.XmlSerialize().ToString();

                    utility.WriteECarLog(tbp.Trip.Id, ECarServiceAPIConst.Texas, ECarServiceMethodType.Book, null,
                        requestMessage, responseMessage, requestDateTime, responseDateTime);
                }
                #endregion
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Message:{0}", ex.Message);
                logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
                logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);

                //change the status of trip
                utility.UpdateFinalStatus(tbp.Trip.Id, ECarTripStatusType.Error, null, null, null, null, null);

                throw;
            }

            sw.Stop();
            logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Texas, "Book");
            #endregion

            return response;
        }

        //This is copied from MTDATA Taxi Service
        public OTA_GroundBookRS ModifyBook(TokenRS tokenVTOD, OTA_GroundBookRQ request, out List<Tuple<string, int, long>> disptach_Rez_VTOD, ECarBookingParameter ecbp)
        {            
            logger.Info("Start");
            Stopwatch sw = new Stopwatch();
            sw.Start();
            Stopwatch swEachPart = new Stopwatch();
            OTA_GroundBookRS response = null;
            disptach_Rez_VTOD = new List<Tuple<string, int, long>>();
            DateTime requestDateTime = DateTime.Now;
            bool ind = false;
            int condition = 0;
            #region validation

            var references = request.References.FirstOrDefault();
            if (references == null)
            {
                throw VtodException.CreateException(ExceptionType.ECar, 1014);
            }
            var vtodTrip = utility.GetVtodTripWithReferenceID(references.ID, references.Type.Equals(ConfirmationType.Confirmation.ToString(), StringComparison.OrdinalIgnoreCase));
            string dispatchConfirmationId = vtodTrip.ecar_trip.DispatchTripId;


            #region Fetching Endpoint Names
            List<string> endpointNames = utility.MtdataEndpointConfiguration(ecbp.serviceAPIID, ecbp.Fleet.Id);
            #endregion

            request.TPA_Extensions.PickMeUpNow = vtodTrip.PickMeUpNow;

            var bookingresult = DoGetBookingFull(Convert.ToInt32(dispatchConfirmationId), ecbp.Fleet, endpointNames[0], endpointNames[1]);
            if (!bookingresult.Succeeded)
            {
                throw VtodException.CreateException(ExceptionType.ECar, 2022);
            }


            if (!utility.ValidateTexasModifyBookingRequest(tokenVTOD, request, ecbp, bookingresult) || ecbp.Fleet == null)
            {
                throw VtodException.CreateException(ExceptionType.ECar, 2007);
            }
            #endregion

            #region Retrieve the PickUp/DropOff Remarks
            string userName = tokenVTOD.Username.ToLower().Trim();
            List<string> remarksPickUpDropoff = utility.GetRemarks(userName, ecbp.Fleet.Id);
            if (remarksPickUpDropoff != null && remarksPickUpDropoff.Any())
            {
                if (ecbp.PaymentType == PaymentType.PaymentCard)
                {
                    ecbp.RemarkForDropOff = remarksPickUpDropoff[3];
                    ecbp.RemarkForPickup = remarksPickUpDropoff[2];
                }
                else
                {
                    ecbp.RemarkForDropOff = remarksPickUpDropoff[1];
                    ecbp.RemarkForPickup = remarksPickUpDropoff[0];
                }

            }

            #endregion

            #region Adding Customer Phone#
            if (ecbp.PhoneNumber != null)
            {
                var addingText = string.Format("Customer phone: {0}.", ecbp.PhoneNumber);
                ecbp.RemarkForPickup = ecbp.RemarkForPickup.Replace("{PhoneNumber}", addingText);
                ecbp.RemarkForDropOff = ecbp.RemarkForDropOff.Replace("{PhoneNumber}", addingText);
            }
            #endregion
            #region Adding Tip to MTData
            //***********************************************************************************************************
            //**************** we are adding tip just for MTData in its domain calss: MTDataTaxiService *****************
            //**************** so if you add Tip to all Taxi in TaxiDomain calss please comment this block **************
            //***********************************************************************************************************

            var specialInputs = request.GroundReservations.First().RateQualifiers.First().SpecialInputs;
            if (specialInputs != null && specialInputs.Any())
            {
                var gratuity = specialInputs.Where(s => s.Name.ToLower().Trim() == "gratuity").FirstOrDefault();
                if (gratuity != null)
                {
                    var addingText = string.Format(" Tip is {0}", gratuity.Value);
                    ecbp.RemarkForPickup += addingText;
                    ecbp.RemarkForDropOff += addingText;
                }
            }
            ecbp.RemarkForPickup = ecbp.RemarkForPickup.Replace("{Gratuity}", "");
            ecbp.RemarkForDropOff = ecbp.RemarkForDropOff.Replace("{Gratuity}", "");
            if (request != null && request.GroundReservations != null && request.GroundReservations.Any())
            {
                if (request.GroundReservations.First().Service != null && request.GroundReservations.First().Service.Location != null && request.GroundReservations.First().Service.Location.Pickup != null)
                    request.GroundReservations.First().Service.Location.Pickup.Remark = ecbp.RemarkForPickup;
            }
            if (request != null && request.GroundReservations != null && request.GroundReservations.Any())
            {
                if (request.GroundReservations.First().Service != null && request.GroundReservations.First().Service.Location != null && request.GroundReservations.First().Service.Location.Dropoff != null)

                    request.GroundReservations.First().Service.Location.Dropoff.Remark = ecbp.RemarkForDropOff;
            }
            #endregion

            #region Retrieve the Trip Details
            ecbp.Trip = vtodTrip;
            #endregion

            #region Accessibility
            if (request != null && request.GroundReservations != null && request.GroundReservations.Any())
            {
                if (request.GroundReservations.FirstOrDefault().Service != null && request.GroundReservations.FirstOrDefault().Service.DisabilityVehicleInd == true)
                {

                    ind = GetDisabilityInd(tokenVTOD, ecbp.Fleet, out condition, ecbp.AuthenticationServiceEndpointName, ecbp.BookingWebServiceEndpointName);
                }
            }
            #endregion

            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
            #endregion


            DateTime? requestedLocalTime = null;
            string remarks = string.Empty;
            requestedLocalTime = ecbp.PickupDateTime;

            try
            {
                #region format request data
                #region replace the street number in pickup address
                if (ecbp.PickupAddress != null && !string.IsNullOrWhiteSpace(ecbp.PickupAddress.StreetNo)
                            && ecbp.PickupAddress.StreetNo.ToArray().Where(x => x.Equals('–')).Any())
                {
                    logger.InfoFormat("The orginial pickup street No. is {0}", ecbp.PickupAddress.StreetNo);
                    string[] sep = { "–" };
                    string[] streetNumbers = ecbp.PickupAddress.StreetNo.Split(sep, StringSplitOptions.RemoveEmptyEntries);
                    ecbp.PickupAddress.StreetNo = streetNumbers[0];
                    ecbp.Trip.ecar_trip.RemarkForPickup += string.Format(" {0} Customer on {1} block of {2}",
                        ecbp.Trip.ecar_trip.RemarkForPickup, ecbp.PickupAddress.StreetNo, ecbp.PickupAddress.Street);
                    logger.InfoFormat("Remove range street number in pickup address. Pickup street No. is {0}",
                        ecbp.PickupAddress.StreetNo);
                    logger.InfoFormat("Pickup Comment: {0}", ecbp.Trip.ecar_trip.RemarkForPickup);
                    if (TrackTime != null)
                        TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Texas, "Book_ReplacePickupStreetNumberRange");
                }
                #endregion

                #region replace the street number in drop address
                if (ecbp.DropOffAddress != null && !string.IsNullOrWhiteSpace(ecbp.DropOffAddress.StreetNo) &&
                            ecbp.DropOffAddress.StreetNo.ToArray().Where(x => x.Equals('–')).Any())
                {
                    logger.InfoFormat("The orginial dropoff street No. is {0}", ecbp.DropOffAddress.StreetNo);
                    string[] sep = { "–" };
                    string[] streetNumbers = ecbp.DropOffAddress.StreetNo.Split(sep,
                        StringSplitOptions.RemoveEmptyEntries);
                    ecbp.DropOffAddress.StreetNo = streetNumbers[0];
                    logger.InfoFormat("Remove range street number in dropoff address. Dropoff street No. is {0}",
                        ecbp.DropOffAddress.StreetNo);
                    if (TrackTime != null)
                        TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Texas, "Book_ReplaceDropoffStreetNumberRange");
                }
                #endregion

                #region replace zipCode with 5 digits zipcode only
                //replace pickup zipCode with 5 digits zipcode only
                if (ecbp.PickupAddress != null && Regex.IsMatch(ecbp.PickupAddress.ZipCode, @"([\d]+)\-[\d]+"))
                {
                    logger.InfoFormat("Original pickup zipCode: {0}", ecbp.PickupAddress.ZipCode);
                    ecbp.PickupAddress.ZipCode =
                        Regex.Match(ecbp.PickupAddress.ZipCode, @"([\d]+)\-[\d]+", RegexOptions.IgnoreCase).Groups[1]
                            .Value;
                    logger.InfoFormat("Replace: {0}", ecbp.PickupAddress.ZipCode);
                    if (TrackTime != null)
                        TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Texas, "Book_ReplacePickupZipCode");
                }

                //replace dropoff zipCode with 5 digits zipcode only
                if (ecbp.DropOffAddress != null && Regex.IsMatch(ecbp.DropOffAddress.ZipCode, @"([\d]+)\-[\d]+"))
                {
                    logger.InfoFormat("Original dropoff zipCode: {0}", ecbp.DropOffAddress.ZipCode);
                    ecbp.DropOffAddress.ZipCode =
                        Regex.Match(ecbp.DropOffAddress.ZipCode, @"([\d]+)\-[\d]+", RegexOptions.IgnoreCase).Groups[1]
                            .Value;
                    logger.InfoFormat("Replace: {0}", ecbp.DropOffAddress.ZipCode);
                    if (TrackTime != null)
                        TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Texas, "Book_ReplaceDropoffZipCode");
                }
                #endregion

                #endregion

                #region convert address to MTData address

                var mtdataAddresses = new List<AddressWS.Address>();

                var pickupAddress = GetAddress(ecbp.Trip.Id, ecbp.PickupAddressType, ecbp.Fleet, ecbp.AuthenticationServiceEndpointName, ecbp.BookingWebServiceEndpointName, ecbp.PickupAirport, ecbp.PickupAddress);
                if (pickupAddress != null)
                    mtdataAddresses.Add(pickupAddress);

                var dropOffAddress = GetAddress(ecbp.Trip.Id, ecbp.DropOffAddressType, ecbp.Fleet, ecbp.AuthenticationServiceEndpointName, ecbp.BookingWebServiceEndpointName, ecbp.DropoffAirport, ecbp.DropOffAddress);
                if(dropOffAddress != null)
                    mtdataAddresses.Add(dropOffAddress);
                
                #endregion

                #region setup request

                var bookingSettings = new ImportBookingSettings
                {
                    Booking = new Booking
                    {
                        ContactName = bookingresult.Settings.Booking.ContactName,
                        ContactPhoneNumber = bookingresult.Settings.Booking.ContactPhoneNumber,
                        Fleet = new Fleet() { ID = Convert.ToInt32(ecbp.Fleet.CCSiFleetId) },
                        Locations = new Location[2],
                        BookingID = bookingresult.Settings.Booking.BookingID
                        //Conditions = new BookingCondition[] { new BookingCondition { ID = 6 } }
                    },
                    RemoteSystemID = bookingresult.Settings.RemoteSystemID,
                    RemoteReference = bookingresult.Settings.RemoteReference,


                };
                bookingSettings.Booking.Locations[0] = new Location
                {
                    Address = ConvertAddress(mtdataAddresses[0]),
                    AddressType = (UDI.VTOD.MTData.BookingWebService.AddressType)mtdataAddresses[0].AddressType,
                    CustomerName = string.Format("{0} {1}", ecbp.FirstName,
                        ecbp.LastName),
                    CustomerPhoneNumber = ecbp.PhoneNumber,
                    CustomerEmail = "",
                    LocationType = LocationType.PickUp,
                    Time = ecbp.PickupDateTime,
                    TimeMode = ecbp.PickupNow ? TimeMode.NextAvailable : TimeMode.SpecificTime,
                    #region Pax
                    Pax = ecbp.NumberOfPassenger > 0 ? ecbp.NumberOfPassenger : 1,
                    #endregion

                };

                if (ecbp.Fleet.IVREnable.HasValue && ecbp.Fleet.IVREnable.Value == true)
                {
                    bookingSettings.RemoteSystemID = IVR_SystemID;
                    bookingSettings.Booking.Locations[0].Notifications = new UDI.VTOD.MTData.BookingWebService.Notification { AllowCustomerCallOut = true, CallOnApproach = true };
                }



                if (ecbp.PickupAddressType != AddressType.Airport && string.IsNullOrWhiteSpace(mtdataAddresses[0].Number))
                {
                    bookingSettings.Booking.Locations[0].SendPhoneToDriver = true;
                }
                bookingSettings.Booking.Locations[0].Remark = ecbp.RemarkForPickup;
                bookingSettings.Booking.Locations[1] = new Location();
                if (mtdataAddresses.Count > 1)
                {
                    bookingSettings.Booking.Locations[1].Address = ConvertAddress(mtdataAddresses[1]);
                    bookingSettings.Booking.Locations[1].AddressType = (UDI.VTOD.MTData.BookingWebService.AddressType)mtdataAddresses[1].AddressType;
                    bookingSettings.Booking.Locations[1].CustomerName = string.Format("{0} {1}", ecbp.FirstName,
                        ecbp.LastName);
                    bookingSettings.Booking.Locations[1].CustomerPhoneNumber = ecbp.PhoneNumber;
                    bookingSettings.Booking.Locations[1].CustomerEmail = "";
                    bookingSettings.Booking.Locations[1].LocationType = LocationType.DropOff;
                    if (ecbp.DropOffDateTime == null)
                    {
                        bookingSettings.Booking.Locations[1].TimeMode = TimeMode.None;
                    }
                    else
                    {
                        bookingSettings.Booking.Locations[1].Time = ecbp.DropOffDateTime ?? DateTime.MinValue;
                        bookingSettings.Booking.Locations[1].TimeMode = TimeMode.SpecificTime;
                    }
                    bookingSettings.Booking.Locations[1].Remark = ecbp.RemarkForDropOff;// string.Format(ConfigurationManager.AppSettings["MTData_Pickup_Remark"], tbp.PickupAddress.Street);
                }
                else
                {
                    bookingSettings.Booking.Locations[1].AddressType = UDI.VTOD.MTData.BookingWebService.AddressType.None;
                    bookingSettings.Booking.Locations[1].LocationType = LocationType.DropOff;
                }

                // set fixed price if needed
                if (ecbp.IsFixedPrice)
                {
                    bookingSettings.Booking.Price = (int)(ecbp.FixedPrice * 100);
                    bookingSettings.Booking.Options = new[] { BookingOption.FixedPrice, BookingOption.PriceRequired };
                }
                else
                {
                    bookingSettings.Booking.Options = new[] { /*BookingOption.FixedPrice,*/ BookingOption.PriceRequired };
                }

                // payment
                if (ecbp.PaymentType == PaymentType.PaymentCard)
                {
                    bookingSettings.Booking.PaymentMethod = new PaymentMethod
                    {
                        ID = MTDataNameValue.PaymentMethod_Account_ID,
                    };
                    bookingSettings.Booking.Account = new Account { AccountNumber = ecbp.User.AccountNumber };
                }
                else if (ecbp.PaymentType == PaymentType.Cash)
                {
                    //Not support cash trip
                    var vtodException = VtodException.CreateException(ExceptionType.ECar, 2029);
                    throw vtodException;
                }
                else
                {
                    bookingSettings.Booking.PaymentMethod = new PaymentMethod
                    {
                        ID = MTDataNameValue.PaymentMethod_Cash_ID
                    };
                }

                // tip info
                if (!string.IsNullOrWhiteSpace(ecbp.Gratuity))
                {
                    bookingSettings.Booking.TipInfo = new TipInfo();
                    if (ecbp.Gratuity != null)
                    {
                        if (ecbp.GratuityType.Trim().ToLower() == "Percent".ToLower())
                        {
                            bookingSettings.Booking.TipInfo.TipType = TipType.Percent;
                            bookingSettings.Booking.TipInfo.Tip = decimal.Parse(ecbp.Gratuity);
                        }
                        else
                        {
                            bookingSettings.Booking.TipInfo.TipType = TipType.Fixed;
                            bookingSettings.Booking.TipInfo.Tip = decimal.Parse(ecbp.Gratuity);
                        }
                    }
                }

                #endregion

                swEachPart.Restart();

                var result = DoImportBooking(bookingSettings, ecbp.Fleet, condition, ecbp);

                swEachPart.Stop();
                logger.DebugFormat("Call MTData API to book a trip. Duration:{0} ms.", sw.ElapsedMilliseconds);

                if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Texas, "Book_SendingBookingRequest");

                var responseDateTime = DateTime.Now;

                #region handle response

                if (result.Succeeded)
                {
                    string confirmationNumber = result.Booking.BookingID.ToString();
                    if (!string.IsNullOrWhiteSpace(confirmationNumber))
                    {
                        bool IsModifyTrip = utility.UpdateModifyEcarTrip(ecbp);
                        if (IsModifyTrip == false)
                        {
                            logger.InfoFormat("Error in Modification");
                        }

                        if (ecbp.EnableResponseAndLog)
                        {
                            GroundReservation reservation = ecbp.request.GroundReservations.FirstOrDefault();
                            if (reservation != null && reservation.Service != null)
                            {
                                if (reservation.Service.Location != null && reservation.Service.Location != null)
                                    reservation.Service.Location.Pickup.Remark = ecbp.RemarkForPickup;
                            }
                            response = new OTA_GroundBookRS
                            {
                                EchoToken = ecbp.request.EchoToken,
                                Target = ecbp.request.Target,
                                Version = ecbp.request.Version,
                                PrimaryLangID = ecbp.request.PrimaryLangID,
                                Success = new Success(),
                                Reservations = new List<Reservation>()
                            };

                            //Reservation
                            var r = new Reservation
                            {
                                Confirmation = new Confirmation
                                {
                                    ID = ecbp.Trip.Id.ToString(),
                                    Type = ConfirmationType.Confirmation.ToString()
                                },
                                Passenger = reservation.Passenger,
                                Service = reservation.Service
                            };
                            r.Confirmation.TPA_Extensions = new TPA_Extensions
                            {
                                Confirmations = new List<Confirmation>
                                {
                                    new Confirmation
                                    {
                                        ID = ecbp.Trip.ecar_trip.DispatchTripId,
                                        Type = ConfirmationType.DispatchConfirmation.ToString()
                                    }
                                }
                            };
                            #region FleetInfo
                            ecar_fleet eCarFleet = null;
                            using (var db = new DataAccess.VTOD.VTODEntities())
                            {
                                Int64 fleetId = 0;
                                var eCarTrip = db.ecar_trip.Where(p => p.Id == ecbp.Trip.Id).FirstOrDefault();
                                if (eCarTrip != null)
                                {
                                    fleetId = eCarTrip.FleetId;
                                }
                                if (fleetId > 0)
                                {
                                    eCarFleet = db.ecar_fleet.Where(p => p.Id == fleetId).FirstOrDefault();
                                }

                            }
                            if (eCarFleet != null)
                            {
                                r.TPA_Extensions = new TPA_Extensions();
                                r.TPA_Extensions.Contacts = new Contacts();
                                r.TPA_Extensions.Contacts.Items = new List<Contact>();
                                var contactDetails = new Contact { Type = Common.DTO.Enum.ContactType.Dispatch.ToString() };
                                if (!string.IsNullOrWhiteSpace(eCarFleet.Name))
                                {
                                    contactDetails.Name = eCarFleet.Name;
                                }
                                if (!string.IsNullOrWhiteSpace(eCarFleet.PhoneNumber))
                                {
                                    contactDetails.Telephone = new Telephone { PhoneNumber = eCarFleet.PhoneNumber };
                                }
                                r.TPA_Extensions.Contacts.Items.Add(contactDetails);
                            }
                            #endregion

                            response.Reservations.Add(r);

                            #region Update status into Ecar_trip_status
                            if (ecbp.IsFixedPrice)
                            {
                                utility.UpdateFinalStatus(ecbp.Trip.Id, ECarTripStatusType.ModifyBooked, ecbp.FixedPrice.ToDecimal(), null, null, null, null);
                            }

                            utility.SaveTripStatus(ecbp.Trip.Id, ECarTripStatusType.ModifyBooked, requestedLocalTime, null, null, null, null, null, null, null, null, string.Empty, string.Empty, null);
                            #endregion

                        }
                        else
                        {
                            logger.Info("Disable response and log for booking");
                        }
                    }
                    else
                    {
                        string error = string.Format("Cannot get DispatchTripId. Or dispatchTripId is white space.");
                        logger.Error(error);
                        utility.MarkThisTripAsError(ecbp.Trip.Id, "BookingID is empty");
                        throw new ECarException(error);
                    }

                    if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Texas, "Book_CreateResponseObject");
                }
                else
                {
                    logger.Error(result.ErrorMessage);
                    utility.MarkThisTripAsError(ecbp.Trip.Id, result.ErrorMessage);
                    throw new ECarException(result.ErrorMessage);
                }

                #endregion

                #region Write Ecar Log
                if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
                {
                    var requestMessage = ecbp.XmlSerialize().ToString();
                    var responseMessage = result.XmlSerialize().ToString();

                    utility.WriteECarLog(ecbp.Trip.Id, ECarServiceAPIConst.Texas,  ECarServiceMethodType.Book, null,
                        requestMessage, responseMessage, requestDateTime, responseDateTime);
                }
                #endregion
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Message:{0}", ex.Message);
                logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
                logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);

                //change the status of trip
                utility.UpdateFinalStatus(ecbp.Trip.Id,   ECarTripStatusType.Error, null, null, null, null, null);

                throw;
            }

            sw.Stop();
            logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "Book");
            #endregion

            return response;
        }
        public OTA_GroundResRetrieveRS Status(TokenRS tokenVTOD, OTA_GroundResRetrieveRQ request, ECarStatusParameter ecsp)
        {
            logger.Info("Start");
            Stopwatch sw = new Stopwatch();
            sw.Start();
            OTA_GroundResRetrieveRS response = null;
            int? etaWithTraffic = null;
            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
            #endregion
            #region Set token, request
            ecsp.tokenVTOD = tokenVTOD;
            ecsp.request = request;
            #endregion
            string dispatchConfirmationId = ecsp.Trip.ecar_trip.DispatchTripId;
           
            #region get response back
            try
            {
                var requestDateTime = DateTime.Now;
                decimal MinutesAway = 0;
                decimal MinutesAwayWithTraffic = 0;

                if (!string.IsNullOrEmpty(dispatchConfirmationId))
                {
                    GetBookingResult statusResult = DoGetBooking(Convert.ToInt64(dispatchConfirmationId), ecsp);
                    OSIBookingResult bookingResult = DoGetBookingByID(Convert.ToInt64(dispatchConfirmationId), ecsp);

                    var responseDateTime = DateTime.Now;
                    string driverID = null;

                    if (statusResult.Succeeded)
                    {
                        #region handle response

                        response = new OTA_GroundResRetrieveRS
                        {
                            Success = new Success(),
                            EchoToken = request.EchoToken,
                            Target = request.Target,
                            Version = request.Version,
                            PrimaryLangID = request.PrimaryLangID,
                        };

                        if (statusResult.BookingSummary != null)
                        {
                            var vtodTripStatus = utility.ConvertTripStatusForMTData(statusResult.BookingSummary.Status);

                            driverID = bookingResult.Booking.Driver != null ? bookingResult.Booking.Driver.Value : null;

                            #region Caculate ETA by ourself
                            if (
                                (vtodTripStatus.ToUpperInvariant().Equals(ECarTripStatusType.Accepted.ToUpperInvariant()) || vtodTripStatus.ToUpperInvariant().Equals(ECarTripStatusType.Assigned.ToUpperInvariant()) || vtodTripStatus.ToUpperInvariant().Equals(ECarTripStatusType.InTransit.ToUpperInvariant()))
                                &&
                                (statusResult.BookingSummary.VehicleLat != 0 && statusResult.BookingSummary.VehicleLng != 0)
                                &&
                                (ecsp.Trip.ecar_trip.PickupLatitude.HasValue && ecsp.Trip.ecar_trip.PickupLongitude.HasValue)
                            )
                            {
                                MapService ms = new MapService();
                                double d = ms.GetDirectDistanceInMeter(ecsp.Trip.ecar_trip.PickupLatitude.Value, ecsp.Trip.ecar_trip.PickupLongitude.Value, Convert.ToDouble(statusResult.BookingSummary.VehicleLat), Convert.ToDouble(statusResult.BookingSummary.VehicleLng));
                                var rc = new UDI.SDS.RoutingController(ecsp.tokenVTOD, null);
                                UDI.SDS.RoutingService.Point start = new UDI.SDS.RoutingService.Point { Latitude = ecsp.Trip.ecar_trip.PickupLatitude.Value, Longitude = ecsp.Trip.ecar_trip.PickupLongitude.Value };
                                UDI.SDS.RoutingService.Point end = new UDI.SDS.RoutingService.Point { Latitude = Convert.ToDouble(statusResult.BookingSummary.VehicleLat), Longitude = Convert.ToDouble(statusResult.BookingSummary.VehicleLng) };
                                var result = rc.GetRouteMetrics(start, end);
                                MinutesAway = result.TotalMinutes;

                            }
                            #endregion

                            #region ETA with Traffic
                            //ToDo: it should be configurable. It's hardcoded just for now but should be configuratble.
                            if (ecsp.User.name.ToLower() == "ztrip" || !ecsp.Trip.DriverAcceptedETA.HasValue)
                            {
                                if (
                                                        vtodTripStatus.Trim().ToLower() == ECarTripStatusType.Accepted.Trim().ToLower() ||
                                                        vtodTripStatus.Trim().ToLower() == ECarTripStatusType.Assigned.Trim().ToLower() ||
                                                        vtodTripStatus.Trim().ToLower() == ECarTripStatusType.InTransit.Trim().ToLower()
                                                        )
                                {
                                    if (statusResult.BookingSummary.VehicleNumber != null && statusResult.BookingSummary.VehicleLat != 0 && statusResult.BookingSummary.VehicleLng != 0)
                                    {
                                        if (ecsp.Trip != null && ecsp.Trip.ecar_trip != null && ecsp.Trip.ecar_trip.PickupLatitude.HasValue && ecsp.Trip.ecar_trip.PickupLatitude.Value != 0 && ecsp.Trip.ecar_trip.PickupLongitude.HasValue && ecsp.Trip.ecar_trip.PickupLongitude.Value != 0)
                                        {
                                            try
                                            {
                                                var error = string.Empty;
                                                var map = new UDI.Map.MapService(Map.DTO.MapApiProvider.Google);
                                                var etaResult = map.GetETA(new Map.DTO.Geolocation { Latitude = statusResult.BookingSummary.VehicleLat.ToDecimal(), Longitude = statusResult.BookingSummary.VehicleLng.ToDecimal() },
                                                    new Map.DTO.Geolocation { Latitude = ecsp.Trip.ecar_trip.PickupLatitude.Value.ToDecimal(), Longitude = ecsp.Trip.ecar_trip.PickupLongitude.ToDecimal() }, UDI.Map.DTO.DistanceUnit.Mile, out error);

                                                if (string.IsNullOrWhiteSpace(error))
                                                {
                                                    etaWithTraffic = System.Math.Round((decimal)(etaResult.DurationInTrafficInSecond / 60)).ToInt32();
                                                    MinutesAwayWithTraffic = etaWithTraffic.ToDecimal();
                                                }
                                            }
                                            catch
                                            { }
                                        }
                                    }
                                }
                            }
                            #endregion


                            response.TPA_Extensions = new TPA_Extensions
                            {
                                Vehicles = new Vehicles
                                {
                                    Items = new List<Vehicle>
                                {
                                    new Vehicle
                                    {
                                        ID = statusResult.BookingSummary.VehicleNumber,
                                        MinutesAway = MinutesAway,
                                        Geolocation = new Geolocation
                                        {
                                            Lat = (decimal) statusResult.BookingSummary.VehicleLat,
                                            Long = (decimal) statusResult.BookingSummary.VehicleLng
                                        }
                                    }
                                }
                                },
                                Statuses = new Statuses
                                {
                                    Status = new List<Status>
                                {
                                    new Status
                                    {
                                        Value =
                                            utility.ConvertTripStatusMessageForFrontEndDevice(ecsp.Fleet_User.UserId,
                                                vtodTripStatus)
                                    }
                                }
                                },
                                PickupLocation = new Pickup_Dropoff_Stop
                                {
                                    Address = new Common.DTO.OTA.Address
                                    {
                                        StreetNmbr = statusResult.BookingSummary.PickupStreetNumber,
                                        AddressLine = string.Format("{0} {1}",
                                            statusResult.BookingSummary.PickupStreetName,
                                            statusResult.BookingSummary.PickupDesignation),
                                        CityName = statusResult.BookingSummary.PickupSuburb,
                                        Latitude =
                                    ecsp.Trip == null || !ecsp.Trip.ecar_trip.PickupLatitude.HasValue
                                                ? ""
                                                : ecsp.Trip.ecar_trip.PickupLatitude.Value.ToString(CultureInfo.InvariantCulture),
                                        Longitude =
                                    ecsp.Trip == null || ecsp.Trip.ecar_trip.PickupLongitude == null
                                                ? ""
                                                : ecsp.Trip.ecar_trip.PickupLongitude.Value.ToString(CultureInfo.InvariantCulture)
                                    }
                                },
                                DropoffLocation = new Pickup_Dropoff_Stop
                                {
                                    Address = new Common.DTO.OTA.Address
                                    {
                                        Latitude =
                                    ecsp.Trip == null || ecsp.Trip.ecar_trip.DropOffLatitude == null
                                                ? ""
                                                : ecsp.Trip.ecar_trip.DropOffLatitude.Value.ToString(CultureInfo.InvariantCulture),
                                        Longitude =
                                    ecsp.Trip == null || ecsp.Trip.ecar_trip.DropOffLongitude == null
                                                ? ""
                                                : ecsp.Trip.ecar_trip.DropOffLongitude.Value.ToString(CultureInfo.InvariantCulture)
                                    }
                                },
                                ServiceCharges = new ServiceCharges
                                {
                                    Amount = statusResult.BookingSummary.Charge.ToString(CultureInfo.InvariantCulture)
                                }
                        ,
                                PickupTimes = new PickupTimeList
                                {                                    
                                    PickupTimes = new List<PickupTime> { new PickupTime { StartDateTime = utility.ConvertToLocalTime(ecsp.Fleet, statusResult.BookingSummary.Time), EndDateTime = utility.ConvertToLocalTime(ecsp.Fleet, statusResult.BookingSummary.Time) } }

                                },

                            };
                            #region Logic For Modify Reservation
                            if (ecsp.Trip.PickupDateTimeUTC > DateTime.UtcNow)
                            {
                                if (ecsp.Trip.FinalStatus == ECarTripStatusType.Completed || ecsp.Trip.FinalStatus == ECarTripStatusType.Canceled || ecsp.Trip.FinalStatus == ECarTripStatusType.NoShow)
                                {
                                    response.TPA_Extensions.IsModify = false;
                                }
                                else
                                {
                                    response.TPA_Extensions.IsModify = true;
                                }

                            }
                            else
                            {
                                response.TPA_Extensions.IsModify = false;
                            }

                            #endregion
                            

                            #region Get Driver Info

                            vtod_driver_info driverInfo = null;
                            if (ecsp.Fleet.DriverInfo == UDI.VTOD.Common.Const.DriveInfoSource.TCS)
                            {
                                /**** Start-Get Driver Info From the vtod_driver_info****/
                                if (ecsp.Trip.DriverInfoID.HasValue && ecsp.Trip.DriverInfoID.Value > 0)
                                {
                                    using (var db = new DataAccess.VTOD.VTODEntities())
                                    {
                                        driverInfo = db.vtod_driver_info.FirstOrDefault(t => t.Id == ecsp.Trip.DriverInfoID.Value);
                                    }
                                }
                                /****End-Get Driver Info From the vtod_driver_info****/
                                else
                                {
                                    if (ecsp.tokenVTOD != null && ecsp.lastTripStatus.DriverId != null && !string.IsNullOrWhiteSpace(ecsp.lastTripStatus.DriverId))
                                    {
                                        driverInfo = utility.GetDriverInfoByDriverId(ecsp.tokenVTOD.Username, ecsp.Trip, ecsp.lastTripStatus.Status, ecsp.lastTripStatus.DriverId);
                                    }
                                    if (driverInfo == null)
                                    {
                                        if (ecsp.tokenVTOD != null && !string.IsNullOrWhiteSpace(statusResult.BookingSummary.VehicleNumber) && !string.IsNullOrEmpty(driverID))
                                        {
                                            driverInfo = utility.GetDriverInfoByVehicleNumber(ecsp.tokenVTOD.Username, vtodTripStatus, statusResult.BookingSummary.VehicleNumber, driverID, ecsp.Trip);
                                        }
                                    }
                                }
                                if (ecsp.Trip.DriverInfoID.HasValue && ecsp.Trip.DriverInfoID.Value > 0)
                                {
                                    using (var db = new DataAccess.VTOD.VTODEntities())
                                    {
                                        driverInfo = db.vtod_driver_info.FirstOrDefault(t => t.Id == ecsp.Trip.DriverInfoID.Value);
                                    }
                                }


                            }
                            else if (ecsp.Fleet.DriverInfo == UDI.VTOD.Common.Const.DriveInfoSource.MTData)
                            {

                                if (ecsp.tokenVTOD != null && !string.IsNullOrWhiteSpace(statusResult.BookingSummary.VehicleNumber) && bookingResult.Booking.Driver != null)
                                {
                                    try
                                    {
                                        if (!string.IsNullOrWhiteSpace(ecsp.tokenVTOD.Username))
                                        {
                                            if (utility.IsAllowedPullingDriverInfo(ecsp.tokenVTOD.Username) && ecsp.Trip != null)
                                            {
                                                if (!ecsp.Trip.DriverInfoID.HasValue || ecsp.Trip.DriverInfoID.Value == 0)
                                                {
                                                    if (utility.HasDriver(vtodTripStatus))
                                                    {
                                                        OSIDriverResult oSIDriverResult = DoGetOSIDriverResultByID(bookingResult.Booking.Driver.ID, ecsp);

                                                        driverInfo = MapOSIDriverResultToVtod_driver_info(oSIDriverResult);

                                                        using (var db = new DataAccess.VTOD.VTODEntities())
                                                        {
                                                            VTOD.DriverInfo driverInformation = new VTOD.DriverInfo(logger);

                                                            driverInfo = driverInformation.UpsertDriverDetails(driverInfo, null);

                                                            db.SP_vtod_Update_TripDriverInfoID(ecsp.Trip.Id, driverInfo.Id);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (ecsp.Trip.DriverInfoID.HasValue && ecsp.Trip.DriverInfoID.Value > 0)
                                                    {
                                                        using (var db = new DataAccess.VTOD.VTODEntities())
                                                        {
                                                            driverInfo = db.vtod_driver_info.Where(s => s.Id == ecsp.Trip.DriverInfoID.Value).FirstOrDefault();
                                                        }
                                                    }
                                                }
                                            }
                                            //It is IVR trip if trip is null 
                                            //In IVR case, we don't need to save driver info
                                            else
                                            {
                                                if (ecsp.Trip.DriverInfoID.HasValue && ecsp.Trip.DriverInfoID.Value > 0)
                                                {
                                                    using (var db = new DataAccess.VTOD.VTODEntities())
                                                    {
                                                        driverInfo = db.vtod_driver_info.Where(s => s.Id == ecsp.Trip.DriverInfoID.Value).FirstOrDefault();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                                    {
                                        foreach (var eve in ex.EntityValidationErrors)
                                        {
                                            logger.ErrorFormat("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                                eve.Entry.Entity.GetType().Name, eve.Entry.State);
                                            foreach (var ve in eve.ValidationErrors)
                                            {
                                                logger.ErrorFormat("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName, ve.ErrorMessage);
                                            }
                                        }
                                        throw;
                                    }

                                }
                            }
                            else if (ecsp.Fleet.DriverInfo == UDI.VTOD.Common.Const.DriveInfoSource.Local)
                            {
                                if (ecsp.Trip.DriverInfoID.HasValue && ecsp.Trip.DriverInfoID.Value > 0)
                                {
                                    if (ecsp.Trip != null)
                                    {
                                        using (var db = new DataAccess.VTOD.VTODEntities())
                                        {
                                            driverInfo = db.vtod_driver_info.FirstOrDefault(t => t.Id == ecsp.Trip.DriverInfoID.Value);
                                        }
                                    }
                                }
                                else
                                {
                                    if (ecsp.tokenVTOD != null && ecsp.lastTripStatus.DriverId != null && !string.IsNullOrWhiteSpace(ecsp.lastTripStatus.DriverId))
                                    {
                                        using (var db = new DataAccess.VTOD.VTODEntities())
                                        {
                                            driverInfo = db.vtod_driver_info.FirstOrDefault(t => t.DriverID == ecsp.lastTripStatus.DriverId && t.FleetID == ecsp.Fleet.Id && t.FleetType == "ExecuCar");
                                            if (driverInfo != null)
                                                db.SP_vtod_Update_TripDriverInfoID(ecsp.Trip.Id, driverInfo.Id);
                                        }

                                    }
                                }
                            }
                            #endregion

                            #region Add dispatch info & driver info

                            response.TPA_Extensions.Contacts = new Contacts { Items = new List<Contact>() };

                            var dispatchContact = new Contact
                            {
                                Type = ContactType.Dispatch.ToString(),
                                Name = ecsp.Fleet.Alias,
                                Telephone = new Telephone { PhoneNumber = ecsp.Fleet.PhoneNumber.CleanPhone10Digit() },
                                PhotoUrl = ConfigHelper.GetDriverPhotoVersionDefault()
                            };
                            response.TPA_Extensions.Contacts.Items.Add(dispatchContact);

                            if (driverInfo != null && !string.IsNullOrWhiteSpace(driverInfo.CellPhone))
                            {
                                var driverContact = new Contact
                                {
                                    Type = ContactType.Driver.ToString(),
                                    Name = string.Format("{0} {1}", driverInfo.FirstName, driverInfo.LastName),
                                    Telephone = new Telephone { PhoneNumber = driverInfo.CellPhone.CleanPhone10Digit() },
                                    PhotoUrl = driverInfo.PhotoUrl,
                                    ZtripDateOfDesignation = driverInfo.QualificationDate,
                                    ZtripDesignation = driverInfo.zTripQualified
                                };
                                response.TPA_Extensions.Contacts.Items.Add(driverContact);
                            }

                            #endregion

                            #region Add split payment info
                            if (ecsp.Trip != null)
                            {
                                using (var db = new VTODEntities())
                                {
                                    var vtodTrip = db.vtod_trip.FirstOrDefault(m => m.Id == ecsp.Trip.Id);
                                    response.TPA_Extensions.SplitPayment = new SplitPaymentInfo();
                                    if (vtodTrip.IsSplitPayment != null && vtodTrip.IsSplitPayment.Value)
                                    {
                                        response.TPA_Extensions.SplitPayment.IsSplitPayment = true;
                                        response.TPA_Extensions.SplitPayment.SplitPaymentStatus = ((SplitPaymentStatus)vtodTrip.SplitPaymentStatus).ToString();

                                        var ecar_trip = db.ecar_trip.FirstOrDefault(m => m.Id == ecsp.Trip.Id);
                                        response.TPA_Extensions.SplitPayment.MemberA = new SplitPaymentUserInfo
                                        {
                                            MemberID = vtodTrip.MemberID,
                                            FirstName = ecar_trip.FirstName,
                                            LastName = ecar_trip.LastName
                                        };
                                        response.TPA_Extensions.SplitPayment.MemberB = new SplitPaymentUserInfo
                                        {
                                            MemberID = vtodTrip.SplitPaymentMemberB,
                                            //FirstName = ecar_trip.MemberBFirstName,
                                            //LastName = ecar_trip.MemberBLastName
                                        };
                                    }
                                    else
                                    {
                                        response.TPA_Extensions.SplitPayment.IsSplitPayment = false;
                                    }
                                }
                            }
                            #endregion

                            if (ecsp.Trip != null)
                            {
                                #region Update status & fare

                                try
                                {
                                    decimal? fare = null;
                                    decimal? gratuity = null;
                                    if (vtodTripStatus == ECarTripStatusType.Completed)
                                    {
                                        try
                                        {
                                            fare = CalculateFare(bookingResult.Booking);
                                            gratuity = bookingResult.Booking.Tips;

                                            decimal? overwriteFare = null;

                                            try
                                            {
                                                overwriteFare = utility.DetermineOverwriteFare(ecsp, response.TPA_Extensions.Vehicles.Items.First().Geolocation.Long, response.TPA_Extensions.Vehicles.Items.First().Geolocation.Lat, fare);
                                                fare = (overwriteFare.HasValue && overwriteFare.Value > 0) ? overwriteFare.Value : fare;
                                            }
                                            catch (Exception ex)
                                            {
                                                logger.Error("Cannot find Flat Rate", ex);
                                            }

                                            logger.InfoFormat("TripID:{0}, Price:{1}, Extras:{2}, FlagFall:{3}, ServiceFee:{4}, Tolls:{5}, AccountCharge:{6}, Discount:{7}, Tips:{8}", ecsp.Trip.ecar_trip.Id, bookingResult.Booking.Price, bookingResult.Booking.Extras, bookingResult.Booking.FlagFall, bookingResult.Booking.ServiceFee, bookingResult.Booking.Tolls, bookingResult.Booking.AccountCharge, bookingResult.Booking.Discount, bookingResult.Booking.Tips);

                                            //fare = statusResult.BookingSummary.Charge;
                                        }
                                        catch (Exception ex)
                                        {
                                            logger.Error(ex.ToString());
                                        }
                                    }

                                    decimal? lng = (decimal)statusResult.BookingSummary.VehicleLng;
                                    if (lng == 0) lng = null;
                                    decimal? lat = (decimal)statusResult.BookingSummary.VehicleLat;
                                    if (lat == 0) lat = null;
                                    DateTime? tripStartTime = null;
                                    DateTime? tripEndTime = null;
                                    DateTime? statusTime = null;
                                    if (!statusTime.HasValue || statusTime.Value <= DateTime.MinValue || statusTime.Value < new DateTime(2010, 1, 1))
                                    {
                                        if (ecsp.Fleet != null && ecsp.Fleet.ServerUTCOffset != null)
                                        {
                                            statusTime = DateTime.UtcNow.FromUtcNullAble(ecsp.Fleet.ServerUTCOffset);
                                        }
                                    }

                                    if ((vtodTripStatus.Trim().ToLower() == ECarTripStatusType.PickUp.Trim().ToLower()) && (ecsp.Trip.TripStartTime == null))
                                    {
                                        tripStartTime = statusTime;
                                    }
                                    else
                                    {
                                        tripStartTime = ecsp.Trip.TripStartTime;
                                    }

                                    if ((vtodTripStatus.Trim().ToLower() == ECarTripStatusType.Completed.Trim().ToLower() || vtodTripStatus.Trim().ToLower() == ECarTripStatusType.MeterOff.Trim().ToLower()) && (ecsp.Trip.TripEndTime == null))
                                    {
                                        tripEndTime = statusTime;
                                    }
                                    else
                                    {
                                        tripEndTime = ecsp.Trip.TripEndTime;
                                    }

                                    if (vtodTripStatus.ToUpperInvariant() == ECarTripStatusType.Canceled.ToUpperInvariant())
                                    {
                                        utility.SaveTripStatus(ecsp.Trip.Id, ECarTripStatusType.Canceled, statusTime, null, null, null, null, null, null, null, null, string.Empty, vtodTripStatus, null);
                                        utility.UpdateFinalStatus(ecsp.Trip.ecar_trip.Id, vtodTripStatus, fare, gratuity, null, null, null);
                                    }
                                    else if (vtodTripStatus.ToUpperInvariant() == ECarTripStatusType.FastMeter.ToUpperInvariant())
                                    {
                                        utility.SaveTripStatus(ecsp.Trip.ecar_trip.Id, vtodTripStatus, statusTime,
                                      statusResult.BookingSummary.VehicleNumber, lat, lng, fare, null, null, null, null, null,
                                      statusResult.BookingSummary.Status.ToString(), driverID);
                                        utility.UpdateFinalStatus(ecsp.Trip.ecar_trip.Id, vtodTripStatus, fare, gratuity, tripStartTime, tripEndTime, null);
                                    }
                                    else
                                    {
                                        decimal? overwriteFare = utility.DetermineOverwriteFare(ecsp, response.TPA_Extensions.Vehicles.Items.First().Geolocation.Long, response.TPA_Extensions.Vehicles.Items.First().Geolocation.Lat, fare);

                                        response.TPA_Extensions.Statuses.Status[0].Fare = overwriteFare.HasValue ? overwriteFare.Value : response.TPA_Extensions.Statuses.Status[0].Fare;


                                        utility.SaveTripStatus(ecsp.Trip.ecar_trip.Id, vtodTripStatus, statusTime,
                                        statusResult.BookingSummary.VehicleNumber, lat, lng, fare, null, MinutesAway.ToInt32(), etaWithTraffic, null, null,
                                        statusResult.BookingSummary.Status.ToString(), driverID);

                                        //Save final status
                                        if (ecsp.Trip.FinalStatus.ToLower() == ECarTripStatusType.Accepted.ToLower() || ecsp.Trip.FinalStatus.ToLower() == ECarTripStatusType.Assigned.ToLower())
                                        {
                                            utility.UpdateFinalStatus(ecsp.Trip.Id, vtodTripStatus, overwriteFare, null, tripStartTime, tripEndTime, etaWithTraffic);
                                        }
                                        else
                                        {
                                            utility.UpdateFinalStatus(ecsp.Trip.Id, vtodTripStatus, overwriteFare, null, tripStartTime, tripEndTime, null);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    logger.Error(ex.ToString());
                                }

                                #endregion

                                #region SharedETA Link
                                if (request.TPA_Extensions != null && request.TPA_Extensions.RateQualifiers != null && request.TPA_Extensions.RateQualifiers.Any())
                                {
                                    string uniqueID = null;
                                    string rateQualifier = null;                                    
                                    string appSharedLink = ConfigurationManager.AppSettings["SharedETALink"];
                                    string link = null;
                                    string type = request.Reference.First().Type;
                                    if (!String.IsNullOrWhiteSpace(type))
                                    {
                                        link = "?t=" + type.Substring(0, 1).ToLower();
                                    }
                                    if (request.TPA_Extensions.RateQualifiers != null && request.TPA_Extensions.RateQualifiers.Any())
                                    {
                                        rateQualifier = request.TPA_Extensions.RateQualifiers.FirstOrDefault().RateQualifierValue;
                                        link = link + "&RQ=" + rateQualifier;
                                    }
                                    if (request.Reference != null && request.Reference.Any())
                                    {
                                        uniqueID = request.Reference.FirstOrDefault().ID;
                                    }


                                    link = link + "&id=" + UDI.Utility.Helper.Cryptography.ConvertStringToHex(uniqueID);
                                    response.TPA_Extensions.SharedLink = appSharedLink + link;
                                }
                                #endregion
                            }
                        }

                        #endregion
                    }
                    else
                    {
                        const string error = "Fail to get MTData booking status.";
                        logger.Error(error);
                        logger.Error(string.Format("error={0}, error_message={1}",
                            statusResult.Error, statusResult.ErrorMessage));
                        throw VtodException.CreateException(ExceptionType.ECar, 5001);
                    }

                    #region Write Ecar Log
                    if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]) && ecsp.Trip != null)
                    {
                        var requestMessage = ecsp.Trip.ecar_trip.DispatchTripId;
                        var responseMessage = statusResult.XmlSerialize().ToString();

                        utility.WriteECarLog(ecsp.Trip.Id, ECarServiceAPIConst.Texas, ECarServiceMethodType.Status,
                            null, requestMessage, responseMessage, requestDateTime, responseDateTime);
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Message:{0}", ex.Message);
                logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
                logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
                throw new ECarException(ex.Message);
            }
            #endregion

            sw.Stop();
            logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Texas, "Status");
            #endregion

            return response;
        }
        public OTA_GroundCancelRS Cancel(TokenRS tokenVTOD, OTA_GroundCancelRQ request, ECarCancelParameter eccp)
        {
            logger.Info("Start");
            Stopwatch sw = new Stopwatch();
            sw.Start();
            OTA_GroundCancelRS response;
            DateTime requestDateTime = DateTime.Now;

            #region Validate required fields                   
            string dispatchConfirmationId = eccp.Trip.ecar_trip.DispatchTripId;

            if (utility.IsVTODEcarTrip(dispatchConfirmationId, ECarServiceAPIConst.Texas))
            {
                //vtod trip
                if (!utility.ValidateCancelRequest(tokenVTOD, request, eccp))
                {
                    throw VtodException.CreateException(ExceptionType.ECar, 5005);
                }
            }
           
            #endregion

            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
            #endregion

            DateTime? requestedLocalTime = null;
            if (eccp.Fleet != null && eccp.Fleet.ServerUTCOffset != null)
            {
                requestedLocalTime = DateTime.UtcNow.FromUtcNullAble(eccp.Fleet.ServerUTCOffset);
            }

            try
            {
                CancelBookingResult cancelResult;
                DateTime responseDateTime;
                //vtod trip
                if ( eccp.Trip == null || utility.IsAbleToCancelThisTrip(eccp.Trip.FinalStatus))
                {
                    cancelResult = DoCancelBooking(Convert.ToInt64(dispatchConfirmationId), eccp);

                    if (cancelResult.Succeeded)
                    {
                        responseDateTime = DateTime.Now;

                        response = new OTA_GroundCancelRS
                        {
                            Success = new Success(),
                            EchoToken = request.EchoToken,
                            Target = request.Target,
                            Version = request.Version,
                            PrimaryLangID = request.PrimaryLangID
                        };

                        if (eccp.Trip != null)
                        {
                            response.Reservation = new Reservation
                            {
                                CancelConfirmation = new CancelConfirmation
                                {
                                    UniqueID = new UniqueID { ID = eccp.Trip.Id.ToString() }
                                }
                            };

                            #region Put status into ecar_trip_status
                            utility.SaveTripStatus(eccp.Trip.Id, ECarTripStatusType.Canceled, requestedLocalTime, null,
                                null, null, null, null,null,null, null,string.Empty, string.Empty, null);
                            #endregion

                            #region Set final status
                            utility.UpdateFinalStatus(eccp.Trip.Id, ECarTripStatusType.Canceled, null, null, null, null, null);
                            #endregion
                        }

                    }
                    else
                    {
                        const string error = "Fail to cancel a TexasECar(MTData) booking.";
                        logger.Error(error);
                        logger.Error(string.Format("error={0}, error_message={1}",
                            cancelResult.Error, cancelResult.ErrorMessage));
                        throw VtodException.CreateException(ExceptionType.ECar, 5001);
                    }
                }
                else
                {
                    throw VtodException.CreateException(ExceptionType.ECar, 5001);
                }

                #region Write ECar Log
                if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]) && eccp.Trip != null)
                {
                    var requestMessage = eccp.XmlSerialize().ToString();
                    var responseMessage = cancelResult.XmlSerialize().ToString();

                    utility.WriteECarLog(eccp.Trip.Id, ECarServiceAPIConst.Texas, ECarServiceMethodType.Cancel, null,
                        requestMessage, responseMessage, requestDateTime, responseDateTime);
                }
                #endregion
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Message:{0}", ex.Message);
                logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
                logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
                throw;
            }

            sw.Stop();
            logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Texas, "Cancel");
            #endregion

            return response;
        }
        public UDI.VTOD.Common.DTO.OTA.Fleet GetAvailableFleets(TokenRS token, UtilityAvailableFleetsRQ input)
        {
            logger.Info("Start");
            Stopwatch sw = new Stopwatch();
            sw.Start();
            Common.DTO.OTA.Fleet result = null;

            try
            {
                bool pickMeUpNow = true;
                bool pickMeUpLater = true;
                if (utility.ValidateGetAvailableFleets(token, input, out pickMeUpNow, out pickMeUpLater))
                {
                    result = new Common.DTO.OTA.Fleet { Type = FleetType.ExecuCar.ToString(), Now = pickMeUpNow.ToString(), Later = pickMeUpLater.ToString() };
                }
            }
            catch (Exception ex)
            {

                logger.ErrorFormat("Message:{0}", ex.Message);
                logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
                logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
                throw;
            }

            sw.Stop();
            logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
            return result;
        }

        public UDI.VTOD.Common.DTO.OTA.Fleet GetLeadTime(TokenRS token, UtilityGetLeadTimeRQ input)
        {
            throw new NotImplementedException();
        }
        public Common.DTO.OTA.UtilityGetClientTokenRS GetCorporateClientToken(Common.DTO.OTA.TokenRS token, Common.DTO.OTA.UtilityGetClientTokenRQ input)
        {
            throw new NotImplementedException();
        }
        public bool NotifyPickupDriver(TokenRS token, UtilityNotifyDriverRQ input)
        {

            string message = System.Configuration.ConfigurationManager.AppSettings["DriverPickupMessage"];
            logger.Info("Start");
            logger.Info("Start MTData for testing Notify Driver");
            var sw = new Stopwatch();
            sw.Start();
            
            string vehicleNumber = string.Empty;
            int driverID = 0;
            int cityID = 0;
            List<string> endpointNames = null;
            PaymentType paymentType = PaymentType.Unknown;

            var vtodTripIncludesEcarTrip = utility.GetECarTrip(input.TripID);

            if (vtodTripIncludesEcarTrip != null && vtodTripIncludesEcarTrip.ecar_trip != null)
            {
                var ecarTrip = vtodTripIncludesEcarTrip.ecar_trip;
                var ecarFleet = ecarTrip.ecar_fleet;
                long serviceApiId = utility.GetECarFleetServiceAPIPreference(ecarFleet.Id, ECarServiceAPIPreferenceConst.NotifyDriver).ServiceAPIId;
                endpointNames = utility.MtdataEndpointConfiguration(serviceApiId, ecarFleet.Id);
                cityID = ecarFleet.DispatchCityCode.ToInt32();
                driverID = vtodTripIncludesEcarTrip.ecar_trip_status.Last().DriverId.ToInt32();
                paymentType = (PaymentType)Enum.Parse(typeof(PaymentType), vtodTripIncludesEcarTrip.PaymentType);

                if (vtodTripIncludesEcarTrip.GratuityRate != null)
                {
                    var addingText = string.Format("{0}", vtodTripIncludesEcarTrip.GratuityRate);
                    message = message.Replace("{Gratuity}", addingText);
                }
                else
                {
                    message = message.Replace("{Gratuity}", "0");
                }

                if (driverID != 0)
                {   
                    logger.InfoFormat("Payment Type : {0}", paymentType);
                    if (paymentType == PaymentType.PaymentCard)
                    {
                        if (!string.IsNullOrEmpty(message))
                        {
                            var result = DoSendDriverMessageForCity(driverID, message, cityID, ecarFleet, endpointNames[0], endpointNames[1]);
                                                        
                            if (!result.Succeeded)
                            {
                                string error = result.ErrorMessage;
                                logger.Error(error);
                                throw new ECarException(error);
                            }
                        }
                    }
                }
            }
            else
            {
                throw VtodException.CreateException(ExceptionType.ECar, 9003);
            }

            sw.Stop();
            logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "SendMessagePickUpToVehicle");
            #endregion

            return true;
        }
        public bool NotifyDriver(TokenRS token, UtilityNotifyDriverRQ input)
        {
            logger.Info("Start");
            logger.Info("Start MTData for testing Notify Driver");
            var sw = new Stopwatch();
            sw.Start();                        
            int driverID = 0;
            int cityID = 0;
            List<string> endpointNames = null;

            var vtodTripIncludesEcarTrip = utility.GetECarTrip(input.TripID);
            var lastTripStatus = vtodTripIncludesEcarTrip.ecar_trip_status.LastOrDefault();

            if (vtodTripIncludesEcarTrip != null && vtodTripIncludesEcarTrip.ecar_trip != null && lastTripStatus != null 
                && !string.IsNullOrWhiteSpace(input.Message) && !string.IsNullOrWhiteSpace(lastTripStatus.VehicleNumber))
            {
                var ecarTrip = vtodTripIncludesEcarTrip.ecar_trip;
                var ecarFleet = ecarTrip.ecar_fleet;
                long serviceApiId = utility.GetECarFleetServiceAPIPreference(ecarFleet.Id, ECarServiceAPIPreferenceConst.NotifyDriver).ServiceAPIId;
                endpointNames = utility.MtdataEndpointConfiguration(serviceApiId, ecarFleet.Id);
                cityID = ecarFleet.DispatchCityCode.ToInt32();
                driverID = vtodTripIncludesEcarTrip.ecar_trip_status.Last().DriverId.ToInt32();

                if (driverID != 0)
                {
                    logger.Info("MTData for testing Notify Driver : Start DoSendDriverMessageForCity");

                    var result = DoSendDriverMessageForCity(driverID, input.Message, cityID, ecarFleet, endpointNames[0], endpointNames[1]);

                    logger.InfoFormat("MTData for testing Notify Driver : Result DoSendDriverMessageForCity: {0}", result.XmlSerialize().ToString());

                    if (!result.Succeeded)
                    {
                        string error = result.ErrorMessage;
                        logger.Error(error);
                        throw new ECarException(error);
                    }
                }
                
            }
            else
            {
                throw VtodException.CreateException(ExceptionType.ECar, 9003);
            }

            sw.Stop();
            logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "SendMessageToVehicle");
            #endregion

            return true;
        }
        public OTA_GroundAvailRS GetVehicleInfo(TokenRS tokenVTOD, OTA_GroundAvailRQ request, ECarGetVehicleInfoParameter ecgvip)
        {
            logger.Info("Start");
            Stopwatch sw = new Stopwatch();
            sw.Start();
            var response = new OTA_GroundAvailRS
            {
                Success = new Success(),
                TPA_Extensions = new TPA_Extensions
                {
                    Vehicles = new Vehicles { Items = new List<Vehicle>() }
                }
            };
            var requestDateTime = DateTime.Now;
            int condition = 0;
            bool ind = false;
            #region Validate required fields
            ECarGetVehicleInfoParameter tgvip;
            if (!utility.ValidateGetVehicleInfoRequest(tokenVTOD, request, out tgvip) || tgvip.Fleet == null)
            {
                string error = string.Format("This GetVehicleInfo request is invalid.");
                throw new ValidationException(error);
            }
            #endregion
            #region Add Accessibility
            if (request != null && request.DisabilityInfo != null && request.DisabilityInfo.RequiredInd == true)
            {
                tgvip.DisabilityVehicleInd = request.DisabilityInfo.RequiredInd;
                ind = GetDisabilityInd(tokenVTOD, tgvip.Fleet, out condition, tgvip.AuthenticationServiceEndpointName, tgvip.BookingWebServiceEndpointName);
            }

            #endregion
            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
            #endregion

            try
            {
                tgvip.Radius = Convert.ToDouble(Convert.ToDecimal(tgvip.Radius).ConvertDistance(
                    DistanceUnit.Mile, DistanceUnit.Feet));

                var vehiclesResult = DoGetFleetVacanVehiclesForBookingLocation(
                    tgvip.Fleet, tgvip.Latitude, tgvip.Longtitude, (float)tgvip.Radius, condition, tgvip.AuthenticationServiceEndpointName, tgvip.BookingWebServiceEndpointName);

                var responseDateTime = DateTime.Now;
                var mapService = new MapService();
                if (vehiclesResult.Succeeded && vehiclesResult.Vehicles.Any())
                {
                    #region find nearest vehicle
                    double? shortestDistance = null;
                    DispatchVehicle nearestVehicle = null;
                    var sortedVehicles = new SortedList<double, DispatchVehicle>();
                    foreach (var vehicle in vehiclesResult.Vehicles)
                    {
                        try
                        {
                            double d = mapService.GetDirectDistanceInMile(tgvip.Latitude, tgvip.Longtitude, vehicle.Latitude, vehicle.Longitude);

                            sortedVehicles.Add(d, vehicle);

                            if (shortestDistance.HasValue)
                            {
                                if (shortestDistance.Value > d)
                                {
                                    shortestDistance = d;
                                    nearestVehicle = vehicle;
                                }
                            }
                            else
                            {
                                shortestDistance = d;
                                nearestVehicle = vehicle;
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.InfoFormat("Exception", ex.Message.ToString());
                            logger.InfoFormat("Vehicle ID:{0}", vehicle);
                        }
                    }

                    if (nearestVehicle != null)
                    {
                        var rc = new RoutingController(tgvip.tokenVTOD, null);
                        var start = new Point { Latitude = tgvip.Latitude, Longitude = tgvip.Longtitude };
                        var end = new Point { Longitude = nearestVehicle.Longitude, Latitude = nearestVehicle.Latitude };
                        var result = rc.GetRouteMetrics(start, end);
                        response.TPA_Extensions.Vehicles.NearestVehicleETA = result.TotalMinutes.ToString();
                    }

                    response.TPA_Extensions.Vehicles.NumberOfVehicles = vehiclesResult.Vehicles.Count().ToString();
                    #endregion

                    Random rnd = new Random();

                    #region put all vehicle in the response
                    foreach (var vehicle in sortedVehicles.Select(item => new Vehicle
                    {
                        Geolocation = new Geolocation
                        {
                            Long = decimal.Parse(item.Value.Longitude.ToString(CultureInfo.InvariantCulture)),
                            Lat = decimal.Parse(item.Value.Latitude.ToString(CultureInfo.InvariantCulture))
                        },
                        Distance = item.Key.ToDecimal(),
                        ID = item.Value.VehicleNumber,
                        Course = rnd.Next(1, 359)
                    }))
                    {
                        response.TPA_Extensions.Vehicles.Items.Add(vehicle);
                    }
                    #endregion

                }
                else
                {
                    // no vehicles found
                    logger.Info("Found no vehicles");
                    response.TPA_Extensions.Vehicles.NumberOfVehicles = "0";
                }

                #region Write Ecar Log
                if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
                {
                    var requestMessage = tgvip.XmlSerialize().ToString();
                    var responseMessage = vehiclesResult.XmlSerialize().ToString();

                    utility.WriteECarLog(null, ECarServiceAPIConst.Texas, ECarServiceMethodType.GetVehicleInfo, null,
                        requestMessage, responseMessage, requestDateTime, responseDateTime);
                }
                #endregion

            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Message:{0}", ex.Message);
                logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
                logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
                throw new ECarException(ex.Message);
            }
            sw.Stop();
            logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Texas, "GetVehicleInfo");
            #endregion

            return response;
        }
        public virtual TrackTime TrackTime { get; set; }
        #endregion

        #region Private Methods
        /// <summary>
		/// Call GetFleet method of MTData
		/// </summary>
		/// <param name="fleet"></param>
        /// <param name="authenticateEndpoint"></param>
        /// <param name="bookingEndpoint"></param>
		/// <returns></returns>
		private GetFleetsResult DoGetFleet(ecar_fleet fleet, string authenticateEndpoint, string bookingEndpoint)
        {
            var currentToken = Token;
            RefreshToken(ref currentToken, fleet, authenticateEndpoint);

            using (var client = new BookingWebServiceSoapClient(bookingEndpoint))
            {
                var result = client.GetFleets(currentToken);
                if (result.Succeeded || result.Error != GetFleetsError.NotAuthenticated) return result;

                // retry if token is expired
                RefreshToken(ref currentToken, fleet, authenticateEndpoint);
                result = client.GetFleets(currentToken);

                return result;
            }
        }
        private ImportBookingResult DoImportBooking(ImportBookingSettings request, ecar_fleet fleet, int condition, ECarBookingParameter tbp)
        {
            var currentToken = Token;
            BookingCondition bCondition = new BookingCondition();
            bCondition.ID = condition;
            if (condition > 0)
            {
                request.Booking.Conditions = new BookingCondition[1];
                request.Booking.Conditions[0] = bCondition;
            }

            string debug_request_string = request.XmlSerialize().ToString();
            logger.InfoFormat("MTData booking request={0}", debug_request_string);
            RefreshToken(ref currentToken, fleet, tbp.AuthenticationServiceEndpointName);
            using (var client = new BookingWebServiceSoapClient(tbp.BookingWebServiceEndpointName))
            {
                var result = client.ImportBooking(currentToken, request);

                if (result.Succeeded || result.Error != ODIError.NotAuthenticated) return result;

                // retry if token is expired
                RefreshToken(ref currentToken, fleet, tbp.AuthenticationServiceEndpointName);

                result = client.ImportBooking(currentToken, request);

                return result;
            }
        }
        private GetFleetQuickLocationsResult DoGetFleetQuickLocations(ecar_fleet fleet, string AuthenticateServiceEndpointName, string BookingServiceEndpointName)
        {
            var currentToken = Token;
            RefreshToken(ref currentToken, fleet, AuthenticateServiceEndpointName);

            using (var client = new BookingWebServiceSoapClient(BookingServiceEndpointName))
            {
                var result = client.GetQuickLocationFleet(currentToken, Convert.ToInt32(fleet.CCSiFleetId));
                logger.InfoFormat("MTData:Start:DoGetFleetQuickLocations:{0}", result);
                if (result.Succeeded || result.Error != GetQuickLocationsError.NotAuthenticated) return result;

                // retry if token is expired
                RefreshToken(ref currentToken, fleet, AuthenticateServiceEndpointName);
                result = client.GetQuickLocationFleet(currentToken, Convert.ToInt32(fleet.CCSiFleetId));
                logger.InfoFormat("MTData:Start:DoGetFleetQuickLocations:{0}", result);
                return result;
            }
        }
        /// <summary>
        /// Call Authenticate method to get a new token 
        /// </summary>
        /// <param name="currentToken"></param>
        /// <param name="fleet"></param>
        /// <param name="endpointName"></param>
        /// <returns></returns>
        private void RefreshToken(ref string currentToken, ecar_fleet fleet, string endpointName)
        {
            lock (ObjSync)
            {
                if (currentToken == Token)
                {
                    using (var client = new AuthenticationWebServiceSoapClient(endpointName))
                    {
                        AuthenticationResult authResult = null;
                        if ((bool)fleet.IVREnable)
                        {
                            //IVR trip
                            authResult = client.Authenticate(BookingChannelType.SolidusIvr, IVR_UserName, IVR_Password);
                        }
                        else
                        {
                            //zTrip
                            authResult = client.Authenticate(BookingChannelType.GenesisIvr, UserName, Password);
                        }

                        if (authResult.Error != null)
                        {
                            Token = authResult.Token;
                        }
                        else
                        {
                            throw new AuthException(authResult.ErrorMessage);
                        }

                    }
                }

                currentToken = Token;
            }
        }
        /// <summary>
		/// Convert AddressWebService Address object to BookingWebService Address object
		/// </summary>
		/// <param name="address"></param>
		/// <returns></returns>
		private UDI.VTOD.MTData.BookingWebService.Address ConvertAddress(AddressWS.Address address)
        {
            var result = new UDI.VTOD.MTData.BookingWebService.Address();

            result.Designation = address.Designation == null ? null : new Designation
            {
                ID = address.Designation.ID,
                Name = address.Designation.Name,
                Postcode = address.Designation.Postcode
            };

            result.Number = address.Number;
            result.Place = address.Place == null ? null : new Place
            {
                ID = address.Place.ID,
                Name = address.Place.Name
            };
            result.Position = new Position();
            if (address.Position != null)
            {
                result.Position.ID = address.Position.ID;
                result.Position.Latitude = address.Position.Latitude;
                result.Position.Longitude = address.Position.Longitude;
            }
            else if (address.Latitude.HasValue && address.Longitude.HasValue)
            {
                //result.Position.ID = address.Position.ID;
                result.Position.Latitude = (float)address.Latitude;
                result.Position.Longitude = (float)address.Longitude;
            }
            //result.Position = address.Position == null ? null : new Position
            //{
            //	ID = address.Position.ID,
            //	Latitude = address.Position.Latitude,
            //	Longitude = address.Position.Longitude
            //};
            result.Street = address.Street == null ? null : new Street
            {
                ID = address.Street.ID,
                Name = address.Street.Name,
                Postcode = address.Street.Postcode
            };
            result.Suburb = address.Suburb == null ? null : new Suburb
            {
                ID = address.Suburb.ID,
                Name = address.Suburb.Name,
                Postcode = address.Suburb.Postcode
            };
            result.CityName = address.CityName;
            result.Latitude = address.Latitude;
            result.Longitude = address.Longitude;
            result.Unit = address.Unit;
            result.AddressType = (UDI.VTOD.MTData.BookingWebService.AddressType)address.AddressType;
            //result.ForceAddress = address.ForceAddress;
            if (address.AddressType != UDI.VTOD.MTData.AddressWebService.AddressType.Place)
            {
                result.ForceAddress = true;
            }

            return result;
        }


        /// <summary>
        /// Call GetBooking method of MTData
        /// </summary>
        /// <param name="bookingID"></param>
        /// <param name="fleet"></param>
        /// <param name="authenticateWebServiceEndpointName"></param>
        /// <param name="bookingWebServiceEndpointName"></param>
        /// <returns></returns>
        private GetBookingFullResult DoGetBookingFull(long bookingID, ecar_fleet fleet, string authenticateWebServiceEndpointName, string bookingWebServiceEndpointName)
        {
            var currentToken = Token;
            RefreshToken(ref currentToken, fleet, authenticateWebServiceEndpointName);

            using (var client = new BookingWebServiceSoapClient(bookingWebServiceEndpointName))
            {
                var result = client.GetBookingFull(currentToken, bookingID, TexasSystemID);
                if (result.Succeeded || result.Error != ODIError.NotAuthenticated) return result;

                // retry if token is expired
                RefreshToken(ref currentToken, fleet, authenticateWebServiceEndpointName);
                result = client.GetBookingFull(currentToken, bookingID, TexasSystemID);

                return result;
            }
        }


        /// <summary>
		/// Call ConvertLatLongToAddress method of MTData
		/// </summary>
		/// <param name="fleet"></param>
		/// <param name="latitude"></param>
		/// <param name="longitude"></param>
		/// <param name="errorMsg"></param>
		/// <returns></returns>
		private AddressWS.Address ConvertLatLongToAddress(ecar_fleet fleet, double latitude, double longitude, out string errorMsg, string AuthenticateServiceEndpointName, string AddressServiceEndpointName)
        {
            var currentToken = Token;
            //if (string.IsNullOrWhiteSpace(currentToken))
            RefreshToken(ref currentToken, fleet, AuthenticateServiceEndpointName);

            using (var client = new AddressWS.AddressWebServiceSoapClient(AddressServiceEndpointName))
            {
                var setting = new AddressWS.ConvertLatLongToAddressSettings
                {
                    FleetID = Convert.ToInt32(fleet.CCSiFleetId),
                    Latitude = latitude,
                    Longitude = longitude
                };

                var result = client.ConvertLatLongToAddress(currentToken, setting);
                if (result.Succeeded || result.Error != AddressWS.ConvertLatLongToAddressError.NotAuthenticated)
                {
                    errorMsg = result.ErrorMessage;
                    return result.Succeeded ? result.Address : null;
                }

                // retry if token is expired
                RefreshToken(ref currentToken, fleet, AuthenticateServiceEndpointName);
                result = client.ConvertLatLongToAddress(currentToken, setting);

                errorMsg = result.ErrorMessage;
                return result.Succeeded ? result.Address : null;
            }
        }
        /// <summary>
		/// Call GetChargeEstimate method of MTData
		/// </summary>
		/// <param name="settings"></param>
		/// <param name="fleet"></param>
		/// <returns></returns>
		private GetChargeEstimateResult DoGetChargeEstimate(ChargeEstimateSettings settings, ecar_fleet fleet, string AuthenticateServiceEndpointName, string BookingServiceEndpointName)
        {
            var currentToken = Token;
            RefreshToken(ref currentToken, fleet, AuthenticateServiceEndpointName);

            using (var client = new BookingWebServiceSoapClient(BookingServiceEndpointName))
            {
                var result = client.GetChargeEstimate(currentToken, settings);
                if (result.Succeeded || result.Error != GetChargeEstimateError.NotAuthenticated) return result;

                // retry if token is expired
                RefreshToken(ref currentToken, fleet, AuthenticateServiceEndpointName);
                result = client.GetChargeEstimate(currentToken, settings);

                return result;
            }
        }
        /// <summary>
        /// Call GetFleetVacantVehiclesForBookingLocation method of MTData
        /// </summary>
        /// <param name="fleet"></param>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <param name="radius"></param>
        /// <param name="condition"></param>
        /// <returns></returns>
        private GetFleetVacantVehiclesResult DoGetFleetVacanVehiclesForBookingLocation(
            ecar_fleet fleet, double latitude, double longitude, float radius, int condition, string AuthenticateServiceEndpointName, string BookingServiceEndpointName)
        {
            var currentToken = Token;
            RefreshToken(ref currentToken, fleet, AuthenticateServiceEndpointName);

            var location = new BookingLocationInfo
            {
                FleetId = Convert.ToInt32(fleet.CCSiFleetId),
                Latitude = latitude,
                Longitude = longitude,
                Radius = radius

            };
            if (condition > 0)
            {
                location.VehicleConditions = new int[1];
                location.VehicleConditions[0] = condition;
            }

            using (var client = new BookingWebServiceSoapClient(BookingServiceEndpointName))
            {
                var result = client.GetFleetVacantVehiclesForBookingLocation(currentToken, location);
                if (result.Succeeded || result.Error != ODIError.NotAuthenticated) return result;
                RefreshToken(ref currentToken, fleet, AuthenticateServiceEndpointName);
                result = client.GetFleetVacantVehiclesForBookingLocation(currentToken, location);

                return result;
            }
        }


        private CancelBookingResult DoCancelBooking(long bookingID, ECarCancelParameter parameter)
        {
            var currentToken = Token;
            RefreshToken(ref currentToken, parameter.Fleet, parameter.AuthenticationServiceEndpointName);

            using (var client = new BookingWebServiceSoapClient(parameter.BookingWebServiceEndpointName))
            {
                var result = client.CancelBooking(currentToken, bookingID);
                if (result.Succeeded || result.Error != CancelBookingError.NotAuthenticated) return result;

                // retry if token is expired
                RefreshToken(ref currentToken, parameter.Fleet, parameter.AuthenticationServiceEndpointName);
                result = client.CancelBooking(currentToken, bookingID);

                return result;
            }
        }

        private vtod_driver_info MapOSIDriverResultToVtod_driver_info(OSIDriverResult oSIDriverResult)
        {
            vtod_driver_info result = null;

            if (oSIDriverResult != null)
            {
                var newDriver = new vtod_driver_info();

                newDriver.DriverID = oSIDriverResult.Driver.DriverID.ToString();
                newDriver.DriverNumber = oSIDriverResult.Driver.DriverNumber;
                newDriver.FirstName = oSIDriverResult.Driver.DriverName;
                newDriver.LastName = oSIDriverResult.Driver.DriverSurname;
                newDriver.CellPhone = oSIDriverResult.Driver.MobilePhone;
                newDriver.HomePhone = oSIDriverResult.Driver.HomePhone;
                newDriver.StartDate = oSIDriverResult.Driver.StartDate.ToDateTime();
                newDriver.FleetType = Common.DTO.Enum.FleetType.ExecuCar.ToString();
                newDriver.AppendDate = System.DateTime.Now;

                //Below properties are not provided by OSIDriverResult

                //newDriver.VehicleID = row["VehicleID"].ToString();
                //newDriver.AlternatePhone = row["AlternatePhone"].ToString();
                //newDriver.QualificationDate = row["QualificationDate"].ToDateTime();
                //newDriver.zTripQualified = row["zTripQualified"].ToBool();
                //newDriver.HrDays = row["HrDays"].ToString();
                //newDriver.LeaseDate = row["LeaseDate"].ToDateTime();

                result = newDriver;

                #region Log
                logger.InfoFormat("Driver Found: ID:{0}, VehicleID:{1}", newDriver.DriverID, newDriver.VehicleID);
                #endregion
            }

            return result;
        }

        private OSIDriverResult DoGetOSIDriverResultByID(int driverId, ECarStatusParameter parameter)
        {
            var currentToken = Token;
            RefreshToken(ref currentToken, parameter.Fleet, parameter.AuthenticationServiceEndpointName);

            using (OSIWebServiceSoapClient client = new OSIWebServiceSoapClient(parameter.OsiWebServiceEndpointName))
            {
                OSIDriverResult result = client.GetDriverByID(currentToken, driverId);
                if (result.Succeeded || result.Error != OSIDriverError.NotAuthenticated) return result;

                // retry if token is expired
                RefreshToken(ref currentToken, parameter.Fleet, parameter.AuthenticationServiceEndpointName);
                result = client.GetDriverByID(currentToken, driverId);

                return result;
            }
        }

        private GetBookingResult DoGetBooking(long bookingID, ECarStatusParameter eCarStatusParameter)
        {
            var currentToken = Token;
            RefreshToken(ref currentToken, eCarStatusParameter.Fleet, eCarStatusParameter.AuthenticationServiceEndpointName);

            using (var client = new BookingWebServiceSoapClient(eCarStatusParameter.BookingWebServiceEndpointName))
            {
                var result = client.GetBooking(currentToken, bookingID);
                if (result.Succeeded || result.Error != GetBookingError.NotAuthenticated) return result;

                // retry if token is expired
                RefreshToken(ref currentToken, eCarStatusParameter.Fleet, eCarStatusParameter.AuthenticationServiceEndpointName);
                result = client.GetBooking(currentToken, bookingID);

                return result;
            }
        }

        private OSIBookingResult DoGetBookingByID(long bookingID, ECarStatusParameter eCarStatusParameter)
        {
            var currentToken = Token;
            if (string.IsNullOrWhiteSpace(currentToken))
                RefreshToken(ref currentToken, eCarStatusParameter.Fleet, eCarStatusParameter.AuthenticationServiceEndpointName);

            using (var client = new OSIWebServiceSoapClient(eCarStatusParameter.OsiWebServiceEndpointName))
            {
                var result = client.GetBookingByID(currentToken, bookingID);
                if (result.Succeeded || result.Error != OSIBookingError.NotAuthenticated) return result;

                // retry if token is expired
                RefreshToken(ref currentToken, eCarStatusParameter.Fleet, eCarStatusParameter.AuthenticationServiceEndpointName);
                result = client.GetBookingByID(currentToken, bookingID);

                return result;
            }
        }

        private decimal CalculateFare(OSIBooking booking)
        {
            if (booking.IsFixedPrice)
            {
                return booking.FixedPrice;
            }

            return booking.Price + booking.Extras + booking.FlagFall + booking.ServiceFee + booking.Tolls - booking.Discount;
        }


        private SendDriverMessageResult DoSendDriverMessageForCity(int driverNumber, string message, int cityID, ecar_fleet fleet, string AuthenticateServiceEndpointName, string BookingServiceEndpointName)
        {
            logger.InfoFormat("MTData:Start:DoSendDriverMessageForCity:driverNumber:{0}, message:{1}, cityID:{2}", driverNumber, message, cityID);

            var currentToken = Token;

            logger.InfoFormat("MTData:Start:DoSendDriverMessageForCity:Token:{0}", Token.ToString());
            RefreshToken(ref currentToken, fleet, AuthenticateServiceEndpointName);
            logger.InfoFormat("MTData:Start:DoSendDriverMessageForCity:RefereshToken:{0}", Token.ToString());


            using (var client = new BookingWebServiceSoapClient(BookingServiceEndpointName))
            {
                SendDriverMessageResult result = client.SendDriverMessage1(currentToken, driverNumber, message, cityID);
                if (result.Succeeded || result.Error != ODIError.NotAuthenticated)
                {
                    logger.InfoFormat("MTData:Response:SendDriverMessageForCity:{0}", result.XmlSerialize().ToString());

                    return result;
                }

                RefreshToken(ref currentToken, fleet, AuthenticateServiceEndpointName);
                result = client.SendDriverMessage1(currentToken, driverNumber, message, cityID);
                logger.InfoFormat("MTData:Response:SendDriverMessageForCity:{0}", result.XmlSerialize().ToString());

                return result;
            }
        }

        private AddressWS.Address GetAddress(long tripId, string addressType, ecar_fleet fleet, string authenticationEndpoint, string bookingEndpoint, string airport, Map.DTO.Address address)
        {
            AddressWS.Address mtdataAddress = null;

            if (AddressType.Airport.Equals(addressType, StringComparison.OrdinalIgnoreCase))
            {
                // airport code mapping
                var quickLocation = DoGetFleetQuickLocations(fleet,authenticationEndpoint, bookingEndpoint);

                if (quickLocation.Succeeded && quickLocation.FleetQuickLocationCollection.Any())
                {
                    var place = quickLocation.FleetQuickLocationCollection.FirstOrDefault(m => m.PlaceName.Equals(airport,StringComparison.OrdinalIgnoreCase));

                    if (place == null)
                    {
                        List<string> airportFullNames = utility.GetSynonyms(airport);
                        place = quickLocation.FleetQuickLocationCollection.FirstOrDefault(m => airportFullNames.Contains(m.PlaceName.ToLower().Trim()));
                    }

                    if (place != null)
                    {
                        mtdataAddress = new AddressWS.Address();
                        mtdataAddress.AddressType = AddressWS.AddressType.Place;
                        mtdataAddress.Place = new AddressWS.Place { ID = place.PlaceID, Name = place.PlaceName };

                        #region Adding lat/lon
                        if (place != null && place.Latitude != 0 && place.Longitude != 0)
                        {
                            mtdataAddress.Latitude = place.Latitude;
                            mtdataAddress.Longitude = place.Longitude;
                        }
                        else if (address.Geolocation != null && address.Geolocation.Latitude != 0 && address.Geolocation.Longitude != 0)
                        {
                            mtdataAddress.Latitude = (double)address.Geolocation.Latitude;
                            mtdataAddress.Longitude = (double)address.Geolocation.Longitude;
                        }
                        #endregion
                    }
                    else
                    {
                        string error = string.Format("Cannot find matching airport. message={0}", quickLocation.ErrorMessage);
                        logger.Error(error);
                        utility.MarkThisTripAsError(tripId, quickLocation.ErrorMessage);
                        throw new ECarException(error);
                    }
                }
            }
            else if (address != null)
            {
                mtdataAddress = new AddressWS.Address();

                mtdataAddress.Street = new AddressWS.Street { Name = address.Street};
                mtdataAddress.Number = address.StreetNo;
                mtdataAddress.Suburb = new AddressWS.Suburb { Name = address.City};                
                mtdataAddress.Latitude = (double)address.Geolocation.Latitude;
                mtdataAddress.Longitude = (double)address.Geolocation.Longitude;
                mtdataAddress.AddressType = AddressWS.AddressType.Street;
                

                if (System.Configuration.ConfigurationManager.AppSettings["MTData_Force_ZipCode"].ToBool())
                {
                    if (mtdataAddress.Street != null && string.IsNullOrWhiteSpace(mtdataAddress.Street.Postcode))
                    {
                        mtdataAddress.Street.Postcode = address.ZipCode;
                    }

                    if (mtdataAddress.Suburb != null && string.IsNullOrWhiteSpace(mtdataAddress.Suburb.Postcode))
                    {
                        mtdataAddress.Suburb.Postcode = address.ZipCode;
                    }
                }
            }

            return mtdataAddress;
        }

        #endregion
    }
}
