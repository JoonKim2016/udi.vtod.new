﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO.OTA;
using UDI.SDS;
using UDI.VTOD.Common.Helper;
using UDI.VTOD.Common.DTO;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.DataAccess.VTOD;
using UDI.SDS.Helper;
using UDI.Utility.Helper;
using UDI.Utility.Serialization;
using UDI.SDS.MembershipService;
using UDI.VTOD.Common.Track;
using UDI.SDS.DTO;

namespace UDI.VTOD.Domain.Accounting
{
	public class GroupDomain : BaseSetting
	{
		private VTOD.VTODDomain vtod_domain = new VTOD.VTODDomain();

		#region Methods
		internal GroupValidateDiscountCodeRS ValidateDiscountCode(TokenRS token, GroupValidateDiscountCodeRQ input)
		{
			try
			{
				GroupValidateDiscountCodeRS result = null;
				var groupsController = new UDI.SDS.GroupsController(token, TrackTime);

				#region Input Conversion
				//var sdsMembership = new UDI.SDS.MembershipService.MembershipRecord();


				#endregion

				var sdsResult = groupsController.ValidateDiscountCode(input.DiscountCode);

				#region Output Conversion
				result = new GroupValidateDiscountCodeRS();
				if (sdsResult > 0)
					result.IsValid = true;
				else
					result.IsValid = false;

				result.DiscountID = sdsResult;

				#region Common
				result.EchoToken = input.EchoToken;
				result.PrimaryLangID = input.PrimaryLangID;
				result.Target = input.Target;
				result.Version = input.Version;
				#endregion
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("GroupDomain.ValidateDiscountCode:: {0}", ex.ToString() + ex.StackTrace);

				throw VtodException.CreateException(ExceptionType.Group, 14002);// (ex.Message); 
			}
		}

		internal GroupGetDiscountRS GetDiscount(TokenRS token, GroupGetDiscountRQ input)
		{
			try
			{
				GroupGetDiscountRS result = null;
				var groupsController = new UDI.SDS.GroupsController(token, TrackTime);

				#region Input Conversion
				//var sdsMembership = new UDI.SDS.MembershipService.MembershipRecord();


				#endregion

				var sdsResult = groupsController.GetDiscount(input.DiscountID, input.AirportCode);

				#region Output Conversion
				result = new GroupGetDiscountRS();

				if (sdsResult != null)
				{
					result.AllZips = sdsResult.AllZips;
					result.DiscountCode = sdsResult.DiscountCode;
					result.DiscountID = sdsResult.DiscountID;
					result.EndDate = sdsResult.EndDate > DateTime.Now.AddYears(100) ? DateTime.Now.AddYears(100) : sdsResult.EndDate;// sdsResult.EndDate;
					result.GroupID = sdsResult.GroupID;
					result.GroupName = sdsResult.GroupName;
					result.HideBlueVanRates = sdsResult.HideBlueVanRates;
					result.HideEcarRates = sdsResult.HideEcarRates;
					result.HideWelcomeScreen = sdsResult.HideWelcomeScreen;
					result.StartDate = sdsResult.StartDate;
					result.CustomerReadOnly = sdsResult.CustomerReadOnly;
					result.HideDiscountCodeFromCustomer = sdsResult.HideDiscountCodeFromCustomer;
					result.StartDate = sdsResult.StartDate;
					if (sdsResult.DiscountAirports != null && sdsResult.DiscountAirports.DiscountAirportRecordsArray != null && sdsResult.DiscountAirports.DiscountAirportRecordsArray.Any())
					{
						result.Airports = new List<Airport>();
						foreach (var item in sdsResult.DiscountAirports.DiscountAirportRecordsArray)
						{
							var airport = new Airport
							{
								AirportCode = item.AirportCode,
								DiscountApplies = item.DiscountApplies
							};

							result.Airports.Add(airport);
						}
					}
					if (sdsResult.DiscountServiceTypes != null && sdsResult.DiscountServiceTypes.DiscountServiceTypeRecordsArray != null && sdsResult.DiscountServiceTypes.DiscountServiceTypeRecordsArray.Any())
					{
						result.DiscountServiceTypes = new List<DiscountServiceType>();
						foreach (var item in sdsResult.DiscountServiceTypes.DiscountServiceTypeRecordsArray)
						{
							var discountServiceType = new DiscountServiceType();
							discountServiceType.DiscountMethod = item.DiscountMethod;
							discountServiceType.OneWayDollarAmount = item.OneWayDollarAmount;
							discountServiceType.OneWayPercentage = item.OneWayPercentage;
							discountServiceType.RoundtripDollarAmount = item.RoundtripDollarAmount;
							discountServiceType.RoundtripPercentage = item.RoundtripPercentage;
							discountServiceType.ServiceTypeCode = item.ServiceTypeCode;
							discountServiceType.ServiceTypeDescription = item.ServiceTypeDescription;

							result.DiscountServiceTypes.Add(discountServiceType);
						}
					}
				}


				#region Common
				result.EchoToken = input.EchoToken;
				result.PrimaryLangID = input.PrimaryLangID;
				result.Target = input.Target;
				result.Version = input.Version;
				#endregion
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("GroupDomain.ValidateDiscountCode:: {0}", ex.ToString() + ex.StackTrace);

				throw VtodException.CreateException(ExceptionType.Group, 14001);// (ex.Message); 
			}
		}

		internal GroupGetPromotionTypeRS GetPromotionCodeType(TokenRS token, GroupGetPromotionTypeRQ input)
		{
			try
			{
				GroupGetPromotionTypeRS result = null;
				var groupsController = new UDI.SDS.GroupsController(token, TrackTime);

				#region Input Conversion
				//var sdsMembership = new UDI.SDS.MembershipService.MembershipRecord();


				#endregion

				var sdsResult = groupsController.GetPromotionCodeType(input.PromotionCode);

				#region Output Conversion
				result = new GroupGetPromotionTypeRS();

				result.PromotionType = Enum.GetName(typeof(Common.DTO.Enum.PromotionType), sdsResult.PromotionCodeType);

				#region Common
				result.EchoToken = input.EchoToken;
				result.PrimaryLangID = input.PrimaryLangID;
				result.Target = input.Target;
				result.Version = input.Version;
				#endregion
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("GroupDomain.GetPromotionCodeType:: {0}", ex.ToString() + ex.StackTrace);

				throw VtodException.CreateException(ExceptionType.Group, 14001);// (ex.Message); 
			}
		}

		internal GroupGetDefaultsRS GetDefaults(TokenRS token, GroupGetDefaultsRQ input)
		{
			try
			{
				GroupGetDefaultsRS result = null;
				var groupsController = new UDI.SDS.GroupsController(token, TrackTime);

				#region Input Conversion
				//var sdsMembership = new UDI.SDS.MembershipService.MembershipRecord();


				#endregion

				var sdsResult = groupsController.GetDefaults(input.DiscountID);
				result = new GroupGetDefaultsRS();

				#region Output Conversion

				if (sdsResult != null)
				{
					result.AllowInboundOutboundRoundtrip = sdsResult.AllowInboundOutboundRoundtrip;
					result.AllowInboundService = sdsResult.AllowInboundService;
					result.AllowOutboundInboundRoundtrip = sdsResult.AllowOutboundInboundRoundtrip;
					result.AllowOutboundService = sdsResult.AllowOutboundService;
					result.BillingType = sdsResult.BillingType;
					result.DefaultDirection = sdsResult.DefaultDirection;
					result.GroupID = sdsResult.GroupID;
					result.RestrictLandmarks = sdsResult.RestrictLandmarks;
					result.SedanGratuityAmount = sdsResult.SedanGratuityAmount;
					result.SedanGratuityPercent = sdsResult.SedanGratuityPercent;
					result.SedanGratuityType = sdsResult.SedanGratuityType;
					result.VanGratuityAmount = sdsResult.VanGratuityAmount;
					result.VanGratuityPercent = sdsResult.VanGratuityPercent;
					result.VanGratuityType = sdsResult.VanGratuityType;
				}

				#region Common
				result.EchoToken = input.EchoToken;
				result.PrimaryLangID = input.PrimaryLangID;
				result.Target = input.Target;
				result.Version = input.Version;
				#endregion
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("GroupDomain.GetDefaults:: {0}", ex.ToString() + ex.StackTrace);

				throw VtodException.CreateException(ExceptionType.Group, 14001);// (ex.Message); 
			}
		}

		internal GroupGetLandmarksRS GetLandmarks(TokenRS token, GroupGetLandmarksRQ input)
		{
			try
			{
				GroupGetLandmarksRS result = null;
				var groupsController = new UDI.SDS.GroupsController(token, TrackTime);

				#region Input Conversion
				//var sdsMembership = new UDI.SDS.MembershipService.MembershipRecord();


				#endregion

				var sdsResult = groupsController.GetLandmarks(input.GroupID, input.AirportCode);
				result = new GroupGetLandmarksRS();

				#region Output Conversion
				if (sdsResult != null && sdsResult.LandmarkRecordsArray != null && sdsResult.LandmarkRecordsArray.Any())
				{
					result.Landmarks = new List<Landmark>();
					foreach (var item in sdsResult.LandmarkRecordsArray)
					{
						var landmark = new Landmark();

						landmark.LandmarkID = item.LandmarkID;
						landmark.MasterLandmarkID = item.ToMasterLandmarkID();
						if (item.LandmarkAddress != null)
						{
							landmark.Location = new UDI.VTOD.Common.DTO.OTA.Location();
							landmark.Location.Address = new Address();
							landmark.Location.Telephone = new Telephone();
							landmark.Location.ID = item.LandmarkAddress.ID;
							landmark.Location.Address.CountryName = new CountryName { Code = item.LandmarkAddress.CountryCode };
							landmark.Location.Address.StateProv = new StateProv { StateCode = item.LandmarkAddress.CountrySubDivision };
							landmark.Location.Address.Latitude = item.LandmarkAddress.Latitude.ToString();
							landmark.Location.Address.Longitude = item.LandmarkAddress.Longitude.ToString();
							landmark.Location.PrimaryLangID = item.LandmarkAddress.LocalizationCode;
							landmark.Location.Address.LocationName = item.LandmarkAddress.LocationName;
							landmark.Location.Address.LocationType = new LocationType { Value = item.LandmarkAddress.LocationType };
							landmark.Location.Address.CityName = item.LandmarkAddress.Municipality;
							landmark.Location.Telephone.PhoneNumber = item.LandmarkAddress.PhoneNumber;
							landmark.Location.Telephone.AreaCityCode = item.LandmarkAddress.PhoneNumberDialingPrefix;
							landmark.Location.Address.PostalCode = item.LandmarkAddress.PostalCode;
							landmark.Location.Address.AddressLine = item.LandmarkAddress.StreetName;
							landmark.Location.Address.StreetNmbr = item.LandmarkAddress.StreetNumber;
							if (!string.IsNullOrWhiteSpace(item.LandmarkAddress.SubZipName))
							{
								landmark.Location.Address.TPA_Extensions = new TPA_Extensions { SubZipName = item.LandmarkAddress.SubZipName };
							}
							landmark.Location.Address.BldgRoom = item.LandmarkAddress.UnitNumber;
						}

						result.Landmarks.Add(landmark);
					}
				}

				#region Common
				result.EchoToken = input.EchoToken;
				result.PrimaryLangID = input.PrimaryLangID;
				result.Target = input.Target;
				result.Version = input.Version;
				#endregion
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("GroupDomain.GetDefaults:: {0}", ex.ToString() + ex.StackTrace);

				throw VtodException.CreateException(ExceptionType.Group, 14001);// (ex.Message); 
			}
		}

		internal GroupGetLocalizedServicedAirportsRS GetLocalizedServicedAirports(TokenRS token, GroupGetLocalizedServicedAirportsRQ input)
		{
			try
			{
				GroupGetLocalizedServicedAirportsRS result = null;
				var groupsController = new UDI.SDS.GroupsController(token, TrackTime);

				#region Input Conversion
				//var sdsMembership = new UDI.SDS.MembershipService.MembershipRecord();


				#endregion

				var sdsResult = groupsController.GetLocalizedServicedAirports(input.GroupID, input.PrimaryLangID);

				#region Output Conversion
				result = new GroupGetLocalizedServicedAirportsRS();
				if (sdsResult != null && sdsResult.AirportRecordsArray != null && sdsResult.AirportRecordsArray.Any())
				{
					result.Airports = new List<Airport>();

					foreach (var item in sdsResult.AirportRecordsArray)
					{
						var airport = new Airport();

						airport.AirportCode = item.AirportCode;
						airport.AirportName = item.AirportName;
						airport.AllowASAPRequests = item.AllowASAPRequests;
						airport.AllowChildSeatInput = item.AllowChildSeatInput;
						airport.AllowInfantSeatInput = item.AllowInfantSeatInput;
						airport.AllowWMVRequests = item.AllowWMVRequests;
						airport.City = item.City;
						airport.Country = item.Country;
						airport.CountryCode = item.CountryCode;
						airport.ECARServiced = item.ECARServiced;
						airport.IsAffiliate = item.IsAffiliate;
						airport.IsDefaultAirport = item.IsDefaultAirport;
						airport.Latitude = item.Latitude.ToString();
						airport.Latitude = item.Longitude.ToString();
						airport.PortType = item.PortType.ToString();
						airport.Serviced = item.Serviced;
						airport.State = item.State;
						airport.StateName = item.StateName;
						airport.ZipCode = item.ZipCode;

						result.Airports.Add(airport);
					}
				}



				#region Common
				result.EchoToken = input.EchoToken;
				result.PrimaryLangID = input.PrimaryLangID;
				result.Target = input.Target;
				result.Version = input.Version;
				#endregion
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("GroupDomain.GetDefaults:: {0}", ex.ToString() + ex.StackTrace);

				throw VtodException.CreateException(ExceptionType.Group, 14001);// (ex.Message); 
			}
		}
		#endregion

		#region Properties
		public TrackTime TrackTime { get; set; }
		#endregion
	}
}
