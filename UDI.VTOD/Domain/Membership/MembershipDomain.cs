﻿using System;
using System.Collections.Generic;
using System.Linq;
using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO;
using UDI.VTOD.Common.DTO.OTA;
using UDI.Utility.Helper;
using UDI.VTOD.Common.Track;
using UDI.VTOD.DataAccess.VTOD;
using UDI.VTOD.Common.DTO.Enum;
using UDI.Map;
using UDI.Notification.Service.Controller;
using UDI.Notification.Service.Class;
using UDI.VTOD.Domain.ECar.Aleph;
using UDI.Utility.Serialization;
using System.Configuration;
using System.Web;
using UDI.VTOD.Helper;
namespace UDI.VTOD.Domain.Membership
{
	public class MembershipDomain : BaseSetting
	{
		private VTOD.VTODDomain vtod_domain = new VTOD.VTODDomain();

		public MembershipDomain()
		{
			vtod_domain.TrackTime = vtod_domain.TrackTime = TrackTime; 
		}

		public bool ValidateToken(string userName, string securityKey)
		{
			var result = false;

			var sds = new UDI.SDS.SecurityController(null, TrackTime);
			result = sds.ValidateToken(userName, securityKey.ToGuid());

			return result;
		}

		internal MemberCreateRS MemberCreate(TokenRS token, MemberCreateRQ input)
		{
			try
			{
				MemberCreateRS result = null;
				var membershipController = new UDI.SDS.MembershipController(token, TrackTime);
                int referralEnabled = System.Configuration.ConfigurationManager.AppSettings["EnableReferral"].ToInt32();

                #region Input Conversion
                var sdsMembership = new UDI.SDS.MembershipService.MembershipRecord();
                if (input.MembershipRecord.SubscribeToEmail != null)
                {
                    sdsMembership.AllowSpam = (bool)input.MembershipRecord.SubscribeToEmail;
                }
				sdsMembership.ContactNumber = (input.MembershipRecord.Telephone.AreaCityCode + input.MembershipRecord.Telephone.PhoneNumber).CleanPhone();
				sdsMembership.ContactNumberDialingPrefix = input.MembershipRecord.Telephone.CountryAccessCode;
				sdsMembership.EmailAddress = input.MembershipRecord.EmailAddress;
				sdsMembership.FirstName = input.MembershipRecord.FirstName;
				sdsMembership.LastName = input.MembershipRecord.LastName;
				if (input.MembershipRecord.SubscribeToSms != null)
				{
					sdsMembership.SubscribeToSms = (bool)input.MembershipRecord.SubscribeToSms;
				}
                if (input.MembershipRecord.DisabilityVehicleInd != null)
                {
                    sdsMembership.AccessibilityDefault = (bool)input.MembershipRecord.DisabilityVehicleInd;
                }
                else
                {
                    sdsMembership.AccessibilityDefault = false;
                }
                sdsMembership.GratuityRate = input.MembershipRecord.DefaultGratuityPercentage / 100;
				sdsMembership.HonorificId = input.MembershipRecord.HonorificID;
				sdsMembership.BirthDate = !string.IsNullOrWhiteSpace(input.MembershipRecord.BirthDate) ? input.MembershipRecord.BirthDate.ToDateTime().Value : DateTime.MinValue;
				sdsMembership.HomeAirport = input.MembershipRecord.HomeAirport;
				sdsMembership.IsPhoneNumberVerified = false; // Always false when the member is created
				if (input.CorporateAccount != null)
				{
					sdsMembership.CorporateAccount = new UDI.SDS.MembershipService.CorporateAccount();
					sdsMembership.CorporateAccount.Corporation = input.CorporateAccount.Corporation;
					sdsMembership.CorporateAccount.UserId = input.CorporateAccount.UserID;
					sdsMembership.CorporateAccount.UserKey = input.CorporateAccount.UserKey;
				}

				#region Source and Device
				if (input.TPA_Extensions != null)
				{
					sdsMembership.ProfileSource = input.TPA_Extensions.Source;
					sdsMembership.SourceDevice = input.TPA_Extensions.Device;
				}
				#endregion

				#endregion

				var sdsMembershipRecord = membershipController.CreateMembership(sdsMembership, input.Password);

                #region Store Membership info 
                using (var db = new DataAccess.VTOD.VTODEntities())
                {
                    //create member info
                    vtod_member_info newMember = CreateVtod_member_infoWithSDSMemberShipRecord(sdsMembershipRecord);

                    //RefereePhoneNumber is not unique on DB Schema, need to find latest one
                    vtod_referral_logs referralLog = db.vtod_referral_logs.Where(x => x.RefereePhoneNumber.Equals(sdsMembershipRecord.ContactNumber))
                                                                          .OrderByDescending(x => x.CreatedOn).FirstOrDefault();

                    //check if member has been referred with the Phone#
                    if (referralLog != null)
                    {
                        //Credit To Referrer                                                
                        DateTime currentTime = DateTime.Now;
                        decimal creditAmount = db.vtod_referral_credit_configuration
                                              .Where(x => x.ReferralType == (int)UDI.SDS.MembershipService.ZTripCreditType.ReferralNewUser 
                                                       && x.StartDate < currentTime && x.EndDate > currentTime)
                                              .Select(x=> x.CreditAmount).FirstOrDefault();

                        newMember.ReferrerSDSMemberID = referralLog.ReferrerSDSMemberId;
                        newMember.ReferralCreditAmount = creditAmount;

                        //Windows Service will call verificationDomain.IsPhoneVerify so we need to have ContactNumberDialingPrefix right here.
                        newMember.FirstRegisteredPhoneNumber = sdsMembershipRecord.ContactNumberDialingPrefix + sdsMembershipRecord.ContactNumber; 

                        //Windows service will find it and try to call CreateNewZTripCredit if there is ReferralCreditEarned and the phone# is verified but CreditEarnedOn is null
                    }

                    db.vtod_member_info.Add(newMember);


                    //Create BOBO Member On DB
                    if (input.MembershipRecord.IsBOBO.HasValue && input.MembershipRecord.IsBOBO.Value)
                    {
                        vtod_BOBO_member boboMember = new vtod_BOBO_member();
                        boboMember.SDSMemberId = sdsMembershipRecord.MemberID;
                        boboMember.FirstName = sdsMembershipRecord.FirstName;
                        boboMember.LastName = sdsMembershipRecord.LastName;
                        boboMember.Email = sdsMembershipRecord.EmailAddress;
                        boboMember.PhoneNumber = string.Format("{0}{1}{2}", input.MembershipRecord.Telephone.CountryAccessCode, input.MembershipRecord.Telephone.AreaCityCode, input.MembershipRecord.Telephone.PhoneNumber).CleanPhone();
                        boboMember.IsEnabled = true; 
                        boboMember.PickupAddressType = input.Address.AddressType;
                        boboMember.PickupLongitude = input.Address.Longitude.ToDouble();
                        boboMember.PickupLatitude = input.Address.Latitude.ToDouble();                        
                        boboMember.PickupStreetNo = input.Address.StreetNmbr;
                        boboMember.PickupStreetName = input.Address.AddressLine;
                        boboMember.PickupStreetType = input.Address.StreetSuffix;
                        boboMember.PickupAptNo = input.Address.BldgRoom;
                        boboMember.PickupCity = input.Address.CityName;
                        boboMember.PickupStateCode = input.Address.StateProv.StateCode;
                        boboMember.PickupZipCode = input.Address.PostalCode;
                        boboMember.PickupCountryCode = input.Address.CountryName.Code;
                        boboMember.PickupFullAddress = string.Format("{0} {1} {2} {3}, {4} {5}", boboMember.PickupStreetNo, boboMember.PickupStreetName, 
                                                                     boboMember.PickupCity,  boboMember.PickupZipCode, boboMember.PickupStateCode, boboMember.PickupCountryCode);

                        //Intentionally hard-coded 
                        boboMember.BusinessId = 1;

                        boboMember.AppendTime = DateTime.Now;

                        db.vtod_BOBO_member.Add(boboMember);
                    }

                    db.SaveChanges();
                }

                #endregion


                #region Output Conversion
                result = new MemberCreateRS();
				result.MemberID = sdsMembershipRecord.MemberID;
                if (sdsMembershipRecord.CustomerGuid != null)
                    result.CustomerGuid = sdsMembershipRecord.CustomerGuid;
                result.Success = new Success();

				#region Common
				result.EchoToken = input.EchoToken;
				result.PrimaryLangID = input.PrimaryLangID;
				result.Target = input.Target;
				result.Version = input.Version;
				#endregion
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("MembershipDomain.CreateMembership:: {0}", ex.ToString() + ex.StackTrace);

				if (!string.IsNullOrWhiteSpace(ex.Message) && ex.Message.ToLower().Contains("email") && ex.Message.ToLower().Contains("duplicate"))
				{
					throw VtodException.CreateException(ExceptionType.Membership, 7016);// (ex.Message); 
				}
				else
				{
					throw VtodException.CreateException(ExceptionType.Membership, 7001);// (ex.Message); 
				}
			}
		}

		internal MemberAddLocationRS MemberAddLocation(TokenRS token, MemberAddLocationRQ input)
		{
			try
			{
				MemberAddLocationRS result = null;
				var membershipController = new UDI.SDS.MembershipController(token, TrackTime);

				#region Reverse GeoCode
				try
				{
					if (string.IsNullOrWhiteSpace(input.MemberLocation.Address.StreetNmbr))
					{
						if (!string.IsNullOrWhiteSpace(input.MemberLocation.Address.Latitude) && !string.IsNullOrWhiteSpace(input.MemberLocation.Address.Longitude))
						{
							var lat = input.MemberLocation.Address.Latitude.ToDecimal();
							var lon = input.MemberLocation.Address.Longitude.ToDecimal();

							if (lat != 0 && lon != 0)
							{
								var geo = new UDI.Map.DTO.Geolocation { Latitude = lat, Longitude = lon };
								var mapService = new MapService();
								var mapError = string.Empty;
								var newAddresses = mapService.GetAddressesFromGeoLocationWithProximity(geo, out mapError);
								if (string.IsNullOrWhiteSpace(mapError) && newAddresses != null && newAddresses.Any())
								{
									var newAddress = newAddresses.First();
									input.MemberLocation.Address.StreetNmbr = newAddress.StreetNo;
									input.MemberLocation.Address.AddressLine = newAddress.Street;
									input.MemberLocation.Address.CountryName.Code = newAddress.Country;
									input.MemberLocation.Address.CityName = newAddress.City;
									input.MemberLocation.Address.PostalCode = newAddress.ZipCode;
									input.MemberLocation.Address.StateProv.StateCode = newAddress.State;
								}
							}
						}

					}
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}
				#endregion

				#region Input Conversion

				var sdsMembership = new UDI.SDS.MembershipService.AddressRecord();

				sdsMembership.Comments = input.MemberLocation.Comment;
				sdsMembership.CountryCode = input.MemberLocation.Address.CountryName.Code;
				sdsMembership.CountrySubDivision = input.MemberLocation.Address.StateProv.StateCode;
				sdsMembership.Municipality = input.MemberLocation.Address.CityName;
				sdsMembership.Latitude = input.MemberLocation.Address.Latitude.ToDecimal();
				sdsMembership.Longitude = input.MemberLocation.Address.Longitude.ToDecimal();
				sdsMembership.LocalizationCode = "en-us";
				if (input.MemberLocation.Telephone != null)
				{
					sdsMembership.PhoneNumber = (input.MemberLocation.Telephone.AreaCityCode + input.MemberLocation.Telephone.PhoneNumber).CleanPhone();
					sdsMembership.PhoneNumberDialingPrefix = input.MemberLocation.Telephone.CountryAccessCode;
				}
				sdsMembership.PostalCode = input.MemberLocation.Address.PostalCode;
				sdsMembership.StreetAddress = string.Format("{0} {1}", input.MemberLocation.Address.StreetNmbr, input.MemberLocation.Address.AddressLine);
				sdsMembership.StreetName = input.MemberLocation.Address.AddressLine;
				sdsMembership.StreetNumber = input.MemberLocation.Address.StreetNmbr;
				sdsMembership.UnitNumber = input.MemberLocation.Address.BldgRoom;
				sdsMembership.LocationName = input.MemberLocation.Address.LocationName;
				sdsMembership.LocationType = input.MemberLocation.Address.LocationType != null ? input.MemberLocation.Address.LocationType.Value : string.Empty;
				//sdsMembership
				#endregion

				var sdsResult = membershipController.MemberAddLocation(sdsMembership, input.MemberLocation.MemberID.ToInt32());

				#region Output Conversion
				result = new MemberAddLocationRS();
				//result.Success = false;
				if (sdsResult.LocationID > 0)
				{
					result.Success = new Success();
					result.SavedLocation = input.MemberLocation;
					result.SavedLocation.LocationID = sdsResult.LocationID.ToString();
				}
				else
				{
					throw VtodException.CreateException(ExceptionType.Membership, 7004);//throw new MembershipException(Messages.Membership_MemberAddLocation);
				}

				#region Common
				result.EchoToken = input.EchoToken;
				result.PrimaryLangID = input.PrimaryLangID;
				result.Target = input.Target;
				result.Version = input.Version;
				#endregion
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("MembershipDomain.MemberAddLocation:: {0}", ex.ToString() + ex.StackTrace);

				#region Throw MembershipException
				throw VtodException.CreateException(ExceptionType.Membership, 7004);//throw new MembershipException(Messages.Membership_MemberAddLocation);
				#endregion
			}
		}

		internal MemberChangeEmailAddressRS MemberChangeEmailAddress(TokenRS token, MemberChangeEmailAddressRQ input)
		{
			try
			{
				MemberChangeEmailAddressRS result = null;
				var membershipController = new UDI.SDS.MembershipController(token, TrackTime);

				var sdsResult = membershipController.MemberChangeEmailAddress(input.MemberId, input.NewEmailAddress);

				#region Output Conversion
				result = new MemberChangeEmailAddressRS();
				if (sdsResult)
				{
					result.Success = new Success();
				}
				else
				{
					throw VtodException.CreateException(ExceptionType.Membership, 7006);//throw new MembershipException(Messages.Membership_MemberChangeEmailAddress);
				}

				#region Common
				result.EchoToken = input.EchoToken;
				result.PrimaryLangID = input.PrimaryLangID;
				result.Target = input.Target;
				result.Version = input.Version;
				#endregion

				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("MembershipDomain.MemberChangeEmailAddress:: {0}", ex.ToString() + ex.StackTrace);

				#region Throw MembershipException
				if (ex.ToString().ToLower().Contains("duplicate email"))
				{
					throw VtodException.CreateException(ExceptionType.Membership, 7018);//throw new MembershipException(Messages.Membership_MemberChangeEmailAddress); 
				}
				else
				{
					throw VtodException.CreateException(ExceptionType.Membership, 7006);//throw new MembershipException(Messages.Membership_MemberChangeEmailAddress); 
				}
				#endregion
			}
		}

		internal MemberChangePasswordRS MemberChangePassword(TokenRS token, MemberChangePasswordRQ input)
		{
			try
			{
				MemberChangePasswordRS result = null;
				var membershipController = new UDI.SDS.MembershipController(token, TrackTime);

				var sdsResult = membershipController.MemberChangePassword(input.EmailAddress, input.OldPassword, input.NewPassword);

				#region Output Conversion
				result = new MemberChangePasswordRS();
				if (sdsResult)
				{
					result.Success = new Success();
				}
				else
				{
					throw VtodException.CreateException(ExceptionType.Membership, 7007);//throw new MembershipException(Messages.Membership_MemberChangePassword);
				}

				#region Common
				result.EchoToken = input.EchoToken;
				result.PrimaryLangID = input.PrimaryLangID;
				result.Target = input.Target;
				result.Version = input.Version;
				#endregion
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("MembershipDomain.MemberChangePassword:: {0}", ex.ToString() + ex.StackTrace);

				#region Throw MembershipException
				throw VtodException.CreateException(ExceptionType.Membership, 7007);//throw new MembershipException(Messages.Membership_MemberChangePassword);
				#endregion
			}
		}

		internal MemberDeleteLocationRS MemberDeleteLocation(TokenRS token, MemberDeleteLocationRQ input)
		{
			try
			{
				MemberDeleteLocationRS result = null;
				var membershipController = new UDI.SDS.MembershipController(token, TrackTime);

				var sdsResult = membershipController.MemberDeleteLocation(input.LocationId);

				#region Output Conversion
				result = new MemberDeleteLocationRS();
				if (sdsResult)
				{
					result.Success = new Success();
				}
				else
				{
					throw VtodException.CreateException(ExceptionType.Membership, 7008);//throw new MembershipException(Messages.Membership_MemberDeleteLocation);
				}

				#region Common
				result.EchoToken = input.EchoToken;
				result.PrimaryLangID = input.PrimaryLangID;
				result.Target = input.Target;
				result.Version = input.Version;
				#endregion
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("MembershipDomain.MemberDeleteLocation:: {0}", ex.ToString() + ex.StackTrace);

				#region Throw MembershipException
				throw VtodException.CreateException(ExceptionType.Membership, 7008);//throw new MembershipException(Messages.Membership_MemberDeleteLocation);
				#endregion
			}
		}

		internal MemberForgotPasswordRS MemberForgotPassword(TokenRS token, MemberForgotPasswordRQ input)
		{
			try
			{
				MemberForgotPasswordRS result = null;
				var membershipController = new UDI.SDS.MembershipController(token, TrackTime);

                #region Handling of "SourceWebSite"
                UDI.SDS.MembershipService.EnumerationsWebSiteBrands sourceWebSite = UDI.SDS.MembershipService.EnumerationsWebSiteBrands.Unknown;
                try
                {
                    if (!string.IsNullOrEmpty(input.SourceWebsite))
                    {
                        if (input.SourceWebsite.ToLower() == "SuperShuttle".ToLower())
                       {
                           sourceWebSite = UDI.SDS.MembershipService.EnumerationsWebSiteBrands.SuperShuttle;
                       }
                        else if (input.SourceWebsite.ToLower() == "ExecuCar".ToLower())
                       {
                           sourceWebSite = UDI.SDS.MembershipService.EnumerationsWebSiteBrands.ExecuCar;
                       }
                        else if (input.SourceWebsite.ToLower() == "ExecuCarExpress".ToLower())
                       {
                           sourceWebSite = UDI.SDS.MembershipService.EnumerationsWebSiteBrands.ExecuCarExpress;
                       }
                        else if (input.SourceWebsite.ToLower() == "SuperShuttleMobile".ToLower())
                       {
                           sourceWebSite = UDI.SDS.MembershipService.EnumerationsWebSiteBrands.SuperShuttleMobile;
                       }
                        else if (input.SourceWebsite.ToLower() == "ExecuCarMobile".ToLower())
                       {
                           sourceWebSite = UDI.SDS.MembershipService.EnumerationsWebSiteBrands.ExecuCarMobile;
                       }
                        else if (input.SourceWebsite.ToLower() == "SouthwestAirlines".ToLower())
                       {
                           sourceWebSite = UDI.SDS.MembershipService.EnumerationsWebSiteBrands.SouthwestAirlines; 
                       }
                    }
                    else
                    {
                        if (token.Username.ToLower() == "ZTRIP".ToLower())
                        {
                            sourceWebSite = UDI.SDS.MembershipService.EnumerationsWebSiteBrands.ExecuCar;
                        }
                    }
                    
                }
                catch(Exception ex)
                {
                    logger.InfoFormat("Error while handling the SourceWebsite : {0}", ex);
                }
                #endregion

                var sdsResult = membershipController.MemberForgotPassword(input.EmailAddress, sourceWebSite);

				#region Output Conversion
				result = new MemberForgotPasswordRS();
				if (sdsResult.RequestResult == UDI.SDS.MembershipService.PasswordRequestResultCodes.Success)
				{
					result.Success = new Success();
				}
				else
				{
					throw VtodException.CreateException(ExceptionType.Membership, 7009);//throw new MembershipException(Messages.Membership_MemberForgotPassword);
				}

				#region Common
				result.EchoToken = input.EchoToken;
				result.PrimaryLangID = input.PrimaryLangID;
				result.Target = input.Target;
				result.Version = input.Version;
				#endregion
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("MembershipDomain.MemberForgotPassword:: {0}", ex.ToString() + ex.StackTrace);

				#region Throw MembershipException
				throw VtodException.CreateException(ExceptionType.Membership, 7009);//throw new MembershipException(Messages.Membership_MemberForgotPassword);
				#endregion
			}
		}

		internal MemberGetPasswordRequestRS MemberGetPasswordRequest(TokenRS token, MemberGetPasswordRequestRQ input)
		{
			try
			{
				MemberGetPasswordRequestRS result = null;
				var membershipController = new UDI.SDS.MembershipController(token, TrackTime);

				var sdsResult = membershipController.MemberGetPasswordRequest(input.RequestGuid);

				#region Output Conversion
				result = new MemberGetPasswordRequestRS();
				result.EmailAddress = sdsResult.EmailAddress;
				result.FirstName = sdsResult.FirstName;
				result.LastName = sdsResult.LastName;
				result.MemberID = sdsResult.MemberID;
				result.RequestGuid = sdsResult.RequestGuid;
				result.RequestResult = sdsResult.RequestResult.ToString();
				result.RequestStatus = sdsResult.RequestStatus.ToString();
				result.SourceWebSite = sdsResult.SourceWebSite.ToString();

				#region Common
				result.EchoToken = input.EchoToken;
				result.PrimaryLangID = input.PrimaryLangID;
				result.Target = input.Target;
				result.Version = input.Version;
				result.Success = new Success();
				#endregion
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("MembershipDomain.MemberForgotPassword:: {0}", ex.ToString() + ex.StackTrace);

				#region Throw MembershipException
				throw VtodException.CreateException(ExceptionType.Membership, 7009);//throw new MembershipException(Messages.Membership_MemberForgotPassword);
				#endregion
			}
		}

		internal MemberResetPasswordRS MemberResetPassword(TokenRS token, MemberResetPasswordRQ input)
		{
			try
			{
				MemberResetPasswordRS result = null;
				var membershipController = new UDI.SDS.MembershipController(token, TrackTime);

				var sdsResult = membershipController.ResetPassword(input.MemberID, input.NewPassword, input.RequestGuid);

				#region Output Conversion
				if (sdsResult)
				{
					result = new MemberResetPasswordRS();

					#region Common
					result.EchoToken = input.EchoToken;
					result.PrimaryLangID = input.PrimaryLangID;
					result.Target = input.Target;
					result.Version = input.Version;
					result.Success = new Success();
					#endregion
				}
				else
				{
					throw VtodException.CreateException(ExceptionType.Membership, 7017);//throw new MembershipException(Messages.Membership_MemberForgotPassword);
				}
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("MembershipDomain.MemberForgotPassword:: {0}", ex.ToString() + ex.StackTrace);

				#region Throw MembershipException
				throw VtodException.CreateException(ExceptionType.Membership, 7017);//throw new MembershipException(Messages.Membership_MemberForgotPassword);
				#endregion
			}
		}

		internal MemberGetLocationsRS MemberGetLocations(TokenRS token, MemberGetLocationsRQ input)
		{
			try
			{
				MemberGetLocationsRS result = null;
				MemberLocation mlRec = null;
				var membershipController = new UDI.SDS.MembershipController(token, TrackTime);


				var sdsResult = membershipController.MemberGetLocations(input.MemberId);

				#region Output Conversion
				result = new MemberGetLocationsRS();
				//result.HasRecords = sdsResult.HasRecords;

				//if (result.HasRecords)
				//{
				result.MemberLocations = new List<MemberLocation>();

				if (sdsResult != null && sdsResult.MembershipLocationsRecordsArray != null)
				{
					foreach (UDI.SDS.MembershipService.MembershipLocationRecord mRec in sdsResult.MembershipLocationsRecordsArray)
					{
						mlRec = new MemberLocation();

						mlRec.Comment = mRec.Comments;
						mlRec.Address = new Address();
						mlRec.Telephone = new Telephone();
						mlRec.Address.CountryName = new CountryName();
						mlRec.Address.CountryName.Code = mRec.CountryCode;
						mlRec.Address.StateProv = new StateProv();
						mlRec.Address.StateProv.StateCode = mRec.CountrySubDivision;
						mlRec.Address.Latitude = mRec.Latitude.ToString();
						mlRec.Address.Longitude = mRec.Longitude.ToString();
						mlRec.LocationID = mRec.LocationID.ToString();
						mlRec.Address.LocationName = mRec.LocationName;
						mlRec.Address.CityName = mRec.Municipality;
						mlRec.Telephone.PhoneNumber = mRec.PhoneNumber;
						mlRec.Telephone.CountryAccessCode = mRec.PhoneNumberDialingPrefix;
						mlRec.Address.PostalCode = mRec.PostalCode;
						//mlRec.Address.AddressLine = mRec.StreetAddress;
						mlRec.Address.AddressLine = mRec.StreetName;
						mlRec.Address.StreetNmbr = mRec.StreetNumber;
						mlRec.Address.BldgRoom = mRec.UnitNumber;
						mlRec.Address.LocationType = new LocationType { Value = mRec.LocationType };

						result.MemberLocations.Add(mlRec);
						result.Success = new Success();
					}
				}
				//}

				#region Common
				result.EchoToken = input.EchoToken;
				result.PrimaryLangID = input.PrimaryLangID;
				result.Target = input.Target;
				result.Version = input.Version;
				#endregion
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("MembershipDomain.MemberGetLocations:: {0}", ex.ToString() + ex.StackTrace);

				#region Throw MembershipException
				throw VtodException.CreateException(ExceptionType.Membership, 7010);//throw new MembershipException(Messages.Membership_MemberGetLocations);
				#endregion
			}
		}

		internal MemberGetLocationsRS MemberGetRecentLocations(TokenRS token, MemberGetLocationsRQ input)
		{
			throw new NotImplementedException();
		}

		internal MemberGetReservationRS MemberGetReservation(TokenRS token, MemberGetReservationRQ input)
		{
			try
			{
				var numberOfReturnedMemberReservation = 25;

				try
				{
					numberOfReturnedMemberReservation = System.Configuration.ConfigurationManager.AppSettings["NumberOfReturnedMemberReservation"].ToInt32();
					logger.InfoFormat("Number of Reservations Retrieved : {0}:", numberOfReturnedMemberReservation);
				}
				catch (Exception e)
				{
					logger.ErrorFormat(e.Message);
				}

				MemberGetReservationRS result = null;
				MembershipReservation mrRec = null;
                var sdsTrip = new vtod_trip();

                var membershipController = new UDI.SDS.MembershipController(token, TrackTime);
				string webserviceState = null;

				var sdsResult = membershipController.MemberGetReservation(input.MemberId, numberOfReturnedMemberReservation);
				var vtodDomain = new VTOD.VTODDomain(); vtodDomain.TrackTime = TrackTime;
				
				//var sdsResult2 = sdsResult.MembershipReservationArray.OrderByDescending(s => int.Parse(s.ConfirmationNumber)).ToList();

				#region Output Conversion
				result = new MemberGetReservationRS();
				//result.HasRecords = sdsResult.HasRecords;

				result.MemberReservationsList = new List<MembershipReservation>();

				#region Process SDS Trips
				if (sdsResult.MembershipReservationArray != null && sdsResult.MembershipReservationArray.Any())
				{

					foreach (UDI.SDS.MembershipService.MembershipReservation mRec in sdsResult.MembershipReservationArray /*.Where(s => s.StatusCode.ToLower() != "x")*/)
					{
						mrRec = new MembershipReservation();
						mrRec.Confirmation = new List<Confirmation>();
						long vtodID = 0;
						FleetType fleetType = FleetType.ExecuCar;

						mrRec.Confirmation.Add(new Confirmation { ID = mRec.ConfirmationNumber, Type = ConfirmationType.DispatchConfirmation.ToString() }); //= mRec.ConfirmationNumber;
						using (var db = new DataAccess.VTOD.VTODEntities())
						{
							#region Track
							if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Membership_Domain, "Call");
							#endregion

							//var vtodIDs = db.SP_SDS_GetVtodId(mRec.RezID).ToList();

							#region Track
							if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_DB, "db.SP_SDS_GetVtodId(mRec.RezID).ToList()");
							#endregion

							//if (vtodIDs != null && vtodIDs.Any() && vtodIDs.Count() == 1)
							//{
							//	vtodID = vtodIDs.First().Value;
							//	//var test = sdsResult.VtodReservations.Where(s => s.TripID == vtodID);
							//}
							#region FleetType should come from SDS. For temporary solution I get it from DB
							sdsTrip = db.vtod_trip.Include("sds_trip").Where(s => s.sds_trip.RezID == mRec.RezID).FirstOrDefault();

							if (sdsTrip != null)
							{
								fleetType = FleetType.ExecuCar;
								vtodID = sdsTrip.Id;
								webserviceState = vtodDomain.Get_WS_State("vtod_trip", vtodID, "CorporateIDs");
							}
							else
							{
								var taxiTrip = db.taxi_trip.Where(s => s.RezId == mRec.RezID).FirstOrDefault();

								if (taxiTrip != null)
								{
									fleetType = FleetType.Taxi;
									vtodID = taxiTrip.Id;
								}

							}

							#endregion

						}

						mrRec.Confirmation.Add(new Confirmation { ID = vtodID.ToString(), Type = ConfirmationType.Confirmation.ToString() }); //= mRec.ConfirmationNumber;

						if (!string.IsNullOrWhiteSpace(mRec.DropOffLocation) && mRec.DropOffLocation.Trim().ToLower() != "as directed")
						{
							mrRec.DropOffLocation = string.Format("{0} {1} {2} {3}", mRec.DropOffLocation, mRec.DropOffCity, mRec.DropOffState, mRec.DropOffZip);
							mrRec.DropoffUnitNumber = mRec.DropOffUnitNumber;
							mrRec.DropOffAddress = mRec.DropOffAddress;
                            mrRec.DropOffCity = mRec.DropOffCity;
                            mrRec.DropOffState = mRec.DropOffState;
                            mrRec.DropOffZipCode = mRec.DropOffZip;
                            if (sdsTrip != null)
                            {
                                if(sdsTrip.sds_trip.DropoffLocationLatitude!=null)
                                mrRec.DropoffLatitude = sdsTrip.sds_trip.DropoffLocationLatitude.ToString();
                                if (sdsTrip.sds_trip.DropoffLocationLongitude != null)
                                    mrRec.DropoffLongitude= sdsTrip.sds_trip.DropoffLocationLongitude.ToString();
                            }
						}
						else if (string.IsNullOrWhiteSpace(mRec.DropOffLocation))
						{
							mrRec.DropOffLocation = "As directed";
							mrRec.DropoffUnitNumber = "";
							mrRec.DropOffAddress = "";
                            mrRec.DropOffCity = "";
                            mrRec.DropOffState ="";
                            mrRec.DropOffZipCode = "";
                        }
						else
						{
							mrRec.DropOffLocation = mRec.DropOffLocation;
							mrRec.DropoffUnitNumber = mRec.DropOffUnitNumber;
							mrRec.DropOffAddress = mRec.DropOffAddress;
                            mrRec.DropOffCity = mRec.DropOffCity;
                            mrRec.DropOffState = mRec.DropOffState;
                            mrRec.DropOffZipCode = mRec.DropOffZip;
                            if (sdsTrip != null)
                            {
                                if (sdsTrip.sds_trip.DropoffLocationLatitude != null)
                                    mrRec.DropoffLatitude = sdsTrip.sds_trip.DropoffLocationLatitude.ToString();
                                if (sdsTrip.sds_trip.DropoffLocationLongitude != null)
                                    mrRec.DropoffLongitude = sdsTrip.sds_trip.DropoffLocationLongitude.ToString();
                            }
                        }

						mrRec.LastName = mRec.LastName;
						mrRec.PhoneNumber = mRec.PhoneNumber;
						mrRec.PickMeUpNow = mRec.PickMeUpNow;
						mrRec.PickupLocation = string.Format("{0} {1} {2} {3}", mRec.PickupLocation, mRec.PickupCity, mRec.PickupState, mRec.PickupZip);
						mrRec.PickupAddress = mRec.PickupAddress;
						mrRec.PickupUnitNumber = mRec.PickupUnitNumber;
                        mrRec.DropOffCity = mRec.PickupCity;
                        mrRec.DropOffState = mRec.PickupState;
                        mrRec.DropOffZipCode = mRec.PickupZip;
                        mrRec.PickupTime = mRec.PickupTime.ToString();
						mrRec.PostalCode = mRec.PostalCode;
						mrRec.WebServiceState = webserviceState;
                        if (sdsTrip != null)
                        {
                            if (sdsTrip.sds_trip.PickupLocationLatitude != null)
                                mrRec.PickupLatitude = sdsTrip.sds_trip.PickupLocationLatitude.ToString();
                            if (sdsTrip.sds_trip.PickupLocationLongitude != null)
                                mrRec.PickupLongitude = sdsTrip.sds_trip.PickupLocationLongitude.ToString();
                        }
                        #region Wrap the status
                        //status.Value = vtod_domain.ConvertSDSTripStatus(sdsStatus.Status);
                        mrRec.StatusCode = vtod_domain.ConvertSDSTripStatus(mRec.DispatchStatus);
						#endregion


						#region Convert status for front end device

						//mrRec.StatusCode = vtod_domain.ConvertSDSTripStatusMessageForFrontEndDevice(token.Username, mRec.StatusCode);
						#endregion

						mrRec.RateQualifier = new RateQualifier { RateQualifierValue = fleetType.ToString() };

						#region Dispatch contact info
						var companyContact = new Contact
						{
							Type = ContactType.Dispatch.ToString(),
							Name = mRec.CompanyName ?? string.Empty,
							Telephone = string.IsNullOrWhiteSpace(mRec.CompanyPhoneNumber)
									   ? null : new Telephone
									   {
										   PhoneNumber = mRec.CompanyPhoneNumber
									   }
						};

						if (mrRec.Contacts == null)
							mrRec.Contacts = new Contacts();
						if (mrRec.Contacts.Items == null)
							mrRec.Contacts.Items = new List<Contact>();

						mrRec.Contacts.Items.Add(companyContact);
						#endregion
						result.MemberReservationsList.Add(mrRec);
					}

				}
				#endregion

				#region Process Non SDS Trips (Taxi, non_sds_ecar, Aleph, ...)
				if (sdsResult.VtodReservations != null && sdsResult.VtodReservations.Any())
				{

					foreach (var mRec in sdsResult.VtodReservations)
					{
						UDI.VTOD.Domain.Taxi.TaxiUtility u = new Taxi.TaxiUtility(logger);
						//var trip = u.GetTaxiTrip(mRec.TripID);
						vtod_trip trip = null;// = u.GetTaxiTrip(mRec.TripID);
						using (var db = new DataAccess.VTOD.VTODEntities())
						{
							trip = db.vtod_trip.Include("taxi_trip").Include("ecar_trip").Include("van_trip").Where(s => s.Id == mRec.TripID).FirstOrDefault();
						}

						//using (var db = new DataAccess.VTOD.VTODEntities())
						//{
						//var trip = (taxi_trip)db.vtod_trip.Where(s => s.Id == mRec.TripID).FirstOrDefault();

						if (trip != null && trip.Id > 0 && trip.FinalStatus.Trim().ToLower() != "error")
						{
                            if (
                                trip.FleetType.ToLower() == Common.DTO.Enum.FleetType.Taxi.ToString().ToLower()
                                //&&
                                //trip.FinalStatus.ToLower() != "canceled" && trip.FinalStatus.ToLower() != "cancel"
                                )
                            {
                                mrRec = new MembershipReservation();
                                mrRec.Confirmation = new List<Confirmation>();
                                long vtodID = trip.Id;
                                #region Trip To be Modified
                                if (trip.FleetTripCode == FleetTripCode.Taxi_MTDATA)
                                {
                                    if (trip.PickupDateTimeUTC > DateTime.UtcNow)
                                    {
                                        if (trip.FinalStatus == UDI.VTOD.Domain.Taxi.Const.TaxiTripStatusType.DispatchPending)
                                        {
                                            mrRec.IsModify = true;
                                        }
                                        else
                                        {
                                            mrRec.IsModify = false;
                                        }

                                    }
                                    else
                                    {
                                        mrRec.IsModify = false;
                                    }

                                }
                                else
                                {
                                    mrRec.IsModify = false;
                                }

                                #endregion
                                mrRec.Confirmation.Add(new Confirmation { ID = trip.taxi_trip.DispatchTripId, Type = ConfirmationType.DispatchConfirmation.ToString() }); //= mRec.ConfirmationNumber;
                                mrRec.Confirmation.Add(new Confirmation { ID = vtodID.ToString(), Type = ConfirmationType.Confirmation.ToString() }); //= mRec.ConfirmationNumber;

                                if (string.IsNullOrWhiteSpace(trip.taxi_trip.DropOffFullAddress))
                                {
                                    mrRec.DropOffLocation = "As directed";
                                    mrRec.DropoffUnitNumber = "";
                                    mrRec.DropOffAddress = "";
                                    mrRec.DropOffCity = "";
                                    mrRec.DropOffCountry = "";
                                    mrRec.DropOffState = "";
                                    mrRec.DropOffAddressType = "";
                                    mrRec.DropOffZipCode = "";
                                    mrRec.DropOffAddressType = "";
                                    mrRec.DropOffStreetName = "";
                                    mrRec.DropOffStreetNumber = "";
                                }
                                else
                                {
                                    mrRec.DropOffLocation = trip.taxi_trip.DropOffFullAddress;
                                    mrRec.DropoffUnitNumber = trip.taxi_trip.DropOffAptNo;
                                    mrRec.DropOffAddress = trip.taxi_trip.DropOffFullAddress;
                                    if (trip.taxi_trip.DropOffLatitude != null)
                                        mrRec.DropoffLatitude = trip.taxi_trip.DropOffLatitude.ToString();
                                    if (trip.taxi_trip.DropOffLongitude != null)
                                        mrRec.DropoffLongitude = trip.taxi_trip.DropOffLongitude.ToString();
                                    if (!string.IsNullOrWhiteSpace(mrRec.DropoffUnitNumber))
                                        mrRec.DropOffAddress.Replace(mrRec.DropoffUnitNumber, "");
                                    mrRec.DropOffCity = trip.taxi_trip.DropOffCity;
                                    mrRec.DropOffState = trip.taxi_trip.DropOffStateCode;
                                    mrRec.DropOffCountry = trip.taxi_trip.DropOffCountryCode;
                                    mrRec.DropOffZipCode = trip.taxi_trip.DropOffZipCode;
                                    mrRec.DropOffAddressType = trip.taxi_trip.DropOffAddressType;
                                    mrRec.DropOffStreetName = trip.taxi_trip.DropOffStreetName;
                                    mrRec.DropOffStreetNumber = trip.taxi_trip.DropOffStreetNo;
                                }
                                mrRec.LastName = trip.taxi_trip.LastName;
                                mrRec.FirstName = trip.taxi_trip.FirstName;
                                mrRec.PhoneNumber = trip.taxi_trip.PhoneNumber;
                                mrRec.PickMeUpNow = trip.PickMeUpNow.HasValue ? trip.PickMeUpNow.Value : false;
                                mrRec.PickupLocation = trip.taxi_trip.PickupFullAddress;
                                mrRec.PickupAddress = trip.taxi_trip.PickupFullAddress;
                                mrRec.PickupUnitNumber = trip.taxi_trip.PickupAptNo;
                                mrRec.PickupCity = trip.taxi_trip.PickupCity;
                                mrRec.PickupState = trip.taxi_trip.PickupStateCode;
                                mrRec.PickupCountry = trip.taxi_trip.PickupCountryCode;
                                mrRec.PickupZipCode = trip.taxi_trip.PickupZipCode;
                                mrRec.PickupAddressType = trip.taxi_trip.PickupAddressType;
                                mrRec.PickupStreetName = trip.taxi_trip.PickupStreetName;
                                mrRec.PickupStreetNumber = trip.taxi_trip.PickupStreetNo;
                                if (!string.IsNullOrWhiteSpace(mrRec.PickupUnitNumber))
                                    mrRec.PickupAddress.Replace(mrRec.PickupUnitNumber, "");
                                mrRec.PickupTime = trip.PickupDateTime.Value.ToString();
                                if (trip.taxi_trip.PickupLatitude != null)
                                    mrRec.PickupLatitude = trip.taxi_trip.PickupLatitude.ToString();
                                if (trip.taxi_trip.PickupLongitude != null)
                                    mrRec.PickupLongitude = trip.taxi_trip.PickupLongitude.ToString();
                                mrRec.PostalCode = trip.taxi_trip.PickupZipCode;
                                //mrRec.RezId = mRec.RezID;
                                var taxiUtility = new Domain.Taxi.TaxiUtility(logger);

                                mrRec.StatusCode = taxiUtility.ConvertTripStatusMessageForFrontEndDevice(trip.UserID, trip.FinalStatus);//
                                mrRec.RateQualifier = new RateQualifier { RateQualifierValue = Common.DTO.Enum.FleetType.Taxi.ToString() };
                                mrRec.WebServiceState = vtodDomain.Get_WS_State("vtod_trip", vtodID, "CorporateIDs");
                                #region Dispatch contact info
                                taxi_fleet fleetObj = null;
                                using (var db = new DataAccess.VTOD.VTODEntities())
                                {
                                    Int64 fleetId = 0;
                                    taxi_trip tripObj = db.taxi_trip.Where(p => p.Id == trip.Id).FirstOrDefault();
                                    if (tripObj != null)
                                    {
                                        fleetId = tripObj.FleetId;
                                    }
                                    if (fleetId > 0)
                                    {
                                        fleetObj = db.taxi_fleet.Where(p => p.Id == fleetId).FirstOrDefault();
                                    }

                                }
                                if (fleetObj != null)
                                {

                                    var contactDetails = new Contact { Type = Common.DTO.Enum.ContactType.Dispatch.ToString() };
                                    if (!string.IsNullOrWhiteSpace(fleetObj.Name))
                                    {
                                        contactDetails.Name = fleetObj.Name;
                                    }
                                    if (!string.IsNullOrWhiteSpace(fleetObj.PhoneNumber))
                                    {
                                        contactDetails.Telephone = new Telephone { PhoneNumber = fleetObj.PhoneNumber };
                                    }
                                    if (mrRec.Contacts == null)
                                        mrRec.Contacts = new Contacts();
                                    if (mrRec.Contacts.Items == null)
                                        mrRec.Contacts.Items = new List<Contact>();
                                    mrRec.Contacts.Items.Add(contactDetails);
                                }
                                #endregion

                                #region Split Payment Info
                                mrRec.SplitPayment = new SplitPaymentInfo();
                                if (trip.IsSplitPayment != null && trip.IsSplitPayment.Value)
                                {
                                    mrRec.SplitPayment.IsSplitPayment = true;
                                    mrRec.SplitPayment.SplitPaymentStatus = ((SplitPaymentStatus)trip.SplitPaymentStatus).ToString();

                                    mrRec.SplitPayment.MemberA = new SplitPaymentUserInfo
                                    {
                                        MemberID = trip.MemberID,
                                        FirstName = trip.taxi_trip.FirstName,
                                        LastName = trip.taxi_trip.LastName
                                    };
                                    mrRec.SplitPayment.MemberB = new SplitPaymentUserInfo
                                    {
                                        MemberID = trip.SplitPaymentMemberB,
                                        FirstName = trip.taxi_trip.MemberBFirstName,
                                        LastName = trip.taxi_trip.MemberBLastName
                                    };
                                }
                                else
                                {
                                    mrRec.SplitPayment.IsSplitPayment = false;
                                }
                                #endregion

                                result.MemberReservationsList.Add(mrRec);

                            }
                            else if (
                                trip.FleetType.ToLower() == Common.DTO.Enum.FleetType.ExecuCar.ToString().ToLower() && trip.ecar_trip != null
                                )
                            {
                                mrRec = new MembershipReservation();
                                mrRec.Confirmation = new List<Confirmation>();
                                long vtodID = trip.Id;
                                if (trip.ecar_trip.DispatchTripId.Contains("##") == false)
                                {
                                    mrRec.Confirmation.Add(new Confirmation { ID = trip.ecar_trip.DispatchTripId, Type = ConfirmationType.DispatchConfirmation.ToString() }); //= mRec.ConfirmationNumber;

                                }
                                else if (trip.ecar_trip.DispatchTripId.Contains("##") == true)
                                {
                                    mrRec.Confirmation.Add(new Confirmation { ID = trip.ecar_trip.DispatchTripId.Substring(trip.ecar_trip.DispatchTripId.IndexOf("##") + 2), Type = ConfirmationType.DispatchConfirmation.ToString() }); //= mRec.ConfirmationNumber;

                                }
                                mrRec.Confirmation.Add(new Confirmation { ID = vtodID.ToString(), Type = ConfirmationType.Confirmation.ToString() }); //= mRec.ConfirmationNumber;

                                if (string.IsNullOrWhiteSpace(trip.ecar_trip.DropOffFullAddress))
                                {
                                    mrRec.DropOffLocation = "As directed";
                                    mrRec.DropoffUnitNumber = "";
                                    mrRec.DropOffAddress = "";
                                }
                                else
                                {
                                    mrRec.DropOffLocation = trip.ecar_trip.DropOffFullAddress;
                                    mrRec.DropoffUnitNumber = trip.ecar_trip.DropOffAptNo; ;
                                    mrRec.DropOffAddress = trip.ecar_trip.DropOffFullAddress;
                                    if (mrRec.DropoffUnitNumber != null)
                                        mrRec.DropOffAddress.Replace(mrRec.DropoffUnitNumber, "");
                                    if(trip.ecar_trip.DropOffLatitude!=null)
                                    mrRec.DropoffLatitude = trip.ecar_trip.DropOffLatitude.ToString();
                                    if (trip.ecar_trip.DropOffLongitude != null)
                                        mrRec.DropoffLongitude = trip.ecar_trip.DropOffLongitude.ToString();
                                }
                                mrRec.LastName = trip.ecar_trip.LastName;
                                mrRec.PhoneNumber = trip.ecar_trip.PhoneNumber;
                                mrRec.PickMeUpNow = trip.PickMeUpNow.HasValue ? trip.PickMeUpNow.Value : false;
                                mrRec.PickupLocation = trip.ecar_trip.PickupFullAddress;
                                mrRec.PickupAddress = trip.ecar_trip.PickupFullAddress;
                                mrRec.PickupUnitNumber = trip.ecar_trip.PickupAptNo;
                                if (trip.ecar_trip.PickupLatitude != null)
                                    mrRec.PickupLatitude = trip.ecar_trip.PickupLatitude.ToString();
                                if (trip.ecar_trip.PickupLongitude != null)
                                    mrRec.PickupLongitude = trip.ecar_trip.PickupLongitude.ToString();
                                if (mrRec.PickupUnitNumber != null)
                                    mrRec.PickupAddress.Replace(mrRec.PickupUnitNumber, "");
                                mrRec.PickupTime = trip.PickupDateTime.Value.ToString();
                                mrRec.PostalCode = trip.ecar_trip.PickupZipCode;
                                //mrRec.RezId = mRec.RezID;
                                var ecarUtility = new Domain.ECar.ECarUtility(logger);


                                mrRec.WebServiceState = vtodDomain.Get_WS_State("vtod_trip", vtodID, "CorporateIDs");
                                mrRec.RateQualifier = new RateQualifier { RateQualifierValue = Common.DTO.Enum.FleetType.ExecuCar.ToString() };
                                var status = trip.FinalStatus;
                                try
                                {
                                    if (trip.FinalStatus.ToLower() != "completed" || trip.FinalStatus.ToLower() != "canceled" || trip.FinalStatus.ToLower() != "error")
                                    {
                                        var otaStatus = new OTA_GroundResRetrieveRQ();
                                        otaStatus.Reference = new List<Reference>();
                                        var reference = new Reference { ID = trip.Id.ToString(), Type = Common.DTO.Enum.ConfirmationType.Confirmation.ToString() };
                                        otaStatus.Reference.Add(reference);
                                        otaStatus.TPA_Extensions = new TPA_Extensions();
                                        otaStatus.TPA_Extensions.RateQualifiers = new List<RateQualifier>();
                                        var rateQualifier = new RateQualifier { RateQualifierValue = Common.DTO.Enum.FleetType.ExecuCar.ToString() };
                                        var specialinputs = mrRec.WebServiceState.JsonDeserialize<List<NameValue>>();
                                        rateQualifier.SpecialInputs = specialinputs;
                                        var ecarDomain = new ECar.ECarDomain();
                                        var statusRS = ecarDomain.Status(token, otaStatus);
                                        status = statusRS.TPA_Extensions.Statuses.Status.First().Value;
                                    }
                                }
                                catch
                                { }

                                mrRec.StatusCode = status;

                                #region Dispatch contact info
                                string jsonCompanyInfo = vtod_domain.Get_WS_State("vtod_trip", vtodID, "CompanyInfo");
                                if (!string.IsNullOrWhiteSpace(jsonCompanyInfo))
                                {
                                    var companyInfo = jsonCompanyInfo.JsonDeserialize<AlephCompanyInfo>();
                                    if (companyInfo != null)
                                    {
                                        var companyContact = new Contact
                                        {
                                            Type = ContactType.Dispatch.ToString(),
                                            Name = companyInfo.company_name ?? string.Empty,
                                            Telephone = string.IsNullOrWhiteSpace(companyInfo.company_phone_number)
                                                ? null : new Telephone
                                                {
                                                    PhoneNumber = companyInfo.company_phone_number
                                                }
                                        };

                                        if (mrRec.Contacts == null)
                                            mrRec.Contacts = new Contacts();
                                        if (mrRec.Contacts.Items == null)
                                            mrRec.Contacts.Items = new List<Contact>();

                                        mrRec.Contacts.Items.Add(companyContact);
                                    }
                                }
                                else
                                {
                                    ecar_fleet fleetObj = null;
                                    using (var db = new DataAccess.VTOD.VTODEntities())
                                    {
                                        Int64 fleetId = 0;
                                        ecar_trip tripObj = db.ecar_trip.Where(p => p.Id == trip.Id).FirstOrDefault();
                                        if (tripObj != null)
                                        {
                                            fleetId = tripObj.FleetId;
                                        }
                                        if (fleetId > 0)
                                        {
                                            fleetObj = db.ecar_fleet.Where(p => p.Id == fleetId).FirstOrDefault();
                                        }

                                    }
                                    if (fleetObj != null)
                                    {

                                        var contactDetails = new Contact { Type = Common.DTO.Enum.ContactType.Dispatch.ToString() };
                                        if (!string.IsNullOrWhiteSpace(fleetObj.Name))
                                        {
                                            contactDetails.Name = fleetObj.Name;
                                        }
                                        if (!string.IsNullOrWhiteSpace(fleetObj.PhoneNumber))
                                        {
                                            contactDetails.Telephone = new Telephone { PhoneNumber = fleetObj.PhoneNumber };
                                        }
                                        if (mrRec.Contacts == null)
                                            mrRec.Contacts = new Contacts();
                                        if (mrRec.Contacts.Items == null)
                                            mrRec.Contacts.Items = new List<Contact>();
                                        mrRec.Contacts.Items.Add(contactDetails);
                                    }
                                }
                                #endregion
                               
                                result.MemberReservationsList.Add(mrRec);
                            }
                            if (
                                trip.FleetType.ToLower() == Common.DTO.Enum.FleetType.Van.ToString().ToLower()
                                //&&
                                //trip.FinalStatus.ToLower() != "canceled" && trip.FinalStatus.ToLower() != "cancel"
                                )
                            {
                                mrRec = new MembershipReservation();
                                mrRec.Confirmation = new List<Confirmation>();
                                long vtodID = trip.Id;

                                mrRec.Confirmation.Add(new Confirmation { ID = trip.van_trip.DispatchTripId, Type = ConfirmationType.DispatchConfirmation.ToString() }); //= mRec.ConfirmationNumber;
                                mrRec.Confirmation.Add(new Confirmation { ID = vtodID.ToString(), Type = ConfirmationType.Confirmation.ToString() }); //= mRec.ConfirmationNumber;

                                if (string.IsNullOrWhiteSpace(trip.van_trip.DropOffFullAddress))
                                {
                                    mrRec.DropOffLocation = "As directed";
                                    mrRec.DropoffUnitNumber = "";
                                    mrRec.DropOffAddress = "";
                                }
                                else
                                {
                                    mrRec.DropOffLocation = trip.van_trip.DropOffFullAddress;
                                    mrRec.DropoffUnitNumber = trip.van_trip.DropOffAptNo;
                                    mrRec.DropOffAddress = trip.van_trip.DropOffFullAddress;
                                    if (trip.van_trip.DropOffLatitude != null)
                                        mrRec.DropoffLatitude = trip.van_trip.DropOffLatitude.ToString();
                                    if (trip.van_trip.DropOffLongitude != null)
                                        mrRec.DropoffLongitude = trip.van_trip.DropOffLongitude.ToString();
                                    if (!string.IsNullOrWhiteSpace(mrRec.DropoffUnitNumber))
                                        mrRec.DropOffAddress.Replace(mrRec.DropoffUnitNumber, "");
                                }
                                mrRec.LastName = trip.van_trip.LastName;
                                mrRec.PhoneNumber = trip.van_trip.PhoneNumber;
                                mrRec.PickMeUpNow = trip.PickMeUpNow.HasValue ? trip.PickMeUpNow.Value : false;
                                mrRec.PickupLocation = trip.van_trip.PickupFullAddress;
                                mrRec.PickupAddress = trip.van_trip.PickupFullAddress;
                                mrRec.PickupUnitNumber = trip.van_trip.PickupAptNo;
                                if (trip.van_trip.PickupLatitude != null)
                                    mrRec.PickupLatitude = trip.van_trip.PickupLatitude.ToString();
                                if (trip.van_trip.PickupLongitude != null)
                                    mrRec.PickupLongitude = trip.van_trip.PickupLongitude.ToString();
                                if (!string.IsNullOrWhiteSpace(mrRec.PickupUnitNumber))
                                    mrRec.PickupAddress.Replace(mrRec.PickupUnitNumber, "");
                                mrRec.PickupTime = trip.PickupDateTime.Value.ToString();
                                mrRec.PostalCode = trip.van_trip.PickupZipCode;
                                //mrRec.RezId = mRec.RezID;
                                var vanUtility = new Domain.Van.VanUtility(logger);

                                mrRec.StatusCode = vanUtility.ConvertTripStatusMessageForFrontEndDevice(trip.UserID, trip.FinalStatus);//
                                mrRec.RateQualifier = new RateQualifier { RateQualifierValue = Common.DTO.Enum.FleetType.Van.ToString() };
                                mrRec.WebServiceState = vtodDomain.Get_WS_State("vtod_trip", vtodID, "CorporateIDs");
                                #region Dispatch contact info
                                van_fleet fleetObj = null;
                                using (var db = new DataAccess.VTOD.VTODEntities())
                                {
                                    Int64 fleetId = 0;
                                    van_trip tripObj = db.van_trip.Where(p => p.Id == trip.Id).FirstOrDefault();
                                    if (tripObj != null)
                                    {
                                        fleetId = tripObj.FleetId;
                                    }
                                    if (fleetId > 0)
                                    {
                                        fleetObj = db.van_fleet.Where(p => p.Id == fleetId).FirstOrDefault();
                                    }

                                }
                                if (fleetObj != null)
                                {

                                    var contactDetails = new Contact { Type = Common.DTO.Enum.ContactType.Dispatch.ToString() };
                                    if (!string.IsNullOrWhiteSpace(fleetObj.Name))
                                    {
                                        contactDetails.Name = fleetObj.Name;
                                    }
                                    if (!string.IsNullOrWhiteSpace(fleetObj.PhoneNumber))
                                    {
                                        contactDetails.Telephone = new Telephone { PhoneNumber = fleetObj.PhoneNumber };
                                    }
                                    if (mrRec.Contacts == null)
                                        mrRec.Contacts = new Contacts();
                                    if (mrRec.Contacts.Items == null)
                                        mrRec.Contacts.Items = new List<Contact>();
                                    mrRec.Contacts.Items.Add(contactDetails);
                                }
                                #endregion
                                result.MemberReservationsList.Add(mrRec);

                            }

							//}
						}




					}

				}
				#endregion

				#region Get Trips for more fields

				#endregion

				#region Sort
				if (result.MemberReservationsList != null && result.MemberReservationsList.Any())
				{
					try
					{
						result.MemberReservationsList = result.MemberReservationsList.OrderByDescending(s => DateTime.Parse(s.PickupTime)).ToList();
					}
					catch
					{ }
				}
				#endregion

				result.Success = new Success();

				#region Common
				result.EchoToken = input.EchoToken;
				result.PrimaryLangID = input.PrimaryLangID;
				result.Target = input.Target;
				result.Version = input.Version;
				#endregion
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("MembershipDomain.MemberGetReservation:: {0}", ex.ToString() + ex.StackTrace);

				#region Throw MembershipException
				throw VtodException.CreateException(ExceptionType.Membership, 7011);//throw new MembershipException(Messages.Membership_MemberGetReservation);
				#endregion
			}
		}

		internal MemberGetReservationRS MemberGetReservationTripReminder(TokenRS token, MemberGetReservationRQ input)
		{
			try
			{
				var numberOfReturnedMemberReservation = 25;

				try
				{
					numberOfReturnedMemberReservation = System.Configuration.ConfigurationManager.AppSettings["NumberOfReturnedMemberReservation"].ToInt32();
					logger.InfoFormat("Number of Reservations Retrieved : {0}:", numberOfReturnedMemberReservation);
				}
				catch (Exception e)
				{
					logger.ErrorFormat(e.Message);
				}

				MemberGetReservationRS result = null;
				MembershipReservation mrRec = null;
				var membershipController = new UDI.SDS.MembershipController(token, TrackTime);
				string webserviceState = null;
				var vtodDomain = new VTOD.VTODDomain(); vtodDomain.TrackTime = TrackTime;
				
				#region Output Conversion
				result = new MemberGetReservationRS();
				result.MemberReservationsList = new List<MembershipReservation>();
				#region Process SDS Trips
				using (var db = new DataAccess.VTOD.VTODEntities())
				{
                    var Result = db.sp_vtod_TripReminder(input.MemberId);
					if (Result != null)
					{

						foreach (var mRec in Result)
						{
							mrRec = new MembershipReservation();
							mrRec.Confirmation = new List<Confirmation>();
							long vtodID = mRec.ID.ToInt64();
							FleetType fleetType = FleetType.Unkown;
							if (mRec.FleetTripCode != null)
							{
								if (mRec.FleetTripCode == FleetTripCode.SDS_Shuttle_Airport || mRec.FleetTripCode == FleetTripCode.SDS_Shuttle_Charter || mRec.FleetTripCode == FleetTripCode.SDS_Shuttle_Hourly)
								{
									fleetType = FleetType.ExecuCar;
								}
								else if (mRec.FleetTripCode == FleetTripCode.Taxi_CSCI || mRec.FleetTripCode == FleetTripCode.Taxi_MTDATA || mRec.FleetTripCode == FleetTripCode.Taxi_SDS || mRec.FleetTripCode == FleetTripCode.Taxi_UDI33)
								{
									fleetType = FleetType.Taxi;
								}
								else if (mRec.FleetTripCode == FleetTripCode.Ecar_Aleph || mRec.FleetTripCode == FleetTripCode.Ecar_GreenTomato || mRec.FleetTripCode == FleetTripCode.Ecar_Texas)
                                {
									fleetType = FleetType.ExecuCar;
								}
                                else if (mRec.FleetTripCode == FleetTripCode.Van_CSCI)
                                {
                                    fleetType = FleetType.Van;
                                }
							}
							if (mRec.DispatchTripId != null)
							{
								if (mRec.DispatchTripId.Contains("##") == false)
								{
									mrRec.Confirmation.Add(new Confirmation { ID = mRec.ConfirmationNumber, Type = ConfirmationType.DispatchConfirmation.ToString() }); //= mRec.ConfirmationNumber;

								}
								else if (mRec.DispatchTripId.Contains("##") == true)
								{
									mrRec.Confirmation.Add(new Confirmation { ID = mRec.DispatchTripId.Substring(mRec.DispatchTripId.IndexOf("##") + 2), Type = ConfirmationType.DispatchConfirmation.ToString() }); //= mRec.ConfirmationNumber;

								}
							}
							mrRec.Confirmation.Add(new Confirmation { ID = vtodID.ToString(), Type = ConfirmationType.Confirmation.ToString() }); //= mRec.ConfirmationNumber;
							#region Track
							if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Membership_Domain, "Call");
							#endregion

							#region Track
							if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_DB, "db.SP_SDS_GetVtodId(mRec.RezID).ToList()");
							#endregion



							if (!string.IsNullOrWhiteSpace(mRec.DropoffLocation) && mRec.DropoffLocation.Trim().ToLower() != "as directed")
							{
								mrRec.DropOffLocation = string.Format("{0} {1} {2} {3}", mRec.DropoffLocation, mRec.DropOffCity, mRec.DropOffStateCode, mRec.DropOffZipCode);
								mrRec.DropoffUnitNumber = mRec.DropOffBuilding;
								mrRec.DropOffAddress = mRec.DropOffFullAddress;
                                mrRec.DropOffCity = mRec.DropOffCity;
                                mrRec.DropOffCountry = mRec.DropOffCountry;
                                mrRec.DropOffState = mRec.DropOffStateCode;
                                mrRec.DropOffAddressType = mRec.DropOffAddressType;
                                mrRec.DropOffZipCode = mRec.DropOffZipCode;
                                mrRec.DropOffAddressType = mRec.DropOffAddressType;
                                mrRec.DropOffStreetName = mRec.DropOffStreetName;
                                mrRec.DropOffStreetNumber = mRec.DropOffStreetNumber;
                            }
							else if (string.IsNullOrWhiteSpace(mRec.DropoffLocation))
							{
								mrRec.DropOffLocation = "As directed";
								mrRec.DropoffUnitNumber = "";
								mrRec.DropOffAddress = "";
                                mrRec.DropOffCity = "";
                                mrRec.DropOffCountry = "";
                                mrRec.DropOffState = "";
                                mrRec.DropOffAddressType = "";
                                mrRec.DropOffZipCode = "";
                                mrRec.DropOffStreetName = "";
                                mrRec.DropOffStreetNumber = "";
                            }
							else
							{
								mrRec.DropOffLocation = mRec.DropoffLocation;
								mrRec.DropoffUnitNumber = mRec.DropOffBuilding;
								mrRec.DropOffAddress = mRec.DropOffFullAddress;
                                mrRec.DropOffCountry = mRec.DropOffCountry;
                                mrRec.DropOffState = mRec.DropOffStateCode;
                                mrRec.DropOffAddressType = mRec.DropOffAddressType;
                                mrRec.DropOffZipCode = mRec.DropOffZipCode;
                                mrRec.DropOffAddressType = mRec.DropOffAddressType;
                                mrRec.DropOffStreetName = mRec.DropOffStreetName;
                                mrRec.DropOffStreetNumber = mRec.DropOffStreetNumber;
                                if (mRec.DropoffLongitude != null)
                                    mrRec.DropoffLongitude = mRec.DropoffLongitude.ToString();
                                if (mRec.DropoffLatitude != null)
                                    mrRec.DropoffLatitude = mRec.DropoffLatitude.ToString();

                            }

							mrRec.LastName = mRec.LastName;
							mrRec.PhoneNumber = mRec.PhoneNumber;
							mrRec.PickMeUpNow = mRec.PickMeUpNow.ToBool();
							mrRec.PickupLocation = string.Format("{0} {1} {2} {3}", mRec.PickupLocation, mRec.PickupCity, mRec.PickupStateCode, mRec.PickupZipCode);
							mrRec.PickupAddress = mRec.PickupFullAddress;
							mrRec.PickupUnitNumber = mRec.PickupBuilding;
							mrRec.PickupTime = mRec.PickupDateTime.ToString();
                            mrRec.PickupCountry = mRec.PickupCountryCode;
                            mrRec.PickupState = mRec.PickupStateCode;
                            mrRec.PickupAddressType = mRec.PickupAddressType;
                            mrRec.PickupZipCode = mRec.PickupZipCode;
                            mrRec.PickupAddressType = mRec.PickupAddressType;
                            mrRec.PickupStreetName = mRec.PickupStreetName;
                            mrRec.PickupStreetNumber = mRec.PickupStreetNumber;
                            if (mRec.PickupLongitude != null)
                                mrRec.PickupLongitude = mRec.PickupLongitude.ToString();
                            if (mRec.PickupLatitude != null)
                                mrRec.PickupLatitude = mRec.PickupLatitude.ToString();

                            mrRec.PostalCode = mRec.PickupZipCode;
							if (mRec.FleetTripCode != null)
							{
								if (mRec.FleetTripCode == FleetTripCode.Taxi_CSCI || mRec.FleetTripCode == FleetTripCode.Taxi_MTDATA || mRec.FleetTripCode == FleetTripCode.Taxi_SDS || mRec.FleetTripCode == FleetTripCode.Taxi_UDI33)
								{
									#region Dispatch contact info For Taxi
									taxi_fleet fleetObj = null;
									if (mRec.FleetId.ToInt64() > 0)
									{
										fleetObj = db.taxi_fleet.Where(p => p.Id == mRec.FleetId.ToInt64()).FirstOrDefault();
									}
									var contactDetails = new Contact { Type = Common.DTO.Enum.ContactType.Dispatch.ToString() };
									if (fleetObj != null)
									{
										if (!string.IsNullOrWhiteSpace(fleetObj.Name))
										{
											contactDetails.Name = fleetObj.Name;
										}
										if (!string.IsNullOrWhiteSpace(fleetObj.PhoneNumber))
										{
											contactDetails.Telephone = new Telephone { PhoneNumber = fleetObj.PhoneNumber };
										}
									}
									if (mrRec.Contacts == null)
										mrRec.Contacts = new Contacts();
									if (mrRec.Contacts.Items == null)
										mrRec.Contacts.Items = new List<Contact>();
									mrRec.Contacts.Items.Add(contactDetails);
                                    #endregion
                                    #region Trip To be Modified
                                    if (mRec.FleetTripCode == FleetTripCode.Taxi_MTDATA)
                                    {
                                        if (mRec.PickupDateTime > DateTime.Now)
                                        {
                                            if (mRec.FinalStatus == UDI.VTOD.Domain.Taxi.Const.TaxiTripStatusType.DispatchPending)
                                            {
                                                mrRec.IsModify = true;
                                            }
                                            else
                                            {
                                                mrRec.IsModify = false;
                                            }

                                        }
                                        else
                                        {
                                            mrRec.IsModify = false;
                                        }

                                        #region Split Payment Info
                                        var trip = db.vtod_trip.Include("taxi_trip").FirstOrDefault(s => s.Id == mRec.ID);
                                        if (trip != null)
                                        {
                                            mrRec.SplitPayment = new SplitPaymentInfo();
                                            if (trip.IsSplitPayment != null && trip.IsSplitPayment.Value)
                                            {
                                                mrRec.SplitPayment.IsSplitPayment = true;
                                                mrRec.SplitPayment.SplitPaymentStatus = ((SplitPaymentStatus)trip.SplitPaymentStatus).ToString();

                                                mrRec.SplitPayment.MemberA = new SplitPaymentUserInfo
                                                {
                                                    MemberID = trip.MemberID,
                                                    FirstName = trip.taxi_trip.FirstName,
                                                    LastName = trip.taxi_trip.LastName
                                                };
                                                mrRec.SplitPayment.MemberB = new SplitPaymentUserInfo
                                                {
                                                    MemberID = trip.SplitPaymentMemberB,
                                                    FirstName = trip.taxi_trip.MemberBFirstName,
                                                    LastName = trip.taxi_trip.MemberBLastName
                                                };
                                            }
                                            else
                                            {
                                                mrRec.SplitPayment.IsSplitPayment = false;
                                            }
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        mrRec.IsModify = false;
                                    }

                                    #endregion
                                    #region Status
                                    var taxiUtility = new Domain.Taxi.TaxiUtility(logger);
									mrRec.StatusCode = taxiUtility.ConvertTripStatusMessageForFrontEndDevice(mRec.UserID.ToInt32(), mRec.FinalStatus);//
									#endregion

									#region WebServiceState
									webserviceState = vtodDomain.Get_WS_State("vtod_trip", vtodID, "CorporateIDs");
									mrRec.WebServiceState = webserviceState;
									#endregion
								}


								else if (mRec.FleetTripCode == FleetTripCode.Ecar_Aleph || mRec.FleetTripCode == FleetTripCode.Ecar_GreenTomato || mRec.FleetTripCode == FleetTripCode.Ecar_Texas)
								{
									#region Dispatch contact info
									string jsonCompanyInfo = vtod_domain.Get_WS_State("vtod_trip", vtodID, "CompanyInfo");
									if (!string.IsNullOrWhiteSpace(jsonCompanyInfo))
									{
										var companyInfo = jsonCompanyInfo.JsonDeserialize<AlephCompanyInfo>();
										if (companyInfo != null)
										{
											var companyContact = new Contact
											{
												Type = ContactType.Dispatch.ToString(),
												Name = companyInfo.company_name ?? string.Empty,
												Telephone = string.IsNullOrWhiteSpace(companyInfo.company_phone_number)
													? null : new Telephone
													{
														PhoneNumber = companyInfo.company_phone_number
													}
											};

											if (mrRec.Contacts == null)
												mrRec.Contacts = new Contacts();
											if (mrRec.Contacts.Items == null)
												mrRec.Contacts.Items = new List<Contact>();

											mrRec.Contacts.Items.Add(companyContact);
										}
									}
                                    else
                                    {
                                        ecar_fleet fleetObj = null;
                                        using (var db1 = new DataAccess.VTOD.VTODEntities())
                                        {
                                            Int64 fleetId = 0;
                                            ecar_trip tripObj = db1.ecar_trip.Where(p => p.Id == vtodID).FirstOrDefault();
                                            if (tripObj != null)
                                            {
                                                fleetId = tripObj.FleetId;
                                            }
                                            if (fleetId > 0)
                                            {
                                                fleetObj = db1.ecar_fleet.Where(p => p.Id == fleetId).FirstOrDefault();
                                            }

                                        }
                                        if (fleetObj != null)
                                        {

                                            var contactDetails = new Contact { Type = Common.DTO.Enum.ContactType.Dispatch.ToString() };
                                            if (!string.IsNullOrWhiteSpace(fleetObj.Name))
                                            {
                                                contactDetails.Name = fleetObj.Name;
                                            }
                                            if (!string.IsNullOrWhiteSpace(fleetObj.PhoneNumber))
                                            {
                                                contactDetails.Telephone = new Telephone { PhoneNumber = fleetObj.PhoneNumber };
                                            }
                                            if (mrRec.Contacts == null)
                                                mrRec.Contacts = new Contacts();
                                            if (mrRec.Contacts.Items == null)
                                                mrRec.Contacts.Items = new List<Contact>();
                                            mrRec.Contacts.Items.Add(contactDetails);
                                        }
                                    }
                                    #endregion
                                    #region Trip To be Modified
                                    if (mRec.FleetTripCode == FleetTripCode.Ecar_Aleph)
                                    {
                                        DateTime? tripTime = mRec.PickupDateTime.ToDateTime();
                                        if (tripTime > DateTime.UtcNow)
                                        {
                                            if (mRec.FinalStatus == UDI.VTOD.Domain.ECar.Const.ECarTripStatusType.DispatchPending)
                                            {
                                                mrRec.IsModify = true;
                                            }
                                            else
                                            {
                                                mrRec.IsModify = false;
                                            }

                                        }
                                        else
                                        {
                                            mrRec.IsModify = false;
                                        }

                                    }
                                    else
                                    {
                                        mrRec.IsModify = false;
                                    }

                                    #endregion
                                    #region Status for Ecar
                                    var status = mRec.FinalStatus;
									mrRec.WebServiceState = vtodDomain.Get_WS_State("vtod_trip", vtodID, "CorporateIDs");
									var ecarUtility = new Domain.ECar.ECarUtility(logger);
									try
									{
										if (mRec.FinalStatus.ToLower() != "completed" || mRec.FinalStatus.ToLower() != "canceled" || mRec.FinalStatus.ToLower() != "error")
										{
											var otaStatus = new OTA_GroundResRetrieveRQ();
											otaStatus.Reference = new List<Reference>();
											var reference = new Reference { ID = mRec.ID.ToString(), Type = Common.DTO.Enum.ConfirmationType.Confirmation.ToString() };
											otaStatus.Reference.Add(reference);
											otaStatus.TPA_Extensions = new TPA_Extensions();
											otaStatus.TPA_Extensions.RateQualifiers = new List<RateQualifier>();
											var rateQualifier = new RateQualifier { RateQualifierValue = Common.DTO.Enum.FleetType.ExecuCar.ToString() };
											var specialinputs = mrRec.WebServiceState.JsonDeserialize<List<NameValue>>();
											rateQualifier.SpecialInputs = specialinputs;
                                            var ecarDomain = new ECar.ECarDomain();
                                            var statusRS = ecarDomain.Status(token, otaStatus);
											status = statusRS.TPA_Extensions.Statuses.Status.First().Value;
										}
									}
									catch
									{ }

									mrRec.StatusCode = ecarUtility.ConvertTripStatusMessageForFrontEndDevice(mRec.UserID.ToInt32(), status);

									#endregion
								}

								else if (mRec.FleetTripCode == FleetTripCode.SDS_Shuttle_Airport || mRec.FleetTripCode == FleetTripCode.SDS_Shuttle_Charter || mRec.FleetTripCode == FleetTripCode.SDS_Shuttle_Hourly)
								{

									#region Wrap the status
									mrRec.StatusCode = vtod_domain.ConvertSDSTripStatus(mRec.FinalStatus);
									#endregion

									#region WebServiceState
									webserviceState = vtodDomain.Get_WS_State("vtod_trip", vtodID, "CorporateIDs");
									mrRec.WebServiceState = webserviceState;
									#endregion
								}
                                else if (mRec.FleetTripCode == FleetTripCode.Van_CSCI)
                                {
                                    #region Dispatch contact info For Van
                                    van_fleet fleetObj = null;
                                    if (mRec.FleetId.ToInt64() > 0)
                                    {
                                        fleetObj = db.van_fleet.Where(p => p.Id == mRec.FleetId.ToInt64()).FirstOrDefault();
                                    }
                                    var contactDetails = new Contact { Type = Common.DTO.Enum.ContactType.Dispatch.ToString() };
                                    if (fleetObj != null)
                                    {
                                        if (!string.IsNullOrWhiteSpace(fleetObj.Name))
                                        {
                                            contactDetails.Name = fleetObj.Name;
                                        }
                                        if (!string.IsNullOrWhiteSpace(fleetObj.PhoneNumber))
                                        {
                                            contactDetails.Telephone = new Telephone { PhoneNumber = fleetObj.PhoneNumber };
                                        }
                                    }
                                    if (mrRec.Contacts == null)
                                        mrRec.Contacts = new Contacts();
                                    if (mrRec.Contacts.Items == null)
                                        mrRec.Contacts.Items = new List<Contact>();
                                    mrRec.Contacts.Items.Add(contactDetails);
                                    #endregion

                                    #region Status
                                    var vanUtility = new Domain.Van.VanUtility(logger);
                                    mrRec.StatusCode = vanUtility.ConvertTripStatusMessageForFrontEndDevice(mRec.UserID.ToInt32(), mRec.FinalStatus);//
                                    #endregion

                                    #region WebServiceState
                                    webserviceState = vtodDomain.Get_WS_State("vtod_trip", vtodID, "CorporateIDs");
                                    mrRec.WebServiceState = webserviceState;
                                    #endregion
                                }

							}

							mrRec.RateQualifier = new RateQualifier { RateQualifierValue = fleetType.ToString() };
							result.MemberReservationsList.Add(mrRec);
						}

					}
				}
				#endregion

				#region Get Trips for more fields

				#endregion

				#region Sort
				if (result.MemberReservationsList != null && result.MemberReservationsList.Any())
				{
					try
					{
						result.MemberReservationsList = result.MemberReservationsList.OrderBy(s => DateTime.Parse(s.PickupTime)).Take(numberOfReturnedMemberReservation).ToList();
					}
					catch
					{ }
				}
				#endregion

				result.Success = new Success();

				#region Common
				result.EchoToken = input.EchoToken;
				result.PrimaryLangID = input.PrimaryLangID;
				result.Target = input.Target;
				result.Version = input.Version;
				#endregion
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("MembershipDomain.MemberGetReservationTripReminder:: {0}", ex.ToString() + ex.StackTrace);

				#region Throw MembershipException
				throw VtodException.CreateException(ExceptionType.Membership, 7011);
				#endregion
			}
		}

		internal MemberGetReservationRS MemberGetReservationForCorporate(TokenRS token, MemberGetReservationRQ input)

		{
			try
			{
				#region Init
				var numberOfReturnedMemberReservation = 25;
				MembershipReservation mrRec = null;
                PaymentInfo payementInfo = null;

                try
				{
					numberOfReturnedMemberReservation = System.Configuration.ConfigurationManager.AppSettings["NumberOfReturnedMemberReservation"].ToInt32();
					logger.InfoFormat("Number Of trips :{0}:", numberOfReturnedMemberReservation);
				}
				catch (Exception ex)
				{
					logger.ErrorFormat(ex.Message);
				}


				MemberGetReservationRS result = null;
				var alephAPICall = new AlephAPICall();
				var membershipController = new UDI.SDS.MembershipController(token, TrackTime);
				string alephKey = null;
				#endregion

				#region Call Domain Method
				try
				{
					var corporateAccount = membershipController.GetCorporateAccountKey(input.Corporate.CorporateAccount.Corporation, input.Corporate.CorporateAccount.UserID);
					if (corporateAccount != null && !string.IsNullOrWhiteSpace(corporateAccount.UserKey))
					{
						alephKey = corporateAccount.UserKey;
					}
				}
				catch (Exception ex1)
				{
					throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(ex1.Message));
				}
				logger.InfoFormat("Start Aleph API Call");
				var alephResult = alephAPICall.GetAllReservation(input.Corporate.CorporateAccount.UserID, alephKey, numberOfReturnedMemberReservation);
				logger.InfoFormat("End Aleph API Call");
				#endregion

				#region Output Formatting
				logger.InfoFormat("Start---Aleph Output Formatting");
				AlephECarUtility utility = new AlephECarUtility(logger);
				result = new MemberGetReservationRS();
				string statusCode = "";
				FleetType fleetType = FleetType.ExecuCar;
				result.MemberReservationsList = new List<MembershipReservation>();
				if (alephResult != null && alephResult.Any())
				{
					logger.InfoFormat("Aleph Result Count :", alephResult.Count());
					foreach (UDI.VTOD.Domain.ECar.Aleph.AlephReservationRS alephRec in alephResult)
					{
						mrRec = new MembershipReservation();

						#region Confirmation
						mrRec.Confirmation = new List<Confirmation>();
						mrRec.Confirmation.Add(new Confirmation { ID = alephRec.confirmation_no + "##" + alephRec.vendor_confirmation_no, Type = ConfirmationType.DispatchConfirmation.ToString() });
						logger.InfoFormat("Confirmation Number : {0}:", alephRec.vendor_confirmation_no);
						logger.InfoFormat("Confirmation Type : {0}:", ConfirmationType.DispatchConfirmation.ToString());
						#endregion

						#region Drop Off
                        if (alephRec.dropoff != null && alephRec.is_dropoff_as_directed == false && alephRec.dropoff.address_type == "Airport")
                        {
                            mrRec.DropOffAddressType = "Airport";
                            mrRec.DropOffAirport_pickup_point = alephRec.dropoff.airport_pickup_point;
                            mrRec.DropOffAirportCode = alephRec.dropoff.airport_code;
                            mrRec.DropOffAirline = alephRec.dropoff.airline;
                            mrRec.DropOffAirlineFlightNumber = alephRec.dropoff.flight_number;
                            mrRec.DropOffFlightDateTime = alephRec.dropoff.flightDateTime;
                            mrRec.DropOffZipCode = alephRec.dropoff.zip_code;
                            mrRec.DropOffState = alephRec.dropoff.state;
                            mrRec.DropOffCountry = alephRec.dropoff.country;
                            mrRec.DropOffLocation = string.Format("{0} {1} {2} {3} {4}", alephRec.dropoff.airport_code, alephRec.dropoff.city, alephRec.dropoff.zip_code, alephRec.dropoff.state, alephRec.dropoff.country);
                            mrRec.DropOffAddress = mrRec.DropOffLocation;
                            if (alephRec.dropoff.Latitude != null)
                                mrRec.DropoffLatitude = alephRec.dropoff.Latitude.ToString();
                            if (alephRec.dropoff.Longitude != null)
                                mrRec.DropoffLongitude = alephRec.dropoff.Longitude.ToString();
                        }
						else if (alephRec.dropoff == null)
						{
							mrRec.DropOffLocation = "As directed";
							mrRec.DropOffAddress = "";
							logger.InfoFormat("DropOfflocation :{0} :", mrRec.DropOffLocation);
						}
                       
                        else
                        {
                            mrRec.DropOffLocation = string.Format("{0} {1} {2} {3} {4} {5}", alephRec.dropoff.street_no, alephRec.dropoff.street_name, alephRec.dropoff.city, alephRec.dropoff.zip_code, alephRec.dropoff.state, alephRec.dropoff.country);
                            mrRec.DropOffAddress = mrRec.DropOffLocation;
                            mrRec.DropOffStreetNumber = alephRec.dropoff.street_no;
                            mrRec.DropOffStreetName = alephRec.dropoff.street_name;
                            mrRec.DropOffCity = alephRec.dropoff.city;
                            mrRec.DropOffZipCode = alephRec.dropoff.zip_code;
                            mrRec.DropOffState = alephRec.dropoff.state;
                            mrRec.DropOffCountry = alephRec.dropoff.country;
                            mrRec.DropOffAddressType = alephRec.dropoff.address_type;
                            if (alephRec.pickup.Latitude != null)
                                mrRec.DropoffLatitude = alephRec.dropoff.Latitude.ToString();
                            if (alephRec.pickup.Longitude != null)
                                mrRec.DropoffLongitude = alephRec.dropoff.Longitude.ToString();
                            logger.InfoFormat("DropOfflocation :{0} :", mrRec.DropOffLocation);
                        }
						#endregion

						#region PickUp Details
						mrRec.PickMeUpNow = alephRec.is_asap;
						logger.InfoFormat("PickMeUpNow :{0} :", mrRec.PickMeUpNow);
						mrRec.PickupLocation = string.Format("{0} {1} {2} {3} {4} {5}", alephRec.pickup.street_no, alephRec.pickup.street_name, alephRec.pickup.city, alephRec.pickup.zip_code, alephRec.pickup.state, alephRec.pickup.country);
						logger.InfoFormat("PickupLocation :{0} :", mrRec.PickupLocation);
						mrRec.PickupAddress = string.Format("{0} {1} {2} {3} {4} {5}", alephRec.pickup.street_no, alephRec.pickup.street_name, alephRec.pickup.city, alephRec.pickup.zip_code, alephRec.pickup.state, alephRec.pickup.country);
						logger.InfoFormat("PickupAddress :{0} :", mrRec.PickupAddress);
						mrRec.PickupTime = alephRec.requested_datetime.DateTime.ToString();
                        mrRec.Remarks = alephRec.pickup.pickup_point;
                        if (alephRec.pickup.address_type == "Airport")
                        {
                            mrRec.PickupAddressType = "Airport";
                            mrRec.PickupAirport_pickup_point = alephRec.pickup.airport_pickup_point;
                            mrRec.PickupAirportCode = alephRec.pickup.airport_code;
                            mrRec.PickupAirline = alephRec.pickup.airline;
                            mrRec.PickupAirlineFlightNumber = alephRec.pickup.flight_number;
                            mrRec.PickupFlightDateTime = alephRec.pickup.flightDateTime;
                            mrRec.PickupZipCode = alephRec.pickup.zip_code;
                            mrRec.PickupState = alephRec.pickup.state;
                            mrRec.PickupCountry = alephRec.pickup.country;
                        }
                        else
                        {
                            mrRec.PickupStreetNumber = alephRec.pickup.street_no;
                            mrRec.PickupStreetName = alephRec.pickup.street_name;
                            mrRec.PickupCity = alephRec.pickup.city;
                            mrRec.PickupZipCode = alephRec.pickup.zip_code;
                            mrRec.PickupState = alephRec.pickup.state;
                            mrRec.PickupCountry = alephRec.pickup.country;
                            mrRec.PickupAddressType = alephRec.pickup.address_type;
                        }
                        if (alephRec.pickup.Latitude!=null)
                        mrRec.PickupLatitude = alephRec.pickup.Latitude.ToString();
                        if (alephRec.pickup.Longitude != null)
                            mrRec.PickupLongitude = alephRec.pickup.Longitude.ToString(); 
						logger.InfoFormat("PickupTime :{0} :", mrRec.PickupTime);
						#endregion

						mrRec.PostalCode = alephRec.pickup.zip_code;

						#region Status Code
						//if (alephRec.status_messages != null && alephRec.status_messages.Any())
						//{
						//    statusCode = alephRec.status_messages.OrderByDescending(p => p.message_datetime).Select(t => t.status_message_type).FirstOrDefault();
						//    mrRec.StatusCode = statusCode;
						//    logger.InfoFormat("statusCode :{0} :", mrRec.StatusCode);
						//}
						//else
						//{
						//    mrRec.StatusCode = statusCode;
						//    logger.InfoFormat("statusCode :{0} :", mrRec.StatusCode);
						//}
						//
						string status = "";
						string originalStatus = "";

						//If Booking Details have a valid 'cancelled_timestamp' value then the trip has been cancelled
						if (alephRec.cancelled_timestamp.HasValue)
						{
							originalStatus = "Canceled";
							logger.InfoFormat("statusCode :{0} :", originalStatus);
						}
						else if (alephRec.status_messages != null && alephRec.status_messages.Any())
						{
							//string statusMessageTime = null;

							bool hasTime = alephRec.status_messages.Where(o => !string.IsNullOrWhiteSpace(o.message_datetime)).Any();


							//AlephStatusMessage firstAlephMessage = alephRec.status_messages.Where(o => !string.IsNullOrWhiteSpace(o.message_datetime)).FirstOrDefault();
							//if (firstAlephMessage != null)
							//{
							//	statusMessageTime = alephRec.status_messages.Where(o => o.message_datetime == null).FirstOrDefault().message_datetime;
							//}
							//else
							//{
							//	statusMessageTime = "";
							//}

							//string statusMessageTime = alephRec.status_messages.Where(o => o.message_datetime == null).FirstOrDefault().message_datetime;
							if (hasTime)
							{
								originalStatus = alephRec.status_messages.Where(o => !string.IsNullOrWhiteSpace(o.message_datetime)).OrderByDescending(f => f.message_datetime.ToDateTime().Value).FirstOrDefault().status_message_type;
							}
							else
							{
								originalStatus = alephRec.status_messages.LastOrDefault().status_message_type;
							}
							logger.InfoFormat("statusCode :{0} :", originalStatus);
						}

						//Map Aleph Status to Vtod Status
                        string device = null;
                        string version = null;
                        if (input.TPA_Extensions != null)
                        {
                            if (!string.IsNullOrEmpty(input.TPA_Extensions.Device))
                            {
                                device = input.TPA_Extensions.Device;
                            }
                            if (!string.IsNullOrEmpty(input.TPA_Extensions.Source))
                            {
                                version = input.TPA_Extensions.Source;
                            }
                        }
                        status = utility.ConvertTripStatus(originalStatus, device, version);

						string frontEndStatus = string.Empty;
						int UserID = utility.GetUserDetails(token.Username);
						if (UserID != 0)
						{
							frontEndStatus = utility.ConvertTripStatusMessageForFrontEndDevice(UserID, status);
						}

						//Status s = new Common.DTO.OTA.Status { Value = status, Fare = succ.TripInfo[0].FareAmount.ToDecimal() };
						if (!string.IsNullOrWhiteSpace(frontEndStatus) && !string.IsNullOrEmpty(frontEndStatus))
							mrRec.StatusCode = frontEndStatus;
						else
							mrRec.StatusCode = status;
						#endregion

						#region WebServiceState
						List<NameValue> specialInputs = null;
						if (!string.IsNullOrWhiteSpace(mrRec.WebServiceState)) specialInputs = mrRec.WebServiceState.JsonDeserialize<List<NameValue>>();
						var newSpecialInputs = utility.InsertorUpdateCorporateSpecialInstructionsForCorporate(specialInputs, alephRec);
						mrRec.WebServiceState = newSpecialInputs.JsonSerialize().ToString();
						logger.InfoFormat("WebServiceState :{0} :", mrRec.WebServiceState);
						#endregion

						#region Contacts
						var companyContact = new Contact
						{
							Type = ContactType.Dispatch.ToString(),
							Name = alephRec.company_name ?? string.Empty,
							Telephone = string.IsNullOrWhiteSpace(alephRec.company_phone_number)
										   ? null : new Telephone
										   {
											   PhoneNumber = alephRec.company_phone_number
										   }
						};

						if (mrRec.Contacts == null)
							mrRec.Contacts = new Contacts();
						if (mrRec.Contacts.Items == null)
							mrRec.Contacts.Items = new List<Contact>();

						mrRec.Contacts.Items.Add(companyContact);
						logger.InfoFormat("Company Contact :{0} :", companyContact);
						#endregion

                        #region Corporate Details
                        mrRec.Account = alephRec.corporation_account_name;
                        mrRec.CorporateName = alephRec.corporation_name;
						#endregion

						#region Booking Criteria (corporate requriement)
						if (alephRec.booking_criteria !=null && alephRec.booking_criteria.Any())
						{
							mrRec.CorporateRequirements = new List<RequirementDetail>();
							foreach (var criterion in alephRec.booking_criteria)
							{
								mrRec.CorporateRequirements.Add(new RequirementDetail {RequirementName = criterion.booking_criterion_name, RequirementValue = criterion.booking_criterion_value });
							}
						}
						#endregion

						#region PaymentMethod
						//mrRec.PaymentInfo = new PaymentInfo {Type = alephRec.payment_method };
                        payementInfo = new PaymentInfo { Type = alephRec.payment_method };
                        if (alephRec.reservation_payment != null)
                        {
                            payementInfo.PaymentCard = alephRec.reservation_payment.payment_info.ToPaymentCard();
                        }
                        mrRec.PaymentInfo = payementInfo;
                        #endregion

                        #region Special Instructions
                        mrRec.SpecialInstructions = alephRec.special_instructions;
                        #endregion

                        #region Trip To be Modified
                        vtod_trip trip= null;

                        if (!string.IsNullOrEmpty(alephRec.vendor_confirmation_no))
                        {
                            ecar_trip ecartrip = null;

                            using (var db = new DataAccess.VTOD.VTODEntities())
                            {
                                ecartrip = db.ecar_trip.Where(p => p.DispatchTripId.Contains(alephRec.confirmation_no)).FirstOrDefault();

                                if (ecartrip != null)
                                {
                                    trip = db.vtod_trip.Where(p => p.Id == ecartrip.Id).FirstOrDefault();
                                }
                            }
                        }

                        if (trip != null)
                        {
                            DateTime tripTime = trip.PickupDateTimeUTC.Value; //TODO : Double check if PickupDateTimeUTC is always not null
                            DateTime currentTime = DateTime.UtcNow;
                            double timeDiff = tripTime.Subtract(currentTime).TotalMinutes; 
                            if (timeDiff > 60)
                            {
                                if (mrRec.StatusCode == UDI.VTOD.Domain.ECar.Const.ECarTripStatusType.DispatchPending)
                                {
                                    mrRec.IsModify = true;
                                }
                                else
                                {
                                    mrRec.IsModify = false;
                                }

                            }
                            else
                            {
                                mrRec.IsModify = false;
                            }
                        }
                        else
                        {
                            mrRec.IsModify = false;
                        }
                    
                    #endregion


                    mrRec.RateQualifier = new RateQualifier { RateQualifierValue = fleetType.ToString() };
						logger.InfoFormat("Fleet Type :{0} :", fleetType);
						result.MemberReservationsList.Add(mrRec);
					}
				}
				logger.InfoFormat("End---Aleph Output Formatting");
				#endregion

				#region Sort
				if (result.MemberReservationsList != null && result.MemberReservationsList.Any())
				{
					try
					{
						result.MemberReservationsList = result.MemberReservationsList.OrderByDescending(s => DateTime.Parse(s.PickupTime)).ToList();
					}
					catch (Exception e)
					{
						logger.ErrorFormat(e.Message);
					}
				}
				#endregion

				#region Success Object
				result.Success = new Success();
				#endregion

				#region Common
				result.EchoToken = input.EchoToken;
				result.PrimaryLangID = input.PrimaryLangID;
				result.Target = input.Target;
				result.Version = input.Version;
				#endregion
				//#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("MembershipDomain.MemberGetReservation:: {0}", ex.ToString() + ex.StackTrace);
				#region Throw MembershipException
				throw VtodException.CreateException(ExceptionType.Membership, 7011);//throw new MembershipException(Messages.Membership_MemberGetReservation);
				#endregion
			}
		}

        internal MemberGetReferralLinkRS MemberGetReferralLink(TokenRS token, MemberGetReferralLinkRQ input)
        {
            try
            {
                MemberGetReferralLinkRS result = new MemberGetReferralLinkRS();
                var membershipController = new UDI.SDS.MembershipController(token, TrackTime);
                var sdsProfile = membershipController.GetProfileCommunicationSettings(input.MemberID);

                //If hit here means succeed to get sdsProfile
                using (var db = new VTODEntities())
                {                    
                    var existingMember = db.vtod_member_info.Where(x => x.SDSMemberId.Equals(input.MemberID)).FirstOrDefault();
                    var newMember = CreateVtod_member_infoWithSDSProfile(sdsProfile, input.MemberID);

                    if (existingMember == null)
                    {                        
                        db.vtod_member_info.Add(newMember);
                    }
                    else if (string.IsNullOrWhiteSpace(existingMember.ReferralCode))
                    {
                        //update case : Row created by US3094 script
                        //Member info exist but no first name/last name/ReferralCode/ReferralLink

                        existingMember.FirstName = newMember.FirstName;
                        existingMember.LastName = newMember.LastName;
                        existingMember.ReferralCode = newMember.ReferralCode;
                        existingMember.ReferralLink = newMember.ReferralLink;
                    }

                    db.SaveChanges();
                    result.ReferralLink = newMember.ReferralLink;

                }

                #region Common
                result.EchoToken = input.EchoToken;
                result.PrimaryLangID = input.PrimaryLangID;
                result.Target = input.Target;
                result.Version = input.Version;
                #endregion

                return result;

            }
            catch (Exception ex)
            {
                logger.ErrorFormat("MembershipDomain.MemberLogin:: {0}", ex.ToString() + ex.StackTrace);

                #region Throw MembershipException
                throw VtodException.CreateException(ExceptionType.Membership, 7013);
                #endregion
            }
            
        }
       
		internal MemberLoginRS MemberLogin(TokenRS token, MemberLoginRQ input)
		{
			try
			{
				MemberLoginRS result = null;
				var membershipController = new UDI.SDS.MembershipController(token, TrackTime);
				int phoneVerificationEnabled = System.Configuration.ConfigurationManager.AppSettings["EnablePhoneVerification"].ToInt32();
				var sdsResult = membershipController.MemberLogin(input.EmailAddress, input.Password);

				#region Output Conversion
				result = new MemberLoginRS();
				//result.Success = false;
				if (sdsResult.MemberID > 0)
				{
					result.Success = new Success();
					result.MembershipRecord = new MembershipRecord();
					result.MembershipRecord.SubscribeToEmail = sdsResult.AllowSpam;
					result.MembershipRecord.Telephone = new Telephone();
					result.MembershipRecord.Telephone.PhoneNumber = sdsResult.ContactNumber;
					result.MembershipRecord.Telephone.CountryAccessCode = sdsResult.ContactNumberDialingPrefix;
					result.MembershipRecord.EmailAddress = sdsResult.EmailAddress;
					result.MembershipRecord.FirstName = sdsResult.FirstName;
					result.MembershipRecord.LastName = sdsResult.LastName;
					result.MembershipRecord.MemberID = sdsResult.MemberID;
                    result.MembershipRecord.SubscribeToSms = (bool)sdsResult.SubscribeToSms;
                    result.MembershipRecord.SubscribeToEmail = (bool)sdsResult.AllowSpam;
                    result.MembershipRecord.DefaultGratuityPercentage = sdsResult.GratuityRate * 100;
					result.MembershipRecord.HonorificID = sdsResult.HonorificId;
					result.MembershipRecord.HomeAirport = sdsResult.HomeAirport;
					result.MembershipRecord.BirthDate = sdsResult.BirthDate.ToString();
                    result.MembershipRecord.DisabilityVehicleInd = (bool)sdsResult.AccessibilityDefault;
                    result.MembershipRecord.IsConciergeChatEnabled = (bool)sdsResult.IsConciergeChatEnabled;
                    if(sdsResult.CustomerGuid!=null)
                    result.MembershipRecord.CustomerGuid = sdsResult.CustomerGuid;

                    if (phoneVerificationEnabled == 1)
						result.MembershipRecord.IsPhoneVerified = sdsResult.IsPhoneNumberVerified;
					else
						result.MembershipRecord.IsPhoneVerified = true;

					if (sdsResult.CorporateAccount != null)
					{
						result.Corporates = new Corporates();
						result.Corporates.CorporateList = new List<Corporate>();
						var corporate = new Corporate();
						corporate.CorporateAccount = new CorporateAccount();
						corporate.CorporateAccount.Corporation = sdsResult.CorporateAccount.Corporation;
						corporate.CorporateAccount.UserID = sdsResult.CorporateAccount.UserId;
						corporate.CorporateAccount.UserKey = sdsResult.CorporateAccount.UserKey;
						result.Corporates.CorporateList.Add(corporate);
					}

                    using (var db = new DataAccess.VTOD.VTODEntities())
                    {
                        //if there is no recode with the memberID on vtod_BOBO_member, it will throw an exception
                        var boboMember = db.vtod_BOBO_member.Where(x => x.SDSMemberId.Equals(sdsResult.MemberID) && x.IsEnabled).SingleOrDefault();
                        if (boboMember != null)
                        {
                            var address = new Address();
                            address.AddressType = boboMember.PickupAddressType;
                            address.Longitude = boboMember.PickupLongitude.ToString();
                            address.Latitude = boboMember.PickupLatitude.ToString();
                            address.BldgRoom = boboMember.PickupAptNo;
                            address.StreetNmbr = boboMember.PickupStreetNo;
                            address.AddressLine = boboMember.PickupStreetName;
                            address.StreetSuffix = boboMember.PickupStreetType;
                            address.CityName = boboMember.PickupCity;
                            address.PostalCode = boboMember.PickupZipCode;

                            var stateProv = new StateProv();
                            stateProv.StateCode = boboMember.PickupStateCode;
                            address.StateProv = stateProv;

                            var countryName = new CountryName();
                            countryName.Code = boboMember.PickupStateCode;
                            address.CountryName = countryName;

                            result.Address = address;
                            result.MembershipRecord.IsBOBO = true;
                            result.MembershipRecord.BOBOMemberID = sdsResult.MemberID;
                        }
                        
                    }
                }
				else
				{
					#region Throw MembershipException
					throw VtodException.CreateException(ExceptionType.Membership, 7013);
					#endregion
				}

				#region Common
				result.EchoToken = input.EchoToken;
				result.PrimaryLangID = input.PrimaryLangID;
				result.Target = input.Target;
				result.Version = input.Version;
				#endregion
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("MembershipDomain.MemberLogin:: {0}", ex.ToString() + ex.StackTrace);

				#region Throw MembershipException
				throw VtodException.CreateException(ExceptionType.Membership, 7013);
				#endregion
			}
		}

		internal MemberUpdateLocationRS MemberUpdateLocation(TokenRS token, MemberUpdateLocationRQ input)
		{
			try
			{
				MemberUpdateLocationRS result = null;
				var membershipController = new UDI.SDS.MembershipController(token, TrackTime);

				#region Input Conversion

				var sdsMembership = new UDI.SDS.MembershipService.MembershipLocationRecord();

				sdsMembership.Comments = input.MemberLocation.Comment;
				sdsMembership.CountryCode = input.MemberLocation.Address.CountryName.Code;
				sdsMembership.CountrySubDivision = input.MemberLocation.Address.StateProv.StateCode;
				sdsMembership.Municipality = input.MemberLocation.Address.CityName;
				sdsMembership.Latitude = input.MemberLocation.Address.Latitude.ToDecimal();
				sdsMembership.Longitude = input.MemberLocation.Address.Longitude.ToDecimal();
				sdsMembership.LocalizationCode = "en-us";
				sdsMembership.PhoneNumber = (input.MemberLocation.Telephone.AreaCityCode + input.MemberLocation.Telephone.PhoneNumber).CleanPhone();
				sdsMembership.PhoneNumberDialingPrefix = input.MemberLocation.Telephone.CountryAccessCode;
				sdsMembership.PostalCode = input.MemberLocation.Address.PostalCode;
				sdsMembership.StreetAddress = string.Format("{0} {1}", input.MemberLocation.Address.StreetNmbr, input.MemberLocation.Address.AddressLine);
				sdsMembership.StreetName = input.MemberLocation.Address.AddressLine;
				sdsMembership.StreetNumber = input.MemberLocation.Address.StreetNmbr;
				sdsMembership.UnitNumber = input.MemberLocation.Address.BldgRoom;
				sdsMembership.LocationName = input.MemberLocation.Address.LocationName;
				sdsMembership.LocationName1 = input.MemberLocation.Address.LocationName;
				sdsMembership.LocationType = input.MemberLocation.Address.LocationType != null ? input.MemberLocation.Address.LocationType.Value : string.Empty;

				sdsMembership.LocationID = input.MemberLocation.LocationID.ToInt32();

				#endregion

				var sdsResult = membershipController.MemberUpdateLocation(sdsMembership, input.MemberLocation.MemberID.ToInt32());

				#region Output Conversion
				result = new MemberUpdateLocationRS();
				if (sdsResult)
				{
					result.Success = new Success();
				}
				else
				{
					throw VtodException.CreateException(ExceptionType.Membership, 7014);//throw new MembershipException(Messages.Membership_MemberUpdateLocation);
				}

				#region Common
				result.EchoToken = input.EchoToken;
				result.PrimaryLangID = input.PrimaryLangID;
				result.Target = input.Target;
				result.Version = input.Version;
				#endregion
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("MembershipDomain.MemberUpdateLocation:: {0}", ex.ToString() + ex.StackTrace);

				#region Throw MembershipException
				throw VtodException.CreateException(ExceptionType.Membership, 7014);//throw new MembershipException(Messages.Membership_MemberUpdateLocation);
				#endregion
			}
		}

		internal MemberUpdateProfileRS MemberUpdateProfile(TokenRS token, MemberUpdateProfileRQ input)
		{
			try
			{
				MemberUpdateProfileRS result = null;
				var membershipController = new UDI.SDS.MembershipController(token, TrackTime);


				#region Input Conversion
				var sdsMembership = new UDI.SDS.MembershipService.MembershipRecord();
                if (input.MembershipRecord.SubscribeToEmail != null)
                {
                    sdsMembership.AllowSpam = (bool)input.MembershipRecord.SubscribeToEmail;
                }
				sdsMembership.ContactNumber = (input.MembershipRecord.Telephone.AreaCityCode + input.MembershipRecord.Telephone.PhoneNumber).CleanPhone();
				sdsMembership.ContactNumberDialingPrefix = input.MembershipRecord.Telephone.CountryAccessCode;
				sdsMembership.EmailAddress = input.MembershipRecord.EmailAddress;
				sdsMembership.FirstName = input.MembershipRecord.FirstName;
				sdsMembership.LastName = input.MembershipRecord.LastName;
				sdsMembership.MemberID = input.MembershipRecord.MemberID;
				if (input.MembershipRecord.SubscribeToSms != null)
				{
					sdsMembership.SubscribeToSms = (bool)input.MembershipRecord.SubscribeToSms;
				}
                if (input.MembershipRecord.DisabilityVehicleInd != null)
                {
                    sdsMembership.AccessibilityDefault = (bool)input.MembershipRecord.DisabilityVehicleInd;
                }
                else
                {
                    sdsMembership.AccessibilityDefault = false;
                }
                sdsMembership.HonorificId = input.MembershipRecord.HonorificID;
				sdsMembership.BirthDate = !string.IsNullOrWhiteSpace(input.MembershipRecord.BirthDate) ? input.MembershipRecord.BirthDate.ToDateTime().Value : DateTime.MinValue;
				sdsMembership.HomeAirport = input.MembershipRecord.HomeAirport;
				sdsMembership.GratuityRate = input.MembershipRecord.DefaultGratuityPercentage / 100;
				if (input.MembershipRecord.IsPhoneVerified != null && input.MembershipRecord.IsPhoneVerified.HasValue)
				{
					sdsMembership.IsPhoneNumberVerified = (bool)input.MembershipRecord.IsPhoneVerified;
				}
				else
				{
					sdsMembership.IsPhoneNumberVerified = false;
				}

				if (input.CorporateAccount != null)
				{
					sdsMembership.CorporateAccount = new UDI.SDS.MembershipService.CorporateAccount();
					sdsMembership.CorporateAccount.Corporation = input.CorporateAccount.Corporation;
					sdsMembership.CorporateAccount.UserId = input.CorporateAccount.UserID;
					sdsMembership.CorporateAccount.UserKey = input.CorporateAccount.UserKey;
				}
				#endregion


				var sdsResult = membershipController.MemberUpdateProfile(sdsMembership);

				#region Output Conversion
				result = new MemberUpdateProfileRS();
				if (sdsResult)
				{
					result.Success = new Success();
				}
				else
				{
					throw VtodException.CreateException(ExceptionType.Membership, 7015);//throw new MembershipException(Messages.Membership_MemberUpdateProfile);
				}

				#region Common
				result.EchoToken = input.EchoToken;
				result.PrimaryLangID = input.PrimaryLangID;
				result.Target = input.Target;
				result.Version = input.Version;
				#endregion
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("MembershipDomain.MemberUpdateProfile:: {0}", ex.ToString() + ex.StackTrace);

				#region Throw MembershipException
				throw VtodException.CreateException(ExceptionType.Membership, 7015);//throw new MembershipException(Messages.Membership_MemberUpdateProfile);
				#endregion
			}
		}

		internal void LinkReservation(TokenRS token, long vtodTripID, int sdsRezID, int memberID)
		{
			try
			{
				var membershipController = new UDI.SDS.MembershipController(token, TrackTime);

				membershipController.LinkReservation(vtodTripID, sdsRezID, memberID);
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("MembershipDomain.LinkReservation:: {0}", ex.ToString() + ex.StackTrace);

				#region Throw MembershipException
				throw VtodException.CreateException(ExceptionType.Membership, 7003);//throw new MembershipException(Messages.Membership_LinkReservation);
				#endregion
			}
		}

        internal void DelinkReservation(TokenRS token, long vtodTripID, int sdsRezID, int memberID)
        {
            try
            {
                var membershipController = new UDI.SDS.MembershipController(token, TrackTime);

                membershipController.DelinkReservation(vtodTripID, sdsRezID, memberID);
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("MembershipDomain.DelinkReservation:: {0}", ex.ToString() + ex.StackTrace);

                #region Throw MembershipException
                throw VtodException.CreateException(ExceptionType.Membership, 7003);//throw new MembershipException(Messages.Membership_LinkReservation);
                #endregion
            }
        }

        internal UDI.SDS.MembershipService.CommunicationSettings GetProfileCommunicationSettings(TokenRS token, int memberID)
		{
			try
			{
				var membershipController = new UDI.SDS.MembershipController(token, TrackTime);

				var result = membershipController.GetProfileCommunicationSettings(memberID);

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("MembershipDomain.CommunicationSettings:: {0}", ex.ToString() + ex.StackTrace);

				#region Throw MembershipException
				throw new MembershipException(ex.Message);
				#endregion
			}
		}

		internal MemberRequestEmailConfirmationRS MemberRequestEmailConfirmation(TokenRS token, MemberRequestEmailConfirmationRQ input)
		{
			var reservationController = new UDI.SDS.ReservationsController(token, TrackTime);


			try
			{
				var result = new MemberRequestEmailConfirmationRS();
				vtod_trip trip = null;
				long tripID = 0;
				string memberID = string.Empty;
				string dispatchTripID = null;
				vtod_trip_account_transaction transaction = null;


				using (var db = new VTODEntities())
				{
					memberID = input.MemberID;

					#region Find TripID if UniqueID is DispatchConfirmation

					if (input.UniqueID.Type.ToLower().Trim() == Common.DTO.Enum.ConfirmationType.DispatchConfirmation.ToString().ToLower().Trim())
					{
						//For now we cover DispatchConfirmation type just for SDS
						if (input.Corporate == null)
						{
							var sdsTrip = db.sds_trip.Where(s => s.ConfirmationNumber == input.UniqueID.ID).FirstOrDefault();
							if (sdsTrip != null)
							{
								tripID = sdsTrip.Id;
							}
							// for Taxi 
							var taxiTrip = db.taxi_trip.Where(s => s.DispatchTripId == input.UniqueID.ID).FirstOrDefault();
							if (taxiTrip != null)
							{
								tripID = taxiTrip.Id;
							}
                            //for van
                            var vanTrip = db.van_trip.Where(s => s.DispatchTripId == input.UniqueID.ID).FirstOrDefault();
                            if (vanTrip != null)
                            {
                                tripID = vanTrip.Id;
                            }
                            var ecarTrip = db.ecar_trip.Where(s => s.DispatchTripId == input.UniqueID.ID).FirstOrDefault();
                            if (ecarTrip != null)
                            {
                                tripID = ecarTrip.Id;
                            }
                        }

					}
					else
					{
						if (input.Corporate == null || input.Corporate.CorporateAccount ==null)
						{
							tripID = input.UniqueID.ID.ToInt64();
						}
						else
						{
							#region MyRegion
							string alephID = input.UniqueID.ID;
							string trID = string.Empty;
							#region Parsing
							if (alephID.ToString().Contains("##"))
							{
								if (alephID.ToString().IndexOf("##") != -1)
								{
									trID = alephID.ToString().Substring(0, alephID.ToString().IndexOf("##"));
								}
								else
									trID = alephID;
							}
							else
								trID = alephID;

							#endregion
							var ecarTrip = db.ecar_trip.Where(s => s.DispatchTripId.ToString() == trID).FirstOrDefault();
							if (input.UniqueID.Type.ToLower().Trim() == Common.DTO.Enum.ConfirmationType.Confirmation.ToString().ToLower().Trim())
							{
                                if (ecarTrip!=null)
								tripID = ecarTrip.DispatchTripId.ToInt64();
							}
							else
							{
								tripID = trID.ToInt64();
							}

							#endregion
                            if (ecarTrip != null)
							trip = db.vtod_trip.Include("ecar_trip").Where(s => s.Id == ecarTrip.Id).FirstOrDefault();
						}
					}
					#endregion

					if (tripID > 0 && trip == null)
					{
						trip = db.vtod_trip.Include("taxi_trip").Include("sds_trip").Where(s => s.Id == tripID /*&& s.MemberID == memberID*/).FirstOrDefault();
                        if (input.UniqueID.Type.ToLower().Trim() == Common.DTO.Enum.ConfirmationType.DispatchConfirmation.ToString().ToLower().Trim())
                        {
                            if (db.van_trip.Where(s => s.DispatchTripId == input.UniqueID.ID).FirstOrDefault() != null)
                            {
                                trip = db.vtod_trip.Include("van_trip").Where(s => s.Id == tripID /*&& s.MemberID == memberID*/).FirstOrDefault();
                            }
                            else if (trip!=null && trip.FleetTripCode==FleetTripCode.Ecar_Texas && db.ecar_trip.Where(s => s.DispatchTripId == input.UniqueID.ID).FirstOrDefault() != null)
                            {
                                trip = db.vtod_trip.Include("ecar_trip").Where(s => s.Id == tripID /*&& s.MemberID == memberID*/).FirstOrDefault();
                            }
                        }
                        else if (input.UniqueID.Type.ToLower().Trim() == Common.DTO.Enum.ConfirmationType.Confirmation.ToString().ToLower().Trim())
                        {
                             if (db.van_trip.Where(s => s.Id.ToString() == input.UniqueID.ID).FirstOrDefault() != null)
                            {
                                trip = db.vtod_trip.Include("van_trip").Where(s => s.Id == tripID /*&& s.MemberID == memberID*/).FirstOrDefault();
                            }
                            else if (trip != null && trip.FleetTripCode == FleetTripCode.Ecar_Texas && db.ecar_trip.Where(s => s.Id.ToString() == input.UniqueID.ID).FirstOrDefault() != null)
                            {
                                trip = db.vtod_trip.Include("ecar_trip").Where(s => s.Id == tripID /*&& s.MemberID == memberID*/).FirstOrDefault();
                            }
                        }
						transaction = db.vtod_trip_account_transaction.Where(s => s.TripID == tripID).OrderByDescending(s => s.Id).FirstOrDefault();
					}

					if (trip != null)
					{
						if (
							(trip.FleetType == FleetType.Taxi.ToString() && trip.taxi_trip != null && trip.taxi_trip.ServiceAPIId != 3 /* 3 = SDS, it's hardcoded as of now. better not to be hardcoded */)
							||
							(trip.FleetType == FleetType.ExecuCar.ToString() && trip.ecar_trip != null)
                            ||
                            (trip.FleetType == FleetType.Van.ToString() && trip.van_trip != null && trip.van_trip.ServiceAPIId != 3 /* 3 = SDS, it's hardcoded as of now. better not to be hardcoded */)
							)
						{
							if (trip.FleetType == FleetType.Taxi.ToString())
							{
								//var taxiTrip = (taxi_trip)trip;
								var fleet = db.taxi_fleet.Where(s => s.Id == trip.taxi_trip.FleetId).FirstOrDefault();

								string fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "EmailConfirmation.txt";

								if (trip.TotalFareAmount.HasValue && trip.TotalFareAmount.Value > 0)
								{
									if (trip.PaymentType.ToLower().Trim() == PaymentType.Cash.ToString().Trim().ToLower())
									{
										fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "EmailCashCardReceipt.txt";
									}
									if (trip.PaymentType.ToLower().Trim() == PaymentType.PaymentCard.ToString().Trim().ToLower())
									{
										if (transaction != null && transaction.CreditsApplied.HasValue && transaction.CreditsApplied != 0)
										{
											fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "EmailCreditCardReceiptWithZTripCredit.txt";
										}
										else
										{
											fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "EmailCreditCardReceipt.txt";
										}
									}
								}

								var templates = fileName.ReadHtmlFromUrl().Split(new string[] { "##" }, StringSplitOptions.None); //File.ReadAllText(fileName).Split(new string[] { "##" }, StringSplitOptions.None);
								var headerTemplate = templates[0];
								var bodyTemplate = templates[1];


								#region Getting Member Profile
								UDI.SDS.MembershipService.CommunicationSettings profile = null;
								var membershipController = new UDI.SDS.MembershipController(token, TrackTime);
								if (!string.IsNullOrWhiteSpace(memberID))
									profile = membershipController.GetProfileCommunicationSettings(memberID.ToInt32());
								#endregion

								#region Making the Body
								var body = (string)bodyTemplate.Clone();
								body = body.Replace("{FirstName}", trip.taxi_trip.FirstName);
								body = body.Replace("{LastName}", trip.taxi_trip.LastName);
								body = body.Replace("{ProviderTripID}", trip.taxi_trip.DispatchTripId);
								body = body.Replace("{PUDateTime}", string.Format("{0:MM-dd-yyyy hh:mm tt}", trip.PickupDateTime));
								body = body.Replace("{PUAddress}", trip.taxi_trip.PickupFullAddress);
								if (profile != null)
								{
									body = body.Replace("{MemberPhone}", profile.ContactNumber.MakeStandardPhoneForamt());
								}
								else
								{
									if (input.Verification != null && input.Verification.TelephoneInfo != null && input.Verification.TelephoneInfo.CountryAccessCode != null && input.Verification.TelephoneInfo.AreaCityCode != null && input.Verification.TelephoneInfo.PhoneNumber != null)
									{
										string phoneNumber = input.Verification.TelephoneInfo.CountryAccessCode + input.Verification.TelephoneInfo.AreaCityCode + input.Verification.TelephoneInfo.PhoneNumber;
										body = body.Replace("{MemberPhone}", phoneNumber.MakeStandardPhoneForamt());
									}
								}
								if (!string.IsNullOrWhiteSpace(trip.taxi_trip.DropOffFullAddress))
								{
									body = body.Replace("{DOAddress}", trip.taxi_trip.DropOffFullAddress);
								}
								else
								{
									body = body.Replace("{DOAddress}", "As Directed");
								}
								body = body.Replace("{FleetName}", fleet.Alias);
								body = body.Replace("{FleetPhone}", fleet.PhoneNumber.MakeStandardPhoneForamt());

								//if (tripStatus == Taxi.Const.TaxiTripStatusType.Completed)
								//{
								if (trip.TotalFareAmount.HasValue && trip.TotalFareAmount.Value > 0)
								{
									if (transaction != null && transaction.CreditsApplied.HasValue && transaction.CreditsApplied.Value != 0)
									{
										if (trip.Gratuity.HasValue && trip.Gratuity.Value > 0)
										{
											body = body.Replace("{Fare}", (trip.TotalFareAmount - trip.Gratuity).ToString());
											body = body.Replace("{Tip}", trip.Gratuity.Value.ToString());
											body = body.Replace("{zTripCredit}", string.Format("({0})", System.Math.Abs(transaction.CreditsApplied.Value)));
											body = body.Replace("{TotalFare}", (trip.TotalFareAmount - System.Math.Abs(transaction.CreditsApplied.Value)).ToString());
										}
										else
										{
											body = body.Replace("{Fare}", trip.TotalFareAmount.ToString());
											body = body.Replace("{Tip}", string.Empty);
											body = body.Replace("{zTripCredit}", string.Format("({0})", System.Math.Abs(transaction.CreditsApplied.Value)));
											body = body.Replace("{TotalFare}", (trip.TotalFareAmount - System.Math.Abs(transaction.CreditsApplied.Value)).ToString());
										}
									}
									else
									{
										if (trip.Gratuity.HasValue && trip.Gratuity.Value > 0)
										{
											body = body.Replace("{Fare}", (trip.TotalFareAmount - trip.Gratuity).ToString());
											body = body.Replace("{Tip}", trip.Gratuity.Value.ToString());
											body = body.Replace("{TotalFare}", (trip.TotalFareAmount).ToString());
										}
										else
										{
											body = body.Replace("{Fare}", trip.TotalFareAmount.ToString());
											body = body.Replace("{Tip}", string.Empty);
											body = body.Replace("{TotalFare}", trip.TotalFareAmount.ToString());
										}
									}

									if (trip.PaymentType.ToLower().Trim() == PaymentType.PaymentCard.ToString().Trim().ToLower())
									{

										var accountController = new UDI.SDS.AccoutingController(token, TrackTime);
										var creditCardList = accountController.GetCreditCardAccounts(memberID.ToInt32());
										if (creditCardList != null && creditCardList.Any())
										{
											var creditcard = creditCardList.Where(s => s.AccountID == trip.CreditCardID.ToInt32()).FirstOrDefault();
											if (creditcard != null)
											{
												body = body.Replace("{CardType}", creditcard.CardType.ToString());
												body = body.Replace("{BilledTo}", creditcard.BillingName.ToString());
												body = body.Replace("{CreditCardLast4}", creditcard.Last4.ToString());

												string cardFormat;

												switch (creditcard.CardType)
												{
													case UDI.SDS.MembershipService.CreditCardTypesEnumeration.AMEX:
														cardFormat = "XXXX-XXXXXX-X{0}";
														break;
													case UDI.SDS.MembershipService.CreditCardTypesEnumeration.DINERS:
														cardFormat = "XXXX-XXXXXX-{0}";
														break;
													case UDI.SDS.MembershipService.CreditCardTypesEnumeration.JCB:
														cardFormat = "XXXX-XXXXXXXX{0}";
														break;
													default:
														cardFormat = "XXXX-XXXX-XXXX-{0}";
														break;
												}
												body = body.Replace("{CardNumber}", string.Format(cardFormat, creditcard.Last4));
											}
											else
											{
												body = body.Replace("{CardType}", string.Empty);
												body = body.Replace("{CardNumber}", string.Empty);
												body = body.Replace("{BilledTo}", string.Empty);
												body = body.Replace("{CreditCardLast4}", string.Empty);
											}
										}

									}
									if (trip.PaymentType.ToLower().Trim() == PaymentType.Cash.ToString().Trim().ToLower())
									{
									}
								}
								//}
								#endregion

								#region Making the Header
								var header = (string)headerTemplate.Clone();
								header = header.Replace("{ProviderTripID}", trip.taxi_trip.DispatchTripId);
								#endregion

								#region Sending Email
								try
								{
									var from = System.Configuration.ConfigurationManager.AppSettings["Notification_Email_From"];
									var host = System.Configuration.ConfigurationManager.AppSettings["Notification_Email_SMTP_Host"];
									var port = System.Configuration.ConfigurationManager.AppSettings["Notification_Email_SMTP_Port"];

									MessageController messageController = new MessageController();
									SMTPEmailMessage smtpMessage = new SMTPEmailMessage();
									smtpMessage.From = from;// "NoReply@zTrip.com";

									#region To Email
									string email = input.EmailAddress;

									if (string.IsNullOrWhiteSpace(email))
									{
										email = profile.EmailAddress;
									}


									if (email.Contains("<"))
									{
										var l = email.Split('<');
										smtpMessage.To = new List<string> { l.First().Trim() };
									}
									else
									{
										smtpMessage.To = new List<string> { email.Trim() };
									}
									#endregion

									//smtpMessage.Subject = headerTemplate;
									smtpMessage.Subject = header;
									smtpMessage.MessageBody = body;
									smtpMessage.SMTP_Host = host;// "192.168.20.110";
									smtpMessage.SMTP_Port = port.ToInt32();// 25;
									smtpMessage.IsBodyHTML = true;

									var errorMessage = string.Empty;
									if (messageController.SendSMTPEmailMessage(smtpMessage, out errorMessage))
									{
										logger.InfoFormat("notifyBookedStatus for MemberID: {0} sent successfully", memberID);
									}
									else
									{
										logger.ErrorFormat("Error for notifyBookedStatus for MemberID: {0} -- {1}", memberID, errorMessage);
									}
								}
								catch (Exception ex)
								{
									var t = ex.GetType();
									logger.Error(ex);
									logger.ErrorFormat("Error for MemberRequestEmailConfirmation for MemberID: {0} and TripID {1}", memberID, tripID);
									throw VtodException.CreateException(ExceptionType.Membership, 7005);//throw new MembershipException(Messages.Membership_SendingEmailProblem);

								}
								#endregion
							}
							//This is for Aleph as of now. In future if we need to add another ecar to zTrip, we need to modify this part.
							else if (trip.FleetType == FleetType.ExecuCar.ToString())
							{
								#region Deleted - Old approach
								////var taxiTrip = (taxi_trip)trip;
								////var fleet = db.taxi_fleet.Where(s => s.Id == trip.taxi_trip.FleetId).FirstOrDefault();

								//string fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "AlephEmailConfirmation.txt";
								//var specialInputs = vtod_domain.Get_WS_State("vtod_trip", trip.Id, "CorporateIDs").JsonDeserialize<List<NameValue>>();


								////if (trip.TotalFareAmount.HasValue && trip.TotalFareAmount.Value > 0)
								////{
								////	if (trip.PaymentType.ToLower().Trim() == PaymentType.Cash.ToString().Trim().ToLower())
								////	{
								////		fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "EmailCashCardReceipt.txt";
								////	}
								////	if (trip.PaymentType.ToLower().Trim() == PaymentType.PaymentCard.ToString().Trim().ToLower())
								////	{
								////		if (transaction != null && transaction.CreditsApplied.HasValue && transaction.CreditsApplied != 0)
								////		{
								////			fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "EmailCreditCardReceiptWithZTripCredit.txt";
								////		}
								////		else
								////		{
								////			fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "EmailCreditCardReceipt.txt";
								////		}
								////	}
								////}

								//var templates = fileName.ReadHtmlFromUrl().Split(new string[] { "##" }, StringSplitOptions.None); //File.ReadAllText(fileName).Split(new string[] { "##" }, StringSplitOptions.None);
								//var headerTemplate = templates[0];
								//var bodyTemplate = templates[1];


								//#region Getting Member Profile
								////var membershipController = new UDI.SDS.MembershipController(token, TrackTime);

								////var profile = membershipController.GetProfileCommunicationSettings(memberID.ToInt32());
								//#endregion

								//#region Making the Body
								//var body = (string)bodyTemplate.Clone();
								//body = body.Replace("{FirstName}", trip.ecar_trip.FirstName);
								//body = body.Replace("{LastName}", trip.ecar_trip.LastName);
								//body = body.Replace("{ProviderTripID}", trip.ecar_trip.DispatchTripId);
								//body = body.Replace("{PUDateTime}", string.Format("{0:MM-dd-yyyy hh:mm tt}", trip.PickupDateTime));
								//body = body.Replace("{PUAddress}", trip.ecar_trip.PickupFullAddress);
								////body = body.Replace("{MemberPhone}", profile.ContactNumber.MakeStandardPhoneForamt());
								//if (!string.IsNullOrWhiteSpace(trip.ecar_trip.DropOffFullAddress))
								//{
								//	body = body.Replace("{DOAddress}", trip.ecar_trip.DropOffFullAddress);
								//}
								//else
								//{
								//	body = body.Replace("{DOAddress}", "As Directed");
								//}
								//body = body.Replace("{CorporateCompanyName}", specialInputs.Where(s => s.Name.ToLower().Trim() == "corporatecompanyname").FirstOrDefault().Value);
								////body = body.Replace("{FleetPhone}", fleet.PhoneNumber.MakeStandardPhoneForamt());

								////if (tripStatus == Taxi.Const.TaxiTripStatusType.Completed)
								////{
								////if (trip.TotalFareAmount.HasValue && trip.TotalFareAmount.Value > 0)
								////{
								////	if (transaction != null && transaction.CreditsApplied.HasValue && transaction.CreditsApplied.Value != 0)
								////	{
								////		if (trip.Gratuity.HasValue && trip.Gratuity.Value > 0)
								////		{
								////			body = body.Replace("{Fare}", (trip.TotalFareAmount - trip.Gratuity).ToString());
								////			body = body.Replace("{Tip}", trip.Gratuity.Value.ToString());
								////			body = body.Replace("{zTripCredit}", string.Format("({0})", System.Math.Abs(transaction.CreditsApplied.Value)));
								////			body = body.Replace("{TotalFare}", (trip.TotalFareAmount - System.Math.Abs(transaction.CreditsApplied.Value)).ToString());
								////		}
								////		else
								////		{
								////			body = body.Replace("{Fare}", trip.TotalFareAmount.ToString());
								////			body = body.Replace("{Tip}", string.Empty);
								////			body = body.Replace("{zTripCredit}", string.Format("({0})", System.Math.Abs(transaction.CreditsApplied.Value)));
								////			body = body.Replace("{TotalFare}", (trip.TotalFareAmount - System.Math.Abs(transaction.CreditsApplied.Value)).ToString());
								////		}
								////	}
								////	else
								////	{
								////		if (trip.Gratuity.HasValue && trip.Gratuity.Value > 0)
								////		{
								////			body = body.Replace("{Fare}", (trip.TotalFareAmount - trip.Gratuity).ToString());
								////			body = body.Replace("{Tip}", trip.Gratuity.Value.ToString());
								////			body = body.Replace("{TotalFare}", (trip.TotalFareAmount).ToString());
								////		}
								////		else
								////		{
								////			body = body.Replace("{Fare}", trip.TotalFareAmount.ToString());
								////			body = body.Replace("{Tip}", string.Empty);
								////			body = body.Replace("{TotalFare}", trip.TotalFareAmount.ToString());
								////		}
								////	}

								////	if (trip.PaymentType.ToLower().Trim() == PaymentType.PaymentCard.ToString().Trim().ToLower())
								////	{

								////		var accountController = new UDI.SDS.AccoutingController(token, TrackTime);
								////		var creditCardList = accountController.GetCreditCardAccounts(memberID.ToInt32());
								////		if (creditCardList != null && creditCardList.Any())
								////		{
								////			var creditcard = creditCardList.Where(s => s.AccountID == trip.CreditCardID.ToInt32()).FirstOrDefault();
								////			if (creditcard != null)
								////			{
								////				body = body.Replace("{CardType}", creditcard.CardType.ToString());
								////				body = body.Replace("{BilledTo}", creditcard.BillingName.ToString());
								////				body = body.Replace("{CreditCardLast4}", creditcard.Last4.ToString());

								////				string cardFormat;

								////				switch (creditcard.CardType)
								////				{
								////					case UDI.SDS.MembershipService.CreditCardTypesEnumeration.AMEX:
								////						cardFormat = "XXXX-XXXXXX-X{0}";
								////						break;
								////					case UDI.SDS.MembershipService.CreditCardTypesEnumeration.DINERS:
								////						cardFormat = "XXXX-XXXXXX-{0}";
								////						break;
								////					case UDI.SDS.MembershipService.CreditCardTypesEnumeration.JCB:
								////						cardFormat = "XXXX-XXXXXXXX{0}";
								////						break;
								////					default:
								////						cardFormat = "XXXX-XXXX-XXXX-{0}";
								////						break;
								////				}
								////				body = body.Replace("{CardNumber}", string.Format(cardFormat, creditcard.Last4));
								////			}
								////			else
								////			{
								////				body = body.Replace("{CardType}", string.Empty);
								////				body = body.Replace("{CardNumber}", string.Empty);
								////				body = body.Replace("{BilledTo}", string.Empty);
								////				body = body.Replace("{CreditCardLast4}", string.Empty);
								////			}
								////		}

								////	}
								////	if (trip.PaymentType.ToLower().Trim() == PaymentType.Cash.ToString().Trim().ToLower())
								////	{
								////	}
								////}
								////}
								//#endregion

								//#region Making the Header
								//var header = (string)headerTemplate.Clone();
								//header = header.Replace("{Last4ProviderTripID}", trip.ecar_trip.DispatchTripId.Substring(trip.ecar_trip.DispatchTripId.Length - 4));
								//#endregion

								//#region Sending Email
								//try
								//{
								//	var from = System.Configuration.ConfigurationManager.AppSettings["Notification_Email_From"];
								//	var host = System.Configuration.ConfigurationManager.AppSettings["Notification_Email_SMTP_Host"];
								//	var port = System.Configuration.ConfigurationManager.AppSettings["Notification_Email_SMTP_Port"];

								//	MessageController messageController = new MessageController();
								//	SMTPEmailMessage smtpMessage = new SMTPEmailMessage();
								//	smtpMessage.From = from;// "NoReply@zTrip.com";

								//	#region To Email
								//	string email = input.EmailAddress;

								//	if (string.IsNullOrWhiteSpace(email))
								//	{
								//		email = specialInputs.Where(s => s.Name.ToLower().Trim() == "corporateemail").FirstOrDefault().Value;
								//	}


								//	if (email.Contains("<"))
								//	{
								//		var l = email.Split('<');
								//		smtpMessage.To = new List<string> { l.First().Trim() };
								//	}
								//	else
								//	{
								//		smtpMessage.To = new List<string> { email.Trim() };
								//	}
								//	#endregion

								//	//smtpMessage.Subject = headerTemplate;
								//	smtpMessage.Subject = header;
								//	smtpMessage.MessageBody = body;
								//	smtpMessage.SMTP_Host = host;// "192.168.20.110";
								//	smtpMessage.SMTP_Port = port.ToInt32();// 25;
								//	smtpMessage.IsBodyHTML = false;

								//	var errorMessage = string.Empty;
								//	if (messageController.SendSMTPEmailMessage(smtpMessage, out errorMessage))
								//	{
								//		logger.InfoFormat("notifyBookedStatus for MemberID: {0} sent successfully", memberID);
								//	}
								//	else
								//	{
								//		logger.ErrorFormat("Error for notifyBookedStatus for MemberID: {0} -- {1}", memberID, errorMessage);
								//	}
								//}
								//catch (Exception ex)
								//{
								//	var t = ex.GetType();
								//	logger.Error(ex);
								//	logger.ErrorFormat("Error for MemberRequestEmailConfirmation for MemberID: {0} and TripID {1}", memberID, tripID);
								//	throw VtodException.CreateException(ExceptionType.Membership, 7005);//throw new MembershipException(Messages.Membership_SendingEmailProblem);

								//}
								//#endregion
								#endregion
								string alephTripID = tripID.ToString();
								#region Call Aleph Domain
								if (input != null && input.Corporate != null && input.Corporate.CorporateAccount != null && input.Corporate.CorporateAccount.Corporation != null && input.Corporate.CorporateAccount.Corporation.ToLower().ToString() == "Aleph".ToLower().ToString())
								{

									var alephAPICall = new AlephAPICall();
									string alephKey = null;
									var membershipController = new UDI.SDS.MembershipController(token, TrackTime);
									#region Call Aleph API 
									logger.InfoFormat("Start Aleph API Call");
									try
									{
										var corporateAccount = membershipController.GetCorporateAccountKey(input.Corporate.CorporateAccount.Corporation, input.EmailAddress);
										if (corporateAccount != null && !string.IsNullOrWhiteSpace(corporateAccount.UserKey))
										{
											alephKey = corporateAccount.UserKey;
										}
									}
									catch (Exception ex1)
									{
										throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(ex1.Message));
									}
									logger.InfoFormat("Start Aleph API Call");
									var alephResult = alephAPICall.MemberRequestEmailConfirmation(input.CorporateCompanyRequirements[0].CorporateCompanyName, input.CorporateCompanyRequirements[0].CorporateCompanyAccounts[0].AccountName, alephTripID.ToString(), input.EmailAddress, alephKey);
									logger.InfoFormat("End Aleph API Call");
									#endregion
								}
                                #endregion

                                #region ECar Texas
                                else if (trip.FleetTripCode==FleetTripCode.Ecar_Texas)
                                {
                                   
                                    var fleet = db.ecar_fleet.Where(s => s.Id == trip.ecar_trip.FleetId).FirstOrDefault();

                                    string fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "ECarEmailConfirmation.txt";

                                    if (trip.TotalFareAmount.HasValue && trip.TotalFareAmount.Value > 0)
                                    {
                                        if (trip.PaymentType.ToLower().Trim() == PaymentType.Cash.ToString().Trim().ToLower())
                                        {
                                            fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "ECarEmailCashCardReceipt.txt";
                                        }
                                        if (trip.PaymentType.ToLower().Trim() == PaymentType.PaymentCard.ToString().Trim().ToLower())
                                        {
                                            if (transaction != null && transaction.CreditsApplied.HasValue && transaction.CreditsApplied != 0)
                                            {
                                                fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "ECarEmailCreditCardReceiptWithZTripCredit.txt";
                                            }
                                            else
                                            {
                                                fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "ECarEmailCreditCardReceipt.txt";
                                            }
                                        }
                                    }

                                    var templates = fileName.ReadHtmlFromUrl().Split(new string[] { "##" }, StringSplitOptions.None); //File.ReadAllText(fileName).Split(new string[] { "##" }, StringSplitOptions.None);
                                    var headerTemplate = templates[0];
                                    var bodyTemplate = templates[1];


                                    #region Getting Member Profile
                                    UDI.SDS.MembershipService.CommunicationSettings profile = null;
                                    var membershipController = new UDI.SDS.MembershipController(token, TrackTime);
                                    if (!string.IsNullOrWhiteSpace(memberID))
                                        profile = membershipController.GetProfileCommunicationSettings(memberID.ToInt32());
                                    #endregion

                                    #region Making the Body
                                    var body = (string)bodyTemplate.Clone();
                                    body = body.Replace("{FirstName}", trip.ecar_trip.FirstName);
                                    body = body.Replace("{LastName}", trip.ecar_trip.LastName);
                                    body = body.Replace("{ProviderTripID}", trip.ecar_trip.DispatchTripId);
                                    body = body.Replace("{PUDateTime}", string.Format("{0:MM-dd-yyyy hh:mm tt}", trip.PickupDateTime));
                                    body = body.Replace("{PUAddress}", trip.ecar_trip.PickupFullAddress);
                                    if (profile != null)
                                    {
                                        body = body.Replace("{MemberPhone}", profile.ContactNumber.MakeStandardPhoneForamt());
                                    }
                                    else
                                    {
                                        if (input.Verification != null && input.Verification.TelephoneInfo != null && input.Verification.TelephoneInfo.CountryAccessCode != null && input.Verification.TelephoneInfo.AreaCityCode != null && input.Verification.TelephoneInfo.PhoneNumber != null)
                                        {
                                            string phoneNumber = input.Verification.TelephoneInfo.CountryAccessCode + input.Verification.TelephoneInfo.AreaCityCode + input.Verification.TelephoneInfo.PhoneNumber;
                                            body = body.Replace("{MemberPhone}", phoneNumber.MakeStandardPhoneForamt());
                                        }
                                    }
                                    if (!string.IsNullOrWhiteSpace(trip.ecar_trip.DropOffFullAddress))
                                    {
                                        body = body.Replace("{DOAddress}", trip.ecar_trip.DropOffFullAddress);
                                    }
                                    else
                                    {
                                        body = body.Replace("{DOAddress}", "As Directed");
                                    }
                                    body = body.Replace("{FleetName}", fleet.Alias);
                                    body = body.Replace("{FleetPhone}", fleet.PhoneNumber.MakeStandardPhoneForamt());
                                    
                                    if (trip.TotalFareAmount.HasValue && trip.TotalFareAmount.Value > 0)
                                    {
                                        if (transaction != null && transaction.CreditsApplied.HasValue && transaction.CreditsApplied.Value != 0)
                                        {
                                            if (trip.Gratuity.HasValue && trip.Gratuity.Value > 0)
                                            {
                                                body = body.Replace("{Fare}", (trip.TotalFareAmount - trip.Gratuity).ToString());
                                                body = body.Replace("{Tip}", trip.Gratuity.Value.ToString());
                                                body = body.Replace("{zTripCredit}", string.Format("({0})", System.Math.Abs(transaction.CreditsApplied.Value)));
                                                body = body.Replace("{TotalFare}", (trip.TotalFareAmount - System.Math.Abs(transaction.CreditsApplied.Value)).ToString());
                                            }
                                            else
                                            {
                                                body = body.Replace("{Fare}", trip.TotalFareAmount.ToString());
                                                body = body.Replace("{Tip}", string.Empty);
                                                body = body.Replace("{zTripCredit}", string.Format("({0})", System.Math.Abs(transaction.CreditsApplied.Value)));
                                                body = body.Replace("{TotalFare}", (trip.TotalFareAmount - System.Math.Abs(transaction.CreditsApplied.Value)).ToString());
                                            }
                                        }
                                        else
                                        {
                                            if (trip.Gratuity.HasValue && trip.Gratuity.Value > 0)
                                            {
                                                body = body.Replace("{Fare}", (trip.TotalFareAmount - trip.Gratuity).ToString());
                                                body = body.Replace("{Tip}", trip.Gratuity.Value.ToString());
                                                body = body.Replace("{TotalFare}", (trip.TotalFareAmount).ToString());
                                            }
                                            else
                                            {
                                                body = body.Replace("{Fare}", trip.TotalFareAmount.ToString());
                                                body = body.Replace("{Tip}", string.Empty);
                                                body = body.Replace("{TotalFare}", trip.TotalFareAmount.ToString());
                                            }
                                        }

                                        if (trip.PaymentType.ToLower().Trim() == PaymentType.PaymentCard.ToString().Trim().ToLower())
                                        {

                                            var accountController = new UDI.SDS.AccoutingController(token, TrackTime);
                                            var creditCardList = accountController.GetCreditCardAccounts(memberID.ToInt32());
                                            if (creditCardList != null && creditCardList.Any())
                                            {
                                                var creditcard = creditCardList.Where(s => s.AccountID == trip.CreditCardID.ToInt32()).FirstOrDefault();
                                                if (creditcard != null)
                                                {
                                                    body = body.Replace("{CardType}", creditcard.CardType.ToString());
                                                    body = body.Replace("{BilledTo}", creditcard.BillingName.ToString());
                                                    body = body.Replace("{CreditCardLast4}", creditcard.Last4.ToString());

                                                    string cardFormat;

                                                    switch (creditcard.CardType)
                                                    {
                                                        case UDI.SDS.MembershipService.CreditCardTypesEnumeration.AMEX:
                                                            cardFormat = "XXXX-XXXXXX-X{0}";
                                                            break;
                                                        case UDI.SDS.MembershipService.CreditCardTypesEnumeration.DINERS:
                                                            cardFormat = "XXXX-XXXXXX-{0}";
                                                            break;
                                                        case UDI.SDS.MembershipService.CreditCardTypesEnumeration.JCB:
                                                            cardFormat = "XXXX-XXXXXXXX{0}";
                                                            break;
                                                        default:
                                                            cardFormat = "XXXX-XXXX-XXXX-{0}";
                                                            break;
                                                    }
                                                    body = body.Replace("{CardNumber}", string.Format(cardFormat, creditcard.Last4));
                                                }
                                                else
                                                {
                                                    body = body.Replace("{CardType}", string.Empty);
                                                    body = body.Replace("{CardNumber}", string.Empty);
                                                    body = body.Replace("{BilledTo}", string.Empty);
                                                    body = body.Replace("{CreditCardLast4}", string.Empty);
                                                }
                                            }

                                        }
                                        if (trip.PaymentType.ToLower().Trim() == PaymentType.Cash.ToString().Trim().ToLower())
                                        {
                                        }
                                    }
                                    //}
                                    #endregion

                                    #region Making the Header
                                    var header = (string)headerTemplate.Clone();
                                    header = header.Replace("{ProviderTripID}", trip.ecar_trip.DispatchTripId);
                                    #endregion

                                    #region Sending Email
                                    try
                                    {
                                        var from = System.Configuration.ConfigurationManager.AppSettings["Notification_Email_From"];
                                        var host = System.Configuration.ConfigurationManager.AppSettings["Notification_Email_SMTP_Host"];
                                        var port = System.Configuration.ConfigurationManager.AppSettings["Notification_Email_SMTP_Port"];

                                        MessageController messageController = new MessageController();
                                        SMTPEmailMessage smtpMessage = new SMTPEmailMessage();
                                        smtpMessage.From = from;// "NoReply@zTrip.com";

                                        #region To Email
                                        string email = input.EmailAddress;

                                        if (string.IsNullOrWhiteSpace(email))
                                        {
                                            email = profile.EmailAddress;
                                        }


                                        if (email.Contains("<"))
                                        {
                                            var l = email.Split('<');
                                            smtpMessage.To = new List<string> { l.First().Trim() };
                                        }
                                        else
                                        {
                                            smtpMessage.To = new List<string> { email.Trim() };
                                        }
                                        #endregion

                                        //smtpMessage.Subject = headerTemplate;
                                        smtpMessage.Subject = header;
                                        smtpMessage.MessageBody = body;
                                        smtpMessage.SMTP_Host = host;// "192.168.20.110";
                                        smtpMessage.SMTP_Port = port.ToInt32();// 25;
                                        smtpMessage.IsBodyHTML = true;

                                        var errorMessage = string.Empty;
                                        if (messageController.SendSMTPEmailMessage(smtpMessage, out errorMessage))
                                        {
                                            logger.InfoFormat("notifyBookedStatus for MemberID: {0} sent successfully", memberID);
                                        }
                                        else
                                        {
                                            logger.ErrorFormat("Error for notifyBookedStatus for MemberID: {0} -- {1}", memberID, errorMessage);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        var t = ex.GetType();
                                        logger.Error(ex);
                                        logger.ErrorFormat("Error for MemberRequestEmailConfirmation for MemberID: {0} and TripID {1}", memberID, tripID);
                                        throw VtodException.CreateException(ExceptionType.Membership, 7005);//throw new MembershipException(Messages.Membership_SendingEmailProblem);

                                    }
                                    #endregion
                                }
                                #endregion

                            }
                            else if (trip.FleetType == FleetType.Van.ToString())
                            {
                                //var taxiTrip = (taxi_trip)trip;
                                var fleet = db.van_fleet.Where(s => s.Id == trip.van_trip.FleetId).FirstOrDefault();

                                string fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "VanEmailConfirmation.txt";

                                if (trip.TotalFareAmount.HasValue && trip.TotalFareAmount.Value > 0)
                                {
                                    if (trip.PaymentType.ToLower().Trim() == PaymentType.Cash.ToString().Trim().ToLower())
                                    {
                                        fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "VanEmailCashCardReceipt.txt";
                                    }
                                    if (trip.PaymentType.ToLower().Trim() == PaymentType.PaymentCard.ToString().Trim().ToLower())
                                    {
                                        if (transaction != null && transaction.CreditsApplied.HasValue && transaction.CreditsApplied != 0)
                                        {
                                            fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "VanEmailCreditCardReceiptWithZTripCredit.txt";
                                        }
                                        else
                                        {
                                            fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "VanEmailCreditCardReceipt.txt";
                                        }
                                    }
                                }

                                var templates = fileName.ReadHtmlFromUrl().Split(new string[] { "##" }, StringSplitOptions.None); //File.ReadAllText(fileName).Split(new string[] { "##" }, StringSplitOptions.None);
                                var headerTemplate = templates[0];
                                var bodyTemplate = templates[1];


                                #region Getting Member Profile
                                UDI.SDS.MembershipService.CommunicationSettings profile = null;
                                var membershipController = new UDI.SDS.MembershipController(token, TrackTime);
                                if (!string.IsNullOrWhiteSpace(memberID))
                                    profile = membershipController.GetProfileCommunicationSettings(memberID.ToInt32());
                                #endregion

                                #region Making the Body
                                var body = (string)bodyTemplate.Clone();
                                body = body.Replace("{FirstName}", trip.van_trip.FirstName);
                                body = body.Replace("{LastName}", trip.van_trip.LastName);
                                body = body.Replace("{ProviderTripID}", trip.van_trip.DispatchTripId);
                                body = body.Replace("{PUDateTime}", string.Format("{0:MM-dd-yyyy hh:mm tt}", trip.PickupDateTime));
                                body = body.Replace("{PUAddress}", trip.van_trip.PickupFullAddress);
                                if (profile != null)
                                {
                                    body = body.Replace("{MemberPhone}", profile.ContactNumber.MakeStandardPhoneForamt());
                                }
                                else
                                {
                                    if (input.Verification != null && input.Verification.TelephoneInfo != null && input.Verification.TelephoneInfo.CountryAccessCode != null && input.Verification.TelephoneInfo.AreaCityCode != null && input.Verification.TelephoneInfo.PhoneNumber != null)
                                    {
                                        string phoneNumber = input.Verification.TelephoneInfo.CountryAccessCode + input.Verification.TelephoneInfo.AreaCityCode + input.Verification.TelephoneInfo.PhoneNumber;
                                        body = body.Replace("{MemberPhone}", phoneNumber.MakeStandardPhoneForamt());
                                    }
                                }
                                if (!string.IsNullOrWhiteSpace(trip.van_trip.DropOffFullAddress))
                                {
                                    body = body.Replace("{DOAddress}", trip.van_trip.DropOffFullAddress);
                                }
                                else
                                {
                                    body = body.Replace("{DOAddress}", "As Directed");
                                }
                                body = body.Replace("{FleetName}", fleet.Alias);
                                body = body.Replace("{FleetPhone}", fleet.PhoneNumber.MakeStandardPhoneForamt());

                                //if (tripStatus == Taxi.Const.TaxiTripStatusType.Completed)
                                //{
                                if (trip.TotalFareAmount.HasValue && trip.TotalFareAmount.Value > 0)
                                {
                                    if (transaction != null && transaction.CreditsApplied.HasValue && transaction.CreditsApplied.Value != 0)
                                    {
                                        if (trip.Gratuity.HasValue && trip.Gratuity.Value > 0)
                                        {
                                            body = body.Replace("{Fare}", (trip.TotalFareAmount - trip.Gratuity).ToString());
                                            body = body.Replace("{Tip}", trip.Gratuity.Value.ToString());
                                            body = body.Replace("{zTripCredit}", string.Format("({0})", System.Math.Abs(transaction.CreditsApplied.Value)));
                                            body = body.Replace("{TotalFare}", (trip.TotalFareAmount - System.Math.Abs(transaction.CreditsApplied.Value)).ToString());
                                        }
                                        else
                                        {
                                            body = body.Replace("{Fare}", trip.TotalFareAmount.ToString());
                                            body = body.Replace("{Tip}", string.Empty);
                                            body = body.Replace("{zTripCredit}", string.Format("({0})", System.Math.Abs(transaction.CreditsApplied.Value)));
                                            body = body.Replace("{TotalFare}", (trip.TotalFareAmount - System.Math.Abs(transaction.CreditsApplied.Value)).ToString());
                                        }
                                    }
                                    else
                                    {
                                        if (trip.Gratuity.HasValue && trip.Gratuity.Value > 0)
                                        {
                                            body = body.Replace("{Fare}", (trip.TotalFareAmount - trip.Gratuity).ToString());
                                            body = body.Replace("{Tip}", trip.Gratuity.Value.ToString());
                                            body = body.Replace("{TotalFare}", (trip.TotalFareAmount).ToString());
                                        }
                                        else
                                        {
                                            body = body.Replace("{Fare}", trip.TotalFareAmount.ToString());
                                            body = body.Replace("{Tip}", string.Empty);
                                            body = body.Replace("{TotalFare}", trip.TotalFareAmount.ToString());
                                        }
                                    }

                                    if (trip.PaymentType.ToLower().Trim() == PaymentType.PaymentCard.ToString().Trim().ToLower())
                                    {

                                        var accountController = new UDI.SDS.AccoutingController(token, TrackTime);
                                        var creditCardList = accountController.GetCreditCardAccounts(memberID.ToInt32());
                                        if (creditCardList != null && creditCardList.Any())
                                        {
                                            var creditcard = creditCardList.Where(s => s.AccountID == trip.CreditCardID.ToInt32()).FirstOrDefault();
                                            if (creditcard != null)
                                            {
                                                body = body.Replace("{CardType}", creditcard.CardType.ToString());
                                                body = body.Replace("{BilledTo}", creditcard.BillingName.ToString());
                                                body = body.Replace("{CreditCardLast4}", creditcard.Last4.ToString());

                                                string cardFormat;

                                                switch (creditcard.CardType)
                                                {
                                                    case UDI.SDS.MembershipService.CreditCardTypesEnumeration.AMEX:
                                                        cardFormat = "XXXX-XXXXXX-X{0}";
                                                        break;
                                                    case UDI.SDS.MembershipService.CreditCardTypesEnumeration.DINERS:
                                                        cardFormat = "XXXX-XXXXXX-{0}";
                                                        break;
                                                    case UDI.SDS.MembershipService.CreditCardTypesEnumeration.JCB:
                                                        cardFormat = "XXXX-XXXXXXXX{0}";
                                                        break;
                                                    default:
                                                        cardFormat = "XXXX-XXXX-XXXX-{0}";
                                                        break;
                                                }
                                                body = body.Replace("{CardNumber}", string.Format(cardFormat, creditcard.Last4));
                                            }
                                            else
                                            {
                                                body = body.Replace("{CardType}", string.Empty);
                                                body = body.Replace("{CardNumber}", string.Empty);
                                                body = body.Replace("{BilledTo}", string.Empty);
                                                body = body.Replace("{CreditCardLast4}", string.Empty);
                                            }
                                        }

                                    }
                                    if (trip.PaymentType.ToLower().Trim() == PaymentType.Cash.ToString().Trim().ToLower())
                                    {
                                    }
                                }
                                //}
                                #endregion

                                #region Making the Header
                                var header = (string)headerTemplate.Clone();
                                header = header.Replace("{ProviderTripID}", trip.van_trip.DispatchTripId);
                                #endregion

                                #region Sending Email
                                try
                                {
                                    var from = System.Configuration.ConfigurationManager.AppSettings["Notification_Email_From"];
                                    var host = System.Configuration.ConfigurationManager.AppSettings["Notification_Email_SMTP_Host"];
                                    var port = System.Configuration.ConfigurationManager.AppSettings["Notification_Email_SMTP_Port"];

                                    MessageController messageController = new MessageController();
                                    SMTPEmailMessage smtpMessage = new SMTPEmailMessage();
                                    smtpMessage.From = from;// "NoReply@zTrip.com";

                                    #region To Email
                                    string email = input.EmailAddress;

                                    if (string.IsNullOrWhiteSpace(email))
                                    {
                                        email = profile.EmailAddress;
                                    }


                                    if (email.Contains("<"))
                                    {
                                        var l = email.Split('<');
                                        smtpMessage.To = new List<string> { l.First().Trim() };
                                    }
                                    else
                                    {
                                        smtpMessage.To = new List<string> { email.Trim() };
                                    }
                                    #endregion

                                    //smtpMessage.Subject = headerTemplate;
                                    smtpMessage.Subject = header;
                                    smtpMessage.MessageBody = body;
                                    smtpMessage.SMTP_Host = host;// "192.168.20.110";
                                    smtpMessage.SMTP_Port = port.ToInt32();// 25;
                                    smtpMessage.IsBodyHTML = true;

                                    var errorMessage = string.Empty;
                                    if (messageController.SendSMTPEmailMessage(smtpMessage, out errorMessage))
                                    {
                                        logger.InfoFormat("notifyBookedStatus for MemberID: {0} sent successfully", memberID);
                                    }
                                    else
                                    {
                                        logger.ErrorFormat("Error for notifyBookedStatus for MemberID: {0} -- {1}", memberID, errorMessage);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    var t = ex.GetType();
                                    logger.Error(ex);
                                    logger.ErrorFormat("Error for MemberRequestEmailConfirmation for MemberID: {0} and TripID {1}", memberID, tripID);
                                    throw VtodException.CreateException(ExceptionType.Membership, 7005);//throw new MembershipException(Messages.Membership_SendingEmailProblem);

                                }
                                #endregion
                            }

						}

						else if (
							trip.FleetType == FleetType.SuperShuttle.ToString() || trip.FleetType == FleetType.ExecuCar.ToString() || trip.FleetType == FleetType.SuperShuttle_ExecuCar.ToString() || trip.FleetType == FleetType.SuperShuttleSharedRideOnly.ToString()
							||
							trip.FleetType == FleetType.Taxi.ToString() && trip.taxi_trip != null && trip.taxi_trip.ServiceAPIId == 3 /* 3 = SDS, it's hardcoded as of now. better not to be hardcoded */
                           )
						{
                            if ((string.IsNullOrEmpty(input.MemberID) || memberID == "0") && trip.FleetType == FleetType.Taxi.ToString())
                            {
                                throw VtodException.CreateException(ExceptionType.Membership, 7022);
                            }
							if (input.Verification != null)
							{
								//public void RequestEmailConfirmation(string email, string confirmationNumber, string lastName, string phoneNumber, string postalCode)
								string confNumber = null;
								string lastName = null;
								string phoneNumber = null;
								string postalCode = null;
								if (input.UniqueID.Type.ToLower().Trim() == "dispatchconfirmation")
								{
									confNumber = input.UniqueID.ID;
								}
								else
								{
									if (trip.FleetType == FleetType.Taxi.ToString())
									{
										confNumber = trip.taxi_trip.DispatchTripId;
									}
                                    if (trip.FleetType == FleetType.Van.ToString())
                                    {
                                        confNumber = trip.van_trip.DispatchTripId;
                                    }
								}
								if (!string.IsNullOrWhiteSpace(confNumber))
								{
									if (input.Verification.PersonName != null)
									{
										lastName = input.Verification.PersonName.Surname;
									}
									if (input.Verification.TelephoneInfo != null)
									{
										phoneNumber = string.Format("{0}{1}{2}", input.Verification.TelephoneInfo.CountryAccessCode, input.Verification.TelephoneInfo.AreaCityCode, input.Verification.TelephoneInfo.PhoneNumber);
									}
									if (input.Verification.AddressInfo != null)
									{
										postalCode = input.Verification.AddressInfo.PostalCode;
									}
									reservationController.RequestEmailConfirmation(
										input.EmailAddress, confNumber, lastName, phoneNumber, postalCode
										);
								}
								else
								{
									throw VtodException.CreateException(ExceptionType.Membership, 7002);// new MembershipException(Messages.Taxi_TripNotFound);
								}
							}
							else
							{
								//var sdsTrip = (sds_trip)trip;
								reservationController.RequestEmailConfirmation(
									input.EmailAddress,
									trip.sds_trip != null ? trip.sds_trip.ConfirmationNumber : trip.taxi_trip.DispatchTripId,
									trip.sds_trip != null ? trip.sds_trip.LastName : trip.taxi_trip.LastName,
									string.Empty, string.Empty
									);
							}
						}
					}

					else
					{
						if (input != null && input.Corporate != null && input.Corporate.CorporateAccount != null && input.Corporate.CorporateAccount.Corporation != null && input.Corporate.CorporateAccount.Corporation.ToLower().ToString() == "Aleph".ToLower().ToString())
						{
							#region Call Aleph Domain
							#region MyRegion
							string alephTripID = string.Empty;
							string trID;
							if (input.UniqueID.Type.ToLower().Trim() == Common.DTO.Enum.ConfirmationType.Confirmation.ToString().ToLower().Trim())
							{
								var ecarTrip = db.ecar_trip.Where(s => s.Id.ToString() == input.UniqueID.ID).FirstOrDefault();
								alephTripID = ecarTrip.DispatchTripId;
							}
							else
							{
								alephTripID = input.UniqueID.ID;
							}
							#region Parsing
							if (alephTripID.ToString().Contains("##"))
							{
								if (alephTripID.ToString().IndexOf("##") != -1)
								{
									trID = alephTripID.Substring(0, alephTripID.ToString().IndexOf("##"));
								}
								else
									trID = alephTripID;
							}
							else
								trID = alephTripID;
							#endregion
							#endregion
							var alephAPICall = new AlephAPICall();
							string alephKey = null;
							var membershipController = new UDI.SDS.MembershipController(token, TrackTime);
							#region Call Aleph API 
							logger.InfoFormat("Start Aleph API Call");
							try
							{
								var corporateAccount = membershipController.GetCorporateAccountKey(input.Corporate.CorporateAccount.Corporation, input.EmailAddress);
								if (corporateAccount != null && !string.IsNullOrWhiteSpace(corporateAccount.UserKey))
								{
									alephKey = corporateAccount.UserKey;
								}
							}
							catch (Exception ex1)
							{
								throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(ex1.Message));
							}
							logger.InfoFormat("Start Aleph API Call");
							var alephResult = alephAPICall.MemberRequestEmailConfirmation(input.CorporateCompanyRequirements[0].CorporateCompanyName, input.CorporateCompanyRequirements[0].CorporateCompanyAccounts[0].AccountName, trID.ToString(), input.EmailAddress, alephKey);
							logger.InfoFormat("End Aleph API Call");
							#endregion
							#endregion

						}
						else
							throw VtodException.CreateException(ExceptionType.Membership, 7002);// new MembershipException(Messages.Taxi_TripNotFound);
					}
				}
				result.Success = new Success();

				#region Common
				result.EchoToken = input.EchoToken;
				result.PrimaryLangID = input.PrimaryLangID;
				result.Target = input.Target;
				result.Version = input.Version;
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.Error("VTODDomain.RequestEmailConfirmation", ex);

				#region Throw MembershipException
				throw new MembershipException(ex.Message);
				#endregion
			}
		}

        internal MemberRequestEmailCancellationRS MemberRequestEmailCancellation(TokenRS token, MemberRequestEmailCancellationRQ input)
        {
            var reservationController = new UDI.SDS.ReservationsController(token, TrackTime);
            string emailAddress = null;
            try
            {
                var result = new MemberRequestEmailCancellationRS();
                vtod_trip trip = null;
                long tripID = 0;
                string memberID = string.Empty;
                string dispatchTripID = null;
                vtod_trip_account_transaction transaction = null;
                using (var db = new VTODEntities())
                {
                    memberID = input.MemberID;

                    #region Find TripID if UniqueID is DispatchConfirmation

                    if (input.UniqueID.Type.ToLower().Trim() == Common.DTO.Enum.ConfirmationType.DispatchConfirmation.ToString().ToLower().Trim())
                    {
                        if (input.Corporate == null)
                        {
                            var sdsTrip = db.sds_trip.Where(s => s.ConfirmationNumber == input.UniqueID.ID).FirstOrDefault();
                            if (sdsTrip != null)
                            {
                                tripID = sdsTrip.Id;
                            }
                            var taxiTrip = db.taxi_trip.Where(s => s.DispatchTripId == input.UniqueID.ID).FirstOrDefault();
                            if (taxiTrip != null)
                            {
                                tripID = taxiTrip.Id;
                            }
                            var vanTrip = db.van_trip.Where(s => s.DispatchTripId == input.UniqueID.ID).FirstOrDefault();
                            if (vanTrip != null)
                            {
                                tripID = vanTrip.Id;
                            }
                            var ecarTrip = db.ecar_trip.Where(s => s.DispatchTripId == input.UniqueID.ID).FirstOrDefault();
                            if (ecarTrip != null)
                            {
                                tripID = ecarTrip.Id;
                            }
                        }

                    }
                    else
                    {
                        if (input.Corporate == null || input.Corporate.CorporateAccount == null)
                        {
                            tripID = input.UniqueID.ID.ToInt64();
                        }
                        else
                        {
                            #region MyRegion
                            string alephID = input.UniqueID.ID;
                            string trID = string.Empty;
                            #region Parsing
                            if (alephID.ToString().Contains("##"))
                            {
                                if (alephID.ToString().IndexOf("##") != -1)
                                {
                                    trID = alephID.ToString().Substring(0, alephID.ToString().IndexOf("##"));
                                }
                                else
                                    trID = alephID;
                            }
                            else
                                trID = alephID;

                            #endregion
                            var ecarTrip = db.ecar_trip.Where(s => s.DispatchTripId.ToString() == trID).FirstOrDefault();
                            if (input.UniqueID.Type.ToLower().Trim() == Common.DTO.Enum.ConfirmationType.Confirmation.ToString().ToLower().Trim())
                            {
                                if (ecarTrip != null)
                                    tripID = ecarTrip.DispatchTripId.ToInt64();
                            }
                            else
                            {
                                tripID = trID.ToInt64();
                            }

                            #endregion
                            if (ecarTrip != null)
                                trip = db.vtod_trip.Include("ecar_trip").Where(s => s.Id == ecarTrip.Id).FirstOrDefault();
                        }
                    }
                    #endregion

                    if (tripID > 0 && trip == null)
                    {
                        trip = db.vtod_trip.Include("taxi_trip").Include("sds_trip").Where(s => s.Id == tripID /*&& s.MemberID == memberID*/).FirstOrDefault();
                        if (input.UniqueID.Type.ToLower().Trim() == Common.DTO.Enum.ConfirmationType.DispatchConfirmation.ToString().ToLower().Trim())
                        {
                            if (db.van_trip.Where(s => s.DispatchTripId == input.UniqueID.ID).FirstOrDefault() != null)
                            {
                                trip = db.vtod_trip.Include("van_trip").Where(s => s.Id == tripID /*&& s.MemberID == memberID*/).FirstOrDefault();
                            }
                            if (trip!=null && trip.FleetTripCode==FleetTripCode.Ecar_Texas && db.ecar_trip.Where(s => s.DispatchTripId == input.UniqueID.ID).FirstOrDefault() != null)
                            {
                                trip = db.vtod_trip.Include("ecar_trip").Where(s => s.Id == tripID /*&& s.MemberID == memberID*/).FirstOrDefault();
                            }
                        }
                        else if (input.UniqueID.Type.ToLower().Trim() == Common.DTO.Enum.ConfirmationType.Confirmation.ToString().ToLower().Trim())
                        {
                            if (db.van_trip.Where(s => s.Id.ToString() == input.UniqueID.ID).FirstOrDefault() != null)
                            {
                                trip = db.vtod_trip.Include("van_trip").Where(s => s.Id == tripID /*&& s.MemberID == memberID*/).FirstOrDefault();
                            }
                            if (trip != null && trip.FleetTripCode == FleetTripCode.Ecar_Texas && db.ecar_trip.Where(s => s.Id.ToString() == input.UniqueID.ID).FirstOrDefault() != null)
                            {
                                trip = db.vtod_trip.Include("ecar_trip").Where(s => s.Id == tripID /*&& s.MemberID == memberID*/).FirstOrDefault();
                            }
                        }
                        transaction = db.vtod_trip_account_transaction.Where(s => s.TripID == tripID).OrderByDescending(s => s.Id).FirstOrDefault();
                    }

                    if (trip != null)
                    {
                        if (
                            (trip.FleetType == FleetType.Taxi.ToString() && trip.taxi_trip != null && trip.taxi_trip.ServiceAPIId != 3 /* 3 = SDS, it's hardcoded as of now. better not to be hardcoded */)
                            ||
                            (trip.FleetType == FleetType.ExecuCar.ToString() && trip.ecar_trip != null)
                            ||
                            (trip.FleetType == FleetType.Van.ToString() && trip.van_trip != null && trip.van_trip.ServiceAPIId != 3 /* 3 = SDS, it's hardcoded as of now. better not to be hardcoded */)
                           
                            )
                        {
                            if (trip.FleetType == FleetType.Taxi.ToString())
                            {
                              
                                if (string.IsNullOrEmpty(input.MemberID))
                                {
                                    if (input.MemberID != "0")
                                        memberID = input.MemberID;
                                }
                                #region "Logic for No MemberID"
                                if (string.IsNullOrEmpty(memberID) || memberID=="0")
                                {
                                    if (input.UniqueID.Type.ToLower().Trim() == Common.DTO.Enum.ConfirmationType.DispatchConfirmation.ToString().ToLower().Trim())
                                    {
                                        string firstname = null;
                                        string lastName = null;
                                        string phoneNumber = null;
                                        string postalCode = null;
                                        if (input.Verification.PersonName != null)
                                        {
                                            firstname = input.Verification.PersonName.GivenName;
                                            lastName = input.Verification.PersonName.Surname;
                                        }
                                        if (input.Verification.TelephoneInfo != null)
                                        {
                                            phoneNumber = string.Format("{0}{1}", input.Verification.TelephoneInfo.AreaCityCode, input.Verification.TelephoneInfo.PhoneNumber).CleanPhone();
                                        }
                                        if (input.Verification.AddressInfo != null)
                                        {
                                            postalCode = input.Verification.AddressInfo.PostalCode;
                                        }
                                        if (input.UniqueID.ID == trip.taxi_trip.DispatchTripId && firstname.Trim() == trip.taxi_trip.FirstName.Trim() && lastName.Trim() == trip.taxi_trip.LastName.Trim())
                                        {
                                            emailAddress = trip.EmailAddress;
                                        }
                                        else if (input.UniqueID.ID == trip.taxi_trip.DispatchTripId && phoneNumber.Trim() == trip.taxi_trip.PhoneNumber.Trim())
                                        {
                                            emailAddress = trip.EmailAddress;
                                        }
                                        else if (input.UniqueID.ID == trip.taxi_trip.DispatchTripId && postalCode.Trim() == trip.taxi_trip.PickupZipCode.Trim())
                                        {
                                            emailAddress = trip.EmailAddress;
                                        }
                                        if(string.IsNullOrEmpty(emailAddress))
                                        {
                                            throw VtodException.CreateException(ExceptionType.Membership, 7002);
                                        }
                                    }
                                }
                                #endregion

                                var fleet = db.taxi_fleet.Where(s => s.Id == trip.taxi_trip.FleetId).FirstOrDefault();

                                string fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "EmailCanceled.txt";
                                var templates = fileName.ReadHtmlFromUrl().Split(new string[] { "##" }, StringSplitOptions.None); 
                                var headerTemplate = templates[0];
                                var bodyTemplate = templates[1];


                                #region Getting Member Profile
                                UDI.SDS.MembershipService.CommunicationSettings profile = null;
                                var membershipController = new UDI.SDS.MembershipController(token, TrackTime);
                                if (!string.IsNullOrWhiteSpace(memberID) && memberID!="0")
                                    profile = membershipController.GetProfileCommunicationSettings(memberID.ToInt32());
                                #endregion

                                #region Making the Body
                                var body = (string)bodyTemplate.Clone();
                                body = body.Replace("{FirstName}", trip.taxi_trip.FirstName);
                                body = body.Replace("{LastName}", trip.taxi_trip.LastName);
                                body = body.Replace("{ProviderTripID}", trip.taxi_trip.DispatchTripId);
                                body = body.Replace("{PUDateTime}", string.Format("{0:MM-dd-yyyy hh:mm tt}", trip.PickupDateTime));
                                body = body.Replace("{PUAddress}", trip.taxi_trip.PickupFullAddress);
                                if (profile != null)
                                {
                                    body = body.Replace("{MemberPhone}", profile.ContactNumber.MakeStandardPhoneForamt());
                                }
                                else
                                {
                                    if (input.Verification != null && input.Verification.TelephoneInfo != null && input.Verification.TelephoneInfo.CountryAccessCode != null && input.Verification.TelephoneInfo.AreaCityCode != null && input.Verification.TelephoneInfo.PhoneNumber != null)
                                    {
                                        string phoneNumber = input.Verification.TelephoneInfo.CountryAccessCode + input.Verification.TelephoneInfo.AreaCityCode + input.Verification.TelephoneInfo.PhoneNumber;
                                        body = body.Replace("{MemberPhone}", phoneNumber.MakeStandardPhoneForamt());
                                    }
                                }
                                if (!string.IsNullOrWhiteSpace(trip.taxi_trip.DropOffFullAddress))
                                {
                                    body = body.Replace("{DOAddress}", trip.taxi_trip.DropOffFullAddress);
                                }
                                else
                                {
                                    body = body.Replace("{DOAddress}", "As Directed");
                                }
                                body = body.Replace("{FleetName}", fleet.Alias);
                                body = body.Replace("{FleetPhone}", fleet.PhoneNumber.MakeStandardPhoneForamt());
                                if (trip.TotalFareAmount.HasValue && trip.TotalFareAmount.Value > 0)
                                {
                                    if (transaction != null && transaction.CreditsApplied.HasValue && transaction.CreditsApplied.Value != 0)
                                    {
                                        if (trip.Gratuity.HasValue && trip.Gratuity.Value > 0)
                                        {
                                            body = body.Replace("{Fare}", (trip.TotalFareAmount - trip.Gratuity).ToString());
                                            body = body.Replace("{Tip}", trip.Gratuity.Value.ToString());
                                            body = body.Replace("{zTripCredit}", string.Format("({0})", System.Math.Abs(transaction.CreditsApplied.Value)));
                                            body = body.Replace("{TotalFare}", (trip.TotalFareAmount - System.Math.Abs(transaction.CreditsApplied.Value)).ToString());
                                        }
                                        else
                                        {
                                            body = body.Replace("{Fare}", trip.TotalFareAmount.ToString());
                                            body = body.Replace("{Tip}", string.Empty);
                                            body = body.Replace("{zTripCredit}", string.Format("({0})", System.Math.Abs(transaction.CreditsApplied.Value)));
                                            body = body.Replace("{TotalFare}", (trip.TotalFareAmount - System.Math.Abs(transaction.CreditsApplied.Value)).ToString());
                                        }
                                    }
                                    else
                                    {
                                        if (trip.Gratuity.HasValue && trip.Gratuity.Value > 0)
                                        {
                                            body = body.Replace("{Fare}", (trip.TotalFareAmount - trip.Gratuity).ToString());
                                            body = body.Replace("{Tip}", trip.Gratuity.Value.ToString());
                                            body = body.Replace("{TotalFare}", (trip.TotalFareAmount).ToString());
                                        }
                                        else
                                        {
                                            body = body.Replace("{Fare}", trip.TotalFareAmount.ToString());
                                            body = body.Replace("{Tip}", string.Empty);
                                            body = body.Replace("{TotalFare}", trip.TotalFareAmount.ToString());
                                        }
                                    }

                                    if (trip.PaymentType.ToLower().Trim() == PaymentType.PaymentCard.ToString().Trim().ToLower())
                                    {

                                        var accountController = new UDI.SDS.AccoutingController(token, TrackTime);
                                        var creditCardList = accountController.GetCreditCardAccounts(memberID.ToInt32());
                                        if (creditCardList != null && creditCardList.Any())
                                        {
                                            var creditcard = creditCardList.Where(s => s.AccountID == trip.CreditCardID.ToInt32()).FirstOrDefault();
                                            if (creditcard != null)
                                            {
                                                body = body.Replace("{CardType}", creditcard.CardType.ToString());
                                                body = body.Replace("{BilledTo}", creditcard.BillingName.ToString());
                                                body = body.Replace("{CreditCardLast4}", creditcard.Last4.ToString());

                                                string cardFormat;

                                                switch (creditcard.CardType)
                                                {
                                                    case UDI.SDS.MembershipService.CreditCardTypesEnumeration.AMEX:
                                                        cardFormat = "XXXX-XXXXXX-X{0}";
                                                        break;
                                                    case UDI.SDS.MembershipService.CreditCardTypesEnumeration.DINERS:
                                                        cardFormat = "XXXX-XXXXXX-{0}";
                                                        break;
                                                    case UDI.SDS.MembershipService.CreditCardTypesEnumeration.JCB:
                                                        cardFormat = "XXXX-XXXXXXXX{0}";
                                                        break;
                                                    default:
                                                        cardFormat = "XXXX-XXXX-XXXX-{0}";
                                                        break;
                                                }
                                                body = body.Replace("{CardNumber}", string.Format(cardFormat, creditcard.Last4));
                                            }
                                            else
                                            {
                                                body = body.Replace("{CardType}", string.Empty);
                                                body = body.Replace("{CardNumber}", string.Empty);
                                                body = body.Replace("{BilledTo}", string.Empty);
                                                body = body.Replace("{CreditCardLast4}", string.Empty);
                                            }
                                        }

                                    }
                                    if (trip.PaymentType.ToLower().Trim() == PaymentType.Cash.ToString().Trim().ToLower())
                                    {
                                    }
                                }
                                //}
                                #endregion

                                #region Making the Header
                                var header = (string)headerTemplate.Clone();
                                header = header.Replace("{ProviderTripID}", trip.taxi_trip.DispatchTripId);
                                #endregion

                                #region Sending Email
                                try
                                {
                                    var from = System.Configuration.ConfigurationManager.AppSettings["Notification_Email_From"];
                                    var host = System.Configuration.ConfigurationManager.AppSettings["Notification_Email_SMTP_Host"];
                                    var port = System.Configuration.ConfigurationManager.AppSettings["Notification_Email_SMTP_Port"];

                                    MessageController messageController = new MessageController();
                                    SMTPEmailMessage smtpMessage = new SMTPEmailMessage();
                                    smtpMessage.From = from;// "NoReply@zTrip.com";

                                    #region To Email
                                    
                                    string email =null;
                                    if (!string.IsNullOrWhiteSpace(input.EmailAddress))
                                    {
                                        email=input.EmailAddress;
                                    }

                                    else if(!string.IsNullOrWhiteSpace(memberID) && memberID!="0")
                                    {
                                        email = profile.EmailAddress;
                                    }
                                    else if (!string.IsNullOrEmpty(emailAddress))
                                    {
                                        email = emailAddress;
                                    }
                                    if (string.IsNullOrEmpty(emailAddress))
                                    {
                                        throw VtodException.CreateException(ExceptionType.Membership, 7002);
                                    }
                                    else
                                    {
                                        if (email.Contains("<"))
                                        {
                                            var l = email.Split('<');
                                            smtpMessage.To = new List<string> { l.First().Trim() };
                                        }
                                        else
                                        {
                                            smtpMessage.To = new List<string> { email.Trim() };
                                        }
                                    }
                                    #endregion
                                    smtpMessage.Subject = header;
                                    smtpMessage.MessageBody = body;
                                    smtpMessage.SMTP_Host = host;// "192.168.20.110";
                                    smtpMessage.SMTP_Port = port.ToInt32();// 25;
                                    smtpMessage.IsBodyHTML = true;

                                    var errorMessage = string.Empty;
                                    if (messageController.SendSMTPEmailMessage(smtpMessage, out errorMessage))
                                    {
                                        logger.InfoFormat("notifyBookedStatus for MemberID: {0} sent successfully", memberID);
                                    }
                                    else
                                    {
                                        logger.ErrorFormat("Error for notifyBookedStatus for MemberID: {0} -- {1}", memberID, errorMessage);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    var t = ex.GetType();
                                    logger.Error(ex);
                                    logger.ErrorFormat("Error for MemberRequestEmailCancellation for MemberID: {0} and TripID {1}", memberID, tripID);
                                    throw VtodException.CreateException(ExceptionType.Membership, 7005);

                                }
                                #endregion
                            }
                            //This is for Aleph as of now. In future if we need to add another ecar to zTrip, we need to modify this part.
                            else if (trip.FleetType == FleetType.ExecuCar.ToString())
                            {
                                string alephTripID = tripID.ToString();
                                #region Call Aleph Domain
                                if (input != null && input.Corporate != null && input.Corporate.CorporateAccount != null && input.Corporate.CorporateAccount.Corporation != null && input.Corporate.CorporateAccount.Corporation.ToLower().ToString() == "Aleph".ToLower().ToString())
                                {

                                    var alephAPICall = new AlephAPICall();
                                    string alephKey = null;
                                    var membershipController = new UDI.SDS.MembershipController(token, TrackTime);
                                    #region Call Aleph API
                                    logger.InfoFormat("Start Aleph API Call");
                                    try
                                    {
                                        var corporateAccount = membershipController.GetCorporateAccountKey(input.Corporate.CorporateAccount.Corporation, input.EmailAddress);
                                        if (corporateAccount != null && !string.IsNullOrWhiteSpace(corporateAccount.UserKey))
                                        {
                                            alephKey = corporateAccount.UserKey;
                                        }
                                    }
                                    catch (Exception ex1)
                                    {
                                        throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(ex1.Message));
                                    }
                                    logger.InfoFormat("Start Aleph API Call");
                                    var alephResult = alephAPICall.MemberRequestEmailConfirmation(input.CorporateCompanyRequirements[0].CorporateCompanyName, input.CorporateCompanyRequirements[0].CorporateCompanyAccounts[0].AccountName, alephTripID.ToString(), input.EmailAddress, alephKey);
                                    logger.InfoFormat("End Aleph API Call");
                                    #endregion
                                }
                                #endregion

                                #region ECar Texas
                                else if (trip!=null && trip.FleetTripCode==FleetTripCode.Ecar_Texas)
                                {
                                    if (string.IsNullOrEmpty(input.MemberID))
                                    {
                                        if (input.MemberID != "0")
                                            memberID = input.MemberID;
                                    }
                                    #region "Logic for No MemberID"
                                    if (string.IsNullOrEmpty(memberID) || memberID == "0")
                                    {
                                        if (input.UniqueID.Type.ToLower().Trim() == Common.DTO.Enum.ConfirmationType.DispatchConfirmation.ToString().ToLower().Trim())
                                        {
                                            string firstname = null;
                                            string lastName = null;
                                            string phoneNumber = null;
                                            string postalCode = null;
                                            if (input.Verification.PersonName != null)
                                            {
                                                firstname = input.Verification.PersonName.GivenName;
                                                lastName = input.Verification.PersonName.Surname;
                                            }
                                            if (input.Verification.TelephoneInfo != null)
                                            {
                                                phoneNumber = string.Format("{0}{1}", input.Verification.TelephoneInfo.AreaCityCode, input.Verification.TelephoneInfo.PhoneNumber).CleanPhone();
                                            }
                                            if (input.Verification.AddressInfo != null)
                                            {
                                                postalCode = input.Verification.AddressInfo.PostalCode;
                                            }
                                            if (input.UniqueID.ID == trip.van_trip.DispatchTripId && firstname.Trim() == trip.van_trip.FirstName.Trim() && lastName.Trim() == trip.van_trip.LastName.Trim())
                                            {
                                                emailAddress = trip.EmailAddress;
                                            }
                                            else if (input.UniqueID.ID == trip.van_trip.DispatchTripId && phoneNumber.Trim() == trip.van_trip.PhoneNumber.Trim())
                                            {
                                                emailAddress = trip.EmailAddress;
                                            }
                                            else if (input.UniqueID.ID == trip.van_trip.DispatchTripId && postalCode.Trim() == trip.van_trip.PickupZipCode.Trim())
                                            {
                                                emailAddress = trip.EmailAddress;
                                            }
                                            if (string.IsNullOrEmpty(emailAddress))
                                            {
                                                throw VtodException.CreateException(ExceptionType.Membership, 7002);
                                            }
                                        }
                                    }
                                    #endregion

                                    var fleet = db.ecar_fleet.Where(s => s.Id == trip.ecar_trip.FleetId).FirstOrDefault();

                                    string fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "ECarEmailCanceled.txt";
                                    var templates = fileName.ReadHtmlFromUrl().Split(new string[] { "##" }, StringSplitOptions.None);
                                    var headerTemplate = templates[0];
                                    var bodyTemplate = templates[1];


                                    #region Getting Member Profile
                                    UDI.SDS.MembershipService.CommunicationSettings profile = null;
                                    var membershipController = new UDI.SDS.MembershipController(token, TrackTime);
                                    if (!string.IsNullOrWhiteSpace(memberID) && memberID != "0")
                                        profile = membershipController.GetProfileCommunicationSettings(memberID.ToInt32());
                                    #endregion

                                    #region Making the Body
                                    var body = (string)bodyTemplate.Clone();
                                    body = body.Replace("{FirstName}", trip.ecar_trip.FirstName);
                                    body = body.Replace("{LastName}", trip.ecar_trip.LastName);
                                    body = body.Replace("{ProviderTripID}", trip.ecar_trip.DispatchTripId);
                                    body = body.Replace("{PUDateTime}", string.Format("{0:MM-dd-yyyy hh:mm tt}", trip.PickupDateTime));
                                    body = body.Replace("{PUAddress}", trip.ecar_trip.PickupFullAddress);
                                    if (profile != null)
                                    {
                                        body = body.Replace("{MemberPhone}", profile.ContactNumber.MakeStandardPhoneForamt());
                                    }
                                    else
                                    {
                                        if (input.Verification != null && input.Verification.TelephoneInfo != null && input.Verification.TelephoneInfo.CountryAccessCode != null && input.Verification.TelephoneInfo.AreaCityCode != null && input.Verification.TelephoneInfo.PhoneNumber != null)
                                        {
                                            string phoneNumber = input.Verification.TelephoneInfo.CountryAccessCode + input.Verification.TelephoneInfo.AreaCityCode + input.Verification.TelephoneInfo.PhoneNumber;
                                            body = body.Replace("{MemberPhone}", phoneNumber.MakeStandardPhoneForamt());
                                        }
                                    }
                                    if (!string.IsNullOrWhiteSpace(trip.ecar_trip.DropOffFullAddress))
                                    {
                                        body = body.Replace("{DOAddress}", trip.ecar_trip.DropOffFullAddress);
                                    }
                                    else
                                    {
                                        body = body.Replace("{DOAddress}", "As Directed");
                                    }
                                    body = body.Replace("{FleetName}", fleet.Alias);
                                    body = body.Replace("{FleetPhone}", fleet.PhoneNumber.MakeStandardPhoneForamt());
                                    if (trip.TotalFareAmount.HasValue && trip.TotalFareAmount.Value > 0)
                                    {
                                        if (transaction != null && transaction.CreditsApplied.HasValue && transaction.CreditsApplied.Value != 0)
                                        {
                                            if (trip.Gratuity.HasValue && trip.Gratuity.Value > 0)
                                            {
                                                body = body.Replace("{Fare}", (trip.TotalFareAmount - trip.Gratuity).ToString());
                                                body = body.Replace("{Tip}", trip.Gratuity.Value.ToString());
                                                body = body.Replace("{zTripCredit}", string.Format("({0})", System.Math.Abs(transaction.CreditsApplied.Value)));
                                                body = body.Replace("{TotalFare}", (trip.TotalFareAmount - System.Math.Abs(transaction.CreditsApplied.Value)).ToString());
                                            }
                                            else
                                            {
                                                body = body.Replace("{Fare}", trip.TotalFareAmount.ToString());
                                                body = body.Replace("{Tip}", string.Empty);
                                                body = body.Replace("{zTripCredit}", string.Format("({0})", System.Math.Abs(transaction.CreditsApplied.Value)));
                                                body = body.Replace("{TotalFare}", (trip.TotalFareAmount - System.Math.Abs(transaction.CreditsApplied.Value)).ToString());
                                            }
                                        }
                                        else
                                        {
                                            if (trip.Gratuity.HasValue && trip.Gratuity.Value > 0)
                                            {
                                                body = body.Replace("{Fare}", (trip.TotalFareAmount - trip.Gratuity).ToString());
                                                body = body.Replace("{Tip}", trip.Gratuity.Value.ToString());
                                                body = body.Replace("{TotalFare}", (trip.TotalFareAmount).ToString());
                                            }
                                            else
                                            {
                                                body = body.Replace("{Fare}", trip.TotalFareAmount.ToString());
                                                body = body.Replace("{Tip}", string.Empty);
                                                body = body.Replace("{TotalFare}", trip.TotalFareAmount.ToString());
                                            }
                                        }

                                        if (trip.PaymentType.ToLower().Trim() == PaymentType.PaymentCard.ToString().Trim().ToLower())
                                        {

                                            var accountController = new UDI.SDS.AccoutingController(token, TrackTime);
                                            var creditCardList = accountController.GetCreditCardAccounts(memberID.ToInt32());
                                            if (creditCardList != null && creditCardList.Any())
                                            {
                                                var creditcard = creditCardList.Where(s => s.AccountID == trip.CreditCardID.ToInt32()).FirstOrDefault();
                                                if (creditcard != null)
                                                {
                                                    body = body.Replace("{CardType}", creditcard.CardType.ToString());
                                                    body = body.Replace("{BilledTo}", creditcard.BillingName.ToString());
                                                    body = body.Replace("{CreditCardLast4}", creditcard.Last4.ToString());

                                                    string cardFormat;

                                                    switch (creditcard.CardType)
                                                    {
                                                        case UDI.SDS.MembershipService.CreditCardTypesEnumeration.AMEX:
                                                            cardFormat = "XXXX-XXXXXX-X{0}";
                                                            break;
                                                        case UDI.SDS.MembershipService.CreditCardTypesEnumeration.DINERS:
                                                            cardFormat = "XXXX-XXXXXX-{0}";
                                                            break;
                                                        case UDI.SDS.MembershipService.CreditCardTypesEnumeration.JCB:
                                                            cardFormat = "XXXX-XXXXXXXX{0}";
                                                            break;
                                                        default:
                                                            cardFormat = "XXXX-XXXX-XXXX-{0}";
                                                            break;
                                                    }
                                                    body = body.Replace("{CardNumber}", string.Format(cardFormat, creditcard.Last4));
                                                }
                                                else
                                                {
                                                    body = body.Replace("{CardType}", string.Empty);
                                                    body = body.Replace("{CardNumber}", string.Empty);
                                                    body = body.Replace("{BilledTo}", string.Empty);
                                                    body = body.Replace("{CreditCardLast4}", string.Empty);
                                                }
                                            }

                                        }
                                        if (trip.PaymentType.ToLower().Trim() == PaymentType.Cash.ToString().Trim().ToLower())
                                        {
                                        }
                                    }
                                    //}
                                    #endregion

                                    #region Making the Header
                                    var header = (string)headerTemplate.Clone();
                                    header = header.Replace("{ProviderTripID}", trip.ecar_trip.DispatchTripId);
                                    #endregion

                                    #region Sending Email
                                    try
                                    {
                                        var from = System.Configuration.ConfigurationManager.AppSettings["Notification_Email_From"];
                                        var host = System.Configuration.ConfigurationManager.AppSettings["Notification_Email_SMTP_Host"];
                                        var port = System.Configuration.ConfigurationManager.AppSettings["Notification_Email_SMTP_Port"];

                                        MessageController messageController = new MessageController();
                                        SMTPEmailMessage smtpMessage = new SMTPEmailMessage();
                                        smtpMessage.From = from;// "NoReply@zTrip.com";

                                        #region To Email

                                        string email = null;
                                        if (!string.IsNullOrWhiteSpace(input.EmailAddress))
                                        {
                                            email = input.EmailAddress;
                                        }

                                        else if (!string.IsNullOrWhiteSpace(memberID) && memberID != "0")
                                        {
                                            email = profile.EmailAddress;
                                        }
                                        else if (!string.IsNullOrEmpty(emailAddress))
                                        {
                                            email = emailAddress;
                                        }
                                        if (string.IsNullOrEmpty(email))
                                        {
                                            throw VtodException.CreateException(ExceptionType.Membership, 7002);
                                        }
                                        else
                                        {
                                            if (email.Contains("<"))
                                            {
                                                var l = email.Split('<');
                                                smtpMessage.To = new List<string> { l.First().Trim() };
                                            }
                                            else
                                            {
                                                smtpMessage.To = new List<string> { email.Trim() };
                                            }
                                        }
                                        #endregion
                                        smtpMessage.Subject = header;
                                        smtpMessage.MessageBody = body;
                                        smtpMessage.SMTP_Host = host;// "192.168.20.110";
                                        smtpMessage.SMTP_Port = port.ToInt32();// 25;
                                        smtpMessage.IsBodyHTML = true;

                                        var errorMessage = string.Empty;
                                        if (messageController.SendSMTPEmailMessage(smtpMessage, out errorMessage))
                                        {
                                            logger.InfoFormat("notifyBookedStatus for MemberID: {0} sent successfully", memberID);
                                        }
                                        else
                                        {
                                            logger.ErrorFormat("Error for notifyBookedStatus for MemberID: {0} -- {1}", memberID, errorMessage);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        var t = ex.GetType();
                                        logger.Error(ex);
                                        logger.ErrorFormat("Error for MemberRequestEmailCancellation for MemberID: {0} and TripID {1}", memberID, tripID);
                                        throw VtodException.CreateException(ExceptionType.Membership, 7005);

                                    }
                                    #endregion
                                }

                                #endregion
                            }
                            if (trip.FleetType == FleetType.Van.ToString())
                            {

                                if (string.IsNullOrEmpty(input.MemberID))
                                {
                                    if (input.MemberID != "0")
                                        memberID = input.MemberID;
                                }
                                #region "Logic for No MemberID"
                                if (string.IsNullOrEmpty(memberID) || memberID == "0")
                                {
                                    if (input.UniqueID.Type.ToLower().Trim() == Common.DTO.Enum.ConfirmationType.DispatchConfirmation.ToString().ToLower().Trim())
                                    {
                                        string firstname = null;
                                        string lastName = null;
                                        string phoneNumber = null;
                                        string postalCode = null;
                                        if (input.Verification.PersonName != null)
                                        {
                                            firstname = input.Verification.PersonName.GivenName;
                                            lastName = input.Verification.PersonName.Surname;
                                        }
                                        if (input.Verification.TelephoneInfo != null)
                                        {
                                            phoneNumber = string.Format("{0}{1}", input.Verification.TelephoneInfo.AreaCityCode, input.Verification.TelephoneInfo.PhoneNumber).CleanPhone();
                                        }
                                        if (input.Verification.AddressInfo != null)
                                        {
                                            postalCode = input.Verification.AddressInfo.PostalCode;
                                        }
                                        if (input.UniqueID.ID == trip.van_trip.DispatchTripId && firstname.Trim() == trip.van_trip.FirstName.Trim() && lastName.Trim() == trip.van_trip.LastName.Trim())
                                        {
                                            emailAddress = trip.EmailAddress;
                                        }
                                        else if (input.UniqueID.ID == trip.van_trip.DispatchTripId && phoneNumber.Trim() == trip.van_trip.PhoneNumber.Trim())
                                        {
                                            emailAddress = trip.EmailAddress;
                                        }
                                        else if (input.UniqueID.ID == trip.van_trip.DispatchTripId && postalCode.Trim() == trip.van_trip.PickupZipCode.Trim())
                                        {
                                            emailAddress = trip.EmailAddress;
                                        }
                                        if (string.IsNullOrEmpty(emailAddress))
                                        {
                                            throw VtodException.CreateException(ExceptionType.Membership, 7002);
                                        }
                                    }
                                }
                                #endregion

                                var fleet = db.van_fleet.Where(s => s.Id == trip.van_trip.FleetId).FirstOrDefault();

                                string fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "VanEmailCanceled.txt";
                                var templates = fileName.ReadHtmlFromUrl().Split(new string[] { "##" }, StringSplitOptions.None);
                                var headerTemplate = templates[0];
                                var bodyTemplate = templates[1];


                                #region Getting Member Profile
                                UDI.SDS.MembershipService.CommunicationSettings profile = null;
                                var membershipController = new UDI.SDS.MembershipController(token, TrackTime);
                                if (!string.IsNullOrWhiteSpace(memberID) && memberID != "0")
                                    profile = membershipController.GetProfileCommunicationSettings(memberID.ToInt32());
                                #endregion

                                #region Making the Body
                                var body = (string)bodyTemplate.Clone();
                                body = body.Replace("{FirstName}", trip.van_trip.FirstName);
                                body = body.Replace("{LastName}", trip.van_trip.LastName);
                                body = body.Replace("{ProviderTripID}", trip.van_trip.DispatchTripId);
                                body = body.Replace("{PUDateTime}", string.Format("{0:MM-dd-yyyy hh:mm tt}", trip.PickupDateTime));
                                body = body.Replace("{PUAddress}", trip.van_trip.PickupFullAddress);
                                if (profile != null)
                                {
                                    body = body.Replace("{MemberPhone}", profile.ContactNumber.MakeStandardPhoneForamt());
                                }
                                else
                                {
                                    if (input.Verification != null && input.Verification.TelephoneInfo != null && input.Verification.TelephoneInfo.CountryAccessCode != null && input.Verification.TelephoneInfo.AreaCityCode != null && input.Verification.TelephoneInfo.PhoneNumber != null)
                                    {
                                        string phoneNumber = input.Verification.TelephoneInfo.CountryAccessCode + input.Verification.TelephoneInfo.AreaCityCode + input.Verification.TelephoneInfo.PhoneNumber;
                                        body = body.Replace("{MemberPhone}", phoneNumber.MakeStandardPhoneForamt());
                                    }
                                }
                                if (!string.IsNullOrWhiteSpace(trip.van_trip.DropOffFullAddress))
                                {
                                    body = body.Replace("{DOAddress}", trip.van_trip.DropOffFullAddress);
                                }
                                else
                                {
                                    body = body.Replace("{DOAddress}", "As Directed");
                                }
                                body = body.Replace("{FleetName}", fleet.Alias);
                                body = body.Replace("{FleetPhone}", fleet.PhoneNumber.MakeStandardPhoneForamt());
                                if (trip.TotalFareAmount.HasValue && trip.TotalFareAmount.Value > 0)
                                {
                                    if (transaction != null && transaction.CreditsApplied.HasValue && transaction.CreditsApplied.Value != 0)
                                    {
                                        if (trip.Gratuity.HasValue && trip.Gratuity.Value > 0)
                                        {
                                            body = body.Replace("{Fare}", (trip.TotalFareAmount - trip.Gratuity).ToString());
                                            body = body.Replace("{Tip}", trip.Gratuity.Value.ToString());
                                            body = body.Replace("{zTripCredit}", string.Format("({0})", System.Math.Abs(transaction.CreditsApplied.Value)));
                                            body = body.Replace("{TotalFare}", (trip.TotalFareAmount - System.Math.Abs(transaction.CreditsApplied.Value)).ToString());
                                        }
                                        else
                                        {
                                            body = body.Replace("{Fare}", trip.TotalFareAmount.ToString());
                                            body = body.Replace("{Tip}", string.Empty);
                                            body = body.Replace("{zTripCredit}", string.Format("({0})", System.Math.Abs(transaction.CreditsApplied.Value)));
                                            body = body.Replace("{TotalFare}", (trip.TotalFareAmount - System.Math.Abs(transaction.CreditsApplied.Value)).ToString());
                                        }
                                    }
                                    else
                                    {
                                        if (trip.Gratuity.HasValue && trip.Gratuity.Value > 0)
                                        {
                                            body = body.Replace("{Fare}", (trip.TotalFareAmount - trip.Gratuity).ToString());
                                            body = body.Replace("{Tip}", trip.Gratuity.Value.ToString());
                                            body = body.Replace("{TotalFare}", (trip.TotalFareAmount).ToString());
                                        }
                                        else
                                        {
                                            body = body.Replace("{Fare}", trip.TotalFareAmount.ToString());
                                            body = body.Replace("{Tip}", string.Empty);
                                            body = body.Replace("{TotalFare}", trip.TotalFareAmount.ToString());
                                        }
                                    }

                                    if (trip.PaymentType.ToLower().Trim() == PaymentType.PaymentCard.ToString().Trim().ToLower())
                                    {

                                        var accountController = new UDI.SDS.AccoutingController(token, TrackTime);
                                        var creditCardList = accountController.GetCreditCardAccounts(memberID.ToInt32());
                                        if (creditCardList != null && creditCardList.Any())
                                        {
                                            var creditcard = creditCardList.Where(s => s.AccountID == trip.CreditCardID.ToInt32()).FirstOrDefault();
                                            if (creditcard != null)
                                            {
                                                body = body.Replace("{CardType}", creditcard.CardType.ToString());
                                                body = body.Replace("{BilledTo}", creditcard.BillingName.ToString());
                                                body = body.Replace("{CreditCardLast4}", creditcard.Last4.ToString());

                                                string cardFormat;

                                                switch (creditcard.CardType)
                                                {
                                                    case UDI.SDS.MembershipService.CreditCardTypesEnumeration.AMEX:
                                                        cardFormat = "XXXX-XXXXXX-X{0}";
                                                        break;
                                                    case UDI.SDS.MembershipService.CreditCardTypesEnumeration.DINERS:
                                                        cardFormat = "XXXX-XXXXXX-{0}";
                                                        break;
                                                    case UDI.SDS.MembershipService.CreditCardTypesEnumeration.JCB:
                                                        cardFormat = "XXXX-XXXXXXXX{0}";
                                                        break;
                                                    default:
                                                        cardFormat = "XXXX-XXXX-XXXX-{0}";
                                                        break;
                                                }
                                                body = body.Replace("{CardNumber}", string.Format(cardFormat, creditcard.Last4));
                                            }
                                            else
                                            {
                                                body = body.Replace("{CardType}", string.Empty);
                                                body = body.Replace("{CardNumber}", string.Empty);
                                                body = body.Replace("{BilledTo}", string.Empty);
                                                body = body.Replace("{CreditCardLast4}", string.Empty);
                                            }
                                        }

                                    }
                                    if (trip.PaymentType.ToLower().Trim() == PaymentType.Cash.ToString().Trim().ToLower())
                                    {
                                    }
                                }
                                //}
                                #endregion

                                #region Making the Header
                                var header = (string)headerTemplate.Clone();
                                header = header.Replace("{ProviderTripID}", trip.van_trip.DispatchTripId);
                                #endregion

                                #region Sending Email
                                try
                                {
                                    var from = System.Configuration.ConfigurationManager.AppSettings["Notification_Email_From"];
                                    var host = System.Configuration.ConfigurationManager.AppSettings["Notification_Email_SMTP_Host"];
                                    var port = System.Configuration.ConfigurationManager.AppSettings["Notification_Email_SMTP_Port"];

                                    MessageController messageController = new MessageController();
                                    SMTPEmailMessage smtpMessage = new SMTPEmailMessage();
                                    smtpMessage.From = from;// "NoReply@zTrip.com";

                                    #region To Email

                                    string email = null;
                                    if (!string.IsNullOrWhiteSpace(input.EmailAddress))
                                    {
                                        email = input.EmailAddress;
                                    }

                                    else if (!string.IsNullOrWhiteSpace(memberID) && memberID != "0")
                                    {
                                        email = profile.EmailAddress;
                                    }
                                    else if (!string.IsNullOrEmpty(emailAddress))
                                    {
                                        email = emailAddress;
                                    }
                                    if (string.IsNullOrEmpty(emailAddress))
                                    {
                                        throw VtodException.CreateException(ExceptionType.Membership, 7002);
                                    }
                                    else
                                    {
                                        if (email.Contains("<"))
                                        {
                                            var l = email.Split('<');
                                            smtpMessage.To = new List<string> { l.First().Trim() };
                                        }
                                        else
                                        {
                                            smtpMessage.To = new List<string> { email.Trim() };
                                        }
                                    }
                                    #endregion
                                    smtpMessage.Subject = header;
                                    smtpMessage.MessageBody = body;
                                    smtpMessage.SMTP_Host = host;// "192.168.20.110";
                                    smtpMessage.SMTP_Port = port.ToInt32();// 25;
                                    smtpMessage.IsBodyHTML = true;

                                    var errorMessage = string.Empty;
                                    if (messageController.SendSMTPEmailMessage(smtpMessage, out errorMessage))
                                    {
                                        logger.InfoFormat("notifyBookedStatus for MemberID: {0} sent successfully", memberID);
                                    }
                                    else
                                    {
                                        logger.ErrorFormat("Error for notifyBookedStatus for MemberID: {0} -- {1}", memberID, errorMessage);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    var t = ex.GetType();
                                    logger.Error(ex);
                                    logger.ErrorFormat("Error for MemberRequestEmailCancellation for MemberID: {0} and TripID {1}", memberID, tripID);
                                    throw VtodException.CreateException(ExceptionType.Membership, 7005);

                                }
                                #endregion
                            }

                        }

                        else if (
                            trip.FleetType == FleetType.SuperShuttle.ToString() || trip.FleetType == FleetType.ExecuCar.ToString() || trip.FleetType == FleetType.SuperShuttle_ExecuCar.ToString() || trip.FleetType == FleetType.SuperShuttleSharedRideOnly.ToString()
                            ||
                            trip.FleetType == FleetType.Taxi.ToString() && trip.taxi_trip != null && trip.taxi_trip.ServiceAPIId == 3 /* 3 = SDS, it's hardcoded as of now. better not to be hardcoded */
                           )
                        {

                            if ((string.IsNullOrEmpty(input.MemberID) || memberID == "0") && trip.FleetType == FleetType.Taxi.ToString())
                            {
                                throw VtodException.CreateException(ExceptionType.Membership, 7022);
                            }
                            if (input.Verification != null)
                            {
                                string confNumber = null;
                                string lastName = null;
                                string phoneNumber = null;
                                string postalCode = null;
                                if (input.UniqueID.Type.ToLower().Trim() == "dispatchconfirmation")
                                {
                                    confNumber = input.UniqueID.ID;
                                }
                                if (!string.IsNullOrWhiteSpace(confNumber))
                                {
                                    if (input.Verification.PersonName != null)
                                    {
                                        lastName = input.Verification.PersonName.Surname;
                                    }
                                    if (input.Verification.TelephoneInfo != null)
                                    {
                                        phoneNumber = string.Format("{0}{1}", input.Verification.TelephoneInfo.AreaCityCode, input.Verification.TelephoneInfo.PhoneNumber).CleanPhone();
                                    }
                                    if (input.Verification.AddressInfo != null)
                                    {
                                        postalCode = input.Verification.AddressInfo.PostalCode;
                                    }
                                    #region With no Member ID
                                    //if (string.IsNullOrEmpty(memberID) || memberID == "0")
                                    //{
                                    //    if (string.IsNullOrEmpty(input.EmailAddress))
                                    //    {
                                    //        if (input.UniqueID.ID == trip.taxi_trip.DispatchTripId && input.Verification.PersonName.GivenName.Trim() == trip.taxi_trip.FirstName.Trim() && lastName.Trim() == trip.taxi_trip.LastName.Trim())
                                    //        {
                                    //            emailAddress = trip.EmailAddress;
                                    //        }
                                    //        else if (input.UniqueID.ID == trip.taxi_trip.DispatchTripId && phoneNumber.Trim() == trip.taxi_trip.PhoneNumber.Trim())
                                    //        {
                                    //            emailAddress = trip.EmailAddress;
                                    //        }
                                    //        else if (input.UniqueID.ID == trip.taxi_trip.DispatchTripId && postalCode.Trim() == trip.taxi_trip.PickupZipCode.Trim())
                                    //        {
                                    //            emailAddress = trip.EmailAddress;
                                    //        }
                                    //    }
                                    //    else
                                    //    {
                                    //        emailAddress = input.EmailAddress;
                                    //    }
                                    //}
                                    //else
                                    //{
                                    //    emailAddress = input.EmailAddress;
                                    //}
                                    //if (string.IsNullOrEmpty(emailAddress))
                                    //{
                                    //    throw VtodException.CreateException(ExceptionType.Membership, 7002);
                                    //} 
                                    #endregion
                                    emailAddress = input.EmailAddress;
                                    reservationController.RequestEmailConfirmation(
                                       emailAddress, confNumber, lastName, phoneNumber, postalCode
                                        );
                                }
                                else
                                {
                                    throw VtodException.CreateException(ExceptionType.Membership, 7002);
                                }
                            }
                            else
                            {
                                reservationController.RequestEmailConfirmation(
                                    input.EmailAddress,
                                    trip.sds_trip != null ? trip.sds_trip.ConfirmationNumber : trip.taxi_trip.DispatchTripId,
                                    trip.sds_trip != null ? trip.sds_trip.LastName : trip.taxi_trip.LastName,
                                    string.Empty, string.Empty
                                    );
                            }
                        }
                    }

                    else
                    {
                        if (input != null && input.Corporate != null && input.Corporate.CorporateAccount != null && input.Corporate.CorporateAccount.Corporation != null && input.Corporate.CorporateAccount.Corporation.ToLower().ToString() == "Aleph".ToLower().ToString())
                        {
                            #region Call Aleph Domain
                            #region MyRegion
                            string alephTripID = string.Empty;
                            string trID;
                            if (input.UniqueID.Type.ToLower().Trim() == Common.DTO.Enum.ConfirmationType.Confirmation.ToString().ToLower().Trim())
                            {
                                var ecarTrip = db.ecar_trip.Where(s => s.Id.ToString() == input.UniqueID.ID).FirstOrDefault();
                                alephTripID = ecarTrip.DispatchTripId;
                            }
                            else
                            {
                                alephTripID = input.UniqueID.ID;
                            }
                            #region Parsing
                            if (alephTripID.ToString().Contains("##"))
                            {
                                if (alephTripID.ToString().IndexOf("##") != -1)
                                {
                                    trID = alephTripID.Substring(0, alephTripID.ToString().IndexOf("##"));
                                }
                                else
                                    trID = alephTripID;
                            }
                            else
                                trID = alephTripID;
                            #endregion
                            #endregion
                            var alephAPICall = new AlephAPICall();
                            string alephKey = null;
                            var membershipController = new UDI.SDS.MembershipController(token, TrackTime);
                            #region Call Aleph API
                            logger.InfoFormat("Start Aleph API Call");
                            try
                            {
                                var corporateAccount = membershipController.GetCorporateAccountKey(input.Corporate.CorporateAccount.Corporation, input.EmailAddress);
                                if (corporateAccount != null && !string.IsNullOrWhiteSpace(corporateAccount.UserKey))
                                {
                                    alephKey = corporateAccount.UserKey;
                                }
                            }
                            catch (Exception ex1)
                            {
                                throw VtodException.CreateException(ExceptionType.ECar, Convert.ToInt32(ex1.Message));
                            }
                            logger.InfoFormat("Start Aleph API Call");
                            var alephResult = alephAPICall.MemberRequestEmailConfirmation(input.CorporateCompanyRequirements[0].CorporateCompanyName, input.CorporateCompanyRequirements[0].CorporateCompanyAccounts[0].AccountName, trID.ToString(), input.EmailAddress, alephKey);
                            logger.InfoFormat("End Aleph API Call");
                            #endregion
                            #endregion

                        }
                        else
                            throw VtodException.CreateException(ExceptionType.Membership, 7002);
                    }
                }
                result.Success = new Success();

                #region Common
                result.EchoToken = input.EchoToken;
                result.PrimaryLangID = input.PrimaryLangID;
                result.Target = input.Target;
                result.Version = input.Version;
                #endregion

                return result;
            }
            catch (Exception ex)
            {
                logger.Error("VTODDomain.RequestEmailCancellation", ex);

                #region Throw MembershipException
                throw new MembershipException(ex.Message);
                #endregion
            }
        }

		internal MemberGetLatestUnRatedTripRS MemberGetLatestUnRatedTrip(TokenRS token, MemberGetLatestUnRatedTripRQ input)
		{
			try
			{
				MemberGetLatestUnRatedTripRS result = null;
				List<vtod_trip> trips = null;
				List<Int64?> tripIDs = null;
				vtod_trip latestUnratedTrip = null;
				var isRated = false;
				var status = string.Empty;
				//decimal? lat = null;
				//decimal? lon = null;
				var sdsDomain = new SDS.SDSDomain(TrackTime);				
				var taxiDomain = new Taxi.TaxiDomain(); taxiDomain.TrackTime = TrackTime;
				var accountingDomain = new Accounting.AccountingDomain(); accountingDomain.TrackTime = TrackTime;

				using (var db = new DataAccess.VTOD.VTODEntities())
				{
					tripIDs = db.SP_VTOD_GetMemberLatestTripIDsUpToNow(input.MemberId).ToList();
				}

				//if (trips != null && trips.Any())
				if (tripIDs != null && tripIDs.Any())
				{
					#region Get the latest UnCompletedTrip
					//foreach (var item in trips/*.Where(s => s.Id == 20106 || s.Id == 20158)*/)
					foreach (var tripID in tripIDs/*.Where(s => s.Id == 20106 || s.Id == 20158)*/)
					{
						vtod_trip trip = null;

						using (var db = new DataAccess.VTOD.VTODEntities())
						{
							trip = db.vtod_trip.Where(s => s.Id == tripID.Value).FirstOrDefault();
						}

						if (trip != null)
						{
							if (trip.FleetType.ToLower() == Common.DTO.Enum.FleetType.Taxi.ToString().ToLower())
							{
								status = trip.FinalStatus;
								//if (status.ToLower() == Taxi.Const.TaxiTripStatusType.Completed.ToLower())
								//{
								UDI.VTOD.Domain.Taxi.TaxiUtility u = new Taxi.TaxiUtility(logger);
								latestUnratedTrip = u.GetTaxiTrip(trip.Id);
								if (latestUnratedTrip.taxi_trip.ServiceAPIId != Taxi.Const.TaxiServiceAPIConst.SDS)
								{
									break;
								}
								else
								{
									var statusRQ = new OTA_GroundResRetrieveRQ();
									statusRQ.Reference = new List<Reference>();
									var _ref = new Reference { Type = Common.DTO.Enum.ConfirmationType.Confirmation.ToString(), ID = trip.Id.ToString() };
									statusRQ.Reference.Add(_ref);
									statusRQ.TPA_Extensions = new TPA_Extensions();
									statusRQ.TPA_Extensions.RateQualifiers = new List<RateQualifier>();
									var rateQualifier = new RateQualifier { RateQualifierValue = "Taxi" };
									statusRQ.TPA_Extensions.RateQualifiers.Add(rateQualifier);

									var statusRS = sdsDomain.GroundResRetrieve(token, statusRQ);
									var statusObj = statusRS.TPA_Extensions.Statuses.Status.FirstOrDefault();
									if (statusObj != null)
									{
										status = statusObj.Value;
										if (!string.IsNullOrWhiteSpace(status) && status.ToLower() == "completed")
										{
											break;
										}
										else
										{
											latestUnratedTrip = null;
										}
									}
								}
								//}
							}
							else if (
								trip.FleetType.ToLower() == Common.DTO.Enum.FleetType.ExecuCar.ToString().ToLower()
								||
								trip.FleetType.ToLower() == Common.DTO.Enum.FleetType.SuperShuttle.ToString().ToLower()
								||
								trip.FleetType.ToLower() == Common.DTO.Enum.FleetType.SuperShuttle_ExecuCar.ToString().ToLower()
								||
								trip.FleetType.ToLower() == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly.ToString().ToLower()
								)
							{
								sds_trip sdsTrip = null;
								ecar_trip ecarTrip = null;
								using (VTODEntities context = new VTODEntities())
								{
									sdsTrip = context.sds_trip.Where(x => x.Id == trip.Id).Select(x => x).FirstOrDefault();
									if (sdsTrip != null)
									{
										trip.sds_trip = sdsTrip;
										//latestUnratedTrip = trip;
									}
									else
									{
										ecarTrip = context.ecar_trip.Where(x => x.Id == trip.Id).Select(x => x).FirstOrDefault();
										if (ecarTrip != null)
										{
											trip.ecar_trip = ecarTrip;
											//latestUnratedTrip = trip;
										}

									}

								}

								var statusRQ = new OTA_GroundResRetrieveRQ();
								OTA_GroundResRetrieveRS statusRS = null;
								try
								{
									statusRQ.Reference = new List<Reference>();
									var _ref = new Reference { Type = Common.DTO.Enum.ConfirmationType.Confirmation.ToString(), ID = trip.Id.ToString() };
									statusRQ.Reference.Add(_ref);
									statusRQ.TPA_Extensions = new TPA_Extensions();
									statusRQ.TPA_Extensions.RateQualifiers = new List<RateQualifier>();
									if (sdsTrip != null)
									{
										var rateQualifier = new RateQualifier { RateQualifierValue = "SuperShuttle_ExecuCar" };
										statusRQ.TPA_Extensions.RateQualifiers.Add(rateQualifier);
										statusRS = sdsDomain.GroundResRetrieve(token, statusRQ);
									}
									else if (ecarTrip != null)
									{
										var rateQualifier = new RateQualifier { RateQualifierValue = "ExecuCar" };
										statusRQ.TPA_Extensions.RateQualifiers.Add(rateQualifier);
                                        var ecarDomain = new ECar.ECarDomain();
                                        statusRS = ecarDomain.Status(token, statusRQ);
									}
								}
								catch
								{ }


								if (statusRS != null)
								{
									var statusObj = statusRS.TPA_Extensions.Statuses.Status.FirstOrDefault();
									if (statusObj != null)
									{
										status = statusObj.Value;
										if (!string.IsNullOrWhiteSpace(status) && status.ToLower() == "completed")
										{
											//	using (VTODEntities context = new VTODEntities())
											//	{
											//		latestUnratedTrip = context.vtod_trip.Include("sds_trip").Where(x => x.Id == trip.Id).Select(x => x).FirstOrDefault();
											//	}

											latestUnratedTrip = trip;
											break;
										}
									}
								}

							}
						}
					}
					#endregion

					#region IsRated
					if (latestUnratedTrip != null)
					{
						using (var db = new DataAccess.VTOD.VTODEntities())
						{
							var rate = db.vtod_trip_rate.Where(s => s.TripID == latestUnratedTrip.Id).FirstOrDefault();
							if (rate != null)
							{
								isRated = true;
							}
						}
					}
					else
					{
						isRated = true;
					}
					#endregion

				}

				#region Make Response
				result = new MemberGetLatestUnRatedTripRS();
				if (!isRated && latestUnratedTrip != null)
				{

					result.HasRecords = true;
					result.MemberReservationsList = new List<MembershipReservation>();

					var record = new MembershipReservation();
					record.Confirmation = new List<Confirmation>();
					var confirmation = new Confirmation { Type = Common.DTO.Enum.ConfirmationType.Confirmation.ToString(), ID = latestUnratedTrip.Id.ToString() };
					record.Confirmation.Add(confirmation);
					record.StatusCode = status;
					record.PickupTime = latestUnratedTrip.PickupDateTime.ToString();
					record.PickMeUpNow = latestUnratedTrip.PickMeUpNow.HasValue ? latestUnratedTrip.PickMeUpNow.Value : false;

					#region Get Fare Details
					var fareDetailRQ = new GetFareDetailRQ();
					fareDetailRQ.Reference = new List<Reference>();
					var reference = new Reference { Type = Common.DTO.Enum.ConfirmationType.Confirmation.ToString(), ID = latestUnratedTrip.Id.ToString() };
					fareDetailRQ.Reference.Add(reference);
					fareDetailRQ.TPA_Extensions = new TPA_Extensions();
					fareDetailRQ.TPA_Extensions.RateQualifiers = new List<RateQualifier>();
					var rateQualifier = new RateQualifier { RateQualifierValue = latestUnratedTrip.FleetType };
					fareDetailRQ.TPA_Extensions.RateQualifiers.Add(rateQualifier);

					GetFareDetailRS fareDetails = null;

                    if (latestUnratedTrip.FleetType.ToLower() == Common.DTO.Enum.FleetType.Taxi.ToString().ToLower())
                    {
                        fareDetails = taxiDomain.GetFareDetail(token, fareDetailRQ);
                        using (var db = new DataAccess.VTOD.VTODEntities())
                        {
                            var taxi_trip_status = db.taxi_trip_status.Where(s => s.TripID == latestUnratedTrip.Id && s.VehicleLatitude != null && s.VehicleLongitude != null).OrderBy(p => p.Id).ToList();
                            if (taxi_trip_status != null)
                            {
                                if (taxi_trip_status.Any())
                                {
                                    record.DropoffLatitude = taxi_trip_status.FirstOrDefault().VehicleLatitude.ToString();
                                    record.DropoffLongitude = taxi_trip_status.FirstOrDefault().VehicleLongitude.ToString();
                                }
                            }
                        }
                    }
                    else if (
                        latestUnratedTrip.FleetType.ToLower() == Common.DTO.Enum.FleetType.ExecuCar.ToString().ToLower()
                        ||
                        latestUnratedTrip.FleetType.ToLower() == Common.DTO.Enum.FleetType.SuperShuttle.ToString().ToLower()
                        ||
                        latestUnratedTrip.FleetType.ToLower() == Common.DTO.Enum.FleetType.SuperShuttle_ExecuCar.ToString().ToLower()
                        ||
                        latestUnratedTrip.FleetType.ToLower() == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly.ToString().ToLower()
                        )
                    {
                        fareDetails = accountingDomain.GetFareDetail(token, fareDetailRQ);
                    }



					record.FareItems = fareDetails.FareItems;
					record.TripInfo = fareDetails.TripInfo;
					record.PaymentInfo = fareDetails.PaymentInfo;
					record.Contacts = fareDetails.Contacts;


					record.PickupLatitude = fareDetails.PickupLatitude;
					record.PickupLongitude = fareDetails.PickupLongitude;
					record.PickupLocation = fareDetails.PickupLocation;

					record.DropOffLocation = fareDetails.DropOffLocation;

					#endregion

					#region Get Trip Location, lat, long, ..


					#endregion
					result.MemberReservationsList.Add(record);
				}

				#endregion

				result.Success = new Success();

				#region Common
				result.EchoToken = input.EchoToken;
				result.PrimaryLangID = input.PrimaryLangID;
				result.Target = input.Target;
				result.Version = input.Version;
				#endregion


				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("MembershipDomain.MemberGetLatestUnRatedTrip:: {0}", ex.ToString() + ex.StackTrace);

				#region Throw MembershipException
				throw VtodException.CreateException(ExceptionType.Membership, 7012);//throw new MembershipException(Messages.Membership_MemberGetLatestUnRatedTrip);
				#endregion
			}
		}

        internal MemberGetLatestTripRS MemberGetLatestTrip(TokenRS token, MemberGetLatestTripRQ input)
        {
            try
            {
                MemberGetLatestTripRS result = null;
                List<vtod_trip> trips = null;
                List<Int64?> tripIDs = null;
                vtod_trip latestTrip = null;
                var status = string.Empty;
                var sdsDomain = new SDS.SDSDomain(TrackTime);                
                var taxiDomain = new Taxi.TaxiDomain(); taxiDomain.TrackTime = TrackTime;
                var accountingDomain = new Accounting.AccountingDomain(); accountingDomain.TrackTime = TrackTime;
                string rateQualifer = string.Empty;
                using (var db = new DataAccess.VTOD.VTODEntities())
                {
                    tripIDs = db.SP_VTOD_GetLatestTripIDForMember(input.MemberId).ToList();
                }

               
                if (tripIDs != null && tripIDs.Any())
                {
                    #region Get the latest UnCompletedTrip
                    foreach (var tripID in tripIDs)
                    {
                        vtod_trip trip = null;
                        using (var db = new DataAccess.VTOD.VTODEntities())
                        {
                            trip = db.vtod_trip.Where(s => s.Id == tripID.Value).FirstOrDefault();
                        }

                        if (trip != null)
                        {
                            rateQualifer = trip.FleetType;
                            if (trip.FleetType.ToLower() == Common.DTO.Enum.FleetType.Taxi.ToString().ToLower())
                            {
                                  status = trip.FinalStatus;
                                  UDI.VTOD.Domain.Taxi.TaxiUtility u = new Taxi.TaxiUtility(logger);
                                  var statusRQ = new OTA_GroundResRetrieveRQ();
                                  statusRQ.Reference = new List<Reference>();
                                   var _ref = new Reference { Type = Common.DTO.Enum.ConfirmationType.Confirmation.ToString(), ID = trip.Id.ToString() };
                                   statusRQ.Reference.Add(_ref);
                                   statusRQ.TPA_Extensions = new TPA_Extensions();
                                   statusRQ.TPA_Extensions.RateQualifiers = new List<RateQualifier>();
                                   var rateQualifier = new RateQualifier { RateQualifierValue = "Taxi" };
                                   statusRQ.TPA_Extensions.RateQualifiers.Add(rateQualifier);

                                   var statusRS = taxiDomain.Status(token, statusRQ);
                                    var statusObj = statusRS.TPA_Extensions.Statuses.Status.FirstOrDefault();
                                    if (statusObj != null)
                                    {
                                        status = statusObj.Value;
                                        if (!string.IsNullOrWhiteSpace(status) && (status.ToLower()=="boarded" || status.ToLower() == "pickup" || status.ToLower() == "assigned" || status.ToLower() == "accepted" || status.ToLower() == "arrived" || status.ToLower() == "inservice" || status.ToLower() == "intransit" || status.ToLower() == "accept" || status.ToLower() == "assign" || status.ToLower() == "driver on the way"))
                                        {
                                            latestTrip = trip;
                                            break;
                                        }
                                    }
                            }
                            else if (
                                trip.FleetType.ToLower() == Common.DTO.Enum.FleetType.ExecuCar.ToString().ToLower()
                                ||
                                trip.FleetType.ToLower() == Common.DTO.Enum.FleetType.SuperShuttle.ToString().ToLower()
                                ||
                                trip.FleetType.ToLower() == Common.DTO.Enum.FleetType.SuperShuttle_ExecuCar.ToString().ToLower()
                                ||
                                trip.FleetType.ToLower() == Common.DTO.Enum.FleetType.SuperShuttleSharedRideOnly.ToString().ToLower()
                                )
                            {
                                sds_trip sdsTrip = null;
                                ecar_trip ecarTrip = null;
                                using (VTODEntities context = new VTODEntities())
                                {
                                    sdsTrip = context.sds_trip.Where(x => x.Id == trip.Id).Select(x => x).FirstOrDefault();
                                    if (sdsTrip != null)
                                    {
                                        trip.sds_trip = sdsTrip;
                                    }
                                    else
                                    {
                                        ecarTrip = context.ecar_trip.Where(x => x.Id == trip.Id).Select(x => x).FirstOrDefault();
                                        if (ecarTrip != null)
                                        {
                                            trip.ecar_trip = ecarTrip;
                                        }

                                    }

                                }

                                var statusRQ = new OTA_GroundResRetrieveRQ();
                                OTA_GroundResRetrieveRS statusRS = null;
                                try
                                {
                                    statusRQ.Reference = new List<Reference>();
                                    var _ref = new Reference { Type = Common.DTO.Enum.ConfirmationType.Confirmation.ToString(), ID = trip.Id.ToString() };
                                    statusRQ.Reference.Add(_ref);
                                    statusRQ.TPA_Extensions = new TPA_Extensions();
                                    statusRQ.TPA_Extensions.RateQualifiers = new List<RateQualifier>();
                                    if (sdsTrip != null)
                                    {
                                        var rateQualifier = new RateQualifier { RateQualifierValue = "SuperShuttle_ExecuCar" };
                                        statusRQ.TPA_Extensions.RateQualifiers.Add(rateQualifier);
                                        statusRS = sdsDomain.GroundResRetrieve(token, statusRQ);
                                    }
                                    else if (ecarTrip != null)
                                    {
                                        ECar.ECarDomain ecarDomain = new ECar.ECarDomain();
                                        var rateQualifier = new RateQualifier { RateQualifierValue = "ExecuCar" };
                                        statusRQ.TPA_Extensions.RateQualifiers.Add(rateQualifier);
                                        statusRS = ecarDomain.Status(token, statusRQ);
                                    }
                                    
                                }
                                catch
                                { }


                                if (statusRS != null)
                                {
                                    var statusObj = statusRS.TPA_Extensions.Statuses.Status.FirstOrDefault();
                                    if (statusObj != null)
                                    {
                                        status = statusObj.Value;
                                        if (!string.IsNullOrWhiteSpace(status) && (status.ToLower() == "boarded" || status.ToLower() == "driver on the way" || status.ToLower() == "pickup" || status.ToLower() == "assigned" || status.ToLower() == "accepted" || status.ToLower() == "arrived" || status.ToLower() == "inservice" || status.ToLower() == "intransit" || status.ToLower() == "accept" || status.ToLower() == "assign"))
                                        {
                                            latestTrip = trip;
                                            break;
                                        }
                                    }
                                }

                            }
                        }
                    }
                    #endregion

                   
                }

                #region Make Response
                result = new MemberGetLatestTripRS();
                if ( latestTrip != null)
                {

                    result.HasRecords = true;
                    //result.MemberReservations = new MembershipReservation();

                    var record = new MembershipReservation();
                    record.Confirmation = new List<Confirmation>();
                    var confirmation = new Confirmation { Type = Common.DTO.Enum.ConfirmationType.Confirmation.ToString(), ID = latestTrip.Id.ToString() };
                    record.Confirmation.Add(confirmation);
                    record.StatusCode = status;
                    var rateQualifier = new RateQualifier { RateQualifierValue = rateQualifer };
                    record.RateQualifier = rateQualifier;
                    record.PickupTime = latestTrip.PickupDateTime.ToString();
                    record.PickMeUpNow = latestTrip.PickMeUpNow.HasValue ? latestTrip.PickMeUpNow.Value : false;
                    result.MemberReservations = record;
                }

                #endregion

                result.Success = new Success();

                #region Common
                result.EchoToken = input.EchoToken;
                result.PrimaryLangID = input.PrimaryLangID;
                result.Target = input.Target;
                result.Version = input.Version;
                #endregion


                return result;
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("MembershipDomain.MemberGetLatestTrip:: {0}", ex.ToString() + ex.StackTrace);

                #region Throw MembershipException
                throw VtodException.CreateException(ExceptionType.Membership, 7012);
                #endregion
            }
        }

		internal MemberDeleteRS MemberDelete(TokenRS token, MemberDeleteRQ input)
		{
			try
			{
				MemberDeleteRS result = null;

				var membershipController = new UDI.SDS.MembershipController(token, TrackTime);

				var sdsResult = membershipController.MemberDelete(input.EmailAddress, input.Password);
                

				if (sdsResult)
				{
					result = new MemberDeleteRS();
					result.Success = new Success();

					#region Common
					result.EchoToken = input.EchoToken;
					result.PrimaryLangID = input.PrimaryLangID;
					result.Target = input.Target;
					result.Version = input.Version;
					#endregion
				}
				else
				{
					throw new Exception("We cannot delete the member record");
				}

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("MembershipDomain.MemberDelete:: {0}", ex.ToString() + ex.StackTrace);

				#region Throw MembershipException
				throw VtodException.CreateException(ExceptionType.Membership, 7019);//throw new MembershipException(Messages.Membership_MemberGetLatestUnRatedTrip);
				#endregion
			}

		}

		//public bool IsUserInRole(string username, string role)
		//{
		//	//var result = Roles.IsUserInRole(username, role);
		//	var result = IsUserInRoles(username, new List<string> { role, "Administrator" });
		//	return result;
		//}

		//public bool IsUserInRole(string username, string fleetRole, string methodRole)
		//{
		//	var result = false;
		//	var roles = GetRolesForUser(username);
		//	if (roles.Contains("Administrator"))
		//	{
		//		result = true;
		//	}
		//	else
		//	{
		//		if (!string.IsNullOrWhiteSpace(fleetRole))
		//		{
		//			if (roles.Contains("Fleet_" + fleetRole) && roles.Contains("Method_" + methodRole))
		//				result = true;
		//			else
		//				result = false;
		//		}
		//		else
		//		{
		//			if (roles.Contains("Method_" + methodRole))
		//				result = true;
		//			else
		//				result = false;
		//		}
		//	}
		//	return result;
		//}

		//public bool IsUserInPaymentRole(string username, string paymentType)
		//{
		//	var result = false;
		//	var roles = GetRolesForUser(username);
		//	if (!string.IsNullOrWhiteSpace(paymentType))
		//	{
		//		if (roles.Contains("Payment_" + paymentType))
		//			result = true;
		//		else
		//			result = false;
		//	}

		//	return result;
		//}

		#region Corporate
		public List<CorporateName> GetCorporateNames(string corporateID)
		{
			var result = new List<CorporateName>();

			#region Aleph
			try
			{
				var alephAPICall = new AlephAPICall();
				var isAleph = alephAPICall.VerifyUser(corporateID).verified;
				if (isAleph)
				{
					result.Add(CorporateName.Aleph);
				}
			}
			catch (Exception ex)
			{
				logger.Error(ex);
			}
			#endregion

			return result;
		}

		public bool RegisterCorporateUser(CorporateName corporateName, string corporateID, string password)
		{
			var result = false;
			try
			{

				switch (corporateName)
				{
					case CorporateName.Aleph:
						var alephAPICall = new AlephAPICall();
						result = alephAPICall.RegisterUser(corporateID, password).registered;
						break;
				}
			}
			catch (Exception ex)
			{
				logger.Error(ex);
			}

			return result;
		}

		public bool RegisterCorporateDeviceName(TokenRS tokenVTOD, CorporateName corporateName, string corporateID, string password, string deviceToken, string platformName, string platformVersion, string phoneNumber,int memberID)
		{
			var result = false;
			try
			{

				switch (corporateName)
				{
					case CorporateName.Aleph:
						var alephAPICall = new AlephAPICall();
                        if (!string.IsNullOrEmpty(platformName))
                        {
                            if (platformName.ToUpper() == "IPHONE")
                                platformName = "ios";
                        }
						result = alephAPICall.RegisterDevice(deviceToken, platformName, platformVersion, phoneNumber, corporateID, password,memberID).registered;
						break;
				}
			}
			catch (Exception ex)
			{
				logger.Error(ex);
			}

			return result;
		}
		public bool DeleteCorporateDeviceName(TokenRS tokenVTOD, CorporateName corporateName, string corporateID, string password, string deviceToken,int memberID)
		{
			var result = false;
			try
			{

				switch (corporateName)
				{
					case CorporateName.Aleph:
						var alephAPICall = new AlephAPICall();
                        result = alephAPICall.DeleteDevice(corporateID, password, deviceToken, memberID).registered;
						break;
				}
			}
			catch (Exception ex)
			{
				logger.Error(ex);
			}

			return result;
		}
		public bool DeleteCorporateUser(CorporateName corporateName, string corporateID, string password)
		{
			var result = false;
			password = string.IsNullOrWhiteSpace(password) ? string.Empty : password;
			try
			{

				switch (corporateName)
				{
					case CorporateName.Aleph:
						var alephAPICall = new AlephAPICall();
						result = alephAPICall.DeleteUser(corporateID, password).deleted;
						break;
				}
			}
			catch (Exception ex)
			{
				logger.Error(ex);
			}

			return result;
		}

		public Corporate GetCorporate(CorporateName corporateName, string corporateID, string password)
		{
			var result = new Corporate();
			try
			{

				switch (corporateName)
				{
					case CorporateName.Aleph:
						var alephAPICall = new AlephAPICall();
						var alephResult = alephAPICall.GetUserProfiles(corporateID, password);
						if (alephResult != null && alephResult.profiles != null && alephResult.profiles.Any())
						{
							var corporate = new Corporate();
							corporate.CorporateAccount = new CorporateAccount { Corporation = corporateName.ToString(), UserID = corporateID };
							corporate.CorporateProfiles = new CorporateProfiles();
							corporate.CorporateProfiles.CorporateProfileList = new List<CorporateProfile>();

							foreach (var item in alephResult.profiles)
							{
								var profile = new CorporateProfile();
								profile.CompanyName = item.company_name;
								profile.DefaultPaymentMethod = item.default_payment_method;
								profile.PreferredCompany = item.preferred_company;
								profile.ProfileID = item.profile_id;
								profile.ProviderAccountId = item.provider_account_id;
								profile.ProviderID = item.provider_id;
								profile.Username = item.username;

								corporate.CorporateProfiles.CorporateProfileList.Add(profile);
							}
							result = corporate;
						}

						break;

				}
			}
			catch (Exception ex)
			{
				logger.Error(ex);
			}

			return result;
		}

        public List<AlephCorporate> GetCorporate2(CorporateName corporateName, string corporateID, string password, string latitude, string longitude)
        {
            var result = new List<AlephCorporate>();
            try
            {

                switch (corporateName)
                {
                    case CorporateName.Aleph:
                        var alephAPICall = new AlephAPICall();
                        var alephResult = alephAPICall.GetUserProfiles2(corporateID, password, latitude, longitude);
                        if (alephResult != null &&  alephResult.Any())
                        {
                            foreach (var corporateItem in alephResult)
                            {
                                AlephCorporate _corporateItem = new AlephCorporate();
                                _corporateItem.CorporateName = corporateItem.corporation_name;
                                if (corporateItem.corporation_accounts != null && corporateItem.corporation_accounts.Any())
                                {
                                    _corporateItem.AlephAccount=new List<AlephCorporateAccount>();
                                    foreach (var accountItem in corporateItem.corporation_accounts)
                                    {
                                        AlephCorporateAccount _accountItem = new AlephCorporateAccount();
                                        _accountItem.PreferredCompany = accountItem.preferred_company;
                                        _accountItem.Account = accountItem.account_name;
                                        _accountItem.AlephProfile = new List<CorporateProfile>();
                                        if (accountItem != null && accountItem.account_providers.Any())
                                        {
                                            foreach (var profileItem in accountItem.account_providers)
                                            {
                                                CorporateProfile _profileItem = new CorporateProfile();
                                                _profileItem.ProfileID = profileItem.profile_id;
                                                _profileItem.CompanyName = profileItem.company_name;
                                                _profileItem.ProviderID = profileItem.provider_id;
                                                _profileItem.ProviderAccountId = profileItem.provider_account_id;
                                                _profileItem.Username = profileItem.username;
                                                _profileItem.DefaultPaymentMethod = profileItem.default_payment_method;
                                                _accountItem.AlephProfile.Add(_profileItem);
                                            }
                                        }
                                        _corporateItem.AlephAccount.Add(_accountItem);
                                    }
                                }
                                result.Add(_corporateItem);
                            }
                            
                        }

                        break;

                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }

            return result;
        }

		public bool ChangePassword(CorporateName corporateName, string corporateID, string newPassword, string oldpassword)
		{
			var result = false;
			try
			{

				switch (corporateName)
				{
					case CorporateName.Aleph:
						var alephAPICall = new AlephAPICall();
						result = alephAPICall.ChangeUserPassword(corporateID, oldpassword, newPassword).changed;


						break;

				}
			}
			catch (Exception ex)
			{
				logger.Error(ex);
			}

			return result;
		}

		internal MemberGetCorporateProfileRS MemberGetCorporateProfile(TokenRS token, MemberGetCorporateProfileRQ input)
		{
			MemberGetCorporateProfileRS result = new MemberGetCorporateProfileRS();
			result.Corporates = new Corporates();
			result.Corporates.CorporateList = new List<Corporate>();
			result.Success = new Success();
			#region Common
			result.EchoToken = input.EchoToken;
			result.PrimaryLangID = input.PrimaryLangID;
			result.Target = input.Target;
			result.Version = input.Version;
			#endregion

			try
			{
				var membershipController = new UDI.SDS.MembershipController(token, TrackTime);

				var sdsResult = membershipController.GetCorporateAccountKey(CorporateName.Aleph.ToString(), input.CorporateID);

				#region Aleph
				if (sdsResult != null)
				{
					var corporate = GetCorporate(CorporateName.Aleph, input.CorporateID, sdsResult.UserKey);
					if (corporate != null)
					{
						result.Corporates.CorporateList.Add(corporate);
					}
				}
				#endregion

				#region MembershipRecord
				if (sdsResult.MembershipRecord != null)
				{
					result.MembershipRecord = new MembershipRecord();
					result.MembershipRecord.SubscribeToEmail = sdsResult.MembershipRecord.AllowSpam;
					result.MembershipRecord.Telephone = new Telephone();
					result.MembershipRecord.Telephone.PhoneNumber = sdsResult.MembershipRecord.ContactNumber;
					result.MembershipRecord.Telephone.CountryAccessCode = sdsResult.MembershipRecord.ContactNumberDialingPrefix;
					result.MembershipRecord.EmailAddress = sdsResult.MembershipRecord.EmailAddress;
					result.MembershipRecord.FirstName = sdsResult.MembershipRecord.FirstName;
					result.MembershipRecord.LastName = sdsResult.MembershipRecord.LastName;
					result.MembershipRecord.MemberID = sdsResult.MembershipRecord.MemberID;
					if (sdsResult.MembershipRecord.SubscribeToSms != null)
					{
						result.MembershipRecord.SubscribeToSms = (bool)sdsResult.MembershipRecord.SubscribeToSms;
					}
					result.MembershipRecord.SubscribeToSms = (bool)sdsResult.MembershipRecord.SubscribeToSms;
					result.MembershipRecord.DefaultGratuityPercentage = sdsResult.MembershipRecord.GratuityRate * 100;
					result.MembershipRecord.HonorificID = sdsResult.MembershipRecord.HonorificId;
					result.MembershipRecord.HomeAirport = sdsResult.MembershipRecord.HomeAirport;
					result.MembershipRecord.BirthDate = sdsResult.MembershipRecord.BirthDate.ToString();
				}
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("MembershipDomain.CommunicationSettings:: {0}", ex.ToString() + ex.StackTrace);

				#region Throw MembershipException
				throw new MembershipException(ex.Message);
				#endregion
			}
		}

        internal MemberGetCorporateProfile2RS MemberGetCorporateProfile2(TokenRS token, MemberGetCorporateProfile2RQ input)
        {
            MemberGetCorporateProfile2RS result = new MemberGetCorporateProfile2RS();            
            result.AlephCorporate = new List<AlephCorporate>();
            result.Success = new Success();
            #region Common
            result.EchoToken = input.EchoToken;
            result.PrimaryLangID = input.PrimaryLangID;
            result.Target = input.Target;
            result.Version = input.Version;
            #endregion

            try
            {
                var membershipController = new UDI.SDS.MembershipController(token, TrackTime);

                var sdsResult = membershipController.GetCorporateAccountKey(CorporateName.Aleph.ToString(), input.CorporateID);

                #region Aleph
                if (sdsResult != null)
                {
                    var corporate = GetCorporate2(CorporateName.Aleph, input.CorporateID, sdsResult.UserKey,input.Address.Latitude,input.Address.Longitude);
                    if (corporate != null)
                    {
                        result.AlephCorporate=corporate;
                    }
                }
                #endregion

                #region MembershipRecord
                //if (sdsResult.MembershipRecord != null)
                //{
                //    result.MembershipRecord = new MembershipRecord();
                //    result.MembershipRecord.SubscribeToEmail = sdsResult.MembershipRecord.AllowSpam;
                //    result.MembershipRecord.Telephone = new Telephone();
                //    result.MembershipRecord.Telephone.PhoneNumber = sdsResult.MembershipRecord.ContactNumber;
                //    result.MembershipRecord.Telephone.CountryAccessCode = sdsResult.MembershipRecord.ContactNumberDialingPrefix;
                //    result.MembershipRecord.EmailAddress = sdsResult.MembershipRecord.EmailAddress;
                //    result.MembershipRecord.FirstName = sdsResult.MembershipRecord.FirstName;
                //    result.MembershipRecord.LastName = sdsResult.MembershipRecord.LastName;
                //    result.MembershipRecord.MemberID = sdsResult.MembershipRecord.MemberID;
                //    if (sdsResult.MembershipRecord.SubscribeToSms != null)
                //    {
                //        result.MembershipRecord.SubscribeToSms = (bool)sdsResult.MembershipRecord.SubscribeToSms;
                //    }
                //    result.MembershipRecord.SubscribeToSms = (bool)sdsResult.MembershipRecord.SubscribeToSms;
                //    result.MembershipRecord.DefaultGratuityPercentage = sdsResult.MembershipRecord.GratuityRate * 100;
                //    result.MembershipRecord.HonorificID = sdsResult.MembershipRecord.HonorificId;
                //    result.MembershipRecord.HomeAirport = sdsResult.MembershipRecord.HomeAirport;
                //    result.MembershipRecord.BirthDate = sdsResult.MembershipRecord.BirthDate.ToString();
                //}
                #endregion

                return result;
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("MembershipDomain.GetCorporateProfile2:: {0}", ex.ToString() + ex.StackTrace);

                #region Throw MembershipException
                throw new MembershipException(ex.Message);
                #endregion
            }
        }
        #endregion

        #region Private

        private vtod_member_info CreateVtod_member_infoWithSDSProfile(UDI.SDS.MembershipService.CommunicationSettings sdsMembershipRecord, int memberId)
        {
            vtod_member_info newMember = new vtod_member_info();
            newMember.SDSMemberId = memberId;
            newMember.FirstName = sdsMembershipRecord.FirstName;
            newMember.LastName = sdsMembershipRecord.LastName;
            newMember.ReferralCode = Utilities.GetLetterFromNumber(memberId);            
            newMember.ReferralLink = ConfigurationManager.AppSettings["zTripReferralUrl"] + HttpUtility.UrlEncode(newMember.ReferralCode);
            newMember.HasBeenBooked = false;

            return newMember;
        }

        private vtod_member_info CreateVtod_member_infoWithSDSMemberShipRecord(UDI.SDS.MembershipService.MembershipRecord sdsMembershipRecord)
        {
            vtod_member_info newMember = new vtod_member_info();
            newMember.SDSMemberId = sdsMembershipRecord.MemberID; 
            newMember.FirstName = sdsMembershipRecord.FirstName;
            newMember.LastName = sdsMembershipRecord.LastName;
            newMember.ReferralCode = Utilities.GetLetterFromNumber(sdsMembershipRecord.MemberID);
            newMember.ReferralLink = ConfigurationManager.AppSettings["zTripReferralUrl"] + HttpUtility.UrlEncode(newMember.ReferralCode);
            newMember.HasBeenBooked = false;

            return newMember;
        }


        public string getTableName(Int64 tripID)
		{
			string tableName = "";
			try
			{
				using (var db = new DataAccess.VTOD.VTODEntities())
				{
					var trip = db.vtod_trip.Where(p => p.Id == tripID);
					if (trip != null)
					{
						if (trip.FirstOrDefault().FleetTripCode == UDI.VTOD.Common.DTO.Enum.FleetTripCode.Taxi_CSCI || trip.FirstOrDefault().FleetTripCode == UDI.VTOD.Common.DTO.Enum.FleetTripCode.Taxi_MTDATA || trip.FirstOrDefault().FleetTripCode == UDI.VTOD.Common.DTO.Enum.FleetTripCode.Taxi_SDS || trip.FirstOrDefault().FleetTripCode == UDI.VTOD.Common.DTO.Enum.FleetTripCode.Taxi_UDI33)
						{
							tableName = "taxi_trip";
						}
						else if (trip.FirstOrDefault().FleetTripCode == UDI.VTOD.Common.DTO.Enum.FleetTripCode.SDS_Shuttle_Airport || trip.FirstOrDefault().FleetTripCode == UDI.VTOD.Common.DTO.Enum.FleetTripCode.SDS_Shuttle_Charter || trip.FirstOrDefault().FleetTripCode == UDI.VTOD.Common.DTO.Enum.FleetTripCode.SDS_Shuttle_Hourly)
						{
							tableName = "sds_trip";
						}
						else if (trip.FirstOrDefault().FleetTripCode == UDI.VTOD.Common.DTO.Enum.FleetTripCode.Ecar_Aleph || trip.FirstOrDefault().FleetTripCode == UDI.VTOD.Common.DTO.Enum.FleetTripCode.Ecar_GreenTomato)
						{
							tableName = "ecar_trip";
						}
					}
				}
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Error:{0}:", ex);
			}
			return tableName;
		}
		#endregion

		#region Properties
		public TrackTime TrackTime { get; set; }
		#endregion
	}
}
