﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
//using System.Web.Security;
using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Domain.Membership;
using UDI.Utility.Helper;
using UDI.VTOD.Common.Track;
using UDI.VTOD.Common.Helper;
using UDI.VTOD.DataAccess.VTOD;
using UDI.VTOD.Common.DTO.Enum;
using UDI.Map;
using System.IO;
using UDI.Notification.Service.Controller;
using UDI.Notification.Service.Class;
using UDI.VTOD.Domain.Taxi;
using UDI.VTOD.Domain.ECar.Const;
namespace UDI.VTOD.Domain.Notification
{
	public class NotificationDomain : BaseSetting
	{
		static object bookedStatusLocker = new object();
		static object canceledStatusLocker = new object();
		static object completedStatusLocker = new object();
		static object assignedStatusLocker = new object();
		static object bookedVanStatusLocker = new object();
		static object canceledVanStatusLocker = new object();
		static object completedVanStatusLocker = new object();
		static object assignedVanStatusLocker = new object();
        static object bookedEcarStatusLocker = new object();
        static object canceledEcarStatusLocker = new object();
        static object completedEcarStatusLocker = new object();
        static object assignedEcarStatusLocker = new object();

        internal void notifyBookedStatus(object tokenObject)
		{
			lock (bookedStatusLocker)
			{
				try
				{
					using (var db = new DataAccess.VTOD.VTODEntities())
					{
						var tripIDs = db.SP_Taxi_GetTripIDsForBookedNotification().ToList();

						if (tripIDs != null && tripIDs.Any())
						{
							var token = (TokenRS)tokenObject;

							foreach (var tripId in tripIDs)
							{
                                RefreshToken(ref token);
								SentEmail(token, tripId.Value, Taxi.Const.TaxiTripStatusType.Booked);//, headerTemplate, bodyTemplate);
							}
						}

					}

				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}
			}
		}

		internal void notifyCanceledStatus(object tokenObject)
		{
			lock (canceledStatusLocker)
			{
				try
				{
					using (var db = new DataAccess.VTOD.VTODEntities())
					{
						var tripIDs = db.SP_Taxi_GetTripIDsForCanceledNotification().ToList();

						if (tripIDs != null && tripIDs.Any())
						{
							var token = (TokenRS)tokenObject;

							foreach (var tripId in tripIDs)
							{
                                RefreshToken(ref token);
								SentEmail(token, tripId.Value, Taxi.Const.TaxiTripStatusType.Canceled);//, headerTemplate, bodyTemplate);
							}
						}

					}

				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}
			}
		}

		internal void notifyCompletedStatus(object tokenObject)
		{
			lock (completedStatusLocker)
			{
				try
				{
					using (var db = new DataAccess.VTOD.VTODEntities())
					{
						var tripIDs = db.SP_Taxi_GetTripIDsForCompletedNotification().ToList();

						if (tripIDs != null && tripIDs.Any())
						{
							var token = (TokenRS)tokenObject;

							foreach (var tripId in tripIDs)
							{
                                RefreshToken(ref token);
								SentEmail(token, tripId.Value, Taxi.Const.TaxiTripStatusType.Completed);//, headerTemplate, bodyTemplate);
							}
						}

					}

				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}
			}
		}

		internal void notifyAssignedStatus(object tokenObject)
		{
			lock (assignedStatusLocker)
			{
				try
				{
					using (var db = new DataAccess.VTOD.VTODEntities())
					{
						var tripIDs = db.SP_Taxi_GetTripIDsForAssignedNotification().ToList();

						if (tripIDs != null && tripIDs.Any())
						{
							var token = (TokenRS)tokenObject;

							foreach (var tripId in tripIDs)
							{
                                RefreshToken(ref token);
								SentSMS(token, tripId.Value, Taxi.Const.TaxiTripStatusType.Assigned);//, headerTemplate, bodyTemplate);
							}
						}

					}

				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}
			}
		}

        internal void notifyEcarBookedStatus(object tokenObject)
        {
            lock (bookedEcarStatusLocker)
            {
                try
                {
                    using (var db = new DataAccess.VTOD.VTODEntities())
                    {
                        var tripIDs = db.SP_Ecar_GetTripIDsForBookedNotification().ToList();

                        if (tripIDs != null && tripIDs.Any())
                        {
                            var token = (TokenRS)tokenObject;

                            foreach (var tripId in tripIDs)
                            {
                                RefreshToken(ref token);
                                SentEcarEmail(token, tripId.Value, ECarTripStatusType.Booked);
                            }
                        }

                    }

                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }
            }
        }

        internal void notifyEcarCanceledStatus(object tokenObject)
        {
            lock (canceledEcarStatusLocker)
            {
                try
                {
                    using (var db = new DataAccess.VTOD.VTODEntities())
                    {
                        var tripIDs = db.SP_Ecar_GetTripIDsForCanceledNotification().ToList();

                        if (tripIDs != null && tripIDs.Any())
                        {
                            var token = (TokenRS)tokenObject;

                            foreach (var tripId in tripIDs)
                            {
                                RefreshToken(ref token);
                                SentEcarEmail(token, tripId.Value, ECarTripStatusType.Canceled);
                            }
                        }

                    }

                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }
            }
        }

        internal void notifyEcarCompletedStatus(object tokenObject)
        {
            lock (completedEcarStatusLocker)
            {
                try
                {
                    using (var db = new DataAccess.VTOD.VTODEntities())
                    {
                        var tripIDs = db.SP_Ecar_GetTripIDsForCompletedNotification().ToList();

                        if (tripIDs != null && tripIDs.Any())
                        {
                            var token = (TokenRS)tokenObject;

                            foreach (var tripId in tripIDs)
                            {
                                RefreshToken(ref token);
                                SentEcarEmail(token, tripId.Value, ECarTripStatusType.Completed);
                            }
                        }

                    }

                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }
            }
        }

        internal void notifyEcarAssignedStatus(object tokenObject)
        {
            lock (assignedEcarStatusLocker)
            {
                try
                {
                    using (var db = new DataAccess.VTOD.VTODEntities())
                    {
                        var tripIDs = db.SP_Ecar_GetTripIDsForAssignedNotification().ToList();

                        if (tripIDs != null && tripIDs.Any())
                        {
                            var token = (TokenRS)tokenObject;

                            foreach (var tripId in tripIDs)
                            {
                                RefreshToken(ref token);
                                SentEcarSMS(token, tripId.Value, ECarTripStatusType.Assigned);
                            }
                        }

                    }

                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }
            }
        }
        public void SentEmail(TokenRS token, long tripId, string tripStatus)//, string headerTemplate, string bodyTemplate)
		{
			try
			{
				using (var db = new DataAccess.VTOD.VTODEntities())
				{
					UDI.VTOD.Domain.Taxi.TaxiUtility u = new Taxi.TaxiUtility(logger);
					var taxiTrip = u.GetTaxiTrip(tripId);
					if (
						(tripStatus == Taxi.Const.TaxiTripStatusType.Booked && taxiTrip.PickMeUpNow.HasValue && !taxiTrip.PickMeUpNow.Value)
						||
						(tripStatus != Taxi.Const.TaxiTripStatusType.Booked)
						)
					{
						var transaction = db.vtod_trip_account_transaction.Where(s => s.TripID == tripId).OrderByDescending(s => s.Id).FirstOrDefault();
						var notification = db.vtod_trip_notification.Where(s => s.Id == tripId).FirstOrDefault();
						var fleet = db.taxi_fleet.Where(s => s.Id == taxiTrip.taxi_trip.FleetId).FirstOrDefault();
						var memberID = taxiTrip.MemberID;
						string error = null;
						int status = (int)Common.DTO.Enum.NotificationStatus.UnSent;
						if (!string.IsNullOrWhiteSpace(memberID) && memberID.ToInt32() > 0)
						{
							#region Getting Member Profile
							var membershipController = new UDI.SDS.MembershipController(token, TrackTime);
							var profile = membershipController.GetProfileCommunicationSettings(memberID.ToInt32());
							#endregion

                            #region Filtering email
                            string configured = System.Configuration.ConfigurationManager.AppSettings["IsTest"];
                            bool isTest;
                            if (!bool.TryParse(configured, out isTest))
                            {
                                isTest = false;
                            }

                            bool isOkToSend = false;
                            if (isTest)
                            {
                                string emailWhiteList = System.Configuration.ConfigurationManager.AppSettings["Email_Whitelist_TEST"];
                                List<string> whiteListEmails = emailWhiteList.Split('|').Select(x => x.Trim()).ToList();
                                isOkToSend = whiteListEmails.Contains(profile.EmailAddress.Trim()); 
                            }
                            else
                            {
                                isOkToSend = true;
                            }
                            #endregion

                            if (isOkToSend)
                            {
							#region Get Tempalte
							var fileName = string.Empty;
							switch (tripStatus)
							{
								case Taxi.Const.TaxiTripStatusType.Booked:
									fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "EmailConfirmation.txt";
									break;
								case Taxi.Const.TaxiTripStatusType.Canceled:
									fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "EmailCanceled.txt";
									break;
								case Taxi.Const.TaxiTripStatusType.Completed:
									fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "EmailConfirmation.txt";
									if (taxiTrip.TotalFareAmount.HasValue && taxiTrip.TotalFareAmount.Value > 0)
									{
										if (taxiTrip.PaymentType.ToLower().Trim() == PaymentType.Cash.ToString().Trim().ToLower())
										{
											fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "EmailCashCardReceipt.txt";
										}
										if (taxiTrip.PaymentType.ToLower().Trim() == PaymentType.PaymentCard.ToString().Trim().ToLower())
										{
											if (transaction != null && transaction.CreditsApplied.HasValue && transaction.CreditsApplied != 0)
											{
												fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "EmailCreditCardReceiptWithZTripCredit.txt";
											}
											else
											{
												fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "EmailCreditCardReceipt.txt";
											}
										}
									}
									break;
								default:
									fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "EmailConfirmation.txt";
									break;
							}
							var templates = fileName.ReadHtmlFromUrl().Split(new string[] { "##" }, StringSplitOptions.None); //File.ReadAllText(fileName).Split(new string[] { "##" }, StringSplitOptions.None);
							var headerTemplate = templates[0];
							var bodyTemplate = templates[1];
							#endregion

							#region Making the Body
							var body = (string)bodyTemplate.Clone();
							body = body.Replace("{FirstName}", taxiTrip.taxi_trip.FirstName);
							body = body.Replace("{LastName}", taxiTrip.taxi_trip.LastName);
							body = body.Replace("{ProviderTripID}", taxiTrip.taxi_trip.DispatchTripId);
							body = body.Replace("{PUDateTime}", string.Format("{0:MM-dd-yyyy hh:mm tt}", taxiTrip.PickupDateTime));
							body = body.Replace("{PUAddress}", taxiTrip.taxi_trip.PickupFullAddress);
							body = body.Replace("{MemberPhone}", profile.ContactNumber.MakeStandardPhoneForamt());
							if (!string.IsNullOrWhiteSpace(taxiTrip.taxi_trip.DropOffFullAddress))
							{
								body = body.Replace("{DOAddress}", taxiTrip.taxi_trip.DropOffFullAddress);
							}
							else
							{
								body = body.Replace("{DOAddress}", "As Directed");
							}
							body = body.Replace("{FleetName}", fleet.Alias);
							body = body.Replace("{FleetPhone}", fleet.PhoneNumber.MakeStandardPhoneForamt());

							if (tripStatus == Taxi.Const.TaxiTripStatusType.Completed)
							{
								if (taxiTrip.TotalFareAmount.HasValue && taxiTrip.TotalFareAmount.Value > 0)
								{
									if (transaction != null && transaction.CreditsApplied.HasValue && transaction.CreditsApplied.Value != 0)
									{
										if (taxiTrip.Gratuity.HasValue && taxiTrip.Gratuity.Value > 0)
										{
											body = body.Replace("{Fare}", (taxiTrip.TotalFareAmount - taxiTrip.Gratuity).ToString());
											body = body.Replace("{Tip}", taxiTrip.Gratuity.Value.ToString());
											body = body.Replace("{zTripCredit}", string.Format("({0})", System.Math.Abs(transaction.CreditsApplied.Value)));
											body = body.Replace("{TotalFare}", (taxiTrip.TotalFareAmount - System.Math.Abs(transaction.CreditsApplied.Value)).ToString());
										}
										else
										{
											body = body.Replace("{Fare}", taxiTrip.TotalFareAmount.ToString());
											body = body.Replace("{Tip}", string.Empty);
											body = body.Replace("{zTripCredit}", string.Format("({0})", System.Math.Abs(transaction.CreditsApplied.Value)));
											body = body.Replace("{TotalFare}", (taxiTrip.TotalFareAmount - System.Math.Abs(transaction.CreditsApplied.Value)).ToString());
										}
									}
									else
									{
										if (taxiTrip.Gratuity.HasValue && taxiTrip.Gratuity.Value > 0)
										{
											body = body.Replace("{Fare}", (taxiTrip.TotalFareAmount - taxiTrip.Gratuity).ToString());
											body = body.Replace("{Tip}", taxiTrip.Gratuity.Value.ToString());
											body = body.Replace("{TotalFare}", (taxiTrip.TotalFareAmount).ToString());
										}
										else
										{
											body = body.Replace("{Fare}", taxiTrip.TotalFareAmount.ToString());
											body = body.Replace("{Tip}", string.Empty);
											body = body.Replace("{TotalFare}", taxiTrip.TotalFareAmount.ToString());
										}
									}


									if (taxiTrip.PaymentType.ToLower().Trim() == PaymentType.PaymentCard.ToString().Trim().ToLower())
									{

										var accountController = new UDI.SDS.AccoutingController(token, TrackTime);
										var creditCardList = accountController.GetCreditCardAccounts(memberID.ToInt32());
										if (creditCardList != null && creditCardList.Any())
										{
											var creditcard = creditCardList.Where(s => s.AccountID == taxiTrip.CreditCardID.ToInt32()).FirstOrDefault();
											if (creditcard != null)
											{
												body = body.Replace("{CardType}", creditcard.CardType.ToString());
												body = body.Replace("{BilledTo}", creditcard.BillingName.ToString());
												body = body.Replace("{CreditCardLast4}", creditcard.Last4.ToString());

												string cardFormat;

												switch (creditcard.CardType)
												{
													case UDI.SDS.MembershipService.CreditCardTypesEnumeration.AMEX:
														cardFormat = "XXXX-XXXXXX-X{0}";
														break;
													case UDI.SDS.MembershipService.CreditCardTypesEnumeration.DINERS:
														cardFormat = "XXXX-XXXXXX-{0}";
														break;
													case UDI.SDS.MembershipService.CreditCardTypesEnumeration.JCB:
														cardFormat = "XXXX-XXXXXXXX{0}";
														break;
													default:
														cardFormat = "XXXX-XXXX-XXXX-{0}";
														break;
												}
												body = body.Replace("{CardNumber}", string.Format(cardFormat, creditcard.Last4));
											}
											else
											{
												body = body.Replace("{CardType}", string.Empty);
												body = body.Replace("{CardNumber}", string.Empty);
												body = body.Replace("{BilledTo}", string.Empty);
												body = body.Replace("{CreditCardLast4}", string.Empty);
											}
										}

									}
									if (taxiTrip.PaymentType.ToLower().Trim() == PaymentType.Cash.ToString().Trim().ToLower())
									{
									}
								}
							}
							#endregion

							#region Making the Header
							var header = (string)headerTemplate.Clone();
							header = header.Replace("{ProviderTripID}", taxiTrip.taxi_trip.DispatchTripId);
							#endregion

							#region Sending Email
							try
							{
								var from = System.Configuration.ConfigurationManager.AppSettings["Notification_Email_From"];
								var host = System.Configuration.ConfigurationManager.AppSettings["Notification_Email_SMTP_Host"];
								var port = System.Configuration.ConfigurationManager.AppSettings["Notification_Email_SMTP_Port"];

								MessageController messageController = new MessageController();
								SMTPEmailMessage smtpMessage = new SMTPEmailMessage();
								smtpMessage.From = from;
								if (profile.EmailAddress.Contains("<"))
								{
									var l = profile.EmailAddress.Split('<');
									smtpMessage.To = new List<string> { l.First().Trim() };
								}
								else
								{
									smtpMessage.To = new List<string> { profile.EmailAddress.Trim() };
								}
								//smtpMessage.To = new List<string> { "Eliseogjr@gmail.com" };
								smtpMessage.Subject = header;
								smtpMessage.MessageBody = body;
								smtpMessage.SMTP_Host = host;
								smtpMessage.SMTP_Port = port.ToInt32();
								smtpMessage.IsBodyHTML = true;

								var errorMessage = string.Empty;
								if (messageController.SendSMTPEmailMessage(smtpMessage, out errorMessage))
								{
									logger.InfoFormat("Email for MemberID: {0} sent successfully, TripID: {1}, Status: {2}", memberID, tripId, tripStatus);
									//logger.InfoFormat("Header: {0} , trip: {1}", memberID, tripId, tripStatus);
									status = (int)Common.DTO.Enum.NotificationStatus.Sent;
								}
								else
								{
									logger.ErrorFormat("Error for sending email for MemberID: {0} TripID: {1}, Status: {2}-- {3}", memberID, tripId, tripStatus, errorMessage);
									status = (int)Common.DTO.Enum.NotificationStatus.Error;
									error = errorMessage;
								}
							}
							catch (Exception ex)
							{
								logger.Error(ex);
								logger.ErrorFormat("Error for sending email for MemberID: {0} TripID: {1}, Status: {2}-- {3}", memberID, tripId, tripStatus, ex.Message);
								status = (int)Common.DTO.Enum.NotificationStatus.Error;
								error = ex.Message;
							}
							finally
							{
								#region Updating Database
								try
								{
									db.SP_vtod_Update_trip_notification(tripId, tripStatus, status, error);
								}
								catch (Exception ex)
								{
									logger.Error(ex);
								}
								#endregion
							}
							#endregion

                            }
						}
					}
				}

			}
			catch (Exception ex)
			{
				logger.Error(ex);
			}
		}

		public void SentSMS(TokenRS token, long tripId, string tripStatus)//, string headerTemplate, string bodyTemplate)
		{
			try
			{
				using (var db = new DataAccess.VTOD.VTODEntities())
				{
					TaxiUtility u = new TaxiUtility(logger);
					var taxiTrip = u.GetTaxiTrip(tripId);
					var notification = db.vtod_trip_notification.Where(s => s.Id == tripId).FirstOrDefault();
					var fleet = db.taxi_fleet.Where(s => s.Id == taxiTrip.taxi_trip.FleetId).FirstOrDefault();
					var memberID = taxiTrip.MemberID;
					string error = null;
					int status = (int)Common.DTO.Enum.NotificationStatus.UnSent;

					if (!string.IsNullOrWhiteSpace(memberID) && memberID.ToInt32() > 0)
					{
						#region Getting Member Profile
						var membershipController = new UDI.SDS.MembershipController(token, TrackTime);

						var profile = membershipController.GetProfileCommunicationSettings(memberID.ToInt32());
						#endregion

						#region Get Tempalte
						var fileName = string.Empty;
						switch (tripStatus)
						{
							case Taxi.Const.TaxiTripStatusType.Assigned:
								fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "SmsAssigned.txt";
								break;
							default:
								//fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "EmailConfirmation.txt";
								break;
						}
						var bodyTemplate = fileName.ReadHtmlFromUrl(); //File.ReadAllText(fileName).Split(new string[] { "##" }, StringSplitOptions.None);
																	   //var headerTemplate = templates[0];
																	   //var bodyTemplate = templates[1];
						#endregion

						#region Get TripStatus
						var tripStatusDAO = db.taxi_trip_status.Where(s => s.TripID == tripId && s.Status == Taxi.Const.TaxiTripStatusType.Assigned).FirstOrDefault();
						#endregion

						try
						{
							if (profile.AllowSms)
							{
								if (tripStatusDAO != null)
								{
									#region Making the Body
									var body = (string)bodyTemplate.Clone();

									body = body.Replace("{DriverName}", tripStatusDAO.DriverName);
									body = body.Replace("{ETA}", tripStatusDAO.ETA.HasValue ? tripStatusDAO.ETA.Value.ToString() : string.Empty);
									body = body.Replace("{VehicleNumber}", tripStatusDAO.VehicleNumber);
									body = body.Replace("{FleetPhone}", fleet.PhoneNumber.MakeStandardPhoneForamt());
									body = body.Replace("{ProviderTripID}", taxiTrip.taxi_trip.DispatchTripId);
									#endregion

									#region Sending SMS

									var account = System.Configuration.ConfigurationManager.AppSettings["Notification_SMS_Twilio_Account"];
									var twilioToken = System.Configuration.ConfigurationManager.AppSettings["Notification_SMS_Twilio_Token"];
									var from = System.Configuration.ConfigurationManager.AppSettings["Notification_SMS_Twilio_From"];

									MessageController messageController = new MessageController();
									TwilioSMSMessage twilioSMSMessage = new TwilioSMSMessage();

									twilioSMSMessage.Account = account;
									twilioSMSMessage.Token = twilioToken;
									twilioSMSMessage.From = from;

									twilioSMSMessage.To = new List<string> { profile.ContactNumber.CleanPhone() };

									twilioSMSMessage.MessageBody = body;

									var errorMessage = string.Empty;

									if (messageController.SendTwilioSMSMessage(twilioSMSMessage, out errorMessage))
									{
										logger.InfoFormat("SMS sent for MemberID: {0} sent successfully, TripID: {1}, TripStatus: {2}", memberID, tripId, tripStatus);
										status = (int)Common.DTO.Enum.NotificationStatus.Sent;

									}
									else
									{
										logger.ErrorFormat("Error for sending SMS for MemberID: {0}, TripID: {1}, TripStatus: {2} -- {3}", memberID, tripId, tripStatus, errorMessage);
										status = (int)Common.DTO.Enum.NotificationStatus.Error;
										error = errorMessage;
									}
									#endregion
								}
								else
								{
									status = (int)Common.DTO.Enum.NotificationStatus.Error;
									error = string.Format("TripStatusDAO not found in database. TripID: {0}", tripId);
								}
							}
							else
							{
								status = (int)Common.DTO.Enum.NotificationStatus.NotAllowed;
								error = string.Format("Sending SMS for this user is not allowed. TripID: {0}", tripId);
							}
						}
						catch (Exception ex)
						{
							logger.Error(ex);
							logger.ErrorFormat("Error for sending SMS for MemberID: {0}, TripID: {1}, TripStatus: {2} -- {3}", memberID, tripId, tripStatus, ex.Message);
							status = (int)Common.DTO.Enum.NotificationStatus.Error;
							error = ex.Message;
						}
						finally
						{
							#region Updating Database
							try
							{
								db.SP_vtod_Update_trip_notification(tripId, tripStatus, status, error);
							}
							catch (Exception ex)
							{
								logger.Error(ex);
							}
							#endregion
						}
					}
				}



			}
			catch (Exception ex)
			{
				logger.Error(ex);
			}
		}


		internal void notifyVanBookedStatus(object tokenObject)
		{
			lock (bookedVanStatusLocker)
			{
				try
				{
					using (var db = new DataAccess.VTOD.VTODEntities())
					{
						var tripIDs = db.SP_Van_GetTripIDsForBookedNotification().ToList();

						if (tripIDs != null && tripIDs.Any())
						{
							var token = (TokenRS)tokenObject;

							foreach (var tripId in tripIDs)
							{

								SentVanEmail(token, tripId.Value, Van.Const.VanTripStatusType.Booked);//, headerTemplate, bodyTemplate);
							}
						}

					}

				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}
			}
		}

		internal void notifyVanCanceledStatus(object tokenObject)
		{
			lock (canceledVanStatusLocker)
			{
				try
				{
					using (var db = new DataAccess.VTOD.VTODEntities())
					{
						var tripIDs = db.SP_Van_GetTripIDsForCanceledNotification().ToList();

						if (tripIDs != null && tripIDs.Any())
						{
							var token = (TokenRS)tokenObject;

							foreach (var tripId in tripIDs)
							{

								SentVanEmail(token, tripId.Value, Van.Const.VanTripStatusType.Canceled);//, headerTemplate, bodyTemplate);
							}
						}

					}

				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}
			}
		}

		internal void notifyVanCompletedStatus(object tokenObject)
		{
			lock (completedVanStatusLocker)
			{
				try
				{
					using (var db = new DataAccess.VTOD.VTODEntities())
					{
						var tripIDs = db.SP_Van_GetTripIDsForCompletedNotification().ToList();

						if (tripIDs != null && tripIDs.Any())
						{
							var token = (TokenRS)tokenObject;

							foreach (var tripId in tripIDs)
							{

								SentVanEmail(token, tripId.Value, Van.Const.VanTripStatusType.Completed);//, headerTemplate, bodyTemplate);
							}
						}

					}

				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}
			}
		}

		internal void notifyVanAssignedStatus(object tokenObject)
		{
			lock (assignedVanStatusLocker)
			{
				try
				{
					using (var db = new DataAccess.VTOD.VTODEntities())
					{
						var tripIDs = db.SP_Van_GetTripIDsForAssignedNotification().ToList();

						if (tripIDs != null && tripIDs.Any())
						{
							var token = (TokenRS)tokenObject;

							foreach (var tripId in tripIDs)
							{

								SentVanSMS(token, tripId.Value, Van.Const.VanTripStatusType.Assigned);//, headerTemplate, bodyTemplate);
							}
						}

					}

				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}
			}
		}

		public void SentVanEmail(TokenRS token, long tripId, string tripStatus)//, string headerTemplate, string bodyTemplate)
		{
			try
			{
				using (var db = new DataAccess.VTOD.VTODEntities())
				{
					UDI.VTOD.Domain.Van.VanUtility u = new Van.VanUtility(logger);
					var vanTrip = u.GetVanTrip(tripId);
					var transaction = db.vtod_trip_account_transaction.Where(s => s.TripID == tripId).OrderByDescending(s => s.Id).FirstOrDefault();
					var notification = db.vtod_trip_notification.Where(s => s.Id == tripId).FirstOrDefault();
					var fleet = db.van_fleet.Where(s => s.Id == vanTrip.van_trip.FleetId).FirstOrDefault();
					var memberID = vanTrip.MemberID;
					string error = null;
					int status = (int)Common.DTO.Enum.NotificationStatus.UnSent;

					if (!string.IsNullOrWhiteSpace(memberID) && memberID.ToInt32() > 0)
					{
						#region Getting Member Profile
						var membershipController = new UDI.SDS.MembershipController(token, TrackTime);

						var profile = membershipController.GetProfileCommunicationSettings(memberID.ToInt32());
						#endregion

						#region Get Tempalte
						var fileName = string.Empty;
						switch (tripStatus)
						{
							case Van.Const.VanTripStatusType.Booked:
								fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "VanEmailConfirmation.txt";
								break;
							case Van.Const.VanTripStatusType.Canceled:
								fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "VanEmailCanceled.txt";
								break;
							case Van.Const.VanTripStatusType.Completed:
								fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "VanEmailConfirmation.txt";
								if (vanTrip.TotalFareAmount.HasValue && vanTrip.TotalFareAmount.Value > 0)
								{
									if (vanTrip.PaymentType.ToLower().Trim() == PaymentType.Cash.ToString().Trim().ToLower())
									{
										fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "VanEmailCashCardReceipt.txt";
									}
									if (vanTrip.PaymentType.ToLower().Trim() == PaymentType.PaymentCard.ToString().Trim().ToLower())
									{
										if (transaction != null && transaction.CreditsApplied.HasValue && transaction.CreditsApplied != 0)
										{
											fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "VanEmailCreditCardReceiptWithZTripCredit.txt";
										}
										else
										{
											fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "VanEmailCreditCardReceipt.txt";
										}
									}
								}
								break;
							default:
								fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "VanEmailConfirmation.txt";
								break;
						}
						var templates = fileName.ReadHtmlFromUrl().Split(new string[] { "##" }, StringSplitOptions.None); //File.ReadAllText(fileName).Split(new string[] { "##" }, StringSplitOptions.None);
						var headerTemplate = templates[0];
						var bodyTemplate = templates[1];
						#endregion

						#region Making the Body
						var body = (string)bodyTemplate.Clone();
						body = body.Replace("{FirstName}", vanTrip.van_trip.FirstName);
						body = body.Replace("{LastName}", vanTrip.van_trip.LastName);
						body = body.Replace("{ProviderTripID}", vanTrip.van_trip.DispatchTripId);
						body = body.Replace("{PUDateTime}", string.Format("{0:MM-dd-yyyy hh:mm tt}", vanTrip.PickupDateTime));
						body = body.Replace("{PUAddress}", vanTrip.van_trip.PickupFullAddress);
						body = body.Replace("{MemberPhone}", profile.ContactNumber.MakeStandardPhoneForamt());
						if (!string.IsNullOrWhiteSpace(vanTrip.van_trip.DropOffFullAddress))
						{
							body = body.Replace("{DOAddress}", vanTrip.van_trip.DropOffFullAddress);
						}
						else
						{
							body = body.Replace("{DOAddress}", "As Directed");
						}
						body = body.Replace("{FleetName}", fleet.Alias);
						body = body.Replace("{FleetPhone}", fleet.PhoneNumber.MakeStandardPhoneForamt());

						if (tripStatus == Van.Const.VanTripStatusType.Completed)
						{
							if (vanTrip.TotalFareAmount.HasValue && vanTrip.TotalFareAmount.Value > 0)
							{
								if (transaction != null && transaction.CreditsApplied.HasValue && transaction.CreditsApplied.Value != 0)
								{
									if (vanTrip.Gratuity.HasValue && vanTrip.Gratuity.Value > 0)
									{
										body = body.Replace("{Fare}", (vanTrip.TotalFareAmount - vanTrip.Gratuity).ToString());
										body = body.Replace("{Tip}", vanTrip.Gratuity.Value.ToString());
										body = body.Replace("{zTripCredit}", string.Format("({0})", System.Math.Abs(transaction.CreditsApplied.Value)));
										body = body.Replace("{TotalFare}", (vanTrip.TotalFareAmount - System.Math.Abs(transaction.CreditsApplied.Value)).ToString());
									}
									else
									{
										body = body.Replace("{Fare}", vanTrip.TotalFareAmount.ToString());
										body = body.Replace("{Tip}", string.Empty);
										body = body.Replace("{zTripCredit}", string.Format("({0})", System.Math.Abs(transaction.CreditsApplied.Value)));
										body = body.Replace("{TotalFare}", (vanTrip.TotalFareAmount - System.Math.Abs(transaction.CreditsApplied.Value)).ToString());
									}
								}
								else
								{
									if (vanTrip.Gratuity.HasValue && vanTrip.Gratuity.Value > 0)
									{
										body = body.Replace("{Fare}", (vanTrip.TotalFareAmount - vanTrip.Gratuity).ToString());
										body = body.Replace("{Tip}", vanTrip.Gratuity.Value.ToString());
										body = body.Replace("{TotalFare}", (vanTrip.TotalFareAmount).ToString());
									}
									else
									{
										body = body.Replace("{Fare}", vanTrip.TotalFareAmount.ToString());
										body = body.Replace("{Tip}", string.Empty);
										body = body.Replace("{TotalFare}", vanTrip.TotalFareAmount.ToString());
									}
								}


								if (vanTrip.PaymentType.ToLower().Trim() == PaymentType.PaymentCard.ToString().Trim().ToLower())
								{

									var accountController = new UDI.SDS.AccoutingController(token, TrackTime);
									var creditCardList = accountController.GetCreditCardAccounts(memberID.ToInt32());
									if (creditCardList != null && creditCardList.Any())
									{
										var creditcard = creditCardList.Where(s => s.AccountID == vanTrip.CreditCardID.ToInt32()).FirstOrDefault();
										if (creditcard != null)
										{
											body = body.Replace("{CardType}", creditcard.CardType.ToString());
											body = body.Replace("{BilledTo}", creditcard.BillingName.ToString());
											body = body.Replace("{CreditCardLast4}", creditcard.Last4.ToString());

											string cardFormat;

											switch (creditcard.CardType)
											{
												case UDI.SDS.MembershipService.CreditCardTypesEnumeration.AMEX:
													cardFormat = "XXXX-XXXXXX-X{0}";
													break;
												case UDI.SDS.MembershipService.CreditCardTypesEnumeration.DINERS:
													cardFormat = "XXXX-XXXXXX-{0}";
													break;
												case UDI.SDS.MembershipService.CreditCardTypesEnumeration.JCB:
													cardFormat = "XXXX-XXXXXXXX{0}";
													break;
												default:
													cardFormat = "XXXX-XXXX-XXXX-{0}";
													break;
											}
											body = body.Replace("{CardNumber}", string.Format(cardFormat, creditcard.Last4));
										}
										else
										{
											body = body.Replace("{CardType}", string.Empty);
											body = body.Replace("{CardNumber}", string.Empty);
											body = body.Replace("{BilledTo}", string.Empty);
											body = body.Replace("{CreditCardLast4}", string.Empty);
										}
									}

								}
								if (vanTrip.PaymentType.ToLower().Trim() == PaymentType.Cash.ToString().Trim().ToLower())
								{
								}
							}
						}
						#endregion

						#region Making the Header
						var header = (string)headerTemplate.Clone();
						header = header.Replace("{ProviderTripID}", vanTrip.van_trip.DispatchTripId);
						#endregion

						#region Sending Email
						try
						{
							var from = System.Configuration.ConfigurationManager.AppSettings["Notification_Email_From"];
							var host = System.Configuration.ConfigurationManager.AppSettings["Notification_Email_SMTP_Host"];
							var port = System.Configuration.ConfigurationManager.AppSettings["Notification_Email_SMTP_Port"];

							MessageController messageController = new MessageController();
							SMTPEmailMessage smtpMessage = new SMTPEmailMessage();
							smtpMessage.From = from;
							if (profile.EmailAddress.Contains("<"))
							{
								var l = profile.EmailAddress.Split('<');
								smtpMessage.To = new List<string> { l.First().Trim() };
							}
							else
							{
								smtpMessage.To = new List<string> { profile.EmailAddress.Trim() };
							}
							//smtpMessage.To = new List<string> { "Eliseogjr@gmail.com" };
							smtpMessage.Subject = header;
							smtpMessage.MessageBody = body;
							smtpMessage.SMTP_Host = host;
							smtpMessage.SMTP_Port = port.ToInt32();
							smtpMessage.IsBodyHTML = true;

							var errorMessage = string.Empty;
							if (messageController.SendSMTPEmailMessage(smtpMessage, out errorMessage))
							{
								logger.InfoFormat("Email for MemberID: {0} sent successfully, TripID: {1}, Status: {2}", memberID, tripId, tripStatus);
								status = (int)Common.DTO.Enum.NotificationStatus.Sent;
							}
							else
							{
								logger.ErrorFormat("Error for sending email for MemberID: {0} TripID: {1}, Status: {2}-- {3}", memberID, tripId, tripStatus, errorMessage);
								status = (int)Common.DTO.Enum.NotificationStatus.Error;
								error = errorMessage;
							}
						}
						catch (Exception ex)
						{
							logger.Error(ex);
							logger.ErrorFormat("Error for sending email for MemberID: {0} TripID: {1}, Status: {2}-- {3}", memberID, tripId, tripStatus, ex.Message);
							status = (int)Common.DTO.Enum.NotificationStatus.Error;
							error = ex.Message;
						}
						finally
						{
							#region Updating Database
							try
							{
								db.SP_vtod_Update_trip_notification(tripId, tripStatus, status, error);
							}
							catch (Exception ex)
							{
								logger.Error(ex);
							}
							#endregion
						}
						#endregion
					}
				}

			}
			catch (Exception ex)
			{
				logger.Error(ex);
			}
		}

		public void SentVanSMS(TokenRS token, long tripId, string tripStatus)//, string headerTemplate, string bodyTemplate)
		{
			try
			{
				using (var db = new DataAccess.VTOD.VTODEntities())
				{
					UDI.VTOD.Domain.Van.VanUtility u = new Van.VanUtility(logger);
					var vanTrip = u.GetVanTrip(tripId);
					var notification = db.vtod_trip_notification.Where(s => s.Id == tripId).FirstOrDefault();
					var fleet = db.van_fleet.Where(s => s.Id == vanTrip.van_trip.FleetId).FirstOrDefault();
					var memberID = vanTrip.MemberID;
					string error = null;
					int status = (int)Common.DTO.Enum.NotificationStatus.UnSent;

					if (!string.IsNullOrWhiteSpace(memberID) && memberID.ToInt32() > 0)
					{
						#region Getting Member Profile
						var membershipController = new UDI.SDS.MembershipController(token, TrackTime);

						var profile = membershipController.GetProfileCommunicationSettings(memberID.ToInt32());
						#endregion

						#region Get Tempalte
						var fileName = string.Empty;
						switch (tripStatus)
						{
							case Van.Const.VanTripStatusType.Assigned:
								fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "SmsAssigned.txt";
								break;
							default:
								//fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "EmailConfirmation.txt";
								break;
						}
						var bodyTemplate = fileName.ReadHtmlFromUrl(); //File.ReadAllText(fileName).Split(new string[] { "##" }, StringSplitOptions.None);
																	   //var headerTemplate = templates[0];
																	   //var bodyTemplate = templates[1];
						#endregion

						#region Get TripStatus
						var tripStatusDAO = db.van_trip_status.Where(s => s.TripID == tripId && s.Status == Van.Const.VanTripStatusType.Assigned).FirstOrDefault();
						#endregion

						try
						{
							if (profile.AllowSms)
							{
								if (tripStatusDAO != null)
								{
									#region Making the Body
									var body = (string)bodyTemplate.Clone();

									body = body.Replace("{DriverName}", tripStatusDAO.DriverName);
									body = body.Replace("{ETA}", tripStatusDAO.ETA.HasValue ? tripStatusDAO.ETA.Value.ToString() : string.Empty);
									body = body.Replace("{VehicleNumber}", tripStatusDAO.VehicleNumber);
									body = body.Replace("{FleetPhone}", fleet.PhoneNumber.MakeStandardPhoneForamt());
									body = body.Replace("{ProviderTripID}", vanTrip.van_trip.DispatchTripId);
									#endregion

									#region Sending SMS

									var account = System.Configuration.ConfigurationManager.AppSettings["Notification_SMS_Twilio_Account"];
									var twilioToken = System.Configuration.ConfigurationManager.AppSettings["Notification_SMS_Twilio_Token"];
									var from = System.Configuration.ConfigurationManager.AppSettings["Notification_SMS_Twilio_From"];

									MessageController messageController = new MessageController();
									TwilioSMSMessage twilioSMSMessage = new TwilioSMSMessage();

									twilioSMSMessage.Account = account;
									twilioSMSMessage.Token = twilioToken;
									twilioSMSMessage.From = from;

									twilioSMSMessage.To = new List<string> { profile.ContactNumber.CleanPhone() };

									twilioSMSMessage.MessageBody = body;

									var errorMessage = string.Empty;

									if (messageController.SendTwilioSMSMessage(twilioSMSMessage, out errorMessage))
									{
										logger.InfoFormat("SMS sent for MemberID: {0} sent successfully, TripID: {1}, TripStatus: {2}", memberID, tripId, tripStatus);
										status = (int)Common.DTO.Enum.NotificationStatus.Sent;

									}
									else
									{
										logger.ErrorFormat("Error for sending SMS for MemberID: {0}, TripID: {1}, TripStatus: {2} -- {3}", memberID, tripId, tripStatus, errorMessage);
										status = (int)Common.DTO.Enum.NotificationStatus.Error;
										error = errorMessage;
									}
									#endregion
								}
								else
								{
									status = (int)Common.DTO.Enum.NotificationStatus.Error;
									error = string.Format("TripStatusDAO not found in database. TripID: {0}", tripId);
								}
							}
							else
							{
								status = (int)Common.DTO.Enum.NotificationStatus.NotAllowed;
								error = string.Format("Sending SMS for this user is not allowed. TripID: {0}", tripId);
							}
						}
						catch (Exception ex)
						{
							logger.Error(ex);
							logger.ErrorFormat("Error for sending SMS for MemberID: {0}, TripID: {1}, TripStatus: {2} -- {3}", memberID, tripId, tripStatus, ex.Message);
							status = (int)Common.DTO.Enum.NotificationStatus.Error;
							error = ex.Message;
						}
						finally
						{
							#region Updating Database
							try
							{
								db.SP_vtod_Update_trip_notification(tripId, tripStatus, status, error);
							}
							catch (Exception ex)
							{
								logger.Error(ex);
							}
							#endregion
						}
					}
				}



			}
			catch (Exception ex)
			{
				logger.Error(ex);
			}
		}


        public void SentEcarEmail(TokenRS token, long tripId, string tripStatus)
        {
            try
            {
                using (var db = new DataAccess.VTOD.VTODEntities())
                {
                    UDI.VTOD.Domain.ECar.ECarUtility u = new UDI.VTOD.Domain.ECar.ECarUtility(logger);
                    var ecarTrip = u.GetECarTrip(tripId);
                    var transaction = db.vtod_trip_account_transaction.Where(s => s.TripID == tripId).OrderByDescending(s => s.Id).FirstOrDefault();
                    var notification = db.vtod_trip_notification.Where(s => s.Id == tripId).FirstOrDefault();
                    var fleet = db.ecar_fleet.Where(s => s.Id == ecarTrip.ecar_trip.FleetId).FirstOrDefault();
                    var memberID = ecarTrip.MemberID;
                    string error = null;
                    int status = (int)Common.DTO.Enum.NotificationStatus.UnSent;

                    if (!string.IsNullOrWhiteSpace(memberID) && memberID.ToInt32() > 0)
                    {
                        #region Getting Member Profile
                        var membershipController = new UDI.SDS.MembershipController(token, TrackTime);

                        var profile = membershipController.GetProfileCommunicationSettings(memberID.ToInt32());
                        #endregion

                        #region Get Tempalte
                        var fileName = string.Empty;
                        switch (tripStatus)
                        {
                            case ECarTripStatusType.Booked:
                                fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "ECarEmailConfirmation.txt";
                                break;
                            case ECarTripStatusType.Canceled:
                                fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "ECarEmailCanceled.txt";
                                break;
                            case ECarTripStatusType.Completed:
                                fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "ECarEmailConfirmation.txt";
                                if (ecarTrip.TotalFareAmount.HasValue && ecarTrip.TotalFareAmount.Value > 0)
                                {
                                    if (ecarTrip.PaymentType.ToLower().Trim() == PaymentType.Cash.ToString().Trim().ToLower())
                                    {
                                        fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "ECarEmailCashCardReceipt.txt";
                                    }
                                    if (ecarTrip.PaymentType.ToLower().Trim() == PaymentType.PaymentCard.ToString().Trim().ToLower())
                                    {
                                        if (transaction != null && transaction.CreditsApplied.HasValue && transaction.CreditsApplied != 0)
                                        {
                                            fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "ECarEmailCreditCardReceiptWithZTripCredit.txt";
                                        }
                                        else
                                        {
                                            fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "ECarEmailCreditCardReceipt.txt";
                                        }
                                    }
                                }
                                break;
                            default:
                                fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "ECarEmailConfirmation.txt";
                                break;
                        }
                        var templates = fileName.ReadHtmlFromUrl().Split(new string[] { "##" }, StringSplitOptions.None); //File.ReadAllText(fileName).Split(new string[] { "##" }, StringSplitOptions.None);
                        var headerTemplate = templates[0];
                        var bodyTemplate = templates[1];
                        #endregion

                        #region Making the Body
                        var body = (string)bodyTemplate.Clone();
                        body = body.Replace("{FirstName}", ecarTrip.ecar_trip.FirstName);
                        body = body.Replace("{LastName}", ecarTrip.ecar_trip.LastName);
                        body = body.Replace("{ProviderTripID}", ecarTrip.ecar_trip.DispatchTripId);
                        body = body.Replace("{PUDateTime}", string.Format("{0:MM-dd-yyyy hh:mm tt}", ecarTrip.PickupDateTime));
                        body = body.Replace("{PUAddress}", ecarTrip.ecar_trip.PickupFullAddress);
                        body = body.Replace("{MemberPhone}", profile.ContactNumber.MakeStandardPhoneForamt());
                        if (!string.IsNullOrWhiteSpace(ecarTrip.ecar_trip.DropOffFullAddress))
                        {
                            body = body.Replace("{DOAddress}", ecarTrip.ecar_trip.DropOffFullAddress);
                        }
                        else
                        {
                            body = body.Replace("{DOAddress}", "As Directed");
                        }
                        body = body.Replace("{FleetName}", fleet.Alias);
                        body = body.Replace("{FleetPhone}", fleet.PhoneNumber.MakeStandardPhoneForamt());

                        if (tripStatus == ECarTripStatusType.Completed)
                        {
                            if (ecarTrip.TotalFareAmount.HasValue && ecarTrip.TotalFareAmount.Value > 0)
                            {
                                if (transaction != null && transaction.CreditsApplied.HasValue && transaction.CreditsApplied.Value != 0)
                                {
                                    if (ecarTrip.Gratuity.HasValue && ecarTrip.Gratuity.Value > 0)
                                    {
                                        body = body.Replace("{Fare}", (ecarTrip.TotalFareAmount - ecarTrip.Gratuity).ToString());
                                        body = body.Replace("{Tip}", ecarTrip.Gratuity.Value.ToString());
                                        body = body.Replace("{zTripCredit}", string.Format("({0})", System.Math.Abs(transaction.CreditsApplied.Value)));
                                        body = body.Replace("{TotalFare}", (ecarTrip.TotalFareAmount - System.Math.Abs(transaction.CreditsApplied.Value)).ToString());
                                    }
                                    else
                                    {
                                        body = body.Replace("{Fare}", ecarTrip.TotalFareAmount.ToString());
                                        body = body.Replace("{Tip}", string.Empty);
                                        body = body.Replace("{zTripCredit}", string.Format("({0})", System.Math.Abs(transaction.CreditsApplied.Value)));
                                        body = body.Replace("{TotalFare}", (ecarTrip.TotalFareAmount - System.Math.Abs(transaction.CreditsApplied.Value)).ToString());
                                    }
                                }
                                else
                                {
                                    if (ecarTrip.Gratuity.HasValue && ecarTrip.Gratuity.Value > 0)
                                    {
                                        body = body.Replace("{Fare}", (ecarTrip.TotalFareAmount - ecarTrip.Gratuity).ToString());
                                        body = body.Replace("{Tip}", ecarTrip.Gratuity.Value.ToString());
                                        body = body.Replace("{TotalFare}", (ecarTrip.TotalFareAmount).ToString());
                                    }
                                    else
                                    {
                                        body = body.Replace("{Fare}", ecarTrip.TotalFareAmount.ToString());
                                        body = body.Replace("{Tip}", string.Empty);
                                        body = body.Replace("{TotalFare}", ecarTrip.TotalFareAmount.ToString());
                                    }
                                }


                                if (ecarTrip.PaymentType.ToLower().Trim() == PaymentType.PaymentCard.ToString().Trim().ToLower())
                                {

                                    var accountController = new UDI.SDS.AccoutingController(token, TrackTime);
                                    var creditCardList = accountController.GetCreditCardAccounts(memberID.ToInt32());
                                    if (creditCardList != null && creditCardList.Any())
                                    {
                                        var creditcard = creditCardList.Where(s => s.AccountID == ecarTrip.CreditCardID.ToInt32()).FirstOrDefault();
                                        if (creditcard != null)
                                        {
                                            body = body.Replace("{CardType}", creditcard.CardType.ToString());
                                            body = body.Replace("{BilledTo}", creditcard.BillingName.ToString());
                                            body = body.Replace("{CreditCardLast4}", creditcard.Last4.ToString());

                                            string cardFormat;

                                            switch (creditcard.CardType)
                                            {
                                                case UDI.SDS.MembershipService.CreditCardTypesEnumeration.AMEX:
                                                    cardFormat = "XXXX-XXXXXX-X{0}";
                                                    break;
                                                case UDI.SDS.MembershipService.CreditCardTypesEnumeration.DINERS:
                                                    cardFormat = "XXXX-XXXXXX-{0}";
                                                    break;
                                                case UDI.SDS.MembershipService.CreditCardTypesEnumeration.JCB:
                                                    cardFormat = "XXXX-XXXXXXXX{0}";
                                                    break;
                                                default:
                                                    cardFormat = "XXXX-XXXX-XXXX-{0}";
                                                    break;
                                            }
                                            body = body.Replace("{CardNumber}", string.Format(cardFormat, creditcard.Last4));
                                        }
                                        else
                                        {
                                            body = body.Replace("{CardType}", string.Empty);
                                            body = body.Replace("{CardNumber}", string.Empty);
                                            body = body.Replace("{BilledTo}", string.Empty);
                                            body = body.Replace("{CreditCardLast4}", string.Empty);
                                        }
                                    }

                                }
                                if (ecarTrip.PaymentType.ToLower().Trim() == PaymentType.Cash.ToString().Trim().ToLower())
                                {
                                }
                            }
                        }
                        #endregion

                        #region Making the Header
                        var header = (string)headerTemplate.Clone();
                        header = header.Replace("{ProviderTripID}", ecarTrip.ecar_trip.DispatchTripId);
                        #endregion

                        #region Sending Email
                        try
                        {
                            var from = System.Configuration.ConfigurationManager.AppSettings["Notification_Email_From"];
                            var host = System.Configuration.ConfigurationManager.AppSettings["Notification_Email_SMTP_Host"];
                            var port = System.Configuration.ConfigurationManager.AppSettings["Notification_Email_SMTP_Port"];

                            MessageController messageController = new MessageController();
                            SMTPEmailMessage smtpMessage = new SMTPEmailMessage();
                            smtpMessage.From = from;
                            if (profile.EmailAddress.Contains("<"))
                            {
                                var l = profile.EmailAddress.Split('<');
                                smtpMessage.To = new List<string> { l.First().Trim() };
                            }
                            else
                            {
                                smtpMessage.To = new List<string> { profile.EmailAddress.Trim() };
                            }
                            smtpMessage.Subject = header;
                            smtpMessage.MessageBody = body;
                            smtpMessage.SMTP_Host = host;
                            smtpMessage.SMTP_Port = port.ToInt32();
                            smtpMessage.IsBodyHTML = true;

                            var errorMessage = string.Empty;
                            if (messageController.SendSMTPEmailMessage(smtpMessage, out errorMessage))
                            {
                                logger.InfoFormat("Email for MemberID: {0} sent successfully, TripID: {1}, Status: {2}", memberID, tripId, tripStatus);
                                status = (int)Common.DTO.Enum.NotificationStatus.Sent;
                            }
                            else
                            {
                                logger.ErrorFormat("Error for sending email for MemberID: {0} TripID: {1}, Status: {2}-- {3}", memberID, tripId, tripStatus, errorMessage);
                                status = (int)Common.DTO.Enum.NotificationStatus.Error;
                                error = errorMessage;
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.Error(ex);
                            logger.ErrorFormat("Error for sending email for MemberID: {0} TripID: {1}, Status: {2}-- {3}", memberID, tripId, tripStatus, ex.Message);
                            status = (int)Common.DTO.Enum.NotificationStatus.Error;
                            error = ex.Message;
                        }
                        finally
                        {
                            #region Updating Database
                            try
                            {
                                db.SP_vtod_Update_trip_notification(tripId, tripStatus, status, error);
                            }
                            catch (Exception ex)
                            {
                                logger.Error(ex);
                            }
                            #endregion
                        }
                        #endregion
                    }
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        public void SentEcarSMS(TokenRS token, long tripId, string tripStatus)//, string headerTemplate, string bodyTemplate)
        {
            try
            {
                using (var db = new DataAccess.VTOD.VTODEntities())
                {
                    UDI.VTOD.Domain.ECar.ECarUtility u = new ECar.ECarUtility(logger);
                    var ecarTrip = u.GetECarTrip(tripId);
                    var notification = db.vtod_trip_notification.Where(s => s.Id == tripId).FirstOrDefault();
                    var fleet = db.ecar_fleet.Where(s => s.Id == ecarTrip.ecar_trip.FleetId).FirstOrDefault();
                    var memberID = ecarTrip.MemberID;
                    string error = null;
                    int status = (int)Common.DTO.Enum.NotificationStatus.UnSent;

                    if (!string.IsNullOrWhiteSpace(memberID) && memberID.ToInt32() > 0)
                    {
                        #region Getting Member Profile
                        var membershipController = new UDI.SDS.MembershipController(token, TrackTime);

                        var profile = membershipController.GetProfileCommunicationSettings(memberID.ToInt32());
                        #endregion

                        #region Get Tempalte
                        var fileName = string.Empty;
                        switch (tripStatus)
                        {
                            case ECarTripStatusType.Assigned:
                                fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "SmsAssigned.txt";
                                break;
                            default:
                                //fileName = System.Configuration.ConfigurationManager.AppSettings["TemplatesAddress"] + "EmailConfirmation.txt";
                                break;
                        }
                        var bodyTemplate = fileName.ReadHtmlFromUrl(); //File.ReadAllText(fileName).Split(new string[] { "##" }, StringSplitOptions.None);
                                                                       //var headerTemplate = templates[0];
                                                                       //var bodyTemplate = templates[1];
                        #endregion

                        #region Get TripStatus
                        var tripStatusDAO = db.ecar_trip_status.Where(s => s.TripID == tripId && s.Status == ECarTripStatusType.Assigned).FirstOrDefault();
                        #endregion

                        try
                        {
                            if (profile.AllowSms)
                            {
                                if (tripStatusDAO != null)
                                {
                                    #region Making the Body
                                    var body = (string)bodyTemplate.Clone();

                                    body = body.Replace("{DriverName}", tripStatusDAO.DriverName);
                                    body = body.Replace("{ETA}", tripStatusDAO.ETA.HasValue ? tripStatusDAO.ETA.Value.ToString() : string.Empty);
                                    body = body.Replace("{VehicleNumber}", tripStatusDAO.VehicleNumber);
                                    body = body.Replace("{FleetPhone}", fleet.PhoneNumber.MakeStandardPhoneForamt());
                                    body = body.Replace("{ProviderTripID}", ecarTrip.ecar_trip.DispatchTripId);
                                    #endregion

                                    #region Sending SMS

                                    var account = System.Configuration.ConfigurationManager.AppSettings["Notification_SMS_Twilio_Account"];
                                    var twilioToken = System.Configuration.ConfigurationManager.AppSettings["Notification_SMS_Twilio_Token"];
                                    var from = System.Configuration.ConfigurationManager.AppSettings["Notification_SMS_Twilio_From"];

                                    MessageController messageController = new MessageController();
                                    TwilioSMSMessage twilioSMSMessage = new TwilioSMSMessage();

                                    twilioSMSMessage.Account = account;
                                    twilioSMSMessage.Token = twilioToken;
                                    twilioSMSMessage.From = from;

                                    twilioSMSMessage.To = new List<string> { profile.ContactNumber.CleanPhone() };

                                    twilioSMSMessage.MessageBody = body;

                                    var errorMessage = string.Empty;

                                    if (messageController.SendTwilioSMSMessage(twilioSMSMessage, out errorMessage))
                                    {
                                        logger.InfoFormat("SMS sent for MemberID: {0} sent successfully, TripID: {1}, TripStatus: {2}", memberID, tripId, tripStatus);
                                        status = (int)Common.DTO.Enum.NotificationStatus.Sent;

                                    }
                                    else
                                    {
                                        logger.ErrorFormat("Error for sending SMS for MemberID: {0}, TripID: {1}, TripStatus: {2} -- {3}", memberID, tripId, tripStatus, errorMessage);
                                        status = (int)Common.DTO.Enum.NotificationStatus.Error;
                                        error = errorMessage;
                                    }
                                    #endregion
                                }
                                else
                                {
                                    status = (int)Common.DTO.Enum.NotificationStatus.Error;
                                    error = string.Format("TripStatusDAO not found in database. TripID: {0}", tripId);
                                }
                            }
                            else
                            {
                                status = (int)Common.DTO.Enum.NotificationStatus.NotAllowed;
                                error = string.Format("Sending SMS for this user is not allowed. TripID: {0}", tripId);
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.Error(ex);
                            logger.ErrorFormat("Error for sending SMS for MemberID: {0}, TripID: {1}, TripStatus: {2} -- {3}", memberID, tripId, tripStatus, ex.Message);
                            status = (int)Common.DTO.Enum.NotificationStatus.Error;
                            error = ex.Message;
                        }
                        finally
                        {
                            #region Updating Database
                            try
                            {
                                db.SP_vtod_Update_trip_notification(tripId, tripStatus, status, error);
                            }
                            catch (Exception ex)
                            {
                                logger.Error(ex);
                            }
                            #endregion
                        }
                    }
                }



            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }
        internal void RefreshToken(ref TokenRS token)
        {
            var membershipDomain = new MembershipDomain();
            bool isAuthenticate = false;
            if (token != null)
              isAuthenticate = membershipDomain.ValidateToken(token.Username, token.SecurityKey);
            #region Token expires
            if (!isAuthenticate)
            {
                #region Read from Config
                var username = System.Configuration.ConfigurationManager.AppSettings["SDS_Username"];
                var password = System.Configuration.ConfigurationManager.AppSettings["SDS_Password"];
                var fleetType = "SuperShuttle_ExecuCar";
                var version = System.Configuration.ConfigurationManager.AppSettings["Version"];
                #endregion

                #region Making TokenRQ
                var tokenRQ = new UDI.VTOD.Common.DTO.OTA.TokenRQ();
                tokenRQ.Username = username;
                tokenRQ.Password = password;
                tokenRQ.FleetType = fleetType;
                tokenRQ.Version = version;
                #endregion

                #region Get TokenRS
                var queryController = new UDI.VTOD.Controller.QueryController();
                token = queryController.GetToken(tokenRQ);
                #endregion

                #region Modify Token
                token.ClientIPAddress = System.Configuration.ConfigurationManager.AppSettings["SDS_TokenClientIPAddress"];
                token.ClientType = System.Configuration.ConfigurationManager.AppSettings["SDS_TokenClientType"].ToInt32();
                #endregion

                UDI.VTOD.Common.DTO.Static.Token.TokenRS = token;
                UDI.VTOD.Common.DTO.Static.Token.DateTime = DateTime.Now;
            }
            #endregion
		}

		#region Properties
		public TrackTime TrackTime { get; set; }
		#endregion
	}
}
