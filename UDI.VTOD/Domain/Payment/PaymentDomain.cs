﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
//using System.Web.Security;
using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO;
using UDI.VTOD.Common.DTO.OTA;
using UDI.Utility.Helper;
using UDI.VTOD.Common.Track;
using UDI.VTOD.Common.Helper;
using UDI.VTOD.DataAccess.VTOD;
using UDI.VTOD.Common.DTO.Enum;
using UDI.SDS.PaymentsService;
namespace UDI.VTOD.Domain.Utility
{
	public class PaymentDomain : BaseSetting
	{
        public PreSaleVerificationResponse PreSaleCardVerification(TokenRS token, int accountID, decimal amount, int memberID, int merchandID)
		{
			try
			{
				var paymentController = new UDI.SDS.PaymentController(token, TrackTime);
				var result = paymentController.PreSaleCardVerification(accountID, amount, memberID, merchandID);

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("PaymentDomain.PreSaleCardVerification:: {0}", ex.ToString() + ex.StackTrace);

				throw ex;
			}
		}

		#region Properties
		public TrackTime TrackTime { get; set; }
		#endregion
	}
}
