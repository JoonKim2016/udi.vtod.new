﻿using System;
using System.Linq;
using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.Track;
using UDI.VTOD.DataAccess.VTOD;
using UDI.VTOD.Common.DTO.Enum;
using System.Collections.Generic;
using UDI.VTOD.Common.DTO;
using UDI.VTOD.Domain.Utility;
using UDI.SDS.PaymentsService;
using System.Configuration;
using UDI.VTOD.Domain.Membership;

namespace UDI.VTOD.Domain.Payment
{
    /// <summary>
    /// 
    /// </summary>
    public class SplitPaymentDomain : BaseSetting
    {
        internal SplitPaymentInviteRS Invite(TokenRS token, long tripID, string memberAID, string memberBID)
        {
            try
            {
                var membershipDomain = new MembershipDomain();
                membershipDomain.TrackTime = TrackTime;

                // get member b's first name and last name
                var memberProfile = membershipDomain.GetProfileCommunicationSettings(token, int.Parse(memberBID));
                if (memberProfile == null)
                {
                    throw VtodException.CreateException(ExceptionType.Membership, 7001);
                }

                var memberBFirstName = memberProfile.FirstName;
                var memberBLastName = memberProfile.LastName;

                #region Save split payment info into vtod_trip table and add a push notification
                using (var db = new VTODEntities())
                {
                    var vtodTrip = db.vtod_trip.FirstOrDefault(m => m.Id == tripID);
                    vtodTrip.SplitPaymentStatus = (int)SplitPaymentStatus.Offered;
                    vtodTrip.SplitPaymentMemberB = memberBID;
                    vtodTrip.IsSplitPayment = true;
                    vtodTrip.MemberBCreditCardID = null;

                    var taxiTrip = db.taxi_trip.FirstOrDefault(m => m.Id == tripID);
                    taxiTrip.MemberBFirstName = memberBFirstName;
                    taxiTrip.MemberBLastName = memberBLastName;

                    var userNameA = string.Join(" ", taxiTrip.FirstName, taxiTrip.LastName);
                    var userNameB = string.Join(" ", memberBFirstName, memberBLastName); 

                    db.SP_AddAPushNotification("SplitPayment", tripID, long.Parse(memberBID), DateTime.UtcNow, "1", DateTime.UtcNow.AddMinutes(30), "ZTrip", string.Join(" ",userNameB,userNameA));

                    db.SaveChanges();
                }
                #endregion

                return new SplitPaymentInviteRS
                {
                    Success = new Success()
                };
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("SplitPaymentDomain.Invite:: {0}", ex.ToString() + ex.StackTrace);

                throw ex;
            }
        }

        internal SplitPaymentUpdateStatusRS UpdateStatus(TokenRS token, long tripID, string memberAID, string memberBID, bool accepted)
        {
            try
            {
                #region Update split payment status

                if (!string.IsNullOrWhiteSpace(memberAID)) // member a sends it
                {
                    using (var db = new VTODEntities())
                    {
                        var vtodTrip = db.vtod_trip.FirstOrDefault(m => m.Id == tripID);
                        // change status to "canceled"
                        vtodTrip.SplitPaymentStatus = (int)SplitPaymentStatus.Canceled;
                        vtodTrip.MemberBCreditCardID = null;
                        db.SaveChanges();
                    }
                }
                else if (!string.IsNullOrWhiteSpace(memberBID)) // member b sends it
                {
                    if (accepted)
                    {
                        taxi_trip taxiTrip = null;
                        taxi_fleet_user fleetUser = null;
                        taxi_fleet taxiFleet = null;
                        using (var db = new VTODEntities())
                        {
                            taxiTrip = db.taxi_trip.FirstOrDefault(m => m.Id == tripID);
                            fleetUser = db.SP_Taxi_GetFleet_User2(taxiTrip.FleetId, token.Username).FirstOrDefault();
                            taxiFleet = db.taxi_fleet.FirstOrDefault(m => m.Id == taxiTrip.FleetId);
                        }

                        if (fleetUser == null)
                        {
                            throw VtodException.CreateException(ExceptionType.Taxi, 102);
                        }

                        #region get member B's credit card

                        var accountingDomain = new Accounting.AccountingDomain();
                        accountingDomain.TrackTime = TrackTime;

                        PaymentCard paymentCardOfUserB = null;
                        var rs = accountingDomain.AccountingGetMemberPaymentCards(token, new AccountingGetMemberPaymentCardsRQ
                        {
                            MemberId = int.Parse(memberBID)
                        });

                        if (rs.PaymentCards != null && rs.PaymentCards.Count > 0)
                        {
                            // get default payment card
                            paymentCardOfUserB = rs.PaymentCards.FirstOrDefault(m => m.TPA_Extensions.IsDefault.HasValue && m.TPA_Extensions.IsDefault.Value);
                            if (paymentCardOfUserB == null)
                            {
                                // no default payment card, use the first one
                                paymentCardOfUserB = rs.PaymentCards.FirstOrDefault();
                            }
                        }

                        if (paymentCardOfUserB == null)
                        {
                            throw VtodException.CreateException(ExceptionType.Accounting, 8012);
                        }

                        #endregion

                        #region Preauthorize credit card
                        if (fleetUser.PreAuthCreditCardOption)
                        {
                            var pd = new PaymentDomain();
                            PreSaleVerificationResponse preAuthResult;
                            try
                            {
                                preAuthResult = pd.PreSaleCardVerification(
                                    token,
                                    int.Parse(paymentCardOfUserB.CardNumber.ID),
                                    fleetUser.PreAuthCreditCardAmount,
                                    int.Parse(memberBID),
                                    Convert.ToInt32(taxiFleet.SDS_FleetMerchantID.Value)
                                    );

                            }
                            catch (Exception ex)
                            {
                                logger.Error("Unable to pre auth credit card information");
                                logger.ErrorFormat("ex={0}", ex.Message);
                                throw;
                            }

                            if (preAuthResult != null)
                            {
                                if (preAuthResult.IsPrepaidCard == true)
                                {
                                    throw VtodException.CreateException(ExceptionType.Taxi, 2021);
                                }
                                if (preAuthResult.TransactionApproved == false)
                                {
                                    throw VtodException.CreateException(ExceptionType.Taxi, 2010);
                                }
                            }
                            else
                                throw VtodException.CreateException(ExceptionType.Taxi, 2010);
                        }
                        #endregion

                        // change status to "accepted"
                        using (var db = new VTODEntities())
                        {
                            var vtodTrip = db.vtod_trip.FirstOrDefault(m => m.Id == tripID);
                            vtodTrip.SplitPaymentStatus = (int)SplitPaymentStatus.Accepted;
                            vtodTrip.MemberBCreditCardID = paymentCardOfUserB.CardNumber.ID;
                            db.SaveChanges();
                        }
                    }
                    else  // change status to "declined"
                    {
                        using (var db = new VTODEntities())
                        {
                            var vtodTrip = db.vtod_trip.FirstOrDefault(m => m.Id == tripID);
                            vtodTrip.SplitPaymentStatus = (int)SplitPaymentStatus.Declined;
                            db.SaveChanges();
                        }
                    }
                }
                #endregion

                return new SplitPaymentUpdateStatusRS
                {
                    Success = new Success()
                };
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("SplitPaymentDomain.UpdateStatus:: {0}", ex.ToString() + ex.StackTrace);

                throw ex;
            }
        }

        internal SplitPaymentGetInvitationsRS GetInvitations(TokenRS token, string memberBID, long? tripID)
        {
            try
            {
                DateTime dt = DateTime.UtcNow.AddDays(-1);
                var validStatus = ConfigurationManager.AppSettings["SplitPaymentValidStatus"].Split(',');

                List<vtod_trip> vtodTrips;
                using (var db = new VTODEntities())
                {
                    IQueryable<vtod_trip> q;
                    if (tripID == null)
                        q = db.vtod_trip.Where(m => m.SplitPaymentMemberB == memberBID 
                            && m.PickupDateTimeUTC.HasValue && m.PickupDateTimeUTC.Value > dt
                            && validStatus.Contains(m.FinalStatus)
                            && m.SplitPaymentStatus.HasValue && (m.SplitPaymentStatus.Value == (int)SplitPaymentStatus.Offered || m.SplitPaymentStatus.Value == (int)SplitPaymentStatus.Accepted));
                    else
                        q = db.vtod_trip.Where(m => m.Id == tripID && m.SplitPaymentMemberB == memberBID);

                    vtodTrips = q.OrderByDescending(m => m.SplitPaymentStatus).ToList();
                }

                var response = new SplitPaymentGetInvitationsRS
                {
                    Success = new Success(),
                    Invitations = new List<SplitPaymentInvitation>()
                };

                foreach (var t in vtodTrips)
                {
                    response.Invitations.Add(new SplitPaymentInvitation
                    {
                        TripID = t.Id,
                        MemberAID = t.MemberID,
                        MemberBID = t.SplitPaymentMemberB,
                        SplitPaymentStatus = ((SplitPaymentStatus)t.SplitPaymentStatus).ToString()
                    });
                }

                return response;
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("SplitPaymentDomain.UpdateStatus:: {0}", ex.ToString() + ex.StackTrace);

                throw ex;
            }
        }

        #region Properties
        public TrackTime TrackTime { get; set; }
        #endregion

    }
}
