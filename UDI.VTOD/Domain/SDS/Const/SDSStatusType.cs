﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.SDS.Const
{
	public class SDSStatusType
	{
		
		public const string Booked = "Booked";
		public const string FastMeter = "FastMeter";
		public const string DispatchPending = "DispatchPending";
		public const string Canceled = "Canceled";
		public const string Accepted = "Accepted";
		public const string Assigned = "Assigned";
		public const string InTransit = "InTransit";
		public const string InService = "InService";
		public const string MeterON = "MeterOn";
		public const string NoShow = "NoShow";
		public const string Matched = "Matched";
		public const string UnMatched = "UnMatched";
		public const string Completed = "Completed";
		public const string Fare = "Fare";
		public const string Error = "Error";
		public const string MeterOff = "MeterOff";
		public const string Arrived = "Arrived";
		public const string Offered = "Offered";
		public const string PickUp = "PickUp";
		public const string DropOff = "DropOff";
		public const string TripNotFound = "TripNotFound";
		public const string Closed = "Closed";
		public const string Updated = "Updated";
		public const string GPS = "GPS";
		public const string Unknown = "Unknown";
	}
}
