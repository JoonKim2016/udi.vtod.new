﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO.OTA;
using UDI.SDS;
using UDI.VTOD.Common.Helper;
using UDI.VTOD.Common.DTO;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.DataAccess.VTOD;
using UDI.SDS.Helper;
using UDI.Utility.Helper;
//using UDI.SDS.Helper;
using UDI.Utility.Serialization;
using UDI.SDS.DTO;
using UDI.SDS.DTO.Enum;
using UDI.SDS.DispatchUpdatesService;
using UDI.VTOD.Common.Track;
using UDI.SDS.RatesService;
using System.Data.Entity.Validation;
using UDI.VTOD.Domain.VTOD;
using System.Text.RegularExpressions;
using System.Configuration;
using UDI.SDS.ASAPService;
using System.Transactions;
using System.Data.Entity;
using UDI.VTOD.Utility.Common.Cost;

namespace UDI.VTOD.Domain.SDS
{
	public class SDSDomain : BaseSetting
	{
        #region Properties
        public TrackTime TrackTime { get; set; }
        #endregion
        private VTOD.VTODDomain vtod_domain = new VTOD.VTODDomain();

		#region Constructor		
        public SDSDomain(TrackTime trackTime = null)
        {
            this.TrackTime = trackTime;
            vtod_domain.TrackTime = TrackTime;
        }
        #endregion

        #region Methods
        internal TokenRS GetToken(TokenRQ authRequest)
		{

			//System.Diagnostics.TextWriterTraceListener
			try
			{
				#region Modification
				#endregion

				#region Validation
				authRequest.Validate();
				#endregion

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Validation");
				#endregion

				#region Init
				TokenRS result = null;
				//var sdsReservation = new UDI.SDS.ReservationService();
				//sdsReservation.TrackTime = TrackTime;
				var securityController = new SecurityController(null, TrackTime);

				UserInfo userInfo;
				#endregion

				result = securityController.GetToken(authRequest, out userInfo);

				#region Set Extension Data - UserInfo
				#region This was deleted as info are configurable in DB
				//setUserInfo(userInfo); 
				#endregion
				#endregion

				result.CreateDateTimeUTC = DateTime.UtcNow;

				result.Success = new Success();

				return result;
			}
			catch (VtodException ex)
			{
				logger.ErrorFormat("SDSDomain.GetToken:: {0}", ex.ToString() + ex.StackTrace);

				//#region Throw SDSException || Validation Exception
				//if (ex.ExceptionType == ExceptionType.Validation)
				//{
				throw;
				//}
				//else
				//{
				//	if (ex.Message.Contains("::"))//Remove method name
				//		throw new MembershipException(ex.Message.Substring(ex.Message.IndexOf("::") + 2));
				//	else
				//		throw new MembershipException(ex.Message);
				//}
				//#endregion
			}
			catch (Exception ex)
			{
				throw VtodException.CreateException(ExceptionType.Auth, 101);
			}
		}

		internal OTA_GroundAvailRS GroundAvail(TokenRS tokenVTOD, OTA_GroundAvailRQ groundAvailRQ)
		{
			var tripType = TripType.Unknown;
			OTA_GroundAvailRS result = null;

			tripType = UDI.SDS.Helper.Utility.GetTripType(groundAvailRQ.Service, groundAvailRQ.RateQualifiers.FirstOrDefault());

			switch (tripType)
			{
				case TripType.AsDirected:
					result = groundAvail_Charter(tokenVTOD, groundAvailRQ, tripType);
					break;
				case TripType.LocationToLocation:
					result = groundAvail_Charter(tokenVTOD, groundAvailRQ, tripType);
					break;
				case TripType.Taxi:
					result = groundAvail_Charter(tokenVTOD, groundAvailRQ, tripType);
					break;
				case TripType.Airport:
					result = groundAvail_Airport(tokenVTOD, groundAvailRQ);
					break;
				case TripType.Hourly:
					result = groundAvail_Hourly(tokenVTOD, groundAvailRQ);
					break;
				case TripType.Unknown:
					throw VtodException.CreateException(ExceptionType.SDS, 1006);// new Exception(string.Format(Messages.UDI_SDS_TripType, "GroundAvail"));
			}
			return result;
		}

		//This is just for passing TaxiTripID into this class. Because for Taxi all data is being saved in Taxi domain.
		internal OTA_GroundBookRS GroundBook(TokenRS tokenVTOD, OTA_GroundBookRQ groundBookRQ, out List<Tuple<string, int, long>> disptach_Rez_VTOD, long? vtodTripID, out int fleetTripCode)
		{
			var tripType = TripType.Unknown;
			OTA_GroundBookRS result = null;
			int vtopFleetCode;
			fleetTripCode = UDI.VTOD.Common.DTO.Enum.FleetTripCode.Unkonwn;
			disptach_Rez_VTOD = null;

			tripType = UDI.SDS.Helper.Utility.GetTripType(groundBookRQ.GroundReservations.First().Service, groundBookRQ.GroundReservations.First().RateQualifiers.FirstOrDefault());

			switch (tripType)
			{
				case TripType.AsDirected:
					vtopFleetCode = UDI.VTOD.Common.DTO.Enum.FleetTripCode.SDS_Shuttle_Charter;
					fleetTripCode = vtopFleetCode;
					result = groundBook_Charter(tokenVTOD, groundBookRQ, out disptach_Rez_VTOD, tripType, vtodTripID, vtopFleetCode);
					break;
				case TripType.LocationToLocation:
					vtopFleetCode = UDI.VTOD.Common.DTO.Enum.FleetTripCode.SDS_Shuttle_Charter;
					fleetTripCode = vtopFleetCode;
					result = groundBook_Charter(tokenVTOD, groundBookRQ, out disptach_Rez_VTOD, tripType, vtodTripID, vtopFleetCode);
					break;
				case TripType.Taxi:
					vtopFleetCode = UDI.VTOD.Common.DTO.Enum.FleetTripCode.Taxi_SDS;
					fleetTripCode = vtopFleetCode;
					result = groundBook_Charter(tokenVTOD, groundBookRQ, out disptach_Rez_VTOD, tripType, vtodTripID, vtopFleetCode);
					break;
				case TripType.Airport:
					vtopFleetCode = UDI.VTOD.Common.DTO.Enum.FleetTripCode.SDS_Shuttle_Airport;
					fleetTripCode = vtopFleetCode;
					result = groundBook_Airport(tokenVTOD, groundBookRQ, out disptach_Rez_VTOD, vtopFleetCode);
					break;
				case TripType.Hourly:
					vtopFleetCode = UDI.VTOD.Common.DTO.Enum.FleetTripCode.SDS_Shuttle_Hourly;
					fleetTripCode = vtopFleetCode;
					result = groundBook_Hourly(tokenVTOD, groundBookRQ, out disptach_Rez_VTOD, vtopFleetCode);
					break;
				case TripType.Unknown:
					throw VtodException.CreateException(ExceptionType.SDS, 1006);//throw new Exception(string.Format(Messages.UDI_SDS_TripType, "GroundBook"));
			}

			return result;
		}

        internal OTA_GroundBookRS ModifyBook(TokenRS tokenVTOD, OTA_GroundBookRQ groundBookRQ, out List<Tuple<string, int, long>> disptach_Rez_VTOD, long? vtodTripID, out int fleetTripCode)
        {
            var tripType = TripType.Unknown;
            OTA_GroundBookRS result = null;
            int vtopFleetCode;
            fleetTripCode = UDI.VTOD.Common.DTO.Enum.FleetTripCode.Unkonwn;
            disptach_Rez_VTOD = null;

            tripType = UDI.SDS.Helper.Utility.GetTripType(groundBookRQ.GroundReservations.First().Service, groundBookRQ.GroundReservations.First().RateQualifiers.FirstOrDefault());

            //We'd better throw an exception here if there is no confirmation # in our DB
            //We don't allowe consumer to modify the trip which was not made on VTOD API
            IEnumerable<string> confrimations = groundBookRQ.TPA_Extensions.Confirmations.Select(x => x.ID).ToList();

            using (var db = new DataAccess.VTOD.VTODEntities())
            {
                foreach (string confirmation in confrimations.Distinct())
                {
                    if (!db.sds_trip.Any(x => x.ConfirmationNumber.Equals(confirmation)))
                    {
                        throw VtodException.CreateException(ExceptionType.SDS, 1001);
                    }
                }
            }
                
            switch (tripType)
            {                
                case TripType.Airport:
                    vtopFleetCode = UDI.VTOD.Common.DTO.Enum.FleetTripCode.SDS_Shuttle_Airport;
                    fleetTripCode = vtopFleetCode;
                    //groundBook_Airport is correct method for modifying SDS(TripType.Airport)
                    result = groundBook_Airport(tokenVTOD, groundBookRQ, out disptach_Rez_VTOD, vtopFleetCode,true);
                    break;
                case TripType.Unknown:
                    throw VtodException.CreateException(ExceptionType.SDS, 1006);//throw new Exception(string.Format(Messages.UDI_SDS_TripType, "GroundBook"));
                default:
                    throw new NotImplementedException();                    
            }

            return result;
        }

        internal OTA_GroundCancelRS GroundCancel(TokenRS tokenVTOD, OTA_GroundCancelRQ groundCancelRQ)
		{
			var rateQualifier = string.Empty;
			vtod_trip trip = null;

			try
			{
				rateQualifier = groundCancelRQ.TPA_Extensions.RateQualifiers.First().RateQualifierValue;
			}
			catch
			{ }

			try
			{
				OTA_GroundCancelRS response = new OTA_GroundCancelRS();

				#region Init
				UDI.SDS.ReservationsController reservationController = new UDI.SDS.ReservationsController(tokenVTOD, TrackTime);

				var userName = tokenVTOD.Username; //groundCancelRQ.Reservation.Verification.PersonName.GivenName + " " + lastName;
				var lastName = string.Empty;
				var phoneNumber = string.Empty;
				var postalCode = string.Empty;
                var userId = string.Empty;
				#endregion

				#region Modification
				#region Modify phone numbers
				if (groundCancelRQ != null && groundCancelRQ.Reservation != null && groundCancelRQ.Reservation.Verification != null && groundCancelRQ.Reservation.Verification.TelephoneInfo != null)
				{
					groundCancelRQ.Reservation.Verification.TelephoneInfo.AreaCityCode = string.IsNullOrWhiteSpace(groundCancelRQ.Reservation.Verification.TelephoneInfo.AreaCityCode) ? groundCancelRQ.Reservation.Verification.TelephoneInfo.AreaCityCode : groundCancelRQ.Reservation.Verification.TelephoneInfo.AreaCityCode.CleanPhone();
					groundCancelRQ.Reservation.Verification.TelephoneInfo.PhoneNumber = string.IsNullOrWhiteSpace(groundCancelRQ.Reservation.Verification.TelephoneInfo.PhoneNumber) ? groundCancelRQ.Reservation.Verification.TelephoneInfo.PhoneNumber : groundCancelRQ.Reservation.Verification.TelephoneInfo.PhoneNumber.CleanPhone();
				}
				#endregion
				#endregion

				#region Validation
				groundCancelRQ.Validate();
				#endregion

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Validation");
				#endregion

				#region Set Common objects in OTA_GroundCancelRS

				response.EchoToken = groundCancelRQ.EchoToken;
				response.Target = groundCancelRQ.Target;
				response.Version = groundCancelRQ.Version;
				response.PrimaryLangID = groundCancelRQ.PrimaryLangID;

                #endregion
                #region "Call Center Agent"
                if(userName.ToLower()== "Rez".ToLower())
                {
                    if (groundCancelRQ != null && groundCancelRQ.TPA_Extensions != null && !string.IsNullOrEmpty(groundCancelRQ.TPA_Extensions.UserId))
                    {
                        userName = groundCancelRQ.TPA_Extensions.UserId.ToString();
                    }
                }
                #endregion

                #region 01- ConfirmationID for regular trip
                var confirmationIDs = groundCancelRQ.Reservation.UniqueID.Where(s => s.Type.Trim().ToLower() == ConfirmationType.Confirmation.ToString().ToLower());
				long? confirmationID = null;
				if (confirmationIDs != null && confirmationIDs.Any())
				{
					confirmationID = confirmationIDs.First().ID.ToInt64();//.tolo;
				}
				#endregion

				#region 02 - DispatchConfirmationID for regular trip
				var dispatchConfirmationIDs = groundCancelRQ.Reservation.UniqueID.Where(s => s.Type.Trim().ToLower() == ConfirmationType.DispatchConfirmation.ToString().ToLower());
				string dispatchConfirmationID = null;
				if (dispatchConfirmationIDs != null && dispatchConfirmationIDs.Any())
				{
					dispatchConfirmationID = dispatchConfirmationIDs.First().ID;
				}
				#endregion

				#region 03 - Get AgencyConfirmationID for Paratransit
				var agencyConfirmationIDs = groundCancelRQ.Reservation.UniqueID.Where(s => s.Type.Trim().ToLower() == ConfirmationType.AgencyConfirmation.ToString().ToLower());

				long? agencyConfirmationID = null;
				string agencyContext = string.Empty;
				if (agencyConfirmationIDs != null && agencyConfirmationIDs.Any())
				{
					agencyConfirmationID = agencyConfirmationIDs.First().ID.ToInt64();
					agencyContext = agencyConfirmationIDs.First().ID_Context;
				}
				#endregion

				#region Get DisptachConfirmation
				if (confirmationID.HasValue && confirmationID.Value > 0)
				{
					try
					{

						using (var db = new DataAccess.VTOD.VTODEntities())
						{
							#region Track
							if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
							#endregion


							#region Track
							if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_DB, "db.vtod_trip.Where(s => s.Id == confirmationID).ToList()");
							#endregion

							//if (trip != null)
							//{
							//var sdstrip = (sds_trip)sds_trips.First();
							if (rateQualifier.Trim().ToLower() == "taxi")
							{
								trip = db.vtod_trip.Include("taxi_trip").Where(s => s.Id == confirmationID).FirstOrDefault();

								if (trip != null)
								{
									dispatchConfirmationID = trip.taxi_trip.DispatchTripId;
								}

							}
							else
							{
								trip = db.vtod_trip.Include("sds_trip").Where(s => s.Id == confirmationID).FirstOrDefault();

								if (trip != null)
								{

									dispatchConfirmationID = trip.sds_trip.ConfirmationNumber;
								}
							}
							//}
						}


					}
					catch (Exception ex)
					{
						logger.Error("GroundCancel", ex);
						throw Common.DTO.VtodException.CreateFieldRequiredValidationException("UniqueID"); // new Exception(Messages.VTOD_SDSDomain_GroundCancel_UniqueID);
					}
				}
				#endregion
                
				#region Validation
				if (!string.IsNullOrWhiteSpace(dispatchConfirmationID))
				{
					var validation = false;

					#region Last Name and User Name
					try
					{
						if (groundCancelRQ.Reservation.Verification != null && groundCancelRQ.Reservation.Verification.PersonName != null && !string.IsNullOrWhiteSpace(groundCancelRQ.Reservation.Verification.PersonName.Surname))
						{
							lastName = groundCancelRQ.Reservation.Verification.PersonName.Surname;
							validation = true;
						}
					}
					catch { }
					#endregion

					#region Phone
					try
					{
						if (groundCancelRQ.Reservation.Verification != null && groundCancelRQ.Reservation.Verification.TelephoneInfo != null && !string.IsNullOrWhiteSpace(groundCancelRQ.Reservation.Verification.TelephoneInfo.PhoneNumber))
						{
							phoneNumber = (string.IsNullOrWhiteSpace(groundCancelRQ.Reservation.Verification.TelephoneInfo.AreaCityCode) ? string.Empty : groundCancelRQ.Reservation.Verification.TelephoneInfo.AreaCityCode) + groundCancelRQ.Reservation.Verification.TelephoneInfo.PhoneNumber;
							validation = true;
						}
					}
					catch { }
					#endregion

					#region Postal Code
					try
					{
						if (groundCancelRQ.Reservation.Verification != null && groundCancelRQ.Reservation.Verification.AddressInfo != null && !string.IsNullOrWhiteSpace(groundCancelRQ.Reservation.Verification.AddressInfo.PostalCode))
						{
							postalCode = groundCancelRQ.Reservation.Verification.AddressInfo.PostalCode;
							validation = true;
						}
					}
					catch { }
					#endregion

					if (!validation)
					{
						throw VtodException.CreateFieldRequiredValidationException("PostalCode or PhoneNumber or PersonName"); // new Exception(string.Format(Messages.VTOD_SDSDomain_GroundCancel_PostalCode, "GroundCancel"));
					}
				}
				#endregion

				if (!string.IsNullOrWhiteSpace(dispatchConfirmationID) || (agencyConfirmationID.HasValue && agencyConfirmationID.Value > 0))
				{
					bool result = false;

					if (!string.IsNullOrWhiteSpace(dispatchConfirmationID))
						result = reservationController.CancelReservation(dispatchConfirmationID, userName, lastName, phoneNumber, postalCode);
					else if (agencyConfirmationID.HasValue && agencyConfirmationID.Value > 0)
						result = reservationController.CancelReservation(agencyConfirmationID.ToString(), agencyContext);

					if (result)
					{
						response.Reservation = new Common.DTO.OTA.Reservation();
						response.Reservation.CancelConfirmation = new CancelConfirmation();
						response.Reservation.CancelConfirmation.UniqueID = groundCancelRQ.Reservation.UniqueID.FirstOrDefault();
					}

					if (trip != null) //For NORTA and zTrip.com
					{
						UpdateFinalStatus(trip.Id, UDI.VTOD.Domain.SDS.Const.SDSStatusType.Canceled, null, null, null);
					}

					response.Success = new Success();
				}
				else
				{
					throw VtodException.CreateException(ExceptionType.SDS, 5001);//throw new Exception(Messages.UDI_SDS_CancelFailed);
				}

				return response;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("SDSDomain.GroundCancel:: {0}", ex.ToString() + ex.StackTrace);
				throw;
			}
		}

		internal OTA_GroundResRetrieveRS GroundResRetrieve(TokenRS tokenVTOD, OTA_GroundResRetrieveRQ groundResRetrieveRQ)
		{
			try
			{
				var rateQualifier = string.Empty;

				try
				{
					rateQualifier = groundResRetrieveRQ.TPA_Extensions.RateQualifiers.First().RateQualifierValue;
				}
				catch
				{ }

				OTA_GroundResRetrieveRS response = new OTA_GroundResRetrieveRS();
				vtod_trip trip = null;
				vtod_driver_info returnedDriverInfo = null;

				#region Modification
				#endregion

				#region Validation
				groundResRetrieveRQ.Validate();
				#endregion

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Validation");
				#endregion

				#region Set Common objects in OTA_GroundResRetrieveRS

				response.EchoToken = groundResRetrieveRQ.EchoToken;
				response.Target = groundResRetrieveRQ.Target;
				response.Version = groundResRetrieveRQ.Version;
				response.PrimaryLangID = groundResRetrieveRQ.PrimaryLangID;
                 
				#endregion

				#region Init
				//var vehiclesController = new UDI.SDS.VehiclesController(tokenVTOD, TrackTime);
				var reservationsController = new UDI.SDS.ReservationsController(tokenVTOD, TrackTime);
				var vtodDomain = new VTOD.VTODDomain(); vtodDomain.TrackTime = TrackTime;
				#endregion

				#region Get ConfirmationID for regular trip
				var confirmationIDs = groundResRetrieveRQ.Reference.Where(s => s.Type.Trim().ToLower() == ConfirmationType.Confirmation.ToString().ToLower());
				long? confirmationID = null;
				if (confirmationIDs != null && confirmationIDs.Any())
				{
					confirmationID = confirmationIDs.First().ID.ToInt64();
				}
				#endregion

				#region Get DispatchConfirmationID for Paratransit
				var dispatchConfirmationIDs = groundResRetrieveRQ.Reference.Where(s => s.Type.Trim().ToLower() == ConfirmationType.DispatchConfirmation.ToString().ToLower());
				string dispatchConfirmationID = null;
				if (dispatchConfirmationIDs != null && dispatchConfirmationIDs.Any())
				{
					dispatchConfirmationID = dispatchConfirmationIDs.First().ID;
				}
				#endregion

				#region Get AgencyConfirmationID for Paratransit
				var agencyConfirmationIDs = groundResRetrieveRQ.Reference.Where(s => s.Type.Trim().ToLower() == ConfirmationType.AgencyConfirmation.ToString().ToLower());
				long? agencyConfirmationID = null;
				string agencyContext = null;
				if (agencyConfirmationIDs != null && agencyConfirmationIDs.Any())
				{
					agencyConfirmationID = agencyConfirmationIDs.First().ID.ToInt64();
					agencyContext = agencyConfirmationIDs.First().ID_Context;
				}
				#endregion

				#region Do not delete it
				//#region DispatchConfirmationID
				//var dispatchConfirmationIDs = groundResRetrieveRQ.Reference.Where(s => s.Type.Trim().ToLower() == ConfirmationType.DispatchConfirmation.ToString().ToLower());
				//string dispatchConfirmationID = null;
				//if (dispatchConfirmationIDs != null && dispatchConfirmationIDs.Any())
				//{
				//	dispatchConfirmationID = dispatchConfirmationIDs.First().ID;
				//}
				//#endregion

				//#region Get Confirmation Number From DB
				//if (string.IsNullOrWhiteSpace(dispatchConfirmationID))
				//{
				//	try
				//	{
				//		using (var db = new DataAccess.VTOD.VTODEntities())
				//		{
				//			var sds_trips = db.vtod_trip.Where(s => s.Id == confirmationID).ToList();
				//			if (sds_trips != null && sds_trips.Any())
				//			{
				//				var sdstrip = (sds_trip)sds_trips.First();
				//				dispatchConfirmationID = sdstrip.ConfirmationNumber;
				//			}
				//		}
				//	}
				//	catch (Exception ex)
				//	{
				//		logger.Error("GroundCancel", ex);
				//		throw new Exception(Messages.VTOD_SDSDomain_GroundCancel_UniqueID);
				//	}
				//}
				//#endregion

				#endregion

				#region Get RezId
				long? rezID = null;
				if (
					(confirmationID.HasValue && confirmationID.Value > 0)
					||
					(!string.IsNullOrWhiteSpace(dispatchConfirmationID))
					)
				{
					try
					{

						using (var db = new DataAccess.VTOD.VTODEntities())
						{
							#region Track
							if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
							#endregion


							#region Track
							if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_DB, "db.vtod_trip.Where(s => s.Id == confirmationID).ToList()");
							#endregion

							//if (trip != null)
							//{
							//var sdstrip = (sds_trip)sds_trips.First();
							if (rateQualifier.Trim().ToLower() == "taxi")
							{
								if (confirmationID.HasValue && confirmationID.Value > 0)
									trip = db.vtod_trip.Include("taxi_trip").Where(s => s.Id == confirmationID).FirstOrDefault();
								else if (!string.IsNullOrWhiteSpace(dispatchConfirmationID))
									trip = db.vtod_trip.Include("taxi_trip").Where(s => s.taxi_trip.DispatchTripId == dispatchConfirmationID).FirstOrDefault();

								if (trip != null)
								{
									rezID = trip.taxi_trip.RezId.Value.ToInt64();
								}

							}
							else
							{
								if (confirmationID.HasValue && confirmationID.Value > 0)
									trip = db.vtod_trip.Include("sds_trip").Where(s => s.Id == confirmationID).FirstOrDefault();
								else if (!string.IsNullOrWhiteSpace(dispatchConfirmationID))
									trip = db.vtod_trip.Include("sds_trip").Where(s => s.sds_trip.ConfirmationNumber == dispatchConfirmationID).FirstOrDefault();

								if (trip != null)
								{
									rezID = trip.sds_trip.RezID.Value;
								}
							}
							//}
						}


					}
					catch (Exception ex)
					{
						logger.Error("GroundCancel", ex);
						throw Common.DTO.VtodException.CreateFieldRequiredValidationException("UniqueID"); // new Exception(Messages.VTOD_SDSDomain_GroundCancel_UniqueID);
					}
				}
				#endregion

				#region Do not Deleted it
				//#region Get Status

				//#region Step #1, Get Vehicle Status from SDS web service: GetVehicleAndTripLocation (GetVehicleStatusForTrip)
				//if (!string.IsNullOrWhiteSpace(dispatchConfirmationID))
				//{
				//	var sdsStatusResult = vehiclesController.GetVehicleStatusForTrip(dispatchConfirmationID);

				//	if (sdsStatusResult != null)
				//	{
				//		#region Driver
				//		var firstName = sdsStatusResult.DriverName.Split(',').LastOrDefault();
				//		var lastName = sdsStatusResult.DriverName.Split(',').FirstOrDefault();

				//		response.TPA_Extensions = new TPA_Extensions();
				//		response.TPA_Extensions.Driver = new Person();
				//		response.TPA_Extensions.Driver.PersonName = new PersonName();
				//		response.TPA_Extensions.Driver.PersonName.GivenName = firstName;
				//		response.TPA_Extensions.Driver.PersonName.Surname = lastName;
				//		#endregion

				//		#region Vehicle
				//		response.TPA_Extensions.Vehicles = new Vehicles();
				//		response.TPA_Extensions.Vehicles.Items = new List<Vehicle>();
				//		var vehicle = new Vehicle();
				//		vehicle.Geolocation = new Geolocation();
				//		vehicle.Geolocation.Lat = sdsStatusResult.VehicleLatitude;
				//		vehicle.Geolocation.Long = sdsStatusResult.VehicleLongitude;
				//		vehicle.Course = sdsStatusResult.TripDirection;
				//		vehicle.VehicleNo = sdsStatusResult.VehicleNumber.ToString();
				//		vehicle.MinutesAway = sdsStatusResult.EtaMinutes;
				//		vehicle.Distance = sdsStatusResult.Distance.ToDecimal();

				//		response.TPA_Extensions.Vehicles.Items.Add(vehicle);
				//		#endregion
				//	}
				//}
				//#endregion

				//#region Step #2, Get Trip Status from our DB. Thsoe statuses were pulled from SDS by Windows Service
				//if (!string.IsNullOrWhiteSpace(dispatchConfirmationID))
				//{
				//	sds_tripstatus dbResult = null;

				//	using (var db = new VTODEntities())
				//	{
				//		//var results2 = db.sds_tripstatus.Where(r => r.ConfirmationNumber == confirmationNumber).LastOrDefault();
				//		var dbResults = db.SP_SDS_GetLastStatus(dispatchConfirmationID).ToList();
				//		if (dbResults != null && dbResults.Any())
				//		{
				//			dbResult = db.SP_SDS_GetLastStatus(dispatchConfirmationID).First();
				//		}
				//	}

				//	response.TPA_Extensions.Statuses = new Statuses();
				//	response.TPA_Extensions.Statuses.Status = new List<Status>();
				//	Status status = new Status();

				//	if (dbResult != null)
				//	{
				//		#region Status
				//		status.DateTime = dbResult.LocalTime.ToString();
				//		status.Value = dbResult.ActionType;
				//		#endregion
				//	}
				//	else
				//	{
				//		#region Status
				//		status.Value = "Unknown";
				//		#endregion
				//	}

				//	response.TPA_Extensions.Statuses.Status.Add(status);

				//	//else
				//	//{
				//	//	throw new Exception(string.Format(Messages.VTOD_SDSDomain_GroundResRetrieve_NoStatusFound, "GroundResRetrieve", dispatchConfirmationID));
				//	//}
				//#endregion

				//	#region Save to Database
				//	using (var db = new VTODEntities())
				//	{
				//		db.SP_SDS_InsertLog(vtodTXId, UDI.SDS.DTO.Enum.LogType.ResRetrieve.ToString(), null, Guid.NewGuid().ToString(),
				//			string.Format("dispatchConfirmationID={0}", dispatchConfirmationID), response.ToString());
				//	}
				//	#endregion
				//}
				//else
				//{
				//	throw new Exception(string.Format(Messages.VTOD_SDSDomain_GroundResRetrieve_ReferenceID, "GroundResRetrieve"));
				//}

				//#endregion

				#endregion

				UDI.SDS.ReservationsService.TripStatus sdsStatus = null;
				if ((rezID.HasValue && rezID.Value > 0))
					sdsStatus = reservationsController.GetTripStatus(rezID);
				else if (agencyConfirmationID.HasValue && agencyConfirmationID.Value > 0)
					sdsStatus = reservationsController.GetTripStatus(agencyConfirmationID, agencyContext);
				else
				{
					throw VtodException.CreateException(ExceptionType.SDS, 3001);// new Exception(Messages.UDI_SDS_StatusFailed);
				}

				#region Driver Info
				if (sdsStatus.DriverId > 0 || !string.IsNullOrWhiteSpace(sdsStatus.DriverImageUrl) || !string.IsNullOrWhiteSpace(sdsStatus.DriverName) || !string.IsNullOrWhiteSpace(sdsStatus.DriverPhoneNumber))
				{
					//get image url from db if exist and not updated if not update db
					var driverInfo = new DriverInfo(logger);

					byte[] driverImage = null;

					var newDriver = new vtod_driver_info();


					newDriver.DriverID = sdsStatus.DriverId > 0 ? sdsStatus.DriverId.ToString() : null;


					if (!string.IsNullOrWhiteSpace(sdsStatus.DriverName))
					{
						try
						{
							newDriver.FirstName = sdsStatus.DriverName.Split(' ')[0];
							newDriver.LastName = sdsStatus.DriverName.Split(' ')[1];
						}
						catch { }
					}

					newDriver.CellPhone = sdsStatus.DriverPhoneNumber.CleanPhone10Digit();
					newDriver.zTripQualified = sdsStatus.ZTripCertified;
					newDriver.VehicleID = sdsStatus.VehicleNumber.ToString();

					newDriver.FleetType = trip.FleetType;// Common.DTO.Enum.FleetType.SuperShuttle.ToString();

					#region Image
					if (trip.DriverInfoID.HasValue && trip.DriverInfoID.Value > 0)
					{
						using (var db = new DataAccess.VTOD.VTODEntities())
						{
							returnedDriverInfo = db.vtod_driver_info.FirstOrDefault(s => s.Id == trip.DriverInfoID.Value);
						}
					}
					else
					{
						try
						{
							//var id = Guid.NewGuid();

							if (!string.IsNullOrEmpty(sdsStatus.DriverImageUrl))
							{
								//extract an integer from the beginning of the Guid
								driverImage = new System.Net.WebClient().DownloadData(sdsStatus.DriverImageUrl);

								#region Get DispatchUniqueKey
								var regex = new Regex(@"^http[s]{0,1}://.*/(.*)\.[jpg|png]+$");
								Match match = regex.Match(sdsStatus.DriverImageUrl);
								if (match.Success)
								{
									try
									{
										newDriver.DispatchUniqueKey = match.Groups[1].Value;
									}
									catch (Exception ex)
									{
										logger.Error(string.Format("We cannot get the DispatchUniqueKey from ", sdsStatus.DriverImageUrl), ex);
									}
								}
								#endregion

								//sdsStatus.DriverImageUrl = driverInfo.UpsertDriverDetails(driverInfo: newDriver, driverImage: driverImage).PhotoUrl;
							}

							//update db and save new image to cdn 
							//0 FleetID means SDS
							returnedDriverInfo = driverInfo.UpsertDriverDetails(driverInfo: newDriver, driverImage: driverImage);
							using (var db = new VTODEntities())
							{
								db.SP_vtod_Update_TripDriverInfoID(trip.Id, returnedDriverInfo.Id);
							}
						}
						catch (Exception ex)
						{
							logger.Error(ex);
						}
					}
					#endregion
				}

				#endregion

				#region Make response
				response.TPA_Extensions = new TPA_Extensions();
				response.TPA_Extensions.Statuses = new Statuses();
				response.TPA_Extensions.Statuses.Status = new List<Status>();
				Status status = new Status();

				#region Wrap the status
				status.Value = vtod_domain.ConvertSDSTripStatus(sdsStatus.Status);

				#endregion
				if (trip != null) //NORTA trips do not have record in our DB. So check that
				{
                    if (status.Value.ToLower()== UDI.VTOD.Domain.SDS.Const.SDSStatusType.Accepted.ToLower() || status.Value.ToLower() == UDI.VTOD.Domain.SDS.Const.SDSStatusType.Assigned.ToLower())
                    {
                        UpdateFinalStatus(trip.Id, status.Value, null, null, null, sdsStatus.VehicleEtaMinutes);
                    }
                    else
                    {
                        UpdateFinalStatus(trip.Id, status.Value, null, null, null);
                    }
				}

				#region Convert status for front end device
				status.Value = vtod_domain.ConvertSDSTripStatusMessageForFrontEndDevice(tokenVTOD.Username, status.Value);
				#endregion

				response.TPA_Extensions.Statuses.Status.Add(status);

				response.TPA_Extensions.Vehicles = new Vehicles();
				response.TPA_Extensions.Vehicles.Items = new List<Vehicle>();
				var vehicle = new Vehicle();
				vehicle.ID = sdsStatus.VehicleNumber.ToString();

				if (sdsStatus.VehicleCoordinates != null)
				{
					vehicle.Geolocation = new Geolocation();
					vehicle.Geolocation.Lat = sdsStatus.VehicleCoordinates.Latitude.ToDecimal();
					vehicle.Geolocation.Long = sdsStatus.VehicleCoordinates.Longitude.ToDecimal();
					vehicle.Course = sdsStatus.VehicleCourse.ToDecimal();
					vehicle.MinutesAway = sdsStatus.VehicleEtaMinutes;
				}

				response.TPA_Extensions.Vehicles.Items.Add(vehicle);

				if (sdsStatus.PickupPoint != null)
				{
					response.TPA_Extensions.PickupLocation = new Pickup_Dropoff_Stop();
					response.TPA_Extensions.PickupLocation.Address = new Address();
					response.TPA_Extensions.PickupLocation.Address.Latitude = sdsStatus.PickupPoint.Latitude.ToString();
					response.TPA_Extensions.PickupLocation.Address.Longitude = sdsStatus.PickupPoint.Longitude.ToString();
				}

				#region SharedETA Link
				if (groundResRetrieveRQ.TPA_Extensions != null && groundResRetrieveRQ.TPA_Extensions.RateQualifiers != null && groundResRetrieveRQ.TPA_Extensions.RateQualifiers.Any())
				{
					string uniqueID = null;
					string appSharedLink = ConfigurationManager.AppSettings["SharedETALink"];
					string link;
					if (groundResRetrieveRQ.Reference.FirstOrDefault().Type.Contains("Dispatch"))
					{
						link = "?t=d";
					}
					else
					{
						link = "?t=c";
					}
					if (groundResRetrieveRQ.TPA_Extensions.RateQualifiers != null && groundResRetrieveRQ.TPA_Extensions.RateQualifiers.Any())
					{
						rateQualifier = groundResRetrieveRQ.TPA_Extensions.RateQualifiers.FirstOrDefault().RateQualifierValue;
						link = link + "&RQ=" + rateQualifier;
					}
					if (groundResRetrieveRQ.Reference != null && groundResRetrieveRQ.Reference.Any())
					{
						uniqueID = groundResRetrieveRQ.Reference.FirstOrDefault().ID;
					}
					link = link + "&id=" + UDI.Utility.Helper.Cryptography.ConvertStringToHex(uniqueID);
					response.TPA_Extensions.SharedLink = appSharedLink + link; 
				}
                #endregion

                #region Find the DropOffLocation
                string latitude = null;
                string longitude=null;
                if(trip!=null)
                {
                    if (trip.FleetTripCode == FleetTripCode.SDS_Shuttle_Airport || trip.FleetTripCode == FleetTripCode.SDS_Shuttle_Charter || trip.FleetTripCode == FleetTripCode.SDS_Shuttle_Hourly)
                    {
                        latitude = trip.sds_trip.DropoffLocationLatitude == null ? "" : trip.sds_trip.DropoffLocationLatitude.ToString();
                        longitude = trip.sds_trip.DropoffLocationLongitude == null ? "" : trip.sds_trip.DropoffLocationLongitude.ToString();
                       
                    }
                    else if(trip.FleetTripCode==FleetTripCode.Taxi_SDS)
                    {
                        latitude = trip.taxi_trip.DropOffLatitude == null ? "" : trip.taxi_trip.DropOffLatitude.ToString();
                        longitude = trip.taxi_trip.DropOffLongitude == null ? "" : trip.taxi_trip.DropOffLongitude.ToString();
                    }
                }
                #endregion
                if (!string.IsNullOrEmpty(latitude) || !string.IsNullOrEmpty(longitude))
                {
                    response.TPA_Extensions.DropoffLocation = new Pickup_Dropoff_Stop();
                    response.TPA_Extensions.DropoffLocation.Address = new Address();
                    response.TPA_Extensions.DropoffLocation.Address.Latitude = latitude;
                    response.TPA_Extensions.DropoffLocation.Address.Longitude = longitude;
                }

				response.TPA_Extensions.Contacts = new Contacts();
				response.TPA_Extensions.Contacts.Items = new List<Contact>();

				//Driver contact
				if (returnedDriverInfo != null)
				{
					var contact = new Contact { Name = sdsStatus.DriverName, Telephone = new Telephone { PhoneNumber = sdsStatus.DriverPhoneNumber.CleanPhone10Digit() }, Type = Common.DTO.Enum.ContactType.Driver.ToString(), PhotoUrl = returnedDriverInfo.PhotoUrl, ZtripDesignation = sdsStatus.ZTripCertified };
					response.TPA_Extensions.Contacts.Items.Add(contact);
				}

				//dispatch contact 
				Contact dispatchContact = null;
				string dispatchPhone = sdsStatus.CompanyPhoneNumber;

				if (tokenVTOD.Username.ToLower() == "ztrip" || tokenVTOD.Username.ToLower() == "udi")
				{
					dispatchContact = new Contact
					{
						Type = Common.DTO.Enum.ContactType.Dispatch.ToString(),
						Name = sdsStatus.CompanyName,
						Telephone = new Telephone { PhoneNumber = dispatchPhone.CleanPhone10Digit() },
                        PhotoUrl = ConfigHelper.GetDriverPhotoVersionDefault()
					};
				}
				else
				{
					dispatchContact = new Contact
					{
						Type = Common.DTO.Enum.ContactType.Dispatch.ToString(),
						Name = sdsStatus.CompanyName,
						Telephone = new Telephone { PhoneNumber = dispatchPhone }

					};
				}

				response.TPA_Extensions.Contacts.Items.Add(dispatchContact);

				response.Success = new Success();
				#endregion


				return response;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("SDSDomain.GroundResRetrieve:: {0}", ex.ToString() + ex.StackTrace);
				throw;
			}
		}

		internal OTA_GroundResRetrieveRS GroundOriginalResRetrieve(TokenRS token, OTA_GroundResRetrieveRQ input)
		{
			try
			{
				OTA_GroundResRetrieveRS result = null;

				var reservationsController = new UDI.SDS.ReservationsController(token, TrackTime);
				UDI.SDS.ReservationsService.Reservation sdsResult = null;


				#region Input
				string confirmationNumber = null;
				string lastName = null;
				string phoneNumber = null;
				string postalCode = null;
				int rezId = 0;

				if (input.Reference != null)
				{
					var refObject = input.Reference.Where(s => s.Type == Common.DTO.Enum.ConfirmationType.DispatchConfirmation.ToString()).FirstOrDefault();
					if (refObject != null)
					{
						confirmationNumber = refObject.ID;
					}

					if (input.TPA_Extensions != null && input.TPA_Extensions.Verification != null)
					{
						var verification = input.TPA_Extensions.Verification;

						if (verification.PersonName != null)
							lastName = verification.PersonName.Surname;
						if (verification.TelephoneInfo != null)
							phoneNumber = string.Format("{0}{1}{2}", verification.TelephoneInfo.CountryAccessCode, verification.TelephoneInfo.AreaCityCode, verification.TelephoneInfo.PhoneNumber);
						if (verification.AddressInfo != null)
							postalCode = verification.AddressInfo.PostalCode;
					}

					sdsResult = reservationsController.RetrieveReservation(confirmationNumber, lastName, phoneNumber, postalCode, rezId);
				}
				#endregion

				#region Output
				if (sdsResult != null)
				{
					result = new OTA_GroundResRetrieveRS();
					result.SummaryReservation = new List<SummaryReservation>();
					foreach (var segment in sdsResult.Segments)
					{
						var summaryReservation = new SummaryReservation();
						summaryReservation.Confirmation = new Confirmation { Type = Common.DTO.Enum.ConfirmationType.DispatchConfirmation.ToString(), ID = segment.Confirmation.Number };
						summaryReservation.Confirmation.TPA_Extensions = new TPA_Extensions();
						summaryReservation.Confirmation.TPA_Extensions.Confirmations = new List<Confirmation>();
						var rezIDConf = new Confirmation { Type = Common.DTO.Enum.ConfirmationType.RezID.ToString(), ID = segment.Confirmation.Id.ToString() }; ;
						summaryReservation.Confirmation.TPA_Extensions.Confirmations.Add(rezIDConf);

						summaryReservation.Service = new Service();
						summaryReservation.Service.Pickup = new Pickup_Dropoff_Stop();

						if (segment.TravelSchedule != null && segment.TravelSchedule.PickupItinerary != null)
						{
							summaryReservation.Service.Pickup.DateTime = segment.TravelSchedule.PickupItinerary.BeginDateTime.ToString();
						}
						if (segment.PickupLocation != null)
						{
							if (segment.PickupLocation.Address != null)
							{
								summaryReservation.Service.Pickup.Address = new Address();
								summaryReservation.Service.Pickup.Address.StreetNmbr = segment.PickupLocation.Address.StreetNumber;
								summaryReservation.Service.Pickup.Address.AddressLine = segment.PickupLocation.Address.StreetName;
								summaryReservation.Service.Pickup.Address.CityName = segment.PickupLocation.Address.Municipality;
								summaryReservation.Service.Pickup.Address.StateProv = new StateProv { StateCode = segment.PickupLocation.Address.CountrySubDivision };
								summaryReservation.Service.Pickup.Address.CountryName = new CountryName { Code = segment.PickupLocation.Address.CountryCode };
								summaryReservation.Service.Pickup.Address.PostalCode = segment.PickupLocation.Address.PostalCode;
							}
							if (segment.PickupLocation.Airport != null)
							{
								if (segment.TravelSchedule != null && segment.TravelSchedule != null && segment.TravelSchedule.Flight != null && segment.TravelSchedule.Flight.ArrivalDateTime != null)
								{
									summaryReservation.Service.Pickup.Airline = new Airline { FlightDateTime = segment.TravelSchedule.Flight.ArrivalDateTime.ToString() };
								}
								summaryReservation.Service.Pickup.AirportInfo = new AirportInfo();
								summaryReservation.Service.Pickup.AirportInfo.Arrival = new Arrival_Departure { LocationCode = segment.PickupLocation.Airport.LocationCode };
							}
						}
						if (segment.DropoffLocation != null)
						{
							summaryReservation.Service.Dropoff = new Pickup_Dropoff_Stop();
							if (segment.DropoffLocation.Address != null)
							{
								summaryReservation.Service.Dropoff.Address = new Address();
								summaryReservation.Service.Dropoff.Address.StreetNmbr = segment.DropoffLocation.Address.StreetNumber;
								summaryReservation.Service.Dropoff.Address.AddressLine = segment.DropoffLocation.Address.StreetName;
								summaryReservation.Service.Dropoff.Address.CityName = segment.DropoffLocation.Address.Municipality;
								summaryReservation.Service.Dropoff.Address.StateProv = new StateProv { StateCode = segment.DropoffLocation.Address.CountrySubDivision };
								summaryReservation.Service.Dropoff.Address.CountryName = new CountryName { Code = segment.DropoffLocation.Address.CountryCode };
								summaryReservation.Service.Dropoff.Address.PostalCode = segment.DropoffLocation.Address.PostalCode;
							}
							if (segment.DropoffLocation.Airport != null)
							{
								if (segment.TravelSchedule != null && segment.TravelSchedule != null && segment.TravelSchedule.Flight != null && segment.TravelSchedule.Flight.DepartureDateTime != null)
								{
									summaryReservation.Service.Dropoff.Airline = new Airline { FlightDateTime = segment.TravelSchedule.Flight.DepartureDateTime.ToString() };
								}

								summaryReservation.Service.Dropoff.AirportInfo = new AirportInfo();
								summaryReservation.Service.Dropoff.AirportInfo.Departure = new Arrival_Departure { LocationCode = segment.DropoffLocation.Airport.LocationCode };
							}
						}
						result.SummaryReservation.Add(summaryReservation);
					}
				}
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("GroundOriginalResRetrieve:: {0}", ex.ToString() + ex.StackTrace);

				#region Throw
				throw;
				#endregion
			}
		}

		internal OTA_GroundResRetrieveRS GroundCancelFee(TokenRS tokenVTOD, OTA_GroundResRetrieveRQ input)
		{
			try
			{
				OTA_GroundResRetrieveRS result = null;
				var reservationsController = new UDI.SDS.ReservationsController(tokenVTOD, TrackTime);

				#region Input Conversion
				var sdsRequest = new UDI.SDS.ReservationsService.GetReservationCriteria();
				if (input.ReservationSearchCriteria != null)
				{
					if (input.ReservationSearchCriteria.Passengers != null && input.ReservationSearchCriteria.Passengers.Any())
					{
						var passenger = input.ReservationSearchCriteria.Passengers.First();
						if (passenger.Primary != null)
						{
							#region Get Phone Number
							//if (passenger.Primary.Telephones !=null && passenger.Primary.Telephones.Any())
							//{
							//	var phone = passenger.Primary.Telephones.First();
							//	sdsRequest.PhoneNumber = phone.CountryAccessCode.CleanPhone() + phone.AreaCityCode.CleanPhone() + phone.PhoneNumber.CleanPhone();
							//}
							#endregion

							#region Get Phone
							if (passenger.Primary.PersonName != null)
							{
								sdsRequest.LastName = passenger.Primary.PersonName.Surname;
							}
							#endregion

							#region Get LastName
							if (passenger.Primary.Addresses != null && passenger.Primary.Addresses.Any())
							{
								var address = passenger.Primary.Addresses.First();
								sdsRequest.PostalCode = address.PostalCode;
							}
							#endregion
						}
					}
				}

				#region Get DisptachConfirmation from ConfirmationID (VTODTripID)
				try
				{
					var confirmationID = input.Reference.First().ID.ToInt64();
					using (var db = new DataAccess.VTOD.VTODEntities())
					{
						#region Track
						if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
						#endregion

						var sdsConfirmations = db.SP_SDS_GetConfirmationNumber(confirmationID).ToList();

						#region Track
						if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_DB, "SP_SDS_GetConfirmationNumber");
						#endregion

						//var sds_trips = db.vtod_trip.Where(s => s.Id == confirmationID).ToList();
						if (sdsConfirmations != null && sdsConfirmations.Any())
						{
							//var sdstrip = (sds_trip)sds_trips.First();
							//dispatchConfirmationID = sdstrip.ConfirmationNumber;
							sdsRequest.ConfirmationNumber = sdsConfirmations.First();
						}
					}
				}
				catch (Exception ex)
				{
					logger.Error("GroundCancel", ex);
					throw Common.DTO.VtodException.CreateFieldRequiredValidationException("UniqueID"); //throw new Exception(Messages.VTOD_SDSDomain_GroundCancel_UniqueID);
				}
				#endregion
				#endregion

				var sdsResult = reservationsController.GetCancelFee(sdsRequest);

				#region Output Conversion
				result = new OTA_GroundResRetrieveRS();
				result.Success = new Success();
				result.TPA_Extensions = new TPA_Extensions();

				result.TPA_Extensions.ServiceCharges = new ServiceCharges { Description = sdsResult.Message, Amount = sdsResult.Amount.ToString(), CurrencyCode = "USD" };
				var chargePurpose = new ChargePurpose();
				chargePurpose.Code = Common.DTO.Enum.ChargePurposeValue.CancelFee.ToString();
				chargePurpose.Value = Common.DTO.Enum.ChargePurposeValue.Other_.ToString();
				result.TPA_Extensions.ServiceCharges.ChargePurpose = chargePurpose;


				#region Common
				result.EchoToken = input.EchoToken;
				result.PrimaryLangID = input.PrimaryLangID;
				result.Target = input.Target;
				result.Version = input.Version;
				#endregion
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("GroundCancelFee:: {0}", ex.ToString() + ex.StackTrace);
				throw;

				#region Throw UtilityException
				//throw new UtilityException(Messages.Utility_VehicleGetDefaultVehicleTypeForPoint);
				//throw new SDSException(ex.Message);
				#endregion
			}
		}

		internal OTA_GroundAvailRS GroundCreateAsapRequest(TokenRS tokenVTOD, OTA_GroundAvailRQ groundAvailRQ)
		{
			var tripType = TripType.Unknown;
			OTA_GroundAvailRS result = null;
			int? sdsResult = null;
			tripType = UDI.SDS.Helper.Utility.GetTripType(groundAvailRQ.Service, groundAvailRQ.RateQualifiers.FirstOrDefault());

			#region Init
			var asapController = new UDI.SDS.ASAPController(tokenVTOD, TrackTime);
			var utilityController = new UDI.SDS.UtilityController(tokenVTOD, TrackTime);
			var franchiseController = new UDI.SDS.FranchiseController(tokenVTOD, TrackTime);
			var landmarksController = new UDI.SDS.LandmarksController(tokenVTOD, TrackTime);
			#endregion

			#region Validation
			groundAvailRQ.Validate_GroundCreateAsapRequest();
			#endregion

			int fareID = 0;
			try
			{
				fareID = groundAvailRQ.References.First().ID.ToInt32();
			}
			catch { }

			switch (tripType)
			{
				case TripType.LocationToLocation:
				case TripType.AsDirected:
					{
						#region Generating SDS object
						//result = groundAvail_Charter(tokenVTOD, groundAvailRQ, vtodTXId);
						//AddressRecord dropoffAddress, bool asDirected, string franchiseCode, string fleetType, string SerivceType
						var franchiseCode = groundAvailRQ.Service.ServiceLevel.SourceName;
						var fleetType = groundAvailRQ.Service.VehicleType.Code;
						var SerivceType = groundAvailRQ.Service.ServiceLevel.Code;
						var asDirected = (tripType == TripType.AsDirected) ? true : false;
						int payingPax = 0;
						int childSeats = 0;

						#region PayingPax, FreePax
						if (groundAvailRQ.Passengers != null)
						{
							foreach (var pax in groundAvailRQ.Passengers)
							{
								if (pax.Category != null && pax.Category.Value.ToUpper().Equals("ADULT"))
								{
									payingPax = pax.Quantity;
								}
								else
								{
									childSeats = pax.Quantity;
								}
							}
						}
						#endregion

						AddressRecord dropOffAddress = null;
						AddressRecord pickupAddress = null;                        

                        #region PU address
                        pickupAddress = new AddressRecord();
						var otaPuAddress = groundAvailRQ.Service.Pickup.Address;
						pickupAddress.StreetNumber = otaPuAddress.StreetNmbr;
						pickupAddress.StreetName = otaPuAddress.AddressLine;
						pickupAddress.Municipality = otaPuAddress.CityName;
						pickupAddress.CountryCode = otaPuAddress.CountryName.Code;
						pickupAddress.CountrySubDivision = otaPuAddress.StateProv.StateCode;
						if (!string.IsNullOrWhiteSpace(otaPuAddress.Latitude) && !string.IsNullOrWhiteSpace(otaPuAddress.Longitude))
						{
							pickupAddress.Latitude = otaPuAddress.Latitude.ToDecimal();
							pickupAddress.Longitude = otaPuAddress.Longitude.ToDecimal();
						}
						pickupAddress.PostalCode = otaPuAddress.PostalCode;
						pickupAddress.UnitNumber = otaPuAddress.BldgRoom;

						#region LocationType
						var puLocationType = UDI.SDS.DTO.Enum.Defaults.LocationType;
						puLocationType = landmarksController.GetLandmarksByCoordinates(string.Empty, new UDI.SDS.DTO.Location { Latitude = pickupAddress.Latitude, Longitude = pickupAddress.Longitude }).ToLocationType();
						#endregion

						pickupAddress.LocationType = puLocationType;
						#endregion

						#region DropOff address
						if (tripType == TripType.LocationToLocation)
						{
							dropOffAddress = new AddressRecord();
							var otaDoAddress = groundAvailRQ.Service.Dropoff.Address;
							dropOffAddress.StreetNumber = otaDoAddress.StreetNmbr;
							dropOffAddress.StreetName = otaDoAddress.AddressLine;
							dropOffAddress.Municipality = otaDoAddress.CityName;
							dropOffAddress.CountryCode = otaDoAddress.CountryName.Code;
							dropOffAddress.CountrySubDivision = otaDoAddress.StateProv.StateCode;
							if (!string.IsNullOrWhiteSpace(otaDoAddress.Latitude) && !string.IsNullOrWhiteSpace(otaDoAddress.Longitude))
							{
								dropOffAddress.Latitude = otaDoAddress.Latitude.ToDecimal();
								dropOffAddress.Longitude = otaDoAddress.Longitude.ToDecimal();
							}
							dropOffAddress.PostalCode = otaDoAddress.PostalCode;
							dropOffAddress.UnitNumber = otaDoAddress.BldgRoom;

							#region LocationType
							var doLocationType = UDI.SDS.DTO.Enum.Defaults.LocationType;
							doLocationType = landmarksController.GetLandmarksByCoordinates(string.Empty, new UDI.SDS.DTO.Location { Latitude = dropOffAddress.Latitude, Longitude = dropOffAddress.Longitude }).ToLocationType();
							#endregion

							dropOffAddress.LocationType = doLocationType;
						}
						#endregion


						var charterAsapRequest = new CharterAsapRequest();
						charterAsapRequest.DropoffAddress = dropOffAddress;
						charterAsapRequest.GuestAddress = pickupAddress;
						charterAsapRequest.DropoffAsDirected = asDirected;
						charterAsapRequest.FranchiseCode = franchiseCode;
						charterAsapRequest.Fleet = fleetType;
						charterAsapRequest.ServiceTypeCode = SerivceType;
						charterAsapRequest.AirportCode = UDI.SDS.DTO.Enum.Defaults.CharterAirport;
						charterAsapRequest.TripDirection = 2;
						charterAsapRequest.FareID = fareID;
						charterAsapRequest.PayingPax = payingPax.ToByte();
						charterAsapRequest.FreePax = childSeats.ToByte();
						charterAsapRequest.UserID = tokenVTOD.Username;
						charterAsapRequest.PickupTime = groundAvailRQ.Service.Pickup.DateTime.ToDateTime().Value;
                        if (groundAvailRQ.Service.Dropoff != null && groundAvailRQ.Service.Dropoff.Airline != null && !string.IsNullOrEmpty(groundAvailRQ.Service.Dropoff.Airline.FlightDateTime))
                        {
                            charterAsapRequest.TravelTime = groundAvailRQ.Service.Dropoff.Airline.FlightDateTime.ToDateTime().Value;
                        }
                        else if (groundAvailRQ.Service.Pickup != null && groundAvailRQ.Service.Pickup.Airline != null && !string.IsNullOrEmpty(groundAvailRQ.Service.Pickup.Airline.FlightDateTime))
                        {
                            charterAsapRequest.TravelTime = groundAvailRQ.Service.Dropoff.Airline.FlightDateTime.ToDateTime().Value;
                        }
                        else
                        {
                            charterAsapRequest.TravelTime = groundAvailRQ.Service.Pickup.DateTime.ToDateTime().Value;
                        }
						#endregion

						sdsResult = asapController.CreatCharterASAPRequest(charterAsapRequest);
					}
					break;
				case TripType.Airport:
					{
						#region init
						var airportAsapRequest = new RequestDetails();
						Address otaAddress = null;
						AirportInfo airportInfo = null;
						Airline airline = null;
						var tripDirection = TripDirection.Unknown;
						var accessibleServiceRequired = false;
						int payingPax = 0;
						int childSeats = 0;
						var addressLocationType = UDI.SDS.DTO.Enum.Defaults.LocationType;
						UDI.SDS.LandmarksService.LandmarkRecord landmarkRecord = null;
						var airportCode = string.Empty;
						#endregion

						#region Generating SDS object

						#region PayingPax, FreePax
						if (groundAvailRQ.Passengers != null)
						{
							foreach (var pax in groundAvailRQ.Passengers)
							{
								if (pax.Category != null && pax.Category.Value.ToUpper().Equals("ADULT"))
								{
									payingPax = pax.Quantity;
								}
								else
								{
									childSeats = pax.Quantity;
								}
							}
						}
						#endregion

						#region Location Address
						var sdsObjectFactory = new SDSObjectFactory();
						var location = new UDI.SDS.DTO.Location();

                        if (groundAvailRQ.Service.Dropoff.Airline != null || groundAvailRQ.Service.Dropoff.AirportInfo != null)
                        {
                            otaAddress = groundAvailRQ.Service.Pickup.Address;
                            airportInfo = groundAvailRQ.Service.Dropoff.AirportInfo;
                            airportCode = (airportInfo.Arrival != null && !string.IsNullOrEmpty(airportInfo.Arrival.LocationCode)) ? airportInfo.Arrival.LocationCode : airportInfo.Departure.LocationCode;
                            airline = groundAvailRQ.Service.Dropoff.Airline;
                            tripDirection = TripDirection.Inbound;
                            location = sdsObjectFactory.GetLocationObject(groundAvailRQ.Service.Pickup, LocationMode.Pickup);
                        }
                        else
                        {
                            otaAddress = groundAvailRQ.Service.Dropoff.Address;
                            airportInfo = groundAvailRQ.Service.Pickup.AirportInfo;
                            airportCode = (airportInfo.Arrival != null && !string.IsNullOrWhiteSpace(airportInfo.Arrival.LocationCode)) ? airportInfo.Arrival.LocationCode : airportInfo.Departure.LocationCode;
                            airline = groundAvailRQ.Service.Pickup.Airline;
                            tripDirection = TripDirection.Outbound;
                            location = sdsObjectFactory.GetLocationObject(groundAvailRQ.Service.Dropoff, LocationMode.Dropoff);
                        }
						var address = new AddressRecord();
						address.StreetNumber = otaAddress.StreetNmbr;
						address.StreetName = otaAddress.AddressLine;
						address.Municipality = otaAddress.CityName;
						address.CountryCode = otaAddress.CountryName.Code;
						address.CountrySubDivision = otaAddress.StateProv.StateCode;
						address.Latitude = otaAddress.Latitude.ToDecimal();
						address.Longitude = otaAddress.Longitude.ToDecimal();
						address.PostalCode = otaAddress.PostalCode;
						address.UnitNumber = otaAddress.BldgRoom;

						#region LocationType
						if (address.Latitude == 0 || address.Longitude == 0)
						{
							landmarkRecord = landmarksController.GetLandmarkRecordByAddress(airportCode, location);
						}
						else
						{
							landmarkRecord = landmarksController.GetLandmarksByCoordinates(airportCode, location);
						}

						addressLocationType = landmarkRecord.ToLocationType();
						address.LocationType = addressLocationType;
						#endregion

						airportAsapRequest.GuestAddress = address;
						#endregion


						if (groundAvailRQ.DisabilityInfo != null)
						{
							accessibleServiceRequired = groundAvailRQ.DisabilityInfo.RequiredInd;
						}

						airportAsapRequest.AccessibleServiceRequired = accessibleServiceRequired;
						airportAsapRequest.AirportCode = airportCode;
						airportAsapRequest.AgentComments = string.Empty;
						airportAsapRequest.ConfirmationNumber = string.Empty;
						airportAsapRequest.PayingPax = payingPax.ToByte();
						airportAsapRequest.FreePax = childSeats.ToByte();
						airportAsapRequest.PickupTime = groundAvailRQ.Service.Pickup.DateTime.ToDateTime().Value;
						airportAsapRequest.SpecialHandling = false;
						airportAsapRequest.TravelTime = airline.FlightDateTime.ToDateTime().Value;
						airportAsapRequest.TripDirection = (int)tripDirection;
						airportAsapRequest.UserID = tokenVTOD.Username;
						airportAsapRequest.FareID = fareID;
						#endregion

						sdsResult = asapController.CreatAirportASAPRequest(airportAsapRequest);
					}
					break;
				case TripType.Unknown:
					throw VtodException.CreateException(ExceptionType.SDS, 1006);//throw new Exception(string.Format(Messages.UDI_SDS_TripType, "GroundAvail"));
			}

			#region Response Conversion
			result = new OTA_GroundAvailRS();
			result.Success = new Success();
			result.GroundServices = new GroundServiceList();
			result.GroundServices.GroundServices = new List<GroundService>();
			var groundService = new GroundService();
			groundService.Reference = new Reference();
			groundService.Reference.ID = sdsResult.ToString();
			result.GroundServices.GroundServices.Add(groundService);

			#region Common
			result.EchoToken = groundAvailRQ.EchoToken;
			result.PrimaryLangID = groundAvailRQ.PrimaryLangID;
			result.Target = groundAvailRQ.Target;
			result.Version = groundAvailRQ.Version;
			#endregion
			#endregion

			return result;
		}

		internal GroundGetASAPRequestStatusRS GroundGetASAPRequestStatus(TokenRS token, GroundGetASAPRequestStatusRQ input)
		{
			try
			{
				GroundGetASAPRequestStatusRS result = null;
				var asapController = new UDI.SDS.ASAPController(token, TrackTime);

				var sdsResult = asapController.GetASAPRequestStatus(input.Reference.ID.ToInt32());

				#region Output Conversion
				result = new GroundGetASAPRequestStatusRS();
				result.ApprovedPickupTime = sdsResult.ApprovedPickupTime;
				result.DispositionCode = sdsResult.DispositionCode.ToString();
				result.Guaranteed = sdsResult.ServiceIsAOR;
				result.Success = new Success();

				#region Common
				result.EchoToken = input.EchoToken;
				result.PrimaryLangID = input.PrimaryLangID;
				result.Target = input.Target;
				result.Version = input.Version;
				#endregion
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("GroundGetASAPRequestStatus:: {0}", ex.ToString() + ex.StackTrace);

				#region Throw MembershipException
				throw;
				#endregion
			}
		}

		internal GroundAbandonASAPRequestRS GroundAbandonASAPRequest(TokenRS token, GroundAbandonASAPRequestRQ input)
		{
			try
			{
				GroundAbandonASAPRequestRS result = null;
				var asapController = new UDI.SDS.ASAPController(token, TrackTime);

				asapController.AbandonRequest(token.Username, input.Reference.ID.ToInt32());

				#region Output Conversion
				result = new GroundAbandonASAPRequestRS();
				result.Success = new Success();

				#region Common
				result.EchoToken = input.EchoToken;
				result.PrimaryLangID = input.PrimaryLangID;
				result.Target = input.Target;
				result.Version = input.Version;
				#endregion
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("GroundGetASAPRequestStatus:: {0}", ex.ToString() + ex.StackTrace);

				#region Throw MembershipException
				throw;
				#endregion
			}
		}

		internal OTA_GroundAvailRS GetVehicleInfo(TokenRS token, OTA_GroundAvailRQ request)
		{
			try
			{
				OTA_GroundAvailRS result = null;
				var vehiclesController = new UDI.SDS.VehiclesController(token, TrackTime);

				#region Validation
				request.ValidateGetVehicleInfoRequest();
				#endregion

				#region Get VehcileType
				var vehicleType = UDI.VTOD.Common.DTO.Enum.VehicleType.Unknown;

				if (request.RateQualifiers.First().RateQualifierValue.ToLower() == "supershuttle")
				{
					vehicleType = UDI.VTOD.Common.DTO.Enum.VehicleType.Shuttle;
				}
				if (request.RateQualifiers.First().RateQualifierValue.ToLower() == "execucar")
				{
					vehicleType = UDI.VTOD.Common.DTO.Enum.VehicleType.ExecuCar;
				}
				if (request.RateQualifiers.First().RateQualifierValue.ToLower() == "taxi")
				{
					vehicleType = UDI.VTOD.Common.DTO.Enum.VehicleType.Taxi;
				}
				#endregion

				if (vehicleType != UDI.VTOD.Common.DTO.Enum.VehicleType.Unknown)
				{
					var lat = request.Service.Pickup.Address.Latitude.ToDecimal();
					var lon = request.Service.Pickup.Address.Longitude.ToDecimal();

					var sdsResult = vehiclesController.GetSDSVehicleInfo(lat, lon, vehicleType);

					#region Output Conversion
					result = new OTA_GroundAvailRS();
					result.Success = new Success();
					result.TPA_Extensions = new TPA_Extensions();
					result.TPA_Extensions.Vehicles = new Vehicles();
					result.TPA_Extensions.Vehicles.Items = new List<Vehicle>();
                    Random rnd = new Random();
					foreach (var item in sdsResult.VehicleLocations)
					{
						var vehicle = new Vehicle();
						vehicle.Geolocation = new Geolocation();
						vehicle.Geolocation.Lat = item.Latitude.ToDecimal();
						vehicle.Geolocation.Long = item.Longitude.ToDecimal();
                        if (item.Course != 0)
                            vehicle.Course = item.Course.ToDecimal();
                        else
                            vehicle.Course = rnd.Next(1, 359);
						vehicle.VehicleStatus = "AVAILABLE";
						vehicle.ID = item.VehicleNumber.ToString();
						result.TPA_Extensions.Vehicles.Items.Add(vehicle);
					}

					if (vehicleType == UDI.VTOD.Common.DTO.Enum.VehicleType.Shuttle)
					{
						result.TPA_Extensions.Vehicles.NearestVehicleETA = sdsResult.MinutesToClosestShuttle.ToString();
					}
					else if (vehicleType == UDI.VTOD.Common.DTO.Enum.VehicleType.ExecuCar)
					{
						result.TPA_Extensions.Vehicles.NearestVehicleETA = sdsResult.MinutesToClosestSedan.ToString();
					}
					else if (vehicleType == UDI.VTOD.Common.DTO.Enum.VehicleType.Taxi)
					{
						result.TPA_Extensions.Vehicles.NearestVehicleETA = sdsResult.MinutesToClosestTaxi.ToString();
					}

					result.TPA_Extensions.Vehicles.NumberOfVehicles = sdsResult.VehicleLocations.Count().ToString();

					#region Common
					result.EchoToken = request.EchoToken;
					result.PrimaryLangID = request.PrimaryLangID;
					result.Target = request.Target;
					result.Version = request.Version;
					#endregion

					#endregion
				}
				else
				{
					throw Common.DTO.VtodException.CreateFieldRequiredValidationException("RateQualifierValue");
				}

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("GroundGetASAPRequestStatus:: {0}", ex.ToString() + ex.StackTrace);

				#region Throw MembershipException
				throw;
				#endregion
			}
		}
		internal GroundGetPickupTimeRS GetPickupTime(TokenRS token, GroundGetPickupTimeRQ input)
		{
			try
			{
				GroundGetPickupTimeRS result = null;
				var reservationController = new UDI.SDS.ReservationsController(token, TrackTime);

				#region Input Conversion

				var isAccessibleServiceRequired = input.DisabilityInfo != null ? input.DisabilityInfo.RequiredInd.ToBool() : false;
				var hotelConciergeID = 0;
				var isInternationalFlight = string.IsNullOrEmpty(input.CodeContext) ? false : (input.CodeContext.ToLower().Trim() == "international" ? true : false);
				var masterLandmarkID = 0;
				var vtodDirection = input.TripDirection.ToTripDirection();
				var sdsDirection = Direction.Inbound;
				switch (vtodDirection)
				{
					case TripDirection.Inbound:
						sdsDirection = Direction.Inbound;
						break;
					case TripDirection.Outbound:
						sdsDirection = Direction.Outbound;

						break;
				}


				var serviceInfo = new ServiceInfo { FareID = input.FareID.ToInt32(), Direction = sdsDirection };
				var travelTime = input.TravelTime.ToDateTime();
				var airportCode = string.Empty;
				var childSeat = 0;
				var payingPax = 0;
				#endregion

				var sdsResult = reservationController.GetPickupTimes(isAccessibleServiceRequired, hotelConciergeID, isInternationalFlight, masterLandmarkID, serviceInfo, travelTime.ToDateTime().Value, airportCode, childSeat, payingPax);

				#region Output Conversion
				result = new GroundGetPickupTimeRS();
				result.Success = new Success();
				result.PickupTimes = new PickupTimeList();

				if (sdsResult != null && sdsResult.Any())
				{
					result.PickupTimes = new PickupTimeList();
					result.PickupTimes.PickupTimes = new List<PickupTime>();
					foreach (UDI.SDS.ReservationsService.PickupTimeRecord item in sdsResult)
					{
						var put = new PickupTime { EndDateTime = item.EndTime.ToString(), StartDateTime = item.StartTime.ToString(), Status = item.Status.ToString() };
						result.PickupTimes.PickupTimes.Add(put);
					}
				}

				#region Common
				result.EchoToken = input.EchoToken;
				result.PrimaryLangID = input.PrimaryLangID;
				result.Target = input.Target;
				result.Version = input.Version;
				#endregion
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("MembershipDomain.GetPickupTime:: {0}", ex.ToString() + ex.StackTrace);

				#region Throw MembershipException
				throw VtodException.CreateException(ExceptionType.Membership, 7004);//throw new MembershipException(Messages.Membership_MemberAddLocation);
				#endregion
			}
		}

		internal GroundGetFareScheduleWithFeesRS GroundGetFareScheduleWithFees(TokenRS token, GroundGetFareScheduleWithFeesRQ input)
		{
			try
			{
				GroundGetFareScheduleWithFeesRS result = null;
				var rateController = new UDI.SDS.RateController(token, TrackTime);

				#region Input Conversion
				var sdsInput = new FareScheduleRequestWithFees();
				sdsInput.CurrencyID = (int)input.POS.Source.ISOCurrency.ToCurrency();
				sdsInput.DiscountCode = input.PromotionCode;
				sdsInput.GroupID = input.GroupID;
				sdsInput.InboundFareID = input.InboundFareID;
				if (!string.IsNullOrWhiteSpace(input.InboundTravelDate))
				{
					sdsInput.InboundTravelDate = input.InboundTravelDate.ToDateTime().Value;
				}
				sdsInput.OutboundFareID = input.OutboundFareID;
				if (!string.IsNullOrWhiteSpace(input.OutboundTravelDate))
				{
					sdsInput.OutboundTravelDate = input.OutboundTravelDate.ToDateTime().Value;
				}
				sdsInput.PayingPax = input.PayingPax;
				sdsInput.PaymentType = (byte)input.PaymentType.ToSDSPaymentType();

				#endregion

				var sdsResult = rateController.GetFareScheduleWithFees(sdsInput);

				#region Output Conversion
				result = new GroundGetFareScheduleWithFeesRS();
				result.Success = new Success();
				result.GroundServices = new GroundServiceList();
				result.GroundServices.GroundServices = new List<GroundService>();


				if (sdsResult != null)
				{
					#region Inbound
					if (sdsResult.InboundTotal > 0)
					{
						var inboundGroundService = new GroundService();
						inboundGroundService.Reference = new Reference();
						inboundGroundService.Reference.TPA_Extensions = new TPA_Extensions();
						inboundGroundService.ServiceCharges = new List<ServiceCharges>();
						inboundGroundService.Fees = new List<Fees>();

						#region Init
						ServiceCharges servCharges = null;
						decimal totalCharge = 0;
						Fees fees = null;
						#endregion

						#region Persons Fees
						#region First Person Fare

						servCharges = new ServiceCharges();
						servCharges.Description = Description.FirstPassengerFare.ToString();
						servCharges.ChargePurpose = new Common.DTO.OTA.ChargePurpose();
						servCharges.ChargePurpose.Value = UDI.VTOD.Common.DTO.Enum.ChargePurposeValue.BaseRate.ToString();

						servCharges.Calculations = new List<Calculation>();
						Calculation cal = new Calculation();
						cal.UnitCharge = System.Math.Round(sdsResult.InboundFirstPaxFare, 2);
						cal.Quantity = 1;
						cal.MaxQuantity = 1;
						cal.Total = cal.UnitCharge;
						servCharges.Calculations.Add(cal);

						totalCharge += cal.Total;

						inboundGroundService.ServiceCharges.Add(servCharges);

						#endregion

						#region Addtional Persons Fare

						servCharges = new ServiceCharges();

						servCharges.Description = Description.AdditionalPassengers.ToString();
						servCharges.ChargePurpose = new ChargePurpose();
						servCharges.ChargePurpose.Value = UDI.VTOD.Common.DTO.Enum.ChargePurposeValue.AdditionalPassengers.ToString();

						servCharges.Calculations = new List<Calculation>();
						cal = new Calculation();
						cal.Quantity = input.PayingPax - 1;
						cal.Total = System.Math.Round(sdsResult.OutboundAdditionalPaxFare, 2);//cal.UnitCharge * cal.Quantity;
						cal.UnitCharge = System.Math.Round(cal.Total / cal.Quantity, 2);

						servCharges.Calculations.Add(cal);

						totalCharge += cal.Total;

						inboundGroundService.ServiceCharges.Add(servCharges);

						#endregion
						#endregion

						#region Other Fees
						#region Fixed List
						#region Fuel Surcharge Fee
						if (input.InboundFareID != 0)
						{
							if (sdsResult.InboundFuelSurcharge > 0)
							{
								fees = new Fees();
								fees.Description = Description.FuelSurchargeFee.ToString();
								fees.Calculations = new List<Calculation>();
								cal = new Calculation();
								cal.UnitCharge = System.Math.Round(sdsResult.InboundFuelSurcharge, 2);
								cal.Quantity = 1;//At this time, the rates web service returns the calculated fuel surcharge. So the quantity would always be one (regardless of how that fee is calculated).
								cal.MaxQuantity = 1;
								cal.Total = cal.UnitCharge * cal.Quantity;
								fees.Calculations.Add(cal);

								totalCharge += cal.Total;

								inboundGroundService.Fees.Add(fees);
							}
						}
						else if (input.OutboundFareID != 0)
						{
							if (sdsResult.OutboundFuelSurcharge > 0)
							{
								fees = new Fees();
								fees.Description = Description.FuelSurchargeFee.ToString();
								fees.Calculations = new List<Calculation>();
								cal = new Calculation();
								cal.UnitCharge = System.Math.Round(sdsResult.OutboundFuelSurcharge, 2);
								cal.Quantity = 1;//At this time, the rates web service returns the calculated fuel surcharge. So the quantity would always be one (regardless of how that fee is calculated).
								cal.MaxQuantity = 1;
								cal.Total = cal.UnitCharge * cal.Quantity;
								fees.Calculations.Add(cal);

								totalCharge += cal.Total;

								inboundGroundService.Fees.Add(fees);
							}
						}

						#endregion

						#region Discount
						if (input.InboundFareID != 0)
						{
							if (sdsResult.InboundDiscount > 0)
							{
								fees = new Fees();
								fees.Description = Description.Discount.ToString();
								fees.ChargePurpose = new ChargePurpose();
								fees.ChargePurpose.Value = UDI.VTOD.Common.DTO.Enum.ChargePurposeValue.Discount.ToString();

								fees.Calculations = new List<Calculation>();
								cal = new Calculation();
								cal.UnitCharge = System.Math.Round(sdsResult.InboundDiscount, 2);
								cal.Quantity = 1;
								cal.MaxQuantity = 1;
								cal.Total = cal.UnitCharge * cal.Quantity;
								fees.Calculations.Add(cal);
								inboundGroundService.Fees.Add(fees);
							}
						}
						else if (input.OutboundFareID != 0)
						{
							if (sdsResult.OutboundDiscount > 0)
							{
								fees = new Fees();
								fees.Description = Description.Discount.ToString();
								fees.ChargePurpose = new ChargePurpose();
								fees.ChargePurpose.Value = UDI.VTOD.Common.DTO.Enum.ChargePurposeValue.Discount.ToString();

								fees.Calculations = new List<Calculation>();
								cal = new Calculation();
								cal.UnitCharge = System.Math.Round(sdsResult.OutboundDiscount, 2);
								cal.Quantity = 1;
								cal.MaxQuantity = 1;
								cal.Total = cal.UnitCharge * cal.Quantity;
								fees.Calculations.Add(cal);
								inboundGroundService.Fees.Add(fees);
							}
						}
						#endregion

						#region Gratuity
						if (input.InboundFareID != 0)
						{
							if (sdsResult.InboundGratuity > 0)
							{
								fees = new Fees();
								fees.Description = Description.Gratuity.ToString();
								fees.ChargePurpose = new ChargePurpose();
								fees.ChargePurpose.Value = UDI.VTOD.Common.DTO.Enum.ChargePurposeValue.Gratuity.ToString();

								fees.Calculations = new List<Calculation>();
								cal = new Calculation();
								cal.UnitCharge = System.Math.Round(sdsResult.InboundGratuity, 2);
								cal.Quantity = 1;
								cal.MaxQuantity = 1;
								cal.Total = cal.UnitCharge * cal.Quantity;
								fees.Calculations.Add(cal);

								totalCharge += cal.Total;

								inboundGroundService.Fees.Add(fees);
							}
						}
						else if (input.OutboundFareID != 0)
						{
							if (sdsResult.OutboundGratuity > 0)
							{
								fees = new Fees();
								fees.Description = Description.Gratuity.ToString();
								fees.ChargePurpose = new ChargePurpose();
								fees.ChargePurpose.Value = UDI.VTOD.Common.DTO.Enum.ChargePurposeValue.Gratuity.ToString();

								fees.Calculations = new List<Calculation>();
								cal = new Calculation();
								cal.UnitCharge = System.Math.Round(sdsResult.OutboundGratuity, 2);
								cal.Quantity = 1;
								cal.MaxQuantity = 1;
								cal.Total = cal.UnitCharge * cal.Quantity;
								fees.Calculations.Add(cal);

								totalCharge += cal.Total;

								inboundGroundService.Fees.Add(fees);
							}
						}

						#endregion
						#endregion

						#region Flexible List
						#region CompanyFeesList
						if (input.InboundFareID != 0)
						{
							if (sdsResult.InboundCompanyFeesList != null && sdsResult.InboundCompanyFeesList.Any())
							{
								foreach (var fee in sdsResult.InboundCompanyFeesList)
								{
									fees = new Fees();
									fees.Description = fee.FeeType;
									fees.Calculations = new List<Calculation>();
									cal = new Calculation();
									cal.UnitCharge = System.Math.Round(fee.Amount, 2);
									cal.Quantity = 1;
									cal.MaxQuantity = 1;
									cal.Total = cal.UnitCharge * cal.Quantity;
									fees.Calculations.Add(cal);

									totalCharge += cal.Total;

									inboundGroundService.Fees.Add(fees);
								}
							}
						}
						else if (input.OutboundFareID != 0)
						{
							if (sdsResult.OutboundCompanyFeesList != null && sdsResult.OutboundCompanyFeesList.Any())
							{
								foreach (var fee in sdsResult.OutboundCompanyFeesList)
								{
									fees = new Fees();
									fees.Description = fee.FeeType;
									fees.Calculations = new List<Calculation>();
									cal = new Calculation();
									cal.UnitCharge = System.Math.Round(fee.Amount, 2);
									cal.Quantity = 1;
									cal.MaxQuantity = 1;
									cal.Total = cal.UnitCharge * cal.Quantity;
									fees.Calculations.Add(cal);

									totalCharge += cal.Total;

									inboundGroundService.Fees.Add(fees);
								}
							}
						}
						#endregion

						#region DriverFeesList
						if (input.InboundFareID != 0)
						{

							if (sdsResult.InboundDriverFeesList != null && sdsResult.InboundDriverFeesList.Any())
							{
								foreach (var fee in sdsResult.InboundDriverFeesList)
								{
									fees = new Fees();
									fees.Description = fee.FeeType;
									fees.Calculations = new List<Calculation>();
									cal = new Calculation();
									cal.UnitCharge = System.Math.Round(fee.Amount, 2);
									cal.Quantity = 1;
									cal.MaxQuantity = 1;
									cal.Total = cal.UnitCharge * cal.Quantity;
									fees.Calculations.Add(cal);

									totalCharge += cal.Total;

									inboundGroundService.Fees.Add(fees);
								}
							}
						}
						else if (input.OutboundFareID != 0)
						{
							if (sdsResult.OutboundDriverFeesList != null && sdsResult.OutboundDriverFeesList.Any())
							{
								foreach (var fee in sdsResult.OutboundDriverFeesList)
								{
									fees = new Fees();
									fees.Description = fee.FeeType;
									fees.Calculations = new List<Calculation>();
									cal = new Calculation();
									cal.UnitCharge = System.Math.Round(fee.Amount, 2);
									cal.Quantity = 1;
									cal.MaxQuantity = 1;
									cal.Total = cal.UnitCharge * cal.Quantity;
									fees.Calculations.Add(cal);

									totalCharge += cal.Total;

									inboundGroundService.Fees.Add(fees);
								}
							}

						}
						#endregion
						#endregion
						#endregion

						#region TotalCharge
						inboundGroundService.TotalCharge = new TotalCharge();
						//Doug asked us to use serviceInfo.Total instead of we sum them up for fixing VAT, NYC, ... issues. 2014_06_26 (Pouyan Paryas)
						//groundService.TotalCharge.RateTotalAmount = totalCharge;
						if (input.InboundFareID != 0)
						{
							inboundGroundService.TotalCharge.RateTotalAmount = System.Math.Round(sdsResult.InboundTotal, 2);
							inboundGroundService.TotalCharge.EstimatedTotalAmount = System.Math.Round(sdsResult.InboundTotal, 2);
						}
						else if (input.OutboundFareID != 0)
						{

							inboundGroundService.TotalCharge.RateTotalAmount = System.Math.Round(sdsResult.OutboundTotal, 2);
							inboundGroundService.TotalCharge.EstimatedTotalAmount = System.Math.Round(sdsResult.OutboundTotal, 2);
						}
						#endregion

						#region Reference
						if (input.InboundFareID != 0)
						{
							inboundGroundService.Reference.ID = input.InboundFareID.ToString();
						}
						else if (input.OutboundFareID != 0)
						{
							inboundGroundService.Reference.ID = input.OutboundFareID.ToString();
						}
						#endregion

						#region DiscountDescription (Status)
						inboundGroundService.Reference.TPA_Extensions.DiscountDescription = sdsResult.DiscountName;
						#endregion

						#region TripDirection
						inboundGroundService.Reference.TPA_Extensions.TripDirection = Common.DTO.Enum.TripDirection.Inbound.ToString();
						#endregion

						result.GroundServices.GroundServices.Add(inboundGroundService);
					}

					#endregion

					#region Outbound
					if (sdsResult.OutboundTotal > 0)
					{
						var outboundGroundService = new GroundService();
						outboundGroundService.Reference = new Reference();
						outboundGroundService.Reference.TPA_Extensions = new TPA_Extensions();
						outboundGroundService.ServiceCharges = new List<ServiceCharges>();
						outboundGroundService.Fees = new List<Fees>();

						#region Init
						ServiceCharges servCharges = null;
						decimal totalCharge = 0;
						Fees fees = null;
						#endregion

						#region Persons Fees
						#region First Person Fare

						servCharges = new ServiceCharges();
						servCharges.Description = Description.FirstPassengerFare.ToString();
						servCharges.ChargePurpose = new Common.DTO.OTA.ChargePurpose();
						servCharges.ChargePurpose.Value = UDI.VTOD.Common.DTO.Enum.ChargePurposeValue.BaseRate.ToString();

						servCharges.Calculations = new List<Calculation>();
						Calculation cal = new Calculation();
						cal.UnitCharge = System.Math.Round(sdsResult.InboundFirstPaxFare, 2);
						cal.Quantity = 1;
						cal.MaxQuantity = 1;
						cal.Total = cal.UnitCharge;
						servCharges.Calculations.Add(cal);

						totalCharge += cal.Total;

						outboundGroundService.ServiceCharges.Add(servCharges);

						#endregion

						#region Addtional Persons Fare

						servCharges = new ServiceCharges();

						servCharges.Description = Description.AdditionalPassengers.ToString();
						servCharges.ChargePurpose = new ChargePurpose();
						servCharges.ChargePurpose.Value = UDI.VTOD.Common.DTO.Enum.ChargePurposeValue.AdditionalPassengers.ToString();

						servCharges.Calculations = new List<Calculation>();
						cal = new Calculation();
						cal.Quantity = input.PayingPax - 1;
						cal.Total = System.Math.Round(sdsResult.OutboundAdditionalPaxFare, 2);//cal.UnitCharge * cal.Quantity;
						cal.UnitCharge = System.Math.Round(cal.Total / cal.Quantity, 2);


						//servCharges.Calculations = new List<Calculation>();
						//cal = new Calculation();
						//cal.UnitCharge = System.Math.Round(sdsResult.OutboundAdditionalPaxFare, 2);
						//cal.Quantity = input.PayingPax - 1;
						//cal.Total = cal.UnitCharge * cal.Quantity;
						servCharges.Calculations.Add(cal);

						totalCharge += cal.Total;

						outboundGroundService.ServiceCharges.Add(servCharges);

						#endregion
						#endregion

						#region Other Fees
						#region Fixed List
						#region Fuel Surcharge Fee
						if (input.InboundFareID != 0)
						{
							if (sdsResult.InboundFuelSurcharge > 0)
							{
								fees = new Fees();
								fees.Description = Description.FuelSurchargeFee.ToString();
								fees.Calculations = new List<Calculation>();
								cal = new Calculation();
								cal.UnitCharge = System.Math.Round(sdsResult.InboundFuelSurcharge, 2);
								cal.Quantity = 1;//At this time, the rates web service returns the calculated fuel surcharge. So the quantity would always be one (regardless of how that fee is calculated).
								cal.MaxQuantity = 1;
								cal.Total = cal.UnitCharge * cal.Quantity;
								fees.Calculations.Add(cal);

								totalCharge += cal.Total;

								outboundGroundService.Fees.Add(fees);
							}
						}
						else if (input.OutboundFareID != 0)
						{
							if (sdsResult.OutboundFuelSurcharge > 0)
							{
								fees = new Fees();
								fees.Description = Description.FuelSurchargeFee.ToString();
								fees.Calculations = new List<Calculation>();
								cal = new Calculation();
								cal.UnitCharge = System.Math.Round(sdsResult.OutboundFuelSurcharge, 2);
								cal.Quantity = 1;//At this time, the rates web service returns the calculated fuel surcharge. So the quantity would always be one (regardless of how that fee is calculated).
								cal.MaxQuantity = 1;
								cal.Total = cal.UnitCharge * cal.Quantity;
								fees.Calculations.Add(cal);

								totalCharge += cal.Total;

								outboundGroundService.Fees.Add(fees);
							}
						}

						#endregion

						#region Discount
						if (input.InboundFareID != 0)
						{
							if (sdsResult.InboundDiscount > 0)
							{
								fees = new Fees();
								fees.Description = Description.Discount.ToString();
								fees.ChargePurpose = new ChargePurpose();
								fees.ChargePurpose.Value = UDI.VTOD.Common.DTO.Enum.ChargePurposeValue.Discount.ToString();

								fees.Calculations = new List<Calculation>();
								cal = new Calculation();
								cal.UnitCharge = System.Math.Round(sdsResult.InboundDiscount, 2);
								cal.Quantity = 1;
								cal.MaxQuantity = 1;
								cal.Total = cal.UnitCharge * cal.Quantity;
								fees.Calculations.Add(cal);
								outboundGroundService.Fees.Add(fees);
							}
						}
						else if (input.OutboundFareID != 0)
						{
							if (sdsResult.OutboundDiscount > 0)
							{
								fees = new Fees();
								fees.Description = Description.Discount.ToString();
								fees.ChargePurpose = new ChargePurpose();
								fees.ChargePurpose.Value = UDI.VTOD.Common.DTO.Enum.ChargePurposeValue.Discount.ToString();

								fees.Calculations = new List<Calculation>();
								cal = new Calculation();
								cal.UnitCharge = System.Math.Round(sdsResult.OutboundDiscount, 2);
								cal.Quantity = 1;
								cal.MaxQuantity = 1;
								cal.Total = cal.UnitCharge * cal.Quantity;
								fees.Calculations.Add(cal);
								outboundGroundService.Fees.Add(fees);
							}
						}
						#endregion

						#region Gratuity
						if (input.InboundFareID != 0)
						{
							if (sdsResult.InboundGratuity > 0)
							{
								fees = new Fees();
								fees.Description = Description.Gratuity.ToString();
								fees.ChargePurpose = new ChargePurpose();
								fees.ChargePurpose.Value = UDI.VTOD.Common.DTO.Enum.ChargePurposeValue.Gratuity.ToString();

								fees.Calculations = new List<Calculation>();
								cal = new Calculation();
								cal.UnitCharge = System.Math.Round(sdsResult.InboundGratuity, 2);
								cal.Quantity = 1;
								cal.MaxQuantity = 1;
								cal.Total = cal.UnitCharge * cal.Quantity;
								fees.Calculations.Add(cal);

								totalCharge += cal.Total;

								outboundGroundService.Fees.Add(fees);
							}
						}
						else if (input.OutboundFareID != 0)
						{
							if (sdsResult.OutboundGratuity > 0)
							{
								fees = new Fees();
								fees.Description = Description.Gratuity.ToString();
								fees.ChargePurpose = new ChargePurpose();
								fees.ChargePurpose.Value = UDI.VTOD.Common.DTO.Enum.ChargePurposeValue.Gratuity.ToString();

								fees.Calculations = new List<Calculation>();
								cal = new Calculation();
								cal.UnitCharge = System.Math.Round(sdsResult.OutboundGratuity, 2);
								cal.Quantity = 1;
								cal.MaxQuantity = 1;
								cal.Total = cal.UnitCharge * cal.Quantity;
								fees.Calculations.Add(cal);

								totalCharge += cal.Total;

								outboundGroundService.Fees.Add(fees);
							}
						}

						#endregion
						#endregion

						#region Flexible List
						#region CompanyFeesList
						if (input.InboundFareID != 0)
						{
							if (sdsResult.InboundCompanyFeesList != null && sdsResult.InboundCompanyFeesList.Any())
							{
								foreach (var fee in sdsResult.InboundCompanyFeesList)
								{
									fees = new Fees();
									fees.Description = fee.FeeType;
									fees.Calculations = new List<Calculation>();
									cal = new Calculation();
									cal.UnitCharge = System.Math.Round(fee.Amount, 2);
									cal.Quantity = 1;
									cal.MaxQuantity = 1;
									cal.Total = cal.UnitCharge * cal.Quantity;
									fees.Calculations.Add(cal);

									totalCharge += cal.Total;

									outboundGroundService.Fees.Add(fees);
								}
							}
						}
						else if (input.OutboundFareID != 0)
						{
							if (sdsResult.OutboundCompanyFeesList != null && sdsResult.OutboundCompanyFeesList.Any())
							{
								foreach (var fee in sdsResult.OutboundCompanyFeesList)
								{
									fees = new Fees();
									fees.Description = fee.FeeType;
									fees.Calculations = new List<Calculation>();
									cal = new Calculation();
									cal.UnitCharge = System.Math.Round(fee.Amount, 2);
									cal.Quantity = 1;
									cal.MaxQuantity = 1;
									cal.Total = cal.UnitCharge * cal.Quantity;
									fees.Calculations.Add(cal);

									totalCharge += cal.Total;

									outboundGroundService.Fees.Add(fees);
								}
							}
						}
						#endregion

						#region DriverFeesList
						if (input.InboundFareID != 0)
						{

							if (sdsResult.InboundDriverFeesList != null && sdsResult.InboundDriverFeesList.Any())
							{
								foreach (var fee in sdsResult.InboundDriverFeesList)
								{
									fees = new Fees();
									fees.Description = fee.FeeType;
									fees.Calculations = new List<Calculation>();
									cal = new Calculation();
									cal.UnitCharge = System.Math.Round(fee.Amount, 2);
									cal.Quantity = 1;
									cal.MaxQuantity = 1;
									cal.Total = cal.UnitCharge * cal.Quantity;
									fees.Calculations.Add(cal);

									totalCharge += cal.Total;

									outboundGroundService.Fees.Add(fees);
								}
							}
						}
						else if (input.OutboundFareID != 0)
						{
							if (sdsResult.OutboundDriverFeesList != null && sdsResult.OutboundDriverFeesList.Any())
							{
								foreach (var fee in sdsResult.OutboundDriverFeesList)
								{
									fees = new Fees();
									fees.Description = fee.FeeType;
									fees.Calculations = new List<Calculation>();
									cal = new Calculation();
									cal.UnitCharge = System.Math.Round(fee.Amount, 2);
									cal.Quantity = 1;
									cal.MaxQuantity = 1;
									cal.Total = cal.UnitCharge * cal.Quantity;
									fees.Calculations.Add(cal);

									totalCharge += cal.Total;

									outboundGroundService.Fees.Add(fees);
								}
							}

						}
						#endregion
						#endregion
						#endregion

						#region TotalCharge
						outboundGroundService.TotalCharge = new TotalCharge();
						//Doug asked us to use serviceInfo.Total instead of we sum them up for fixing VAT, NYC, ... issues. 2014_06_26 (Pouyan Paryas)
						//groundService.TotalCharge.RateTotalAmount = totalCharge;
						if (input.InboundFareID != 0)
						{
							outboundGroundService.TotalCharge.RateTotalAmount = System.Math.Round(sdsResult.InboundTotal, 2);
							outboundGroundService.TotalCharge.EstimatedTotalAmount = System.Math.Round(sdsResult.InboundTotal, 2);
						}
						else if (input.OutboundFareID != 0)
						{

							outboundGroundService.TotalCharge.RateTotalAmount = System.Math.Round(sdsResult.OutboundTotal, 2);
							outboundGroundService.TotalCharge.EstimatedTotalAmount = System.Math.Round(sdsResult.OutboundTotal, 2);
						}
						#endregion

						#region Reference
						if (input.InboundFareID != 0)
						{
							outboundGroundService.Reference.ID = input.InboundFareID.ToString();
						}
						else if (input.OutboundFareID != 0)
						{
							outboundGroundService.Reference.ID = input.OutboundFareID.ToString();
						}
						#endregion

						#region DiscountDescription (Status)
						outboundGroundService.Reference.TPA_Extensions.DiscountDescription = sdsResult.DiscountName;
						#endregion

						#region TripDirection
						outboundGroundService.Reference.TPA_Extensions.TripDirection = Common.DTO.Enum.TripDirection.Outbound.ToString();
						#endregion

						result.GroundServices.GroundServices.Add(outboundGroundService);
					}

					#endregion


				}

				#region Common
				result.EchoToken = input.EchoToken;
				result.PrimaryLangID = input.PrimaryLangID;
				result.Target = input.Target;
				result.Version = input.Version;
				#endregion
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("MembershipDomain.GetPickupTime:: {0}", ex.ToString() + ex.StackTrace);

				#region Throw MembershipException
				throw VtodException.CreateException(ExceptionType.Membership, 7004);//throw new MembershipException(Messages.Membership_MemberAddLocation);
				#endregion
			}
		}
               
		public driver_info GetTaxiDriverInfo(string fleetId, string dispatchTripId)
		{
			var taxiDrvController = new UDI.SDS.TaxiDrvController(TrackTime);
			var result = taxiDrvController.GetTaxiDriverInfo(fleetId, dispatchTripId);
			return result;
		}
		
        public void UpdateFinalStatus(long Id, string status, decimal? fare, DateTime? tripStartTime, DateTime? tripEndTime, int? driverAcceptedEta = null)
        {
            using (VTODEntities context = new VTODEntities())
            {
                vtod_trip t = context.vtod_trip.Where(x => x.Id == Id).Select(x => x).FirstOrDefault();
                decimal? gratuity = null;
                decimal? totalFareAmount = fare;
                DateTime? newTripStartTime = null;
                DateTime? newTripEndTime = null;

                #region Update Fare
                #region Calculate Gratuity
                if (t.TotalFareAmount == null || t.TotalFareAmount.Value <= 0)
                {
                    if (fare.HasValue && fare > 0)
                    {
                        if (!t.Gratuity.HasValue || t.Gratuity <= 0)
                        {
                            if (t.GratuityRate.HasValue && t.GratuityRate > 0)
                            {
                                gratuity = fare.Value * t.GratuityRate.Value / 100;
                            }
                        }
                        else
                        {
                            gratuity = t.Gratuity;
                        }
                    }
                }
                else
                {
                    gratuity = t.Gratuity;
                }
                #endregion

                #region Calculate TotalFare
                if (t.TotalFareAmount == null || t.TotalFareAmount.Value <= 0)
                {
                    if (fare.HasValue && fare > 0 && gratuity.HasValue && gratuity.Value > 0)
                    {
                        totalFareAmount = fare + gratuity;
                    }
                    else if (fare.HasValue && fare > 0)
                    {
                        totalFareAmount = fare;
                    }
                }
                else
                {
                    totalFareAmount = t.TotalFareAmount;
                }
                #endregion
                #endregion

                #region Update Trip Time

                if (t.TripStartTime == null)
                    newTripStartTime = tripStartTime;
                else
                    newTripStartTime = t.TripStartTime;

                if (t.TripEndTime == null)
                    newTripEndTime = tripEndTime;
                else
                    newTripEndTime = t.TripEndTime;
                #endregion

                #region Update ReferralTrip
                using (TransactionScope transaction = new TransactionScope())
                {
                    context.SP_vtod_Update_FinalStatus(Id, status, totalFareAmount, gratuity, newTripStartTime, newTripEndTime, driverAcceptedEta);

                    if (status.Equals("Completed", StringComparison.OrdinalIgnoreCase))
                    {
                        ReferralTrip referralTrip = context.ReferralTrips.Where(x => x.VtodTripID.Equals(Id)).SingleOrDefault();

                        if (referralTrip != null)
                        {
                            bool anyTripsInprocessOrCredited = context.ReferralTrips.Where(x => x.SDSMemberID.Equals(referralTrip.SDSMemberID)
                                                                                           && (x.ReferralCreditFlow == (int)ReferrerCreditFlow.Credit_Inprogress
                                                                                                || x.ReferralCreditFlow == (int)ReferrerCreditFlow.Credit_SDS_Error
                                                                                                || x.ReferralCreditFlow == (int)ReferrerCreditFlow.Credit_Completed)).Any();
                            if (!anyTripsInprocessOrCredited)
                            {
                                referralTrip.TripStatus = "SDSTrip-Completed";
                                referralTrip.ReferralCreditFlow = (int)ReferrerCreditFlow.Credit_Inprogress;
                                referralTrip.UpdatedOn = DateTime.Now;

                                //Update the other trips as 'Trip_Duplicate'  to deactivate the trips
                                foreach (ReferralTrip referralTripNeedTobeInactive in context.ReferralTrips.Where(x => x.SDSMemberID.Equals(referralTrip.SDSMemberID) && !x.VtodTripID.Equals(Id)))
                                {
                                    referralTripNeedTobeInactive.ReferralCreditFlow = (int)ReferrerCreditFlow.Trip_Duplicate;
                                    referralTripNeedTobeInactive.UpdatedOn = DateTime.Now;
                                }

                                context.SaveChanges();
                            }                            
                        }
                    }                    

                    transaction.Complete();
                }
                #endregion
            }
        }
		#region Private
		#region Extension Data - UserInfo (Setter/Getter)
		private UserInfo getUserInfo(string userName)
		{
			UserInfo result = null;


			using (var db = new VTODEntities())
			{
				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
				#endregion

				var userInfos = db.SP_SDS_GetUserInfo(userName).ToList();

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_DB, "SP_SDS_GetUserInfo");
				#endregion

				if (userInfos != null && userInfos.Any())
				{
					var userInfo = userInfos.First();
					result = new UserInfo { UserName = userInfo.UserName, AllowClosedSplits = userInfo.AllowClosedSplits, ShowSplits = userInfo.ShowSplits, PickupTimeApproach = userInfo.PickupTimeApproach };
				}
			}

			return result;
		}

		private void setUserInfo(UserInfo userInfo)
		{
			using (var db = new VTODEntities())
			{
				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
				#endregion

				db.SP_SDS_InsertUserInfo(userInfo.UserName, userInfo.AllowClosedSplits, userInfo.ShowSplits);

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_DB, "SP_SDS_InsertUserInfo");
				#endregion
			}
		}
		#endregion

		#region Airport
		private OTA_GroundAvailRS groundAvail_Airport(TokenRS tokenVTOD, OTA_GroundAvailRQ groundAvailRQ)
		{
			var request = new BookRequest();

			try
			{

				#region Init
				var reservationController = new UDI.SDS.ReservationsController(tokenVTOD, TrackTime);

				var rateController = new RateController(tokenVTOD, TrackTime);

				var sdsObjectFactory = new SDSObjectFactory();
				#endregion

				#region Modification
				#endregion

				#region Validation
				groundAvailRQ.ValidateAirport();
				#endregion

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Validation");
				#endregion

				OTA_GroundAvailRS response = new OTA_GroundAvailRS();

				#region Init Respose
				response.EchoToken = groundAvailRQ.EchoToken;
				response.Target = groundAvailRQ.Target;
				response.Version = groundAvailRQ.Version;
				response.PrimaryLangID = groundAvailRQ.PrimaryLangID;
				request.RequestUtcTime = DateTime.UtcNow;
				response.GroundServices = new GroundServiceList();
				response.GroundServices.GroundServices = new List<GroundService>();
				#endregion

				#region Get Segments
				request.Segments = sdsObjectFactory.GetSegmentsWithGoundAvailRQ(groundAvailRQ);
				#endregion

				#region Get DiscountCode
				var discountCode = (groundAvailRQ.RateQualifiers != null
												&& groundAvailRQ.RateQualifiers.FirstOrDefault().PromotionCode != null
												) ? groundAvailRQ.RateQualifiers.FirstOrDefault().PromotionCode : string.Empty;
				#endregion

				#region Get UserInfo
				UserInfo userInfo = getUserInfo(tokenVTOD.Username);
				reservationController.UserInfo = userInfo;
				rateController.UserInfo = userInfo;
				#endregion

				#region Meet and Greet
				bool? meetAndGreet = null;
				if (groundAvailRQ.PassengerPrefs != null)
				{
					meetAndGreet = groundAvailRQ.PassengerPrefs.MeetAndGreetInd;
				}
				#endregion

				#region GratuityFix and GratuityRate
				decimal gratuityRate = 0;
				decimal gratuity = 0;
				try
				{
					if (groundAvailRQ.RateQualifiers.First().SpecialInputs != null && groundAvailRQ.RateQualifiers.First().SpecialInputs.Any())
					{
						var gratuities = groundAvailRQ.RateQualifiers.First().SpecialInputs.Where(s => s.Name.ToLower() == "gratuity").ToList();
						if (gratuities != null && gratuities.Any())
						{
							var gratuityElementValue = gratuities.First().Value;
							if (gratuityElementValue.Contains("%"))
							{
								gratuityRate = gratuityElementValue.GetPercentageAmount();
							}
							else
							{
								gratuity = gratuityElementValue.ToDecimal();
							}
						}
					}
				}
				catch
				{ }
				#endregion

				#region Get sds_user_subfleet (Subfleet Permission)
				Dictionary<string, bool> subfleetPermission = null;
				using (var db = new VTODEntities())
				{
					subfleetPermission = db.sds_user_subfleet.Where(s => s.my_aspnet_users.name == tokenVTOD.Username).ToDictionary(s => s.subfleet, s => s.Allow);
				}
				#endregion

				#region BookingFee
				decimal bookingFee = 0;
				try
				{
					if (groundAvailRQ.RateQualifiers.FirstOrDefault().SpecialInputs != null && groundAvailRQ.RateQualifiers.FirstOrDefault().SpecialInputs.Any())
					{
						//if (groundAvailRQ.RateQualifiers.FirstOrDefault().SpecialInputs.Where(s => s.Name.Trim().ToLower() == "bookingfee").Any())
						//{
						var bookingFeeElm = groundAvailRQ.RateQualifiers.FirstOrDefault().SpecialInputs.Where(s => s.Name.Trim().ToLower() == "bookingfee").FirstOrDefault();
						bookingFee = bookingFeeElm.Value.ToDecimal();
						//}
					}
				}
				catch (Exception ex)
				{
					logger.Error("Booking FeeIssue", ex);
				}
				#endregion

				#region DirectBillAccountId
				int directBillID = 0;
				if (groundAvailRQ.TPA_Extensions != null && groundAvailRQ.TPA_Extensions.Payments != null && groundAvailRQ.TPA_Extensions.Payments.Payments != null && groundAvailRQ.TPA_Extensions.Payments.Payments.Any())
				{
					var payment = groundAvailRQ.TPA_Extensions.Payments.Payments.FirstOrDefault();
					if (payment != null && payment.DirectBill != null && !string.IsNullOrWhiteSpace(payment.DirectBill.ID))
					{
						directBillID = payment.DirectBill.ID.ToInt32();
					}
				}
				#endregion

				#region VehicleCode
				var vehicleCode = "0";

				try
				{
					if (groundAvailRQ.VehiclePrefs != null && groundAvailRQ.VehiclePrefs.Any())
					{
						var vehiclePrefs = groundAvailRQ.VehiclePrefs.First();
						if (vehiclePrefs.Type != null && !string.IsNullOrWhiteSpace(vehiclePrefs.Type.Code))
						{
							vehicleCode = vehiclePrefs.Type.Code;
						}

					}
				}
				catch
				{

					throw;
				}
				#endregion

				#region Currency
				var currency = Common.DTO.Enum.Currency.USD;

				if (groundAvailRQ.POS != null && groundAvailRQ.POS.Source != null && !string.IsNullOrEmpty(groundAvailRQ.POS.Source.ISOCurrency))
				{
					currency = groundAvailRQ.POS.Source.ISOCurrency.ToCurrency();
				}
                #endregion

                #region CultureCode
                var CultureCode = "en-US";
                if (groundAvailRQ != null && !string.IsNullOrEmpty(groundAvailRQ.PrimaryLangID))
                    CultureCode = groundAvailRQ.PrimaryLangID;
                #endregion

                #region IsAccessible
                bool IsAccessible = false;
                if(groundAvailRQ.Service!=null && groundAvailRQ.Service.DisabilityVehicleInd!=false)
                {
                    IsAccessible = groundAvailRQ.Service.DisabilityVehicleInd;
                }
                #endregion

                #region IsInboundRideNow 
                bool? IsInboundRideNow = false;
                if (groundAvailRQ.TPA_Extensions != null && groundAvailRQ.TPA_Extensions.IsInboundRideNow != false)
                {
                    IsInboundRideNow = groundAvailRQ.TPA_Extensions.IsInboundRideNow;
                }
                #endregion

                #region IsOutboundRideNow 
                bool? IsOutboundRideNow = false;
                if (groundAvailRQ.TPA_Extensions != null && groundAvailRQ.TPA_Extensions.IsOutboundRideNow != false)
                {
                    IsOutboundRideNow = groundAvailRQ.TPA_Extensions.IsOutboundRideNow;
                }
                #endregion

                #region UserId
                string UserId = string.Empty;

                if (groundAvailRQ.TPA_Extensions != null  && !string.IsNullOrEmpty(groundAvailRQ.TPA_Extensions.UserId))
                {
                    UserId = groundAvailRQ.TPA_Extensions.UserId;
                }
                #endregion
                #region RemoveCallCenterBookingFee  
                bool? RemoveCallCenterBookingFee = false;
                if (groundAvailRQ.TPA_Extensions != null &&  groundAvailRQ.TPA_Extensions.RemoveCallCenterBookingFee!= null)                {
                    RemoveCallCenterBookingFee = groundAvailRQ.TPA_Extensions.RemoveCallCenterBookingFee;
                }
                #endregion
                #region GetRates
                var rates = rateController.GetAirportRate(request.Segments, subfleetPermission, discountCode, meetAndGreet, gratuityRate, bookingFee, directBillID, vehicleCode, currency, IsAccessible, IsInboundRideNow, IsOutboundRideNow, UserId, RemoveCallCenterBookingFee, CultureCode);
				#endregion

				#region Get Rates, PU Time, Adjusting Gratuity, Linked Trips

				var linkedTrips = new Dictionary<int, List<int>>();

				List<UDI.SDS.ReservationsService.PickupTimeRecord> pickupTimes = null;

				foreach (var serviceInfo in rates.ServiceInfoRates)
				{
					#region PickupTime
					var rateSegment = request.Segments.Where(s => s.TripDirection.ToString() == serviceInfo.Direction.ToString()).FirstOrDefault();
					pickupTimes = reservationController.GetAirportPickupTimes(rateSegment, serviceInfo, /*rates.BookRequestID,*/ 0, Method.GroundAvail);
				
                    #endregion

					#region Adjust Gratuity - This adjustment is just for fixed gratuity. If request is for GratuityRate (%), result comes and be adjusted by SDS api.
					if (gratuity > 0 && gratuityRate <= 0)
					{
						serviceInfo.Gratuity = gratuity;
						serviceInfo.Total += gratuity;
					}
					#endregion

					var groundService = sdsObjectFactory.GetGroundServiceObjectForAirport(tokenVTOD, serviceInfo, pickupTimes, groundAvailRQ);

					response.GroundServices.GroundServices.Add(groundService);
					if (serviceInfo.LinkedID > 0)
					{
						if (!linkedTrips.Where(s => s.Key == serviceInfo.LinkedID).Any())
						{
							var fareList = new List<int>();
							linkedTrips.Add(serviceInfo.LinkedID, fareList);
						}

						linkedTrips.Where(s => s.Key == serviceInfo.LinkedID).First().Value.Add(serviceInfo.FareID);
					}
				}
				#endregion

				#region Add Linked Trip IDs in TPA_Extension
				if (linkedTrips != null && linkedTrips.Any())
				{
					if (response.TPA_Extensions == null)
					{
						response.TPA_Extensions = new TPA_Extensions();
					}

					response.TPA_Extensions.References = new ReferenceList();
					response.TPA_Extensions.References.LinkedReferences = new List<LinkedReference>();

					foreach (var linkedTrip in linkedTrips)
					{
						var linkedReferences = new LinkedReference();
						linkedReferences.References = new List<Reference>();

						foreach (var item in linkedTrip.Value)
						{
							var reference = new Reference { ID = item.ToString() };
							linkedReferences.References.Add(reference);
							//response.TPA_Extensions.References.LinkedReference.References.Add(reference);
						}

						response.TPA_Extensions.References.LinkedReferences.Add(linkedReferences);
					}
				}
				#endregion

				response.Success = new Success();

				return response;
			}
			catch (VtodException ex)
			{
				logger.ErrorFormat("SDSDomain.GroundAvail:: {0}", ex.ToString() + ex.StackTrace);
				throw ex;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("SDSDomain.GroundAvail:: {0}", ex.ToString() + ex.StackTrace);
				throw;
				#region Throw SDSException || Validation Exception
				//if (ex.GetType() == typeof(ValidationException))
				//{
				//	throw new ValidationException(ex.Message);
				//}
				//else
				//{
				//	if (ex.Message.Contains("::"))//Remove method name
				//		throw new SDSException(ex.Message.Substring(ex.Message.IndexOf("::") + 2));
				//	else
				//		throw new SDSException(ex.Message);
				//}

				#endregion
			}
		}

		private OTA_GroundBookRS groundBook_Airport(TokenRS tokenVTOD, OTA_GroundBookRQ groundBookRQ, out List<Tuple<string, int, long>> disptach_Rez_VTOD, int FleetTripCode, bool isModified = false )
		{            
            var request = new BookRequest();
			disptach_Rez_VTOD = new List<Tuple<string, int, long>>();

			try
			{
				#region Modification
				#region Modify phone numbers
				if (groundBookRQ != null && groundBookRQ.GroundReservations != null && groundBookRQ.GroundReservations.Any() && groundBookRQ.GroundReservations.First().Passenger != null && groundBookRQ.GroundReservations.First().Passenger.Primary != null && groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones != null && groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones.Any())
				{
					groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones.First().CountryAccessCode = string.IsNullOrWhiteSpace(groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones.First().CountryAccessCode) ? groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones.First().CountryAccessCode : groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones.First().CountryAccessCode.CleanPhone().CleanCountryCode();
					groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones.First().AreaCityCode = string.IsNullOrWhiteSpace(groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones.First().AreaCityCode) ? groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones.First().AreaCityCode : groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones.First().AreaCityCode.CleanPhone();
					groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones.First().PhoneNumber = string.IsNullOrWhiteSpace(groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones.First().PhoneNumber) ? groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones.First().PhoneNumber : groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones.First().PhoneNumber.CleanPhone();
				}
				#endregion
				#endregion

				#region Validation
                groundBookRQ.Username = tokenVTOD.Username;
				groundBookRQ.Validate();
				#endregion

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Validation");
				#endregion

				OTA_GroundBookRS response = new OTA_GroundBookRS();

				#region Init
				UDI.SDS.ReservationsController reservationController = new UDI.SDS.ReservationsController(tokenVTOD, TrackTime);

				var rateController = new RateController(tokenVTOD, TrackTime);
				var geocoderController = new GeocoderController(tokenVTOD, TrackTime);
				var asapController = new ASAPController(tokenVTOD, TrackTime);
				var sdsObjectFactory = new SDSObjectFactory();
				#endregion

				#region Init Response
				response.EchoToken = groundBookRQ.EchoToken;
				response.Target = groundBookRQ.Target;
				response.Version = groundBookRQ.Version;
				response.PrimaryLangID = groundBookRQ.PrimaryLangID;
				response.Reservations = new List<Common.DTO.OTA.Reservation>();
				#endregion

				if (groundBookRQ.GroundReservations != null && groundBookRQ.TPA_Extensions != null && groundBookRQ.References != null)
				{
					var groundReservation = groundBookRQ.GroundReservations.FirstOrDefault();

					#region Init
					var pickupTimeResponseFirstSegment = new PickupTimeResponse();
					var pickupTimeResponseSecondSegment = new PickupTimeResponse();
					var getPickupTimesFirstSegment = new List<UDI.SDS.ReservationsService.PickupTimeRecord>();
					var getPickupTimesSecondSegment = new List<UDI.SDS.ReservationsService.PickupTimeRecord>();
					bool findMatchingRateFlagFirstSegment = false;
					bool findMatchPickupTimeFlagFirstSegment = false;
					bool findMatchingRateFlagSecondSegment = false;
					bool findMatchPickupTimeFlagSecondSegment = false;
					#endregion

					#region Set SDS Component BookRequest
					request.RequestUtcTime = DateTime.UtcNow;

					//SetSegments
					request.Segments = sdsObjectFactory.GetSegmentsWithGroundReservation(groundReservation, groundBookRQ, null);

                    //Adding ChildSeats and InfantSeats which Doug has asked
                    foreach (var segment in request.Segments)
                    {
                        segment.IsChildSeats = groundBookRQ.TPA_Extensions.ChildSeats;
                        segment.IsInfantSeats = groundBookRQ.TPA_Extensions.InfantSeats;
                    }


                    //Set ReservationRequest
                    request.ReservationRequest = sdsObjectFactory.GetReservationRequestObject(tokenVTOD, groundBookRQ);
					#endregion

					#region Get DiscountCode
					var discountCode = (groundReservation.RateQualifiers != null
															&& groundReservation.RateQualifiers.FirstOrDefault().PromotionCode != null
															) ? groundReservation.RateQualifiers.FirstOrDefault().PromotionCode : string.Empty;
					#endregion

					#region Get UserInfo
					UserInfo userInfo = getUserInfo(tokenVTOD.Username);
					rateController.UserInfo = userInfo;
					reservationController.UserInfo = userInfo;
					#endregion

					#region GratuityFix and GratuityRate
					decimal gratuityRate = 0;
					decimal gratuity = 0;
					try
					{
						if (groundBookRQ.GroundReservations.First().RateQualifiers.First().SpecialInputs != null && groundBookRQ.GroundReservations.First().RateQualifiers.First().SpecialInputs.Any())
						{
							var gratuities = groundBookRQ.GroundReservations.First().RateQualifiers.First().SpecialInputs.Where(s => s.Name.ToLower() == "gratuity").ToList();
							if (gratuities != null && gratuities.Any())
							{
								var gratuityElementValue = gratuities.First().Value;
								if (gratuityElementValue.Contains("%"))
								{
									gratuityRate = gratuityElementValue.GetPercentageAmount();
								}
								else
								{
									gratuity = gratuityElementValue.ToDecimal();
								}
							}
						}
					}
					catch
					{ }
					#endregion

					#region Get sds_user_subfleet (Subfleet Permission)
					Dictionary<string, bool> subfleetPermission = null;
					using (var db = new VTODEntities())
					{
						subfleetPermission = db.sds_user_subfleet.Where(s => s.my_aspnet_users.name == tokenVTOD.Username).ToDictionary(s => s.subfleet, s => s.Allow);
					}
					#endregion

					#region BookingFee
					decimal bookingFee = 0;
					try
					{
						if (groundReservation.RateQualifiers.FirstOrDefault().SpecialInputs != null && groundReservation.RateQualifiers.FirstOrDefault().SpecialInputs.Any())
						{
							//if (groundAvailRQ.RateQualifiers.FirstOrDefault().SpecialInputs.Where(s => s.Name.Trim().ToLower() == "bookingfee").Any())
							//{
							var bookingFeeElm = groundReservation.RateQualifiers.FirstOrDefault().SpecialInputs.Where(s => s.Name.Trim().ToLower() == "bookingfee").FirstOrDefault();
							bookingFee = bookingFeeElm.Value.ToDecimal();
							//}
						}
					}
					catch (Exception ex)
					{
						logger.Error("Booking FeeIssue", ex);
					}
					#endregion

					#region DirectBillAccountId
					int directBillID = 0;
					if (groundBookRQ.Payments != null && groundBookRQ.Payments.Payments != null && groundBookRQ.Payments.Payments.Any())
					{
						var payment = groundBookRQ.Payments.Payments.FirstOrDefault();
						if (payment != null && payment.DirectBill != null && !string.IsNullOrWhiteSpace(payment.DirectBill.ID))
						{
							directBillID = payment.DirectBill.ID.ToInt32();
						}
					}
					#endregion

					#region VehicleCode
					var vehicleCode = "0";
					// We do not need vehicleCode for booking as "0" means return all vehicle. So we have all the fareIDs to check.
					#endregion

					#region Currency
					var currency = Common.DTO.Enum.Currency.USD;

					if (groundBookRQ.POS != null && groundBookRQ.POS.Source != null && !string.IsNullOrEmpty(groundBookRQ.POS.Source.ISOCurrency))
					{
						currency = groundBookRQ.POS.Source.ISOCurrency.ToCurrency();
					}
                    #endregion

                    #region IsAccessible
                    bool IsAccessible = false;
                    if (groundReservation != null && groundReservation.Service != null && groundReservation.Service.DisabilityVehicleInd!=false)
                    {
                        IsAccessible = groundReservation.Service.DisabilityVehicleInd;
                        
                    }
                    #endregion

                    #region IsInboundRideNow 
                    bool? IsInboundRideNow = false;
                    if (groundBookRQ.TPA_Extensions != null && groundBookRQ.TPA_Extensions.IsInboundRideNow != false)
                    {
                        IsInboundRideNow = groundBookRQ.TPA_Extensions.IsInboundRideNow;
                    }
                    #endregion

                    #region IsOutboundRideNow 
                    bool? IsOutboundRideNow = false;
                    if (groundBookRQ.TPA_Extensions != null && groundBookRQ.TPA_Extensions.IsOutboundRideNow != false)
                    {
                        IsOutboundRideNow = groundBookRQ.TPA_Extensions.IsOutboundRideNow;
                    }
                    #endregion
                    #region UserId
                    string UserId = string.Empty;

                    if (groundBookRQ.TPA_Extensions != null && !string.IsNullOrEmpty(groundBookRQ.TPA_Extensions.UserId))
                    {
                        UserId = groundBookRQ.TPA_Extensions.UserId;
                    }
                    #endregion
                    #region RemoveCallCenterBookingFee  
                    bool? RemoveCallCenterBookingFee = false;
                    if (groundBookRQ.TPA_Extensions != null && groundBookRQ.TPA_Extensions.RemoveCallCenterBookingFee != false)
                    {
                        RemoveCallCenterBookingFee = groundBookRQ.TPA_Extensions.RemoveCallCenterBookingFee;
                    }
                    #endregion

                    #region CultureCode
                    var CultureCode = "en-US";
                    if (groundBookRQ != null && !string.IsNullOrEmpty(groundBookRQ.PrimaryLangID))
                        CultureCode = groundBookRQ.PrimaryLangID;
                    #endregion
                    #region GetRates
                    //We do not need to pass MeetAndGreetInd in the book. Customer should pass it in Get Rate and if he pass the correct RateID, we know it is MeetAndGreet
                    var rates = rateController.GetAirportRate(request.Segments, subfleetPermission, discountCode, null, gratuityRate, bookingFee, directBillID, vehicleCode, currency, IsAccessible, IsInboundRideNow, IsOutboundRideNow, UserId, RemoveCallCenterBookingFee, CultureCode);
					#endregion

					//var serviceInfos = rates.ServiceInfoRates.GroupBy(s => s.Direction).ToList();

					RatesResponse firstSegmentRate = null;
					RatesResponse secondSegmentRate = null;
					var serviceInfoSegments = rates.ServiceInfoRates.GroupBy(s => s.Direction);

					#region Set FirstSegmentRate (either inbound or outbound)
					//firstSegmentRate = rates.Where(s => s.TripDirection == request.Segments.First().TripDirection).First();

					firstSegmentRate = rates.Clone();
					firstSegmentRate.ServiceInfoRates = serviceInfoSegments.Where(s => s.Key.ToString() == request.Segments.First().TripDirection.ToString()).SelectMany(s => s).ToList();
					#endregion

					#region Set SecondSegmentRate (either inbound or outbound)
					if (request.Segments.Count == 2)
					{
						secondSegmentRate = rates.Clone();
						secondSegmentRate.ServiceInfoRates = serviceInfoSegments.Where(s => s.Key.ToString() == request.Segments.Last().TripDirection.ToString()).SelectMany(s => s).ToList();


						//secondSegmentRate = rates.Where(s => s.TripDirection == request.Segments.Last().TripDirection).First();
					}

					#endregion

					#region ASAP
					int? asapfirstSegmentRequestID = null;
                    int? asapsecondSegmentRequestID = null;

					if (groundBookRQ.TPA_Extensions.Confirmations != null)
					{						
                        var confirmations = groundBookRQ.TPA_Extensions.Confirmations.Where(s => "asapconfirmation".Equals(s.Type , StringComparison.OrdinalIgnoreCase)).ToList();

                        if (confirmations.Any(x => x.Segment.Equals(0)))
                        {
                            asapfirstSegmentRequestID = confirmations.Where(x => x.Segment==0).Single().ID.ToInt32();
                        }

                        if (confirmations.Any(x => x.Segment.Equals(1)))
                        {
                            asapsecondSegmentRequestID = confirmations.Where(x => x.Segment == 1).Single().ID.ToInt32();
                        }
                    }
					#endregion

					#region Match RateID -> Get Pickup Times
					var rateFleet = string.Empty;//Used for Pickup Time Matching

					if (firstSegmentRate.ServiceInfoRates != null)
					{
						#region First Segment -> Get Pickuptimes
						foreach (var serviceInfoRate in firstSegmentRate.ServiceInfoRates)
						{
							if (serviceInfoRate.FareID.ToString().Trim().ToLower() == groundBookRQ.References.First().ID.ToLower().Trim())
							{
								#region Adjust Gratuity
								if (gratuity > 0 && gratuityRate <= 0)
								{
									serviceInfoRate.Gratuity = gratuity;
								}
								#endregion

								/* Set serviceInfoRate */
								firstSegmentRate.ServiceInfoRates = new List<ServiceInfo>();
								firstSegmentRate.ServiceInfoRates.Add(serviceInfoRate);

								findMatchingRateFlagFirstSegment = true;

								rateFleet = serviceInfoRate.FleetType;

								var rateSegment = request.Segments.Where(s => s.TripDirection.ToString() == serviceInfoRate.Direction.ToString()).FirstOrDefault();
								getPickupTimesFirstSegment = reservationController.GetAirportPickupTimes(rateSegment, serviceInfoRate, /*firstSegmentRate.BookRequestID,*/ 0, Method.GroundBook);
                                
                                break;
							}
						}
						#endregion

						#region Second Segment -> Get Pickup times
						if (request.Segments.Count == 2)
						{
							if (secondSegmentRate != null && secondSegmentRate.ServiceInfoRates != null)
							{
								foreach (var serviceInfoRate in secondSegmentRate.ServiceInfoRates)
								{
									#region Adjust Gratuity
									if (gratuity > 0 && gratuityRate <= 0)
									{
										serviceInfoRate.Gratuity = gratuity;
									}
									#endregion

									if (serviceInfoRate.FareID.ToString().Trim().ToLower() == groundBookRQ.References.Last().ID.ToLower().Trim())
									{
										/* Set serviceInfoRate */
										secondSegmentRate.ServiceInfoRates = new List<ServiceInfo>();
										secondSegmentRate.ServiceInfoRates.Add(serviceInfoRate);

										findMatchingRateFlagSecondSegment = true;

										rateFleet = serviceInfoRate.FleetType;

										var serviceRate = new ServiceRate { ID = serviceInfoRate.FareID, Fleet = serviceInfoRate.FleetType, Service = serviceInfoRate.ServiceType };
										var rateSegment = request.Segments.Where(s => s.TripDirection.ToString() == serviceInfoRate.Direction.ToString()).FirstOrDefault();
										getPickupTimesSecondSegment = reservationController.GetAirportPickupTimes(rateSegment, serviceInfoRate, /*secondSegmentRate.BookRequestID,*/ 0, Method.GroundBook);

										break;
									}
								}
							}
						}
						#endregion

					}
					else
					{
						throw Common.DTO.VtodException.CreateFieldRequiredValidationException("Reference(FareID)"); // throw new Exception(string.Format(Messages.VTOD_SDSDomain_NoFare, "GroundBook"));
					}

					#region Throw exception if no matching rate found
					if (request.Segments.Count == 2)
					{
						if (!findMatchingRateFlagFirstSegment || !findMatchingRateFlagSecondSegment)
							throw VtodException.CreateException(ExceptionType.SDS, 2003); // new Exception(string.Format(Messages.VTOD_SDSDomain_GroundBook_NoMatchingRateFound, "GroundBook"));
					}
					else
					{
						if (!findMatchingRateFlagFirstSegment)
							throw VtodException.CreateException(ExceptionType.SDS, 2003); //throw new Exception(string.Format(Messages.VTOD_SDSDomain_GroundBook_NoMatchingRateFound, "GroundBook"));
					}
                    #endregion
                    #endregion

                    //Doug requested to bypass pickup time validation in the vtod api if it is a request for modifing trip
                    //On Thursday, June 30, 2016 9:05 AM             
                    // Below exceptions in Match PickupTime Region are filtered by !isModified       
                    #region Match Pickup Time and Bypass
                    if (isModified)
                    {
                        #region Bypass                        
                        pickupTimeResponseFirstSegment.AvailablePickupEndTime = request.Segments.First().RequestPickupTime;
                        pickupTimeResponseFirstSegment.AvailablePickupTime = request.Segments.First().RequestPickupTime;
                        if (getPickupTimesFirstSegment != null && getPickupTimesFirstSegment.Any())
                        {
                            foreach (var pickupTime in getPickupTimesFirstSegment)
                            {
                                var segment = request.Segments.FirstOrDefault();
                                if (pickupTime.StartTime == segment.RequestPickupTime)
                                {
                                    pickupTimeResponseFirstSegment.IsGuaranteed = pickupTime.IsGuaranteedTime;
                                    pickupTimeResponseFirstSegment.IsScheduledStop = pickupTime.IsScheduledStop;
                                    break;
                                }
                            }
                        }
                        if (request.Segments.Count == 2)
                        {
                            pickupTimeResponseSecondSegment.AvailablePickupEndTime = request.Segments.Last().RequestPickupTime;
                            pickupTimeResponseSecondSegment.AvailablePickupTime = request.Segments.Last().RequestPickupTime;
                            if (getPickupTimesSecondSegment != null && getPickupTimesSecondSegment.Any())
                            {
                                foreach (var pickupTime in getPickupTimesSecondSegment)
                                {
                                    var segment = request.Segments.LastOrDefault();
                                    if (pickupTime.StartTime == segment.RequestPickupTime)
                                    {
                                        pickupTimeResponseSecondSegment.IsGuaranteed = pickupTime.IsGuaranteedTime;
                                        pickupTimeResponseSecondSegment.IsScheduledStop = pickupTime.IsScheduledStop;
                                        break;
                                    }
                                }
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        #region Match PickupTime
                        if (asapfirstSegmentRequestID.HasValue)
                        {
                            var newAsapStatus = asapController.GetASAPRequestStatus(asapfirstSegmentRequestID.Value);
                            if (newAsapStatus.DispositionCode == ASAPDispositionCodesEnumeration.NewPickupTimeApproved || newAsapStatus.DispositionCode == ASAPDispositionCodesEnumeration.OriginalPickupTimeApproved)
                            {
                                var segment = request.Segments.FirstOrDefault();
                                if (segment.RequestPickupTime != newAsapStatus.ApprovedPickupTime)
                                {
                                    throw Common.DTO.VtodException.CreateException(ExceptionType.SDS, 6002);
                                }
                                pickupTimeResponseFirstSegment = new PickupTimeResponse();
                                pickupTimeResponseFirstSegment.AsapRequestId = asapfirstSegmentRequestID.Value;
                                pickupTimeResponseFirstSegment.AvailablePickupTime = newAsapStatus.ApprovedPickupTime;
                                pickupTimeResponseFirstSegment.IsGuaranteed = newAsapStatus.ServiceIsAOR;
                                pickupTimeResponseFirstSegment.AvailablePickupEndTime = newAsapStatus.ApprovedPickupTime.AddMinutes(15);

                                findMatchPickupTimeFlagFirstSegment = true;

                            }
                            else
                            {
                                throw Common.DTO.VtodException.CreateException(ExceptionType.SDS, 2004);
                            }

                            if (request.Segments.Count == 2)
                                if (getPickupTimesSecondSegment != null && getPickupTimesSecondSegment.Count > 0)
                                {
                                    foreach (var pickupTime in getPickupTimesSecondSegment)
                                    {
                                        var segment = request.Segments.LastOrDefault();
                                        if (pickupTime.StartTime == segment.RequestPickupTime)
                                        {
                                            if (pickupTime.Status == UDI.SDS.ReservationsService.PickupTimeStatusCodesEnumeration.PickupExpired)
                                            {
                                                if (IsOutboundRideNow == false)
                                                    throw Common.DTO.VtodException.CreateException(ExceptionType.SDS, 2009);
                                            }
                                            if (pickupTime.Status != UDI.SDS.ReservationsService.PickupTimeStatusCodesEnumeration.AllowPickup)
                                            {
                                                if (IsOutboundRideNow == false)
                                                    throw VtodException.CreateException(ExceptionType.SDS, 2005);
                                            }

                                            pickupTimeResponseSecondSegment.AvailablePickupEndTime = pickupTime.EndTime;
                                            pickupTimeResponseSecondSegment.AvailablePickupTime = pickupTime.StartTime;
                                            try
                                            {
                                                if (asapsecondSegmentRequestID != null)
                                                {
                                                    pickupTimeResponseSecondSegment.AsapRequestId = asapsecondSegmentRequestID.Value;
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                logger.Error(ex);
                                            }
                                            //pickupTimeResponseSecondSegment.BookRequestID = secondSegmentRate.BookRequestID;
                                            pickupTimeResponseSecondSegment.IsGuaranteed = pickupTime.IsGuaranteedTime;
                                            pickupTimeResponseSecondSegment.IsScheduledStop = pickupTime.IsScheduledStop;

                                            findMatchPickupTimeFlagSecondSegment = true;

                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    throw Common.DTO.VtodException.CreateException(ExceptionType.SDS, 1004);
                                }
                            #region Throw exception if no matching pickup time found
                            if (request.Segments.Count == 2)
                            {
                                if (asapfirstSegmentRequestID.HasValue && asapsecondSegmentRequestID.HasValue)
                                {
                                    if (!findMatchPickupTimeFlagFirstSegment || !findMatchPickupTimeFlagSecondSegment)
                                        throw VtodException.CreateException(ExceptionType.SDS, 2003);
                                }
                                else if (asapfirstSegmentRequestID.HasValue)
                                {
                                    if (!findMatchPickupTimeFlagFirstSegment)
                                        throw VtodException.CreateException(ExceptionType.SDS, 2003);
                                }
                                else if (asapsecondSegmentRequestID.HasValue)
                                {
                                    if (!findMatchPickupTimeFlagSecondSegment)
                                        throw VtodException.CreateException(ExceptionType.SDS, 2003);
                                }
                            }
                            else
                            {
                                if (!findMatchPickupTimeFlagFirstSegment)
                                    throw VtodException.CreateException(ExceptionType.SDS, 2003);
                            }
                            #endregion
                        }
                        else if (asapsecondSegmentRequestID.HasValue)
                        {
                            var newAsapStatus = asapController.GetASAPRequestStatus(asapsecondSegmentRequestID.Value);
                            if (newAsapStatus.DispositionCode == ASAPDispositionCodesEnumeration.NewPickupTimeApproved || newAsapStatus.DispositionCode == ASAPDispositionCodesEnumeration.OriginalPickupTimeApproved)
                            {
                                var segment = request.Segments.LastOrDefault();
                                if (segment.RequestPickupTime != newAsapStatus.ApprovedPickupTime)
                                {
                                    throw Common.DTO.VtodException.CreateException(ExceptionType.SDS, 6002);
                                }
                                pickupTimeResponseSecondSegment = new PickupTimeResponse();
                                pickupTimeResponseSecondSegment.AsapRequestId = asapsecondSegmentRequestID.Value;
                                pickupTimeResponseSecondSegment.AvailablePickupTime = newAsapStatus.ApprovedPickupTime;
                                pickupTimeResponseSecondSegment.IsGuaranteed = newAsapStatus.ServiceIsAOR;
                                pickupTimeResponseSecondSegment.AvailablePickupEndTime = newAsapStatus.ApprovedPickupTime.AddMinutes(15);

                                findMatchPickupTimeFlagSecondSegment = true;

                            }
                            else
                            {
                                throw Common.DTO.VtodException.CreateException(ExceptionType.SDS, 2004);
                            }

                            if (request.Segments.Count == 2)
                                if (getPickupTimesFirstSegment != null && getPickupTimesFirstSegment.Count > 0)
                                {
                                    foreach (var pickupTime in getPickupTimesFirstSegment)
                                    {
                                        var segment = request.Segments.FirstOrDefault();
                                        if (pickupTime.StartTime == segment.RequestPickupTime)
                                        {
                                            if (pickupTime.Status == UDI.SDS.ReservationsService.PickupTimeStatusCodesEnumeration.PickupExpired)
                                            {
                                                if (IsOutboundRideNow == false)
                                                    throw Common.DTO.VtodException.CreateException(ExceptionType.SDS, 2009);
                                            }
                                            if (pickupTime.Status != UDI.SDS.ReservationsService.PickupTimeStatusCodesEnumeration.AllowPickup)
                                            {
                                                if (IsOutboundRideNow == false)
                                                    throw VtodException.CreateException(ExceptionType.SDS, 2005);
                                            }

                                            pickupTimeResponseFirstSegment.AvailablePickupEndTime = pickupTime.EndTime;
                                            pickupTimeResponseFirstSegment.AvailablePickupTime = pickupTime.StartTime;
                                            try
                                            {
                                                if (asapfirstSegmentRequestID != null)
                                                {
                                                    pickupTimeResponseFirstSegment.AsapRequestId = asapfirstSegmentRequestID.Value;
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                logger.Error(ex);
                                            }
                                            //pickupTimeResponseSecondSegment.BookRequestID = secondSegmentRate.BookRequestID;
                                            pickupTimeResponseSecondSegment.IsGuaranteed = pickupTime.IsGuaranteedTime;
                                            pickupTimeResponseSecondSegment.IsScheduledStop = pickupTime.IsScheduledStop;

                                            findMatchPickupTimeFlagFirstSegment = true;

                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    throw Common.DTO.VtodException.CreateException(ExceptionType.SDS, 1004);
                                }
                            #region Throw exception if no matching pickup time found
                            if (request.Segments.Count == 2)
                            {
                                if (asapfirstSegmentRequestID.HasValue && asapsecondSegmentRequestID.HasValue)
                                {
                                    if (!findMatchPickupTimeFlagFirstSegment || !findMatchPickupTimeFlagSecondSegment)
                                        throw VtodException.CreateException(ExceptionType.SDS, 2003);
                                }
                                else if (asapfirstSegmentRequestID.HasValue)
                                {
                                    if (!findMatchPickupTimeFlagFirstSegment)
                                        throw VtodException.CreateException(ExceptionType.SDS, 2003);
                                }
                                else if (asapsecondSegmentRequestID.HasValue)
                                {
                                    if (!findMatchPickupTimeFlagSecondSegment)
                                        throw VtodException.CreateException(ExceptionType.SDS, 2003);
                                }
                            }
                            else
                            {
                                if (!findMatchPickupTimeFlagFirstSegment)
                                    throw VtodException.CreateException(ExceptionType.SDS, 2003);
                            }
                            #endregion
                        }
                        else
                        {

                            if (getPickupTimesFirstSegment != null && getPickupTimesFirstSegment.Count > 0)
                            {
                                foreach (var pickupTime in getPickupTimesFirstSegment)
                                {
                                    var segment = request.Segments.FirstOrDefault();
                                    if (pickupTime.StartTime == segment.RequestPickupTime)
                                    {
                                        if (pickupTime.Status == UDI.SDS.ReservationsService.PickupTimeStatusCodesEnumeration.PickupExpired)
                                        {
                                            if (IsOutboundRideNow == false)
                                                throw Common.DTO.VtodException.CreateException(ExceptionType.SDS, 2009);
                                        }
                                        if (pickupTime.Status != UDI.SDS.ReservationsService.PickupTimeStatusCodesEnumeration.AllowPickup)
                                        {
                                            if (IsOutboundRideNow == false)
                                                throw VtodException.CreateException(ExceptionType.SDS, 2005);
                                        }

                                        pickupTimeResponseFirstSegment.AvailablePickupEndTime = pickupTime.EndTime;
                                        pickupTimeResponseFirstSegment.AvailablePickupTime = pickupTime.StartTime;

                                        pickupTimeResponseFirstSegment.IsGuaranteed = pickupTime.IsGuaranteedTime;
                                        pickupTimeResponseFirstSegment.IsScheduledStop = pickupTime.IsScheduledStop;

                                        findMatchPickupTimeFlagFirstSegment = true;

                                        break;
                                    }
                                }
                            }
                            else
                            {
                                throw Common.DTO.VtodException.CreateException(ExceptionType.SDS, 1004);
                            }

                            if (request.Segments.Count == 2)
                                if (getPickupTimesSecondSegment != null && getPickupTimesSecondSegment.Count > 0)
                                {
                                    foreach (var pickupTime in getPickupTimesSecondSegment)
                                    {
                                        var segment = request.Segments.LastOrDefault();
                                        if (pickupTime.StartTime == segment.RequestPickupTime)
                                        {
                                            if (pickupTime.Status == UDI.SDS.ReservationsService.PickupTimeStatusCodesEnumeration.PickupExpired)
                                            {
                                                if (IsOutboundRideNow == false)
                                                    throw Common.DTO.VtodException.CreateException(ExceptionType.SDS, 2009);
                                            }
                                            if (pickupTime.Status != UDI.SDS.ReservationsService.PickupTimeStatusCodesEnumeration.AllowPickup)
                                            {
                                                if (IsOutboundRideNow == false)
                                                    throw VtodException.CreateException(ExceptionType.SDS, 2005);
                                            }

                                            pickupTimeResponseSecondSegment.AvailablePickupEndTime = pickupTime.EndTime;
                                            pickupTimeResponseSecondSegment.AvailablePickupTime = pickupTime.StartTime;
                                            pickupTimeResponseSecondSegment.IsGuaranteed = pickupTime.IsGuaranteedTime;
                                            pickupTimeResponseSecondSegment.IsScheduledStop = pickupTime.IsScheduledStop;

                                            findMatchPickupTimeFlagSecondSegment = true;

                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    throw Common.DTO.VtodException.CreateException(ExceptionType.SDS, 1004);
                                }

                            #region Throw exception if no matching pickup time found
                            if (request.Segments.Count == 2)
                            {
                                if (!findMatchPickupTimeFlagFirstSegment || !findMatchPickupTimeFlagSecondSegment)
                                    throw VtodException.CreateException(ExceptionType.SDS, 2003);
                            }
                            else
                            {
                                if (!findMatchPickupTimeFlagFirstSegment)
                                    throw VtodException.CreateException(ExceptionType.SDS, 2003);
                            }
                            #endregion

                        }

                        #endregion
                    }
                    #endregion
                    #region Modify Addresses
                    bool locationModified = false;
					var firstSegment = request.Segments.FirstOrDefault();
					var secondSegment = request.Segments.LastOrDefault();
					if (firstSegment != null && firstSegment.Locations != null && firstSegment.Locations.Any())
						foreach (var location in firstSegment.Locations)
						{
							if (location != null)
							{
								if (location.Longitude == 0 || location.Latitude == 0)
								{
									geocoderController.ModifyAddress(location, out locationModified);
								}
							}
						}

					if (secondSegment != null && secondSegment.Locations != null && secondSegment.Locations.Any())
						foreach (var location in secondSegment.Locations)
						{
							if (location != null)
							{
								if (location.Longitude == 0 || location.Latitude == 0)
								{
									geocoderController.ModifyAddress(location, out locationModified);
								}
							}
						}
					#endregion

					#region AirLine Rewards
					if (groundBookRQ.TPA_Extensions != null && groundBookRQ.TPA_Extensions.AirlineReward != null)
					{
						if (groundBookRQ.TPA_Extensions.AirlineReward.RewardID > 0 && !string.IsNullOrWhiteSpace(groundBookRQ.TPA_Extensions.AirlineReward.AccountNumber))
						{
							request.ReservationRequest.RewardsAccountNumber = groundBookRQ.TPA_Extensions.AirlineReward.AccountNumber;
							request.ReservationRequest.RewardsID = groundBookRQ.TPA_Extensions.AirlineReward.RewardID;
						}
					}
					#endregion

					#region Affiliate
					request.ReservationRequest.AffiliateInfo = new AffiliateInfo();
					if (!string.IsNullOrWhiteSpace(groundBookRQ.EchoToken))
					{
						request.ReservationRequest.AffiliateInfo.TrackingNumber = groundBookRQ.EchoToken;
					}
					try
					{
						var specialInputs = groundBookRQ.GroundReservations.First().RateQualifiers.First().SpecialInputs;
						if (specialInputs != null && specialInputs.Any())
						{
							var affiliateUserIDObj = specialInputs.Where(s => s.Name.ToLower() == "affiliateuserid").FirstOrDefault();
							if (affiliateUserIDObj != null)
							{
								request.ReservationRequest.AffiliateInfo.UserID = affiliateUserIDObj.Value;
							}

							var affiliateEmailAddressObj = specialInputs.Where(s => s.Name.ToLower() == "affiliateemailaddress").FirstOrDefault();
							if (affiliateEmailAddressObj != null)
							{
								request.ReservationRequest.AffiliateInfo.Email = affiliateEmailAddressObj.Value;
							}
						}
					}
					catch { }

                    #endregion

                    #region ReserveCsr
                    if (groundBookRQ.Username.ToLower() == "Rez".ToLower())
                    {
                        if (groundBookRQ != null && groundBookRQ.TPA_Extensions != null && !string.IsNullOrEmpty(groundBookRQ.TPA_Extensions.ReserveCsr))
                            request.ReservationRequest.ReserveCsr = groundBookRQ.TPA_Extensions.ReserveCsr;
                        else
                            request.ReservationRequest.ReserveCsr = string.Empty;
                    }
                    else
                    {
                        request.ReservationRequest.ReserveCsr = groundBookRQ.Username;
                    }
                    #endregion
                    #region CultureCode
                    if(groundBookRQ!=null && !string.IsNullOrEmpty(groundBookRQ.PrimaryLangID))
                      request.ReservationRequest.CultureCode = groundBookRQ.PrimaryLangID;
                    #endregion

                    #region Book
                    var bookResponse = reservationController.BookAirport(request, firstSegmentRate, pickupTimeResponseFirstSegment, secondSegmentRate, pickupTimeResponseSecondSegment);
					#endregion

					#region Save to Database

					using (var db = new VTODEntities())
					{
						/* Save to SDS_Trip table */
						var returnConfirmationNumber = string.Empty;
						var confirmationNumber = string.Empty;
						var returnRezID = 0;
						var rezID = 0;
						vtod_trip tripDAO = null;
						vtod_trip tripReturnDAO = null;
						request.FleetTripCode = FleetTripCode;
						var reservationResponse = bookResponse.ReservationResponses.FirstOrDefault();
						confirmationNumber = reservationResponse.AirportReservationConfirmations.FirstOrDefault().ConfirmationNumber;
						rezID = reservationResponse.AirportReservationConfirmations.FirstOrDefault().RezID;

						tripDAO = sdsObjectFactory.GetSDS_TripObject(request, bookResponse, request.Segments.FirstOrDefault(), groundBookRQ, confirmationNumber,isModified, logger);
						//db.sds_trip.sds_trip.Add(tbl_tripBooking);

						#region Modify TotalFareAmount
						if (firstSegmentRate != null && firstSegmentRate.ServiceInfoRates != null && firstSegmentRate.ServiceInfoRates.Any())
						{
							decimal totalFare = 0;
							var serviceInfoRate = firstSegmentRate.ServiceInfoRates.First();
							totalFare = serviceInfoRate.Total;
							tripDAO.TotalFareAmount = totalFare;
						}
						#endregion

						#region Set Payment Type
						tripDAO.PaymentType = Common.DTO.Enum.PaymentType.Unknown.ToString();
						if (groundBookRQ.Payments != null && groundBookRQ.Payments.Payments != null && groundBookRQ.Payments.Payments.Any())
						{
							if (groundBookRQ.Payments.Payments.First().PaymentCard != null)
							{
								tripDAO.PaymentType = Common.DTO.Enum.PaymentType.PaymentCard.ToString();
							}
							else if (groundBookRQ.Payments.Payments.First().DirectBill != null)
							{
								tripDAO.PaymentType = Common.DTO.Enum.PaymentType.DirectBill.ToString();
							}
							//We do not have Cash for SDS
							//if (groundBookRQ.Payments.Payments.First().Cash != null && groundBookRQ.Payments.Payments.First().Cash.CashIndicator == true)
							//{
							//	tbl_tripBooking.PaymentType = Common.DTO.Enum.PaymentType.Cash.ToString();
							//}
						}
						#endregion

						#region GetUser
						var user = db.my_aspnet_users.Where(s => s.name.ToLower() == tokenVTOD.Username.ToLower()).ToList().FirstOrDefault();
						#endregion

						#region Set UserID
						tripDAO.UserID = user.id;
						#endregion

						#region Set Ref, Source, Device
						if (groundBookRQ.TPA_Extensions != null && !string.IsNullOrWhiteSpace(groundBookRQ.TPA_Extensions.Device))
						{
							tripDAO.ConsumerDevice = groundBookRQ.TPA_Extensions.Device;
						}
                        else
                        {
                            if (user != null)
                            {
                                tripDAO.ConsumerDevice = user.name;
                            }
                        }
						if (groundBookRQ.TPA_Extensions != null && !string.IsNullOrWhiteSpace(groundBookRQ.TPA_Extensions.Source))
						{
							tripDAO.ConsumerSource = groundBookRQ.TPA_Extensions.Source;
						}
						if (!string.IsNullOrWhiteSpace(groundBookRQ.EchoToken))
						{
							tripDAO.ConsumerRef = groundBookRQ.EchoToken;
						}
						#endregion

						#region Set server booking time
						tripDAO.BookingServerDateTime = System.DateTime.Now;
						#endregion

						#region PromisedETA
						try
						{
                            if (groundBookRQ.TPA_Extensions.PickMeUpNow.HasValue && groundBookRQ.TPA_Extensions.PickMeUpNow == true)
                            {
                                var rateQuialifier = groundBookRQ.GroundReservations.First().RateQualifiers.First();
                                if (rateQuialifier.SpecialInputs != null)
                                {
                                    var promisedetas = rateQuialifier.SpecialInputs.Where(s => s.Name.ToLower() == "promisedeta").ToList();

                                    if (promisedetas != null && promisedetas.Any())
                                    {
                                        tripDAO.PromisedETA = promisedetas.First().Value.ToInt32();
                                    }
                                }
                            }
						}
						catch (Exception ex)
						{
							logger.Error("Taxi:PromisedETA", ex);
						}
						#endregion

						#region Status
						tripDAO.FinalStatus = "Booked";
						#endregion

						#region Track
						if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
                        #endregion

                        if (!isModified)
                        {
                            db.vtod_trip.Add(tripDAO);
                        }
                        else
                        {
                            db.Entry(tripDAO).State = EntityState.Modified;
                            db.Entry(tripDAO.sds_trip).State = EntityState.Modified;                          
                        }
						

						#region Track
						if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_DB, "db.vtod_trip.Add(tbl_tripBooking)");
						#endregion

						if (reservationResponse.AirportReservationConfirmations.Count == 2)
						{
							returnConfirmationNumber = reservationResponse.AirportReservationConfirmations.Last().ConfirmationNumber;
							returnRezID = reservationResponse.AirportReservationConfirmations.Last().RezID;
							tripReturnDAO = sdsObjectFactory.GetSDS_TripObject(request, bookResponse, request.Segments.Last(), groundBookRQ, returnConfirmationNumber, isModified, logger);
							//db.sds_trip.Add(tbl_returnTripBooking);

							#region Modify TotalFareAmount
							if (secondSegmentRate != null && secondSegmentRate.ServiceInfoRates != null && secondSegmentRate.ServiceInfoRates.Any())
							{
								decimal totalFare = 0;
								var serviceInfoRate = secondSegmentRate.ServiceInfoRates.First();
								totalFare = serviceInfoRate.Total;
								tripReturnDAO.TotalFareAmount = totalFare;
							}
                            #endregion

                            #region Set Payment Type
                            tripDAO.PaymentType = Common.DTO.Enum.PaymentType.Unknown.ToString();
							if (groundBookRQ.Payments != null && groundBookRQ.Payments.Payments != null && groundBookRQ.Payments.Payments.Any())
							{
								if (groundBookRQ.Payments.Payments.First().PaymentCard != null)
								{
									tripReturnDAO.PaymentType = Common.DTO.Enum.PaymentType.PaymentCard.ToString();
								}
								else if (groundBookRQ.Payments.Payments.First().DirectBill != null)
								{
									tripReturnDAO.PaymentType = Common.DTO.Enum.PaymentType.DirectBill.ToString();
								}								
							}
							#endregion

							#region Set UserID
							tripReturnDAO.UserID = user.id;
							#endregion

							#region Set Ref, Source, Device
							if (groundBookRQ.TPA_Extensions != null && !string.IsNullOrWhiteSpace(groundBookRQ.TPA_Extensions.Device))
							{
								tripReturnDAO.ConsumerDevice = groundBookRQ.TPA_Extensions.Device;
							}
                            else
                            {
                                if (user != null)
                                {
                                    tripReturnDAO.ConsumerDevice = user.name;
                                }
                            }
							if (groundBookRQ.TPA_Extensions != null && !string.IsNullOrWhiteSpace(groundBookRQ.TPA_Extensions.Source))
							{
								tripReturnDAO.ConsumerSource = groundBookRQ.TPA_Extensions.Source;
							}
							if (!string.IsNullOrWhiteSpace(groundBookRQ.EchoToken))
							{
								tripReturnDAO.ConsumerRef = groundBookRQ.EchoToken;
							}
							#endregion

							#region Set server booking time
							tripReturnDAO.BookingServerDateTime = System.DateTime.Now;
							#endregion

							#region Status
							tripReturnDAO.FinalStatus = "Booked";
							#endregion


							#region Track
							if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
                            #endregion
                                                       

                            if (!isModified)
                            {
                                db.vtod_trip.Add(tripReturnDAO);
                            }
                            else
                            {
                                db.Entry(tripReturnDAO).State = EntityState.Modified;
                                db.Entry(tripReturnDAO.sds_trip).State = EntityState.Modified;
                            }

                            #region Track
                            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_DB, "db.vtod_trip.Add(tbl_returnTripBooking);");
							#endregion

							//#region Insert Trip Transaction Info
							//#region Track
							//if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
							//#endregion
							//if (groundBookRQ.TPA_Extensions != null && !string.IsNullOrWhiteSpace(groundBookRQ.TPA_Extensions.Source))
							//{
							//	source = groundBookRQ.TPA_Extensions.Source;
							//}

							//if (groundBookRQ.TPA_Extensions != null && !string.IsNullOrWhiteSpace(groundBookRQ.TPA_Extensions.Device))
							//{
							//	device = groundBookRQ.TPA_Extensions.Device;
							//}

							//db.SP_vtod_InsertTripTransactionInfo(tbl_tripBooking.Id, tokenVTOD.Username, source, device, DateTime.Now);

							//#region Track
							//if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_DB, "SP_vtod_InsertTripTransactionInfo");
							//#endregion
							//#endregion

						}

						#region Track
						if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
						#endregion

						db.SaveChanges();

						#region Track
						if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_DB, "db.SaveChanges()");
						#endregion

						/* Remove payment from request */
						request.ReservationRequest.Payment = null;

						/* Save to Log table */
						//db.SP_SDS_InsertLog(vtodTXId, UDI.SDS.DTO.Enum.LogType.Book.ToString(), null, request.BookRequestID.ToString(), request.XmlSerialize().ToString(), bookResponse.XmlSerialize().ToString());

						#region Make sds_VTOD_TripIDs
						//This is for out paramter. do not touch it!
						if (tripDAO != null && tripDAO.Id > 0 && !string.IsNullOrWhiteSpace(confirmationNumber))
							//disptach_Rez_VTOD.Add(rezID.ToString(), tripDAO.Id);
							disptach_Rez_VTOD.Add(new Tuple<string, int, long>(confirmationNumber, rezID, tripDAO.Id));
						if (tripReturnDAO != null && tripReturnDAO.Id > 0 && !string.IsNullOrWhiteSpace(returnConfirmationNumber))
							//disptach_Rez_VTOD.Add(returnRezID.ToString(), tripReturnDAO.Id);
							disptach_Rez_VTOD.Add(new Tuple<string, int, long>(returnConfirmationNumber, returnRezID, tripReturnDAO.Id));
						#endregion
					}

					#endregion

					#region Set OTA_GroundBookRS
					var OTAreservation = sdsObjectFactory.GetReservationObject(bookResponse, groundReservation, disptach_Rez_VTOD);

					response.Reservations.Add(OTAreservation);
					#endregion
				}
				else
				{
					throw Common.DTO.VtodException.CreateFieldRequiredValidationException("GroundReservations|TPA_Extensions|Reference"); //new Exception(string.Format(Messages.VTOD_SDSDomain_GroundBookRQ_MissingData, "GroundBook"));
				}


				response.Success = new Success();
				return response;
			}
			catch (DbEntityValidationException e)
			{
				foreach (var eve in e.EntityValidationErrors)
				{
					logger.ErrorFormat("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
						eve.Entry.Entity.GetType().Name, eve.Entry.State);
					foreach (var ve in eve.ValidationErrors)
					{
						logger.ErrorFormat("- Property: \"{0}\", Error: \"{1}\"",
							ve.PropertyName, ve.ErrorMessage);
					}
				}
				throw;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("SDSDomain.GroundBook:: {0}", ex.ToString() + ex.StackTrace);

				throw;
			}            
		}
		#endregion
        
		#region Charter
		private OTA_GroundAvailRS groundAvail_Charter(TokenRS tokenVTOD, OTA_GroundAvailRQ groundAvailRQ, TripType tripType)
		{
			var request = new BookRequest();

			try
			{
				#region Init
				var reservationController = new UDI.SDS.ReservationsController(tokenVTOD, TrackTime);
				var rateController = new RateController(tokenVTOD, TrackTime);
				var landmarksController = new LandmarksController(tokenVTOD, TrackTime);
				var geocoderController = new GeocoderController(tokenVTOD, TrackTime);
				var utilityController = new UtilityController(tokenVTOD, TrackTime);
				var sdsObjectFactory = new SDSObjectFactory();
				#endregion

				#region Modification
				#endregion

				#region Validation
				groundAvailRQ.ValidateCharter();
				#endregion

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Validation");
				#endregion

				OTA_GroundAvailRS response = new OTA_GroundAvailRS();

				#region Init Respose
				response.EchoToken = groundAvailRQ.EchoToken;
				response.Target = groundAvailRQ.Target;
				response.Version = groundAvailRQ.Version;
				response.PrimaryLangID = groundAvailRQ.PrimaryLangID;
				request.RequestUtcTime = DateTime.UtcNow;
				response.GroundServices = new GroundServiceList();
				response.GroundServices.GroundServices = new List<GroundService>();
				#endregion

				#region Get Segments
				request.Segments = sdsObjectFactory.GetSegmentsWithGoundAvailRQ(groundAvailRQ);                
                #endregion

                #region GeoCode the addresses
                if (request.Segments != null && request.Segments.Any())
				{
					foreach (var segment in request.Segments)
					{
						foreach (var location in segment.Locations.Where(s => s != null))
						{
							if (location != null)
							{
								if (location.Latitude == 0 || location.Longitude == 0)
								{
									var modified = false;
									geocoderController.ModifyAddress(location, out modified);
								}
							}
						}
					}
				}
				#endregion

				#region Landmark
				if (request.Segments != null && request.Segments.Any())
				{
					foreach (var segment in request.Segments)
					{
						foreach (var location in segment.Locations.Where(s => s != null))
						{
							UDI.SDS.LandmarksService.LandmarkRecord landmarkRecord = null;
							if (location.Latitude == 0 || location.Longitude == 0)
							{
								landmarkRecord = landmarksController.GetLandmarkRecordByAddress(string.Empty, location);
							}
							else
							{
								landmarkRecord = landmarksController.GetLandmarksByCoordinates(string.Empty, location);
							}

							//if (landmarkRecord != null)
							//{
							//we do not have subzip for charter. //subZipName = landmarkRecord.ToSubZipName();
							location.LocationType = landmarkRecord.ToLocationType();
							location.LocationName = landmarkRecord.ToLocationName();
                            #region Override LocationName with the GroundAvail LocationName
                            if (string.IsNullOrEmpty(location.LocationName) && string.IsNullOrWhiteSpace(location.LocationName))
                            {
                                if (location!=null)
                                {
                                    if (!string.IsNullOrEmpty(location.PropertyName))
                                    {
                                        location.LocationName = location.PropertyName;
                                    }
                                }

                            } 
                            #endregion
							location.MasterLandmarkID = landmarkRecord.ToMasterLandmarkID();
							location.LandmarkID = landmarkRecord.ToLandmarkID();
							location.SubZipName = landmarkRecord.ToSubZipName();
							//}
						}
					}
				}
				#endregion

				#region Get DiscountCode
				var discountCode = (groundAvailRQ.RateQualifiers != null
												&& groundAvailRQ.RateQualifiers.FirstOrDefault().PromotionCode != null
												) ? groundAvailRQ.RateQualifiers.FirstOrDefault().PromotionCode : string.Empty;
				#endregion

				#region Get UserInfo
				//UserInfo userInfo = getUserInfo(tokenVTOD.Username);
				//rateController.UserInfo = userInfo;
				#endregion

				#region PickMeUpNow
				var pickMeUpNow = false;

				if (groundAvailRQ.TPA_Extensions != null && groundAvailRQ.TPA_Extensions.PickMeUpNow.HasValue)
				{
					pickMeUpNow = groundAvailRQ.TPA_Extensions.PickMeUpNow.Value;
				}
				#endregion

				#region Meet and Greet
				bool? meetAndGreet = null;
				if (groundAvailRQ.PassengerPrefs != null)
				{
					meetAndGreet = groundAvailRQ.PassengerPrefs.MeetAndGreetInd;
				}
				#endregion

				#region GratuityFix and GratuityRate
				decimal gratuityRate = 0;
				decimal gratuity = 0;
				try
				{
					if (groundAvailRQ.RateQualifiers.First().SpecialInputs != null && groundAvailRQ.RateQualifiers.First().SpecialInputs.Any())
					{
						var gratuities = groundAvailRQ.RateQualifiers.First().SpecialInputs.Where(s => s.Name.ToLower() == "gratuity").ToList();
						if (gratuities != null && gratuities.Any())
						{
							var gratuityElementValue = gratuities.First().Value;
							if (gratuityElementValue.Contains("%"))
							{
								gratuityRate = gratuityElementValue.GetPercentageAmount();
							}
							else
							{
								gratuity = gratuityElementValue.ToDecimal();
							}
						}
					}
				}
				catch
				{ }
				#endregion

				#region Get sds_user_subfleet (Subfleet Permission)
				Dictionary<string, bool> subfleetPermission = null;
				using (var db = new VTODEntities())
				{
					subfleetPermission = db.sds_user_subfleet.Where(s => s.my_aspnet_users.name == tokenVTOD.Username).ToDictionary(s => s.subfleet, s => s.Allow);
				}
				#endregion

				#region Currency
				var currency = Common.DTO.Enum.Currency.USD;

				if (groundAvailRQ.POS != null && groundAvailRQ.POS.Source != null && !string.IsNullOrEmpty(groundAvailRQ.POS.Source.ISOCurrency))
				{
					currency = groundAvailRQ.POS.Source.ISOCurrency.ToCurrency();
				}
                #endregion

                #region GetRates
                var rates = rateController.GetLocationToLocationRate(request.Segments, subfleetPermission, discountCode, pickMeUpNow, meetAndGreet, gratuityRate, currency);
				#endregion

				List<UDI.SDS.ReservationsService.PickupTimeRecord> pickupTimes = new List<UDI.SDS.ReservationsService.PickupTimeRecord>();

				if (rates != null && rates.Any())
				{
					foreach (var rate in rates)
					{

						#region Validate PickupTime
						//var serviceInfo = new ServiceInfo{ FareID = rate.FareID, FleetType = rate.Fleet, ServiceType = rate.ServiceTypeCode };
						//pickupTimes = reservationController.GetAirportPickupTimes(request.Segments.First() /* there is just one segment for charter */, serviceInfo, rate.BookRequestID, 0);

						if (pickMeUpNow)
						{
							#region Make pickMeUpNowService Request
							var pickMeUpNowServiceRequest = new UDI.SDS.UtilityService.PickMeUpNowServiceRequest();
							pickMeUpNowServiceRequest.Address = groundAvailRQ.Service.Pickup.Address.AddressLine;
							pickMeUpNowServiceRequest.City = groundAvailRQ.Service.Pickup.Address.CityName;
							pickMeUpNowServiceRequest.Latitude = !string.IsNullOrWhiteSpace(groundAvailRQ.Service.Pickup.Address.Latitude) ? groundAvailRQ.Service.Pickup.Address.Latitude.ToDouble() : 0;
							pickMeUpNowServiceRequest.Longitude = !string.IsNullOrWhiteSpace(groundAvailRQ.Service.Pickup.Address.Longitude) ? groundAvailRQ.Service.Pickup.Address.Longitude.ToDouble() : 0;
							pickMeUpNowServiceRequest.State = groundAvailRQ.Service.Pickup.Address.StateProv.StateCode;

							if (request.Segments.First().RequestedFleet == SDSFleet.ExecuCar)
								pickMeUpNowServiceRequest.VehicleFleet = (int)UDI.VTOD.Common.DTO.Enum.VehicleType.ExecuCar;
							else if (request.Segments.First().RequestedFleet == SDSFleet.Taxi)
								pickMeUpNowServiceRequest.VehicleFleet = (int)UDI.VTOD.Common.DTO.Enum.VehicleType.Taxi;
							else
								pickMeUpNowServiceRequest.VehicleFleet = (int)UDI.VTOD.Common.DTO.Enum.VehicleType.Shuttle;


							pickMeUpNowServiceRequest.Zip = groundAvailRQ.Service.Pickup.Address.PostalCode;
							if (groundAvailRQ.TPA_Extensions != null && !string.IsNullOrWhiteSpace(groundAvailRQ.TPA_Extensions.MemberID))
							{
								pickMeUpNowServiceRequest.MemberId = groundAvailRQ.TPA_Extensions.MemberID.ToInt32();
							}
							#endregion

							var pickupTime = reservationController.ValidateCharterPickupMeUpNowTime(pickMeUpNowServiceRequest, groundAvailRQ.Service.Pickup.DateTime.ToDateTime().Value);
							if (pickupTime != null)
							{
								pickupTimes.Add(pickupTime);
							}
						}
						else
						{
							pickupTimes = reservationController.GetCharterPickupTimes(rate.WebAdvanceNoticeMinutes, rate.FranchiseCode, request.Segments.First().RequestPickupTime, Method.GroundAvail);
						}

						#endregion

						#region Adjust Gratuity
						if (gratuity > 0 && gratuityRate <= 0)
						{
							rate.Gratuity = gratuity;
						}
						#endregion

						if (pickupTimes != null && pickupTimes.Any())
						{
							var groundService = sdsObjectFactory.GetGroundServiceObjectForCharter(tokenVTOD, rate, pickupTimes.First() /* there is just one pickupTime for charter */, groundAvailRQ);

							response.GroundServices.GroundServices.Add(groundService);
						}
					}
				}

				else
				{
					throw Common.DTO.VtodException.CreateFieldRequiredValidationException("Reference(FareID)"); // throw new Exception(string.Format(Messages.VTOD_SDSDomain_NoFare, "GroundAvail"));
				}

				response.Success = new Success();

				return response;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("SDSDomain.GroundAvail:: {0}", ex.ToString() + ex.StackTrace);
				throw;

				#region Throw SDSException || Validation Exception
				//if (ex.GetType() == typeof(ValidationException))
				//{
				//	throw new ValidationException(ex.Message);
				//}
				//else
				//{
				//	if (ex.Message.Contains("::"))//Remove method name
				//		throw new SDSException(ex.Message.Substring(ex.Message.IndexOf("::") + 2));
				//	else
				//		throw new SDSException(ex.Message);
				//}

				#endregion
			}
		}

		private OTA_GroundBookRS groundBook_Charter(TokenRS tokenVTOD, OTA_GroundBookRQ groundBookRQ, out List<Tuple<string, int, long>> disptach_Rez_VTOD, TripType tripType, long? vtodTripID, int fleetTripCode)
		{
			var request = new BookRequest();
			disptach_Rez_VTOD = new List<Tuple<string, int, long>>();

			try
			{
				#region Modification
				#region Modify phone numbers
				if (groundBookRQ != null && groundBookRQ.GroundReservations != null && groundBookRQ.GroundReservations.Any() && groundBookRQ.GroundReservations.First().Passenger != null && groundBookRQ.GroundReservations.First().Passenger.Primary != null && groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones != null && groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones.Any())
				{
					groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones.First().CountryAccessCode = string.IsNullOrWhiteSpace(groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones.First().CountryAccessCode) ? groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones.First().CountryAccessCode : groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones.First().CountryAccessCode.CleanPhone().CleanCountryCode();
					groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones.First().AreaCityCode = string.IsNullOrWhiteSpace(groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones.First().AreaCityCode) ? groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones.First().AreaCityCode : groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones.First().AreaCityCode.CleanPhone();
					groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones.First().PhoneNumber = string.IsNullOrWhiteSpace(groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones.First().PhoneNumber) ? groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones.First().PhoneNumber : groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones.First().PhoneNumber.CleanPhone();
				}
				#endregion
				#endregion

				#region Validation
                groundBookRQ.Username = tokenVTOD.Username;
				groundBookRQ.Validate();
				#endregion

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Validation");
				#endregion

				OTA_GroundBookRS response = new OTA_GroundBookRS();

				#region Init
				UDI.SDS.ReservationsController reservationController = new UDI.SDS.ReservationsController(tokenVTOD, TrackTime);
				var rateController = new RateController(tokenVTOD, TrackTime);
				var geocoderController = new GeocoderController(tokenVTOD, TrackTime);
				var landmarksController = new LandmarksController(tokenVTOD, TrackTime);
				var asapController = new ASAPController(tokenVTOD, TrackTime);
				var sdsObjectFactory = new SDSObjectFactory();
				#endregion

				#region Init Response
				response.EchoToken = groundBookRQ.EchoToken;
				response.Target = groundBookRQ.Target;
				response.Version = groundBookRQ.Version;
				response.PrimaryLangID = groundBookRQ.PrimaryLangID;
				response.Reservations = new List<Common.DTO.OTA.Reservation>();
				#endregion

				if (
					(groundBookRQ.GroundReservations != null && groundBookRQ.TPA_Extensions != null && groundBookRQ.References != null)
					||
					(tripType == TripType.Taxi)
					)
				{
					var groundReservation = groundBookRQ.GroundReservations.FirstOrDefault();

					#region Init
					CharterServiceTypeRecord foundRate = null;
					var foundPickupTime = new PickupTimeResponse();
					bool findMatchingRateFlag = false;
					bool findMatchPickupTimeFlag = false;
					#endregion

					#region Set SDS Component BookRequest
					request.RequestUtcTime = DateTime.UtcNow;
					#endregion

					#region SetSegments
					request.Segments = sdsObjectFactory.GetSegmentsWithGroundReservation(groundReservation, groundBookRQ, null);

                    #endregion
                    //Adding ChildSeats and InfantSeats which Doug has asked
                    foreach (var segment in request.Segments)
                    {
                        segment.IsChildSeats = groundBookRQ.TPA_Extensions.ChildSeats;
                        segment.IsInfantSeats = groundBookRQ.TPA_Extensions.InfantSeats;
                    }

                    #region GeoCode the addresses
                    if (request.Segments != null && request.Segments.Any())
					{
						foreach (var segment in request.Segments)
						{
							foreach (var location in segment.Locations)
							{
								if (location != null)
								{
									if (location.Latitude == 0 || location.Longitude == 0)
									{
										var modified = false;
										geocoderController.ModifyAddress(location, out modified);
									}
								}
							}
						}
					}
					#endregion

					#region Landmark
					if (request.Segments != null && request.Segments.Any())
					{
						foreach (var segment in request.Segments)
						{
							foreach (var location in segment.Locations)
							{
								UDI.SDS.LandmarksService.LandmarkRecord landmarkRecord = null;
								if (location.Latitude == 0 || location.Longitude == 0)
								{
									landmarkRecord = landmarksController.GetLandmarkRecordByAddress(string.Empty, location);
								}
								else
								{
									landmarkRecord = landmarksController.GetLandmarksByCoordinates(string.Empty, location);
								}

								//if (landmarkRecord != null)
								//{
								//we do not have subzip for charter. //subZipName = landmarkRecord.ToSubZipName();
								location.LocationType = landmarkRecord.ToLocationType();
								location.LocationName = landmarkRecord.ToLocationName();
                                #region Override LocationName with the GroundBook LocationName
                                if (string.IsNullOrEmpty(location.LocationName) && string.IsNullOrWhiteSpace(location.LocationName))
                                {
                                   if(location!=null)
                                   {
                                       if (!string.IsNullOrEmpty(location.PropertyName))
                                       {
                                           location.LocationName = location.PropertyName;
                                       }
                                   }
                                } 
                                #endregion
								location.MasterLandmarkID = landmarkRecord.ToMasterLandmarkID();
								location.LandmarkID = landmarkRecord.ToLandmarkID();
								location.SubZipName = landmarkRecord.ToSubZipName();
								//}
							}
						}
					}
					#endregion

					#region Set ReservationRequest
					request.ReservationRequest = sdsObjectFactory.GetReservationRequestObject(tokenVTOD, groundBookRQ);
					#endregion

					#region Get DiscountCode
					var discountCode = (groundReservation.RateQualifiers != null
															&& groundReservation.RateQualifiers.FirstOrDefault().PromotionCode != null
															) ? groundReservation.RateQualifiers.FirstOrDefault().PromotionCode : string.Empty;
					#endregion

					#region Get UserInfo
					//UserInfo userInfo = getUserInfo(tokenVTOD.Username);
					//rateController.UserInfo = userInfo;
					#endregion

					#region PickMeUpNow
					var pickMeUpNow = false;

					if (groundBookRQ.TPA_Extensions != null && groundBookRQ.TPA_Extensions.PickMeUpNow.HasValue)
					{
						pickMeUpNow = groundBookRQ.TPA_Extensions.PickMeUpNow.Value;
					}
					#endregion

					#region GratuityFix and GratuityRate
					decimal gratuityRate = 0;
					decimal gratuity = 0;
					try
					{
						if (groundBookRQ.GroundReservations.First().RateQualifiers.First().SpecialInputs != null && groundBookRQ.GroundReservations.First().RateQualifiers.First().SpecialInputs.Any())
						{
							var gratuities = groundBookRQ.GroundReservations.First().RateQualifiers.First().SpecialInputs.Where(s => s.Name.ToLower() == "gratuity").ToList();
							if (gratuities != null && gratuities.Any())
							{
								var gratuityElementValue = gratuities.First().Value;
								if (gratuityElementValue.Contains("%"))
								{
									gratuityRate = gratuityElementValue.GetPercentageAmount();
								}
								else
								{
									gratuity = gratuityElementValue.ToDecimal();
								}
							}
						}
					}
					catch
					{ }
					#endregion

					#region Get sds_user_subfleet (Subfleet Permission)
					Dictionary<string, bool> subfleetPermission = null;
					using (var db = new VTODEntities())
					{
						subfleetPermission = db.sds_user_subfleet.Where(s => s.my_aspnet_users.name == tokenVTOD.Username).ToDictionary(s => s.subfleet, s => s.Allow);
					}
					#endregion

					#region Currency
					var currency = Common.DTO.Enum.Currency.USD;

					if (groundBookRQ.POS != null && groundBookRQ.POS.Source != null && !string.IsNullOrEmpty(groundBookRQ.POS.Source.ISOCurrency))
					{
						currency = groundBookRQ.POS.Source.ISOCurrency.ToCurrency();
					}
					#endregion

					#region GetRates
					//We do not need to pass MeetAndGreetInd in the book. Customer should pass it in Get Rate and if he pass the correct RateID, we know it is MeetAndGreet
					var rates = rateController.GetLocationToLocationRate(request.Segments, subfleetPermission, discountCode, pickMeUpNow, null, gratuityRate, currency);
					#endregion

					#region Match RateID -> Get Pickup Times
					var rateFleet = string.Empty;//Used for Pickup Time Matching

					//This part is just a workaround for backward compatibility as zTrip app is sending hardcoded ref# = 1
					//string[] sources = new string[] { "zTrip 2.3.1", "zTrip 2.3.3", "zTrip 2.3.2", "zTrip 2.3", "zTrip 2.2", "zTrip", "zTrip first version", "zTrip 2.1" };

					if (
							(
							tripType == TripType.Taxi
							&&
							(groundBookRQ.References == null || !groundBookRQ.References.Any() || string.IsNullOrWhiteSpace(groundBookRQ.References.First().ID))
							&&
							(rates != null && rates.Any())
							)
						||
							//This part is just a workaround for backward compatibility as zTrip app is sending hardcoded ref# = 1
							(
							tripType == TripType.Taxi
							&&
							(groundBookRQ.References != null && groundBookRQ.References.Any() && !string.IsNullOrWhiteSpace(groundBookRQ.References.First().ID) && groundBookRQ.References.First().ID == "1")
							//&&
							//(groundBookRQ.TPA_Extensions != null && !string.IsNullOrWhiteSpace(groundBookRQ.TPA_Extensions.Source))
							//&&
							//(sources.Contains(groundBookRQ.TPA_Extensions.Source))
							&&
							(rates != null && rates.Any())
							)
						)
					{
						foundRate = rates.First();

						#region Adjust Gratuity
						if (gratuity > 0 && gratuityRate <= 0)
						{
							foundRate.Gratuity = gratuity;
						}
						#endregion

						findMatchingRateFlag = true;
						rateFleet = foundRate.Fleet;

					}
					else
					{

						//if (firstSegmentRate.ServiceInfoRates != null)
						//{
						#region Get Pickuptimes
						foreach (var rate in rates)
						{
							if (rate.FareID.ToString().Trim().ToLower() == groundBookRQ.References.First().ID.ToLower().Trim())
							{

								foundRate = rate;

								#region Adjust Gratuity
								if (gratuity > 0 && gratuityRate <= 0)
								{
									foundRate.Gratuity = gratuity;
								}
								#endregion


								findMatchingRateFlag = true;
								rateFleet = rate.Fleet;

								break;
							}
						}
						#endregion
						//} 
					}

					#region Throw exception if no matching rate found
					if (!findMatchingRateFlag)
						throw VtodException.CreateException(ExceptionType.SDS, 2003); //throw new Exception(string.Format(Messages.VTOD_SDSDomain_GroundBook_NoMatchingRateFound, "GroundBook"));
					#endregion
					#endregion

					#region ASAP
					int? asapRequestID = null;

					if (groundBookRQ.TPA_Extensions.Confirmations != null && groundBookRQ.TPA_Extensions.Confirmations.Where(s => s.Type.ToLower() == "asapconfirmation").Any())
					{
						var confirmations = groundBookRQ.TPA_Extensions.Confirmations.Where(s => s.Type.ToLower() == "asapconfirmation").ToList();
						if (confirmations != null && confirmations.Any())
						{
							asapRequestID = confirmations.First().ID.ToInt32();
						}
					}
					#endregion

					#region Match PickupTime

					if (asapRequestID.HasValue)
					{
						var newAsapStatus = asapController.GetASAPRequestStatus(asapRequestID.Value);
						if (newAsapStatus.DispositionCode == ASAPDispositionCodesEnumeration.NewPickupTimeApproved || newAsapStatus.DispositionCode == ASAPDispositionCodesEnumeration.OriginalPickupTimeApproved)
						{
							if (request.Segments.First().RequestPickupTime != newAsapStatus.ApprovedPickupTime)
							{
								throw Common.DTO.VtodException.CreateException(ExceptionType.SDS, 2004);
							}
							foundPickupTime = new PickupTimeResponse();
							foundPickupTime.AsapRequestId = asapRequestID.Value;
							foundPickupTime.AvailablePickupTime = newAsapStatus.ApprovedPickupTime;
							foundPickupTime.IsGuaranteed = newAsapStatus.ServiceIsAOR;
							foundPickupTime.AvailablePickupEndTime = newAsapStatus.ApprovedPickupTime.AddMinutes(15);
						}
						else
						{
							throw Common.DTO.VtodException.CreateException(ExceptionType.SDS, 2004);
						}
					}
					else
					{
						List<UDI.SDS.ReservationsService.PickupTimeRecord> pickupTimes = new List<UDI.SDS.ReservationsService.PickupTimeRecord>();

						#region Validate PickupTime

						if (pickMeUpNow)
						{
							pickupTimes = new List<UDI.SDS.ReservationsService.PickupTimeRecord>();

							//try
							//{
							#region Make pickMeUpNowService Request
							var pickMeUpNowServiceRequest = new UDI.SDS.UtilityService.PickMeUpNowServiceRequest();
							pickMeUpNowServiceRequest.Address = groundBookRQ.GroundReservations.First().Service.Location.Pickup.Address.AddressLine;
							pickMeUpNowServiceRequest.City = groundBookRQ.GroundReservations.First().Service.Location.Pickup.Address.CityName;
							pickMeUpNowServiceRequest.Latitude = !string.IsNullOrWhiteSpace(groundBookRQ.GroundReservations.First().Service.Location.Pickup.Address.Latitude) ? groundBookRQ.GroundReservations.First().Service.Location.Pickup.Address.Latitude.ToDouble() : 0;
							pickMeUpNowServiceRequest.Longitude = !string.IsNullOrWhiteSpace(groundBookRQ.GroundReservations.First().Service.Location.Pickup.Address.Longitude) ? groundBookRQ.GroundReservations.First().Service.Location.Pickup.Address.Longitude.ToDouble() : 0;
							pickMeUpNowServiceRequest.State = groundBookRQ.GroundReservations.First().Service.Location.Pickup.Address.StateProv.StateCode;

							if (tripType == TripType.Taxi)
								pickMeUpNowServiceRequest.VehicleFleet = (int)UDI.VTOD.Common.DTO.Enum.VehicleType.Taxi;
							else
								pickMeUpNowServiceRequest.VehicleFleet = (int)UDI.VTOD.Common.DTO.Enum.VehicleType.ExecuCar;

							pickMeUpNowServiceRequest.Zip = groundBookRQ.GroundReservations.First().Service.Location.Pickup.Address.PostalCode;
							if (groundBookRQ.TPA_Extensions != null && !string.IsNullOrWhiteSpace(groundBookRQ.TPA_Extensions.MemberID))
							{
								pickMeUpNowServiceRequest.MemberId = groundBookRQ.TPA_Extensions.MemberID.ToInt32();
							}
							#endregion

							var pickupTime = reservationController.ValidateCharterPickupMeUpNowTime(pickMeUpNowServiceRequest, groundBookRQ.GroundReservations.First().Service.Location.Pickup.DateTime.ToDateTime().Value);
							pickupTimes.Add(pickupTime);
							//}
							//catch (Exception ex)
							//{
							//	logger.Error(ex);
							//}
						}
						else
						{
							pickupTimes = reservationController.GetCharterPickupTimes(foundRate.WebAdvanceNoticeMinutes, foundRate.FranchiseCode, request.Segments.First().RequestPickupTime, Method.GroundBook);
						}



						#endregion

						if (pickupTimes != null && pickupTimes.Any())
						{
							foreach (var pickupTime in pickupTimes)
							{
								if (pickupTime.StartTime == request.Segments.First().RequestPickupTime)
								{
									if (pickupTime.Status == UDI.SDS.ReservationsService.PickupTimeStatusCodesEnumeration.PickupExpired)
									{
										throw Common.DTO.VtodException.CreateException(ExceptionType.SDS, 2009);// new Exception(string.Format("{0}: {1}", pickupTime.Status.ToString(), Messages.Validation_ExpiredPickupTime));
									}
									if (pickupTime.Status != UDI.SDS.ReservationsService.PickupTimeStatusCodesEnumeration.AllowPickup)
									{
										throw VtodException.CreateException(ExceptionType.SDS, 2005);// new Exception(string.Format("{0}: {1}", pickupTime.Status, Messages.UDI_SDS_Invalid_PickupTime_StatusCode));
									}

									foundPickupTime.AvailablePickupEndTime = pickupTime.EndTime;
									foundPickupTime.AvailablePickupTime = pickupTime.StartTime;
									//pickupTimeResponse.BookRequestID = foundRate.BookRequestID;
									foundPickupTime.IsGuaranteed = pickupTime.IsGuaranteedTime;
									foundPickupTime.IsScheduledStop = pickupTime.IsScheduledStop;

									findMatchPickupTimeFlag = true;

									break;
								}
							}
						}
						else
						{
							throw Common.DTO.VtodException.CreateException(ExceptionType.SDS, 1004); //new Exception(string.Format(Messages.VTOD_SDSDomain_NoAvailablePickupTimes, "GroundBook"));
						}

						#region Throw exception if no matching pickup time found
						if (!findMatchPickupTimeFlag)
							throw VtodException.CreateException(ExceptionType.SDS, 2003); //throw new Exception(string.Format(Messages.VTOD_SDSDomain_GroundBook_NoMatchingRateFound, "GroundBook"));
						#endregion

						//} 
					}
					#endregion

					#region Affiliate
					request.ReservationRequest.AffiliateInfo = new AffiliateInfo();
					if (!string.IsNullOrWhiteSpace(groundBookRQ.EchoToken))
					{
						request.ReservationRequest.AffiliateInfo.TrackingNumber = groundBookRQ.EchoToken;
					}
					try
					{
						var specialInputs = groundBookRQ.GroundReservations.First().RateQualifiers.First().SpecialInputs;
						if (specialInputs != null && specialInputs.Any())
						{
							var affiliateUserIDObj = specialInputs.Where(s => s.Name.ToLower() == "affiliateuserid").FirstOrDefault();
							if (affiliateUserIDObj != null)
							{
								request.ReservationRequest.AffiliateInfo.UserID = affiliateUserIDObj.Value;
							}

							var affiliateEmailAddressObj = specialInputs.Where(s => s.Name.ToLower() == "affiliateemailaddress").FirstOrDefault();
							if (affiliateEmailAddressObj != null)
							{
								request.ReservationRequest.AffiliateInfo.Email = affiliateEmailAddressObj.Value;
							}
						}
					}
					catch { }

                    #endregion

                    #region ReserveCsr
                    if (groundBookRQ.Username.ToLower() == "Rez".ToLower())
                    {
                        if (groundBookRQ != null && groundBookRQ.TPA_Extensions != null && !string.IsNullOrEmpty(groundBookRQ.TPA_Extensions.ReserveCsr))
                            request.ReservationRequest.ReserveCsr = groundBookRQ.TPA_Extensions.ReserveCsr;
                        else
                            request.ReservationRequest.ReserveCsr = string.Empty;
                    }
                    else
                    {
                        request.ReservationRequest.ReserveCsr = groundBookRQ.Username;
                    }
                    #endregion
                    #region "CultureCode"
                    if (groundBookRQ != null && !string.IsNullOrEmpty(groundBookRQ.PrimaryLangID))
                        request.ReservationRequest.CultureCode = groundBookRQ.PrimaryLangID;
                    #endregion
                    #region Book
                    var bookResponse = reservationController.BookCharter(request.Segments.First(), request.ReservationRequest, foundRate, foundPickupTime);

					//var xxx = bookResponse.XmlSerialize();
					#endregion

					#region Save to Database
					//We do not save any information in this domain for Taxi trips. They are being saved in Taxi domain
					var reservationResponse = bookResponse.ReservationResponses.FirstOrDefault();
					var rezID = reservationResponse.CharterReservationConfirmation.RezID;
					var dispatchConfirmatinoNumber = !string.IsNullOrWhiteSpace(reservationResponse.CharterReservationConfirmation.ConfirmationNumber) ? reservationResponse.CharterReservationConfirmation.ConfirmationNumber : reservationResponse.AirportReservationConfirmations.FirstOrDefault().ConfirmationNumber;

					if (tripType != TripType.Taxi)
					{
						using (var db = new VTODEntities())
						{

							/* Save to SDS_Trip table */
							request.FleetTripCode = fleetTripCode;
							var tbl_tripBooking = sdsObjectFactory.GetSDS_TripObject(request, bookResponse, request.Segments.FirstOrDefault(), groundBookRQ, dispatchConfirmatinoNumber);

							#region Modify TotalFareAmount, Pickmeupnow, ...
							if (foundRate != null)
							{
								if (foundRate.Fare > 0)
								{
									tbl_tripBooking.TotalFareAmount = foundRate.Fare;
								}
							}
							tbl_tripBooking.PickMeUpNow = pickMeUpNow;
							#endregion

							#region Set Payment Type
							tbl_tripBooking.PaymentType = Common.DTO.Enum.PaymentType.Unknown.ToString();
							if (groundBookRQ.Payments != null && groundBookRQ.Payments.Payments != null && groundBookRQ.Payments.Payments.Any())
							{
								if (groundBookRQ.Payments.Payments.First().PaymentCard != null)
								{
									tbl_tripBooking.PaymentType = Common.DTO.Enum.PaymentType.PaymentCard.ToString();
								}
								else if (groundBookRQ.Payments.Payments.First().DirectBill != null)
								{
									tbl_tripBooking.PaymentType = Common.DTO.Enum.PaymentType.DirectBill.ToString();
								}
								//We do not have Cash for SDS
								//if (groundBookRQ.Payments.Payments.First().Cash != null && groundBookRQ.Payments.Payments.First().Cash.CashIndicator == true)
								//{
								//	tbl_tripBooking.PaymentType = Common.DTO.Enum.PaymentType.Cash.ToString();
								//}
							}
							#endregion
                            
							#region GetUser
							var user = db.my_aspnet_users.Where(s => s.name.ToLower() == tokenVTOD.Username.ToLower()).ToList().FirstOrDefault();
							#endregion

							#region Set UserID
							tbl_tripBooking.UserID = user.id;
							#endregion

							#region Set Ref, Source, Device
							if (groundBookRQ.TPA_Extensions != null && !string.IsNullOrWhiteSpace(groundBookRQ.TPA_Extensions.Device))
							{
								tbl_tripBooking.ConsumerDevice = groundBookRQ.TPA_Extensions.Device;
							}
                            else
                            {
                                if (user != null)
                                {
                                    tbl_tripBooking.ConsumerDevice = user.name;
                                }
                            }
							if (groundBookRQ.TPA_Extensions != null && !string.IsNullOrWhiteSpace(groundBookRQ.TPA_Extensions.Source))
							{								tbl_tripBooking.ConsumerSource = groundBookRQ.TPA_Extensions.Source;
							}
							if (!string.IsNullOrWhiteSpace(groundBookRQ.EchoToken))
							{
								tbl_tripBooking.ConsumerRef = groundBookRQ.EchoToken;
							}
							#endregion

							#region Set server booking time
							tbl_tripBooking.BookingServerDateTime = System.DateTime.Now;
							#endregion

							#region PromisedETA
							try
							{
								var rateQuialifier = groundBookRQ.GroundReservations.First().RateQualifiers.First();
								if (rateQuialifier.SpecialInputs != null)
								{
									var promisedetas = rateQuialifier.SpecialInputs.Where(s => s.Name.ToLower() == "promisedeta").ToList();

									if (promisedetas != null && promisedetas.Any())
									{
										tbl_tripBooking.PromisedETA = promisedetas.First().Value.ToInt32();
									}
								}
							}
							catch (Exception ex)
							{
								logger.Error("Taxi:PromisedETA", ex);
							}
							#endregion

							#region Status
							tbl_tripBooking.FinalStatus = "Booked";
							#endregion

							#region Track
							if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
							#endregion

							db.vtod_trip.Add(tbl_tripBooking);

							db.SaveChanges();

							#region Track
							if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_DB, "db.vtod_trip.Add(tbl_tripBooking);");
							#endregion

							//#region Insert Trip Transaction Info
							//#region Track
							//if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
							//#endregion
							//var source = tokenVTOD.Username;
							//if (groundBookRQ.TPA_Extensions != null && !string.IsNullOrWhiteSpace(groundBookRQ.TPA_Extensions.Source))
							//{
							//	source = groundBookRQ.TPA_Extensions.Source;
							//}

							//var device = tokenVTOD.Username;
							//if (groundBookRQ.TPA_Extensions != null && !string.IsNullOrWhiteSpace(groundBookRQ.TPA_Extensions.Device))
							//{
							//	device = groundBookRQ.TPA_Extensions.Device;
							//}

							//db.SP_vtod_InsertTripTransactionInfo(tbl_tripBooking.Id, tokenVTOD.Username, source, device, DateTime.Now);

							//#region Track
							//if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_DB, "SP_vtod_InsertTripTransactionInfo");
							//#endregion
							//#endregion

							/* Remove payment from request */
							request.ReservationRequest.Payment = null;

							/* Save to Log table */
							//db.SP_SDS_InsertLog(vtodTXId, UDI.SDS.DTO.Enum.LogType.Book.ToString(), null, request.BookRequestID.ToString(), request.XmlSerialize().ToString(), bookResponse.XmlSerialize().ToString());

							#region Make sds_VTOD_TripIDs
							//This is for out parameter. Do not delete it.
							if (tbl_tripBooking != null && tbl_tripBooking.Id > 0 && !string.IsNullOrWhiteSpace(dispatchConfirmatinoNumber))
								//disptach_VTOD_TripIDs.Add(rezID.ToString(), tbl_tripBooking.Id);
								disptach_Rez_VTOD.Add(new Tuple<string, int, long>(dispatchConfirmatinoNumber, rezID, tbl_tripBooking.Id));
							#endregion

						}
					}
					else
					{
						//disptach_VTOD_TripIDs.Add(rezID.ToString(), vtodTripID.Value);
						disptach_Rez_VTOD.Add(new Tuple<string, int, long>(dispatchConfirmatinoNumber, rezID, vtodTripID.Value));
					}

					#endregion

					#region Set OTA_GroundBookRS

					var OTAreservation = sdsObjectFactory.GetReservationObject(bookResponse, groundReservation, disptach_Rez_VTOD);

					response.Reservations.Add(OTAreservation);

					#endregion
				}
				else
				{
					throw VtodException.CreateFieldRequiredValidationException("GroundReservations|TPA_Extensions|Reference"); // new Exception(string.Format(Messages.VTOD_SDSDomain_GroundBookRQ_MissingData, "GroundBook"));
				}

				response.Success = new Success();

				return response;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("SDSDomain.GroundBook:: {0}", ex.ToString() + ex.StackTrace);
				throw;
			}
		}
		#endregion

		#region Hourly
		private OTA_GroundAvailRS groundAvail_Hourly(TokenRS tokenVTOD, OTA_GroundAvailRQ groundAvailRQ)
		{
			var request = new BookRequest();

			try
			{
				#region Init
				var reservationController = new UDI.SDS.ReservationsController(tokenVTOD, TrackTime);
				var rateController = new RateController(tokenVTOD, TrackTime);
				var landmarksController = new LandmarksController(tokenVTOD, TrackTime);
				var geocoderController = new GeocoderController(tokenVTOD, TrackTime);
				var utilityController = new UtilityController(tokenVTOD, TrackTime);
				var sdsObjectFactory = new SDSObjectFactory();
				#endregion

				#region Modification
				#endregion

				#region Validation
				groundAvailRQ.ValidateHourly();
				#endregion

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Validation");
				#endregion

				OTA_GroundAvailRS response = new OTA_GroundAvailRS();

				#region Init Respose
				response.EchoToken = groundAvailRQ.EchoToken;
				response.Target = groundAvailRQ.Target;
				response.Version = groundAvailRQ.Version;
				response.PrimaryLangID = groundAvailRQ.PrimaryLangID;
				//request.RequestUtcTime = DateTime.UtcNow;
				response.GroundServices = new GroundServiceList();
				response.GroundServices.GroundServices = new List<GroundService>();
				#endregion


				#region GratuityFix and GratuityRate
				decimal gratuityRate = 0;
				decimal gratuity = 0;
				try
				{
					if (groundAvailRQ.RateQualifiers.First().SpecialInputs != null && groundAvailRQ.RateQualifiers.First().SpecialInputs.Any())
					{
						var gratuities = groundAvailRQ.RateQualifiers.First().SpecialInputs.Where(s => s.Name.ToLower() == "gratuity").ToList();
						if (gratuities != null && gratuities.Any())
						{
							var gratuityElementValue = gratuities.First().Value;
							if (gratuityElementValue.Contains("%"))
							{
								gratuityRate = gratuityElementValue.GetPercentageAmount();
							}
							else
							{
								gratuity = gratuityElementValue.ToDecimal();
							}
						}
					}
				}
				catch
				{ }
				#endregion

				#region GetRates
				#region input parameters
				var fleetType = groundAvailRQ.RateQualifiers.First().RateQualifierValue.ToSDSFleet();
				var zipCode = groundAvailRQ.Service.Pickup.Address.PostalCode;
				int minute = groundAvailRQ.RateQualifiers.First().SpecialInputs.Where(s => s.Name.Trim().ToLower() == "minute").First().Value.ToDecimal().RoundUp(10);

				#region wheelChairs
				var wheelChairs = 0;
				if (groundAvailRQ.DisabilityInfo != null)
				{
					wheelChairs = groundAvailRQ.DisabilityInfo.RequiredInd ? 1 : 0;
				}
				#endregion

				#region Pax
				var payingPax = 1;
				var freePax = 0;

				if (groundAvailRQ.Passengers != null)
				{
					foreach (var pax in groundAvailRQ.Passengers)
					{
						if (pax.Category != null && pax.Category.Value.ToUpper().Equals("ADULT"))
						{
							payingPax = pax.Quantity;
						}
						else
						{
							freePax = pax.Quantity;
						}
					}
				}
				#endregion
				#endregion

				var rates = rateController.GetHourlyRate(fleetType, zipCode, minute, payingPax, wheelChairs, freePax);
				#endregion

				List<UDI.SDS.ReservationsService.PickupTimeRecord> pickupTimes = new List<UDI.SDS.ReservationsService.PickupTimeRecord>();

				if (rates != null && rates.Any())
				{
					foreach (var rate in rates)
					{

						#region Adjust Gratuity
						if (gratuity > 0 && gratuityRate <= 0)
						{
							rate.Gratuity = gratuity;
						}
						#endregion

						//if (pickupTimes != null && pickupTimes.Any())
						//{
						var groundService = sdsObjectFactory.GetGroundServiceObjectForHourly(tokenVTOD, rate, groundAvailRQ);

						response.GroundServices.GroundServices.Add(groundService);
						//}
					}
				}

				else
				{
					throw Common.DTO.VtodException.CreateFieldRequiredValidationException("Reference(FareID)"); // throw new Exception(string.Format(Messages.VTOD_SDSDomain_NoFare, "GroundAvail"));
				}

				response.Success = new Success();

				return response;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("SDSDomain.GroundAvail:: {0}", ex.ToString() + ex.StackTrace);
				throw;

				#region Throw SDSException || Validation Exception
				//if (ex.GetType() == typeof(ValidationException))
				//{
				//	throw new ValidationException(ex.Message);
				//}
				//else
				//{
				//	if (ex.Message.Contains("::"))//Remove method name
				//		throw new SDSException(ex.Message.Substring(ex.Message.IndexOf("::") + 2));
				//	else
				//		throw new SDSException(ex.Message);
				//}

				#endregion
			}
		}

		private OTA_GroundBookRS groundBook_Hourly(TokenRS tokenVTOD, OTA_GroundBookRQ groundBookRQ, out List<Tuple<string, int, long>> disptach_Rez_VTOD, int FleetTripCode)
		{
			var request = new BookRequest();
			disptach_Rez_VTOD = new List<Tuple<string, int, long>>();

			try
			{
				#region Modification
				#region Modify phone numbers
				if (groundBookRQ != null && groundBookRQ.GroundReservations != null && groundBookRQ.GroundReservations.Any() && groundBookRQ.GroundReservations.First().Passenger != null && groundBookRQ.GroundReservations.First().Passenger.Primary != null && groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones != null && groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones.Any())
				{
					groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones.First().CountryAccessCode = string.IsNullOrWhiteSpace(groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones.First().CountryAccessCode) ? groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones.First().CountryAccessCode : groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones.First().CountryAccessCode.CleanPhone().CleanCountryCode();
					groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones.First().AreaCityCode = string.IsNullOrWhiteSpace(groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones.First().AreaCityCode) ? groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones.First().AreaCityCode : groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones.First().AreaCityCode.CleanPhone();
					groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones.First().PhoneNumber = string.IsNullOrWhiteSpace(groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones.First().PhoneNumber) ? groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones.First().PhoneNumber : groundBookRQ.GroundReservations.First().Passenger.Primary.Telephones.First().PhoneNumber.CleanPhone();
				}
				#endregion
				#endregion

				#region Validation
                groundBookRQ.Username = tokenVTOD.Username;
				groundBookRQ.Validate();
				#endregion

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Validation");
				#endregion

				OTA_GroundBookRS response = new OTA_GroundBookRS();

				#region Init
				UDI.SDS.ReservationsController reservationController = new UDI.SDS.ReservationsController(tokenVTOD, TrackTime);
				var rateController = new RateController(tokenVTOD, TrackTime);
				var geocoderController = new GeocoderController(tokenVTOD, TrackTime);
				var landmarksController = new LandmarksController(tokenVTOD, TrackTime);
				var asapController = new ASAPController(tokenVTOD, TrackTime);
				var sdsObjectFactory = new SDSObjectFactory();
				#endregion

				#region Init Response
				response.EchoToken = groundBookRQ.EchoToken;
				response.Target = groundBookRQ.Target;
				response.Version = groundBookRQ.Version;
				response.PrimaryLangID = groundBookRQ.PrimaryLangID;
				response.Reservations = new List<Common.DTO.OTA.Reservation>();
				#endregion


				if (groundBookRQ.GroundReservations != null && groundBookRQ.TPA_Extensions != null && groundBookRQ.References != null)
				{
					var groundReservation = groundBookRQ.GroundReservations.FirstOrDefault();


					#region Init
					CharterServiceTypeRecord foundRate = null;
					var foundPickupTime = new PickupTimeResponse();
					bool findMatchingRateFlag = false;
					bool findMatchPickupTimeFlag = false;

					var fleetType = groundReservation.RateQualifiers.First().RateQualifierValue.ToSDSFleet();
					var zipCode = groundReservation.Service.Location.Pickup.Address.PostalCode;
					int minute = groundReservation.RateQualifiers.First().SpecialInputs.Where(s => s.Name.Trim().ToLower() == "minute").First().Value.ToDecimal().RoundUp(10);

					#region wheelChairs
					var wheelChairs = 0;
					//if (groundReservation.Service.DisabilityVehicleInd)
					//{
					wheelChairs = groundReservation.Service.DisabilityVehicleInd ? 1 : 0;
					//}
					#endregion

					#region Pax
					var payingPax = 1;
					var freePax = 0;

					if (groundBookRQ.TPA_Extensions != null && groundBookRQ.TPA_Extensions.Passengers != null)
					{
						foreach (var pax in groundBookRQ.TPA_Extensions.Passengers)
						{
							if (pax.Category != null && pax.Category.Value.ToUpper().Equals("ADULT"))
							{
								payingPax = pax.Quantity;
							}
							else
							{
								freePax = pax.Quantity;
							}
						}
					}
					#endregion

					#endregion

					#region Set SDS Component BookRequest
					request.RequestUtcTime = DateTime.UtcNow;
					#endregion

					#region SetSegments
					request.Segments = sdsObjectFactory.GetSegmentsWithGroundReservation(groundReservation, groundBookRQ, minute);
					#endregion

					#region GeoCode the addresses
					if (request.Segments != null && request.Segments.Any())
					{
						foreach (var segment in request.Segments)
						{
							foreach (var location in segment.Locations)
							{
								if (location != null)
								{
									if (location.Latitude == 0 || location.Longitude == 0)
									{
										var modified = false;
										geocoderController.ModifyAddress(location, out modified);
									}
								}
							}
						}
					}
					#endregion

					#region Landmark
					if (request.Segments != null && request.Segments.Any())
					{
						foreach (var segment in request.Segments)
						{
							foreach (var location in segment.Locations)
							{
								UDI.SDS.LandmarksService.LandmarkRecord landmarkRecord = null;
								if (location.Latitude == 0 || location.Longitude == 0)
								{
									landmarkRecord = landmarksController.GetLandmarkRecordByAddress(string.Empty, location);
								}
								else
								{
									landmarkRecord = landmarksController.GetLandmarksByCoordinates(string.Empty, location);
								}

								//if (landmarkRecord != null)
								//{
								//we do not have subzip for charter. //subZipName = landmarkRecord.ToSubZipName();
								location.LocationType = landmarkRecord.ToLocationType();
								location.LocationName = landmarkRecord.ToLocationName();
                                #region Override LocationName with the GroundBook LocationName
                                if (string.IsNullOrEmpty(location.LocationName) && string.IsNullOrWhiteSpace(location.LocationName))
                                {
                                    if (location != null)
                                    {
                                        if (!string.IsNullOrEmpty(location.PropertyName))
                                        {
                                            location.LocationName = location.PropertyName;
                                        }
                                    }
                                }
                                #endregion
								location.MasterLandmarkID = landmarkRecord.ToMasterLandmarkID();
								location.LandmarkID = landmarkRecord.ToLandmarkID();
								location.SubZipName = landmarkRecord.ToSubZipName();
								//}
							}
						}
					}
					#endregion

					#region Set ReservationRequest
					request.ReservationRequest = sdsObjectFactory.GetReservationRequestObject(tokenVTOD, groundBookRQ);
					#endregion

					#region Get DiscountCode
					var discountCode = (groundReservation.RateQualifiers != null
															&& groundReservation.RateQualifiers.FirstOrDefault().PromotionCode != null
															) ? groundReservation.RateQualifiers.FirstOrDefault().PromotionCode : string.Empty;
					#endregion

					#region Get UserInfo
					//UserInfo userInfo = getUserInfo(tokenVTOD.Username);
					//rateController.UserInfo = userInfo;
					#endregion

					#region PickMeUpNow
					var pickMeUpNow = false;

					if (groundBookRQ.TPA_Extensions != null && groundBookRQ.TPA_Extensions.PickMeUpNow.HasValue)
					{
						pickMeUpNow = groundBookRQ.TPA_Extensions.PickMeUpNow.Value;
					}
					#endregion

					#region GratuityFix and GratuityRate
					decimal gratuityRate = 0;
					decimal gratuity = 0;
					try
					{
						if (groundBookRQ.GroundReservations.First().RateQualifiers.First().SpecialInputs != null && groundBookRQ.GroundReservations.First().RateQualifiers.First().SpecialInputs.Any())
						{
							var gratuities = groundBookRQ.GroundReservations.First().RateQualifiers.First().SpecialInputs.Where(s => s.Name.ToLower() == "gratuity").ToList();
							if (gratuities != null && gratuities.Any())
							{
								var gratuityElementValue = gratuities.First().Value;
								if (gratuityElementValue.Contains("%"))
								{
									gratuityRate = gratuityElementValue.GetPercentageAmount();
								}
								else
								{
									gratuity = gratuityElementValue.ToDecimal();
								}
							}
						}
					}
					catch
					{ }
					#endregion

					#region Get sds_user_subfleet (Subfleet Permission)
					Dictionary<string, bool> subfleetPermission = null;
					using (var db = new VTODEntities())
					{
						subfleetPermission = db.sds_user_subfleet.Where(s => s.my_aspnet_users.name == tokenVTOD.Username).ToDictionary(s => s.subfleet, s => s.Allow);
					}
					#endregion

					#region GetRates
					var rates = rateController.GetHourlyRate(fleetType, zipCode, minute, payingPax, wheelChairs, freePax);
					#endregion

					#region Match RateID -> Get Pickup Times
					var rateFleet = string.Empty;//Used for Pickup Time Matching

					//if (firstSegmentRate.ServiceInfoRates != null)
					//{
					#region Get Pickuptimes
					foreach (var rate in rates)
					{
						if (rate.FareID.ToString().Trim().ToLower() == groundBookRQ.References.First().ID.ToLower().Trim())
						{

							foundRate = rate;
							#region Adjust Gratuity
							if (gratuity > 0 && gratuityRate <= 0)
							{
								foundRate.Gratuity = gratuity;
							}
							#endregion


							findMatchingRateFlag = true;
							rateFleet = rate.Fleet;

							break;
						}
					}
					#endregion
					//}

					#region Throw exception if no matching rate found
					if (!findMatchingRateFlag)
						throw VtodException.CreateException(ExceptionType.SDS, 2003); //throw new Exception(string.Format(Messages.VTOD_SDSDomain_GroundBook_NoMatchingRateFound, "GroundBook"));
					#endregion
					#endregion

					#region ASAP
					int? asapRequestID = null;

					if (groundBookRQ.TPA_Extensions.Confirmations != null && groundBookRQ.TPA_Extensions.Confirmations.Where(s => s.Type.ToLower() == "asapconfirmation").Any())
					{
						var confirmations = groundBookRQ.TPA_Extensions.Confirmations.Where(s => s.Type.ToLower() == "asapconfirmation").ToList();
						if (confirmations != null && confirmations.Any())
						{
							asapRequestID = confirmations.First().ID.ToInt32();
						}
					}
					#endregion

					#region Match PickupTime

					if (asapRequestID.HasValue)
					{
						var newAsapStatus = asapController.GetASAPRequestStatus(asapRequestID.Value);
						if (newAsapStatus.DispositionCode == ASAPDispositionCodesEnumeration.NewPickupTimeApproved || newAsapStatus.DispositionCode == ASAPDispositionCodesEnumeration.OriginalPickupTimeApproved)
						{
							if (request.Segments.First().RequestPickupTime != newAsapStatus.ApprovedPickupTime)
							{
								throw Common.DTO.VtodException.CreateException(ExceptionType.SDS, 2004);
							}
							foundPickupTime = new PickupTimeResponse();
							foundPickupTime.AsapRequestId = asapRequestID.Value;
							foundPickupTime.AvailablePickupTime = newAsapStatus.ApprovedPickupTime;
							foundPickupTime.IsGuaranteed = newAsapStatus.ServiceIsAOR;
							foundPickupTime.AvailablePickupEndTime = newAsapStatus.ApprovedPickupTime.AddMinutes(15);
						}
						else
						{
							throw Common.DTO.VtodException.CreateException(ExceptionType.SDS, 2004);
						}
					}
					else
					{
						List<UDI.SDS.ReservationsService.PickupTimeRecord> pickupTimes = new List<UDI.SDS.ReservationsService.PickupTimeRecord>();

						#region Validate PickupTime

						if (pickMeUpNow)
						{
							pickupTimes = new List<UDI.SDS.ReservationsService.PickupTimeRecord>();

							//try
							//{
							#region Make pickMeUpNowService Request
							var pickMeUpNowServiceRequest = new UDI.SDS.UtilityService.PickMeUpNowServiceRequest();
							pickMeUpNowServiceRequest.Address = groundBookRQ.GroundReservations.First().Service.Location.Pickup.Address.AddressLine;
							pickMeUpNowServiceRequest.City = groundBookRQ.GroundReservations.First().Service.Location.Pickup.Address.CityName;
							pickMeUpNowServiceRequest.Latitude = !string.IsNullOrWhiteSpace(groundBookRQ.GroundReservations.First().Service.Location.Pickup.Address.Latitude) ? groundBookRQ.GroundReservations.First().Service.Location.Pickup.Address.Latitude.ToDouble() : 0;
							pickMeUpNowServiceRequest.Longitude = !string.IsNullOrWhiteSpace(groundBookRQ.GroundReservations.First().Service.Location.Pickup.Address.Longitude) ? groundBookRQ.GroundReservations.First().Service.Location.Pickup.Address.Longitude.ToDouble() : 0;
							pickMeUpNowServiceRequest.State = groundBookRQ.GroundReservations.First().Service.Location.Pickup.Address.StateProv.StateCode;
							pickMeUpNowServiceRequest.VehicleFleet = (int)UDI.VTOD.Common.DTO.Enum.VehicleType.ExecuCar;
							pickMeUpNowServiceRequest.Zip = groundBookRQ.GroundReservations.First().Service.Location.Pickup.Address.PostalCode;
							if (groundBookRQ.TPA_Extensions != null && !string.IsNullOrWhiteSpace(groundBookRQ.TPA_Extensions.MemberID))
							{
								pickMeUpNowServiceRequest.MemberId = groundBookRQ.TPA_Extensions.MemberID.ToInt32();
							}
							#endregion

							var pickupTime = reservationController.ValidateCharterPickupMeUpNowTime(pickMeUpNowServiceRequest, groundBookRQ.GroundReservations.First().Service.Location.Pickup.DateTime.ToDateTime().Value);
							pickupTimes.Add(pickupTime);
							//}
							//catch (Exception ex)
							//{
							//	logger.Error(ex);
							//}
						}
						else
						{
							pickupTimes = reservationController.GetCharterPickupTimes(foundRate.WebAdvanceNoticeMinutes, foundRate.FranchiseCode, request.Segments.First().RequestPickupTime, Method.GroundBook);
						}



						#endregion

						if (pickupTimes != null && pickupTimes.Any())
						{
							foreach (var pickupTime in pickupTimes)
							{
								if (pickupTime.StartTime == request.Segments.First().RequestPickupTime)
								{
									if (pickupTime.Status == UDI.SDS.ReservationsService.PickupTimeStatusCodesEnumeration.PickupExpired)
									{
										throw Common.DTO.VtodException.CreateException(ExceptionType.SDS, 2009);// new Exception(string.Format("{0}: {1}", pickupTime.Status.ToString(), Messages.Validation_ExpiredPickupTime));
									}
									if (pickupTime.Status != UDI.SDS.ReservationsService.PickupTimeStatusCodesEnumeration.AllowPickup)
									{
										throw VtodException.CreateException(ExceptionType.SDS, 2005);// new Exception(string.Format("{0}: {1}", pickupTime.Status, Messages.UDI_SDS_Invalid_PickupTime_StatusCode));
									}

									foundPickupTime.AvailablePickupEndTime = pickupTime.EndTime;
									foundPickupTime.AvailablePickupTime = pickupTime.StartTime;
									//pickupTimeResponse.BookRequestID = foundRate.BookRequestID;
									foundPickupTime.IsGuaranteed = pickupTime.IsGuaranteedTime;
									foundPickupTime.IsScheduledStop = pickupTime.IsScheduledStop;

									findMatchPickupTimeFlag = true;

									break;
								}
							}
						}
						else
						{
							throw Common.DTO.VtodException.CreateException(ExceptionType.SDS, 1004); //new Exception(string.Format(Messages.VTOD_SDSDomain_NoAvailablePickupTimes, "GroundBook"));
						}

						#region Throw exception if no matching pickup time found
						if (!findMatchPickupTimeFlag)
							throw VtodException.CreateException(ExceptionType.SDS, 2003); //throw new Exception(string.Format(Messages.VTOD_SDSDomain_GroundBook_NoMatchingRateFound, "GroundBook"));
						#endregion

						//} 
					}
					#endregion

					#region Affiliate
					request.ReservationRequest.AffiliateInfo = new AffiliateInfo();
					if (!string.IsNullOrWhiteSpace(groundBookRQ.EchoToken))
					{
						request.ReservationRequest.AffiliateInfo.TrackingNumber = groundBookRQ.EchoToken;
					}
					try
					{
						var specialInputs = groundBookRQ.GroundReservations.First().RateQualifiers.First().SpecialInputs;
						if (specialInputs != null && specialInputs.Any())
						{
							var affiliateUserIDObj = specialInputs.Where(s => s.Name.ToLower() == "affiliateuserid").FirstOrDefault();
							if (affiliateUserIDObj != null)
							{
								request.ReservationRequest.AffiliateInfo.UserID = affiliateUserIDObj.Value;
							}

							var affiliateEmailAddressObj = specialInputs.Where(s => s.Name.ToLower() == "affiliateemailaddress").FirstOrDefault();
							if (affiliateEmailAddressObj != null)
							{
								request.ReservationRequest.AffiliateInfo.Email = affiliateEmailAddressObj.Value;
							}
						}
					}
					catch { }

                    #endregion

                    #region ReserveCsr
                    if (groundBookRQ.Username.ToLower() == "Rez".ToLower())
                    {
                        if (groundBookRQ != null && groundBookRQ.TPA_Extensions != null && !string.IsNullOrEmpty(groundBookRQ.TPA_Extensions.ReserveCsr))
                            request.ReservationRequest.ReserveCsr = groundBookRQ.TPA_Extensions.ReserveCsr;
                        else
                            request.ReservationRequest.ReserveCsr = string.Empty;
                    }
                    else
                    {
                        request.ReservationRequest.ReserveCsr = groundBookRQ.Username;
                    }
                    #endregion

                    #region Book
                    var bookResponse = reservationController.BookHourly(request.Segments.First(), request.ReservationRequest, foundRate, foundPickupTime);

					//var xxx = bookResponse.XmlSerialize();
					#endregion

					#region Save to Database

					using (var db = new VTODEntities())
					{

						/* Save to SDS_Trip table */
						var reservationResponse = bookResponse.ReservationResponses.FirstOrDefault();
						var confirmationNumber = !string.IsNullOrWhiteSpace(reservationResponse.CharterReservationConfirmation.ConfirmationNumber) ? reservationResponse.CharterReservationConfirmation.ConfirmationNumber : reservationResponse.AirportReservationConfirmations.FirstOrDefault().ConfirmationNumber;
						var rezID = reservationResponse.CharterReservationConfirmation.RezID;
						request.FleetTripCode = FleetTripCode;
						var tbl_tripBooking = sdsObjectFactory.GetSDS_TripObject(request, bookResponse, request.Segments.FirstOrDefault(), groundBookRQ, confirmationNumber);

						#region Modify TotalFareAmount, Pickmeupnow, ...
						if (foundRate != null)
						{
							if (foundRate.Fare > 0)
							{
								tbl_tripBooking.TotalFareAmount = foundRate.Fare;
							}
						}
						tbl_tripBooking.PickMeUpNow = pickMeUpNow;
						#endregion

						#region Set Payment Type
						tbl_tripBooking.PaymentType = Common.DTO.Enum.PaymentType.Unknown.ToString();
						if (groundBookRQ.Payments != null && groundBookRQ.Payments.Payments != null && groundBookRQ.Payments.Payments.Any())
						{
							if (groundBookRQ.Payments.Payments.First().PaymentCard != null)
							{
								tbl_tripBooking.PaymentType = Common.DTO.Enum.PaymentType.PaymentCard.ToString();
							}
							else if (groundBookRQ.Payments.Payments.First().DirectBill != null)
							{
								tbl_tripBooking.PaymentType = Common.DTO.Enum.PaymentType.DirectBill.ToString();
							}
							//We do not have Cash for SDS
							//if (groundBookRQ.Payments.Payments.First().Cash != null && groundBookRQ.Payments.Payments.First().Cash.CashIndicator == true)
							//{
							//	tbl_tripBooking.PaymentType = Common.DTO.Enum.PaymentType.Cash.ToString();
							//}
						}
						#endregion

						#region GetUser
						var user = db.my_aspnet_users.Where(s => s.name.ToLower() == tokenVTOD.Username.ToLower()).ToList().FirstOrDefault();
						#endregion

						#region Set UserID
						tbl_tripBooking.UserID = user.id;
						#endregion

						#region Set Ref, Source, Device
						if (groundBookRQ.TPA_Extensions != null && !string.IsNullOrWhiteSpace(groundBookRQ.TPA_Extensions.Device))
						{
							tbl_tripBooking.ConsumerDevice = groundBookRQ.TPA_Extensions.Device;
						}
                        else
                        {
                            if (user != null)
                            {
                                tbl_tripBooking.ConsumerDevice = user.name;
                            }
                        }
						if (groundBookRQ.TPA_Extensions != null && !string.IsNullOrWhiteSpace(groundBookRQ.TPA_Extensions.Source))
						{
							tbl_tripBooking.ConsumerSource = groundBookRQ.TPA_Extensions.Source;
						}
						if (!string.IsNullOrWhiteSpace(groundBookRQ.EchoToken))
						{
							tbl_tripBooking.ConsumerRef = groundBookRQ.EchoToken;
						}
						#endregion

						#region Set server booking time
						tbl_tripBooking.BookingServerDateTime = System.DateTime.Now;
						#endregion

						#region PromisedETA
						try
						{
							var rateQuialifier = groundBookRQ.GroundReservations.First().RateQualifiers.First();
							if (rateQuialifier.SpecialInputs != null)
							{
								var promisedetas = rateQuialifier.SpecialInputs.Where(s => s.Name.ToLower() == "promisedeta").ToList();

								if (promisedetas != null && promisedetas.Any())
								{
									tbl_tripBooking.PromisedETA = promisedetas.First().Value.ToInt32();
								}
							}
						}
						catch (Exception ex)
						{
							logger.Error("Taxi:PromisedETA", ex);
						}
						#endregion

						#region Status
						tbl_tripBooking.FinalStatus = "Booked";
						#endregion

						#region Track
						if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
						#endregion

						db.vtod_trip.Add(tbl_tripBooking);

						db.SaveChanges();

						#region Track
						if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_DB, "db.vtod_trip.Add(tbl_tripBooking);");
						#endregion

						//#region Insert Trip Transaction Info
						//#region Track
						//if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
						//#endregion
						//var source = tokenVTOD.Username;
						//if (groundBookRQ.TPA_Extensions != null && !string.IsNullOrWhiteSpace(groundBookRQ.TPA_Extensions.Source))
						//{
						//	source = groundBookRQ.TPA_Extensions.Source;
						//}

						//var device = tokenVTOD.Username;
						//if (groundBookRQ.TPA_Extensions != null && !string.IsNullOrWhiteSpace(groundBookRQ.TPA_Extensions.Device))
						//{
						//	device = groundBookRQ.TPA_Extensions.Device;
						//}

						//db.SP_vtod_InsertTripTransactionInfo(tbl_tripBooking.Id, tokenVTOD.Username, source, device, DateTime.Now);

						//#region Track
						//if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_DB, "SP_vtod_InsertTripTransactionInfo");
						//#endregion
						//#endregion

						/* Remove payment from request */
						request.ReservationRequest.Payment = null;
                        						

						#region Make sds_VTOD_TripIDs
						//This is for out parameter. Do not delete it.
						if (tbl_tripBooking != null && tbl_tripBooking.Id > 0 && !string.IsNullOrWhiteSpace(confirmationNumber))
							//disptach_VTOD_TripIDs.Add(rezID.ToString(), tbl_tripBooking.Id);
							disptach_Rez_VTOD.Add(new Tuple<string, int, long>(confirmationNumber, rezID, tbl_tripBooking.Id));
						#endregion

					}

					#endregion

					#region Set OTA_GroundBookRS

					var OTAreservation = sdsObjectFactory.GetReservationObject(bookResponse, groundReservation, disptach_Rez_VTOD);

					response.Reservations.Add(OTAreservation);

					#endregion
				}
				else
				{
					throw VtodException.CreateFieldRequiredValidationException("GroundReservations|TPA_Extensions|Reference"); // new Exception(string.Format(Messages.VTOD_SDSDomain_GroundBookRQ_MissingData, "GroundBook"));
				}

				response.Success = new Success();

				return response;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("SDSDomain.GroundBook:: {0}", ex.ToString() + ex.StackTrace);
				throw;
			}
		}
        #endregion
        #endregion

        #region Do not delete       
        //public void GetStatus(TokenRS tokenVTOD, string rezSource)
        //{
        //	try
        //	{

        //		#region Init
        //		var dispatchController = new UDI.SDS.DispatchUpdatesController(tokenVTOD, TrackTime);
        //		//reservation.TrackTime = TrackTime;

        //		//var token = reservation.GetToken(tokenVTOD);
        //		#endregion

        //		#region Get Status
        //		var result = dispatchController.GetStatus(rezSource);
        //		#endregion

        //		#region Log
        //		if (isDebug)
        //		{
        //			try
        //			{
        //				logger.InfoFormat("StatusResult: {0}", result.XmlSerialize().ToString());
        //			}
        //			catch { }
        //		}
        //		#endregion

        //		#region Save into database
        //		if (result != null && result.Any())
        //		{

        //			using (var db = new VTODEntities())
        //			{
        //				foreach (var tripStatusRecord in result)
        //				{
        //					#region Deleted
        //					//var tripStatusRecord = new TripStatusRecord();
        //					//tripStatusRecord.ActionType = "COMPLETE";
        //					//tripStatusRecord.CityCode = "PHX";
        //					//tripStatusRecord.ConfirmationNumber = "PHX3427381";
        //					//tripStatusRecord.Fleet = "BLUEV";
        //					//tripStatusRecord.LocalTime = "2013-07-18T14:14:21.503".ToDateTime().Value;
        //					//tripStatusRecord.SystemTime = "2013-07-18T14:14:21.503".ToDateTime().Value;
        //					//tripStatusRecord.VehicleID = 0;
        //					//tripStatusRecord.VehicleLatitude = 0;
        //					//tripStatusRecord.VehicleLongitude = 0; 
        //					#endregion

        //					if (!string.IsNullOrEmpty(tripStatusRecord.ConfirmationNumber))
        //					{
        //						sds_tripstatus tripStatus = new sds_tripstatus();

        //						#region Remove CityCode from ConfirmationNumber
        //						if (tripStatusRecord.ConfirmationNumber.StartsWith(tripStatusRecord.CityCode))
        //							tripStatus.ConfirmationNumber = tripStatusRecord.ConfirmationNumber.TrimStart(tripStatusRecord.CityCode.ToCharArray());
        //						else
        //							tripStatus.ConfirmationNumber = tripStatusRecord.ConfirmationNumber;
        //						#endregion

        //						tripStatus.ActionType = tripStatusRecord.ActionType;
        //						tripStatus.AppendTime = DateTime.Now;
        //						tripStatus.CityCode = tripStatusRecord.CityCode;
        //						tripStatus.DriverName = tripStatusRecord.DriverName;
        //						tripStatus.Fleet = tripStatusRecord.Fleet;
        //						tripStatus.LocalTime = tripStatusRecord.LocalTime;
        //						tripStatus.SystemTimeSDS = tripStatusRecord.SystemTime;
        //						tripStatus.VehicleID = tripStatusRecord.VehicleID;
        //						tripStatus.VehicleLatitude = tripStatusRecord.VehicleLatitude;
        //						tripStatus.VehicleLongitude = tripStatusRecord.VehicleLongitude;

        //						#region Track
        //						if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
        //						#endregion

        //						db.sds_tripstatus.Add(tripStatus);

        //						#region Track
        //						if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_DB, "db.sds_tripstatus.Add(tripStatus)");
        //						#endregion

        //					}
        //				}

        //				#region Track
        //				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_SDS_Domain, "Call");
        //				#endregion

        //				db.SaveChanges();

        //				#region Track
        //				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_DB, "db.SaveChanges()");
        //				#endregion
        //			}

        //		}

        //		#endregion
        //	}
        //	catch (Exception ex)
        //	{
        //		logger.ErrorFormat("SDSDomain.GetStatus:: {0}", ex.ToString() + ex.StackTrace);

        //		#region Throw SDSException

        //		if (ex.Message.Contains("::"))//Remove method name
        //			throw new SDSException(ex.Message.Substring(ex.Message.IndexOf("::")));
        //		else
        //			throw new SDSException(ex.Message);

        //		#endregion
        //	}
        //} 
        #endregion

        #endregion
        
	}
}
