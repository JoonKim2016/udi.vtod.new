﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.SDS.DTO;
using UDI.SDS.DTO.Enum;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.Helper;
using UDI.Utility.Helper;
using UDI.VTOD.DataAccess.VTOD;
using UDI.SDS.Helper;
using UDI.SDS.RatesService;
using System.Text.RegularExpressions;
using System.Net;
using UDI.Utility.Serialization;
using UDI.Utility.DTO;
using log4net;

namespace UDI.VTOD.Domain.SDS
{
	public class SDSObjectFactory
	{
        public List<Segment> GetSegmentsWithGoundAvailRQ(OTA_GroundAvailRQ groundAvailRQ)
        {
            bool disabilityRequired = groundAvailRQ.DisabilityInfo != null ? groundAvailRQ.DisabilityInfo.RequiredInd : false;
            return GetSegmentsObject(groundAvailRQ.Service, groundAvailRQ.Passengers, groundAvailRQ.RateQualifiers, disabilityRequired, null, Method.GroundAvail);
        }

        public List<Segment> GetSegmentsWithGroundReservation(GroundReservation groundReservation, OTA_GroundBookRQ groundBookRQ, int? charterMinute)
        {
            return GetSegmentsObject(groundReservation.Service.Location, groundBookRQ.TPA_Extensions.Passengers, 
                                     groundReservation.RateQualifiers, groundReservation.Service.DisabilityVehicleInd, 
                                    charterMinute, Method.GroundBook, groundBookRQ.TPA_Extensions.Confirmations!=null ? groundBookRQ.TPA_Extensions.Confirmations:null, groundBookRQ.TPA_Extensions.AffiliateProviders!=null? groundBookRQ.TPA_Extensions.AffiliateProviders:null);
        }


        private List<Segment> GetSegmentsObject(Service service, List<Passengers> passengers, List<RateQualifier> rateQualifiers, bool disabilityRequired, 
                                                int? charterMinute, Method callingBy, List<Confirmation> confirmations = null, List<Confirmation> affiliateProviders = null)
		{
			//var request = new BookRequest();
			List<Segment> segments = new List<Segment>();
			//request.RequestUtcTime = DateTime.UtcNow;
			//request.Segments = segments;

			if (service != null && service.Pickup != null)
			{
				#region Locations, IsRoundTrip

				if (service.Stops != null && service.Stops.Stops.Count == 2 && service.Dropoff != null)
				{
					#region Round Trip

					#region First Segment

					var segment_first_leg = new Segment();
					Pickup_Dropoff_Stop firstStop = service.Stops.Stops.FirstOrDefault();
					bool segment_first_leg_isInternatioal;
					segment_first_leg.Locations = GetLocationsObject(service.Pickup, firstStop, out segment_first_leg_isInternatioal);
					segment_first_leg.IsRoundTrip = true;
                    segment_first_leg.ConfirmationId = (confirmations != null && confirmations.Any(x => x.Segment.Equals(0))) ? confirmations.Where(x => x.Segment.Equals(0)).Select(x => x.ID).Single() : string.Empty;                    
                    //segment_first_leg.AffiliateProviderId = (affiliateProviders != null && confirmations.Any()) ? affiliateProviders.Where(x => x.Segment.Equals(0)).Select(x => x.ID).Single() : string.Empty;
                    segment_first_leg.AffiliateProviderId = (affiliateProviders != null && affiliateProviders.Any()) ? affiliateProviders.Where(x => x.Segment.Equals(0)).Select(x => x.ID).Single() : string.Empty;

                    #region Flight Information
                    if (service.Pickup.Airline != null)
					{
						segment_first_leg.AirlineCode = service.Pickup.Airline.Code;

						try
						{
							if (!string.IsNullOrEmpty(service.Pickup.Airline.FlightDateTime))
							{
								segment_first_leg.FlightDateTime = Convert.ToDateTime(service.Pickup.Airline.FlightDateTime); 
							}
							else
							{
								//According to Doug, default is Now + 340 days. 2016.01.16 8:37 PM
								segment_first_leg.FlightDateTime = DateTime.Now.AddDays(340);
							}
						}
						catch (FormatException)
						{
							throw Common.DTO.VtodException.CreateFieldFormatValidationException("FlightDateTime", "DateTime");// Exception(string.Format(Messages.VTOD_SDSDomain_DateTime_FormatException, "SetSegments", "Pickup.Airline.FlightDateTime"));
						}

						segment_first_leg.FlightNumber = service.Pickup.Airline.FlightNumber;
					}
					else if (firstStop.Airline != null)
					{
						segment_first_leg.AirlineCode = firstStop.Airline.Code;

						try
						{
							if (!string.IsNullOrEmpty(firstStop.Airline.FlightDateTime))
							{
								segment_first_leg.FlightDateTime = Convert.ToDateTime(firstStop.Airline.FlightDateTime); 
							}
							else
							{
								//According to Doug, default is Now + 340 days. 2016.01.16 8:37 PM
								segment_first_leg.FlightDateTime = DateTime.Now.AddDays(340);
							}
						}
						catch (FormatException)
						{
							throw Common.DTO.VtodException.CreateFieldFormatValidationException("FlightDateTime", "DateTime");  //throw new Exception(string.Format(Messages.VTOD_SDSDomain_DateTime_FormatException, "SetSegments", "firstStop.Airline.FlightDateTime"));
						}

						segment_first_leg.FlightNumber = firstStop.Airline.FlightNumber;
					}
					segment_first_leg.IsInternationalFlight = segment_first_leg_isInternatioal;
					#endregion

					#region Requested Pickup Time
					if (callingBy == Method.GroundBook)
					{
						//if (service.Pickup.Airline == null)
						segment_first_leg.RequestPickupTime = Convert.ToDateTime(service.Pickup.DateTime);
						//else
						//	segment_first_leg.RequestPickupTime = Convert.ToDateTime(service.Pickup.Airline.FlightDateTime);
					}
					else
					{
						if (!string.IsNullOrWhiteSpace(service.Pickup.DateTime))
						{
							segment_first_leg.RequestPickupTime = Convert.ToDateTime(service.Pickup.DateTime);
						}
						//else
						//{
						//	segment_first_leg.RequestPickupTime = segment_first_leg.FlightDateTime;
						//}
						//For the first version, we just suport flghtTime and based on that, we return several pickuptimes based on flight time for booking.
						//For the second version, we may consider to accept requested pickup time.
						//segment_first_leg.RequestPickupTime = Convert.ToDateTime(service.Pickup.Airline != null ? service.Pickup.Airline.FlightDateTime : firstStop.Airline.FlightDateTime);
					}
					#endregion

					segments.Add(segment_first_leg);

					#endregion

					#region Second Segment - Return

					var segment_second_leg = new Segment();
					bool segment_second_leg_isInternatioal;
					Pickup_Dropoff_Stop secondStop = service.Stops.Stops.LastOrDefault();
					segment_second_leg.Locations = GetLocationsObject(secondStop, service.Dropoff, out segment_second_leg_isInternatioal);
					segment_second_leg.IsRoundTrip = true;                    
                    segment_second_leg.ConfirmationId = (confirmations != null && confirmations.Any(x => x.Segment.Equals(1))) ? confirmations.Where(x => x.Segment.Equals(1)).Select(x => x.ID).Single() : string.Empty;

                    //segment_second_leg.AffiliateProviderId = (affiliateProviders != null && confirmations.Any()) ? affiliateProviders.Where(x => x.Segment.Equals(1)).Select(x => x.ID).Single() : string.Empty;
                    segment_second_leg.AffiliateProviderId = (affiliateProviders != null && affiliateProviders.Any()) ? affiliateProviders.Where(x => x.Segment.Equals(1)).Select(x => x.ID).Single() : string.Empty;
                    #region Flight Information
                    if (secondStop.Airline != null)
					{
						segment_second_leg.AirlineCode = secondStop.Airline.Code;

						try
						{
							if (!string.IsNullOrEmpty(secondStop.Airline.FlightDateTime))
							{
								segment_second_leg.FlightDateTime = Convert.ToDateTime(secondStop.Airline.FlightDateTime); 
							}
							else
							{
								//According to Doug, default is Now + 340 days. 2016.01.16 8:37 PM
								segment_second_leg.FlightDateTime = DateTime.Now.AddDays(340);
							}
						}
						catch (FormatException)
						{
							throw Common.DTO.VtodException.CreateFieldFormatValidationException("FlightDateTime", "DateTime"); //throw new Exception(string.Format(Messages.VTOD_SDSDomain_DateTime_FormatException, "SetSegments", "secondStop.Airline.FlightDateTime"));
						}

						segment_second_leg.FlightNumber = secondStop.Airline.FlightNumber;
					}
					else if (service.Dropoff.Airline != null)
					{
						segment_second_leg.AirlineCode = service.Dropoff.Airline.Code;

						try
						{
							if (!string.IsNullOrEmpty( service.Dropoff.Airline.FlightDateTime))
							{
								segment_second_leg.FlightDateTime = Convert.ToDateTime(service.Dropoff.Airline.FlightDateTime); 
							}
							else
							{
								//According to Doug, default is Now + 340 days. 2016.01.16 8:37 PM
								segment_second_leg.FlightDateTime = DateTime.Now.AddDays(340);
							}
						}
						catch (FormatException)
						{
							throw Common.DTO.VtodException.CreateFieldFormatValidationException("FlightDateTime", "DateTime"); //throw new Exception(string.Format(Messages.VTOD_SDSDomain_DateTime_FormatException, "SetSegments", "Dropoff.Airline.FlightDateTime"));
						}

						segment_second_leg.FlightNumber = service.Dropoff.Airline.FlightNumber;
					}
					segment_second_leg.IsInternationalFlight = segment_second_leg_isInternatioal;
					#endregion

					#region Requested Pickup Time
					if (callingBy == Method.GroundBook)
					{
						//if (secondStop.Airline == null)
						segment_second_leg.RequestPickupTime = Convert.ToDateTime(secondStop.DateTime);
						//else
						//	segment_second_leg.RequestPickupTime = Convert.ToDateTime(secondStop.Airline.FlightDateTime);
					}
					else
					{
						if (!string.IsNullOrWhiteSpace(secondStop.DateTime))
						{
							segment_second_leg.RequestPickupTime = Convert.ToDateTime(secondStop.DateTime);
						}
						//else
						//{
						//	segment_second_leg.RequestPickupTime = segment_second_leg.FlightDateTime;
						//}
						//For the first version, we just suport flghtTime and based on that, we return several pickuptimes based on flight time for booking.
						//For the second version, we may consider to accept requested pickup time.
						//segment_second_leg.RequestPickupTime = Convert.ToDateTime(secondStop.Airline != null ? secondStop.Airline.FlightDateTime : service.Dropoff.Airline.FlightDateTime);

					}
					#endregion

					segments.Add(segment_second_leg);

					#endregion

					#endregion
				}
				else
				{
					#region One Way Trip

					var segment = new Segment();
					bool isInternatioal;
					if (service.Dropoff != null)
						segment.Locations = GetLocationsObject(service.Pickup, service.Dropoff, out isInternatioal);
					else
						segment.Locations = GetLocationsObject(service.Pickup, null, out isInternatioal);//AsDirected -> Hourly Charter

					segment.IsRoundTrip = false;
                    segment.ConfirmationId = (confirmations != null && confirmations.Any()) ? confirmations.Select(x => x.ID).Single() : string.Empty;
                    //segment.AffiliateProviderId = (affiliateProviders != null && confirmations.Any()) ? affiliateProviders.Select(x => x.ID).Single() : string.Empty;
                    segment.AffiliateProviderId = (affiliateProviders != null && affiliateProviders.Any()) ? affiliateProviders.Select(x => x.ID).Single() : string.Empty;
                    #region Flight Information
                    if (service.Pickup.Airline != null)
					{
						segment.AirlineCode = service.Pickup.Airline.Code;

						try
						{
							if (!string.IsNullOrEmpty(service.Pickup.Airline.FlightDateTime))
							{
								segment.FlightDateTime = Convert.ToDateTime(service.Pickup.Airline.FlightDateTime); 
							}
							else
							{
								//According to Doug, default is Now + 340 days. 2016.01.16 8:37 PM
								segment.FlightDateTime = DateTime.Now.AddDays(340);
							}
						}
						catch (FormatException)
						{
							throw Common.DTO.VtodException.CreateFieldFormatValidationException("FlightDateTime", "DateTime"); //throw new Exception(string.Format(Messages.VTOD_SDSDomain_DateTime_FormatException, "SetSegments", "Pickup.Airline.FlightDateTime"));
						}

						segment.FlightNumber = service.Pickup.Airline.FlightNumber;
					}
					else if (service.Dropoff != null && service.Dropoff.Airline != null)
					{
						segment.AirlineCode = service.Dropoff.Airline.Code;

						try
						{
							if (!string.IsNullOrEmpty(service.Dropoff.Airline.FlightDateTime))
							{
								segment.FlightDateTime = Convert.ToDateTime(service.Dropoff.Airline.FlightDateTime); 
							}
							else
							{
								//According to Doug, default is Now + 340 days. 2016.01.16 8:37 PM
								segment.FlightDateTime = DateTime.Now.AddDays(340);
							}
						}
						catch (FormatException)
						{
							throw Common.DTO.VtodException.CreateFieldFormatValidationException("FlightDateTime", "DateTime"); //throw new Exception(string.Format(Messages.VTOD_SDSDomain_DateTime_FormatException, "SetSegments", "Dropoff.Airline.FlightDateTime"));
						}

						segment.FlightNumber = service.Dropoff.Airline.FlightNumber;
					}
					segment.IsInternationalFlight = isInternatioal;
					#endregion

					#region Requested Pickup Time
					if (callingBy == Method.GroundBook)
					{
						segment.RequestPickupTime = Convert.ToDateTime(service.Pickup.DateTime);
					}
					else
					{
						if (!string.IsNullOrWhiteSpace(service.Pickup.DateTime))
						{
							segment.RequestPickupTime = Convert.ToDateTime(service.Pickup.DateTime);
						}
						//else
						//{
						//	segment.RequestPickupTime = segment.FlightDateTime;
						//}
					}

					//For the first version, we just suport flghtTime and based on that, we return several pickuptimes based on flight time for booking.
					//For the second version, we may consider to accept requested pickup time.
					//segment.RequestPickupTime = Convert.ToDateTime(service.Pickup.Airline != null ? service.Pickup.Airline.FlightDateTime : service.Dropoff.Airline.FlightDateTime);
					//}
					#endregion

					#region CharterMinute
					//int minute = groundReservation.RateQualifiers.First().SpecialInputs.Where(s => s.Name.Trim().ToLower() == "minute").First().Value.ToDecimal().RoundUp(10);

					if (charterMinute.HasValue)
					{
						segment.CharterMinutes = charterMinute.Value;
					}
					#endregion

					segments.Add(segment);

					#endregion
				}

				#endregion

				#region Set Common fields: Pickup Time, Payingpax, FreePax, Fleet

				foreach (var segment in segments)
				{
					#region  Requested Pickup Time

					//if (!string.IsNullOrEmpty(service.Pickup.DateTime))
					//	segment.RequestPickupTime = Convert.ToDateTime(service.Pickup.DateTime);
					//else
					//	throw new Exception(string.Format(Messages.VTOD_SDSDomain_MisingPickupTime, "SetSegments"));

					#endregion

					#region PayingPax, FreePax
					if (passengers != null)
					{
						foreach (var pax in passengers)
						{
							if (pax.Category != null && pax.Category.Value.ToUpper().Equals("ADULT"))
							{
								segment.PayingPax = pax.Quantity;
							}
							else
							{
								segment.ChildSeats = pax.Quantity;
							}
						}
					}

					if (segment.PayingPax == 0)
						throw Common.DTO.VtodException.CreateFieldRequiredValidationException("PassengersQuantity"); // new Exception(string.Format(Messages.VTOD_SDSDomain_MissingPassengerNumber, "SetSegments"));

					#endregion

					#region Fleet
					if (rateQualifiers != null && rateQualifiers.FirstOrDefault().RateQualifierValue.Trim().ToLower() == SDSFleet.ExecuCar.ToString().ToLower())
						segment.RequestedFleet = SDSFleet.ExecuCar;
					else if (rateQualifiers != null && rateQualifiers.FirstOrDefault().RateQualifierValue.Trim().ToLower() == SDSFleet.SuperShuttle.ToString().ToLower())
						segment.RequestedFleet = SDSFleet.SuperShuttle;
					else if (rateQualifiers != null && rateQualifiers.FirstOrDefault().RateQualifierValue.Trim().ToLower() == SDSFleet.SuperShuttleSharedRideOnly.ToString().ToLower())
						segment.RequestedFleet = SDSFleet.SuperShuttleSharedRideOnly;
					else if (rateQualifiers != null && rateQualifiers.FirstOrDefault().RateQualifierValue.Trim().ToLower() == SDSFleet.Taxi.ToString().ToLower())
						segment.RequestedFleet = SDSFleet.Taxi;
					else
						segment.RequestedFleet = SDSFleet.All;
					#endregion

					#region TripDirection
					if (segment.Locations.First().type == UDI.SDS.DTO.Enum.LocationType.Address)
					{
						segment.TripDirection = TripDirection.Inbound;
					}
					else
					{
						segment.TripDirection = TripDirection.Outbound;
					}
					#endregion

					segment.Wheelchairs = disabilityRequired == true ? 1 : 0;
				}

				#endregion
			}
			else
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("Pickup Location"); // new Exception(string.Format(Messages.VTOD_SDSDomain_MisingPickupLocation, "SetSegments"));

			//return request;
			return segments;
		}

		internal List<UDI.SDS.DTO.Location> GetLocationsObject(Pickup_Dropoff_Stop pickupAddr, Pickup_Dropoff_Stop dropoffAddr, out bool isInternational)
		{
			isInternational = false;

			List<UDI.SDS.DTO.Location> locations = new List<UDI.SDS.DTO.Location>();
			UDI.SDS.DTO.Location pickupLocation = null;
			UDI.SDS.DTO.Location dropoffLocation = null;

			if (pickupAddr != null)
				pickupLocation = GetLocationObject(pickupAddr, LocationMode.Pickup);
			if (dropoffAddr != null)
				dropoffLocation = GetLocationObject(dropoffAddr, LocationMode.Dropoff);

			#region Hourly Charter
			//if (dropoffLocation == pickupLocation)
			//	dropoffLocation.type = UDI.SDS.DTO.Enum.LocationType.AsDirected;
			#endregion

			locations.Add(pickupLocation);
			if (dropoffLocation != null)
				locations.Add(dropoffLocation);

			#region set isInternational
			try
			{
				Airline airline = (pickupAddr.Airline != null) ? pickupAddr.Airline : (dropoffAddr.Airline != null ? dropoffAddr.Airline : null);
				if (airline != null && !string.IsNullOrWhiteSpace(airline.CodeContext))
				{
					if (airline.CodeContext.ToLower() == AirLineCodeContext.International.ToString().ToLower())
						isInternational = true;
				}
			}
			catch
			{ }
			#endregion

			return locations;
		}

		internal UDI.SDS.DTO.Location GetLocationObject(Pickup_Dropoff_Stop addr, LocationMode mode)
		{
			UDI.SDS.DTO.Location location = null;

			if (addr.AirportInfo != null)
			{
				location = new UDI.SDS.DTO.Location(UDI.SDS.DTO.Enum.LocationType.Airport, mode);

				if (addr.AirportInfo.Departure == null && addr.AirportInfo.Arrival == null)
					throw Common.DTO.VtodException.CreateFieldRequiredValidationException("AirportInfo");// new Exception(string.Format(Messages.VTOD_SDSDomain_NoDepartureNoArrival, "SetLocation"));

				if (addr.AirportInfo.Departure != null && addr.AirportInfo.Arrival != null)
				{
					switch (mode)
					{
						case LocationMode.Pickup:
							location.AiportCode = addr.AirportInfo.Arrival.LocationCode;
							location.SecondryAiportCode = addr.AirportInfo.Departure.LocationCode;
							break;
						case LocationMode.Dropoff:
							location.AiportCode = addr.AirportInfo.Departure.LocationCode;
							location.SecondryAiportCode = addr.AirportInfo.Arrival.LocationCode;
							break;
					}
				}
				else
				{
					location.AiportCode = addr.AirportInfo.Departure == null ? addr.AirportInfo.Arrival.LocationCode : addr.AirportInfo.Departure.LocationCode;
				}
				location.Latitude = 0;
				location.Longitude = 0;
			}
			else if (addr.Address != null && !string.IsNullOrEmpty(addr.Address.AddressLine) && addr.Address.StateProv != null)
			{
				location = new UDI.SDS.DTO.Location(UDI.SDS.DTO.Enum.LocationType.Address, mode);

				//var indexOfFirstWord = addr.Address.AddressLine.Trim().IndexOf(' ');
				var street = string.Empty;
				var streetNumber = string.Empty;
				#region Split Street
				try
				{
					if (!string.IsNullOrWhiteSpace(addr.Address.AddressLine.Trim()))
					{
						if (string.IsNullOrWhiteSpace(addr.Address.StreetNmbr))
						{
							//Regex regex = new Regex(@"^(\w+)[\s](.*)", RegexOptions.IgnoreCase);
							Regex regex = new Regex(@"^([0-9-–]+[\s]{0,}[0-9\/]{0,})[\s](.*)", RegexOptions.IgnoreCase);
							var matches = regex.Matches(addr.Address.AddressLine.Trim());
							//if (matches.Count > 0)
							//{
							if (matches.Count > 0 && matches[0].Groups.Count == 3)
							{
								//if (matches[0].Groups[1].Value.IsNumeric())
								//{
								streetNumber = matches[0].Groups[1].Value;
								street = matches[0].Groups[2].Value;
								//}
							}
							else
							{
								street = addr.Address.AddressLine.Trim();
							}
						}
						else
						{
							streetNumber = addr.Address.StreetNmbr;
							street = addr.Address.AddressLine;
						}
					}
				}
				catch { }
				#endregion

				try
				{
					if (addr.Address.CountryName != null && !string.IsNullOrWhiteSpace(addr.Address.CountryName.Code))
					{
						location.Country = addr.Address.CountryName.Code;
					}
					location.StateOrProvince = addr.Address.StateProv.StateCode;
					location.PostalCode = addr.Address.PostalCode;
					location.City = addr.Address.CityName;
					location.Unit = addr.Address.BldgRoom;//added for the unit
					location.PropertyName = !string.IsNullOrEmpty(addr.Address.LocationName) ? addr.Address.LocationName : string.Empty;
					location.LocationType = addr.Address.LocationType != null ? addr.Address.LocationType.Value : string.Empty;

					if (!string.IsNullOrWhiteSpace(addr.Address.AddressLine))
					{
						location.StreetName = street; //addr.Address.AddressLine.Substring(indexOfFirstWord).Trim();
						location.StreetNumber = streetNumber; //addr.Address.AddressLine.Substring(0, indexOfFirstWord).Trim(); 
					}

					location.Latitude = Convert.ToDecimal(addr.Address.Latitude);
					location.Longitude = Convert.ToDecimal(addr.Address.Longitude);
				}
				catch (NullReferenceException)
				{
					throw Common.DTO.VtodException.CreateFieldRequiredValidationException("Address"); // new Exception(string.Format(Messages.VTOD_SDSDomain_AddressMissingData, "SetLocation"));
				}
				catch (FormatException)
				{
					throw Common.DTO.VtodException.CreateFieldFormatValidationException("Latitude/Longitude", "Decimal");
					//throw new Exception(string.Format(Messages.VTOD_SDSDomain_LatLon_FormatException, "SetLocation"));
				}
			}
			else
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("Address");// new Exception(string.Format(Messages.VTOD_SDSDomain_NoAddress, "SetLocation"));

			return location;
		}

		/// <summary>
		/// CreditCardInfo, Gratuity, ...
		/// </summary>
		/// <param name="tokenVTOD"></param>
		/// <param name="groundBookRQ"></param>
		/// <returns></returns>
		internal ReservationRequest GetReservationRequestObject(TokenRS tokenVTOD, OTA_GroundBookRQ groundBookRQ)
		{
			//request.reservationRequest = new ReservationRequest();
			var vtodDomain = new VTOD.VTODDomain();


			var reservationRequest = new ReservationRequest();
			var groundReservation = groundBookRQ.GroundReservations.FirstOrDefault();
			var paymentList = groundBookRQ.Payments;
			var rateQualifier = groundReservation.RateQualifiers.First();

			if (groundReservation.Passenger != null && groundReservation.Passenger.Primary != null)
			{
				if (groundReservation.Passenger.Primary.Telephones != null
					&& groundReservation.Passenger.Primary.PersonName != null)
				{
					var telephone = groundReservation.Passenger.Primary.Telephones.Find(t => t.CountryAccessCode != null && t.AreaCityCode != null && t.PhoneNumber != null);
					reservationRequest.ContactNumber = telephone.AreaCityCode + telephone.PhoneNumber;
					reservationRequest.ContactNumberDialingPrefix = telephone.CountryAccessCode;

					reservationRequest.FirstName = groundReservation.Passenger.Primary.PersonName.GivenName;
					reservationRequest.LastName = groundReservation.Passenger.Primary.PersonName.Surname;
				}
				else
					throw Common.DTO.VtodException.CreateFieldRequiredValidationException("PassengerInfo");// new Exception(string.Format(Messages.VTOD_SDSDomain_MissingPassengerInfo_NameOrPhone, "SetBookRequest"));

				if (groundReservation.Passenger.Primary.Emails != null && groundReservation.Passenger.Primary.Emails.Count > 0
					&& groundReservation.Passenger.Primary.Emails.FirstOrDefault() != null)
					reservationRequest.EmailAddress = groundReservation.Passenger.Primary.Emails.FirstOrDefault().Value;
			}
			else
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("Passenger");// new Exception(string.Format(Messages.VTOD_SDSDomain_MissingPassengerInfo, "SetBookRequest"));

			#region Gratuity
			decimal gratuity = 0;
			try
			{
				if (rateQualifier.SpecialInputs != null && rateQualifier.SpecialInputs.Any())
				{
					var gratuities = rateQualifier.SpecialInputs.Where(s => s.Name.ToLower() == "gratuity").ToList();
					if (gratuities != null && gratuities.Any())
					{
						#region Check if it's percentage or amount
						gratuity = gratuities.First().Value.GetPercentageAmount();
						if (gratuity > 0)
						{
							reservationRequest.GratuityType = GratuityType.Percentage;
						}
						else
						{
							gratuity = gratuities.First().Value.ToDecimal();
							reservationRequest.GratuityType = GratuityType.Amount;
						}
						#endregion
					}
				}
			}
			catch { }
			reservationRequest.Gratuity = gratuity;
			#endregion

			#region UserName
			reservationRequest.UserName = tokenVTOD.Username;
			#endregion

			#region PickMeUpNow
			if (groundBookRQ.TPA_Extensions != null)
			{
				reservationRequest.pickMeUpNow = groundBookRQ.TPA_Extensions.PickMeUpNow;
			}
            #endregion
            #region IsAccessible
            if (groundReservation.Service != null && groundReservation.Service.DisabilityVehicleInd!=false)
            {
                reservationRequest.IsAccessible = groundReservation.Service.DisabilityVehicleInd;

            }
            #endregion

            #region IsInboundRideNow 
            if (groundBookRQ.TPA_Extensions != null && groundBookRQ.TPA_Extensions.IsInboundRideNow != false)
            {
                reservationRequest.IsInboundRideNow = groundBookRQ.TPA_Extensions.IsInboundRideNow;
            }
            #endregion

            #region IsOutboundRideNow 
            if (groundBookRQ.TPA_Extensions != null && groundBookRQ.TPA_Extensions.IsOutboundRideNow != false)
            {
                reservationRequest.IsOutboundRideNow = groundBookRQ.TPA_Extensions.IsOutboundRideNow;
            }
            #endregion

            #region Member
            reservationRequest.MemberID = 0;
			try
			{
				if (groundBookRQ.TPA_Extensions != null && !string.IsNullOrWhiteSpace(groundBookRQ.TPA_Extensions.MemberID))
				{
					reservationRequest.MemberID = groundBookRQ.TPA_Extensions.MemberID.ToInt32();
				}
			}
			catch { }
			#endregion

			#region Payment
			reservationRequest.Payment = new UDI.SDS.ReservationsServiceSecure.Payment();
			reservationRequest.Payment.ARCNumber = string.Empty;

			if (paymentList != null && paymentList.Payments != null)
			{
				foreach (var payment in paymentList.Payments)
				{
					if (payment.DirectBill != null)
					{
						reservationRequest.Payment.DirectBillPaymentInfo = new UDI.SDS.ReservationsServiceSecure.DirectBillPayment();
						reservationRequest.Payment.DirectBillPaymentInfo.AccountNumber = payment.DirectBill.BillingNumber;
						reservationRequest.Payment.DirectBillPaymentInfo.AccountPassword = payment.DirectBill.Password;
						if (!string.IsNullOrWhiteSpace(payment.DirectBill.ID))
						{
							reservationRequest.Payment.DirectBillPaymentInfo.ID = payment.DirectBill.ID.ToInt32();
						}
						reservationRequest.Payment.PaymentType = (byte)UDI.SDS.DTO.Enum.PaymentType.DirectBill;
					}
					else if (payment.PaymentCard != null && payment.PaymentCard.CardNumber != null)
					{
						reservationRequest.Payment.CreditCardPaymentInfo = new UDI.SDS.ReservationsServiceSecure.CreditCardPayment();
						reservationRequest.Payment.CreditCardPaymentInfo.BillingName = payment.PaymentCard.CardHolderName;

						if (payment.PaymentCard.Address != null)
						{
							reservationRequest.Payment.CreditCardPaymentInfo.BillingAddress = payment.PaymentCard.Address.AddressLine;
							reservationRequest.Payment.CreditCardPaymentInfo.BillingCity = payment.PaymentCard.Address.CityName;
							reservationRequest.Payment.CreditCardPaymentInfo.BillingState = payment.PaymentCard.Address.StateProv.StateCode;
							reservationRequest.Payment.CreditCardPaymentInfo.BillingZipCode = payment.PaymentCard.Address.PostalCode;
						}
						else
						{
							var userInfo = vtodDomain.GetUserInfo(tokenVTOD.Username);
							if (userInfo != null)
							{
								reservationRequest.Payment.CreditCardPaymentInfo.BillingAddress = string.Format("{0} {1}", userInfo.vtod_users_info.StreetNumber, userInfo.vtod_users_info.StreetName);
								reservationRequest.Payment.CreditCardPaymentInfo.BillingCity = userInfo.vtod_users_info.City;
								reservationRequest.Payment.CreditCardPaymentInfo.BillingState = userInfo.vtod_users_info.State;
								reservationRequest.Payment.CreditCardPaymentInfo.BillingZipCode = userInfo.vtod_users_info.PostalCode;
							}
						}

						reservationRequest.Payment.CreditCardPaymentInfo.UserIPAddress = (new Utilities()).GetIPAddressFromContext();
						//var obj = Enum.Parse(typeof(UDI.SDS.ReservationsServiceSecure.CreditCardTypesEnumeration), payment.PaymentCard.CardType.Value, true);
						//request.reservationRequest.Payment.CreditCardPaymentInfo.CardType = (UDI.SDS.ReservationsServiceSecure.CreditCardTypesEnumeration)obj;

						reservationRequest.Payment.CreditCardPaymentInfo.CardType = payment.PaymentCard.CardType.Value.ToCreditCard();
						if (payment.PaymentCard.CardNumber.ID.ToInt32() > 0 && !string.IsNullOrWhiteSpace(payment.PaymentCard.CardNumber.EncryptedValue))
						{
							reservationRequest.Payment.CreditCardPaymentInfo.ID = payment.PaymentCard.CardNumber.ID.ToInt32();
							reservationRequest.Payment.CreditCardPaymentInfo.CreditCardNumber = string.Format("{0}|{1}", payment.PaymentCard.CardNumber.ID, payment.PaymentCard.CardNumber.EncryptedValue);
						}
						else
						{
							if (string.IsNullOrWhiteSpace(payment.PaymentCard.CardNumber.PlainText))
							{
								throw new Exception(Messages.Validation_InvalidCreditCardNumber);
							}
							else
							{
								reservationRequest.Payment.CreditCardPaymentInfo.CreditCardNumber = payment.PaymentCard.CardNumber.PlainText;
							}
						}
						reservationRequest.Payment.CreditCardPaymentInfo.ExpirationDate = payment.PaymentCard.ExpireDate.ToDateTime().Value;
						if (payment.PaymentCard.SeriesCode != null && !string.IsNullOrWhiteSpace(payment.PaymentCard.SeriesCode.PlainText)) reservationRequest.Payment.CreditCardPaymentInfo.CVVNumber = payment.PaymentCard.SeriesCode.PlainText;

						if (groundBookRQ.TPA_Extensions != null && groundBookRQ.TPA_Extensions.PickMeUpNow.HasValue && groundBookRQ.TPA_Extensions.PickMeUpNow.Value)
						{
							reservationRequest.Payment.PaymentType = (byte)UDI.SDS.DTO.Enum.PaymentType.PaymentCardHold;
						}
						else
						{
							reservationRequest.Payment.PaymentType = (byte)UDI.SDS.DTO.Enum.PaymentType.PaymentCard;
						}
					}
					else if (payment.Cash != null)
					{
						reservationRequest.Payment.PaymentType = (byte)UDI.SDS.DTO.Enum.PaymentType.Cash;
					}
					else
					{
						throw Common.DTO.VtodException.CreateFieldRequiredValidationException("Payment");// new Exception(string.Format(Messages.VTOD_SDSDomain_MissingPayment, "SetBookRequest"));
					}
				}
			}
			else
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("Payment");  //throw new Exception(string.Format(Messages.VTOD_SDSDomain_MissingPayment, "SetBookRequest"));
			}

			#endregion

			#region SID
			if (groundBookRQ.TPA_Extensions != null && !string.IsNullOrWhiteSpace(groundBookRQ.TPA_Extensions.Device))
			{
				//ToDo: it's hardcode but should be configurable
				if (tokenVTOD.Username.ToLower().Trim() == "ztrip")
				{
					reservationRequest.SID = groundBookRQ.TPA_Extensions.Device;
				}
				else
				{
					reservationRequest.SID = groundBookRQ.TPA_Extensions.SID;
				}
			}
			#endregion

			#region SubscribeToPromotions
			if (groundBookRQ.TPA_Extensions != null)
			{
				reservationRequest.SubscribeToPromotions = groundBookRQ.TPA_Extensions.SubscribeToPromotions;
			}
			#endregion

			#region Commnets
			try
			{
				if (!string.IsNullOrWhiteSpace(groundBookRQ.GroundReservations.FirstOrDefault().Service.Location.Pickup.Remark))
				{
					reservationRequest.Comments = groundBookRQ.GroundReservations.FirstOrDefault().Service.Location.Pickup.Remark;
				}
			}
			catch { }
			#endregion

			#region VehicleNumber
			if (groundBookRQ.TPA_Extensions != null)
			{
				reservationRequest.RequestVehicleNumber = groundBookRQ.TPA_Extensions.RequestVehicleNumber;
			}
			#endregion

			#region AllowSMS
			if (groundBookRQ.TPA_Extensions != null)
			{
				reservationRequest.AllowSMS = groundBookRQ.TPA_Extensions.AllowSMS;
			}
			#endregion

			#region LuggageNumber
			try
			{
				if (groundBookRQ.TPA_Extensions.LuggageQty.HasValue)
				{
					reservationRequest.LuggageCount = groundBookRQ.TPA_Extensions.LuggageQty.Value;
				}
			}
			catch { }
			#endregion
			return reservationRequest;
		}

		/// <summary>
		/// Calculate the Rate, Fees, Charges, ...
		/// </summary>
		internal GroundService GetGroundServiceObjectForAirport(TokenRS tokenVTOD, ServiceInfo serviceInfo, List<UDI.SDS.ReservationsService.PickupTimeRecord> pickupTimes, OTA_GroundAvailRQ groundAvailRQ)
		{
			GroundService groundService = new GroundService();

			#region Restrictions --- Ignore it
			//groundService.Restrictions = new Restrictions();
			//groundService.Restrictions.AdvanceBooking
			#endregion

			groundService.Reference = new Reference();
			groundService.Reference.TPA_Extensions = new TPA_Extensions();

			#region PickupTimes
			groundService.Reference.TPA_Extensions.PickupTimes = new PickupTimeList();
			groundService.Reference.TPA_Extensions.PickupTimes.PickupTimes = new List<PickupTime>();
			if (pickupTimes != null)
			{
				foreach (var pickupTime in pickupTimes)
				{
					PickupTime PUtime = new PickupTime();
                    //PUtime.AdvanceNoticeMinutes = charterServiceTypeRecord.WebAdvanceNoticeMinutes;
                    PUtime.StartDateTime = pickupTime.StartTime.ToString();
                   
                    if (pickupTime.EndTime != DateTime.MinValue)
						PUtime.EndDateTime = pickupTime.EndTime.ToString();

					PUtime.Status = pickupTime.Status.ToString();
					if (pickupTime.Status != UDI.SDS.ReservationsService.PickupTimeStatusCodesEnumeration.AllowPickup)
					{
						PUtime.Description = UDI.VTOD.Common.DTO.VtodException.CreateException(ExceptionType.SDS, 2005).ExceptionMessage.Message; // Messages.UDI_SDS_Invalid_PickupTime_StatusCode;
					}
                    PUtime.DisplayEndTime = pickupTime.DisplayEndTime;
                    PUtime.AdvanceNoticeMinutes = serviceInfo.AdvanceNoticeMinutes;

                    groundService.Reference.TPA_Extensions.PickupTimes.PickupTimes.Add(PUtime);
					#region Direction
					switch (serviceInfo.Direction)
					{
						case Direction.Inbound:
							groundService.Reference.TPA_Extensions.TripDirection = "ToAirport";
							break;
						case Direction.Outbound:
							groundService.Reference.TPA_Extensions.TripDirection = "FromAirport";
							break;
					}
					//groundService.Reference.TPA_Extensions.TripDirection == serviceInfo.Direction 
					#endregion
				}
			}

			#endregion

			#region ImageURL
			groundService.Reference.TPA_Extensions.ImageURL = serviceInfo.ImageUrl;
			#endregion

			#region ServiceCharge, Fees, ReferenceID(FareID), RateQualifier
            
			groundService.ServiceCharges = new List<ServiceCharges>();
			groundService.Fees = new List<Fees>();

			#region Passenger
			if (groundAvailRQ.Passengers.Find(r => r.Category.Value.ToUpper() == "ADULT") == null)
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("PassengersQuantity"); //throw new Exception(string.Format(Messages.VTOD_SDSDomain_MissingPassengerNumber, "SetGroundService"));

			var adultPax = groundAvailRQ.Passengers.Find(r => r.Category.Value.ToUpper() == "ADULT").Quantity;
			if (adultPax == 0)
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("PassengersQuantity"); //throw new Exception(string.Format(Messages.VTOD_SDSDomain_MissingPassengerNumber, "SetGroundService"));

			var freePax = groundAvailRQ.Passengers.Find(r => r.Category.Value.ToUpper() != "ADULT") != null ? groundAvailRQ.Passengers.Find(r => r.Category.Value.ToUpper() != "ADULT").Quantity : 0;
			#endregion

			if (serviceInfo != null)
			{
				#region Init
				ServiceCharges servCharges = null;
				decimal totalCharge = 0;
				Fees fees = null;
				#endregion

				#region Persons Fees
				#region First Person Fare

				servCharges = new ServiceCharges();
				servCharges.Description = Description.FirstPassengerFare.ToString();
				servCharges.ChargePurpose = new Common.DTO.OTA.ChargePurpose();
				servCharges.ChargePurpose.Value = UDI.VTOD.Common.DTO.Enum.ChargePurposeValue.BaseRate.ToString();

				servCharges.Calculations = new List<Calculation>();
				Calculation cal = new Calculation();
				cal.UnitCharge = System.Math.Round(serviceInfo.FirstPassengerFare, 2);
				cal.Quantity = 1;
				cal.MaxQuantity = 1;
				cal.Total = cal.UnitCharge;
				servCharges.Calculations.Add(cal);

				totalCharge += cal.Total;

               
				groundService.ServiceCharges.Add(servCharges);

				#endregion

				#region Addtional Persons Fare

				servCharges = new ServiceCharges();

				servCharges.Description = Description.AdditionalPassengers.ToString();
				servCharges.ChargePurpose = new ChargePurpose();
				servCharges.ChargePurpose.Value = UDI.VTOD.Common.DTO.Enum.ChargePurposeValue.AdditionalPassengers.ToString();

				servCharges.Calculations = new List<Calculation>();
				cal = new Calculation();
				cal.UnitCharge = System.Math.Round(serviceInfo.AdditionalPassengerFare, 2);
				cal.Quantity = adultPax - 1;
				cal.MaxQuantity = serviceInfo.MaxGuests - 1; //1 is the first passenager
				cal.Total = cal.UnitCharge * cal.Quantity;
				servCharges.Calculations.Add(cal);

				totalCharge += cal.Total;
             
                groundService.ServiceCharges.Add(servCharges);

				#endregion
				#endregion

				#region Other Fees
				#region Fixed List
				#region Fuel Surcharge Fee
				if (serviceInfo.FuelSurcharge > 0)
				{
					fees = new Fees();
					fees.Description = Description.FuelSurchargeFee.ToString();
					fees.Calculations = new List<Calculation>();
					cal = new Calculation();
					cal.UnitCharge = System.Math.Round(serviceInfo.FuelSurcharge, 2);
					cal.Quantity = 1;//At this time, the rates web service returns the calculated fuel surcharge. So the quantity would always be one (regardless of how that fee is calculated).
					cal.MaxQuantity = 1;
					cal.Total = cal.UnitCharge * cal.Quantity;
					fees.Calculations.Add(cal);

					totalCharge += cal.Total;

					groundService.Fees.Add(fees);
				}
				#endregion

				#region Discount
				if (serviceInfo.Discount > 0)
				{
					fees = new Fees();
					fees.Description = Description.Discount.ToString();
					fees.ChargePurpose = new ChargePurpose();
					fees.ChargePurpose.Value = UDI.VTOD.Common.DTO.Enum.ChargePurposeValue.Discount.ToString();

					fees.Calculations = new List<Calculation>();
					cal = new Calculation();
					cal.UnitCharge = System.Math.Round(serviceInfo.Discount, 2);
					cal.Quantity = 1;
					cal.MaxQuantity = 1;
					cal.Total = cal.UnitCharge * cal.Quantity;
					fees.Calculations.Add(cal);
					groundService.Fees.Add(fees);
				}

				#endregion

				#region Gratuity
				if (serviceInfo.Gratuity > 0)
				{
					fees = new Fees();
					fees.Description = Description.Gratuity.ToString();
					fees.ChargePurpose = new ChargePurpose();
					fees.ChargePurpose.Value = UDI.VTOD.Common.DTO.Enum.ChargePurposeValue.Gratuity.ToString();

					fees.Calculations = new List<Calculation>();
					cal = new Calculation();
					cal.UnitCharge = System.Math.Round(serviceInfo.Gratuity, 2);
					cal.Quantity = 1;
					cal.MaxQuantity = 1;
					cal.Total = cal.UnitCharge * cal.Quantity;
					fees.Calculations.Add(cal);

					totalCharge += cal.Total;

					groundService.Fees.Add(fees);
				}

				#endregion


				#endregion

				#region Flexible List
				#region CompanyFeesList
				if (serviceInfo.CompanyFees != null && serviceInfo.CompanyFees.Any())
				{
					foreach (var fee in serviceInfo.CompanyFees)
					{
						fees = new Fees();
						fees.Description = fee.FeeType;
						fees.Calculations = new List<Calculation>();
						cal = new Calculation();
						cal.UnitCharge = System.Math.Round(fee.Amount, 2);
						cal.Quantity = 1;
						cal.MaxQuantity = 1;
						cal.Total = cal.UnitCharge * cal.Quantity;
						fees.Calculations.Add(cal);

						totalCharge += cal.Total;

						groundService.Fees.Add(fees);
					}
				}
				#endregion

				#region DriverFeesList
				if (serviceInfo.DriverFees != null && serviceInfo.DriverFees.Any())
				{
					foreach (var fee in serviceInfo.DriverFees)
					{
						fees = new Fees();
						fees.Description = fee.FeeType;
						fees.Calculations = new List<Calculation>();
						cal = new Calculation();
						cal.UnitCharge = System.Math.Round(fee.Amount, 2);
						cal.Quantity = 1;
						cal.MaxQuantity = 1;
						cal.Total = cal.UnitCharge * cal.Quantity;
						fees.Calculations.Add(cal);

						totalCharge += cal.Total;

						groundService.Fees.Add(fees);
					}
				}
				#endregion
				#endregion
				#endregion

				#region TotalCharge
				groundService.TotalCharge = new TotalCharge();
				//Doug asked us to use serviceInfo.Total instead of we sum them up for fixing VAT, NYC, ... issues. 2014_06_26 (Pouyan Paryas)
				//groundService.TotalCharge.RateTotalAmount = totalCharge;
				groundService.TotalCharge.RateTotalAmount = System.Math.Round(serviceInfo.Total, 2);
				groundService.TotalCharge.EstimatedTotalAmount = System.Math.Round(serviceInfo.Total, 2);
				#endregion

				#region Reference
				groundService.Reference.ID = serviceInfo.FareID.ToString();
				groundService.Reference.TPA_Extensions.MaxAccessibleGuests = serviceInfo.MaxAccessibleGuests;
                groundService.Reference.TPA_Extensions.LogoImageUrl = serviceInfo.LogoImageUrl;
                groundService.Reference.TPA_Extensions.Commission = serviceInfo.Commission;
                groundService.Reference.TPA_Extensions.CommissionID = serviceInfo.CommissionID;
                groundService.Reference.TPA_Extensions.SecondaryImageUrl = serviceInfo.SecondaryImageUrl;
                groundService.Reference.TPA_Extensions.TripIconFontCode = serviceInfo.TripIconFontCode;
                groundService.Reference.TPA_Extensions.MaxPassengers = serviceInfo.MaxGuests;
                groundService.Reference.TPA_Extensions.IsRideNowAllowed = serviceInfo.IsRideNowAllowed;
                groundService.Reference.TPA_Extensions.AffiliateProviderId = serviceInfo.AffiliateProviderId;
                groundService.Reference.TPA_Extensions.CityCode = serviceInfo.CityCode;
                if (serviceInfo.RideNowBehavior != null)
                    groundService.Reference.TPA_Extensions.RideNowBehavior = serviceInfo.RideNowBehavior;
                #endregion

                #region Service Type, arrival instruction
                //var showServiceType = true;

                //try
                //{
                //showServiceType = System.Configuration.ConfigurationManager.AppSettings["SDS_ShowServiceType"].ToBool();
                //}
                //catch
                //{ }

                //if (showServiceType)
                //{
                var service = new Service();

				var serviceLevel = new ServiceLevel();
				serviceLevel.Code = serviceInfo.ServiceType;
				service.ServiceLevel = serviceLevel;

				var vehicleType = new UDI.VTOD.Common.DTO.OTA.VehicleType();
				vehicleType.Code = serviceInfo.FleetType;
				service.VehicleType = vehicleType;
				service.Notes = serviceInfo.AirportArrivalInstructions;

				groundService.Service = service;
				//}
				#endregion

				#region RateQualifier
				groundService.RateQualifier = new RateQualifier();
				groundService.RateQualifier.Category = new Category();
				if (tokenVTOD.Username.Trim().ToLower() == "ztrip")
				{
					groundService.RateQualifier.Category.Description = serviceInfo.ZTripDescription;// charterServiceTypeRecord.WebDescription; 
				}
				else
				{
					groundService.RateQualifier.Category.Description = serviceInfo.WebDescription;// charterServiceTypeRecord.WebDescription; 
				}
				groundService.RateQualifier.Category.Value = "Other_";
				groundService.RateQualifier.Category.Code = FareType.Fare.ToString();
				#endregion

				#region DiscountDescription (Status)
				if (!string.IsNullOrWhiteSpace(serviceInfo.DiscountDescription))
				{
					groundService.Reference.TPA_Extensions.DiscountDescription = serviceInfo.DiscountDescription;
				}
				#endregion
			}
			else
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("Reference(FareID)");// new Exception(string.Format(Messages.VTOD_SDSDomain_NoFare, "SetGroundService"));
			}

			#endregion

			return groundService;
		}

		internal GroundService GetGroundServiceObjectForCharter(TokenRS tokenVTOD, CharterServiceTypeRecord charterServiceTypeRecord, UDI.SDS.ReservationsService.PickupTimeRecord pickupTime, OTA_GroundAvailRQ groundAvailRQ)
		{
			GroundService groundService = new GroundService();

			#region Restrictions --- Ignore it
			//groundService.Restrictions = new Restrictions();
			#endregion

			#region PickupTimes

			groundService.Reference = new Reference();
			groundService.Reference.TPA_Extensions = new TPA_Extensions();
			groundService.Reference.TPA_Extensions.PickupTimes = new PickupTimeList();
			groundService.Reference.TPA_Extensions.PickupTimes.PickupTimes = new List<PickupTime>();

			PickupTime PUtime = new PickupTime();
            PUtime.AdvanceNoticeMinutes = charterServiceTypeRecord.WebAdvanceNoticeMinutes;
            PUtime.StartDateTime = pickupTime.StartTime.ToString();

			if (pickupTime.EndTime != DateTime.MinValue)
				PUtime.EndDateTime = pickupTime.EndTime.ToString();

			PUtime.Status = pickupTime.Status.ToString();
			if (pickupTime.Status != UDI.SDS.ReservationsService.PickupTimeStatusCodesEnumeration.AllowPickup)
			{
				PUtime.Description = UDI.VTOD.Common.DTO.VtodException.CreateException(ExceptionType.SDS, 2005).ExceptionMessage.Message;// Messages.UDI_SDS_Invalid_PickupTime_StatusCode;
			}
			groundService.Reference.TPA_Extensions.PickupTimes.PickupTimes.Add(PUtime);

			#endregion

			#region ImageURL
			groundService.Reference.TPA_Extensions.ImageURL = charterServiceTypeRecord.ImageUrl;
			#endregion

			#region RateQualifier
			groundService.RateQualifier = new RateQualifier();
			groundService.RateQualifier.Category = new Category();
			if (tokenVTOD.Username.Trim().ToLower() == "ztrip")
			{
				groundService.RateQualifier.Category.Description = charterServiceTypeRecord.ZTripDescription;// charterServiceTypeRecord.WebDescription; 
			}
			else
			{
				groundService.RateQualifier.Category.Description = charterServiceTypeRecord.WebDescription;// charterServiceTypeRecord.WebDescription; 
			}
			groundService.RateQualifier.Category.Value = "Other_";
			#endregion

			#region ServiceCharge, Fees, ReferenceID(FareID), RateQualifier

			groundService.ServiceCharges = new List<ServiceCharges>();
			groundService.Fees = new List<Fees>();

			#region Passenger
			if (groundAvailRQ.Passengers.Find(r => r.Category.Value.ToUpper() == "ADULT") == null)
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("PassengersQuantity"); //throw new Exception(string.Format(Messages.VTOD_SDSDomain_MissingPassengerNumber, "SetGroundService"));

			var adultPax = groundAvailRQ.Passengers.Find(r => r.Category.Value.ToUpper() == "ADULT").Quantity;
			if (adultPax == 0)
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("PassengersQuantity"); //throw new Exception(string.Format(Messages.VTOD_SDSDomain_MissingPassengerNumber, "SetGroundService"));

			var freePax = groundAvailRQ.Passengers.Find(r => r.Category.Value.ToUpper() != "ADULT") != null ? groundAvailRQ.Passengers.Find(r => r.Category.Value.ToUpper() != "ADULT").Quantity : 0;
			#endregion

			#region Charges > For as directed
			if (charterServiceTypeRecord.CharterFareItems != null && charterServiceTypeRecord.CharterFareItems.Any())
			{
				groundService.RateQualifier.Category.Code = FareType.FarePolicy.ToString();
				//groundService.RateQualifier.SpecialInputs = new List<NameValue>(); ;
				//NameValue kvpTypeRate = new NameValue { Name = "Type", Value = "Rate" };
				//groundService.RateQualifier.SpecialInputs.Add(kvpTypeRate);

				for (int i = 0; i < 3; i++)
				{
					var charterFare = charterServiceTypeRecord.CharterFareItems[i];
					ServiceCharges serviceCharges = null;
					if (i == 0)
						serviceCharges = new ServiceCharges { ChargePurpose = new ChargePurpose { Code = FareDetail.Minimum.ToString(), Value = "Other_" }, CurrencyCode = "USD", Amount = charterFare.Cost.ToString() };

					if (i == 1)
						serviceCharges = new ServiceCharges { ChargePurpose = new ChargePurpose { Code = FareDetail.PerDistance.ToString(), Value = "Other_" }, CurrencyCode = "USD", Description = "Mile", Amount = charterFare.Cost.ToString() };

					if (i == 2)
						serviceCharges = new ServiceCharges { ChargePurpose = new ChargePurpose { Code = FareDetail.FlagDrop.ToString(), Value = "Other_" }, CurrencyCode = "USD", Amount = charterFare.Cost.ToString() };

					groundService.ServiceCharges.Add(serviceCharges);
				}

			}

			#region TotalCharge > For PointToPoint
			if (charterServiceTypeRecord.Fare > 0)
			{
				var fees = new Fees();
				var cal = new Calculation();
				decimal totalCharge = 0;
				var servCharges = new ServiceCharges();

				#region Base Rate
				servCharges.Description = Description.BaseRate.ToString();
				servCharges.ChargePurpose = new Common.DTO.OTA.ChargePurpose();
				servCharges.ChargePurpose.Value = UDI.VTOD.Common.DTO.Enum.ChargePurposeValue.BaseRate.ToString();

				servCharges.Calculations = new List<Calculation>();
				cal.UnitCharge = System.Math.Round(charterServiceTypeRecord.Fare, 2);
				cal.Quantity = 1;
				cal.MaxQuantity = 1;
				cal.Total = cal.UnitCharge;
				servCharges.Calculations.Add(cal);

				totalCharge += cal.Total;

				groundService.ServiceCharges.Add(servCharges);
				#endregion


				#region Gratuity
				if (charterServiceTypeRecord.Gratuity > 0)
				{
					fees.Description = Description.Gratuity.ToString();
					fees.ChargePurpose = new ChargePurpose();
					fees.ChargePurpose.Value = UDI.VTOD.Common.DTO.Enum.ChargePurposeValue.Gratuity.ToString();

					fees.Calculations = new List<Calculation>();
					cal = new Calculation();
					cal.UnitCharge = System.Math.Round(charterServiceTypeRecord.Gratuity, 2);
					cal.Quantity = 1;
					cal.MaxQuantity = 1;
					cal.Total = cal.UnitCharge * cal.Quantity;
					fees.Calculations.Add(cal);

					//groundService.TotalCharge.RateTotalAmount
					totalCharge += cal.Total;

					groundService.Fees.Add(fees);
				}
				#endregion

				groundService.TotalCharge = new TotalCharge();
				//Doug asked us to use serviceInfo.Total instead of we sum them up for fixing VAT, NYC, ... issues. 2014_06_26 (Pouyan Paryas)
				//groundService.TotalCharge.RateTotalAmount = totalCharge;
				groundService.TotalCharge.RateTotalAmount = totalCharge;
				groundService.TotalCharge.EstimatedTotalAmount = totalCharge;
				groundService.RateQualifier.Category.Code = FareType.Fare.ToString();

			}
			#endregion

			#endregion

			#region Reference
			groundService.Reference.ID = charterServiceTypeRecord.FareID.ToString();
			groundService.Reference.TPA_Extensions.MaxAccessibleGuests = charterServiceTypeRecord.MaxAccessibleGuests;
			groundService.Reference.TPA_Extensions.MaxPassengers = charterServiceTypeRecord.MaxGuests;
			#endregion

			#region Service Type
			//var showServiceType = false;

			//try
			//{
			//	showServiceType = System.Configuration.ConfigurationManager.AppSettings["SDS_ShowServiceType"].ToBool();
			//}
			//catch
			//{ }

			//if (showServiceType)
			//{
			var service = new Service();

			var serviceLevel = new ServiceLevel();
			//ServiceTypeCode = Franchise Code
			serviceLevel.Code = charterServiceTypeRecord.ServiceTypeCode;
			serviceLevel.SourceName = charterServiceTypeRecord.FranchiseCode;
			service.ServiceLevel = serviceLevel;

			var vehicleType = new UDI.VTOD.Common.DTO.OTA.VehicleType();
			vehicleType.Code = charterServiceTypeRecord.Fleet;
			service.VehicleType = vehicleType;

			groundService.Service = service;
			//}
			#endregion
			#endregion

			return groundService;
		}

		internal GroundService GetGroundServiceObjectForHourly(TokenRS tokenVTOD, CharterServiceTypeRecord charterServiceTypeRecord, OTA_GroundAvailRQ groundAvailRQ)
		{
			GroundService groundService = new GroundService();

			int minute = groundAvailRQ.RateQualifiers.First().SpecialInputs.Where(s => s.Name.Trim().ToLower() == "minute").First().Value.ToDecimal().RoundUp(10);


			#region PickupTimes

			groundService.Reference = new Reference();
			groundService.Reference.TPA_Extensions = new TPA_Extensions();
			groundService.Reference.TPA_Extensions.PickupTimes = new PickupTimeList();
			groundService.Reference.TPA_Extensions.PickupTimes.PickupTimes = new List<PickupTime>();

			PickupTime PUtime = new PickupTime();
			PUtime.AdvanceNoticeMinutes = charterServiceTypeRecord.WebAdvanceNoticeMinutes;
			//PUtime.StartDateTime = pickupTime.StartTime.ToString();

			//if (pickupTime.EndTime != DateTime.MinValue)
			//	PUtime.EndDateTime = pickupTime.EndTime.ToString();

			//PUtime.Status = pickupTime.Status.ToString();
			//if (pickupTime.Status != UDI.SDS.ReservationsService.PickupTimeStatusCodesEnumeration.AllowPickup)
			//{
			//	PUtime.Description = UDI.VTOD.Common.DTO.VtodException.CreateException(ExceptionType.SDS, 2005).ExceptionMessage.Message;// Messages.UDI_SDS_Invalid_PickupTime_StatusCode;
			//}
			groundService.Reference.TPA_Extensions.PickupTimes.PickupTimes.Add(PUtime);

			#endregion

			#region ImageURL
			groundService.Reference.TPA_Extensions.ImageURL = charterServiceTypeRecord.ImageUrl;
			#endregion

			#region RateQualifier
			groundService.RateQualifier = new RateQualifier();
			groundService.RateQualifier.Category = new Category();
			if (tokenVTOD.Username.Trim().ToLower() == "ztrip")
			{
				groundService.RateQualifier.Category.Description = charterServiceTypeRecord.ZTripDescription;// charterServiceTypeRecord.WebDescription; 
			}
			else
			{
				groundService.RateQualifier.Category.Description = charterServiceTypeRecord.WebDescription;// charterServiceTypeRecord.WebDescription; 
			}
			groundService.RateQualifier.Category.Value = "Other_";
			#endregion

			#region ServiceCharge, Fees, ReferenceID(FareID), RateQualifier

			groundService.ServiceCharges = new List<ServiceCharges>();
			groundService.Fees = new List<Fees>();

			#region Passenger
			if (groundAvailRQ.Passengers.Find(r => r.Category.Value.ToUpper() == "ADULT") == null)
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("PassengersQuantity"); //throw new Exception(string.Format(Messages.VTOD_SDSDomain_MissingPassengerNumber, "SetGroundService"));

			var adultPax = groundAvailRQ.Passengers.Find(r => r.Category.Value.ToUpper() == "ADULT").Quantity;
			if (adultPax == 0)
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("PassengersQuantity"); //throw new Exception(string.Format(Messages.VTOD_SDSDomain_MissingPassengerNumber, "SetGroundService"));

			var freePax = groundAvailRQ.Passengers.Find(r => r.Category.Value.ToUpper() != "ADULT") != null ? groundAvailRQ.Passengers.Find(r => r.Category.Value.ToUpper() != "ADULT").Quantity : 0;
			#endregion

			#region TotalCharge > For PointToPoint
			//if (charterServiceTypeRecord.Fare > 0)
			//{
			var fees = new Fees();
			var cal = new Calculation();
			decimal totalCharge = 0;
			var servCharges = new ServiceCharges();

			#region Hourly
			servCharges.Description = Description.Hourly.ToString();
			servCharges.ChargePurpose = new Common.DTO.OTA.ChargePurpose();
			servCharges.ChargePurpose.Value = UDI.VTOD.Common.DTO.Enum.ChargePurposeValue.Hourly.ToString();

			servCharges.Calculations = new List<Calculation>();
			cal.UnitCharge = System.Math.Round(charterServiceTypeRecord.HourlyRate, 2);
			var realMinute = (minute > charterServiceTypeRecord.MinimumDurationMinutes ? minute.ToDecimal() : charterServiceTypeRecord.MinimumDurationMinutes.ToDecimal()) / 60;
			cal.Quantity = System.Math.Round(realMinute, 2);
			cal.MinQuantity = System.Math.Round(charterServiceTypeRecord.MinimumDurationMinutes.ToDecimal() / 60, 2);
			cal.Total = System.Math.Round(realMinute * cal.UnitCharge, 2);
			servCharges.Calculations.Add(cal);

			totalCharge += cal.Total;

			groundService.ServiceCharges.Add(servCharges);
			#endregion


			#region Gratuity
			if (charterServiceTypeRecord.Gratuity > 0)
			{
				fees.Description = Description.Gratuity.ToString();
				fees.ChargePurpose = new ChargePurpose();
				fees.ChargePurpose.Value = UDI.VTOD.Common.DTO.Enum.ChargePurposeValue.Gratuity.ToString();

				fees.Calculations = new List<Calculation>();
				cal = new Calculation();
				cal.UnitCharge = System.Math.Round(charterServiceTypeRecord.Gratuity, 2);
				cal.Quantity = 1;
				cal.MaxQuantity = 1;
				cal.Total = cal.UnitCharge * cal.Quantity;
				fees.Calculations.Add(cal);

				//groundService.TotalCharge.RateTotalAmount
				totalCharge += cal.Total;

				groundService.Fees.Add(fees);
			}
			#endregion

			groundService.TotalCharge = new TotalCharge();
			//Doug asked us to use serviceInfo.Total instead of we sum them up for fixing VAT, NYC, ... issues. 2014_06_26 (Pouyan Paryas)
			//groundService.TotalCharge.RateTotalAmount = totalCharge;
			groundService.TotalCharge.RateTotalAmount = totalCharge;
			groundService.TotalCharge.EstimatedTotalAmount = totalCharge;
			groundService.RateQualifier.Category.Code = FareType.Fare.ToString();

			//}
			#endregion

			#region Reference
			groundService.Reference.ID = charterServiceTypeRecord.FareID.ToString();
			groundService.Reference.TPA_Extensions.MaxAccessibleGuests = charterServiceTypeRecord.MaxAccessibleGuests;
			groundService.Reference.TPA_Extensions.MaxPassengers = charterServiceTypeRecord.MaxGuests;
			#endregion

			#region Service Type
			//var showServiceType = false;

			//try
			//{
			//	showServiceType = System.Configuration.ConfigurationManager.AppSettings["SDS_ShowServiceType"].ToBool();
			//}
			//catch
			//{ }

			//if (showServiceType)
			//{
			var service = new Service();

			var serviceLevel = new ServiceLevel();
			//ServiceTypeCode = Franchise Code
			serviceLevel.Code = charterServiceTypeRecord.ServiceTypeCode;
			serviceLevel.SourceName = charterServiceTypeRecord.FranchiseCode;
			service.ServiceLevel = serviceLevel;

			var vehicleType = new UDI.VTOD.Common.DTO.OTA.VehicleType();
			vehicleType.Code = charterServiceTypeRecord.Fleet;
			service.VehicleType = vehicleType;

			groundService.Service = service;
			//}
			#endregion
			#endregion

			return groundService;
		}

		//internal GroundService GetGroundServiceObject(UDI.SDS.RatesService.CharterServiceTypeRecord charterFare, List<UDI.SDS.ReservationsService.PickupTimeRecord> pickupTimes, OTA_GroundAvailRQ groundAvailRQ, UDI.SDS.RatesService.FareScheduleWithFees otherFees)
		//{
		//	GroundService groundService = new GroundService();

		//	#region Restrictions --- Ignore it
		//	groundService.Restrictions = new Restrictions();
		//	//groundService.Restrictions.AdvanceBooking
		//	#endregion

		//	#region PickupTimes

		//	groundService.Reference = new Reference();
		//	groundService.Reference.TPA_Extensions = new TPA_Extensions();
		//	groundService.Reference.TPA_Extensions.PickupTimes = new PickupTimeList();
		//	groundService.Reference.TPA_Extensions.PickupTimes.PickupTimes = new List<PickupTime>();
		//	if (pickupTimes != null)
		//	{
		//		foreach (var pickupTime in pickupTimes)
		//		{
		//			PickupTime PUtime = new PickupTime();
		//			PUtime.StartDateTime = pickupTime.StartTime.ToString();

		//			if (pickupTime.EndTime != DateTime.MinValue)
		//				PUtime.EndDateTime = pickupTime.EndTime.ToString();

		//			PUtime.Status = pickupTime.Status.ToString();
		//			if (pickupTime.Status != UDI.SDS.ReservationsService.PickupTimeStatusCodesEnumeration.AllowPickup)
		//			{
		//				PUtime.Description = UDI.VTOD.Common.DTO.VtodException.CreateException(ExceptionType.SDS, 2005).ExceptionMessage.Message;// Messages.UDI_SDS_Invalid_PickupTime_StatusCode;
		//			}
		//			groundService.Reference.TPA_Extensions.PickupTimes.PickupTimes.Add(PUtime);
		//		}
		//	}

		//	#endregion

		//	#region ServiceCharge, Fees, ReferenceID(FareID), RateQualifier

		//	groundService.ServiceCharges = new List<ServiceCharges>();
		//	groundService.Fees = new List<Fees>();

		//	#region Passenger
		//	if (groundAvailRQ.Passengers.Find(r => r.Category.Value.ToUpper() == "ADULT") == null)
		//		throw Common.DTO.VtodException.CreateFieldRequiredValidationException("PassengersQuantity"); //throw new Exception(string.Format(Messages.VTOD_SDSDomain_MissingPassengerNumber, "SetGroundService"));

		//	var adultPax = groundAvailRQ.Passengers.Find(r => r.Category.Value.ToUpper() == "ADULT").Quantity;
		//	if (adultPax == 0)
		//		throw Common.DTO.VtodException.CreateFieldRequiredValidationException("PassengersQuantity"); //throw new Exception(string.Format(Messages.VTOD_SDSDomain_MissingPassengerNumber, "SetGroundService"));

		//	var freePax = groundAvailRQ.Passengers.Find(r => r.Category.Value.ToUpper() != "ADULT") != null ? groundAvailRQ.Passengers.Find(r => r.Category.Value.ToUpper() != "ADULT").Quantity : 0;
		//	#endregion

		//	if (charterFare != null)
		//	{
		//		ServiceCharges servCharges = null;
		//		decimal totalCharge = 0;

		//		#region Point to Point Charter Fare

		//		servCharges = new ServiceCharges();
		//		servCharges.Description = Description.PointToPointService.ToString();
		//		servCharges.ChargePurpose = new Common.DTO.OTA.ChargePurpose();
		//		servCharges.ChargePurpose.Value = UDI.VTOD.Common.DTO.Enum.ChargePurposeValue.BaseRate.ToString();

		//		servCharges.Calculations = new List<Calculation>();
		//		Calculation cal = new Calculation();
		//		cal.UnitCharge = charterFare.Fare;
		//		cal.Quantity = adultPax + freePax;
		//		cal.MaxQuantity = charterFare.MaxGuests;
		//		cal.Total = cal.UnitCharge;
		//		servCharges.Calculations.Add(cal);

		//		totalCharge += cal.Total;

		//		groundService.ServiceCharges.Add(servCharges);

		//		#endregion

		//		#region Hourly Charter Fare
		//		/*
		//		servCharges = new ServiceCharges();
		//		servCharges.Description = Description.HourlyFare.ToString();
		//		servCharges.ChargePurpose = new Common.DTO.OTA.ChargePurpose();
		//		servCharges.ChargePurpose.Value = UDI.VTOD.Common.DTO.Enum.ChargePurposeValue.BaseRate.ToString();

		//		servCharges.Calculations = new List<Calculation>();
		//		cal = new Calculation();
		//		cal.UnitCharge = charterFare.HourlyRate;
		//		cal.Quantity = adultPax + freePax;
		//		cal.MaxQuantity = charterFare.MaxGuests;
		//		cal.Total = cal.UnitCharge;
		//		servCharges.Calculations.Add(cal);

		//		totalCharge += cal.Total;

		//		groundService.ServiceCharges.Add(servCharges);
		//		*/
		//		#endregion

		//		#region InboundCountyTax

		//		Fees fees = new Fees();
		//		fees.Description = Description.InboundCountyTax.ToString();
		//		fees.Calculations = new List<Calculation>();
		//		cal = new Calculation();
		//		cal.UnitCharge = otherFees.InboundCountyTax;
		//		cal.Quantity = 1;
		//		cal.MaxQuantity = 1;
		//		cal.Total = cal.UnitCharge * cal.Quantity;
		//		fees.Calculations.Add(cal);

		//		totalCharge += cal.Total;

		//		groundService.Fees.Add(fees);

		//		#endregion

		//		#region InboundStateTax

		//		fees = new Fees();
		//		fees.Description = Description.InboundStateTax.ToString();
		//		fees.Calculations = new List<Calculation>();
		//		cal = new Calculation();
		//		cal.UnitCharge = otherFees.InboundStateTax;
		//		cal.Quantity = 1;
		//		cal.MaxQuantity = 1;
		//		cal.Total = cal.UnitCharge * cal.Quantity;
		//		fees.Calculations.Add(cal);

		//		totalCharge += cal.Total;

		//		groundService.Fees.Add(fees);

		//		#endregion

		//		#region OutboundCountyTax

		//		fees = new Fees();
		//		fees.Description = Description.OutboundCountyTax.ToString();
		//		fees.Calculations = new List<Calculation>();
		//		cal = new Calculation();
		//		cal.UnitCharge = otherFees.OutboundCountyTax;
		//		cal.Quantity = 1;
		//		cal.MaxQuantity = 1;
		//		cal.Total = cal.UnitCharge * cal.Quantity;
		//		fees.Calculations.Add(cal);

		//		totalCharge += cal.Total;

		//		groundService.Fees.Add(fees);

		//		#endregion

		//		#region OutboundStateTax

		//		fees = new Fees();
		//		fees.Description = Description.OutboundStateTax.ToString();
		//		fees.Calculations = new List<Calculation>();
		//		cal = new Calculation();
		//		cal.UnitCharge = otherFees.OutboundStateTax;
		//		cal.Quantity = 1;
		//		cal.MaxQuantity = 1;
		//		cal.Total = cal.UnitCharge * cal.Quantity;
		//		fees.Calculations.Add(cal);

		//		totalCharge += cal.Total;

		//		groundService.Fees.Add(fees);

		//		#endregion

		//		#region TotalCharge
		//		groundService.TotalCharge = new TotalCharge();
		//		groundService.TotalCharge.RateTotalAmount = totalCharge;
		//		#endregion

		//		#region Reference
		//		groundService.Reference.ID = charterFare.FareID.ToString();
		//		groundService.Reference.TPA_Extensions.MaxAccessibleGuests = charterFare.MaxAccessibleGuests;
		//		groundService.Reference.TPA_Extensions.MaxPassengers = charterFare.MaxGuests;
		//		#endregion

		//		#region RateQualifier
		//		groundService.RateQualifier = new RateQualifier();
		//		groundService.RateQualifier.Category = new Category();
		//		groundService.RateQualifier.Category.Description = charterFare.ZTripDescription;// charterFare.WebDescription;
		//		groundService.RateQualifier.Category.Value = "Other_";
		//		#endregion
		//	}
		//	else
		//	{
		//		throw Common.DTO.VtodException.CreateFieldRequiredValidationException("Reference(FareID)");// new Exception(string.Format(Messages.VTOD_SDSDomain_NoFare, "SetGroundService"));
		//	}

		//	#endregion

		//	return groundService;
		//}

		//internal GroundService GetGroundServiceObject(UDI.SDS.RatesService.ServiceTypeRecord airportFare, List<UDI.SDS.ReservationsService.PickupTimeRecord> pickupTimes, OTA_GroundAvailRQ groundAvailRQ, UDI.SDS.RatesService.FareScheduleWithFees otherFees)
		//{
		//	GroundService groundService = new GroundService();

		//	#region Restrictions --- Ignore it
		//	groundService.Restrictions = new Restrictions();
		//	//groundService.Restrictions.AdvanceBooking
		//	#endregion

		//	#region PickupTimes

		//	groundService.Reference = new Reference();
		//	groundService.Reference.TPA_Extensions = new TPA_Extensions();
		//	groundService.Reference.TPA_Extensions.PickupTimes = new PickupTimeList();
		//	groundService.Reference.TPA_Extensions.PickupTimes.PickupTimes = new List<PickupTime>();
		//	if (pickupTimes != null)
		//	{
		//		foreach (var pickupTime in pickupTimes)
		//		{
		//			PickupTime PUtime = new PickupTime();
		//			PUtime.StartDateTime = pickupTime.StartTime.ToString();

		//			if (pickupTime.EndTime != DateTime.MinValue)
		//				PUtime.EndDateTime = pickupTime.EndTime.ToString();

		//			PUtime.Status = pickupTime.Status.ToString();
		//			if (pickupTime.Status != UDI.SDS.ReservationsService.PickupTimeStatusCodesEnumeration.AllowPickup)
		//			{
		//				PUtime.Description = Messages.UDI_SDS_Invalid_PickupTime_StatusCode;
		//			}
		//			groundService.Reference.TPA_Extensions.PickupTimes.PickupTimes.Add(PUtime);
		//		}
		//	}

		//	#endregion

		//	#region ServiceCharge, Fees, ReferenceID(FareID), RateQualifier

		//	groundService.ServiceCharges = new List<ServiceCharges>();
		//	groundService.Fees = new List<Fees>();

		//	#region Passenger
		//	if (groundAvailRQ.Passengers.Find(r => r.Category.Value.ToUpper() == "ADULT") == null)
		//		throw new Exception(string.Format(Messages.VTOD_SDSDomain_MissingPassengerNumber, "SetGroundService"));

		//	var adultPax = groundAvailRQ.Passengers.Find(r => r.Category.Value.ToUpper() == "ADULT").Quantity;
		//	if (adultPax == 0)
		//		throw new Exception(string.Format(Messages.VTOD_SDSDomain_MissingPassengerNumber, "SetGroundService"));

		//	var freePax = groundAvailRQ.Passengers.Find(r => r.Category.Value.ToUpper() != "ADULT") != null ? groundAvailRQ.Passengers.Find(r => r.Category.Value.ToUpper() != "ADULT").Quantity : 0;
		//	#endregion

		//	if (airportFare != null)
		//	{
		//		#region Init
		//		ServiceCharges servCharges = null;
		//		decimal totalCharge = 0;
		//		Fees fees = null;
		//		#endregion

		//		#region Persons Fees
		//		#region First Person Fare

		//		servCharges = new ServiceCharges();
		//		servCharges.Description = Description.FirstPassengerFare.ToString();
		//		servCharges.ChargePurpose = new Common.DTO.OTA.ChargePurpose();
		//		servCharges.ChargePurpose.Value = UDI.VTOD.Common.DTO.Enum.ChargePurposeValue.BaseRate.ToString();

		//		servCharges.Calculations = new List<Calculation>();
		//		Calculation cal = new Calculation();
		//		cal.UnitCharge = System.Math.Round(airportFare.FirstPersonFare, 2);
		//		cal.Quantity = 1;
		//		cal.MaxQuantity = 1;
		//		cal.Total = cal.UnitCharge;
		//		servCharges.Calculations.Add(cal);

		//		totalCharge += cal.Total;

		//		groundService.ServiceCharges.Add(servCharges);

		//		#endregion

		//		#region Addtional Persons Fare

		//		servCharges = new ServiceCharges();

		//		servCharges.Description = Description.AdditionalPassengers.ToString();
		//		servCharges.ChargePurpose = new ChargePurpose();
		//		servCharges.ChargePurpose.Value = UDI.VTOD.Common.DTO.Enum.ChargePurposeValue.AdditionalPassengers.ToString();

		//		servCharges.Calculations = new List<Calculation>();
		//		cal = new Calculation();
		//		cal.UnitCharge = System.Math.Round(airportFare.AdditionalPersonFare, 2);
		//		cal.Quantity = adultPax - 1;
		//		cal.MaxQuantity = airportFare.MaxGuests - 1; //1 is the first passenager
		//		cal.Total = cal.UnitCharge * cal.Quantity;
		//		servCharges.Calculations.Add(cal);

		//		totalCharge += cal.Total;

		//		groundService.ServiceCharges.Add(servCharges);

		//		#endregion
		//		#endregion

		//		#region Other Fees
		//		#region Fixed List
		//		#region Fuel Surcharge Fee
		//		if (airportFare.FuelSurcharge > 0)
		//		{
		//			fees = new Fees();
		//			fees.Description = Description.FuelSurchargeFee.ToString();
		//			fees.Calculations = new List<Calculation>();
		//			cal = new Calculation();
		//			cal.UnitCharge = System.Math.Round(airportFare.FuelSurcharge, 2);
		//			cal.Quantity = 1;//At this time, the rates web service returns the calculated fuel surcharge. So the quantity would always be one (regardless of how that fee is calculated).
		//			cal.MaxQuantity = 1;
		//			cal.Total = cal.UnitCharge * cal.Quantity;
		//			fees.Calculations.Add(cal);

		//			totalCharge += cal.Total;

		//			groundService.Fees.Add(fees);
		//		}
		//		#endregion

		//		#region InboundDiscount
		//		if (otherFees.InboundDiscount > 0)
		//		{
		//			fees = new Fees();
		//			fees.Description = Description.InboundDiscount.ToString();
		//			fees.ChargePurpose = new ChargePurpose();
		//			fees.ChargePurpose.Value = UDI.VTOD.Common.DTO.Enum.ChargePurposeValue.Discount.ToString();

		//			fees.Calculations = new List<Calculation>();
		//			cal = new Calculation();
		//			cal.UnitCharge = System.Math.Round(otherFees.InboundDiscount, 2);
		//			cal.Quantity = 1;
		//			cal.MaxQuantity = 1;
		//			cal.Total = cal.UnitCharge * cal.Quantity;
		//			fees.Calculations.Add(cal);
		//			groundService.Fees.Add(fees);
		//		}

		//		#endregion

		//		#region OutboundDiscount
		//		if (otherFees.OutboundDiscount > 0)
		//		{
		//			fees = new Fees();
		//			fees.Description = Description.OutboundDiscount.ToString();
		//			fees.ChargePurpose = new ChargePurpose();
		//			fees.ChargePurpose.Value = UDI.VTOD.Common.DTO.Enum.ChargePurposeValue.Discount.ToString();

		//			fees.Calculations = new List<Calculation>();
		//			cal = new Calculation();
		//			cal.UnitCharge = System.Math.Round(otherFees.OutboundDiscount, 2);
		//			cal.Quantity = 1;
		//			cal.MaxQuantity = 1;
		//			cal.Total = cal.UnitCharge * cal.Quantity;
		//			fees.Calculations.Add(cal);

		//			//totalCharge += cal.Total;

		//			groundService.Fees.Add(fees);
		//		}

		//		#endregion
		//		#endregion

		//		#region Flexible List
		//		#region InboundCompanyFeesList
		//		if (otherFees.InboundCompanyFeesList != null && otherFees.InboundCompanyFeesList.Any())
		//		{
		//			foreach (var fee in otherFees.InboundCompanyFeesList)
		//			{
		//				fees = new Fees();
		//				fees.Description = "Inbound " + fee.FeeType;
		//				fees.Calculations = new List<Calculation>();
		//				cal = new Calculation();
		//				cal.UnitCharge = System.Math.Round(fee.Amount, 2);
		//				cal.Quantity = 1;
		//				cal.MaxQuantity = 1;
		//				cal.Total = cal.UnitCharge * cal.Quantity;
		//				fees.Calculations.Add(cal);

		//				totalCharge += cal.Total;

		//				groundService.Fees.Add(fees);
		//			}
		//		}
		//		#endregion

		//		#region OutboundCompanyFeesList
		//		if (otherFees.OutboundCompanyFeesList != null && otherFees.OutboundCompanyFeesList.Any())
		//		{
		//			foreach (var fee in otherFees.OutboundCompanyFeesList)
		//			{
		//				fees = new Fees();
		//				fees.Description = "Outbound" + fee.FeeType;
		//				fees.Calculations = new List<Calculation>();
		//				cal = new Calculation();
		//				cal.UnitCharge = System.Math.Round(fee.Amount, 2);
		//				cal.Quantity = 1;
		//				cal.MaxQuantity = 1;
		//				cal.Total = cal.UnitCharge * cal.Quantity;
		//				fees.Calculations.Add(cal);

		//				totalCharge += cal.Total;

		//				groundService.Fees.Add(fees);
		//			}
		//		}
		//		#endregion

		//		#region InboundDriverFeesList
		//		if (otherFees.InboundDriverFeesList != null && otherFees.InboundDriverFeesList.Any())
		//		{
		//			foreach (var fee in otherFees.InboundDriverFeesList)
		//			{
		//				fees = new Fees();
		//				fees.Description = "Inbound " + fee.FeeType;
		//				fees.Calculations = new List<Calculation>();
		//				cal = new Calculation();
		//				cal.UnitCharge = System.Math.Round(fee.Amount, 2);
		//				cal.Quantity = 1;
		//				cal.MaxQuantity = 1;
		//				cal.Total = cal.UnitCharge * cal.Quantity;
		//				fees.Calculations.Add(cal);

		//				totalCharge += cal.Total;

		//				groundService.Fees.Add(fees);
		//			}
		//		}
		//		#endregion

		//		#region OutboundDriverFeesList
		//		if (otherFees.OutboundDriverFeesList != null && otherFees.OutboundDriverFeesList.Any())
		//		{
		//			foreach (var fee in otherFees.OutboundDriverFeesList)
		//			{
		//				fees = new Fees();
		//				fees.Description = "Outbound" + fee.FeeType;
		//				fees.Calculations = new List<Calculation>();
		//				cal = new Calculation();
		//				cal.UnitCharge = System.Math.Round(fee.Amount, 2);
		//				cal.Quantity = 1;
		//				cal.MaxQuantity = 1;
		//				cal.Total = cal.UnitCharge * cal.Quantity;
		//				fees.Calculations.Add(cal);

		//				totalCharge += cal.Total;

		//				groundService.Fees.Add(fees);
		//			}
		//		}
		//		#endregion
		//		#endregion
		//		#endregion

		//		#region TotalCharge
		//		groundService.TotalCharge = new TotalCharge();
		//		groundService.TotalCharge.RateTotalAmount = totalCharge;
		//		#endregion

		//		#region Reference
		//		groundService.Reference.ID = airportFare.FareID.ToString();
		//		groundService.Reference.TPA_Extensions.MaxAccessibleGuests = airportFare.MaxAccessibleGuests;
		//		groundService.Reference.TPA_Extensions.MaxPassengers = airportFare.MaxGuests;

		//		#endregion

		//		#region Service Type
		//		//var showServiceType = false;

		//		//try
		//		//{
		//		//	showServiceType = System.Configuration.ConfigurationManager.AppSettings["SDS_ShowServiceType"].ToBool();
		//		//}
		//		//catch
		//		//{ }

		//		//if (showServiceType)
		//		//{
		//		var service = new Service();

		//		var serviceLevel = new ServiceLevel();
		//		serviceLevel.Code = airportFare.ServiceTypeCode;
		//		service.ServiceLevel = serviceLevel;

		//		var vehicleType = new UDI.VTOD.Common.DTO.OTA.VehicleType();
		//		vehicleType.Code = airportFare.Fleet;
		//		service.VehicleType = vehicleType;

		//		groundService.Service = service;
		//		//}
		//		#endregion

		//		#region RateQualifier
		//		groundService.RateQualifier = new RateQualifier();
		//		groundService.RateQualifier.Category = new Category();
		//		groundService.RateQualifier.Category.Description = airportFare.WebDescription;
		//		groundService.RateQualifier.Category.Value = "Other_";
		//		#endregion
		//	}
		//	else
		//	{
		//		throw new Exception(string.Format(Messages.VTOD_SDSDomain_NoFare, "SetGroundService"));
		//	}

		//	#endregion

		//	return groundService;
		//}

		internal Common.DTO.OTA.Reservation GetReservationObject(BookResponse bookResponse, GroundReservation groundReservation, List<Tuple<string, int, long>> disptach_Rez_VTOD)
		{
			Common.DTO.OTA.Reservation response = new Common.DTO.OTA.Reservation();

			#region Set Confiramtion
			response.Confirmation = new Confirmation();
			response.Confirmation.TPA_Extensions = new TPA_Extensions();
            if(bookResponse!=null)
            {
                if(bookResponse.ReservationResponses.Any())
                {
                     if(bookResponse.ReservationResponses.FirstOrDefault().CharterReservationConfirmation!=null)
                     {
                        response.TPA_Extensions = new TPA_Extensions();
                        response.TPA_Extensions.Contacts = new Contacts();
                        response.TPA_Extensions.Contacts.Items = new List<Contact>();
                        var contactDetails = new Contact { Type = Common.DTO.Enum.ContactType.Dispatch.ToString() };
                        if (!string.IsNullOrWhiteSpace(bookResponse.ReservationResponses.FirstOrDefault().CharterReservationConfirmation.CompanyName))
                        {
                            contactDetails.Name = bookResponse.ReservationResponses.FirstOrDefault().CharterReservationConfirmation.CompanyName;
                        }
                        if (!string.IsNullOrWhiteSpace(bookResponse.ReservationResponses.FirstOrDefault().CharterReservationConfirmation.CompanyPhoneNumber))
                        {
                            contactDetails.Telephone = new Telephone { PhoneNumber = bookResponse.ReservationResponses.FirstOrDefault().CharterReservationConfirmation.CompanyPhoneNumber };
                        }
                        response.TPA_Extensions.Contacts.Items.Add(contactDetails);
                     }
                     if (bookResponse.ReservationResponses.FirstOrDefault().AirportReservationConfirmations != null)
                     {
                         if (bookResponse.ReservationResponses.FirstOrDefault().AirportReservationConfirmations.Any())
                         {
                             response.TPA_Extensions = new TPA_Extensions();
                             response.TPA_Extensions.Contacts = new Contacts();
                             response.TPA_Extensions.Contacts.Items = new List<Contact>();
                             var contactDetails = new Contact { Type = Common.DTO.Enum.ContactType.Dispatch.ToString() };
                             if (!string.IsNullOrWhiteSpace(bookResponse.ReservationResponses.FirstOrDefault().AirportReservationConfirmations.FirstOrDefault().CompanyName))
                             {
                                 contactDetails.Name = bookResponse.ReservationResponses.FirstOrDefault().AirportReservationConfirmations.FirstOrDefault().CompanyName;
                             }
                             if (!string.IsNullOrWhiteSpace(bookResponse.ReservationResponses.FirstOrDefault().AirportReservationConfirmations.FirstOrDefault().CompanyPhoneNumber))
                             {
                                 contactDetails.Telephone = new Telephone { PhoneNumber = bookResponse.ReservationResponses.FirstOrDefault().AirportReservationConfirmations.FirstOrDefault().CompanyPhoneNumber };
                             }
                             response.TPA_Extensions.Contacts.Items.Add(contactDetails);
                         }
                     }
                }
            }
			//response.Confirmation.TPA_Extensions.DispatchConfirmation = new Confirmation();
			response.Confirmation.TPA_Extensions.Confirmations = new List<Confirmation>();
			var res = bookResponse.ReservationResponses.FirstOrDefault();

			if (res.AirportReservationConfirmations != null)
			{
				response.Confirmation.ID = disptach_Rez_VTOD.First().Item3.ToString();
				//response.Confirmation.TPA_Extensions.DispatchConfirmation.ID = res.AirportReservationConfirmations.FirstOrDefault().ConfirmationNumber;
				response.Confirmation.TPA_Extensions.Confirmations.Add(new Confirmation { Type = ConfirmationType.DispatchConfirmation.ToString(), ID = res.AirportReservationConfirmations.FirstOrDefault().ConfirmationNumber });// .ID = res.AirportReservationConfirmations.FirstOrDefault().ConfirmationNumber;

				if (res.AirportReservationConfirmations.Count == 2)
				{
					//response.Confirmation.ID = disptach_VTOD_TripIDs.Last().Value.ToString();
					//response.Confirmation.TPA_Extensions.ReturnConfirmation = new Confirmation();
					//response.Confirmation.TPA_Extensions.DispatchReturnConfirmation = new Confirmation();
					response.Confirmation.TPA_Extensions.Confirmations.Add(new Confirmation { Type = ConfirmationType.ReturnConfirmation.ToString(), ID = disptach_Rez_VTOD.Last().Item3.ToString() }); // = new Confirmation();
					response.Confirmation.TPA_Extensions.Confirmations.Add(new Confirmation { Type = ConfirmationType.DispatchReturnConfirmation.ToString(), ID = res.AirportReservationConfirmations.Last().ConfirmationNumber }); // = new Confirmation();
					//response.Confirmation.TPA_Extensions.ReturnConfirmation.ID = disptach_VTOD_TripIDs.Last().Value.ToString();
					//response.Confirmation.TPA_Extensions.DispatchReturnConfirmation.ID = res.AirportReservationConfirmations.Last().ConfirmationNumber;
				}
			}
			else if (res.CharterReservationConfirmation != null)
			{
				response.Confirmation.ID = disptach_Rez_VTOD.First().Item3.ToString();
				response.Confirmation.TPA_Extensions.Confirmations = new List<Confirmation>();
				//response.Confirmation.TPA_Extensions.DispatchConfirmation.ID = res.CharterReservationConfirmation.ConfirmationNumber;
				response.Confirmation.TPA_Extensions.Confirmations.Add(new Confirmation { Type = ConfirmationType.DispatchConfirmation.ToString(), ID = res.CharterReservationConfirmation.ConfirmationNumber }); // = res.CharterReservationConfirmation.ConfirmationNumber;
			}

			#endregion

			response.Passenger = new Passenger();
			response.Passenger.Primary = new Person();
			response.Passenger.Primary = groundReservation.Passenger.Primary;

			response.Service = new Service();
			response.Service = groundReservation.Service;

			return response;
		}
        private bool GetLongitudeAndLatitudeForAirport(string airportCode, out double? longitude, out double? latitude)
        {
            bool result = false;


            //#region Track
            //if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Domain, "Call");
            //#endregion

            using (VTODEntities context = new VTODEntities())
            {
                vtod_polygon zone = context.vtod_polygon.Where(x => x.Name == airportCode && x.Type == UDI.VTOD.Domain.ECar.Const.AddressType.Airport).Select(x => x).FirstOrDefault();
                if (zone != null)
                {

                    double lon;
                    double lat;
                    if (zone.CenterLongitude.HasValue && double.TryParse(zone.CenterLongitude.Value.ToString(), out lon))
                    {
                        longitude = lon;
                        result = true;
                    }
                    else
                    {
                       
                        result = false;
                        longitude = null;
                    }

                    if (zone.CenterLatitude.HasValue && double.TryParse(zone.CenterLatitude.Value.ToString(), out lat))
                    {
                        latitude = lat;
                        result = true;
                    }
                    else
                    {
                        
                        result = false;
                        latitude = null;
                    }
                }
                else
                {
                    result = false;
                    longitude = null;
                    latitude = null;
                }

            }

            //#region Track
            //if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_ECar_Utility, "GetLongitudeAndLatitudeForAirport");
            //#endregion

            return result;
        }
		internal vtod_trip GetSDS_TripObject(BookRequest request, BookResponse bookResponse, Segment segment, OTA_GroundBookRQ groundBookRQ, string confirmationNumber,bool isModified = false, ILog logger = null)
		{
            vtod_trip trip = new vtod_trip();

            if (isModified)
            {
                using (VTODEntities context = new VTODEntities())
                {
                    sds_trip sdsTrip = null;

                    //Temporary solution: we have many dup confirmation numbers already, so we don't want to throw an exception here.                    
                    try
                    {
                        sdsTrip = context.sds_trip.Where(x => x.ConfirmationNumber.Equals(segment.ConfirmationId)).SingleOrDefault();
                    }
                    catch
                    {
                        logger.InfoFormat("Dup confirmation # :{0} ", segment.ConfirmationId);
                        sdsTrip = context.sds_trip.Where(x => x.ConfirmationNumber.Equals(segment.ConfirmationId)).OrderByDescending(p => p.Id).FirstOrDefault();
                    }                    


                    if (sdsTrip != null)
                    {
                        trip = sdsTrip.vtod_trip;

                        //history
                        var history = new sds_trip_history();
                        history.VtodTripId = sdsTrip.Id;
                        history.LastName = sdsTrip.LastName;
                        history.FirstName = sdsTrip.FirstName;
                        history.IsRoundTrip = sdsTrip.IsRoundTrip;
                        history.PayingPax = sdsTrip.PayingPax;
                        history.FreePax = sdsTrip.FreePax;
                        history.AccessibleServiceRequired = sdsTrip.AccessibleServiceRequired;
                        history.ContactNumberDialingPrefix = sdsTrip.ContactNumberDialingPrefix;
                        history.ContactNumber = sdsTrip.ContactNumber;
                        history.PickupLocation = sdsTrip.PickupLocation;
                        history.PickupLocationLatitude = sdsTrip.PickupLocationLatitude;
                        history.PickupLocationLongitude = sdsTrip.PickupLocationLongitude;
                        history.DropoffLocation = sdsTrip.DropoffLocation;
                        history.DropoffLocationLatitude = sdsTrip.DropoffLocationLatitude;
                        history.DropoffLocationLongitude = sdsTrip.DropoffLocationLongitude;
                        history.RequestedFleet = sdsTrip.RequestedFleet;
                        history.FlightDateTime = sdsTrip.FlightDateTime;
                        history.IsInternationalFlight = sdsTrip.IsInternationalFlight;
                        history.FlightNumber = sdsTrip.FlightNumber;
                        history.AirlineCode = sdsTrip.AirlineCode;
                        history.CharterMinutes = sdsTrip.CharterMinutes;
                        history.ConfirmationNumber = sdsTrip.ConfirmationNumber;
                        history.RezID = sdsTrip.RezID;
                        history.PickupDateTime = sdsTrip.vtod_trip.PickupDateTime;
                        history.PickupDateTimeUTC = sdsTrip.vtod_trip.PickupDateTimeUTC;

                        sdsTrip.sds_trip_history.Add(history);
                    }
                    context.SaveChanges();
                }
            }
            else
            {
                trip.sds_trip = new sds_trip();
            }
			
			var reservation = bookResponse.ReservationResponses.FirstOrDefault();
			
            TimeHelper utcTimeHelper = new TimeHelper();
			trip.sds_trip.AccessibleServiceRequired = segment.Wheelchairs > 0 ? true : false;
			trip.sds_trip.AirlineCode = segment.AirlineCode;
			trip.sds_trip.CharterMinutes = segment.CharterMinutes;
			trip.sds_trip.ConfirmationNumber = confirmationNumber;
			trip.sds_trip.ContactNumber = request.ReservationRequest.ContactNumber;
			trip.sds_trip.ContactNumberDialingPrefix = request.ReservationRequest.ContactNumberDialingPrefix;
			trip.sds_trip.FirstName = request.ReservationRequest.FirstName;
			trip.sds_trip.FlightDateTime = segment.FlightDateTime;
			trip.sds_trip.FlightNumber = segment.FlightNumber;
			trip.sds_trip.FreePax = 0; //Doug:FreePax will always be zero. That field is obsolete.
            trip.FleetTripCode = request.FleetTripCode;
			#region Gratuity
			if (request.ReservationRequest.GratuityType == GratuityType.Amount)
				trip.Gratuity = request.ReservationRequest.Gratuity;
			else if (request.ReservationRequest.GratuityType == GratuityType.Percentage)
				trip.GratuityRate = request.ReservationRequest.Gratuity;
			#endregion
			#region MemberID
			if (request.ReservationRequest.MemberID > 0)
			{
				trip.MemberID = request.ReservationRequest.MemberID.ToString();
			}
			#endregion

            #region Email Address
            if (!string.IsNullOrEmpty(request.ReservationRequest.EmailAddress))
            {
                trip.EmailAddress = request.ReservationRequest.EmailAddress;
            }
            #endregion
			#region CreditCardID
			if (request.ReservationRequest != null && request.ReservationRequest.Payment != null && request.ReservationRequest.Payment.CreditCardPaymentInfo != null && request.ReservationRequest.Payment.CreditCardPaymentInfo.ID > 0)
			{
				trip.CreditCardID = request.ReservationRequest.Payment.CreditCardPaymentInfo.ID.ToString();
			}
			#endregion
			#region DirectBillID
			if (request.ReservationRequest != null && request.ReservationRequest.Payment != null && request.ReservationRequest.Payment.DirectBillPaymentInfo != null && request.ReservationRequest.Payment.DirectBillPaymentInfo.ID > 0)
			{
				trip.DirectBillAccountID = request.ReservationRequest.Payment.DirectBillPaymentInfo.ID.ToString();
			}
			#endregion
			trip.sds_trip.IsInternationalFlight = segment.IsInternationalFlight;
			trip.sds_trip.IsRoundTrip = segment.IsRoundTrip;
			trip.sds_trip.LastName = request.ReservationRequest.LastName;
			trip.sds_trip.PayingPax = segment.PayingPax;
			trip.sds_trip.RequestedFleet = segment.RequestedFleet.ToString();
			//Tbl_trip.RequestedPickupTime = segment.RequestPickupTime;
			trip.sds_trip.RezID = reservation.CharterReservationConfirmation != null ? reservation.CharterReservationConfirmation.RezID : reservation.AirportReservationConfirmations.FirstOrDefault().RezID;
			//Tbl_trip.UDISDSRequestId = request.BookRequestID;
			trip.AppendTime = DateTime.Now;
			trip.PickupDateTime = segment.RequestPickupTime;
			//ToDoZtrip: find correct fleettype!
			trip.FleetType = groundBookRQ.GroundReservations.First().RateQualifiers.First().RateQualifierValue;// Common.DTO.Enum.FleetType.SuperShuttle_ExecuCar.ToString();
            
			#region Location
			foreach (var location in segment.Locations)
			{
				var addr = string.Empty;
				decimal lat = 0;
				decimal lon = 0;

				#region Address
				if (!string.IsNullOrEmpty(location.AiportCode))
					addr = location.AiportCode;
				else
				{
					addr = string.IsNullOrEmpty(location.StreetNumber) ? string.Empty : location.StreetNumber + " ";
					addr += string.IsNullOrEmpty(location.StreetName) ? string.Empty : location.StreetName + ",";
					addr += string.IsNullOrEmpty(location.Unit) ? string.Empty : location.Unit + ",";
					addr += string.IsNullOrEmpty(location.City) ? string.Empty : location.City + ",";
					addr += string.IsNullOrEmpty(location.StateOrProvince) ? string.Empty : location.StateOrProvince + " ";
					addr += string.IsNullOrEmpty(location.PostalCode) ? string.Empty : location.PostalCode + ",";
					addr += string.IsNullOrEmpty(location.Country) ? string.Empty : location.Country;
				}
				#endregion

				#region Lat/Lon
				lat = location.Latitude;
				lon = location.Longitude;

				if (!string.IsNullOrEmpty(location.AiportCode))
				{
					if (lat == 0 && lon == 0)
					{
                        var vtodDomain = new Domain.VTOD.VTODDomain();
                        var airport = vtodDomain.GetAirport(location.AiportCode);
						if (airport != null)
						{
							lat = airport.CenterLatitude.HasValue ? airport.CenterLatitude.Value : 0;
							lon = airport.CenterLongitude.HasValue ? airport.CenterLongitude.Value : 0;
						}
					}
				}
				#endregion

				if (location.mode == LocationMode.Pickup)
				{
					trip.sds_trip.PickupLocation = addr;
					trip.sds_trip.PickupLocationLatitude = lat;
					trip.sds_trip.PickupLocationLongitude = lon;
                    #region PickUp UTC Time
                    try
                    {
                        double? latLine=trip.sds_trip.PickupLocationLatitude.ToDouble();
                        double? longLine=trip.sds_trip.PickupLocationLongitude.ToDouble();
                        if (trip.sds_trip.PickupLocationLatitude != null && trip.sds_trip.PickupLocationLongitude != null)
                        {
                            trip.PickupDateTimeUTC = utcTimeHelper.GetUTCFromLocalTime(latLine, longLine,(DateTime) trip.PickupDateTime);
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(location.AiportCode))
                            {
                                double? latitude;
                                double? longitude;

                                if (GetLongitudeAndLatitudeForAirport(location.AiportCode, out longitude, out latitude))
                                {
                                   trip.PickupDateTimeUTC = utcTimeHelper.GetUTCFromLocalTime(latitude,longitude,(DateTime) trip.PickupDateTime);
                                }
                            }
                            else
                            {
                                trip.PickupDateTimeUTC = segment.RequestPickupTime.ToUniversalTime();
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                        trip.PickupDateTimeUTC = segment.RequestPickupTime.ToUniversalTime();
                    }
                    #endregion
				}
				else
				{
					trip.sds_trip.DropoffLocation = addr;
					trip.sds_trip.DropoffLocationLatitude = lat;
					trip.sds_trip.DropoffLocationLongitude = lon;
				}
			}
            #endregion
            if(segment!=null && !string.IsNullOrEmpty(segment.ConfirmationId))
            logger.InfoFormat("test confirmation # :{0} ", segment.ConfirmationId);
            return trip;
		}
	}
}
