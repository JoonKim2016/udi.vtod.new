﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.Helper;
using UDI.Utility.Helper;
using UDI.VTOD.Common.DTO.Enum;

namespace UDI.VTOD.Domain.SDS
{
	public static class SDSValidation
	{
		#region GroundAvail
		public static void ValidateAirport(this OTA_GroundAvailRQ input)
		{
			#region Init
			Airline firstAirline = null;
			Airline secondAirline = null;
			DateTime? firstFlightTime = null;
			DateTime? secondFlightTime = null;
			DateTime? firstPickupTime = null;
			DateTime? secondPickupTime = null;
			var firstTrimDirection = TripDirection.Unknown;
			var secondTrimDirection = TripDirection.Unknown;
			#endregion

			#region Check Service
			if (input == null || input.Service == null)
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("Service"); //error = Messages.Validation_OTA_Service_Null;
			}
			#endregion

			#region Pickup and Dropoff are required for both one way and round trip
			if (input.Service.Pickup == null || input.Service.Dropoff == null)
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("Pickup & Dropoff");
			}
			#endregion

			#region Validate One Way - Round
			#region First Segment
			if (input.Service.Stops == null || input.Service.Stops.Stops == null || !input.Service.Stops.Stops.Any())
			{
				if (input.Service.Pickup.AirportInfo != null && input.Service.Dropoff.AirportInfo != null)
				{
					Common.DTO.VtodException.CreateValidationException(Messages.Validation_TwoAirportsInOneSegment);
				}

				#region Get Airline
				if (input.Service.Pickup != null && input.Service.Pickup.Airline != null)
				{
					firstAirline = input.Service.Pickup.Airline;
					firstTrimDirection = TripDirection.Outbound;
				}
				else if (input.Service.Dropoff != null && input.Service.Dropoff.Airline != null)
				{
					firstAirline = input.Service.Dropoff.Airline;
					firstTrimDirection = TripDirection.Inbound;
				}
				#endregion

				#region Get Pickup Time
				if (!string.IsNullOrWhiteSpace(input.Service.Pickup.DateTime))
				{
					try
					{
						firstPickupTime = input.Service.Pickup.DateTime.ToDateTime();
					}
					catch (Exception)
					{
						throw Common.DTO.VtodException.CreateFieldFormatValidationException("PickupDateTime", "DateTime");// Messages.UDI_SDS_Invalid_PickupTime;
					}
				}
				#endregion
			}
			#endregion

			#region Second Segment
			else if (input.Service.Stops != null && input.Service.Stops.Stops != null && input.Service.Stops.Stops.Any())
			{
				if (input.Service.Stops.Stops.Count() != 2)
				{
					throw Common.DTO.VtodException.CreateFieldRequiredValidationException("Stops"); //error = Messages.Validation_SDS_AddressSegments;
				}

				#region Get Airline
				if (input.Service.Pickup != null && input.Service.Pickup.Airline != null)
				{
					firstAirline = input.Service.Pickup.Airline;
					firstTrimDirection = TripDirection.Outbound;
				}
				else if (input.Service.Stops != null && input.Service.Stops.Stops != null && input.Service.Stops.Stops.Any() && input.Service.Stops.Stops.First().Airline != null)
				{
					firstAirline = input.Service.Stops.Stops.First().Airline;
					firstTrimDirection = TripDirection.Inbound;
				}

				if (input.Service.Dropoff != null && input.Service.Dropoff.Airline != null)
				{
					secondAirline = input.Service.Dropoff.Airline;
					firstTrimDirection = TripDirection.Inbound;
				}
				else if (input.Service.Stops != null && input.Service.Stops.Stops != null && input.Service.Stops.Stops.Any() && input.Service.Stops.Stops.Last().Airline != null)
				{
					secondAirline = input.Service.Stops.Stops.Last().Airline;
					firstTrimDirection = TripDirection.Outbound;
				}
				#endregion

				#region Get Pickup Time
				if (!string.IsNullOrWhiteSpace(input.Service.Pickup.DateTime))
				{
					try
					{
						firstPickupTime = input.Service.Pickup.DateTime.ToDateTime();
					}
					catch (Exception)
					{
						throw Common.DTO.VtodException.CreateFieldFormatValidationException("PickupDateTime", "DateTime");
					}
				}

				if (!string.IsNullOrWhiteSpace(input.Service.Stops.Stops.Last().DateTime))
				{
					try
					{
						secondPickupTime = input.Service.Stops.Stops.Last().DateTime.ToDateTime();
					}
					catch (Exception)
					{
						throw Common.DTO.VtodException.CreateFieldFormatValidationException("PickupDateTime", "DateTime");
					}
				}
				#endregion
			}
			#endregion
			#endregion

			#region Having Pickup time
			/*
			#region First Segment
			if
				(
					(
				//Outbound trip and ...
						input.Service.Pickup.AirportInfo != null
						||
				//!Ecar fleet ...
						!(input.RateQualifiers.First().RateQualifierValue.ToLower() == FleetType.SuperShuttle_ExecuCar.ToString().ToLower() || input.RateQualifiers.First().RateQualifierValue.ToLower() == FleetType.ExecuCar.ToString().ToLower())
					)
					&&
				// cannot have pickup time!
					!string.IsNullOrWhiteSpace(input.Service.Pickup.DateTime)
					)
			{
				throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_PickupTimeNotSupported);
			}
			#endregion

			#region Second Segment
			if (input.Service.Stops != null && input.Service.Stops.Stops != null)
			{
				#region Other fleet rather than Ecar cannot have pickup time
				if
				(
					(
					//Outbound trip and ...
						(input.Service.Stops.Stops.Last() != null && input.Service.Stops.Stops.Last().AirportInfo != null)
						||
					//!Ecar fleet ...
						!(input.RateQualifiers.First().RateQualifierValue.ToLower() == FleetType.SuperShuttle_ExecuCar.ToString().ToLower() || input.RateQualifiers.First().RateQualifierValue.ToLower() == FleetType.ExecuCar.ToString().ToLower())
					)
					&&
					// cannot have pickup time!
					(
						(input.Service.Stops.Stops.First() != null && !string.IsNullOrWhiteSpace(input.Service.Stops.Stops.First().DateTime))
						||
						(input.Service.Stops.Stops.Last() != null && !string.IsNullOrWhiteSpace(input.Service.Stops.Stops.Last().DateTime))
					)
				)
				{
					throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_PickupTimeNotSupported);
				}
				#endregion

				#region Ecar just in Inbound can have pickup time
				if (input.RateQualifiers.First().RateQualifierValue.ToLower() == FleetType.SuperShuttle_ExecuCar.ToString().ToLower() || input.RateQualifiers.First().RateQualifierValue.ToLower() == FleetType.ExecuCar.ToString().ToLower())
				{
					if (input.Service.Stops.Stops.Last() != null && input.Service.Stops.Stops.Last().AirportInfo != null)
					{

					}
				}
				#endregion
			}
			#endregion
			*/
			#endregion

			#region Get FlightTime
			try
			{
				if (firstAirline != null)
					firstFlightTime = firstAirline.FlightDateTime.ToDateTime();
				if (secondAirline != null)
					secondFlightTime = secondAirline.FlightDateTime.ToDateTime();
			}
			catch (Exception)
			{
				throw Common.DTO.VtodException.CreateFieldFormatValidationException("FlightDateTime", "DateTime");// Messages.UDI_SDS_Invalid_FlightTime;
			}
			#endregion

			#region FlightTime > now + 1 year away
			if (firstAirline != null && firstAirline.FlightDateTime.ToDateTime() >= DateTime.Now.AddYears(1))
			{
				throw Common.DTO.VtodException.CreateException(ExceptionType.Validation,13009);
			}
			if (secondAirline != null && secondAirline.FlightDateTime.ToDateTime() >= DateTime.Now.AddYears(1))
			{
                throw Common.DTO.VtodException.CreateException(ExceptionType.Validation, 13009);
			}
			#endregion

			#region PickkupTime > FlightTime
			//if (firstTrimDirection == UDI.SDS.DTO.Enum.TripDirection.Inbound)
			//{
			//	if (firstPickupTime.HasValue && firstFlightTime.HasValue)
			//	{
			//		if (firstPickupTime >= firstFlightTime)
			//		{
			//			error = Messages.UDI_SDS_PickupTime_After_FlightTime;
			//			return false;
			//		}
			//	}
			//}

			//if (secondTrimDirection == UDI.SDS.DTO.Enum.TripDirection.Inbound)
			//{
			//	if (secondPickupTime.HasValue && secondFlightTime.HasValue)
			//	{
			//		if (secondPickupTime >= secondFlightTime)
			//		{
			//			error = Messages.UDI_SDS_PickupTime_After_FlightTime;
			//			return false;
			//		}
			//	}
			//}
			#endregion
		}

		public static void ValidateCharter(this OTA_GroundAvailRQ input)
		{
			#region Init
			Airline firstAirline = null;
			Airline secondAirline = null;
			DateTime? firstFlightTime = null;
			DateTime? secondFlightTime = null;
			DateTime? firstPickupTime = null;
			DateTime? secondPickupTime = null;
			var firstTrimDirection = TripDirection.Unknown;
			var secondTrimDirection = TripDirection.Unknown;
			#endregion

			#region Check Service
			if (input == null || input.Service == null)
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("Service"); // error = Messages.Validation_OTA_Service_Null;
			}
			#endregion

			#region Pickup and Dropoff are required for both one way and round trip
			if (input.Service.Pickup == null)
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("Pickup"); //error = Messages.Validation_SDS_AddressSegments;
			}
			#endregion

			#region Validate One Way - Round
			#region First Segment
			if (input.Service.Stops == null || input.Service.Stops.Stops == null || !input.Service.Stops.Stops.Any())
			{
				if (input.Service.Pickup.AirportInfo != null && input.Service.Dropoff.AirportInfo != null)
				{
					throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_TwoAirportsInOneSegment); //error = Messages.Validation_SDS_TwoAirportsInOneSegment;
				}

				#region Get Airline
				if (input.Service.Pickup != null && input.Service.Pickup.Airline != null)
				{
					firstAirline = input.Service.Pickup.Airline;
					firstTrimDirection = TripDirection.Outbound;
				}
				else if (input.Service.Dropoff != null && input.Service.Dropoff.Airline != null)
				{
					firstAirline = input.Service.Dropoff.Airline;
					firstTrimDirection = TripDirection.Inbound;
				}
				#endregion

				#region Get Pickup Time
				if (!string.IsNullOrWhiteSpace(input.Service.Pickup.DateTime))
				{
					try
					{
						firstPickupTime = input.Service.Pickup.DateTime.ToDateTime();
					}
					catch (Exception)
					{
						throw Common.DTO.VtodException.CreateFieldFormatValidationException("PickupDateTime", "DateTime");
					}
				}
				#endregion
			}
			#endregion

			#region Second Segment
			else if (input.Service.Stops != null && input.Service.Stops.Stops != null && input.Service.Stops.Stops.Any())
			{
				if (input.Service.Stops.Stops.Count() != 2)
				{
					throw Common.DTO.VtodException.CreateFieldRequiredValidationException("Stops"); //
				}

				#region Get Airline
				if (input.Service.Pickup != null && input.Service.Pickup.Airline != null)
				{
					firstAirline = input.Service.Pickup.Airline;
					firstTrimDirection = TripDirection.Outbound;
				}
				else if (input.Service.Stops != null && input.Service.Stops.Stops != null && input.Service.Stops.Stops.Any() && input.Service.Stops.Stops.First().Airline != null)
				{
					firstAirline = input.Service.Stops.Stops.First().Airline;
					firstTrimDirection = TripDirection.Inbound;
				}

				if (input.Service.Dropoff != null && input.Service.Dropoff.Airline != null)
				{
					secondAirline = input.Service.Dropoff.Airline;
					firstTrimDirection = TripDirection.Inbound;
				}
				else if (input.Service.Stops != null && input.Service.Stops.Stops != null && input.Service.Stops.Stops.Any() && input.Service.Stops.Stops.Last().Airline != null)
				{
					secondAirline = input.Service.Stops.Stops.Last().Airline;
					firstTrimDirection = TripDirection.Outbound;
				}
				#endregion

				#region Get Pickup Time
				if (!string.IsNullOrWhiteSpace(input.Service.Pickup.DateTime))
				{
					try
					{
						firstPickupTime = input.Service.Pickup.DateTime.ToDateTime();
					}
					catch (Exception)
					{
						throw Common.DTO.VtodException.CreateFieldFormatValidationException("PickupDateTime", "DateTime");
					}
				}

				if (!string.IsNullOrWhiteSpace(input.Service.Stops.Stops.Last().DateTime))
				{
					try
					{
						secondPickupTime = input.Service.Stops.Stops.Last().DateTime.ToDateTime();
					}
					catch (Exception)
					{
						throw Common.DTO.VtodException.CreateFieldFormatValidationException("PickupDateTime", "DateTime");
					}
				}
				#endregion
			}
			#endregion
			#endregion

		}

		public static void ValidateHourly(this OTA_GroundAvailRQ input)
		{
			#region Init
			#endregion

			#region Check Service
			if (input == null || input.Service == null)
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("Service"); // error = Messages.Validation_OTA_Service_Null;
			}
			#endregion

			#region Pickup and Dropoff are required for both one way and round trip
			if (input.Service.Pickup == null || input.Service.Pickup.Address == null)
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("Pickup"); //error = Messages.Validation_SDS_AddressSegments;
			}
			#endregion

			#region Validate zipcode
			if (string.IsNullOrWhiteSpace(input.Service.Pickup.Address.PostalCode))
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("PostalCode"); //error = Messages.Validation_SDS_AddressSegments;
			}
			#endregion

			#region RateQualifier
			if (input.RateQualifiers == null || !input.RateQualifiers.Any())
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("RateQualifiers"); //error = Messages.Validation_SDS_AddressSegments;
			}

			if (input.RateQualifiers.First().Category == null || string.IsNullOrWhiteSpace(input.RateQualifiers.First().RateQualifierValue))
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("Category"); //error = Messages.Validation_SDS_AddressSegments;
			}

			if (!input.RateQualifiers.First().SpecialInputs.Where(s => s.Name.ToLower().Trim() == "minute").Any())
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("Minute"); //error = Messages.Validation_SDS_AddressSegments;
			}
			#endregion

		}

		#endregion

		#region GroundBook
		public static void Validate(this OTA_GroundBookRQ input)
		{
			#region Init
			Airline firstAirline = null;
			Airline secondAirline = null;
			DateTime? firstFlightTime = null;
			DateTime? secondFlightTime = null;
			DateTime? firstPickupTime = null;
			DateTime? secondPickupTime = null;
			var firstTrimDirection = TripDirection.Unknown;
			var secondTrimDirection = TripDirection.Unknown;
			#endregion

			if (input == null)
			{
				throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_NullRequest);
			}

			#region Check GroundReservations
			if (input.GroundReservations == null || !input.GroundReservations.Any() || input.GroundReservations.First().Service == null || input.GroundReservations.First().Service.Location == null)
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("GroundReservations"); //error = Messages.Validation_OTA_GroundReservation_Null;
			}
			#endregion

			#region Pickup and Dropoff are required for both one way and round trip
			if (input.GroundReservations.First().Service.Location.Pickup == null)
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("Pickup"); //error = Messages.Validation_SDS_AddressSegments;
			}
			#endregion

			#region Validate One Way - Round
			#region First Segment
			if (input.GroundReservations.First().Service.Location.Stops == null || input.GroundReservations.First().Service.Location.Stops.Stops == null || !input.GroundReservations.First().Service.Location.Stops.Stops.Any())
			{
				if (input.GroundReservations.First().Service.Location.Pickup.AirportInfo != null && input.GroundReservations.First().Service.Location.Dropoff.AirportInfo != null)
				{
					throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_TwoAirportsInOneSegment);
				}

				#region Get Airline
				if (input.GroundReservations.First().Service.Location.Pickup != null && input.GroundReservations.First().Service.Location.Pickup.Airline != null)
				{
					firstAirline = input.GroundReservations.First().Service.Location.Pickup.Airline;
					firstTrimDirection = TripDirection.Outbound;
				}
				else if (input.GroundReservations.First().Service.Location.Dropoff != null && input.GroundReservations.First().Service.Location.Dropoff.Airline != null)
				{
					firstAirline = input.GroundReservations.First().Service.Location.Dropoff.Airline;
					firstTrimDirection = TripDirection.Inbound;
				}
				#endregion

				#region Get Pickup Time
				if (!string.IsNullOrWhiteSpace(input.GroundReservations.First().Service.Location.Pickup.DateTime))
				{
					try
					{
						firstPickupTime = input.GroundReservations.First().Service.Location.Pickup.DateTime.ToDateTime();
					}
					catch (Exception)
					{
						throw Common.DTO.VtodException.CreateFieldFormatValidationException("PickupDateTime", "DateTime");
					}
				}
				#endregion

			}
			#endregion

			#region Second Segment
			else if (input.GroundReservations.First().Service.Location.Stops != null && input.GroundReservations.First().Service.Location.Stops.Stops != null && input.GroundReservations.First().Service.Location.Stops.Stops.Any())
			{
				if (input.GroundReservations.First().Service.Location.Stops.Stops.Count() != 2)
				{
					throw Common.DTO.VtodException.CreateFieldRequiredValidationException("Stops"); //
				}


				#region Get Airline
				if (input.GroundReservations.First().Service.Location.Pickup != null && input.GroundReservations.First().Service.Location.Pickup.Airline != null)
				{
					firstAirline = input.GroundReservations.First().Service.Location.Pickup.Airline;
					firstTrimDirection = TripDirection.Outbound;
				}
				else if (input.GroundReservations.First().Service.Location.Stops != null && input.GroundReservations.First().Service.Location.Stops.Stops != null && input.GroundReservations.First().Service.Location.Stops.Stops.Any() && input.GroundReservations.First().Service.Location.Stops.Stops.First().Airline != null)
				{
					firstAirline = input.GroundReservations.First().Service.Location.Stops.Stops.First().Airline;
					firstTrimDirection = TripDirection.Inbound;
				}

				if (input.GroundReservations.First().Service.Location.Dropoff != null && input.GroundReservations.First().Service.Location.Dropoff.Airline != null)
				{
					secondAirline = input.GroundReservations.First().Service.Location.Dropoff.Airline;
					secondTrimDirection = TripDirection.Inbound;
				}
				else if (input.GroundReservations.First().Service.Location.Stops != null && input.GroundReservations.First().Service.Location.Stops.Stops != null && input.GroundReservations.First().Service.Location.Stops.Stops.Any() && input.GroundReservations.First().Service.Location.Stops.Stops.Last().Airline != null)
				{
					secondAirline = input.GroundReservations.First().Service.Location.Stops.Stops.Last().Airline;
					secondTrimDirection = TripDirection.Outbound;
				}
				#endregion

				#region Get Pickup Time
				if (!string.IsNullOrWhiteSpace(input.GroundReservations.First().Service.Location.Pickup.DateTime))
				{
					try
					{
						firstPickupTime = input.GroundReservations.First().Service.Location.Pickup.DateTime.ToDateTime();
					}
					catch (Exception)
					{
						throw Common.DTO.VtodException.CreateFieldFormatValidationException("PickupDateTime", "DateTime");
					}
				}

				if (!string.IsNullOrWhiteSpace(input.GroundReservations.First().Service.Location.Stops.Stops.Last().DateTime))
				{
					try
					{
						secondPickupTime = input.GroundReservations.First().Service.Location.Stops.Stops.Last().DateTime.ToDateTime();
					}
					catch (Exception)
					{
						throw Common.DTO.VtodException.CreateFieldFormatValidationException("PickupDateTime", "DateTime");
					}
				}
				#endregion

			}
			#endregion
			#endregion

			#region Pickup time For both One way and round trip
			#region First Segment
			if (string.IsNullOrWhiteSpace(input.GroundReservations.First().Service.Location.Pickup.DateTime))
			{
				//pickup time is required
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("Pickup.DateTime"); //error = Messages.Validation_PickupTime;
			}
			#endregion

			#region Second Segment
			if (
					(input.GroundReservations.First().Service.Location.Stops != null && input.GroundReservations.First().Service.Location.Stops.Stops != null && input.GroundReservations.First().Service.Location.Stops.Stops.Any())
					&&
					string.IsNullOrWhiteSpace(input.GroundReservations.First().Service.Location.Stops.Stops.Last().DateTime))
			{
				//pickup time is required
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("Stop.Pickup.DateTime"); //error = Messages.Validation_PickupTime;
			}
			#endregion
			#endregion

			#region Get FlightTime
			try
			{
				if (firstAirline != null)
					firstFlightTime = firstAirline.FlightDateTime.ToDateTime();
				if (secondAirline != null)
					secondFlightTime = secondAirline.FlightDateTime.ToDateTime();
			}
			catch (Exception)
			{
				throw Common.DTO.VtodException.CreateFieldFormatValidationException("FlightDateTime", "DateTime"); //error = Messages.UDI_SDS_Invalid_FlightTime;
			}
			#endregion

			#region FlightTime > now + 1 year away
			if (firstAirline != null && firstAirline.FlightDateTime.ToDateTime() >= DateTime.Now.AddYears(1))
			{
                throw Common.DTO.VtodException.CreateException(ExceptionType.Validation, 13009);
			}
			if (secondAirline != null && secondAirline.FlightDateTime.ToDateTime() >= DateTime.Now.AddYears(1))
			{
                throw Common.DTO.VtodException.CreateException(ExceptionType.Validation, 13009);
			}
			#endregion

			#region PickkupTime > FlightTime
			//if (firstTrimDirection == UDI.SDS.DTO.Enum.TripDirection.Inbound)
			//{
			//	if (firstPickupTime.HasValue && firstFlightTime.HasValue)
			//	{
			//		if (firstPickupTime >= firstFlightTime)
			//		{
			//			error = Messages.UDI_SDS_PickupTime_After_FlightTime;
			//			return false;
			//		}
			//	}
			//}

			//if (secondTrimDirection == UDI.SDS.DTO.Enum.TripDirection.Inbound)
			//{
			//	if (secondPickupTime.HasValue && secondFlightTime.HasValue)
			//	{
			//		if (secondPickupTime >= secondFlightTime)
			//		{
			//			error = Messages.UDI_SDS_PickupTime_After_FlightTime;
			//			return false;
			//		}
			//	}
			//}
			#endregion

			#region Check Payment
			if (input.Payments == null)
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("Payment");
			}
			if (input.Payments.Payments == null || !input.Payments.Payments.Any())
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("Payment");
			}
			if (input.Payments.Payments.First().DirectBill == null && input.Payments.Payments.First().PaymentCard == null && input.Payments.Payments.First().Cash == null)
			{
                if (input.Username.ToUpper() == "ANDROIDAPP" || input.Username.ToUpper() == "IOSAPP")
                {
                    throw Common.DTO.VtodException.CreateException(Common.DTO.Enum.ExceptionType.SDS, 2010);
                }
                else
                {
                    throw Common.DTO.VtodException.CreateFieldRequiredValidationException("Payment");
                }
				
			}
			if (input.Payments.Payments.First().PaymentCard != null)
			{
				var dateTime = input.Payments.Payments.First().PaymentCard.ExpireDate.ToDateTime();
				if (dateTime == null || dateTime <= DateTime.MinValue)
				{
					throw Common.DTO.VtodException.CreateException(Common.DTO.Enum.ExceptionType.Validation, 13006); // (Messages.Validation_CardExpiredDate);
				}
			}
			#endregion

			#region Check Refrences
			if (input.GroundReservations.First().RateQualifiers.First().RateQualifierValue.Trim().ToLower() != "taxi")
			{
				if (input.References == null || !input.References.Any())
				{
					throw Common.DTO.VtodException.CreateFieldRequiredValidationException("Reference"); //error = Messages.Validation_SDS_NullReference;
				}
				else
				{
					if (input.GroundReservations.First().Service.Location.Stops != null && input.GroundReservations.First().Service.Location.Stops.Stops != null && input.GroundReservations.First().Service.Location.Stops.Stops.Any())
					{
						if (input.References.Count() != 2)
						{
							throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_TwoReference);
						}
					}
				}
			}
			#endregion

			#region ASAP 
            //commented as per doug's requirements
            //if (input.TPA_Extensions != null && input.TPA_Extensions.Confirmations != null && input.TPA_Extensions.Confirmations.Any())
            //{
            //    if (input.TPA_Extensions.Confirmations.Where(s => s.Type.ToLower() == "asapconfirmation").Any())
            //    {
            //        if (input.GroundReservations.First().Service.Location.Stops != null)
            //        {
            //            throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_ASAP_ReturnTrips);
            //        }
            //    }
            //}
			#endregion
		}
		#endregion

		#region GroundResRetrieve
		public static void Validate(this OTA_GroundResRetrieveRQ input)
		{
		}
		#endregion

		#region GroundCancel
		public static void Validate(this OTA_GroundCancelRQ input)
		{
		}
		#endregion

		#region GroundAsap
		public static void Validate_GroundCreateAsapRequest(this OTA_GroundAvailRQ input)
		{
			#region Init
			Airline firstAirline = null;
			Airline secondAirline = null;
			DateTime? firstFlightTime = null;
			DateTime? secondFlightTime = null;
			DateTime? firstPickupTime = null;
			DateTime? secondPickupTime = null;
			var firstTrimDirection = TripDirection.Unknown;
			var secondTrimDirection = TripDirection.Unknown;
			#endregion

			#region Check Service
			if (input == null || input.Service == null)
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("Service"); //error = Messages.Validation_OTA_Service_Null;
			}
			if (input.Service.ServiceLevel == null || input.Service.VehicleType == null)
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("ServiceLevel & VehicleType"); //error = Messages.Validation_OTA_Service_Fleet_Type;
			}

			if (string.IsNullOrWhiteSpace(input.Service.ServiceLevel.Code) || string.IsNullOrWhiteSpace(input.Service.VehicleType.Code))
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("ServiceLevel & VehicleType"); //error = Messages.Validation_OTA_Service_Fleet_Type;
			}
			#endregion

			#region Address
			if (input.Service.Pickup == null || string.IsNullOrWhiteSpace(input.Service.Pickup.DateTime))
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("Pickup DateTime"); //Messages.UDI_SDS_ASAP_NoPickupTimeFound;
			}
			if (input.Service.Pickup.Address == null)
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("Address"); //Messages.UDI_SDS_ASAP_NoPickupTimeFound;
			}
			if (string.IsNullOrWhiteSpace(input.Service.Pickup.Address.PostalCode))
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("PostalCode");// Messages.UDI_SDS_PostalCodeRequired;
			}
			if (string.IsNullOrWhiteSpace(input.Service.Pickup.Address.StreetNmbr))
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("StreetNmbr"); //Messages.UDI_SDS_StreetNumberRequired;
			}
			#endregion

			#region Reference
			if (input.References == null || !input.References.Any())
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("UniqueID");// Messages.UDI_SDS_ASAP_ReferenceID;
			}
			#endregion
		}
		#endregion

		#region Ground Get Vechilce Info
		public static void ValidateGetVehicleInfoRequest(this OTA_GroundAvailRQ input)
		{

			if (input.Service == null)
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("Service"); //error = Messages.Validation_OTA_Service_Null;
			}

			if (input.Service.Pickup == null || input.Service.Pickup.Address == null)
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("Pickup Location");// Messages.VTOD_SDSDomain_MisingPickupLocation;
			}

			if (string.IsNullOrWhiteSpace(input.Service.Pickup.Address.Latitude) || string.IsNullOrWhiteSpace(input.Service.Pickup.Address.Longitude))
			{
				throw Common.DTO.VtodException.CreateFieldFormatValidationException("Latitude/Longitude", "Decimal");// Messages.VTOD_SDSDomain_LatLon_FormatException;
			}
		}

		#endregion

		#region Token
		public static void Validate(this TokenRQ input)
		{
		}
		#endregion
	}
}
