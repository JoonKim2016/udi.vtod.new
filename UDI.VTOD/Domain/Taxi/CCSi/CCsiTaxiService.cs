﻿using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UDI.VTOD.Common.DTO;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.Track;
using UDI.VTOD.Domain.Taxi.Class;
using UDI.VTOD.DataAccess.VTOD;
using UDI.VTOD.Domain.Taxi.Const;
using UDI.Utility.Helper;
using System.Configuration;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using UDI.VTOD.CCSiService;
using UDI.Map;
using System.Web;
using System.Globalization;
using UDI.Utility.Serialization;
using UDI.VTOD.Common.Helper;

namespace UDI.VTOD.Domain.Taxi.CCSi
{
	public class CCSiTaxiService : ITaxiService
	{
		private ILog logger;
		private TaxiUtility utility;
		//private TokenRS tokenVTOD;

		public CCSiTaxiService(TaxiUtility util, ILog log)
		{
			this.logger = log;
			this.utility = util;
		}

		#region Public
		#region old way
		public TokenRS GetSDSToken()
		{
			#region Token
			TokenRS token = null;
			//if (UDI.VTOD.Common.DTO.Static.Token.TokenRS == null || UDI.VTOD.Common.DTO.Static.Token.DateTime.AddMinutes(10) <= DateTime.Now)
			//{
			#region Read from Config
			var username = System.Configuration.ConfigurationManager.AppSettings["SDS_Username"];
			var password = System.Configuration.ConfigurationManager.AppSettings["SDS_Password"];
			var fleetType = "Taxi";
			var version = System.Configuration.ConfigurationManager.AppSettings["Version"];
			#endregion

			#region Making TokenRQ
			var tokenRQ = new UDI.VTOD.Common.DTO.OTA.TokenRQ();
			tokenRQ.Username = username;
			tokenRQ.Password = password;
			tokenRQ.FleetType = fleetType;
			tokenRQ.Version = version;
			#endregion

			#region Get TokenRS
			var queryController = new UDI.VTOD.Controller.QueryController();
			token = queryController.GetToken(tokenRQ);
			#endregion

			#region Modify Token
			token.ClientIPAddress = System.Configuration.ConfigurationManager.AppSettings["SDS_TokenClientIPAddress"];
			token.ClientType = System.Configuration.ConfigurationManager.AppSettings["SDS_TokenClientType"].ToInt32();
			#endregion

			//UDI.VTOD.Common.DTO.Static.Token.TokenRS = token;
			//UDI.VTOD.Common.DTO.Static.Token.DateTime = DateTime.Now;
			//}
			//else
			//{
			//	token = UDI.VTOD.Common.DTO.Static.Token.TokenRS;
			//}

			return token;
			#endregion

		}

		public Common.DTO.OTA.OTA_GroundBookRS Book(Class.TaxiBookingParameter tbp,int fleetTripCode)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			Stopwatch swEachPart = new Stopwatch();
			OTA_GroundBookRS response = null;
			DateTime requestDateTime = DateTime.Now;
			DateTime responseDateTime;

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion

			DateTime? requestedLocalTime = null;
			if (tbp.Fleet != null && tbp.Fleet.ServerUTCOffset != null)
			{
				requestedLocalTime = DateTime.UtcNow.FromUtcNullAble(tbp.Fleet.ServerUTCOffset);
			}
			try
			{
				using (var client = new CCSiService.TripSoapClient())
				{
					#region Trip_CreateExtended
					//string CCSiSource = ""; //Flee based, from DB
					//string CCSiFleetId = ""; //Fleet based from DB

					//replace the street number in pickup address
					if (tbp.PickupAddress.StreetNo.ToArray().Where(x => x.Equals('–')).Any())
					{
						logger.InfoFormat("The orginial pickup street No. is {0}", tbp.PickupAddress.StreetNo);
						string[] sep = { "–" };
						string[] streetNumbers = tbp.PickupAddress.StreetNo.Split(sep, StringSplitOptions.RemoveEmptyEntries);
						tbp.PickupAddress.StreetNo = streetNumbers[0];
						tbp.Trip.taxi_trip.RemarkForPickup = string.Format("{0} Customer on {1} block of {2}", tbp.Trip.taxi_trip.RemarkForPickup, tbp.PickupAddress.StreetNo, tbp.PickupAddress.Street);
						logger.InfoFormat("Remove range street number in pickup address. Pickup street No. is {0}", tbp.PickupAddress.StreetNo);
						logger.InfoFormat("Pickup Comment: {0}", tbp.Trip.taxi_trip.RemarkForPickup);
						if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_CCSi, "Book_ReplacePickupStreetNumberRange");
					}

					//replace the street number in drop address
					if (tbp.DropOffAddress != null && !string.IsNullOrWhiteSpace(tbp.DropOffAddress.StreetNo) && tbp.DropOffAddress.StreetNo.ToArray().Where(x => x.Equals('–')).Any())
					{
						logger.InfoFormat("The orginial dropoff street No. is {0}", tbp.DropOffAddress.StreetNo);
						string[] sep = { "–" };
						string[] streetNumbers = tbp.DropOffAddress.StreetNo.Split(sep, StringSplitOptions.RemoveEmptyEntries);
						tbp.DropOffAddress.StreetNo = streetNumbers[0];
						logger.InfoFormat("Remove range street number in dropoff address. Dropoff street No. is {0}", tbp.DropOffAddress.StreetNo);
						if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_CCSi, "Book_ReplaceDropoffStreetNumberRange");
					}



					//remove the phone number in comment
					if (Regex.IsMatch(tbp.Trip.taxi_trip.RemarkForPickup, @"CUSTOMER PHONE: [\d]+", RegexOptions.IgnoreCase))
					{
						tbp.Trip.taxi_trip.RemarkForPickup = Regex.Replace(tbp.Trip.taxi_trip.RemarkForPickup, @"CUSTOMER PHONE: [\d]+", "", RegexOptions.IgnoreCase);
						logger.Info("Remove cusomter phone in comment.");
						logger.InfoFormat("Pickup Comment: {0}", tbp.Trip.taxi_trip.RemarkForPickup);
						if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_CCSi, "Book_ReplacePhoneNumberInComment");
					}

					//replace ztrip to z trip
					if (Regex.IsMatch(tbp.Trip.taxi_trip.RemarkForPickup, @"zTrip", RegexOptions.IgnoreCase))
					{
						tbp.Trip.taxi_trip.RemarkForPickup = Regex.Replace(tbp.Trip.taxi_trip.RemarkForPickup, @"zTrip", "Z Trip", RegexOptions.IgnoreCase);
						logger.Info("Replace zTrip with Z Trip");
						logger.InfoFormat("Pickup Comment: {0}", tbp.Trip.taxi_trip.RemarkForPickup);
						if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_CCSi, "Book_ReplaceZtripWording");
					}


					//replace pickup zipCode with 5 digits zipcode only
					if (Regex.IsMatch(tbp.PickupAddress.ZipCode, @"([\d]+)\-[\d]+"))
					{
						logger.InfoFormat("Original pickup zipCode: {0}", tbp.PickupAddress.ZipCode);
						tbp.PickupAddress.ZipCode = Regex.Match(tbp.PickupAddress.ZipCode, @"([\d]+)\-[\d]+", RegexOptions.IgnoreCase).Groups[1].Value;
						logger.InfoFormat("Replace: {0}", tbp.PickupAddress.ZipCode);
						if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_CCSi, "Book_ReplacePickupZipCode");
					}

					//replace dropoff zipCode with 5 digits zipcode only
					if (tbp.DropOffAddress != null && Regex.IsMatch(tbp.DropOffAddress.ZipCode, @"([\d]+)\-[\d]+"))
					{
						logger.InfoFormat("Original dropoff zipCode: {0}", tbp.DropOffAddress.ZipCode);
						tbp.DropOffAddress.ZipCode = Regex.Match(tbp.DropOffAddress.ZipCode, @"([\d]+)\-[\d]+", RegexOptions.IgnoreCase).Groups[1].Value;
						logger.InfoFormat("Replace: {0}", tbp.DropOffAddress.ZipCode);
						if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_CCSi, "Book_ReplaceDropoffZipCode");
					}




					swEachPart.Restart();
					var serviceResult = client.Trip_CreateExtended(
						tbp.Fleet.CCSiSource, //"9970", //Source 
						tbp.Fleet.CCSiFleetId, //"1",//Fleet
						tbp.Trip.taxi_trip.AccountNumber,//chargenbr
						tbp.PhoneNumber,//!string.IsNullOrWhiteSpace(trip.ClientPhone1) ? trip.ClientPhone1.CleanPhone10Digit() : trip.ClientPhone2.CleanPhone10Digit(), //PkupPhNbr 
						"", //PkupPhExt 
						"", // PkupLocation 
						Convert.ToInt32(Regex.Match(tbp.PickupAddress.StreetNo, "([0-9]+)").Groups[1].Value),//trip.PickupAddress.StreetNo.ToInt32(), //PkupStrNbr 
						tbp.PickupAddress.Street,//trip.PickupAddress.Street, //PkupStrName 
						tbp.PickupAddress.ApartmentNo,//trip.PickupAddress.ApartmentNo, //PkupApt 
						tbp.PickupAddress.City,//trip.PickupAddress.City, //PkupCity 
						tbp.PickupAddress.State,//trip.PickupAddress.State, //PkupState 
						tbp.PickupAddress.ZipCode,//trip.PickupAddress.ZipCode, //PkupZip 
						tbp.PickupDateTime.ToString("yyyy-MM-dd HH:mm:ss"),//System.Convert.ToDateTime(trip.PickupTime).ToString("yyyy-MM-dd HH:mm:ss"), //PkupDateTime YYYY-MM-DD HH:MM:SS (in 24 hour format) 
						Convert.ToDouble(tbp.PickupAddress.Geolocation.Latitude),//trip.PickupAddress.Latitude.Value.ToDouble(), //PkupLat 
						Convert.ToDouble(tbp.PickupAddress.Geolocation.Longitude), //trip.PickupAddress.Longitude.Value.ToDouble(), //PkupLon 
						tbp.Trip.taxi_trip.RemarkForPickup,//trip.PU_Comments, //PkupComment 
						tbp.FirstName,//!string.IsNullOrWhiteSpace(trip.ClientFullName) ? trip.ClientFullName.Split(' ').FirstOrDefault() : string.Empty, //PassFirstName 
						tbp.LastName,//!string.IsNullOrWhiteSpace(trip.ClientFullName) ? trip.ClientFullName.Split(' ').LastOrDefault() : string.Empty,  //PassLastName 
						"",//DestPhNbr
						"", //DestPhExt 
						"", //DestLocation 
						(tbp.DropOffAddress != null) ? Convert.ToInt32(Regex.Match(tbp.DropOffAddress.StreetNo, "([0-9]+)").Groups[1].Value) : 0,//trip.DropoffAddress.StreetNo.ToInt32(), //DestStrNbr 
						(tbp.DropOffAddress != null) ? tbp.DropOffAddress.Street : "", //DestStrName 
						(tbp.DropOffAddress != null) ? tbp.DropOffAddress.ApartmentNo : "", //DestApt 
						(tbp.DropOffAddress != null) ? tbp.DropOffAddress.City : "", //DestCity 
						(tbp.DropOffAddress != null) ? tbp.DropOffAddress.State : "", //DestState 
						(tbp.DropOffAddress != null) ? tbp.DropOffAddress.ZipCode : "", //DestZip 
						(tbp.DropOffAddress != null && tbp.DropOffDateTime.HasValue) ? tbp.DropOffDateTime.Value.ToString("yyyy-MM-dd HH:mm:ss") : string.Empty,//trip.DropoffTime.HasValue ? System.Convert.ToDateTime(trip.DropoffTime).ToString("yyyy-MM-dd HH:mm:ss") : string.Empty, //DestDateTime 
						(tbp.DropOffAddress != null && tbp.DropOffAddress.Geolocation != null) ? Convert.ToDouble(tbp.DropOffAddress.Geolocation.Latitude) : 0,//trip.DropoffAddress.Latitude.Value.ToDouble(), //DestLat 
						(tbp.DropOffAddress != null && tbp.DropOffAddress.Geolocation != null) ? Convert.ToDouble(tbp.DropOffAddress.Geolocation.Longitude) : 0,//trip.DropoffAddress.Longitude.Value.ToDouble(), //DestLon 
						"P", //TripType
						0.0, //CallRate
						"", //VehicleAttributes 
						"", //RequestedBy
						"", //Custome_Field1
						"",//trip.FareAmount.HasValue ? System.Math.Round(trip.FareAmount.Value, 2).ToString() : "0.00", //Custoem_Field2
						"", //CustomField3
						"", //CustomField4
						"", //Custom_Field5
						0.0//trip.FareAmount.HasValue ? System.Math.Round(trip.FareAmount.Value, 2).ToDouble() : 0//Co_Pay
					);
					swEachPart.Stop();
					logger.DebugFormat("Call CCSi API to book a trip. Duration:{0} ms.", sw.ElapsedMilliseconds);

					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_CCSi, "Book_SendingBookingRequest");
					#endregion

					if (serviceResult.result)
					{
						responseDateTime = DateTime.Now;
						string confirmationNumber = serviceResult.confNbr.ToString();
						if (!string.IsNullOrWhiteSpace(confirmationNumber))
						{
                            tbp.FleetTripCode = fleetTripCode;
                            tbp.Trip = utility.UpdateTaxiTrip(tbp.Trip, confirmationNumber, tbp.FleetTripCode);

							if (tbp.EnableResponseAndLog)
							{
								GroundReservation reservation = tbp.request.GroundReservations.FirstOrDefault();
								response = new OTA_GroundBookRS();
								response.EchoToken = tbp.request.EchoToken;
								response.Target = tbp.request.Target;
								response.Version = tbp.request.Version;
								response.PrimaryLangID = tbp.request.PrimaryLangID;

								response.Success = new Success();

								//Reservation
								response.Reservations = new List<Reservation>();
								Reservation r = new Reservation();
								r.Confirmation = new Confirmation();
								//r.Confirmation.Type = "???";
								r.Confirmation.ID = tbp.Trip.Id.ToString();
								r.Confirmation.Type = ConfirmationType.Confirmation.ToString();

								r.Passenger = reservation.Passenger;
								r.Service = reservation.Service;
								r.Confirmation.TPA_Extensions = new TPA_Extensions();
								//r.Confirmation.TPA_Extensions.DispatchConfirmation = new Confirmation();
								//r.Confirmation.TPA_Extensions.DispatchConfirmation.ID = tbp.Trip.DispatchTripId;
								r.Confirmation.TPA_Extensions.Confirmations = new List<Confirmation>();
								r.Confirmation.TPA_Extensions.Confirmations.Add(new Confirmation { ID = tbp.Trip.taxi_trip.DispatchTripId, Type = ConfirmationType.DispatchConfirmation.ToString() });// = tbp.Trip.DispatchTripId;
								//r.Confirmation.TPA_Extensions.Statuses = new Statuses();
								//r.Confirmation.TPA_Extensions.Statuses.Status = new List<Common.DTO.OTA.Status>();
								response.Reservations.Add(r);

								#region Put status into taxi_trip_status
								//utility.SaveTripStatus(tbp.Trip.Id, TaxiTripStatusType.Booked);



								utility.SaveTripStatus(tbp.Trip.Id, TaxiTripStatusType.Booked, requestedLocalTime, null, null, null, null, null, null, null, null, string.Empty, string.Empty, null);
								#endregion

								//#region Set final status
								//utility.UpdateFinalStatus(tbp.Trip.Id, TaxiTripStatusType.Booked);
								//#endregion

							}
							else
							{
								logger.Info("Disable response and log for booking");
							}
						}
						else
						{
							string error = string.Format("Cannot get DispatchTripId. Or dispatchTripId is white space. message={0} ", serviceResult.errorMsg);
							logger.Error(error);
							utility.MarkThisTripAsError(tbp.Trip.Id, serviceResult.errorMsg);
							throw new TaxiException(error);
						}

						if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_CCSi, "Book_CreateResponseObject");
					}
					else
					{
						logger.Error(serviceResult.errorMsg);
						utility.MarkThisTripAsError(tbp.Trip.Id, serviceResult.errorMsg);
						throw new TaxiException(serviceResult.errorMsg);
					}

					#region Write Taxi Log
					if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
					{
						string requestMessage = "";
						requestMessage = tbp.XmlSerialize().ToString();

						string responseMessage = "";
						responseMessage = serviceResult.XmlSerialize().ToString();

						utility.WriteTaxiLog(tbp.Trip.Id, TaxiServiceAPIConst.CCSi, TaxiServiceMethodType.Book, null, requestMessage, responseMessage, requestDateTime, responseDateTime);
					}
					#endregion
				}
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);

				//change the status of trip
				utility.UpdateFinalStatus(tbp.Trip.Id, TaxiTripStatusType.Error, null, null, null, null, null);

				throw ex;
			}

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_CCSi, "Book");
			#endregion

			return response;
		}

		public Common.DTO.OTA.OTA_GroundResRetrieveRS Status(Class.TaxiStatusParameter tsp)
		{
			//CCis pushs status information through UDI.VTOD.CCSiWeb

			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			OTA_GroundResRetrieveRS response = null;

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion

			try
			{
				DateTime requestDateTime = DateTime.Now;
				DateTime responseDateTime;

				response = new OTA_GroundResRetrieveRS();
				response.Success = new Success();
				response.EchoToken = tsp.request.EchoToken;
				response.Target = tsp.request.Target;
				response.Version = tsp.request.Version;
				response.PrimaryLangID = tsp.request.PrimaryLangID;
				response.TPA_Extensions = new TPA_Extensions();
				response.TPA_Extensions.Driver = utility.ExtractDriverName(tsp.Trip.DriverName);
				response.TPA_Extensions.Vehicles = new Vehicles();
				response.TPA_Extensions.Vehicles.Items = new List<Vehicle>();
				var vehicle = new Vehicle();

				#region Vehicle
				if (tsp.lastTripStatus != null)
				{
					vehicle.MinutesAway = tsp.lastTripStatus.ETA.HasValue ? tsp.lastTripStatus.ETA.Value : 0;
					vehicle.MinutesAwayWithTraffic = tsp.lastTripStatus.ETAWithTraffic.HasValue ? tsp.lastTripStatus.ETAWithTraffic.Value : 0;

					if (vehicle.MinutesAwayWithTraffic < 3)
						vehicle.MinutesAwayWithTraffic = 3;

					if (tsp.lastTripStatus.VehicleLatitude.HasValue && tsp.lastTripStatus.VehicleLongitude.HasValue)
					{
						vehicle.Geolocation = new Geolocation();
						vehicle.Geolocation.Lat = tsp.lastTripStatus.VehicleLatitude.Value;
						vehicle.Geolocation.Long = tsp.lastTripStatus.VehicleLongitude.Value;

					}
					vehicle.ID = tsp.lastTripStatus.VehicleNumber;
				}

				#region ETA
				if (tsp.lastTripStatus.ETA.HasValue)
				{
					vehicle.MinutesAway = Convert.ToDecimal(tsp.lastTripStatus.ETA.Value);
				}
				#endregion
				response.TPA_Extensions.Vehicles.Items.Add(vehicle);

				#endregion

				#region Making the result
				//Convert status to front-end status
				//string frontEndStatus = utility.ConvertTripStatusMessageForFrontEndDevice(tsp.Fleet_User.UserId, tsp.lastTripStatus.Status, tsp.Fleet.FastMeterAction);
				#region Front End Status
				string frontEndStatus = utility.ConvertTripStatusMessageForFrontEndDevice(tsp.Fleet_User.UserId, tsp.lastTripStatus.Status);
				#endregion


				Status s = new Common.DTO.OTA.Status { Value = frontEndStatus, Fare = tsp.lastTripStatus.Fare.HasValue ? tsp.lastTripStatus.Fare.Value : 0 };
				response.TPA_Extensions.Statuses = new Statuses();
				response.TPA_Extensions.Statuses.Status = new List<Status>();
				response.TPA_Extensions.Statuses.Status.Add(s);
				response.TPA_Extensions.Contacts = new Contacts();
				response.TPA_Extensions.Contacts.Items = new List<Contact>();


				#region Adding Driver Info and DispatchInfo


				vtod_driver_info driverInfo = null;
				if (tsp.Trip.DriverInfoID.HasValue && tsp.Trip.DriverInfoID.Value > 0)
				{
					using (var db = new DataAccess.VTOD.VTODEntities())
					{
						driverInfo = db.vtod_driver_info.FirstOrDefault(t => t.Id == tsp.Trip.DriverInfoID.Value);
					}
				}
				else
				{

					if (tsp.tokenVTOD != null && tsp.lastTripStatus.DriverId != null && !string.IsNullOrWhiteSpace(tsp.lastTripStatus.DriverId))
					{
						driverInfo = utility.GetDriverInfoByDriverId(tsp.tokenVTOD.Username, tsp.Trip, tsp.lastTripStatus.Status, tsp.lastTripStatus.DriverId);
					}
					if (driverInfo == null)
					{
						if (tsp.tokenVTOD != null && !string.IsNullOrWhiteSpace(vehicle.ID))
						{
							driverInfo = utility.GetDriverInfoByVehicleNumber(tsp.tokenVTOD.Username, tsp.lastTripStatus.Status, vehicle.ID, "", tsp.Trip);
						}
					}
				}


				Contact dispatchContact = null;
				Contact driverContact = null;

				//Disptch Info
				string dispatchPhone = tsp.Fleet.PhoneNumber;
				#region Work around for zTrip ver 2.2, please delete this part after release of zTrip ver 2.3
				//if (driverInfo != null && !string.IsNullOrEmpty(driverInfo.CellPhone))
				//{
				//	dispatchPhone = driverInfo.CellPhone;
				//}
				#endregion
				//todo add logic here for expedia and other companies logos
				if (tsp.User.name.ToLower() == "ztrip" || tsp.User.name.ToLower() == "udi")
				{
                    dispatchContact = new Contact { Type = Common.DTO.Enum.ContactType.Dispatch.ToString(), Name = tsp.Fleet.Alias, Telephone = new Telephone { PhoneNumber = dispatchPhone.CleanPhone10Digit() }, PhotoUrl = ConfigHelper.GetDriverPhotoVersionDefault() };

				}
				else
				{
					dispatchContact = new Contact { Type = Common.DTO.Enum.ContactType.Dispatch.ToString(), Name = tsp.Fleet.Alias, Telephone = new Telephone { PhoneNumber = dispatchPhone.CleanPhone10Digit() } };

				}
				response.TPA_Extensions.Contacts.Items.Add(dispatchContact);

				//Driver Info
				if (driverInfo != null && !string.IsNullOrEmpty(driverInfo.CellPhone))
				{
					driverContact = new Contact { Type = Common.DTO.Enum.ContactType.Driver.ToString(), Name = string.Format("{0} {1}", driverInfo.FirstName, driverInfo.LastName), Telephone = new Telephone { PhoneNumber = driverInfo.CellPhone.CleanPhone10Digit() }, PhotoUrl = driverInfo.PhotoUrl, ZtripDateOfDesignation = driverInfo.QualificationDate, ZtripDesignation = driverInfo.zTripQualified };
					response.TPA_Extensions.Contacts.Items.Add(driverContact);
				}
				#endregion


				var contact = new Contact { Type = Common.DTO.Enum.ContactType.Dispatch.ToString(), Name = tsp.Fleet.Alias, Telephone = new Telephone { PhoneNumber = tsp.Fleet.PhoneNumber } };
				response.TPA_Extensions.Contacts.Items.Add(contact);

				if (tsp.Trip != null && tsp.Trip.taxi_trip.PickupLatitude.HasValue && tsp.Trip.taxi_trip.PickupLatitude.Value != 0 && tsp.Trip.taxi_trip.PickupLongitude.HasValue && tsp.Trip.taxi_trip.PickupLongitude.Value != 0)
				{
					response.TPA_Extensions.PickupLocation = new Pickup_Dropoff_Stop();
					response.TPA_Extensions.PickupLocation.Address = new Common.DTO.OTA.Address();
					response.TPA_Extensions.PickupLocation.Address.Latitude = tsp.Trip.taxi_trip.PickupLatitude.Value.ToString();
					response.TPA_Extensions.PickupLocation.Address.Longitude = tsp.Trip.taxi_trip.PickupLongitude.Value.ToString();
				}
				#endregion

				responseDateTime = DateTime.Now;

				#region Write Taxi Log
				if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
				{

					string requestMessage = "";
					requestMessage = tsp.XmlSerialize().ToString();

					string responseMessage = "";
					responseMessage = response.XmlSerialize().ToString();

					utility.WriteTaxiLog(tsp.Trip.Id, TaxiServiceAPIConst.CCSi, TaxiServiceMethodType.Status, null, requestMessage, responseMessage, requestDateTime, responseDateTime);
				}
				#endregion

			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw new TaxiException(ex.Message);
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);


			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_CCSi, "Status");
			#endregion

			return response;
		}

		public Common.DTO.OTA.OTA_GroundCancelRS Cancel(Class.TaxiCancelParameter tcp)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			OTA_GroundCancelRS response = null;
			DateTime requestDateTime = DateTime.Now;
			DateTime responseDateTime;

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion

			DateTime? requestedLocalTime = null;
			if (tcp.Fleet != null && tcp.Fleet.ServerUTCOffset != null)
			{
				requestedLocalTime = DateTime.UtcNow.FromUtcNullAble(tcp.Fleet.ServerUTCOffset);
			}

			try
			{
				TRIP_CANCEL_RESULT serviceResult = null;
				if (utility.IsAbleToCancelThisTrip(tcp.Trip.FinalStatus))
				{

					using (var client = new CCSiService.TripSoapClient())
					{
						serviceResult = client.Trip_Cancel(tcp.Fleet.CCSiSource, Convert.ToInt32(tcp.Trip.taxi_trip.DispatchTripId));

						if (serviceResult.result)
						{
							responseDateTime = DateTime.Now;

							response = new OTA_GroundCancelRS();
							response.Success = new Success();
							response.EchoToken = tcp.request.EchoToken;
							response.Target = tcp.request.Target;
							response.Version = tcp.request.Version;
							response.PrimaryLangID = tcp.request.PrimaryLangID;

							response.Reservation = new Reservation();
							response.Reservation.CancelConfirmation = new CancelConfirmation();
							response.Reservation.CancelConfirmation.UniqueID = new UniqueID { ID = tcp.Trip.Id.ToString() };

							#region Put status into taxi_trip_status
							//utility.SaveTripStatus(tcp.Trip.Id, TaxiTripStatusType.Canceled);
							//var time = DateTime.UtcNow.t
							//tcp.Fleet.ServerUTCOffset
							utility.SaveTripStatus(tcp.Trip.Id, TaxiTripStatusType.Canceled, requestedLocalTime, null, null, null, null, null, null, null, null, string.Empty, string.Empty, null);

							#region Set final status
							utility.UpdateFinalStatus(tcp.Trip.Id, TaxiTripStatusType.Canceled, null, null, null, null, null);
							#endregion

							#endregion
						}
						else
						{
							string error = "Fail to do cancel. ";
							logger.Error(error);
							logger.Error(serviceResult.result);
							//throw new TaxiException(string.Format("{0}.\r\n{1}", error, serviceResult.result));
							throw VtodException.CreateException(ExceptionType.Taxi, 5001); //throw new TaxiException(Messages.Taxi_CancelFailure);
						}
					}
				}
				else
				{
					throw VtodException.CreateException(ExceptionType.Taxi, 5001); //throw new TaxiException(Messages.Taxi_CancelFailure);
				}

				#region Write Taxi Log
				if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
				{

					string requestMessage = "";
					requestMessage = tcp.XmlSerialize().ToString();

					string responseMessage = "";
					responseMessage = serviceResult.XmlSerialize().ToString();

					utility.WriteTaxiLog(tcp.Trip.Id, TaxiServiceAPIConst.CCSi, TaxiServiceMethodType.Cancel, null, requestMessage, responseMessage, requestDateTime, responseDateTime);
				}
				#endregion
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				//throw new TaxiException(ex.Message);
				throw;// VtodException.CreateException(ExceptionType.Taxi, 5001); //throw new TaxiException(Messages.Taxi_CancelFailure);
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_CCSi, "Cancel");
			#endregion

			return response;
		}

		public Common.DTO.OTA.OTA_GroundAvailRS GetVehicleInfo(Class.TaxiGetVehicleInfoParameter tgvip)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			var response = new OTA_GroundAvailRS();
			response.Success = new Success();
			response.TPA_Extensions = new TPA_Extensions();
			response.TPA_Extensions.Vehicles = new Vehicles();
			response.TPA_Extensions.Vehicles.Items = new List<Vehicle>();
			DateTime requestDateTime = DateTime.Now;
			DateTime responseDateTime;

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion

			DateTime? requestedLocalTime = null;
			if (tgvip.Fleet != null && tgvip.Fleet.ServerUTCOffset != null)
			{
				requestedLocalTime = DateTime.UtcNow.FromUtcNullAble(tgvip.Fleet.ServerUTCOffset);
			}

			try
			{
				AVAIL_VEH_RESULTS serviceResult = null;
				using (var client = new CCSiService.TripSoapClient())
				{

					//convert distance to Mile
					tgvip.Radius = Convert.ToDouble(Convert.ToDecimal(tgvip.Radius).ConvertDistance(UDI.Utility.DTO.Enum.DistanceUnit.Mile, UDI.Utility.DTO.Enum.DistanceUnit.Feet));


					serviceResult = client.AvailableVehiclesForLatLong(tgvip.Fleet.CCSiFleetId, Convert.ToDouble(tgvip.Latitude), Convert.ToDouble(tgvip.Longtitude), Convert.ToInt32(tgvip.Radius));
					responseDateTime = DateTime.Now;
					MapService ms = new MapService();
					if (serviceResult.vehicles.Any())
					{
                       
						#region find nearest vehicle
						int shortestIndex = -1;
						double? shortestDistance = null;
						for (int index = 0; index < serviceResult.vehicles.Count(); index++)
						{
							double d = ms.GetDirectDistanceInMile(tgvip.Latitude, tgvip.Longtitude, serviceResult.vehicles[index].latitude, serviceResult.vehicles[index].longitude);
							serviceResult.vehicles[index].Distance = d;
							if (shortestDistance.HasValue)
							{
								if (shortestDistance.Value > d)
								{
									shortestDistance = d;
									shortestIndex = index;
								}
							}
							else
							{
								shortestDistance = d;
								shortestIndex = index;
							}
						}



						if (shortestIndex != -1)
						{
							//get ETA
							var rc = new UDI.SDS.RoutingController(tgvip.tokenVTOD, null);
							UDI.SDS.RoutingService.Point start = new UDI.SDS.RoutingService.Point { Latitude = tgvip.Latitude, Longitude = tgvip.Longtitude };
							UDI.SDS.RoutingService.Point end = new UDI.SDS.RoutingService.Point { Longitude = serviceResult.vehicles[shortestIndex].longitude, Latitude = serviceResult.vehicles[shortestIndex].latitude };
							var result = rc.GetRouteMetrics(start, end);
							response.TPA_Extensions.Vehicles.NearestVehicleETA = result.TotalMinutes.ToString();
						}

						response.TPA_Extensions.Vehicles.NumberOfVehicles = serviceResult.vehicles.Count().ToString();
						#endregion
                        Random rnd = new Random();
						#region put all vehicle in the response
						foreach (var item in serviceResult.vehicles.OrderBy(s => s.Distance))
						{
							var vehicle = new Vehicle();
							vehicle.Geolocation = new Geolocation();
							vehicle.Geolocation.Long = decimal.Parse(item.longitude.ToString());
							vehicle.Geolocation.Lat = decimal.Parse(item.latitude.ToString());
							//vehicle.Geolocation.PriorLong = decimal.Parse(item.PriorLongitude.ToString());
							//vehicle.Geolocation.PriorLat = decimal.Parse(item.PriorLatitude.ToString());
                            if (item.direction != 0 && item.direction!=null)
                                vehicle.Course = item.direction;
                            else
                                vehicle.Course = rnd.Next(1, 359);
							vehicle.Distance = item.Distance.ToDecimal();
							//vehicle.Distance = item.Distance;
							//vehicle.DriverID = item.DriverID;
							//vehicle.DriverName = item.DriverName;
							//vehicle.FleetID = tgvip.Fleet.Id.ToString();
							vehicle.ID = item.vehicleNbr.ToString();
							vehicle.VehicleStatus = item.status;
							//vehicle.VehicleType = item.VehicleType;
							//vehicle.Zone = item.Zone;
							//vehicle.MinutesAway = Convert.ToDecimal(item.ETA.ToString());
							response.TPA_Extensions.Vehicles.Items.Add(vehicle);
						}
						#endregion
					}
					else
					{
						// no vehicles found
						logger.Info("Found no vehicles");
						response.TPA_Extensions.Vehicles.NumberOfVehicles = serviceResult.vehicles.Count().ToString();
					}
				}

				#region Write Taxi Log
				if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
				{
					string requestMessage = "";
					requestMessage = tgvip.XmlSerialize().ToString();

					string responseMessage = "";
					responseMessage = serviceResult.XmlSerialize().ToString();

					utility.WriteTaxiLog(null, TaxiServiceAPIConst.CCSi, TaxiServiceMethodType.GetVehicleInfo, null, requestMessage, responseMessage, requestDateTime, responseDateTime);
				}
				#endregion
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw new TaxiException(ex.Message);
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_CCSi, "GetVehicleInfo");
			#endregion

			return response;
		}

		public bool NotifyDriver(Class.TaxiSendMessageToVehicleParameter tsmtvp)
		{
			throw new NotImplementedException();
		}

		//public void ProcessPushedStatus(System.Collections.Specialized.NameValueCollection input)
		public void ProcessPushedStatus(TokenRS token, System.Collections.Specialized.NameValueCollection input)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion

			try
			{
				#region Convert Query String to status
				var pushedStatus = new CCSi_TaxiPushedStatus();
				pushedStatus.DispatchTripID = !string.IsNullOrWhiteSpace(input[CCSi_StatusConstant.TRIP_NBR]) ? input[CCSi_StatusConstant.TRIP_NBR] : input[CCSi_StatusConstant.GFC_PARAM_TRIP_NBR];
				pushedStatus.DispatchFleetID = !string.IsNullOrWhiteSpace(input[CCSi_StatusConstant.FLEET_ID]) ? input[CCSi_StatusConstant.FLEET_ID] : input[CCSi_StatusConstant.GFC_PARAM_API_KEY];
				pushedStatus.VehicleNumber = !string.IsNullOrWhiteSpace(input[CCSi_StatusConstant.VEHICLE_NBR]) ? input[CCSi_StatusConstant.VEHICLE_NBR] : input[CCSi_StatusConstant.GFC_PARAM_VEHICLE];
				pushedStatus.StatusTime = !string.IsNullOrWhiteSpace(input[CCSi_StatusConstant.DATE_TIME]) ? input[CCSi_StatusConstant.DATE_TIME] : input[CCSi_StatusConstant.GFC_PARAM_TIME];
				pushedStatus.StatusTime = HttpUtility.UrlDecode(pushedStatus.StatusTime);
				pushedStatus.Latitude = !string.IsNullOrWhiteSpace(input[CCSi_StatusConstant.GFC_PARAM_LAT]) ? input[CCSi_StatusConstant.GFC_PARAM_LAT] : input[CCSi_StatusConstant.LAT];
				pushedStatus.Longitude = !string.IsNullOrWhiteSpace(input[CCSi_StatusConstant.GFC_PARAM_LON]) ? input[CCSi_StatusConstant.GFC_PARAM_LON] : input[CCSi_StatusConstant.LON];
				pushedStatus.Status = input[CCSi_StatusConstant.GFC_PARAM_STATUS];
				pushedStatus.DriverID = input[CCSi_StatusConstant.GFC_PARAM_DRIVER_ID];
				pushedStatus.DriverName = input[CCSi_StatusConstant.GFC_PARAM_DRIVER];
				pushedStatus.FareAmount = input[CCSi_StatusConstant.GFC_PARAM_FARE];
				if (!string.IsNullOrWhiteSpace(pushedStatus.FareAmount))
				{
					logger.Info("Convert cent to dollar");
					logger.InfoFormat("cent: {0}", pushedStatus.FareAmount);
					pushedStatus.FareAmount = (Convert.ToDouble(pushedStatus.FareAmount) / 100.00).ToString("#.##");
					logger.InfoFormat("dollar: {0}", pushedStatus.FareAmount);
				}
				pushedStatus.Company = input[CCSi_StatusConstant.GFC_PARAM_COMPANY];
				if (!string.IsNullOrWhiteSpace(input[CCSi_StatusConstant.EVENT_ID]))
					pushedStatus.EventID = Convert.ToInt32(input[CCSi_StatusConstant.EVENT_ID]);
				if (!string.IsNullOrWhiteSpace(input[CCSi_StatusConstant.EVENT_STATUS]))
					pushedStatus.EventStatus = Convert.ToInt32(input[CCSi_StatusConstant.EVENT_STATUS]);
				pushedStatus.EventDesc = input[CCSi_StatusConstant.EVENT_DESC];

				//ETA
				if (pushedStatus.EventID == 20)
				{
					pushedStatus.ETA = !string.IsNullOrWhiteSpace(input[CCSi_StatusConstant.Info1]) ? input[CCSi_StatusConstant.Info1] : string.Empty;
				}
				#endregion

				#region Validate taxi trip status
				TaxiStatusParameter tsp = null;
				int? etaWithTraffic = null;

				if (utility.ValidateCCSiStatusRequest(pushedStatus, out tsp))
				{
					string unknownStatusMessage = null;
					string status = utility.ConvertTripStatusForCCSi(pushedStatus.EventID, pushedStatus.EventStatus.ToString(), out unknownStatusMessage);

					#region Convert status to InTransit or InService
					if (status == "GPS")
					{
						//check if there is a "Pcikup","Arrived", "MeterOn" in this trip 
						List<string> statusList = utility.GetPreviousTripStatus(tsp.Trip.Id);
                        if (statusList.Where(x => x.ToLowerInvariant() == "pickup" /*|| x.ToLowerInvariant() == "arrived" || */ || x.ToLowerInvariant() == "meteron").Any())
						{
							status = TaxiTripStatusType.InService;
						}
						else
						{
							status = TaxiTripStatusType.InTransit;
						}
					}

					#endregion

					#region Faster Meter option
					//01. if status is Completed or Fare
					//02. and fare exists and (Fare<=Min)
					//03. then change status to fast meter
					switch (tsp.Fleet.DetectFastMeter)
					{
						case FastMeterOption.ByMinFare:
							logger.Info("Detect fastmeter option: by min fare");
							if (utility.DetectFastMeterByMinFare(tsp.Trip.Id, status, pushedStatus.FareAmount, tsp.Fleet.MinPriceFastMeter))
							{
								status = TaxiTripStatusType.FastMeter;
							}
							break;
						case FastMeterOption.Off:
							logger.Info("Detect fastmeter option is off");
							break;
						default:
							logger.Info("Detect fastmeter option is off");
							break;
					}
					#endregion

					#region Caculate ETA by ourself
					int? minutesAway = Convert.ToInt32(pushedStatus.ETA);

					//int minutesAway = 0;

					try
					{
						if (
							string.IsNullOrWhiteSpace(pushedStatus.ETA)
							&&
							(!string.IsNullOrWhiteSpace(pushedStatus.Latitude) && !string.IsNullOrWhiteSpace(pushedStatus.Longitude))
							&&
							(pushedStatus.Latitude.ToDecimal() != 0 && pushedStatus.Longitude.ToDecimal() != 0)
							&&
							(tsp.Trip.taxi_trip.PickupLatitude.HasValue && tsp.Trip.taxi_trip.PickupLongitude.HasValue)
							&&
							(status.ToUpperInvariant().Equals(TaxiTripStatusType.Accepted.ToUpperInvariant()) || status.ToUpperInvariant().Equals(TaxiTripStatusType.Assigned.ToUpperInvariant()) || status.ToUpperInvariant().Equals(TaxiTripStatusType.InTransit.ToUpperInvariant()))
							)
						{
							//tsp.tokenVTOD = GetSDSToken();
							tsp.tokenVTOD = token;
							MapService ms = new MapService();
							double d = ms.GetDirectDistanceInMeter(tsp.Trip.taxi_trip.PickupLatitude.Value, tsp.Trip.taxi_trip.PickupLongitude.Value, Convert.ToDouble(pushedStatus.Latitude), Convert.ToDouble(pushedStatus.Longitude));
							var rc = new UDI.SDS.RoutingController(tsp.tokenVTOD, null);
							UDI.SDS.RoutingService.Point start = new UDI.SDS.RoutingService.Point { Latitude = Convert.ToDouble(pushedStatus.Latitude), Longitude = Convert.ToDouble(pushedStatus.Longitude) };
							UDI.SDS.RoutingService.Point end = new UDI.SDS.RoutingService.Point { Latitude = tsp.Trip.taxi_trip.PickupLatitude.Value, Longitude = tsp.Trip.taxi_trip.PickupLongitude.Value };
							var result = rc.GetRouteMetrics(start, end);
							minutesAway = result.TotalMinutes;

						}
						else if (!string.IsNullOrWhiteSpace(pushedStatus.ETA))
						{
							minutesAway = Convert.ToInt32(pushedStatus.ETA);
						}
						else
						{
							minutesAway = null;
						}
					}
					catch (Exception ex)
					{
						logger.Error(ex);
						//string.Format("{0} Error: {1}", System.DateTime.Now, ex.Message).WriteToConsole();
					}

					#endregion

					#region ETA with Traffic
					try
					{
						if (
							string.IsNullOrWhiteSpace(pushedStatus.ETA)
							&&
							(!string.IsNullOrWhiteSpace(pushedStatus.Latitude) && !string.IsNullOrWhiteSpace(pushedStatus.Longitude))
							&&
							(pushedStatus.Latitude.ToDecimal() != 0 && pushedStatus.Longitude.ToDecimal() != 0)
							&&
							(tsp.Trip.taxi_trip.PickupLatitude.HasValue && tsp.Trip.taxi_trip.PickupLongitude.HasValue)
							&&
							(status.ToUpperInvariant().Equals(TaxiTripStatusType.Accepted.ToUpperInvariant()) || status.ToUpperInvariant().Equals(TaxiTripStatusType.Assigned.ToUpperInvariant()) || status.ToUpperInvariant().Equals(TaxiTripStatusType.InTransit.ToUpperInvariant()))
							)
						{

							//ToDo: it should be configurable. It's hardcoded just for now but should be configuratble.
							if (tsp.User.name.ToLower() == "ztrip" || !tsp.Trip.DriverAcceptedETA.HasValue)
							{
								//if (
								//						status.Trim().ToLower() == Const.TaxiTripStatusType.Accepted.Trim().ToLower() ||
								//						status == Const.TaxiTripStatusType.Assigned.Trim().ToLower() ||
								//						status == Const.TaxiTripStatusType.InTransit.Trim().ToLower()
								//						)
								//{
								//if (vehicle != null && vehicle.Geolocation != null && vehicle.Geolocation.Lat != 0 && vehicle.Geolocation.Long != 0)
								//{
								//if (tsp.Trip != null && tsp.Trip.taxi_trip != null && tsp.Trip.taxi_trip.PickupLatitude.HasValue && tsp.Trip.taxi_trip.PickupLatitude.Value != 0 && tsp.Trip.taxi_trip.PickupLongitude.HasValue && tsp.Trip.taxi_trip.PickupLongitude.Value != 0)
								//{
								try
								{
									var error = string.Empty;
									var map = new UDI.Map.MapService(Map.DTO.MapApiProvider.Google);
									var etaResult = map.GetETA(new Map.DTO.Geolocation { Latitude = pushedStatus.Latitude.ToDecimal(), Longitude = pushedStatus.Longitude.ToDecimal() },
										new Map.DTO.Geolocation { Latitude = tsp.Trip.taxi_trip.PickupLatitude.Value.ToDecimal(), Longitude = tsp.Trip.taxi_trip.PickupLongitude.ToDecimal() }, UDI.Map.DTO.DistanceUnit.Mile, out error);

									if (string.IsNullOrWhiteSpace(error))
									{
										etaWithTraffic = System.Math.Round((decimal)(etaResult.DurationInTrafficInSecond / 60)).ToInt32();
										//vehicle.MinutesAwayWithTraffic = etaWithTraffic.ToDecimal();
									}
								}
								catch
								{ }
								//}
								//}
								//}
							}
						}
					}
					catch (Exception)
					{

						throw;
					}
					#endregion

					#region calculate trip time
					DateTime? tripStartTime = null;
					DateTime? tripEndTime = null;
					//DateTime statusTime = DateTime.Now;
					DateTime statusTime = DateTime.UtcNow.FromUtc(tsp.Fleet.ServerUTCOffset);




					if (DateTime.TryParseExact(pushedStatus.StatusTime, "yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.AllowWhiteSpaces, out statusTime))
					{
						//if (statusTime > DateTime.MinValue)
						//{
						//if (tsp.Fleet != null && tsp.Fleet.ServerUTCOffset != null)
						//{
						//	DateTime? dt = DateTime.UtcNow.FromUtcNullAble(tsp.Fleet.ServerUTCOffset);
						//	if (dt.HasValue)
						//	{
						//		statusTime = dt.Value;
						//	}
						//}
						//}
						if ((status.Trim().ToLower() == TaxiTripStatusType.InService.Trim().ToLower() || status.Trim().ToLower() == TaxiTripStatusType.MeterON.Trim().ToLower()) && (tsp.Trip.TripStartTime == null))
						{
							tripStartTime = statusTime;
						}
						else
						{
							tripStartTime = tsp.Trip.TripStartTime;
						}

						if ((status.Trim().ToLower() == TaxiTripStatusType.Completed.Trim().ToLower() || status.Trim().ToLower() == TaxiTripStatusType.MeterOff.Trim().ToLower()) && (tsp.Trip.TripEndTime == null))
						{
							tripEndTime = statusTime;
						}
						else
						{
							tripEndTime = tsp.Trip.TripEndTime;
						}
					}
					#endregion


					//Save Status
					string originalStatus = string.Format("EventStatus={0}, EventID={1}", pushedStatus.EventStatus, pushedStatus.EventID);


					//if (status.ToUpperInvariant() == TaxiTripStatusType.FastMeter.ToUpperInvariant())
					//{
					//    if (tsp.Fleet.FastMeterAction == TaxiFleetFastMeterAction.ReDispatch)
					//    {
					//        utility.SaveTripStatus_ReDispatch(tsp.Trip.Id, statusTime, null, null, null, null, null, null, null, string.Empty, originalStatus, null);
					//        utility.UpdateFinalStatus(tsp.Trip.Id, TaxiTripStatusType.Booked, null, null, null);
					//    }
					//    else if (tsp.Fleet.FastMeterAction == TaxiFleetFastMeterAction.Cancel)
					//    {
					//        utility.SaveTripStatus(tsp.Trip.Id, TaxiTripStatusType.FastMeter, statusTime, null, null, null, null, null, null, null, string.Empty, originalStatus, null);
					//        utility.UpdateFinalStatus(tsp.Trip.Id, TaxiTripStatusType.FastMeter, null, null, null);
					//    }
					//}

					if (status.ToUpperInvariant() == TaxiTripStatusType.FastMeter.ToUpperInvariant())
					{
						//FastMeter status 
						//utility.SaveTripStatus(tsp.Trip.Id, TaxiTripStatusType.FastMeter, statusTime, null, null, null, null, null, null, null, string.Empty, originalStatus, null);
						decimal? overwriteFare = utility.DetermineOverwriteFare(tsp, Convert.ToDecimal(pushedStatus.Longitude), Convert.ToDecimal(pushedStatus.Latitude), Convert.ToDecimal(pushedStatus.FareAmount));

						utility.SaveTripStatus(
							tsp.Trip.Id, TaxiTripStatusType.FastMeter, statusTime, pushedStatus.VehicleNumber,
							Convert.ToDecimal(pushedStatus.Latitude), Convert.ToDecimal(pushedStatus.Longitude), overwriteFare, Convert.ToDecimal(pushedStatus.FareAmount), minutesAway, etaWithTraffic, pushedStatus.DriverName, unknownStatusMessage, originalStatus, pushedStatus.DriverID);



						utility.UpdateFinalStatus(tsp.Trip.Id, TaxiTripStatusType.FastMeter, null, null, null, null, etaWithTraffic);
					}
					else
					{
						decimal? overwriteFare = utility.DetermineOverwriteFare(tsp, Convert.ToDecimal(pushedStatus.Longitude), Convert.ToDecimal(pushedStatus.Latitude), Convert.ToDecimal(pushedStatus.FareAmount));

						#region Add Fare to Completed Status
						if (status.ToLower() == Domain.Taxi.Const.TaxiTripStatusType.Completed.ToString().ToLower())
						{
							if (!overwriteFare.HasValue || overwriteFare.Value == 0)
							{
								var newFare = utility.GetFareAmountForCCSi(tsp.Trip.Id);
								if (newFare > 0)
								{
									overwriteFare = newFare;
								}
								//Put Fare into it; 
							}
						}
						#endregion

						utility.SaveTripStatus(
							tsp.Trip.Id, status, statusTime, pushedStatus.VehicleNumber,
							Convert.ToDecimal(pushedStatus.Latitude), Convert.ToDecimal(pushedStatus.Longitude), overwriteFare, Convert.ToDecimal(pushedStatus.FareAmount), minutesAway, etaWithTraffic, pushedStatus.DriverName, unknownStatusMessage, originalStatus, pushedStatus.DriverID);

						//Save final status
						//utility.UpdateFinalStatus(tsp.Trip.Id, status, Convert.ToDecimal(pushedStatus.FareAmount), tripStartTime, tripEndTime);
						if (tsp.Trip.FinalStatus.ToLower() != TaxiTripStatusType.FastMeter.ToLower())
						{
							utility.UpdateFinalStatus(tsp.Trip.Id, status, overwriteFare, null, tripStartTime, tripEndTime, etaWithTraffic);
						}
					}

				}
				#endregion

			}
			catch (Exception ex)
			{
				logger.Error(ex);
				//string.Format("{0} Error: {1}", System.DateTime.Now, ex.Message).WriteToConsole();
			}


			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_CCSi, "ProcessPushedStatus");
			#endregion

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
		}


		#endregion

		#region new way

		public OTA_GroundAvailRS GetEstimation(TokenRS tokenVTOD, OTA_GroundAvailRQ request)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			OTA_GroundAvailRS response = new OTA_GroundAvailRS();
			try
			{
				TaxiUtility utility = new TaxiUtility(logger);

				#region [Taxi 5.0] Validate required fields
				TaxiGetEstimationParameter tgep = null;
				if (!utility.ValidateGetEstimationRequest(tokenVTOD, request, out tgep))
				{
					string error = string.Format("This GetEstimation request is invalid.");
					throw new ValidationException(error);
				}
				#endregion

				response = utility.GetRestimationResponse(tgep);

				#region Write Taxi Log
				utility.WriteTaxiLog(null, TaxiServiceAPIConst.None, TaxiServiceMethodType.Estimation, null, null, null, null, null);
				#endregion

				response.Success = new Success();
			}
			catch (Exception ex)
			{				
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw;				
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return response;
		}

        public OTA_GroundBookRS Book(TokenRS tokenVTOD, OTA_GroundBookRQ request, out List<Tuple<string, int, long>> disptach_Rez_VTOD, int fleetTripCode, taxi_fleet fleet)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			Stopwatch swEachPart = new Stopwatch();
			OTA_GroundBookRS response = null;
			disptach_Rez_VTOD = new List<Tuple<string, int, long>>();
			DateTime requestDateTime = DateTime.Now;
			DateTime responseDateTime;

			#region validation
			TaxiUtility utility = new TaxiUtility(logger);

			#region [Taxi 1.0] Validate required fields
			TaxiBookingParameter tbp = new TaxiBookingParameter();
			if (!utility.ValidateBookingRequest(tokenVTOD, request, out tbp,fleet))
			{
				throw VtodException.CreateException(ExceptionType.Taxi, 2007);
			}
			#endregion


			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion
            tbp.FleetTripCode = fleetTripCode;
			DateTime? requestedLocalTime = null;
            requestedLocalTime = tbp.PickupDateTime;
			try
			{
				using (var client = new CCSiService.TripSoapClient())
				{
					#region Trip_CreateExtended
					//string CCSiSource = ""; //Flee based, from DB
					//string CCSiFleetId = ""; //Fleet based from DB

					//replace the street number in pickup address
					if (tbp.PickupAddress.StreetNo.ToArray().Where(x => x.Equals('–')).Any())
					{
						logger.InfoFormat("The orginial pickup street No. is {0}", tbp.PickupAddress.StreetNo);
						string[] sep = { "–" };
						string[] streetNumbers = tbp.PickupAddress.StreetNo.Split(sep, StringSplitOptions.RemoveEmptyEntries);
						tbp.PickupAddress.StreetNo = streetNumbers[0];
						tbp.Trip.taxi_trip.RemarkForPickup = string.Format("{0} Customer on {1} block of {2}", tbp.Trip.taxi_trip.RemarkForPickup, tbp.PickupAddress.StreetNo, tbp.PickupAddress.Street);
						logger.InfoFormat("Remove range street number in pickup address. Pickup street No. is {0}", tbp.PickupAddress.StreetNo);
						logger.InfoFormat("Pickup Comment: {0}", tbp.Trip.taxi_trip.RemarkForPickup);
						if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_CCSi, "Book_ReplacePickupStreetNumberRange");
					}

					//replace the street number in drop address
					if (tbp.DropOffAddress != null && !string.IsNullOrWhiteSpace(tbp.DropOffAddress.StreetNo) && tbp.DropOffAddress.StreetNo.ToArray().Where(x => x.Equals('–')).Any())
					{
						logger.InfoFormat("The orginial dropoff street No. is {0}", tbp.DropOffAddress.StreetNo);
						string[] sep = { "–" };
						string[] streetNumbers = tbp.DropOffAddress.StreetNo.Split(sep, StringSplitOptions.RemoveEmptyEntries);
						tbp.DropOffAddress.StreetNo = streetNumbers[0];
						logger.InfoFormat("Remove range street number in dropoff address. Dropoff street No. is {0}", tbp.DropOffAddress.StreetNo);
						if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_CCSi, "Book_ReplaceDropoffStreetNumberRange");
					}



					//remove the phone number in comment
					if (Regex.IsMatch(tbp.Trip.taxi_trip.RemarkForPickup, @"CUSTOMER PHONE: [\d]+", RegexOptions.IgnoreCase))
					{
						tbp.Trip.taxi_trip.RemarkForPickup = Regex.Replace(tbp.Trip.taxi_trip.RemarkForPickup, @"CUSTOMER PHONE: [\d]+", "", RegexOptions.IgnoreCase);
						logger.Info("Remove cusomter phone in comment.");
						logger.InfoFormat("Pickup Comment: {0}", tbp.Trip.taxi_trip.RemarkForPickup);
						if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_CCSi, "Book_ReplacePhoneNumberInComment");
					}

					//replace ztrip to z trip
					if (Regex.IsMatch(tbp.Trip.taxi_trip.RemarkForPickup, @"zTrip", RegexOptions.IgnoreCase))
					{
						tbp.Trip.taxi_trip.RemarkForPickup = Regex.Replace(tbp.Trip.taxi_trip.RemarkForPickup, @"zTrip", "Z Trip", RegexOptions.IgnoreCase);
						logger.Info("Replace zTrip with Z Trip");
						logger.InfoFormat("Pickup Comment: {0}", tbp.Trip.taxi_trip.RemarkForPickup);
						if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_CCSi, "Book_ReplaceZtripWording");
					}


					//replace pickup zipCode with 5 digits zipcode only
					if (Regex.IsMatch(tbp.PickupAddress.ZipCode, @"([\d]+)\-[\d]+"))
					{
						logger.InfoFormat("Original pickup zipCode: {0}", tbp.PickupAddress.ZipCode);
						tbp.PickupAddress.ZipCode = Regex.Match(tbp.PickupAddress.ZipCode, @"([\d]+)\-[\d]+", RegexOptions.IgnoreCase).Groups[1].Value;
						logger.InfoFormat("Replace: {0}", tbp.PickupAddress.ZipCode);
						if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_CCSi, "Book_ReplacePickupZipCode");
					}

					//replace dropoff zipCode with 5 digits zipcode only
					if (tbp.DropOffAddress != null && Regex.IsMatch(tbp.DropOffAddress.ZipCode, @"([\d]+)\-[\d]+"))
					{
						logger.InfoFormat("Original dropoff zipCode: {0}", tbp.DropOffAddress.ZipCode);
						tbp.DropOffAddress.ZipCode = Regex.Match(tbp.DropOffAddress.ZipCode, @"([\d]+)\-[\d]+", RegexOptions.IgnoreCase).Groups[1].Value;
						logger.InfoFormat("Replace: {0}", tbp.DropOffAddress.ZipCode);
						if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_CCSi, "Book_ReplaceDropoffZipCode");
					}




					swEachPart.Restart();
					//var serviceResult = client.Trip_CreateExtended(
					var serviceResult = client.Trip_CreateExtended_zTripTaxiSedan(
						tbp.Fleet.CCSiSource, //"9970", //Source 
						tbp.Fleet.CCSiFleetId, //"1",//Fleet
						tbp.Trip.taxi_trip.AccountNumber,//chargenbr
						tbp.PhoneNumber,//!string.IsNullOrWhiteSpace(trip.ClientPhone1) ? trip.ClientPhone1.CleanPhone10Digit() : trip.ClientPhone2.CleanPhone10Digit(), //PkupPhNbr 
						"", //PkupPhExt 
						"", // PkupLocation 
						Convert.ToInt32(Regex.Match(tbp.PickupAddress.StreetNo, "([0-9]+)").Groups[1].Value),//trip.PickupAddress.StreetNo.ToInt32(), //PkupStrNbr 
						tbp.PickupAddress.Street,//trip.PickupAddress.Street, //PkupStrName 
						tbp.PickupAddress.ApartmentNo,//trip.PickupAddress.ApartmentNo, //PkupApt 
						tbp.PickupAddress.City,//trip.PickupAddress.City, //PkupCity 
						tbp.PickupAddress.State,//trip.PickupAddress.State, //PkupState 
						tbp.PickupAddress.ZipCode,//trip.PickupAddress.ZipCode, //PkupZip 
						tbp.PickupDateTime.ToString("yyyy-MM-dd HH:mm:ss"),//System.Convert.ToDateTime(trip.PickupTime).ToString("yyyy-MM-dd HH:mm:ss"), //PkupDateTime YYYY-MM-DD HH:MM:SS (in 24 hour format) 
						Convert.ToDouble(tbp.PickupAddress.Geolocation.Latitude),//trip.PickupAddress.Latitude.Value.ToDouble(), //PkupLat 
						Convert.ToDouble(tbp.PickupAddress.Geolocation.Longitude), //trip.PickupAddress.Longitude.Value.ToDouble(), //PkupLon 
						tbp.Trip.taxi_trip.RemarkForPickup,//trip.PU_Comments, //PkupComment 
						tbp.FirstName,//!string.IsNullOrWhiteSpace(trip.ClientFullName) ? trip.ClientFullName.Split(' ').FirstOrDefault() : string.Empty, //PassFirstName 
						tbp.LastName,//!string.IsNullOrWhiteSpace(trip.ClientFullName) ? trip.ClientFullName.Split(' ').LastOrDefault() : string.Empty,  //PassLastName 
						"",//DestPhNbr
						"", //DestPhExt 
						"", //DestLocation 
						(tbp.DropOffAddress != null) ? Convert.ToInt32(Regex.Match(tbp.DropOffAddress.StreetNo, "([0-9]+)").Groups[1].Value) : 0,//trip.DropoffAddress.StreetNo.ToInt32(), //DestStrNbr 
						(tbp.DropOffAddress != null) ? tbp.DropOffAddress.Street : "", //DestStrName 
						(tbp.DropOffAddress != null) ? tbp.DropOffAddress.ApartmentNo : "", //DestApt 
						(tbp.DropOffAddress != null) ? tbp.DropOffAddress.City : "", //DestCity 
						(tbp.DropOffAddress != null) ? tbp.DropOffAddress.State : "", //DestState 
						(tbp.DropOffAddress != null) ? tbp.DropOffAddress.ZipCode : "", //DestZip 
						(tbp.DropOffAddress != null && tbp.DropOffDateTime.HasValue) ? tbp.DropOffDateTime.Value.ToString("yyyy-MM-dd HH:mm:ss") : string.Empty,//trip.DropoffTime.HasValue ? System.Convert.ToDateTime(trip.DropoffTime).ToString("yyyy-MM-dd HH:mm:ss") : string.Empty, //DestDateTime 
						(tbp.DropOffAddress != null && tbp.DropOffAddress.Geolocation != null) ? Convert.ToDouble(tbp.DropOffAddress.Geolocation.Latitude) : 0,//trip.DropoffAddress.Latitude.Value.ToDouble(), //DestLat 
						(tbp.DropOffAddress != null && tbp.DropOffAddress.Geolocation != null) ? Convert.ToDouble(tbp.DropOffAddress.Geolocation.Longitude) : 0,//trip.DropoffAddress.Longitude.Value.ToDouble(), //DestLon 
						"P", //TripType
						0.0, //CallRate
						"", //VehicleAttributes 
						"", //RequestedBy
						"", //Custome_Field1
						"",//trip.FareAmount.HasValue ? System.Math.Round(trip.FareAmount.Value, 2).ToString() : "0.00", //Custoem_Field2
						"", //CustomField3
						"", //CustomField4
						"", //Custom_Field5
						0.0//trip.FareAmount.HasValue ? System.Math.Round(trip.FareAmount.Value, 2).ToDouble() : 0//Co_Pay
					);
					swEachPart.Stop();
					logger.DebugFormat("Call CCSi API to book a trip. Duration:{0} ms.", sw.ElapsedMilliseconds);

					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_CCSi, "Book_SendingBookingRequest");
					#endregion

					if (serviceResult.result)
					{
						responseDateTime = DateTime.Now;
						string confirmationNumber = serviceResult.confNbr.ToString();
						if (!string.IsNullOrWhiteSpace(confirmationNumber))
						{
                            tbp.Trip = utility.UpdateTaxiTrip(tbp.Trip, confirmationNumber, tbp.FleetTripCode);

							if (tbp.EnableResponseAndLog)
							{
								GroundReservation reservation = tbp.request.GroundReservations.FirstOrDefault();
								response = new OTA_GroundBookRS();
								response.EchoToken = tbp.request.EchoToken;
								response.Target = tbp.request.Target;
								response.Version = tbp.request.Version;
								response.PrimaryLangID = tbp.request.PrimaryLangID;

								response.Success = new Success();

								//Reservation
								response.Reservations = new List<Reservation>();
								Reservation r = new Reservation();
								r.Confirmation = new Confirmation();
								//r.Confirmation.Type = "???";
								r.Confirmation.ID = tbp.Trip.Id.ToString();
								r.Confirmation.Type = ConfirmationType.Confirmation.ToString();

								r.Passenger = reservation.Passenger;
								r.Service = reservation.Service;
								r.Confirmation.TPA_Extensions = new TPA_Extensions();
								//r.Confirmation.TPA_Extensions.DispatchConfirmation = new Confirmation();
								//r.Confirmation.TPA_Extensions.DispatchConfirmation.ID = tbp.Trip.DispatchTripId;
								r.Confirmation.TPA_Extensions.Confirmations = new List<Confirmation>();
								r.Confirmation.TPA_Extensions.Confirmations.Add(new Confirmation { ID = tbp.Trip.taxi_trip.DispatchTripId, Type = ConfirmationType.DispatchConfirmation.ToString() });// = tbp.Trip.DispatchTripId;
								//r.Confirmation.TPA_Extensions.Statuses = new Statuses();
								//r.Confirmation.TPA_Extensions.Statuses.Status = new List<Common.DTO.OTA.Status>();

                                #region FleetInfo
                                taxi_fleet fleetObj = null;
                                using (var db = new DataAccess.VTOD.VTODEntities())
                                {
                                    Int64 fleetId = 0;
                                    taxi_trip tripObj = db.taxi_trip.Where(p => p.Id == tbp.Trip.Id).FirstOrDefault();
                                    if (tripObj != null)
                                    {
                                        fleetId = tripObj.FleetId;
                                    }
                                    if (fleetId > 0)
                                    {
                                        fleetObj = db.taxi_fleet.Where(p => p.Id == fleetId).FirstOrDefault();
                                    }

                                }
                                if (fleetObj != null)
                                {
                                    r.TPA_Extensions = new TPA_Extensions();
                                    r.TPA_Extensions.Contacts = new Contacts();
                                    r.TPA_Extensions.Contacts.Items = new List<Contact>();
                                    var contactDetails = new Contact { Type = Common.DTO.Enum.ContactType.Dispatch.ToString() };
                                    if (!string.IsNullOrWhiteSpace(fleetObj.Name))
                                    {
                                        contactDetails.Name = fleetObj.Name;
                                    }
                                    if (!string.IsNullOrWhiteSpace(fleetObj.PhoneNumber))
                                    {
                                        contactDetails.Telephone = new Telephone { PhoneNumber = fleetObj.PhoneNumber };
                                    }
                                    r.TPA_Extensions.Contacts.Items.Add(contactDetails);
                                }
                                #endregion
								response.Reservations.Add(r);

								#region Put status into taxi_trip_status
								//utility.SaveTripStatus(tbp.Trip.Id, TaxiTripStatusType.Booked);



								utility.SaveTripStatus(tbp.Trip.Id, TaxiTripStatusType.Booked, requestedLocalTime, null, null, null, null, null, null, null, null, string.Empty, string.Empty, null);
								#endregion

								//#region Set final status
								//utility.UpdateFinalStatus(tbp.Trip.Id, TaxiTripStatusType.Booked);
								//#endregion

							}
							else
							{
								logger.Info("Disable response and log for booking");
							}
						}
						else
						{
							string error = string.Format("Cannot get DispatchTripId. Or dispatchTripId is white space. message={0} ", serviceResult.errorMsg);
							logger.Error(error);
							utility.MarkThisTripAsError(tbp.Trip.Id, serviceResult.errorMsg);
							throw new TaxiException(error);
						}

						if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_CCSi, "Book_CreateResponseObject");
					}
					else
					{
						logger.Error(serviceResult.errorMsg);
						utility.MarkThisTripAsError(tbp.Trip.Id, serviceResult.errorMsg);
						throw new TaxiException(serviceResult.errorMsg);
					}

					#region Write Taxi Log
					if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
					{
						string requestMessage = "";
						requestMessage = tbp.XmlSerialize().ToString();

						string responseMessage = "";
						responseMessage = serviceResult.XmlSerialize().ToString();

						utility.WriteTaxiLog(tbp.Trip.Id, TaxiServiceAPIConst.CCSi, TaxiServiceMethodType.Book, null, requestMessage, responseMessage, requestDateTime, responseDateTime);
					}
					#endregion
				}
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);

				//change the status of trip
				utility.UpdateFinalStatus(tbp.Trip.Id, TaxiTripStatusType.Error, null, null, null, null, null);

				throw ex;
			}

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_CCSi, "Book");
			#endregion

			return response;
		}
        public OTA_GroundBookRS ModifyBook(TokenRS tokenVTOD, OTA_GroundBookRQ request, out List<Tuple<string, int, long>> disptach_Rez_VTOD, int fleetTripCode, taxi_fleet fleet,long serviceAPIID)
        {
            throw new NotImplementedException();
        }

        public OTA_GroundResRetrieveRS Status(TokenRS tokenVTOD, OTA_GroundResRetrieveRQ request,string type)
		{
			//CCis pushs status information through UDI.VTOD.CCSiWeb

			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			OTA_GroundResRetrieveRS response = null;

			TaxiUtility utility = new TaxiUtility(logger);

			#region [Taxi 2.0] Validate required fields
			TaxiStatusParameter tsp = null;
			if (!utility.ValidateStatusRequest(tokenVTOD, request, out tsp))
			{
				throw VtodException.CreateException(ExceptionType.Taxi, 3003);
			}
			#endregion

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion

			try
			{
				DateTime requestDateTime = DateTime.Now;
				DateTime responseDateTime;

				response = new OTA_GroundResRetrieveRS();
				response.Success = new Success();
				response.EchoToken = tsp.request.EchoToken;
				response.Target = tsp.request.Target;
				response.Version = tsp.request.Version;
				response.PrimaryLangID = tsp.request.PrimaryLangID;
				response.TPA_Extensions = new TPA_Extensions();
				response.TPA_Extensions.Driver = utility.ExtractDriverName(tsp.Trip.DriverName);
				response.TPA_Extensions.Vehicles = new Vehicles();
				response.TPA_Extensions.Vehicles.Items = new List<Vehicle>();
				var vehicle = new Vehicle();

				#region Vehicle
				if (tsp.lastTripStatus != null)
				{
					vehicle.MinutesAway = tsp.lastTripStatus.ETA.HasValue ? tsp.lastTripStatus.ETA.Value : 0;
					vehicle.MinutesAwayWithTraffic = tsp.lastTripStatus.ETAWithTraffic.HasValue ? tsp.lastTripStatus.ETAWithTraffic.Value : 0;

					if (vehicle.MinutesAwayWithTraffic < 3)
						vehicle.MinutesAwayWithTraffic = 3;

					if (tsp.lastTripStatus.VehicleLatitude.HasValue && tsp.lastTripStatus.VehicleLongitude.HasValue)
					{
						vehicle.Geolocation = new Geolocation();
						vehicle.Geolocation.Lat = tsp.lastTripStatus.VehicleLatitude.Value;
						vehicle.Geolocation.Long = tsp.lastTripStatus.VehicleLongitude.Value;

					}
					vehicle.ID = tsp.lastTripStatus.VehicleNumber;
				}

				#region ETA
				if (tsp.lastTripStatus.ETA.HasValue)
				{
					vehicle.MinutesAway = Convert.ToDecimal(tsp.lastTripStatus.ETA.Value);
				}
				#endregion
				response.TPA_Extensions.Vehicles.Items.Add(vehicle);

				#endregion

				#region Making the result
				//Convert status to front-end status
				//string frontEndStatus = utility.ConvertTripStatusMessageForFrontEndDevice(tsp.Fleet_User.UserId, tsp.lastTripStatus.Status, tsp.Fleet.FastMeterAction);
				#region Front End Status
				string frontEndStatus = utility.ConvertTripStatusMessageForFrontEndDevice(tsp.Fleet_User.UserId, tsp.lastTripStatus.Status);
				#endregion


				Status s = new Common.DTO.OTA.Status { Value = frontEndStatus, Fare = tsp.lastTripStatus.Fare.HasValue ? tsp.lastTripStatus.Fare.Value : 0 };
				response.TPA_Extensions.Statuses = new Statuses();
				response.TPA_Extensions.Statuses.Status = new List<Status>();
				response.TPA_Extensions.Statuses.Status.Add(s);
				response.TPA_Extensions.Contacts = new Contacts();
				response.TPA_Extensions.Contacts.Items = new List<Contact>();


				#region Adding Driver Info and DispatchInfo


				vtod_driver_info driverInfo = null;
                if (tsp.Fleet.DriverInfo == UDI.VTOD.Common.Const.DriveInfoSource.TCS)
                {
                    if (tsp.Trip.DriverInfoID.HasValue && tsp.Trip.DriverInfoID.Value > 0)
                    {
                        using (var db = new DataAccess.VTOD.VTODEntities())
                        {
                            driverInfo = db.vtod_driver_info.FirstOrDefault(t => t.Id == tsp.Trip.DriverInfoID.Value);
                        }
                    }
                    else
                    {

                        if (tsp.tokenVTOD != null && tsp.lastTripStatus.DriverId != null && !string.IsNullOrWhiteSpace(tsp.lastTripStatus.DriverId))
                        {
                            driverInfo = utility.GetDriverInfoByDriverId(tsp.tokenVTOD.Username, tsp.Trip, tsp.lastTripStatus.Status, tsp.lastTripStatus.DriverId);
                        }
                        if (driverInfo == null)
                        {
                            if (tsp.tokenVTOD != null && !string.IsNullOrWhiteSpace(vehicle.ID))
                            {
                                driverInfo = utility.GetDriverInfoByVehicleNumber(tsp.tokenVTOD.Username, tsp.lastTripStatus.Status, vehicle.ID, "", tsp.Trip);
                            }
                        }
                    }
                }
                else if (tsp.Fleet.DriverInfo == UDI.VTOD.Common.Const.DriveInfoSource.Local)
                {
                    if (tsp.Trip.DriverInfoID.HasValue && tsp.Trip.DriverInfoID.Value > 0)
                    {
                        if (tsp.Trip != null)
                        {
                            using (var db = new DataAccess.VTOD.VTODEntities())
                            {
                                driverInfo = db.vtod_driver_info.FirstOrDefault(t => t.Id == tsp.Trip.DriverInfoID.Value);
                            }
                        }
                    }
                    else
                    {
                        if (tsp.tokenVTOD != null && tsp.lastTripStatus.DriverId != null && !string.IsNullOrWhiteSpace(tsp.lastTripStatus.DriverId))
                        {
                            using (var db = new DataAccess.VTOD.VTODEntities())
                            {
                                driverInfo = db.vtod_driver_info.FirstOrDefault(t => t.DriverID == tsp.lastTripStatus.DriverId && t.FleetID==tsp.Fleet.Id && t.FleetType=="Taxi");
                                if (driverInfo != null)
                                db.SP_vtod_Update_TripDriverInfoID(tsp.Trip.Id, driverInfo.Id);
                            }
                           
                        }
                    }
                }

				Contact dispatchContact = null;
				Contact driverContact = null;

				//Disptch Info
				string dispatchPhone = tsp.Fleet.PhoneNumber;
				#region Work around for zTrip ver 2.2, please delete this part after release of zTrip ver 2.3
				//if (driverInfo != null && !string.IsNullOrEmpty(driverInfo.CellPhone))
				//{
				//	dispatchPhone = driverInfo.CellPhone;
				//}
				#endregion
				//todo add logic here for expedia and other companies logos
				if (tsp.User.name.ToLower() == "ztrip" || tsp.User.name.ToLower() == "udi")
				{
                    dispatchContact = new Contact { Type = Common.DTO.Enum.ContactType.Dispatch.ToString(), Name = tsp.Fleet.Alias, Telephone = new Telephone { PhoneNumber = dispatchPhone.CleanPhone10Digit() }, PhotoUrl = ConfigHelper.GetDriverPhotoVersionDefault() };

				}
				else
				{
					dispatchContact = new Contact { Type = Common.DTO.Enum.ContactType.Dispatch.ToString(), Name = tsp.Fleet.Alias, Telephone = new Telephone { PhoneNumber = dispatchPhone.CleanPhone10Digit() } };

				}
				response.TPA_Extensions.Contacts.Items.Add(dispatchContact);

				//Driver Info
				if (driverInfo != null && !string.IsNullOrEmpty(driverInfo.CellPhone))
				{
					driverContact = new Contact { Type = Common.DTO.Enum.ContactType.Driver.ToString(), Name = string.Format("{0} {1}", driverInfo.FirstName, driverInfo.LastName), Telephone = new Telephone { PhoneNumber = driverInfo.CellPhone.CleanPhone10Digit() }, PhotoUrl = driverInfo.PhotoUrl, ZtripDateOfDesignation = driverInfo.QualificationDate, ZtripDesignation = driverInfo.zTripQualified };
					response.TPA_Extensions.Contacts.Items.Add(driverContact);
				}
				#endregion


				var contact = new Contact { Type = Common.DTO.Enum.ContactType.Dispatch.ToString(), Name = tsp.Fleet.Alias, Telephone = new Telephone { PhoneNumber = tsp.Fleet.PhoneNumber } };
				response.TPA_Extensions.Contacts.Items.Add(contact);

				if (tsp.Trip != null && tsp.Trip.taxi_trip.PickupLatitude.HasValue && tsp.Trip.taxi_trip.PickupLatitude.Value != 0 && tsp.Trip.taxi_trip.PickupLongitude.HasValue && tsp.Trip.taxi_trip.PickupLongitude.Value != 0)
				{
					response.TPA_Extensions.PickupLocation = new Pickup_Dropoff_Stop();
					response.TPA_Extensions.PickupLocation.Address = new Common.DTO.OTA.Address();
					response.TPA_Extensions.PickupLocation.Address.Latitude = tsp.Trip.taxi_trip.PickupLatitude.Value.ToString();
					response.TPA_Extensions.PickupLocation.Address.Longitude = tsp.Trip.taxi_trip.PickupLongitude.Value.ToString();
				}

                if (tsp.Trip != null && tsp.Trip.taxi_trip.DropOffLatitude.HasValue && tsp.Trip.taxi_trip.DropOffLatitude.Value != 0 && tsp.Trip.taxi_trip.DropOffLongitude.HasValue && tsp.Trip.taxi_trip.DropOffLongitude.Value != 0)
                {
                    response.TPA_Extensions.DropoffLocation = new Pickup_Dropoff_Stop();
                    response.TPA_Extensions.DropoffLocation.Address = new Common.DTO.OTA.Address();
                    response.TPA_Extensions.DropoffLocation.Address.Latitude = tsp.Trip.taxi_trip.DropOffLatitude.Value.ToString();
                    response.TPA_Extensions.DropoffLocation.Address.Longitude = tsp.Trip.taxi_trip.DropOffLongitude.Value.ToString();
                }
				#endregion
				#region SharedETA Link
				if (request.TPA_Extensions != null && request.TPA_Extensions.RateQualifiers != null && request.TPA_Extensions.RateQualifiers.Any())
				{
					string uniqueID = null;
					string rateQualifier = null;
					string sharedLink = null;
					string appSharedLink = ConfigurationManager.AppSettings["SharedETALink"];
					string link = null;
					if (!String.IsNullOrWhiteSpace(type))
					{
						link = "?t=" + type.Substring(0, 1).ToLower();
					}
					if (request.TPA_Extensions.RateQualifiers != null && request.TPA_Extensions.RateQualifiers.Any())
					{
						rateQualifier = request.TPA_Extensions.RateQualifiers.FirstOrDefault().RateQualifierValue;
						link = link + "&RQ=" + rateQualifier;
					}
					if (request.Reference != null && request.Reference.Any())
					{
						uniqueID = request.Reference.FirstOrDefault().ID;
					}


					link = link + "&id=" + UDI.Utility.Helper.Cryptography.ConvertStringToHex(uniqueID);
					response.TPA_Extensions.SharedLink = appSharedLink + link; 
				}
                #endregion
				responseDateTime = DateTime.Now;

				#region Write Taxi Log
				if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
				{

					string requestMessage = "";
					requestMessage = tsp.XmlSerialize().ToString();

					string responseMessage = "";
					responseMessage = response.XmlSerialize().ToString();

					utility.WriteTaxiLog(tsp.Trip.Id, TaxiServiceAPIConst.CCSi, TaxiServiceMethodType.Status, null, requestMessage, responseMessage, requestDateTime, responseDateTime);
				}
				#endregion

			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw new TaxiException(ex.Message);
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);


			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_CCSi, "Status");
			#endregion

			return response;
		}

		public OTA_GroundCancelRS Cancel(TokenRS tokenVTOD, OTA_GroundCancelRQ request)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			OTA_GroundCancelRS response = null;
			DateTime requestDateTime = DateTime.Now;
			DateTime responseDateTime;

			TaxiUtility utility = new TaxiUtility(logger);

			#region [Taxi 3.0] Validate required fields
			TaxiCancelParameter tcp = null;
			if (!utility.ValidateCancelRequest(tokenVTOD, request, out tcp))
			{
				throw VtodException.CreateException(ExceptionType.Taxi, 5005);
			}
			#endregion

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion

			DateTime? requestedLocalTime = null;
			if (tcp.Fleet != null && tcp.Fleet.ServerUTCOffset != null)
			{
				requestedLocalTime = DateTime.UtcNow.FromUtcNullAble(tcp.Fleet.ServerUTCOffset);
			}

			try
			{
				TRIP_CANCEL_RESULT serviceResult = null;
				if (utility.IsAbleToCancelThisTrip(tcp.Trip.FinalStatus))
				{

					using (var client = new CCSiService.TripSoapClient())
					{
						serviceResult = client.Trip_Cancel(tcp.Fleet.CCSiSource, Convert.ToInt32(tcp.Trip.taxi_trip.DispatchTripId));

						if (serviceResult.result)
						{
							responseDateTime = DateTime.Now;

							response = new OTA_GroundCancelRS();
							response.Success = new Success();
							response.EchoToken = tcp.request.EchoToken;
							response.Target = tcp.request.Target;
							response.Version = tcp.request.Version;
							response.PrimaryLangID = tcp.request.PrimaryLangID;

							response.Reservation = new Reservation();
							response.Reservation.CancelConfirmation = new CancelConfirmation();
							response.Reservation.CancelConfirmation.UniqueID = new UniqueID { ID = tcp.Trip.Id.ToString() };

							#region Put status into taxi_trip_status
							//utility.SaveTripStatus(tcp.Trip.Id, TaxiTripStatusType.Canceled);
							//var time = DateTime.UtcNow.t
							//tcp.Fleet.ServerUTCOffset
							utility.SaveTripStatus(tcp.Trip.Id, TaxiTripStatusType.Canceled, requestedLocalTime, null, null, null, null, null, null, null, null, string.Empty, string.Empty, null);

							#region Set final status
							utility.UpdateFinalStatus(tcp.Trip.Id, TaxiTripStatusType.Canceled, null, null, null, null, null);
							#endregion

							#endregion
						}
						else
						{
							string error = "Fail to do cancel. ";
							logger.Error(error);
							logger.Error(serviceResult.result);
							//throw new TaxiException(string.Format("{0}.\r\n{1}", error, serviceResult.result));
							throw VtodException.CreateException(ExceptionType.Taxi, 5001); //throw new TaxiException(Messages.Taxi_CancelFailure);
						}
					}
				}
				else
				{
					throw VtodException.CreateException(ExceptionType.Taxi, 5001); //throw new TaxiException(Messages.Taxi_CancelFailure);
				}

				#region Write Taxi Log
				if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
				{

					string requestMessage = "";
					requestMessage = tcp.XmlSerialize().ToString();

					string responseMessage = "";
					responseMessage = serviceResult.XmlSerialize().ToString();

					utility.WriteTaxiLog(tcp.Trip.Id, TaxiServiceAPIConst.CCSi, TaxiServiceMethodType.Cancel, null, requestMessage, responseMessage, requestDateTime, responseDateTime);
				}
				#endregion
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				//throw new TaxiException(ex.Message);
				throw;// VtodException.CreateException(ExceptionType.Taxi, 5001); //throw new TaxiException(Messages.Taxi_CancelFailure);
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_CCSi, "Cancel");
			#endregion

			return response;
		}

		public OTA_GroundAvailRS GetVehicleInfo(TokenRS tokenVTOD, OTA_GroundAvailRQ request)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			var response = new OTA_GroundAvailRS();
			response.Success = new Success();
			response.TPA_Extensions = new TPA_Extensions();
			response.TPA_Extensions.Vehicles = new Vehicles();
			response.TPA_Extensions.Vehicles.Items = new List<Vehicle>();
			DateTime requestDateTime = DateTime.Now;
			DateTime responseDateTime;

			TaxiUtility utility = new TaxiUtility(logger);

			#region [Taxi 4.0] Validate required fields
			TaxiGetVehicleInfoParameter tgvip = null;
			if (!utility.ValidateGetVehicleInfoRequest(tokenVTOD, request, out tgvip))
			{
				string error = string.Format("This GetVehicleInfo request is invalid.");
				throw new ValidationException(error);
			}
			#endregion

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion

			DateTime? requestedLocalTime = null;
			if (tgvip.Fleet != null && tgvip.Fleet.ServerUTCOffset != null)
			{
				requestedLocalTime = DateTime.UtcNow.FromUtcNullAble(tgvip.Fleet.ServerUTCOffset);
			}

			try
			{
				AVAIL_VEH_RESULTS serviceResult = null;
				using (var client = new CCSiService.TripSoapClient())
				{

					//convert distance to Mile
					tgvip.Radius = Convert.ToDouble(Convert.ToDecimal(tgvip.Radius).ConvertDistance(UDI.Utility.DTO.Enum.DistanceUnit.Mile, UDI.Utility.DTO.Enum.DistanceUnit.Feet));


					//serviceResult = client.AvailableVehiclesForLatLong(tgvip.Fleet.CCSiFleetId, Convert.ToDouble(tgvip.Latitude), Convert.ToDouble(tgvip.Longtitude), Convert.ToInt32(tgvip.Radius));
					serviceResult = client.AvailableVehiclesForLatLong_zTripTaxiSedan(tgvip.Fleet.CCSiFleetId, Convert.ToDouble(tgvip.Latitude), Convert.ToDouble(tgvip.Longtitude), Convert.ToInt32(tgvip.Radius));
					responseDateTime = DateTime.Now;
					MapService ms = new MapService();
					if (serviceResult.vehicles.Any())
					{



						#region find nearest vehicle
						int shortestIndex = -1;
						double? shortestDistance = null;
						for (int index = 0; index < serviceResult.vehicles.Count(); index++)
						{
							double d = ms.GetDirectDistanceInMile(tgvip.Latitude, tgvip.Longtitude, serviceResult.vehicles[index].latitude, serviceResult.vehicles[index].longitude);
							serviceResult.vehicles[index].Distance = d;
							if (shortestDistance.HasValue)
							{
								if (shortestDistance.Value > d)
								{
									shortestDistance = d;
									shortestIndex = index;
								}
							}
							else
							{
								shortestDistance = d;
								shortestIndex = index;
							}
						}



						if (shortestIndex != -1)
						{
							//get ETA
							var rc = new UDI.SDS.RoutingController(tgvip.tokenVTOD, null);
							UDI.SDS.RoutingService.Point start = new UDI.SDS.RoutingService.Point { Latitude = tgvip.Latitude, Longitude = tgvip.Longtitude };
							UDI.SDS.RoutingService.Point end = new UDI.SDS.RoutingService.Point { Longitude = serviceResult.vehicles[shortestIndex].longitude, Latitude = serviceResult.vehicles[shortestIndex].latitude };
							var result = rc.GetRouteMetrics(start, end);
							response.TPA_Extensions.Vehicles.NearestVehicleETA = result.TotalMinutes.ToString();
						}

						response.TPA_Extensions.Vehicles.NumberOfVehicles = serviceResult.vehicles.Count().ToString();
						#endregion

						#region put all vehicle in the response
						foreach (var item in serviceResult.vehicles.OrderBy(s => s.Distance))
						{
							var vehicle = new Vehicle();
							vehicle.Geolocation = new Geolocation();
							vehicle.Geolocation.Long = decimal.Parse(item.longitude.ToString());
							vehicle.Geolocation.Lat = decimal.Parse(item.latitude.ToString());
							//vehicle.Geolocation.PriorLong = decimal.Parse(item.PriorLongitude.ToString());
							//vehicle.Geolocation.PriorLat = decimal.Parse(item.PriorLatitude.ToString());

							vehicle.Course = item.direction;
							vehicle.Distance = item.Distance.ToDecimal();
							//vehicle.Distance = item.Distance;
							//vehicle.DriverID = item.DriverID;
							//vehicle.DriverName = item.DriverName;
							//vehicle.FleetID = tgvip.Fleet.Id.ToString();
							vehicle.ID = item.vehicleNbr.ToString();
							vehicle.VehicleStatus = item.status;
							//vehicle.VehicleType = item.VehicleType;
							//vehicle.Zone = item.Zone;
							//vehicle.MinutesAway = Convert.ToDecimal(item.ETA.ToString());
							response.TPA_Extensions.Vehicles.Items.Add(vehicle);
						}
						#endregion
					}
					else
					{
						// no vehicles found
						logger.Info("Found no vehicles");
						response.TPA_Extensions.Vehicles.NumberOfVehicles = serviceResult.vehicles.Count().ToString();
					}
				}

				#region Write Taxi Log
				if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
				{
					string requestMessage = "";
					requestMessage = tgvip.XmlSerialize().ToString();

					string responseMessage = "";
					responseMessage = serviceResult.XmlSerialize().ToString();

					utility.WriteTaxiLog(null, TaxiServiceAPIConst.CCSi, TaxiServiceMethodType.GetVehicleInfo, null, requestMessage, responseMessage, requestDateTime, responseDateTime);
				}
				#endregion
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw new TaxiException(ex.Message);
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_CCSi, "GetVehicleInfo");
			#endregion

			return response;
		}

        public OTA_GroundGetFinalRouteRS GetFinalRoute(TokenRS tokenVTOD, OTA_GroundGetFinalRouteRQ request)
        {
            throw new NotImplementedException();
        }
        public bool NotifyDriver(TokenRS token, UtilityNotifyDriverRQ input)
		{
			throw new NotImplementedException();
		}
        public bool NotifyPickupDriver(TokenRS token, UtilityNotifyDriverRQ input)
        {
            throw new NotImplementedException();
        }
        public bool NotifyDispatchPendingDriver(TokenRS token, UtilityNotifyDriverRQ input)
        {
            throw new NotImplementedException();
        }
		public Common.DTO.OTA.Fleet GetAvailableFleets(TokenRS token, UtilityAvailableFleetsRQ input)
		{
			//result.FleetTypes.Items.Add(new Common.DTO.OTA.Fleet { Type = FleetType.Taxi.ToString(), Now = true, Later = true });
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			Common.DTO.OTA.Fleet result = null;

			try
			{
				TaxiUtility utility = new TaxiUtility(logger);
				bool pickMeUpNow = true;
				bool pickMeUpLater = true;
				if (utility.ValidateGetAvailableFleets(token, input, out pickMeUpNow, out pickMeUpLater))
				{
					result = new Common.DTO.OTA.Fleet { Type = FleetType.Taxi.ToString(), Now = pickMeUpNow.ToString(), Later = pickMeUpLater.ToString() };
				}
			}
			catch (Exception ex)
			{

				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw;
				//if (ex.GetType().Equals(typeof(ValidationException)))
				//{
				//	throw new ValidationException(ex.Message);
				//}
				//else
				//{
				//	throw VtodException.CreateException(ExceptionType.Taxi, 1011);// new TaxiException(Messages.Taxi_Common_UnableToGetAvailableFleets);
				//}
			}


			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return result;
		}

        public  bool GetDisabilityInd(TokenRS tokenVTOD, taxi_fleet f,out int  condition,string authenticateEndpoint, string bookingEndpoint)
        {
            throw new NotImplementedException();
        }

        public UtilityCustomerInfoRS GetCustomerInfo(TokenRS token, UtilityCustomerInfoRQ input)
		{
			throw new NotImplementedException();
		}

		public UtilityGetLastTripRS GetLastTrip(TokenRS token, UtilityGetLastTripRQ input)
		{
			throw new NotImplementedException();
		}

		public UtilityGetPickupAddressRS GetPickupAddress(TokenRS token, UtilityGetPickupAddressRQ input)
		{
			throw new NotImplementedException();
		}

		public UtilityGetBlackoutPeriodRS GetBlackoutPeriod(TokenRS token, UtilityGetBlackoutPeriodRQ input)
		{
			throw new NotImplementedException();
		}

		public UtilityGetNotificationRS GetStaleNotification(TokenRS token, UtilityGetNotificationRQ input)
		{
			throw new NotImplementedException();
		}

		public UtilityGetNotificationRS GetArrivalNotification(TokenRS token, UtilityGetNotificationRQ input)
		{
			throw new NotImplementedException();
		}

		public UtilityGetNotificationRS GetNightBeforeReminder(TokenRS token, UtilityGetNotificationRQ input)
		{
			throw new NotImplementedException();
		}
		#endregion

		#endregion

		#endregion

		#region Properties
		public TrackTime TrackTime { get; set; }
		#endregion
    		

		#region Wrong Implementation
		/*
        public UtilityCustomerInfoRS GetCustomerInfo(TokenRS token, UtilityCustomerInfoRQ input)
        {
            throw new NotImplementedException();
        }

        public UtilityGetLastTripRS GetLastTrip(TokenRS token, UtilityGetLastTripRQ input)
        {
            throw new NotImplementedException();
        }

        public UtilityGetPickupAddressRS GetPickupAddress(TokenRS token, UtilityGetPickupAddressRQ input)
        {
            throw new NotImplementedException();
        }


        public UtilityGetBlackoutPeriodRS GetBlackoutPeriod(TokenRS token, UtilityGetBlackoutPeriodRQ input)
        {
            throw new NotImplementedException();
        }





        public UtilityGetNotificationRS GetStaleNotification(TokenRS token, UtilityGetNotificationRQ input)
        {
            throw new NotImplementedException();
        }

        public UtilityGetNotificationRS GetArrivalNotification(TokenRS token, UtilityGetNotificationRQ input)
        {
            throw new NotImplementedException();
        }

        public UtilityGetNotificationRS GetNightBeforeReminder(TokenRS token, UtilityGetNotificationRQ input)
        {
            throw new NotImplementedException();
        }

        OTA_GroundBookRS ITaxiService.Book(TaxiBookingParameter tbp, int fleetTripCode)
        {
            throw new NotImplementedException();
        }

        OTA_GroundResRetrieveRS ITaxiService.Status(TaxiStatusParameter tsp)
        {
            throw new NotImplementedException();
        }

        OTA_GroundCancelRS ITaxiService.Cancel(TaxiCancelParameter tcp)
        {
            throw new NotImplementedException();
        }

        OTA_GroundAvailRS ITaxiService.GetVehicleInfo(TaxiGetVehicleInfoParameter tgvip)
        {
            throw new NotImplementedException();
        }

        bool ITaxiService.NotifyDriver(TaxiSendMessageToVehicleParameter tsmtvp)
        {
            throw new NotImplementedException();
        }

        Fleet ITaxiService.GetAvailableFleets(TokenRS token, UtilityAvailableFleetsRQ input, long? transactionID)
        {
            throw new NotImplementedException();
        }

        TrackTime ITaxiService.TrackTime
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        OTA_GroundBookRS ITaxiService.Book(TokenRS tokenVTOD, OTA_GroundBookRQ request, long? vtodTXId, out List<Tuple<string, int, long>> disptach_Rez_VTOD, int fleetTripCode, taxi_fleet fleet)
        {
            throw new NotImplementedException();
        }

        OTA_GroundResRetrieveRS ITaxiService.Status(TokenRS tokenVTOD, OTA_GroundResRetrieveRQ request, long? vtodTXId, string type)
        {
            throw new NotImplementedException();
        }

        OTA_GroundCancelRS ITaxiService.Cancel(TokenRS tokenVTOD, OTA_GroundCancelRQ request, long? vtodTXId)
        {
            throw new NotImplementedException();
        }

        OTA_GroundAvailRS ITaxiService.GetVehicleInfo(TokenRS tokenVTOD, OTA_GroundAvailRQ request, long? vtodTXId)
        {
            throw new NotImplementedException();
        }

        bool ITaxiService.NotifyDriver(TokenRS token, UtilityNotifyDriverRQ input, long? transactionID)
        {
            throw new NotImplementedException();
        }

        OTA_GroundAvailRS ITaxiService.GetEstimation(TokenRS tokenVTOD, OTA_GroundAvailRQ request, long? vtodTXId)
        {
            throw new NotImplementedException();
        }

        UtilityCustomerInfoRS ITaxiService.GetCustomerInfo(TokenRS token, UtilityCustomerInfoRQ input)
        {
            throw new NotImplementedException();
        }

        UtilityGetLastTripRS ITaxiService.GetLastTrip(TokenRS token, UtilityGetLastTripRQ input)
        {
            throw new NotImplementedException();
        }

        UtilityGetPickupAddressRS ITaxiService.GetPickupAddress(TokenRS token, UtilityGetPickupAddressRQ input)
        {
            throw new NotImplementedException();
        }

        UtilityGetBlackoutPeriodRS ITaxiService.GetBlackoutPeriod(TokenRS token, UtilityGetBlackoutPeriodRQ input)
        {
            throw new NotImplementedException();
        }

        UtilityGetNotificationRS ITaxiService.GetStaleNotification(TokenRS token, UtilityGetNotificationRQ input)
        {
            throw new NotImplementedException();
        }

        UtilityGetNotificationRS ITaxiService.GetArrivalNotification(TokenRS token, UtilityGetNotificationRQ input)
        {
            throw new NotImplementedException();
        }

        UtilityGetNotificationRS ITaxiService.GetNightBeforeReminder(TokenRS token, UtilityGetNotificationRQ input)
        {
            throw new NotImplementedException();
        }
		*/ 

		#endregion


	}

}
