﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.Taxi.Class
{
    public class FlatRateData
    {
        public const string Code = "FlatRate";
        public string PickupZone { set; get; }
        public string DropoffZone { set; get; }
        public string CurrencyMode { set; get; }
        public double EstimationTotalAmount { set; get; }
    }
}
