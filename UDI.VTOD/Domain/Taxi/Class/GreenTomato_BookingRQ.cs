﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.Taxi.Class
{
	public class GreenTomato_BookingRQ
	{
        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
		public string id { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string secondNumber { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string optLock { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string bookingDate { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string bookingDateType { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string phone { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string email { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string customerAccountNumber { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public List<GreenTomato_stopRecord> stopRecordList = new List<GreenTomato_stopRecord>();

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string additionalInstructions { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public List<GreenTomato_passengerRecord> passengerRecordList = new List<GreenTomato_passengerRecord>();

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public List<GreenTomato_referenceRecord> referenceRecordList = new List<GreenTomato_referenceRecord>();

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public List<GreenTomato_extraRecord> extrasRecordList { set; get; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string bookerId { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string serviceId { get; set; }
	}
}
