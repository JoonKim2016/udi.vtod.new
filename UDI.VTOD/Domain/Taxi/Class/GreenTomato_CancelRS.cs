﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.Taxi.Class
{
    public class GreenTomato_CancelRS
	{
        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
		public string success { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public List<GreenTomato_error> errors { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public List<string> warnings { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public List<string> rows { get; set; }
	}
}
