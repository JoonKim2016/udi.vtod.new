﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace UDI.VTOD.Domain.Taxi.Class
{
    public class GreenTomato_GetVehicleInfoRQ
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string startIndex { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string count { get; set; }

    }
}
