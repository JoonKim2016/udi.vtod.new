﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.Taxi.Class
{
    public class GreenTomato_SendMessageToDriverRQ
	{
        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
		public string driverid { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string title { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string message { get; set; }

       
	}
}
