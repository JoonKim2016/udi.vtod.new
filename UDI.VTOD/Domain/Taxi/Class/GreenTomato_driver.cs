﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.Taxi.Class
{
	public class GreenTomato_driver
	{
        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
		public string id { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Include)]
		public string fullName { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Include)]
		public string phone { get; set; }
	}
}
