﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.Taxi.Class
{
	public class GreenTomato_vehicle

	{
        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
		public string id { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Include)]
		public string regNumber { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Include)]
		public string makeAndModel { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string type { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string status { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string driverId { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string driverName { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string currentLocationLatitude { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string currentLocationLongitude { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string currentLocationName { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string currentLocationTime { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string availableLatitude { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string availableLongitude { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string availableAddress { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string bookingId { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string bookingStartTime { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string bookingStatus { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string driverPhone { get; set; }
	}
}
