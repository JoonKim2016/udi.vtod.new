﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.DataAccess.VTOD;

namespace UDI.VTOD.Domain.Taxi.Class
{
    public class TaxiCancelParameter
    {
		public TaxiCancelParameter()
		{
			this.EnableResponseAndLog = true;
            //this.Trip.taxi_trip = new taxi_trip();
		}

        public TokenRS tokenVTOD { set; get; }
        public OTA_GroundCancelRQ request { set; get; }

        public taxi_fleet Fleet { set; get; }

		public long serviceAPIID { set; get; }

        //public taxi_customer Customer { get; set; }
        //public string FirstName { set; get; }
        //public string LastName { set; get; }
        //public string PhoneNumber { set; get; }



        public taxi_fleet_user User { get; set; }
        //public taxi_trip Trip { get; set; }
        public vtod_trip Trip { get; set; }

		public bool EnableResponseAndLog { set; get; }
        public string AuthenticationServiceEndpointName { set; get; }
        public string BookingWebServiceEndpointName { set; get; }
        public string OsiWebServiceEndpointName { set; get; }
        public string AddressWebServiceEndpointName { set; get; }
    }
}
