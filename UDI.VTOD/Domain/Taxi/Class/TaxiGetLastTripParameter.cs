﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.DataAccess.VTOD;

namespace UDI.VTOD.Domain.Taxi.Class
{
    public class TaxiGetLastTripParameter
    {
        public TokenRS tokenVTOD { set; get; }
        public UtilityGetLastTripRQ request { set; get; }
        public taxi_fleet Fleet { set; get; }
        public long serviceAPIID { set; get; }


        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string PhoneNumber { set; get; }
        public string SearchType { set; get; }

        public int FromMinutesInPast { set; get; }

        public string AuthenticationServiceEndpointName { set; get; }
        public string BookingWebServiceEndpointName { set; get; }
        public string OsiWebServiceEndpointName { set; get; }
        public string AddressWebServiceEndpointName { set; get; }
    }
}
