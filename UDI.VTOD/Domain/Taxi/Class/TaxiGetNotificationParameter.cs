﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.DataAccess.VTOD;

namespace UDI.VTOD.Domain.Taxi.Class
{
    public class TaxiGetNotificationParameter
    {
        public TokenRS tokenVTOD { set; get; }
        public UtilityGetNotificationRQ request { set; get; }
        public taxi_fleet Fleet { set; get; }
        public long serviceAPIID { set; get; }
        public int Type { set; get; }

        public string AuthenticationServiceEndpointName { set; get; }
        public string BookingWebServiceEndpointName { set; get; }
        public string OsiWebServiceEndpointName { set; get; }
        public string AddressWebServiceEndpointName { set; get; }
    }
}

