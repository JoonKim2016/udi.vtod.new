﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.DataAccess.VTOD;

namespace UDI.VTOD.Domain.Taxi.Class
{
    public class TaxiGetVehicleInfoParameter
    {
		public TaxiGetVehicleInfoParameter()
		{
			this.EnableResponseAndLog = true;
		}

        public TokenRS tokenVTOD { set; get; }
        public OTA_GroundAvailRQ request { set; get; }

        public string PickupAddressType { set; get; }
        public Map.DTO.Address PickupAddress { set; get; }
        public string PickupLandmark { set; get; }

		public List<Vehicle> VehicleStausList = new List<Vehicle>();
        public double Longtitude { set; get; }
        public double Latitude { set; get; }
        public string PolygonType { set; get; } //Circle|Retengle

        //Circle
        public double Radius { set; get; } 
        

        public taxi_fleet Fleet { set; get; }

		public long serviceAPIID { set; get; }

		public long? extendedServiceAPIID { set; get; }
		
		public string APIInformation { set; get; }

		public bool EnableResponseAndLog { set; get; }

        public string VehicleStatus { set; get; }

        public bool DisabilityVehicleInd { set; get; }


        public string AuthenticationServiceEndpointName { set; get; }
        public string BookingWebServiceEndpointName { set; get; }
        public string OsiWebServiceEndpointName { set; get; }
        public string AddressWebServiceEndpointName { set; get; }
    }

    
}
