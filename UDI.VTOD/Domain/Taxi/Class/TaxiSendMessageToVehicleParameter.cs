﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.DataAccess.VTOD;

namespace UDI.VTOD.Domain.Taxi.Class
{
	public class TaxiSendMessageToVehicleParameter
	{
        public taxi_trip Trip { set; get; } //Required for windows service
		//public vtod_trip Trip { set; get; } //Required for windows service
		public string VehicleNunber { set; get; }
		public taxi_fleet Fleet { set; get; }//Required for windows service
		public string Message { set; get; }//Required for windows service
	}
}
