﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.Taxi.Const
{
    public class AddressType
    {
        //public const string Address ="Address";
        //public const string Landmark = "Landmark";

		//public const string Empty = "Empty";
		//public const string CountryZipCode = "CountryZipCode";
		//public const string CountryStateCity = "CountryStateCity";
		//public const string LocationName = "LocationName";
		//public const string AirportInfo = "AirportInfo";
		//public const string LatitudeAndLongtidtude = "LatitudeAndLongtidtude";
		public const string Empty = "Empty";
		//public const string CountryZipCode = "CountryZipCode";
		//public const string CountryStateCity = "CountryStateCity";
		//public const string LocationName = "LocationName";
		public const string Airport = "Airport";
		public const string Address = "Address";
		//public const string LatitudeAndLongtidtude = "LatitudeAndLongtidtude";
        public const string Landmark = "Landmark";

	}
}
