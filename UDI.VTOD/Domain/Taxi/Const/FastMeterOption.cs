﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.Taxi.Const
{
    public class FastMeterOption
    {
        public const int Off = 0;
        public const int ByMinFare = 1;
        //public const int ByDiffTimeBetweenMeterOnMeterOff = 2;

    }
}
