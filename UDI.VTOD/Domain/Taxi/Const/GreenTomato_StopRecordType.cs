﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.Taxi.Const
{
	public class GreenTomato_StopRecordType
	{
		public const string DROP = "DROP";
		public const string PICKUP = "PICKUP";
	}
}
