﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.Taxi.Const
{
    public class MTDataNameValue
    {
        public const int PaymentMethod_Split_ID = 5;
        public const int PaymentMethod_Cash_ID = 1;
        public const int PaymentMethod_Account_ID = 2;
        public const int PaymentMethod_CreditCard_ID = 3;


    }
}
