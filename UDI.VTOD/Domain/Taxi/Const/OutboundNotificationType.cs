﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.Taxi.Const
{
    public class OutboundNotificationType
    {
        public const int ArrivalNotification = 1;
        public const int NightBeforeReminder = 2;
        public const int StaleNotification = 3;

    }
}
