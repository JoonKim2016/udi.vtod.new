﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.Taxi.Const
{
    public class RestrictBookingOption
    {
        public static int On = 1;
        public static int Off = 0;
    }
}
