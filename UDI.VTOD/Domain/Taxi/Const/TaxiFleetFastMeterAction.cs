﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.Taxi.Const
{
    public class TaxiFleetFastMeterAction
    {
        public const int Cancel = 0;
        public const int ReDispatch = 1;
    }
}
