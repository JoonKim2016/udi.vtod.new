﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.Taxi.Const
{
    public class TaxiServiceAPIConst
    {
        public const long None = 0;
        public const long UDI33 = 1;
        public const long CCSi = 2;
		public const long SDS = 3;
		public const long MTData = 4;
        public const long Texas = 5;
        public const long Phoenix = 6;
    }
}
