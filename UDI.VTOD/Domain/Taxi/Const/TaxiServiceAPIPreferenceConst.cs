﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.Taxi.Const
{
	public class TaxiServiceAPIPreferenceConst
	{
		public const string Book = "Book";
		public const string Status = "Status";
		public const string Cancel = "Cancel";
        public const string GetVehicleInfo = "GetVehicleInfo";
		public const string GetEstimation = "GetEstimation";
		public const string GetAvailableFleets = "GetAvailableFleets";
        public const string NotifyDriver = "NotifyDriver";
        public const string CalculateVehicleMovement = "CalculateVehicleMovement";
        public const string GetCustomerInfo = "GetCustomerInfo";
        public const string GetPickupAddress = "GetPickupAddress";
        public const string GetLastTrip = "GetLastTrip";
        public const string GetBlackoutPeriod = "GetBlackoutPeriod";
        public const string GetStaleNotification = "GetStaleNotification";
        public const string GetArrivalNotification = "GetArrivalNotification";
        public const string GetNightBeforeReminder = "GetNightBeforeReminder";
        public const string GetFinalRoute = "GetFinalRoute";
    }
}
