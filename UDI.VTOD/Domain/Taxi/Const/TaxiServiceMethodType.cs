﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.Taxi.Const
{
    public class TaxiServiceMethodType
    {
        public const int Validation = 0;
        public const int Book = 1;
        public const int Status = 2;
        public const int Cancel = 3;
        public const int GetVehicleInfo = 4;
		public const int Estimation = 5;
		public const int SendMessageToVehicle = 6;
        public const int GetCustomerInfo = 7;
        public const int GetPickupAddress = 8;
        public const int GetLastTrip = 9;
        public const int GetBlackoutPeriod = 10;
        public const int ModifyBook = 11;
        public const int GetFinalRoute = 12;
    }
}
