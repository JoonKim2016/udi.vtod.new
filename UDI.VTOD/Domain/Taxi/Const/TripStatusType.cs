﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.Taxi.Const
{
	public class TaxiTripStatusType
	{
        //And we will use the status from UDI33
		public const string Booked = "Booked";
		public const string FastMeter = "FastMeter";
		public const string DispatchPending = "DispatchPending";
		public const string Canceled = "Canceled";
		public const string Accepted = "Accepted";
		public const string Assigned = "Assigned";
		public const string InTransit = "InTransit";
		public const string InService = "InService";
		public const string MeterON = "MeterOn";
		public const string NoShow = "NoShow";
		public const string Matched = "Matched";
		public const string UnMatched = "UnMatched";
		public const string Completed = "Completed";
		public const string Fare = "Fare";
		//public const string Charged = "Charged";
		//public const string UnCharged = "UnCharged";
		public const string Error = "Error";
		public const string MeterOff= "MeterOff";
        public const string Arrived = "Arrived";
        public const string Offered = "Offered";
        public const string PickUp = "PickUp";
		public const string DropOff = "DropOff";
		public const string TripNotFound = "TripNotFound";
        public const string ModifyBooked = "ModifyBooked";
        //CCSi
        //AcceptedByDriver ---> Accepted
        //OnSite-->Arrived
        //AssignedByProvider--> Assigned
        //maybe GPS exists in transit
        public const string Closed = "Closed";
        public const string Updated = "Updated";
        public const string GPS = "GPS";

        //Defined
        public const string Unknown = "Unknown";
        

        public static string ConvertCCSiStatus(int eventID, int? eventStatus, string eventDesc)
        {
            string result = TaxiTripStatusType.Unknown;

            switch (eventID)
            {
                case 13:
                    result = TaxiTripStatusType.GPS;
                    break;
                case 17:
                    if (eventStatus == 1)
                        result = TaxiTripStatusType.Accepted;
                    break;
                case 23:
                    result = TaxiTripStatusType.MeterON;
                    break;
                case 24:
                    result = TaxiTripStatusType.MeterOff;
                    break;
                case 25:
                    result = TaxiTripStatusType.PickUp;
                    break;
                case 26:
                    result = TaxiTripStatusType.Closed;
                    break;
                case 27:
                    result = TaxiTripStatusType.PickUp;
                    break;
                case 28:
                    result = TaxiTripStatusType.MeterOff;
                    break;
                case 29:
                    result = TaxiTripStatusType.PickUp;
                    break;
                case 30:
                    result = TaxiTripStatusType.MeterOff;
                    break;
                case 33:
                    result = TaxiTripStatusType.Arrived;
                    break;
                case 75:
                    result = TaxiTripStatusType.Updated;
                    break;
                case 101:
                    if (eventStatus == 32)
                        result = TaxiTripStatusType.Canceled;
                    break;
                case 152:
                    result = TaxiTripStatusType.Assigned;
                    break;
                case 170:
                    result = TaxiTripStatusType.Offered;
                    break;
                case 200:
                    result = TaxiTripStatusType.Accepted;
                    break;
                case 201:
                    result = TaxiTripStatusType.Arrived;
                    break;
                case 204:
                    result = TaxiTripStatusType.PickUp;
                    break;
                case 205:
                    result = TaxiTripStatusType.Closed;
                    break;
                default:
                    //if (!string.IsNullOrWhiteSpace(eventDesc))
                    //   result = eventDesc;
                    result = TaxiTripStatusType.Unknown;
                    break;
            }

            return result;
        }
		
	}
}
