﻿using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.Track;
using UDI.Utility.Helper;
using UDI.VTOD.Domain.Taxi.Const;
using UDI.VTOD.Domain.Taxi.Class;
using UDI.Utility.Serialization;
using System.Text.RegularExpressions;
using UDI.VTOD.Common.DTO;
using UDI.Notification.Service.Class;
using UDI.VTOD.Common.Helper;
using UDI.Map;
using System.Configuration;

namespace UDI.VTOD.Domain.Taxi.GreenTomato
{
	public class GreenTomatoTaxiService : ITaxiService
	{
		private ILog logger;
		private TaxiUtility utility;
		//private TokenRS tokenVTOD;


		public GreenTomatoTaxiService(TaxiUtility util, ILog log)
		{
			this.logger = log;
			this.utility = util;
		}

		#region Public method
		public OTA_GroundBookRS Book(TaxiBookingParameter tbp)
		{
			Stopwatch sw = new Stopwatch();
			sw.Start();
			OTA_GroundBookRS response = null;

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion

			DateTime? requestedLocalTime = null;
			if (tbp.Fleet != null && tbp.Fleet.ServerUTCOffset != null)
			{
				requestedLocalTime = DateTime.UtcNow.FromUtcNullAble(tbp.Fleet.ServerUTCOffset);
			}

			try
			{

				Dictionary<string, string> requestHeader = new Dictionary<string, string>();
				Dictionary<string, string> responseHeader = new Dictionary<string, string>();

				if (Login(out requestHeader))
				{
					//Get contact
					GreenTomato_contact contact = GetContact(tbp.FirstName, tbp.LastName, tbp.User.AccountNumber, tbp.PhoneNumber, requestHeader);
					if (contact != null)
					{
						//get service Id
						GreenTomato_ServiceListRS serviceRS = GetService(tbp.User.AccountNumber, requestHeader);
						if (serviceRS != null && serviceRS.success.ToLowerInvariant() == "true" && serviceRS.rows.Any() && !string.IsNullOrWhiteSpace(serviceRS.rows[0].id))
						{

							//get reference type
							GreenTomato_ReferenceTypeRS refTypeRS = GetReferenceType(tbp.User.AccountNumber, requestHeader);
							if (refTypeRS != null && refTypeRS.success.ToLowerInvariant() == "true" && refTypeRS.rows.Any())
							{

								#region Book
								#region Create JSON Message
								string postContent = BuildBookingRequest(tbp, contact.id, serviceRS.rows[0].id, refTypeRS.rows[0]);
								#endregion

								#region Send HttpRequest and set response into object
								string status = "";
								int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["GreenTomatoAPI_Https_Timeout"]);
								string bookingResult = WebRequestHelper.ProcessWebRequest(GetHttpURL(ConfigurationManager.AppSettings["GreenTomatoAPI_URL_Book"]), postContent, "application/json", "POST", string.Empty, string.Empty, timeout, requestHeader, out status, out  responseHeader);
								GreenTomato_BookingRS bookingResponse = bookingResult.JsonDeserialize<GreenTomato_BookingRS>();

								if (bookingResponse.success.ToLowerInvariant().Equals("false"))
								{
									//failure
									string errorMessage = "";
									string msg = "";
									if (bookingResponse.errors.Any())
									{
										errorMessage = bookingResponse.errors[0].message;
									}
									else
									{
										errorMessage = "No error messages from API";
									}
									msg = string.Format("Unable to book this trip. tripID={0}, errormessage={1}", tbp.Trip.Id, bookingResponse.errors[0].message);
									logger.Warn(msg);
									throw new GreenTomatoException(msg);
								}
								else
								{
									//sucess
									logger.Info("Booking sucessful");

									tbp.Trip = utility.UpdateTaxiTrip(tbp.Trip, bookingResponse.rows[0].id);
									if (!string.IsNullOrWhiteSpace(bookingResponse.rows[0].id))
									{
										if (tbp.EnableResponseAndLog)
										{
											GroundReservation reservation = tbp.request.GroundReservations.FirstOrDefault();
											response = new OTA_GroundBookRS();
											response.EchoToken = tbp.request.EchoToken;
											response.Target = tbp.request.Target;
											response.Version = tbp.request.Version;
											response.PrimaryLangID = tbp.request.PrimaryLangID;

											response.Success = new Success();

											//Reservation
											response.Reservations = new List<Reservation>();
											Reservation r = new Reservation();
											r.Confirmation = new Confirmation();
											//r.Confirmation.Type = "???";
											r.Confirmation.ID = tbp.Trip.Id.ToString();
											r.Confirmation.Type = ConfirmationType.Confirmation.ToString();

											r.Passenger = reservation.Passenger;
											r.Service = reservation.Service;
											r.Confirmation.TPA_Extensions = new TPA_Extensions();
											//r.Confirmation.TPA_Extensions.DispatchConfirmation = new Confirmation();
											//r.Confirmation.TPA_Extensions.DispatchConfirmation.ID = tbp.Trip.DispatchTripId;
											r.Confirmation.TPA_Extensions.Confirmations = new List<Confirmation>();
											r.Confirmation.TPA_Extensions.Confirmations.Add(new Confirmation { ID = tbp.Trip.DispatchTripId, Type = ConfirmationType.DispatchConfirmation.ToString() });// = tbp.Trip.DispatchTripId;
											//r.Confirmation.TPA_Extensions.Statuses = new Statuses();
											//r.Confirmation.TPA_Extensions.Statuses.Status = new List<Common.DTO.OTA.Status>();
											response.Reservations.Add(r);

											#region Put status into taxi_trip_status
											//utility.SaveTripStatus(tbp.Trip.Id, TaxiTripStatusType.Booked);



											utility.SaveTripStatus(tbp.Trip.Id, TaxiTripStatusType.Booked, requestedLocalTime, null, null, null, null, null, null, string.Empty, string.Empty,null);
											#endregion

											//#region Set final status
											//utility.UpdateFinalStatus(tbp.Trip.Id, TaxiTripStatusType.Booked);
											//#endregion

										}
										else
										{
											logger.Info("Disable response and log for booking");
										}
									}
									else
									{
										string error = string.Format("Cannot get DispatchTripId. Or dispatchTripId is white space.");
										logger.Error(error);
										utility.MarkThisTripAsError(tbp.Trip.Id, error);
										throw new UDI33Exception(error);
									}
								}
								#endregion
								#endregion

							}
							else
							{
								throw new GreenTomatoException(Messages.Taxi_UnableToGetReferenceTypeGreenTomato);
							}


						}
						else
						{
							throw new GreenTomatoException(Messages.Taxi_UnableToGetServiceGreenTomato);
						}
					}
					else
					{
						throw new GreenTomatoException(Messages.Taxi_UnableToGetOrCreateContactGreenTomato);
					}

				}
				else
				{
					//login failed
					logger.WarnFormat(Messages.Taxi_UnableToLoginGreenTomato);
					throw new GreenTomatoException(Messages.Taxi_UnableToLoginGreenTomato);
				}



				#region Write Taxi Log
				//if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]) /*&& tbp.EnableResponseAndLog*/)
				//{
				//    string requestMessage = udi33Request.XmlSerialize().ToString().Replace(" encoding=\"utf-16\"", "");
				//    string responseMessage = XMLObjectToString.StringFromMsgType<UDIXMLSchema.UDI>(returnMsg.UDIMsg);
				//    responseMessage = UDIUtils.IndentXMLString(responseMessage);
				//    utility.WriteTaxiLog(tbp.Trip.Id, TaxiServiceAPIConst.UDI33, TaxiServiceMethodType.Book, null, requestMessage, responseMessage, requestDateTime, responseDateTime);
				//}
				#endregion

			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);

				//change the status of trip
				utility.UpdateFinalStatus(tbp.Trip.Id, TaxiTripStatusType.Error, null, null, null);

				throw ex;
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_UDI33, "Book");
			#endregion

			return response;

		}

		public OTA_GroundResRetrieveRS Status(TaxiStatusParameter tsp)
		{
			Stopwatch sw = new Stopwatch();
			sw.Start();
			OTA_GroundResRetrieveRS response = null;

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion

			DateTime? requestedLocalTime = null;
			if (tsp.Fleet != null && tsp.Fleet.ServerUTCOffset != null)
			{
				requestedLocalTime = DateTime.UtcNow.FromUtcNullAble(tsp.Fleet.ServerUTCOffset);
			}

			try
			{
				Dictionary<string, string> requestHeader = new Dictionary<string, string>();
				Dictionary<string, string> responseHeader = new Dictionary<string, string>();

				if (Login(out requestHeader))
				{
					string HttpStatus = "";
					int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["GreenTomatoAPI_Https_Timeout"]);
					//string url = string.Format("https://80.87.31.28/ext-api/api/bookings/getById/{0}", tsp.Trip.DispatchTripId);
					string url = string.Format("{0}/{1}", GetHttpURL(ConfigurationManager.AppSettings["GreenTomatoAPI_URL_Status"]), tsp.Trip.DispatchTripId);
					string statusResult = WebRequestHelper.ProcessWebRequest(url, string.Empty, "application/json", "GET", string.Empty, string.Empty, timeout, requestHeader, out HttpStatus, out  responseHeader);
					GreenTomato_StatusRS statusResponse = statusResult.JsonDeserialize<GreenTomato_StatusRS>();


					if (statusResponse.success.ToLowerInvariant().Equals("true") && statusResponse.rows.Any())
					{
						response = new OTA_GroundResRetrieveRS();
						response.Success = new Success();
						response.EchoToken = tsp.request.EchoToken;
						response.Target = tsp.request.Target;
						response.Version = tsp.request.Version;
						response.PrimaryLangID = tsp.request.PrimaryLangID;

						response.TPA_Extensions = new TPA_Extensions();
						response.TPA_Extensions.Driver = (statusResponse.rows[0].driverRecord == null) ? null : utility.ExtractDriverName(statusResponse.rows[0].driverRecord.fullName);
						response.TPA_Extensions.Vehicles = new Vehicles();
						response.TPA_Extensions.Vehicles.Items = new List<Vehicle>();


						//calcute ETA
						int? minutesAway = null;

						//Vehicle
						var vehicle = new Vehicle();
						vehicle.Geolocation = new Geolocation();
						GreenTomato_GetVehicleInfoForStatusRS vInfo = null;
						if (!string.IsNullOrWhiteSpace(statusResponse.rows[0].vehicleRecord.id))
						{
							vehicle.ID = statusResponse.rows[0].vehicleRecord.id;
							vInfo = GetVehicleInfoForStautus(vehicle.ID, requestHeader);
							if (vInfo.success.ToLowerInvariant().Equals("true") && vInfo.rows.Any())
							{
								vehicle.Geolocation.Lat = vInfo.rows[0].currentLocationLatitude.ToDecimal();
								vehicle.Geolocation.Long = vInfo.rows[0].currentLocationLongitude.ToDecimal();
							}
						}
						#region Caculate ETA by ourself


						//Replace status
						string originalStatus = statusResponse.rows[0].status;
						string status = utility.ConvertTripStatusForGreenTomato(originalStatus);



						#region Others
						//fare
						decimal? totalFare = null;
						if (status.Trim().ToLower() == TaxiTripStatusType.Completed.Trim().ToLower() || status.Trim().ToLower() == TaxiTripStatusType.Fare.Trim().ToLower())
						{
							totalFare = statusResponse.rows[0].totalPrice.ToDecimal();
						}
						else
						{
							totalFare = tsp.Trip.TotalFareAmount;
						}

						//Convert status to front-end status
						string frontEndStatus = utility.ConvertTripStatusMessageForFrontEndDevice(tsp.Fleet_User.UserId, status,tsp.Fleet.FastMeterAction);
						//Status s = new Common.DTO.OTA.Status { Value = status, Fare = succ.TripInfo[0].FareAmount.ToDecimal() };
						Status s = new Common.DTO.OTA.Status { Value = frontEndStatus };
						if (totalFare.HasValue)
						{
							s.Fare = totalFare.Value;
						}
						response.TPA_Extensions.Statuses = new Statuses();
						response.TPA_Extensions.Statuses.Status = new List<Status>();
						response.TPA_Extensions.Statuses.Status.Add(s);
						response.TPA_Extensions.Contacts = new Contacts();
						response.TPA_Extensions.Contacts.Items = new List<Contact>();
						var contact = new Contact { Type = Common.DTO.Enum.ContactType.Dispatch.ToString(), Name = tsp.Fleet.Alias, Telephone = new Telephone { PhoneNumber = tsp.Fleet.PhoneNumber } };
						response.TPA_Extensions.Contacts.Items.Add(contact);

						if (tsp.Trip != null && tsp.Trip.PickupLatitude.HasValue && tsp.Trip.PickupLatitude.Value != 0 && tsp.Trip.PickupLongitude.HasValue && tsp.Trip.PickupLongitude.Value != 0)
						{
							response.TPA_Extensions.PickupLocation = new Pickup_Dropoff_Stop();
							response.TPA_Extensions.PickupLocation.Address = new Common.DTO.OTA.Address();
							response.TPA_Extensions.PickupLocation.Address.Latitude = tsp.Trip.PickupLatitude.Value.ToString();
							response.TPA_Extensions.PickupLocation.Address.Longitude = tsp.Trip.PickupLongitude.Value.ToString();
						}
						#endregion

						if ((vehicle.Geolocation.Lat != 0 && vehicle.Geolocation.Long != 0)
							&&
							(tsp.Trip.PickupLatitude.HasValue && tsp.Trip.PickupLongitude.HasValue)
						)
						{
							MapService ms = new MapService();
							double d = ms.GetDirectDistanceInMeter(tsp.Trip.PickupLatitude.Value, tsp.Trip.PickupLongitude.Value, Convert.ToDouble(vehicle.Geolocation.Lat), Convert.ToDouble(vehicle.Geolocation.Long));
							var rc = new UDI.SDS.RoutingController(tsp.tokenVTOD, null);
							UDI.SDS.RoutingService.Point start = new UDI.SDS.RoutingService.Point { Latitude = tsp.Trip.PickupLatitude.Value, Longitude = tsp.Trip.PickupLongitude.Value };
							UDI.SDS.RoutingService.Point end = new UDI.SDS.RoutingService.Point { Latitude = Convert.ToDouble(vehicle.Geolocation.Lat), Longitude = Convert.ToDouble(vehicle.Geolocation.Long) };
							var result = rc.GetRouteMetrics(start, end);
							vehicle.MinutesAway = result.TotalMinutes;

						}
						response.TPA_Extensions.Vehicles.Items.Add(vehicle);
						#endregion





						//Others

						//update taxi trip
						string driverName = (statusResponse.rows[0].driverRecord == null) ? string.Empty : statusResponse.rows[0].driverRecord.fullName;
						utility.UpdateTaxiTrip(tsp.Trip.Id, statusResponse.rows[0].serviceRecord.id, minutesAway, tsp.Trip.DispatchTripId, driverName);


						DateTime? statusTime = null;
						DateTime? tripStartTime = null;
						DateTime? tripEndTime = null;

						if (!statusTime.HasValue || statusTime.Value <= DateTime.MinValue)
						{
							if (tsp.Fleet != null && tsp.Fleet.ServerUTCOffset != null)
							{
								statusTime = DateTime.UtcNow.FromUtcNullAble(tsp.Fleet.ServerUTCOffset);
							}
						}

						if ((status.Trim().ToLower() == TaxiTripStatusType.InService.Trim().ToLower() || status.Trim().ToLower() == TaxiTripStatusType.MeterON.Trim().ToLower()) && (tsp.Trip.TripStartTime == null))
						{
							tripStartTime = statusTime;
						}
						else
						{
							tripStartTime = tsp.Trip.TripStartTime;
						}

						if ((status.Trim().ToLower() == TaxiTripStatusType.Completed.Trim().ToLower() || status.Trim().ToLower() == TaxiTripStatusType.MeterOff.Trim().ToLower()) && (tsp.Trip.TripEndTime == null))
						{
							tripEndTime = statusTime;
						}
						else
						{
							tripEndTime = tsp.Trip.TripEndTime;
						}

						if (status.ToUpperInvariant() == TaxiTripStatusType.Canceled.ToUpperInvariant())
						{
							utility.SaveTripStatus(tsp.Trip.Id, TaxiTripStatusType.Canceled, statusTime, null, null, null, null, null, null, string.Empty, originalStatus,null);
							utility.UpdateFinalStatus(tsp.Trip.Id, TaxiTripStatusType.Canceled, null, null, null);
                        }
                        else if (status.ToUpperInvariant() == TaxiTripStatusType.FastMeter.ToUpperInvariant())
                        {
                            if (tsp.Fleet.FastMeterAction == TaxiFleetFastMeterAction.ReDispatch)
                            {
                                utility.SaveTripStatus_ReDispatch(tsp.Trip.Id, statusTime, null, null, null, null, null, null, string.Empty, originalStatus,null );
                                utility.UpdateFinalStatus(tsp.Trip.Id, TaxiTripStatusType.Booked, null, null, null);
                            }
                            else if (tsp.Fleet.FastMeterAction == TaxiFleetFastMeterAction.Cancel)
                            {
                                utility.SaveTripStatus(tsp.Trip.Id, TaxiTripStatusType.FastMeter, statusTime, null, null, null, null, null, null, string.Empty, originalStatus,null);
                                utility.UpdateFinalStatus(tsp.Trip.Id, TaxiTripStatusType.Canceled, null, null, null);
                            }
                        }
						else
						{
							//Save trip status
							utility.SaveTripStatus(tsp.Trip.Id, status, statusTime, response.TPA_Extensions.Vehicles.Items.First().ID, response.TPA_Extensions.Vehicles.Items.First().Geolocation.Lat, response.TPA_Extensions.Vehicles.Items.First().Geolocation.Long, totalFare, response.TPA_Extensions.Vehicles.Items.First().MinutesAway.ToInt32(), driverName, string.Empty, originalStatus,null);

							//Save final status
							utility.UpdateFinalStatus(tsp.Trip.Id, status, totalFare, tripStartTime, tripEndTime);
						}
					}
					else
					{
						//unable to do status
						logger.WarnFormat(Messages.Taxi_StatusFailure);
						throw new GreenTomatoException(Messages.Taxi_StatusFailure);
					}

				}
				else
				{
					//login failed
					logger.WarnFormat(Messages.Taxi_UnableToLoginGreenTomato);
					throw new GreenTomatoException(Messages.Taxi_UnableToLoginGreenTomato);
				}




			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);

				throw ex;
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_GreenTomato, "Book");
			#endregion

			return response;
		}

		public OTA_GroundCancelRS Cancel(TaxiCancelParameter tcp)
		{
			OTA_GroundCancelRS response = null;
			Stopwatch sw = new Stopwatch();
			sw.Start();

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion

			DateTime? requestedLocalTime = null;
			if (tcp.Fleet != null && tcp.Fleet.ServerUTCOffset != null)
			{
				requestedLocalTime = DateTime.UtcNow.FromUtcNullAble(tcp.Fleet.ServerUTCOffset);
			}
			try
			{

				if (utility.IsAbleToCancelThisTrip(tcp.Trip.FinalStatus))
				{

					Dictionary<string, string> requestHeader = new Dictionary<string, string>();
					Dictionary<string, string> responseHeader = new Dictionary<string, string>();

					if (Login(out requestHeader))
					{
						GreenTomato_CancelRQ rq = new GreenTomato_CancelRQ();
						rq.bookingId = tcp.Trip.DispatchTripId;
						string rqString = rq.JsonSerialize();
						rqString = "bookingId=" + tcp.Trip.DispatchTripId;
						string status = "";
						int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["GreenTomatoAPI_Https_Timeout"]);
						//string result = WebRequestHelper.ProcessWebRequest("https://80.87.31.28/ext-api/api/bookings/cancel", rqString, "application/json", "POST", string.Empty, string.Empty, 3000, requestHeader, out status);
						//string result = WebRequestHelper.ProcessWebRequest(GetHttpURL(ConfigurationManager.AppSettings["GreenTomatoAPI_URL_Cancel"]), rqString, "application/json", "POST", string.Empty, string.Empty, 3000, requestHeader, out status);
						string result = WebRequestHelper.ProcessWebRequest(GetHttpURL(ConfigurationManager.AppSettings["GreenTomatoAPI_URL_Cancel"]), rqString, "application/x-www-form-urlencoded", "POST", string.Empty, string.Empty, timeout, requestHeader, out status);
						var cancelRS = result.JsonDeserialize<GreenTomato_CancelRS>();

						if (cancelRS != null && cancelRS.success.ToLowerInvariant().Equals("true"))
						{
							response = new OTA_GroundCancelRS();
							response.Success = new Success();
							response.EchoToken = tcp.request.EchoToken;
							response.Target = tcp.request.Target;
							response.Version = tcp.request.Version;
							response.PrimaryLangID = tcp.request.PrimaryLangID;

							response.Reservation = new Reservation();
							response.Reservation.CancelConfirmation = new CancelConfirmation();
							response.Reservation.CancelConfirmation.UniqueID = new UniqueID { ID = tcp.Trip.Id.ToString() };

							#region Set final status
							utility.UpdateFinalStatus(tcp.Trip.Id, TaxiTripStatusType.Canceled, null, null, null);
							#endregion

							#region Put status into taxi_trip_status
							utility.SaveTripStatus(tcp.Trip.Id, TaxiTripStatusType.Canceled, requestedLocalTime, null, null, null, null, null, null, string.Empty, string.Empty,null);
							#endregion

						}
						else
						{
							logger.WarnFormat("Unable to cancel this trip. Trip Id ={0}, dispatchId={1}", tcp.Trip.Id, tcp.Trip.DispatchTripId);
							throw new GreenTomatoException(Messages.Taxi_UnableToCancelGreenTomato);

						}


					}
					else
					{
						//login failed
						logger.WarnFormat(Messages.Taxi_UnableToLoginGreenTomato);
						throw new GreenTomatoException(Messages.Taxi_UnableToLoginGreenTomato);
					}
				}
				else {
					throw new UDI33Exception(Messages.Taxi_CancelFailure);
				}
			}
			catch (Exception ex)
			{

				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				response = null;

			}

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_GreenTomato, "Cancel");
			#endregion

			return response;
		}

		public OTA_GroundAvailRS GetVehicleInfo(TaxiGetVehicleInfoParameter tgvip)
		{

			OTA_GroundAvailRS response = new OTA_GroundAvailRS();
			response.Success = new Success();
			response.TPA_Extensions = new TPA_Extensions();
			response.TPA_Extensions.Vehicles = new Vehicles();
			response.TPA_Extensions.Vehicles.Items = new List<Vehicle>();

			Stopwatch sw = new Stopwatch();
			sw.Start();

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion

			DateTime? requestedLocalTime = null;
			if (tgvip.Fleet != null && tgvip.Fleet.ServerUTCOffset != null)
			{
				requestedLocalTime = DateTime.UtcNow.FromUtcNullAble(tgvip.Fleet.ServerUTCOffset);
			}
			try
			{
				Dictionary<string, string> requestHeader = new Dictionary<string, string>();
				Dictionary<string, string> responseHeader = new Dictionary<string, string>();

				if (Login(out requestHeader))
				{
					GreenTomato_GetContactRQ rq = new GreenTomato_GetContactRQ();
					//put long , lat and radius in it
					string postContent = @"{""startIndex"":0, ""count"":200}";
					string status = "";
					int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["GreenTomatoAPI_Https_Timeout"]);
					//string result = WebRequestHelper.ProcessWebRequest("https://80.87.31.28/ext-api/api/vehicle/detailsList", string.Empty, "application/json", "POST", string.Empty, string.Empty, 3000, requestHeader, out status);
					string result = WebRequestHelper.ProcessWebRequest(GetHttpURL(ConfigurationManager.AppSettings["GreenTomatoAPI_URL_GetVehicleInfo"]), postContent, "application/json", "POST", string.Empty, string.Empty, timeout, requestHeader, out status);
					//string result = WebRequestHelper.ProcessWebRequest(GetHttpURL(ConfigurationManager.AppSettings["GreenTomatoAPI_URL_GetVehicleInfo"]), string.Empty, "application/x-www-form-urlencoded", "POST", string.Empty, string.Empty, 3000, requestHeader, out status);
					var vehicleInfoResult = result.JsonDeserialize<GreenTomato_GetVehicleInfoForStatusRS>();

					if (vehicleInfoResult != null && vehicleInfoResult.success.ToLowerInvariant().Equals("true"))
					{
						if (vehicleInfoResult.rows.Any())
						{
							MapService ms = new MapService();

							//UDI33 cannot provide ETA for vehicles. We need to use supershuttle way to do it.
							//response.TPA_Extensions.Vehicles.NearestVehicleETA = Convert.ToDecimal(succ.Vehicles.OrderBy(x => x.ETA).Select(x => x.ETA).FirstOrDefault()).ToString();

							int shortestIndex = -1;
							double? shortestDistance = null;
							for (int index = 0; index < vehicleInfoResult.rows.Count(); index++)
							{
								double d = ms.GetDirectDistanceInMeter(tgvip.Latitude, tgvip.Longtitude, vehicleInfoResult.rows[index].currentLocationLatitude.ToDouble(), vehicleInfoResult.rows[index].currentLocationLongitude.ToDouble());
								if (shortestDistance.HasValue)
								{
									if (shortestDistance.Value > d)
									{
										shortestDistance = d;
										shortestIndex = index;
									}
								}
								else
								{
									shortestDistance = d;
									shortestIndex = index;
								}
							}

							if (shortestIndex != -1)
							{
								//get ETA
								var rc = new UDI.SDS.RoutingController(tgvip.tokenVTOD, null);
								UDI.SDS.RoutingService.Point start = new UDI.SDS.RoutingService.Point { Latitude = tgvip.Latitude, Longitude = tgvip.Longtitude };
								UDI.SDS.RoutingService.Point end = new UDI.SDS.RoutingService.Point { Latitude = vehicleInfoResult.rows[shortestIndex].currentLocationLatitude.ToDouble(), Longitude = vehicleInfoResult.rows[shortestIndex].currentLocationLongitude.ToDouble() };
								var routeResult = rc.GetRouteMetrics(start, end);
								response.TPA_Extensions.Vehicles.NearestVehicleETA = routeResult.TotalMinutes.ToString();
							}

							response.TPA_Extensions.Vehicles.NumberOfVehicles = vehicleInfoResult.rows.Count().ToString();


							foreach (var item in vehicleInfoResult.rows)
							{
								var vehicle = new Vehicle();
								vehicle.Geolocation = new Geolocation();
								vehicle.Geolocation.Long = decimal.Parse(item.currentLocationLongitude.ToString());
								vehicle.Geolocation.Lat = decimal.Parse(item.currentLocationLatitude.ToString());
								vehicle.Geolocation.PriorLong = decimal.Parse(item.availableLongitude.ToString());
								vehicle.Geolocation.PriorLat = decimal.Parse(item.availableLatitude.ToString());

								//need to find out the course
								//vehicle.Course = decimal.Parse(item.Course.ToString());
								//vehicle.Distance = item.Distance;
								vehicle.DriverID = item.driverId;
								vehicle.DriverName = item.driverName;
								vehicle.FleetID = tgvip.Fleet.Id.ToString();
								vehicle.ID = item.id;
								vehicle.VehicleStatus = item.status;
								vehicle.VehicleType = item.type;
								//vehicle.Zone = item.Zone;
								//vehicle.MinutesAway = Convert.ToDecimal(item.ETA.ToString());

								#region ETA
								var rc = new UDI.SDS.RoutingController(tgvip.tokenVTOD, null);
								UDI.SDS.RoutingService.Point start = new UDI.SDS.RoutingService.Point { Latitude = tgvip.Latitude, Longitude = tgvip.Longtitude };
								UDI.SDS.RoutingService.Point end = new UDI.SDS.RoutingService.Point { Latitude = item.currentLocationLatitude.ToDouble(), Longitude = item.currentLocationLongitude.ToDouble() };
								var routeResult = rc.GetRouteMetrics(start, end);
								vehicle.MinutesAway = routeResult.TotalMinutes;
								#endregion

								response.TPA_Extensions.Vehicles.Items.Add(vehicle);
							}
						}
						else
						{
							logger.Info("Found no vehicles");
							response.TPA_Extensions.Vehicles.NumberOfVehicles = "0";
						}
					}
					else
					{
						logger.Warn(Messages.Taxi_GetVehicleInfoFailure);
						throw new GreenTomatoException(Messages.Taxi_GetVehicleInfoFailure);
					}
				}
				else
				{
					//login failed
					logger.WarnFormat(Messages.Taxi_UnableToLoginGreenTomato);
					throw new GreenTomatoException(Messages.Taxi_UnableToLoginGreenTomato);
				}
			}
			catch (Exception ex)
			{

				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				response = null;

			}

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_GreenTomato, "GetVehicleInfo");
			#endregion

			return response;
		}

		public bool SendMessageToVehicle(TaxiSendMessageToVehicleParameter tsmtvp)
		{
			bool result = false;
			Stopwatch sw = new Stopwatch();
			sw.Start();

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion

			DateTime? requestedLocalTime = null;
			if (tsmtvp.Fleet != null && tsmtvp.Fleet.ServerUTCOffset != null)
			{
				requestedLocalTime = DateTime.UtcNow.FromUtcNullAble(tsmtvp.Fleet.ServerUTCOffset);
			}

			try
			{
				Dictionary<string, string> requestHeader = new Dictionary<string, string>();
				Dictionary<string, string> responseHeader = new Dictionary<string, string>();

				if (Login(out requestHeader))
				{

					GreenTomato_GetVehicleInfoForStatusRS rsVehicleInfo = GetVehicleInfoForStautus(tsmtvp.VehicleNunber, requestHeader);

					GreenTomato_SendMessageToDriverRQ rq = new GreenTomato_SendMessageToDriverRQ();
					rq.driverid = rsVehicleInfo.rows[0].driverId;
					rq.title = "zTrip";
					rq.message = tsmtvp.Message;
					string status = "";
					int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["GreenTomatoAPI_Https_Timeout"]);
					//string webRS = WebRequestHelper.ProcessWebRequest("https://80.87.31.28/ext-api/api/vehicle/sendMessageToDriver", string.Empty, "application/json", "POST", string.Empty, string.Empty, 3000, requestHeader, out status);
					string webRS = WebRequestHelper.ProcessWebRequest(GetHttpURL(ConfigurationManager.AppSettings["GreenTomatoAPI_URL_SendMessageToVehicle"]), string.Empty, "application/json", "POST", string.Empty, string.Empty, timeout, requestHeader, out status);
					var sendMsgResult = webRS.JsonDeserialize<GreenTomato_SendMessageToDriverRS>();

					if (sendMsgResult.success.ToLowerInvariant().Equals("true"))
					{
						result = true;
					}
					else
					{
						string error = Messages.Taxi_UnableToSendMessageToVehicle;
						logger.Warn(error);
						throw new GreenTomatoException(error);
					}
				}
				else
				{
					//login failed
					logger.WarnFormat(Messages.Taxi_UnableToLoginGreenTomato);
					throw new GreenTomatoException(Messages.Taxi_UnableToLoginGreenTomato);
				}
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_GreenTomato, "SendMessageToVehicle");
			#endregion

			return result;

		}
		#endregion

		#region Properties
		public TrackTime TrackTime { get; set; }
		#endregion

		#region Private method
		private bool Login(out Dictionary<string, string> newHeaders)
		{


			Stopwatch sw = new Stopwatch();
			sw.Start();

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion



			bool result = false;
			newHeaders = new Dictionary<string, string>();

			try
			{
				string username = ConfigurationManager.AppSettings["GreenTomatoAPI_UserName"];
				string pwd = ConfigurationManager.AppSettings["GreenTomatoAPI_Password"];
				string method = "GET";
				//string prefixURL = "https://80.87.31.28/ext-api/api/auth/login"; //https://80.87.31.28/ext-api/api/auth/login?username=zTrip&password=DaKQN284
				string url = string.Format("{0}?username={1}&password={2}", GetHttpURL(ConfigurationManager.AppSettings["GreenTomatoAPI_URL_Login"]), username, pwd);
				string contenTtype = "application/x-www-form-urlencoded";
				Dictionary<string, string> headers = new Dictionary<string, string>();
				string status = "";
				int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["GreenTomatoAPI_Https_Timeout"]);


				logger.InfoFormat("url: {0}, contentType: {1}, method: {2}", url, contenTtype, method);


				try
				{
					string loginResult = WebRequestHelper.ProcessWebRequest(url, "", contenTtype, method, timeout, null, out status, out headers);
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

				try
				{
					logger.InfoFormat("status: {0}", status);
				}
				catch (Exception ex)
				{

					logger.Error(ex);
				}

				try
				{
					logger.InfoFormat("headers: {0}", string.Join(System.Environment.NewLine, headers.Select(s => string.Format("key: {0}, value: {1}", s.Key, s.Value)).ToArray()));
				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}

				//extract JSESSIONID
				if (Regex.IsMatch(headers["Set-Cookie"], @"JSESSIONID=(\w+);{1}"))
				{

					newHeaders.Add("Cookie", "JSESSIONID=" + Regex.Match(headers["Set-Cookie"], @"JSESSIONID=(\w+);{1}").Groups[1].Value);
					result = true;
				}
				else
				{
					result = false;
				}
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				result = false;
			}

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_GreenTomato, "Login");
			#endregion

			return result;
		}

		private GreenTomato_contact GetContact(string firstName, string lastName, string customerAccountNumber, string phone, Dictionary<string, string> requestHeader)
		{
			GreenTomato_contact contact = null;

			GreenTomato_CreateContactRS rs = GetContact(customerAccountNumber, phone, requestHeader);
			if (rs.rows.Any())
			{
				//contact exist
				contact = rs.rows[0];
				logger.InfoFormat("Get a contact. FirstName={0}, LastName={1}, Phone={2}, customerAccountNumber={3}, Id={4}", contact.firstName, contact.surname, contact.mobilePhone, contact.customerAccountNumber, contact.id);
			}
			else
			{
				//create contact
				var result = CreateContact(firstName, lastName, phone, customerAccountNumber, requestHeader);
				if (result.rows.Any())
				{
					contact = result.rows[0];
					logger.InfoFormat("Create a contact. FirstName={0}, LastName={1}, Phone={2}, customerAccountNumber={3}, Id={4}", contact.firstName, contact.surname, contact.mobilePhone, contact.customerAccountNumber, contact.id);
				}
				else
				{
					contact = null;
					logger.Warn("Unable to get/create this contact");
				}
			}

			return contact;
		}

		private GreenTomato_ServiceListRS GetService(string customerAccountNumber, Dictionary<string, string> headers)
		{
			GreenTomato_ServiceListRS response = null;
			try
			{
				string status = "";
				var getServiceRQ = new GreenTomato_ServiceListRQ();
				getServiceRQ.startIndex = "0";
				getServiceRQ.count = "1";
				getServiceRQ.customerAccountNumber = customerAccountNumber;
				var strRQ = getServiceRQ.JsonSerialize();
				int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["GreenTomatoAPI_Https_Timeout"]);
				//string serviceResult = WebRequestHelper.ProcessWebRequest("https://80.87.31.28/ext-api/api/services/list", strRQ, "application/json", "POST", string.Empty, string.Empty, 3000, headers, out status);
				string serviceResult = WebRequestHelper.ProcessWebRequest(GetHttpURL(ConfigurationManager.AppSettings["GreenTomatoAPI_URL_GetService"]), strRQ, "application/json", "POST", string.Empty, string.Empty, timeout, headers, out status);
				response = serviceResult.JsonDeserialize<GreenTomato_ServiceListRS>();
			}
			catch (Exception ex)
			{

				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				response = null;

			}
			return response;
		}

		private GreenTomato_CreateContactRS GetContact(string customerAccountNumber, string phone, Dictionary<string, string> headers)
		{
			GreenTomato_CreateContactRS response = null;

			try
			{
				var getContactRQ = new GreenTomato_GetContactRQ();
				string status = "";
				getContactRQ.startIndex = "0";
				getContactRQ.count = "1";
				getContactRQ.customerAccountNumber = customerAccountNumber;
				getContactRQ.phone = phone;
				var strCreateContactRQ = getContactRQ.JsonSerialize();
				int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["GreenTomatoAPI_Https_Timeout"]);
				//string contactResult = WebRequestHelper.ProcessWebRequest("https://80.87.31.28/ext-api/api/contacts/list", strCreateContactRQ, "application/json", "POST", string.Empty, string.Empty, 3000, headers, out status);
				string contactResult = WebRequestHelper.ProcessWebRequest(GetHttpURL(ConfigurationManager.AppSettings["GreenTomatoAPI_URL_GetContact"]), strCreateContactRQ, "application/json", "POST", string.Empty, string.Empty, timeout, headers, out status);
				response = contactResult.JsonDeserialize<GreenTomato_CreateContactRS>();
			}
			catch (Exception ex)
			{

				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				response = null;

			}
			return response;

		}

		private GreenTomato_CreateContactRS CreateContact(string firstName, string lastName, string phone, string customerAccountNumber, Dictionary<string, string> headers)
		{
			GreenTomato_CreateContactRS response = null;

			try
			{
				string status = "";
				var createContactRQ = new GreenTomato_CreateContactRQ();
				createContactRQ.firstName = firstName;
				createContactRQ.surname = lastName;
				createContactRQ.customerAccountNumber = customerAccountNumber;
				createContactRQ.mobilePhone = phone;
				var strCreateContactRQ = createContactRQ.JsonSerialize();
				//string contactResult = WebRequestHelper.ProcessWebRequest("https://80.87.31.28/ext-api/api/contacts/update", strCreateContactRQ, "application/json", "POST", string.Empty, string.Empty, 3000, headers, out status);
				string contactResult = WebRequestHelper.ProcessWebRequest(GetHttpURL(ConfigurationManager.AppSettings["GreenTomatoAPI_URL_CreateContact"]), strCreateContactRQ, "application/json", "POST", string.Empty, string.Empty, 3000, headers, out status);
				response = contactResult.JsonDeserialize<GreenTomato_CreateContactRS>();
			}
			catch (Exception ex)
			{

				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				response = null;

			}
			return response;

		}

		private string BuildBookingRequest(TaxiBookingParameter tbp, string bookerId, string serviceId, GreenTomato_rowsForReferenceRQ refType)
		{
			string jsonString = "";

			GreenTomato_BookingRQ bookingRQ = new GreenTomato_BookingRQ();

			//ASAP(now) or PICKUP(future trip)
			if (tbp.PickupNow)
			{
				//ASAP
				bookingRQ.bookingDateType = GreenTomato_BookingDateType.ASAP;
			}
			else
			{
				//PICKUP
				bookingRQ.bookingDateType = GreenTomato_BookingDateType.PICKUP;
				//bookingRQ.bookingDate = System.DateTime.Now.AddDays(2).ToString("dd/MM/yyyy HH:mm");
				//bookingRQ.bookingDate = System.DateTime.UtcNow.FromUtc(tbp.Fleet.ServerUTCOffset).ToString("dd/MM/yyyy HH:mm");
				bookingRQ.bookingDate = tbp.PickupDateTime.ToString("dd/MM/yyyy HH:mm");
			}

			//phone 
			bookingRQ.phone = tbp.PhoneNumber;

			//customer account number
			bookingRQ.customerAccountNumber = tbp.User.AccountNumber;

			//pickup
			GreenTomato_stopRecord pickup = new GreenTomato_stopRecord();
			pickup.address = tbp.PickupAddress.FullAddress;
			pickup.type = GreenTomato_StopRecordType.PICKUP;
			pickup.postcode = tbp.PickupAddress.ZipCode;
			pickup.latitude = tbp.PickupAddress.Geolocation.Latitude.ToString();
			pickup.longitude = tbp.PickupAddress.Geolocation.Longitude.ToString();
			bookingRQ.stopRecordList.Add(pickup);

			//dropoff
			if (tbp.DropOffAddress != null)
			{
				GreenTomato_stopRecord dropoff = new GreenTomato_stopRecord();
				dropoff.address = tbp.DropOffAddress.FullAddress;
				dropoff.type = GreenTomato_StopRecordType.DROP;
				dropoff.postcode = tbp.DropOffAddress.ZipCode;
				dropoff.latitude = tbp.DropOffAddress.Geolocation.Latitude.ToString();
				dropoff.longitude = tbp.DropOffAddress.Geolocation.Longitude.ToString();
				bookingRQ.stopRecordList.Add(dropoff);
			}

			//additional instructions
			bookingRQ.additionalInstructions = tbp.Trip.RemarkForPickup;

			//passenger record 
			GreenTomato_passengerRecord passenger = new GreenTomato_passengerRecord();
			passenger.fullName = string.Format("{0} {1}", tbp.Trip.LastName, tbp.Trip.FirstName);
			passenger.mobilePhone = tbp.Trip.PhoneNumber;
			passenger.landlinePhone = tbp.Trip.PhoneNumber;
			bookingRQ.passengerRecordList.Add(passenger);

			//bookerId serviceId
			bookingRQ.bookerId = bookerId;
			bookingRQ.serviceId = serviceId;

			//reference type
			bookingRQ.referenceRecordList.Add(new GreenTomato_referenceRecord { type = refType.name, value = "VTODAPI" });

			jsonString = bookingRQ.JsonSerialize();


			return jsonString;
		}

		private GreenTomato_ReferenceTypeRS GetReferenceType(string customerAccountNumber, Dictionary<string, string> headers)
		{
			GreenTomato_ReferenceTypeRS response = null;

			try
			{
				GreenTomato_ReferenceTypeRQ rq = new GreenTomato_ReferenceTypeRQ();
				rq.count = "30";
				rq.startIndex = "0";
				rq.customerAccountNumber = customerAccountNumber;
				string rqString = rq.JsonSerialize();
				string status = "";

				//string result = WebRequestHelper.ProcessWebRequest("https://80.87.31.28/ext-api/api/references/types", rqString, "application/json", "POST", string.Empty, string.Empty, 3000, headers, out status);
				string result = WebRequestHelper.ProcessWebRequest(GetHttpURL(ConfigurationManager.AppSettings["GreenTomatoAPI_URL_GetReferenceType"]), rqString, "application/json", "POST", string.Empty, string.Empty, 3000, headers, out status);
				response = result.JsonDeserialize<GreenTomato_ReferenceTypeRS>();
			}
			catch (Exception ex)
			{

				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				response = null;

			}

			return response;
		}

		private GreenTomato_GetVehicleInfoForStatusRS GetVehicleInfoForStautus(string vehicleId, Dictionary<string, string> headers)
		{
			GreenTomato_GetVehicleInfoForStatusRS response = null;

			try
			{
				string status = "";
				//string url = string.Format("https://80.87.31.28/ext-api/api/vehicle/getById/{0}",vehicleId);
				string url = string.Format("{0}/{1}", GetHttpURL(ConfigurationManager.AppSettings["GreenTomatoAPI_URL_GetVehicleInfoForStatus"]), vehicleId);
				string result = WebRequestHelper.ProcessWebRequest(url, string.Empty, "application/json", "GET", string.Empty, string.Empty, 3000, headers, out status);
				response = result.JsonDeserialize<GreenTomato_GetVehicleInfoForStatusRS>();
			}
			catch (Exception ex)
			{

				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				response = null;

			}

			return response;
		}

		private string GetHttpURL(string URI)
		{
			return string.Format("https://{0}{1}", ConfigurationManager.AppSettings["GreenTomatoAPI_DomainName"], URI);
		}

		#endregion


		//public TokenRS TokenVTOD
		//{
		//	set
		//	{
		//		tokenVTOD = value;
		//	}
		//}
	}

}
