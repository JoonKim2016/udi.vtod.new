﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.Track;
using UDI.VTOD.DataAccess.VTOD;
using UDI.VTOD.Domain.Taxi.Class;

namespace UDI.VTOD.Domain.Taxi
{
	interface ITaxiService
	{
        OTA_GroundBookRS Book(TaxiBookingParameter tbp, int fleetTripCode);

		OTA_GroundResRetrieveRS Status(TaxiStatusParameter tsp);

		OTA_GroundCancelRS Cancel(TaxiCancelParameter tcp);

		OTA_GroundAvailRS GetVehicleInfo(TaxiGetVehicleInfoParameter tgvip);

		bool NotifyDriver(TaxiSendMessageToVehicleParameter tsmtvp);

		Common.DTO.OTA.Fleet GetAvailableFleets(TokenRS token, UtilityAvailableFleetsRQ input);

        bool GetDisabilityInd(TokenRS tokenVTOD, taxi_fleet f, out int condition, string authenticateEndpoint, string bookingEndpoint);

        TrackTime TrackTime { get; set; }

		//TokenRS TokenVTOD { set; }

		#region New Service API interface
        OTA_GroundBookRS Book(TokenRS tokenVTOD, OTA_GroundBookRQ request, out List<Tuple<string, int, long>> disptach_Rez_VTOD, int fleetTripCode, taxi_fleet fleet);
        OTA_GroundBookRS ModifyBook(TokenRS tokenVTOD, OTA_GroundBookRQ request, out List<Tuple<string, int, long>> disptach_Rez_VTOD, int fleetTripCode, taxi_fleet fleet,long serviceAPIID);

        OTA_GroundResRetrieveRS Status(TokenRS tokenVTOD, OTA_GroundResRetrieveRQ request,string type);

		OTA_GroundCancelRS Cancel(TokenRS tokenVTOD, OTA_GroundCancelRQ request);

		OTA_GroundAvailRS GetVehicleInfo(TokenRS tokenVTOD, OTA_GroundAvailRQ request);

        OTA_GroundGetFinalRouteRS GetFinalRoute(TokenRS tokenVTOD, OTA_GroundGetFinalRouteRQ request);
        bool NotifyDriver(TokenRS token, UtilityNotifyDriverRQ input);

        bool NotifyPickupDriver(TokenRS token, UtilityNotifyDriverRQ input);
        bool NotifyDispatchPendingDriver(TokenRS token, UtilityNotifyDriverRQ input);

		OTA_GroundAvailRS GetEstimation(TokenRS tokenVTOD, OTA_GroundAvailRQ request);
      
		#endregion

        #region IVR MT Data
        UtilityCustomerInfoRS GetCustomerInfo(TokenRS token, UtilityCustomerInfoRQ input);

        UtilityGetLastTripRS GetLastTrip(TokenRS token, UtilityGetLastTripRQ input);

        UtilityGetPickupAddressRS GetPickupAddress(TokenRS token, UtilityGetPickupAddressRQ input);

        UtilityGetBlackoutPeriodRS GetBlackoutPeriod(TokenRS token, UtilityGetBlackoutPeriodRQ input);

        #region outbound
        UtilityGetNotificationRS GetStaleNotification(TokenRS token, UtilityGetNotificationRQ input);

        UtilityGetNotificationRS GetArrivalNotification(TokenRS token, UtilityGetNotificationRQ input);

        UtilityGetNotificationRS GetNightBeforeReminder(TokenRS token, UtilityGetNotificationRQ input);

        #endregion

        #endregion
    }
}
