﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Core.Metadata.Edm;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using log4net;
using UDI.Map;
using UDI.Map.Adapter.MapQuest.DTO;
using UDI.SDS;
using UDI.SDS.RoutingService;
using UDI.Utility.DTO.Enum;
using UDI.Utility.Helper;
using UDI.Utility.Serialization;
using UDI.VTOD.Common.DTO;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.Helper;
using UDI.VTOD.DataAccess.VTOD;
using UDI.VTOD.Domain.Taxi.Class;
using UDI.VTOD.Domain.Taxi.Const;
using UDI.VTOD.MTData.AuthenticationWebService;
using UDI.VTOD.MTData.BookingWebService;
using Address = UDI.Map.DTO.Address;
using AddressType = UDI.VTOD.Domain.Taxi.Const.AddressType;
using AddressWS = UDI.VTOD.MTData.AddressWebService;
using BookingChannelType = UDI.VTOD.MTData.AuthenticationWebService.BookingChannelType;
using Fleet = UDI.VTOD.MTData.BookingWebService.Fleet;
using Location = UDI.VTOD.MTData.BookingWebService.Location;
using LocationType = UDI.VTOD.MTData.BookingWebService.LocationType;
using UDI.VTOD.MTData.OSIWebService;
using System.ServiceModel;
using UDI.VTOD.Utility.Domain.Verification;

namespace UDI.VTOD.Domain.Taxi.MTData
{
	/// <summary>
	/// Service for MTData API
	/// </summary>
	public class MTDataTaxiService : TaxiServiceBase
	{
		#region variables

		private static string Token = string.Empty;
		private static readonly object ObjSync = new object();
		private readonly string UserName = ConfigurationManager.AppSettings["MTData_Username"];
		private readonly string Password = ConfigurationManager.AppSettings["MTData_Password"];
        private readonly string SystemID = ConfigurationManager.AppSettings["MTData_SystemID"];
		private readonly string IVR_UserName = ConfigurationManager.AppSettings["MTData_IVR_Username"];
		private readonly string IVR_Password = ConfigurationManager.AppSettings["MTData_IVR_Password"];
		private readonly string IVR_SystemID = ConfigurationManager.AppSettings["MTData_IVR_SystemID"];                
        #endregion

        #region constructors       
        public MTDataTaxiService(TaxiUtility util, ILog log, string systemID = null)
			: base(util, log)
		{
            if (!string.IsNullOrEmpty(systemID))
            {
                SystemID = systemID;
            }
                         
        }       

        public MTDataTaxiService(TaxiUtility util, ILog log, int fleetTripCode, string systemID = null) : base(util, log, fleetTripCode)
        {
            if (!string.IsNullOrEmpty(systemID))
            {
                SystemID = systemID;
            }
        }
    
        #endregion

        #region public methods
        /// <summary>
        /// GetDisabilityInd
        /// </summary>
        public override bool GetDisabilityInd(TokenRS tokenVTOD, taxi_fleet f,out int condition,string authenticateEndpoint,string bookingEndpoint)
        {
            string bookingCondition = ConfigurationManager.AppSettings["MTData_Booking_Condition"];
            bool result = false;
            condition = 0;
            logger.Info("Start");
            Stopwatch sw = new Stopwatch();
            sw.Start();
            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
            #endregion
            var quickLocation = DoGetFleet(f, authenticateEndpoint, bookingEndpoint);
            if (quickLocation.Succeeded && quickLocation.Fleets.Any())
            {
                var fleets = quickLocation.Fleets.Where(p => p.ID == f.CCSiFleetId.ToInt32()).FirstOrDefault();
                if (fleets != null && fleets.BookingConditions.Any())
                {
                    var bookCondition= fleets.BookingConditions.Where(p => p.Name == bookingCondition).FirstOrDefault();
                    if(bookCondition!=null)
                    {
                        condition = bookCondition.ID;
                        result = true;
                    }
                }
                    
            }
             sw.Stop();
            logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_MTData, "GetDisabilityInd");
            #endregion
            return result;
        }
           
    /// <summary>
    /// Placing a booking
    /// </summary>
    /// <param name="tokenVTOD"></param>
    /// <param name="request"></param>
    /// <param name="vtodTXId"></param>
    /// <param name="disptach_Rez_VTOD"></param>
    /// <returns></returns>
    /// <exception cref="VtodException"></exception>
    /// <exception cref="TaxiException"></exception>
    public override OTA_GroundBookRS Book(TokenRS tokenVTOD, OTA_GroundBookRQ request, out List<Tuple<string, int, long>> disptach_Rez_VTOD, int fleetTripCode, taxi_fleet fleet)
		{
       
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			Stopwatch swEachPart = new Stopwatch();
			OTA_GroundBookRS response = null;
			disptach_Rez_VTOD = new List<Tuple<string, int, long>>();
			DateTime requestDateTime = DateTime.Now;
            bool ind = false;
            int condition = 0;
            #region validation
            TaxiBookingParameter tbp;
            
            if (!utility.ValidateMTDataBookingRequest(tokenVTOD, request, out tbp, fleet) || tbp.Fleet == null)
			{
				throw VtodException.CreateException(ExceptionType.Taxi, 2007);
			}
            #endregion

            #region Accessibility
            if (request != null && request.GroundReservations != null && request.GroundReservations.Any())
            {
                if (request.GroundReservations.FirstOrDefault().Service != null  && request.GroundReservations.FirstOrDefault().Service.DisabilityVehicleInd == true)
                {
                  
                    ind = GetDisabilityInd(tokenVTOD, tbp.Fleet, out condition,tbp.AuthenticationServiceEndpointName,tbp.BookingWebServiceEndpointName);
                }
            } 
            #endregion
            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion

			DateTime? requestedLocalTime = null;
			
            requestedLocalTime = tbp.PickupDateTime;

			try
			{
				//var fleetID = int.Parse(tbp.Fleet.CCSiFleetId);

				#region format request data
				#region replace the street number in pickup address
				if (tbp.PickupAddress != null && !string.IsNullOrWhiteSpace(tbp.PickupAddress.StreetNo)
							&& tbp.PickupAddress.StreetNo.ToArray().Where(x => x.Equals('–')).Any())
				{
					logger.InfoFormat("The orginial pickup street No. is {0}", tbp.PickupAddress.StreetNo);
					string[] sep = { "–" };
					string[] streetNumbers = tbp.PickupAddress.StreetNo.Split(sep, StringSplitOptions.RemoveEmptyEntries);
					tbp.PickupAddress.StreetNo = streetNumbers[0];
					tbp.Trip.taxi_trip.RemarkForPickup += string.Format(" {0} Customer on {1} block of {2}",
						tbp.Trip.taxi_trip.RemarkForPickup, tbp.PickupAddress.StreetNo, tbp.PickupAddress.Street);
					logger.InfoFormat("Remove range street number in pickup address. Pickup street No. is {0}",
						tbp.PickupAddress.StreetNo);
					logger.InfoFormat("Pickup Comment: {0}", tbp.Trip.taxi_trip.RemarkForPickup);
					if (TrackTime != null)
						TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_MTData, "Book_ReplacePickupStreetNumberRange");
				}
				#endregion

				#region replace the street number in drop address
				if (tbp.DropOffAddress != null && !string.IsNullOrWhiteSpace(tbp.DropOffAddress.StreetNo) &&
							tbp.DropOffAddress.StreetNo.ToArray().Where(x => x.Equals('–')).Any())
				{
					logger.InfoFormat("The orginial dropoff street No. is {0}", tbp.DropOffAddress.StreetNo);
					string[] sep = { "–" };
					string[] streetNumbers = tbp.DropOffAddress.StreetNo.Split(sep,
						StringSplitOptions.RemoveEmptyEntries);
					tbp.DropOffAddress.StreetNo = streetNumbers[0];
					logger.InfoFormat("Remove range street number in dropoff address. Dropoff street No. is {0}",
						tbp.DropOffAddress.StreetNo);
					if (TrackTime != null)
						TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_MTData, "Book_ReplaceDropoffStreetNumberRange");
				}
				#endregion

				#region remove the phone number in comment
				//if (tbp.Trip.taxi_trip.RemarkForPickup != null
				//	&& Regex.IsMatch(tbp.Trip.taxi_trip.RemarkForPickup, @"CUSTOMER PHONE: [\d]+", RegexOptions.IgnoreCase))
				//{
				//	tbp.Trip.taxi_trip.RemarkForPickup = Regex.Replace(tbp.Trip.taxi_trip.RemarkForPickup,
				//		@"CUSTOMER PHONE: [\d]+", "", RegexOptions.IgnoreCase);
				//	logger.Info("Remove cusomter phone in comment.");
				//	logger.InfoFormat("Pickup Comment: {0}", tbp.Trip.taxi_trip.RemarkForPickup);
				//	if (TrackTime != null)
				//		TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_MTData, "Book_ReplacePhoneNumberInComment");
				//} 
				#endregion

				#region replace ztrip to z trip
				//if (tbp.Trip.taxi_trip.RemarkForPickup != null
				//	&& Regex.IsMatch(tbp.Trip.taxi_trip.RemarkForPickup, @"zTrip", RegexOptions.IgnoreCase))
				//{
				//	tbp.Trip.taxi_trip.RemarkForPickup = Regex.Replace(tbp.Trip.taxi_trip.RemarkForPickup, @"zTrip",
				//		"Z Trip", RegexOptions.IgnoreCase);
				//	logger.Info("Replace zTrip with Z Trip");
				//	logger.InfoFormat("Pickup Comment: {0}", tbp.Trip.taxi_trip.RemarkForPickup);
				//	if (TrackTime != null)
				//		TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_MTData, "Book_ReplaceZtripWording");
				//} 
				#endregion

				#region replace zipCode with 5 digits zipcode only
				//replace pickup zipCode with 5 digits zipcode only
				if (tbp.PickupAddress != null && Regex.IsMatch(tbp.PickupAddress.ZipCode, @"([\d]+)\-[\d]+"))
				{
					logger.InfoFormat("Original pickup zipCode: {0}", tbp.PickupAddress.ZipCode);
					tbp.PickupAddress.ZipCode =
						Regex.Match(tbp.PickupAddress.ZipCode, @"([\d]+)\-[\d]+", RegexOptions.IgnoreCase).Groups[1]
							.Value;
					logger.InfoFormat("Replace: {0}", tbp.PickupAddress.ZipCode);
					if (TrackTime != null)
						TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_MTData, "Book_ReplacePickupZipCode");
				}

				//replace dropoff zipCode with 5 digits zipcode only
				if (tbp.DropOffAddress != null && Regex.IsMatch(tbp.DropOffAddress.ZipCode, @"([\d]+)\-[\d]+"))
				{
					logger.InfoFormat("Original dropoff zipCode: {0}", tbp.DropOffAddress.ZipCode);
					tbp.DropOffAddress.ZipCode =
						Regex.Match(tbp.DropOffAddress.ZipCode, @"([\d]+)\-[\d]+", RegexOptions.IgnoreCase).Groups[1]
							.Value;
					logger.InfoFormat("Replace: {0}", tbp.DropOffAddress.ZipCode);
					if (TrackTime != null)
						TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_MTData, "Book_ReplaceDropoffZipCode");
				}
				#endregion

				
				#endregion

				#region convert address to MTData address

				var mtdataAddresses = new List<AddressWS.Address>();
				//string mapAddressesErrMsg;

				#region pickup
				if (tbp.PickupAddressType == AddressType.Airport)
				{
					// airport code mapping
					var quickLocation = DoGetFleetQuickLocations(tbp.Fleet,tbp.AuthenticationServiceEndpointName,tbp.BookingWebServiceEndpointName);
					if (quickLocation.Succeeded && quickLocation.FleetQuickLocationCollection.Any())
					{
						var place = quickLocation.FleetQuickLocationCollection.FirstOrDefault(m => m.PlaceName.ToLower().Trim() == tbp.PickupAirport.ToLower().Trim());

						if (place == null)
						{
                            List<string> airportFullNames = utility.GetSynonyms(tbp.PickupAirport);
                            
                            place = quickLocation.FleetQuickLocationCollection.FirstOrDefault(m => airportFullNames.Contains(m.PlaceName.ToLower().Trim()));
						}

						if (place != null)
						{
							var mtdataAddr = new AddressWS.Address
							{
								AddressType = AddressWS.AddressType.Place,
								Place = new AddressWS.Place { ID = place.PlaceID , Name = place.Name}
							};

							#region Adding lat/lon
							if (place != null && place.Latitude != 0 && place.Longitude != 0)
							{
								mtdataAddr.Latitude = place.Latitude;
								mtdataAddr.Longitude = place.Longitude;
							}
							else if (tbp.PickupAddress.Geolocation != null && tbp.PickupAddress.Geolocation.Latitude != 0 && tbp.PickupAddress.Geolocation.Longitude != 0)
							{
								mtdataAddr.Latitude = (double)tbp.PickupAddress.Geolocation.Latitude;
								mtdataAddr.Longitude = (double)tbp.PickupAddress.Geolocation.Longitude;
							}
							#endregion

							mtdataAddresses.Add(mtdataAddr);
						}
						else
						{
							string error = string.Format("Cannot find matching airport. message={0}", quickLocation.ErrorMessage);
							logger.Error(error);
							utility.MarkThisTripAsError(tbp.Trip.Id, quickLocation.ErrorMessage);
							throw new TaxiException(error);
						}
					}
					else
					{
						string error = string.Format("Cannot find matching airport. message={0}", quickLocation.ErrorMessage);
						logger.Error(error);
						utility.MarkThisTripAsError(tbp.Trip.Id, quickLocation.ErrorMessage);
						throw new TaxiException(error);
					}
				}
				else
				{
					var mapPickupAddress = new AddressWS.Address();
					mapPickupAddress.Designation = new AddressWS.Designation();
					mapPickupAddress.Street = new AddressWS.Street { Name = tbp.PickupAddress.Street /*, Postcode = tbp.PickupAddress.ZipCode */};
					mapPickupAddress.Number = tbp.PickupAddress.StreetNo;
					mapPickupAddress.Suburb = new AddressWS.Suburb { Name = tbp.PickupAddress.City /*, Postcode = tbp.PickupAddress.ZipCode */};
					//mapPickupAddress.CityName = tbp.PickupAddress.City;
					mapPickupAddress.Latitude = (double)tbp.PickupAddress.Geolocation.Latitude;
					mapPickupAddress.Longitude = (double)tbp.PickupAddress.Geolocation.Longitude;
					mapPickupAddress.AddressType = AddressWS.AddressType.Street;
					// }


					mapPickupAddress.Latitude = (double)tbp.PickupAddress.Geolocation.Latitude;
					mapPickupAddress.Longitude = (double)tbp.PickupAddress.Geolocation.Longitude;

					if (System.Configuration.ConfigurationManager.AppSettings["MTData_Force_ZipCode"].ToBool())
					{
						if (mapPickupAddress.Street != null && string.IsNullOrWhiteSpace(mapPickupAddress.Street.Postcode))
						{
							mapPickupAddress.Street.Postcode = tbp.PickupAddress.ZipCode;
						}

						if (mapPickupAddress.Suburb != null && string.IsNullOrWhiteSpace(mapPickupAddress.Suburb.Postcode))
						{
							mapPickupAddress.Suburb.Postcode = tbp.PickupAddress.ZipCode;
						}
					}

					mtdataAddresses.Add(mapPickupAddress);
				}
				#endregion

				#region dropoff
				if (tbp.DropOffAddressType == AddressType.Airport)
				{
					// airport code mapping
					var quickLocation = DoGetFleetQuickLocations(tbp.Fleet,tbp.AuthenticationServiceEndpointName,tbp.BookingWebServiceEndpointName);
					if (quickLocation.Succeeded && quickLocation.FleetQuickLocationCollection.Any())
					{
						var place = quickLocation.FleetQuickLocationCollection.FirstOrDefault(m => m.PlaceName.ToLower().Trim() == tbp.DropoffAirport.ToLower().Trim());

						if (place == null)
						{
                            List<string> airportFullNames = utility.GetSynonyms(tbp.DropoffAirport);
							place = quickLocation.FleetQuickLocationCollection.FirstOrDefault(m => airportFullNames.Contains(m.PlaceName.ToLower().Trim()));
						}

						if (place != null)
						{
							var mtdataAddr = new AddressWS.Address
							{
								AddressType = AddressWS.AddressType.Place,
								Place = new AddressWS.Place { ID = place.PlaceID, Name = place.Name }
							};

							#region Adding lat/lon
							if (place != null && place.Latitude != 0 && place.Longitude != 0)
							{
								mtdataAddr.Latitude = place.Latitude;
								mtdataAddr.Longitude = place.Longitude;
							}
							else if (tbp.DropOffAddress.Geolocation != null && tbp.DropOffAddress.Geolocation.Latitude != 0 && tbp.DropOffAddress.Geolocation.Longitude != 0)
							{
								mtdataAddr.Latitude = (double)tbp.DropOffAddress.Geolocation.Latitude;
								mtdataAddr.Longitude = (double)tbp.DropOffAddress.Geolocation.Longitude;
							}
							#endregion

							mtdataAddresses.Add(mtdataAddr);
						}
						else
						{
							string error = string.Format("Cannot find matching airport. message={0}", quickLocation.ErrorMessage);
							logger.Error(error);
							utility.MarkThisTripAsError(tbp.Trip.Id, quickLocation.ErrorMessage);
							throw new TaxiException(error);
						}
					}
					else
					{
						string error = string.Format("Cannot find matching airport. message={0}", quickLocation.ErrorMessage);
						logger.Error(error);
						utility.MarkThisTripAsError(tbp.Trip.Id, quickLocation.ErrorMessage);
						throw new TaxiException(error);
					}
				}
				else if (tbp.DropOffAddress != null)
				{
					var mapDropOffAddress = new AddressWS.Address();
					mapDropOffAddress.Designation = new AddressWS.Designation();
					mapDropOffAddress.Street = new AddressWS.Street { Name = tbp.DropOffAddress.Street/*, Postcode = tbp.DropOffAddress.ZipCode */};
					mapDropOffAddress.Number = tbp.DropOffAddress.StreetNo;
					mapDropOffAddress.Suburb = new AddressWS.Suburb { Name = tbp.DropOffAddress.City/*, Postcode = tbp.DropOffAddress.ZipCode */};
					//mapDropOffAddress.CityName = tbp.DropOffAddress.City;
					mapDropOffAddress.Latitude = (double)tbp.DropOffAddress.Geolocation.Latitude;
					mapDropOffAddress.Longitude = (double)tbp.DropOffAddress.Geolocation.Longitude;
					mapDropOffAddress.AddressType = AddressWS.AddressType.Street;
					//}


					mapDropOffAddress.Latitude = (double)tbp.DropOffAddress.Geolocation.Latitude;
					mapDropOffAddress.Longitude = (double)tbp.DropOffAddress.Geolocation.Longitude;

					if (System.Configuration.ConfigurationManager.AppSettings["MTData_Force_ZipCode"].ToBool())
					{
						if (mapDropOffAddress.Street != null && string.IsNullOrWhiteSpace(mapDropOffAddress.Street.Postcode))
						{
							mapDropOffAddress.Street.Postcode = tbp.DropOffAddress.ZipCode;
						}

						if (mapDropOffAddress.Suburb != null && string.IsNullOrWhiteSpace(mapDropOffAddress.Suburb.Postcode))
						{
							mapDropOffAddress.Suburb.Postcode = tbp.DropOffAddress.ZipCode;
						}
					}


					mtdataAddresses.Add(mapDropOffAddress);
				}
                #endregion

                #endregion


                #region setup request              
                var bookingSettings = new ImportBookingSettings
				{
					Booking = new Booking
					{
						ContactName = string.Format("{0} {1}", tbp.FirstName, tbp.LastName),
						ContactPhoneNumber = tbp.PhoneNumber,
						Fleet = new Fleet() { ID = Convert.ToInt32(tbp.Fleet.CCSiFleetId) },
						Locations = new Location[2],
						//Conditions = new BookingCondition[] { new BookingCondition { ID = 6 } }
					},
					RemoteSystemID = SystemID,
					RemoteReference = tbp.Trip.Id.ToString()
				};


				//try
				//{
				//	if (tbp.Fleet.IVREnable)
				//	{

				//		bookingSettings.Booking.Conditions = new BookingCondition[] { new BookingCondition { ID = 6 } };
				//	}
				//}
				//catch (Exception ex)
				//{
				//	logger.Error("Booking condition error", (ex));
				//}
                
				bookingSettings.Booking.Locations[0] = new Location
				{
					Address = ConvertAddress(mtdataAddresses[0]),
					AddressType = (UDI.VTOD.MTData.BookingWebService.AddressType)mtdataAddresses[0].AddressType,
					CustomerName = string.Format("{0} {1}", tbp.FirstName,
						tbp.LastName),
					CustomerPhoneNumber = tbp.PhoneNumber,
					CustomerEmail = "",
					LocationType = LocationType.PickUp,
					//Time = tbp.PickupNow ? DateTime.MinValue : tbp.PickupDateTime,
					Time = tbp.PickupDateTime,
					TimeMode = tbp.PickupNow ? TimeMode.NextAvailable : TimeMode.SpecificTime,

					//Remark = tbp.Trip.taxi_trip.RemarkForPickup //(tbp.PickupAddressType == AddressType.Airport || !string.IsNullOrWhiteSpace(mtdataAddresses[0].Number)) ? string.Empty : string.Format(ConfigurationManager.AppSettings["MTData_Pickup_Remark"], tbp.PickupAddress.Street)
					#region Pax
					Pax = tbp.Trip.taxi_trip.NumberOfPassenger.HasValue ? tbp.Trip.taxi_trip.NumberOfPassenger.Value : 1,

					#endregion

				};

				if (tbp.Fleet.IVREnable)
				{
					bookingSettings.RemoteSystemID = IVR_SystemID;
					bookingSettings.Booking.Locations[0].Notifications = new UDI.VTOD.MTData.BookingWebService.Notification { AllowCustomerCallOut = true, CallOnApproach = true };
				}



				if (tbp.PickupAddressType != AddressType.Airport && string.IsNullOrWhiteSpace(mtdataAddresses[0].Number))
				{
					//bookingSettings.Booking.Locations[0].Remark = tbp.Trip.taxi_trip.RemarkForPickup;// string.Format(ConfigurationManager.AppSettings["MTData_Pickup_Remark"], tbp.PickupAddress.Street);
					bookingSettings.Booking.Locations[0].SendPhoneToDriver = true;
				}
				//bookingSettings.Booking.Locations[0].Remark = tbp.Trip.taxi_trip.RemarkForPickup;// string.Format(ConfigurationManager.AppSettings["MTData_Pickup_Remark"], tbp.PickupAddress.Street);
                bookingSettings.Booking.Locations[0].Remark = tbp.Trip.taxi_trip.RemarkForPickup;																			 //bookingSettings.Booking.UseUTCTime=
																							 // address
				bookingSettings.Booking.Locations[1] = new Location();
				if (mtdataAddresses.Count > 1)
				{
					bookingSettings.Booking.Locations[1].Address = ConvertAddress(mtdataAddresses[1]);
					bookingSettings.Booking.Locations[1].AddressType = (UDI.VTOD.MTData.BookingWebService.AddressType)mtdataAddresses[1].AddressType;
					bookingSettings.Booking.Locations[1].CustomerName = string.Format("{0} {1}", tbp.FirstName,
						tbp.LastName);
					bookingSettings.Booking.Locations[1].CustomerPhoneNumber = tbp.PhoneNumber;
					bookingSettings.Booking.Locations[1].CustomerEmail = "";
					bookingSettings.Booking.Locations[1].LocationType = LocationType.DropOff;
					if (tbp.DropOffDateTime == null)
					{
						bookingSettings.Booking.Locations[1].TimeMode = TimeMode.None;
					}
					else
					{
						bookingSettings.Booking.Locations[1].Time = tbp.DropOffDateTime ?? DateTime.MinValue;
						bookingSettings.Booking.Locations[1].TimeMode = TimeMode.SpecificTime;
					}
					bookingSettings.Booking.Locations[1].Remark = tbp.Trip.taxi_trip.RemarkForDropOff;// string.Format(ConfigurationManager.AppSettings["MTData_Pickup_Remark"], tbp.PickupAddress.Street);
                    
				}
				else
				{
					bookingSettings.Booking.Locations[1].AddressType = UDI.VTOD.MTData.BookingWebService.AddressType.None;
					bookingSettings.Booking.Locations[1].LocationType = LocationType.DropOff;
				}

				// set fixed price if needed
				if (tbp.IsFixedPrice)
				{
					bookingSettings.Booking.Price = (int)(tbp.FixedPrice * 100);
					bookingSettings.Booking.Options = new[] { BookingOption.FixedPrice, BookingOption.PriceRequired };
				}
				else
				{
					bookingSettings.Booking.Options = new[] { /*BookingOption.FixedPrice,*/ BookingOption.PriceRequired };
				}

				// payment
				if (tbp.PaymentType == PaymentType.PaymentCard)
				{
					//bookingSettings.Booking.PaymentMethod = new PaymentMethod
					//{
					//	ID = MTDataNameValue.PaymentMethod_CreditCard_ID
					//};
					bookingSettings.Booking.PaymentMethod = new PaymentMethod
					{
						ID = MTDataNameValue.PaymentMethod_Account_ID,
					};
					bookingSettings.Booking.Account = new Account { AccountNumber = tbp.User.AccountNumber }; //, AccountPin = "1234" };
				}
				else if (tbp.PaymentType == PaymentType.Cash)
				{
					bookingSettings.Booking.PaymentMethod = new PaymentMethod
					{
						ID = MTDataNameValue.PaymentMethod_Cash_ID,
					};
					if (!string.IsNullOrEmpty(tbp.User.CashAccountNumber))
					{
						bookingSettings.Booking.Account = new Account { AccountNumber = tbp.User.CashAccountNumber };
					}
				}
				else
				{
					bookingSettings.Booking.PaymentMethod = new PaymentMethod
					{
						ID = MTDataNameValue.PaymentMethod_Cash_ID
					};
				}

				// tip info
				if (!string.IsNullOrWhiteSpace(tbp.Gratuity))
				{
					bookingSettings.Booking.TipInfo = new TipInfo();

					//if (tbp.Gratuity.IndexOf("%", StringComparison.Ordinal) >= 0)
					if (tbp.Gratuity != null && !string.IsNullOrWhiteSpace(tbp.Gratuity))
					{
						if (tbp.Gratuity.Contains("%"))
						{
							bookingSettings.Booking.TipInfo.TipType = TipType.Percent;
							bookingSettings.Booking.TipInfo.Tip = decimal.Parse(tbp.Gratuity.Replace("%", ""));
						}
						else
						{
							bookingSettings.Booking.TipInfo.TipType = TipType.Fixed;
							bookingSettings.Booking.TipInfo.Tip = decimal.Parse(tbp.Gratuity);
						}
					}
					//{

					//	if (tbp.Gratuity.IndexOf("%", StringComparison.Ordinal) == 0)
					//	{
					//		bookingSettings.Booking.TipInfo.TipType = TipType.Percent;
					//		bookingSettings.Booking.TipInfo.Tip = 0;
					//	}
					//	else
					//	{
					//		bookingSettings.Booking.TipInfo.TipType = TipType.Percent;
					//		bookingSettings.Booking.TipInfo.Tip = decimal.Parse(tbp.Gratuity.Replace("%", ""));
					//	}
					//}
					//else
					//{
					//	bookingSettings.Booking.TipInfo.TipType = TipType.Fixed;
					//	bookingSettings.Booking.TipInfo.Tip = decimal.Parse(tbp.Gratuity);
					//}
				}

				#endregion

				swEachPart.Restart();

				var result = DoImportBooking(bookingSettings, fleet,condition,tbp);
				swEachPart.Stop();
				logger.DebugFormat("Call MTData API to book a trip. Duration:{0} ms.", sw.ElapsedMilliseconds);

				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_MTData, "Book_SendingBookingRequest");

				var responseDateTime = DateTime.Now;

				#region handle response

				if (result.Succeeded)
				{
					string confirmationNumber = result.Booking.BookingID.ToString();
					if (!string.IsNullOrWhiteSpace(confirmationNumber))
					{
						tbp.Trip = utility.UpdateTaxiTrip(tbp.Trip, confirmationNumber, fleetTripCode);

						if (tbp.EnableResponseAndLog)
						{
							GroundReservation reservation = tbp.request.GroundReservations.FirstOrDefault();
							response = new OTA_GroundBookRS
							{
								EchoToken = tbp.request.EchoToken,
								Target = tbp.request.Target,
								Version = tbp.request.Version,
								PrimaryLangID = tbp.request.PrimaryLangID,
								Success = new Success(),
								Reservations = new List<Reservation>()
							};

							//Reservation
							var r = new Reservation
							{
								Confirmation = new Confirmation
								{
									ID = tbp.Trip.Id.ToString(),
									Type = ConfirmationType.Confirmation.ToString()
								},
								Passenger = reservation.Passenger,
								Service = reservation.Service
							};
							r.Confirmation.TPA_Extensions = new TPA_Extensions
							{
								Confirmations = new List<Confirmation>
								{
									new Confirmation
									{
										ID = tbp.Trip.taxi_trip.DispatchTripId,
										Type = ConfirmationType.DispatchConfirmation.ToString()
									}
								}
							};
							#region FleetInfo
							taxi_fleet fleetObj = null;
							using (var db = new DataAccess.VTOD.VTODEntities())
							{
								Int64 fleetId = 0;
								taxi_trip tripObj = db.taxi_trip.Where(p => p.Id == tbp.Trip.Id).FirstOrDefault();
								if (tripObj != null)
								{
									fleetId = tripObj.FleetId;
								}
								if (fleetId > 0)
								{
									fleetObj = db.taxi_fleet.Where(p => p.Id == fleetId).FirstOrDefault();
								}

							}
							if (fleetObj != null)
							{
								r.TPA_Extensions = new TPA_Extensions();
								r.TPA_Extensions.Contacts = new Contacts();
								r.TPA_Extensions.Contacts.Items = new List<Contact>();
								var contactDetails = new Contact { Type = Common.DTO.Enum.ContactType.Dispatch.ToString() };
								if (!string.IsNullOrWhiteSpace(fleetObj.Name))
								{
									contactDetails.Name = fleetObj.Name;
								}
								if (!string.IsNullOrWhiteSpace(fleetObj.PhoneNumber))
								{
									contactDetails.Telephone = new Telephone { PhoneNumber = fleetObj.PhoneNumber };
								}
								r.TPA_Extensions.Contacts.Items.Add(contactDetails);
							}
							#endregion

							response.Reservations.Add(r);

							#region Update status into taxi_trip_status
							if (tbp.IsFixedPrice)
							{
								utility.UpdateFinalStatus(tbp.Trip.Id, TaxiTripStatusType.Booked, tbp.FixedPrice.ToDecimal(), null, null, null, null);
							}

							utility.SaveTripStatus(tbp.Trip.Id, TaxiTripStatusType.Booked, requestedLocalTime, null, null, null, null, null, null, null, null, string.Empty, string.Empty, null);
							#endregion

						}
						else
						{
							logger.Info("Disable response and log for booking");
						}
					}
					else
					{
						string error = string.Format("Cannot get DispatchTripId. Or dispatchTripId is white space.");
						logger.Error(error);
						utility.MarkThisTripAsError(tbp.Trip.Id, "BookingID is empty");
						throw new TaxiException(error);
					}

					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_MTData, "Book_CreateResponseObject");


                    #region Inserting Referral log  if it's not registered on referral log   
                    if (request.TPA_Extensions.IsBOBO.ToBool() && !string.IsNullOrEmpty(tbp.PhoneNumber))
                    {
                        var verificationDomain = new VerificationDomain();
                        using (var db = new DataAccess.VTOD.VTODEntities())
                        {
                            if (!db.vtod_referral_logs.Any(x => x.RefereePhoneNumber.Equals(tbp.PhoneNumber)))
                            {
                                vtod_referral_logs referralLog = new vtod_referral_logs();
                                referralLog.RefereePhoneNumber = tbp.PhoneNumber;
                                referralLog.ReferrerSDSMemberId = tbp.BOBOMemberID.Value;
                                referralLog.CreatedOn = DateTime.Now;

                                db.vtod_referral_logs.Add(referralLog);
                                db.SaveChanges();
                            }
                        }
                    }

                    #endregion
                }
                else
				{
					logger.Error(result.ErrorMessage);
					utility.MarkThisTripAsError(tbp.Trip.Id, result.ErrorMessage);
					throw new TaxiException(result.ErrorMessage);
				}

				#endregion

				#region Write Taxi Log
				if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
				{
					var requestMessage = tbp.XmlSerialize().ToString();
					var responseMessage = result.XmlSerialize().ToString();

					utility.WriteTaxiLog(tbp.Trip.Id, TaxiServiceAPIConst.MTData, TaxiServiceMethodType.Book, null,
						requestMessage, responseMessage, requestDateTime, responseDateTime);
				}
				#endregion
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);

				//change the status of trip
				utility.UpdateFinalStatus(tbp.Trip.Id, TaxiTripStatusType.Error, null, null, null, null, null);

				throw;
			}

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_MTData, "Book");
			#endregion

			return response;
		}

    public override OTA_GroundBookRS ModifyBook(TokenRS tokenVTOD, OTA_GroundBookRQ request, out List<Tuple<string, int, long>> disptach_Rez_VTOD, int fleetTripCode, taxi_fleet fleet,long serviceAPIID)
        {
            logger.Info("Start");
            Stopwatch sw = new Stopwatch();
            sw.Start();
            Stopwatch swEachPart = new Stopwatch();
            OTA_GroundBookRS response = null;
            disptach_Rez_VTOD = new List<Tuple<string, int, long>>();
            DateTime requestDateTime = DateTime.Now;
            bool ind = false;
            int condition = 0;
            #region validation
            TaxiBookingParameter tbp;
            vtod_trip v = new vtod_trip();
          
            string dispatchConfirmationId = string.Empty;
            if (request.References!=null && request.References.Any())
            {
                if (request.References.FirstOrDefault().Type == "Confirmation")
                {
                    dispatchConfirmationId = utility.GetDispatchTripId(Convert.ToInt64(request.References.FirstOrDefault().ID));
                }
                else
                {
                    dispatchConfirmationId = request.References.FirstOrDefault().ID;
                }
            }
            else
            {
                throw VtodException.CreateException(ExceptionType.Taxi, 1014);
            }
            v = utility.RetrieveTripDetails(dispatchConfirmationId);

            #region Fetching Endpoint Names
            List<string> endpointNames = utility.MtdataEndpointConfiguration(serviceAPIID, fleet.Id);
            #endregion

            request.TPA_Extensions.PickMeUpNow = v.PickMeUpNow;           
            var bookingresult = DoGetBookingFull(Convert.ToInt32(dispatchConfirmationId), fleet,endpointNames[0],endpointNames[1], SystemID);
            if(!bookingresult.Succeeded)
            {
                throw VtodException.CreateException(ExceptionType.Taxi, 2022);
            }
           

            if (!utility.ValidateMTDataModifyBookingRequest(tokenVTOD, request, out tbp, fleet, bookingresult) || tbp.Fleet == null)
            {
                    throw VtodException.CreateException(ExceptionType.Taxi, 2007);
            }
            #endregion
            #region Retrieve the PickUp/DropOff Remarks
            string userName = tokenVTOD.Username.ToLower().Trim();
            List<string> remarksPickUpDropoff = utility.GetRemarks(userName, fleet.Id);
            if (remarksPickUpDropoff != null && remarksPickUpDropoff.Any())
            {
                if (tbp.PaymentType == PaymentType.PaymentCard)
                {
                    tbp.RemarkForDropOff = remarksPickUpDropoff[3];
                    tbp.RemarkForPickup = remarksPickUpDropoff[2];
                }
                else
                {
                    tbp.RemarkForDropOff = remarksPickUpDropoff[1];
                    tbp.RemarkForPickup = remarksPickUpDropoff[0];
                }
               
            }

            #endregion

            #region Adding Customer Phone#
            if (tbp.PhoneNumber != null)
            {
                var addingText = string.Format("Customer phone: {0}.", tbp.PhoneNumber);
                tbp.RemarkForPickup = tbp.RemarkForPickup.Replace("{PhoneNumber}", addingText);
                tbp.RemarkForDropOff = tbp.RemarkForDropOff.Replace("{PhoneNumber}", addingText);
            }
            #endregion
            #region Adding Tip to MTData
            //***********************************************************************************************************
            //**************** we are adding tip just for MTData in its domain calss: MTDataTaxiService *****************
            //**************** so if you add Tip to all Taxi in TaxiDomain calss please comment this block **************
            //***********************************************************************************************************
            
            var specialInputs = request.GroundReservations.First().RateQualifiers.First().SpecialInputs;
            if (specialInputs != null && specialInputs.Any())
            {
                var gratuity = specialInputs.Where(s => s.Name.ToLower().Trim() == "gratuity").FirstOrDefault();
                if (gratuity != null)
                {
                    var addingText = string.Format(" Tip is {0}", gratuity.Value);
                    tbp.RemarkForPickup += addingText;
                    tbp.RemarkForDropOff += addingText;
                }
            }
            tbp.RemarkForPickup = tbp.RemarkForPickup.Replace("{Gratuity}", "");
            tbp.RemarkForDropOff = tbp.RemarkForDropOff.Replace("{Gratuity}", "");
            if (request != null && request.GroundReservations != null && request.GroundReservations.Any())
            {
                if (request.GroundReservations.First().Service != null && request.GroundReservations.First().Service.Location!=null && request.GroundReservations.First().Service.Location.Pickup != null)
                    request.GroundReservations.First().Service.Location.Pickup.Remark = tbp.RemarkForPickup;
            }
            if (request != null && request.GroundReservations != null && request.GroundReservations.Any())
            {
                if (request.GroundReservations.First().Service != null && request.GroundReservations.First().Service.Location != null && request.GroundReservations.First().Service.Location.Dropoff != null)

                    request.GroundReservations.First().Service.Location.Dropoff.Remark = tbp.RemarkForDropOff;
            }
            #endregion

            #region Retrieve the Trip Details
            tbp.Trip = v;
            #endregion

            #region Accessibility
            if (request != null && request.GroundReservations != null && request.GroundReservations.Any())
            {
                if (request.GroundReservations.FirstOrDefault().Service != null && request.GroundReservations.FirstOrDefault().Service.DisabilityVehicleInd == true)
                {

                    ind = GetDisabilityInd(tokenVTOD, tbp.Fleet, out condition,tbp.AuthenticationServiceEndpointName,tbp.BookingWebServiceEndpointName);
                }
            }
            #endregion

            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
            #endregion


            DateTime? requestedLocalTime = null;
            string remarks=string.Empty;
            requestedLocalTime = tbp.PickupDateTime;

            try
            {
                #region format request data
                #region replace the street number in pickup address
                if (tbp.PickupAddress != null && !string.IsNullOrWhiteSpace(tbp.PickupAddress.StreetNo)
                            && tbp.PickupAddress.StreetNo.ToArray().Where(x => x.Equals('–')).Any())
                {
                    logger.InfoFormat("The orginial pickup street No. is {0}", tbp.PickupAddress.StreetNo);
                    string[] sep = { "–" };
                    string[] streetNumbers = tbp.PickupAddress.StreetNo.Split(sep, StringSplitOptions.RemoveEmptyEntries);
                    tbp.PickupAddress.StreetNo = streetNumbers[0];
                    tbp.Trip.taxi_trip.RemarkForPickup += string.Format(" {0} Customer on {1} block of {2}",
                        tbp.Trip.taxi_trip.RemarkForPickup, tbp.PickupAddress.StreetNo, tbp.PickupAddress.Street);
                    logger.InfoFormat("Remove range street number in pickup address. Pickup street No. is {0}",
                        tbp.PickupAddress.StreetNo);
                    logger.InfoFormat("Pickup Comment: {0}", tbp.Trip.taxi_trip.RemarkForPickup);
                    if (TrackTime != null)
                        TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_MTData, "Book_ReplacePickupStreetNumberRange");
                }
                #endregion

                #region replace the street number in drop address
                if (tbp.DropOffAddress != null && !string.IsNullOrWhiteSpace(tbp.DropOffAddress.StreetNo) &&
                            tbp.DropOffAddress.StreetNo.ToArray().Where(x => x.Equals('–')).Any())
                {
                    logger.InfoFormat("The orginial dropoff street No. is {0}", tbp.DropOffAddress.StreetNo);
                    string[] sep = { "–" };
                    string[] streetNumbers = tbp.DropOffAddress.StreetNo.Split(sep,
                        StringSplitOptions.RemoveEmptyEntries);
                    tbp.DropOffAddress.StreetNo = streetNumbers[0];
                    logger.InfoFormat("Remove range street number in dropoff address. Dropoff street No. is {0}",
                        tbp.DropOffAddress.StreetNo);
                    if (TrackTime != null)
                        TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_MTData, "Book_ReplaceDropoffStreetNumberRange");
                }
                #endregion

                #region replace zipCode with 5 digits zipcode only
                //replace pickup zipCode with 5 digits zipcode only
                if (tbp.PickupAddress != null && Regex.IsMatch(tbp.PickupAddress.ZipCode, @"([\d]+)\-[\d]+"))
                {
                    logger.InfoFormat("Original pickup zipCode: {0}", tbp.PickupAddress.ZipCode);
                    tbp.PickupAddress.ZipCode =
                        Regex.Match(tbp.PickupAddress.ZipCode, @"([\d]+)\-[\d]+", RegexOptions.IgnoreCase).Groups[1]
                            .Value;
                    logger.InfoFormat("Replace: {0}", tbp.PickupAddress.ZipCode);
                    if (TrackTime != null)
                        TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_MTData, "Book_ReplacePickupZipCode");
                }

                //replace dropoff zipCode with 5 digits zipcode only
                if (tbp.DropOffAddress != null && Regex.IsMatch(tbp.DropOffAddress.ZipCode, @"([\d]+)\-[\d]+"))
                {
                    logger.InfoFormat("Original dropoff zipCode: {0}", tbp.DropOffAddress.ZipCode);
                    tbp.DropOffAddress.ZipCode =
                        Regex.Match(tbp.DropOffAddress.ZipCode, @"([\d]+)\-[\d]+", RegexOptions.IgnoreCase).Groups[1]
                            .Value;
                    logger.InfoFormat("Replace: {0}", tbp.DropOffAddress.ZipCode);
                    if (TrackTime != null)
                        TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_MTData, "Book_ReplaceDropoffZipCode");
                }
                #endregion

                #endregion

                #region convert address to MTData address

                var mtdataAddresses = new List<AddressWS.Address>();
                //string mapAddressesErrMsg;

                #region pickup
                if (tbp.PickupAddressType == AddressType.Airport)
                {
                    // airport code mapping
                    var quickLocation = DoGetFleetQuickLocations(tbp.Fleet,tbp.AuthenticationServiceEndpointName,tbp.BookingWebServiceEndpointName);
                    if (quickLocation.Succeeded && quickLocation.FleetQuickLocationCollection.Any())
                    {
                        var place = quickLocation.FleetQuickLocationCollection.FirstOrDefault(m => m.PlaceName.ToLower().Trim() == tbp.PickupAirport.ToLower().Trim());

                        if (place == null)
                        {
                            List<string>airportFullNames = utility.GetSynonyms(tbp.PickupAirport);
                            place = quickLocation.FleetQuickLocationCollection.FirstOrDefault(m => airportFullNames.Contains(m.PlaceName.ToLower().Trim()));
                        }

                        if (place != null)
                        {
                            var mtdataAddr = new AddressWS.Address
                            {
                                AddressType = AddressWS.AddressType.Place,
                                Place = new AddressWS.Place { ID = place.PlaceID }
                            };

                            #region Adding lat/lon
                            if (place != null && place.Latitude != 0 && place.Longitude != 0)
                            {
                                mtdataAddr.Latitude = place.Latitude;
                                mtdataAddr.Longitude = place.Longitude;
                            }
                            else if (tbp.PickupAddress.Geolocation != null && tbp.PickupAddress.Geolocation.Latitude != 0 && tbp.PickupAddress.Geolocation.Longitude != 0)
                            {
                                mtdataAddr.Latitude = (double)tbp.PickupAddress.Geolocation.Latitude;
                                mtdataAddr.Longitude = (double)tbp.PickupAddress.Geolocation.Longitude;
                            }
                            #endregion

                            mtdataAddresses.Add(mtdataAddr);
                        }
                        else
                        {
                            string error = string.Format("Cannot find matching airport. message={0}", quickLocation.ErrorMessage);
                            logger.Error(error);
                            utility.MarkThisTripAsError(tbp.Trip.Id, quickLocation.ErrorMessage);
                            throw new TaxiException(error);
                        }
                    }
                    else
                    {
                        string error = string.Format("Cannot find matching airport. message={0}", quickLocation.ErrorMessage);
                        logger.Error(error);
                        utility.MarkThisTripAsError(tbp.Trip.Id, quickLocation.ErrorMessage);
                        throw new TaxiException(error);
                    }
                }
                else
                {
                    var mapPickupAddress = new AddressWS.Address();
                    mapPickupAddress.Designation = new AddressWS.Designation();
                    mapPickupAddress.Street = new AddressWS.Street { Name = tbp.PickupAddress.Street /*, Postcode = tbp.PickupAddress.ZipCode */};
                    mapPickupAddress.Number = tbp.PickupAddress.StreetNo;
                    mapPickupAddress.Suburb = new AddressWS.Suburb { Name = tbp.PickupAddress.City /*, Postcode = tbp.PickupAddress.ZipCode */};
                    //mapPickupAddress.CityName = tbp.PickupAddress.City;
                    mapPickupAddress.Latitude = (double)tbp.PickupAddress.Geolocation.Latitude;
                    mapPickupAddress.Longitude = (double)tbp.PickupAddress.Geolocation.Longitude;
                    mapPickupAddress.AddressType = AddressWS.AddressType.Street;
                    // }
                    
                    if (System.Configuration.ConfigurationManager.AppSettings["MTData_Force_ZipCode"].ToBool())
                    {
                        if (mapPickupAddress.Street != null && string.IsNullOrWhiteSpace(mapPickupAddress.Street.Postcode))
                        {
                            mapPickupAddress.Street.Postcode = tbp.PickupAddress.ZipCode;
                        }

                        if (mapPickupAddress.Suburb != null && string.IsNullOrWhiteSpace(mapPickupAddress.Suburb.Postcode))
                        {
                            mapPickupAddress.Suburb.Postcode = tbp.PickupAddress.ZipCode;
                        }
                    }

                    mtdataAddresses.Add(mapPickupAddress);
                }
                #endregion

                #region dropoff
                if (tbp.DropOffAddressType == AddressType.Airport)
                {
                    // airport code mapping
                    var quickLocation = DoGetFleetQuickLocations(tbp.Fleet,tbp.AuthenticationServiceEndpointName,tbp.BookingWebServiceEndpointName);
                    if (quickLocation.Succeeded && quickLocation.FleetQuickLocationCollection.Any())
                    {
                        var place = quickLocation.FleetQuickLocationCollection.FirstOrDefault(m => m.PlaceName.ToLower().Trim() == tbp.DropoffAirport.ToLower().Trim());

                        if (place == null)
                        {
                            List<string> airportFullNames = utility.GetSynonyms(tbp.DropoffAirport);
                            place = quickLocation.FleetQuickLocationCollection.FirstOrDefault(m => airportFullNames.Contains(m.PlaceName.ToLower().Trim()));
                        }

                        if (place != null)
                        {
                            var mtdataAddr = new AddressWS.Address
                            {
                                AddressType = AddressWS.AddressType.Place,
                                Place = new AddressWS.Place { ID = place.PlaceID, Name = place.Name }
                            };

                            #region Adding lat/lon
                            if (place != null && place.Latitude != 0 && place.Longitude != 0)
                            {
                                mtdataAddr.Latitude = place.Latitude;
                                mtdataAddr.Longitude = place.Longitude;
                            }
                            else if (tbp.DropOffAddress.Geolocation != null && tbp.DropOffAddress.Geolocation.Latitude != 0 && tbp.DropOffAddress.Geolocation.Longitude != 0)
                            {
                                mtdataAddr.Latitude = (double)tbp.DropOffAddress.Geolocation.Latitude;
                                mtdataAddr.Longitude = (double)tbp.DropOffAddress.Geolocation.Longitude;
                            }
                            #endregion

                            mtdataAddresses.Add(mtdataAddr);
                        }
                        else
                        {
                            string error = string.Format("Cannot find matching airport. message={0}", quickLocation.ErrorMessage);
                            logger.Error(error);
                            utility.MarkThisTripAsError(tbp.Trip.Id, quickLocation.ErrorMessage);
                            throw new TaxiException(error);
                        }
                    }
                    else
                    {
                        string error = string.Format("Cannot find matching airport. message={0}", quickLocation.ErrorMessage);
                        logger.Error(error);
                        utility.MarkThisTripAsError(tbp.Trip.Id, quickLocation.ErrorMessage);
                        throw new TaxiException(error);
                    }
                }
                else if (tbp.DropOffAddress != null)
                {
                    var mapDropOffAddress = new AddressWS.Address();
                    mapDropOffAddress.Designation = new AddressWS.Designation();
                    mapDropOffAddress.Street = new AddressWS.Street { Name = tbp.DropOffAddress.Street/*, Postcode = tbp.DropOffAddress.ZipCode */};
                    mapDropOffAddress.Number = tbp.DropOffAddress.StreetNo;
                    mapDropOffAddress.Suburb = new AddressWS.Suburb { Name = tbp.DropOffAddress.City/*, Postcode = tbp.DropOffAddress.ZipCode */};
                    //mapDropOffAddress.CityName = tbp.DropOffAddress.City;
                    mapDropOffAddress.Latitude = (double)tbp.DropOffAddress.Geolocation.Latitude;                    
                    mapDropOffAddress.Longitude = (double)tbp.DropOffAddress.Geolocation.Longitude;
                    mapDropOffAddress.AddressType = AddressWS.AddressType.Street;
      
                    if (System.Configuration.ConfigurationManager.AppSettings["MTData_Force_ZipCode"].ToBool())
                    {
                        if (mapDropOffAddress.Street != null && string.IsNullOrWhiteSpace(mapDropOffAddress.Street.Postcode))
                        {
                            mapDropOffAddress.Street.Postcode = tbp.DropOffAddress.ZipCode;
                        }

                        if (mapDropOffAddress.Suburb != null && string.IsNullOrWhiteSpace(mapDropOffAddress.Suburb.Postcode))
                        {
                            mapDropOffAddress.Suburb.Postcode = tbp.DropOffAddress.ZipCode;
                        }
                    }


                    mtdataAddresses.Add(mapDropOffAddress);
                }
                #endregion

                #endregion

                #region setup request

                var bookingSettings = new ImportBookingSettings
                {
                    Booking = new Booking
                    {
                        ContactName = bookingresult.Settings.Booking.ContactName,
                        ContactPhoneNumber = bookingresult.Settings.Booking.ContactPhoneNumber,
                        Fleet = new Fleet() { ID = Convert.ToInt32(tbp.Fleet.CCSiFleetId) },
                        Locations = new Location[2],
                        BookingID = bookingresult.Settings.Booking.BookingID
                        //Conditions = new BookingCondition[] { new BookingCondition { ID = 6 } }
                    },
                    RemoteSystemID = bookingresult.Settings.RemoteSystemID,
                    RemoteReference = bookingresult.Settings.RemoteReference,
                   
                    
                };
                bookingSettings.Booking.Locations[0] = new Location
                {
                    Address = ConvertAddress(mtdataAddresses[0]),
                    AddressType = (UDI.VTOD.MTData.BookingWebService.AddressType)mtdataAddresses[0].AddressType,
                    CustomerName = string.Format("{0} {1}", tbp.FirstName,
                        tbp.LastName),
                    CustomerPhoneNumber = tbp.PhoneNumber,
                    CustomerEmail = "",
                    LocationType = LocationType.PickUp,
                    Time = tbp.PickupDateTime,
                    TimeMode = tbp.PickupNow ? TimeMode.NextAvailable : TimeMode.SpecificTime,
                    #region Pax
                    Pax = tbp.NumberOfPassenger>0 ? tbp.NumberOfPassenger : 1,
                    #endregion

                };

                if (tbp.Fleet.IVREnable)
                {
                    bookingSettings.RemoteSystemID = IVR_SystemID;
                    bookingSettings.Booking.Locations[0].Notifications = new UDI.VTOD.MTData.BookingWebService.Notification { AllowCustomerCallOut = true, CallOnApproach = true };
                }



                if (tbp.PickupAddressType != AddressType.Airport && string.IsNullOrWhiteSpace(mtdataAddresses[0].Number))
                {
                    bookingSettings.Booking.Locations[0].SendPhoneToDriver = true;
                }
                bookingSettings.Booking.Locations[0].Remark = tbp.RemarkForPickup;
                bookingSettings.Booking.Locations[1] = new Location();
                if (mtdataAddresses.Count > 1)
                {
                    bookingSettings.Booking.Locations[1].Address = ConvertAddress(mtdataAddresses[1]);
                    bookingSettings.Booking.Locations[1].AddressType = (UDI.VTOD.MTData.BookingWebService.AddressType)mtdataAddresses[1].AddressType;
                    bookingSettings.Booking.Locations[1].CustomerName = string.Format("{0} {1}", tbp.FirstName,
                        tbp.LastName);
                    bookingSettings.Booking.Locations[1].CustomerPhoneNumber = tbp.PhoneNumber;
                    bookingSettings.Booking.Locations[1].CustomerEmail = "";
                    bookingSettings.Booking.Locations[1].LocationType = LocationType.DropOff;
                    if (tbp.DropOffDateTime == null)
                    {
                        bookingSettings.Booking.Locations[1].TimeMode = TimeMode.None;
                    }
                    else
                    {
                        bookingSettings.Booking.Locations[1].Time = tbp.DropOffDateTime ?? DateTime.MinValue;
                        bookingSettings.Booking.Locations[1].TimeMode = TimeMode.SpecificTime;
                    }
                    bookingSettings.Booking.Locations[1].Remark = tbp.RemarkForDropOff;// string.Format(ConfigurationManager.AppSettings["MTData_Pickup_Remark"], tbp.PickupAddress.Street);
                     }
                else
                {
                    bookingSettings.Booking.Locations[1].AddressType = UDI.VTOD.MTData.BookingWebService.AddressType.None;
                    bookingSettings.Booking.Locations[1].LocationType = LocationType.DropOff;
                }

                // set fixed price if needed
                if (tbp.IsFixedPrice)
                {
                    bookingSettings.Booking.Price = (int)(tbp.FixedPrice * 100);
                    bookingSettings.Booking.Options = new[] { BookingOption.FixedPrice, BookingOption.PriceRequired };
                }
                else
                {
                    bookingSettings.Booking.Options = new[] { /*BookingOption.FixedPrice,*/ BookingOption.PriceRequired };
                }

                // payment
                if (tbp.PaymentType == PaymentType.PaymentCard)
                {
                    bookingSettings.Booking.PaymentMethod = new PaymentMethod
                    {
                        ID = MTDataNameValue.PaymentMethod_Account_ID,
                    };
                    bookingSettings.Booking.Account = new Account { AccountNumber = tbp.User.AccountNumber }; 
                }
                else if (tbp.PaymentType == PaymentType.Cash)
                {
                    bookingSettings.Booking.PaymentMethod = new PaymentMethod
                    {
                        ID = MTDataNameValue.PaymentMethod_Cash_ID,
                    };
                    if (!string.IsNullOrEmpty(tbp.User.CashAccountNumber))
                    {
                        bookingSettings.Booking.Account = new Account { AccountNumber = tbp.User.CashAccountNumber };
                    }
                }
                else
                {
                    bookingSettings.Booking.PaymentMethod = new PaymentMethod
                    {
                        ID = MTDataNameValue.PaymentMethod_Cash_ID
                    };
                }

                // tip info
                if (!string.IsNullOrWhiteSpace(tbp.Gratuity))
                {
                    bookingSettings.Booking.TipInfo = new TipInfo();
                    if (tbp.Gratuity != null)
                    {
                        if (tbp.GratuityType.Trim().ToLower()== "Percent".ToLower())
                        {
                            bookingSettings.Booking.TipInfo.TipType = TipType.Percent;
                            bookingSettings.Booking.TipInfo.Tip = decimal.Parse(tbp.Gratuity);
                        }
                        else
                        {
                            bookingSettings.Booking.TipInfo.TipType = TipType.Fixed;
                            bookingSettings.Booking.TipInfo.Tip = decimal.Parse(tbp.Gratuity);
                        }
                    }
                }

                #endregion

                swEachPart.Restart();

                var result = DoImportBooking(bookingSettings, fleet, condition,tbp);

                swEachPart.Stop();
                logger.DebugFormat("Call MTData API to book a trip. Duration:{0} ms.", sw.ElapsedMilliseconds);

                if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_MTData, "Book_SendingBookingRequest");

                var responseDateTime = DateTime.Now;

                #region handle response

                if (result.Succeeded)
                {
                    string confirmationNumber = result.Booking.BookingID.ToString();
                    if (!string.IsNullOrWhiteSpace(confirmationNumber))
                    {
                        bool IsModifyTrip= utility.UpdateModifyTaxiTrip(tbp);
                        if(IsModifyTrip==false)
                        {
                            logger.InfoFormat("Error in Modification");
                        }

                        if (tbp.EnableResponseAndLog)
                        {
                            GroundReservation reservation = tbp.request.GroundReservations.FirstOrDefault();
                            if (reservation != null && reservation.Service != null)
                            {
                                if (reservation.Service.Location != null && reservation.Service.Location != null)
                                    reservation.Service.Location.Pickup.Remark = tbp.RemarkForPickup;
                            }
                            response = new OTA_GroundBookRS
                            {
                                EchoToken = tbp.request.EchoToken,
                                Target = tbp.request.Target,
                                Version = tbp.request.Version,
                                PrimaryLangID = tbp.request.PrimaryLangID,
                                Success = new Success(),
                                Reservations = new List<Reservation>()
                            };

                            //Reservation
                            var r = new Reservation
                            {
                                Confirmation = new Confirmation
                                {
                                    ID = tbp.Trip.Id.ToString(),
                                    Type = ConfirmationType.Confirmation.ToString()
                                },
                                Passenger = reservation.Passenger,
                                Service = reservation.Service
                            };
                            r.Confirmation.TPA_Extensions = new TPA_Extensions
                            {
                                Confirmations = new List<Confirmation>
                                {
                                    new Confirmation
                                    {
                                        ID = tbp.Trip.taxi_trip.DispatchTripId,
                                        Type = ConfirmationType.DispatchConfirmation.ToString()
                                    }
                                }
                            };
                            #region FleetInfo
                            taxi_fleet fleetObj = null;
                            using (var db = new DataAccess.VTOD.VTODEntities())
                            {
                                Int64 fleetId = 0;
                                taxi_trip tripObj = db.taxi_trip.Where(p => p.Id == tbp.Trip.Id).FirstOrDefault();
                                if (tripObj != null)
                                {
                                    fleetId = tripObj.FleetId;
                                }
                                if (fleetId > 0)
                                {
                                    fleetObj = db.taxi_fleet.Where(p => p.Id == fleetId).FirstOrDefault();
                                }

                            }
                            if (fleetObj != null)
                            {
                                r.TPA_Extensions = new TPA_Extensions();
                                r.TPA_Extensions.Contacts = new Contacts();
                                r.TPA_Extensions.Contacts.Items = new List<Contact>();
                                var contactDetails = new Contact { Type = Common.DTO.Enum.ContactType.Dispatch.ToString() };
                                if (!string.IsNullOrWhiteSpace(fleetObj.Name))
                                {
                                    contactDetails.Name = fleetObj.Name;
                                }
                                if (!string.IsNullOrWhiteSpace(fleetObj.PhoneNumber))
                                {
                                    contactDetails.Telephone = new Telephone { PhoneNumber = fleetObj.PhoneNumber };
                                }
                                r.TPA_Extensions.Contacts.Items.Add(contactDetails);
                            }
                            #endregion

                            response.Reservations.Add(r);

                            #region Update status into taxi_trip_status
                            if (tbp.IsFixedPrice)
                            {
                                utility.UpdateFinalStatus(tbp.Trip.Id, TaxiTripStatusType.ModifyBooked, tbp.FixedPrice.ToDecimal(), null, null, null, null);
                            }

                            utility.SaveTripStatus(tbp.Trip.Id, TaxiTripStatusType.ModifyBooked, requestedLocalTime, null, null, null, null, null, null, null, null, string.Empty, string.Empty, null);
                            #endregion

                        }
                        else
                        {
                            logger.Info("Disable response and log for booking");
                        }
                    }
                    else
                    {
                        string error = string.Format("Cannot get DispatchTripId. Or dispatchTripId is white space.");
                        logger.Error(error);
                        utility.MarkThisTripAsError(tbp.Trip.Id, "BookingID is empty");
                        throw new TaxiException(error);
                    }

                    if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_MTData, "Book_CreateResponseObject");
                }
                else
                {
                    logger.Error(result.ErrorMessage);
                    utility.MarkThisTripAsError(tbp.Trip.Id, result.ErrorMessage);
                    throw new TaxiException(result.ErrorMessage);
                }

                #endregion

                #region Write Taxi Log
                if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
                {
                    var requestMessage = tbp.XmlSerialize().ToString();
                    var responseMessage = result.XmlSerialize().ToString();

                    utility.WriteTaxiLog(tbp.Trip.Id, TaxiServiceAPIConst.MTData, TaxiServiceMethodType.Book, null,
                        requestMessage, responseMessage, requestDateTime, responseDateTime);
                }
                #endregion
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Message:{0}", ex.Message);
                logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
                logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);

                //change the status of trip
                utility.UpdateFinalStatus(tbp.Trip.Id, TaxiTripStatusType.Error, null, null, null, null, null);

                throw;
            }

            sw.Stop();
            logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_MTData, "Book");
            #endregion

            return response;
        }

        /// <summary>
        /// Retrieving the status of a booking
        /// </summary>
        /// <param name="tokenVTOD"></param>
        /// <param name="request"></param>
        /// <param name="vtodTXId"></param>
        /// <returns></returns>
        public override OTA_GroundResRetrieveRS Status(TokenRS tokenVTOD, OTA_GroundResRetrieveRQ request, string type)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			OTA_GroundResRetrieveRS response = null;
			int? etaWithTraffic = null;
			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion

			#region Validate required fields
			TaxiStatusParameter tsp = null;
			string dispatchConfirmationId = string.Empty;
			if (type == "Confirmation" && request.Reference.Where(x => x.Type == type).Any())
			{
				dispatchConfirmationId = utility.GetDispatchTripId(Convert.ToInt64(request.Reference.Where(x => x.Type == type).FirstOrDefault().ID));
			}
			else
			{
				dispatchConfirmationId = request.Reference.Where(x => x.Type == type).FirstOrDefault().ID;
			}


			if (utility.IsVTODTaxiTrip(dispatchConfirmationId, TaxiServiceAPIConst.MTData) ||
                utility.IsVTODTaxiTrip(dispatchConfirmationId, TaxiServiceAPIConst.Texas) ||
                utility.IsVTODTaxiTrip(dispatchConfirmationId, TaxiServiceAPIConst.Phoenix))
			{
				//vtod trip
				logger.Info("Vtod trip");

				if (!utility.ValidateStatusRequest(tokenVTOD, request, out tsp))
				{
					logger.ErrorFormat("TripID:{0}", tsp.Trip.Id);
					throw VtodException.CreateException(ExceptionType.Taxi, 3003);
				}
			}           
            else
			{
				//non - vtod trip
				logger.Info("Non-Vtod trip");
				var fleet = utility.GetFleet(request.TPA_Extensions.FleetId);
				string MTDataToken = string.Empty;
				RefreshToken(ref MTDataToken, fleet,tsp.AuthenticationServiceEndpointName);

				#region non-vtod trip
				if (!utility.ValidateMTDataStatusRequest(tokenVTOD, request, out tsp, MTDataToken))
				{
					throw VtodException.CreateException(ExceptionType.Taxi, 3003);
				}
				#endregion
			}
			#endregion

			#region get response back
			try
			{
				var requestDateTime = DateTime.Now;
				decimal MinutesAway = 0;
				decimal MinutesAwayWithTraffic = 0;
				
				if (!string.IsNullOrEmpty(dispatchConfirmationId))
				{
                    GetBookingResult statusResult = DoGetBooking(Convert.ToInt64(dispatchConfirmationId), tsp);
                    OSIBookingResult bookingResult = DoGetBookingByID(Convert.ToInt64(dispatchConfirmationId), tsp);

					var responseDateTime = DateTime.Now;
					string driverID = null;

					if (statusResult.Succeeded)
					{
						#region handle response

						response = new OTA_GroundResRetrieveRS
						{
							Success = new Success(),
							EchoToken = tsp.request.EchoToken,
							Target = tsp.request.Target,
							Version = tsp.request.Version,
							PrimaryLangID = tsp.request.PrimaryLangID,
						};

						if (statusResult.BookingSummary != null)
						{
							var vtodTripStatus = utility.ConvertTripStatusForMTData(statusResult.BookingSummary.Status);

                            driverID = bookingResult.Booking.Driver != null ? bookingResult.Booking.Driver.Value : null;

                            #region Caculate ETA by ourself
                            if (
								(vtodTripStatus.ToUpperInvariant().Equals(TaxiTripStatusType.Accepted.ToUpperInvariant()) || vtodTripStatus.ToUpperInvariant().Equals(TaxiTripStatusType.Assigned.ToUpperInvariant()) || vtodTripStatus.ToUpperInvariant().Equals(TaxiTripStatusType.InTransit.ToUpperInvariant()))
								&&
								(statusResult.BookingSummary.VehicleLat != 0 && statusResult.BookingSummary.VehicleLng != 0)
								&&
								(tsp.Trip.taxi_trip.PickupLatitude.HasValue && tsp.Trip.taxi_trip.PickupLongitude.HasValue)
							)
							{
								MapService ms = new MapService();
								double d = ms.GetDirectDistanceInMeter(tsp.Trip.taxi_trip.PickupLatitude.Value, tsp.Trip.taxi_trip.PickupLongitude.Value, Convert.ToDouble(statusResult.BookingSummary.VehicleLat), Convert.ToDouble(statusResult.BookingSummary.VehicleLng));
								var rc = new UDI.SDS.RoutingController(tsp.tokenVTOD, null);
								UDI.SDS.RoutingService.Point start = new UDI.SDS.RoutingService.Point { Latitude = tsp.Trip.taxi_trip.PickupLatitude.Value, Longitude = tsp.Trip.taxi_trip.PickupLongitude.Value };
								UDI.SDS.RoutingService.Point end = new UDI.SDS.RoutingService.Point { Latitude = Convert.ToDouble(statusResult.BookingSummary.VehicleLat), Longitude = Convert.ToDouble(statusResult.BookingSummary.VehicleLng) };
								var result = rc.GetRouteMetrics(start, end);
								MinutesAway = result.TotalMinutes;

							}
							#endregion

							#region ETA with Traffic
							//ToDo: it should be configurable. It's hardcoded just for now but should be configuratble.
							if (tsp.User.name.ToLower() == "ztrip" || !tsp.Trip.DriverAcceptedETA.HasValue)
							{
								if (
														vtodTripStatus.Trim().ToLower() == Const.TaxiTripStatusType.Accepted.Trim().ToLower() ||
														vtodTripStatus.Trim().ToLower() == Const.TaxiTripStatusType.Assigned.Trim().ToLower() ||
														vtodTripStatus.Trim().ToLower() == Const.TaxiTripStatusType.InTransit.Trim().ToLower()
														)
								{
									if (statusResult.BookingSummary.VehicleNumber != null && statusResult.BookingSummary.VehicleLat != 0 && statusResult.BookingSummary.VehicleLng != 0)
									{
										if (tsp.Trip != null && tsp.Trip.taxi_trip != null && tsp.Trip.taxi_trip.PickupLatitude.HasValue && tsp.Trip.taxi_trip.PickupLatitude.Value != 0 && tsp.Trip.taxi_trip.PickupLongitude.HasValue && tsp.Trip.taxi_trip.PickupLongitude.Value != 0)
										{
											try
											{
												var error = string.Empty;
												var map = new UDI.Map.MapService(Map.DTO.MapApiProvider.Google);
												var etaResult = map.GetETA(new Map.DTO.Geolocation { Latitude = statusResult.BookingSummary.VehicleLat.ToDecimal(), Longitude = statusResult.BookingSummary.VehicleLng.ToDecimal() },
													new Map.DTO.Geolocation { Latitude = tsp.Trip.taxi_trip.PickupLatitude.Value.ToDecimal(), Longitude = tsp.Trip.taxi_trip.PickupLongitude.ToDecimal() }, UDI.Map.DTO.DistanceUnit.Mile, out error);

												if (string.IsNullOrWhiteSpace(error))
												{
													etaWithTraffic = System.Math.Round((decimal)(etaResult.DurationInTrafficInSecond / 60)).ToInt32();
													MinutesAwayWithTraffic = etaWithTraffic.ToDecimal();
												}
											}
											catch
											{ }
										}
									}
								}
							}
							#endregion


							response.TPA_Extensions = new TPA_Extensions
							{
								Vehicles = new Vehicles
								{
									Items = new List<Vehicle>
								{
									new Vehicle
									{
										ID = statusResult.BookingSummary.VehicleNumber,
										MinutesAway = MinutesAway,
										Geolocation = new Geolocation
										{
											Lat = (decimal) statusResult.BookingSummary.VehicleLat,
											Long = (decimal) statusResult.BookingSummary.VehicleLng
										}
									}
								}
								},
								Statuses = new Statuses
								{
									Status = new List<Status>
								{
									new Status
									{
										Value =
											utility.ConvertTripStatusMessageForFrontEndDevice(tsp.Fleet_User.UserId,
												vtodTripStatus)
									}
								}
								},
								PickupLocation = new Pickup_Dropoff_Stop
								{
									Address = new Common.DTO.OTA.Address
									{
										StreetNmbr = statusResult.BookingSummary.PickupStreetNumber,
										AddressLine = string.Format("{0} {1}",
											statusResult.BookingSummary.PickupStreetName,
											statusResult.BookingSummary.PickupDesignation),
										CityName = statusResult.BookingSummary.PickupSuburb,
										Latitude =
									tsp.Trip == null || tsp.Trip.taxi_trip.PickupLatitude == null
												? ""
												: tsp.Trip.taxi_trip.PickupLatitude.Value.ToString(CultureInfo.InvariantCulture),
										Longitude =
									tsp.Trip == null || tsp.Trip.taxi_trip.PickupLongitude == null
												? ""
												: tsp.Trip.taxi_trip.PickupLongitude.Value.ToString(CultureInfo.InvariantCulture)
									}
								},
								DropoffLocation = new Pickup_Dropoff_Stop
								{
									Address = new Common.DTO.OTA.Address
									{
										Latitude =
									tsp.Trip == null || tsp.Trip.taxi_trip.DropOffLatitude == null
												? ""
												: tsp.Trip.taxi_trip.DropOffLatitude.Value.ToString(CultureInfo.InvariantCulture),
										Longitude =
									tsp.Trip == null || tsp.Trip.taxi_trip.DropOffLongitude == null
												? ""
												: tsp.Trip.taxi_trip.DropOffLongitude.Value.ToString(CultureInfo.InvariantCulture)
									}
								},
								ServiceCharges = new ServiceCharges
								{
									Amount = statusResult.BookingSummary.Charge.ToString(CultureInfo.InvariantCulture)
								}
						,
								PickupTimes = new PickupTimeList
								{
									//PickupTimes = new List<PickupTime> { new PickupTime { StartDateTime = utility.ConvertToLocalTime(tsp.Fleet, statusResult.BookingSummary.Time), EndDateTime = utility.ConvertToLocalTime(tsp.Fleet,statusResult.BookingSummary.Time) } }
									PickupTimes = new List<PickupTime> { new PickupTime { StartDateTime = utility.ConvertToLocalTime(tsp.Fleet, statusResult.BookingSummary.Time), EndDateTime = utility.ConvertToLocalTime(tsp.Fleet, statusResult.BookingSummary.Time) } }

								},
                                
							};
                            #region Logic For Modify Reservation
                            if (tsp.Trip.PickupDateTimeUTC > DateTime.UtcNow)
                            {
                                if (tsp.Trip.FinalStatus == UDI.VTOD.Domain.Taxi.Const.TaxiTripStatusType.Completed || tsp.Trip.FinalStatus == UDI.VTOD.Domain.Taxi.Const.TaxiTripStatusType.Canceled || tsp.Trip.FinalStatus == UDI.VTOD.Domain.Taxi.Const.TaxiTripStatusType.NoShow)
                                {
                                    response.TPA_Extensions.IsModify = false;
                                }
                                else
                                {
                                    response.TPA_Extensions.IsModify = true;
                                }

                            }
                            else
                            {
                                response.TPA_Extensions.IsModify = false;
                            }

                            #endregion


                            #region Get Geolocation from MapQuest
                            //MapService ms = new MapService();
                            //string errorMessage = "";
                            //var addresses = ms.GetAddresses(string.Format("{0} {1} {2} {3}", statusResult.BookingSummary.PickupStreetNumber, statusResult.BookingSummary.PickupStreetName, statusResult.BookingSummary.PickupDesignation, statusResult.BookingSummary.PickupSuburb), out errorMessage);
                            //if (addresses != null && addresses.Any()) {
                            //    response.TPA_Extensions.PickupLocation.Address.Latitude = addresses.FirstOrDefault().Geolocation.Latitude.ToString();
                            //    response.TPA_Extensions.PickupLocation.Address.Longitude = addresses.FirstOrDefault().Geolocation.Longitude.ToString();
                            //}
                            #endregion



                            #region Get Driver Info

                            vtod_driver_info driverInfo = null;
							if (tsp.Fleet.DriverInfo == UDI.VTOD.Common.Const.DriveInfoSource.TCS)
							{
                                /**** Start-Get Driver Info From the vtod_driver_info****/
                                if (tsp.Trip.DriverInfoID.HasValue && tsp.Trip.DriverInfoID.Value > 0)
                                {
                                    using (var db = new DataAccess.VTOD.VTODEntities())
                                    {
                                        driverInfo = db.vtod_driver_info.FirstOrDefault(t => t.Id == tsp.Trip.DriverInfoID.Value);
                                    }
                                }
                                /****End-Get Driver Info From the vtod_driver_info****/
                                else
                                {

                                    if (tsp.tokenVTOD != null && tsp.lastTripStatus.DriverId != null && !string.IsNullOrWhiteSpace(tsp.lastTripStatus.DriverId))
                                    {
                                        driverInfo = utility.GetDriverInfoByDriverId(tsp.tokenVTOD.Username, tsp.Trip, tsp.lastTripStatus.Status, tsp.lastTripStatus.DriverId);
                                    }
                                    if (driverInfo == null)
                                    {
                                        if (tsp.tokenVTOD != null && !string.IsNullOrWhiteSpace(statusResult.BookingSummary.VehicleNumber) && !string.IsNullOrEmpty(driverID))
                                        {
                                            driverInfo = utility.GetDriverInfoByVehicleNumber(tsp.tokenVTOD.Username, vtodTripStatus, statusResult.BookingSummary.VehicleNumber, driverID, tsp.Trip);
                                        }
                                    }
                                }
                                if (tsp.Trip.DriverInfoID.HasValue && tsp.Trip.DriverInfoID.Value > 0)
                                {
                                    using (var db = new DataAccess.VTOD.VTODEntities())
                                    {
                                        driverInfo = db.vtod_driver_info.FirstOrDefault(t => t.Id == tsp.Trip.DriverInfoID.Value);
                                    }
                                }
                               
                               
							}
                            else if (tsp.Fleet.DriverInfo == UDI.VTOD.Common.Const.DriveInfoSource.MTData)
                            {

                                if (tsp.tokenVTOD != null && !string.IsNullOrWhiteSpace(statusResult.BookingSummary.VehicleNumber) && bookingResult.Booking.Driver != null)
                                {   
                                    try
                                    {
                                        if (!string.IsNullOrWhiteSpace(tsp.tokenVTOD.Username))
                                        {
                                            if (utility.IsAllowedPullingDriverInfo(tsp.tokenVTOD.Username) && tsp.Trip != null)
                                            {
                                                if (!tsp.Trip.DriverInfoID.HasValue || tsp.Trip.DriverInfoID.Value == 0)
                                                {
                                                    if (utility.HasDriver(vtodTripStatus))
                                                    {
                                                        OSIDriverResult oSIDriverResult = DoGetOSIDriverResultByID(bookingResult.Booking.Driver.ID, tsp);
                                                        
                                                        driverInfo = MapOSIDriverResultToVtod_driver_info(oSIDriverResult);

                                                        using (var db = new DataAccess.VTOD.VTODEntities())
                                                        {
                                                            VTOD.DriverInfo driverInformation = new VTOD.DriverInfo(logger);

                                                            driverInfo = driverInformation.UpsertDriverDetails(driverInfo, null);

                                                            db.SP_vtod_Update_TripDriverInfoID(tsp.Trip.Id, driverInfo.Id);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (tsp.Trip.DriverInfoID.HasValue && tsp.Trip.DriverInfoID.Value > 0)
                                                    {
                                                        using (var db = new DataAccess.VTOD.VTODEntities())
                                                        {
                                                            driverInfo = db.vtod_driver_info.Where(s => s.Id == tsp.Trip.DriverInfoID.Value).FirstOrDefault();
                                                        }
                                                    }
                                                }
                                            }
                                            //It is IVR trip if trip is null 
                                            //In IVR case, we don't need to save driver info
                                            else
                                            {
                                                if (tsp.Trip.DriverInfoID.HasValue && tsp.Trip.DriverInfoID.Value > 0)
                                                {
                                                    using (var db = new DataAccess.VTOD.VTODEntities())
                                                    {
                                                        driverInfo = db.vtod_driver_info.Where(s => s.Id == tsp.Trip.DriverInfoID.Value).FirstOrDefault();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                                    {
                                        foreach (var eve in ex.EntityValidationErrors)
                                        {
                                            logger.ErrorFormat("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                                eve.Entry.Entity.GetType().Name, eve.Entry.State);
                                            foreach (var ve in eve.ValidationErrors)
                                            {
                                                logger.ErrorFormat("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName, ve.ErrorMessage);
                                            }
                                        }
                                        throw;
                                    }

                                }
                            }
                            else if (tsp.Fleet.DriverInfo == UDI.VTOD.Common.Const.DriveInfoSource.Local)
							{
								if (tsp.Trip.DriverInfoID.HasValue && tsp.Trip.DriverInfoID.Value > 0)
								{
									if (tsp.Trip != null)
									{
										using (var db = new DataAccess.VTOD.VTODEntities())
										{
											driverInfo = db.vtod_driver_info.FirstOrDefault(t => t.Id == tsp.Trip.DriverInfoID.Value);
										}
									}
								}
								else
								{
									if (tsp.tokenVTOD != null && tsp.lastTripStatus.DriverId != null && !string.IsNullOrWhiteSpace(tsp.lastTripStatus.DriverId))
									{
										using (var db = new DataAccess.VTOD.VTODEntities())
										{
											driverInfo = db.vtod_driver_info.FirstOrDefault(t => t.DriverID == tsp.lastTripStatus.DriverId && t.FleetID == tsp.Fleet.Id && t.FleetType == "Taxi");
											if (driverInfo != null)
												db.SP_vtod_Update_TripDriverInfoID(tsp.Trip.Id, driverInfo.Id);
										}

									}
								}
							}
							#endregion

							#region Add dispatch info & driver info

							response.TPA_Extensions.Contacts = new Contacts { Items = new List<Contact>() };

							var dispatchContact = new Contact
							{
								Type = ContactType.Dispatch.ToString(),
								Name = tsp.Fleet.Alias,
								Telephone = new Telephone { PhoneNumber = tsp.Fleet.PhoneNumber.CleanPhone10Digit() },
								PhotoUrl = ConfigHelper.GetDriverPhotoVersionDefault()
							};
							response.TPA_Extensions.Contacts.Items.Add(dispatchContact);

							if (driverInfo != null && !string.IsNullOrWhiteSpace(driverInfo.CellPhone))
							{
								var driverContact = new Contact
								{
									Type = ContactType.Driver.ToString(),
									Name = string.Format("{0} {1}", driverInfo.FirstName, driverInfo.LastName),
									Telephone = new Telephone { PhoneNumber = driverInfo.CellPhone.CleanPhone10Digit() },
									PhotoUrl = driverInfo.PhotoUrl,
									ZtripDateOfDesignation = driverInfo.QualificationDate,
									ZtripDesignation = driverInfo.zTripQualified
								};
								response.TPA_Extensions.Contacts.Items.Add(driverContact);
							}

                            #endregion

                            
                            #region Add split payment info 
                            if (tsp.Trip != null)
                            {
                                using (var db = new VTODEntities())
                                {
                                    var vtodTrip = db.vtod_trip.FirstOrDefault(m => m.Id == tsp.Trip.Id);
                                    response.TPA_Extensions.SplitPayment = new SplitPaymentInfo();
                                    if (vtodTrip.IsSplitPayment != null && vtodTrip.IsSplitPayment.Value)
                                    {
                                        response.TPA_Extensions.SplitPayment.IsSplitPayment = true;
                                        response.TPA_Extensions.SplitPayment.SplitPaymentStatus = ((SplitPaymentStatus)vtodTrip.SplitPaymentStatus).ToString();

                                        var taxiTrip = db.taxi_trip.FirstOrDefault(m => m.Id == tsp.Trip.Id);
                                        response.TPA_Extensions.SplitPayment.MemberA = new SplitPaymentUserInfo
                                        {
                                            MemberID = vtodTrip.MemberID,
                                            FirstName = taxiTrip.FirstName,
                                            LastName = taxiTrip.LastName
                                        };
                                        response.TPA_Extensions.SplitPayment.MemberB = new SplitPaymentUserInfo
                                        {
                                            MemberID = vtodTrip.SplitPaymentMemberB,
                                            FirstName = taxiTrip.MemberBFirstName,
                                            LastName = taxiTrip.MemberBLastName
                                        };
                                    }
                                    else
                                    {
                                        response.TPA_Extensions.SplitPayment.IsSplitPayment = false;
                                    }
                                }
                            }
                            #endregion

                            if (tsp.Trip != null)
							{
								#region Update status & fare

								try
								{  
                                    decimal? fare = null;
									decimal? gratuity = null;
									if (vtodTripStatus == TaxiTripStatusType.Completed)
									{
										try
										{
                                            fare = CalculateFare(bookingResult.Booking);
                                            gratuity = bookingResult.Booking.Tips;

                                            decimal? overwriteFare = null;

											try
											{
												overwriteFare = utility.DetermineOverwriteFare(tsp, response.TPA_Extensions.Vehicles.Items.First().Geolocation.Long, response.TPA_Extensions.Vehicles.Items.First().Geolocation.Lat, fare);
												fare = (overwriteFare.HasValue && overwriteFare.Value > 0) ? overwriteFare.Value : fare;
											}
											catch (Exception ex)
											{
												logger.Error("Cannot find Flat Rate", ex);
											}

											logger.InfoFormat("TripID:{0}, Price:{1}, Extras:{2}, FlagFall:{3}, ServiceFee:{4}, Tolls:{5}, AccountCharge:{6}, Discount:{7}, Tips:{8}", tsp.Trip.taxi_trip.Id, bookingResult.Booking.Price, bookingResult.Booking.Extras, bookingResult.Booking.FlagFall, bookingResult.Booking.ServiceFee, bookingResult.Booking.Tolls, bookingResult.Booking.AccountCharge, bookingResult.Booking.Discount, bookingResult.Booking.Tips);

											//fare = statusResult.BookingSummary.Charge;
										}
										catch (Exception ex)
										{
											logger.Error(ex.ToString());
										}
									}

									decimal? lng = (decimal)statusResult.BookingSummary.VehicleLng;
									if (lng == 0) lng = null;
									decimal? lat = (decimal)statusResult.BookingSummary.VehicleLat;
									if (lat == 0) lat = null;
									DateTime? tripStartTime = null;
									DateTime? tripEndTime = null;
									DateTime? statusTime = null;
									if (!statusTime.HasValue || statusTime.Value <= DateTime.MinValue || statusTime.Value < new DateTime(2010, 1, 1))
									{
										if (tsp.Fleet != null && tsp.Fleet.ServerUTCOffset != null)
										{
											statusTime = DateTime.UtcNow.FromUtcNullAble(tsp.Fleet.ServerUTCOffset);
										}
									}

									if ((vtodTripStatus.Trim().ToLower() == TaxiTripStatusType.PickUp.Trim().ToLower()) && (tsp.Trip.TripStartTime == null))
									{
										tripStartTime = statusTime;
									}
									else
									{
										tripStartTime = tsp.Trip.TripStartTime;
									}

									if ((vtodTripStatus.Trim().ToLower() == TaxiTripStatusType.Completed.Trim().ToLower() || vtodTripStatus.Trim().ToLower() == TaxiTripStatusType.MeterOff.Trim().ToLower()) && (tsp.Trip.TripEndTime == null))
									{
										tripEndTime = statusTime;
									}
									else
									{
										tripEndTime = tsp.Trip.TripEndTime;
									}

									if (vtodTripStatus.ToUpperInvariant() == TaxiTripStatusType.Canceled.ToUpperInvariant())
									{
										utility.SaveTripStatus(tsp.Trip.Id, TaxiTripStatusType.Canceled, statusTime, null, null, null, null, null, null, null, null, string.Empty, vtodTripStatus, null);
										utility.UpdateFinalStatus(tsp.Trip.taxi_trip.Id, vtodTripStatus, fare, gratuity, null, null, null);
									}
									else if (vtodTripStatus.ToUpperInvariant() == TaxiTripStatusType.FastMeter.ToUpperInvariant())
									{
										utility.SaveTripStatus(tsp.Trip.taxi_trip.Id, vtodTripStatus, statusTime,
									  statusResult.BookingSummary.VehicleNumber, lat, lng, fare, null, null, null, null, null,
									  statusResult.BookingSummary.Status.ToString(), driverID);
										utility.UpdateFinalStatus(tsp.Trip.taxi_trip.Id, vtodTripStatus, fare, gratuity, tripStartTime, tripEndTime, null);
									}
									else
									{
										decimal? overwriteFare = utility.DetermineOverwriteFare(tsp, response.TPA_Extensions.Vehicles.Items.First().Geolocation.Long, response.TPA_Extensions.Vehicles.Items.First().Geolocation.Lat, fare);

										response.TPA_Extensions.Statuses.Status[0].Fare = overwriteFare.HasValue ? overwriteFare.Value : response.TPA_Extensions.Statuses.Status[0].Fare;


										utility.SaveTripStatus(tsp.Trip.taxi_trip.Id, vtodTripStatus, statusTime,
										statusResult.BookingSummary.VehicleNumber, lat, lng, fare, null, MinutesAway.ToInt32(), etaWithTraffic, null, null,
										statusResult.BookingSummary.Status.ToString(), driverID);

										//Save final status
										if (tsp.Trip.FinalStatus.ToLower() == TaxiTripStatusType.Accepted.ToLower() || tsp.Trip.FinalStatus.ToLower() == TaxiTripStatusType.Assigned.ToLower())
										{
											utility.UpdateFinalStatus(tsp.Trip.Id, vtodTripStatus, overwriteFare, null, tripStartTime, tripEndTime, etaWithTraffic);
										}
										else
										{
											utility.UpdateFinalStatus(tsp.Trip.Id, vtodTripStatus, overwriteFare, null, tripStartTime, tripEndTime, null);
										}
									}
								}
								catch (Exception ex)
								{
									logger.Error(ex.ToString());
								}

								#endregion

								#region SharedETA Link
								if (request.TPA_Extensions != null && request.TPA_Extensions.RateQualifiers != null && request.TPA_Extensions.RateQualifiers.Any())
								{
									string uniqueID = null;
									string rateQualifier = null;
									string sharedLink = null;
									string appSharedLink = ConfigurationManager.AppSettings["SharedETALink"];
									string link = null;
									if (!String.IsNullOrWhiteSpace(type))
									{
										link = "?t=" + type.Substring(0, 1).ToLower();
									}
									if (request.TPA_Extensions.RateQualifiers != null && request.TPA_Extensions.RateQualifiers.Any())
									{
										rateQualifier = request.TPA_Extensions.RateQualifiers.FirstOrDefault().RateQualifierValue;
										link = link + "&RQ=" + rateQualifier;
									}
									if (request.Reference != null && request.Reference.Any())
									{
										uniqueID = request.Reference.FirstOrDefault().ID;
									}

									link = link + "&id=" + UDI.Utility.Helper.Cryptography.ConvertStringToHex(uniqueID);                                   
									response.TPA_Extensions.SharedLink = appSharedLink + link;
								}
                                #endregion


                                if (tsp.Trip.IsBOBO.ToBool())
                                {
                                    response.Reference = new List<Reference>();

                                    Reference reference = new Reference();
                                    reference.Type = ConfirmationType.DispatchConfirmation.ToString();
                                    reference.ID = tsp.Trip.taxi_trip.DispatchTripId;

                                    Verification verification = new Verification();

                                    PersonName personName = new PersonName();
                                    personName.GivenName = tsp.Trip.taxi_trip.FirstName;
                                    personName.Surname = tsp.Trip.taxi_trip.LastName;
                                    verification.PersonName = personName;

                                    if (!string.IsNullOrWhiteSpace(tsp.Trip.taxi_trip.PhoneNumber))
                                    {
                                        Telephone telephone = new Telephone();
                                        telephone.PhoneNumber = tsp.Trip.taxi_trip.PhoneNumber;
                                        verification.TelephoneInfo = telephone;
                                    }

                                    reference.Verification = verification;

                                    response.Reference.Add(reference);
                                }
							}
						}

						#endregion
					}
					else
					{
						const string error = "Fail to get MTData booking status.";
						logger.Error(error);
						logger.Error(string.Format("error={0}, error_message={1}",
							statusResult.Error, statusResult.ErrorMessage));
						throw VtodException.CreateException(ExceptionType.Taxi, 5001);
					}

					#region Write Taxi Log
					if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]) && tsp.Trip != null)
					{
						var requestMessage = tsp.Trip.taxi_trip.DispatchTripId;
						var responseMessage = statusResult.XmlSerialize().ToString();

						utility.WriteTaxiLog(tsp.Trip.Id, TaxiServiceAPIConst.MTData, TaxiServiceMethodType.Status,
							null, requestMessage, responseMessage, requestDateTime, responseDateTime);
					}
					#endregion
				}
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw new TaxiException(ex.Message);
			}
			#endregion

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_MTData, "Status");
			#endregion

			return response;
		}



		/// <summary>
		/// Cancelling a booking
		/// </summary>
		/// <param name="tokenVTOD"></param>
		/// <param name="request"></param>		
		/// <returns></returns>
		/// <exception cref="VtodException"></exception>
		public override OTA_GroundCancelRS Cancel(TokenRS tokenVTOD, OTA_GroundCancelRQ request)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			OTA_GroundCancelRS response;
			DateTime requestDateTime = DateTime.Now;

			#region Validate required fields
			TaxiCancelParameter tcp;
			string dispatchConfirmationId = string.Empty;
			if (request.Reservation.UniqueID.Where(x => x.Type == "Confirmation").Any())
			{
				dispatchConfirmationId = utility.GetDispatchTripId(Convert.ToInt64(request.Reservation.UniqueID.Where(x => x.Type == "Confirmation").FirstOrDefault().ID));
			}
			else
			{
				dispatchConfirmationId = request.Reservation.UniqueID.Where(x => x.Type == "DispatchConfirmation").FirstOrDefault().ID;
			}
           
            if (utility.IsVTODTaxiTrip(dispatchConfirmationId, TaxiServiceAPIConst.MTData) ||
                utility.IsVTODTaxiTrip(dispatchConfirmationId, TaxiServiceAPIConst.Texas)  ||
                utility.IsVTODTaxiTrip(dispatchConfirmationId, TaxiServiceAPIConst.Phoenix))
			{
				//vtod trip
				if (!utility.ValidateCancelRequest(tokenVTOD, request, out tcp))
				{
					throw VtodException.CreateException(ExceptionType.Taxi, 5005);
				}
			}           
            else
			{
				//non vtod trip
				var fleet = utility.GetFleet(request.TPA_Extensions.FleetId);
				string MTDataToken = string.Empty;
				
				if (!utility.ValidateMTDataCancelRequest(tokenVTOD, request, out tcp, MTDataToken))
				{
					throw VtodException.CreateException(ExceptionType.Taxi, 5005);
				}
                RefreshToken(ref MTDataToken, fleet,tcp.AuthenticationServiceEndpointName);

			}
			#endregion

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion

			DateTime? requestedLocalTime = null;
			if (tcp.Fleet != null && tcp.Fleet.ServerUTCOffset != null)
			{
				requestedLocalTime = DateTime.UtcNow.FromUtcNullAble(tcp.Fleet.ServerUTCOffset);
			}

			try
			{
				CancelBookingResult cancelResult;
				DateTime responseDateTime;
				//vtod trip
				if (
					tcp.Trip == null
					||
					utility.IsAbleToCancelThisTrip(tcp.Trip.FinalStatus)
					)
				{
					cancelResult = DoCancelBooking(Convert.ToInt64(dispatchConfirmationId), tcp.Fleet,tcp);

					if (cancelResult.Succeeded)
					{
						responseDateTime = DateTime.Now;

						response = new OTA_GroundCancelRS
						{
							Success = new Success(),
							EchoToken = tcp.request.EchoToken,
							Target = tcp.request.Target,
							Version = tcp.request.Version,
							PrimaryLangID = tcp.request.PrimaryLangID
						};

						if (tcp.Trip != null)
						{
							response.Reservation = new Reservation
							{
								CancelConfirmation = new CancelConfirmation
								{
									UniqueID = new UniqueID { ID = tcp.Trip.Id.ToString() }
								}
							};

							#region Put status into taxi_trip_status
							utility.SaveTripStatus(tcp.Trip.Id, TaxiTripStatusType.Canceled, requestedLocalTime, null,
								null, null, null, null, null, null, null, string.Empty, string.Empty, null);
							#endregion

							#region Set final status
							utility.UpdateFinalStatus(tcp.Trip.Id, TaxiTripStatusType.Canceled, null, null, null, null, null);
							#endregion
						}

					}
					else
					{
						const string error = "Fail to cancel a MTData booking.";
						logger.Error(error);
						logger.Error(string.Format("error={0}, error_message={1}",
							cancelResult.Error, cancelResult.ErrorMessage));
						throw VtodException.CreateException(ExceptionType.Taxi, 5001);
					}
				}
				else
				{
					throw VtodException.CreateException(ExceptionType.Taxi, 5001);
				}

				#region Write Taxi Log
				if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]) && tcp.Trip != null)
				{
					var requestMessage = tcp.XmlSerialize().ToString();
					var responseMessage = cancelResult.XmlSerialize().ToString();

					utility.WriteTaxiLog(tcp.Trip.Id, TaxiServiceAPIConst.MTData, TaxiServiceMethodType.Cancel, null,
						requestMessage, responseMessage, requestDateTime, responseDateTime);
				}
				#endregion
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw;
			}

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_MTData, "Cancel");
			#endregion

			return response;
		}

		/// <summary>
		/// Retrieving available vehicles in an area
		/// </summary>
		/// <param name="tokenVTOD"></param>
		/// <param name="request"></param>
		/// <param name="vtodTXId"></param>
		/// <returns></returns>
		public override OTA_GroundAvailRS GetVehicleInfo(TokenRS tokenVTOD, OTA_GroundAvailRQ request)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			var response = new OTA_GroundAvailRS
			{
				Success = new Success(),
				TPA_Extensions = new TPA_Extensions
				{
					Vehicles = new Vehicles { Items = new List<Vehicle>() }
				}
			};
			var requestDateTime = DateTime.Now;
            int condition = 0;
            bool ind = false;
            #region Validate required fields
            TaxiGetVehicleInfoParameter tgvip;
			if (!utility.ValidateGetVehicleInfoRequest(tokenVTOD, request, out tgvip) || tgvip.Fleet == null)
			{
				string error = string.Format("This GetVehicleInfo request is invalid.");
				throw new ValidationException(error);
			}
            #endregion
            #region Add Accessibility
            if (request != null && request.DisabilityInfo != null && request.DisabilityInfo.RequiredInd == true)
            {
                tgvip.DisabilityVehicleInd = request.DisabilityInfo.RequiredInd;
                ind=GetDisabilityInd(tokenVTOD, tgvip.Fleet, out condition, tgvip.AuthenticationServiceEndpointName, tgvip.BookingWebServiceEndpointName);
            }
           
            #endregion
            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion

			try
			{
				//var fleetID = int.Parse(tgvip.Fleet.CCSiFleetId);

				//convert distance to Mile
				tgvip.Radius = Convert.ToDouble(Convert.ToDecimal(tgvip.Radius).ConvertDistance(
					DistanceUnit.Mile, DistanceUnit.Feet));
                
				var vehiclesResult = DoGetFleetVacanVehiclesForBookingLocation(
					tgvip.Fleet, tgvip.Latitude, tgvip.Longtitude, (float)tgvip.Radius, condition, tgvip.AuthenticationServiceEndpointName, tgvip.BookingWebServiceEndpointName);

				var responseDateTime = DateTime.Now;
				var mapService = new MapService();
				if (vehiclesResult.Succeeded && vehiclesResult.Vehicles.Any())
				{
					#region find nearest vehicle
					double? shortestDistance = null;
					DispatchVehicle nearestVehicle = null;
					var sortedVehicles = new SortedList<double, DispatchVehicle>();
					foreach (var vehicle in vehiclesResult.Vehicles)
					{
						try
						{
							double d = mapService.GetDirectDistanceInMile(tgvip.Latitude, tgvip.Longtitude, vehicle.Latitude, vehicle.Longitude);

							sortedVehicles.Add(d, vehicle);

							if (shortestDistance.HasValue)
							{
								if (shortestDistance.Value > d)
								{
									shortestDistance = d;
									nearestVehicle = vehicle;
								}
							}
							else
							{
								shortestDistance = d;
								nearestVehicle = vehicle;
							}
						}
						catch (Exception ex)
						{
							logger.InfoFormat("Exception", ex.Message.ToString());
							logger.InfoFormat("Vehicle ID:{0}", vehicle);
						}
					}

					if (nearestVehicle != null)
					{
						// get ETA
						// TODO: replace it with the method of MTData
						var rc = new RoutingController(tgvip.tokenVTOD, null);
						var start = new Point { Latitude = tgvip.Latitude, Longitude = tgvip.Longtitude };
						var end = new Point { Longitude = nearestVehicle.Longitude, Latitude = nearestVehicle.Latitude };
						var result = rc.GetRouteMetrics(start, end);
						response.TPA_Extensions.Vehicles.NearestVehicleETA = result.TotalMinutes.ToString();
					}

					response.TPA_Extensions.Vehicles.NumberOfVehicles = vehiclesResult.Vehicles.Count().ToString();
					#endregion

					Random rnd = new Random();

					#region put all vehicle in the response
					foreach (var vehicle in sortedVehicles.Select(item => new Vehicle
					{
						Geolocation = new Geolocation
						{
							Long = decimal.Parse(item.Value.Longitude.ToString(CultureInfo.InvariantCulture)),
							Lat = decimal.Parse(item.Value.Latitude.ToString(CultureInfo.InvariantCulture))
						},
						Distance = item.Key.ToDecimal(),
						ID = item.Value.VehicleNumber,
						Course = rnd.Next(1, 359)
					}))
					{
						response.TPA_Extensions.Vehicles.Items.Add(vehicle);
					}
					#endregion

				}
				else
				{
					// no vehicles found
					logger.Info("Found no vehicles");
					response.TPA_Extensions.Vehicles.NumberOfVehicles = "0";
				}

				#region Write Taxi Log
				if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
				{
					var requestMessage = tgvip.XmlSerialize().ToString();
					var responseMessage = vehiclesResult.XmlSerialize().ToString();

					utility.WriteTaxiLog(null, TaxiServiceAPIConst.MTData, TaxiServiceMethodType.GetVehicleInfo, null,
						requestMessage, responseMessage, requestDateTime, responseDateTime);
				}
				#endregion

			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw new TaxiException(ex.Message);
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_MTData, "GetVehicleInfo");
			#endregion

			return response;
		}

        /// <summary>
		/// Retrieving final route of a trip
		/// </summary>
		/// <param name="tokenVTOD"></param>
		/// <param name="request"></param>
		/// <param name="vtodTXId"></param>
		/// <returns></returns>
		public override OTA_GroundGetFinalRouteRS GetFinalRoute(TokenRS tokenVTOD, OTA_GroundGetFinalRouteRQ request)
        {
            DateTime requestDateTime = DateTime.Now;
            taxi_trip trip = null;
            List<taxi_trip_status> taxi_trip_status = new List<taxi_trip_status>();
            logger.Info("Start");
            Stopwatch sw = new Stopwatch();
            string status = ConfigurationManager.AppSettings["MTData_GetFinalRoute_Status"];
            Int64 tripID ;
            sw.Start();
            var response = new OTA_GroundGetFinalRouteRS();
            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
            #endregion

            try
            {
                if(request.Reference!=null && request.Reference.Any())
                {
                    if (request.Reference.Where(x => x.Type.Equals("Confirmation")).Any())
                    {
                        tripID = Convert.ToInt64(request.Reference.Where(x => x.Type.Equals("Confirmation")).First().ID);
                        #region Retrieve the trip details
                        using (var context = new VTODEntities())
                        {
                            trip = context.taxi_trip.Where(
                                   x =>
                                       x.Id == tripID
                               ).FirstOrDefault();

                        } 
                        #endregion
                    }
                    else if (request.Reference.Where(x => x.Type.Equals("DispatchConfirmation")).Any())
                    {
                        Int64 dispatchtripid = Convert.ToInt64(request.Reference.Where(x => x.Type.Equals("DispatchConfirmation")).First().ID);
                        #region Retrieve the trip details
                        using (var context = new VTODEntities())
                        {
                             trip = context.taxi_trip.Where(
                                    x =>
                                        x.DispatchTripId == dispatchtripid.ToString()
                                ).FirstOrDefault();
                            
                        }
                        #endregion
                    }
                }
                #region Retrieve the trip status details
                using (var context = new VTODEntities())
                {
                    taxi_trip_status = context.taxi_trip_status.Where(x => x.TripID == trip.Id && !status.Contains(x.Status)).OrderBy(p=>p.Id).ToList();
                }
                #endregion
                if (taxi_trip_status!=null && taxi_trip_status.Any())
                {
                    #region Populate the Geolocations
                    response.Geolocations = new List<Geolocation>();
                    foreach (var taxi_status in taxi_trip_status)
                    {
                        Geolocation geo = new Geolocation();
                        geo.Lat = taxi_status.VehicleLatitude.ToDecimal();
                        geo.Long = taxi_status.VehicleLongitude.ToDecimal();
                        response.Geolocations.Add(geo);
                    } 
                    #endregion
                }
                DateTime responseDateTime = DateTime.Now;
                #region Write Taxi Log
                if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
                {
                    var requestMessage = request.ToString();
                    var responseMessage = response.ToString();

                    utility.WriteTaxiLog(null, TaxiServiceAPIConst.MTData, TaxiServiceMethodType.GetFinalRoute, null,
                        requestMessage, responseMessage, requestDateTime, responseDateTime);
                }
                #endregion

            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Message:{0}", ex.Message);
                logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
                logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
                throw new TaxiException(ex.Message);
            }
            sw.Stop();
            logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_MTData, "GetFinalRoute");
            #endregion

            return response;
        }
        /// <summary>
        /// Not implemented
        /// </summary>
        /// <param name="token"></param>
        /// <param name="input"></param>
        
        /// <returns></returns>
        public override bool NotifyDriver(TokenRS token, UtilityNotifyDriverRQ input)
		{
			logger.Info("Start");
			logger.Info("Start MTData for testing Notify Driver");
			var sw = new Stopwatch();
			sw.Start();
			TaxiSendMessageToVehicleParameter tsmtvp;
			string vehicleNumber = string.Empty;
			int driverID = 0;
			int cityID = 0;
            List<string> endpointNames = null;
            if (utility.ValidateMTDataSendMessageToVehicle(token, input, out tsmtvp, out vehicleNumber))
			{
				logger.Info("MTData for testing Notify Driver : ValidateMTDataSendMessageToVehicle completed");
				using (var db = new DataAccess.VTOD.VTODEntities())
				{

					if (tsmtvp.Trip != null)
					{
						var fleetObj = db.taxi_fleet.Where(p => p.Id == tsmtvp.Trip.FleetId).FirstOrDefault();
                       #region Fetcing the Endpoints
                        if (fleetObj != null)
                        {
                            long serviceapiid = db.taxi_fleet_api_reference.Where(p => p.FleetId == tsmtvp.Trip.FleetId).Select(p => p.ServiceAPIId).FirstOrDefault();
                            endpointNames = utility.MtdataEndpointConfiguration(serviceapiid, fleetObj.Id);
                        }
                        #endregion

                        if (fleetObj != null)
						{
							if (fleetObj.DispatchCityCode != null)
								cityID = fleetObj.DispatchCityCode.ToInt32();
							logger.InfoFormat("MTData for testing Notify Driver : fleetId: {0}", fleetObj.CCSiFleetId);
						}
						var statusObj = db.taxi_trip_status.Where(p => p.TripID == tsmtvp.Trip.Id && p.DriverId != null && p.DriverId != string.Empty).OrderByDescending(p => p.Id);
						if (statusObj != null)
						{
							if (statusObj.Any())
							{
								if (!string.IsNullOrEmpty(statusObj.FirstOrDefault().DriverId))
								{
									driverID = statusObj.FirstOrDefault().DriverId.ToInt32();
									logger.InfoFormat("MTData for testing Notify Driver : driverID: {0}", fleetObj.CCSiFleetId);
								}
							}
						}
					}
				}
				if (driverID != 0)
				{
					logger.Info("MTData for testing Notify Driver : Start DoSendDriverMessageForCity");

					var result = DoSendDriverMessageForCity(driverID, tsmtvp.Message, cityID, tsmtvp.Fleet, endpointNames[0], endpointNames[1]);

					logger.InfoFormat("MTData for testing Notify Driver : Result DoSendDriverMessageForCity: {0}", result.XmlSerialize().ToString());
					if (!result.Succeeded)
					{
						string error = result.ErrorMessage;
						logger.Error(error);
						throw new TaxiException(error);
					}
				}
				//if (vehicleNumber != string.Empty)
				//{
				//    var result = DoSendBookingVehicleMessage(fleetId, vehicleNumber, tsmtvp.Message);
				//    if (!result.Succeeded)
				//    {
				//        string error = result.ErrorMessage;
				//        logger.Error(error);
				//        throw new TaxiException(error);
				//    }
				//}
			}
			else
			{
				throw VtodException.CreateException(ExceptionType.Taxi, 9003);
			}


			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_UDI33, "SendMessageToVehicle");
			#endregion

			return true;
		}

		public override bool NotifyPickupDriver(TokenRS token, UtilityNotifyDriverRQ input)
		{
			string message = System.Configuration.ConfigurationManager.AppSettings["DriverPickupMessage"];
			logger.Info("Start");
			logger.Info("Start MTData for testing Notify Driver");
			var sw = new Stopwatch();
			sw.Start();
			TaxiSendMessageToVehicleParameter tsmtvp;
			Int32 fleetId = 0;
			string vehicleNumber = string.Empty;
			int driverID = 0;
			int cityID = 0;
            List<string> endpointNames=null;

            UDI.VTOD.Common.DTO.Enum.PaymentType paymentType = PaymentType.Unknown;
			if (utility.ValidateMTDataPickMessageToVehicle(token, input, out tsmtvp))
			{
				logger.Info("MTData for testing Notify Driver : ValidateMTDataPickMessageToVehicle completed");
				using (var db = new DataAccess.VTOD.VTODEntities())
				{

					if (tsmtvp.Trip != null)
					{
						var fleetObj = db.taxi_fleet.Where(p => p.Id == tsmtvp.Trip.FleetId).FirstOrDefault();
                       #region Fetcing the Endpoints
                        if (fleetObj != null)
                        {
                            long serviceapiid = db.taxi_fleet_api_reference.Where(p => p.FleetId == tsmtvp.Trip.FleetId).Select(p => p.ServiceAPIId).FirstOrDefault();
                             endpointNames = utility.MtdataEndpointConfiguration(serviceapiid, fleetObj.Id);
                        }
                        #endregion
                        if (fleetObj != null)
						{
							fleetId = fleetObj.CCSiFleetId.ToInt32();
							if (fleetObj.DispatchCityCode != null)
								cityID = fleetObj.DispatchCityCode.ToInt32();
							logger.InfoFormat("MTData for testing Notify Driver : fleetId: {0}", fleetId);
						}
						var statusObj = db.taxi_trip_status.Where(p => p.TripID == tsmtvp.Trip.Id && p.DriverId != null && p.DriverId != string.Empty).OrderByDescending(p => p.Id);
						if (statusObj != null)
						{
							if (statusObj.Any())
							{
								if (!string.IsNullOrEmpty(statusObj.FirstOrDefault().DriverId))
								{
									driverID = statusObj.FirstOrDefault().DriverId.ToInt32();
									logger.InfoFormat("MTData for testing Notify Driver : driverID: {0}", fleetId);
								}
							}
						}
						#region Adding Gratuity
						var tripObj = db.vtod_trip.Where(p => p.Id == tsmtvp.Trip.Id).FirstOrDefault();
						if (tripObj != null)
						{
							if (!string.IsNullOrEmpty(tripObj.PaymentType))
							{
								paymentType = (UDI.VTOD.Common.DTO.Enum.PaymentType)Enum.Parse(typeof(UDI.VTOD.Common.DTO.Enum.PaymentType), tripObj.PaymentType);
							}
							if (tripObj.GratuityRate != null)
							{
								var addingText = string.Format("{0}", tripObj.GratuityRate);
								message = message.Replace("{Gratuity}", addingText);
							}
							else
							{
								message = message.Replace("{Gratuity}", "0");
							}
						}
						else
						{
							message = message.Replace("{Gratuity}", "0");
						}
						#endregion
					}
				}

				if (driverID != 0)
				{

					logger.Info("MTData for testing Notify Driver : Start DoSendDriverMessageForCity");
					logger.InfoFormat("Payment Type : {0}", paymentType);
					if (paymentType == PaymentType.PaymentCard)
					{
						if (!string.IsNullOrEmpty(message))
						{

							var result = DoSendDriverMessageForCity(driverID, message, cityID, tsmtvp.Fleet, endpointNames[0], endpointNames[1]);

							logger.InfoFormat("MTData for testing Notify Driver : Result DoSendDriverMessageForCity: {0}", result.XmlSerialize().ToString());
							if (!result.Succeeded)
							{
								string error = result.ErrorMessage;
								logger.Error(error);
								throw new TaxiException(error);
							}
						}
					}
				}
			}
			else
			{
				throw VtodException.CreateException(ExceptionType.Taxi, 9003);
			}

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_UDI33, "SendMessagePickUpToVehicle");
			#endregion

			return true;
		}

        public override bool NotifyDispatchPendingDriver(TokenRS token, UtilityNotifyDriverRQ input)
        {
            string message = System.Configuration.ConfigurationManager.AppSettings["DriverDispatchPendingMessage"];
            logger.Info("Start");
            logger.Info("Start MTData for testing Notify Driver");
            var sw = new Stopwatch();
            sw.Start();
            TaxiSendMessageToVehicleParameter tsmtvp;
            Int32 fleetId = 0;
            string vehicleNumber = string.Empty;
            long bookingID = 0;
            bool driverMessageToBeSend = false;
            List<string> endpointNames = null;
            if (utility.ValidateMTDataDispatchPendingMessageToVehicle(token, input, out tsmtvp))
            {
                logger.Info("MTData for testing Notify Driver : ValidateMTDataDispatchPendingMessageToVehicle completed");
                using (var db = new DataAccess.VTOD.VTODEntities())
                {
                    if (tsmtvp.Trip != null)
                    {
                        var fleetObj = db.taxi_fleet.Where(p => p.Id == tsmtvp.Trip.FleetId).FirstOrDefault();
                        if (fleetObj != null)
                        {
                            fleetId = fleetObj.CCSiFleetId.ToInt32();
                            logger.InfoFormat("MTData for testing Notify Driver : fleetId: {0}", fleetId);
                        }
                        #region Fetcing the Endpoints
                        if (fleetObj != null)
                        {
                            long serviceapiid = db.taxi_fleet_api_reference.Where(p => p.FleetId == tsmtvp.Trip.FleetId).Select(p => p.ServiceAPIId).FirstOrDefault();
                            endpointNames = utility.MtdataEndpointConfiguration(serviceapiid, fleetObj.Id);
                        }
                        #endregion
                        var statusObj = db.taxi_trip_status.Where(p => p.TripID == tsmtvp.Trip.Id && p.DriverId == null).OrderByDescending(p => p.Id);
                        if (statusObj != null)
                        {
                            if (statusObj.Any())
                            {
                                if (statusObj.FirstOrDefault().Status.ToLower() == UDI.VTOD.Domain.Taxi.Const.TaxiTripStatusType.DispatchPending.ToLower())
                                {
                                    driverMessageToBeSend = true;
                                    logger.InfoFormat("MTData for testing Notify Driver For DispatchPending : FleetId:{0}", fleetId);
                                }
                            }
                        }
                        #region Adding Gratuity
                        var tripObj = db.vtod_trip.Where(p => p.Id == tsmtvp.Trip.Id).FirstOrDefault();
                        var taxiTripObj = db.taxi_trip.Where(p => p.Id == tsmtvp.Trip.Id).FirstOrDefault();
                        if (tripObj != null && taxiTripObj != null)
                        {
                            bookingID = taxiTripObj.DispatchTripId.ToInt64();
                            if (tripObj.GratuityRate != null)
                            {
                                var addingText = string.Format("{0}", tripObj.GratuityRate);
                                message = message.Replace("{Gratuity}", addingText);
                            }
                            else
                            {
                                message = message.Replace("{Gratuity}", "0");
                            }
                        }
                        else
                        {
                            message = message.Replace("{Gratuity}", "0");
                        }
                        #endregion
                    }
                }
                if (bookingID != 0)
                {
                    if (driverMessageToBeSend)
                    {

                        logger.Info("MTData for testing Notify Driver : Start DoSendDriverMessageForCity");
                        if (!string.IsNullOrEmpty(message))
                        {

                            var result = DoSendBookingDriverMessage(bookingID, message, tsmtvp.Fleet, endpointNames[0], endpointNames[1]);

                            logger.InfoFormat("MTData for testing Notify Driver : Result DoSendBookingDriverMessage: {0}", result.XmlSerialize().ToString());
                            if (!result.Succeeded)
                            {
                                string error = result.ErrorMessage;
                                logger.Error(error);
                                throw new TaxiException(error);
                            }
                        }
                    }
                }
            }
            else
            {
                throw VtodException.CreateException(ExceptionType.Taxi, 9003);
            }

            sw.Stop();
            logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_UDI33, "SendMessageDispatchPendingToVehicle");
            #endregion

            return true;
        }
		/// <summary>
		/// Retrieving a charge estimate
		/// </summary>
		/// <param name="tokenVTOD"></param>
		/// <param name="request"></param>
		/// <param name="vtodTXId"></param>
		/// <returns></returns>
		public override OTA_GroundAvailRS GetEstimation(TokenRS tokenVTOD, OTA_GroundAvailRQ request)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			Stopwatch swEachPart = new Stopwatch();
			var response = new OTA_GroundAvailRS();

			#region For as directed, return empty ground service
			if (request.Service.Dropoff == null)
			{
				response.Success = new Success();
				response.GroundServices = new GroundServiceList
				{
					GroundServices = new List<GroundService> { new GroundService() }
                
                };
				return response;
			}
			#endregion

			#region Validate required fields
			TaxiGetEstimationParameter tgep;
			if (!utility.ValidateMTDataGetEstimationRequest(tokenVTOD, request, out tgep))
			{
				string error = string.Format("This GetEstimation request is invalid.");
				throw new ValidationException(error);
			}
           
            #endregion

            try
            {
				if (!string.IsNullOrWhiteSpace(tgep.EstimationType) &&
					tgep.EstimationType == EstimationResultType.Info_FlatRate)   // for flat rate, just return the rate from VTOD DB
				{
					#region Build response
					response.Success = new Success();
                    response.GroundServices = new GroundServiceList
                    {
                        GroundServices = new List<GroundService>
                            {
                                new GroundService
                                {
                                  
                                     RateQualifier = new RateQualifier
									{
										Category =
											new Category { Value = "Other_", Code = FareType.FlatRate.ToString() },
										SpecialInputs = new List<NameValue>
										{
											new NameValue { Name = "Pickup", Value = tgep.FlatRateEstimation.PickupZone },
											new NameValue { Name = "DropOff", Value = tgep.FlatRateEstimation.DropoffZone }
										}
									},
									TotalCharge = new TotalCharge
									{
										EstimatedTotalAmount = (decimal)tgep.FlatRateEstimation.EstimationTotalAmount,
										RateTotalAmount = (decimal)tgep.FlatRateEstimation.EstimationTotalAmount
									}
                                   
								}
							}
					};
					#endregion
				}
				else  // non flat rate, call MTData API
				{
					//var fleetID = int.Parse(tgep.Fleet.CCSiFleetId);

					#region convert address to MTData address

					var mtdataAddresses = new List<AddressWS.Address>();
					string mapAddressesErrMsg;

					#region pickup
                    					
					if (tgep.PickupAddressType == AddressType.Airport)
					{
                        // airport code mapping
                        List<string> airportFullNames = utility.GetSynonyms(tgep.PickupAirport);
						
						var quickLocation = DoGetFleetQuickLocations(tgep.Fleet, tgep.AuthenticationServiceEndpointName, tgep.BookingWebServiceEndpointName);
						if (quickLocation.Succeeded && quickLocation.FleetQuickLocationCollection.Any())
						{
							var place = quickLocation.FleetQuickLocationCollection.FirstOrDefault(m => m.PlaceName.ToLower().Trim() == tgep.PickupAirport.ToLower().Trim());

							if (place == null)
							{
								place = quickLocation.FleetQuickLocationCollection.FirstOrDefault(m => airportFullNames.Contains(m.PlaceName.ToLower().Trim()));
							}

							if (place != null)
							{
								var mtdataAddr = new AddressWS.Address
								{
									AddressType = AddressWS.AddressType.Place,
									Place = new AddressWS.Place { ID = place.PlaceID }
								};

								#region Adding lat/lon
								if (tgep.PickupAddress.Geolocation != null && tgep.PickupAddress.Geolocation.Latitude != 0 && tgep.PickupAddress.Geolocation.Longitude != 0)
								{
									mtdataAddr.Latitude = (double)tgep.PickupAddress.Geolocation.Latitude;
									mtdataAddr.Longitude = (double)tgep.PickupAddress.Geolocation.Longitude;
								}
								#endregion

								mtdataAddresses.Add(mtdataAddr);
							}
							else
							{
								string error = string.Format("Cannot find matching airport. message={0}",
									quickLocation.ErrorMessage);
								logger.Error(error);
								throw new TaxiException(error);
							}

						}
						else
						{
							string error = string.Format("Cannot find matching airport. message={0}",
								quickLocation.ErrorMessage);
							logger.Error(error);
							throw new TaxiException(error);
						}
					}
					else
					{
						var mapPickupAddress = ConvertLatLongToAddress(tgep.Fleet,
							(double)tgep.PickupAddress.Geolocation.Latitude,
							(double)tgep.PickupAddress.Geolocation.Longitude, out mapAddressesErrMsg, tgep.AuthenticationServiceEndpointName, tgep.AddressWebServiceEndpointName);

						if (mapPickupAddress == null)
						{
							string error = string.Format("Cannot find matching address. message={0}", mapAddressesErrMsg);
							logger.Error(error);
							throw new TaxiException(error);
						}

						// Since ConvertLatLongToAddress method might change our street number, especially for address range,
						// we replace it with original street number
						if (!string.IsNullOrWhiteSpace(tgep.PickupAddress.StreetNo))
							mapPickupAddress.Number = tgep.PickupAddress.StreetNo;
						mapPickupAddress.Latitude = (double)tgep.PickupAddress.Geolocation.Latitude;
						mapPickupAddress.Longitude = (double)tgep.PickupAddress.Geolocation.Longitude;

						if (System.Configuration.ConfigurationManager.AppSettings["MTData_Force_ZipCode"].ToBool())
						{
							if (mapPickupAddress.Street != null && string.IsNullOrWhiteSpace(mapPickupAddress.Street.Postcode))
							{
								mapPickupAddress.Street.Postcode = tgep.PickupAddress.ZipCode;
							}

							if (mapPickupAddress.Suburb != null && string.IsNullOrWhiteSpace(mapPickupAddress.Suburb.Postcode))
							{
								mapPickupAddress.Suburb.Postcode = tgep.PickupAddress.ZipCode;
							}
						}

						mtdataAddresses.Add(mapPickupAddress);
					}

					#endregion

					#region dropoff

					if (tgep.DropOffAddressType == AddressType.Airport)
					{
                        // airport code mapping
                        List<string> airportFullNames = utility.GetSynonyms(tgep.DropoffAirport);
						
						// get place id of pickup airport
						var quickLocation = DoGetFleetQuickLocations(tgep.Fleet, tgep.AuthenticationServiceEndpointName, tgep.BookingWebServiceEndpointName);
						if (quickLocation.Succeeded && quickLocation.FleetQuickLocationCollection.Any())
						{
							var place = quickLocation.FleetQuickLocationCollection.FirstOrDefault(m => m.PlaceName.ToLower().Trim() == tgep.DropoffAirport.ToLower().Trim());

							if (place == null)
							{
								place = quickLocation.FleetQuickLocationCollection.FirstOrDefault(m => airportFullNames.Contains(m.PlaceName.ToLower().Trim()));
							}

							if (place != null)
							{
								var mtdataAddr = new AddressWS.Address
								{
									AddressType = AddressWS.AddressType.Place,
									Place = new AddressWS.Place { ID = place.PlaceID }
								};

								#region Adding lat/lon
								if (tgep.DropoffAddress.Geolocation != null && tgep.DropoffAddress.Geolocation.Latitude != 0 && tgep.DropoffAddress.Geolocation.Longitude != 0)
								{
									mtdataAddr.Latitude = (double)tgep.DropoffAddress.Geolocation.Latitude;
									mtdataAddr.Longitude = (double)tgep.DropoffAddress.Geolocation.Longitude;
								}
								#endregion

								mtdataAddresses.Add(mtdataAddr);
							}
							else
							{
								string error = string.Format("Cannot find matching airport. message={0}",
									quickLocation.ErrorMessage);
								logger.Error(error);
								throw new TaxiException(error);
							}

						}
						else
						{
							string error = string.Format("Cannot find matching airport. message={0}",
								quickLocation.ErrorMessage);
							logger.Error(error);
							throw new TaxiException(error);
						}
					}
					else if (tgep.DropoffAddress != null)
					{
						var mapDropOffAddress = ConvertLatLongToAddress(tgep.Fleet,
							(double)tgep.DropoffAddress.Geolocation.Latitude,
							(double)tgep.DropoffAddress.Geolocation.Longitude, out mapAddressesErrMsg, tgep.AuthenticationServiceEndpointName, tgep.AddressWebServiceEndpointName);

						if (mapDropOffAddress == null)
						{
							string error = string.Format("Cannot find matching address. message={0}",
								mapAddressesErrMsg);
							logger.Error(error);
							throw new TaxiException(error);
						}

						// Since ConvertLatLongToAddress method might change our street number, especially for address range,
						// we replace it with original street number
						if (!string.IsNullOrWhiteSpace(tgep.DropoffAddress.StreetNo))
							mapDropOffAddress.Number = tgep.DropoffAddress.StreetNo;
						mapDropOffAddress.Latitude = (double)tgep.DropoffAddress.Geolocation.Latitude;
						mapDropOffAddress.Longitude = (double)tgep.DropoffAddress.Geolocation.Longitude;

						if (System.Configuration.ConfigurationManager.AppSettings["MTData_Force_ZipCode"].ToBool())
						{
							if (mapDropOffAddress.Street != null && string.IsNullOrWhiteSpace(mapDropOffAddress.Street.Postcode))
							{
								mapDropOffAddress.Street.Postcode = tgep.DropoffAddress.ZipCode;
							}

							if (mapDropOffAddress.Suburb != null && string.IsNullOrWhiteSpace(mapDropOffAddress.Suburb.Postcode))
							{
								mapDropOffAddress.Suburb.Postcode = tgep.DropoffAddress.ZipCode;
							}
						}

						mtdataAddresses.Add(mapDropOffAddress);
					}

					#endregion

					#endregion

					#region setup request

					var pickupAddress = ConvertAddress(mtdataAddresses[0]);
					var dropoffAddress = ConvertAddress(mtdataAddresses[1]);

					var settings = new ChargeEstimateSettings
					{
						Fleet = new Fleet { ID = Convert.ToInt32(tgep.Fleet.CCSiFleetId) },
						Time = DateTime.Parse(request.Service.Pickup.DateTime),
						PickUpAddressType = pickupAddress.AddressType,
						PickUpAddress = pickupAddress,
						DropOffAddressType = dropoffAddress.AddressType,
						DropOffAddress = dropoffAddress
					};

					#endregion

					swEachPart.Restart();

					var result = DoGetChargeEstimate(settings, tgep.Fleet, tgep.AuthenticationServiceEndpointName, tgep.BookingWebServiceEndpointName);

					swEachPart.Stop();
					logger.DebugFormat("Call MTData API to get estimation. Duration:{0} ms.", sw.ElapsedMilliseconds);

					if (TrackTime != null)
						TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_MTData, "Book_SendingGetEstimationRequest");

					#region handle response

					if (result.Succeeded)
					{
						response.Success = new Success();
						response.GroundServices = new GroundServiceList
						{
							GroundServices = new List<GroundService>
							{
								new GroundService
								{
                                    RateQualifier = new RateQualifier
									{
										Category =
											new Category {Value = "Other_", Code = FareType.Estimation.ToString()}
									},
									TotalCharge = new TotalCharge
									{
										EstimatedTotalAmount = result.ChargeExcludingTolls,
										RateTotalAmount = result.ChargeExcludingTolls
									}
								}
							}
						};

						if (TrackTime != null)
							TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_MTData,
								"Book_SendingGetEstimationRequest");
					}
					else
					{
						logger.Error(result.ErrorMessage);
						throw new TaxiException(result.ErrorMessage);
					}

					#endregion
				}

				#region Write Taxi Log
				utility.WriteTaxiLog(null, TaxiServiceAPIConst.MTData, TaxiServiceMethodType.Estimation, null, null, null, null, null);
				#endregion
			}
			catch (Exception ex)
			{				
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw;
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return response;
		}
		#endregion

		#region IVR methods
		public override UtilityCustomerInfoRS GetCustomerInfo(TokenRS tokenVTOD, UtilityCustomerInfoRQ request)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			Stopwatch swEachPart = new Stopwatch();
			var response = new UtilityCustomerInfoRS();

			try
			{
				//1. validate request
				//-get fleet
				TaxiGetCustomerInfoParameter tgcip = null;
				string MTDataToken = string.Empty;
				if (utility.ValidateMTDataGetCustomerInfo(tokenVTOD, request, out tgcip))
				{
					//2. get token
					RefreshToken(ref MTDataToken, tgcip.Fleet,tgcip.AuthenticationServiceEndpointName);

					//3. send MTData GetTemplate request
					TemplateKeywordType type = TemplateKeywordType.PhoneNumber;
					if (tgcip.request.DispatchRecord.KeyWordName.Equals("PhoneNumber"))
					{
						type = TemplateKeywordType.PhoneNumber;
					}
					else
					{
						type = TemplateKeywordType.Alias;
					}
					using (BookingWebServiceSoapClient client = new BookingWebServiceSoapClient(tgcip.BookingWebServiceEndpointName))
					{
						var result = client.GetTemplate(MTDataToken, Convert.ToInt32(tgcip.Fleet.CCSiFleetId), tgcip.request.DispatchRecord.KeyWordValue, type);

						if (result != null && result.Template != null && result.Template.Locations != null && result.Template.Locations.Where(x => x.LocationType == LocationType.PickUp).Any())
						{
							//4. if response is valid, return customer information back.
							var pickupLocation = result.Template.Locations.Where(x => x.LocationType == LocationType.PickUp).FirstOrDefault();

							#region name
							string firstName = pickupLocation.CustomerName;
							string lastName = string.Empty;
							string[] sep = new string[] { " " };
							if (pickupLocation.CustomerName.Contains(" "))
							{
								List<string> names = pickupLocation.CustomerName.Split(sep, StringSplitOptions.RemoveEmptyEntries).ToList();
								firstName = string.Join(" ", names.Take(names.Count - 1));
								lastName = names.Last();
							}
							#endregion

							#region phone
							string countryCode = string.Empty;
							string areaCode = string.Empty;
							string phone = string.Empty;
							Match phoneMatch = Regex.Match(pickupLocation.CustomerPhoneNumber, @"([\d]{3})([\d]{3})([\d]{4})", RegexOptions.IgnorePatternWhitespace);
							if (phoneMatch.Success)
							{
								countryCode = "1";
								areaCode = phoneMatch.Groups[1].Value;
								phone = string.Format("{0}-{1}", phoneMatch.Groups[2].Value, phoneMatch.Groups[3].Value);
							}
							else
							{
								throw new Exception("Wrong phone format in MTData");
							}
							#endregion

							#region assign name and phone into response
							response.CustomerDetails = new Person
							{
								PersonName = new PersonName
								{
									GivenName = firstName,
									Surname = lastName
								},
								Telephones = new List<Telephone>{
									new Telephone{
										PhoneNumber=phone,
										CountryAccessCode=countryCode,
										AreaCityCode=areaCode
									}
								}
							};
							#endregion

							response.Success = new Success();
						}
						else
						{
							throw VtodException.CreateException(ExceptionType.Taxi, 9101);
						}
					}

				}
				else
				{
					throw VtodException.CreateException(ExceptionType.Taxi, 9151);
				}

				#region Write Taxi Log
				utility.WriteTaxiLog(null, TaxiServiceAPIConst.MTData, TaxiServiceMethodType.GetCustomerInfo, null, null, null, null, null);
				#endregion
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw;
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return response;

		}

		public override UtilityGetPickupAddressRS GetPickupAddress(TokenRS tokenVTOD, UtilityGetPickupAddressRQ request)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			Stopwatch swEachPart = new Stopwatch();
			var response = new UtilityGetPickupAddressRS();

			try
			{
				//1. validate request
				//-get fleet
				TaxiGetPickupAddressParameter tgpap = null;
				string MTDataToken = string.Empty;
				if (utility.ValidateMTDataGetPickupAddress(tokenVTOD, request, out tgpap))
				{
					//2. get token
					RefreshToken(ref MTDataToken, tgpap.Fleet, tgpap.AuthenticationServiceEndpointName);

					//3. send MTData GetTemplate request
					TemplateKeywordType type = TemplateKeywordType.PhoneNumber;
					if (tgpap.request.DispatchRecord.KeyWordName.Equals("PhoneNumber"))
					{
						type = TemplateKeywordType.PhoneNumber;
					}
					else
					{
						type = TemplateKeywordType.Alias;
					}
					using (BookingWebServiceSoapClient client = new BookingWebServiceSoapClient(tgpap.BookingWebServiceEndpointName))
					{
						var result = client.GetTemplate(MTDataToken, Convert.ToInt32(tgpap.Fleet.CCSiFleetId), tgpap.request.DispatchRecord.KeyWordValue, type);

                        if (result != null && result.Template != null && result.Template.Locations != null && result.Template.Locations.Any() && result.Template.Locations.Where(x => x.LocationType == LocationType.PickUp).Any())
                        {
							//4. if response is valid, return pickup address back.
							var pickupLocations = result.Template.Locations.Where(x => x.LocationType == LocationType.PickUp).ToList();
							response.Addresses = new List<Common.DTO.OTA.Address>();
							foreach (var l in pickupLocations)
							{
								var a = new Common.DTO.OTA.Address
								{
									//AddressName=l.Address.Place.Name,
									StreetNmbr = l.Address.Number,
									StreetName = l.Address.Street.Name,
									StreetSuffix = l.Address.Designation.Name,

									Latitude = l.Address.Latitude.Value.ToString(),
									Longitude = l.Address.Longitude.Value.ToString(),
									LocationType = new Common.DTO.OTA.LocationType { Value = "Pickup" },
									Unit = l.Address.Unit,
									//City=l.Address.CityName
									CityName = l.Address.Suburb.Name,
									LocationName = l.Address.Place.Name

								};


								#region Get Geolocation from MapQuest
								//MapService ms = new MapService();
								//string errorMessage = "";
								//var addresses = ms.GetAddresses(string.Format("{0} {1} {2} {3}", l.Address.Number, l.Address.Street.Name, l.Address.Designation.Name, l.Address.Suburb.Name), out errorMessage);
								//if ("0".Equals(a.Latitude) && "0".Equals(a.Longitude) && addresses != null && addresses.Any())
								//{
								//    a.Latitude = addresses.FirstOrDefault().Geolocation.Latitude.ToString();
								//    a.Longitude = addresses.FirstOrDefault().Geolocation.Longitude.ToString();
								//}
								#endregion

								response.Addresses.Add(a);

							}



							response.Success = new Success();
						}
						else
						{
							throw VtodException.CreateException(ExceptionType.Taxi, 9102);
						}
					}


				}
				else
				{
					throw VtodException.CreateException(ExceptionType.Taxi, 9152);
				}


				#region Write Taxi Log
				utility.WriteTaxiLog(null, TaxiServiceAPIConst.MTData, TaxiServiceMethodType.GetPickupAddress, null, null, null, null, null);
				#endregion
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw;
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return response;

		}

		public override UtilityGetLastTripRS GetLastTrip(TokenRS tokenVTOD, UtilityGetLastTripRQ request)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			Stopwatch swEachPart = new Stopwatch();
			var response = new UtilityGetLastTripRS();

			try
			{
				//1. validate request
				//-get fleet
				TaxiGetLastTripParameter tgltp = null;
				string MTDataToken = string.Empty;
				if (utility.ValidateMTDataGetLastTrip(tokenVTOD, request, out tgltp))
				{
					//2. get token
					RefreshToken(ref MTDataToken, tgltp.Fleet, tgltp.AuthenticationServiceEndpointName);

					//3. send MTData GetLastBooking request
					TemplateKeywordType type = TemplateKeywordType.PhoneNumber;


					using (BookingWebServiceSoapClient client = new BookingWebServiceSoapClient(tgltp.BookingWebServiceEndpointName))
					{
						string phone = string.Format("{0}{1}", tgltp.request.Telephone.AreaCityCode, tgltp.request.Telephone.PhoneNumber.Replace("-", ""));
						var result = client.GetLastBooking(MTDataToken, Convert.ToInt32(tgltp.Fleet.CCSiFleetId), phone, type, Convert.ToInt32(request.FromMinutesInPast));

						if (result != null && result.BookingSummary != null)
						{
                            if (result.BookingSummary.BookingID == 0) {
                                throw VtodException.CreateException(ExceptionType.Taxi, 9108);
                            }

							var resultBooking = client.GetBooking(MTDataToken, result.BookingSummary.BookingID);

							if (resultBooking != null && resultBooking.Succeeded )
							{
								//5. if response is valid, return the information back.
								response.BookingDetails = new BookingDetails
								{
									BookingChannel = result.BookingSummary.BookingChannel.ToString(),
									VehicleId = result.BookingSummary.VehicleID.ToString(),
									Time = result.BookingSummary.Time.ToString("yyyy-MM-ddTHH:mm:ss"),
									//StatusCode=result.BookingSummary.
									BookingId = result.BookingSummary.BookingID,
									Status = utility.ConvertTripStatusForMTData(result.BookingSummary.Status),
									PickupAddress = new Common.DTO.OTA.Address
									{
										StreetName = resultBooking.BookingSummary.PickupStreetName,
										StreetNmbr = resultBooking.BookingSummary.PickupStreetNumber,
										CityName = resultBooking.BookingSummary.PickupSuburb,
										StreetSuffix = resultBooking.BookingSummary.PickupDesignation,
										LocationName = (!string.IsNullOrWhiteSpace(resultBooking.BookingSummary.PickupPlaceName)) ? resultBooking.BookingSummary.PickupPlaceName : string.Empty,
										LocationType = new Common.DTO.OTA.LocationType { Value = "Pickup" }
									}
								};


								#region Get Geolocation from MapQuest
								//MapService ms = new MapService();
								//string errorMessage = "";
								//var addresses = ms.GetAddresses(string.Format("{0} {1} {2} {3}", resultBooking.BookingSummary.PickupStreetNumber, resultBooking.BookingSummary.PickupStreetName, resultBooking.BookingSummary.PickupDesignation, resultBooking.BookingSummary.PickupSuburb), out errorMessage);
								//if (addresses != null && addresses.Any())
								//{
								//    response.BookingDetails.PickupAddress.Latitude = addresses.FirstOrDefault().Geolocation.Latitude.ToString();
								//    response.BookingDetails.PickupAddress.Longitude = addresses.FirstOrDefault().Geolocation.Longitude.ToString();
								//}
								#endregion

								response.Success = new Success();
							}
							else
							{
								throw VtodException.CreateException(ExceptionType.Taxi, 9108);
							}
						}
						else
						{
							throw VtodException.CreateException(ExceptionType.Taxi, 9103);
						}

					}
				}
				else
				{
					throw VtodException.CreateException(ExceptionType.Taxi, 9153);
				}


				#region Write Taxi Log
				utility.WriteTaxiLog(null, TaxiServiceAPIConst.MTData, TaxiServiceMethodType.GetPickupAddress, null, null, null, null, null);
				#endregion
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw;
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return response;

		}


		public override UtilityGetBlackoutPeriodRS GetBlackoutPeriod(TokenRS token, UtilityGetBlackoutPeriodRQ input)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			Stopwatch swEachPart = new Stopwatch();
			var response = new UtilityGetBlackoutPeriodRS();

			try
			{
				//1. validate request
				//-get fleet
				TaxiGetBlackoutPeriodParameter tgbpp = null;
				string MTDataToken = string.Empty;
				if (utility.ValidateMTDataGetBlackoutPeriod(token, input, out tgbpp))
				{
					//2. get token
					RefreshToken(ref MTDataToken, tgbpp.Fleet, tgbpp.AuthenticationServiceEndpointName);

					//3. Call BookingChannelGetExceptionDates
					using (BookingWebServiceSoapClient client = new BookingWebServiceSoapClient(tgbpp.BookingWebServiceEndpointName))
					{
						var result = client.BookingChannelGetExceptionDates(MTDataToken);

						if (result != null && result.ExceptionDates.Where(x => x.FleetID == Convert.ToInt32(tgbpp.Fleet.CCSiFleetId)).Any())
						{
							//4. if response is valid, return blackout dates
							response.BlackoutPeriods = result.ExceptionDates.Where(x => x.FleetID == Convert.ToInt32(tgbpp.Fleet.CCSiFleetId)).Select(x => new BlackoutPeriod
							{
								CityName = x.CityName,
								StartTime = x.ExceptionDate.AddMinutes(x.StartTime).ToString("yyyy-MM-ddTHH:mm:ss"),
								EndTime = x.ExceptionDate.AddMinutes(x.EndTime).ToString("yyyy-MM-ddTHH:mm:ss"),
								Reason = x.ExceptionDateName,
								Message = x.Message

							}).ToList();



							response.Success = new Success();
						}
						else
						{
							throw VtodException.CreateException(ExceptionType.Taxi, 9104);
						}
					}

				}
				else
				{
					throw VtodException.CreateException(ExceptionType.Taxi, 9154);
				}

				#region Write Taxi Log
				utility.WriteTaxiLog(null, TaxiServiceAPIConst.MTData, TaxiServiceMethodType.GetBlackoutPeriod, null, null, null, null, null);
				#endregion
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw;
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return response;
		}

		public override UtilityGetNotificationRS GetStaleNotification(TokenRS token, UtilityGetNotificationRQ input)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			Stopwatch swEachPart = new Stopwatch();
			var response = new UtilityGetNotificationRS();

			try
			{
				//1. validate request
				//-get fleet
				TaxiGetNotificationParameter tgnp = null;
				string MTDataToken = string.Empty;
				if (utility.ValidateMTDataGetNotification(token, input, out tgnp))
				{
					//2. get token
					RefreshToken(ref MTDataToken, tgnp.Fleet, tgnp.AuthenticationServiceEndpointName);

					//3. Call GetStaleBookings
					using (BookingWebServiceSoapClient client = new BookingWebServiceSoapClient(tgnp.BookingWebServiceEndpointName))
					{
						var result = client.GetStaleBookings(MTDataToken, input.minutesDispatching);

						if (result != null && result.ActiveBookings != null && result.ActiveBookings.Any())
						{
							//4. if response is valid, return notifications back
							response.Notifications = result.ActiveBookings.Select(x => new OutboundNotification
							{
								CustomerName = x.CustomerName,
								PhoneNumber = x.CustomerPhone,
								TripDateTime = x.Time.ToString("yyyy-MM-ddTHH:mm:ss"),
								Type = OutboundNotificationType.StaleNotification,
								BookingTripId = x.BookingID.ToString(),
								VehicleNumber = x.VehicleID.ToString()
							}).ToList();

							response.Success = new Success();
						}
						else
						{
							throw VtodException.CreateException(ExceptionType.Taxi, 9105);
						}
					}

				}
				else
				{
					throw VtodException.CreateException(ExceptionType.Taxi, 9155);
				}

				#region Write Taxi Log
				utility.WriteTaxiLog(null, TaxiServiceAPIConst.MTData, TaxiServiceMethodType.GetBlackoutPeriod, null, null, null, null, null);
				#endregion
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw;
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return response;
		}

		public override UtilityGetNotificationRS GetNightBeforeReminder(TokenRS token, UtilityGetNotificationRQ input)
		{
			throw new NotImplementedException();
		}

		public override UtilityGetNotificationRS GetArrivalNotification(TokenRS token, UtilityGetNotificationRQ input)
		{
			throw new NotImplementedException();
		}



		#endregion

		#region private methods

		/// <summary>
		/// Call ConvertLatLongToAddress method of MTData
		/// </summary>
		/// <param name="fleet"></param>
		/// <param name="latitude"></param>
		/// <param name="longitude"></param>
		/// <param name="errorMsg"></param>
		/// <returns></returns>
		private AddressWS.Address ConvertLatLongToAddress(taxi_fleet fleet, double latitude, double longitude, out string errorMsg, string AuthenticateServiceEndpointName, string AddressServiceEndpointName)
		{
			var currentToken = Token;
			//if (string.IsNullOrWhiteSpace(currentToken))
			RefreshToken(ref currentToken, fleet, AuthenticateServiceEndpointName);

			using (var client = new AddressWS.AddressWebServiceSoapClient(AddressServiceEndpointName))
			{
				var setting = new AddressWS.ConvertLatLongToAddressSettings
				{
					FleetID = Convert.ToInt32(fleet.CCSiFleetId),
					Latitude = latitude,
					Longitude = longitude
				};

				var result = client.ConvertLatLongToAddress(currentToken, setting);
				if (result.Succeeded || result.Error != AddressWS.ConvertLatLongToAddressError.NotAuthenticated)
				{
					errorMsg = result.ErrorMessage;
					return result.Succeeded ? result.Address : null;
				}

				// retry if token is expired
				RefreshToken(ref currentToken, fleet, AuthenticateServiceEndpointName);
				result = client.ConvertLatLongToAddress(currentToken, setting);

				errorMsg = result.ErrorMessage;
				return result.Succeeded ? result.Address : null;
			}
		}

		private vtod_driver_info MapOSIDriverResultToVtod_driver_info(OSIDriverResult oSIDriverResult)
		{
			vtod_driver_info result = null;

			if (oSIDriverResult != null)
			{
				var newDriver = new vtod_driver_info();

				newDriver.DriverID = oSIDriverResult.Driver.DriverID.ToString();
                newDriver.DriverNumber = oSIDriverResult.Driver.DriverNumber;
                newDriver.FirstName = oSIDriverResult.Driver.DriverName;
				newDriver.LastName = oSIDriverResult.Driver.DriverSurname;
				newDriver.CellPhone = oSIDriverResult.Driver.MobilePhone;
				newDriver.HomePhone = oSIDriverResult.Driver.HomePhone;
				newDriver.StartDate = oSIDriverResult.Driver.StartDate.ToDateTime();
				newDriver.FleetType = Common.DTO.Enum.FleetType.Taxi.ToString();
				newDriver.AppendDate = System.DateTime.Now;

				//Below properties are not provided by OSIDriverResult

				//newDriver.VehicleID = row["VehicleID"].ToString();
				//newDriver.AlternatePhone = row["AlternatePhone"].ToString();
				//newDriver.QualificationDate = row["QualificationDate"].ToDateTime();
				//newDriver.zTripQualified = row["zTripQualified"].ToBool();
				//newDriver.HrDays = row["HrDays"].ToString();
				//newDriver.LeaseDate = row["LeaseDate"].ToDateTime();

				result = newDriver;

				#region Log
				logger.InfoFormat("Driver Found: ID:{0}, VehicleID:{1}", newDriver.DriverID, newDriver.VehicleID);
				#endregion
			}

			return result;
		}

		#region Do not delete
		/*
		/// <summary>
		/// Call MapAddresses method of MTData
		/// </summary>
		/// <param name="fleetID"></param>
		/// <param name="addresses"></param>
		/// <param name="errorMsg"></param>
		/// <returns></returns>
		private List<AddressWS.Address> MapAddresses(int fleetID, IEnumerable<Address> addresses,
			out string errorMsg)
		{
			var textAddresses = new List<AddressWS.TextAddress>();
			textAddresses.AddRange(addresses.Select(addr => new AddressWS.TextAddress
			{
				Number = addr.StreetNo,
				StreetName = addr.Street,
				Locality1 = addr.City,
				AdminLevel1 = addr.State,
				PostCode = addr.ZipCode,
				GEOLocation = new AddressWS.GEOLocation
				{
					Latitude = (double)addr.Geolocation.Latitude,
					Longitude = (double)addr.Geolocation.Longitude
				}
			}));

			return MapAddresses(fleetID, textAddresses, out errorMsg);
		}

		/// <summary>
		/// Call MapAddresses method of MTData
		/// </summary>
		/// <param name="fleetID"></param>
		/// <param name="addresses"></param>
		/// <param name="errorMsg"></param>
		/// <returns></returns>
		private List<AddressWS.Address> MapAddresses(int fleetID, IEnumerable<AddressWS.TextAddress> addresses, 
			out string errorMsg)
		{
			errorMsg = string.Empty;

			var currentToken = Token;
			if (string.IsNullOrWhiteSpace(currentToken))
				RefreshToken(ref currentToken);

			var mappedAddresses = new List<AddressWS.Address>();

			using (var client = new AddressWS.AddressWebServiceSoapClient())
			{
				var request = new AddressWS.MappedAddressRequest
				{
					FleetID = fleetID,
					Addresses = addresses.ToArray()
				};

				var result = client.MapAddresses(currentToken, request);
				if (result.Succeeded)
				{
					foreach (var addr in result.Addresses)
					{
						if (addr.ErrorCode == AddressWS.MappedAddressResultCode.Success)
						{
							mappedAddresses.Add(addr.Address);
						}
						else
						{
							errorMsg = addr.ErrorCode.ToString();
							return null;
						}
					}

					return mappedAddresses;
				}
				if (result.ErrorCode != AddressWS.MappedAddressErrorCode.NotAuthenticated)
				{
					errorMsg = result.ErrorMessage;
					return null;
				}

				// retry if token is expired
				RefreshToken(ref currentToken);
				result = client.MapAddresses(currentToken, request);
				if (result.Succeeded)
				{
					foreach (var addr in result.Addresses)
					{
						if (addr.ErrorCode == AddressWS.MappedAddressResultCode.Success)
						{
							mappedAddresses.Add(addr.Address);
						}
						else
						{
							errorMsg = addr.ErrorCode.ToString();
							return null;
						}
					}
					return mappedAddresses;
				}

				errorMsg = result.ErrorMessage;
				return null;
			}
		}
		 * */
		#endregion

		/// <summary>
		/// Convert AddressWebService Address object to BookingWebService Address object
		/// </summary>
		/// <param name="address"></param>
		/// <returns></returns>
		private UDI.VTOD.MTData.BookingWebService.Address ConvertAddress(AddressWS.Address address)
		{
			var result = new UDI.VTOD.MTData.BookingWebService.Address();

			result.Designation = address.Designation == null ? null : new Designation
			{
				ID = address.Designation.ID,
				Name = address.Designation.Name,
				Postcode = address.Designation.Postcode
			};

			result.Number = address.Number;
			result.Place = address.Place == null ? null : new Place
			{
				ID = address.Place.ID,
				Name = address.Place.Name
			};
			result.Position = new Position();
			if (address.Position != null)
			{
				result.Position.ID = address.Position.ID;
				result.Position.Latitude = address.Position.Latitude;
				result.Position.Longitude = address.Position.Longitude;
			}
			else if (address.Latitude.HasValue && address.Longitude.HasValue)
			{
				//result.Position.ID = address.Position.ID;
				result.Position.Latitude = (float)address.Latitude;
				result.Position.Longitude = (float)address.Longitude;
			}
			//result.Position = address.Position == null ? null : new Position
			//{
			//	ID = address.Position.ID,
			//	Latitude = address.Position.Latitude,
			//	Longitude = address.Position.Longitude
			//};
			result.Street = address.Street == null ? null : new Street
			{
				ID = address.Street.ID,
				Name = address.Street.Name,
				Postcode = address.Street.Postcode
			};
			result.Suburb = address.Suburb == null ? null : new Suburb
			{
				ID = address.Suburb.ID,
				Name = address.Suburb.Name,
				Postcode = address.Suburb.Postcode
			};
			result.CityName = address.CityName;
			result.Latitude = address.Latitude;
			result.Longitude = address.Longitude;
			result.Unit = address.Unit;
			result.AddressType = (UDI.VTOD.MTData.BookingWebService.AddressType)address.AddressType;
			//result.ForceAddress = address.ForceAddress;
			if (address.AddressType != UDI.VTOD.MTData.AddressWebService.AddressType.Place)
			{
				result.ForceAddress = true;
			}

			return result;
		}

        /// <summary>
		/// CalculateFare with OSIBooking
		/// </summary>
		/// <param name="booking"></param>
		/// <returns></returns>
        private decimal CalculateFare(OSIBooking booking)
        {
            if (booking.IsFixedPrice)
            {
                return booking.FixedPrice;
            }                        
                        
            return booking.Price + booking.Extras + booking.FlagFall + booking.ServiceFee + booking.Tolls - booking.Discount;            
        }

        /// <summary>
        /// Call ImportBooking method of MTData
        /// </summary>
        /// <param name="request"></param>
        /// <param name="fleet"></param>
        /// <param name="condition"></param>
        /// <returns></returns>
        private ImportBookingResult DoImportBooking(ImportBookingSettings request, taxi_fleet fleet,int condition,TaxiBookingParameter tbp)
		{
			var currentToken = Token;
            BookingCondition bCondition = new BookingCondition();
            bCondition.ID = condition;
            if (condition>0)
            {
                request.Booking.Conditions = new BookingCondition[1];
                request.Booking.Conditions[0] = bCondition;
            }

           string debug_request_string = request.XmlSerialize().ToString();
			logger.InfoFormat("MTData booking request={0}", debug_request_string);
			RefreshToken(ref currentToken, fleet,tbp.AuthenticationServiceEndpointName);
            using (var client = new BookingWebServiceSoapClient(tbp.BookingWebServiceEndpointName))
           {
				var result = client.ImportBooking(currentToken, request);

				if (result.Succeeded || result.Error != ODIError.NotAuthenticated) return result;

				// retry if token is expired
				RefreshToken(ref currentToken, fleet, tbp.AuthenticationServiceEndpointName);

				result = client.ImportBooking(currentToken, request);

				return result;
			}
		}

        /// <summary>
        /// Call GetBooking method of MTData
        /// </summary>
        /// <param name="bookingID"></param>
        /// <param name="taxiStatusParameter"></param>
        /// <returns></returns>
        private GetBookingResult DoGetBooking(long bookingID, TaxiStatusParameter taxiStatusParameter)
		{
			var currentToken = Token;
			RefreshToken(ref currentToken, taxiStatusParameter.Fleet, taxiStatusParameter.AuthenticationServiceEndpointName);

			using (var client = new BookingWebServiceSoapClient(taxiStatusParameter.BookingWebServiceEndpointName))
			{
				var result = client.GetBooking(currentToken, bookingID);
				if (result.Succeeded || result.Error != GetBookingError.NotAuthenticated) return result;

				// retry if token is expired
				RefreshToken(ref currentToken, taxiStatusParameter.Fleet, taxiStatusParameter.AuthenticationServiceEndpointName);
				result = client.GetBooking(currentToken, bookingID);

				return result;
			}
		}

        private OSIDriverResult DoGetOSIDriverResultByID(int driverId, TaxiStatusParameter taxiStatusParameter)
        {
            var currentToken = Token;
            RefreshToken(ref currentToken, taxiStatusParameter.Fleet, taxiStatusParameter.AuthenticationServiceEndpointName);

            using (OSIWebServiceSoapClient client = new OSIWebServiceSoapClient(taxiStatusParameter.OsiWebServiceEndpointName))
            {
                OSIDriverResult result = client.GetDriverByID(currentToken, driverId);
                if (result.Succeeded || result.Error != OSIDriverError.NotAuthenticated) return result;

                // retry if token is expired
                RefreshToken(ref currentToken, taxiStatusParameter.Fleet, taxiStatusParameter.AuthenticationServiceEndpointName);
                result = client.GetDriverByID(currentToken, driverId);

                return result;
            }
        }

        /// <summary>
        /// Call GetBooking method of MTData
        /// </summary>
        /// <param name="bookingID"></param>
        /// <param name="fleet"></param>
        /// <returns></returns>
        private GetBookingFullResult DoGetBookingFull(long bookingID, taxi_fleet fleet, string AuthenticateWebServiceEndpointName, string BookingWebServiceEndpointName,string systemID)
        {
            var currentToken = Token;
            RefreshToken(ref currentToken, fleet, AuthenticateWebServiceEndpointName);

            using (var client = new BookingWebServiceSoapClient(BookingWebServiceEndpointName))
            {
                var result = client.GetBookingFull(currentToken, bookingID, systemID);
                if (result.Succeeded || result.Error != ODIError.NotAuthenticated) return result;

                // retry if token is expired
                RefreshToken(ref currentToken, fleet, AuthenticateWebServiceEndpointName);
                result = client.GetBookingFull(currentToken, bookingID, systemID);

                return result;
            }
        }
        /// <summary>
        /// Call GetBooking method of MTData
        /// </summary>
        /// <param name="bookingID"></param>
        /// <param name="taxiStatusParameter"></param>
        /// <returns></returns>
        private OSIBookingResult DoGetBookingByID(long bookingID, TaxiStatusParameter taxiStatusParameter)
        {
			var currentToken = Token;
			if (string.IsNullOrWhiteSpace(currentToken))
				RefreshToken(ref currentToken, taxiStatusParameter.Fleet , taxiStatusParameter.AuthenticationServiceEndpointName);

			using (var client = new OSIWebServiceSoapClient(taxiStatusParameter.OsiWebServiceEndpointName))
			{
				var result = client.GetBookingByID(currentToken, bookingID);
				if (result.Succeeded || result.Error != OSIBookingError.NotAuthenticated) return result;

				// retry if token is expired
				RefreshToken(ref currentToken, taxiStatusParameter.Fleet, taxiStatusParameter.AuthenticationServiceEndpointName);
				result = client.GetBookingByID(currentToken, bookingID);

				return result;
			}
		}

		/// <summary>
		/// Call CancelBooking method of MTData
		/// </summary>
		/// <param name="bookingID"></param>
		/// <param name="fleet"></param>
		/// <returns></returns>
		private CancelBookingResult DoCancelBooking(long bookingID, taxi_fleet fleet,TaxiCancelParameter tcp)
		{
			var currentToken = Token;
            RefreshToken(ref currentToken, fleet,tcp.AuthenticationServiceEndpointName);

			using (var client = new BookingWebServiceSoapClient(tcp.BookingWebServiceEndpointName))
			{
				var result = client.CancelBooking(currentToken, bookingID);
				if (result.Succeeded || result.Error != CancelBookingError.NotAuthenticated) return result;

				// retry if token is expired
				RefreshToken(ref currentToken, fleet,tcp.AuthenticationServiceEndpointName);
				result = client.CancelBooking(currentToken, bookingID);

				return result;
			}
		}

        /// <summary>
        /// Call GetFleetVacantVehiclesForBookingLocation method of MTData
        /// </summary>
        /// <param name="fleet"></param>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <param name="radius"></param>
        /// <param name="condition"></param>
        /// <returns></returns>
        private GetFleetVacantVehiclesResult DoGetFleetVacanVehiclesForBookingLocation(
			taxi_fleet fleet, double latitude, double longitude, float radius,int condition,string AuthenticateServiceEndpointName,string BookingServiceEndpointName)
		{
			var currentToken = Token;
			RefreshToken(ref currentToken, fleet, AuthenticateServiceEndpointName);

			var location = new BookingLocationInfo
			{
				FleetId = Convert.ToInt32(fleet.CCSiFleetId),
				Latitude = latitude,
				Longitude = longitude,
				Radius = radius
               
            };
            if (condition > 0)
            {
                location.VehicleConditions = new int[1];
                location.VehicleConditions[0] = condition;
            }

            using (var client = new BookingWebServiceSoapClient(BookingServiceEndpointName))
			{
				var result = client.GetFleetVacantVehiclesForBookingLocation(currentToken, location);
				if (result.Succeeded || result.Error != ODIError.NotAuthenticated) return result;

				// retry if token is expired
				RefreshToken(ref currentToken, fleet, AuthenticateServiceEndpointName);
				result = client.GetFleetVacantVehiclesForBookingLocation(currentToken, location);

				return result;
			}
		}

		/// <summary>
		/// Call GetChargeEstimate method of MTData
		/// </summary>
		/// <param name="settings"></param>
		/// <param name="fleet"></param>
		/// <returns></returns>
		private GetChargeEstimateResult DoGetChargeEstimate(ChargeEstimateSettings settings, taxi_fleet fleet, string AuthenticateServiceEndpointName, string BookingServiceEndpointName)
		{
			var currentToken = Token;
			RefreshToken(ref currentToken, fleet, AuthenticateServiceEndpointName);

			using (var client = new BookingWebServiceSoapClient(BookingServiceEndpointName))
			{
				var result = client.GetChargeEstimate(currentToken, settings);
				if (result.Succeeded || result.Error != GetChargeEstimateError.NotAuthenticated) return result;

				// retry if token is expired
				RefreshToken(ref currentToken, fleet, AuthenticateServiceEndpointName);
				result = client.GetChargeEstimate(currentToken, settings);

				return result;
			}
		}

		/// <summary>
		/// Call GetQuickLocationFleet method of MTData
		/// </summary>
		/// <param name="fleet"></param>
		/// <returns></returns>
		private GetFleetQuickLocationsResult DoGetFleetQuickLocations(taxi_fleet fleet, string AuthenticateServiceEndpointName, string BookingServiceEndpointName)
		{
			var currentToken = Token;
			RefreshToken(ref currentToken, fleet, AuthenticateServiceEndpointName);

			using (var client = new BookingWebServiceSoapClient(BookingServiceEndpointName))
			{
				var result = client.GetQuickLocationFleet(currentToken, Convert.ToInt32(fleet.CCSiFleetId));
                logger.InfoFormat("MTData:Start:DoGetFleetQuickLocations:{0}", result);
                if (result.Succeeded || result.Error != GetQuickLocationsError.NotAuthenticated) return result;

				// retry if token is expired
				RefreshToken(ref currentToken, fleet, AuthenticateServiceEndpointName);
				result = client.GetQuickLocationFleet(currentToken, Convert.ToInt32(fleet.CCSiFleetId));
                logger.InfoFormat("MTData:Start:DoGetFleetQuickLocations:{0}", result);
                return result;
			}
		}

		/// <summary>
		/// Send message to the driver of a booking
		/// </summary>
		/// <param name="bookingID"></param>
		/// <param name="messageText"></param>
		/// <param name="fleet"></param>
		/// <returns></returns>
		private SendBookingDriverMessageResult DoSendBookingDriverMessage(long bookingID, string messageText, taxi_fleet fleet, string AuthenticateServiceEndpointName, string BookingServiceEndpointName)
		{
            logger.InfoFormat("MTData:Start:DoSendBookingDriverMessage:bookingID:{0}, message:{1}", bookingID, messageText);

			var currentToken = Token;

            logger.InfoFormat("MTData:Start:DoSendBookingDriverMessage:Token:{0}", Token.ToString());
            RefreshToken(ref currentToken, fleet, AuthenticateServiceEndpointName);
            logger.InfoFormat("MTData:Start:DoSendBookingDriverMessage:RefereshToken:{0}", Token.ToString());
            

			using (var client = new BookingWebServiceSoapClient(BookingServiceEndpointName))
			{
				var result = client.SendBookingDriverMessage(currentToken, bookingID, messageText);
                if (result.Succeeded || result.Error != ODIError.NotAuthenticated)
                {
                    logger.InfoFormat("MTData:Response:SendBookingDriverMessage:{0}", result.XmlSerialize().ToString());
                    return result;
                }

				// retry if token is expired
				RefreshToken(ref currentToken, fleet, AuthenticateServiceEndpointName);
				result = client.SendBookingDriverMessage(currentToken, bookingID, messageText);
                logger.InfoFormat("MTData:Response:SendBookingDriverMessage:{0}", result.XmlSerialize().ToString());
				return result;
			}
		}

		/// <summary>
		/// Send message to the Vehicle 
		/// </summary>
		/// <param name="fleet"></param>
		///  <param name="carNumber"></param>
		/// <param name="messageText"></param>
		/// <returns></returns>
		private SendVehicleMessageResult DoSendBookingVehicleMessage(taxi_fleet fleet, string carNumber, string messageText, string AuthenticateServiceEndpointName, string BookingServiceEndpointName)
		{
			var currentToken = Token;
			RefreshToken(ref currentToken, fleet, AuthenticateServiceEndpointName);

			using (var client = new BookingWebServiceSoapClient(BookingServiceEndpointName))
			{
				var result = client.SendVehicleMessage(currentToken, Convert.ToInt32(fleet.CCSiFleetId), carNumber, messageText);
				if (result.Succeeded || result.Error != ODIError.NotAuthenticated) return result;
				RefreshToken(ref currentToken, fleet, AuthenticateServiceEndpointName);
				result = client.SendVehicleMessage(currentToken, Convert.ToInt32(fleet.CCSiFleetId), carNumber, messageText);
				return result;
			}
		}
        /// <summary>
		/// Call GetFleet method of MTData
		/// </summary>
		/// <param name="fleet"></param>
        /// <param name="authenticateEndpoint"></param>
        /// <param name="bookingEndpoint"></param>
		/// <returns></returns>
		private GetFleetsResult DoGetFleet(taxi_fleet fleet, string AuthenticateServiceEndpointName, string BookingServiceEndpointName)
        {
            var currentToken = Token;
            RefreshToken(ref currentToken, fleet, AuthenticateServiceEndpointName);

            using (var client = new BookingWebServiceSoapClient(BookingServiceEndpointName))
            {
                var result = client.GetFleets(currentToken);
                if (result.Succeeded || result.Error != GetFleetsError.NotAuthenticated) return result;

                // retry if token is expired
                RefreshToken(ref currentToken, fleet, AuthenticateServiceEndpointName);
                result = client.GetFleets(currentToken);

                return result;
            }
        }

        /// <summary>
        /// Call Authenticate method to get a new token 
        /// </summary>
        /// <param name="currentToken"></param>
        /// <param name="fleet"></param>
        /// <param name="endpointName"></param>
        /// <returns></returns>
        private void RefreshToken(ref string currentToken, taxi_fleet fleet,string endpointName)
		{
			lock (ObjSync)
			{
				if (currentToken == Token)
				{
                    using (var client = new AuthenticationWebServiceSoapClient(endpointName))
                    {
                        AuthenticationResult authResult = null;
                        if (fleet.IVREnable)
                        {
                            //IVR trip
                            authResult = client.Authenticate(BookingChannelType.SolidusIvr, IVR_UserName, IVR_Password);
                        }
                        else
                        {
                            //zTrip
                            authResult = client.Authenticate(BookingChannelType.GenesisIvr, UserName, Password);
                        }

                        if (authResult.Error!=null)
                        {
                            Token = authResult.Token;
                        }
                        else
                        {
                            throw new AuthException(authResult.ErrorMessage);
                        }

                    }
                }

				currentToken = Token;
			}
		}

        private SendDriverMessageResult DoSendDriverMessageForCity(int driverNumber, string message, int cityID, taxi_fleet fleet, string AuthenticateServiceEndpointName, string BookingServiceEndpointName)
		{
			logger.InfoFormat("MTData:Start:DoSendDriverMessageForCity:driverNumber:{0}, message:{1}, cityID:{2}", driverNumber, message, cityID);

			var currentToken = Token;

			logger.InfoFormat("MTData:Start:DoSendDriverMessageForCity:Token:{0}", Token.ToString());
            RefreshToken(ref currentToken, fleet, AuthenticateServiceEndpointName);
			logger.InfoFormat("MTData:Start:DoSendDriverMessageForCity:RefereshToken:{0}", Token.ToString());
			

			using (var client = new BookingWebServiceSoapClient(BookingServiceEndpointName))
			{
				SendDriverMessageResult result = client.SendDriverMessage1(currentToken, driverNumber, message, cityID);
				if (result.Succeeded || result.Error != ODIError.NotAuthenticated)
				{
					logger.InfoFormat("MTData:Response:SendDriverMessageForCity:{0}", result.XmlSerialize().ToString());

					return result;
				}

				RefreshToken(ref currentToken, fleet, AuthenticateServiceEndpointName);
				result = client.SendDriverMessage1(currentToken, driverNumber, message, cityID);
				logger.InfoFormat("MTData:Response:SendDriverMessageForCity:{0}", result.XmlSerialize().ToString());

				return result;
			}
		}
        #endregion
    }










}
