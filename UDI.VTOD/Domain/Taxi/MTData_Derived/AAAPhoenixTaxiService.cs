﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Domain.Taxi.MTData;

namespace UDI.VTOD.Domain.Taxi.MTData_Derived
{
    public class AAAPhoenixTaxiService : MTDataTaxiService
    {

        public AAAPhoenixTaxiService(TaxiUtility util, ILog log)
            : base(util, log, ConfigurationManager.AppSettings["AAA_SystemID"])
        {
            
        }

        public AAAPhoenixTaxiService(TaxiUtility util, ILog log, int fleetTripCode) 
            : base(util, log, fleetTripCode, ConfigurationManager.AppSettings["AAA_SystemID"])
        {   
            
        }
    }
}
