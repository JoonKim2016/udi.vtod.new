﻿using log4net;
using System.Configuration;

namespace UDI.VTOD.Domain.Taxi.MTData
{
    /// <summary>
    /// Service for MTData API
    /// </summary>
    public class TexasTaxiService : MTDataTaxiService
    {   

        public TexasTaxiService(TaxiUtility util, ILog log, int fleetTripCode)
            : base(util, log, fleetTripCode, ConfigurationManager.AppSettings["Texas_SystemID"])
        {
        }

        public TexasTaxiService(TaxiUtility util, ILog log) : base(util, log, ConfigurationManager.AppSettings["Texas_SystemID"])
        {

        }
        

    }
}
