﻿using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Common.DTO;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Domain.Taxi.UDI33;
using UDI.SDS.VehiclesService;
using UDI.VTOD.Domain.Taxi.Const;
using UDIXMLSchema;
using UDI.VTOD.Common.Track;
using UDI.Utility.Serialization;
using System.Data;
using UDI.VTOD.Domain.Taxi.Class;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Domain.SDS;
using UDI.Utility.Helper;
using UDI.VTOD.DataAccess.VTOD;
using System.Configuration;
namespace UDI.VTOD.Domain.Taxi.SDS
{
    public class SDSTaxiService : TaxiServiceBase
	{

		private ILog logger;
		private TaxiUtility utility;
		public TokenRS tokenVTOD;
		public SDSDomain sdsDomain;
        

		#region Public
        public SDSTaxiService(TaxiUtility util, ILog log)
            : base(util, log)
		{
			this.logger = log;
			this.utility = util;
			this.sdsDomain = new SDSDomain();			
		}

        public SDSTaxiService(TokenRS token, TaxiUtility util, ILog log)
            : base(util, log)
		{
			this.logger = log;
			this.utility = util;
			tokenVTOD = token;
			this.sdsDomain = new SDSDomain();			
		}
		#region old way
		public Common.DTO.OTA.OTA_GroundBookRS Book(Class.TaxiBookingParameter tbp)
		{
			throw new NotImplementedException();
		}

		public Common.DTO.OTA.OTA_GroundResRetrieveRS Status(Class.TaxiStatusParameter tsp)
		{
			throw new NotImplementedException();
		}

		public Common.DTO.OTA.OTA_GroundCancelRS Cancel(Class.TaxiCancelParameter tcp)
		{
			throw new NotImplementedException();
		}

		public Common.DTO.OTA.OTA_GroundAvailRS GetVehicleInfo(Class.TaxiGetVehicleInfoParameter tgvip)
		{
			return GetVehicleInfoByExtendedServiceAPI(tgvip);
		}
		#endregion

		#region new way
        public override OTA_GroundBookRS Book(TokenRS tokenVTOD, OTA_GroundBookRQ request, out List<Tuple<string, int, long>> disptach_Rez_VTOD, int fleetTripCode, taxi_fleet fleet)
		{
			OTA_GroundBookRS response = new OTA_GroundBookRS();
			TaxiUtility utility = new TaxiUtility(logger);
			GroundReservation reservation = request.GroundReservations.FirstOrDefault();

			#region [Taxi 1.0] Validate required fields
			TaxiBookingParameter tbp = new TaxiBookingParameter();
			if (!utility.ValidateBookingRequest(tokenVTOD, request, out tbp,fleet))
			{
				throw VtodException.CreateException(ExceptionType.Taxi, 2007);
			}
			#endregion

			#region [Taxi 1.1] Modify Request for calling SDS

			#region Removing comments
			try
			{
				request.GroundReservations.First().Service.Location.Pickup.Remark = string.Empty;

				if (request.GroundReservations.First().Service.Location.Dropoff != null)
				{
					request.GroundReservations.First().Service.Location.Dropoff.Remark = string.Empty;
				}
			}
			catch { }
			#endregion
			#endregion


			try
			{
                response = sdsDomain.GroundBook(tokenVTOD, request, out disptach_Rez_VTOD, tbp.Trip.Id,out  fleetTripCode);


				string confirmationNumber = disptach_Rez_VTOD.First().Item1;
				int rezId = disptach_Rez_VTOD.First().Item2;


				if (rezId != 0 && !string.IsNullOrWhiteSpace(confirmationNumber))
				{
					//update confirmationId and RezId
                    utility.UpdateTaxiTrip(tbp.Trip.Id, confirmationNumber, rezId, Taxi.Const.TaxiTripStatusType.Booked, fleetTripCode);
				}
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);

				logger.Error(ex.Message);
				utility.MarkThisTripAsError(tbp.Trip.Id, ex.Message);

				//change the status of trip
				utility.UpdateFinalStatus(tbp.Trip.Id, TaxiTripStatusType.Error, null, null, null, null, null);

				throw ex;
			}

			return response;
		}

        public override OTA_GroundBookRS ModifyBook(TokenRS tokenVTOD, OTA_GroundBookRQ request, out List<Tuple<string, int, long>> disptach_Rez_VTOD, int fleetTripCode, taxi_fleet fleet,long serviceAPIID)
        {
            throw new NotImplementedException();
        }
        public override OTA_GroundResRetrieveRS Status(TokenRS tokenVTOD, OTA_GroundResRetrieveRQ request,string type)
		{
			OTA_GroundResRetrieveRS response = new OTA_GroundResRetrieveRS();
			TaxiUtility utility = new TaxiUtility(logger);
            Int32 etaWithTrafficDB = 0;
			#region [Taxi 2.0] Validate required fields
			TaxiStatusParameter tsp = null;
			if (!utility.ValidateStatusRequest(tokenVTOD, request, out tsp))
			{
				throw VtodException.CreateException(ExceptionType.Taxi, 3003);
			}
			#endregion

			response = sdsDomain.GroundResRetrieve(tokenVTOD, request);

			#region ETA with Traffic
			//ToDo: it should be configurable. It's hardcoded just for now but should be configuratble.
			if (tsp.User.name.ToLower() == "ztrip" || !tsp.Trip.DriverAcceptedETA.HasValue)
			{
				var status = response.TPA_Extensions.Statuses.Status.FirstOrDefault();
				var vehicle = response.TPA_Extensions.Vehicles.Items.FirstOrDefault();

				if (status != null && !string.IsNullOrWhiteSpace(status.Value))
				{
					if (
													status.Value.Trim().ToLower() == Const.TaxiTripStatusType.Accepted.Trim().ToLower() ||
													status.Value.Trim().ToLower() == Const.TaxiTripStatusType.Assigned.Trim().ToLower() ||
													status.Value.Trim().ToLower() == Const.TaxiTripStatusType.InTransit.Trim().ToLower()
													)
					{
						if (vehicle != null && vehicle.Geolocation != null && vehicle.Geolocation.Lat != 0 && vehicle.Geolocation.Long != 0)
						{
							if (tsp.Trip != null && tsp.Trip.taxi_trip != null && tsp.Trip.taxi_trip.PickupLatitude.HasValue && tsp.Trip.taxi_trip.PickupLatitude.Value != 0 && tsp.Trip.taxi_trip.PickupLongitude.HasValue && tsp.Trip.taxi_trip.PickupLongitude.Value != 0)
							{
								try
								{
									var error = string.Empty;
									var map = new UDI.Map.MapService(Map.DTO.MapApiProvider.Google);
									var etaResult = map.GetETA(new Map.DTO.Geolocation { Latitude = vehicle.Geolocation.Lat, Longitude = vehicle.Geolocation.Long },
										new Map.DTO.Geolocation { Latitude = tsp.Trip.taxi_trip.PickupLatitude.Value.ToDecimal(), Longitude = tsp.Trip.taxi_trip.PickupLongitude.ToDecimal() }, UDI.Map.DTO.DistanceUnit.Mile, out error);

									if (string.IsNullOrWhiteSpace(error))
									{
										var etaWithTraffic = System.Math.Round((decimal)(etaResult.DurationInTrafficInSecond / 60)).ToInt32();
										//if (etaWithTraffic < 3)
										//	etaWithTraffic = 3;
										vehicle.MinutesAwayWithTraffic = etaWithTraffic.ToDecimal();
                                        etaWithTrafficDB = etaWithTraffic.ToInt32();
									}
								}
								catch
								{ }
							}
						}
					}
				}
			}
			#endregion

            #region SharedETA Link
            string uniqueID = null;
            string rateQualifier = null;
            string appSharedLink = ConfigurationManager.AppSettings["SharedETALink"];
            string link=null;
            if (!String.IsNullOrWhiteSpace(type))
            {
                link = "?t=" + type.Substring(0,1).ToLower();
            }
            if (request.TPA_Extensions.RateQualifiers != null && request.TPA_Extensions.RateQualifiers.Any())
            {
                rateQualifier = request.TPA_Extensions.RateQualifiers.FirstOrDefault().RateQualifierValue;
                link = link + "&RQ=" + rateQualifier;
            }
            if (request.Reference != null && request.Reference.Any())
            {
                uniqueID = request.Reference.FirstOrDefault().ID;
            }


            link = link + "&id=" + UDI.Utility.Helper.Cryptography.ConvertStringToHex(uniqueID);
            response.TPA_Extensions.SharedLink = appSharedLink + link;
            #endregion
			try
			{
                if(response.TPA_Extensions.Statuses.Status.First().Value== TaxiTripStatusType.Accepted.ToLower() || response.TPA_Extensions.Statuses.Status.First().Value == TaxiTripStatusType.Assigned.ToLower())
                    utility.UpdateFinalStatus(tsp.Trip.Id, response.TPA_Extensions.Statuses.Status.First().Value, null, null, null, null, etaWithTrafficDB);
                else
                    utility.UpdateFinalStatus(tsp.Trip.Id, response.TPA_Extensions.Statuses.Status.First().Value, null, null, null, null, null);
            }
			catch (Exception ex)
			{
				logger.Error(ex);
			}

			return response;
		}

        public override OTA_GroundCancelRS Cancel(TokenRS tokenVTOD, OTA_GroundCancelRQ request)
		{
			OTA_GroundCancelRS response = new OTA_GroundCancelRS();
			TaxiUtility utility = new TaxiUtility(logger);

			#region [Taxi 3.0] Validate required fields
			TaxiCancelParameter tcp = null;
			if (!utility.ValidateCancelRequest(tokenVTOD, request, out tcp))
			{
				throw VtodException.CreateException(ExceptionType.Taxi, 5005);
			}
			#endregion

			response = sdsDomain.GroundCancel(tokenVTOD, request);

			try
			{
				utility.UpdateFinalStatus(tcp.Trip.Id, Taxi.Const.TaxiTripStatusType.Canceled, null, null, null, null, null);
			}
			catch (Exception ex)
			{
				logger.Error(ex);
			}

			return response;
		}

        public override OTA_GroundAvailRS GetVehicleInfo(TokenRS tokenVTOD, OTA_GroundAvailRQ request)
		{
			OTA_GroundAvailRS response = new OTA_GroundAvailRS();

			TaxiUtility utility = new TaxiUtility(logger);

			#region [Taxi 4.0] Validate required fields
			TaxiGetVehicleInfoParameter tgvip = null;
			if (!utility.ValidateGetVehicleInfoRequest(tokenVTOD, request, out tgvip))
			{
				string error = string.Format("This GetVehicleInfo request is invalid.");
				throw new ValidationException(error);
			}
			#endregion

			if (tgvip.extendedServiceAPIID == null)
			{
				response = sdsDomain.GetVehicleInfo(tokenVTOD, request);
			}
			else
			{
				response = GetVehicleInfoByExtendedServiceAPI(tgvip);
			}

			return response;
		}
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenVTOD"></param>
        /// <param name="request"></param>        
        /// <returns></returns>
        public override OTA_GroundGetFinalRouteRS GetFinalRoute(TokenRS tokenVTOD, OTA_GroundGetFinalRouteRQ request)
        {
            throw new NotImplementedException();
        }
        public override OTA_GroundAvailRS GetEstimation(TokenRS tokenVTOD, OTA_GroundAvailRQ request)
		{
			OTA_GroundAvailRS response = new OTA_GroundAvailRS();
			TaxiUtility utility = new TaxiUtility(logger);

			#region [Taxi 5.0] Validate required fields
			#region For SDS taxi we do not need validation
			//TaxiGetEstimationParameter tgep = null;
			//if (!utility.ValidateGetEstimationRequest(tokenVTOD, request, vtodTXId, out tgep))
			//{
			//	string error = string.Format("This GetEstimation request is invalid.");
			//	throw new ValidationException(error);
			//} 
			#endregion
			#endregion

			response = sdsDomain.GroundAvail(tokenVTOD, request);

			return response;
		}
        public override bool GetDisabilityInd(TokenRS tokenVTOD, taxi_fleet f, out int condition, string AuthenticateServiceEndpointName, string BookingServiceEndpointName)
        {
            throw new NotImplementedException();
        }
        public override bool NotifyDriver(TokenRS token, UtilityNotifyDriverRQ input)
		{
			throw new NotImplementedException();
		}
        public override bool NotifyPickupDriver(TokenRS token, UtilityNotifyDriverRQ input)
        {
            throw new NotImplementedException();
        }
        public override bool NotifyDispatchPendingDriver(TokenRS token, UtilityNotifyDriverRQ input)
        {
            throw new NotImplementedException();
        }
       
		#endregion
		#endregion


		#region Private
		private OTA_GroundAvailRS GetVehicleInfoByExtendedServiceAPI(Class.TaxiGetVehicleInfoParameter tgvip)
		{
			Common.DTO.OTA.OTA_GroundAvailRS result = null;

			switch (tgvip.extendedServiceAPIID)
			{
				case null:
				case (Int64)TaxiServiceAPIConst.None:
					result = GetVehicleInfo_SDS(tgvip);
					break;
				case (Int64)TaxiServiceAPIConst.UDI33:
					result = GetVehicleInfo_SDS_UDI33(tgvip);
					break;
				case (Int64)TaxiServiceAPIConst.CCSi:
					result = GetVehicleInfo_SDS_CCSi(tgvip);
					break;
			}

			return result;
		}

		private Common.DTO.OTA.OTA_GroundAvailRS GetVehicleInfo_SDS(Class.TaxiGetVehicleInfoParameter tgvip)
		{
			Stopwatch sw = new Stopwatch();
			sw.Start();
			OTA_GroundAvailRS response = null;
			DateTime requestDateTime = DateTime.Now;
			try
			{
				//0. Declare parameters
				List<Vehicle> vList = new List<Vehicle>();

				//1. Get vehicle info. from SDS
				var adapter = new UDI.SDS.VehiclesController(tokenVTOD, null);
				VehiclesNearPoint vnp = adapter.GetSDSVehicleInfoCompressed(Convert.ToDecimal(tgvip.Latitude.ToString()), Convert.ToDecimal(tgvip.Longtitude.ToString()), UDI.VTOD.Common.DTO.Enum.VehicleType.Taxi);
				//VehiclesNearPoint vnp = adapter.GetSDSVehicleInfo(Convert.ToDecimal(tgvip.Latitude.ToString()), Convert.ToDecimal(tgvip.Longtitude.ToString()), UDI.SDS.DTO.Enum.VehicleType.Taxi);
				List<Vehicle> vInfoList = null;
				List<Vehicle> vInfoNewList = new List<Vehicle>();

				if (vnp != null && vnp.VehicleLocations != null && vnp.VehicleLocations.Any())
				{
					//2. After we get a taxi list from SDS, call UDI33(DDS) to get another list, 
					//   and then do left join from SDS to DDS.
					ITaxiService UDI33Service = new UDI33TaxiService(utility, logger);
					var resultFromDDS = UDI33Service.GetVehicleInfo(tgvip);
					vInfoList = resultFromDDS.TPA_Extensions.Vehicles.Items.ToList();


					//2.1 Get ETA from DDS info, and put it into SDS list
					//    (There is ETA (Double) in Min. Get it and put in the MinuteAway in Vehicle Object)
                    Random rnd = new Random();
					foreach (Point p in vnp.VehicleLocations)
					{

						//if both two vehicle have the same vehicle number, we will add this into vInfoList
						if (vInfoList.Where(x => x.ID.Trim() == p.VehicleNumber.ToString()).Any())
						{

							Vehicle vFromUDI33 = vInfoList.Where(x => x.ID.Trim() == p.VehicleNumber.ToString()).FirstOrDefault();

							Vehicle v = new Vehicle();
							v.Geolocation = new Geolocation();
							v.Geolocation.Long = Convert.ToDecimal(p.Longitude);
							v.Geolocation.Lat = Convert.ToDecimal(p.Latitude);
							//v.Geolocation.PriorLong = vFromUDI33.Geolocation.PriorLong;
							//v.Geolocation.PriorLat = vFromUDI33.Geolocation.PriorLat;
                            if (p.Course != 0)
                                v.Course = Convert.ToDecimal(p.Course);
                            else
                                v.Course = rnd.Next(1, 359);
							//v.MinutesAway = vFromUDI33.MinutesAway;
							//v.Distance = item.Distance;
							v.DriverID = vFromUDI33.DriverID;
							v.DriverName = vFromUDI33.DriverName;
							v.FleetID = vFromUDI33.FleetID;
							v.ID = vFromUDI33.ID;
							v.VehicleStatus = vFromUDI33.VehicleStatus;
							v.VehicleType = vFromUDI33.VehicleType;

							vInfoNewList.Add(v);

						}
					}

				}

				DateTime responseDateTime = DateTime.Now;


				//3. create response
				//NumOfVehicles
				//response.TPA_Extensions.Vehicles.
				//NearVehileETA
				response = new OTA_GroundAvailRS();
				response.TPA_Extensions = new TPA_Extensions();
				response.TPA_Extensions.Vehicles = new Vehicles();
				response.TPA_Extensions.Vehicles.Items = new List<Vehicle>();
				if (vInfoNewList != null && vInfoNewList.Any())
				{
					foreach (var vi in vInfoNewList)
					{
						response.TPA_Extensions.Vehicles.Items.Add(vi);
					}

					var rc = new UDI.SDS.RoutingController(tokenVTOD, null);
					var v = vInfoNewList.FirstOrDefault();
					UDI.SDS.RoutingService.Point start = new UDI.SDS.RoutingService.Point { Latitude = tgvip.Latitude, Longitude = tgvip.Longtitude };
					UDI.SDS.RoutingService.Point end = new UDI.SDS.RoutingService.Point { Longitude = Convert.ToDouble(v.Geolocation.Long), Latitude = Convert.ToDouble(v.Geolocation.Lat) };

					var result = rc.GetRouteMetrics(start, end);

					response.TPA_Extensions.Vehicles.NearestVehicleETA = result.TotalMinutes.ToString();
					response.TPA_Extensions.Vehicles.NumberOfVehicles = vInfoNewList.Count().ToString();
				}


				#region Write Taxi Log
				if (bool.Parse(System.Configuration.ConfigurationManager.AppSettings["IsDetailDebug"]) && tgvip.EnableResponseAndLog)
				{
					string requestMessage = string.Format("SDS GetVehicleInfo, lat={0}, long={1}", tgvip.Latitude, tgvip.Longtitude);
					string responseMessage = string.Format("Vehicles={0}", vInfoNewList.Count());
					//responseMessage = UDIUtils.IndentXMLString(responseMessage);
					utility.WriteTaxiLog(null, TaxiServiceAPIConst.SDS, TaxiServiceMethodType.GetVehicleInfo, null, requestMessage, responseMessage, requestDateTime, responseDateTime);
				}
				#endregion
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw new SDSException(ex.Message);
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return response;

		}

		private Common.DTO.OTA.OTA_GroundAvailRS GetVehicleInfo_SDS_UDI33(Class.TaxiGetVehicleInfoParameter tgvip)
		{
			Stopwatch sw = new Stopwatch();
			sw.Start();

			#region Init
			OTA_GroundAvailRS response = null;
			DateTime requestDateTime = DateTime.Now;

			var adapter = new UDI.SDS.TaxiDrvController(null);
			ITaxiService UDI33Service = new UDI33TaxiService(utility, logger);
			#endregion


			try
			{
				//0. Declare parameters
				//1. Get vehicle info. from SDS and UDI33 with TPL


				DataSet sdsDriverDataset = adapter.GetZtripDriver(tgvip.Fleet.Id.ToString());
				OTA_GroundAvailRS udi33result = null;
				List<Vehicle> udi33VehicleList = null;
				List<Vehicle> resultVehicleList = new List<Vehicle>();

				#region Make TPL
				var tasks = new List<Task>();

				#region Get vehicle info. from SDS
				var taskSDS = new Task(
					() =>
					{
						try
						{
							sdsDriverDataset = adapter.GetZtripDriver(tgvip.Fleet.Id.ToString());
						}
						catch (Exception ex)
						{
							#region Add errors to OTA
							//errorListForSuccess.Errors.Add(new Common.DTO.OTA.Error { Type = "SDS", Value = ex.Message });
							#endregion

							logger.Error(ex);
						}
					}
					,
					TaskCreationOptions.AttachedToParent);
				tasks.Add(taskSDS);
				#endregion

				#region Get ETA from UDI33 info
				var taskUDI33 = new Task(
					() =>
					{
						try
						{
							udi33result = UDI33Service.GetVehicleInfo(tgvip);
						}
						catch (Exception ex)
						{
							#region Add errors to OTA
							//errorListForSuccess.Errors.Add(new Common.DTO.OTA.Error { Type = "SDS", Value = ex.Message });
							#endregion

							logger.Error(ex);
						}
					}
					,
					TaskCreationOptions.AttachedToParent);
				tasks.Add(taskUDI33);
				#endregion
				#endregion

				#region TPL Run
				if (tasks.Count > 0)
				{
					foreach (var item in tasks)
					{
						item.Start();
					}
				}
				#endregion

				#region TPL WaitAll
				Task.WaitAll(tasks.ToArray());
				#endregion


				if (sdsDriverDataset != null && sdsDriverDataset.Tables != null && sdsDriverDataset.Tables.Count > 0)
				{
					var sdsDriverDataTable = sdsDriverDataset.Tables[0];


					//2. After we get a taxi list from SDS, call UDI33(DDS) to get another list, 
					//   and then do left join from SDS to DDS.
					udi33VehicleList = udi33result.TPA_Extensions.Vehicles.Items.ToList();


					//2.1 Get ETA from DDS info, and put it into SDS list
					//    (There is ETA (Double) in Min. Get it and put in the MinuteAway in Vehicle Object)

					//foreach (Point p in vnp.VehicleLocations)
                    Random rnd = new Random();
					foreach (DataRow row in sdsDriverDataTable.Rows)
					{
						string sdsDriverID = row["DriverID"].ToString();
						string sdsVehicleID = row["VehicleID"].ToString();
						string sdsDriverFirstName = row["FirstName"].ToString();
						string sdsDriverLastName = row["LastName"].ToString();



						//if both two vehicle have the same vehicle number, we will add this into vInfoList
						if (udi33VehicleList.Where(x => x.ID.Trim() == sdsVehicleID).Any())
						{

							Vehicle udi33Vehicle = udi33VehicleList.Where(x => x.ID.Trim() == sdsVehicleID).FirstOrDefault();

							Vehicle resultVehicle = new Vehicle();

							resultVehicle.Geolocation = new Geolocation();
							if (udi33Vehicle.Geolocation != null)
							{
								resultVehicle.Geolocation.Long = udi33Vehicle.Geolocation.Long;
								resultVehicle.Geolocation.Lat = udi33Vehicle.Geolocation.Lat;
								resultVehicle.Geolocation.PriorLong = udi33Vehicle.Geolocation.PriorLong;
								resultVehicle.Geolocation.PriorLat = udi33Vehicle.Geolocation.PriorLat;
							}
                            if (udi33Vehicle.Course != 0)
                                resultVehicle.Course = Convert.ToDecimal(udi33Vehicle.Course);
                            else
                                resultVehicle.Course = rnd.Next(1, 359);
							resultVehicle.MinutesAway = udi33Vehicle.MinutesAway;
							resultVehicle.Distance = udi33Vehicle.Distance;
							resultVehicle.DriverID = sdsDriverID;
							resultVehicle.DriverName = string.Format("{0} {1}", sdsDriverFirstName, sdsDriverLastName);
							resultVehicle.FleetID = udi33Vehicle.FleetID;
							resultVehicle.ID = sdsVehicleID;
							resultVehicle.VehicleStatus = udi33Vehicle.VehicleStatus;
							resultVehicle.VehicleType = udi33Vehicle.VehicleType;

							resultVehicleList.Add(resultVehicle);

						}
					}
				}

				DateTime responseDateTime = DateTime.Now;


				//3. create response
				//NumOfVehicles
				//response.TPA_Extensions.Vehicles.
				//NearVehileETA
				response = new OTA_GroundAvailRS();
				response.TPA_Extensions = new TPA_Extensions();
				response.TPA_Extensions.Vehicles = new Vehicles();
				response.TPA_Extensions.Vehicles.Items = new List<Vehicle>();
				if (resultVehicleList != null && resultVehicleList.Any())
				{
					foreach (var vi in resultVehicleList)
					{
						response.TPA_Extensions.Vehicles.Items.Add(vi);
					}

					var rc = new UDI.SDS.RoutingController(tokenVTOD, null);
					var v = resultVehicleList.FirstOrDefault();
					UDI.SDS.RoutingService.Point start = new UDI.SDS.RoutingService.Point { Latitude = tgvip.Latitude, Longitude = tgvip.Longtitude };
					UDI.SDS.RoutingService.Point end = new UDI.SDS.RoutingService.Point { Longitude = Convert.ToDouble(v.Geolocation.Long), Latitude = Convert.ToDouble(v.Geolocation.Lat) };

					var result = rc.GetRouteMetrics(start, end);

					response.TPA_Extensions.Vehicles.NearestVehicleETA = result.TotalMinutes.ToString();
					response.TPA_Extensions.Vehicles.NumberOfVehicles = resultVehicleList.Count().ToString();
				}


				#region Write Taxi Log
				if (bool.Parse(System.Configuration.ConfigurationManager.AppSettings["IsDetailDebug"]) && tgvip.EnableResponseAndLog)
				{
					string requestMessage = string.Format("SDS GetVehicleInfo, lat={0}, long={1}", tgvip.Latitude, tgvip.Longtitude);
					string responseMessage = string.Format("Vehicles={0}", resultVehicleList.Count());
					//responseMessage = UDIUtils.IndentXMLString(responseMessage);
					utility.WriteTaxiLog(null, TaxiServiceAPIConst.SDS, TaxiServiceMethodType.GetVehicleInfo, null, requestMessage, responseMessage, requestDateTime, responseDateTime);
				}
				#endregion
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw new SDSException(ex.Message);
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return response;

		}

		private Common.DTO.OTA.OTA_GroundAvailRS GetVehicleInfo_SDS_CCSi(Class.TaxiGetVehicleInfoParameter tgvip)
		{
			throw new NotImplementedException();
		}
		#endregion


		TokenRS TokenVTOD
		{
			set
			{
				tokenVTOD = value; ;
			}
		}


        public override UtilityCustomerInfoRS GetCustomerInfo(TokenRS token, UtilityCustomerInfoRQ input)
        {
            throw new NotImplementedException();
        }

        public override UtilityGetLastTripRS GetLastTrip(TokenRS token, UtilityGetLastTripRQ input)
        {
            throw new NotImplementedException();
        }

        public override UtilityGetPickupAddressRS GetPickupAddress(TokenRS token, UtilityGetPickupAddressRQ input)
        {
            throw new NotImplementedException();
        }

        public override UtilityGetBlackoutPeriodRS GetBlackoutPeriod(TokenRS token, UtilityGetBlackoutPeriodRQ input)
        {
            throw new NotImplementedException();
        }



        public override UtilityGetNotificationRS GetStaleNotification(TokenRS token, UtilityGetNotificationRQ input)
        {
            throw new NotImplementedException();
        }

        public override UtilityGetNotificationRS GetArrivalNotification(TokenRS token, UtilityGetNotificationRQ input)
        {
            throw new NotImplementedException();
        }

        public override UtilityGetNotificationRS GetNightBeforeReminder(TokenRS token, UtilityGetNotificationRQ input)
        {
            throw new NotImplementedException();
        }
    }
}
