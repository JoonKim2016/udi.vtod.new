﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using UDI.Map;
using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.Helper;
using UDI.VTOD.DataAccess.VTOD;
using UDILibrary;
using UDITools;
using UDIXMLSchema;
using System.Text.RegularExpressions;
using UDI.VTOD.Common.DTO.Enum;
using UDI.Map.DTO;
using UDI.Map.Utility;
using System.Xml.Linq;
using System.Configuration;
using UDI.VTOD.Domain.Taxi.UDI33;
using UDI.VTOD.Domain.Taxi.CCSi;
using UDI.VTOD.Domain.Taxi.Const;
using UDI.VTOD.Domain.Taxi.Class;
using UDI.Utility.Helper;
using UDI.VTOD.Domain.Taxi.SDS;
using UDI.VTOD.Common.Track;
using UDI.VTOD.Domain.Taxi.MTData;
using UDI.VTOD.Domain.Utility;
using UDI.VTOD.Domain.Taxi.MTData_Derived;

namespace UDI.VTOD.Domain.Taxi
{
	internal class TaxiDomain : BaseSetting
	{

		#region Constructors
		internal TaxiDomain()
		{
			OwnerType = LogOwnerType.Taxi;
		}
		#endregion

		#region private


        private ITaxiService GetServiceAPI(long serviceAPIID, TokenRS tokenVTOD)
        {
            ITaxiService service = null;
            TaxiUtility utility = new TaxiUtility(this.logger);
            if (serviceAPIID == TaxiServiceAPIConst.UDI33)
            {
                service = new UDI33TaxiService(utility, logger);

            }
            else if (serviceAPIID == TaxiServiceAPIConst.CCSi)
            {
                service = new CCSiTaxiService(utility, logger);
            }
            else if (serviceAPIID == TaxiServiceAPIConst.SDS)
            {
                service = new SDSTaxiService(tokenVTOD, utility, logger);
            }
            else if (serviceAPIID == TaxiServiceAPIConst.MTData)
            {
                service = new MTDataTaxiService(utility, logger);
            }
            else if (serviceAPIID == TaxiServiceAPIConst.Texas)
            {
                service = new TexasTaxiService(utility, logger);
            }
            else if (serviceAPIID == TaxiServiceAPIConst.Phoenix)
            {
                service = new AAAPhoenixTaxiService(utility, logger);
            }
            else
            {
                var vtodException = VtodException.CreateException(ExceptionType.Taxi, 1009);
                logger.Error(vtodException.ExceptionMessage.Message);

                throw vtodException;
            }
            return service;
        }

		private ITaxiService GetServiceAPI(long serviceAPIID, TokenRS tokenVTOD, out int FleetTripCode)
		{
			ITaxiService service = null;
			TaxiUtility utility = new TaxiUtility(this.logger);
			if (serviceAPIID == TaxiServiceAPIConst.UDI33)
			{
				FleetTripCode = UDI.VTOD.Common.DTO.Enum.FleetTripCode.Taxi_UDI33;
				service = new UDI33TaxiService(utility, logger);

			}
			else if (serviceAPIID == TaxiServiceAPIConst.CCSi)
			{
				FleetTripCode = UDI.VTOD.Common.DTO.Enum.FleetTripCode.Taxi_CSCI;
				service = new CCSiTaxiService(utility, logger);
			}
			else if (serviceAPIID == TaxiServiceAPIConst.SDS)
			{
				FleetTripCode = UDI.VTOD.Common.DTO.Enum.FleetTripCode.Taxi_SDS;
				service = new SDSTaxiService(tokenVTOD, utility, logger);
			}
			else if (serviceAPIID == TaxiServiceAPIConst.MTData)
			{
				FleetTripCode = UDI.VTOD.Common.DTO.Enum.FleetTripCode.Taxi_MTDATA;
				service = new MTDataTaxiService(utility, logger);
			}
            else if (serviceAPIID == TaxiServiceAPIConst.Texas)
            {
                FleetTripCode = UDI.VTOD.Common.DTO.Enum.FleetTripCode.Taxi_Texas;
                service = new TexasTaxiService(utility, logger,FleetTripCode);
            }
            else if (serviceAPIID == TaxiServiceAPIConst.Phoenix)
            {
                FleetTripCode = UDI.VTOD.Common.DTO.Enum.FleetTripCode.Taxi_AAA_Phoenix;
                service = new AAAPhoenixTaxiService(utility, logger, FleetTripCode);
            }
            else
			{
				var vtodException = VtodException.CreateException(ExceptionType.Taxi, 1009);
				logger.Error(vtodException.ExceptionMessage.Message);

				throw vtodException;
			}
			return service;
		}
		#endregion

		#region public
		public OTA_GroundBookRS Book(TokenRS tokenVTOD, OTA_GroundBookRQ request, out List<Tuple<string, int, long>> disptach_Rez_VTOD, out int fleetTrCode)
		{
            //decimal thresholdDriveIncentive = System.Configuration.ConfigurationManager.AppSettings["ThresholdForNotificationGratuity"].ToDecimal();
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			OTA_GroundBookRS response = null;
			disptach_Rez_VTOD = new List<Tuple<string, int, long>>();
			int fleetTripCode;
			try
			{
				#region Pre
				#region Address Modification
				try
				{
					if (request.GroundReservations.First().Service.Location.Pickup != null && request.GroundReservations.First().Service.Location.Pickup.Address != null && string.IsNullOrWhiteSpace(request.GroundReservations.First().Service.Location.Pickup.Address.StreetNmbr))
					{
						#region Regex
						Regex regex = new Regex(@"^([0-9-–]+[\s]{0,}[0-9\/]{0,})[\s](.*)", RegexOptions.IgnoreCase);
						var matches = regex.Matches(request.GroundReservations.First().Service.Location.Pickup.Address.AddressLine.Trim());
						if (matches.Count > 0 && matches[0].Groups.Count == 3)
						{
							request.GroundReservations.First().Service.Location.Pickup.Address.StreetNmbr = matches[0].Groups[1].Value;
							request.GroundReservations.First().Service.Location.Pickup.Address.AddressLine = matches[0].Groups[2].Value;
						}
						#endregion
					}
					if (request.GroundReservations.First().Service.Location.Dropoff != null && request.GroundReservations.First().Service.Location.Dropoff.Address != null && string.IsNullOrWhiteSpace(request.GroundReservations.First().Service.Location.Dropoff.Address.StreetNmbr))
					{
						#region Regex
						Regex regex = new Regex(@"^([0-9-–]+[\s]{0,}[0-9\/]{0,})[\s](.*)", RegexOptions.IgnoreCase);
						var matches = regex.Matches(request.GroundReservations.First().Service.Location.Dropoff.Address.AddressLine.Trim());
						if (matches.Count > 0 && matches[0].Groups.Count == 3)
						{
							request.GroundReservations.First().Service.Location.Dropoff.Address.StreetNmbr = matches[0].Groups[1].Value;
							request.GroundReservations.First().Service.Location.Dropoff.Address.AddressLine = matches[0].Groups[2].Value;
						}
						#endregion
					}
				}
				catch { }




				#endregion

				#region [Work Around] Modify RQ for adding PU Remark
				//if (tokenVTOD.Username.ToLower().Trim() == "ztrip")
				//{
				//    if (string.IsNullOrWhiteSpace(request.GroundReservations.First().Service.Location.Pickup.Remark))
				//    {
				//        if (request.Payments != null && request.Payments.Payments != null && request.Payments.Payments.Any() && request.Payments.Payments.First().PaymentCard != null)
				//        {
				//            request.GroundReservations.First().Service.Location.Pickup.Remark = "zTrip Customer is paid on zTrip Account. Do Not Collect Payment.";
				//        }
				//        else
				//        {
				//            request.GroundReservations.First().Service.Location.Pickup.Remark = "Customer will pay by cash.";
				//        }

				//        #region Adding Cutomer Phone#
				//        if (request.GroundReservations.First().Passenger != null && request.GroundReservations.First().Passenger.Primary != null && request.GroundReservations.First().Passenger.Primary.Telephones != null && request.GroundReservations.First().Passenger.Primary.Telephones.Any())
				//        {
				//            var addingText = string.Format(" Customer phone: {0}-{1}.", request.GroundReservations.First().Passenger.Primary.Telephones.First().AreaCityCode, request.GroundReservations.First().Passenger.Primary.Telephones.First().PhoneNumber);
				//            request.GroundReservations.First().Service.Location.Pickup.Remark += addingText;
				//        }
				//        #endregion

				//        #region Adding Gratuity
				//        //********************************************************************************************
				//        //**************** we are adding tip just for MTData in its domain calss: MTDataTaxiService **
				//        //**************** so if you uncomment this block please comment MTDtata block ***************
				//        //********************************************************************************************

				//        //var specialInputs = request.GroundReservations.First().RateQualifiers.First().SpecialInputs;
				//        //if (specialInputs.Any())
				//        //{
				//        //	var gratuity = specialInputs.Where(s => s.Name.ToLower().Trim() == "gratuity").FirstOrDefault();
				//        //	if (gratuity !=null)
				//        //	{
				//        //		var addingText = string.Format(" Tip is {0}", gratuity.Value);
				//        //		request.GroundReservations.First().Service.Location.Pickup.Remark += addingText;
				//        //	}
				//        //}
				//        #endregion

				//    }
				//}
				#endregion

				#endregion

				#region new way
				taxi_fleet fleet = null;
				TaxiUtility utility = new TaxiUtility(logger);
				long serviceAPIID = utility.GetServiceAPIIdByRequest(request, out fleet);

				#region Remarks For PickUp
				string userName = tokenVTOD.Username.ToLower().Trim();
				List<string> remarksPickUpDropoff = utility.GetRemarks(userName, fleet.Id);
				if (request.GroundReservations.First().Service.Location.Pickup != null)
				{
					if (string.IsNullOrWhiteSpace(request.GroundReservations.First().Service.Location.Pickup.Remark))
					{
						if (remarksPickUpDropoff != null && remarksPickUpDropoff.Any())
						{
							if (request.Payments != null && request.Payments.Payments != null && request.Payments.Payments.Any() && request.Payments.Payments.First().PaymentCard != null)
								request.GroundReservations.First().Service.Location.Pickup.Remark = remarksPickUpDropoff[2];
							else
								request.GroundReservations.First().Service.Location.Pickup.Remark = remarksPickUpDropoff[0];

							#region Adding Cutomer Phone#
							if (request.GroundReservations.First().Passenger != null && request.GroundReservations.First().Passenger.Primary != null && request.GroundReservations.First().Passenger.Primary.Telephones != null && request.GroundReservations.First().Passenger.Primary.Telephones.Any())
							{
								var addingText = string.Format(" Customer Phone : {0}-{1}.", request.GroundReservations.First().Passenger.Primary.Telephones.First().AreaCityCode, request.GroundReservations.First().Passenger.Primary.Telephones.First().PhoneNumber);
								request.GroundReservations.First().Service.Location.Pickup.Remark = request.GroundReservations.First().Service.Location.Pickup.Remark.Replace("{PhoneNumber}", addingText);

							}
							#endregion

							#region Adding Gratuity
							//********************************************************************************************
							//**************** we are adding tip just for MTData in its domain calss: MTDataTaxiService **
							//**************** so if you uncomment this block please comment MTDtata block ***************
							//********************************************************************************************

							var specialInputs = request.GroundReservations.First().RateQualifiers.First().SpecialInputs;
							if (specialInputs != null && specialInputs.Any())
							{
								var gratuity = specialInputs.Where(s => s.Name.ToLower().Trim() == "gratuity").FirstOrDefault();
								if (gratuity != null)
								{
									var addingText = string.Format(" Tip is : {0}", gratuity.Value);
									request.GroundReservations.First().Service.Location.Pickup.Remark = request.GroundReservations.First().Service.Location.Pickup.Remark.Replace("{Gratuity}", addingText);
								}
							}
							#endregion

                            #region Adding Incentive
                            //********************************************************************************************
                            //**************** we are adding tip just for MTData in its domain calss: MTDataTaxiService **
                            //**************** so if you uncomment this block please comment MTDtata block ***************
                            //********************************************************************************************

                            var specialInpts = request.GroundReservations.First().RateQualifiers.First().SpecialInputs;
                            bool? isIncentive = request.TPA_Extensions.IsIncentive;
                            if (specialInputs != null && specialInpts.Any())
                            {
                                if (isIncentive == true)
                                {
                                    var gratuity = specialInputs.Where(s => s.Name.ToLower().Trim() == "gratuity").FirstOrDefault();
                                    if (gratuity != null)
                                    {
                                        decimal incentive=gratuity.Value.ToString().Replace("%","").Trim().ToDecimal();
                                       //if (decimal.Compare(incentive, thresholdDriveIncentive) >= 0)*/
                                       // {
                                            var addingText = string.Format(" Tip is : {0}", gratuity.Value);
                                            request.GroundReservations.First().Service.Location.Pickup.Remark = request.GroundReservations.First().Service.Location.Pickup.Remark.Replace("{Incentive}", addingText);
                                       // }
                                     }
                                }

                                else
                                {
                                        request.GroundReservations.First().Service.Location.Pickup.Remark = request.GroundReservations.First().Service.Location.Pickup.Remark.Replace("{Incentive}", "");
                                }

                            }
                            #endregion
						}

					}
				}
				if (request.GroundReservations.First().Service.Location.Dropoff != null)
				{
					if (string.IsNullOrWhiteSpace(request.GroundReservations.First().Service.Location.Dropoff.Remark))
					{
						if (remarksPickUpDropoff != null && remarksPickUpDropoff.Any())
						{
							if (request.Payments != null && request.Payments.Payments != null && request.Payments.Payments.Any() && request.Payments.Payments.First().PaymentCard != null)
								request.GroundReservations.First().Service.Location.Dropoff.Remark = remarksPickUpDropoff[3];
							else
								request.GroundReservations.First().Service.Location.Dropoff.Remark = remarksPickUpDropoff[1];
							#region Adding Cutomer Phone#
							if (request.GroundReservations.First().Passenger != null && request.GroundReservations.First().Passenger.Primary != null && request.GroundReservations.First().Passenger.Primary.Telephones != null && request.GroundReservations.First().Passenger.Primary.Telephones.Any())
							{
								var addingText = string.Format(" Customer Phone : {0}-{1}.", request.GroundReservations.First().Passenger.Primary.Telephones.First().AreaCityCode, request.GroundReservations.First().Passenger.Primary.Telephones.First().PhoneNumber);
								request.GroundReservations.First().Service.Location.Dropoff.Remark = request.GroundReservations.First().Service.Location.Dropoff.Remark.Replace("{PhoneNumber}", addingText);

							}
							#endregion

							#region Adding Gratuity
							//********************************************************************************************
							//**************** we are adding tip just for MTData in its domain calss: MTDataTaxiService **
							//**************** so if you uncomment this block please comment MTDtata block ***************
							//********************************************************************************************

							var specialInputs = request.GroundReservations.First().RateQualifiers.First().SpecialInputs;
							if (specialInputs.Any())
							{
								var gratuity = specialInputs.Where(s => s.Name.ToLower().Trim() == "gratuity").FirstOrDefault();
								if (gratuity != null)
								{
									var addingText = string.Format(" Tip is : {0}", gratuity.Value);
									request.GroundReservations.First().Service.Location.Dropoff.Remark = request.GroundReservations.First().Service.Location.Dropoff.Remark.Replace("{Gratuity}", addingText);

								}
							}
							#endregion
						}

					}
				}
				#endregion
				ITaxiService service = this.GetServiceAPI(serviceAPIID, tokenVTOD, out fleetTripCode);

				fleetTrCode = fleetTripCode;
				response = service.Book(tokenVTOD, request, out disptach_Rez_VTOD, fleetTripCode, fleet);
				#endregion

				#region [Taxi 1.2] Link  disptach_VTOD_TripIDs (for supershuttel system)
				if (response.Reservations != null && response.Reservations.Any())
				{
					if (!disptach_Rez_VTOD.Any())
					{
						var rez = response.Reservations.First();
						//disptach_VTOD_TripIDs.Add(rez.Confirmation.TPA_Extensions.DispatchConfirmation.ID, rez.Confirmation.ID.ToInt64());
						var dispatchConfirmationID = rez.Confirmation.TPA_Extensions.Confirmations.Where(s => s.Type.Trim().ToLower() == ConfirmationType.DispatchConfirmation.ToString().ToLower()).FirstOrDefault().ID;
						//var DispatchConfirmationID = rez.Confirmation.TPA_Extensions.Confirmations.Where(s => s.Type.Trim().ToLower() == ConfirmationType.DispatchConfirmation.ToString().ToLower()).FirstOrDefault().ID;

						disptach_Rez_VTOD.Add(new Tuple<string, int, long>(dispatchConfirmationID, 0, rez.Confirmation.ID.ToInt64()));
						//disptach_VTOD_TripIDs.Add(dispatchConfirmationID, rez.Confirmation.ID.ToInt64());
					}
				}
				#endregion

				//response.Success = new Success();
			}
			catch (System.Data.Entity.Validation.DbEntityValidationException ex)
			{
				foreach (var eve in ex.EntityValidationErrors)
				{
					logger.ErrorFormat("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
						eve.Entry.Entity.GetType().Name, eve.Entry.State);
					foreach (var ve in eve.ValidationErrors)
					{
						logger.ErrorFormat("- Property: \"{0}\", Error: \"{1}\"",
							ve.PropertyName, ve.ErrorMessage);
					}
				}
				throw;
			}
			catch (Exception ex)
			{				
				logger.Error(ex);				
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				if (ex.GetType().Equals(typeof(VtodException)))
				{
					throw;
				}
				else
				{
					throw VtodException.CreateException(ExceptionType.Taxi, 2001);// new TaxiException(Messages.Taxi_BookingFailure);
				}
			}

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return response;
		}

        public OTA_GroundBookRS ModifyBook(TokenRS tokenVTOD, OTA_GroundBookRQ request, out List<Tuple<string, int, long>> disptach_Rez_VTOD, out int fleetTrCode)
        {
            logger.Info("Start");
            Stopwatch sw = new Stopwatch();
            sw.Start();
            OTA_GroundBookRS response = null;            
            int fleetTripCode;
            try
            {
                #region Pre
                #region Address Modification
                try
                {
                    if (request.GroundReservations.First().Service.Location.Pickup != null && request.GroundReservations.First().Service.Location.Pickup.Address != null && string.IsNullOrWhiteSpace(request.GroundReservations.First().Service.Location.Pickup.Address.StreetNmbr))
                    {
                        #region Regex
                        Regex regex = new Regex(@"^([0-9-–]+[\s]{0,}[0-9\/]{0,})[\s](.*)", RegexOptions.IgnoreCase);
                        var matches = regex.Matches(request.GroundReservations.First().Service.Location.Pickup.Address.AddressLine.Trim());
                        if (matches.Count > 0 && matches[0].Groups.Count == 3)
                        {
                            request.GroundReservations.First().Service.Location.Pickup.Address.StreetNmbr = matches[0].Groups[1].Value;
                            request.GroundReservations.First().Service.Location.Pickup.Address.AddressLine = matches[0].Groups[2].Value;
                        }
                        #endregion
                    }
                    if (request.GroundReservations.First().Service.Location.Dropoff != null && request.GroundReservations.First().Service.Location.Dropoff.Address != null && string.IsNullOrWhiteSpace(request.GroundReservations.First().Service.Location.Dropoff.Address.StreetNmbr))
                    {
                        #region Regex
                        Regex regex = new Regex(@"^([0-9-–]+[\s]{0,}[0-9\/]{0,})[\s](.*)", RegexOptions.IgnoreCase);
                        var matches = regex.Matches(request.GroundReservations.First().Service.Location.Dropoff.Address.AddressLine.Trim());
                        if (matches.Count > 0 && matches[0].Groups.Count == 3)
                        {
                            request.GroundReservations.First().Service.Location.Dropoff.Address.StreetNmbr = matches[0].Groups[1].Value;
                            request.GroundReservations.First().Service.Location.Dropoff.Address.AddressLine = matches[0].Groups[2].Value;
                        }
                        #endregion
                    }
                }
                catch { }




                #endregion

                #endregion

                #region new way
                taxi_fleet fleet = null;
                TaxiUtility utility = new TaxiUtility(logger);
                long serviceAPIID = utility.GetServiceAPIIdByRequest(request, out fleet);

                ITaxiService service = this.GetServiceAPI(serviceAPIID, tokenVTOD, out fleetTripCode);

                fleetTrCode = fleetTripCode;
                response = service.ModifyBook(tokenVTOD, request, out disptach_Rez_VTOD, fleetTripCode, fleet, serviceAPIID);
                #endregion
               
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                foreach (var eve in ex.EntityValidationErrors)
                {
                    logger.ErrorFormat("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        logger.ErrorFormat("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            catch (Exception ex)
            {                
                logger.Error(ex);             
                logger.ErrorFormat("Message:{0}", ex.Message);
                logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
                logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
                if (ex.GetType().Equals(typeof(VtodException)))
                {
                    throw;
                }
                else
                {
                    throw VtodException.CreateException(ExceptionType.Taxi, 2001);
                }
            }

            sw.Stop();
            logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
            return response;
        }

        public OTA_GroundResRetrieveRS Status(TokenRS tokenVTOD, OTA_GroundResRetrieveRQ request)
		{
			//NotifyDriver(tokenVTOD, new UtilityNotifyDriverRQ { Message = "Hello world", TripID = 311 }, null);
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			OTA_GroundResRetrieveRS response = null;
			string type = request.Reference.FirstOrDefault().Type;
            logger.InfoFormat("Confirmation Type={0}",type);
            long serviceAPIID = -1;

			try
			{
				TaxiUtility utility = new TaxiUtility(logger);
                serviceAPIID = utility.GetServiceAPIIdByRequest(request);
                //request = utility.CorrectVTODRequest(request);
				ITaxiService service = this.GetServiceAPI(serviceAPIID, tokenVTOD);
                /*
                logger.InfoFormat("Any={0}", request.Reference.Count());
                //logger.InfoFormat("DispatchConfirmation={0}", request.Reference.Where(x=>x.Type==type).Count());
                logger.InfoFormat("ID={0}", request.Reference.FirstOrDefault().ID);

                 */
				response = service.Status(tokenVTOD, request, type);
			}
			catch (Exception ex)
			{				
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);

				throw;
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return response;
		}
		public OTA_GroundCancelRS Cancel(TokenRS tokenVTOD, OTA_GroundCancelRQ request)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			int fleetTripCode;
			OTA_GroundCancelRS response = null;
			try
			{

				TaxiUtility utility = new TaxiUtility(logger);
				long serviceAPIID = utility.GetServiceAPIIdByRequest(request);
                //request = utility.CorrectVTODRequest(request);
				ITaxiService service = this.GetServiceAPI(serviceAPIID, tokenVTOD, out fleetTripCode);
				response = service.Cancel(tokenVTOD, request);
			}
			catch (Exception ex)
			{				
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);

				throw;
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return response;
		}
		public OTA_GroundAvailRS GetVehicleInfo(TokenRS tokenVTOD, OTA_GroundAvailRQ request)
		{
			logger.Info("Start");
			//Please do not delete it. It's just for my test :) Pouyan
			//var adapter = new UDI.SDS.VehiclesController(tokenVTOD, null);
			//var result = adapter.GetTaxiVehicleInfo("Kansas City 10/10");

			Stopwatch sw = new Stopwatch();
			sw.Start();
			OTA_GroundAvailRS response = null;
			try
			{
				#region old way
				//TaxiUtility utility = new TaxiUtility(logger);

				//#region [Taxi 4.0] Validate required fields
				//TaxiGetVehicleInfoParameter tgvip = null;
				//if (!utility.ValidateGetVehicleInfoRequest(tokenVTOD, request, out tgvip))
				//{
				//    string error = string.Format("This GetVehicleInfo request is invalid.");
				//    throw new ValidationException(error);
				//}
				//#endregion

				//#region [Taxi 4.1] Choose ServiceAPI
				//ITaxiService service = this.GetServiceAPI(tgvip.serviceAPIID, tokenVTOD);

				////service.TrackTime = TrackTime;
				//#endregion

				//#region [Taxi 4.2] GetVehicleInfo
				//response = service.GetVehicleInfo(tgvip);
				//#endregion 
				#endregion

				#region new way
				int fleetTripCode;
				TaxiUtility utility = new TaxiUtility(logger);
				taxi_fleet fleet = null;
				long serviceAPIID = utility.GetServiceAPIIdByRequest(request, TaxiServiceAPIPreferenceConst.GetVehicleInfo, out fleet);
				ITaxiService service = this.GetServiceAPI(serviceAPIID, tokenVTOD, out fleetTripCode);
				response = service.GetVehicleInfo(tokenVTOD, request);


				#endregion


				response.Success = new Success();
				#region Common
				response.EchoToken = request.EchoToken;
				response.PrimaryLangID = request.PrimaryLangID;
				response.Target = request.Target;
				response.Version = request.Version;
				#endregion

			}
			catch (Exception ex)
			{				
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw;				
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return response;
		}
		public OTA_GroundAvailRS GetEstimation(TokenRS tokenVTOD, OTA_GroundAvailRQ request)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			OTA_GroundAvailRS response = new OTA_GroundAvailRS();
			OTA_GroundAvailRS test = new OTA_GroundAvailRS();
			try
			{
				#region Pre
				#region Address Modification
				try
				{
					if (request.Service.Pickup != null && request.Service.Pickup.Address != null && string.IsNullOrWhiteSpace(request.Service.Pickup.Address.StreetNmbr))
					{
						#region Regex
						Regex regex = new Regex(@"^([0-9-–]+[\s]{0,}[0-9\/]{0,})[\s](.*)", RegexOptions.IgnoreCase);
						var matches = regex.Matches(request.Service.Pickup.Address.AddressLine.Trim());
						if (matches.Count > 0 && matches[0].Groups.Count == 3)
						{
							request.Service.Pickup.Address.StreetNmbr = matches[0].Groups[1].Value;
							request.Service.Pickup.Address.AddressLine = matches[0].Groups[2].Value;
						}
						#endregion
					}
					if (request.Service.Dropoff != null && request.Service.Dropoff.Address != null && string.IsNullOrWhiteSpace(request.Service.Dropoff.Address.StreetNmbr))
					{
						#region Regex
						Regex regex = new Regex(@"^([0-9-–]+[\s]{0,}[0-9\/]{0,})[\s](.*)", RegexOptions.IgnoreCase);
						var matches = regex.Matches(request.Service.Dropoff.Address.AddressLine.Trim());
						if (matches.Count > 0 && matches[0].Groups.Count == 3)
						{
							request.Service.Dropoff.Address.StreetNmbr = matches[0].Groups[1].Value;
							request.Service.Dropoff.Address.AddressLine = matches[0].Groups[2].Value;
						}
						#endregion
					}
				}
				catch { }




				#endregion
				#endregion

				#region old way
				//TaxiUtility utility = new TaxiUtility(logger);

				//#region [Taxi 5.0] Validate required fields
				//TaxiGetEstimationParameter tgep = null;
				//if (!utility.ValidateGetEstimationRequest(tokenVTOD, request, vtodTXId, out tgep))
				//{
				//    string error = string.Format("This GetEstimation request is invalid.");
				//    throw new ValidationException(error);
				//}
				//#endregion

				//#region [Taxi 5.1] Response OTA_GroundAvailRS
				////common
				//response.Success = new Success();
				//response.GroundServices = new GroundServiceList();
				//response.GroundServices.GroundServices = new List<GroundService>();

				////Fare policy
				//#region 01. Rate
				//GroundService fpRate = new GroundService();
				//fpRate.RateQualifier = new RateQualifier { Category = new Category { Code = FareType.FarePolicy.ToString(), Value = "Other_" } };
				////fpRate.RateQualifier.SpecialInputs = new List<KeyValuePair<string, string>>();
				//fpRate.RateQualifier.SpecialInputs = new List<NameValue>();
				////KeyValuePair<string, string> kvpTypeRate = new KeyValuePair<string, string>("Type", "Rate");
				////NameValue kvpTypeRate = new NameValue { Name = "Type", Value = FareType.Estimation.ToString() };
				////fpRate.RateQualifier.SpecialInputs.Add(kvpTypeRate);
				//fpRate.ServiceCharges = new List<ServiceCharges>();
				//ServiceCharges sc_Rate_Init = new ServiceCharges { ChargePurpose = new ChargePurpose { Code = FareDetail.FlagDrop.ToString(), Value = "Other_" }, CurrencyCode = tgep.Info_Rate.AmountUnit, /* Description = tgep.Info_Rate.DistanceUnit, */ Amount = tgep.Info_Rate.InitialAmount.ToString() };
				////ServiceCharges sc_Rate_Init = new ServiceCharges { ChargePurpose = new ChargePurpose { Code = FareDetail.FlagDrop.ToString(), Value = "Other_" }, FreeDistance =111111, CurrencyCode = tgep.Info_Rate.AmountUnit, /* Description = tgep.Info_Rate.DistanceUnit, */ Amount = decimal.Parse(tgep.Info_Rate.InitialAmount.ToString()) };
				//ServiceCharges sc_Rate_PerDistance = new ServiceCharges { ChargePurpose = new ChargePurpose { Code = FareDetail.PerDistance.ToString(), Value = "Other_" }, CurrencyCode = tgep.Info_Rate.AmountUnit, Description = tgep.Info_Rate.DistanceUnit, Amount = tgep.Info_Rate.PerDistanceAmount.ToString() };
				//fpRate.ServiceCharges.Add(sc_Rate_Init);
				//fpRate.ServiceCharges.Add(sc_Rate_PerDistance);
				//response.GroundServices.GroundServices.Add(fpRate);
				//#endregion

				//#region 02. Flatrate part policy (don't remove this)
				////We turn this policy part off now. 
				////This is a part to verify the price policy 
				///*
				//if (tgep.Info_FlatRates != null && tgep.Info_FlatRates.Any())
				//    foreach (TaxiFlatRateData tfrd in tgep.Info_FlatRates)
				//    {
				//        GroundService fpFlatRate = new GroundService();
				//        fpFlatRate.RateQualifier = new RateQualifier { Category = new Category { Code = FareType.FarePolicy.ToString(), Value = "Other_" } };
				//        //fpFlatRate.RateQualifier.SpecialInputs = new List<KeyValuePair<string, string>>();
				//        fpFlatRate.RateQualifier.SpecialInputs = new List<NameValue>();
				//        //KeyValuePair<string, string> kvpTypeFlatRate = new KeyValuePair<string, string>("Type", "FlatRate");
				//        //KeyValuePair<string, string> kvpTypePickup = new KeyValuePair<string, string>("Pickup", tfrd.PickupZoneName);
				//        //KeyValuePair<string, string> kvpTypeDropoff = new KeyValuePair<string, string>("DropOff", tfrd.DropOffZoneName);
				//        //NameValue kvpTypeFlatRate = new NameValue { Name = "Type", Value = FareType.FlatRate.ToString() };
				//        NameValue kvpTypePickup = new NameValue { Name = "Pickup", Value = tfrd.PickupZoneName };
				//        NameValue kvpTypeDropoff = new NameValue { Name = "DropOff", Value = tfrd.DropOffZoneName };

				//        //fpFlatRate.RateQualifier.SpecialInputs.Add(kvpTypeFlatRate);
				//        fpFlatRate.RateQualifier.SpecialInputs.Add(kvpTypePickup);
				//        fpFlatRate.RateQualifier.SpecialInputs.Add(kvpTypeDropoff);
				//        fpFlatRate.ServiceCharges = new List<ServiceCharges>();
				//        ServiceCharges sc_FlatRate_PerDistance = new ServiceCharges { ChargePurpose = new ChargePurpose { Code = FareType.FlatRate.ToString(), Value = "Other_" }, CurrencyCode = tfrd.AmountUnit, Amount = decimal.Parse(tfrd.Amount.ToString()) };
				//        fpFlatRate.ServiceCharges.Add(sc_FlatRate_PerDistance);
				//        response.GroundServices.GroundServices.Add(fpFlatRate);
				//    }
				//*/
				//#endregion

				//if (tgep.EstimationType == EstimationResultType.Info_FlatRate)
				//{

				//    #region 03. Estimation(FlatRate)
				//    GroundService fpFlatRateEstimation = new GroundService();
				//    fpFlatRateEstimation.RateQualifier = new RateQualifier { Category = new Category { Code = FareType.FlatRate.ToString(), Value = "Other_" } };
				//    //fpFlatRateEstimation.RateQualifier.SpecialInputs = new List<KeyValuePair<string, string>>();
				//    fpFlatRateEstimation.RateQualifier.SpecialInputs = new List<NameValue>();
				//    //KeyValuePair<string, string> kvpTypeFlatRateEstimationPickup = new KeyValuePair<string, string>("Pickup", tgep.FlatRateestimation.PickupZone);
				//    //KeyValuePair<string, string> kvpTypeFlatRateEstimationDropoff = new KeyValuePair<string, string>("DropOff", tgep.FlatRateestimation.DropoffZone);

				//    if ((tgep.FlatRateEstimation != null))
				//    {
				//        NameValue kvpTypeFlatRateEstimationPickup = new NameValue { Name = "Pickup", Value = tgep.FlatRateEstimation.PickupZone };
				//        NameValue kvpTypeFlatRateEstimationDropoff = new NameValue { Name = "DropOff", Value = tgep.FlatRateEstimation.DropoffZone };
				//        fpFlatRateEstimation.RateQualifier.SpecialInputs.Add(kvpTypeFlatRateEstimationPickup);
				//        fpFlatRateEstimation.RateQualifier.SpecialInputs.Add(kvpTypeFlatRateEstimationDropoff);
				//        fpFlatRateEstimation.TotalCharge = new TotalCharge { EstimatedTotalAmount = decimal.Parse(tgep.FlatRateEstimation.EstimationTotalAmount.ToString()) };
				//        response.GroundServices.GroundServices.Add(fpFlatRateEstimation);
				//    }
				//    else
				//    {
				//        logger.InfoFormat("No FlatRate estimation");
				//        //throw new ValidationException(Messages.Taxi_GetEstimationFailure);
				//    }
				//    #endregion
				//}
				//else if (tgep.EstimationType == EstimationResultType.Info_Rate)
				//{
				//    #region 04. Estimation(Rate)
				//    GroundService fpRateEstimation = new GroundService();
				//    fpRateEstimation.RateQualifier = new RateQualifier { Category = new Category { Code = FareType.Estimation.ToString(), Value = "Other_" } };
				//    fpRateEstimation.RateQualifier.SpecialInputs = new List<NameValue>();
				//    NameValue kvpTypeFPDistanceUnit = new NameValue { Name = "DistanceUnit", Value = tgep.RateEstimation.DistanceUnit };

				//    NameValue kvpTypeFPDistance;
				//    if (tgep.RateEstimation.DistanceUnit == Map.DTO.DistanceUnit.Mile.ToString())
				//    {
				//        kvpTypeFPDistance = new NameValue { Name = "Distance", Value = tgep.Distance_Mile.ToString() };
				//        fpRateEstimation.RateQualifier.SpecialInputs.Add(kvpTypeFPDistance);
				//    }
				//    else if (tgep.RateEstimation.DistanceUnit == Map.DTO.DistanceUnit.Kilometer.ToString())
				//    {
				//        kvpTypeFPDistance = new NameValue { Name = "Distance", Value = tgep.Distance_Kilometer.ToString() };
				//        fpRateEstimation.RateQualifier.SpecialInputs.Add(kvpTypeFPDistance);
				//    }
				//    fpRateEstimation.RateQualifier.SpecialInputs.Add(kvpTypeFPDistanceUnit);
				//    fpRateEstimation.TotalCharge = new TotalCharge { EstimatedTotalAmount = decimal.Parse(tgep.RateEstimation.EstimationTotalAmount.ToString()) };
				//    response.GroundServices.GroundServices.Add(fpRateEstimation);
				//    #endregion
				//}
				//#endregion

				//#region Write Taxi Log
				//utility.WriteTaxiLog(null, TaxiServiceAPIConst.None, TaxiServiceMethodType.Estimation, null, null, null, null, null);
				//#endregion 
				#endregion


				#region new way
				int fleetTripCode;
				TaxiUtility utility = new TaxiUtility(logger);
				taxi_fleet fleet = null;
				long serviceAPIID = utility.GetServiceAPIIdByRequest(request, TaxiServiceAPIPreferenceConst.GetEstimation, out fleet);
				ITaxiService service = this.GetServiceAPI(serviceAPIID, tokenVTOD, out fleetTripCode);
				response = service.GetEstimation(tokenVTOD, request);
              
                logger.InfoFormat("Start- Calling Method AddEstimationRates");
				this.AddEstimationRates(response, fleet, out test);
				logger.InfoFormat("End- Calling Method AddEstimationRates");
				#endregion

				#region 

				#endregion

				//response.Success = new Success();
			}
			catch (Exception ex)
			{				
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw;
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			response = test;
			return response;
		}
		public void AddEstimationRates(OTA_GroundAvailRS response, taxi_fleet fleet, out OTA_GroundAvailRS test)
		{
			try
			{
				//OTA_GroundAvailRS test=null;
				logger.InfoFormat("Start -- AddEstimatesRates Method");
				decimal? MaxEstimatedRate = 0;
				decimal? MinEstimatedRate = 0;
				decimal? totalAmount = 0;
				logger.InfoFormat("Fleet IsEstimated Property:{0}:", fleet.IsEstimated);
				if (fleet.IsEstimated == true)
				{
					if (response.GroundServices.GroundServices != null)
					{
						foreach (GroundService item in response.GroundServices.GroundServices)
						{
							logger.InfoFormat("Total Charge :{0}:", item.TotalCharge);
							if (item.TotalCharge != null)
							{
								logger.InfoFormat("EstimatedTotalAmount :{0}:", item.TotalCharge.EstimatedTotalAmount);
								if (item.TotalCharge.EstimatedTotalAmount != 0)
								{
									totalAmount = item.TotalCharge.EstimatedTotalAmount;

									if (fleet.MaxEstimatedRate != null)
									{
										MaxEstimatedRate = totalAmount + (totalAmount * (fleet.MaxEstimatedRate / 100));
									}
									logger.InfoFormat("Max Estimated Amount :{0}:", MaxEstimatedRate);
									if (fleet.MinEstimatedRate != null)
									{
										MinEstimatedRate = totalAmount - (totalAmount * fleet.MinEstimatedRate / 100);
									}
									logger.InfoFormat("Min Estimated Amount :{0}:", MinEstimatedRate);
									item.TotalCharge.MaxEstimatedRate = System.Math.Round(MaxEstimatedRate.ToDecimal(), 0);
									item.TotalCharge.MinEstimatedRate = System.Math.Round(MinEstimatedRate.ToDecimal(), 0);
								}

							}
							logger.InfoFormat("GroundService Count:{0}:", response.GroundServices.GroundServices.Count());
						}

					}
				}

				logger.InfoFormat("End -- AddEstimatesRates Method");
				test = response;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw;
			}
		}
		public Common.DTO.OTA.Fleet GetAvailableFleets(TokenRS tokenVTOD, UtilityAvailableFleetsRQ request)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();

			Common.DTO.OTA.Fleet response = null;
			try
			{


				#region Old Way
				////result.FleetTypes.Items.Add(new Common.DTO.OTA.Fleet { Type = FleetType.Taxi.ToString(), Now = true, Later = true });
				//logger.Info("Start");
				//Stopwatch sw = new Stopwatch();
				//sw.Start();
				//Common.DTO.OTA.Fleet result = null;

				//try
				//{
				//	TaxiUtility utility = new TaxiUtility(logger);
				//	bool pickMeUpNow = true;
				//	bool pickMeUpLater = true;
				//	if (utility.ValidateGetAvailableFleets(token, input, transactionID, out pickMeUpNow, out pickMeUpLater))
				//	{
				//		result = new Common.DTO.OTA.Fleet { Type = FleetType.Taxi.ToString(), Now = pickMeUpNow.ToString(), Later = pickMeUpLater.ToString() };
				//	}
				//}
				//catch (Exception ex)
				//{

				//	logger.ErrorFormat("Message:{0}", ex.Message);
				//	logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				//	logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				//	throw;
				//	//if (ex.GetType().Equals(typeof(ValidationException)))
				//	//{
				//	//	throw new ValidationException(ex.Message);
				//	//}
				//	//else
				//	//{
				//	//	throw VtodException.CreateException(ExceptionType.Taxi, 1011);// new TaxiException(Messages.Taxi_Common_UnableToGetAvailableFleets);
				//	//}
				//}


				//sw.Stop();
				//logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
				//return result; 
				#endregion

				#region new way
				int fleetTripCode;
                taxi_fleet f;
                int condition;
                TaxiUtility utility = new TaxiUtility(logger);
				long serviceAPIID = utility.GetServiceAPIIdByRequest(request);
				ITaxiService service = this.GetServiceAPI(serviceAPIID, tokenVTOD, out fleetTripCode);
				response = service.GetAvailableFleets(tokenVTOD, request);
                #region
                if (serviceAPIID == TaxiServiceAPIConst.MTData || serviceAPIID==TaxiServiceAPIConst.Texas || serviceAPIID == TaxiServiceAPIConst.Phoenix)
                {
                    if (utility.ValidateFleets(tokenVTOD, request, out f))
                    {
                        #region Fetching Endpoint Names
                        List<string> endpointNames = utility.MtdataEndpointConfiguration(serviceAPIID, f.Id);
                        #endregion
                        bool disabilityInd = service.GetDisabilityInd(tokenVTOD,f, out condition, endpointNames[0], endpointNames[1]);
                        response.DisabilityVehicleInd = disabilityInd;
                    }
                }
                #endregion
                #region DefaultRadius and  RestrictNowBookingMins and MaxPassengers
                response.MaxPassengers =  System.Configuration.ConfigurationManager.AppSettings["MaxPassengersForTaxi"].ToInt32();
                response.RestrictNowBookingMins = System.Configuration.ConfigurationManager.AppSettings["RestrictNowBookingMins"].ToInt32();
				response.DefaultRadius = System.Configuration.ConfigurationManager.AppSettings["DefaultRadiusForRegular"].ToInt32();
				#endregion
				#endregion
			}
			catch (Exception ex)
			{				
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);

				//if (ex.GetType().Equals(typeof(ValidationException)))
				//{
				//	throw new ValidationException(ex.Message);
				//}
				//else
				//{
				//	throw VtodException.CreateException(ExceptionType.Taxi, 3001); // new TaxiException(Messages.Taxi_StatusFailure);
				//}
				throw;
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return response;
		}
		public UtilityNotifyDriverRS NotifyDriver(TokenRS token, UtilityNotifyDriverRQ input)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			UtilityNotifyDriverRS rs = new UtilityNotifyDriverRS();

			try
			{
				#region old way
				//TaxiUtility utility = new TaxiUtility(logger);
				//TaxiSendMessageToVehicleParameter tsmtvp = new TaxiSendMessageToVehicleParameter();

				//if (utility.ValidateSendMessageToVehicle(token, input, out tsmtvp))
				//{
				//    #region  Choose ServiceAPI
				//    ITaxiService service = this.GetServiceAPI(tsmtvp.Fleet.taxi_fleet_api_reference.FirstOrDefault().ServiceAPIId, token);
				//    #endregion


				//    //UDI33 only
				//    bool result = service.SendMessageToVehicle(tsmtvp);
				//    if (result)
				//    {
				//        rs.Success = new Success();
				//    }
				//    else
				//    {
				//        throw VtodException.CreateException(ExceptionType.Taxi, 9003);// new TaxiException(Messages.Taxi_UnableToSendMessageToVehicle);
				//    }
				//}
				//else
				//{
				//    throw VtodException.CreateException(ExceptionType.Taxi, 9003);//throw new TaxiException(Messages.Taxi_UnableToSendMessageToVehicle);
				//} 
				//#endregion
				#endregion
				int fleetTripCode;
				#region new way
				TaxiUtility utility = new TaxiUtility(logger);
				long serviceAPIID = utility.GetServiceAPIIdByRequest(input);
				ITaxiService service = this.GetServiceAPI(serviceAPIID, token, out fleetTripCode);
				service.NotifyDriver(token, input);

				#endregion
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);

				throw;				
			}

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return rs;
		}

		public UtilityNotifyDriverRS NotifyPickupDriver(TokenRS token, UtilityNotifyDriverRQ input)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			UtilityNotifyDriverRS rs = new UtilityNotifyDriverRS();

			try
			{
				int fleetTripCode;
				#region new way
				TaxiUtility utility = new TaxiUtility(logger);
				long serviceAPIID = utility.GetServiceAPIIdByRequest(input);
				ITaxiService service = this.GetServiceAPI(serviceAPIID, token, out fleetTripCode);
				service.NotifyPickupDriver(token, input);

				#endregion
			}
			catch (Exception ex)
			{

				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);

				throw;
			}

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return rs;
		}

        public UtilityNotifyDriverRS NotifyDispatchPendingDriver(TokenRS token, UtilityNotifyDriverRQ input)
        {
            logger.Info("Start");
            Stopwatch sw = new Stopwatch();
            sw.Start();
            UtilityNotifyDriverRS rs = new UtilityNotifyDriverRS();

            try
            {
                int fleetTripCode;
                #region new way
                TaxiUtility utility = new TaxiUtility(logger);
                long serviceAPIID = utility.GetServiceAPIIdByRequest(input);
                ITaxiService service = this.GetServiceAPI(serviceAPIID, token, out fleetTripCode);
                service.NotifyDispatchPendingDriver(token, input);

                #endregion
            }
            catch (Exception ex)
            {

                logger.ErrorFormat("Message:{0}", ex.Message);
                logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
                logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);

                throw;
            }

            sw.Stop();
            logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
            return rs;
        }


		public GetFareDetailRS GetFareDetail(TokenRS tokenVTOD, GetFareDetailRQ request)
		{
			logger.Info("Start");
			var accountingDomain = new Accounting.AccountingDomain(); accountingDomain.TrackTime = TrackTime;

			try
			{

				TaxiUtility utility = new TaxiUtility(logger);
				long serviceAPIID = utility.GetServiceAPIIdByRequest(request);
				//ITaxiService service = this.GetServiceAPI(serviceAPIID, tokenVTOD);



				GetFareDetailRS result = null;

				if (serviceAPIID == TaxiServiceAPIConst.SDS)
				{
					result = accountingDomain.GetFareDetail(tokenVTOD, request);
				}
				else
				{
					//For Test
					//confirmationID = 1000020;
					vtod_trip trip = null;
					string paymentType = null;// string.Empty;



					if (request.Reference.First().Type == ConfirmationType.Confirmation.ToString())
					{
						long confirmationID = request.Reference.First().ID.ToInt64();
						using (var db = new DataAccess.VTOD.VTODEntities())
						{
							var trips = db.vtod_trip.Include("taxi_trip").Where(s => s.Id == confirmationID).ToList();
							if (trips != null && trips.Any())
							{
								var vtodTrip = trips.First();
								if (vtodTrip.FleetType == Common.DTO.Enum.FleetType.Taxi.ToString())
								{
									trip = vtodTrip;
									paymentType = trip.PaymentType;
								}
								//if (vtodTrip.GetType() == typeof(sds_trip))
								//{
								//	trip = vtodTrip;
								//}
							}
						}
					}
					else if (request.Reference.First().Type == ConfirmationType.DispatchConfirmation.ToString())
					{
						var dispatchConfirmationID = request.Reference.First().ID;
						var dispatchFleetID = request.Reference.First().ID_Context.ToInt64();
						using (var db = new DataAccess.VTOD.VTODEntities())
						{
							//trip = db.vtod_trip.OfType<taxi_trip>().Where(s => s.FleetId == dispatchFleetID && s.DispatchTripId == dispatchConfirmationID).FirstOrDefault();

							long tripId = db.taxi_trip.Where(x => x.FleetId == dispatchFleetID && x.DispatchTripId == dispatchConfirmationID).Select(x => x.Id).FirstOrDefault();
							//TaxiUtility utility = new TaxiUtility(logger);
							trip = utility.GetTaxiTrip(tripId);

						}

					}


					if (trip != null)
					{
						#region Make result
						result = new GetFareDetailRS();
						result.FareItems = new FareItems();// = new List<NameValue>();
						result.FareItems.Items = new List<NameValue>();
						//result.FareItem = new List<NameValue>();
						result.Success = new Success();
						result.TripInfo = new Common.DTO.OTA.TripInfo();
						result.PaymentInfo = new PaymentInfo { Type = paymentType };
						decimal? creditsApplied = null;
						decimal? charged = null;
						string transactionNumber = null;
						DateTime? transactionTime = null;
						string transactionError = null;
						using (var db = new DataAccess.VTOD.VTODEntities())
						{
							var taxi_trip_status = db.taxi_trip_status.Where(s => s.TripID == trip.Id && s.VehicleLatitude != null && s.VehicleLongitude != null).OrderBy(p => p.Id).ToList();
							if (taxi_trip_status != null)
							{
								if (taxi_trip_status.Any())
								{
									result.DropOffLatitude = taxi_trip_status.FirstOrDefault().VehicleLatitude.ToString();
									result.DropOffLongitude = taxi_trip_status.FirstOrDefault().VehicleLongitude.ToString();
								}
							}
						}
						//long? vtodTripID = null;

                        if (trip.IsSplitPayment.HasValue && trip.IsSplitPayment.Value
                            && trip.SplitPaymentStatus.HasValue 
                            && (trip.SplitPaymentStatus.Value == (int)SplitPaymentStatus.Accepted || trip.SplitPaymentStatus.Value == (int)SplitPaymentStatus.Completed))  // split payment
                        {
                            result.SplitMemberInfo = new SplitPaymentInfo();
                            result.SplitMemberInfo.MemberA = new SplitPaymentUserInfo
                            {
                                MemberID = trip.MemberID,
                                FirstName = trip.taxi_trip.FirstName,
                                LastName = trip.taxi_trip.LastName
                            };
                            result.SplitMemberInfo.MemberB = new SplitPaymentUserInfo
                            {
                                MemberID = trip.SplitPaymentMemberB,
                                FirstName = trip.taxi_trip.MemberBFirstName,
                                LastName = trip.taxi_trip.MemberBLastName
                            };

                            result.SplitRideInfo = new List<SplitPaymentFare>();
                            result.SplitRideInfo.Add(new SplitPaymentFare
                            {
                                FareItems = new FareItems
                                {
                                    Items = new List<NameValue>()
                               }
                            });

                            #region Gratuity Rate
                            if (trip.GratuityRate.HasValue)
                            {
                                var gratuityRate = new NameValue();
                                gratuityRate.Name = "GratuityRate";
                                gratuityRate.Value = trip.GratuityRate.Value.ToString();
                                result.FareItems.Items.Add(gratuityRate);

                                result.SplitRideInfo[0].FareItems.Items.Add(gratuityRate);
                            }
                            #endregion

                            #region Gratuity
                            if (trip.Gratuity.HasValue)
                            {
                                var gratuity = new NameValue();
                                gratuity.Name = "Gratuity";
                                gratuity.Value = decimal.Round(trip.Gratuity.Value / 2, 2).ToString();
                                result.FareItems.Items.Add(gratuity);

                                result.SplitRideInfo[0].FareItems.Items.Add(gratuity);
                            }
                            #endregion

                            #region Fare
                            if (trip.TotalFareAmount.HasValue)
                            {
                                var total = new NameValue();
                                total.Name = "Fare";
                                if (trip.Gratuity.HasValue)
                                    total.Value = decimal.Round((trip.TotalFareAmount.Value - trip.Gratuity.Value) / 2, 2).ToString();
                                else
                                    total.Value = decimal.Round(trip.TotalFareAmount.Value / 2, 2).ToString();
                                result.FareItems.Items.Add(total);

                                result.SplitRideInfo[0].FareItems.Items.Add(total);
                            }
                            #endregion

                            if (!string.IsNullOrWhiteSpace(trip.MemberBCreditCardID))
                            {
                                var rq = new AccountingGetMemberPaymentCardsRQ { MemberId = trip.SplitPaymentMemberB.ToInt32() };
                                var memberPaymentCards = accountingDomain.AccountingGetMemberPaymentCards(tokenVTOD, rq);
                                if (memberPaymentCards != null && memberPaymentCards.PaymentCards != null && memberPaymentCards.PaymentCards.Any())
                                {
                                    foreach (var item in memberPaymentCards.PaymentCards)
                                    {
                                        if (item.CardNumber.ID == trip.MemberBCreditCardID)
                                        {
                                            result.SplitRideInfo[0].PaymentInfo = new PaymentInfo
                                            {
                                                PaymentCard = item,
                                                Type = paymentType
                                            };

                                            break;
                                        }
                                    }
                                }
                            }

                            #region zTrip Credit, transactionNumber, error, transactionTime
                            using (var db = new DataAccess.VTOD.VTODEntities())
                            {
                                try
                                {
                                    var tripAccountTransactionForMemberA = db.vtod_trip_account_transaction.Where(s => s.TripID == trip.Id && s.MemberID == trip.MemberID).FirstOrDefault();
                                    var tripDriverInfo = db.vtod_driver_info.Where(s => s.Id == trip.DriverInfoID).OrderByDescending(s => s.Id).FirstOrDefault();

                                    if (tripAccountTransactionForMemberA != null && tripAccountTransactionForMemberA.CreditsApplied.HasValue && tripAccountTransactionForMemberA.CreditsApplied.Value != 0)
                                    {
                                        creditsApplied = -System.Math.Abs(tripAccountTransactionForMemberA.CreditsApplied.Value);
                                    }

                                    decimal? creditsAppliedForMemberB = null;
                                    var tripAccountTransactionForMemberB = db.vtod_trip_account_transaction.Where(s => s.TripID == trip.Id && s.MemberID == trip.SplitPaymentMemberB).FirstOrDefault();
                                    if (tripAccountTransactionForMemberB.CreditsApplied.HasValue && tripAccountTransactionForMemberB.CreditsApplied.Value != 0)
                                    {
                                        creditsAppliedForMemberB = -System.Math.Abs(tripAccountTransactionForMemberB.CreditsApplied.Value);
                                    }

                                    //This section is for TCS.
                                    if (request.Reference.First().Type == ConfirmationType.DispatchConfirmation.ToString())
                                    {
                                        #region Account Transaction
                                        if (tripAccountTransactionForMemberA != null)
                                        {
                                            transactionTime = tripAccountTransactionForMemberA.AppendTime;
                                        }

                                        if (tripAccountTransactionForMemberA != null && tripAccountTransactionForMemberA.AmountCharged.HasValue && tripAccountTransactionForMemberA.AmountCharged.Value != 0)
                                        {
                                            charged = -System.Math.Abs(tripAccountTransactionForMemberA.AmountCharged.Value);
                                        }
                                        if (tripAccountTransactionForMemberA != null && !string.IsNullOrWhiteSpace(tripAccountTransactionForMemberA.TransactionNumber))
                                        {
                                            transactionNumber = tripAccountTransactionForMemberA.TransactionNumber;
                                        }
                                        if (tripAccountTransactionForMemberA != null && !string.IsNullOrWhiteSpace(tripAccountTransactionForMemberA.Error))
                                        {
                                            transactionError = tripAccountTransactionForMemberA.Error;
                                        }


                                        if (creditsApplied.HasValue && creditsApplied != 0)
                                        {
                                            var zTripCreditKeyValue = new NameValue();
                                            zTripCreditKeyValue.Name = "zTrip Credit";
                                            zTripCreditKeyValue.Value = creditsApplied.ToString();
                                            result.FareItems.Items.Add(zTripCreditKeyValue);
                                        }

                                        if (!string.IsNullOrWhiteSpace(transactionNumber))
                                        {
                                            var transactionNumberKeyValue = new NameValue();
                                            transactionNumberKeyValue.Name = "TransactionNumber";
                                            transactionNumberKeyValue.Value = transactionNumber;
                                            result.FareItems.Items.Add(transactionNumberKeyValue);
                                        }
                                        if (transactionTime.HasValue)
                                        {
                                            var transactionTimeKeyValue = new NameValue();
                                            transactionTimeKeyValue.Name = "TransactionTime";
                                            transactionTimeKeyValue.Value = transactionTime.ToString();
                                            result.FareItems.Items.Add(transactionTimeKeyValue);
                                        }
                                        if (charged.HasValue)
                                        {
                                            var chargedTimeKeyValue = new NameValue();
                                            chargedTimeKeyValue.Name = "AmountCharged";
                                            chargedTimeKeyValue.Value = charged.ToString();
                                            result.FareItems.Items.Add(chargedTimeKeyValue);
                                        }
                                        if (!string.IsNullOrEmpty(transactionError))
                                        {
                                            var errorKeyValue = new NameValue();
                                            errorKeyValue.Name = "TransactionError";
                                            errorKeyValue.Value = transactionError.ToString();
                                            result.FareItems.Items.Add(errorKeyValue);
                                        }
                                        //if (!string.IsNullOrEmpty(transactionError))
                                        //{
                                        var confirmationeyValue = new NameValue();
                                        confirmationeyValue.Name = "VtodTripId";
                                        confirmationeyValue.Value = trip.Id.ToString();
                                        result.FareItems.Items.Add(confirmationeyValue);
                                        //}
                                        if (!string.IsNullOrWhiteSpace(trip.MemberID))
                                        {
                                            var memberIDValue = new NameValue();
                                            memberIDValue.Name = "MemberId";
                                            memberIDValue.Value = trip.MemberID;
                                            result.FareItems.Items.Add(memberIDValue);
                                        }
                                        if (!string.IsNullOrWhiteSpace(trip.CreditCardID))
                                        {
                                            var creditCardIDValue = new NameValue();
                                            creditCardIDValue.Name = "CreditCardId";
                                            creditCardIDValue.Value = trip.CreditCardID;
                                            result.FareItems.Items.Add(creditCardIDValue);
                                        }
                                        if (!string.IsNullOrWhiteSpace(trip.PaymentType))
                                        {
                                            var paymentTypeValue = new NameValue();
                                            paymentTypeValue.Name = "PaymentType";
                                            paymentTypeValue.Value = trip.PaymentType;
                                            result.FareItems.Items.Add(paymentTypeValue);
                                        }
                                        #endregion

                                        #region DriverInfo
                                        if (tripDriverInfo != null)
                                        {
                                            if (!string.IsNullOrWhiteSpace(tripDriverInfo.VehicleID))
                                            {
                                                var chargedTimeKeyValue = new NameValue();
                                                chargedTimeKeyValue.Name = "VehicleID";
                                                chargedTimeKeyValue.Value = tripDriverInfo.VehicleID;
                                                result.FareItems.Items.Add(chargedTimeKeyValue);
                                            }

                                            if (!string.IsNullOrWhiteSpace(tripDriverInfo.DriverID))
                                            {
                                                var chargedTimeKeyValue = new NameValue();
                                                chargedTimeKeyValue.Name = "DriverID";
                                                chargedTimeKeyValue.Value = tripDriverInfo.DriverID;
                                                result.FareItems.Items.Add(chargedTimeKeyValue);
                                            }

                                            if (!string.IsNullOrWhiteSpace(tripDriverInfo.FirstName))
                                            {
                                                var chargedTimeKeyValue = new NameValue();
                                                chargedTimeKeyValue.Name = "FirstName";
                                                chargedTimeKeyValue.Value = tripDriverInfo.FirstName;
                                                result.FareItems.Items.Add(chargedTimeKeyValue);
                                            }

                                            if (!string.IsNullOrWhiteSpace(tripDriverInfo.LastName))
                                            {
                                                var chargedTimeKeyValue = new NameValue();
                                                chargedTimeKeyValue.Name = "LastName";
                                                chargedTimeKeyValue.Value = tripDriverInfo.LastName;
                                                result.FareItems.Items.Add(chargedTimeKeyValue);
                                            }



                                        }
                                        #endregion

                                        #region Source and Device
                                        if (!string.IsNullOrWhiteSpace(trip.ConsumerSource))
                                        {
                                            var keyValue = new NameValue();
                                            keyValue.Name = "Source";
                                            keyValue.Value = trip.ConsumerSource;
                                            result.FareItems.Items.Add(keyValue);
                                        }

                                        if (!string.IsNullOrWhiteSpace(trip.ConsumerDevice))
                                        {
                                            var keyValue = new NameValue();
                                            keyValue.Name = "Device";
                                            keyValue.Value = trip.ConsumerDevice;
                                            result.FareItems.Items.Add(keyValue);
                                        }


                                        #endregion

                                        #region Account Transaction for Member B
                                        if (tripAccountTransactionForMemberB != null)
                                        {
                                            decimal? chargedForMemberB = null;
                                            string transactionNumberForMemberB = null;
                                            DateTime? transactionTimeForMemberB = null;
                                            string transactionErrorForMemberB = null;

                                            transactionTimeForMemberB = tripAccountTransactionForMemberB.AppendTime;

                                            if (tripAccountTransactionForMemberB.AmountCharged.HasValue && tripAccountTransactionForMemberB.AmountCharged.Value != 0)
                                            {
                                                chargedForMemberB = -System.Math.Abs(tripAccountTransactionForMemberB.AmountCharged.Value);
                                            }
                                            if (!string.IsNullOrWhiteSpace(tripAccountTransactionForMemberB.TransactionNumber))
                                            {
                                                transactionNumberForMemberB = tripAccountTransactionForMemberB.TransactionNumber;
                                            }
                                            if (!string.IsNullOrWhiteSpace(tripAccountTransactionForMemberB.Error))
                                            {
                                                transactionErrorForMemberB = tripAccountTransactionForMemberB.Error;
                                            }


                                            if (creditsAppliedForMemberB.HasValue && creditsAppliedForMemberB != 0)
                                            {
                                                var zTripCreditKeyValue = new NameValue();
                                                zTripCreditKeyValue.Name = "zTrip Credit";
                                                zTripCreditKeyValue.Value = creditsAppliedForMemberB.ToString();
                                                result.SplitRideInfo[0].FareItems.Items.Add(zTripCreditKeyValue);
                                            }

                                            if (!string.IsNullOrWhiteSpace(transactionNumberForMemberB))
                                            {
                                                var transactionNumberKeyValue = new NameValue();
                                                transactionNumberKeyValue.Name = "TransactionNumber";
                                                transactionNumberKeyValue.Value = transactionNumberForMemberB;
                                                result.SplitRideInfo[0].FareItems.Items.Add(transactionNumberKeyValue);
                                            }
                                            if (transactionTimeForMemberB.HasValue)
                                            {
                                                var transactionTimeKeyValue = new NameValue();
                                                transactionTimeKeyValue.Name = "TransactionTime";
                                                transactionTimeKeyValue.Value = transactionTimeForMemberB.ToString();
                                                result.SplitRideInfo[0].FareItems.Items.Add(transactionTimeKeyValue);
                                            }
                                            if (chargedForMemberB.HasValue)
                                            {
                                                var chargedTimeKeyValue = new NameValue();
                                                chargedTimeKeyValue.Name = "AmountCharged";
                                                chargedTimeKeyValue.Value = chargedForMemberB.ToString();
                                                result.SplitRideInfo[0].FareItems.Items.Add(chargedTimeKeyValue);
                                            }
                                            if (!string.IsNullOrEmpty(transactionErrorForMemberB))
                                            {
                                                var errorKeyValue = new NameValue();
                                                errorKeyValue.Name = "TransactionError";
                                                errorKeyValue.Value = transactionErrorForMemberB.ToString();
                                                result.SplitRideInfo[0].FareItems.Items.Add(errorKeyValue);
                                            }

                                            confirmationeyValue = new NameValue();
                                            confirmationeyValue.Name = "VtodTripId";
                                            confirmationeyValue.Value = trip.Id.ToString();
                                            result.SplitRideInfo[0].FareItems.Items.Add(confirmationeyValue);

                                            if (!string.IsNullOrWhiteSpace(trip.SplitPaymentMemberB))
                                            {
                                                var memberIDValue = new NameValue();
                                                memberIDValue.Name = "MemberId";
                                                memberIDValue.Value = trip.SplitPaymentMemberB;
                                                result.SplitRideInfo[0].FareItems.Items.Add(memberIDValue);
                                            }
                                            if (!string.IsNullOrWhiteSpace(trip.MemberBCreditCardID))
                                            {
                                                var creditCardIDValue = new NameValue();
                                                creditCardIDValue.Name = "CreditCardId";
                                                creditCardIDValue.Value = trip.MemberBCreditCardID;
                                                result.SplitRideInfo[0].FareItems.Items.Add(creditCardIDValue);
                                            }
                                            if (!string.IsNullOrWhiteSpace(trip.PaymentType))
                                            {
                                                var paymentTypeValue = new NameValue();
                                                paymentTypeValue.Name = "PaymentType";
                                                paymentTypeValue.Value = trip.PaymentType;
                                                result.SplitRideInfo[0].FareItems.Items.Add(paymentTypeValue);
                                            }
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        if (creditsApplied.HasValue && creditsApplied != 0)
                                        {
                                            var zTripCreditKeyValue = new NameValue();
                                            zTripCreditKeyValue.Name = "zTrip Credit";
                                            zTripCreditKeyValue.Value = creditsApplied.ToString();
                                            result.FareItems.Items.Add(zTripCreditKeyValue);
                                        }

                                        if (creditsAppliedForMemberB.HasValue && creditsAppliedForMemberB != 0)
                                        {
                                            var zTripCreditKeyValue = new NameValue();
                                            zTripCreditKeyValue.Name = "zTrip Credit";
                                            zTripCreditKeyValue.Value = creditsAppliedForMemberB.ToString();
                                            result.SplitRideInfo[0].FareItems.Items.Add(zTripCreditKeyValue);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {

                                    logger.Error(ex);
                                }

                            }
                            #endregion

                        }
                        else
                        {
                            #region Gratuity Rate
                            if (trip.GratuityRate.HasValue)
                            {
                                var gratuityRate = new NameValue();
                                gratuityRate.Name = "GratuityRate";
                                gratuityRate.Value = trip.GratuityRate.Value.ToString();
                                result.FareItems.Items.Add(gratuityRate);
                            }
                            #endregion

                            #region Gratuity
                            if (trip.Gratuity.HasValue)
                            {
                                var gratuity = new NameValue();
                                gratuity.Name = "Gratuity";
                                gratuity.Value = trip.Gratuity.Value.ToString();
                                result.FareItems.Items.Add(gratuity);
                            }
                            #endregion

                            #region Fare
                            if (trip.TotalFareAmount.HasValue)
                            {
                                var total = new NameValue();
                                total.Name = "Fare";
                                if (trip.Gratuity.HasValue)
                                    total.Value = (trip.TotalFareAmount.Value - trip.Gratuity.Value).ToString();
                                else
                                    total.Value = trip.TotalFareAmount.Value.ToString();
                                result.FareItems.Items.Add(total);
                            }
                            #endregion

                            #region zTrip Credit, transactionNumber, error, transactionTime
                            //if (trip.ChargeStatus == Common.DTO.Enum.ChargeStatus.Charged.ToString())
                            //{
                            using (var db = new DataAccess.VTOD.VTODEntities())
                            {

                                try
                                {
                                    var tripAccountTransactions = db.vtod_trip_account_transaction.Where(s => s.TripID == trip.Id).FirstOrDefault();
                                    var tripDriverInfo = db.vtod_driver_info.Where(s => s.Id == trip.DriverInfoID).OrderByDescending(s => s.Id).FirstOrDefault();

                                    if (tripAccountTransactions != null && tripAccountTransactions.CreditsApplied.HasValue && tripAccountTransactions.CreditsApplied.Value != 0)
                                    {
                                        creditsApplied = -System.Math.Abs(tripAccountTransactions.CreditsApplied.Value);
                                    }

                                    //This section is for TCS.
                                    if (request.Reference.First().Type == ConfirmationType.DispatchConfirmation.ToString())
                                    {


                                        #region Account Transaction
                                        if (tripAccountTransactions != null)
                                        {
                                            transactionTime = tripAccountTransactions.AppendTime;
                                        }

                                        if (tripAccountTransactions != null && tripAccountTransactions.AmountCharged.HasValue && tripAccountTransactions.AmountCharged.Value != 0)
                                        {
                                            charged = -System.Math.Abs(tripAccountTransactions.AmountCharged.Value);
                                        }
                                        if (tripAccountTransactions != null && !string.IsNullOrWhiteSpace(tripAccountTransactions.TransactionNumber))
                                        {
                                            transactionNumber = tripAccountTransactions.TransactionNumber;
                                        }
                                        if (tripAccountTransactions != null && !string.IsNullOrWhiteSpace(tripAccountTransactions.Error))
                                        {
                                            transactionError = tripAccountTransactions.Error;
                                        }


                                        if (creditsApplied.HasValue && creditsApplied != 0)
                                        {
                                            var zTripCreditKeyValue = new NameValue();
                                            zTripCreditKeyValue.Name = "zTrip Credit";
                                            zTripCreditKeyValue.Value = creditsApplied.ToString();
                                            result.FareItems.Items.Add(zTripCreditKeyValue);
                                        }

                                        if (!string.IsNullOrWhiteSpace(transactionNumber))
                                        {
                                            var transactionNumberKeyValue = new NameValue();
                                            transactionNumberKeyValue.Name = "TransactionNumber";
                                            transactionNumberKeyValue.Value = transactionNumber;
                                            result.FareItems.Items.Add(transactionNumberKeyValue);
                                        }
                                        if (transactionTime.HasValue)
                                        {
                                            var transactionTimeKeyValue = new NameValue();
                                            transactionTimeKeyValue.Name = "TransactionTime";
                                            transactionTimeKeyValue.Value = transactionTime.ToString();
                                            result.FareItems.Items.Add(transactionTimeKeyValue);
                                        }
                                        if (charged.HasValue)
                                        {
                                            var chargedTimeKeyValue = new NameValue();
                                            chargedTimeKeyValue.Name = "AmountCharged";
                                            chargedTimeKeyValue.Value = charged.ToString();
                                            result.FareItems.Items.Add(chargedTimeKeyValue);
                                        }
                                        if (!string.IsNullOrEmpty(transactionError))
                                        {
                                            var errorKeyValue = new NameValue();
                                            errorKeyValue.Name = "TransactionError";
                                            errorKeyValue.Value = transactionError.ToString();
                                            result.FareItems.Items.Add(errorKeyValue);
                                        }
                                        //if (!string.IsNullOrEmpty(transactionError))
                                        //{
                                        var confirmationeyValue = new NameValue();
                                        confirmationeyValue.Name = "VtodTripId";
                                        confirmationeyValue.Value = trip.Id.ToString();
                                        result.FareItems.Items.Add(confirmationeyValue);
                                        //}
                                        if (!string.IsNullOrWhiteSpace(trip.MemberID))
                                        {
                                            var memberIDValue = new NameValue();
                                            memberIDValue.Name = "MemberId";
                                            memberIDValue.Value = trip.MemberID;
                                            result.FareItems.Items.Add(memberIDValue);
                                        }
                                        if (!string.IsNullOrWhiteSpace(trip.CreditCardID))
                                        {
                                            var creditCardIDValue = new NameValue();
                                            creditCardIDValue.Name = "CreditCardId";
                                            creditCardIDValue.Value = trip.CreditCardID;
                                            result.FareItems.Items.Add(creditCardIDValue);
                                        }
                                        if (!string.IsNullOrWhiteSpace(trip.PaymentType))
                                        {
                                            var paymentTypeValue = new NameValue();
                                            paymentTypeValue.Name = "PaymentType";
                                            paymentTypeValue.Value = trip.PaymentType;
                                            result.FareItems.Items.Add(paymentTypeValue);
                                        }
                                        #endregion

                                        #region DriverInfo
                                        if (tripDriverInfo != null)
                                        {
                                            if (!string.IsNullOrWhiteSpace(tripDriverInfo.VehicleID))
                                            {
                                                var chargedTimeKeyValue = new NameValue();
                                                chargedTimeKeyValue.Name = "VehicleID";
                                                chargedTimeKeyValue.Value = tripDriverInfo.VehicleID;
                                                result.FareItems.Items.Add(chargedTimeKeyValue);
                                            }

                                            if (!string.IsNullOrWhiteSpace(tripDriverInfo.DriverID))
                                            {
                                                var chargedTimeKeyValue = new NameValue();
                                                chargedTimeKeyValue.Name = "DriverID";
                                                chargedTimeKeyValue.Value = tripDriverInfo.DriverID;
                                                result.FareItems.Items.Add(chargedTimeKeyValue);
                                            }

                                            if (!string.IsNullOrWhiteSpace(tripDriverInfo.FirstName))
                                            {
                                                var chargedTimeKeyValue = new NameValue();
                                                chargedTimeKeyValue.Name = "FirstName";
                                                chargedTimeKeyValue.Value = tripDriverInfo.FirstName;
                                                result.FareItems.Items.Add(chargedTimeKeyValue);
                                            }

                                            if (!string.IsNullOrWhiteSpace(tripDriverInfo.LastName))
                                            {
                                                var chargedTimeKeyValue = new NameValue();
                                                chargedTimeKeyValue.Name = "LastName";
                                                chargedTimeKeyValue.Value = tripDriverInfo.LastName;
                                                result.FareItems.Items.Add(chargedTimeKeyValue);
                                            }



                                        }
                                        #endregion

                                        #region Source and Device
                                        if (!string.IsNullOrWhiteSpace(trip.ConsumerSource))
                                        {
                                            var keyValue = new NameValue();
                                            keyValue.Name = "Source";
                                            keyValue.Value = trip.ConsumerSource;
                                            result.FareItems.Items.Add(keyValue);
                                        }

                                        if (!string.IsNullOrWhiteSpace(trip.ConsumerDevice))
                                        {
                                            var keyValue = new NameValue();
                                            keyValue.Name = "Device";
                                            keyValue.Value = trip.ConsumerDevice;
                                            result.FareItems.Items.Add(keyValue);
                                        }


                                        #endregion
                                    }
                                    else
                                    {
                                        if (creditsApplied.HasValue && creditsApplied != 0)
                                        {
                                            var zTripCreditKeyValue = new NameValue();
                                            zTripCreditKeyValue.Name = "zTrip Credit";
                                            zTripCreditKeyValue.Value = creditsApplied.ToString();
                                            result.FareItems.Items.Add(zTripCreditKeyValue);
                                        }

                                    }
                                }
                                catch (Exception ex)
                                {

                                    logger.Error(ex);
                                }

                            }

                            //}
                            #endregion
                        }

                        if (trip.TripStartTime.HasValue && trip.TripEndTime.HasValue)
						{
							//var duration = new NameValue();
							//duration.Name = "Duration";
							TimeSpan ts = trip.TripEndTime.Value.Subtract(trip.TripStartTime.Value).Duration();
							//duration.Value = string.Format("{0:00}:{1:00}:{2:00}", ts.Hours,ts.Minutes,ts.Seconds);


							result.TripInfo.Duration = string.Format("{0:00}:{1:00}:{2:00}", ts.Hours, ts.Minutes, ts.Seconds);
						}
						#endregion

						accountingDomain.CompleteFareDetail(tokenVTOD, result, request, trip);

					}
					else
					{
						throw VtodException.CreateException(ExceptionType.Taxi, 1012); //new Exception(Messages.Taxi_TripNotFound);
					}
				}


				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("AccountingDomain.GetFareDetail:: {0}", ex.ToString() + ex.StackTrace);

				throw;
			}
		}

        public OTA_GroundGetFinalRouteRS GetFinalRoute(TokenRS tokenVTOD, OTA_GroundGetFinalRouteRQ request)
        {
            logger.Info("Start");
            Stopwatch sw = new Stopwatch();
            sw.Start();
            OTA_GroundGetFinalRouteRS response = null;
            try
            {
                #region new way
                int fleetTripCode;
                TaxiUtility utility = new TaxiUtility(logger);
                long serviceAPIID = utility.GetServiceAPIIdByRequest(request);
                ITaxiService service = this.GetServiceAPI(serviceAPIID, tokenVTOD, out fleetTripCode);
                response = service.GetFinalRoute(tokenVTOD, request);
                #endregion
                response.Success = new Success();

                #region Common
                response.EchoToken = request.EchoToken;
                response.PrimaryLangID = request.PrimaryLangID;
                response.Target = request.Target;
                response.Version = request.Version;
                #endregion

            }
            catch (Exception ex)
            {               
            }
            sw.Stop();
            logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
            return response;
        }

        public UtilityCustomerInfoRS GetCustomerInfo(TokenRS token, UtilityCustomerInfoRQ input)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			UtilityCustomerInfoRS rs = new UtilityCustomerInfoRS();
			int fleetTripCode = -1;
			try
			{


				TaxiUtility utility = new TaxiUtility(logger);
				long serviceAPIID = utility.GetServiceAPIIdByRequest(input);
				ITaxiService service = this.GetServiceAPI(serviceAPIID, token, out fleetTripCode);
				rs = service.GetCustomerInfo(token, input);

			}
			catch (Exception ex)
			{

				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);

				throw;
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return rs;
		}
		public UtilityGetLastTripRS GetLastTrip(TokenRS token, UtilityGetLastTripRQ input)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			UtilityGetLastTripRS rs = new UtilityGetLastTripRS();
			int fleetTripCode = -1;
			try
			{


				TaxiUtility utility = new TaxiUtility(logger);
				long serviceAPIID = utility.GetServiceAPIIdByRequest(input);
				ITaxiService service = this.GetServiceAPI(serviceAPIID, token, out fleetTripCode);
				rs = service.GetLastTrip(token, input);

			}
			catch (Exception ex)
			{

				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);

				throw;
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return rs;
		}
		public UtilityGetPickupAddressRS GetPickupAddress(TokenRS token, UtilityGetPickupAddressRQ input)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			UtilityGetPickupAddressRS rs = new UtilityGetPickupAddressRS();
			int fleetTripCode = -1;
			try
			{


				TaxiUtility utility = new TaxiUtility(logger);
				long serviceAPIID = utility.GetServiceAPIIdByRequest(input);
				ITaxiService service = this.GetServiceAPI(serviceAPIID, token, out fleetTripCode);
				rs = service.GetPickupAddress(token, input);

		}
			catch (Exception ex)
			{

				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);

				throw;
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return rs;
		}
		public UtilityGetBlackoutPeriodRS GetBlackoutPeriod(TokenRS token, UtilityGetBlackoutPeriodRQ input)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			UtilityGetBlackoutPeriodRS rs = new UtilityGetBlackoutPeriodRS();
			int fleetTripCode = -1;
			try
			{


				TaxiUtility utility = new TaxiUtility(logger);
				long serviceAPIID = utility.GetServiceAPIIdByRequest(input);
				ITaxiService service = this.GetServiceAPI(serviceAPIID, token, out fleetTripCode);
				rs = service.GetBlackoutPeriod(token, input);

			}
			catch (Exception ex)
			{

				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);

				throw;
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return rs;
		}
		public UtilityGetNotificationRS GetStaleNotification(TokenRS token, UtilityGetNotificationRQ input)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			UtilityGetNotificationRS rs = new UtilityGetNotificationRS();
			int fleetTripCode = -1;
			try
			{


				TaxiUtility utility = new TaxiUtility(logger);
				long serviceAPIID = utility.GetServiceAPIIdByRequest(input, OutboundNotificationType.StaleNotification);
				ITaxiService service = this.GetServiceAPI(serviceAPIID, token, out fleetTripCode);
				rs = service.GetStaleNotification(token, input);

		}
			catch (Exception ex)
			{

				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);

				throw;
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return rs;
		}
		public UtilityGetNotificationRS GetArrivalNotification(TokenRS token, UtilityGetNotificationRQ input)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			UtilityGetNotificationRS rs = new UtilityGetNotificationRS();
			int fleetTripCode = -1;
			try
			{


				TaxiUtility utility = new TaxiUtility(logger);
				long serviceAPIID = utility.GetServiceAPIIdByRequest(input, OutboundNotificationType.ArrivalNotification);
				ITaxiService service = this.GetServiceAPI(serviceAPIID, token, out fleetTripCode);
				rs = service.GetArrivalNotification(token, input);

			}
			catch (Exception ex)
			{

				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);

				throw;
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return rs;
		}
		public UtilityGetNotificationRS GetNightBeforeReminder(TokenRS token, UtilityGetNotificationRQ input)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			UtilityGetNotificationRS rs = new UtilityGetNotificationRS();
			int fleetTripCode = -1;
			try
			{


				TaxiUtility utility = new TaxiUtility(logger);
				long serviceAPIID = utility.GetServiceAPIIdByRequest(input, OutboundNotificationType.NightBeforeReminder);
				ITaxiService service = this.GetServiceAPI(serviceAPIID, token, out fleetTripCode);
				rs = service.GetNightBeforeReminder(token, input);

			}
			catch (Exception ex)
			{

				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);

				throw;
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return rs;
		}
		#endregion

		#region Properties
		public TrackTime TrackTime { get; set; }
		#endregion

	}


}
