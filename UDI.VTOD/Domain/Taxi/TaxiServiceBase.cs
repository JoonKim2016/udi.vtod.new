﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using log4net;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.Track;
using UDI.VTOD.Domain.Taxi.Class;
using UDI.VTOD.DataAccess.VTOD;
using System.Configuration;
namespace UDI.VTOD.Domain.Taxi
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class TaxiServiceBase : ITaxiService
    {
        protected readonly ILog logger;
        protected readonly TaxiUtility utility;
        protected readonly int FleetTripCode;
        protected TaxiServiceBase(TaxiUtility util, ILog log)
		{
			logger = log;
			utility = util;
		}
        protected TaxiServiceBase(TaxiUtility util, ILog log,int fleetTripCode)
        {
            logger = log;
            utility = util;
            FleetTripCode = fleetTripCode;

        }

        #region old methods - not implemented

        public virtual OTA_GroundBookRS Book(TaxiBookingParameter tbp,int FleetTripCode)
        {
            throw new NotImplementedException();
        }

        public virtual OTA_GroundResRetrieveRS Status(TaxiStatusParameter tsp)
        {
            throw new NotImplementedException();
        }

        public virtual OTA_GroundCancelRS Cancel(TaxiCancelParameter tcp)
        {
            throw new NotImplementedException();
        }

        public virtual OTA_GroundAvailRS GetVehicleInfo(TaxiGetVehicleInfoParameter tgvip)
        {
            throw new NotImplementedException();
        }

        public virtual bool NotifyDriver(TaxiSendMessageToVehicleParameter tsmtvp)
        {
            throw new NotImplementedException();
        }

        #endregion

        public virtual Fleet GetAvailableFleets(TokenRS token, UtilityAvailableFleetsRQ input)
        {
            logger.Info("Start");
            Stopwatch sw = new Stopwatch();
            sw.Start();
            Fleet result = null;

            try
            {
                bool pickMeUpNow;
                bool pickMeUpLater;
                if (utility.ValidateGetAvailableFleets(token, input, out pickMeUpNow, out pickMeUpLater))
                {
                    result = new Fleet { Type = FleetType.Taxi.ToString(), Now = pickMeUpNow.ToString(), Later = pickMeUpLater.ToString() };
                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Message:{0}", ex.Message);
                logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
                logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
                throw;
            }

            sw.Stop();
            logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
            return result;
        }

        public virtual TrackTime TrackTime { get; set; }

        public abstract OTA_GroundBookRS Book(TokenRS tokenVTOD, OTA_GroundBookRQ request, out List<Tuple<string, int, long>> disptach_Rez_VTOD, int FleetTripCode, taxi_fleet fleet);

        public abstract OTA_GroundBookRS ModifyBook(TokenRS tokenVTOD, OTA_GroundBookRQ request,out List<Tuple<string, int, long>> disptach_Rez_VTOD, int FleetTripCode, taxi_fleet fleet,long serviceapIID);
        public abstract OTA_GroundResRetrieveRS Status(TokenRS tokenVTOD, OTA_GroundResRetrieveRQ request,string type);

        public abstract OTA_GroundCancelRS Cancel(TokenRS tokenVTOD, OTA_GroundCancelRQ request);

        public abstract OTA_GroundAvailRS GetVehicleInfo(TokenRS tokenVTOD, OTA_GroundAvailRQ request);
        public abstract bool NotifyDriver(TokenRS token, UtilityNotifyDriverRQ input);
        public abstract bool NotifyPickupDriver(TokenRS token, UtilityNotifyDriverRQ input);
        public abstract bool NotifyDispatchPendingDriver(TokenRS token, UtilityNotifyDriverRQ input);
        public abstract OTA_GroundAvailRS GetEstimation(TokenRS tokenVTOD, OTA_GroundAvailRQ request);



        public abstract UtilityCustomerInfoRS GetCustomerInfo(TokenRS token, UtilityCustomerInfoRQ input);
       

        public abstract UtilityGetLastTripRS GetLastTrip(TokenRS token, UtilityGetLastTripRQ input);

        public abstract UtilityGetPickupAddressRS GetPickupAddress(TokenRS token, UtilityGetPickupAddressRQ input);



        public abstract UtilityGetBlackoutPeriodRS GetBlackoutPeriod(TokenRS token, UtilityGetBlackoutPeriodRQ input);


        public abstract bool GetDisabilityInd(TokenRS tokenVTOD, taxi_fleet f, out int condition,string authenticateEndpoint, string bookingEndpoint);
       


        public abstract UtilityGetNotificationRS GetStaleNotification(TokenRS token, UtilityGetNotificationRQ input);

        public abstract UtilityGetNotificationRS GetArrivalNotification(TokenRS token, UtilityGetNotificationRQ input);


        public abstract UtilityGetNotificationRS GetNightBeforeReminder(TokenRS token, UtilityGetNotificationRQ input);

        public abstract OTA_GroundGetFinalRouteRS GetFinalRoute(TokenRS tokenVTOD, OTA_GroundGetFinalRouteRQ request);

    }
}
