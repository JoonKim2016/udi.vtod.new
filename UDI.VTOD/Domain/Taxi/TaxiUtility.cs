﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;
using UDI.Map;
using UDI.SDS.ServiceAdapter;
using UDI.VTOD.Common.DTO;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.Helper;
using UDI.VTOD.DataAccess.VTOD;
using UDI.VTOD.Domain.Taxi.Class;
using UDI.VTOD.Domain.Taxi.Const;
using UDI.Utility.Helper;
using UDI.SDS.DTO.Enum;
using UDI.VTOD.Domain.Utility;
using UDI.VTOD.Common.Track;
using System.Transactions;
using System.Data;
using UDI.Utility.Serialization;
using System.Threading;
using System.Drawing;
using System.IO;
using System.Security.Policy;
using UDI.VTOD.Domain.VTOD;
using UDI.SDS.PaymentsService;
using UDI.VTOD.MTData.BookingWebService;
using Address = UDI.VTOD.Common.DTO.OTA.Address;
using AddressType = UDI.VTOD.Domain.Taxi.Const.AddressType;
using System.IO.Compression;

namespace UDI.VTOD.Domain.Taxi
{
	public class TaxiUtility
	{
		private ILog logger;

		private string VTODAPIVersion = string.Empty;

		public TaxiUtility(ILog log)
		{
			this.logger = log;
			VTODAPIVersion = ConfigurationManager.AppSettings["Version"];
		}

		#region Public method

		//public string ConvertTripStatusMessageForFrontEndDevice(int userID, string statusMessage, int fastMeterAction)
		//{
		//    string result = "";
		//    using (VTODEntities context = new VTODEntities())
		//    {
		//        if (statusMessage.ToLowerInvariant() == TaxiTripStatusType.FastMeter.ToLowerInvariant())
		//        {
		//            if (fastMeterAction == TaxiFleetFastMeterAction.ReDispatch)
		//            {
		//                result = TaxiTripStatusType.Booked;
		//            }
		//            else if (fastMeterAction == TaxiFleetFastMeterAction.Cancel)
		//            {
		//                result = context.taxi_status_frontendwrapper.Where(x => x.UserId == userID && x.VTODTaxiStatus == statusMessage).Select(x => x.FrontEndStatus).FirstOrDefault();
		//            }

		//        }
		//        else
		//        {
		//            result = context.taxi_status_frontendwrapper.Where(x => x.UserId == userID && x.VTODTaxiStatus == statusMessage).Select(x => x.FrontEndStatus).FirstOrDefault();
		//        }
		//        logger.InfoFormat("Convet trip status for frontend app. userID={0}, vtod_status={1}, frontendStatus={2}", userID, statusMessage, result);
		//    }
		//    return result;
		//}

		public OTA_GroundAvailRS GetRestimationResponse(TaxiGetEstimationParameter tgep)
		{
			OTA_GroundAvailRS response = new OTA_GroundAvailRS();

			#region [Taxi 5.1] Response OTA_GroundAvailRS
			//common
			response.Success = new Success();
			response.GroundServices = new GroundServiceList();
			response.GroundServices.GroundServices = new List<GroundService>();

			//Fare policy
			#region 01. Rate
			GroundService fpRate = new GroundService();
			fpRate.RateQualifier = new RateQualifier { Category = new Category { Code = FareType.FarePolicy.ToString(), Value = "Other_" } };
			//fpRate.RateQualifier.SpecialInputs = new List<KeyValuePair<string, string>>();
			fpRate.RateQualifier.SpecialInputs = new List<NameValue>();
			//KeyValuePair<string, string> kvpTypeRate = new KeyValuePair<string, string>("Type", "Rate");
			//NameValue kvpTypeRate = new NameValue { Name = "Type", Value = FareType.Estimation.ToString() };
			//fpRate.RateQualifier.SpecialInputs.Add(kvpTypeRate);
			fpRate.ServiceCharges = new List<ServiceCharges>();
			ServiceCharges sc_Rate_Init = new ServiceCharges { ChargePurpose = new ChargePurpose { Code = FareDetail.FlagDrop.ToString(), Value = "Other_" }, CurrencyCode = tgep.Info_Rate.AmountUnit, /* Description = tgep.Info_Rate.DistanceUnit, */ Amount = tgep.Info_Rate.InitialAmount.ToString() };
			//ServiceCharges sc_Rate_Init = new ServiceCharges { ChargePurpose = new ChargePurpose { Code = FareDetail.FlagDrop.ToString(), Value = "Other_" }, FreeDistance =111111, CurrencyCode = tgep.Info_Rate.AmountUnit, /* Description = tgep.Info_Rate.DistanceUnit, */ Amount = decimal.Parse(tgep.Info_Rate.InitialAmount.ToString()) };
			ServiceCharges sc_Rate_PerDistance = new ServiceCharges { ChargePurpose = new ChargePurpose { Code = FareDetail.PerDistance.ToString(), Value = "Other_" }, CurrencyCode = tgep.Info_Rate.AmountUnit, Description = tgep.Info_Rate.DistanceUnit, Amount = tgep.Info_Rate.PerDistanceAmount.ToString() };
			fpRate.ServiceCharges.Add(sc_Rate_Init);
			fpRate.ServiceCharges.Add(sc_Rate_PerDistance);
			response.GroundServices.GroundServices.Add(fpRate);
			#endregion

			#region 02. Flatrate part policy (don't remove this)
			//We turn this policy part off now. 
			//This is a part to verify the price policy 
			/*
			if (tgep.Info_FlatRates != null && tgep.Info_FlatRates.Any())
				foreach (TaxiFlatRateData tfrd in tgep.Info_FlatRates)
				{
					GroundService fpFlatRate = new GroundService();
					fpFlatRate.RateQualifier = new RateQualifier { Category = new Category { Code = FareType.FarePolicy.ToString(), Value = "Other_" } };
					//fpFlatRate.RateQualifier.SpecialInputs = new List<KeyValuePair<string, string>>();
					fpFlatRate.RateQualifier.SpecialInputs = new List<NameValue>();
					//KeyValuePair<string, string> kvpTypeFlatRate = new KeyValuePair<string, string>("Type", "FlatRate");
					//KeyValuePair<string, string> kvpTypePickup = new KeyValuePair<string, string>("Pickup", tfrd.PickupZoneName);
					//KeyValuePair<string, string> kvpTypeDropoff = new KeyValuePair<string, string>("DropOff", tfrd.DropOffZoneName);
					//NameValue kvpTypeFlatRate = new NameValue { Name = "Type", Value = FareType.FlatRate.ToString() };
					NameValue kvpTypePickup = new NameValue { Name = "Pickup", Value = tfrd.PickupZoneName };
					NameValue kvpTypeDropoff = new NameValue { Name = "DropOff", Value = tfrd.DropOffZoneName };

					//fpFlatRate.RateQualifier.SpecialInputs.Add(kvpTypeFlatRate);
					fpFlatRate.RateQualifier.SpecialInputs.Add(kvpTypePickup);
					fpFlatRate.RateQualifier.SpecialInputs.Add(kvpTypeDropoff);
					fpFlatRate.ServiceCharges = new List<ServiceCharges>();
					ServiceCharges sc_FlatRate_PerDistance = new ServiceCharges { ChargePurpose = new ChargePurpose { Code = FareType.FlatRate.ToString(), Value = "Other_" }, CurrencyCode = tfrd.AmountUnit, Amount = decimal.Parse(tfrd.Amount.ToString()) };
					fpFlatRate.ServiceCharges.Add(sc_FlatRate_PerDistance);
					response.GroundServices.GroundServices.Add(fpFlatRate);
				}
			*/
			#endregion

			if (tgep.EstimationType == EstimationResultType.Info_FlatRate)
			{

				#region 03. Estimation(FlatRate)
				GroundService fpFlatRateEstimation = new GroundService();
				fpFlatRateEstimation.RateQualifier = new RateQualifier { Category = new Category { Code = FareType.FlatRate.ToString(), Value = "Other_" } };
				//fpFlatRateEstimation.RateQualifier.SpecialInputs = new List<KeyValuePair<string, string>>();
				fpFlatRateEstimation.RateQualifier.SpecialInputs = new List<NameValue>();
				//KeyValuePair<string, string> kvpTypeFlatRateEstimationPickup = new KeyValuePair<string, string>("Pickup", tgep.FlatRateestimation.PickupZone);
				//KeyValuePair<string, string> kvpTypeFlatRateEstimationDropoff = new KeyValuePair<string, string>("DropOff", tgep.FlatRateestimation.DropoffZone);

				if ((tgep.FlatRateEstimation != null))
				{
					NameValue kvpTypeFlatRateEstimationPickup = new NameValue { Name = "Pickup", Value = tgep.FlatRateEstimation.PickupZone };
					NameValue kvpTypeFlatRateEstimationDropoff = new NameValue { Name = "DropOff", Value = tgep.FlatRateEstimation.DropoffZone };
					fpFlatRateEstimation.RateQualifier.SpecialInputs.Add(kvpTypeFlatRateEstimationPickup);
					fpFlatRateEstimation.RateQualifier.SpecialInputs.Add(kvpTypeFlatRateEstimationDropoff);
					fpFlatRateEstimation.TotalCharge = new TotalCharge
					{
						EstimatedTotalAmount = decimal.Parse(tgep.FlatRateEstimation.EstimationTotalAmount.ToString()),
						RateTotalAmount = decimal.Parse(tgep.FlatRateEstimation.EstimationTotalAmount.ToString())
					};
					response.GroundServices.GroundServices.Add(fpFlatRateEstimation);
				}
				else
				{
					logger.InfoFormat("No FlatRate estimation");
					//throw new ValidationException(Messages.Taxi_GetEstimationFailure);
				}
				#endregion
			}
			else if (tgep.EstimationType == EstimationResultType.Info_Rate)
			{
				#region 04. Estimation(Rate)
				GroundService fpRateEstimation = new GroundService();
				fpRateEstimation.RateQualifier = new RateQualifier { Category = new Category { Code = FareType.Estimation.ToString(), Value = "Other_" } };
				fpRateEstimation.RateQualifier.SpecialInputs = new List<NameValue>();
				NameValue kvpTypeFPDistanceUnit = new NameValue { Name = "DistanceUnit", Value = tgep.RateEstimation.DistanceUnit };

				NameValue kvpTypeFPDistance;
				if (tgep.RateEstimation.DistanceUnit == Map.DTO.DistanceUnit.Mile.ToString())
				{
					kvpTypeFPDistance = new NameValue { Name = "Distance", Value = tgep.Distance_Mile.ToString() };
					fpRateEstimation.RateQualifier.SpecialInputs.Add(kvpTypeFPDistance);
				}
				else if (tgep.RateEstimation.DistanceUnit == Map.DTO.DistanceUnit.Kilometer.ToString())
				{
					kvpTypeFPDistance = new NameValue { Name = "Distance", Value = tgep.Distance_Kilometer.ToString() };
					fpRateEstimation.RateQualifier.SpecialInputs.Add(kvpTypeFPDistance);
				}
				fpRateEstimation.RateQualifier.SpecialInputs.Add(kvpTypeFPDistanceUnit);
				fpRateEstimation.TotalCharge = new TotalCharge
				{
					EstimatedTotalAmount = decimal.Parse(tgep.RateEstimation.EstimationTotalAmount.ToString()),
					RateTotalAmount = decimal.Parse(tgep.RateEstimation.EstimationTotalAmount.ToString())

				};
				response.GroundServices.GroundServices.Add(fpRateEstimation);
				#endregion
			}


			#endregion

			return response;
		}


		public bool DetectFastMeterByMinFare(long tripId, string status, string fareString, decimal MinPriceFastMeter)
		{
			bool isFastMeter = false;
			List<string> fastMeterStatus = ConfigurationManager.AppSettings["Taxi_DetectFastMeterStatus"].Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();
			if (fastMeterStatus.Where(x => x.ToLowerInvariant().Equals(status.ToLowerInvariant())).Any())
			{
				decimal fareAmount = 0;
				if (decimal.TryParse(fareString, out fareAmount))
				{
					if (fareAmount <= MinPriceFastMeter && fareAmount > 0)
					{
						isFastMeter = true;
					}
					else
					{
						isFastMeter = false;
					}
				}
				else if (CheckFastMeterPreviousTripStatus(tripId))
				{
					isFastMeter = true;
				}
				else
				{
					isFastMeter = false;
				}
			}
			else
			{
				isFastMeter = false;
			}

			logger.InfoFormat("Is fastmeter:{0}", isFastMeter);
			return isFastMeter;

		}

		public string ConvertTripStatusMessageForFrontEndDevice(int userID, string statusMessage)
		{
			string result = "";
			using (VTODEntities context = new VTODEntities())
			{
				result = context.taxi_status_frontendwrapper.Where(x => x.UserId == userID && x.VTODTaxiStatus == statusMessage).Select(x => x.FrontEndStatus).FirstOrDefault();
				logger.InfoFormat("Convet trip status for frontend app. userID={0}, vtod_status={1}, frontendStatus={2}", userID, statusMessage, result);
			}
			return result;
		}

		public List<string> GetPreviousTripStatus(long tripId)
		{
			List<string> statusList = new List<string>();

			using (VTODEntities context = new VTODEntities())
			{
				statusList = context.taxi_trip_status.Where(x => x.TripID == tripId).Select(x => x.Status).Distinct().ToList();

			}

			return statusList;
		}
		public string CompressString(string text)
		{
			byte[] buffer = Encoding.UTF8.GetBytes(text);
			var memoryStream = new MemoryStream();
			using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Compress, true))
			{
				gZipStream.Write(buffer, 0, buffer.Length);
			}

			memoryStream.Position = 0;

			var compressedData = new byte[memoryStream.Length];
			memoryStream.Read(compressedData, 0, compressedData.Length);

			var gZipBuffer = new byte[compressedData.Length + 4];
			Buffer.BlockCopy(compressedData, 0, gZipBuffer, 4, compressedData.Length);
			Buffer.BlockCopy(BitConverter.GetBytes(buffer.Length), 0, gZipBuffer, 0, 4);
			return Convert.ToBase64String(gZipBuffer);
		}
		public string Data_Hex_Asc(string Data)
		{
			string hex = "";
			foreach (char c in Data)
			{
				int tmp = c;
				hex += String.Format("{0:x2}", (uint)System.Convert.ToUInt32(tmp.ToString()));
			}
			return hex;
		}

		public int GetMinutesAway(int ETA)
		{
			int minutesAway = 0;
			TimeSpan duration = DateTime.UtcNow.AddMinutes(ETA) - DateTime.UtcNow;
			minutesAway = (int)System.Math.Abs(duration.TotalMinutes);
			return minutesAway;
		}

		public string ExtractDigits(string input)
		{
			return Regex.Replace(input, @"[^\d]+", "");
			//return String.Join("", input.ToCharArray().Where(c => c >= '0' && c <= '9').Select(c => c.ToString()).ToArray());
		}

		public OTA_GroundAvailRS GetEstimation(TaxiGetEstimationParameter tgep)
		{
			throw new NotImplementedException();
		}

		#region Validation



		public long GetServiceAPIIdByRequest(UtilityNotifyDriverRQ request)
		{
			long serviceAPIID = -1;

			using (VTODEntities context = new VTODEntities())
			{
				var taxiTrip = context.taxi_trip.FirstOrDefault(m => m.Id == request.TripID);
				if (taxiTrip != null)
				{
					try
					{
						serviceAPIID = GetServiceAPIId(taxiTrip.FleetId, TaxiServiceAPIPreferenceConst.NotifyDriver);
					}
					catch (Exception ex)
					{
						var vtodException = VtodException.CreateException(ExceptionType.Taxi, 1011);
						logger.Warn(vtodException.ExceptionMessage.Message);
						throw vtodException;
					}
				}

				if (serviceAPIID == -1)
				{
					//enforce to use UDI33
					serviceAPIID = TaxiServiceAPIConst.UDI33;
				}
			}

			if (serviceAPIID == -1)
			{
				var vtodException = VtodException.CreateException(ExceptionType.Taxi, 1011);
				logger.Warn(vtodException.ExceptionMessage.Message);
				throw vtodException;
			}

			return serviceAPIID;
		}

        public long GetServiceAPIIdByRequest(OTA_GroundGetFinalRouteRQ request)
        {
            long serviceAPIID = -1;
            taxi_trip trip = null;
            if (request.Reference != null && request.Reference.Any())
            {
                if (request.Reference.Where(x => x.Type.Equals("Confirmation")).Any())
                {
                    using (var context = new VTODEntities())
                    {
                        var tripID = Convert.ToInt64(request.Reference.Where(x => x.Type.Equals("Confirmation")).First().ID);
                         trip = context.taxi_trip.Where(x => x.Id == tripID).FirstOrDefault();
                    }
                }
                else if (request.Reference.Where(x => x.Type.Equals("DispatchConfirmation")).Any())
                {

                    using (var context = new VTODEntities())
                    {
                        var tripID = Convert.ToInt64(request.Reference.Where(x => x.Type.Equals("DispatchConfirmation")).First().ID);
                        
                        trip = context.taxi_trip.Where(
                                x =>
                                    x.DispatchTripId == tripID.ToString()
                            ).FirstOrDefault();
                    }
                }
                if(trip!=null)
                serviceAPIID = GetServiceAPIId(trip.FleetId, TaxiServiceAPIPreferenceConst.GetFinalRoute);
            }
            if (serviceAPIID == -1)
            {
                var vtodException = VtodException.CreateException(ExceptionType.Taxi, 1011);
                logger.Warn(vtodException.ExceptionMessage.Message);
                throw vtodException;
            }

            return serviceAPIID;
        }
        public long GetServiceAPIIdByRequest(UtilityCustomerInfoRQ request)
		{
			long serviceAPIID = -1;

			using (VTODEntities context = new VTODEntities())
			{
				long fleetID = Convert.ToInt64(request.DispatchRecord.FleetId);
				var fleet = context.taxi_fleet.Where(x => x.Id == fleetID && x.IVREnable).FirstOrDefault();
				if (fleet != null)
				{
					try
					{
						serviceAPIID = GetServiceAPIId(fleet.Id, TaxiServiceAPIPreferenceConst.GetCustomerInfo);
					}
					catch (Exception ex)
					{
						var vtodException = VtodException.CreateException(ExceptionType.Taxi, 1011);
						logger.Warn(vtodException.ExceptionMessage.Message);
						throw vtodException;
					}
				}

				if (serviceAPIID == -1)
				{
					//enforce to use UDI33
					serviceAPIID = TaxiServiceAPIConst.MTData;
				}
			}

			if (serviceAPIID == -1)
			{
				var vtodException = VtodException.CreateException(ExceptionType.Taxi, 1011);
				logger.Warn(vtodException.ExceptionMessage.Message);
				throw vtodException;
			}

			return serviceAPIID;
		}

		public long GetServiceAPIIdByRequest(UtilityGetPickupAddressRQ request)
		{
			long serviceAPIID = -1;

			using (VTODEntities context = new VTODEntities())
			{
				long fleetId = Convert.ToInt64(request.DispatchRecord.FleetId);
				var fleet = context.taxi_fleet.Where(x => x.Id == fleetId && x.IVREnable).FirstOrDefault();
				if (fleet != null)
				{
					try
					{
						serviceAPIID = GetServiceAPIId(fleet.Id, TaxiServiceAPIPreferenceConst.GetPickupAddress);
					}
					catch (Exception ex)
					{
						var vtodException = VtodException.CreateException(ExceptionType.Taxi, 1011);
						logger.Warn(vtodException.ExceptionMessage.Message);
						throw vtodException;
					}
				}

				if (serviceAPIID == -1)
				{
					//enforce to use UDI33
					serviceAPIID = TaxiServiceAPIConst.MTData;
				}
			}

			if (serviceAPIID == -1)
			{
				var vtodException = VtodException.CreateException(ExceptionType.Taxi, 1011);
				logger.Warn(vtodException.ExceptionMessage.Message);
				throw vtodException;
			}

			return serviceAPIID;
		}


		public long GetServiceAPIIdByRequest(UtilityGetLastTripRQ request)
		{
			long serviceAPIID = -1;

			using (VTODEntities context = new VTODEntities())
			{
				long fleetId = Convert.ToInt64(request.FleetId);
				var fleet = context.taxi_fleet.Where(x => x.Id == fleetId && x.IVREnable).FirstOrDefault();
				if (fleet != null)
				{
					try
					{
						serviceAPIID = GetServiceAPIId(fleet.Id, TaxiServiceAPIPreferenceConst.GetLastTrip);
					}
					catch (Exception)
					{
						var vtodException = VtodException.CreateException(ExceptionType.Taxi, 1011);
						logger.Warn(vtodException.ExceptionMessage.Message);
						throw vtodException;
					}
				}

				if (serviceAPIID == -1)
				{
					//enforce to use UDI33
					serviceAPIID = TaxiServiceAPIConst.MTData;
				}
			}

			if (serviceAPIID == -1)
			{
				var vtodException = VtodException.CreateException(ExceptionType.Taxi, 1011);
				logger.Warn(vtodException.ExceptionMessage.Message);
				throw vtodException;
			}

			return serviceAPIID;
		}

		public long GetServiceAPIIdByRequest(UtilityGetBlackoutPeriodRQ request)
		{
			long serviceAPIID = -1;

			using (VTODEntities context = new VTODEntities())
			{
				long fleetID = Convert.ToInt64(request.FleetId);
				var fleet = context.taxi_fleet.Where(x => x.Id == fleetID && x.IVREnable).FirstOrDefault();
				if (fleet != null)
				{
					try
					{
						serviceAPIID = GetServiceAPIId(fleet.Id, TaxiServiceAPIPreferenceConst.GetBlackoutPeriod);
					}
					catch (Exception ex)
					{
						var vtodException = VtodException.CreateException(ExceptionType.Taxi, 1011);
						logger.Warn(vtodException.ExceptionMessage.Message);
						throw vtodException;
					}
				}

				if (serviceAPIID == -1)
				{
					//enforce to use UDI33
					serviceAPIID = TaxiServiceAPIConst.MTData;
				}
			}

			if (serviceAPIID == -1)
			{
				var vtodException = VtodException.CreateException(ExceptionType.Taxi, 1011);
				logger.Warn(vtodException.ExceptionMessage.Message);
				throw vtodException;
			}

			return serviceAPIID;
		}

		public long GetServiceAPIIdByRequest(UtilityGetNotificationRQ request, int notificationType)
		{
			long serviceAPIID = -1;

			using (VTODEntities context = new VTODEntities())
			{
				long fleetID = Convert.ToInt64(request.FleetId);
				var fleet = context.taxi_fleet.Where(x => x.Id == fleetID && x.IVREnable).FirstOrDefault();
				if (fleet != null)
				{
					try
					{
						switch (notificationType)
						{
							case OutboundNotificationType.ArrivalNotification:
								serviceAPIID = GetServiceAPIId(fleet.Id, TaxiServiceAPIPreferenceConst.GetArrivalNotification);
								break;
							case OutboundNotificationType.NightBeforeReminder:
								serviceAPIID = GetServiceAPIId(fleet.Id, TaxiServiceAPIPreferenceConst.GetNightBeforeReminder);
								break;
							case OutboundNotificationType.StaleNotification:
								serviceAPIID = GetServiceAPIId(fleet.Id, TaxiServiceAPIPreferenceConst.GetStaleNotification);
								break;
						}

					}
					catch (Exception ex)
					{
						var vtodException = VtodException.CreateException(ExceptionType.Taxi, 1011);
						logger.Warn(vtodException.ExceptionMessage.Message);
						throw vtodException;
					}
				}

				if (serviceAPIID == -1)
				{
					//enforce to use UDI33
					serviceAPIID = TaxiServiceAPIConst.MTData;
				}
			}

			if (serviceAPIID == -1)
			{
				var vtodException = VtodException.CreateException(ExceptionType.Taxi, 1011);
				logger.Warn(vtodException.ExceptionMessage.Message);
				throw vtodException;
			}

			return serviceAPIID;
		}


		public long GetServiceAPIIdByRequest(OTA_GroundAvailRQ request, string action, out taxi_fleet fleet)
		{
			long serviceAPIID = -1;
			bool result = true;
			string PickupAddressType = "";
			Map.DTO.Address PickupAddress = new Map.DTO.Address();

			#region Validate Pickup Address
			if (result)
			{
				Pickup_Dropoff_Stop stop = request.Service.Pickup;

				if ((stop.Address != null) && (stop.AirportInfo == null))
				{
					if (!string.IsNullOrWhiteSpace(stop.Address.CountryName.Code) && !string.IsNullOrWhiteSpace(stop.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(stop.Address.CityName) && !string.IsNullOrWhiteSpace(stop.Address.PostalCode))
					{
						Map.DTO.Address pickupAddress = new Map.DTO.Address { Country = stop.Address.CountryName.Code, State = stop.Address.StateProv.StateCode, City = stop.Address.CityName, ZipCode = stop.Address.PostalCode, Geolocation = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(stop.Address.Latitude), Longitude = Convert.ToDecimal(stop.Address.Longitude) } };
						PickupAddressType = UDI.VTOD.Domain.Taxi.Const.AddressType.Address;
						PickupAddress = pickupAddress;
					}
					else if (!string.IsNullOrWhiteSpace(stop.Address.CountryName.Code) && !string.IsNullOrWhiteSpace(stop.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(stop.Address.CityName))
					{
						Map.DTO.Address pickupAddress = new Map.DTO.Address { Country = stop.Address.CountryName.Code, State = stop.Address.StateProv.StateCode, City = stop.Address.CityName, Geolocation = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(stop.Address.Latitude), Longitude = Convert.ToDecimal(stop.Address.Longitude) } };
						PickupAddressType = UDI.VTOD.Domain.Taxi.Const.AddressType.Address;
						PickupAddress = pickupAddress;

					}
					else if (!string.IsNullOrWhiteSpace(stop.Address.LocationName))
					{
						#region Not support now
						//tgvip.PickupAddressType = UDI.VTOD.Domain.Taxi.Const.AddressType.LocationName;
						//tgvip.PickupLandmark = stop.Address.LocationName;
						#endregion

						result = false;
						logger.Warn("Cannot use locationName from address.");
					}
					else
					{
						result = false;
						logger.Warn("Cannot find any address/locationName from address.");
					}
				}
				else if ((stop.Address == null) && (stop.AirportInfo != null))
				{
					if ((stop.AirportInfo.Arrival != null) && (stop.AirportInfo.Departure == null))
					{
						#region Not support now
						//tgvip.PickupAddressType = UDI.VTOD.Domain.Taxi.Const.AddressType.AirportInfo;
						//tgvip.PickupLandmark = stop.AirportInfo.Arrival.LocationCode;
						#endregion

						result = false;
						logger.Warn("Cannot use aiportinfo for address");
					}
					else if ((stop.AirportInfo.Arrival == null) && (stop.AirportInfo.Departure != null))
					{
						#region Not support now
						//tgvip.PickupAddressType = UDI.VTOD.Domain.Taxi.Const.AddressType.AirportInfo;
						//tgvip.PickupLandmark = stop.AirportInfo.Departure.LocationCode;
						#endregion

						result = false;
						logger.Warn("Cannot use aiportinfo for address");
					}
					else if ((stop.AirportInfo.Arrival != null) && (stop.AirportInfo.Departure != null))
					{
						result = false;
						logger.Warn("This OTA request contains both AirportInfo.Arrival and AirportInfo.Departure");
					}
					else
					{
						result = false;
						logger.Warn("Cannot find any arrival aor depature from AirportInfo");
					}
				}
				else
				{
					//find address by longtitude and latitude
					double Longtitude = 0;
					double Latitude = 0;
					Map.DTO.Address pickupAddress2 = null;

					if (!LookupAddress(Longtitude, Latitude, out pickupAddress2))
					{
						logger.WarnFormat("Cannot find longtitude={0} and latitude={1}", Longtitude, Latitude);
						result = false;
					}
					PickupAddressType = UDI.VTOD.Domain.Taxi.Const.AddressType.Address;
					PickupAddress = pickupAddress2;
				}

			}
			else
			{
				result = false;
			}
			#endregion

			#region Validate Fleet
			fleet = null;
			if (result)
			{
				if (GetFleet(PickupAddressType, PickupAddress, string.Empty, out fleet))
				{
					logger.InfoFormat("Fleet found. ID={0}", fleet.Id);
				}
				else
				{
					result = false;
					logger.Warn("Unable to find fleet by this address or landmark.");
				}
			}
			else
			{
				result = false;
				logger.Warn("Unable to find fleet by invalid address or landmark.");
			}
			#endregion

			if (result)
			{
				serviceAPIID = GetServiceAPIId(fleet.Id, action);
				if (serviceAPIID == -1)
				{
					var vtodException = VtodException.CreateException(ExceptionType.Taxi, 1011);
					logger.Warn(vtodException.ExceptionMessage.Message);
					throw vtodException;// new ValidationException(Messages.Taxi_Common_UnableToFindFleetByAddressOrAirportInfo);
				}
			}

			return serviceAPIID;
		}

		public long GetServiceAPIIdByRequest(OTA_GroundCancelRQ request)
		{
			long serviceAPIID = -1;
			long fleetId = -1;
			//bool isVTODTaxiTrip=true;
			bool result = true;


			if (request.Reservation.UniqueID != null && request.Reservation.UniqueID.Any())
			{
				if (request.Reservation.UniqueID.Where(x => x.Type.Equals("Confirmation")).Any())
				{
					//vtod trip  + confirmation ID
					using (var context = new VTODEntities())
					{
						var tripID = Convert.ToInt64(request.Reservation.UniqueID.Where(x => x.Type.Equals("Confirmation")).First().ID);
						var trip = context.taxi_trip.Where(x => x.Id == tripID).FirstOrDefault();
						fleetId = trip.FleetId;
					}
					//isVTODTaxiTrip = true;
					result = true;
				}
				else if (request.Reservation.UniqueID.Where(x => x.Type.Equals("DispatchConfirmation")).Any())
				{
					if (/*request.TPA_Extensions.FleetId != null &&*/ request.TPA_Extensions.FleetId > 0)
					{
						//Non-vtod trip  + dispatch confirmation ID
						//isVTODTaxiTrip = false;
						fleetId = request.TPA_Extensions.FleetId;
						result = true;
					}
					else if (/* (request.TPA_Extensions.FleetId == null || */ request.TPA_Extensions.FleetId == 0 && request.Reservation.Verification != null && (request.Reservation.Verification.PersonName != null || request.Reservation.Verification.TelephoneInfo != null))
					{
						//vtod trop + dispatch confirmation ID + first name and lastname or zip code
						using (var context = new VTODEntities())
						{
							var tripID = Convert.ToInt64(request.Reservation.UniqueID.Where(x => x.Type.Equals("DispatchConfirmation")).First().ID);
							taxi_trip trip = null;
                            
							if (request.Reservation.Verification.PersonName != null)
							{
								trip = context.taxi_trip.Where(
										x =>
											x.DispatchTripId == tripID.ToString() && x.FirstName == request.Reservation.Verification.PersonName.GivenName && x.LastName == request.Reservation.Verification.PersonName.Surname
									).FirstOrDefault();
							}
						
							//Get Trip with phone#
							if (trip == null)
							{
								if (request.Reservation.Verification.TelephoneInfo != null)
								{
                                    string phoneNumber =request.Reservation.Verification.TelephoneInfo.PhoneNumber.CleanPhone();
                                    trip = context.taxi_trip.Where(
                                                                    x =>
                                                                        x.DispatchTripId == tripID.ToString() && x.PhoneNumber == phoneNumber
                                                                ).FirstOrDefault();
								}
							}

							if (trip != null)
							{
								fleetId = trip.FleetId;
								result = true;
							}
							else
							{
								result = false;
								logger.Warn("The dispatch confirmation ID doesn't exist in VTOD DB.");
							}
						}
					}
				}
				else
				{
					result = false;
					logger.Warn("Unknown confirmation/dispatchConfirmation ID");
				}
			}
			else
			{
				result = false;
				logger.Warn("Reference is empty.");
			}



			if (result)
			{
				serviceAPIID = GetServiceAPIId(fleetId, TaxiServiceAPIPreferenceConst.Cancel);
				if (serviceAPIID == -1)
				{
					var vtodException = VtodException.CreateException(ExceptionType.Taxi, 1011);
					logger.Warn(vtodException.ExceptionMessage.Message);
					throw vtodException;
				}
			}

			return serviceAPIID;




		}

		public OTA_GroundCancelRQ CorrectVTODRequest(OTA_GroundCancelRQ request)
		{
			OTA_GroundCancelRQ result = request;
			string type = request.Reservation.UniqueID.FirstOrDefault().Type;
			if (request.Reservation != null && request.Reservation.UniqueID.Any() && request.Reservation.UniqueID.FirstOrDefault().Type.Trim().ToLower() == Common.DTO.Enum.ConfirmationType.DispatchConfirmation.ToString().Trim().ToLower()
				&& IsVTODTaxiTrip(request.Reservation.UniqueID.FirstOrDefault().ID, request.TPA_Extensions.FleetId, TaxiServiceAPIPreferenceConst.Cancel))
			{
				using (var db = new DataAccess.VTOD.VTODEntities())
				{
					result.Reservation.UniqueID.FirstOrDefault().Type = Common.DTO.Enum.ConfirmationType.Confirmation.ToString();

					string dispatchID = request.Reservation.UniqueID.FirstOrDefault().ID;
					string firstName = "";
					string lastName = "";
					string phoneNumber = "";
					taxi_trip taxiObj = null;


					if (request.Reservation.Verification.PersonName != null)
					{
						if (!string.IsNullOrWhiteSpace(request.Reservation.Verification.PersonName.GivenName))
						{
							firstName = request.Reservation.Verification.PersonName.GivenName.ToLower().ToString();
						}
						if (!string.IsNullOrWhiteSpace(request.Reservation.Verification.PersonName.Surname))
						{
							lastName = request.Reservation.Verification.PersonName.Surname.ToLower().ToString();
						}
					}
					if (request.Reservation.Verification.TelephoneInfo != null)
					{
						if (!string.IsNullOrWhiteSpace(request.Reservation.Verification.TelephoneInfo.PhoneNumber))
						{
							phoneNumber = request.Reservation.Verification.TelephoneInfo.PhoneNumber.CleanPhone();
						}
					}
					if (!string.IsNullOrWhiteSpace(phoneNumber))
					{
						taxiObj = db.taxi_trip.Where(p => p.DispatchTripId == dispatchID && p.PhoneNumber == phoneNumber).FirstOrDefault();
					}


					if (taxiObj == null)
					{
						taxiObj = db.taxi_trip.Where(p => p.DispatchTripId == dispatchID && p.FirstName.ToLower().ToString() == firstName && p.LastName.ToLower().ToString() == lastName).FirstOrDefault();
					}

					if (taxiObj != null)
						result.Reservation.UniqueID.FirstOrDefault().ID = taxiObj.Id.ToString();
				}
			}

			return result;

		}

		public OTA_GroundResRetrieveRQ CorrectVTODRequest(OTA_GroundResRetrieveRQ request)
		{
			OTA_GroundResRetrieveRQ result = request;
			string type = request.Reference.FirstOrDefault().Type;
			if (request.Reference != null && request.Reference.Any() && request.Reference.FirstOrDefault().Type.Trim().ToLower() == Common.DTO.Enum.ConfirmationType.DispatchConfirmation.ToString().Trim().ToLower()
				&& IsVTODTaxiTrip(request.Reference.FirstOrDefault().ID, request.TPA_Extensions.FleetId, TaxiServiceAPIPreferenceConst.Status))
			{
				using (var db = new DataAccess.VTOD.VTODEntities())
				{
					result.Reference.FirstOrDefault().Type = Common.DTO.Enum.ConfirmationType.Confirmation.ToString();

					string dispatchTripID = request.Reference.FirstOrDefault().ID;
					string firstName = "";
					string lastName = "";
					taxi_trip taxiObj = null;


					if (request.Reference.FirstOrDefault().Verification != null && request.Reference.FirstOrDefault().Verification.PersonName != null && !string.IsNullOrWhiteSpace(request.Reference.FirstOrDefault().Verification.PersonName.GivenName))
					{
						firstName = request.Reference.FirstOrDefault().Verification.PersonName.GivenName.ToLower().ToString();
					}
					if (request.Reference.FirstOrDefault().Verification != null && request.Reference.FirstOrDefault().Verification.PersonName != null && !string.IsNullOrWhiteSpace(request.Reference.FirstOrDefault().Verification.PersonName.Surname))
					{
						lastName = request.Reference.FirstOrDefault().Verification.PersonName.Surname.ToLower().ToString();
					}


					if (!string.IsNullOrWhiteSpace(firstName) && !string.IsNullOrWhiteSpace(lastName))
					{
						taxiObj = db.taxi_trip.Where(p => p.DispatchTripId == dispatchTripID && p.FirstName.ToLower().ToString() == firstName && p.LastName.ToLower().ToString() == lastName).FirstOrDefault();
					}

					if (taxiObj != null)
						result.Reference.FirstOrDefault().ID = taxiObj.Id.ToString();
				}
			}

			return result;

		}

		public long GetServiceAPIIdByRequest(OTA_GroundResRetrieveRQ request)
		{
			long serviceAPIID = -1;
			long fleetId = -1;
			//bool isVTODTaxiTrip=true;
			bool result = true;


			if (request.Reference != null && request.Reference.Any())
			{
				if (request.Reference.Where(x => x.Type.Equals("Confirmation")).Any())
				{
					//vtod trip  + confirmation ID
					using (var context = new VTODEntities())
					{
						var tripID = Convert.ToInt64(request.Reference.Where(x => x.Type.Equals("Confirmation")).FirstOrDefault().ID);
						var trip = context.taxi_trip.Where(x => x.Id == tripID).FirstOrDefault();
						fleetId = trip.FleetId;
					}
					//isVTODTaxiTrip = true;
					result = true;
				}
				else if (request.Reference.Where(x => x.Type.Equals("DispatchConfirmation")).Any())
				{
					//if (request.TPA_Extensions.FleetId != null)
					//{
					//	//Non-vtod trip  + dispatch confirmation ID
					//	//isVTODTaxiTrip = false;
					//	fleetId = request.TPA_Extensions.FleetId;
					//	result = true;
					//}
					 if ( request.Reference.FirstOrDefault().Verification != null && request.Reference.FirstOrDefault().Verification.PersonName != null)
					{
                        string firstname = "Nauki";
                        string lastname = "Chon";
                        //vtod trop + dispatch confirmation ID + first name and lastname or zip code
                        using (var context = new VTODEntities())
						{
							var tripID = Convert.ToInt64(request.Reference.Where(x => x.Type.Equals("DispatchConfirmation")).FirstOrDefault().ID);
							var trip = context.taxi_trip.Where(x => x.DispatchTripId == tripID.ToString() &&
								x.FirstName == firstname && x.LastName == lastname
                                ).FirstOrDefault();
							if (trip != null)
							{
								fleetId = trip.FleetId;
								//isVTODTaxiTrip = true;
								result = true;
							}
							else {
								result = false;
								logger.Warn("The dispatch confirmation ID doesn't exist in VTOD DB.");
							}
						}
					}
				}
				else {
					result = false;
					logger.Warn("Unknown confirmation/dispatchConfirmation ID");
				}
			}
			else
			{
				result = false;
				logger.Warn("Reference is empty.");
			}



			if (result)
			{
				serviceAPIID = GetServiceAPIId(fleetId, TaxiServiceAPIPreferenceConst.Status);
				if (serviceAPIID == -1)
				{
					var vtodException = VtodException.CreateException(ExceptionType.Taxi, 1011);
					logger.Warn(vtodException.ExceptionMessage.Message);
					throw vtodException;
				}
			}

			return serviceAPIID;
		}

		public long GetServiceAPIIdByRequest(OTA_GroundBookRQ request, out taxi_fleet fleet)
		{
			long serviceAPIID = -1;
			string PickupAddressType = "";
			var PickupAddress = new Map.DTO.Address();
			string PickupAirport = "";
			GroundReservation reservation = request.GroundReservations.FirstOrDefault();

			if (request.TPA_Extensions.FleetId != null && request.TPA_Extensions.FleetId > 0)
			{
				//for IVR only
				fleet = GetActiveIVRFleet(Convert.ToInt64(request.TPA_Extensions.FleetId));

				//unable to find fleet by fleetID
				if (fleet == null)
				{
					var vtodException = VtodException.CreateException(ExceptionType.Taxi, 1011);
					logger.Warn(vtodException.ExceptionMessage.Message);
					throw vtodException;
				}
			}
			else
			{
				#region Validate Pickup Address
				if (reservation.Service != null)
				{
					if (reservation.Service.Location != null)
					{
						if (reservation.Service.Location.Pickup != null)
						{
							if (((reservation.Service.Location.Pickup.Address != null) && (reservation.Service.Location.Pickup.AirportInfo != null)) || ((reservation.Service.Location.Pickup.Address == null) && (reservation.Service.Location.Pickup.AirportInfo == null)))
							{
								logger.Warn(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
								throw new ValidationException(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
							}
							else if ((reservation.Service.Location.Pickup.Address != null) && (reservation.Service.Location.Pickup.AirportInfo == null))
							{

								if ((string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude) || (string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CountryName.Code)))
								{
									logger.Warn(Messages.Validation_LocationDetail);
									throw new ValidationException(Messages.Validation_LocationDetail);
								}
								else if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.AddressLine) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CityName) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.PostalCode) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CountryName.Code))
								{
									//Address
									Map.DTO.Address pickupAddress = null;
									string pickupAddressStr = GetAddressString(reservation.Service.Location.Pickup.Address);
									//if (reservation.Service.Location.Pickup.Address.TPA_Extensions != null)
									//{
									//	if (reservation.Service.Location.Pickup.Address.TPA_Extensions.AddressValidationRequired.HasValue ? reservation.Service.Location.Pickup.Address.TPA_Extensions.AddressValidationRequired.Value : false)
									//	{
									//		if (!LookupAddress(reservation.Service.Location.Pickup, pickupAddressStr, out pickupAddress))
									//		{

									//			logger.WarnFormat(Messages.Validation_LocationDetailByMapApi);
									//			throw new ValidationException(Messages.Validation_LocationDetailByMapApi);
									//		}
									//	}
									//	else
									//	{
									//		if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Latitude))
									//		{
									//			logger.Info("Skip MAP API Validation.");
									//			pickupAddress = ConvertAddress(reservation.Service.Location.Pickup);
									//		}
									//		else
									//		{

									//			logger.WarnFormat(Messages.Validation_LocationNoLatlon);
									//			throw new ValidationException(Messages.Validation_LocationNoLatlon);
									//		}
									//	}
									//}
									//else
									//{
									logger.Info("Skip MAP API Validation.");
									pickupAddress = ConvertAddress(reservation.Service.Location.Pickup);
									//}
									PickupAddressType = UDI.VTOD.Domain.Taxi.Const.AddressType.Address;
									PickupAddress = pickupAddress;
									logger.Info("Pickup AddressType: Address");
								}
								else if ((!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude) && (!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CountryName.Code)))
								{
									//tbp.PickupAddressOnlyContainsLatAndLong = true;
									MapService ms = new MapService();
									Map.DTO.Geolocation g = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(reservation.Service.Location.Pickup.Address.Latitude), Longitude = Convert.ToDecimal(reservation.Service.Location.Pickup.Address.Longitude) };
									string error = "";
									var addressList = ms.GetAddressesFromGeoLocationWithProximity(g, out error);
									if (string.IsNullOrWhiteSpace(error) && addressList.Any())
									{
										var add = addressList.FirstOrDefault();
										reservation.Service.Location.Pickup.Address.StreetNmbr = add.StreetNo;
										reservation.Service.Location.Pickup.Address.AddressLine = add.Street;
										reservation.Service.Location.Pickup.Address.BldgRoom = add.ApartmentNo;
										reservation.Service.Location.Pickup.Address.CityName = add.City;
										reservation.Service.Location.Pickup.Address.PostalCode = add.ZipCode;
										reservation.Service.Location.Pickup.Address.StateProv = new StateProv { StateCode = add.State };
										reservation.Service.Location.Pickup.Address.CountryName = new CountryName { Code = add.Country };
										reservation.Service.Location.Pickup.Address.StreetNmbr = add.StreetNo;
										reservation.Service.Location.Pickup.Address.StreetNmbr = add.StreetNo;

										PickupAddressType = UDI.VTOD.Domain.Taxi.Const.AddressType.Address;
										PickupAddress = ConvertAddress(reservation.Service.Location.Pickup);
										logger.Info("Pickup AddressType: Address");

									}
									else
									{
										logger.Warn(Messages.Validation_LocationDetail);
										throw new ValidationException(Messages.Validation_LocationDetail);
									}
								}
								else
								{
									var vtodException = VtodException.CreateException(ExceptionType.Taxi, 1);
									logger.Warn(vtodException.ExceptionMessage.Message); // Messages.Taxi_Common_UnknownError);
									throw vtodException;
								}
							}
							else if ((reservation.Service.Location.Pickup.Address == null) && (reservation.Service.Location.Pickup.AirportInfo != null))
							{
								//airport
								if (((reservation.Service.Location.Pickup.AirportInfo.Arrival != null) && (reservation.Service.Location.Pickup.AirportInfo.Departure != null)) || ((reservation.Service.Location.Pickup.AirportInfo.Arrival == null) && (reservation.Service.Location.Pickup.AirportInfo.Departure == null)))
								{
									logger.Warn(Messages.Validation_ArrivalAndDepartureAirportInfo);
									throw VtodException.CreateValidationException(Messages.Validation_ArrivalAndDepartureAirportInfo);
								}
								else if (reservation.Service.Location.Pickup.AirportInfo.Arrival != null && reservation.Service.Location.Pickup.AirportInfo.Departure == null)
								{
									//Arrival
									PickupAddressType = AddressType.Airport;
									PickupAirport = reservation.Service.Location.Pickup.AirportInfo.Arrival.LocationCode;
									logger.Info("Pickup AddressType: AirportInfo.Arrival");
								}
								else if (reservation.Service.Location.Pickup.AirportInfo.Arrival == null && reservation.Service.Location.Pickup.AirportInfo.Departure != null)
								{
									//Depature
									PickupAddressType = AddressType.Airport;
									PickupAirport = reservation.Service.Location.Pickup.AirportInfo.Departure.LocationCode;
									logger.Info("Pickup AddressType: AirportInfo.Depature");
								}

								//longitude and latitude

								double? longitude = null;
								double? latitude = null;
								if (!GetLongitudeAndLatitudeForAirport(PickupAirport, out longitude, out latitude))
								{
									throw VtodException.CreateException(ExceptionType.Taxi, 1010);// new ValidationException(Messages.Taxi_Common_UnableToFindAirportLongitudeAndLatitude);
								}


								//throw new ValidationException(Messages.Taxi_Common_InvalidAirportPickup);
							}
						}
						else
						{
							var vtodException = VtodException.CreateFieldRequiredValidationException("Pickup");
							logger.Warn(vtodException.ExceptionMessage.Message);
							throw vtodException; // new ValidationException(Messages.Taxi_Common_InvalidReservation);
						}
					}
					else
					{
						var vtodException = VtodException.CreateFieldRequiredValidationException("Reservation.Service.Location");
						logger.Warn(vtodException.ExceptionMessage.Message);
						throw vtodException; // new ValidationException(Messages.Taxi_Common_InvalidReservation);
					}
				}
				else
				{
					var vtodException = VtodException.CreateFieldRequiredValidationException("GroundReservation.Service");
					logger.Warn(vtodException.ExceptionMessage.Message);
					throw vtodException; // new ValidationException(Messages.Taxi_Common_InvalidReservation);
				}
				#endregion

				#region validate fleet
				//taxi_fleet fleet;
				if (GetFleet(PickupAddressType, PickupAddress, PickupAirport, out fleet))
				{
					logger.Info("Found fleet");
				}
				else
				{
					var vtodException = VtodException.CreateException(ExceptionType.Taxi, 1011);
					logger.Warn(vtodException.ExceptionMessage.Message);
					throw vtodException;// new ValidationException(Messages.Taxi_Common_UnableToFindFleetByAddressOrAirportInfo);

				}
				#endregion
			}
			#region Temprory HardCoding - Please comment out this block after 6/28 3AM
			var throwBlockOutException = false;

			#region KC 1
			try
			{
				if (fleet.Id == 25)
				{
					if ((request.TPA_Extensions == null) || (!request.TPA_Extensions.PickMeUpNow.HasValue) || !request.TPA_Extensions.PickMeUpNow.Value)
					{
						//if (/* !tbp.PickupNow && */ DateTime.Now < new DateTime(2015, 01, 02, 0, 0, 0))
						//{
						var from = new DateTime(2015, 8, 1, 14, 0, 0);
						var to = new DateTime(2015, 8, 2, 1, 0, 0);

						var puTime = request.GroundReservations.FirstOrDefault().Service.Location.Pickup.DateTime.ToDateTime();
						if (puTime >= from && puTime <= to)
						{
							throwBlockOutException = true;
						}
						//}
					}
				}
			}
			catch { }
			#endregion

			#region KC 2
			try
			{
				if (fleet.Id == 25)
				{
					if ((request.TPA_Extensions == null) || (!request.TPA_Extensions.PickMeUpNow.HasValue) || !request.TPA_Extensions.PickMeUpNow.Value)
					{
						//if (/* !tbp.PickupNow && */ DateTime.Now < new DateTime(2015, 01, 02, 0, 0, 0))
						//{
						var from = new DateTime(2015, 7, 28, 16, 0, 0);
						var to = new DateTime(2015, 7, 29, 1, 0, 0);

						var puTime = request.GroundReservations.FirstOrDefault().Service.Location.Pickup.DateTime.ToDateTime();
						if (puTime >= from && puTime <= to)
						{
							throwBlockOutException = true;
						}
						//}
					}
				}
			}
			catch { }
			#endregion

			#region Pittsburg
			try
			{
				if (fleet.Id == 23)
				{
					if ((request.TPA_Extensions == null) || (!request.TPA_Extensions.PickMeUpNow.HasValue) || !request.TPA_Extensions.PickMeUpNow.Value)
					{
						//if (/* !tbp.PickupNow && */ DateTime.Now < new DateTime(2015, 01, 02, 0, 0, 0))
						//{
						var from = new DateTime(2015, 8, 1, 16, 0, 0);
						var to = new DateTime(2015, 8, 2, 1, 0, 0);

						var puTime = request.GroundReservations.FirstOrDefault().Service.Location.Pickup.DateTime.ToDateTime();
						if (puTime >= from && puTime <= to)
						{
							throwBlockOutException = true;
						}
						//}
					}
				}
			}
			catch { }
			#endregion



			if (throwBlockOutException)
			{
				throw VtodException.CreateException(ExceptionType.Taxi, 2013);// new TaxiException(Messages.Taxi_BookingFailure);
			}
			#endregion


			serviceAPIID = GetServiceAPIId(fleet.Id, TaxiServiceAPIPreferenceConst.Book);

			if (serviceAPIID == -1)
			{
				var vtodException = VtodException.CreateException(ExceptionType.Taxi, 1011);
				logger.Warn(vtodException.ExceptionMessage.Message);
				throw vtodException;// new ValidationException(Messages.Taxi_Common_UnableToFindFleetByAddressOrAirportInfo);
			}

			return serviceAPIID;
		}

		public long GetServiceAPIIdByRequest(GetFareDetailRQ request)
		{
			long serviceAPIID = -1;
			long fleetId = -1;
			bool result = true;
			string tripID = null;

			#region Validate Reference Id
			if (request.Reference == null || !request.Reference.Any() || string.IsNullOrWhiteSpace(request.Reference.First().ID))
			{
				result = false;
				logger.Warn("Reference.ID is empty.");
			}
			else
			{
				logger.Info("Request is valid.");
			}
			#endregion

			#region Validate Trip
			vtod_trip t;
			if (result)
			{

				var confType = request.Reference.First().Type;

				if (confType.ToLower().Trim() == Common.DTO.Enum.ConfirmationType.Confirmation.ToString().ToLower())
				{
					tripID = request.Reference.First().ID;
				}
				else if (confType.ToLower().Trim() == Common.DTO.Enum.ConfirmationType.DispatchConfirmation.ToString().ToLower())
				{
					var dispatchTripID = request.Reference.First().ID;
					var fleetID = request.Reference.First().ID_Context.ToInt32();
					using (var db = new VTODEntities())
					{
						tripID = db.taxi_trip.Where(s => s.DispatchTripId == dispatchTripID && s.FleetId == fleetID).First().Id.ToString();
					}
				}

				t = GetTaxiTrip(tripID);

				if (t != null)
				{
					fleetId = t.taxi_trip.FleetId;
					logger.InfoFormat("Trip found. tripID={0}", tripID);
				}
				else
				{
					result = false;
					logger.WarnFormat("Cannot find this trip. tripID={0}", tripID);
				}
			}
			else
			{
				result = false;
				logger.Warn("Cannot find trip by invalid referenceId");
			}
			#endregion

			if (result)
			{
				serviceAPIID = GetServiceAPIId(fleetId, TaxiServiceAPIPreferenceConst.Status);
				if (serviceAPIID == -1)
				{
					var vtodException = VtodException.CreateException(ExceptionType.Taxi, 1011);
					logger.Warn(vtodException.ExceptionMessage.Message);
					throw vtodException;// new ValidationException(Messages.Taxi_Common_UnableToFindFleetByAddressOrAirportInfo);
				}
			}

			return serviceAPIID;
		}
         
		//UtilityAvailableFleetsRQ
		public long GetServiceAPIIdByRequest(UtilityAvailableFleetsRQ request)
		{
			long serviceAPIID = -1;
			bool result = true;
			Map.DTO.Address address = new Map.DTO.Address();

			#region Validate Pickup Address
			if (result)
			{
				//Pickup_Dropoff_Stop stop = request.Service.Pickup;

				if (request.Address != null)
				{
					if (!string.IsNullOrWhiteSpace(request.Address.CountryName.Code) && !string.IsNullOrWhiteSpace(request.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(request.Address.CityName) && !string.IsNullOrWhiteSpace(request.Address.PostalCode))
					{
						address = new Map.DTO.Address { Country = request.Address.CountryName.Code, State = request.Address.StateProv.StateCode, City = request.Address.CityName, ZipCode = request.Address.PostalCode, Geolocation = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(request.Address.Latitude), Longitude = Convert.ToDecimal(request.Address.Longitude) } };
					}
					else
					{
						result = false;
						logger.Warn("Cannot find any address/locationName from address.");
					}
				}
				else
				{
					result = false;
					logger.Warn("Cannot find any address/locationName from address.");
				}
			}
			else
			{
				result = false;
			}
			#endregion

			#region Validate Fleet
			taxi_fleet fleet = null;
			if (result)
			{
				if (GetFleet(AddressType.Address, address, string.Empty, out fleet))
				{
					logger.InfoFormat("Fleet found. ID={0}", fleet.Id);
				}
				else
				{
					result = false;
					logger.Warn("Unable to find fleet by this address or landmark.");
				}
			}
			else
			{
				result = false;
				logger.Warn("Unable to find fleet by invalid address or landmark.");
			}
			#endregion

			if (result)
			{
				serviceAPIID = GetServiceAPIId(fleet.Id, TaxiServiceAPIPreferenceConst.GetAvailableFleets);
				if (serviceAPIID == -1)
				{
					var vtodException = VtodException.CreateException(ExceptionType.Taxi, 1011);
					logger.Warn(vtodException.ExceptionMessage.Message);
					throw vtodException;// new ValidationException(Messages.Taxi_Common_UnableToFindFleetByAddressOrAirportInfo);
				}
			}

			return serviceAPIID;
		}
        public List<string> MtdataEndpointConfiguration(long serviceAPIID,long Id)
        {
            List<string> endpointNames = new List<string>();
            if (serviceAPIID != 0)
            {
                if (serviceAPIID == TaxiServiceAPIConst.MTData || serviceAPIID == TaxiServiceAPIConst.Texas || serviceAPIID == TaxiServiceAPIConst.Phoenix)
                {
                    taxi_mtdata_configuration config = GetConfigurationNames(Id);
                    if (config != null)
                    {
                        endpointNames.Add(config.AuthenticateServiceEndpointName);
                        endpointNames.Add(config.BookingServiceEndpointName);
                        endpointNames.Add(config.OsiServiceEndpointName);
                        endpointNames.Add(config.AddressServiceEndpointName);
                    }
                    else
                    {
                        endpointNames.Add("MTDataAuthenticationWebServiceSoap");
                        endpointNames.Add("MTDataBookingWebServiceSoap");
                        endpointNames.Add("MTDataOSIWebServiceSoap");
                        endpointNames.Add("MTDataAddressWebServiceSoap");
                    }
                }
            }
            return endpointNames;
        }
		public bool ValidateBookingRequest(TokenRS tokenVTOD, OTA_GroundBookRQ request, out TaxiBookingParameter tbp, taxi_fleet fleet)
		{
			bool result = true;
			Stopwatch sw = new Stopwatch();
			sw.Start();
			tbp = new TaxiBookingParameter();
			tbp.Fleet = fleet;
			Stopwatch swEachPart = new Stopwatch();



			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion

			#region Validate Reservation
			if (request.GroundReservations.Any())
			{
				GroundReservation reservation = request.GroundReservations.FirstOrDefault();

				#region Set token, request
				tbp.tokenVTOD = tokenVTOD;
				tbp.request = request;
				#endregion

				#region Set Source and Device and ref
				if (request.TPA_Extensions != null && !string.IsNullOrWhiteSpace(request.TPA_Extensions.Source))
				{
					tbp.Source = request.TPA_Extensions.Source;
				}

				if (request.TPA_Extensions != null && !string.IsNullOrWhiteSpace(request.TPA_Extensions.Device))
				{
					tbp.Device = request.TPA_Extensions.Device;
				}

				if (!string.IsNullOrWhiteSpace(request.EchoToken))
				{
					tbp.Ref = request.EchoToken;
				}
				#endregion

				swEachPart.Restart();
				#region Validate Member Info
				//As of this version, all requester should pass memberID.
				//We need to modify this based on some others who do not pass memberID
				if (request.TPA_Extensions == null && string.IsNullOrWhiteSpace(request.TPA_Extensions.MemberID))
				{
					result = false;
					var vtodException = VtodException.CreateFieldRequiredValidationException("MemberID");//ExceptionType.Taxi, 2001);
					logger.Warn(vtodException.ExceptionMessage.Message);
					throw vtodException;// new ValidationException(Messages.Taxi_Common_UnableToFindFleetByAddressOrAirportInfo);
				}
				else
				{
					tbp.MemberID = request.TPA_Extensions.MemberID.ToInt32();
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateBookingRequest_ValidateMemberInfo");
				swEachPart.Stop();
				logger.DebugFormat("Validate member Info Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Validate Customer
				if (reservation.Passenger != null)
				{
					//Name
					if (reservation.Passenger.Primary != null)
					{
						if (reservation.Passenger.Primary.PersonName != null)
						{
							if (string.IsNullOrWhiteSpace(reservation.Passenger.Primary.PersonName.GivenName) || string.IsNullOrWhiteSpace(reservation.Passenger.Primary.PersonName.Surname))
							{
								result = false;
								var vtodException = VtodException.CreateFieldRequiredValidationException("PassengerGivenName|PassengerSurname");
								logger.Warn(vtodException.ExceptionMessage.Message);
								throw vtodException; // new ValidationException(Messages.Taxi_Common_InvalidReservation);
							}
							else
							{
								tbp.FirstName = reservation.Passenger.Primary.PersonName.GivenName.Trim();
								tbp.LastName = reservation.Passenger.Primary.PersonName.Surname.Trim();
								logger.Info("First name and last name are valid.");
							}
						}
						else
						{
							result = false;
							var vtodException = VtodException.CreateFieldRequiredValidationException("PassengerName");
							logger.Warn(vtodException.ExceptionMessage.Message);
							throw vtodException; // new ValidationException(Messages.Taxi_Common_InvalidReservation);
						}

						//Telephone
						if (reservation.Passenger.Primary.Telephones.Any())
						{
							Telephone p = reservation.Passenger.Primary.Telephones.FirstOrDefault();
							string areacode = ExtractDigits(p.AreaCityCode);
							string phonenumber = ExtractDigits(p.PhoneNumber);
							if (areacode.Length.Equals(3) && phonenumber.Length.Equals(7))
							{
								tbp.PhoneNumber = string.Format("{0}{1}", areacode, phonenumber);
								logger.Info("Phone number(s) is/are valid.");
							}
							else
							{
								result = false;
								var vtodException = VtodException.CreateFieldFormatValidationException("Telephone", "10 digits");
								logger.Warn(vtodException.ExceptionMessage.Message);
								throw vtodException; // new ValidationException(Messages.Taxi_Common_InvalidReservation);
							}
						}
						else
						{
							result = false;
							var vtodException = VtodException.CreateFieldRequiredValidationException("Telephones");
							logger.Warn(vtodException.ExceptionMessage.Message);
							throw vtodException; // new ValidationException(Messages.Taxi_Common_InvalidReservation);
						}
						//email
						if (reservation.Passenger.Primary.Emails.Any())
						{
							Email e = reservation.Passenger.Primary.Emails.FirstOrDefault();


							string emailPattern = @"^(?("")("".+?(?<!\\)""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
												 @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]*\.)+[a-zA-Z0-9][\-a-zA-Z0-9]{0,22}[a-zA-Z0-9]))$";

							if (!string.IsNullOrWhiteSpace(e.Value) && Regex.IsMatch(e.Value, emailPattern, RegexOptions.IgnorePatternWhitespace))
							{
								tbp.EmailAddress = e.Value;
								logger.Info(Messages.ECar_Common_ValidReservationPassengerPrimaryTelephones);
							}
						}
						if (result)
						{
							////Create or GetCustomer
							//taxi_customer customer = GetTaxiCustomer(reservation);
							//tbp.Customer = customer;
							logger.InfoFormat("Customer firstname={0}, lastname={1}, phonenumber={2}", tbp.FirstName, tbp.LastName, tbp.PhoneNumber);
						}
						else
						{
							result = false;
							var vtodException = VtodException.CreateException(ExceptionType.Taxi, 2008);
							logger.Warn(vtodException.ExceptionMessage.Message);
							throw vtodException; // new ValidationException(Messages.Taxi_Common_InvalidReservation);
						}
					}
					else
					{
						result = false;
						var vtodException = VtodException.CreateFieldRequiredValidationException("PassengerPrimary");
						logger.Warn(vtodException.ExceptionMessage.Message);
						throw vtodException; // new ValidationException(Messages.Taxi_Common_InvalidReservation);
					}
				}
				else
				{
					result = false;
					var vtodException = VtodException.CreateFieldRequiredValidationException("Passenger");
					logger.Warn(vtodException.ExceptionMessage.Message);
					throw vtodException; // new ValidationException(Messages.Taxi_Common_InvalidReservation);
				}

				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateBookingRequest_ValidateCustomer");
				swEachPart.Stop();
				logger.DebugFormat("Validate Customer Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Validate Pickup Address

				if (reservation.Service != null)
				{
					if (reservation.Service.Location != null)
					{
						if (reservation.Service.Location.Pickup != null)
						{
							if (((reservation.Service.Location.Pickup.Address != null) && (reservation.Service.Location.Pickup.AirportInfo != null)) || ((reservation.Service.Location.Pickup.Address == null) && (reservation.Service.Location.Pickup.AirportInfo == null)))
							{
								result = false;
								logger.Warn(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
								throw new ValidationException(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
							}
							else if ((reservation.Service.Location.Pickup.Address != null) && (reservation.Service.Location.Pickup.AirportInfo == null))
							{

								if ((string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude) || (string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CountryName.Code)))
								{
									result = false;
									logger.Warn(Messages.Validation_LocationDetail);
									throw new ValidationException(Messages.Validation_LocationDetail);
								}
								else if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.AddressLine) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CityName) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.PostalCode) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CountryName.Code))
								{
									//Address
									Map.DTO.Address pickupAddress = null;
									string pickupAddressStr = GetAddressString(reservation.Service.Location.Pickup.Address);
									//if (reservation.Service.Location.Pickup.Address.TPA_Extensions != null)
									//{
									//	if (reservation.Service.Location.Pickup.Address.TPA_Extensions.AddressValidationRequired.HasValue ? reservation.Service.Location.Pickup.Address.TPA_Extensions.AddressValidationRequired.Value : false)
									//	{
									//		if (!LookupAddress(reservation.Service.Location.Pickup, pickupAddressStr, out pickupAddress))
									//		{
									//			result = false;
									//			logger.WarnFormat(Messages.Validation_LocationDetailByMapApi);
									//			throw new ValidationException(Messages.Validation_LocationDetailByMapApi);
									//		}
									//	}
									//	else
									//	{
									//		if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Latitude))
									//		{
									//			logger.Info("Skip MAP API Validation.");
									//			pickupAddress = ConvertAddress(reservation.Service.Location.Pickup);
									//		}
									//		else
									//		{
									//			result = false;
									//			logger.WarnFormat(Messages.Validation_LocationNoLatlon);
									//			throw new ValidationException(Messages.Validation_LocationNoLatlon);
									//		}
									//	}
									//}
									//else
									//{
									logger.Info("Skip MAP API Validation.");
									pickupAddress = ConvertAddress(reservation.Service.Location.Pickup);
									//}
									tbp.PickupAddressType = UDI.VTOD.Domain.Taxi.Const.AddressType.Address;
									tbp.PickupAddress = pickupAddress;
									logger.Info("Pickup AddressType: Address");
								}
								else if ((!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude) && (!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CountryName.Code)))
								{
									tbp.PickupAddressOnlyContainsLatAndLong = true;
									MapService ms = new MapService();
									Map.DTO.Geolocation g = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(reservation.Service.Location.Pickup.Address.Latitude), Longitude = Convert.ToDecimal(reservation.Service.Location.Pickup.Address.Longitude) };
									string error = "";
									var addressList = ms.GetAddressesFromGeoLocationWithProximity(g, out error);
									if (string.IsNullOrWhiteSpace(error) && addressList.Any())
									{
										var add = addressList.FirstOrDefault();
										reservation.Service.Location.Pickup.Address.StreetNmbr = add.StreetNo;
										reservation.Service.Location.Pickup.Address.AddressLine = add.Street;
										reservation.Service.Location.Pickup.Address.BldgRoom = add.ApartmentNo;
										reservation.Service.Location.Pickup.Address.CityName = add.City;
										reservation.Service.Location.Pickup.Address.PostalCode = add.ZipCode;
										reservation.Service.Location.Pickup.Address.StateProv = new StateProv { StateCode = add.State };
										reservation.Service.Location.Pickup.Address.CountryName = new CountryName { Code = add.Country };
										reservation.Service.Location.Pickup.Address.StreetNmbr = add.StreetNo;
										reservation.Service.Location.Pickup.Address.StreetNmbr = add.StreetNo;

										tbp.PickupAddressType = UDI.VTOD.Domain.Taxi.Const.AddressType.Address;
										tbp.PickupAddress = ConvertAddress(reservation.Service.Location.Pickup);
										logger.Info("Pickup AddressType: Address");

									}
									else
									{
										result = false;
										logger.Warn(Messages.Validation_LocationDetail);
										throw new ValidationException(Messages.Validation_LocationDetail);
									}
								}
								else
								{
									result = false;
									var vtodException = VtodException.CreateException(ExceptionType.Taxi, 1);
									logger.Warn(vtodException.ExceptionMessage.Message); // Messages.Taxi_Common_UnknownError);
									throw vtodException;
								}
							}
							else if ((reservation.Service.Location.Pickup.Address == null) && (reservation.Service.Location.Pickup.AirportInfo != null))
							{
								//airport
								if (((reservation.Service.Location.Pickup.AirportInfo.Arrival != null) && (reservation.Service.Location.Pickup.AirportInfo.Departure != null)) || ((reservation.Service.Location.Pickup.AirportInfo.Arrival == null) && (reservation.Service.Location.Pickup.AirportInfo.Departure == null)))
								{
									result = false;
									logger.Warn(Messages.Validation_ArrivalAndDepartureAirportInfo);
									throw VtodException.CreateValidationException(Messages.Validation_ArrivalAndDepartureAirportInfo);
								}
								else if (reservation.Service.Location.Pickup.AirportInfo.Arrival != null && reservation.Service.Location.Pickup.AirportInfo.Departure == null)
								{
									//Arrival
									tbp.PickupAddressType = AddressType.Airport;
									tbp.PickupAirport = reservation.Service.Location.Pickup.AirportInfo.Arrival.LocationCode;
									logger.Info("Pickup AddressType: AirportInfo.Arrival");
								}
								else if (reservation.Service.Location.Pickup.AirportInfo.Arrival == null && reservation.Service.Location.Pickup.AirportInfo.Departure != null)
								{
									//Depature

									tbp.PickupAddressType = AddressType.Airport;
									tbp.PickupAirport = reservation.Service.Location.Pickup.AirportInfo.Departure.LocationCode;
									logger.Info("Pickup AddressType: AirportInfo.Depature");
								}

								//longitude and latitude
								if (result)
								{
									double? longitude = null;
									double? latitude = null;
									if (GetLongitudeAndLatitudeForAirport(tbp.PickupAirport, out longitude, out latitude))
									{
										tbp.LongitudeForPickupAirport = longitude.Value;
										tbp.LatitudeForPickupAirport = latitude.Value;
									}
									else
									{
										throw VtodException.CreateException(ExceptionType.Taxi, 1010);// new ValidationException(Messages.Taxi_Common_UnableToFindAirportLongitudeAndLatitude);
									}
								}

								//throw new ValidationException(Messages.Taxi_Common_InvalidAirportPickup);
							}
						}
						else
						{
							result = false;
							var vtodException = VtodException.CreateFieldRequiredValidationException("Pickup");
							logger.Warn(vtodException.ExceptionMessage.Message);
							throw vtodException; // new ValidationException(Messages.Taxi_Common_InvalidReservation);
						}
					}
					else
					{
						result = false;
						var vtodException = VtodException.CreateFieldRequiredValidationException("Reservation.Service.Location");
						logger.Warn(vtodException.ExceptionMessage.Message);
						throw vtodException; // new ValidationException(Messages.Taxi_Common_InvalidReservation);
					}
				}
				else
				{
					result = false;
					var vtodException = VtodException.CreateFieldRequiredValidationException("GroundReservation.Service");
					logger.Warn(vtodException.ExceptionMessage.Message);
					throw vtodException; // new ValidationException(Messages.Taxi_Common_InvalidReservation);
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateBookingRequest_ValidatePickupAddress");
				swEachPart.Stop();
				logger.DebugFormat("Validate Pickup address Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Validate Fleet
				if (result)
				{
					//taxi_fleet fleet = null;
					if (tbp.Fleet != null)//GetFleet(tbp.PickupAddressType, tbp.PickupAddress, tbp.PickupAirport, out fleet))
					{
						//fleet=tbp.Fleet ;
						logger.InfoFormat("Fleet found. ID={0}", tbp.Fleet.Id);

						if (fleet != null)
						{
							tbp.serviceAPIID = GetTaxiFleetServiceAPIPreference(tbp.Fleet.Id, TaxiServiceAPIPreferenceConst.Book).ServiceAPIId;
                            
                        }
					}
					else
					{
						result = false;
						var vtodException = VtodException.CreateException(ExceptionType.Taxi, 1011);
						logger.Warn(vtodException.ExceptionMessage.Message);
						throw vtodException;// new ValidationException(Messages.Taxi_Common_UnableToFindFleetByAddressOrAirportInfo);
					}
				}
				else
				{
					result = false;
					var vtodException = VtodException.CreateException(ExceptionType.Taxi, 2001);
					logger.Warn(vtodException.ExceptionMessage.Message);
					throw vtodException;// new ValidationException(Messages.Taxi_Common_UnableToFindFleetByAddressOrAirportInfo);
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateBookingRequest_ValidateFleet");
				swEachPart.Stop();
				logger.DebugFormat("Validate fleet Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Validate User
				if (result)
				{
					taxi_fleet_user user = null;
					if (GetTaxiFleetUser(tbp.Fleet.Id, tokenVTOD.Username, out user))
					{
						tbp.User = user;
						logger.Info("This user has sufficient privilege.");
					}
					else
					{
						result = false;
						var vtodException = VtodException.CreateException(ExceptionType.Taxi, 102);
						logger.Warn(vtodException.ExceptionMessage.Message);
						throw vtodException; // new ValidationException(Messages.Taxi_Common_InsufficientPrivilege(tokenVTOD.Username, tbp.Fleet.Id));
					}
				}
				else
				{
					result = false;
					var vtodException = VtodException.CreateException(ExceptionType.Taxi, 2001);
					logger.Warn(vtodException.ExceptionMessage.Message);
					throw vtodException;// new ValidationException(Messages.Taxi_Common_UnableToFindFleetByAddressOrAirportInfo);
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateBookingRequest_ValidateUser");
				swEachPart.Stop();
				logger.DebugFormat("Validate user Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Validate PickupDateTime
				if (result)
				{
					#region determine Pick me up now
					if (request.TPA_Extensions != null && request.TPA_Extensions.PickMeUpNow.HasValue)
					{
						if (request.TPA_Extensions.PickMeUpNow.Value)
						{
							tbp.PickupNow = true;
						}
						else
						{
							tbp.PickupNow = false;
						}
					}
					else
					{
						//request
						//if (tbp.PickupDateTime.ToUtc(tbp.Fleet.ServerUTCOffset) <= DateTime.UtcNow.AddMinutes(5))
						try
						{

							if (reservation.Service.Location.Pickup.DateTime.ToDateTime().Value.ToUtc(tbp.Fleet.ServerUTCOffset) <= DateTime.UtcNow.AddMinutes(5))
							{
								tbp.PickupNow = true;
							}
							else
							{
								tbp.PickupNow = false;
							}
						}
						catch
						{ }
						//tbp.PickupNow = false;
					}
					#endregion

					#region Validate Pick me up now against DB fleet
					bool pickMeUpNow = true;
					bool pickMeUpLater = true;
					GetPickMeupOption(tbp.Fleet, out pickMeUpNow, out pickMeUpLater);
                    #region Check Blackout
                    CheckBlackOut(tbp.Fleet, request);
					#endregion
					if (tbp.PickupNow == pickMeUpNow || !tbp.PickupNow == pickMeUpLater)
					{
						logger.InfoFormat("Valid pick me up request. Fleet={0}, Trip={1}", tbp.Fleet.PickMeUpNowOption, tbp.PickupNow);

					}
					else
					{
						logger.WarnFormat("InValid pick me up request. Fleet={0}, Trip={1}", tbp.Fleet.PickMeUpNowOption, tbp.PickupNow);
						result = false;
						if (tbp.PickupNow)
						{
							throw VtodException.CreateException(ExceptionType.Taxi, 2011);
						}
						else
						{
							throw VtodException.CreateException(ExceptionType.Taxi, 2012);
						}
					}

					#endregion


					#region determine pick me up now Time

					DateTime pickupDateTime;
					//if (DateTime.TryParseExact(reservation.Service.Location.Pickup.DateTime, ConfigurationManager.AppSettings["DateTimeFormatForPickupAndDropoff"], null, DateTimeStyles.None, out pickupDateTime))
					if (DateTime.TryParse(reservation.Service.Location.Pickup.DateTime, out pickupDateTime))
					{
						tbp.PickupDateTime = pickupDateTime;
						logger.Info("PickupDateTime is correct");
					}
					else if (tbp.PickupNow && !DateTime.TryParse(reservation.Service.Location.Pickup.DateTime, out pickupDateTime))
					{
						tbp.PickupDateTime = DateTime.UtcNow.FromUtc(tbp.Fleet.ServerUTCOffset);
						logger.InfoFormat("There is no Pickup datetime for this request. But it has pickup now value. So the pickup time will set as {0:yyyy/MM/dd HH:mm:ss}.", tbp.PickupDateTime);
					}
					else
					{
						result = false;
						var vtodException = VtodException.CreateFieldFormatValidationException("PickupDateTime", "DateTime");
						logger.Warn(vtodException.ExceptionMessage.Message);
						throw vtodException; // new ValidationException(Messages.Taxi_Common_InvalidReservationServiceLocationPickupDateTime(reservation.Service.Location.Pickup.DateTime));
					}

					#endregion
				}
				else
				{
					result = false;
					var vtodException = VtodException.CreateException(ExceptionType.Taxi, 2001);
					logger.Warn(vtodException.ExceptionMessage.Message);
					throw vtodException;// new ValidationException(Messages.Taxi_Common_UnableToFindFleetByAddressOrAirportInfo);
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateBookingRequest_ValidatePickupDateTime");
				swEachPart.Stop();
				logger.DebugFormat("Validate PickupTime Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Validate duplicated booking trip
				if (result)
				{
					if (IsAbleToBookTrip(tbp))
					{
						logger.Info("This trip can be booked.");
					}
					else
					{
						result = false;
						var vtodException = VtodException.CreateException(ExceptionType.Taxi, 2006);
						logger.Warn(vtodException.ExceptionMessage.Message);
						throw vtodException; // new ValidationException(Messages.Taxi_Common_DuplicatedTrips);
					}
				}
				else
				{
					result = false;
					var vtodException = VtodException.CreateException(ExceptionType.Taxi, 2001);
					logger.Warn(vtodException.ExceptionMessage.Message);
					throw vtodException;// new ValidationException(Messages.Taxi_Common_UnableToFindFleetByAddressOrAirportInfo);
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateBookingRequest_ValidateDuplicatedBookingTrip");
				swEachPart.Stop();
				logger.DebugFormat("Validate duplicated trip Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Validate Dropoff Address
				//DropOff is optional: By_Pouyan
				if (reservation.Service.Location != null && reservation.Service.Location.Dropoff != null)
					if (result)
					{
						if (reservation.Service != null)
						{
							if (reservation.Service.Location != null)
							{
								if (reservation.Service.Location.Dropoff != null)
								{
									using (VTODEntities context = new VTODEntities())
									{
										if (((reservation.Service.Location.Dropoff.Address != null) && (reservation.Service.Location.Dropoff.AirportInfo != null)) || ((reservation.Service.Location.Dropoff.Address == null) && (reservation.Service.Location.Dropoff.AirportInfo == null)))
										{
											result = false;
											logger.Warn(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
											throw VtodException.CreateValidationException(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
										}
										else if ((reservation.Service.Location.Dropoff.Address != null) && (reservation.Service.Location.Dropoff.AirportInfo == null))
										{
											//Address
											if ((string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.Longitude) || (string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CountryName.Code)))
											//if (string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CountryName.Code))
											{
												result = false;
												logger.Warn(Messages.Validation_LocationDetail);
												throw VtodException.CreateValidationException(Messages.Validation_LocationDetail);
											}
											else if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.AddressLine) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CityName) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.PostalCode) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CountryName.Code))
											{
												Map.DTO.Address dropoffAddress = null;
												string dropoffAddressStr = GetAddressString(reservation.Service.Location.Dropoff.Address);
												//if (reservation.Service.Location.Dropoff.Address.TPA_Extensions != null)
												//{
												//	if (reservation.Service.Location.Dropoff.Address.TPA_Extensions.AddressValidationRequired.HasValue ? reservation.Service.Location.Dropoff.Address.TPA_Extensions.AddressValidationRequired.Value : false)
												//	{
												//		if (!LookupAddress(reservation.Service.Location.Dropoff, dropoffAddressStr, out dropoffAddress))
												//		{
												//			result = false;
												//			logger.WarnFormat(Messages.Validation_LocationDetailByMapApi);
												//			throw VtodException.CreateValidationException(Messages.Validation_LocationDetailByMapApi);
												//		}
												//	}
												//	else
												//	{
												//		if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.Longitude) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.Latitude))
												//		{
												//			logger.Info("Skip MAP API Validation.");
												//			dropoffAddress = ConvertAddress(reservation.Service.Location.Dropoff);
												//		}
												//		else
												//		{
												//			result = false;
												//			logger.WarnFormat(Messages.Validation_LocationNoLatlon);
												//			throw new ValidationException(Messages.Validation_LocationNoLatlon);
												//		}
												//	}
												//}
												//else
												//{
												logger.Info("Skip MAP API Validation.");
												dropoffAddress = ConvertAddress(reservation.Service.Location.Dropoff);
												//}

												tbp.DropOffAddress = dropoffAddress;
												tbp.DropOffAddressType = UDI.VTOD.Domain.Taxi.Const.AddressType.Address;
												logger.Info("Dropoff AddressType=Address");
											}
											else if ((!string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.Longitude) && (!string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CountryName.Code)))
											{
												tbp.DropOffAddressOnlyContainsLatAndLong = true;
												MapService ms = new MapService();
												Map.DTO.Geolocation g = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(reservation.Service.Location.Dropoff.Address.Latitude), Longitude = Convert.ToDecimal(reservation.Service.Location.Dropoff.Address.Longitude) };
												string error = "";
												var addressList = ms.GetAddressesFromGeoLocationWithProximity(g, out error);
												if (string.IsNullOrWhiteSpace(error) && addressList.Any())
												{
													var add = addressList.FirstOrDefault();
													reservation.Service.Location.Dropoff.Address.StreetNmbr = add.StreetNo;
													reservation.Service.Location.Dropoff.Address.AddressLine = add.Street;
													reservation.Service.Location.Dropoff.Address.BldgRoom = add.ApartmentNo;
													reservation.Service.Location.Dropoff.Address.CityName = add.City;
													reservation.Service.Location.Dropoff.Address.PostalCode = add.ZipCode;
													reservation.Service.Location.Dropoff.Address.StateProv = new StateProv { StateCode = add.State };
													reservation.Service.Location.Dropoff.Address.CountryName = new CountryName { Code = add.Country };
													reservation.Service.Location.Dropoff.Address.StreetNmbr = add.StreetNo;
													reservation.Service.Location.Dropoff.Address.StreetNmbr = add.StreetNo;

													tbp.DropOffAddressType = UDI.VTOD.Domain.Taxi.Const.AddressType.Address;
													tbp.DropOffAddress = ConvertAddress(reservation.Service.Location.Dropoff);
													logger.Info("Dropoff AddressType: Address");

												}
												else
												{
													result = false;
													logger.Warn(Messages.Validation_LocationDetail);
													throw new ValidationException(Messages.Validation_LocationDetail);
												}
											}
											else
											{
												result = false;
												var vtodException = VtodException.CreateException(ExceptionType.Taxi, 1);
												logger.Warn(vtodException.ExceptionMessage.Message);
												throw vtodException;
											}
										}
										else if ((reservation.Service.Location.Dropoff.Address == null) && (reservation.Service.Location.Dropoff.AirportInfo != null))
										{

											//airport
											if (((reservation.Service.Location.Dropoff.AirportInfo.Arrival != null) && (reservation.Service.Location.Dropoff.AirportInfo.Departure != null)) || ((reservation.Service.Location.Dropoff.AirportInfo.Arrival == null) && (reservation.Service.Location.Dropoff.AirportInfo.Departure == null)))
											{
												result = false;
												logger.Warn(Messages.Validation_ArrivalAndDepartureAirportInfo);
												throw VtodException.CreateValidationException(Messages.Validation_ArrivalAndDepartureAirportInfo);
											}
											else if (reservation.Service.Location.Dropoff.AirportInfo.Arrival != null && reservation.Service.Location.Dropoff.AirportInfo.Departure == null)
											{
												//Arrival
												tbp.DropOffAddressType = AddressType.Airport;
												tbp.DropoffAirport = reservation.Service.Location.Dropoff.AirportInfo.Arrival.LocationCode;
												logger.Info("Dropoff AddressType: AirportInfo.Arrival");
											}
											else if (reservation.Service.Location.Dropoff.AirportInfo.Arrival == null && reservation.Service.Location.Dropoff.AirportInfo.Departure != null)
											{
												//Depature
												tbp.DropOffAddressType = AddressType.Airport;
												tbp.DropoffAirport = reservation.Service.Location.Dropoff.AirportInfo.Departure.LocationCode;
												logger.Info("Dropoff AddressType: AirportInfo.Depature");
											}

											//longitude and latitude
											if (result)
											{
												double? longitude = null;
												double? latitude = null;
												if (GetLongitudeAndLatitudeForAirport(tbp.DropoffAirport, out longitude, out latitude))
												{
													tbp.LongitudeForDropoffAirport = longitude.Value;
													tbp.LatitudeForDropoffAirport = latitude.Value;
												}
												else
												{
													throw VtodException.CreateException(ExceptionType.Taxi, 1010);// new ValidationException(Messages.Taxi_Common_UnableToFindAirportLongitudeAndLatitude);
												}
											}
										}
									}
								}
								//else
								//{
								//	tbp.DropoffAddressType = AddressType.Empty;
								//	logger.Info(Messages.Taxi_Common_InvalidReservationServiceLocationDropoff);
								//	throw new ValidationException(Messages.Taxi_Common_InvalidReservationServiceLocationDropoff);
								//}
							}
							else
							{
								result = false;
								var vtodException = VtodException.CreateFieldRequiredValidationException("Reservation.Service.Location");
								logger.Warn(vtodException.ExceptionMessage.Message);
								throw vtodException; // new ValidationException(Messages.Taxi_Common_InvalidReservation);
							}
						}
						else
						{
							result = false;
							var vtodException = VtodException.CreateFieldRequiredValidationException("GroundReservation.Service");
							logger.Warn(vtodException.ExceptionMessage.Message);
							throw vtodException; // new ValidationException(Messages.Taxi_Common_InvalidReservation);
						}
					}
					else
					{
						result = false;
						var vtodException = VtodException.CreateException(ExceptionType.Taxi, 2001);
						logger.Warn(vtodException.ExceptionMessage.Message);
						throw vtodException;// new ValidationException(Messages.Taxi_Common_UnableToFindFleetByAddressOrAirportInfo);
					}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateBookingRequest_ValidateDropOffAddress");
				swEachPart.Stop();
				logger.DebugFormat("Validate dropoff address Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Validate DropOffDateTime
				//if (result)
				//{
				//	DateTime dropOffDateTime;
				//	if (reservation.Service.Location.Dropoff != null)
				//	{
				//		if (DateTime.TryParse(reservation.Service.Location.Dropoff.DateTime, out dropOffDateTime))
				//		{
				//			tbp.DropOffDateTime = dropOffDateTime;
				//		}
				//		else
				//		{
				//			tbp.DropOffDateTime = null;
				//		}
				//	}
				//	else
				//	{
				//		tbp.DropOffDateTime = null;
				//	}
				//}
				//else
				//{
				//	logger.Info("No drop off datetime");
				//}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateBookingRequest_ValidateDropoffDateTime");
				swEachPart.Stop();
				logger.DebugFormat("Validate dropoff time Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Validate Available address type
				if (result)
				{
					//string pickupAddressType = tbp.PickupAddressType;
					//string dropoffAddressType = tbp.DropOffAddressType;

					//verify the address type of pickup address and drop off address
					UtilityDomain ud = new UtilityDomain();
					if (tbp.PickupAddressType == AddressType.Address)
					{
						if (tbp.PickupAddress.Geolocation == null)
						{
							result = false;
							var vtodException = VtodException.CreateFieldRequiredValidationException("PickupAddress.Geolocation");
							logger.Warn(vtodException.ExceptionMessage.Message);
							throw vtodException;
						}

						UtilityGetAirportNameRQ uganRQ = new UtilityGetAirportNameRQ();
						uganRQ.Address = new Address();
						uganRQ.Address.Latitude = tbp.PickupAddress.Geolocation.Latitude.ToString();
						uganRQ.Address.Longitude = tbp.PickupAddress.Geolocation.Longitude.ToString();

						UtilityGetAirportNameRS uganRS = ud.GetAirportName(tokenVTOD, uganRQ);
						if (uganRS.Success != null)
						{
							if (!string.IsNullOrWhiteSpace(uganRS.AirportName))
							{
								//pickupAddressType = AddressType.Airport;
								tbp.PickupAddressType = AddressType.Airport;
								tbp.PickupAirport = uganRS.AirportName;
								tbp.LongitudeForPickupAirport = uganRQ.Address.Longitude.ToDouble();
								tbp.LatitudeForPickupAirport = uganRQ.Address.Longitude.ToDouble();
							}
						}

					}

					if (tbp.DropOffAddressType == AddressType.Address)
					{
						if (tbp.DropOffAddress.Geolocation == null)
						{
							result = false;
							var vtodException = VtodException.CreateFieldRequiredValidationException("DropOffAddress.Geolocation");
							logger.Warn(vtodException.ExceptionMessage.Message);
							throw vtodException;
						}

						UtilityGetAirportNameRQ uganRQ = new UtilityGetAirportNameRQ();
						uganRQ.Address = new Address();
						uganRQ.Address.Latitude = tbp.DropOffAddress.Geolocation.Latitude.ToString();
						uganRQ.Address.Longitude = tbp.DropOffAddress.Geolocation.Longitude.ToString();

						UtilityGetAirportNameRS uganRS = ud.GetAirportName(tokenVTOD, uganRQ);
						if (uganRS.Success != null)
						{
							if (!string.IsNullOrWhiteSpace(uganRS.AirportName))
							{
								//dropoffAddressType = AddressType.Airport;
								tbp.DropOffAddressType = AddressType.Airport;
								tbp.DropoffAirport = uganRS.AirportName;
								tbp.LongitudeForDropoffAirport = uganRQ.Address.Longitude.ToDouble();
								tbp.LatitudeForDropoffAirport = uganRQ.Address.Longitude.ToDouble();
							}
						}
					}

					//check database setting
					//1. get taxi_fleet_availableaddresstype
					using (VTODEntities context = new VTODEntities())
					{
						long fleetId = tbp.Fleet.Id;
						if (context.taxi_fleet_availableaddresstype.Where(x => fleetId == x.FleetId).Any())
						{
							taxi_fleet_availableaddresstype tfa = context.taxi_fleet_availableaddresstype.Where(x => fleetId == x.FleetId).Select(x => x).FirstOrDefault();
							//2. check if the type is available or not
							//Taxi_Common_InvalidAddressTypeForFleet
							if (AddressType.Address.ToString() == tbp.PickupAddressType && AddressType.Address.ToString() == tbp.DropOffAddressType)
							{
								//address to address
								if (tfa.AddressToAddress)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.Taxi, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
							else if (AddressType.Address.ToString() == tbp.PickupAddressType && AddressType.Airport.ToString() == tbp.DropOffAddressType)
							{
								//address to airport
								if (tfa.AddressToAirport)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.Taxi, 2020); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
							else if (AddressType.Address.ToString() == tbp.PickupAddressType && null == tbp.DropOffAddressType)
							{
								//address to null
								if (tfa.AddressToNull)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.Taxi, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
							else if (AddressType.Airport.ToString() == tbp.PickupAddressType && AddressType.Address.ToString() == tbp.DropOffAddressType)
							{
								//airport to address
								if (tfa.AirportToAddress)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.Taxi, 2019); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
							else if (AddressType.Airport.ToString() == tbp.PickupAddressType && AddressType.Airport.ToString() == tbp.DropOffAddressType)
							{
								//airport to airport
								if (tfa.AirportToAirport)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.Taxi, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
							else if (AddressType.Airport.ToString() == tbp.PickupAddressType && null == tbp.DropOffAddressType)
							{
								//airport to null
								if (tfa.AirportToNull)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.Taxi, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
						}
						else
						{
							result = false;
							throw VtodException.CreateException(ExceptionType.Taxi, 1011);// new ValidationException(Messages.Taxi_Common_UnableToGetAvailableFleets);
						}
					}


				}
				else
				{
					result = false;
					var vtodException = VtodException.CreateFieldRequiredValidationException("Paymnet");//ExceptionType.Taxi, 2001);
					logger.Warn(vtodException.ExceptionMessage.Message);
					throw vtodException;// new ValidationException(Messages.Taxi_Common_UnableToFindFleetByAddressOrAirportInfo);
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateBookingRequest_ValidateAvailableAddressType");
				swEachPart.Stop();
				logger.DebugFormat("Validate available address type Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Validate PaymentInfo
				//As of this version, all requester should pass payment Info.
				//We need to modify this based on some others who do not pass payementInfo
				if (request.Payments == null || request.Payments.Payments == null || !request.Payments.Payments.Any())
				{
					result = false;
					var vtodException = Common.DTO.VtodException.CreateFieldRequiredValidationException("Payment");
					logger.Warn(vtodException.ExceptionMessage.Message);
					throw vtodException; // new ValidationException(Messages.Validation_NullPayment);
				}
				else
				{
					if (request.Payments.Payments.First().PaymentCard != null && request.Payments.Payments.First().Cash == null)
					{
						if (request.Payments.Payments.First().PaymentCard.CardNumber == null || string.IsNullOrWhiteSpace(request.Payments.Payments.First().PaymentCard.CardNumber.ID))
						{
							result = false;
							var vtodException = Common.DTO.VtodException.CreateFieldRequiredValidationException("CardNumber and CardNumberID");
							logger.Warn(vtodException.ExceptionMessage.Message);
							throw vtodException; // new ValidationException(Messages.Validation_NullPayment);
						}
						else
						{
							tbp.CreditCardID = request.Payments.Payments.First().PaymentCard.CardNumber.ID.ToInt32();
							tbp.PaymentType = Common.DTO.Enum.PaymentType.PaymentCard;


							#region Pre-Authorze creadit card
							//1. Get pre auth information
							//tbp.User.PreAuthCreditCardAmount
							//tbp.User.PreAuthCreditCardOption
							//tbp.MemberID



							//2. Do pre authorization
							if (tbp.User.PreAuthCreditCardOption)
							{
								PaymentDomain pd = new PaymentDomain();
								PreSaleVerificationResponse preAuthResult;
								try
								{
									preAuthResult = pd.PreSaleCardVerification(
										tokenVTOD,
										tbp.CreditCardID.Value,
										tbp.User.PreAuthCreditCardAmount,
										tbp.MemberID.Value,
										Convert.ToInt32(tbp.Fleet.SDS_FleetMerchantID.Value.ToString())
										);

								}
								catch (Exception ex)
								{
									logger.Error("Unable to pre auth credit card information");
									logger.ErrorFormat("ex={0}", ex.Message);
									throw;
								}
								if (preAuthResult != null)
								{
									if (preAuthResult.IsPrepaidCard == true)
									{
										throw VtodException.CreateException(ExceptionType.Taxi, 2021);
									}
									if (preAuthResult.TransactionApproved == false)
									{
										throw VtodException.CreateException(ExceptionType.Taxi, 2010);
									}

								}
								else
									throw VtodException.CreateException(ExceptionType.Taxi, 2010);
							}
							#endregion


						}
					}
					else if (request.Payments.Payments.First().Cash != null && request.Payments.Payments.First().PaymentCard == null && request.Payments.Payments.First().Cash.CashIndicator == true)
					{
						tbp.PaymentType = Common.DTO.Enum.PaymentType.Cash;
					}
					else if (request.Payments.Payments.First().MiscChargeOrder != null && !string.IsNullOrWhiteSpace(request.Payments.Payments.First().MiscChargeOrder.OriginalIssuePlace) && request.Payments.Payments.First().MiscChargeOrder.OriginalIssuePlace.ToUpper() == "SDS")
					{
						#region Authorize
						//var membershipController = new UDI.SDS.MembershipController(tokenVTOD, TrackTime);
						var membershipDomain = new UDI.VTOD.Domain.Membership.MembershipDomain();
						//if (membershipDomain.IsUserInPaymentRole(tokenVTOD.Username, Common.DTO.Enum.PaymentType.ChargedBySDS.ToString()))
						//{
						//	tbp.PaymentType = Common.DTO.Enum.PaymentType.ChargedBySDS;
						//}
						//else
						//{
						//	result = false;
						//	logger.Warn(Messages.Validation_InvalidPaymentType);
						//	throw VtodException.CreateValidationException(Messages.Validation_InvalidPaymentType);// new ValidationException(Messages.General_InvalidPaymentType);
						//}
						#endregion

					}
					else if (request.Payments.Payments.First().MiscChargeOrder != null && !string.IsNullOrWhiteSpace(request.Payments.Payments.First().MiscChargeOrder.OriginalIssuePlace) && request.Payments.Payments.First().MiscChargeOrder.OriginalIssuePlace.ToUpper() == "CONSUMER")
					{
						#region Authorize
						//var membershipController = new UDI.SDS.MembershipController(tokenVTOD, TrackTime);
						var membershipDomain = new UDI.VTOD.Domain.Membership.MembershipDomain();
						//if (membershipDomain.IsUserInPaymentRole(tokenVTOD.Username, Common.DTO.Enum.PaymentType.ChargedByConsumer.ToString()))
						//{
						//	tbp.PaymentType = Common.DTO.Enum.PaymentType.ChargedByConsumer;
						//}
						//else
						//{
						//	result = false;
						//	logger.Warn(Messages.Validation_InvalidPaymentType);
						//	throw VtodException.CreateValidationException(Messages.Validation_InvalidPaymentType);// ValidationException(Messages.General_InvalidPaymentType);
						//}
						#endregion

					}
					else
					{
						result = false;
						logger.Warn(Messages.Validation_CreditCard);
						throw VtodException.CreateValidationException(Messages.Validation_CreditCard); // new ValidationException(Messages.General_BadCreditCard);
					}
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateBookingRequest_ValidatePaymentInfo");
				swEachPart.Stop();
				logger.DebugFormat("Validate payment Id Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Get Gratuity
				try
				{
					var rateQuialifier = request.GroundReservations.First().RateQualifiers.First();
					if (rateQuialifier.SpecialInputs != null)
					{
						var gratuities = rateQuialifier.SpecialInputs.Where(s => s.Name.ToLower() == "gratuity").ToList();

						if (gratuities != null && gratuities.Any())
						{
							tbp.Gratuity = gratuities.First().Value;
						}
					}
				}
				catch (Exception ex)
				{
					logger.Error("Taxi:Gratuity", ex);
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateBookingRequest_ GetGratuity");
				swEachPart.Stop();
				logger.DebugFormat("Get Gratuity Duration:{0} ms.", swEachPart.ElapsedMilliseconds);


				swEachPart.Restart();
				#region Get PromisedETA
				try
				{
                    if (request.TPA_Extensions.PickMeUpNow.HasValue && request.TPA_Extensions.PickMeUpNow == true)
                    {
                        var rateQuialifier = request.GroundReservations.First().RateQualifiers.First();
                        if (rateQuialifier.SpecialInputs != null)
                        {
                            var promisedetas = rateQuialifier.SpecialInputs.Where(s => s.Name.ToLower() == "promisedeta").ToList();

                            if (promisedetas != null && promisedetas.Any())
                            {
                                tbp.PromisedETA = promisedetas.First().Value.ToInt32();
                            }
                        }
                    }
				}
				catch (Exception ex)
				{
					logger.Error("Taxi:PromisedETA", ex);
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateBookingRequest_ PromisedETA");
				swEachPart.Stop();
				logger.DebugFormat("Get PromisedETA Duration:{0} ms.", swEachPart.ElapsedMilliseconds);
				swEachPart.Restart();


				#region Create this trip into database
				if (result)
				{
					tbp.Trip = CreateNewTrip(tbp, tokenVTOD.Username);
					//tbp.Trip = trip;
					//tbp.Trip.taxi_trip = trip;
				}
				else
				{
					result = false;
					var vtodException = VtodException.CreateException(ExceptionType.Taxi, 2001);
					logger.Warn(vtodException.ExceptionMessage.Message);
					throw vtodException;// new ValidationException(Messages.Taxi_Common_UnableCreateTrip);
				}
				#endregion
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateBookingRequest_ GreateTripInDatabase");
				swEachPart.Stop();
				logger.DebugFormat("CreateNewTrip in db  Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

			}
			else
			{
				result = false;
				var vtodException = VtodException.CreateFieldRequiredValidationException("GroundReservation");
				logger.Warn(vtodException.ExceptionMessage.Message);
				throw vtodException; // new ValidationException(Messages.Taxi_Common_InvalidReservation);
			}
			#endregion

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);


			#region Create taxi log
			if (result)
			{
				if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
				{
					WriteTaxiLog(tbp.Trip.Id, tbp.serviceAPIID, TaxiServiceMethodType.Book, sw.ElapsedMilliseconds, string.Empty, string.Empty, null, null);
				}
			}
			else
			{
				result = false;
				var vtodException = Common.DTO.VtodException.CreateException(ExceptionType.Taxi, 2001);// ("CardNumber and CardNumberID");
				logger.Warn(vtodException.ExceptionMessage.Message);
				throw vtodException; // new ValidationException(Messages.Validation_NullPayment);
			}
			#endregion

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateBookingRequest");
			#endregion


			return result;
		}

		public bool ValidateMTDataBookingRequest(TokenRS tokenVTOD, OTA_GroundBookRQ request, out TaxiBookingParameter tbp, taxi_fleet fleet)
		{
			bool result = true;
			Stopwatch sw = new Stopwatch();
			sw.Start();
			tbp = new TaxiBookingParameter();
			tbp.Fleet = fleet;
			Stopwatch swEachPart = new Stopwatch();

			#region Track

			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");

			#endregion

			#region Validate Reservation

			if (request.GroundReservations.Any())
			{
				GroundReservation reservation = request.GroundReservations.FirstOrDefault();

				#region Set token, request

				tbp.tokenVTOD = tokenVTOD;
				tbp.request = request;

				#endregion

				#region Set Source and Device and ref

				if (request.TPA_Extensions != null && !string.IsNullOrWhiteSpace(request.TPA_Extensions.Source))
				{
					tbp.Source = request.TPA_Extensions.Source;
				}

				if (request.TPA_Extensions != null && !string.IsNullOrWhiteSpace(request.TPA_Extensions.Device))
				{
					tbp.Device = request.TPA_Extensions.Device;
				}

				if (!string.IsNullOrWhiteSpace(request.EchoToken))
				{
					tbp.Ref = request.EchoToken;
				}

				#endregion

				swEachPart.Restart();

				#region Validate Member Info

				//As of this version, all requester should pass memberID.
				//We need to modify this based on some others who do not pass memberID
				if (request.TPA_Extensions == null && string.IsNullOrWhiteSpace(request.TPA_Extensions.MemberID))
				{
					result = false;
					var vtodException = VtodException.CreateFieldRequiredValidationException("MemberID");
					//ExceptionType.Taxi, 2001);
					logger.Warn(vtodException.ExceptionMessage.Message);
					throw vtodException;
					// new ValidationException(Messages.Taxi_Common_UnableToFindFleetByAddressOrAirportInfo);
				}
				else
				{
					tbp.MemberID = request.TPA_Extensions.MemberID.ToInt32();
				}

                #endregion


                #region Validate BOBO
                if (request.TPA_Extensions != null && request.TPA_Extensions.IsBOBO.ToBool())
                {
                    tbp.IsBOBO = request.TPA_Extensions.IsBOBO;
                    tbp.BOBOMemberID = request.TPA_Extensions.BOBOMemberID.ToInt32();
                }

                #endregion

                if (TrackTime != null)
					TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility,"ValidateBookingRequest_ValidateMemberInfo");
				swEachPart.Stop();
				logger.DebugFormat("Validate member Info Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();

				#region Validate Customer

				if (reservation.Passenger != null)
				{
					//Name
					if (reservation.Passenger.Primary != null)
					{
						if (reservation.Passenger.Primary.PersonName != null)
						{
							if (string.IsNullOrWhiteSpace(reservation.Passenger.Primary.PersonName.GivenName) ||
								string.IsNullOrWhiteSpace(reservation.Passenger.Primary.PersonName.Surname))
							{
								result = false;
								var vtodException =
									VtodException.CreateFieldRequiredValidationException(
										"PassengerGivenName|PassengerSurname");
								logger.Warn(vtodException.ExceptionMessage.Message);
								throw vtodException; // new ValidationException(Messages.Taxi_Common_InvalidReservation);
							}
							else
							{
								tbp.FirstName = reservation.Passenger.Primary.PersonName.GivenName.Trim();
								tbp.LastName = reservation.Passenger.Primary.PersonName.Surname.Trim();
								logger.Info("First name and last name are valid.");
							}
						}
						else
						{
							result = false;
							var vtodException = VtodException.CreateFieldRequiredValidationException("PassengerName");
							logger.Warn(vtodException.ExceptionMessage.Message);
							throw vtodException; // new ValidationException(Messages.Taxi_Common_InvalidReservation);
						}

                        //Telephone
                        if (reservation.Passenger.Primary.Telephones != null && reservation.Passenger.Primary.Telephones.Any())
                        {
                            Telephone p = reservation.Passenger.Primary.Telephones.FirstOrDefault();
                            string areacode = ExtractDigits(p.AreaCityCode);
                            string phonenumber = ExtractDigits(p.PhoneNumber);
                            if (areacode.Length.Equals(3) && phonenumber.Length.Equals(7))
                            {
                                tbp.PhoneNumber = string.Format("{0}{1}", areacode, phonenumber);
                                logger.Info("Phone number(s) is/are valid.");
                            }
                            else
                            {
                                result = false;
                                var vtodException = VtodException.CreateFieldFormatValidationException("Telephone", "10 digits");
                                logger.Warn(vtodException.ExceptionMessage.Message);
                                throw vtodException; // new ValidationException(Messages.Taxi_Common_InvalidReservation);
                            }
                        }
                        else if (request.TPA_Extensions.IsBOBO.ToBool())
                        {
                            //Fake phone#                            
                            tbp.PhoneNumber = string.Empty;
                        }
                        else
                        {
                            result = false;
                            var vtodException = VtodException.CreateFieldRequiredValidationException("Telephones");
                            logger.Warn(vtodException.ExceptionMessage.Message);
                            throw vtodException; // new ValidationException(Messages.Taxi_Common_InvalidReservation);
                        }
						if (reservation.Passenger.Primary.Emails.Any())
						{
							Email e = reservation.Passenger.Primary.Emails.FirstOrDefault();


							string emailPattern = @"^(?("")("".+?(?<!\\)""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
												 @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]*\.)+[a-zA-Z0-9][\-a-zA-Z0-9]{0,22}[a-zA-Z0-9]))$";

							if (!string.IsNullOrWhiteSpace(e.Value) && Regex.IsMatch(e.Value, emailPattern, RegexOptions.IgnorePatternWhitespace))
							{
								tbp.EmailAddress = e.Value;
								logger.Info(Messages.ECar_Common_ValidReservationPassengerPrimaryTelephones);
							}
						}
						if (result)
						{
							////Create or GetCustomer
							//taxi_customer customer = GetTaxiCustomer(reservation);
							//tbp.Customer = customer;
							logger.InfoFormat("Customer firstname={0}, lastname={1}, phonenumber={2}", tbp.FirstName,
								tbp.LastName, tbp.PhoneNumber);
						}
						else
						{
							result = false;
							var vtodException = VtodException.CreateException(ExceptionType.Taxi, 2008);
							logger.Warn(vtodException.ExceptionMessage.Message);
							throw vtodException; // new ValidationException(Messages.Taxi_Common_InvalidReservation);
						}
					}
					else
					{
						result = false;
						var vtodException = VtodException.CreateFieldRequiredValidationException("PassengerPrimary");
						logger.Warn(vtodException.ExceptionMessage.Message);
						throw vtodException; // new ValidationException(Messages.Taxi_Common_InvalidReservation);
					}
				}
				else
				{
					result = false;
					var vtodException = VtodException.CreateFieldRequiredValidationException("Passenger");
					logger.Warn(vtodException.ExceptionMessage.Message);
					throw vtodException; // new ValidationException(Messages.Taxi_Common_InvalidReservation);
				}

				#endregion

				if (TrackTime != null)
					TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateBookingRequest_ValidateCustomer");
				swEachPart.Stop();
				logger.DebugFormat("Validate Customer Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();

				#region Validate Pickup Address

				if (reservation.Service != null)
				{
					if (reservation.Service.Location != null)
					{
						if (reservation.Service.Location.Pickup != null)
						{
							if (((reservation.Service.Location.Pickup.Address != null) &&
								 (reservation.Service.Location.Pickup.AirportInfo != null)) ||
								((reservation.Service.Location.Pickup.Address == null) &&
								 (reservation.Service.Location.Pickup.AirportInfo == null)))
							{
								result = false;
								logger.Warn(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
								throw new ValidationException(
									Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
							}
							else if ((reservation.Service.Location.Pickup.Address != null) &&
									 (reservation.Service.Location.Pickup.AirportInfo == null))
							{

								if (
									(string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude) ||
									 (string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude))) &&
									(string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.StreetNmbr) ||
									 string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.AddressLine) ||
									 string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CityName) ||
									 string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.PostalCode) ||
									 string.IsNullOrWhiteSpace(
										 reservation.Service.Location.Pickup.Address.StateProv.StateCode) ||
									 string.IsNullOrWhiteSpace(
										 reservation.Service.Location.Pickup.Address.CountryName.Code)))
								{
									result = false;
									logger.Warn(Messages.Validation_LocationDetail);
									throw new ValidationException(Messages.Validation_LocationDetail);
								}
								else if (
								(!string.IsNullOrWhiteSpace(
									reservation.Service.Location.Pickup.Address.Longitude) &&
								 (!string.IsNullOrWhiteSpace(
									 reservation.Service.Location.Pickup.Address.Longitude))) &&
								(string.IsNullOrWhiteSpace(
									reservation.Service.Location.Pickup.Address.StreetNmbr) ||
								 string.IsNullOrWhiteSpace(
									 reservation.Service.Location.Pickup.Address.AddressLine) ||
								 string.IsNullOrWhiteSpace(
									 reservation.Service.Location.Pickup.Address.CityName) ||
								 string.IsNullOrWhiteSpace(
									 reservation.Service.Location.Pickup.Address.PostalCode) ||
								 string.IsNullOrWhiteSpace(
									 reservation.Service.Location.Pickup.Address.StateProv.StateCode) ||
								 string.IsNullOrWhiteSpace(
									 reservation.Service.Location.Pickup.Address.CountryName.Code)))
								{
									tbp.PickupAddressOnlyContainsLatAndLong = true;
									tbp.PickupAddressType = AddressType.Address;
									tbp.PickupAddress = ConvertAddress(reservation.Service.Location.Pickup);
								}
								else if (
									!string.IsNullOrWhiteSpace(
										reservation.Service.Location.Pickup.Address.AddressLine) &&
									!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CityName) &&
									!string.IsNullOrWhiteSpace(
										reservation.Service.Location.Pickup.Address.PostalCode) &&
									!string.IsNullOrWhiteSpace(
										reservation.Service.Location.Pickup.Address.StateProv.StateCode) &&
									!string.IsNullOrWhiteSpace(
										reservation.Service.Location.Pickup.Address.CountryName.Code))
								{
									//Address
									Map.DTO.Address pickupAddress = null;
									//string pickupAddressStr = GetAddressString(reservation.Service.Location.Pickup.Address);
									logger.Info("Skip MAP API Validation.");
									pickupAddress = ConvertAddress(reservation.Service.Location.Pickup);
									tbp.PickupAddressType = AddressType.Address;
									tbp.PickupAddress = pickupAddress;
									logger.Info("Pickup AddressType: Address");
								}
								else
								{
									result = false;
									var vtodException = VtodException.CreateException(ExceptionType.Taxi, 1);
									logger.Warn(vtodException.ExceptionMessage.Message);
									// Messages.Taxi_Common_UnknownError);
									throw vtodException;
								}
							}
							else if ((reservation.Service.Location.Pickup.Address == null) &&
									 (reservation.Service.Location.Pickup.AirportInfo != null))
							{
								//airport
								if (((reservation.Service.Location.Pickup.AirportInfo.Arrival != null) &&
									 (reservation.Service.Location.Pickup.AirportInfo.Departure != null)) ||
									((reservation.Service.Location.Pickup.AirportInfo.Arrival == null) &&
									 (reservation.Service.Location.Pickup.AirportInfo.Departure == null)))
								{
									result = false;
									logger.Warn(Messages.Validation_ArrivalAndDepartureAirportInfo);
									throw VtodException.CreateValidationException(
										Messages.Validation_ArrivalAndDepartureAirportInfo);
								}
								else if (reservation.Service.Location.Pickup.AirportInfo.Arrival != null &&
										 reservation.Service.Location.Pickup.AirportInfo.Departure == null)
								{
									//Arrival
									tbp.PickupAddressType = AddressType.Airport;
									tbp.PickupAirport =
										reservation.Service.Location.Pickup.AirportInfo.Arrival.LocationCode;
									logger.Info("Pickup AddressType: AirportInfo.Arrival");
								}
								else if (reservation.Service.Location.Pickup.AirportInfo.Arrival == null &&
										 reservation.Service.Location.Pickup.AirportInfo.Departure != null)
								{
									//Depature

									tbp.PickupAddressType = AddressType.Airport;
									tbp.PickupAirport =
										reservation.Service.Location.Pickup.AirportInfo.Departure
											.LocationCode;
									logger.Info("Pickup AddressType: AirportInfo.Depature");
								}

								//longitude and latitude
								if (result)
								{
									double? longitude = null;
									double? latitude = null;
									if (GetLongitudeAndLatitudeForAirport(tbp.PickupAirport, out longitude,
										out latitude))
									{
										tbp.LongitudeForPickupAirport = longitude.Value;
										tbp.LatitudeForPickupAirport = latitude.Value;
									}
									else
									{
										throw VtodException.CreateException(ExceptionType.Taxi, 1010);
										// new ValidationException(Messages.Taxi_Common_UnableToFindAirportLongitudeAndLatitude);
									}
								}

								//throw new ValidationException(Messages.Taxi_Common_InvalidAirportPickup);
							}
						}
						else
						{
							result = false;
							var vtodException = VtodException.CreateFieldRequiredValidationException("Pickup");
							logger.Warn(vtodException.ExceptionMessage.Message);
							throw vtodException; // new ValidationException(Messages.Taxi_Common_InvalidReservation);
						}
					}
					else
					{
						result = false;
						var vtodException =
							VtodException.CreateFieldRequiredValidationException("Reservation.Service.Location");
						logger.Warn(vtodException.ExceptionMessage.Message);
						throw vtodException; // new ValidationException(Messages.Taxi_Common_InvalidReservation);
					}
				}
				else
				{
					result = false;
					var vtodException = VtodException.CreateFieldRequiredValidationException("GroundReservation.Service");
					logger.Warn(vtodException.ExceptionMessage.Message);
					throw vtodException; // new ValidationException(Messages.Taxi_Common_InvalidReservation);
				}

				#endregion

				if (TrackTime != null)
					TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility,
						"ValidateBookingRequest_ValidatePickupAddress");
				swEachPart.Stop();
				logger.DebugFormat("Validate Pickup address Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();

				#region Validate Fleet

				if (result)
				{
					//taxi_fleet fleet = null;
					if (tbp.Fleet != null)//GetFleet(tbp.PickupAddressType, tbp.PickupAddress, tbp.PickupAirport, out fleet))
					{
						//tbp.Fleet = fleet;
						logger.InfoFormat("Fleet found. ID={0}", tbp.Fleet.Id);

						if (tbp.Fleet != null)
						{
							tbp.serviceAPIID =
						GetTaxiFleetServiceAPIPreference(tbp.Fleet.Id, TaxiServiceAPIPreferenceConst.Book).ServiceAPIId;
                            #region Fetching Endpoint Names
                            List<string> endpointNames= MtdataEndpointConfiguration(tbp.serviceAPIID, tbp.Fleet.Id);
                            if (endpointNames != null && endpointNames.Any())
                            {
                                tbp.AuthenticationServiceEndpointName = endpointNames[0];
                                tbp.BookingWebServiceEndpointName = endpointNames[1];
                                tbp.OsiWebServiceEndpointName = endpointNames[2];
                                tbp.AddressWebServiceEndpointName = endpointNames[3];
                            }
                            #endregion
                        }
					}
					else
					{
						result = false;
						var vtodException = VtodException.CreateException(ExceptionType.Taxi, 1011);
						logger.Warn(vtodException.ExceptionMessage.Message);
						throw vtodException;
						// new ValidationException(Messages.Taxi_Common_UnableToFindFleetByAddressOrAirportInfo);
					}
				}
				else
				{
					result = false;
					var vtodException = VtodException.CreateException(ExceptionType.Taxi, 2001);
					logger.Warn(vtodException.ExceptionMessage.Message);
					throw vtodException;
					// new ValidationException(Messages.Taxi_Common_UnableToFindFleetByAddressOrAirportInfo);
				}

				#endregion

				if (TrackTime != null)
					TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateBookingRequest_ValidateFleet");
				swEachPart.Stop();
				logger.DebugFormat("Validate fleet Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();

				#region Validate User

				if (result)
				{
					taxi_fleet_user user = null;
					if (GetTaxiFleetUser(tbp.Fleet.Id, tokenVTOD.Username, out user))
					{
						tbp.User = user;
						logger.Info("This user has sufficient privilege.");
					}
					else
					{
						result = false;
						var vtodException = VtodException.CreateException(ExceptionType.Taxi, 102);
						logger.Warn(vtodException.ExceptionMessage.Message);
						throw vtodException;
						// new ValidationException(Messages.Taxi_Common_InsufficientPrivilege(tokenVTOD.Username, tbp.Fleet.Id));
					}
				}

				#endregion

				if (TrackTime != null)
					TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateBookingRequest_ValidateUser");
				swEachPart.Stop();
				logger.DebugFormat("Validate user Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();

				#region Validate PickupDateTime

				if (result)
				{
					//pickiup now

					#region determine Pick me up now

					if (request.TPA_Extensions != null && request.TPA_Extensions.PickMeUpNow.HasValue)
					{
						tbp.PickupNow = request.TPA_Extensions.PickMeUpNow.Value;
					}
					else
					{
						try
						{
							tbp.PickupNow =
								reservation.Service.Location.Pickup.DateTime.ToDateTime()
									.Value.ToUtc(tbp.Fleet.ServerUTCOffset) <= DateTime.UtcNow.AddMinutes(5);
						}
						catch
						{
						}
					}

					#endregion

					#region Validate Pick me up now against DB fleet

					bool pickMeUpNow = true;
					bool pickMeUpLater = true;
					GetPickMeupOption(tbp.Fleet, out pickMeUpNow, out pickMeUpLater);
                    #region Check BlackOut
                    CheckBlackOut(tbp.Fleet, request);

                    
                    #endregion

                    #endregion


                    #region determine pick me up now Time

                    DateTime pickupDateTime;
					//if (DateTime.TryParseExact(reservation.Service.Location.Pickup.DateTime, ConfigurationManager.AppSettings["DateTimeFormatForPickupAndDropoff"], null, DateTimeStyles.None, out pickupDateTime))
					if (DateTime.TryParse(reservation.Service.Location.Pickup.DateTime, out pickupDateTime))
					{
						tbp.PickupDateTime = pickupDateTime;
						logger.Info("PickupDateTime is correct");
					}
					else if (tbp.PickupNow &&
							 !DateTime.TryParse(reservation.Service.Location.Pickup.DateTime, out pickupDateTime))
					{
						tbp.PickupDateTime = DateTime.UtcNow.FromUtc(tbp.Fleet.ServerUTCOffset);
						logger.InfoFormat(
							"There is no Pickup datetime for this request. But it has pickup now value. So the pickup time will set as {0:yyyy/MM/dd HH:mm:ss}.",
							tbp.PickupDateTime);
					}
					else
					{
						result = false;
						var vtodException = VtodException.CreateFieldFormatValidationException("PickupDateTime",
							"DateTime");
						logger.Warn(vtodException.ExceptionMessage.Message);
						throw vtodException;
						// new ValidationException(Messages.Taxi_Common_InvalidReservationServiceLocationPickupDateTime(reservation.Service.Location.Pickup.DateTime));
					}

					#endregion
				}
				else
				{
					result = false;
					var vtodException = VtodException.CreateException(ExceptionType.Taxi, 2001);
					logger.Warn(vtodException.ExceptionMessage.Message);
					throw vtodException;
					// new ValidationException(Messages.Taxi_Common_UnableToFindFleetByAddressOrAirportInfo);
				}

				#endregion

				if (TrackTime != null)
					TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility,
						"ValidateBookingRequest_ValidatePickupDateTime");
				swEachPart.Stop();
				logger.DebugFormat("Validate PickupTime Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();

				#region Validate duplicated booking trip

				if (result)
				{
                    //By pass if BOBO
                    if (!request.TPA_Extensions.IsBOBO.ToBool())
                    {
                        if (IsAbleToBookTrip(tbp))
                        {
                            logger.Info("This trip can be booked.");
                        }
                        else
                        {
                            result = false;
                            var vtodException = VtodException.CreateException(ExceptionType.Taxi, 2006);
                            logger.Warn(vtodException.ExceptionMessage.Message);
                            throw vtodException; 
                        }
                    }
					
				}
				else
				{
					result = false;
					var vtodException = VtodException.CreateException(ExceptionType.Taxi, 2001);
					logger.Warn(vtodException.ExceptionMessage.Message);
					throw vtodException;
				}

				#endregion

				if (TrackTime != null)
					TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility,
						"ValidateBookingRequest_ValidateDuplicatedBookingTrip");
				swEachPart.Stop();
				logger.DebugFormat("Validate duplicated trip Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();

				#region Validate Dropoff Address

				//DropOff is optional: By_Pouyan
				if (reservation.Service.Location != null && reservation.Service.Location.Dropoff != null)
					if (result)
					{
						if (reservation.Service != null)
						{
							if (reservation.Service.Location != null)
							{
								if (reservation.Service.Location.Dropoff != null)
								{
									using (VTODEntities context = new VTODEntities())
									{
										if (((reservation.Service.Location.Dropoff.Address != null) &&
											 (reservation.Service.Location.Dropoff.AirportInfo != null)) ||
											((reservation.Service.Location.Dropoff.Address == null) &&
											 (reservation.Service.Location.Dropoff.AirportInfo == null)))
										{
											result = false;
											logger.Warn(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
											throw VtodException.CreateValidationException(
												Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
										}
										else if ((reservation.Service.Location.Dropoff.Address != null) &&
												 (reservation.Service.Location.Dropoff.AirportInfo == null))
										{
											//Address
											if (
												(string.IsNullOrWhiteSpace(
													reservation.Service.Location.Dropoff.Address.Longitude) ||
												 (string.IsNullOrWhiteSpace(
													 reservation.Service.Location.Dropoff.Address.Longitude))) &&
												(string.IsNullOrWhiteSpace(
													reservation.Service.Location.Dropoff.Address.StreetNmbr) ||
												 string.IsNullOrWhiteSpace(
													 reservation.Service.Location.Dropoff.Address.AddressLine) ||
												 string.IsNullOrWhiteSpace(
													 reservation.Service.Location.Dropoff.Address.CityName) ||
												 string.IsNullOrWhiteSpace(
													 reservation.Service.Location.Dropoff.Address.PostalCode) ||
												 string.IsNullOrWhiteSpace(
													 reservation.Service.Location.Dropoff.Address.StateProv.StateCode) ||
												 string.IsNullOrWhiteSpace(
													 reservation.Service.Location.Dropoff.Address.CountryName.Code)))
											//if (string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CountryName.Code))
											{
												result = false;
												logger.Warn(Messages.Validation_LocationDetail);
												throw VtodException.CreateValidationException(
													Messages.Validation_LocationDetail);
											}
											else if (
												(!string.IsNullOrWhiteSpace(
													reservation.Service.Location.Dropoff.Address.Longitude) &&
												 (!string.IsNullOrWhiteSpace(
													 reservation.Service.Location.Dropoff.Address.Longitude))) &&
												(string.IsNullOrWhiteSpace(
													reservation.Service.Location.Dropoff.Address.StreetNmbr) ||
												 string.IsNullOrWhiteSpace(
													 reservation.Service.Location.Dropoff.Address.AddressLine) ||
												 string.IsNullOrWhiteSpace(
													 reservation.Service.Location.Dropoff.Address.CityName) ||
												 string.IsNullOrWhiteSpace(
													 reservation.Service.Location.Dropoff.Address.PostalCode) ||
												 string.IsNullOrWhiteSpace(
													 reservation.Service.Location.Dropoff.Address.StateProv
														 .StateCode) ||
												 string.IsNullOrWhiteSpace(
													 reservation.Service.Location.Dropoff.Address.CountryName
														 .Code)))
											{
												tbp.DropOffAddressOnlyContainsLatAndLong = true;
												tbp.DropOffAddressType = AddressType.Address;
												tbp.DropOffAddress =
													ConvertAddress(reservation.Service.Location.Dropoff);
											}
											else if (
												!string.IsNullOrWhiteSpace(
													reservation.Service.Location.Dropoff.Address.AddressLine) &&
												!string.IsNullOrWhiteSpace(
													reservation.Service.Location.Dropoff.Address.CityName) &&
												!string.IsNullOrWhiteSpace(
													reservation.Service.Location.Dropoff.Address.PostalCode) &&
												!string.IsNullOrWhiteSpace(
													reservation.Service.Location.Dropoff.Address.StateProv
														.StateCode) &&
												!string.IsNullOrWhiteSpace(
													reservation.Service.Location.Dropoff.Address.CountryName.Code))
											{
												Map.DTO.Address dropoffAddress = null;
												string dropoffAddressStr =
													GetAddressString(reservation.Service.Location.Dropoff.Address);
												logger.Info("Skip MAP API Validation.");
												dropoffAddress =
													ConvertAddress(reservation.Service.Location.Dropoff);
												tbp.DropOffAddress = dropoffAddress;
												tbp.DropOffAddressType =
													UDI.VTOD.Domain.Taxi.Const.AddressType.Address;
												logger.Info("Dropoff AddressType=Address");
											}
											else
											{
												result = false;
												var vtodException =
													VtodException.CreateException(ExceptionType.Taxi, 1);
												logger.Warn(vtodException.ExceptionMessage.Message);
												throw vtodException;
											}
										}
										else if ((reservation.Service.Location.Dropoff.Address == null) &&
												 (reservation.Service.Location.Dropoff.AirportInfo != null))
										{

											//airport
											if (((reservation.Service.Location.Dropoff.AirportInfo.Arrival != null) &&
												 (reservation.Service.Location.Dropoff.AirportInfo.Departure !=
												  null)) ||
												((reservation.Service.Location.Dropoff.AirportInfo.Arrival == null) &&
												 (reservation.Service.Location.Dropoff.AirportInfo.Departure ==
												  null)))
											{
												result = false;
												logger.Warn(Messages.Validation_ArrivalAndDepartureAirportInfo);
												throw VtodException.CreateValidationException(
													Messages.Validation_ArrivalAndDepartureAirportInfo);
											}
											else if (reservation.Service.Location.Dropoff.AirportInfo.Arrival !=
													 null &&
													 reservation.Service.Location.Dropoff.AirportInfo.Departure ==
													 null)
											{
												//Arrival
												tbp.DropOffAddressType = AddressType.Airport;
												tbp.DropoffAirport =
													reservation.Service.Location.Dropoff.AirportInfo.Arrival
														.LocationCode;
												logger.Info("Dropoff AddressType: AirportInfo.Arrival");
											}
											else if (reservation.Service.Location.Dropoff.AirportInfo.Arrival ==
													 null &&
													 reservation.Service.Location.Dropoff.AirportInfo.Departure !=
													 null)
											{
												//Depature
												tbp.DropOffAddressType = AddressType.Airport;
												tbp.DropoffAirport =
													reservation.Service.Location.Dropoff.AirportInfo
														.Departure.LocationCode;
												logger.Info("Dropoff AddressType: AirportInfo.Depature");
											}

											//longitude and latitude
											if (result)
											{
												double? longitude = null;
												double? latitude = null;
												if (GetLongitudeAndLatitudeForAirport(tbp.DropoffAirport,
													out longitude, out latitude))
												{
													tbp.LongitudeForDropoffAirport = longitude.Value;
													tbp.LatitudeForDropoffAirport = latitude.Value;
												}
												else
												{
													throw VtodException.CreateException(ExceptionType.Taxi, 1010);
													// new ValidationException(Messages.Taxi_Common_UnableToFindAirportLongitudeAndLatitude);
												}
											}
										}
									}
								}
							}
							else
							{
								result = false;
								var vtodException =
									VtodException.CreateFieldRequiredValidationException("Reservation.Service.Location");
								logger.Warn(vtodException.ExceptionMessage.Message);
								throw vtodException; // new ValidationException(Messages.Taxi_Common_InvalidReservation);
							}
						}
						else
						{
							result = false;
							var vtodException =
								VtodException.CreateFieldRequiredValidationException("GroundReservation.Service");
							logger.Warn(vtodException.ExceptionMessage.Message);
							throw vtodException; // new ValidationException(Messages.Taxi_Common_InvalidReservation);
						}
					}
					else
					{
						result = false;
						var vtodException = VtodException.CreateException(ExceptionType.Taxi, 2001);
						logger.Warn(vtodException.ExceptionMessage.Message);
						throw vtodException;
						// new ValidationException(Messages.Taxi_Common_UnableToFindFleetByAddressOrAirportInfo);
					}

				#endregion

				if (TrackTime != null)
					TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility,
						"ValidateBookingRequest_ValidateDropOffAddress");
				swEachPart.Stop();
				logger.DebugFormat("Validate dropoff address Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				if (TrackTime != null)
					TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility,
						"ValidateBookingRequest_ValidateDropoffDateTime");
				swEachPart.Stop();
				logger.DebugFormat("Validate dropoff time Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();

				#region Validate Available address type

				if (result)
				{
					//string pickupAddressType = tbp.PickupAddressType;
					//string dropoffAddressType = tbp.DropOffAddressType;

					//verify the address type of pickup address and drop off address
					UtilityDomain ud = new UtilityDomain();
					if (tbp.PickupAddressType == AddressType.Address)
					{
						if (tbp.PickupAddress.Geolocation == null)
						{
							result = false;
							var vtodException =
								VtodException.CreateFieldRequiredValidationException("PickupAddress.Geolocation");
							logger.Warn(vtodException.ExceptionMessage.Message);
							throw vtodException;
						}

						UtilityGetAirportNameRQ uganRQ = new UtilityGetAirportNameRQ();
						uganRQ.Address = new Address();
						uganRQ.Address.Latitude = tbp.PickupAddress.Geolocation.Latitude.ToString();
						uganRQ.Address.Longitude = tbp.PickupAddress.Geolocation.Longitude.ToString();

						UtilityGetAirportNameRS uganRS = ud.GetAirportName(tokenVTOD, uganRQ);
						if (uganRS.Success != null)
						{
							if (!string.IsNullOrWhiteSpace(uganRS.AirportName))
							{
								//pickupAddressType = AddressType.Airport;
								tbp.PickupAddressType = AddressType.Airport;
								tbp.PickupAirport = uganRS.AirportName;
								tbp.LongitudeForPickupAirport = uganRQ.Address.Longitude.ToDouble();
								tbp.LatitudeForPickupAirport = uganRQ.Address.Longitude.ToDouble();
							}
						}

					}

					if (tbp.DropOffAddressType == AddressType.Address)
					{
						if (tbp.DropOffAddress.Geolocation == null)
						{
							result = false;
							var vtodException =
								VtodException.CreateFieldRequiredValidationException("DropOffAddress.Geolocation");
							logger.Warn(vtodException.ExceptionMessage.Message);
							throw vtodException;
						}

						UtilityGetAirportNameRQ uganRQ = new UtilityGetAirportNameRQ();
						uganRQ.Address = new Address();
						uganRQ.Address.Latitude = tbp.DropOffAddress.Geolocation.Latitude.ToString();
						uganRQ.Address.Longitude = tbp.DropOffAddress.Geolocation.Longitude.ToString();

						UtilityGetAirportNameRS uganRS = ud.GetAirportName(tokenVTOD, uganRQ);
						if (uganRS.Success != null)
						{
							if (!string.IsNullOrWhiteSpace(uganRS.AirportName))
							{
								//dropoffAddressType = AddressType.Airport;
								tbp.DropOffAddressType = AddressType.Airport;
								tbp.DropoffAirport = uganRS.AirportName;
								tbp.LongitudeForDropoffAirport = uganRQ.Address.Longitude.ToDouble();
								tbp.LatitudeForDropoffAirport = uganRQ.Address.Longitude.ToDouble();
							}
						}
					}

					//check database setting
					//1. get taxi_fleet_availableaddresstype
					using (VTODEntities context = new VTODEntities())
					{
						long fleetId = tbp.Fleet.Id;
						if (context.taxi_fleet_availableaddresstype.Where(x => fleetId == x.FleetId).Any())
						{
							taxi_fleet_availableaddresstype tfa =
								context.taxi_fleet_availableaddresstype.Where(x => fleetId == x.FleetId)
									.Select(x => x)
									.FirstOrDefault();
							//2. check if the type is available or not
							//Taxi_Common_InvalidAddressTypeForFleet
							if (AddressType.Address.ToString() == tbp.PickupAddressType &&
								AddressType.Address.ToString() == tbp.DropOffAddressType)
							{
								//address to address
								if (tfa.AddressToAddress)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.Taxi, 1013);
									//VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
							else if (AddressType.Address.ToString() == tbp.PickupAddressType &&
									 AddressType.Airport.ToString() == tbp.DropOffAddressType)
							{
								//address to airport
								if (tfa.AddressToAirport)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.Taxi, 2020);
									//VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
							else if (AddressType.Address.ToString() == tbp.PickupAddressType &&
									 null == tbp.DropOffAddressType)
							{
								//address to null
								if (tfa.AddressToNull)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.Taxi, 1013);
									//VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
							else if (AddressType.Airport.ToString() == tbp.PickupAddressType &&
									 AddressType.Address.ToString() == tbp.DropOffAddressType)
							{
								//airport to address
								if (tfa.AirportToAddress)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.Taxi, 2019);
									//VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
							else if (AddressType.Airport.ToString() == tbp.PickupAddressType &&
									 AddressType.Airport.ToString() == tbp.DropOffAddressType)
							{
								//airport to airport
								if (tfa.AirportToAirport)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.Taxi, 1013);
									//VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
							else if (AddressType.Airport.ToString() == tbp.PickupAddressType &&
									 null == tbp.DropOffAddressType)
							{
								//airport to null
								if (tfa.AirportToNull)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.Taxi, 1013);
									//VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
						}
						else
						{
							result = false;
							throw VtodException.CreateException(ExceptionType.Taxi, 1011);
							// new ValidationException(Messages.Taxi_Common_UnableToGetAvailableFleets);
						}
					}


				}
				else
				{
					result = false;
					var vtodException = VtodException.CreateFieldRequiredValidationException("Paymnet");
					//ExceptionType.Taxi, 2001);
					logger.Warn(vtodException.ExceptionMessage.Message);
					throw vtodException;
					// new ValidationException(Messages.Taxi_Common_UnableToFindFleetByAddressOrAirportInfo);
				}

				#endregion

				if (TrackTime != null)
					TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility,
						"ValidateBookingRequest_ValidateAvailableAddressType");
				swEachPart.Stop();
				logger.DebugFormat("Validate available address type Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();

				#region Validate PaymentInfo

				//As of this version, all requester should pass payment Info.
				//We need to modify this based on some others who do not pass payementInfo
				if (request.Payments == null || request.Payments.Payments == null || !request.Payments.Payments.Any())
				{
					result = false;
					var vtodException = Common.DTO.VtodException.CreateFieldRequiredValidationException("Payment");
					logger.Warn(vtodException.ExceptionMessage.Message);
					throw vtodException; // new ValidationException(Messages.Validation_NullPayment);
				}
				else
				{
					if (request.Payments.Payments.First().PaymentCard != null &&
						request.Payments.Payments.First().Cash == null)
					{
						if (request.Payments.Payments.First().PaymentCard.CardNumber == null ||
							string.IsNullOrWhiteSpace(request.Payments.Payments.First().PaymentCard.CardNumber.ID))
						{
							result = false;
							var vtodException =
								Common.DTO.VtodException.CreateFieldRequiredValidationException(
									"CardNumber and CardNumberID");
							logger.Warn(vtodException.ExceptionMessage.Message);
							throw vtodException; // new ValidationException(Messages.Validation_NullPayment);
						}
						else
						{
							tbp.CreditCardID = request.Payments.Payments.First().PaymentCard.CardNumber.ID.ToInt32();
							tbp.PaymentType = Common.DTO.Enum.PaymentType.PaymentCard;


							#region Pre-Authorze creadit card

							//1. Get pre auth information
							//tbp.User.PreAuthCreditCardAmount
							//tbp.User.PreAuthCreditCardOption
							//tbp.MemberID



							//2. Do pre authorization
							if (tbp.User.PreAuthCreditCardOption)
							{
								PaymentDomain pd = new PaymentDomain();
								PreSaleVerificationResponse preAuthResult;
								try
								{
									preAuthResult = pd.PreSaleCardVerification(
										tokenVTOD,
										tbp.CreditCardID.Value,
										tbp.User.PreAuthCreditCardAmount,
										tbp.MemberID.Value,
										Convert.ToInt32(tbp.Fleet.SDS_FleetMerchantID.Value.ToString())
										);

								}
								catch (Exception ex)
								{
									logger.Error("Unable to pre auth credit card information");
									logger.ErrorFormat("ex={0}", ex.Message);
									throw;
								}
                                if (preAuthResult != null)
                                {
                                    if (preAuthResult.IsPrepaidCard == true)
                                    {
                                        throw VtodException.CreateException(ExceptionType.Taxi, 2021);
                                    }
                                    if (preAuthResult.TransactionApproved == false)
                                    {
                                        throw VtodException.CreateException(ExceptionType.Taxi, 2010);
                                    }

                                }
                                else
                                    throw VtodException.CreateException(ExceptionType.Taxi, 2010);
                            }

							#endregion


						}
					}
					else if (request.Payments.Payments.First().Cash != null &&
							 request.Payments.Payments.First().PaymentCard == null &&
							 request.Payments.Payments.First().Cash.CashIndicator == true)
					{
						tbp.PaymentType = Common.DTO.Enum.PaymentType.Cash;
					}
					else if (request.Payments.Payments.First().MiscChargeOrder != null &&
							 !string.IsNullOrWhiteSpace(
								 request.Payments.Payments.First().MiscChargeOrder.OriginalIssuePlace) &&
							 request.Payments.Payments.First().MiscChargeOrder.OriginalIssuePlace.ToUpper() == "SDS")
					{
						#region Authorize

						//var membershipController = new UDI.SDS.MembershipController(tokenVTOD, TrackTime);
						var membershipDomain = new UDI.VTOD.Domain.Membership.MembershipDomain();
						//if (membershipDomain.IsUserInPaymentRole(tokenVTOD.Username, Common.DTO.Enum.PaymentType.ChargedBySDS.ToString()))
						//{
						//	tbp.PaymentType = Common.DTO.Enum.PaymentType.ChargedBySDS;
						//}
						//else
						//{
						//	result = false;
						//	logger.Warn(Messages.Validation_InvalidPaymentType);
						//	throw VtodException.CreateValidationException(Messages.Validation_InvalidPaymentType);// new ValidationException(Messages.General_InvalidPaymentType);
						//}

						#endregion

					}
					else if (request.Payments.Payments.First().MiscChargeOrder != null &&
							 !string.IsNullOrWhiteSpace(
								 request.Payments.Payments.First().MiscChargeOrder.OriginalIssuePlace) &&
							 request.Payments.Payments.First().MiscChargeOrder.OriginalIssuePlace.ToUpper() ==
							 "CONSUMER")
					{
						#region Authorize

						//var membershipController = new UDI.SDS.MembershipController(tokenVTOD, TrackTime);
						var membershipDomain = new UDI.VTOD.Domain.Membership.MembershipDomain();
						//if (membershipDomain.IsUserInPaymentRole(tokenVTOD.Username, Common.DTO.Enum.PaymentType.ChargedByConsumer.ToString()))
						//{
						//	tbp.PaymentType = Common.DTO.Enum.PaymentType.ChargedByConsumer;
						//}
						//else
						//{
						//	result = false;
						//	logger.Warn(Messages.Validation_InvalidPaymentType);
						//	throw VtodException.CreateValidationException(Messages.Validation_InvalidPaymentType);// ValidationException(Messages.General_InvalidPaymentType);
						//}

						#endregion

					}
					else
					{
						result = false;
						logger.Warn(Messages.Validation_CreditCard);
						throw VtodException.CreateValidationException(Messages.Validation_CreditCard);
						// new ValidationException(Messages.General_BadCreditCard);
					}
				}

				#endregion

				if (TrackTime != null)
					TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility,
						"ValidateBookingRequest_ValidatePaymentInfo");
				swEachPart.Stop();
				logger.DebugFormat("Validate payment Id Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();

				#region Get Gratuity

				try
				{
					var rateQuialifier = request.GroundReservations.First().RateQualifiers.First();
					if (rateQuialifier.SpecialInputs != null)
					{
						var gratuities = rateQuialifier.SpecialInputs.Where(s => s.Name.ToLower() == "gratuity").ToList();

						if (gratuities != null && gratuities.Any())
						{
							tbp.Gratuity = gratuities.First().Value;
						}
					}
				}
				catch (Exception ex)
				{
					logger.Error("Taxi:Gratuity", ex);
				}

				#endregion

				if (TrackTime != null)
					TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateBookingRequest_ GetGratuity");
				swEachPart.Stop();
				logger.DebugFormat("Get Gratuity Duration:{0} ms.", swEachPart.ElapsedMilliseconds);


				swEachPart.Restart();

				#region Get PromisedETA

				try
				{
					var rateQuialifier = request.GroundReservations.First().RateQualifiers.First();
                    if (request.TPA_Extensions.PickMeUpNow.HasValue && request.TPA_Extensions.PickMeUpNow == true)
                    {
                        if (rateQuialifier.SpecialInputs != null)
                        {
                            var promisedetas =
                                rateQuialifier.SpecialInputs.Where(s => s.Name.ToLower() == "promisedeta").ToList();

                            if (promisedetas != null && promisedetas.Any())
                            {
                                tbp.PromisedETA = promisedetas.First().Value.ToInt32();
                            }
                        }
                    }
                }
				catch (Exception ex)
				{
					logger.Error("Taxi:PromisedETA", ex);
				}

				#endregion

				if (TrackTime != null)
					TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateBookingRequest_ PromisedETA");
				swEachPart.Stop();
				logger.DebugFormat("Get PromisedETA Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                swEachPart.Restart();
                #region Adding Tip to MTData
                //***********************************************************************************************************
                //**************** we are adding tip just for MTData in its domain calss: MTDataTaxiService *****************
                //**************** so if you add Tip to all Taxi in TaxiDomain calss please comment this block **************
                //***********************************************************************************************************

                var specialInputs = request.GroundReservations.First().RateQualifiers.First().SpecialInputs;
                if (specialInputs != null && specialInputs.Any())
                {
                    var gratuity = specialInputs.Where(s => s.Name.ToLower().Trim() == "gratuity").FirstOrDefault();
                    if (gratuity != null)
                    {
                        var addingText = string.Format(" Tip is {0}", gratuity.Value);
                        request.GroundReservations.First().Service.Location.Pickup.Remark += addingText;
                    }
                }
                #endregion
                if (TrackTime != null)
                    TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility,
                        "Adding Tip To Remarks");
                swEachPart.Stop();
                logger.DebugFormat("Adding Tip To Remarks:{0} ms.", swEachPart.ElapsedMilliseconds);

                swEachPart.Restart();

				#region Check if it's fixed price zone

				if (result && tbp.Fleet.OverWriteFixedPrice)
				{
					var flatRates = new List<TaxiFlatRateData>(); ;
					taxi_fleet_zone pickupZone = null;
					taxi_fleet_zone dropoffZone = null;

					#region Get FlatRate
					using (VTODEntities context = new VTODEntities())
					{
						long pickupfleetID = tbp.Fleet.Id;
						var tfzfList = context.taxi_fleet_zone_flatrate.Where(x => x.PickupZone.FleetId == pickupfleetID).Select(x => x).ToList();
						if (tfzfList.Any())
						{
							foreach (var x in tfzfList)
							{
								var tfrd = new TaxiFlatRateData { Amount = x.Amount, AmountUnit = x.AmountUnit, AppendTime = x.AppendTime, DropoffZoneId = x.DropoffZoneId, Id = x.Id, PickupZoneId = x.PickupZoneId };
								tfrd.PickupZoneName = context.taxi_fleet_zone.Where(xx => xx.Id == tfrd.PickupZoneId).Select(xxx => xxx.Name).FirstOrDefault();
								tfrd.DropOffZoneName = context.taxi_fleet_zone.Where(xx => xx.Id == tfrd.DropoffZoneId).Select(xxx => xxx.Name).FirstOrDefault();
								flatRates.Add(tfrd);
							}
						}
					}
					#endregion

					#region Pickup zone and dropoff zone
					if (result)
					{
						//pickup zone
						if ((tbp.PickupAddressType == AddressType.Address && tbp.PickupAddress.Geolocation != null && tbp.PickupAddress.Geolocation.Latitude != 0 && tbp.PickupAddress.Geolocation.Longitude != 0)
							||
							(tbp.PickupAddressType == AddressType.Airport))
						{
							taxi_fleet_zone zone = null;

							if (tbp.PickupAddressType == AddressType.Address)
							{
								zone = GetFleetZone(tbp.Fleet.Id, tbp.PickupAddress);
							}
							else if (tbp.PickupAddressType == AddressType.Airport)
							{
								zone = GetFleetZoneByAirport(tbp.Fleet.Id, tbp.PickupAirport);
							}

							if (zone != null)
							{
								pickupZone = zone;
							}
						}
					}
					else
					{
						result = false;
						logger.Warn("Cannot process pickup zone by previous invalid request");
					}

					if (result)
					{
						if (reservation.Service.Location.Dropoff != null)
						{
							//drop off zone
							if ((tbp.DropOffAddressType == AddressType.Address && tbp.DropOffAddress.Geolocation != null && tbp.DropOffAddress.Geolocation.Latitude != 0 && tbp.DropOffAddress.Geolocation.Longitude != 0)
								||
								(tbp.DropOffAddressType == AddressType.Airport))
							{
								taxi_fleet_zone zone = null;

								if (tbp.DropOffAddressType == AddressType.Address)
								{
									zone = GetFleetZone(tbp.Fleet.Id, tbp.DropOffAddress);
								}
								else if (tbp.DropOffAddressType == AddressType.Airport)
								{
									zone = GetFleetZoneByAirport(tbp.Fleet.Id, tbp.DropoffAirport);
								}

								if (zone != null)
								{
									dropoffZone = zone;
								}
							}
							else
							{
								result = false;
								logger.Warn("Cannot process drop off zone by previous invalid request");
							}
						}
					}
					else
					{
						result = false;
						logger.Warn("Cannot process drop off address by previous invalid request");
					}
					#endregion

					#region Determine FlatRate or Rate
					if (reservation.Service.Location.Dropoff != null)
					{
						long? pickupZoneId = null;
						long? dropoffZoneId = null;

						if ((pickupZone != null) && (dropoffZone != null))
						{
							pickupZoneId = pickupZone.Id;
							dropoffZoneId = dropoffZone.Id;
						}

						if ((result) && pickupZoneId.HasValue && dropoffZoneId.HasValue && (flatRates.Any())
							&& flatRates.Any(x => x.PickupZoneId == pickupZoneId.Value && x.DropoffZoneId == dropoffZoneId.Value))
						{
							//flatrate
							tbp.IsFixedPrice = true;

							#region Do calculation for flatrate

							//caculate flatrate
							var tfzf = flatRates.Where(x => x.PickupZoneId == pickupZoneId && x.DropoffZoneId == dropoffZoneId).Select(x => x).FirstOrDefault();

							tbp.FixedPrice = tfzf.Amount;

							logger.Info("Flat rate found");

							#endregion
						}
						else
						{
							tbp.IsFixedPrice = false;
						}
					}
					#endregion
				}
				else
				{
					tbp.IsFixedPrice = false;
				}

				#endregion

				if (TrackTime != null)
					TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility,
						"ValidateBookingRequest_CheckFixedPriceZone");
				swEachPart.Stop();
				logger.DebugFormat("Check fixed price zone  Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();

				#region Create this trip into database

				if (result)
				{
					tbp.Trip = CreateNewTrip(tbp, tokenVTOD.Username);				
				}
				else
				{
					result = false;
					var vtodException = VtodException.CreateException(ExceptionType.Taxi, 2001);
					logger.Warn(vtodException.ExceptionMessage.Message);
					throw vtodException; // new ValidationException(Messages.Taxi_Common_UnableCreateTrip);
				}

				#endregion

				if (TrackTime != null)
					TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility,
						"ValidateBookingRequest_ GreateTripInDatabase");
				swEachPart.Stop();
				logger.DebugFormat("CreateNewTrip in db  Duration:{0} ms.", swEachPart.ElapsedMilliseconds);


			}
			else
			{
				var vtodException = VtodException.CreateFieldRequiredValidationException("GroundReservation");
				logger.Warn(vtodException.ExceptionMessage.Message);
				throw vtodException; // new ValidationException(Messages.Taxi_Common_InvalidReservation);
			}

			#endregion

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);


			#region Create taxi log

			if (result)
			{
				if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
				{
					WriteTaxiLog(tbp.Trip.Id, tbp.serviceAPIID, TaxiServiceMethodType.Book, sw.ElapsedMilliseconds,
						string.Empty, string.Empty, null, null);
				}
			}
			else
			{
				result = false;
				var vtodException = Common.DTO.VtodException.CreateException(ExceptionType.Taxi, 2001);
				// ("CardNumber and CardNumberID");
				logger.Warn(vtodException.ExceptionMessage.Message);
				throw vtodException; // new ValidationException(Messages.Validation_NullPayment);
			}

			#endregion

			#region Track

			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateBookingRequest");

			#endregion


			return result;
		}
        public bool ValidateMTDataModifyBookingRequest(TokenRS tokenVTOD, OTA_GroundBookRQ request, out TaxiBookingParameter tbp, taxi_fleet fleet,GetBookingFullResult bookingResult)
        {
            bool result = true;
            Stopwatch sw = new Stopwatch();
            sw.Start();
            tbp = new TaxiBookingParameter();
            tbp.Fleet = fleet;
            Stopwatch swEachPart = new Stopwatch();

            #region Track

            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");

            #endregion

            #region Validate Reservation

            if (request.GroundReservations.Any())
            {
                GroundReservation reservation = request.GroundReservations.FirstOrDefault();

                #region Set token, request

                tbp.tokenVTOD = tokenVTOD;
                tbp.request = request;

                #endregion
                if(request==null || request.TPA_Extensions==null || request.TPA_Extensions.MemberID==null)
                {
                    var vtodException = VtodException.CreateFieldRequiredValidationException("MemberId");
                    logger.Warn(vtodException.ExceptionMessage.Message);
                    throw vtodException;
                }
                else
                {
                    tbp.MemberID = request.TPA_Extensions.MemberID.ToInt32();
                }
                #region Set Source and Device and ref

                //if (request.TPA_Extensions != null && !string.IsNullOrWhiteSpace(request.TPA_Extensions.Source))
                //{
                //    tbp.Source = request.TPA_Extensions.Source;
                //}

                //if (request.TPA_Extensions != null && !string.IsNullOrWhiteSpace(request.TPA_Extensions.Device))
                //{
                //    tbp.Device = request.TPA_Extensions.Device;
                //}

                //if (!string.IsNullOrWhiteSpace(request.EchoToken))
                //{
                //    tbp.Ref = request.EchoToken;
                //}

                #endregion

                swEachPart.Restart();

                #region Validate Customer

                if (bookingResult!= null && bookingResult.Settings!=null)
                {
                        if (bookingResult.Settings.Booking != null)
                        {
                            if (!string.IsNullOrWhiteSpace(bookingResult.Settings.Booking.ContactName))
                            {
                            tbp.FirstName = bookingResult.Settings.Booking.ContactName.Substring(0, bookingResult.Settings.Booking.ContactName.IndexOf(" "));
                            tbp.LastName = bookingResult.Settings.Booking.ContactName.Substring(bookingResult.Settings.Booking.ContactName.IndexOf(" ") + 1);
                            logger.Info("First name and last name are valid.");
                        }
                            else
                            {
                            result = false;
                            var vtodException =
                                VtodException.CreateFieldRequiredValidationException(
                                    "PassengerGivenName|PassengerSurname");
                            logger.Warn(vtodException.ExceptionMessage.Message);
                            throw vtodException;

                        }
                        }
                        else
                        {
                            result = false;
                            var vtodException = VtodException.CreateFieldRequiredValidationException("PassengerName");
                            logger.Warn(vtodException.ExceptionMessage.Message);
                            throw vtodException; 
                        }

                    //Telephone
                        if (!string.IsNullOrWhiteSpace(bookingResult.Settings.Booking.ContactPhoneNumber))
                        { 
                           tbp.PhoneNumber = bookingResult.Settings.Booking.ContactPhoneNumber;
                           logger.Info("Phone number(s) is/are valid.");
                        }
                        else
                        {
                            result = false;
                            var vtodException = VtodException.CreateFieldRequiredValidationException("Telephones");
                            logger.Warn(vtodException.ExceptionMessage.Message);
                            throw vtodException;
                        }
                        if (!string.IsNullOrWhiteSpace(bookingResult.Settings.Booking.ContactEmail))
                        {
                           tbp.EmailAddress = bookingResult.Settings.Booking.ContactEmail;
                           logger.Info(Messages.ECar_Common_ValidReservationPassengerPrimaryTelephones);
                        }
                        if (result)
                        {
                            logger.InfoFormat("Customer firstname={0}, lastname={1}, phonenumber={2}", tbp.FirstName,
                                tbp.LastName, tbp.PhoneNumber);
                        }
                        else
                        {
                            result = false;
                            var vtodException = VtodException.CreateException(ExceptionType.Taxi, 2008);
                            logger.Warn(vtodException.ExceptionMessage.Message);
                            throw vtodException; 
                        }
                }
                else
                {
                    result = false;
                    var vtodException = VtodException.CreateFieldRequiredValidationException("Passenger");
                    logger.Warn(vtodException.ExceptionMessage.Message);
                    throw vtodException; // new ValidationException(Messages.Taxi_Common_InvalidReservation);
                }

                #endregion

                if (TrackTime != null)
                    TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateBookingRequest_ValidateCustomer");
                swEachPart.Stop();
                logger.DebugFormat("Validate Customer Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                //pickiup now

                #region determine Pick me up now
                // if (bookingResult.Settings != null && bookingResult.Settings.Booking != null)
                //{
                //    if (bookingResult.Settings.Booking.Locations != null && bookingResult.Settings.Booking.Locations.Any())
                //    {
                //        var pickupLocation = bookingResult.Settings.Booking.Locations.Where(p => p.LocationType == UDI.VTOD.MTData.BookingWebService.LocationType.PickUp).FirstOrDefault();
                //        if (pickupLocation != null)
                //        {
                //            if (pickupLocation.TimeMode == TimeMode.NextAvailable)
                //            {
                //                tbp.PickupNow = true;
                //            }
                //            else
                //            {
                //                tbp.PickupNow = false;
                //            }
                //        }
                //    }
                //}
                tbp.PickupNow = request.TPA_Extensions.PickMeUpNow.ToBool();
                #endregion



                swEachPart.Restart();

                #region Validate Pickup Address

                if (reservation.Service != null && tbp.PickupNow==false)
                {
                    if (reservation.Service.Location != null)
                    {
                        if (reservation.Service.Location.Pickup != null)
                        {
                            if (((reservation.Service.Location.Pickup.Address != null) &&
                                 (reservation.Service.Location.Pickup.AirportInfo != null)) ||
                                ((reservation.Service.Location.Pickup.Address == null) &&
                                 (reservation.Service.Location.Pickup.AirportInfo == null)))
                            {
                                result = false;
                                logger.Warn(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
                                throw new ValidationException(
                                    Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
                            }
                            else if ((reservation.Service.Location.Pickup.Address != null) &&
                                     (reservation.Service.Location.Pickup.AirportInfo == null))
                            {

                                if (
                                    (string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude) ||
                                     (string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude))) &&
                                    (string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.StreetNmbr) ||
                                     string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.AddressLine) ||
                                     string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CityName) ||
                                     string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.PostalCode) ||
                                     string.IsNullOrWhiteSpace(
                                         reservation.Service.Location.Pickup.Address.StateProv.StateCode) ||
                                     string.IsNullOrWhiteSpace(
                                         reservation.Service.Location.Pickup.Address.CountryName.Code)))
                                {
                                    result = false;
                                    logger.Warn(Messages.Validation_LocationDetail);
                                    throw new ValidationException(Messages.Validation_LocationDetail);
                                }
                                else if (
                                (!string.IsNullOrWhiteSpace(
                                    reservation.Service.Location.Pickup.Address.Longitude) &&
                                 (!string.IsNullOrWhiteSpace(
                                     reservation.Service.Location.Pickup.Address.Longitude))) &&
                                (string.IsNullOrWhiteSpace(
                                    reservation.Service.Location.Pickup.Address.StreetNmbr) ||
                                 string.IsNullOrWhiteSpace(
                                     reservation.Service.Location.Pickup.Address.AddressLine) ||
                                 string.IsNullOrWhiteSpace(
                                     reservation.Service.Location.Pickup.Address.CityName) ||
                                 string.IsNullOrWhiteSpace(
                                     reservation.Service.Location.Pickup.Address.PostalCode) ||
                                 string.IsNullOrWhiteSpace(
                                     reservation.Service.Location.Pickup.Address.StateProv.StateCode) ||
                                 string.IsNullOrWhiteSpace(
                                     reservation.Service.Location.Pickup.Address.CountryName.Code)))
                                {
                                    tbp.PickupAddressOnlyContainsLatAndLong = true;
                                    tbp.PickupAddressType = AddressType.Address;
                                    tbp.PickupAddress = ConvertAddress(reservation.Service.Location.Pickup);
                                }
                                else if (
                                    !string.IsNullOrWhiteSpace(
                                        reservation.Service.Location.Pickup.Address.AddressLine) &&
                                    !string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CityName) &&
                                    !string.IsNullOrWhiteSpace(
                                        reservation.Service.Location.Pickup.Address.PostalCode) &&
                                    !string.IsNullOrWhiteSpace(
                                        reservation.Service.Location.Pickup.Address.StateProv.StateCode) &&
                                    !string.IsNullOrWhiteSpace(
                                        reservation.Service.Location.Pickup.Address.CountryName.Code))
                                {
                                    //Address
                                    Map.DTO.Address pickupAddress = null;
                                    //string pickupAddressStr = GetAddressString(reservation.Service.Location.Pickup.Address);
                                    logger.Info("Skip MAP API Validation.");
                                    pickupAddress = ConvertAddress(reservation.Service.Location.Pickup);
                                    tbp.PickupAddressType = AddressType.Address;
                                    tbp.PickupAddress = pickupAddress;
                                    logger.Info("Pickup AddressType: Address");
                                }
                                else
                                {
                                    result = false;
                                    var vtodException = VtodException.CreateException(ExceptionType.Taxi, 1);
                                    logger.Warn(vtodException.ExceptionMessage.Message);
                                    // Messages.Taxi_Common_UnknownError);
                                    throw vtodException;
                                }
                            }
                            else if ((reservation.Service.Location.Pickup.Address == null) &&
                                     (reservation.Service.Location.Pickup.AirportInfo != null))
                            {
                                //airport
                                if (((reservation.Service.Location.Pickup.AirportInfo.Arrival != null) &&
                                     (reservation.Service.Location.Pickup.AirportInfo.Departure != null)) ||
                                    ((reservation.Service.Location.Pickup.AirportInfo.Arrival == null) &&
                                     (reservation.Service.Location.Pickup.AirportInfo.Departure == null)))
                                {
                                    result = false;
                                    logger.Warn(Messages.Validation_ArrivalAndDepartureAirportInfo);
                                    throw VtodException.CreateValidationException(
                                        Messages.Validation_ArrivalAndDepartureAirportInfo);
                                }
                                else if (reservation.Service.Location.Pickup.AirportInfo.Arrival != null &&
                                         reservation.Service.Location.Pickup.AirportInfo.Departure == null)
                                {
                                    //Arrival
                                    tbp.PickupAddressType = AddressType.Airport;
                                    tbp.PickupAirport =
                                        reservation.Service.Location.Pickup.AirportInfo.Arrival.LocationCode;
                                    logger.Info("Pickup AddressType: AirportInfo.Arrival");
                                }
                                else if (reservation.Service.Location.Pickup.AirportInfo.Arrival == null &&
                                         reservation.Service.Location.Pickup.AirportInfo.Departure != null)
                                {
                                    //Depature

                                    tbp.PickupAddressType = AddressType.Airport;
                                    tbp.PickupAirport =
                                        reservation.Service.Location.Pickup.AirportInfo.Departure
                                            .LocationCode;
                                    logger.Info("Pickup AddressType: AirportInfo.Depature");
                                }

                                //longitude and latitude
                                if (result)
                                {
                                    double? longitude = null;
                                    double? latitude = null;
                                    if (GetLongitudeAndLatitudeForAirport(tbp.PickupAirport, out longitude,
                                        out latitude))
                                    {
                                        tbp.LongitudeForPickupAirport = longitude.Value;
                                        tbp.LatitudeForPickupAirport = latitude.Value;
                                    }
                                    else
                                    {
                                        throw VtodException.CreateException(ExceptionType.Taxi, 1010);
                                    }
                                }

                             
                            }

                        }
                  
                    }
                   
                }
                else
                {
                    if (bookingResult.Settings != null && bookingResult.Settings.Booking != null)
                    {
                        if (bookingResult.Settings.Booking.Locations != null && bookingResult.Settings.Booking.Locations.Any())
                        {
                            var pickupLocation = bookingResult.Settings.Booking.Locations.Where(p => p.LocationType == UDI.VTOD.MTData.BookingWebService.LocationType.PickUp).FirstOrDefault();
                            if (pickupLocation != null)
                            {
                                if (pickupLocation.AddressType.ToString() == AddressType.Airport)
                                {
                                    tbp.PickupAddressType = pickupLocation.AddressType.ToString();
                                    tbp.LatitudeForPickupAirport = pickupLocation.Address.Latitude.ToDouble();
                                    tbp.LongitudeForDropoffAirport = pickupLocation.Address.ToDouble();
                                }
                                else
                                {

                                    Map.DTO.Address pickupAddress = new Map.DTO.Address();
                                    if(pickupLocation.Address.Suburb!=null)
                                        pickupAddress.City = pickupLocation.Address.Suburb.Name;
                                    pickupAddress.Geolocation= new UDI.Map.DTO.Geolocation();
                                    pickupAddress.Geolocation.Latitude = pickupLocation.Address.Latitude.ToDecimal();
                                    pickupAddress.Geolocation.Longitude = pickupLocation.Address.Longitude.ToDecimal();
                                    pickupAddress.StreetNo = pickupLocation.Address.Unit;
                                    if (pickupLocation.Address.Street != null)
                                        pickupAddress.Street = pickupLocation.Address.Street.Name;
                                    if (pickupLocation.Address.Street.Postcode != null)
                                        pickupAddress.ZipCode = pickupLocation.Address.Street.Postcode;
                                    if (pickupLocation.Address.Suburb.Postcode != null)
                                        pickupAddress.ZipCode = pickupLocation.Address.Suburb.Postcode;
                                    tbp.PickupAddress = new Map.DTO.Address();
                                    tbp.PickupAddress = pickupAddress;
                                }
                               
                            }
                        }
                    }
                }
                if (bookingResult.Settings != null && bookingResult.Settings.Booking != null)
                {
                    if (bookingResult.Settings.Booking.Locations != null && bookingResult.Settings.Booking.Locations.Any())
                    {
                        var pickupLocation = bookingResult.Settings.Booking.Locations.Where(p => p.LocationType == UDI.VTOD.MTData.BookingWebService.LocationType.PickUp).FirstOrDefault();
                        if (pickupLocation != null)
                        {
                            tbp.RemarkForPickup = pickupLocation.Remark;
                            tbp.NumberOfPassenger = pickupLocation.Pax;
                        }
                    }
                 }
                   
                #endregion

                if (TrackTime != null)
                    TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility,
                        "ValidateBookingRequest_ValidatePickupAddress");
                swEachPart.Stop();
                logger.DebugFormat("Validate Pickup address Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                swEachPart.Restart();

                #region Validate Fleet

                if (result)
                {
                    //taxi_fleet fleet = null;
                    if (tbp.Fleet != null)//GetFleet(tbp.PickupAddressType, tbp.PickupAddress, tbp.PickupAirport, out fleet))
                    {
                        //tbp.Fleet = fleet;
                        logger.InfoFormat("Fleet found. ID={0}", tbp.Fleet.Id);

                        if (tbp.Fleet != null)
                        {
                            tbp.serviceAPIID =
                                GetTaxiFleetServiceAPIPreference(tbp.Fleet.Id, TaxiServiceAPIPreferenceConst.Book).ServiceAPIId;
                            #region Fetching Endpoint Names
                            List<string> endpointNames = MtdataEndpointConfiguration(tbp.serviceAPIID, tbp.Fleet.Id);
                            if (endpointNames != null && endpointNames.Any())
                            {
                                tbp.AuthenticationServiceEndpointName = endpointNames[0];
                                tbp.BookingWebServiceEndpointName = endpointNames[1];
                                tbp.OsiWebServiceEndpointName = endpointNames[2];
                                tbp.AddressWebServiceEndpointName = endpointNames[3];
                            }
                            #endregion
                        }
                    }
                    else
                    {
                        result = false;
                        var vtodException = VtodException.CreateException(ExceptionType.Taxi, 1011);
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException;
                        // new ValidationException(Messages.Taxi_Common_UnableToFindFleetByAddressOrAirportInfo);
                    }
                }
                else
                {
                    result = false;
                    var vtodException = VtodException.CreateException(ExceptionType.Taxi, 2001);
                    logger.Warn(vtodException.ExceptionMessage.Message);
                    throw vtodException;
                    // new ValidationException(Messages.Taxi_Common_UnableToFindFleetByAddressOrAirportInfo);
                }

                #endregion

                if (TrackTime != null)
                    TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateBookingRequest_ValidateFleet");
                swEachPart.Stop();
                logger.DebugFormat("Validate fleet Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                swEachPart.Restart();

                #region Validate User

                if (result)
                {
                    taxi_fleet_user user = null;
                    if (GetTaxiFleetUser(tbp.Fleet.Id, tokenVTOD.Username, out user))
                    {
                        tbp.User = user;
                        logger.Info("This user has sufficient privilege.");
                    }
                    else
                    {
                        result = false;
                        var vtodException = VtodException.CreateException(ExceptionType.Taxi, 102);
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException;
                        // new ValidationException(Messages.Taxi_Common_InsufficientPrivilege(tokenVTOD.Username, tbp.Fleet.Id));
                    }
                }

                #endregion

                if (TrackTime != null)
                    TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateBookingRequest_ValidateUser");
                swEachPart.Stop();
                logger.DebugFormat("Validate user Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                swEachPart.Restart();

                #region Validate PickupDateTime

                if (result)
                {
                  

                    #region Validate Pick me up now against DB fleet

                    //bool pickMeUpNow = true;
                    //bool pickMeUpLater = true;
                    //GetPickMeupOption(tbp.Fleet, out pickMeUpNow, out pickMeUpLater);
                    //#region PMUL
                    //if ((request.TPA_Extensions == null) || (!request.TPA_Extensions.PickMeUpNow.HasValue) || !request.TPA_Extensions.PickMeUpNow.Value)
                    //{

                    //    if (!string.IsNullOrWhiteSpace(request.GroundReservations.FirstOrDefault().Service.Location.Pickup.DateTime))
                    //    {
                    //        bool pmulResult = CheckPMUL(tbp.Fleet, request.GroundReservations.FirstOrDefault().Service.Location.Pickup.DateTime.ToDateTime());
                    //        logger.InfoFormat("PMUL :{0}:", pmulResult);
                    //        if (pmulResult == false)
                    //            throw VtodException.CreateException(ExceptionType.Taxi, 2013);

                    //    }
                    //}
                    //#endregion
                    //if (tbp.PickupNow == pickMeUpNow || !tbp.PickupNow == pickMeUpLater)
                    //{
                    //    logger.InfoFormat("Valid pick me up request. Fleet={0}, Trip={1}", tbp.Fleet.PickMeUpNowOption,
                    //        tbp.PickupNow);

                    //}
                    //else
                    //{
                    //    logger.WarnFormat("InValid pick me up request. Fleet={0}, Trip={1}", tbp.Fleet.PickMeUpNowOption,
                    //        tbp.PickupNow);
                    //    result = false;
                    //    if (tbp.PickupNow)
                    //    {
                    //        throw VtodException.CreateException(ExceptionType.Taxi, 2011);
                    //    }
                    //    else
                    //    {
                    //        throw VtodException.CreateException(ExceptionType.Taxi, 2012);
                    //    }
                    //}

                    #endregion


                    #region determine pick me up now Time

                    DateTime pickupDateTime;
                    if (tbp.PickupNow == false)
                    {
                        if (bookingResult.Settings != null && bookingResult.Settings.Booking != null)
                        {
                            if (bookingResult.Settings.Booking.Locations != null && bookingResult.Settings.Booking.Locations.Any())
                            {
                                var pickupLocation = bookingResult.Settings.Booking.Locations.Where(p => p.LocationType == UDI.VTOD.MTData.BookingWebService.LocationType.PickUp).FirstOrDefault();
                                if (pickupLocation != null)
                                {
                                    tbp.PickupDateTime = pickupLocation.Time;
                                }
                            }
                        }
                      
                    }
                    else
                    {
                        if (DateTime.TryParse(reservation.Service.Location.Pickup.DateTime, out pickupDateTime))
                        {
                            tbp.PickupDateTime = pickupDateTime;
                            logger.Info("PickupDateTime is correct");
                        }
                        else if (bookingResult.Settings != null && bookingResult.Settings.Booking != null)
                        {
                            if (bookingResult.Settings.Booking.Locations != null && bookingResult.Settings.Booking.Locations.Any())
                            {
                                var pickupLocation = bookingResult.Settings.Booking.Locations.Where(p => p.LocationType == UDI.VTOD.MTData.BookingWebService.LocationType.PickUp).FirstOrDefault();
                                if (pickupLocation != null)
                                {
                                    tbp.PickupDateTime = pickupLocation.Time;
                                }
                            }
                        }
                        else
                        {
                            result = false;
                            var vtodException = VtodException.CreateFieldFormatValidationException("PickupDateTime",
                                "DateTime");
                            logger.Warn(vtodException.ExceptionMessage.Message);
                            throw vtodException;
                        }
                    }

                    #endregion
                }
                else
                {
                    result = false;
                    var vtodException = VtodException.CreateException(ExceptionType.Taxi, 2001);
                    logger.Warn(vtodException.ExceptionMessage.Message);
                    throw vtodException;
                    // new ValidationException(Messages.Taxi_Common_UnableToFindFleetByAddressOrAirportInfo);
                }

                #endregion

                if (TrackTime != null)
                    TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility,
                        "ValidateBookingRequest_ValidatePickupDateTime");
                swEachPart.Stop();
                logger.DebugFormat("Validate PickupTime Duration:{0} ms.", swEachPart.ElapsedMilliseconds);
                swEachPart.Restart();

                #region Validate Dropoff Address

                //DropOff is optional: By_Pouyan
                if (reservation.Service.Location != null && reservation.Service.Location.Dropoff != null)
                    if (result)
                    {
                        if (reservation.Service != null)
                        {
                            if (reservation.Service.Location != null)
                            {
                                if (reservation.Service.Location.Dropoff != null)
                                {
                                    using (VTODEntities context = new VTODEntities())
                                    {
                                        if (((reservation.Service.Location.Dropoff.Address != null) &&
                                             (reservation.Service.Location.Dropoff.AirportInfo != null)) ||
                                            ((reservation.Service.Location.Dropoff.Address == null) &&
                                             (reservation.Service.Location.Dropoff.AirportInfo == null)))
                                        {
                                            result = false;
                                            logger.Warn(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
                                            throw VtodException.CreateValidationException(
                                                Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
                                        }
                                        else if ((reservation.Service.Location.Dropoff.Address != null) &&
                                                 (reservation.Service.Location.Dropoff.AirportInfo == null))
                                        {
                                            //Address
                                            if (
                                                (string.IsNullOrWhiteSpace(
                                                    reservation.Service.Location.Dropoff.Address.Longitude) ||
                                                 (string.IsNullOrWhiteSpace(
                                                     reservation.Service.Location.Dropoff.Address.Longitude))) &&
                                                (string.IsNullOrWhiteSpace(
                                                    reservation.Service.Location.Dropoff.Address.StreetNmbr) ||
                                                 string.IsNullOrWhiteSpace(
                                                     reservation.Service.Location.Dropoff.Address.AddressLine) ||
                                                 string.IsNullOrWhiteSpace(
                                                     reservation.Service.Location.Dropoff.Address.CityName) ||
                                                 string.IsNullOrWhiteSpace(
                                                     reservation.Service.Location.Dropoff.Address.PostalCode) ||
                                                 string.IsNullOrWhiteSpace(
                                                     reservation.Service.Location.Dropoff.Address.StateProv.StateCode) ||
                                                 string.IsNullOrWhiteSpace(
                                                     reservation.Service.Location.Dropoff.Address.CountryName.Code)))
                                            //if (string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CountryName.Code))
                                            {
                                                result = false;
                                                logger.Warn(Messages.Validation_LocationDetail);
                                                throw VtodException.CreateValidationException(
                                                    Messages.Validation_LocationDetail);
                                            }
                                            else if (
                                                (!string.IsNullOrWhiteSpace(
                                                    reservation.Service.Location.Dropoff.Address.Longitude) &&
                                                 (!string.IsNullOrWhiteSpace(
                                                     reservation.Service.Location.Dropoff.Address.Longitude))) &&
                                                (string.IsNullOrWhiteSpace(
                                                    reservation.Service.Location.Dropoff.Address.StreetNmbr) ||
                                                 string.IsNullOrWhiteSpace(
                                                     reservation.Service.Location.Dropoff.Address.AddressLine) ||
                                                 string.IsNullOrWhiteSpace(
                                                     reservation.Service.Location.Dropoff.Address.CityName) ||
                                                 string.IsNullOrWhiteSpace(
                                                     reservation.Service.Location.Dropoff.Address.PostalCode) ||
                                                 string.IsNullOrWhiteSpace(
                                                     reservation.Service.Location.Dropoff.Address.StateProv
                                                         .StateCode) ||
                                                 string.IsNullOrWhiteSpace(
                                                     reservation.Service.Location.Dropoff.Address.CountryName
                                                         .Code)))
                                            {
                                                tbp.DropOffAddressOnlyContainsLatAndLong = true;
                                                tbp.DropOffAddressType = AddressType.Address;
                                                tbp.DropOffAddress =
                                                    ConvertAddress(reservation.Service.Location.Dropoff);
                                            }
                                            else if (
                                                !string.IsNullOrWhiteSpace(
                                                    reservation.Service.Location.Dropoff.Address.AddressLine) &&
                                                !string.IsNullOrWhiteSpace(
                                                    reservation.Service.Location.Dropoff.Address.CityName) &&
                                                !string.IsNullOrWhiteSpace(
                                                    reservation.Service.Location.Dropoff.Address.PostalCode) &&
                                                !string.IsNullOrWhiteSpace(
                                                    reservation.Service.Location.Dropoff.Address.StateProv
                                                        .StateCode) &&
                                                !string.IsNullOrWhiteSpace(
                                                    reservation.Service.Location.Dropoff.Address.CountryName.Code))
                                            {
                                                Map.DTO.Address dropoffAddress = null;
                                                string dropoffAddressStr =
                                                    GetAddressString(reservation.Service.Location.Dropoff.Address);
                                                logger.Info("Skip MAP API Validation.");
                                                dropoffAddress =
                                                    ConvertAddress(reservation.Service.Location.Dropoff);
                                                tbp.DropOffAddress = dropoffAddress;
                                                tbp.DropOffAddressType =
                                                    UDI.VTOD.Domain.Taxi.Const.AddressType.Address;
                                                logger.Info("Dropoff AddressType=Address");
                                            }
                                            else
                                            {
                                                result = false;
                                                var vtodException =
                                                    VtodException.CreateException(ExceptionType.Taxi, 1);
                                                logger.Warn(vtodException.ExceptionMessage.Message);
                                                throw vtodException;
                                            }
                                        }
                                        else if ((reservation.Service.Location.Dropoff.Address == null) &&
                                                 (reservation.Service.Location.Dropoff.AirportInfo != null))
                                        {

                                            //airport
                                            if (((reservation.Service.Location.Dropoff.AirportInfo.Arrival != null) &&
                                                 (reservation.Service.Location.Dropoff.AirportInfo.Departure !=
                                                  null)) ||
                                                ((reservation.Service.Location.Dropoff.AirportInfo.Arrival == null) &&
                                                 (reservation.Service.Location.Dropoff.AirportInfo.Departure ==
                                                  null)))
                                            {
                                                result = false;
                                                logger.Warn(Messages.Validation_ArrivalAndDepartureAirportInfo);
                                                throw VtodException.CreateValidationException(
                                                    Messages.Validation_ArrivalAndDepartureAirportInfo);
                                            }
                                            else if (reservation.Service.Location.Dropoff.AirportInfo.Arrival !=
                                                     null &&
                                                     reservation.Service.Location.Dropoff.AirportInfo.Departure ==
                                                     null)
                                            {
                                                //Arrival
                                                tbp.DropOffAddressType = AddressType.Airport;
                                                tbp.DropoffAirport =
                                                    reservation.Service.Location.Dropoff.AirportInfo.Arrival
                                                        .LocationCode;
                                                logger.Info("Dropoff AddressType: AirportInfo.Arrival");
                                            }
                                            else if (reservation.Service.Location.Dropoff.AirportInfo.Arrival ==
                                                     null &&
                                                     reservation.Service.Location.Dropoff.AirportInfo.Departure !=
                                                     null)
                                            {
                                                //Depature
                                                tbp.DropOffAddressType = AddressType.Airport;
                                                tbp.DropoffAirport =
                                                    reservation.Service.Location.Dropoff.AirportInfo
                                                        .Departure.LocationCode;
                                                logger.Info("Dropoff AddressType: AirportInfo.Depature");
                                            }

                                            //longitude and latitude
                                            if (result)
                                            {
                                                double? longitude = null;
                                                double? latitude = null;
                                                if (GetLongitudeAndLatitudeForAirport(tbp.DropoffAirport,
                                                    out longitude, out latitude))
                                                {
                                                    tbp.LongitudeForDropoffAirport = longitude.Value;
                                                    tbp.LatitudeForDropoffAirport = latitude.Value;
                                                }
                                                else
                                                {
                                                    throw VtodException.CreateException(ExceptionType.Taxi, 1010);
                                                    // new ValidationException(Messages.Taxi_Common_UnableToFindAirportLongitudeAndLatitude);
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (bookingResult.Settings != null && bookingResult.Settings.Booking != null)
                                    {
                                        if (bookingResult.Settings.Booking.Locations != null && bookingResult.Settings.Booking.Locations.Any())
                                        {
                                            var droppffLocation = bookingResult.Settings.Booking.Locations.Where(p => p.LocationType == UDI.VTOD.MTData.BookingWebService.LocationType.DropOff).FirstOrDefault();
                                            if (droppffLocation != null)
                                            {
                                                if (droppffLocation.AddressType.ToString() == AddressType.Airport)
                                                {
                                                    tbp.DropOffAddressType = droppffLocation.AddressType.ToString();
                                                    tbp.LatitudeForDropoffAirport = droppffLocation.Address.Latitude.ToDouble();
                                                    tbp.LongitudeForDropoffAirport = droppffLocation.Address.ToDouble();
                                                }
                                                else
                                                {

                                                    Map.DTO.Address DropOffAddress = new Map.DTO.Address();
                                                    if (droppffLocation.Address.Suburb != null)
                                                        DropOffAddress.City = droppffLocation.Address.Suburb.Name;
                                                    DropOffAddress.Latitude = droppffLocation.Address.Latitude.ToString();
                                                    DropOffAddress.Longitude = droppffLocation.Address.Longitude.ToString();
                                                    if (droppffLocation.Address.Street != null)
                                                        DropOffAddress.Street = droppffLocation.Address.Street.Name;
                                                    DropOffAddress.StreetNo = droppffLocation.Address.Unit;
                                                    if (droppffLocation.Address.Street.Postcode != null)
                                                        DropOffAddress.ZipCode = droppffLocation.Address.Street.Postcode;
                                                    if (droppffLocation.Address.Suburb.Postcode != null)
                                                        DropOffAddress.ZipCode = droppffLocation.Address.Suburb.Postcode;
                                                    DropOffAddress.Geolocation = new UDI.Map.DTO.Geolocation();
                                                    DropOffAddress.Geolocation.Latitude = droppffLocation.Address.Latitude.ToDecimal();
                                                    DropOffAddress.Geolocation.Longitude = droppffLocation.Address.Longitude.ToDecimal();
                                                    tbp.DropOffAddress = new Map.DTO.Address();
                                                    tbp.DropOffAddress = DropOffAddress;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (bookingResult.Settings != null && bookingResult.Settings.Booking != null)
                            {
                                if (bookingResult.Settings.Booking.Locations != null && bookingResult.Settings.Booking.Locations.Any())
                                {
                                    var dropoffLocation = bookingResult.Settings.Booking.Locations.Where(p => p.LocationType == UDI.VTOD.MTData.BookingWebService.LocationType.DropOff).FirstOrDefault();
                                    if (dropoffLocation != null)
                                    {
                                        if (dropoffLocation.AddressType.ToString() == AddressType.Airport)
                                        {
                                            tbp.DropOffAddressType = dropoffLocation.AddressType.ToString();
                                            tbp.LatitudeForDropoffAirport = dropoffLocation.Address.Latitude.ToDouble();
                                            tbp.LongitudeForDropoffAirport = dropoffLocation.Address.ToDouble();
                                        }
                                        else
                                        {
                                            Map.DTO.Address DropOffAddress = new Map.DTO.Address();
                                            if (dropoffLocation.Address.Suburb != null)
                                                DropOffAddress.City = dropoffLocation.Address.Suburb.Name;
                                            DropOffAddress.Latitude = dropoffLocation.Address.Latitude.ToString();
                                            DropOffAddress.Longitude = dropoffLocation.Address.Longitude.ToString();
                                            if (dropoffLocation.Address.Street != null)
                                                DropOffAddress.Street = dropoffLocation.Address.Street.Name;
                                            DropOffAddress.StreetNo = dropoffLocation.Address.Unit;
                                            if (dropoffLocation.Address.Street.Postcode != null)
                                                DropOffAddress.ZipCode = dropoffLocation.Address.Street.Postcode;
                                            if (dropoffLocation.Address.Suburb.Postcode != null)
                                                DropOffAddress.ZipCode = dropoffLocation.Address.Suburb.Postcode;
                                            DropOffAddress.Geolocation = new UDI.Map.DTO.Geolocation();
                                            DropOffAddress.Geolocation.Latitude = dropoffLocation.Address.Latitude.ToDecimal();
                                            DropOffAddress.Geolocation.Longitude = dropoffLocation.Address.Longitude.ToDecimal();
                                            tbp.DropOffAddress = new Map.DTO.Address();
                                            tbp.DropOffAddress = DropOffAddress;
                                        }
                                    }
                                }
                            }
                        }
                    }

                #endregion


                if (bookingResult.Settings != null && bookingResult.Settings.Booking != null)
                {
                    if (bookingResult.Settings.Booking.Locations != null && bookingResult.Settings.Booking.Locations.Any())
                    {
                        var dropoffLocation = bookingResult.Settings.Booking.Locations.Where(p => p.LocationType == UDI.VTOD.MTData.BookingWebService.LocationType.DropOff).FirstOrDefault();
                        if (dropoffLocation != null)
                            tbp.RemarkForDropOff = dropoffLocation.Remark;
                    }
                }
                if (TrackTime != null)
                    TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility,
                        "ValidateBookingRequest_ValidateDropOffAddress");
                swEachPart.Stop();
                logger.DebugFormat("Validate dropoff address Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                swEachPart.Restart();
                if (TrackTime != null)
                    TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility,
                        "ValidateBookingRequest_ValidateDropoffDateTime");
                swEachPart.Stop();
                logger.DebugFormat("Validate dropoff time Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

               
                swEachPart.Restart();

                #region Validate Available address type

                if (result)
                {
                   
                    UtilityDomain ud = new UtilityDomain();
                    if (tbp.PickupAddressType == AddressType.Address)
                    {
                        if (tbp.PickupAddress.Geolocation == null)
                        {
                            result = false;
                            var vtodException =
                                VtodException.CreateFieldRequiredValidationException("PickupAddress.Geolocation");
                            logger.Warn(vtodException.ExceptionMessage.Message);
                            throw vtodException;
                        }

                        UtilityGetAirportNameRQ uganRQ = new UtilityGetAirportNameRQ();
                        uganRQ.Address = new Address();
                        uganRQ.Address.Latitude = tbp.PickupAddress.Geolocation.Latitude.ToString();
                        uganRQ.Address.Longitude = tbp.PickupAddress.Geolocation.Longitude.ToString();

                        UtilityGetAirportNameRS uganRS = ud.GetAirportName(tokenVTOD, uganRQ);
                        if (uganRS.Success != null)
                        {
                            if (!string.IsNullOrWhiteSpace(uganRS.AirportName))
                            {
                                //pickupAddressType = AddressType.Airport;
                                tbp.PickupAddressType = AddressType.Airport;
                                tbp.PickupAirport = uganRS.AirportName;
                                tbp.LongitudeForPickupAirport = uganRQ.Address.Longitude.ToDouble();
                                tbp.LatitudeForPickupAirport = uganRQ.Address.Longitude.ToDouble();
                            }
                        }

                    }

                    if (tbp.DropOffAddressType == AddressType.Address)
                    {
                        if (tbp.DropOffAddress.Geolocation == null)
                        {
                            result = false;
                            var vtodException =
                                VtodException.CreateFieldRequiredValidationException("DropOffAddress.Geolocation");
                            logger.Warn(vtodException.ExceptionMessage.Message);
                            throw vtodException;
                        }

                        UtilityGetAirportNameRQ uganRQ = new UtilityGetAirportNameRQ();
                        uganRQ.Address = new Address();
                        uganRQ.Address.Latitude = tbp.DropOffAddress.Geolocation.Latitude.ToString();
                        uganRQ.Address.Longitude = tbp.DropOffAddress.Geolocation.Longitude.ToString();

                        UtilityGetAirportNameRS uganRS = ud.GetAirportName(tokenVTOD, uganRQ);
                        if (uganRS.Success != null)
                        {
                            if (!string.IsNullOrWhiteSpace(uganRS.AirportName))
                            {
                                //dropoffAddressType = AddressType.Airport;
                                tbp.DropOffAddressType = AddressType.Airport;
                                tbp.DropoffAirport = uganRS.AirportName;
                                tbp.LongitudeForDropoffAirport = uganRQ.Address.Longitude.ToDouble();
                                tbp.LatitudeForDropoffAirport = uganRQ.Address.Longitude.ToDouble();
                            }
                        }
                    }

                    //check database setting
                    //1. get taxi_fleet_availableaddresstype
                    using (VTODEntities context = new VTODEntities())
                    {
                        long fleetId = tbp.Fleet.Id;
                        if (context.taxi_fleet_availableaddresstype.Where(x => fleetId == x.FleetId).Any())
                        {
                            taxi_fleet_availableaddresstype tfa =
                                context.taxi_fleet_availableaddresstype.Where(x => fleetId == x.FleetId)
                                    .Select(x => x)
                                    .FirstOrDefault();
                            //2. check if the type is available or not
                            //Taxi_Common_InvalidAddressTypeForFleet
                            if (AddressType.Address.ToString() == tbp.PickupAddressType &&
                                AddressType.Address.ToString() == tbp.DropOffAddressType)
                            {
                                //address to address
                                if (tfa.AddressToAddress)
                                {
                                    result = true;
                                }
                                else
                                {
                                    result = false;
                                    throw VtodException.CreateException(ExceptionType.Taxi, 1013);
                                    //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                }
                            }
                            else if (AddressType.Address.ToString() == tbp.PickupAddressType &&
                                     AddressType.Airport.ToString() == tbp.DropOffAddressType)
                            {
                                //address to airport
                                if (tfa.AddressToAirport)
                                {
                                    result = true;
                                }
                                else
                                {
                                    result = false;
                                    throw VtodException.CreateException(ExceptionType.Taxi, 2020);
                                    //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                }
                            }
                            else if (AddressType.Address.ToString() == tbp.PickupAddressType &&
                                     null == tbp.DropOffAddressType)
                            {
                                //address to null
                                if (tfa.AddressToNull)
                                {
                                    result = true;
                                }
                                else
                                {
                                    result = false;
                                    throw VtodException.CreateException(ExceptionType.Taxi, 1013);
                                    //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                }
                            }
                            else if (AddressType.Airport.ToString() == tbp.PickupAddressType &&
                                     AddressType.Address.ToString() == tbp.DropOffAddressType)
                            {
                                //airport to address
                                if (tfa.AirportToAddress)
                                {
                                    result = true;
                                }
                                else
                                {
                                    result = false;
                                    throw VtodException.CreateException(ExceptionType.Taxi, 2019);
                                    //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                }
                            }
                            else if (AddressType.Airport.ToString() == tbp.PickupAddressType &&
                                     AddressType.Airport.ToString() == tbp.DropOffAddressType)
                            {
                                //airport to airport
                                if (tfa.AirportToAirport)
                                {
                                    result = true;
                                }
                                else
                                {
                                    result = false;
                                    throw VtodException.CreateException(ExceptionType.Taxi, 1013);
                                    //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                }
                            }
                            else if (AddressType.Airport.ToString() == tbp.PickupAddressType &&
                                     null == tbp.DropOffAddressType)
                            {
                                //airport to null
                                if (tfa.AirportToNull)
                                {
                                    result = true;
                                }
                                else
                                {
                                    result = false;
                                    throw VtodException.CreateException(ExceptionType.Taxi, 1013);
                                    //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                }
                            }
                        }
                        else
                        {
                            result = false;
                            throw VtodException.CreateException(ExceptionType.Taxi, 1011);
                            // new ValidationException(Messages.Taxi_Common_UnableToGetAvailableFleets);
                        }
                    }


                }
                else
                {
                    result = false;
                    var vtodException = VtodException.CreateFieldRequiredValidationException("Paymnet");
                    //ExceptionType.Taxi, 2001);
                    logger.Warn(vtodException.ExceptionMessage.Message);
                    throw vtodException;
                    // new ValidationException(Messages.Taxi_Common_UnableToFindFleetByAddressOrAirportInfo);
                }

                #endregion

                if (TrackTime != null)
                    TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility,
                        "ValidateBookingRequest_ValidateAvailableAddressType");
                swEachPart.Stop();
                logger.DebugFormat("Validate available address type Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                swEachPart.Restart();

                #region Validate PaymentInfo

                if (request.Payments == null || request.Payments.Payments == null || !request.Payments.Payments.Any())
                {
                    result = true;
                    if (bookingResult.Settings.Booking.PaymentMethod != null)
                    {
                        if (bookingResult.Settings.Booking.PaymentMethod.ToString().ToLower() == "Cash".ToLower())
                        {
                            tbp.PaymentType = Common.DTO.Enum.PaymentType.Cash;
                        }
                        else
                        {
                            tbp.PaymentType = Common.DTO.Enum.PaymentType.PaymentCard;
                            tbp.CreditCardID = bookingResult.Settings.Booking.PaymentMethod.ID.ToInt32();
                        }
                    }

                }
                else
                {
                    if (request.Payments.Payments.First().PaymentCard != null &&
                        request.Payments.Payments.First().Cash == null)
                    {
                        if (request.Payments.Payments.First().PaymentCard.CardNumber == null ||
                            string.IsNullOrWhiteSpace(request.Payments.Payments.First().PaymentCard.CardNumber.ID))
                        {
                            result = false;
                            var vtodException =
                                Common.DTO.VtodException.CreateFieldRequiredValidationException(
                                    "CardNumber and CardNumberID");
                            logger.Warn(vtodException.ExceptionMessage.Message);
                            throw vtodException; // new ValidationException(Messages.Validation_NullPayment);
                        }
                        else
                        {
                            tbp.CreditCardID = request.Payments.Payments.First().PaymentCard.CardNumber.ID.ToInt32();
                            tbp.PaymentType = Common.DTO.Enum.PaymentType.PaymentCard;


                            #region Pre-Authorze creadit card

                            //1. Get pre auth information
                            //tbp.User.PreAuthCreditCardAmount
                            //tbp.User.PreAuthCreditCardOption
                            //tbp.MemberID



                            //2. Do pre authorization
                            if (tbp.User.PreAuthCreditCardOption)
                            {
                                PaymentDomain pd = new PaymentDomain();
                                PreSaleVerificationResponse preAuthResult;
                                try
                                {
                                    preAuthResult = pd.PreSaleCardVerification(
                                        tokenVTOD,
                                        tbp.CreditCardID.Value,
                                        tbp.User.PreAuthCreditCardAmount,
                                        tbp.MemberID.Value,
                                        Convert.ToInt32(tbp.Fleet.SDS_FleetMerchantID.Value.ToString())
                                        );

                                }
                                catch (Exception ex)
                                {
                                    logger.Error("Unable to pre auth credit card information");
                                    logger.ErrorFormat("ex={0}", ex.Message);
                                    throw;
                                }
                                if (preAuthResult != null)
                                {
                                    if (preAuthResult.IsPrepaidCard == true)
                                    {
                                        throw VtodException.CreateException(ExceptionType.Taxi, 2021);
                                    }
                                    if (preAuthResult.TransactionApproved == false)
                                    {
                                        throw VtodException.CreateException(ExceptionType.Taxi, 2010);
                                    }

                                }
                                else
                                    throw VtodException.CreateException(ExceptionType.Taxi, 2010);
                            }

                            #endregion


                        }
                    }
                    else if (request.Payments.Payments.First().Cash != null &&
                             request.Payments.Payments.First().PaymentCard == null &&
                             request.Payments.Payments.First().Cash.CashIndicator == true)
                    {
                        tbp.PaymentType = Common.DTO.Enum.PaymentType.Cash;
                    }
                    else if (request.Payments.Payments.First().MiscChargeOrder != null &&
                             !string.IsNullOrWhiteSpace(
                                 request.Payments.Payments.First().MiscChargeOrder.OriginalIssuePlace) &&
                             request.Payments.Payments.First().MiscChargeOrder.OriginalIssuePlace.ToUpper() == "SDS")
                    {
                        #region Authorize

                       
                        var membershipDomain = new UDI.VTOD.Domain.Membership.MembershipDomain();
                        #endregion

                    }
                    else if (request.Payments.Payments.First().MiscChargeOrder != null &&
                             !string.IsNullOrWhiteSpace(
                                 request.Payments.Payments.First().MiscChargeOrder.OriginalIssuePlace) &&
                             request.Payments.Payments.First().MiscChargeOrder.OriginalIssuePlace.ToUpper() ==
                             "CONSUMER")
                    {
                        #region Authorize
                        var membershipDomain = new UDI.VTOD.Domain.Membership.MembershipDomain();
                        #endregion

                    }
                    else
                    {
                        result = false;
                        logger.Warn(Messages.Validation_CreditCard);
                        throw VtodException.CreateValidationException(Messages.Validation_CreditCard);
                        // new ValidationException(Messages.General_BadCreditCard);
                    }
                }

                #endregion

                if (TrackTime != null)
                    TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility,
                        "ValidateBookingRequest_ValidatePaymentInfo");
                swEachPart.Stop();
                logger.DebugFormat("Validate payment Id Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                swEachPart.Restart();

                #region Get Gratuity

                try
                {
                    var tipInfo = bookingResult.Settings.Booking.TipInfo;
                    if (tipInfo != null)
                    {
                        tbp.GratuityType = tipInfo.TipType.ToString();
                        var tipvalue = tipInfo.Tip;
                        tbp.Gratuity = tipvalue.ToString();
                    }
                }
                catch (Exception ex)
                {
                    logger.Error("Taxi:Gratuity", ex);
                }

                #endregion

                if (TrackTime != null)
                    TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateBookingRequest_ GetGratuity");
                swEachPart.Stop();
                logger.DebugFormat("Get Gratuity Duration:{0} ms.", swEachPart.ElapsedMilliseconds);


                swEachPart.Restart();

                #region Check if it's fixed price zone

                if (result && tbp.Fleet.OverWriteFixedPrice)
                {
                    var flatRates = new List<TaxiFlatRateData>(); ;
                    taxi_fleet_zone pickupZone = null;
                    taxi_fleet_zone dropoffZone = null;

                    #region Get FlatRate
                    using (VTODEntities context = new VTODEntities())
                    {
                        long pickupfleetID = tbp.Fleet.Id;
                        var tfzfList = context.taxi_fleet_zone_flatrate.Where(x => x.PickupZone.FleetId == pickupfleetID).Select(x => x).ToList();
                        if (tfzfList.Any())
                        {
                            foreach (var x in tfzfList)
                            {
                                var tfrd = new TaxiFlatRateData { Amount = x.Amount, AmountUnit = x.AmountUnit, AppendTime = x.AppendTime, DropoffZoneId = x.DropoffZoneId, Id = x.Id, PickupZoneId = x.PickupZoneId };
                                tfrd.PickupZoneName = context.taxi_fleet_zone.Where(xx => xx.Id == tfrd.PickupZoneId).Select(xxx => xxx.Name).FirstOrDefault();
                                tfrd.DropOffZoneName = context.taxi_fleet_zone.Where(xx => xx.Id == tfrd.DropoffZoneId).Select(xxx => xxx.Name).FirstOrDefault();
                                flatRates.Add(tfrd);
                            }
                        }
                    }
                    #endregion

                    #region Pickup zone and dropoff zone
                    if (result)
                    {
                        //pickup zone
                        if ((tbp.PickupAddressType == AddressType.Address && tbp.PickupAddress.Geolocation != null && tbp.PickupAddress.Geolocation.Latitude != 0 && tbp.PickupAddress.Geolocation.Longitude != 0)
                            ||
                            (tbp.PickupAddressType == AddressType.Airport))
                        {
                            taxi_fleet_zone zone = null;

                            if (tbp.PickupAddressType == AddressType.Address)
                            {
                                zone = GetFleetZone(tbp.Fleet.Id, tbp.PickupAddress);
                            }
                            else if (tbp.PickupAddressType == AddressType.Airport)
                            {
                                zone = GetFleetZoneByAirport(tbp.Fleet.Id, tbp.PickupAirport);
                            }

                            if (zone != null)
                            {
                                pickupZone = zone;
                            }
                        }
                    }
                    else
                    {
                        result = false;
                        logger.Warn("Cannot process pickup zone by previous invalid request");
                    }

                    if (result)
                    {
                        if (reservation.Service.Location.Dropoff != null)
                        {
                            //drop off zone
                            if ((tbp.DropOffAddressType == AddressType.Address && tbp.DropOffAddress.Geolocation != null && tbp.DropOffAddress.Geolocation.Latitude != 0 && tbp.DropOffAddress.Geolocation.Longitude != 0)
                                ||
                                (tbp.DropOffAddressType == AddressType.Airport))
                            {
                                taxi_fleet_zone zone = null;

                                if (tbp.DropOffAddressType == AddressType.Address)
                                {
                                    zone = GetFleetZone(tbp.Fleet.Id, tbp.DropOffAddress);
                                }
                                else if (tbp.DropOffAddressType == AddressType.Airport)
                                {
                                    zone = GetFleetZoneByAirport(tbp.Fleet.Id, tbp.DropoffAirport);
                                }

                                if (zone != null)
                                {
                                    dropoffZone = zone;
                                }
                            }
                            else
                            {
                                result = false;
                                logger.Warn("Cannot process drop off zone by previous invalid request");
                            }
                        }
                    }
                    else
                    {
                        result = false;
                        logger.Warn("Cannot process drop off address by previous invalid request");
                    }
                    #endregion

                    #region Determine FlatRate or Rate
                    if (reservation.Service.Location.Dropoff != null)
                    {
                        long? pickupZoneId = null;
                        long? dropoffZoneId = null;

                        if ((pickupZone != null) && (dropoffZone != null))
                        {
                            pickupZoneId = pickupZone.Id;
                            dropoffZoneId = dropoffZone.Id;
                        }

                        if ((result) && pickupZoneId.HasValue && dropoffZoneId.HasValue && (flatRates.Any())
                            && flatRates.Any(x => x.PickupZoneId == pickupZoneId.Value && x.DropoffZoneId == dropoffZoneId.Value))
                        {
                            //flatrate
                            tbp.IsFixedPrice = true;

                            #region Do calculation for flatrate

                            //caculate flatrate
                            var tfzf = flatRates.Where(x => x.PickupZoneId == pickupZoneId && x.DropoffZoneId == dropoffZoneId).Select(x => x).FirstOrDefault();

                            tbp.FixedPrice = tfzf.Amount;

                            logger.Info("Flat rate found");

                            #endregion
                        }
                        else
                        {
                            tbp.IsFixedPrice = false;
                        }
                    }
                    #endregion
                }
                else
                {
                    tbp.IsFixedPrice = false;
                }

                #endregion

                if (TrackTime != null)
                    TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility,
                        "ValidateBookingRequest_CheckFixedPriceZone");
                swEachPart.Stop();
                logger.DebugFormat("Check fixed price zone  Duration:{0} ms.", swEachPart.ElapsedMilliseconds);
            }
            else
            {
                var vtodException = VtodException.CreateFieldRequiredValidationException("GroundReservation");
                logger.Warn(vtodException.ExceptionMessage.Message);
                throw vtodException; 
            }

            #endregion

            sw.Stop();
            logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);


            #region Create taxi log

            if (result)
            {
                if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
                {
                    WriteTaxiLog(request.References.FirstOrDefault().ID.ToInt64(), tbp.serviceAPIID, TaxiServiceMethodType.ModifyBook, sw.ElapsedMilliseconds,
                        string.Empty, string.Empty, null, null);
                }
            }
            else
            {
                result = false;
                var vtodException = Common.DTO.VtodException.CreateException(ExceptionType.Taxi, 2001);
                logger.Warn(vtodException.ExceptionMessage.Message);
                throw vtodException; 
            }

            #endregion

            #region Track

            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateModifyBookingRequest");

            #endregion

            return result;
        }

        public bool ValidateStatusRequest(TokenRS tokenVTOD, OTA_GroundResRetrieveRQ request, out TaxiStatusParameter tsp)
		{
			bool result = true;
			Stopwatch sw = new Stopwatch();
			sw.Start();
			tsp = new TaxiStatusParameter();
			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion

			if (request.Reference != null)
			{
				#region Set token, request
				tsp.tokenVTOD = tokenVTOD;
				tsp.request = request;
				#endregion

				#region Validate Reference Id
				if (request.Reference == null || !request.Reference.Any() || string.IsNullOrWhiteSpace(request.Reference.First().ID))
				{
					result = false;
					logger.Warn("Reference.ID is empty.");
				}
				else
				{
					logger.Info("Request is valid.");
				}
				#endregion

				#region Validate Trip
				if (result)
				{
					vtod_trip t = null;

					if (request.Reference.Where(x => x.Type == "Confirmation").Any())
					{
						var r = request.Reference.Where(x => x.Type == "Confirmation").FirstOrDefault();
						t = GetTaxiTrip(Convert.ToInt64(r.ID));
					}
					else 
                    {
						var r = request.Reference.Where(x => x.Type == "DispatchConfirmation").FirstOrDefault();
                        string firstname = string.Empty;
                        string lastname = string.Empty;
                        string phonenumber = string.Empty;
                        if (r.Verification != null &&  r.Verification.PersonName != null)
                        {
                            firstname = r.Verification.PersonName.GivenName;
                            lastname = r.Verification.PersonName.Surname;
                        }
                        if (r.Verification != null && r.Verification.TelephoneInfo != null)
                        {
                            phonenumber = r.Verification.TelephoneInfo.PhoneNumber;
                        }
                        t = GetTaxiTripByDispatchConfirmationId(r.ID, firstname, lastname, phonenumber.CleanPhone());
					}


					if (t != null)
					{
						tsp.Trip = t;
						logger.InfoFormat("Trip found. vtod tripID={0}", request.Reference.First().ID);
					}
					else
					{
						#region find trip by dispatch confirmation ID, first name, last name
						string firstName = string.Empty;
						string lastName = string.Empty;
						string dispatchTripId = request.Reference.First().ID;
						if (request.Reference.FirstOrDefault().Verification != null && request.Reference.FirstOrDefault().Verification.PersonName != null && !string.IsNullOrWhiteSpace(request.Reference.FirstOrDefault().Verification.PersonName.GivenName))
						{
							firstName = request.Reference.FirstOrDefault().Verification.PersonName.GivenName.ToLower().ToString();
						}
						if (request.Reference.FirstOrDefault().Verification != null && request.Reference.FirstOrDefault().Verification.PersonName != null && !string.IsNullOrWhiteSpace(request.Reference.FirstOrDefault().Verification.PersonName.Surname))
						{
							lastName = request.Reference.FirstOrDefault().Verification.PersonName.Surname.ToLower().ToString();
						}

						using (var db = new VTODEntities())
						{
							if (!string.IsNullOrWhiteSpace(firstName) && !string.IsNullOrWhiteSpace(lastName))
							{
								var taxiTrip = db.taxi_trip.Where(p => p.DispatchTripId == dispatchTripId && p.FirstName.ToLower().ToString() == firstName && p.LastName.ToLower().ToString() == lastName).FirstOrDefault();

								t = GetTaxiTrip(taxiTrip.Id);
								if (t != null)
								{
									tsp.Trip = t;
									logger.InfoFormat("Trip found. disptach tripID={0}", t.Id);
								}
								else
								{
									result = false;
									logger.WarnFormat("Cannot find this trip. tripID={0}, type={1}", request.Reference.First().ID, request.Reference.First().Type);
								}
							}
						}
						#endregion
					}
				}
				else
				{
					result = false;
					logger.Warn("Cannot find trip by invalid referenceId");
				}
				#endregion

				#region Validate Fleet
				if (result)
				{
					taxi_fleet fleet = null;
					if (GetFleet(tsp.Trip.taxi_trip.FleetId, out fleet))
					{
						tsp.Fleet = fleet;
						logger.InfoFormat("Fleet found. ID={0}", fleet.Id);

						if (fleet != null)
						{
							tsp.serviceAPIID = GetTaxiFleetServiceAPIPreference(fleet.Id, TaxiServiceAPIPreferenceConst.Status).ServiceAPIId;
                            #region Fetching Endpoint Names
                            List<string> endpointNames = null;
                            endpointNames = MtdataEndpointConfiguration(tsp.serviceAPIID, tsp.Fleet.Id);
                            if (endpointNames != null && endpointNames.Any())
                            {
                                tsp.AuthenticationServiceEndpointName = endpointNames[0];
                                tsp.BookingWebServiceEndpointName = endpointNames[1];
                                tsp.OsiWebServiceEndpointName = endpointNames[2];
                                tsp.AddressWebServiceEndpointName = endpointNames[3];
                            }
                            #endregion

                        }
                    }
					else
					{
						result = false;
						logger.Warn("Unable to find fleet by this fleetID");
					}
				}
				else
				{
					result = false;
					logger.Warn("Unable to find fleet by this fleetID");
				}
				#endregion

				#region Validate Fleet User
				if (result)
				{
					taxi_fleet_user fleet_user = null;
					if (GetTaxiFleetUser(tsp.Trip.taxi_trip.FleetId, tokenVTOD.Username, out fleet_user))
					{
						tsp.Fleet_User = fleet_user;
						logger.Info("This user has sufficient privilege.");

						tsp.User = GetUser(fleet_user.UserId);
					}
					else
					{
						result = false;
						logger.WarnFormat("Insufficient privilege. User={0}, FleetId={1}", tokenVTOD.Username, tsp.Fleet.Id);
					}
				}
				else
				{
					result = false;
					logger.Warn("Cannot validate this user by invalid fleet ");
				}
				#endregion

				#region GetAllTripStatusHistory
				if (result)
				{
					using (VTODEntities context = new VTODEntities())
					{
						long tripId = tsp.Trip.Id;
						tsp.lastTripStatus = context.taxi_trip_status.Where(x => x.TripID == tripId).Select(x => x).OrderByDescending(x => x.Id).FirstOrDefault();
					}
				}
				#endregion



			}
			else
			{
				result = false;
				logger.Info("Reference is null.");
			}

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Create taxi log
			if (result && tsp.Trip != null)
			{
				if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
				{
					WriteTaxiLog(tsp.Trip.Id, tsp.serviceAPIID, TaxiServiceMethodType.Status, sw.ElapsedMilliseconds, string.Empty, string.Empty, null, null);
				}
			}
			else
			{
				result = false;
				logger.Warn("Cannot create or get this taxi log by invalid trip");
			}
			#endregion

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateStatusRequest");
			#endregion

			return result;
		}


		public string ConvertToLocalTime(taxi_fleet fleet, DateTime dt)
		{
			return dt.FromUtc(fleet.ServerUTCOffset).ToString("yyyy-MM-ddTHH:mm:ss");
		}

		public bool IsVTODTaxiTrip(string DispatchTripId, long fleetId, string functionName)
		{
			bool result = true;
			Stopwatch sw = new Stopwatch();
			sw.Start();
			using (VTODEntities context = new VTODEntities())
			{

				taxi_fleet fleet = GetFleet(fleetId);
				if (fleet != null)
				{
					logger.InfoFormat("Fleet found. ID={0}", fleet.Id);
					long serviceAPIID = GetTaxiFleetServiceAPIPreference(fleet.Id, functionName).ServiceAPIId;
					result = IsVTODTaxiTrip(DispatchTripId, serviceAPIID);
				}
				else {
					result = false;
				}
			}
			return result;
		}

		public bool IsVTODTaxiTrip(string DispatchTripId, long serviceAPIId)
		{
			bool result = true;
			Stopwatch sw = new Stopwatch();
			sw.Start();

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion

			using (VTODEntities context = new VTODEntities())
			{
				if (context.taxi_trip.Where(x => x.DispatchTripId == DispatchTripId && x.ServiceAPIId == serviceAPIId).Any())
				{
					result = true;
				}
				else {
					result = false;
				}

			}


			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "IsVTODTaxiTrip");
			#endregion

			return result;
		}

		public bool ValidateMTDataStatusRequest(TokenRS tokenVTOD, OTA_GroundResRetrieveRQ request, out TaxiStatusParameter tsp, string MTDataToken)
		{
			bool result = true;
			Stopwatch sw = new Stopwatch();
			sw.Start();
			tsp = new TaxiStatusParameter();
			var MTDataBookingResult = new GetBookingResult();
			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion

			if (request.Reference != null)
			{
				#region Set token, request
				tsp.tokenVTOD = tokenVTOD;
				tsp.request = request;
				#endregion

				#region Validate Reference Id
				if (request.Reference == null || !request.Reference.Any() || string.IsNullOrWhiteSpace(request.Reference.First().ID))
				{
					result = false;
					logger.Warn("Reference.ID is empty.");
				}
				else
				{
					logger.Info("Request is valid.");
				}
				#endregion

				#region Validate Trip
				if (result)
				{
					using (BookingWebServiceSoapClient client = new BookingWebServiceSoapClient())
					{
						long id = Convert.ToInt64(request.Reference.Where(x => x.Type == "DispatchConfirmation").First().ID);
						MTDataBookingResult = client.GetBooking(MTDataToken, id);
						if (MTDataBookingResult != null)
						{
							logger.InfoFormat("Trip found. MTData tripID={0}", MTDataBookingResult.BookingSummary.BookingID);
						}
						else {
							logger.WarnFormat("Invalid MTData trip ID. ");
							result = false;

						}

					}
				}
				else
				{
					result = false;
					logger.Warn("Cannot find trip by invalid referenceId");
				}
				#endregion

				#region Validate Fleet
				if (result)
				{
					taxi_fleet fleet = null;
					if (GetFleet(request.TPA_Extensions.FleetId, out fleet))
					{
						tsp.Fleet = fleet;
						logger.InfoFormat("Fleet found. ID={0}", fleet.Id);

						if (fleet != null)
						{
							tsp.serviceAPIID = GetTaxiFleetServiceAPIPreference(fleet.Id, TaxiServiceAPIPreferenceConst.Status).ServiceAPIId;
                            #region Fetching Endpoint Names
                            List<string> endpointNames = MtdataEndpointConfiguration(tsp.serviceAPIID, tsp.Fleet.Id);
                            if (endpointNames != null && endpointNames.Any())
                            {
                                tsp.AuthenticationServiceEndpointName = endpointNames[0];
                                tsp.BookingWebServiceEndpointName = endpointNames[1];
                                tsp.OsiWebServiceEndpointName = endpointNames[2];
                                tsp.AddressWebServiceEndpointName = endpointNames[3];
                            }
                            #endregion
                        }
                    }
					else
					{

						result = false;
						logger.Warn("Unable to find fleet");
					}
				}
				else
				{
					result = false;
					logger.Warn("Unable to find fleet by this fleetID");
				}
				#endregion

				#region Validate Fleet User
				if (result)
				{
					taxi_fleet_user fleet_user = null;
					if (GetTaxiFleetUser(tsp.Fleet.Id, tokenVTOD.Username, out fleet_user))
					{
						tsp.Fleet_User = fleet_user;
						logger.Info("This user has sufficient privilege.");

						tsp.User = GetUser(fleet_user.UserId);
					}
					else
					{
						result = false;
						logger.WarnFormat("Insufficient privilege. User={0}, FleetId={1}", tokenVTOD.Username, tsp.Fleet.Id);
					}
				}
				else
				{
					result = false;
					logger.Warn("Cannot validate this user by invalid fleet ");
				}
				#endregion

				#region GetAllTripStatusHistory
				if (result)
				{
					if (MTDataBookingResult != null && MTDataBookingResult.BookingSummary != null)
					{
						//get information from GetBooking.Status
						using (VTODEntities context = new VTODEntities())
						{
							tsp.lastTripStatus = new taxi_trip_status { Status = ConvertTripStatusForMTData(MTDataBookingResult.BookingSummary.Status), VehicleNumber = MTDataBookingResult.BookingSummary.VehicleNumber, OriginalStatus = MTDataBookingResult.BookingSummary.Status.ToString() };
						}

					}
				}
				#endregion
			}
			else
			{
				result = false;
				logger.Info("Reference is null.");
			}

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);



			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateMTDataStatusRequest");
			#endregion

			return result;
		}

		public bool ValidateCCSiStatusRequest(CCSi_TaxiPushedStatus ccsiTps, out TaxiStatusParameter tsp)
		{
			bool result = true;
			Stopwatch sw = new Stopwatch();
			sw.Start();
			tsp = new TaxiStatusParameter();
			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion

			tsp.Trip = GetTaxiTripForCCSi(ccsiTps.DispatchTripID, ccsiTps.DispatchFleetID);
			if (tsp.Trip != null)
			{
				logger.InfoFormat("Trip ID={0}", tsp.Trip.Id);
				//string unknownStatus=string.Empty;
				//tsp.=ConvertTripStatusForCCSi(ccsiTps.EventID, ccsiTps.EventStatus, out unknownStatus);

				//get fleet
				tsp.Fleet = GetFleet(tsp.Trip.taxi_trip.FleetId);

				//get user
				tsp.User = GetUser(tsp.Trip.UserID);

				//tsp.tokenVTOD = token;

			}
			else
			{
				// trip not found 
				logger.WarnFormat("Unable to find this trip ID={0}", ccsiTps.DispatchTripID);
				result = false;
			}

			#region Create taxi log
			if (result)
			{
				if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
				{
					WriteTaxiLog(tsp.Trip.Id, tsp.serviceAPIID, TaxiServiceMethodType.Status, sw.ElapsedMilliseconds, string.Empty, string.Empty, null, null);
				}
			}
			else
			{
				result = false;
				logger.Warn("Cannot create or get this taxi log by invalid trip");
			}
			#endregion

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateCCSiStatusRequest");
			#endregion

			return result;
		}

		public bool ValidateMTDataCancelRequest(TokenRS tokenVTOD, OTA_GroundCancelRQ request, out TaxiCancelParameter tcp, string MTDataToken)
		{
			bool result = true;
			Stopwatch sw = new Stopwatch();
			sw.Start();
			tcp = new TaxiCancelParameter();
			var MTDataBookingResult = new GetBookingResult();
			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion

			#region Set token, request
			tcp.tokenVTOD = tokenVTOD;
			tcp.request = request;
			#endregion

			#region Validate Unique ID
			string uid = request.Reservation.UniqueID.FirstOrDefault().ID;
			if (request.Reservation.UniqueID.Any())
			{
				if (!string.IsNullOrWhiteSpace(uid))
				{
					logger.Info("Request is valid.");
				}
				else
				{
					result = false;
					logger.Info("request.Reservation.UniqueID.Frist().ID is empty.");
				}
			}
			else
			{
				result = false;
				logger.Info("Reservation.UniqueID is empty.");
			}
			#endregion

			#region Validate Trip
			if (result)
			{
				using (BookingWebServiceSoapClient client = new BookingWebServiceSoapClient())
				{
					long id = Convert.ToInt64(request.Reservation.UniqueID.Where(x => x.Type == "DispatchConfirmation").First().ID);
					MTDataBookingResult = client.GetBooking(MTDataToken, id);
					if (MTDataBookingResult != null && MTDataBookingResult.BookingSummary != null)
					{
						logger.InfoFormat("Trip found. MTData tripID={0}", MTDataBookingResult.BookingSummary.BookingID);
					}
					else
					{
						logger.WarnFormat("Invalid MTData trip ID. ");
						result = false;

					}

				}
			}
			else
			{
				result = false;
				logger.Warn("Cannot find trip by invalid referenceId");
			}
			#endregion


			#region Validate Fleet
			if (result)
			{
				taxi_fleet fleet = null;
				if (GetFleet(request.TPA_Extensions.FleetId, out fleet))
				{
					tcp.Fleet = fleet;
					logger.InfoFormat("Fleet found. ID={0}", fleet.Id);

					if (fleet != null)
					{
						tcp.serviceAPIID = GetTaxiFleetServiceAPIPreference(fleet.Id, TaxiServiceAPIPreferenceConst.Cancel).ServiceAPIId;
                        #region Fetching Endpoint Names
                        List<string> endpointNames = MtdataEndpointConfiguration(tcp.serviceAPIID, tcp.Fleet.Id);
                        if (endpointNames != null && endpointNames.Any())
                        {
                            tcp.AuthenticationServiceEndpointName = endpointNames[0];
                            tcp.BookingWebServiceEndpointName = endpointNames[1];
                            tcp.OsiWebServiceEndpointName = endpointNames[2];
                            tcp.AddressWebServiceEndpointName = endpointNames[3];
                        }
                        #endregion
                    }
                }
				else
				{

					result = false;
					logger.Warn("Unable to find fleet");
				}
			}
			else
			{
				result = false;
				logger.Warn("Unable to find fleet by this fleetID");
			}
			#endregion

			#region GetAllTripStatusHistory
			if (result)
			{
				if (MTDataBookingResult != null && MTDataBookingResult.BookingSummary != null && tcp.Trip != null)
				{
					//get information from GetBooking.Status
					using (VTODEntities context = new VTODEntities())
					{
						tcp.Trip.FinalStatus = ConvertTripStatusForMTData(MTDataBookingResult.BookingSummary.Status);
					}

				}
			}
			#endregion

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateCancelRequest");
			#endregion

			return result;
		}


		public bool ValidateCancelRequest(TokenRS tokenVTOD, OTA_GroundCancelRQ request, out TaxiCancelParameter tcp)
		{
			bool result = true;
			Stopwatch sw = new Stopwatch();
			sw.Start();
			tcp = new TaxiCancelParameter();

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion

			#region Set token, request
			tcp.tokenVTOD = tokenVTOD;
			tcp.request = request;
			#endregion

			#region Validate Unique ID
			string uid = request.Reservation.UniqueID.FirstOrDefault().ID;
			if (request.Reservation.UniqueID.Any())
			{
				if (!string.IsNullOrWhiteSpace(uid))
				{
					logger.Info("Request is valid.");
				}
				else
				{
					result = false;
					logger.Info("request.Reservation.UniqueID.Frist().ID is empty.");
				}
			}
			else
			{
				result = false;
				logger.Info("Reservation.UniqueID is empty.");
			}
			#endregion

			#region Validate Trip
			if (result)
			{
				vtod_trip t = null;
				if (request.Reservation.UniqueID.Where(x => x.Type == "Confirmation").Any())
				{
					var r = request.Reservation.UniqueID.Where(x => x.Type == "Confirmation").FirstOrDefault();
					t = GetTaxiTrip(Convert.ToInt64(r.ID));
				}
				else
				{
					var r = request.Reservation.UniqueID.Where(x => x.Type == "DispatchConfirmation").FirstOrDefault();
                    string firstname = string.Empty;
                    string lastname = string.Empty;
                    string phonenumber = string.Empty;
                    if (request.Reservation != null && request.Reservation.Verification != null && request.Reservation.Verification.PersonName!=null)
                    {
                        firstname = request.Reservation.Verification.PersonName.GivenName;
                        lastname = request.Reservation.Verification.PersonName.Surname;
                    }
                    if (request.Reservation != null && request.Reservation.Verification != null && request.Reservation.Verification.TelephoneInfo != null)
                    {
                        phonenumber = request.Reservation.Verification.TelephoneInfo.PhoneNumber;
                    }
                    t = GetTaxiTripByDispatchConfirmationId(r.ID, firstname, lastname, phonenumber.CleanPhone());
				}
				if (t != null)
				{
					tcp.Trip = t;
					logger.InfoFormat("Trip found. vtod tripID={0}", uid);
				}
				else
				{
					#region find trip by dispatch trip ID, firstname and lastname or phonenumber

					string dispatchID = request.Reservation.UniqueID.FirstOrDefault().ID;
					string firstName = "";
					string lastName = "";
					string phoneNumber = "";
					taxi_trip taxiObj = null;


					if (request.Reservation.Verification.PersonName != null)
					{
						if (!string.IsNullOrWhiteSpace(request.Reservation.Verification.PersonName.GivenName))
						{
							firstName = request.Reservation.Verification.PersonName.GivenName.ToLower().ToString();
						}
						if (!string.IsNullOrWhiteSpace(request.Reservation.Verification.PersonName.Surname))
						{
							lastName = request.Reservation.Verification.PersonName.Surname.ToLower().ToString();
						}
					}
					if (request.Reservation.Verification.TelephoneInfo != null)
					{
						if (!string.IsNullOrWhiteSpace(request.Reservation.Verification.TelephoneInfo.PhoneNumber))
						{
							phoneNumber = request.Reservation.Verification.TelephoneInfo.PhoneNumber.CleanPhone();
						}
					}

					using (var db = new VTODEntities())
					{
						if (!string.IsNullOrWhiteSpace(phoneNumber))
						{
							taxiObj = db.taxi_trip.Where(p => p.DispatchTripId == dispatchID && p.PhoneNumber == phoneNumber).FirstOrDefault();
							if (taxiObj != null)
							{
								t = GetTaxiTrip(taxiObj.Id);
							}
						}


						if (taxiObj == null)
						{
							taxiObj = db.taxi_trip.Where(p => p.DispatchTripId == dispatchID && p.FirstName.ToLower().ToString() == firstName && p.LastName.ToLower().ToString() == lastName).FirstOrDefault();
							if (taxiObj != null)
							{
								t = GetTaxiTrip(taxiObj.Id);
							}
						}

						if (t != null)
						{
							tcp.Trip = t;
							logger.InfoFormat("Trip found. dispatch tripID={0}", request.Reservation.UniqueID.FirstOrDefault().ID);
						}
						else
						{
							result = false;
							logger.WarnFormat("Cannot find this trip. tripID={0}, type={1}", uid, request.Reservation.UniqueID.FirstOrDefault().Type);
						}

					}


					#endregion
				}
			}
			else
			{
				result = false;
				logger.Warn("Cannot find trip by invalid referenceId");
			}
			#endregion

			#region Validate Fleet
			if (result)
			{
				taxi_fleet fleet = null;
				if (GetFleet(tcp.Trip.taxi_trip.FleetId, out fleet))
				{
					tcp.Fleet = fleet;
					logger.InfoFormat("Fleet found. ID={0}", fleet.Id);

					if (fleet != null)
					{
						tcp.serviceAPIID = GetTaxiFleetServiceAPIPreference(fleet.Id, TaxiServiceAPIPreferenceConst.Cancel).ServiceAPIId;
                        #region Fetching Endpoint Names
                        List<string> endpointNames = MtdataEndpointConfiguration(tcp.serviceAPIID, tcp.Fleet.Id);
                        if (endpointNames != null && endpointNames.Any())
                        {
                            tcp.AuthenticationServiceEndpointName = endpointNames[0];
                            tcp.BookingWebServiceEndpointName = endpointNames[1];
                            tcp.OsiWebServiceEndpointName = endpointNames[2];
                            tcp.AddressWebServiceEndpointName = endpointNames[3];
                        }
                        #endregion
                    }
                }
				else
				{
					result = false;
					logger.Warn("Unable to find fleet by this fleetID");
				}
			}
			else
			{
				result = false;
				logger.Warn("Unable to find fleet by this fleetID");
			}
			#endregion

			#region Validate User
			if (result)
			{
				taxi_fleet_user user = null;
				if (GetTaxiFleetUser(tcp.Trip.taxi_trip.FleetId, tokenVTOD.Username, out user))
				{
					tcp.User = user;
					logger.Info("This user has sufficient privilege.");
				}
				else
				{
					result = false;
					logger.WarnFormat("Insufficient privilege. User={0}, FleetId={1}", tokenVTOD.Username, tcp.Fleet.Id);
				}
			}
			else
			{
				result = false;
				logger.Warn("Cannot validate this user by invalid fleet ");
			}
			#endregion

			#region Validate customer
			if (!result)
			{
                logger.Warn("Cannot find customer by Invalid trip");
            }			
			#endregion

			#region  Validate trip status
			//need to be added (michael)
			//if (result && (IsAbleToCancelThisTrip(tcp.Trip.FinalStatus)))
			//{
			//	logger.InfoFormat("This trip can be canceled. status={0}", tcp.Trip.FinalStatus);
			//}
			//else {
			//	logger.WarnFormat("This trip can not be canceled. status={0}", tcp.Trip.FinalStatus);
			//	result = false;
			//}
			#endregion

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Create taxi log
			if (result && tcp.Trip != null)
			{
				if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
				{
					WriteTaxiLog(tcp.Trip.Id, tcp.serviceAPIID, TaxiServiceMethodType.Cancel, sw.ElapsedMilliseconds, string.Empty, string.Empty, null, null);
				}
			}
			else
			{
				result = false;
				logger.Warn("Cannot create or get this taxi log by invalid trip");
			}
			#endregion

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateCancelRequest");
			#endregion

			return result;
		}

		public bool ValidateGetVehicleInfoRequest(TokenRS tokenVTOD, OTA_GroundAvailRQ request, out TaxiGetVehicleInfoParameter tgvip)
		{
			bool result = true;
			Stopwatch sw = new Stopwatch();
			sw.Start();
			tgvip = new TaxiGetVehicleInfoParameter();


			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion

			#region Set token, request
			tgvip.tokenVTOD = tokenVTOD;
			tgvip.request = request;
			#endregion


			if (request.Service != null)
			{
				if (request.Service.Pickup != null)
				{
					#region Validate longtitude, latitude
					if (request.Service.Pickup.Address != null)
					{
						if (!string.IsNullOrWhiteSpace(request.Service.Pickup.Address.Latitude) && !string.IsNullOrWhiteSpace(request.Service.Pickup.Address.Longitude))
						{
							double longtitue = 0;
							double latitude = 0;
							if (double.TryParse(request.Service.Pickup.Address.Longitude, out longtitue) && double.TryParse(request.Service.Pickup.Address.Latitude, out latitude))
							{
								tgvip.Longtitude = longtitue;
								tgvip.Latitude = latitude;
							}
							else
							{
								result = false;
								logger.Warn("Invalid longtitude and latittude");
							}
						}
						else
						{
							result = false;
							logger.Warn("Invalid longtitude and latittude");
						}
					}
					else
					{
						result = false;
						logger.Warn("Cannot find any longtitude or latitude from address");
					}
					#endregion

					#region Validate Pickup Address
					if (result)
					{
						Pickup_Dropoff_Stop stop = request.Service.Pickup;

						if ((stop.Address != null) && (stop.AirportInfo == null))
						{
							if (!string.IsNullOrWhiteSpace(stop.Address.CountryName.Code) && !string.IsNullOrWhiteSpace(stop.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(stop.Address.CityName) && !string.IsNullOrWhiteSpace(stop.Address.PostalCode))
							{
								Map.DTO.Address pickupAddress = new Map.DTO.Address { Country = stop.Address.CountryName.Code, State = stop.Address.StateProv.StateCode, City = stop.Address.CityName, ZipCode = stop.Address.PostalCode, Geolocation = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(stop.Address.Latitude), Longitude = Convert.ToDecimal(stop.Address.Longitude) } };
								tgvip.PickupAddressType = UDI.VTOD.Domain.Taxi.Const.AddressType.Address;
								tgvip.PickupAddress = pickupAddress;
							}
							else if (!string.IsNullOrWhiteSpace(stop.Address.CountryName.Code) && !string.IsNullOrWhiteSpace(stop.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(stop.Address.CityName))
							{
								Map.DTO.Address pickupAddress = new Map.DTO.Address { Country = stop.Address.CountryName.Code, State = stop.Address.StateProv.StateCode, City = stop.Address.CityName, Geolocation = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(stop.Address.Latitude), Longitude = Convert.ToDecimal(stop.Address.Longitude) } };
								tgvip.PickupAddressType = UDI.VTOD.Domain.Taxi.Const.AddressType.Address;
								tgvip.PickupAddress = pickupAddress;

							}
							else if (!string.IsNullOrWhiteSpace(stop.Address.LocationName))
							{
								#region Not support now
								//tgvip.PickupAddressType = UDI.VTOD.Domain.Taxi.Const.AddressType.LocationName;
								//tgvip.PickupLandmark = stop.Address.LocationName;
								#endregion

								result = false;
								logger.Warn("Cannot use locationName from address.");
							}
							else
							{
								result = false;
								logger.Warn("Cannot find any address/locationName from address.");
							}
						}
						else if ((stop.Address == null) && (stop.AirportInfo != null))
						{
							if ((stop.AirportInfo.Arrival != null) && (stop.AirportInfo.Departure == null))
							{
								#region Not support now
								//tgvip.PickupAddressType = UDI.VTOD.Domain.Taxi.Const.AddressType.AirportInfo;
								//tgvip.PickupLandmark = stop.AirportInfo.Arrival.LocationCode;
								#endregion

								result = false;
								logger.Warn("Cannot use aiportinfo for address");
							}
							else if ((stop.AirportInfo.Arrival == null) && (stop.AirportInfo.Departure != null))
							{
								#region Not support now
								//tgvip.PickupAddressType = UDI.VTOD.Domain.Taxi.Const.AddressType.AirportInfo;
								//tgvip.PickupLandmark = stop.AirportInfo.Departure.LocationCode;
								#endregion

								result = false;
								logger.Warn("Cannot use aiportinfo for address");
							}
							else if ((stop.AirportInfo.Arrival != null) && (stop.AirportInfo.Departure != null))
							{
								result = false;
								logger.Warn("This OTA request contains both AirportInfo.Arrival and AirportInfo.Departure");
							}
							else
							{
								result = false;
								logger.Warn("Cannot find any arrival aor depature from AirportInfo");
							}
						}
						else
						{
							//find address by longtitude and latitude
							Map.DTO.Address pickupAddress = null;
							if (!LookupAddress(tgvip.Longtitude, tgvip.Latitude, out pickupAddress))
							{
								logger.WarnFormat("Cannot find longtitude={0} and latitude={1}", tgvip.Longtitude, tgvip.Latitude);
								result = false;
							}

							tgvip.PickupAddressType = UDI.VTOD.Domain.Taxi.Const.AddressType.Address;
							tgvip.PickupAddress = pickupAddress;
						}
					}
					else
					{
						result = false;
					}
					#endregion

					#region Validate Fleet
					if (result)
					{
						taxi_fleet fleet = null;
						if (GetFleet(tgvip.PickupAddressType, tgvip.PickupAddress, tgvip.PickupLandmark, out fleet))
						{
							tgvip.Fleet = fleet;
							if (tgvip.Fleet.UDI33DNI != null)
							{
								tgvip.APIInformation = tgvip.Fleet.UDI33DNI.ToString();
							}
							else if ((tgvip.Fleet.CCSiFleetId != null) && (tgvip.Fleet.CCSiSource != null))
							{
								tgvip.APIInformation = string.Format("{0}_{1}", tgvip.Fleet.CCSiSource, tgvip.Fleet.CCSiFleetId);
							}
							logger.InfoFormat("Fleet found. ID={0}", fleet.Id);

							if (fleet != null)
							{
								var serviceAPIPreference = GetTaxiFleetServiceAPIPreference(fleet.Id, TaxiServiceAPIPreferenceConst.GetVehicleInfo);
								tgvip.serviceAPIID = serviceAPIPreference.ServiceAPIId;
								tgvip.extendedServiceAPIID = serviceAPIPreference.ExtendedServiceAPIId;
                                #region Fetching Endpoint Names
                                List<string> endpointNames = MtdataEndpointConfiguration(tgvip.serviceAPIID, tgvip.Fleet.Id);
                                if (endpointNames != null && endpointNames.Any())
                                {
                                    tgvip.AuthenticationServiceEndpointName = endpointNames[0];
                                    tgvip.BookingWebServiceEndpointName = endpointNames[1];
                                    tgvip.OsiWebServiceEndpointName = endpointNames[2];
                                    tgvip.AddressWebServiceEndpointName = endpointNames[3];
                                }
                                #endregion
                            }
                        }
						else
						{
							result = false;
							logger.Warn("Unable to find fleet by this address or landmark.");
						}
					}
					else
					{
						result = false;
						logger.Warn("Unable to find fleet by invalid address or landmark.");
					}
					#endregion

					#region Validate Zone
					if (result)
					{
						//Default: Circle
						if (request.Service.Pickup.Address.TPA_Extensions != null)
						{
							#region Validate Polygon Type
							if (request.Service.Pickup.Address.TPA_Extensions.Zone != null)
							{
								if (request.Service.Pickup.Address.TPA_Extensions.Zone.Circle != null)
								{
									tgvip.Radius = double.Parse(request.Service.Pickup.Address.TPA_Extensions.Zone.Circle.Radius.ToString());
									tgvip.PolygonType = PolygonTypeConst.Circle;
								}

								if (string.IsNullOrWhiteSpace(tgvip.PolygonType))
								{
									result = false;
									logger.Warn("Unable to find the polygon area for GetVehicleInfo");
								}
							}
							else
							{
								result = false;
								logger.Warn("request.TPA_Extensions.Zone");
							}
							#endregion

							#region Validate Vehicle status
							if (request.Service.Pickup.Address.TPA_Extensions.Vehicles != null)
							{
								if (request.Service.Pickup.Address.TPA_Extensions.Vehicles.Items.Any())
								{
									//tgvip.VehicleStausList = request.Service.Pickup.Address.TPA_Extensions.Vehicles.Items;
									if (!"all".Equals(request.Service.Pickup.Address.TPA_Extensions.Vehicles.Items.FirstOrDefault().VehicleStatus.ToLower()))
									{
										Vehicle v = new Vehicle { VehicleStatus = request.Service.Pickup.Address.TPA_Extensions.Vehicles.Items.FirstOrDefault().VehicleStatus.ToString() };
										tgvip.VehicleStausList.Add(v);
									}
									else
									{
										logger.Info("Vehicle status = ALL");
									}
								}
								else
								{
									result = false;
									logger.Warn("request.Service.Pickup.Address.TPA_Extensions.Vehicles.Items is empty");
								}

							}
							else
							{
								result = false;
								logger.Info("Unable to find request.TPA_Extensions.Vehicles");
							}

							#endregion
						}
						else
						{
							result = false;
							logger.Warn("Unable to find request.TPA_Extensions");
						}
					}
					else
					{
						result = false;
						logger.Warn("Cannot get polygon type by invalid request");
					}
                    #endregion

                    //#region GetVehicleStatus
                    //if (result)
                    //{
                    //    if (request.Service.Pickup.Address.TPA_Extensions != null)
                    //    {
                    //        if (request.Service.Pickup.Address.TPA_Extensions.Vehicles != null)
                    //        {
                    //            if (request.Service.Pickup.Address.TPA_Extensions.Vehicles.Items.Any())
                    //            {
                    //                tgvip.VehicleStatus=request.Service.Pickup.Address.TPA_Extensions.Vehicles.Items.FirstOrDefault().VehicleStatus;
                    //            }
                    //            else {
                    //                tgvip.VehicleStatus = "";
                    //            }
                    //        }
                    //        else {
                    //            tgvip.VehicleStatus = "";
                    //        }
                    //    }
                    //    else {
                    //        tgvip.VehicleStatus = "";
                    //    }
                    //}
                    //else {
                    //    result = false;
                    //    logger.Warn("Cannot get vehicle status by invalid request");
                    //}
                    //#endregion
                  

                }
				else
				{
					result = false;
					logger.Warn("request.Service.Pickup is null");
				}
			}
			else
			{
				result = false;
				logger.Warn("request.Service is null");
			}

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);


			#region Create taxi log
			if (result)
			{
				if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
				{
					WriteTaxiLog(null, tgvip.serviceAPIID, TaxiServiceMethodType.GetVehicleInfo, sw.ElapsedMilliseconds, string.Empty, string.Empty, null, null);
				}
			}
			else
			{
				result = false;
				logger.Warn("Cannot create TaxiLog by invalid request");
			}
			#endregion

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateGetVehicleInfoRequest");
			#endregion

			return result;
		}

		public bool ValidateGetEstimationRequest(TokenRS tokenVTOD, OTA_GroundAvailRQ reservation, out TaxiGetEstimationParameter tgep)
		{
			bool result = true;
			Stopwatch sw = new Stopwatch();
			sw.Start();
			tgep = new TaxiGetEstimationParameter();

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion


			if (reservation.Service != null)
			{

				#region Token
				tgep.tokenVTOD = tokenVTOD;
				#endregion

				#region Validate Pickup address
				if (reservation.Service.Pickup != null)
				{
					if (((reservation.Service.Pickup.Address != null) && (reservation.Service.Pickup.AirportInfo != null)) || ((reservation.Service.Pickup.Address == null) && (reservation.Service.Pickup.AirportInfo == null)))
					{
						result = false;
						logger.Warn(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
						throw new ValidationException(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
					}
					else if (reservation.Service.Pickup.Address != null && reservation.Service.Pickup.AirportInfo == null)
					{
						if ((string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.Longitude) || (string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.CountryName.Code)))
						{
							result = false;
							logger.Warn(Messages.Validation_LocationDetail);
							throw new ValidationException(Messages.Validation_LocationDetail);
						}
						else if ((!string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.Longitude) && (!string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.CountryName.Code)))
						{
							tgep.PickupAddressOnlyContainsLatAndLong = true;
							MapService ms = new MapService();
							Map.DTO.Geolocation g = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(reservation.Service.Pickup.Address.Latitude), Longitude = Convert.ToDecimal(reservation.Service.Pickup.Address.Longitude) };
							string error = "";
							var addressList = ms.GetAddressesFromGeoLocationWithProximity(g, out error);

							if (string.IsNullOrWhiteSpace(error) && addressList.Any())
							{
								var add = addressList.FirstOrDefault();

								reservation.Service.Pickup.Address.StreetNmbr = add.StreetNo;
								reservation.Service.Pickup.Address.AddressLine = add.Street;
								reservation.Service.Pickup.Address.BldgRoom = add.ApartmentNo;
								reservation.Service.Pickup.Address.CityName = add.City;
								reservation.Service.Pickup.Address.PostalCode = add.ZipCode;
								reservation.Service.Pickup.Address.StateProv = new StateProv { StateCode = add.State };
								reservation.Service.Pickup.Address.CountryName = new CountryName { Code = add.Country };
								reservation.Service.Pickup.Address.StreetNmbr = add.StreetNo;
								reservation.Service.Pickup.Address.StreetNmbr = add.StreetNo;

								tgep.PickupAddressType = UDI.VTOD.Domain.Taxi.Const.AddressType.Address;
								tgep.PickupAddress = ConvertAddress(reservation.Service.Pickup);
								logger.Info("Pickup AddressType: Address");

							}
							else
							{
								result = false;
								logger.Warn(Messages.Validation_LocationDetail);
								throw new ValidationException(Messages.Validation_LocationDetail);
							}



						}
						else if (!string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.AddressLine) && !string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.CityName) && !string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.PostalCode) && !string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.CountryName.Code))
						{
							//Address
							Map.DTO.Address pickupAddress = null;
							string pickupAddressStr = GetAddressString(reservation.Service.Pickup.Address);
							//if (reservation.Service.Pickup.Address.TPA_Extensions != null)
							//{
							//	if (reservation.Service.Pickup.Address.TPA_Extensions.AddressValidationRequired.HasValue ? reservation.Service.Pickup.Address.TPA_Extensions.AddressValidationRequired.Value : false)
							//	{
							//		if (!LookupAddress(reservation.Service.Pickup, pickupAddressStr, out pickupAddress))
							//		{
							//			result = false;
							//			logger.WarnFormat(Messages.Validation_LocationDetailByMapApi);
							//			throw new ValidationException(Messages.Validation_LocationDetailByMapApi);
							//		}
							//	}
							//	else
							//	{
							//		if (!string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.Longitude) && !string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.Latitude))
							//		{
							//			logger.Info("Skip MAP API Validation.");
							//			pickupAddress = ConvertAddress(reservation.Service.Pickup);
							//		}
							//		else
							//		{
							//			result = false;
							//			logger.WarnFormat(Messages.Validation_LocationNoLatlon);
							//			throw new ValidationException(Messages.Validation_LocationNoLatlon);
							//		}
							//	}
							//}
							//else
							//{
							logger.Info("Skip MAP API Validation.");
							pickupAddress = ConvertAddress(reservation.Service.Pickup);
							//}
							tgep.PickupAddressType = UDI.VTOD.Domain.Taxi.Const.AddressType.Address;
							tgep.PickupAddress = pickupAddress;
							logger.Info("Pickup AddressType: Address");
						}
						else
						{
							result = false;
							var vtodException = VtodException.CreateException(ExceptionType.Taxi, 1);
							logger.Warn(vtodException.ExceptionMessage.Message);
							throw vtodException;
						}
					}
					else if ((reservation.Service.Pickup.Address == null) && (reservation.Service.Pickup.AirportInfo != null))
					{
						//airport
						if (((reservation.Service.Pickup.AirportInfo.Arrival != null) && (reservation.Service.Pickup.AirportInfo.Departure != null)) || ((reservation.Service.Pickup.AirportInfo.Arrival == null) && (reservation.Service.Pickup.AirportInfo.Departure == null)))
						{
							result = false;
							logger.Warn(Messages.Validation_ArrivalAndDepartureAirportInfo);
							throw VtodException.CreateValidationException(Messages.Validation_ArrivalAndDepartureAirportInfo);
						}
						else if (reservation.Service.Pickup.AirportInfo.Arrival != null && reservation.Service.Pickup.AirportInfo.Departure == null)
						{
							//Arrival
							tgep.PickupAddressType = AddressType.Airport;
							tgep.PickupAirport = reservation.Service.Pickup.AirportInfo.Arrival.LocationCode;
							logger.Info("Pickup AddressType: AirportInfo.Arrival");
						}
						else if (reservation.Service.Pickup.AirportInfo.Arrival == null && reservation.Service.Pickup.AirportInfo.Departure != null)
						{
							//Depature

							tgep.PickupAddressType = AddressType.Airport;
							tgep.PickupAirport = reservation.Service.Pickup.AirportInfo.Departure.LocationCode;
							logger.Info("Pickup AddressType: AirportInfo.Depature");
						}

						//longitude and latitude
						if (result)
						{
							double? longitude = null;
							double? latitude = null;
							if (GetLongitudeAndLatitudeForAirport(tgep.PickupAirport, out longitude, out latitude))
							{
								tgep.LongitudeForPickupAirport = longitude.Value;
								tgep.LatitudeForPickupAirport = latitude.Value;
							}
							else
							{
								throw VtodException.CreateException(ExceptionType.Taxi, 1010);// new ValidationException(Messages.Taxi_Common_UnableToFindAirportLongitudeAndLatitude);
							}
						}

						//throw new ValidationException(Messages.Taxi_Common_InvalidAirportPickup);
					}


				}
				else
				{
					result = false;
					logger.Warn("request.Service.Pickup is null");
				}

				#endregion

				#region Validate Pickup Fleet
				if (result)
				{
					taxi_fleet fleet = null;
					if (GetFleet(tgep.PickupAddressType, tgep.PickupAddress, tgep.PickupAirport, out fleet))
					{
						tgep.Fleet = fleet;
						logger.InfoFormat("Fleet found. ID={0}", fleet.Id);
					}
					else
					{
						result = false;
						logger.Warn("Unable to find fleet by this address or landmark.");
					}
				}
				else
				{
					result = false;
					logger.Warn("Unable to find fleet by invalid address or landmark.");
				}
				#endregion

				#region Get Rate and FlatRate
				if (result)
				{
					using (VTODEntities context = new VTODEntities())
					{
						long pickupfleetID = tgep.Fleet.Id;
						//find Info
						//taxi_rate
						tgep.Info_Rate = context.taxi_fleet_rate.Where(x => x.FleetId == pickupfleetID).Select(x => x).FirstOrDefault();

						//List<taxi_flatrate>

						//if (context.taxi_fleet_zone_flatrate.Where(x => x.FleetId == pickupfleetID).Any())
						List<taxi_fleet_zone_flatrate> tfzfList = context.taxi_fleet_zone_flatrate.Where(x => x.PickupZone.FleetId == pickupfleetID).Select(x => x).ToList();

						if (tfzfList.Any())
						{
							tgep.Info_FlatRates = new List<TaxiFlatRateData>();
							//foreach (taxi_fleet_flatrate x in context.taxi_fleet_zone_flatrate.Where(x => x.FleetId == pickupfleetID).Select(x => x).ToList())
							foreach (taxi_fleet_zone_flatrate x in tfzfList)
							{
								TaxiFlatRateData tfrd = new TaxiFlatRateData { Amount = x.Amount, AmountUnit = x.AmountUnit, AppendTime = x.AppendTime, DropoffZoneId = x.DropoffZoneId, Id = x.Id, PickupZoneId = x.PickupZoneId };
								tfrd.PickupZoneName = context.taxi_fleet_zone.Where(xx => xx.Id == tfrd.PickupZoneId).Select(xxx => xxx.Name).FirstOrDefault();
								tfrd.DropOffZoneName = context.taxi_fleet_zone.Where(xx => xx.Id == tfrd.DropoffZoneId).Select(xxx => xxx.Name).FirstOrDefault();
								tgep.Info_FlatRates.Add(tfrd);
							}
						}
					}
				}

				#endregion

				#region Validate Drop off address
				if (result)
				{
					//drop off address can be address or airportInfo
					if (reservation.Service.Dropoff != null)
					{
						using (VTODEntities context = new VTODEntities())
						{
							if (((reservation.Service.Dropoff.Address != null) && (reservation.Service.Dropoff.AirportInfo != null)) || ((reservation.Service.Dropoff.Address == null) && (reservation.Service.Dropoff.AirportInfo == null)))
							{
								result = false;
								logger.Warn(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
								throw VtodException.CreateValidationException(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
							}
							else if ((reservation.Service.Dropoff.Address != null) && (reservation.Service.Dropoff.AirportInfo == null))
							{
								//Address
								if ((string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.Longitude) || (string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.CountryName.Code)))
								{
									result = false;
									logger.Warn(Messages.Validation_LocationDetail);
									throw VtodException.CreateValidationException(Messages.Validation_LocationDetail);
								}
								else if ((!string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.Longitude) && (!string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.CountryName.Code)))
								{
									tgep.DropOffAddressOnlyContainsLatAndLong = true;
									MapService ms = new MapService();
									Map.DTO.Geolocation g = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(reservation.Service.Dropoff.Address.Latitude), Longitude = Convert.ToDecimal(reservation.Service.Dropoff.Address.Longitude) };
									string error = "";
									var addressList = ms.GetAddressesFromGeoLocationWithProximity(g, out error);

									if (string.IsNullOrWhiteSpace(error) && addressList.Any())
									{
										var add = addressList.FirstOrDefault();

										reservation.Service.Dropoff.Address.StreetNmbr = add.StreetNo;
										reservation.Service.Dropoff.Address.AddressLine = add.Street;
										reservation.Service.Dropoff.Address.BldgRoom = add.ApartmentNo;
										reservation.Service.Dropoff.Address.CityName = add.City;
										reservation.Service.Dropoff.Address.PostalCode = add.ZipCode;
										reservation.Service.Dropoff.Address.StateProv = new StateProv { StateCode = add.State };
										reservation.Service.Dropoff.Address.CountryName = new CountryName { Code = add.Country };
										reservation.Service.Dropoff.Address.StreetNmbr = add.StreetNo;
										reservation.Service.Dropoff.Address.StreetNmbr = add.StreetNo;

										tgep.DropOffAddressType = UDI.VTOD.Domain.Taxi.Const.AddressType.Address;
										tgep.DropoffAddress = ConvertAddress(reservation.Service.Dropoff);
										logger.Info("Dropoff AddressType: Address");

									}
									else
									{
										result = false;
										logger.Warn(Messages.Validation_LocationDetail);
										throw new ValidationException(Messages.Validation_LocationDetail);
									}



								}
								else if (!string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.AddressLine) && !string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.CityName) && !string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.PostalCode) && !string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.CountryName.Code))
								{
									Map.DTO.Address dropoffAddress = null;
									string dropoffAddressStr = GetAddressString(reservation.Service.Dropoff.Address);
									//if (reservation.Service.Dropoff.Address.TPA_Extensions != null)
									//{
									//	if (reservation.Service.Dropoff.Address.TPA_Extensions.AddressValidationRequired.HasValue ? reservation.Service.Dropoff.Address.TPA_Extensions.AddressValidationRequired.Value : false)
									//	{
									//		if (!LookupAddress(reservation.Service.Dropoff, dropoffAddressStr, out dropoffAddress))
									//		{
									//			result = false;
									//			logger.WarnFormat(Messages.Validation_LocationDetailByMapApi);
									//			throw VtodException.CreateValidationException(Messages.Validation_LocationDetailByMapApi);
									//		}
									//	}
									//	else
									//	{
									//		if (!string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.Longitude) && !string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.Latitude))
									//		{
									//			logger.Info("Skip MAP API Validation.");
									//			dropoffAddress = ConvertAddress(reservation.Service.Dropoff);
									//		}
									//		else
									//		{
									//			result = false;
									//			logger.WarnFormat(Messages.Validation_LocationNoLatlon);
									//			throw new ValidationException(Messages.Validation_LocationNoLatlon);
									//		}
									//	}
									//}
									//else
									//{
									logger.Info("Skip MAP API Validation.");
									dropoffAddress = ConvertAddress(reservation.Service.Dropoff);
									//}

									tgep.DropoffAddress = dropoffAddress;
									tgep.DropOffAddressType = UDI.VTOD.Domain.Taxi.Const.AddressType.Address;
									logger.Info("Dropoff AddressType=Address");
								}
								else
								{
									result = false;
									var vtodException = VtodException.CreateException(ExceptionType.Taxi, 1);
									logger.Warn(vtodException.ExceptionMessage.Message);
									throw vtodException;
								}
							}
							else if ((reservation.Service.Dropoff.Address == null) && (reservation.Service.Dropoff.AirportInfo != null))
							{

								//airport
								if (((reservation.Service.Dropoff.AirportInfo.Arrival != null) && (reservation.Service.Dropoff.AirportInfo.Departure != null)) || ((reservation.Service.Dropoff.AirportInfo.Arrival == null) && (reservation.Service.Dropoff.AirportInfo.Departure == null)))
								{
									result = false;
									logger.Warn(Messages.Validation_ArrivalAndDepartureAirportInfo);
									throw VtodException.CreateValidationException(Messages.Validation_ArrivalAndDepartureAirportInfo);
								}
								else if (reservation.Service.Dropoff.AirportInfo.Arrival != null && reservation.Service.Dropoff.AirportInfo.Departure == null)
								{
									//Arrival
									tgep.DropOffAddressType = AddressType.Airport;
									tgep.DropoffAirport = reservation.Service.Dropoff.AirportInfo.Arrival.LocationCode;
									logger.Info("Dropoff AddressType: AirportInfo.Arrival");
								}
								else if (reservation.Service.Dropoff.AirportInfo.Arrival == null && reservation.Service.Dropoff.AirportInfo.Departure != null)
								{
									//Depature
									tgep.DropOffAddressType = AddressType.Airport;
									tgep.DropoffAirport = reservation.Service.Dropoff.AirportInfo.Departure.LocationCode;
									logger.Info("Dropoff AddressType: AirportInfo.Depature");
								}

								//longitude and latitude
								if (result)
								{
									double? longitude = null;
									double? latitude = null;
									if (GetLongitudeAndLatitudeForAirport(tgep.DropoffAirport, out longitude, out latitude))
									{
										tgep.LongitudeForDropoffAirport = longitude.Value;
										tgep.LatitudeForDropoffAirport = latitude.Value;
									}
									else
									{
										throw VtodException.CreateException(ExceptionType.Taxi, 1010);// new ValidationException(Messages.Taxi_Common_UnableToFindAirportLongitudeAndLatitude);
									}
								}
							}
						}
					}
					//	else
					//	{
					//		tgep.DropoffAddressType = AddressType.Empty;
					//		logger.Info(Messages.Taxi_Common_InvalidReservationServiceLocationDropoff);
					//		throw new ValidationException(Messages.Taxi_Common_InvalidReservationServiceLocationDropoff);
					//	}
				}
				else
				{
					result = false;
					logger.Warn("Cannot get estimation by previous invalid request ");
				}
				#endregion

				#region Validate Available address type
				if (result)
				{
					//string pickupAddressType = tgep.PickupAddressType;
					//string dropoffAddressType = tgep.DropOffAddressType;

					//verify the address type of pickup address and drop off address
					UtilityDomain ud = new UtilityDomain();
					if (tgep.PickupAddressType == AddressType.Address.ToString())
					{
						UtilityGetAirportNameRQ uganRQ = new UtilityGetAirportNameRQ();
						uganRQ.Address = new Address();
						//tgep.PickupAddress.Geolocation = new Map.DTO.Geolocation();
						uganRQ.Address.Latitude = tgep.PickupAddress.Geolocation.Latitude.ToString();
						uganRQ.Address.Longitude = tgep.PickupAddress.Geolocation.Longitude.ToString();

						UtilityGetAirportNameRS uganRS = ud.GetAirportName(tokenVTOD, uganRQ);
						if (uganRS.Success != null)
						{
							if (!string.IsNullOrWhiteSpace(uganRS.AirportName))
							{
								//pickupAddressType = AddressType.Airport;

								tgep.PickupAddressType = AddressType.Airport;
								tgep.PickupAirport = uganRS.AirportName;
								tgep.LongitudeForPickupAirport = uganRQ.Address.Longitude.ToDouble();
								tgep.LatitudeForPickupAirport = uganRQ.Address.Longitude.ToDouble();
							}
						}

					}

					if (tgep.DropOffAddressType == AddressType.Address.ToString())
					{
						UtilityGetAirportNameRQ uganRQ = new UtilityGetAirportNameRQ();
						uganRQ.Address = new Address();
						//tgep.DropoffAddress.Geolocation = new Map.DTO.Geolocation();
						uganRQ.Address.Latitude = tgep.DropoffAddress.Geolocation.Latitude.ToString();
						uganRQ.Address.Longitude = tgep.DropoffAddress.Geolocation.Longitude.ToString();

						UtilityGetAirportNameRS uganRS = ud.GetAirportName(tokenVTOD, uganRQ);
						if (uganRS.Success != null)
						{
							if (!string.IsNullOrWhiteSpace(uganRS.AirportName))
							{
								//dropoffAddressType = AddressType.Airport;
								tgep.DropOffAddressType = AddressType.Airport;
								tgep.DropoffAirport = uganRS.AirportName;
								tgep.LongitudeForDropoffAirport = uganRQ.Address.Longitude.ToDouble();
								tgep.LatitudeForDropoffAirport = uganRQ.Address.Longitude.ToDouble();
							}
						}
					}

					//check database setting
					//1. get taxi_fleet_availableaddresstype
					using (VTODEntities context = new VTODEntities())
					{
						long fleetId = tgep.Fleet.Id;
						if (context.taxi_fleet_availableaddresstype.Where(x => fleetId == x.FleetId).Any())
						{
							taxi_fleet_availableaddresstype tfa = context.taxi_fleet_availableaddresstype.Where(x => fleetId == x.FleetId).Select(x => x).FirstOrDefault();
							//2. check if the type is available or not
							//Taxi_Common_InvalidAddressTypeForFleet
							if (AddressType.Address.ToString() == tgep.PickupAddressType && AddressType.Address.ToString() == tgep.DropOffAddressType)
							{
								//address to address
								if (tfa.AddressToAddress)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.Taxi, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
							else if (AddressType.Address.ToString() == tgep.PickupAddressType && AddressType.Airport.ToString() == tgep.DropOffAddressType)
							{
								//address to airport
								if (tfa.AddressToAirport)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.Taxi, 2020); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
							else if (AddressType.Address.ToString() == tgep.PickupAddressType && null == tgep.DropOffAddressType)
							{
								//address to null
								if (tfa.AddressToNull)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.Taxi, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
							else if (AddressType.Airport.ToString() == tgep.PickupAddressType && AddressType.Address.ToString() == tgep.DropOffAddressType)
							{
								//airport to address
								if (tfa.AirportToAddress)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.Taxi, 2019); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
							else if (AddressType.Airport.ToString() == tgep.PickupAddressType && AddressType.Airport.ToString() == tgep.DropOffAddressType)
							{
								//airport to airport
								if (tfa.AirportToAirport)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.Taxi, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
							else if (AddressType.Airport.ToString() == tgep.PickupAddressType && null == tgep.DropOffAddressType)
							{
								//airport to null
								if (tfa.AirportToNull)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.Taxi, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
						}
						else
						{
							result = false;
							throw VtodException.CreateException(ExceptionType.Taxi, 1011);// VtodException.CreateException(ExceptionType.Taxi, 1011);// new ValidationException(Messages.Taxi_Common_UnableToGetAvailableFleets);
						}
					}


				}
				else
				{
					result = false;
					var vtodException = Common.DTO.VtodException.CreateFieldRequiredValidationException("Payment");
					logger.Warn(vtodException.ExceptionMessage.Message);
					throw vtodException; // new ValidationException(Messages.Validation_NullPayment);
				}
				#endregion

				#region Pickup zone and dropoff zone
				if (result)
				{
					//pickup zone
					if ((tgep.PickupAddressType == AddressType.Address && tgep.PickupAddress.Geolocation != null && tgep.PickupAddress.Geolocation.Latitude != 0 && tgep.PickupAddress.Geolocation.Longitude != 0)
						||
						(tgep.PickupAddressType == AddressType.Airport))
					{
						taxi_fleet_zone zone = null;

						if (tgep.PickupAddressType == AddressType.Address)
						{
							zone = GetFleetZone(tgep.Fleet.Id, tgep.PickupAddress);
						}
						else if (tgep.PickupAddressType == AddressType.Airport)
						{
							zone = GetFleetZoneByAirport(tgep.Fleet.Id, tgep.PickupAirport);
						}

						if (zone != null)
						{
							tgep.PickupZone = zone;
						}
					}
				}
				else
				{
					result = false;
					logger.Warn("Cannot process pickup zone by previous invalid request");
				}


				if (result)
				{
					if (reservation.Service.Dropoff != null)
					{
						//drop off zone
						if ((tgep.DropOffAddressType == AddressType.Address && tgep.DropoffAddress.Geolocation != null && tgep.DropoffAddress.Geolocation.Latitude != 0 && tgep.DropoffAddress.Geolocation.Longitude != 0)
							||
							(tgep.DropOffAddressType == AddressType.Airport))
						{

							taxi_fleet_zone zone = null;

							if (tgep.DropOffAddressType == AddressType.Address)
							{
								zone = GetFleetZone(tgep.Fleet.Id, tgep.DropoffAddress);
							}
							else if (tgep.DropOffAddressType == AddressType.Airport)
							{
								zone = GetFleetZoneByAirport(tgep.Fleet.Id, tgep.DropoffAirport);
							}

							if (zone != null)
							{
								tgep.DropoffZone = zone;
							}
						}
						else
						{
							result = false;
							logger.Warn("Cannot process drop off zone by previous invalid request");
						}
					}
				}
				else
				{
					result = false;
					logger.Warn("Cannot process drop off address by previous invalid request");
				}
				#endregion

				#region Determine FlatRate or Rate
				if (reservation.Service.Dropoff != null)
				{

					long? pickupZoneId = null;
					long? dropoffZoneId = null;

					if ((tgep.PickupZone != null) && (tgep.DropoffZone != null))
					{
						pickupZoneId = tgep.PickupZone.Id;
						dropoffZoneId = tgep.DropoffZone.Id;
					}

					if ((result) && pickupZoneId.HasValue && dropoffZoneId.HasValue && (tgep.Info_FlatRates.Any()) && tgep.Info_FlatRates.Where(x => x.PickupZoneId == pickupZoneId.Value && x.DropoffZoneId == dropoffZoneId.Value).Any())
					{
						//flatrate
						tgep.EstimationType = EstimationResultType.Info_FlatRate;

						#region Do calculation for flatrate

						//caculate flatrate
						taxi_fleet_zone_flatrate tfzf = tgep.Info_FlatRates.Where(x => x.PickupZoneId == pickupZoneId && x.DropoffZoneId == dropoffZoneId).Select(x => x).FirstOrDefault();

						tgep.FlatRateEstimation = new FlatRateData();
						tgep.FlatRateEstimation.CurrencyMode = tfzf.AmountUnit;
						tgep.FlatRateEstimation.PickupZone = tgep.PickupZone.Name;
						tgep.FlatRateEstimation.DropoffZone = tgep.DropoffZone.Name;
						tgep.FlatRateEstimation.EstimationTotalAmount = tfzf.Amount;
						logger.Info("Flat rate found");

						#endregion
					}
					else
					{
						//rate
						tgep.EstimationType = EstimationResultType.Info_Rate;

						#region Get distance
						if (result)
						{
							//MapService ms = new MapService();
							string error = "";

							string pickupString = "";
							string dropoffString = "";

							if (tgep.PickupAddressType == AddressType.Address)
							{
								pickupString = tgep.PickupAddress.FullAddress;
							}
							else
							{
								pickupString = string.Format("{0} {1}", tgep.PickupZone.CenterLatitude, tgep.PickupZone.CenterLongitude);
							}

							if (tgep.DropOffAddressType == AddressType.Address)
							{
								dropoffString = tgep.DropoffAddress.FullAddress;
							}
							else
							{
                                if (tgep.DropoffZone != null)
                                {
                                    if (tgep.DropoffZone.CenterLatitude != null && tgep.DropoffZone.CenterLongitude != null)
                                    {
                                        dropoffString = string.Format("{0} {1}", tgep.DropoffZone.CenterLatitude, tgep.DropoffZone.CenterLongitude);
                                    }
                                }
							}


							//
							MapService ms = new MapService();
							var pLat = tgep.PickupAddress != null ? tgep.PickupAddress.Geolocation.Latitude.ToDouble() : tgep.PickupZone.CenterLatitude.ToDouble();
							var pLon = tgep.PickupAddress != null ? tgep.PickupAddress.Geolocation.Longitude.ToDouble() : tgep.PickupZone.CenterLongitude.ToDouble();
							var dLat = tgep.DropoffAddress != null ? tgep.DropoffAddress.Geolocation.Latitude.ToDouble() : tgep.DropoffZone.CenterLatitude.ToDouble();
							var dLon = tgep.DropoffAddress != null ? tgep.DropoffAddress.Geolocation.Longitude.ToDouble() : tgep.DropoffZone.CenterLongitude.ToDouble();

							double d = ms.GetDirectDistanceInMeter(pLat, pLon, dLat, dLon);
							var rc = new UDI.SDS.RoutingController(tgep.tokenVTOD, null);
							UDI.SDS.RoutingService.Point start = new UDI.SDS.RoutingService.Point { Latitude = pLat, Longitude = pLon };
							UDI.SDS.RoutingService.Point end = new UDI.SDS.RoutingService.Point { Latitude = dLat, Longitude = dLon };
							var rm = rc.GetRouteMetrics(start, end);
							//vehicle.MinutesAway = result.TotalMinutes;
							tgep.Distance_Mile = rm.Distance.ToDecimal();


							//
							//tgep.Distance_Mile = ms.GetDistance(pickupString, dropoffString, Map.DTO.DistanceUnit.Mile, out error);
							if (!string.IsNullOrWhiteSpace(error))
							{
								result = false;
								logger.Error(error);
							}

							if (result)
							{
								//tgep.Distance_Kilometer = ms.GetDistance(pickupString, dropoffString, Map.DTO.DistanceUnit.Kilometer, out error);
								tgep.Distance_Kilometer = tgep.Distance_Mile * (1.609344).ToDecimal();
								if (!string.IsNullOrWhiteSpace(error))
								{
									result = false;
									logger.Error(error);
								}
							}
						}
						else
						{
							result = false;
							logger.Warn("Cannot get estimation by previous invalid request ");
						}

						#endregion

						#region Do caculation for rate
						//if ((result) && (tgep.Info_Rate != null) && ((tgep.PickupAddressType == AddressType.Address) || (tgep.DropOffAddressType == AddressType.Airport)))
						//{
						//caculate rate estimation
						tgep.RateEstimation = new RateData { CurrencyMode = tgep.Info_Rate.AmountUnit, DistanceUnit = tgep.Info_Rate.DistanceUnit };
						//tgep.RateEstimation = new RateData ();
						if (tgep.RateEstimation.DistanceUnit == Map.DTO.DistanceUnit.Mile.ToString())
						{
							tgep.RateEstimation.Distance = double.Parse(tgep.Distance_Mile.ToString());

							//If the distance is greater than InitialDistance
							//tgep.RateEstimation.EstimationTotalAmount = tgep.Info_Rate.InitialAmount + double.Parse(tgep.Distance_Mile.ToString() - InitialDistance) * tgep.Info_Rate.PerDistanceAmount;
							//If tgep.Distance_Mile <= InitialDistance  --- > ignore it.
							//tgep.RateEstimation.EstimationTotalAmount = tgep.Info_Rate.InitialAmount + double.Parse(tgep.Distance_Mile.ToString()) * tgep.Info_Rate.PerDistanceAmount;

							if (Convert.ToDouble(tgep.Distance_Mile) <= tgep.Info_Rate.InitialFreeDistance)
							{
								tgep.RateEstimation.EstimationTotalAmount = tgep.Info_Rate.InitialAmount + Convert.ToDouble(tgep.Distance_Mile.ToString()) * tgep.Info_Rate.PerDistanceAmount;
							}
							else
							{
								tgep.RateEstimation.EstimationTotalAmount = tgep.Info_Rate.InitialAmount + (Convert.ToDouble(tgep.Distance_Mile.ToString()) - tgep.Info_Rate.InitialFreeDistance) * tgep.Info_Rate.PerDistanceAmount;
							}
						}
						else if (tgep.RateEstimation.DistanceUnit == Map.DTO.DistanceUnit.Kilometer.ToString())
						{
							//tgep.RateEstimation.Distance = double.Parse(tgep.Distance_Kilometer.ToString());
							//tgep.RateEstimation.EstimationTotalAmount = tgep.Info_Rate.InitialAmount + double.Parse(tgep.Distance_Kilometer.ToString()) * tgep.Info_Rate.PerDistanceAmount;

							if (Convert.ToDouble(tgep.Distance_Kilometer) <= tgep.Info_Rate.InitialFreeDistance)
							{
								tgep.RateEstimation.EstimationTotalAmount = tgep.Info_Rate.InitialAmount + Convert.ToDouble(tgep.Distance_Kilometer.ToString()) * tgep.Info_Rate.PerDistanceAmount;
							}
							else
							{
								tgep.RateEstimation.EstimationTotalAmount = tgep.Info_Rate.InitialAmount + (Convert.ToDouble(tgep.Distance_Kilometer.ToString()) - tgep.Info_Rate.InitialFreeDistance) * tgep.Info_Rate.PerDistanceAmount;
							}
						}
						//}
						#endregion

					}
				}
				#endregion

				sw.Stop();
				logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			}

			#region Create taxi log
			if (result)
			{
				if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
				{
					WriteTaxiLog(null, TaxiServiceAPIConst.None, TaxiServiceMethodType.Estimation, sw.ElapsedMilliseconds, string.Empty, string.Empty, null, null);
				}
			}
			else
			{
				result = false;
				logger.Warn("Cannot create or get this taxi log by invalid trip");
			}
			#endregion

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateGetEstimationRequest");
			#endregion

			return result;
		}

		public bool ValidateMTDataGetEstimationRequest(TokenRS tokenVTOD, OTA_GroundAvailRQ reservation, out TaxiGetEstimationParameter tgep)
		{
			bool result = true;
			Stopwatch sw = new Stopwatch();
			sw.Start();
			tgep = new TaxiGetEstimationParameter();

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion

			if (reservation.Service != null)
			{
				#region Token
				tgep.tokenVTOD = tokenVTOD;
				#endregion

				#region Validate Pickup Datetime
				if (string.IsNullOrWhiteSpace(reservation.Service.Pickup.DateTime))
				{
					var vtodException = VtodException.CreateFieldRequiredValidationException("Pickup datetime");
					throw vtodException;
				}
				#endregion

				#region Validate Pickup address
				if (reservation.Service.Pickup != null)
				{
					if (((reservation.Service.Pickup.Address != null) && (reservation.Service.Pickup.AirportInfo != null)) || ((reservation.Service.Pickup.Address == null) && (reservation.Service.Pickup.AirportInfo == null)))
					{
						result = false;
						logger.Warn(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
						throw new ValidationException(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
					}
					else if (reservation.Service.Pickup.Address != null && reservation.Service.Pickup.AirportInfo == null)
					{
						if ((string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.Longitude) || (string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.CountryName.Code)))
						{
							result = false;
							logger.Warn(Messages.Validation_LocationDetail);
							throw new ValidationException(Messages.Validation_LocationDetail);
						}
						else if ((!string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.Longitude) && (!string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.CountryName.Code)))
						{
							tgep.PickupAddressOnlyContainsLatAndLong = true;
							tgep.PickupAddressType = AddressType.Address;
							tgep.PickupAddress = ConvertAddress(reservation.Service.Pickup);
						}
						else if (!string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.AddressLine) && !string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.CityName) && !string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.PostalCode) && !string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.CountryName.Code))
						{
							//Address
							Map.DTO.Address pickupAddress = null;
							string pickupAddressStr = GetAddressString(reservation.Service.Pickup.Address);
							logger.Info("Skip MAP API Validation.");
							pickupAddress = ConvertAddress(reservation.Service.Pickup);
							tgep.PickupAddressType = AddressType.Address;
							tgep.PickupAddress = pickupAddress;
							logger.Info("Pickup AddressType: Address");
						}
						else
						{
							result = false;
							var vtodException = VtodException.CreateException(ExceptionType.Taxi, 1);
							logger.Warn(vtodException.ExceptionMessage.Message);
							throw vtodException;
						}
					}
					else if ((reservation.Service.Pickup.Address == null) && (reservation.Service.Pickup.AirportInfo != null))
					{
						//airport
						if (((reservation.Service.Pickup.AirportInfo.Arrival != null) && (reservation.Service.Pickup.AirportInfo.Departure != null)) || ((reservation.Service.Pickup.AirportInfo.Arrival == null) && (reservation.Service.Pickup.AirportInfo.Departure == null)))
						{
							result = false;
							logger.Warn(Messages.Validation_ArrivalAndDepartureAirportInfo);
							throw VtodException.CreateValidationException(Messages.Validation_ArrivalAndDepartureAirportInfo);
						}
						else if (reservation.Service.Pickup.AirportInfo.Arrival != null && reservation.Service.Pickup.AirportInfo.Departure == null)
						{
							//Arrival
							tgep.PickupAddressType = AddressType.Airport;
							tgep.PickupAirport = reservation.Service.Pickup.AirportInfo.Arrival.LocationCode;
							logger.Info("Pickup AddressType: AirportInfo.Arrival");
						}
						else if (reservation.Service.Pickup.AirportInfo.Arrival == null && reservation.Service.Pickup.AirportInfo.Departure != null)
						{
							//Depature

							tgep.PickupAddressType = AddressType.Airport;
							tgep.PickupAirport = reservation.Service.Pickup.AirportInfo.Departure.LocationCode;
							logger.Info("Pickup AddressType: AirportInfo.Depature");
						}

						//longitude and latitude
						if (result)
						{
							double? longitude = null;
							double? latitude = null;
							if (GetLongitudeAndLatitudeForAirport(tgep.PickupAirport, out longitude, out latitude))
							{
								tgep.LongitudeForPickupAirport = longitude.Value;
								tgep.LatitudeForPickupAirport = latitude.Value;
							}
							else
							{
								throw VtodException.CreateException(ExceptionType.Taxi, 1010);// new ValidationException(Messages.Taxi_Common_UnableToFindAirportLongitudeAndLatitude);
							}
						}

					}
				}
				else
				{
					result = false;
					logger.Warn("request.Service.Pickup is null");
				}

				#endregion

				#region Validate Pickup Fleet
				if (result)
				{
					taxi_fleet fleet = null;
					if (GetFleet(tgep.PickupAddressType, tgep.PickupAddress, tgep.PickupAirport, out fleet))
					{
						tgep.Fleet = fleet;
						logger.InfoFormat("Fleet found. ID={0}", fleet.Id);
                        #region Fetcing the Endpoints
                        if (tgep.Fleet != null)
                        {
                            tgep.serviceAPIID =
                                GetTaxiFleetServiceAPIPreference(tgep.Fleet.Id, TaxiServiceAPIPreferenceConst.GetEstimation).ServiceAPIId;
                            #region Fetching Endpoint Names
                            List<string> endpointNames = MtdataEndpointConfiguration(tgep.serviceAPIID, tgep.Fleet.Id);
                            if (endpointNames != null && endpointNames.Any())
                            {
                                tgep.AuthenticationServiceEndpointName = endpointNames[0];
                                tgep.BookingWebServiceEndpointName = endpointNames[1];
                                tgep.OsiWebServiceEndpointName = endpointNames[2];
                                tgep.AddressWebServiceEndpointName = endpointNames[3];
                            }
                            #endregion
                        }
                        #endregion
                    }
                    else
					{
						result = false;
						logger.Warn("Unable to find fleet by this address or landmark.");
					}
				}
				else
				{
					result = false;
					logger.Warn("Unable to find fleet by invalid address or landmark.");
				}
				#endregion

				#region Validate Drop off address
				if (result)
				{
					//drop off address can be address or airportInfo
					if (reservation.Service.Dropoff != null)
					{
						using (VTODEntities context = new VTODEntities())
						{
							if (((reservation.Service.Dropoff.Address != null) && (reservation.Service.Dropoff.AirportInfo != null)) || ((reservation.Service.Dropoff.Address == null) && (reservation.Service.Dropoff.AirportInfo == null)))
							{
								result = false;
								logger.Warn(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
								throw VtodException.CreateValidationException(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
							}
							else if ((reservation.Service.Dropoff.Address != null) && (reservation.Service.Dropoff.AirportInfo == null))
							{
								//Address
								if ((string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.Longitude) || (string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.CountryName.Code)))
								{
									result = false;
									logger.Warn(Messages.Validation_LocationDetail);
									throw VtodException.CreateValidationException(Messages.Validation_LocationDetail);
								}
								else if ((!string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.Longitude) && (!string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.CountryName.Code)))
								{
									tgep.DropOffAddressOnlyContainsLatAndLong = true;
									tgep.DropOffAddressType = AddressType.Address;
									tgep.DropoffAddress = ConvertAddress(reservation.Service.Dropoff);
								}
								else if (!string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.AddressLine) && !string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.CityName) && !string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.PostalCode) && !string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.CountryName.Code))
								{
									Map.DTO.Address dropoffAddress = null;
									string dropoffAddressStr = GetAddressString(reservation.Service.Dropoff.Address);
									logger.Info("Skip MAP API Validation.");
									dropoffAddress = ConvertAddress(reservation.Service.Dropoff);

									tgep.DropoffAddress = dropoffAddress;
									tgep.DropOffAddressType = AddressType.Address;
									logger.Info("Dropoff AddressType=Address");
								}
								else
								{
									result = false;
									var vtodException = VtodException.CreateException(ExceptionType.Taxi, 1);
									logger.Warn(vtodException.ExceptionMessage.Message);
									throw vtodException;
								}
							}
							else if ((reservation.Service.Dropoff.Address == null) && (reservation.Service.Dropoff.AirportInfo != null))
							{

								//airport
								if (((reservation.Service.Dropoff.AirportInfo.Arrival != null) && (reservation.Service.Dropoff.AirportInfo.Departure != null)) || ((reservation.Service.Dropoff.AirportInfo.Arrival == null) && (reservation.Service.Dropoff.AirportInfo.Departure == null)))
								{
									result = false;
									logger.Warn(Messages.Validation_ArrivalAndDepartureAirportInfo);
									throw VtodException.CreateValidationException(Messages.Validation_ArrivalAndDepartureAirportInfo);
								}
								else if (reservation.Service.Dropoff.AirportInfo.Arrival != null && reservation.Service.Dropoff.AirportInfo.Departure == null)
								{
									//Arrival
									tgep.DropOffAddressType = AddressType.Airport;
									tgep.DropoffAirport = reservation.Service.Dropoff.AirportInfo.Arrival.LocationCode;
									logger.Info("Dropoff AddressType: AirportInfo.Arrival");
								}
								else if (reservation.Service.Dropoff.AirportInfo.Arrival == null && reservation.Service.Dropoff.AirportInfo.Departure != null)
								{
									//Depature
									tgep.DropOffAddressType = AddressType.Airport;
									tgep.DropoffAirport = reservation.Service.Dropoff.AirportInfo.Departure.LocationCode;
									logger.Info("Dropoff AddressType: AirportInfo.Depature");
								}

								//longitude and latitude
								if (result)
								{
									double? longitude = null;
									double? latitude = null;
									if (GetLongitudeAndLatitudeForAirport(tgep.DropoffAirport, out longitude, out latitude))
									{
										tgep.LongitudeForDropoffAirport = longitude.Value;
										tgep.LatitudeForDropoffAirport = latitude.Value;
									}
									else
									{
										throw VtodException.CreateException(ExceptionType.Taxi, 1010);// new ValidationException(Messages.Taxi_Common_UnableToFindAirportLongitudeAndLatitude);
									}
								}
							}
						}
					}
				}
				else
				{
					result = false;
					logger.Warn("Cannot get estimation by previous invalid request ");
				}
				#endregion

				#region Validate Available address type
				if (result)
				{
					//string pickupAddressType = tgep.PickupAddressType;
					//string dropoffAddressType = tgep.DropOffAddressType;

					//verify the address type of pickup address and drop off address
					UtilityDomain ud = new UtilityDomain();
					if (tgep.PickupAddressType == AddressType.Address.ToString())
					{
						UtilityGetAirportNameRQ uganRQ = new UtilityGetAirportNameRQ();
						uganRQ.Address = new Address();
						//tgep.PickupAddress.Geolocation = new Map.DTO.Geolocation();
						uganRQ.Address.Latitude = tgep.PickupAddress.Geolocation.Latitude.ToString();
						uganRQ.Address.Longitude = tgep.PickupAddress.Geolocation.Longitude.ToString();

						UtilityGetAirportNameRS uganRS = ud.GetAirportName(tokenVTOD, uganRQ);
						if (uganRS.Success != null)
						{
							if (!string.IsNullOrWhiteSpace(uganRS.AirportName))
							{
								//pickupAddressType = AddressType.Airport;

								tgep.PickupAddressType = AddressType.Airport;
								tgep.PickupAirport = uganRS.AirportName;
								tgep.LongitudeForPickupAirport = uganRQ.Address.Longitude.ToDouble();
								tgep.LatitudeForPickupAirport = uganRQ.Address.Longitude.ToDouble();
							}
						}

					}

					if (tgep.DropOffAddressType == AddressType.Address.ToString())
					{
						UtilityGetAirportNameRQ uganRQ = new UtilityGetAirportNameRQ();
						uganRQ.Address = new Address();
						//tgep.DropoffAddress.Geolocation = new Map.DTO.Geolocation();
						uganRQ.Address.Latitude = tgep.DropoffAddress.Geolocation.Latitude.ToString();
						uganRQ.Address.Longitude = tgep.DropoffAddress.Geolocation.Longitude.ToString();

						UtilityGetAirportNameRS uganRS = ud.GetAirportName(tokenVTOD, uganRQ);
						if (uganRS.Success != null)
						{
							if (!string.IsNullOrWhiteSpace(uganRS.AirportName))
							{
								//dropoffAddressType = AddressType.Airport;
								tgep.DropOffAddressType = AddressType.Airport;
								tgep.DropoffAirport = uganRS.AirportName;
								tgep.LongitudeForDropoffAirport = uganRQ.Address.Longitude.ToDouble();
								tgep.LatitudeForDropoffAirport = uganRQ.Address.Longitude.ToDouble();
							}
						}
					}

					//check database setting
					//1. get taxi_fleet_availableaddresstype
					using (VTODEntities context = new VTODEntities())
					{
						long fleetId = tgep.Fleet.Id;
						if (context.taxi_fleet_availableaddresstype.Where(x => fleetId == x.FleetId).Any())
						{
							taxi_fleet_availableaddresstype tfa = context.taxi_fleet_availableaddresstype.Where(x => fleetId == x.FleetId).Select(x => x).FirstOrDefault();
							//2. check if the type is available or not
							//Taxi_Common_InvalidAddressTypeForFleet
							if (AddressType.Address.ToString() == tgep.PickupAddressType && AddressType.Address.ToString() == tgep.DropOffAddressType)
							{
								//address to address
								if (tfa.AddressToAddress)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.Taxi, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
							else if (AddressType.Address.ToString() == tgep.PickupAddressType && AddressType.Airport.ToString() == tgep.DropOffAddressType)
							{
								//address to airport
								if (tfa.AddressToAirport)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.Taxi, 2020); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
							else if (AddressType.Address.ToString() == tgep.PickupAddressType && null == tgep.DropOffAddressType)
							{
								//address to null
								if (tfa.AddressToNull)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.Taxi, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
							else if (AddressType.Airport.ToString() == tgep.PickupAddressType && AddressType.Address.ToString() == tgep.DropOffAddressType)
							{
								//airport to address
								if (tfa.AirportToAddress)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.Taxi, 2019); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
							else if (AddressType.Airport.ToString() == tgep.PickupAddressType && AddressType.Airport.ToString() == tgep.DropOffAddressType)
							{
								//airport to airport
								if (tfa.AirportToAirport)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.Taxi, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
							else if (AddressType.Airport.ToString() == tgep.PickupAddressType && null == tgep.DropOffAddressType)
							{
								//airport to null
								if (tfa.AirportToNull)
								{
									result = true;
								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.Taxi, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
								}
							}
						}
						else
						{
							result = false;
							throw VtodException.CreateException(ExceptionType.Taxi, 1011);// VtodException.CreateException(ExceptionType.Taxi, 1011);// new ValidationException(Messages.Taxi_Common_UnableToGetAvailableFleets);
						}
					}


				}
				else
				{
					result = false;
					var vtodException = Common.DTO.VtodException.CreateFieldRequiredValidationException("Payment");
					logger.Warn(vtodException.ExceptionMessage.Message);
					throw vtodException; // new ValidationException(Messages.Validation_NullPayment);
				}
				#endregion

				if (result && tgep.Fleet.OverWriteFixedPrice)
				{
					#region Get FlatRate
					using (VTODEntities context = new VTODEntities())
					{
						long pickupfleetID = tgep.Fleet.Id;
						List<taxi_fleet_zone_flatrate> tfzfList = context.taxi_fleet_zone_flatrate.Where(x => x.PickupZone.FleetId == pickupfleetID).Select(x => x).ToList();
						if (tfzfList.Any())
						{
							tgep.Info_FlatRates = new List<TaxiFlatRateData>();
							foreach (var x in tfzfList)
							{
								var tfrd = new TaxiFlatRateData { Amount = x.Amount, AmountUnit = x.AmountUnit, AppendTime = x.AppendTime, DropoffZoneId = x.DropoffZoneId, Id = x.Id, PickupZoneId = x.PickupZoneId };
								tfrd.PickupZoneName = context.taxi_fleet_zone.Where(xx => xx.Id == tfrd.PickupZoneId).Select(xxx => xxx.Name).FirstOrDefault();
								tfrd.DropOffZoneName = context.taxi_fleet_zone.Where(xx => xx.Id == tfrd.DropoffZoneId).Select(xxx => xxx.Name).FirstOrDefault();
								tgep.Info_FlatRates.Add(tfrd);
							}
						}
					}
					#endregion

					#region Pickup zone and dropoff zone
					if (result)
					{
						//pickup zone
						if ((tgep.PickupAddressType == AddressType.Address && tgep.PickupAddress.Geolocation != null && tgep.PickupAddress.Geolocation.Latitude != 0 && tgep.PickupAddress.Geolocation.Longitude != 0)
							||
							(tgep.PickupAddressType == AddressType.Airport))
						{
							taxi_fleet_zone zone = null;

							if (tgep.PickupAddressType == AddressType.Address)
							{
								zone = GetFleetZone(tgep.Fleet.Id, tgep.PickupAddress);
							}
							else if (tgep.PickupAddressType == AddressType.Airport)
							{
								zone = GetFleetZoneByAirport(tgep.Fleet.Id, tgep.PickupAirport);
							}

							if (zone != null)
							{
								tgep.PickupZone = zone;
							}
						}
					}
					else
					{
						result = false;
						logger.Warn("Cannot process pickup zone by previous invalid request");
					}

					if (result)
					{
						if (reservation.Service.Dropoff != null)
						{
							//drop off zone
							if ((tgep.DropOffAddressType == AddressType.Address && tgep.DropoffAddress.Geolocation != null && tgep.DropoffAddress.Geolocation.Latitude != 0 && tgep.DropoffAddress.Geolocation.Longitude != 0)
								||
								(tgep.DropOffAddressType == AddressType.Airport))
							{
								taxi_fleet_zone zone = null;

								if (tgep.DropOffAddressType == AddressType.Address)
								{
									zone = GetFleetZone(tgep.Fleet.Id, tgep.DropoffAddress);
								}
								else if (tgep.DropOffAddressType == AddressType.Airport)
								{
									zone = GetFleetZoneByAirport(tgep.Fleet.Id, tgep.DropoffAirport);
								}

								if (zone != null)
								{
									tgep.DropoffZone = zone;
								}
							}
							else
							{
								result = false;
								logger.Warn("Cannot process drop off zone by previous invalid request");
							}
						}
					}
					else
					{
						result = false;
						logger.Warn("Cannot process drop off address by previous invalid request");
					}
					#endregion

					#region Determine FlatRate or Rate
					if (reservation.Service.Dropoff != null)
					{
						long? pickupZoneId = null;
						long? dropoffZoneId = null;

						if ((tgep.PickupZone != null) && (tgep.DropoffZone != null))
						{
							pickupZoneId = tgep.PickupZone.Id;
							dropoffZoneId = tgep.DropoffZone.Id;
						}

						if ((result) && pickupZoneId.HasValue && dropoffZoneId.HasValue && tgep.Info_FlatRates != null && tgep.Info_FlatRates.Any()
							&& tgep.Info_FlatRates.Any(x => x.PickupZoneId == pickupZoneId.Value && x.DropoffZoneId == dropoffZoneId.Value))
						{
							//flatrate
							tgep.EstimationType = EstimationResultType.Info_FlatRate;

							#region Do calculation for flatrate

							//caculate flatrate
							taxi_fleet_zone_flatrate tfzf = tgep.Info_FlatRates.Where(x => x.PickupZoneId == pickupZoneId && x.DropoffZoneId == dropoffZoneId).Select(x => x).FirstOrDefault();

							tgep.FlatRateEstimation = new FlatRateData
							{
								CurrencyMode = tfzf.AmountUnit,
								PickupZone = tgep.PickupZone.Name,
								DropoffZone = tgep.DropoffZone.Name,
								EstimationTotalAmount = tfzf.Amount
							};
							logger.Info("Flat rate found");

							#endregion
						}
						else
						{
							//rate
							tgep.EstimationType = EstimationResultType.Info_Rate;
						}
					}
					#endregion
				}

                #region Add Accessibility
                if(reservation!=null & reservation.DisabilityInfo!=null && reservation.DisabilityInfo.RequiredInd==true)
                {
                   

                }
                #endregion

                sw.Stop();
				logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			}

			#region Create taxi log
			if (result)
			{
				if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
				{
					WriteTaxiLog(null, TaxiServiceAPIConst.None, TaxiServiceMethodType.Estimation, sw.ElapsedMilliseconds, string.Empty, string.Empty, null, null);
				}
			}
			else
			{
				result = false;
				logger.Warn("Cannot create or get this taxi log by invalid trip");
			}
			#endregion

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateGetEstimationRequest");
			#endregion

			return result;
		}
		public bool UpdateFlatRate(Int64 tripId, bool IsFlatRate)
		{
			vtod_trip trip = null;
			using (VTODEntities context = new VTODEntities())
			{
				trip = context.vtod_trip.Where(x => x.Id == tripId).FirstOrDefault();
				trip.IsFlatRate = IsFlatRate;
				context.SaveChanges();
			}
			return false;
		}
		public decimal? DetermineOverwriteFare(TaxiStatusParameter tsp, decimal? longtitude, decimal? latitude, decimal? dispatchFare)
		{
			decimal? finalFareWithoutGratuity = dispatchFare;
			Stopwatch sw = new Stopwatch();
			sw.Start();

			try
			{
				if (!longtitude.HasValue || !latitude.HasValue)
				{
					//get longtitude and latitude from last trip status
					using (VTODEntities context = new VTODEntities())
					{
						if (context.taxi_trip_status.Where(x => x.TripID == tsp.Trip.Id && x.VehicleLongitude != null && x.VehicleLatitude != null).Select(x => x).OrderByDescending(x => x.Id).Any())
						{
							taxi_trip_status tts = context.taxi_trip_status.Where(x => x.TripID == tsp.Trip.Id && x.VehicleLongitude != null && x.VehicleLatitude != null).Select(x => x).OrderByDescending(x => x.Id).FirstOrDefault();
							longtitude = tts.VehicleLongitude;
							latitude = tts.VehicleLatitude;
						}
					}

					//if there is no trip status from DB, just use dispatch fare
					if (!longtitude.HasValue || !latitude.HasValue)
					{
						finalFareWithoutGratuity = dispatchFare;
					}

				}

				//Get Flat Rate
				if (longtitude.HasValue && latitude.HasValue && tsp.Trip.taxi_trip.PickupLongitude.HasValue && tsp.Trip.taxi_trip.PickupLatitude.HasValue && tsp.Fleet.OverWriteFixedPrice)
				{
					long pickupfleetID = tsp.Trip.taxi_trip.FleetId;
					Map.DTO.Address pickupAddress = new Map.DTO.Address { Geolocation = new Map.DTO.Geolocation { Latitude = tsp.Trip.taxi_trip.PickupLatitude.ToDecimal(), Longitude = tsp.Trip.taxi_trip.PickupLongitude.ToDecimal() } };
					taxi_fleet_zone pickupZone = GetFleetZone(pickupfleetID, pickupAddress);

					Map.DTO.Address dropOffAddress = new Map.DTO.Address { Geolocation = new Map.DTO.Geolocation { Latitude = latitude.Value, Longitude = longtitude.Value } };
					taxi_fleet_zone dropOffZone = GetFleetZone(pickupfleetID, dropOffAddress);

					if (pickupZone != null && dropOffZone != null)
					{
						using (VTODEntities context = new VTODEntities())
						{
							taxi_fleet_zone_flatrate tfzf = context.taxi_fleet_zone_flatrate.Where(x => x.PickupZoneId == pickupZone.Id && x.DropoffZoneId == dropOffZone.Id).Select(x => x).OrderBy(x => x.Amount).FirstOrDefault();
							if (tfzf != null)
							{
								finalFareWithoutGratuity = tfzf.Amount.ToDecimal();
								UpdateFlatRate(tsp.Trip.Id, true);
							}
							else
							{
								//unable to find flatrate
								finalFareWithoutGratuity = dispatchFare;
							}
						}
					}
					else
					{
						//unable to find any dropoff zone in the flatratezone
						finalFareWithoutGratuity = dispatchFare;
					}

				}
				else
				{
					//just use dispatchFare
					finalFareWithoutGratuity = dispatchFare;
				}

			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw new TaxiException(ex.Message);
			}

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "DetermineOverwritedFare");
			#endregion

			return finalFareWithoutGratuity;
		}

		public bool ValidateSendMessageToVehicle(TokenRS tokenVTOD, UtilityNotifyDriverRQ input, out TaxiSendMessageToVehicleParameter tsmtvp)
		{
			bool result = false;
			Stopwatch sw = new Stopwatch();
			sw.Start();
			tsmtvp = new TaxiSendMessageToVehicleParameter();

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion


			using (VTODEntities context = new VTODEntities())
			{
				tsmtvp.VehicleNunber = context.taxi_trip_status.Where(x => x.TripID == input.TripID && x.VehicleNumber != null).OrderByDescending(x => x.Id).Select(x => x.VehicleNumber).FirstOrDefault();
				//tsmtvp.Trip = (taxi_trip)context.vtod_trip.Where(x => x.Id == input.TripID).FirstOrDefault();
				tsmtvp.Trip = context.taxi_trip.Where(x => x.Id == input.TripID).FirstOrDefault();
				long fleetId = tsmtvp.Trip.FleetId;
				tsmtvp.Fleet = context.taxi_fleet.Where(x => x.Id == fleetId).FirstOrDefault();
				if (!tsmtvp.Fleet.taxi_fleet_api_reference.Any())
				{
					tsmtvp.Fleet.taxi_fleet_api_reference.Add(context.taxi_fleet_api_reference.Where(x => x.FleetId == fleetId).ToList().FirstOrDefault());
				}
				tsmtvp.Message = input.Message;
			}

			if (!string.IsNullOrWhiteSpace(tsmtvp.VehicleNunber) && !string.IsNullOrWhiteSpace(tsmtvp.Message) && tsmtvp != null)
			{
				result = true;
			}
			else
			{
				result = false;
				throw VtodException.CreateException(ExceptionType.Taxi, 9003);//
			}

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateSendMessageToVehicle");
			#endregion

			return result;
		}

		public bool ValidateMTDataSendMessageToVehicle(TokenRS tokenVTOD, UtilityNotifyDriverRQ input, out TaxiSendMessageToVehicleParameter tsmtvp, out string vehicleNumber)
		{
			Stopwatch sw = new Stopwatch();
			sw.Start();
			tsmtvp = new TaxiSendMessageToVehicleParameter();
			vehicleNumber = string.Empty;

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion

			using (var context = new VTODEntities())
			{
				tsmtvp.Trip = context.taxi_trip.FirstOrDefault(x => x.Id == input.TripID);
				tsmtvp.Message = input.Message;

				if (tsmtvp.Trip != null && !string.IsNullOrWhiteSpace(tsmtvp.Message))
				{
					var tripId = tsmtvp.Trip.Id;
					var fleetId = tsmtvp.Trip.FleetId;

					tsmtvp.Fleet = context.taxi_fleet.Where(p => p.Id == fleetId).FirstOrDefault();

					var statusObj = context.taxi_trip_status.Where(p => p.TripID == tripId && p.VehicleNumber != null && p.VehicleNumber != string.Empty).OrderByDescending(p => p.Id).FirstOrDefault();
					if (statusObj == null || string.IsNullOrWhiteSpace(statusObj.VehicleNumber))
					{
						throw VtodException.CreateException(ExceptionType.Taxi, 9003);
					}
					else
					{
						vehicleNumber = statusObj.VehicleNumber;
					}
				}
				else
				{
					throw VtodException.CreateException(ExceptionType.Taxi, 9003);
				}
			}

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateSendMessageToVehicle");
			#endregion

			return true;
		}

		public bool ValidateMTDataPickMessageToVehicle(TokenRS tokenVTOD, UtilityNotifyDriverRQ input, out TaxiSendMessageToVehicleParameter tsmtvp)
		{
			Stopwatch sw = new Stopwatch();
			sw.Start();
			tsmtvp = new TaxiSendMessageToVehicleParameter();

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion

			using (var context = new VTODEntities())
			{
				tsmtvp.Trip = context.taxi_trip.FirstOrDefault(x => x.Id == input.TripID);
			}

			if (tsmtvp.Trip == null)
			{
				throw VtodException.CreateException(ExceptionType.Taxi, 9003);
			}

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateMTDataPickMessageToVehicle");
			#endregion

			return true;
		}

        public bool ValidateMTDataDispatchPendingMessageToVehicle(TokenRS tokenVTOD, UtilityNotifyDriverRQ input, out TaxiSendMessageToVehicleParameter tsmtvp)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            tsmtvp = new TaxiSendMessageToVehicleParameter();

            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
            #endregion

            using (var context = new VTODEntities())
            {
                tsmtvp.Trip = context.taxi_trip.FirstOrDefault(x => x.Id == input.TripID);
                
            }

            if (tsmtvp.Trip == null)
            {
                throw VtodException.CreateException(ExceptionType.Taxi, 9003);
            }
            else
            {
                using (var context = new VTODEntities())
                {
                    int fleetId = 0;
                    fleetId = tsmtvp.Trip.FleetId.ToInt32();
                    tsmtvp.Fleet = context.taxi_fleet.FirstOrDefault(x => x.Id == fleetId);
                   
                }
                
            }

            sw.Stop();
            logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateMTDataDispatchPendingMessageToVehicle");
            #endregion

            return true;
        }
		public bool ValidateMTDataGetCustomerInfo(TokenRS tokenVTOD, UtilityCustomerInfoRQ input, out TaxiGetCustomerInfoParameter tgcip)
		{
			bool result = false;
			Stopwatch sw = new Stopwatch();
			sw.Start();
			tgcip = new TaxiGetCustomerInfoParameter { request = input, tokenVTOD = tokenVTOD, PhoneNumber = input.DispatchRecord.KeyWordValue, SearchType = input.DispatchRecord.KeyWordName };
			long fleetID = Convert.ToInt64(input.DispatchRecord.FleetId);

			using (VTODEntities context = new VTODEntities())
			{
				tgcip.Fleet = context.taxi_fleet.Where(x => x.Id == fleetID && x.IVREnable).FirstOrDefault();
				if (tgcip.Fleet != null)
				{
					try
					{
						tgcip.serviceAPIID = GetServiceAPIId(tgcip.Fleet.Id, TaxiServiceAPIPreferenceConst.GetCustomerInfo);
                        #region Fetching Endpoint Names
                        List<string> endpointNames = MtdataEndpointConfiguration(tgcip.serviceAPIID, tgcip.Fleet.Id);
                        if (endpointNames != null && endpointNames.Any())
                        {
                            tgcip.AuthenticationServiceEndpointName = endpointNames[0];
                            tgcip.BookingWebServiceEndpointName = endpointNames[1];
                            tgcip.OsiWebServiceEndpointName = endpointNames[2];
                            tgcip.AddressWebServiceEndpointName = endpointNames[3];
                        }
                        #endregion
                        result = true;
					}
					catch (Exception ex)
					{
						var vtodException = VtodException.CreateException(ExceptionType.Taxi, 1011);
						logger.Warn(vtodException.ExceptionMessage.Message);
						throw vtodException;
					}
				}

			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateSendMessageToVehicle");
			#endregion


			return result;
		}

		public bool ValidateMTDataGetPickupAddress(TokenRS tokenVTOD, UtilityGetPickupAddressRQ input, out TaxiGetPickupAddressParameter tgpap)
		{
			bool result = false;
			Stopwatch sw = new Stopwatch();
			sw.Start();
			tgpap = new TaxiGetPickupAddressParameter { request = input, tokenVTOD = tokenVTOD, PhoneNumber = input.DispatchRecord.KeyWordValue, SearchType = input.DispatchRecord.KeyWordName };
			long fleetID = Convert.ToInt64(input.DispatchRecord.FleetId);
			using (VTODEntities context = new VTODEntities())
			{
				tgpap.Fleet = context.taxi_fleet.Where(x => x.Id == fleetID && x.IVREnable).FirstOrDefault();
				if (tgpap.Fleet != null)
				{
					try
					{
						tgpap.serviceAPIID = GetServiceAPIId(tgpap.Fleet.Id, TaxiServiceAPIPreferenceConst.GetPickupAddress);
                        #region Fetching Endpoint Names
                        List<string> endpointNames = MtdataEndpointConfiguration(tgpap.serviceAPIID, tgpap.Fleet.Id);
                        if (endpointNames != null && endpointNames.Any())
                        {
                            tgpap.AuthenticationServiceEndpointName = endpointNames[0];
                            tgpap.BookingWebServiceEndpointName = endpointNames[1];
                            tgpap.OsiWebServiceEndpointName = endpointNames[2];
                            tgpap.AddressWebServiceEndpointName = endpointNames[3];
                        }
                        #endregion
                        result = true;
					}
					catch (Exception ex)
					{
						var vtodException = VtodException.CreateException(ExceptionType.Taxi, 1011);
						logger.Warn(vtodException.ExceptionMessage.Message);
						throw vtodException;
					}
				}

			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateSendMessageToVehicle");
			#endregion


			return result;
		}

		public bool ValidateMTDataGetLastTrip(TokenRS tokenVTOD, UtilityGetLastTripRQ input, out TaxiGetLastTripParameter tgltp)
		{
			bool result = false;
			Stopwatch sw = new Stopwatch();
			sw.Start();
			tgltp = new TaxiGetLastTripParameter { request = input, tokenVTOD = tokenVTOD, PhoneNumber = string.Format("{0}{1}{2}", input.Telephone.CountryAccessCode, input.Telephone.AreaCityCode, input.Telephone.AreaCityCode), SearchType = "PhoneNumber", FromMinutesInPast = Convert.ToInt32(input.FromMinutesInPast) };
			long fleetID = Convert.ToInt64(input.FleetId);
			using (VTODEntities context = new VTODEntities())
			{
				tgltp.Fleet = context.taxi_fleet.Where(x => x.Id == fleetID && x.IVREnable).FirstOrDefault();
				if (tgltp.Fleet != null)
				{
					try
					{
						tgltp.serviceAPIID = GetServiceAPIId(tgltp.Fleet.Id, TaxiServiceAPIPreferenceConst.GetLastTrip);
                        #region Fetching Endpoint Names
                        List<string> endpointNames = MtdataEndpointConfiguration(tgltp.serviceAPIID, tgltp.Fleet.Id);
                        if (endpointNames != null && endpointNames.Any())
                        {
                            
                            tgltp.AuthenticationServiceEndpointName = endpointNames[0];
                            tgltp.BookingWebServiceEndpointName = endpointNames[1];
                            tgltp.OsiWebServiceEndpointName = endpointNames[2];
                            tgltp.AddressWebServiceEndpointName = endpointNames[3];
                        }
                        #endregion

                        result = true;
					}
					catch (Exception ex)
					{
						var vtodException = VtodException.CreateException(ExceptionType.Taxi, 1011);
						logger.Warn(vtodException.ExceptionMessage.Message);
						throw vtodException;
					}
				}

			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateSendMessageToVehicle");
			#endregion


			return result;
		}
        public bool ValidateFleets(TokenRS token, UtilityAvailableFleetsRQ input, out taxi_fleet f)
        {
            bool result = false;
            f = null;
            Stopwatch sw = new Stopwatch();
            sw.Start();

            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
            #endregion

            if (input.Address != null)
            {
                using (VTODEntities context = new VTODEntities())
                {
                    //convert to MAPQuest address
                    Map.DTO.Address address = new Map.DTO.Address { Country = input.Address.CountryName.Code, City = input.Address.CityName, State = input.Address.StateProv.StateCode, ZipCode = input.Address.PostalCode, Geolocation = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(input.Address.Latitude), Longitude = Convert.ToDecimal(input.Address.Longitude) } };
                    string addressType = AddressType.Address.ToString();
                    try
                    {
                        if (GetFleet(addressType, address, "", out f))
                        {
                            result = true;
                        }
                    }
                    catch
                    {

                    }
                    }
            }
            sw.Stop();
            logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

            #region Track
            if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateFleets");
            #endregion
            return result;
        }

        public bool ValidateGetAvailableFleets(TokenRS token, UtilityAvailableFleetsRQ input, out bool PickMeUpNow, out bool PickMeUpLater)
		{
			bool result = false;
			PickMeUpNow = true;
			PickMeUpLater = true;
			Stopwatch sw = new Stopwatch();
			sw.Start();

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion

			if (input.Address != null)
			{
				using (VTODEntities context = new VTODEntities())
				{
					//convert to MAPQuest address
					Map.DTO.Address address = new Map.DTO.Address { Country = input.Address.CountryName.Code, City = input.Address.CityName, State = input.Address.StateProv.StateCode, ZipCode = input.Address.PostalCode, Geolocation = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(input.Address.Latitude), Longitude = Convert.ToDecimal(input.Address.Longitude) } };
					taxi_fleet f = new taxi_fleet();
					string addressType = AddressType.Address.ToString();
					try
					{
						if (GetFleet(addressType, address, "", out f))
						{
							if (f != null)
							{
								#region Validate Available address type

								string pickupAddressType = "";


								//verify the address type 
								UtilityDomain ud = new UtilityDomain();

								UtilityGetAirportNameRQ uganRQ = new UtilityGetAirportNameRQ();
								uganRQ.Address = new Address();
								uganRQ.Address.Latitude = input.Address.Latitude.ToString();
								uganRQ.Address.Longitude = input.Address.Longitude.ToString();

								UtilityGetAirportNameRS uganRS = ud.GetAirportName(token, uganRQ);
								if (uganRS.Success != null)
								{
									if (!string.IsNullOrWhiteSpace(uganRS.AirportName))
									{
										pickupAddressType = AddressType.Airport;
									}
									else
									{
										pickupAddressType = AddressType.Address;
									}
								}
								else
								{
									pickupAddressType = AddressType.Address;
								}




								//check database setting
								//1. get taxi_fleet_availableaddresstype

								long fleetId = f.Id;
								if (context.taxi_fleet_availableaddresstype.Where(x => fleetId == x.FleetId).Any())
								{
									taxi_fleet_availableaddresstype tfa = context.taxi_fleet_availableaddresstype.Where(x => fleetId == x.FleetId).Select(x => x).FirstOrDefault();
									//2. check if the type is available or not
									//Taxi_Common_InvalidAddressTypeForFleet
									if (AddressType.Address.ToString() == pickupAddressType)
									{
										//address to address
										if (tfa.AddressToAddress || tfa.AddressToAirport || tfa.AddressToNull)
										{
											result = true;
										}
										else
										{
											result = false;
											throw VtodException.CreateException(ExceptionType.Taxi, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
										}
									}
									else if (AddressType.Airport.ToString() == pickupAddressType)
									{
										//address to airport
										if (tfa.AirportToAddress || tfa.AirportToAirport || tfa.AirportToNull)
										{
											result = true;
										}
										else
										{
											result = false;
											throw VtodException.CreateException(ExceptionType.Taxi, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
										}
									}
									else
									{
										throw new ValidationException("Unknown address type setting");

									}

								}
								else
								{
									result = false;
									throw VtodException.CreateException(ExceptionType.Taxi, 1011);// new ValidationException(Messages.Taxi_Common_UnableToGetAvailableFleets);
								}




								#endregion

								GetPickMeupOption(f, out PickMeUpNow, out PickMeUpLater);
								result = true;
							}
						}
					}
					catch
					{
						//throw;
					}
				}
			}

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateGetAvailableFleets");
			#endregion

			return result;
		}

		public bool ValidateMTDataGetBlackoutPeriod(TokenRS tokenVTOD, UtilityGetBlackoutPeriodRQ input, out TaxiGetBlackoutPeriodParameter tgbpp)
		{
			bool result = false;
			Stopwatch sw = new Stopwatch();
			sw.Start();
			tgbpp = new TaxiGetBlackoutPeriodParameter { request = input, tokenVTOD = tokenVTOD };
			long fleetID = Convert.ToInt64(input.FleetId);

			using (VTODEntities context = new VTODEntities())
			{
				tgbpp.Fleet = context.taxi_fleet.Where(x => x.Id == fleetID && x.IVREnable).FirstOrDefault();
				if (tgbpp.Fleet != null)
				{
					try
					{
						tgbpp.serviceAPIID = GetServiceAPIId(tgbpp.Fleet.Id, TaxiServiceAPIPreferenceConst.GetCustomerInfo);
                        #region Fetching Endpoint Names
                        List<string> endpointNames = MtdataEndpointConfiguration(tgbpp.serviceAPIID, tgbpp.Fleet.Id);
                        if (endpointNames != null && endpointNames.Any())
                        {
                            tgbpp.AuthenticationServiceEndpointName = endpointNames[0];
                            tgbpp.BookingWebServiceEndpointName = endpointNames[1];
                            tgbpp.OsiWebServiceEndpointName = endpointNames[2];
                            tgbpp.AddressWebServiceEndpointName = endpointNames[3];
                        }
                        #endregion
                        result = true;
					}
					catch (Exception ex)
					{
						var vtodException = VtodException.CreateException(ExceptionType.Taxi, 1011);
						logger.Warn(vtodException.ExceptionMessage.Message);
						throw vtodException;
					}
				}

			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateMTDataGetBlackoutPeriod");
			#endregion


			return result;
		}

		public bool ValidateMTDataGetNotification(TokenRS tokenVTOD, UtilityGetNotificationRQ input, out TaxiGetNotificationParameter tgnp)
		{
			bool result = false;
			Stopwatch sw = new Stopwatch();
			sw.Start();
			tgnp = new TaxiGetNotificationParameter { request = input, tokenVTOD = tokenVTOD };
			long fleetID = Convert.ToInt64(input.FleetId);

			if (input.minutesDispatching >= 0)
			{
				using (VTODEntities context = new VTODEntities())
				{
					tgnp.Fleet = context.taxi_fleet.Where(x => x.Id == fleetID && x.IVREnable).FirstOrDefault();
					if (tgnp.Fleet != null)
					{
						try
						{
							tgnp.serviceAPIID = GetServiceAPIId(tgnp.Fleet.Id, TaxiServiceAPIPreferenceConst.GetCustomerInfo);
                            #region Fetching Endpoint Names
                            List<string> endpointNames = MtdataEndpointConfiguration(tgnp.serviceAPIID, tgnp.Fleet.Id);
                            if (endpointNames != null && endpointNames.Any())
                            {
                                tgnp.AuthenticationServiceEndpointName = endpointNames[0];
                                tgnp.BookingWebServiceEndpointName = endpointNames[1];
                                tgnp.OsiWebServiceEndpointName = endpointNames[2];
                                tgnp.AddressWebServiceEndpointName = endpointNames[3];
                            }
                            #endregion

                            result = true;
						}
						catch (Exception ex)
						{
							var vtodException = VtodException.CreateException(ExceptionType.Taxi, 1011);
							logger.Warn(vtodException.ExceptionMessage.Message);
							throw vtodException;
						}
					}

				}
				sw.Stop();
				logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			}

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "ValidateMTDataGetNotification");
			#endregion


			return result;
		}

		#endregion

		public bool GetLongitudeAndLatitudeForAirport(string airportCode, out double? longitude, out double? latitude)
		{
			bool result = false;


			//#region Track
			//if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			//#endregion

			using (VTODEntities context = new VTODEntities())
			{
				taxi_fleet_zone zone = context.taxi_fleet_zone.Where(x => x.Name == airportCode && x.Type == AddressType.Airport).Select(x => x).FirstOrDefault();
				if (zone != null)
				{

					double lon;
					double lat;
					if (zone.CenterLongitude.HasValue && double.TryParse(zone.CenterLongitude.Value.ToString(), out lon))
					{
						longitude = lon;
						result = true;
					}
					else
					{
						logger.Error("Unable to convert longitude");
						result = false;
						longitude = null;
					}

					if (zone.CenterLatitude.HasValue && double.TryParse(zone.CenterLatitude.Value.ToString(), out lat))
					{
						latitude = lat;
						result = true;
					}
					else
					{
						logger.Error("Unable to convert latitude");
						result = false;
						latitude = null;
					}
				}
				else
				{
					result = false;
					longitude = null;
					latitude = null;
				}

			}

			//#region Track
			//if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Utility, "GetLongitudeAndLatitudeForAirport");
			//#endregion

			return result;
		}

		public taxi_fleet GetActiveIVRFleet(long Id)
		{

			taxi_fleet fleet = null;

			using (VTODEntities context = new VTODEntities())
			{
				fleet = context.taxi_fleet.Where(x => x.Id == Id && x.IVREnable).Select(x => x).FirstOrDefault();
				if (fleet != null)
				{
					fleet.taxi_fleet_api_reference = context.taxi_fleet_api_reference.Where(x => x.FleetId == fleet.Id).Select(x => x).ToList();
				}
			}

			return fleet;
		}

		public taxi_fleet GetFleet(long Id)
		{

			taxi_fleet fleet = null;

			using (VTODEntities context = new VTODEntities())
			{
				fleet = context.taxi_fleet.Where(x => x.Id == Id).Select(x => x).FirstOrDefault();
				if (fleet != null)
				{
					fleet.taxi_fleet_api_reference = context.taxi_fleet_api_reference.Where(x => x.FleetId == fleet.Id).Select(x => x).ToList();
				}
			}

			return fleet;
		}
        public vtod_trip RetrieveTripDetails(string dispatchaTripId)
        {

            vtod_trip trip = null;
            taxi_trip t = null;
            using (VTODEntities context = new VTODEntities())
            {
                t = context.taxi_trip.Where(x => x.DispatchTripId == dispatchaTripId).Select(x => x).FirstOrDefault();
                if (t != null)
                {
                    trip = context.vtod_trip.Where(x => x.Id == t.Id).Select(x => x).FirstOrDefault();
                }
                trip.taxi_trip = t;
            }

            return trip;
        }
		public void UpdateFinalStatus(long Id, string status, decimal? fare, decimal? gratuity, DateTime? tripStartTime, DateTime? tripEndTime, int? driverAcceptedEta)
		{
			using (VTODEntities context = new VTODEntities())
			{
				vtod_trip t = context.vtod_trip.Where(x => x.Id == Id).Select(x => x).FirstOrDefault();
				//t.FinalStatus = status;
				//t.TotalFareAmount = fare;
				//decimal? gratuity = null;
				decimal? totalFareAmount = fare;
				DateTime? newTripStartTime = null;
				DateTime? newTripEndTime = null;
				int? newDriverAcceptedEta = null;

				#region Update Fare
				#region Calculate Gratuity
				if (!gratuity.HasValue || gratuity.Value <= 0)
				{
					if (t.TotalFareAmount == null || t.TotalFareAmount.Value <= 0)
					{
						if (fare.HasValue && fare > 0)
						{
							if (!t.Gratuity.HasValue || t.Gratuity <= 0)
							{
								if (t.GratuityRate.HasValue && t.GratuityRate > 0)
								{
									gratuity = fare.Value * t.GratuityRate.Value / 100;
									//t.Gratuity = gratuity;
								}
							}
							else
							{
								gratuity = t.Gratuity;
							}
						}
					}
					else
					{
						gratuity = t.Gratuity;
					}
				}
				#endregion

				#region Calculate TotalFare
				if (t.TotalFareAmount == null || t.TotalFareAmount.Value <= 0)
				{
					if (fare.HasValue && fare > 0 && gratuity.HasValue && gratuity.Value > 0)
					{
						//t.TotalFareAmount = fare + gratuity;
						totalFareAmount = fare + gratuity;
					}
					else if (fare.HasValue && fare > 0)
					{
						totalFareAmount = fare;
					}
				}
				else
				{
					totalFareAmount = t.TotalFareAmount;
				}
				#endregion
				#endregion

				#region Update Trip Time
				if (t.TripStartTime == null)
					newTripStartTime = tripStartTime;
				else
					newTripStartTime = t.TripStartTime;

				if (t.TripEndTime == null)
					newTripEndTime = tripEndTime;
				else
					newTripEndTime = t.TripEndTime;
				#endregion

				#region DriverAcceptedETA
				if (!t.DriverAcceptedETA.HasValue)
				{
					newDriverAcceptedEta = driverAcceptedEta;
				}
				else
				{
					newDriverAcceptedEta = t.DriverAcceptedETA.Value;
				}
				#endregion

				//context.SaveChanges();

				context.SP_vtod_Update_FinalStatus(Id, status, totalFareAmount, gratuity, newTripStartTime, newTripEndTime, newDriverAcceptedEta);
			}
		}

		public void MarkThisTripAsError(long Id, string errorMessage)
		{
			using (VTODEntities context = new VTODEntities())
			{
				vtod_trip t = context.vtod_trip.Where(x => x.Id == Id).Select(x => x).FirstOrDefault();
				t.FinalStatus = TaxiTripStatusType.Error;
				t.Error = errorMessage;
				context.SaveChanges();
			}
		}

		public vtod_trip CreateNewTrip(TaxiBookingParameter tbp, string userName)
		{
			Stopwatch sw = new Stopwatch();
			sw.Start();
			Stopwatch swEachPart = new Stopwatch();
			var vtodTrip = new vtod_trip();
			var taxiTrip = new taxi_trip();
			GroundReservation reservation = tbp.request.GroundReservations.FirstOrDefault();
			TimeHelper utcTimeHelper = new TimeHelper();
			using (VTODEntities context = new VTODEntities())
			{
				swEachPart.Restart();
				#region fleetId and DNI
				taxiTrip.FleetId = tbp.Fleet.Id;
				//t.TripType = "Production";
				if (tbp.Fleet.UDI33DNI != null)
				{
					taxiTrip.APIInformation = tbp.Fleet.UDI33DNI.ToString();
				}
				else if ((tbp.Fleet.CCSiFleetId != null) && (tbp.Fleet.CCSiSource != null))
				{
					taxiTrip.APIInformation = string.Format("{0}_{1}", tbp.Fleet.CCSiSource, tbp.Fleet.CCSiFleetId);
				}

				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set fletId and DNI Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Counsumer Source, Device, Ref
				vtodTrip.ConsumerDevice = tbp.Device;
				vtodTrip.ConsumerSource = tbp.Source;
				vtodTrip.ConsumerRef = tbp.Ref;
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set Consumer Device, Source, Ref:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Expire
				double timeToExpire = 0;
				if (!double.TryParse(ConfigurationManager.AppSettings["Taxi_TimeToExpire"], out timeToExpire))
				{
					timeToExpire = 10;
				}
				DateTime dtExpires;
				if (DateTime.TryParse(reservation.Service.Location.Pickup.DateTime, out dtExpires))
				{
					taxiTrip.Expires = dtExpires.AddMinutes(timeToExpire);
				}
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set expire time Duration:{0} ms.", swEachPart.ElapsedMilliseconds);


				swEachPart.Restart();
				#region PickupTime and DropOffTime
				vtodTrip.PickupDateTime = tbp.PickupDateTime;
				
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set pickuptime and dropioffTime Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Get zone ID from taxi_fleet_zone, Get Consumer AccountNumber
				//Pickup
				if (tbp.PickupAddressType == AddressType.Address)
				{
					var zone = GetFleetZone(tbp.Fleet.Id, tbp.PickupAddress);
					if (zone != null)
					{
						taxiTrip.PickupFlatRateZone = zone.Name;
						logger.InfoFormat("Pickup flat rate zone={0}", taxiTrip.PickupFlatRateZone);
					}
				}
				else if (tbp.PickupAddressType == AddressType.Airport)
				{
					var zone = GetFleetZoneByAirport(tbp.Fleet.Id, tbp.PickupAirport);
					if (zone != null)
					{
						taxiTrip.PickupFlatRateZone = zone.Name;

						try
						{
							taxiTrip.PickupLatitude = System.Convert.ToDouble(zone.CenterLatitude);
							taxiTrip.PickupLongitude = System.Convert.ToDouble(zone.CenterLongitude);
						}
						catch { }

						logger.InfoFormat("Pickup flat rate zone={0}", taxiTrip.PickupFlatRateZone);
					}

				}

				//Dropoff
				if (tbp.DropOffAddressType == AddressType.Address)
				{
					var zone = GetFleetZone(tbp.Fleet.Id, tbp.DropOffAddress);
					if (zone != null)
					{
						taxiTrip.DropOffFlatRateZone = zone.Name;
						logger.InfoFormat("Dropoff flat rate zone={0}", taxiTrip.DropOffFlatRateZone);
					}
				}
				else if (tbp.DropOffAddressType == AddressType.Airport)
				{
					var zone = GetFleetZoneByAirport(tbp.Fleet.Id, tbp.DropoffAirport);
					if (zone != null)
					{
						taxiTrip.DropOffFlatRateZone = zone.Name;

						try
						{
							taxiTrip.DropOffLatitude = System.Convert.ToDouble(zone.CenterLatitude);
							taxiTrip.DropOffLongitude = System.Convert.ToDouble(zone.CenterLongitude);
						}
						catch { }

						logger.InfoFormat("Dropoff flat rate zone={0}", taxiTrip.DropOffFlatRateZone);
					}

				}

				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set zone Id for taxi_fleet_zone Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Get Account number
				//ToDo: it should not be hardcoded. It should read it from DB based on FleetID
				using (var db = new DataAccess.VTOD.VTODEntities())
				{
					var accountNumber = db.SP_Taxi_GetFleet_AccountNumber(taxiTrip.FleetId, tbp.tokenVTOD.Username).ToList().FirstOrDefault();
					if (!string.IsNullOrWhiteSpace(accountNumber))
					{
						taxiTrip.AccountNumber = accountNumber;
					}
				}
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set expire time Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region pickup address
				if (tbp.PickupAddressType == AddressType.Address)
				{
					taxiTrip.PickupAddressType = tbp.PickupAddressType;
					double pickupLat = 0;
					double pickupLongt = 0;
					if (Double.TryParse(tbp.PickupAddress.Geolocation.Latitude.ToString(), out pickupLat))
					{
						taxiTrip.PickupLatitude = pickupLat;
					}

					if (Double.TryParse(tbp.PickupAddress.Geolocation.Longitude.ToString(), out pickupLongt))
					{
						taxiTrip.PickupLongitude = pickupLongt;
					}

					taxiTrip.PickupStreetNo = tbp.PickupAddress.StreetNo;
					taxiTrip.PickupStreetName = tbp.PickupAddress.Street;
					taxiTrip.PickupStreetType = ExtractStreetType(tbp.PickupAddress.Street);
					taxiTrip.PickupAptNo = tbp.PickupAddress.ApartmentNo;
					taxiTrip.PickupCity = tbp.PickupAddress.City;
					taxiTrip.PickupStateCode = tbp.PickupAddress.State;
					taxiTrip.PickupZipCode = tbp.PickupAddress.ZipCode;
					taxiTrip.PickupCountryCode = tbp.PickupAddress.Country;
					taxiTrip.PickupFullAddress = tbp.PickupAddress.FullAddress;
				}
				else if (tbp.PickupAddressType == AddressType.Airport)
				{
					taxiTrip.PickupAddressType = tbp.PickupAddressType;
					taxiTrip.PickupAirport = tbp.PickupAirport;
					taxiTrip.PickupFullAddress = tbp.PickupAirport;
				}
				else
				{
					logger.Warn("No pickup location");
					taxiTrip.PickupAddressType = AddressType.Empty;
				}
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set pickup address Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region PickUp UTC Time
				try
				{
					if (taxiTrip.PickupLatitude != null && taxiTrip.PickupLongitude != null)
					{
						vtodTrip.PickupDateTimeUTC = utcTimeHelper.GetUTCFromLocalTime(taxiTrip.PickupLatitude, taxiTrip.PickupLongitude, tbp.PickupDateTime);
					}
					else
					{
						if (tbp.PickupAddressType == AddressType.Airport)
						{
							double? longitude;
							double? latitude;
							if (GetLongitudeAndLatitudeForAirport(taxiTrip.PickupAirport, out longitude, out latitude))
							{
								vtodTrip.PickupDateTimeUTC = utcTimeHelper.GetUTCFromLocalTime(taxiTrip.PickupLatitude, taxiTrip.PickupLongitude, tbp.PickupDateTime);
							}

						}
						else
						{
							vtodTrip.PickupDateTimeUTC = tbp.PickupDateTime.ToUtc(tbp.Fleet.ServerUTCOffset);
						}
					}
				}
				catch (Exception ex)
				{
					logger.InfoFormat("Exception in PickUpTime UTC:{0}", ex.Message);
					vtodTrip.PickupDateTimeUTC = tbp.PickupDateTime.ToUtc(tbp.Fleet.ServerUTCOffset);
				}
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set pick time utc Duration:{0} ms.", swEachPart.ElapsedMilliseconds);
				swEachPart.Restart();
				#region dropoff address
				//Dropoff is optioanl : By_Pouyan
				if (reservation.Service.Location.Dropoff != null)
				{
					double dropoffLat = 0;
					double dropoffLongt = 0;
					if (tbp.DropOffAddressType == AddressType.Address)
					{
						taxiTrip.DropOffAddressType = tbp.DropOffAddressType;
						if (tbp.DropOffAddress.Geolocation != null)
						{
							if (Double.TryParse(tbp.DropOffAddress.Geolocation.Latitude.ToString(), out dropoffLat))
							{
								taxiTrip.DropOffLatitude = dropoffLat;
							}
							if (Double.TryParse(tbp.DropOffAddress.Geolocation.Longitude.ToString(), out dropoffLongt))
							{
								taxiTrip.DropOffLongitude = dropoffLongt;
							}
						}

						taxiTrip.DropOffStreetNo = tbp.DropOffAddress.StreetNo;
						taxiTrip.DropOffStreetName = tbp.DropOffAddress.Street;
						taxiTrip.DropOffStreetType = ExtractStreetType(tbp.DropOffAddress.Street);
						taxiTrip.DropOffAptNo = tbp.DropOffAddress.ApartmentNo;
						taxiTrip.DropOffCity = tbp.DropOffAddress.City;
						taxiTrip.DropOffStateCode = tbp.DropOffAddress.State;
						taxiTrip.DropOffZipCode = tbp.DropOffAddress.ZipCode;
						taxiTrip.DropOffCountryCode = tbp.DropOffAddress.Country;
						taxiTrip.DropOffFullAddress = tbp.DropOffAddress.FullAddress;

					}
					else if (tbp.DropOffAddressType == AddressType.Airport)
					{
						taxiTrip.DropOffAddressType = tbp.DropOffAddressType;
						taxiTrip.DropOffAirport = tbp.DropoffAirport;
						taxiTrip.DropOffFullAddress = tbp.DropoffAirport;
					}
					else if (tbp.DropOffAddressType == AddressType.Empty)
					{
						taxiTrip.DropOffAddressType = AddressType.Empty;
						logger.Info("No drop off location");
					}
				}
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set dropoff address Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region common fields
				taxiTrip.ServiceAPIId = tbp.serviceAPIID;				
				taxiTrip.FirstName = tbp.FirstName;
				taxiTrip.LastName = tbp.LastName;
				taxiTrip.PhoneNumber = tbp.PhoneNumber;				
				taxiTrip.MinutesAway = null;
				taxiTrip.NumberOfPassenger = tbp.request.TPA_Extensions.Passengers.Any() ? tbp.request.TPA_Extensions.Passengers.Sum(x => x.Quantity) : 1;				
				vtodTrip.ConsumerRef = tbp.request.EchoToken;
				if (tbp.PickupAddressType == AddressType.Address)
				{
					taxiTrip.RemarkForPickup = reservation.Service.Location.Pickup.Remark;
				}
				if (tbp.DropOffAddressType == AddressType.Address)
				{
					taxiTrip.RemarkForDropOff = reservation.Service.Location.Dropoff.Remark;
				}
				taxiTrip.wheelchairAccessible = reservation.Service.DisabilityVehicleInd;

				vtodTrip.AppendTime = DateTime.Now;
				vtodTrip.FleetType = Common.DTO.Enum.FleetType.Taxi.ToString();
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set other common fields Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Payment
				if (tbp.PaymentType == Common.DTO.Enum.PaymentType.PaymentCard)
				{
					vtodTrip.PaymentType = tbp.PaymentType.ToString();
					vtodTrip.CreditCardID = tbp.CreditCardID.HasValue ? tbp.CreditCardID.Value.ToString() : null;
				}
				else
				{
					vtodTrip.PaymentType = tbp.PaymentType.ToString();
				}
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set payment type Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region MemberID
				vtodTrip.MemberID = tbp.MemberID.HasValue ? tbp.MemberID.Value.ToString() : null;
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set member Id:{0} ms.", swEachPart.ElapsedMilliseconds);


				swEachPart.Restart();
				#region Gratuity
				#region Check if it's percentage or amount
				if (!string.IsNullOrWhiteSpace(tbp.Gratuity))
				{
					var gratuity = tbp.Gratuity.GetPercentageAmount();
					//GratuityType gratuityType = GratuityType.Unknown;
					if (gratuity > 0)
					{
						//gratuityType = GratuityType.Percentage;
						vtodTrip.GratuityRate = gratuity;
					}
					else
					{
						gratuity = tbp.Gratuity.ToDecimal();
						vtodTrip.Gratuity = gratuity;
						//gratuityType = GratuityType.Amount;
					}
				}
				#endregion

				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set Gratuity Duration:{0} ms.", swEachPart.ElapsedMilliseconds);


				swEachPart.Restart();
				#region PromisedETA
				#region Check if it's percentage or amount
				vtodTrip.PromisedETA = tbp.PromisedETA;
				#endregion

				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set Gratuity Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region PickMeUpNow
				vtodTrip.PickMeUpNow = tbp.PickupNow;
				//if (v.PickupDateTimeUTC <= DateTime.UtcNow.AddMinutes(5))
				//{
				//    v.PickMeUpNow = true;
				//}
				//else
				//{
				//    v.PickMeUpNow = false;
				//}
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set Pickup now Duration:{0} ms.", swEachPart.ElapsedMilliseconds);


				swEachPart.Restart();
				#region User
				#region GetUser
				var user = context.my_aspnet_users.Where(s => s.name.ToLower() == userName.ToLower()).ToList().FirstOrDefault();
				vtodTrip.UserID = user.id;
				if (string.IsNullOrEmpty(vtodTrip.ConsumerDevice))
				{
					if (user != null)
					{
						vtodTrip.ConsumerDevice = user.name;
					}
				}
				#endregion

				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Set User Id Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Server Booking time
				vtodTrip.BookingServerDateTime = System.DateTime.Now;
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("BookingServerDateTime:{0} ms.", swEachPart.ElapsedMilliseconds);

				swEachPart.Restart();
				#region Email Address 
				if (!string.IsNullOrEmpty(tbp.EmailAddress))
				{
					vtodTrip.EmailAddress = tbp.EmailAddress;
				}
                #endregion

                if (tbp.IsBOBO.ToBool())
                {
                    vtodTrip.IsBOBO = true;
                    vtodTrip.BOBOMemberID = tbp.BOBOMemberID.Value.ToString();
                    vtodTrip.MemberID = "0"; //If it's bobo trip,  consumer needs to send 0 as memberID but it's not guaranteed
                }
                else
                {
                    vtodTrip.IsBOBO = false;
                }

                swEachPart.Stop();
				logger.DebugFormat("Email Address :{0} ms.", swEachPart.ElapsedMilliseconds);

				logger.Info("before we create a taxi trip");
				swEachPart.Restart();
				#region Save
				//context.taxi_trip.Add(t);
				vtodTrip.IsFlatRate = tbp.IsFixedPrice;
				vtodTrip.taxi_trip = taxiTrip;
				context.vtod_trip.Add(vtodTrip);
				context.SaveChanges();
				#endregion
				swEachPart.Stop();
				logger.DebugFormat("Save this trip Duration:{0} ms.", swEachPart.ElapsedMilliseconds);
				logger.Info("after we create a taxi trip");
			}

			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return vtodTrip;
		}


		public vtod_trip UpdateTaxiTrip(vtod_trip t, UDIXMLSchema.UDIBookingSuccess succ, int fleetTripCode)
		{
			using (VTODEntities context = new VTODEntities())
			{
				vtod_trip v = context.vtod_trip.Where(x => x.Id == t.Id).Select(x => x).FirstOrDefault();
				taxi_trip returnTaxiTrip = context.taxi_trip.Where(x => x.Id == t.Id).Select(x => x).FirstOrDefault();
				returnTaxiTrip.DispatchTripId = succ.Order.ConfirmationNo;
				returnTaxiTrip.NumOfTry++;
				v.AppendTime = DateTime.Now;
				v.FinalStatus = TaxiTripStatusType.Booked;
				v.FleetTripCode = fleetTripCode;
				context.SaveChanges();

				//
				t.taxi_trip.DispatchTripId = returnTaxiTrip.DispatchTripId;
				t.taxi_trip.NumOfTry++;
				t.AppendTime = v.AppendTime;
				t.FinalStatus = v.FinalStatus;



			}
			return t;
		}


        public bool UpdateModifyTaxiTrip(TaxiBookingParameter tp)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            Stopwatch swEachPart = new Stopwatch();
            vtod_trip v;
            taxi_trip t;
            taxi_trip_History h=new taxi_trip_History();
            TimeHelper utcTimeHelper = new TimeHelper();
            using (VTODEntities context = new VTODEntities())
            {
                v = context.vtod_trip.Where(x => x.Id == tp.Trip.Id).Select(x => x).FirstOrDefault();
                t= context.taxi_trip.Where(x => x.Id == tp.Trip.Id).Select(x => x).FirstOrDefault();
                h.PickupDateTime = v.PickupDateTime;
                h.PickUpFullAddress = t.PickupFullAddress;
                h.DropOffAddress = t.DropOffFullAddress;
                h.DispatchTripId = t.DispatchTripId;
                h.VtodTripId = t.Id.ToInt32();
                h.PaymentType = v.PaymentType;
                h.CreditCardID = v.CreditCardID;
                h.DirectBillAccountID = v.DirectBillAccountID;
                h.AppendTime = DateTime.Now;
                h.MemberID = v.MemberID;
                context.taxi_trip_History.Add(h);
                v.PickupDateTime = tp.PickupDateTime;
                //Pickup
                if (tp.PickupAddressType == AddressType.Address)
                {
                    var zone = GetFleetZone(tp.Fleet.Id, tp.PickupAddress);
                    if (zone != null)
                    {
                        t.PickupFlatRateZone = zone.Name;
                        logger.InfoFormat("Pickup flat rate zone={0}", t.PickupFlatRateZone);
                    }
                }
                else if (tp.PickupAddressType == AddressType.Airport)
                {
                    var zone = GetFleetZoneByAirport(tp.Fleet.Id, tp.PickupAirport);
                    if (zone != null)
                    {
                        t.PickupFlatRateZone = zone.Name;

                        try
                        {
                            t.PickupLatitude = System.Convert.ToDouble(zone.CenterLatitude);
                            t.PickupLongitude = System.Convert.ToDouble(zone.CenterLongitude);
                        }
                        catch { }

                        logger.InfoFormat("Pickup flat rate zone={0}", t.PickupFlatRateZone);
                    }

                }
                //Dropoff
                if (tp.DropOffAddressType == AddressType.Address)
                {
                    var zone = GetFleetZone(tp.Fleet.Id, tp.DropOffAddress);
                    if (zone != null)
                    {
                        t.DropOffFlatRateZone = zone.Name;
                        logger.InfoFormat("Dropoff flat rate zone={0}", t.DropOffFlatRateZone);
                    }
                }
                else if (tp.DropOffAddressType == AddressType.Airport)
                {
                    var zone = GetFleetZoneByAirport(tp.Fleet.Id, tp.DropoffAirport);
                    if (zone != null)
                    {
                        t.DropOffFlatRateZone = zone.Name;

                        try
                        {
                            t.DropOffLatitude = System.Convert.ToDouble(zone.CenterLatitude);
                            t.DropOffLongitude = System.Convert.ToDouble(zone.CenterLongitude);
                        }
                        catch { }

                        logger.InfoFormat("Dropoff flat rate zone={0}", t.DropOffFlatRateZone);
                    }

                }
                    var accountNumber = context.SP_Taxi_GetFleet_AccountNumber(tp.Fleet.Id, tp.tokenVTOD.Username).ToList().FirstOrDefault();
                    if (!string.IsNullOrWhiteSpace(accountNumber))
                    {
                        t.AccountNumber = accountNumber;
                    }
                swEachPart.Restart();
                #region pickup address
                if (tp.PickupAddressType == AddressType.Address)
                {
                    t.PickupAddressType = tp.PickupAddressType;
                    double pickupLat = 0;
                    double pickupLongt = 0;
                    if (Double.TryParse(tp.PickupAddress.Geolocation.Latitude.ToString(), out pickupLat))
                    {
                        t.PickupLatitude = pickupLat;
                    }

                    if (Double.TryParse(tp.PickupAddress.Geolocation.Longitude.ToString(), out pickupLongt))
                    {
                        t.PickupLongitude = pickupLongt;
                    }

                    t.PickupStreetNo = tp.PickupAddress.StreetNo;
                    t.PickupStreetName = tp.PickupAddress.Street;
                    t.PickupStreetType = ExtractStreetType(tp.PickupAddress.Street);
                    t.PickupAptNo = tp.PickupAddress.ApartmentNo;
                    t.PickupCity = tp.PickupAddress.City;
                    t.PickupStateCode = tp.PickupAddress.State;
                    t.PickupZipCode = tp.PickupAddress.ZipCode;
                    t.PickupCountryCode = tp.PickupAddress.Country;
                    t.PickupFullAddress = tp.PickupAddress.FullAddress;
                }
                else if (tp.PickupAddressType == AddressType.Airport)
                {
                    t.PickupAddressType = tp.PickupAddressType;
                    t.PickupAirport = tp.PickupAirport;
                    t.PickupFullAddress = tp.PickupAirport;
                }
                else
                {
                    logger.Warn("No pickup location");
                    t.PickupAddressType = AddressType.Empty;
                }
                #endregion
                swEachPart.Stop();
                logger.DebugFormat("Set pickup address Duration:{0} ms.", swEachPart.ElapsedMilliseconds);
                swEachPart.Restart();
                #region PickUp UTC Time
                try
                {
                    if (t.PickupLatitude != null && t.PickupLongitude != null)
                    {
                        v.PickupDateTimeUTC = utcTimeHelper.GetUTCFromLocalTime(t.PickupLatitude, t.PickupLongitude, tp.PickupDateTime);
                    }
                    else
                    {
                        if (tp.PickupAddressType == AddressType.Airport)
                        {
                            double? longitude;
                            double? latitude;
                            if (GetLongitudeAndLatitudeForAirport(t.PickupAirport, out longitude, out latitude))
                            {
                                v.PickupDateTimeUTC = utcTimeHelper.GetUTCFromLocalTime(t.PickupLatitude, t.PickupLongitude, tp.PickupDateTime);
                            }

                        }
                        else
                        {
                            v.PickupDateTimeUTC = tp.PickupDateTime.ToUtc(tp.Fleet.ServerUTCOffset);
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.InfoFormat("Exception in PickUpTime UTC:{0}", ex.Message);
                    v.PickupDateTimeUTC = tp.PickupDateTime.ToUtc(tp.Fleet.ServerUTCOffset);
                }
                #endregion
                swEachPart.Stop();
                logger.DebugFormat("Set pick time utc Duration:{0} ms.", swEachPart.ElapsedMilliseconds);
                #region dropoff address
                    double dropoffLat = 0;
                    double dropoffLongt = 0;
                    if (tp.DropOffAddressType == AddressType.Address)
                    {
                        t.DropOffAddressType = tp.DropOffAddressType;
                        if (tp.DropOffAddress.Geolocation != null)
                        {
                            if (Double.TryParse(tp.DropOffAddress.Geolocation.Latitude.ToString(), out dropoffLat))
                            {
                                t.DropOffLatitude = dropoffLat;
                            }
                            if (Double.TryParse(tp.DropOffAddress.Geolocation.Longitude.ToString(), out dropoffLongt))
                            {
                                t.DropOffLongitude = dropoffLongt;
                            }
                        }

                        t.DropOffStreetNo = tp.DropOffAddress.StreetNo;
                        t.DropOffStreetName = tp.DropOffAddress.Street;
                        t.DropOffStreetType = ExtractStreetType(tp.DropOffAddress.Street);
                        t.DropOffAptNo = tp.DropOffAddress.ApartmentNo;
                        t.DropOffCity = tp.DropOffAddress.City;
                        t.DropOffStateCode = tp.DropOffAddress.State;
                        t.DropOffZipCode = tp.DropOffAddress.ZipCode;
                        t.DropOffCountryCode = tp.DropOffAddress.Country;
                        t.DropOffFullAddress = tp.DropOffAddress.FullAddress;

                    }
                    else if (tp.DropOffAddressType == AddressType.Airport)
                    {
                        t.DropOffAddressType = tp.DropOffAddressType;
                        t.DropOffAirport = tp.DropoffAirport;
                        t.DropOffFullAddress = tp.DropoffAirport;
                    }
                    else if (tp.DropOffAddressType == AddressType.Empty)
                    {
                        t.DropOffAddressType = AddressType.Empty;
                        logger.Info("No drop off location");
                    }
                #endregion
                swEachPart.Stop();
                logger.DebugFormat("Set dropoff address Duration:{0} ms.", swEachPart.ElapsedMilliseconds);
                t.RemarkForPickup = tp.RemarkForPickup;
                t.RemarkForDropOff = tp.RemarkForDropOff;
                #region Payment
                swEachPart.Restart();
                if (tp.PaymentType == Common.DTO.Enum.PaymentType.PaymentCard)
                {
                    v.PaymentType = tp.PaymentType.ToString();
                    v.CreditCardID = tp.CreditCardID.HasValue ? tp.CreditCardID.Value.ToString() : null;
                }
                else
                {
                    v.PaymentType = tp.PaymentType.ToString();
                }

                swEachPart.Stop();
                logger.DebugFormat("Set payment type Duration:{0} ms.", swEachPart.ElapsedMilliseconds);
                #endregion
                v.PickMeUpNow = tp.PickupNow;
                context.SaveChanges();
                swEachPart.Stop();
                logger.DebugFormat("Modify this trip Duration:{0} ms.", swEachPart.ElapsedMilliseconds);
                logger.Info("After we modify a trip");
            }
            return true;
        }

        public void UpdateTaxiTrip(long tripId, string confirmationNo, int rezID, string status, int fleetTripCode)
		{
			using (VTODEntities context = new VTODEntities())
			{
				vtod_trip vTrip = context.vtod_trip.Where(x => x.Id == tripId).FirstOrDefault();
				vTrip.AppendTime = DateTime.Now;
				vTrip.FinalStatus = status;
				vTrip.FleetTripCode = fleetTripCode;
				taxi_trip returnTaxiTrip = context.taxi_trip.Where(x => x.Id == tripId).Select(x => x).FirstOrDefault();
				returnTaxiTrip.DispatchTripId = confirmationNo;
				returnTaxiTrip.NumOfTry++;
				returnTaxiTrip.RezId = rezID;
				context.SaveChanges();
			}

		}


		public vtod_trip UpdateTaxiTrip(vtod_trip t, string confirmationNo, int fleetTripCode)
		{
			using (VTODEntities context = new VTODEntities())
			{
				vtod_trip v = context.vtod_trip.Where(x => x.Id == t.Id).Select(x => x).FirstOrDefault();
				taxi_trip returnTaxiTrip = context.taxi_trip.Where(x => x.Id == t.Id).Select(x => x).FirstOrDefault();
				returnTaxiTrip.DispatchTripId = confirmationNo;
				returnTaxiTrip.NumOfTry++;
				v.AppendTime = DateTime.Now;
				v.FinalStatus = TaxiTripStatusType.Booked;
				v.FleetTripCode = fleetTripCode;
				context.SaveChanges();

				t.taxi_trip.DispatchTripId = returnTaxiTrip.DispatchTripId;
				t.taxi_trip.NumOfTry++;
				t.AppendTime = v.AppendTime;
				t.FinalStatus = v.FinalStatus;

			}
			return t;
		}

		//public taxi_trip UpdateTaxiTrip(taxi_trip t, string confirmationNo)
		//{
		//    vtod_trip v;
		//    taxi_trip returnTaxiTrip;
		//    using (VTODEntities context = new VTODEntities())
		//    {
		//        v = context.vtod_trip.Where(x => x.Id == t.Id).Select(x => x).FirstOrDefault();
		//        returnTaxiTrip = context.taxi_trip.Where(x => x.Id == t.Id).Select(x => x).FirstOrDefault();
		//        //trip = (taxi_trip)context.vtod_trip.Where(x => x.Id == t.Id).Select(x => x).FirstOrDefault();
		//        returnTaxiTrip.DispatchTripId = confirmationNo;
		//        returnTaxiTrip.NumOfTry++;
		//        v.AppendTime = DateTime.Now;
		//        v.FinalStatus = TaxiTripStatusType.Booked;
		//        context.SaveChanges();
		//    }
		//    return returnTaxiTrip;
		//}

		public void UpdateTaxiTrip(long Id, string cabNumber, int? minutesAway, string dispatchTripId, string driverName)
		{
			using (VTODEntities context = new VTODEntities())
			{
				vtod_trip v = context.vtod_trip.Where(x => x.Id == Id).Select(x => x).FirstOrDefault();
				var taxiTrip = context.taxi_trip.Where(x => x.Id == Id).Select(x => x).FirstOrDefault();
				//var trip = (taxi_trip)context.vtod_trip.Where(x => x.Id == Id).Select(x => x).FirstOrDefault();
				if (taxiTrip != null)
				{
					if (!string.IsNullOrWhiteSpace(cabNumber))
					{
						taxiTrip.CabNumber = cabNumber;
					}

					if (minutesAway.HasValue)
					{
						taxiTrip.MinutesAway = minutesAway.Value;
					}

					if (!string.IsNullOrWhiteSpace(dispatchTripId))
					{
						taxiTrip.DispatchTripId = dispatchTripId;
					}

					if (!string.IsNullOrWhiteSpace(driverName))
					{
						Person p = ExtractDriverName(driverName);
						v.DriverName = string.Format("{0} {1}", p.PersonName.GivenName, p.PersonName.Surname);
					}

					v.AppendTime = DateTime.Now;

					context.SaveChanges();
				}
			}
		}

		public bool IsAbleToCancelThisTrip(string status)
		{
			bool isAbleToCancel = true;
			using (VTODEntities context = new VTODEntities())
			{
				if (context.taxi_forbiddencancelstatus.Where(x => x.Status == status).Any())
				{
					isAbleToCancel = false;
				}
				else
				{
					isAbleToCancel = true;
				}

			}
			return isAbleToCancel;
		}

		//public bool VerifyNextTryForFastMeter(long tripID, out taxi_trip trip)
		//{
		//    bool returnValue = false;

		//    using (VTODEntities context = new VTODEntities())
		//    {


		//        trip = (taxi_trip)context.vtod_trip.Where(x => x.Id == tripID).Select(x => x).FirstOrDefault();

		//        if (trip != null)
		//        {
		//            long fleetID = trip.FleetId;
		//            taxi_fleet fleet = context.taxi_fleet.Where(x => x.Id == fleetID).Select(x => x).FirstOrDefault();

		//            if (fleet != null)
		//            {
		//                if (fleet.MaxNumOfRetryForFastMeter >= trip.NumOfTry)
		//                {
		//                    //do the booking
		//                    returnValue = true;
		//                }
		//                else
		//                {
		//                    //reach to maximum number of tries
		//                    logger.Info("reach to maximum number of tries for fast meter");
		//                    returnValue = false;
		//                }
		//            }
		//            else
		//            {
		//                returnValue = false;
		//                logger.ErrorFormat("Unable to find this fleet in DB. fleetID={0}", trip.FleetId);
		//                throw new Exception(Messages.Taxi_Common_UnableToFindFleetByFleetID);
		//            }
		//        }
		//        else
		//        {
		//            returnValue = false;
		//            logger.ErrorFormat("Unable to find this trip in DB. tripID={0}", tripID);
		//            throw new Exception(Messages.Taxi_TripNotFound);
		//        }
		//    }
		//    return returnValue;
		//}

		public TaxiStatusParameter ConvertTCPpToTSP(TaxiCancelParameter tcp)
		{
			TaxiStatusParameter tsp = new TaxiStatusParameter();
			try
			{
				tsp.EnableResponseAndLog = false;
				tsp.Fleet = GetFleet(tcp.Fleet.Id);
				//tsp.request = null;
				tsp.request = new OTA_GroundResRetrieveRQ { EchoToken = tcp.request.EchoToken, Target = tcp.request.Target, Version = tcp.request.Version, PrimaryLangID = tcp.request.PrimaryLangID };
				//response.EchoToken = tsp.request.EchoToken;
				//response.Target = tsp.request.Target;
				//response.Version = tsp.request.Version;
				//response.PrimaryLangID = tsp.request.PrimaryLangID;
				tsp.tokenVTOD = tcp.tokenVTOD;
				tsp.Trip = GetTaxiTrip(tcp.Trip.Id.ToString());
				tsp.Fleet_User = new taxi_fleet_user { AccountNumber = tcp.User.AccountNumber, AppendTime = tcp.User.AppendTime, FleetId = tcp.User.FleetId, Id = tcp.User.Id, UserId = tcp.User.UserId };
				tsp.User = GetUser(tcp.User.UserId);
			}
			catch
			{
				throw;
			}
			return tsp;
		}

		public TaxiBookingParameter ConvertTripToTBP(taxi_trip t)
		{
			TaxiBookingParameter tbp = new TaxiBookingParameter();

			try
			{
				//tbp.DropoffAddress = null;
				tbp.DropOffAddressType = t.DropOffAddressType;
				tbp.DropoffAirport = t.DropOffAirport;
				tbp.FirstName = t.FirstName;
				tbp.Fleet = GetFleet(t.FleetId);
				tbp.LastName = t.LastName;
				tbp.PhoneNumber = t.PhoneNumber;
				//tbp.PickupAddress=null;
				tbp.PickupAddressType = t.PickupAddressType;
				tbp.PickupAirport = t.PickupAirport;
				//tbp.request = null;
				//tbp.tokenVTOD = null;
				tbp.Trip = GetTaxiTrip(t.Id.ToString());
				//tbp.User = null;
				tbp.EnableResponseAndLog = false;
			}
			catch
			{
				throw;
			}
			return tbp;
		}

		//public taxi_trip GetTaxiTrip(string Id)
		//{
		//    taxi_trip t = null;
		//    long taxiTripId = 0;
		//    if (long.TryParse(Id, out taxiTripId))
		//    {
		//        t = GetTaxiTrip(taxiTripId);
		//    }

		//    return t;
		//}

		public vtod_trip GetTaxiTrip(string Id)
		{
			vtod_trip t = null;
			long taxiTripId = 0;
			if (long.TryParse(Id, out taxiTripId))
			{
				t = GetTaxiTrip(taxiTripId);
			}

			return t;
		}

		public vtod_trip GetTaxiTripByDispatchConfirmationId(string dispatchTripId, string firstName, string lastName, string phoneNumber)
		{
            taxi_trip taxiTrip = null;
			using (VTODEntities context = new VTODEntities())
			{              
				taxiTrip = context.taxi_trip.Where(x => x.DispatchTripId == dispatchTripId && ((x.FirstName == firstName && x.LastName == lastName) || x.PhoneNumber == phoneNumber)).Select(x => x).FirstOrDefault();				
			}

            if (taxiTrip != null)
            {
                return GetTaxiTrip(taxiTrip.Id);
            }
            else
            {
                throw new Exception(string.Format("Unable to find the trip by dispatch confirmation ID={0}, firstName={1}, lastName={2}", dispatchTripId, firstName, lastName));
            }
        }

		//public taxi_trip GetTaxiTrip(string Id)
		//{
		//    taxi_trip t = null;
		//    using (VTODEntities context = new VTODEntities())
		//    {
		//        long taxiTripId = 0;
		//        if (long.TryParse(Id, out taxiTripId))
		//        {
		//            var trip = context.taxi_trip.Where(x => x.Id == taxiTripId).Select(x => x).FirstOrDefault();
		//            //var trip = (taxi_trip)context.vtod_trip.Where(x => x.Id == taxiTripId).Select(x => x).FirstOrDefault();
		//            t = trip;
		//        }
		//    }
		//    return t;
		//}


		public vtod_trip GetTaxiTripForCCSi(string CCSiTripId, string CCSiFleetId)
		{
			vtod_trip t = null;
			long tripId = 0;
			using (VTODEntities context = new VTODEntities())
			{
				tripId = context.SP_Taxi_GetCCSiTrip(CCSiTripId, CCSiFleetId).Select(x => (long)x.Value).FirstOrDefault();
			}
			t = GetTaxiTrip(tripId.ToString());

			return t;
		}

		//public taxi_trip GetTaxiTripForCCSi(string CCSiTripId, string CCSiFleetId)
		//{
		//    taxi_trip t = null;
		//    using (VTODEntities context = new VTODEntities())
		//    {
		//        long tripId = context.SP_Taxi_GetCCSiTrip(CCSiTripId, CCSiFleetId).Select(x => (long)x.Value).FirstOrDefault();
		//        t = GetTaxiTrip(tripId.ToString());
		//    }

		//    return t;
		//}

		public string ConvertTripStatusForUDI33(string source)
		{
			string target = source;
			using (VTODEntities context = new VTODEntities())
			{
				var t = context.taxi_status_udi33wrapper.Where(x => x.Source == source).Select(x => x.Target).FirstOrDefault();
				if (!string.IsNullOrWhiteSpace(t))
				{
					target = t;
					logger.InfoFormat("Convert status from {0} to {1}", source, target);
				}
			}
			return target;
		}

		public string ConvertTripStatusForMTData(string source)
		{
			string target = source.ToString();
			using (VTODEntities context = new VTODEntities())
			{
				var t = context.taxi_status_mtdatawrapper.Where(x => x.Source == source.ToString()).Select(x => x.Target).FirstOrDefault();
				if (!string.IsNullOrWhiteSpace(t))
				{
					target = t;
					logger.InfoFormat("Convert status from {0} to {1}", source, target);
				}
			}
			return target;
		}

		public string ConvertTripStatusForMTData(BookingStatus source)
		{
			string target = source.ToString();
			using (VTODEntities context = new VTODEntities())
			{
				var t = context.taxi_status_mtdatawrapper.Where(x => x.Source == source.ToString()).Select(x => x.Target).FirstOrDefault();
				if (!string.IsNullOrWhiteSpace(t))
				{
					target = t;
					logger.InfoFormat("Convert status from {0} to {1}", source, target);
				}
			}
			return target;
		}

		public string ConvertTripStatusForCCSi(int sourceEventId, string sourceEventStatus, out string unknownStatus)
		{
			string target = "";
			unknownStatus = "";
			using (VTODEntities context = new VTODEntities())
			{
				if ((context.taxi_status_ccsiwrapper.Where(x => x.SourceEventId == sourceEventId).Select(x => x).Count() > 1))
				{
					if (context.taxi_status_ccsiwrapper.Where(x => x.SourceEventId == sourceEventId && x.SourceEventStatus == sourceEventStatus).Select(x => x).Count() == 1)
					{
						var t = context.taxi_status_ccsiwrapper.Where(x => x.SourceEventId == sourceEventId && x.SourceEventStatus == sourceEventStatus).Select(x => x.Target).FirstOrDefault();
						if (!string.IsNullOrWhiteSpace(t))
						{
							target = t;
							logger.InfoFormat("Convert status from eventId={0}, eventStatus={1} to {2}", sourceEventId, sourceEventStatus, target);
						}
						else
						{
							//unknown
							target = "Unkonwn";
							unknownStatus = string.Format("Convert status from eventId={0}, eventStatus={1} to {2}", sourceEventId, sourceEventStatus, target);
							logger.Error(unknownStatus);

						}
					}
					else
					{
						//unknown
					}
				}
				else if ((context.taxi_status_ccsiwrapper.Where(x => x.SourceEventId == sourceEventId).Select(x => x).Count() == 1))
				{
					var t = context.taxi_status_ccsiwrapper.Where(x => x.SourceEventId == sourceEventId).Select(x => x.Target).FirstOrDefault();
					if (!string.IsNullOrWhiteSpace(t))
					{
						target = t;
						logger.InfoFormat("Convert status from eventId={0} to {1}", sourceEventId, target);
					}
					else
					{
						//unknown
						target = "Unkonwn";
						unknownStatus = string.Format("Convert status from eventId={0}, eventStatus={1} to {2}", sourceEventId, sourceEventStatus, target);
						logger.Error(unknownStatus);
					}
				}
				else
				{
					//unknown
					target = "Unkonwn";
					unknownStatus = string.Format("Convert status from eventId={0}, eventStatus={1} to {2}", sourceEventId, sourceEventStatus, target);
					logger.Error(unknownStatus);
				}

			}
			return target;
		}

		public void SaveTripStatus(long tripID, string status, DateTime? statusTime, string vehicleNumber, decimal? vehicleLatitude, decimal? vehicleLongitude, decimal? fare, decimal? dispatchFare, int? eta, int? etaWithTraffic, string driverName, string comment, string originalStatus, string driverId)
		{
			using (VTODEntities context = new VTODEntities())
			{
				logger.InfoFormat("Trip ID={0}, status={1}", tripID, status);
				//taxi_trip_status tts = new taxi_trip_status { Status = status, TripID = tripID, AppendTime = DateTime.Now, StatusTime = statusTime, VehicleNumber = vehicleNumber, VehicleLongitude = vehicleLongitude, VehicleLatitude = vehicleLatitude, Fare = fare };
				//context.taxi_trip_status.Add(tts);
				//context.SaveChanges();
				context.SP_Taxi_InsertTripStatus(tripID, status, statusTime, vehicleNumber, vehicleLatitude, vehicleLongitude, fare, dispatchFare, eta, etaWithTraffic, driverName, driverId, comment, originalStatus);
			}
		}

		public void SaveTripStatus_ReDispatch(long tripID, DateTime? statusTime, string vehicleNumber, decimal? vehicleLatitude, decimal? vehicleLongitude, decimal? fare, decimal? dispatchFare, int? eta, int? etaWithTraffic, string driverName, string comment, string originalStatus, string driverId)
		{
			using (VTODEntities context = new VTODEntities())
			{
				using (TransactionScope tx = new TransactionScope())
				{
					try
					{
						context.SP_Taxi_InsertTripStatus(tripID, TaxiTripStatusType.FastMeter, statusTime, vehicleNumber, vehicleLatitude, vehicleLongitude, fare, dispatchFare, eta, etaWithTraffic, driverName, comment, originalStatus, driverId);
						logger.InfoFormat("Trip ID={0}, status={1}", tripID, TaxiTripStatusType.FastMeter);
						context.SP_Taxi_InsertTripStatus(tripID, TaxiTripStatusType.Booked, statusTime, vehicleNumber, vehicleLatitude, vehicleLongitude, fare, dispatchFare, eta, etaWithTraffic, driverName, comment, originalStatus, driverId);
						logger.InfoFormat("Trip ID={0}, status={1}", tripID, TaxiTripStatusType.Booked);
						tx.Complete();
					}
					catch (Exception ex)
					{
						logger.Error(ex.Message);
						logger.Error(ex.InnerException);
						logger.Error(ex.StackTrace);
						tx.Dispose();
					}
				}
			}
		}

		public void WriteTaxiLog(long? TripId, long Type, int Action, long? validationDuration, string request, string response, DateTime? sendRequest, DateTime? getResponse)
		{
			#region This is eliminated for having better performance. Everything should be logged in Log4Net
			//using (VTODEntities context = new VTODEntities())
			//{
			//	if (validationDuration.HasValue)
			//	{
			//		//Save validation duration
			//		context.SP_Taxi_InsertLogByValidation(TripId, Type, Action, validationDuration.Value, DateTime.Now);
			//	}
			//	else
			//	{
			//		//Save for request and response
			//		context.SP_Taxi_InsertLogByDateTime(TripId, Type, Action, request, response, sendRequest, getResponse, DateTime.Now);
			//	}
			//} 
			#endregion
		}

		public Person ExtractDriverName(string driverName)
		{
			Person p = null;
			if (!string.IsNullOrWhiteSpace(driverName))
			{
				p = new Person();
				p.PersonName = new PersonName();
				string[] sep = { " " };
				string[] results = driverName.Split(sep, StringSplitOptions.RemoveEmptyEntries);
				if (results.Length == 1)
				{
					p.PersonName.GivenName = driverName;
				}
				else if (results.Length >= 2)
				{
					p.PersonName.GivenName = string.Join(" ", results.Take(results.Length - 1).ToArray());
					p.PersonName.Surname = results[results.Length - 1];
				}
			}

			return p;
		}

		public decimal GetFareAmountForCCSi(long tripID)
		{
			decimal result = 0;
			using (VTODEntities context = new VTODEntities())
			{
				logger.InfoFormat("Trip ID={0}", tripID);
				//taxi_trip_status tts = new taxi_trip_status { Status = status, TripID = tripID, AppendTime = DateTime.Now, StatusTime = statusTime, VehicleNumber = vehicleNumber, VehicleLongitude = vehicleLongitude, VehicleLatitude = vehicleLatitude, Fare = fare };
				//context.taxi_trip_status.Add(tts);
				//context.SaveChanges();
				//context.SP_Taxi_InsertTripStatus(tripID, status, statusTime, vehicleNumber, vehicleLatitude, vehicleLongitude, fare, dispatchFare, eta, driverName, driverId, comment, originalStatus);

				var status = context.taxi_trip_status.Where(s => s.TripID == tripID && s.Status.ToLower() == "fare").FirstOrDefault();
				if (status != null && status.Fare.HasValue && status.Fare.Value > 0)
				{
					result = status.Fare.Value;
				}
			}

			return result;
		}

		public vtod_driver_info GetDriverInfo(vtod_trip trip)
		{
			vtod_driver_info result = null;
			if (trip != null && trip.DriverInfoID.HasValue && trip.DriverInfoID.Value > 0)
			{
				using (var db = new DataAccess.VTOD.VTODEntities())
				{
					result = db.vtod_driver_info.Where(s => s.Id == trip.DriverInfoID).FirstOrDefault();
				}
			}
			return result;
		}

		//public vtod_driver_info GetDriverInfo(taxi_trip trip)
		//{
		//    vtod_driver_info result = null;
		//    if (trip != null && trip.DriverInfoID.HasValue && trip.DriverInfoID.Value > 0)
		//    {
		//        using (var db = new DataAccess.VTOD.VTODEntities())
		//        {
		//            result = db.vtod_driver_info.Where(s => s.Id == trip.DriverInfoID).FirstOrDefault();
		//        }
		//    }
		//    return result;
		//}

		public vtod_driver_info GetDriverInfoByDriverId(string userName, vtod_trip trip, string status, string driverId)
		{
			vtod_driver_info result = null;			

			try
			{
				if (!string.IsNullOrWhiteSpace(userName))
				{
					if (ConfigHelper.IsTest())
					{
						status = TaxiTripStatusType.Accepted;
						trip.DriverInfoID = 0;
					}

					if (IsAllowedPullingDriverInfo(userName))
					{
						if (!trip.DriverInfoID.HasValue || trip.DriverInfoID.Value == 0)
						{
                            if (HasDriver(status))
                            {
                                result = GetDriverInfoFromSDSAndUpdate(driverId, "", trip, true);
                            }
						}
						else
						{
							if (trip.DriverInfoID.HasValue && trip.DriverInfoID.Value > 0)
							{
								using (var db = new DataAccess.VTOD.VTODEntities())
								{
									result = db.vtod_driver_info.Where(s => s.Id == trip.DriverInfoID.Value).FirstOrDefault();
								}
							}
						}
					}
				}
			}
			catch (System.Data.Entity.Validation.DbEntityValidationException ex)
			{
				foreach (var eve in ex.EntityValidationErrors)
				{
					logger.ErrorFormat("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
						eve.Entry.Entity.GetType().Name, eve.Entry.State);
					foreach (var ve in eve.ValidationErrors)
					{
						logger.ErrorFormat("- Property: \"{0}\", Error: \"{1}\"",
							ve.PropertyName, ve.ErrorMessage);
					}
				}
				throw;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("GetDriverInfo: {0}", ex.ToString() + ex.StackTrace);
			}

			return result;
		}
        

        public vtod_driver_info GetDriverInfoByVehicleNumber(string userName, string status, string vehicleNumber, string driverNumber, vtod_trip trip)
        {
			vtod_driver_info result = null;			

			try
			{
				if (!string.IsNullOrWhiteSpace(userName))
				{	
                    
                    if (IsAllowedPullingDriverInfo(userName) && trip != null)
					{
						if (!trip.DriverInfoID.HasValue || trip.DriverInfoID.Value == 0)
						{
							if (HasDriver(status))
							{
                                result = GetDriverInfoFromSDSAndUpdate(driverNumber, vehicleNumber, trip);
                            }
						}                        
					}
                    //It is IVR trip if trip is null 
                    //In IVR case, we don't need to save driver info
                    else
                    {
                        if (trip.DriverInfoID.HasValue && trip.DriverInfoID.Value > 0)
                        {
                            using (var db = new DataAccess.VTOD.VTODEntities())
                            {
                                result = db.vtod_driver_info.Where(s => s.Id == trip.DriverInfoID.Value).FirstOrDefault();
                            }
                        }
                    }
                }
			}
			catch (System.Data.Entity.Validation.DbEntityValidationException ex)
			{
				foreach (var eve in ex.EntityValidationErrors)
				{
					logger.ErrorFormat("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
						eve.Entry.Entity.GetType().Name, eve.Entry.State);
					foreach (var ve in eve.ValidationErrors)
					{
						logger.ErrorFormat("- Property: \"{0}\", Error: \"{1}\"",
							ve.PropertyName, ve.ErrorMessage);
					}
				}
				throw;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("GetDriverInfo: {0}", ex.ToString() + ex.StackTrace);
			}

			return result;
		}

		//public taxi_trip GetTaxiTrip(long tripId)
		//{
		//    taxi_trip v = null;
		//    using (VTODEntities context = new VTODEntities())
		//    {
		//        v = context.taxi_trip.Where(x => x.Id == tripId).Select(x => x).FirstOrDefault();
		//    }
		//    return v;
		//}

		public vtod_trip GetTaxiTrip(long tripId)
		{			
			using (VTODEntities context = new VTODEntities())
			{
                return context.vtod_trip.Include("taxi_trip").Where(x => x.Id == tripId).Select(x => x).FirstOrDefault();				
			}			
		}

		/// <summary>
		/// select all synonyms of a word
		/// </summary>
		/// <param name="word"></param>
		/// <returns></returns>
		public List<string> GetSynonyms(string word)
		{
			using (var db = new VTODEntities())
			{
                return db.vtod_synonym.Where(m => m.Key == word).Select(m => m.Value.ToLower().Trim()).ToList();
            }
		}

		public string GetDispatchTripId(long vtodTripId)
		{
			string dispatchTripId = string.Empty;
			using (VTODEntities context = new VTODEntities())
			{
				dispatchTripId = context.taxi_trip.Where(x => x.Id == vtodTripId).FirstOrDefault().DispatchTripId;
			}
			return dispatchTripId;
		}

		public long? GetVTODTaxiTripId(string dispatchTripId, string firstName, string lastName, string phoneNumber)
		{
			long? vtodTripId = null;
			using (VTODEntities context = new VTODEntities())
			{
				if (context.taxi_trip.Where(x => x.DispatchTripId == dispatchTripId && x.FirstName == firstName && x.LastName == lastName).Any())
				{
					vtodTripId = context.taxi_trip.Where(x => x.DispatchTripId == dispatchTripId && x.FirstName == firstName && x.LastName == lastName).FirstOrDefault().Id;
				}
				else if (context.taxi_trip.Where(x => x.DispatchTripId == dispatchTripId && x.PhoneNumber == phoneNumber).Any())
				{
					vtodTripId = context.taxi_trip.Where(x => x.DispatchTripId == dispatchTripId && x.PhoneNumber == phoneNumber).FirstOrDefault().Id;
				}

			}
			return vtodTripId;
		}

		#endregion


		#region Private method

		private void GetPickMeupOption(taxi_fleet f, out bool PickMeUpNow, out bool PickMeUpLater)
		{
			PickMeUpNow = true;
			PickMeUpLater = true;

			#region Get Pick me up value
			if (f.PickMeUpNowOption == PickMeUpNowOptionConst.Anytime)
			{
				PickMeUpNow = true;
				PickMeUpLater = true;
			}
			else if (f.PickMeUpNowOption == PickMeUpNowOptionConst.DontPickMeUp)
			{
				PickMeUpNow = false;
				PickMeUpLater = false;
			}
			else if (f.PickMeUpNowOption == PickMeUpNowOptionConst.OnlyNow)
			{
				PickMeUpNow = true;
				PickMeUpLater = false;
			}
			else if (f.PickMeUpNowOption == PickMeUpNowOptionConst.OnlyFuture)
			{
				PickMeUpNow = false;
				PickMeUpLater = true;
			}
			#endregion
		}


        private void CheckBlackOut(taxi_fleet fleet, OTA_GroundBookRQ request)
        {
             //Filter has been removed 
            string requestedPickupTimeString = request.GroundReservations.FirstOrDefault().Service.Location.Pickup.DateTime;

            DateTime requestedPickupTime;
            if (DateTime.TryParse(requestedPickupTimeString, out requestedPickupTime))
            {
                using (VTODEntities context = new VTODEntities())
                {
                    taxi_fleet_blackout blackoutInfo = context.taxi_fleet_blackout.Where(p => p.FleetId == fleet.Id).SingleOrDefault();

                    if (blackoutInfo != null)
                    {
                        if (blackoutInfo.BlackoutStartFleetLocalTime.HasValue && blackoutInfo.BlackoutEndFleetLocalTime.HasValue)
                        {
                            if (requestedPickupTime >= blackoutInfo.BlackoutStartFleetLocalTime && requestedPickupTime <= blackoutInfo.BlackoutEndFleetLocalTime)
                            {
                                throw VtodException.CreateException(ExceptionType.Taxi, 2013);
                            }
                        }
                    }
                }
            }
        }        

        private bool CheckFastMeterPreviousTripStatus(long tripId)
		{
			bool hasPreviousFastMeter = false;

			using (VTODEntities context = new VTODEntities())
			{

				if (context.taxi_trip_status.Where(x => x.TripID == tripId && x.Status == "FastMeter").Any())
				{
					hasPreviousFastMeter = true;
				}
				else
				{
					hasPreviousFastMeter = false;
				}

			}

			return hasPreviousFastMeter;
		}

		private string ExtractStreetType(string input)
		{
			string[] sep = { " " };
			string result = "";
			if (!string.IsNullOrWhiteSpace(input))
			{
				result = input.Split(sep, StringSplitOptions.RemoveEmptyEntries).LastOrDefault();
			}
			return result;
		}

		private string ExtractForceZone(string forceZone, Map.DTO.Address address)
		{
			string result;
			int forceZoneNumber;

			if (int.TryParse(forceZone, out forceZoneNumber))
			{
				//Digits
				result = forceZoneNumber.ToString();
			}
			else
			{
				//GPX
				XDocument doc = XDocument.Parse(forceZone);
				MapService map = new MapService();
				result = map.FindZone(address.Geolocation, doc).Name;
			}

			return result;
		}

		private string GetAddressString(Address ad)
		{
			string pickupAddressStr = string.Format("{0} {1} {2} {3} {4} {5}", ad.StreetNmbr, ad.AddressLine, ad.CityName, ad.PostalCode, ad.StateProv.StateCode, ad.CountryName.Code);
			return pickupAddressStr;
		}

		private Map.DTO.Address ConvertAddress(Pickup_Dropoff_Stop stop)
		{
			Map.DTO.Address address = new Map.DTO.Address();
			address.AddressName = "";
			address.ApartmentNo = stop.Address.BldgRoom;
			address.City = stop.Address.CityName;

			//hard code
			//1. USA--> US
			//2. empty --> US
			if ((stop.Address.CountryName.Code.Equals("USA")) || (string.IsNullOrWhiteSpace(stop.Address.CountryName.Code)))
			{
				address.Country = "US";
			}
			else
			{
				address.Country = stop.Address.CountryName.Code;
			}

			//address.County = "";
			address.FullAddress = string.Format("{0} {1} {2} {3} {4} {5}", stop.Address.StreetNmbr, stop.Address.AddressLine, stop.Address.CityName, stop.Address.PostalCode, stop.Address.StateProv.StateCode, stop.Address.CountryName.Code);
			address.FullStreet = stop.Address.AddressLine;
			decimal longtitude = 0;
			decimal latitude = 0;
			if (decimal.TryParse(stop.Address.Latitude, out latitude) && decimal.TryParse(stop.Address.Longitude, out longtitude))
			{
				address.Geolocation = new Map.DTO.Geolocation { Latitude = latitude, Longitude = longtitude };
			}
			//else {
			//	throw new Exception(Messages.Taxi_Common_InvalidReservationServiceLocationInLatAndLong);
			//}
			address.SimpleAddress = null;


			address.State = ConvertUSStateCode(stop.Address.StateProv.StateCode);


			address.Street = stop.Address.AddressLine;
			address.StreetNo = stop.Address.StreetNmbr;
			address.ZipCode = stop.Address.PostalCode;

			if (!string.IsNullOrWhiteSpace(stop.Address.LocationName))
			{
				address.AddressName = stop.Address.LocationName;
			}


			return address;
		}

		private string ConvertUSStateCode(string name)
		{
			string code;

			using (VTODEntities context = new VTODEntities())
			{
				code = context.vtod_usstatecode.Where(x => x.Name == name).Select(x => x.Code).FirstOrDefault();
				if (string.IsNullOrWhiteSpace(code))
				{
					code = name;
				}
			}
			return code;
		}

		private bool GetAccountNumber(OTA_GroundBookRQ request, long fleetId, int userId, out string accountNumber)
		{
			Stopwatch sw = new Stopwatch();
			sw.Start();
			accountNumber = null;
			bool result = false;

			using (VTODEntities context = new VTODEntities())
			{
				accountNumber = context.taxi_fleet_user.Where(x => x.FleetId == fleetId && x.UserId == fleetId).Select(x => x.AccountNumber).FirstOrDefault();
			}

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return result;
		}

		private bool LookupAddress(double longtitude, double latutude, out Map.DTO.Address returnAddress)
		{
			Stopwatch sw = new Stopwatch();
			sw.Start();
			bool result = false;
			returnAddress = null;
			string errorMessage = "";
			MapService ms = new MapService();
			Map.DTO.Geolocation location = new Map.DTO.Geolocation { Longitude = decimal.Parse(longtitude.ToString()), Latitude = decimal.Parse(latutude.ToString()) };
			var addresses = ms.GetAddressesFromGeoLocation(location, out errorMessage);
			if (string.IsNullOrWhiteSpace(errorMessage))
			{
				if (addresses.Count > 1)
				{
					logger.WarnFormat("Multiple addresses found. Source longtitude={0}, latitude={1}", longtitude, latutude);
					logger.WarnFormat("Pickup the first one address= {0}", addresses.FirstOrDefault().FullAddress);
				}
				else
				{
					logger.InfoFormat("Pickup the first one address= {0}", addresses.FirstOrDefault().FullAddress);
				}
				returnAddress = addresses.FirstOrDefault();

				//change Zip Code
				if (returnAddress.Country == "US")
				{
					returnAddress.ZipCode = returnAddress.ZipCode.Substring(0, 5);
				}
				if (returnAddress.Country == "CA")
				{
					returnAddress.ZipCode = returnAddress.ZipCode.Substring(0, 6);
				}
				result = true;
			}
			else
			{
				logger.ErrorFormat("Unable to look up address. Source longtitude={0}, latitude={1}", longtitude, latutude);
				result = false;
			}

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return result;
		}

		private bool LookupAddress(Pickup_Dropoff_Stop stop, string sourceAddress, out Map.DTO.Address returnAddress)
		{
			Stopwatch sw = new Stopwatch();
			sw.Start();
			bool result = false;
			returnAddress = null;
			string errorMessage = "";
			MapService ms = new MapService();
			var addresses = ms.GetAddresses(sourceAddress, out errorMessage);
			if (string.IsNullOrWhiteSpace(errorMessage))
			{
				if (addresses.Count > 1)
				{
					logger.WarnFormat("Multiple addresses found. Source address = {0}", sourceAddress);
					logger.WarnFormat("Pickup the first one address= {0}", addresses.FirstOrDefault().FullAddress);
				}
				else
				{
					logger.InfoFormat("Pickup the first one address= {0}", addresses.FirstOrDefault().FullAddress);
				}
				returnAddress = addresses.FirstOrDefault();

				//change Zip Code
				if (!string.IsNullOrWhiteSpace(returnAddress.ZipCode))
				{
					if (returnAddress.Country == "US")
					{
						returnAddress.ZipCode = returnAddress.ZipCode.Substring(0, 5);
					}
					if (returnAddress.Country == "CA")
					{
						returnAddress.ZipCode = returnAddress.ZipCode.Substring(0, 6);
					}
				}

				returnAddress.ApartmentNo = stop.Address.BldgRoom;

				result = true;
			}
			else
			{
				logger.ErrorFormat("Unable to look up address. Source address = {0}", sourceAddress);
				result = false;
			}

			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return result;
		}

		public taxi_fleet_api_reference GetTaxiFleetServiceAPIPreference(long fleetID, string functionName)
		{
			taxi_fleet_api_reference preference = new taxi_fleet_api_reference();
			using (VTODEntities context = new VTODEntities())
			{
				preference = context.taxi_fleet_api_reference.Where(x => x.FleetId == fleetID && x.Name == functionName).Select(x => x).FirstOrDefault();
			}
			return preference;
		}
        public taxi_mtdata_configuration GetConfigurationNames(long fleetID)
        {
            taxi_mtdata_configuration configurations = new taxi_mtdata_configuration();
            using (VTODEntities context = new VTODEntities())
            {
                configurations = context.taxi_mtdata_configuration.Where(x => x.FleetId == fleetID).Select(x => x).FirstOrDefault();
            }
            return configurations;
        }
        private bool GetFleet(string pickupAddressType, Map.DTO.Address address, string airport, out taxi_fleet fleet)
		{
			Stopwatch sw = new Stopwatch();
			sw.Start();
			bool result = false;
			fleet = null;

			using (VTODEntities context = new VTODEntities())
			{
				if (pickupAddressType == AddressType.Address)
				{
					#region Priority #1: find by Coordinate
					//by Coordinate (Lat/Long)
					if (fleet == null)
						if (address != null && address.Geolocation != null && address.Geolocation.Latitude != 0 && address.Geolocation.Longitude != 0)
						{
							fleet = context.SP_Taxi_GetFleetByCoordinate2(address.Geolocation.Latitude, address.Geolocation.Longitude).FirstOrDefault();

						}
					#endregion

					#region Priority #2: find by zip code
					//by Zip code
					if (fleet == null)
					{
						fleet = context.SP_Taxi_GetFleetByZipCountry2(address.ZipCode, address.Country).Select(x => x).FirstOrDefault();

					}
					#endregion

					#region Priority #3: find by city
					//by City name, State, Country
					if (fleet == null)
					{
						fleet = context.SP_Taxi_GetFleetByCityStateCountry2(address.City, address.State, address.Country).Select(x => x).FirstOrDefault();

					}
					#endregion
				}
				else if (pickupAddressType == AddressType.Airport)
				{
					#region Priority #4 by Airport (for this version, landmark is just Airport)
					//by airport
					fleet = context.SP_Taxi_GetFleetByAirport2(airport).FirstOrDefault();

					#endregion

				}

				if (fleet == null)
				{
					result = false;
					string error = string.Format("Unable to find correct fleet with this address");
					throw VtodException.CreateException(ExceptionType.Taxi, 1011);// new ValidationException(error);
				}
				else
				{
					result = true;
				}
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return result;
		}

		private bool GetFleet(long fleetId, out taxi_fleet fleet)
		{
			Stopwatch sw = new Stopwatch();
			sw.Start();
			bool result = false;
			fleet = null;
			using (VTODEntities context = new VTODEntities())
			{
				fleet = context.taxi_fleet.Where(x => x.Id == fleetId).Select(x => x).FirstOrDefault();
				if (fleet != null)
				{
					result = true;
				}
				else
				{
					result = false;
				}
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return result;
		}

		internal bool GetFleet(string pickupAddressType, Address address, string airport, out taxi_fleet fleet)
		{
			Stopwatch sw = new Stopwatch();
			sw.Start();
			fleet = null;

			using (VTODEntities context = new VTODEntities())
			{
				if (pickupAddressType == AddressType.Address)
				{
					#region Priority #1: find by Coordinate
					//by Coordinate (Lat/Long)
					if (address != null && !string.IsNullOrWhiteSpace(address.Latitude) && !string.IsNullOrWhiteSpace(address.Longitude))
					{
						fleet = context.SP_Taxi_GetFleetByCoordinate2(decimal.Parse(address.Latitude), decimal.Parse(address.Longitude)).FirstOrDefault();
					}
					#endregion

					#region Priority #2: find by zip code
					//by Zip code
					if (address != null && fleet == null && address.CountryName != null)
					{
						fleet = context.SP_Taxi_GetFleetByZipCountry2(address.PostalCode, address.CountryName.Code).Select(x => x).FirstOrDefault();
					}
					#endregion

					#region Priority #3: find by city
					//by City name, State, Country
					if (address != null && fleet == null && address.StateProv != null && address.CountryName != null)
					{
						fleet = context.SP_Taxi_GetFleetByCityStateCountry2(address.CityName, address.StateProv.StateCode, address.CountryName.Code).Select(x => x).FirstOrDefault();
					}
					#endregion
				}
				else if (pickupAddressType == AddressType.Airport)
				{
					#region Priority #4 by Airport (for this version, landmark is just Airport)
					//by airport
					fleet = context.SP_Taxi_GetFleetByAirport2(airport).FirstOrDefault();

					#endregion
				}

				if (fleet == null)
				{
					//string error = string.Format("Unable to find correct fleet with this address");
					throw VtodException.CreateException(ExceptionType.Taxi, 1011);
				}
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return true;
		}

		private bool GetTaxiFleetUser(long fleetId, string userName, out taxi_fleet_user user)
		{

			Stopwatch sw = new Stopwatch();
			sw.Start();
			bool result = false;
			user = null;
			using (VTODEntities context = new VTODEntities())
			{
				//user = context.SP_Taxi_GetFleet_User(fleetId, userName).Select(x => x).FirstOrDefault();
				user = context.SP_Taxi_GetFleet_User2(fleetId, userName).Select(x => x).FirstOrDefault();
				if (user != null)
				{
					result = true;
				}
				else
				{
					result = false;
				}
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return result;
		}

		public my_aspnet_users GetUser(long Id)
		{
			Stopwatch sw = new Stopwatch();
			sw.Start();
			my_aspnet_users user = null;
			using (VTODEntities context = new VTODEntities())
			{
				user = context.my_aspnet_users.Where(x => x.id == Id).Select(x => x).FirstOrDefault();
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return user;
		}

		public List<string> GetRemarks(string userName, long fleetID)
		{
			List<string> remarksList = new List<string>();
			Stopwatch sw = new Stopwatch();
			sw.Start();
			SP_Taxi_GetRemarksForPickUp_DropOff_Result remarks = null;
			using (VTODEntities context = new VTODEntities())
			{
				remarks = context.SP_Taxi_GetRemarksForPickUp_DropOff(userName, fleetID).FirstOrDefault();
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			if (remarks != null)
			{
				remarksList.Add(remarks.RemarkForPickUpCash);
				remarksList.Add(remarks.RemarkForDropOffCash);
				remarksList.Add(remarks.RemarkForPickUpCreditCard);
				remarksList.Add(remarks.RemarkForDropOffCreditCard);
			}
			return remarksList.ToList<string>();
		}

		private taxi_fleet_zone GetFleetZone(long fleetId, Map.DTO.Address address)
		{
			Stopwatch sw = new Stopwatch();
			sw.Start();

			taxi_fleet_zone result = null;
			using (VTODEntities context = new VTODEntities())
			{
				try
				{
					//find fleet zone by geolocation
					if (address != null && address.Geolocation != null && address.Geolocation.Latitude != 0 && address.Geolocation.Longitude != 0)
					{
						result = context.SP_Taxi_GetFleetZone(fleetId, address.Geolocation.Latitude, address.Geolocation.Longitude).FirstOrDefault();
					}

					//find flett zone by zip
					if (result == null)
					{
						result = context.SP_Taxi_GetFlletZoneByZip(fleetId, address.ZipCode.ToString()).FirstOrDefault();
					}

					//find flett zone by city
					if (result == null)
					{
						result = context.SP_Taxi_GetFlletZoneByCity(fleetId, address.City.ToString()).FirstOrDefault();
					}
				}
				catch { }
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return result;
		}

		private taxi_fleet_zone GetFleetZoneByAirport(long fleetId, string airportName)
		{
			Stopwatch sw = new Stopwatch();
			sw.Start();

			taxi_fleet_zone result = null;
			using (VTODEntities context = new VTODEntities())
			{
				try
				{
					if (fleetId > 0 && !string.IsNullOrWhiteSpace(airportName))
					{
						result = context.SP_Taxi_GetFleetZoneByAirport(fleetId, airportName).FirstOrDefault();
					}
				}
				catch { }
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return result;
		}

		private string ExtractPhoneNumber(Telephone phone)
		{
			string result = string.Format("{0}{1}{2}", phone.CountryAccessCode, phone.AreaCityCode, phone.PhoneNumber).Trim();
			result = ExtractDigits(result);
			return result;
		}

		private bool IsAbleToBookTrip(TaxiBookingParameter tbp)
		{
			bool result = false;

			using (VTODEntities context = new VTODEntities())
			{
				#region new logic
				int blockInterval = -10;
				blockInterval = tbp.Fleet.RestrictBookingMins.HasValue ? tbp.Fleet.RestrictBookingMins.Value : 10;

				int? duplicateTrips = null;

				if (tbp.Fleet.RestrictBookingOption == RestrictBookingOption.On)
				{
					if (tbp.PickupAddressType == AddressType.Address)
					{
						//by addresss
						duplicateTrips = context.SP_Taxi_IsDuplicateBookingByAddress(tbp.FirstName, tbp.LastName, tbp.PhoneNumber, tbp.PickupAddress.FullAddress, tbp.PickupAddress.Street, tbp.PickupAddress.City, tbp.PickupAddress.Country, tbp.PickupDateTime, blockInterval, tbp.MemberID.HasValue ? tbp.MemberID.Value.ToString() : null).FirstOrDefault();
						//duplicateTrips = context.SP_Taxi_IsDuplicateBookingByAddress(tbp.FirstName, tbp.LastName, tbp.PhoneNumber, tbp.PickupAddress.FullAddress, tbp.PickupAddress.Street, tbp.PickupAddress.City, tbp.PickupAddress.Country, blockedDateTime).FirstOrDefault();
					}
					else if (tbp.PickupAddressType == AddressType.Airport)
					{
						//by airport
						duplicateTrips = context.SP_Taxi_IsDuplicateBookingByAirport(tbp.FirstName, tbp.LastName, tbp.PhoneNumber, tbp.PickupAirport, tbp.PickupDateTime, blockInterval, tbp.MemberID.HasValue ? tbp.MemberID.Value.ToString() : null).FirstOrDefault();
						//duplicateTrips = context.SP_Taxi_IsDuplicateBookingByAirport(tbp.FirstName, tbp.LastName, tbp.PhoneNumber, tbp.PickupAirport, blockInterval).FirstOrDefault();
					}
				}
				else
				{
					//bypass duplication check
					duplicateTrips = 0;
					logger.Warn("Bypass duplication check");
				}

				if (duplicateTrips.HasValue)
				{
					if (duplicateTrips.Value > 0)
					{
						result = false;
						logger.Warn("Found duplicated trips");
					}
					else
					{
						result = true;
						logger.Info("No duplicated trip");
					}
				}
				else
				{
					string error = string.Format("Unable to look up check duplicated trip");
					throw new Exception(error);
				}

				#endregion
			}
			return result;
		}

        public bool IsAllowedPullingDriverInfo(string userName)
        {
            int taxiUserNameForPulling_Count = 0;

            try
            {
                var taxiUserNameForPulling = System.Configuration.ConfigurationManager.AppSettings["Taxi_UserName_For_Pulling_DriverInfo"];
                if (!string.IsNullOrWhiteSpace(taxiUserNameForPulling))
                {
					//TODO : Need to dobule check to replace count with contain
					if (taxiUserNameForPulling.Contains(","))
                        taxiUserNameForPulling_Count =
                            taxiUserNameForPulling.Split(',')
                                .Where(s => !string.IsNullOrEmpty(s) && s.Trim().ToLower() == userName.Trim().ToLower())
                                .Count();
                    else
                        taxiUserNameForPulling_Count = 1;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                logger.Error(ex.InnerException);
                logger.Error(ex.StackTrace);
            }

            return taxiUserNameForPulling_Count > 0 ? true : false;
        }

        


        private vtod_driver_info GetDriverInfoFromSDSAndUpdate(string driverNumber, string vehicleNumber, vtod_trip trip, bool isCalledFromGetDriverInfoByDriverId = false)
        {
            vtod_driver_info result = null;

            #region Get Driver Info from SDS
            //use test ds if we're in test mode
            var taxiDrvController = new UDI.SDS.TaxiDrvController(TrackTime);
            DataSet drvResult;
            if (isCalledFromGetDriverInfoByDriverId)
            {
                drvResult = !ConfigHelper.IsTest() ? taxiDrvController.GetTaxiDriverInfo(trip.taxi_trip.FleetId.ToString(), driverNumber, "") : taxiDrvController.GetTaxiDriverInfo("16", "", "726");
            }
            else
            {
                drvResult = taxiDrvController.GetTaxiDriverInfo(trip.taxi_trip.FleetId.ToString(), string.IsNullOrWhiteSpace(driverNumber) ? "" : driverNumber, vehicleNumber);
            }
            #endregion


            if (drvResult != null && drvResult.Tables.Count > 0)
            {
                var drvDataTable = drvResult.Tables[0];
                if (drvDataTable != null)
                {
                    if (drvDataTable.Rows != null && drvDataTable.Rows.Count > 0)
                    {
                        var newDriver = new vtod_driver_info();
                        byte[] driverImage = null;
                        foreach (DataRow row in drvDataTable.Rows)
                        {
                            newDriver.DriverID = row["DriverID"].ToString();
                            newDriver.VehicleID = row["VehicleID"].ToString();
                            newDriver.FirstName = row["FirstName"].ToString();
                            newDriver.LastName = row["LastName"].ToString();
                            newDriver.CellPhone = row["CellPhone"].ToString();
                            newDriver.HomePhone = row["HomePhone"].ToString();
                            newDriver.AlternatePhone = row["AlternatePhone"].ToString();
                            newDriver.QualificationDate = row["QualificationDate"].ToDateTime();
                            newDriver.zTripQualified = row["zTripQualified"].ToBool();
                            newDriver.StartDate = row["StartDate"].ToDateTime();
                            newDriver.HrDays = row["HrDays"].ToString();
                            newDriver.LeaseDate = row["LeaseDate"].ToDateTime();
                            newDriver.FleetType = Common.DTO.Enum.FleetType.Taxi.ToString();
                            newDriver.AppendDate = System.DateTime.Now;

                            #region Image
                            //newDriver.Image = null;
                            if (row["Image"] != null && row["Image"] != System.DBNull.Value)
                            {
                                driverImage = (byte[])row["Image"];
                            }
                            #endregion

                        }

                        #region Log
                        logger.InfoFormat("Driver Found: ID:{0}, VehicleID:{1}", newDriver.DriverID, newDriver.VehicleID);
                        #endregion

                        using (var db = new DataAccess.VTOD.VTODEntities())
                        {
                            var driverInfo = new DriverInfo(logger);

                            result = driverInfo.UpsertDriverDetails(newDriver, driverImage);

                            db.SP_vtod_Update_TripDriverInfoID(trip.Id, result.Id);
                        }

                    }
                }
            }            

            return result;
        }

        private long GetServiceAPIId(long fleetId, string action)
		{
			long serviceAPIId = -1;

			try
			{
				serviceAPIId = GetTaxiFleetServiceAPIPreference(fleetId, action).ServiceAPIId;
			}
			catch
			{
				var vtodException = VtodException.CreateException(ExceptionType.Taxi, 1011);
				logger.Warn(vtodException.ExceptionMessage.Message);
				throw vtodException;// new ValidationException(Messages.Taxi_Common_UnableToFindFleetByAddressOrAirportInfo);

			}
			return serviceAPIId;

		}

        #endregion


        public bool HasDriver(string status)
        {
            if (!string.IsNullOrEmpty(status))
            {
                string loweredAndTrimmedStatus = status.ToLower().Trim();

                if (loweredAndTrimmedStatus == TaxiTripStatusType.Accepted.ToLower().Trim()
                   || loweredAndTrimmedStatus == TaxiTripStatusType.InTransit.ToLower().Trim()
                   || loweredAndTrimmedStatus == TaxiTripStatusType.PickUp.ToLower().Trim()
                   || loweredAndTrimmedStatus == TaxiTripStatusType.MeterON.ToLower().Trim()
                   || loweredAndTrimmedStatus == TaxiTripStatusType.InService.ToLower().Trim()
                   || loweredAndTrimmedStatus == TaxiTripStatusType.Completed.ToLower().Trim()
                   || loweredAndTrimmedStatus == TaxiTripStatusType.MeterOff.ToLower().Trim()
                   || loweredAndTrimmedStatus == TaxiTripStatusType.Assigned.ToLower().Trim())
                {
                    return true;
                }
            }

            return false;
        }


        #region Properties
        public TrackTime TrackTime { get; set; }
		#endregion
	}
}