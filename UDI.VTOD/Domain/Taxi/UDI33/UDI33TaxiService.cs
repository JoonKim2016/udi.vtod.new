﻿using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using UDI.Map;
using UDI.VTOD.Common.DTO;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.DataAccess.VTOD;
using UDILibrary;
using UDITools;
using UDIXMLSchema;
using UDI.Utility.Serialization;
using System.Configuration;
using UDI.VTOD.Domain.Taxi.Class;
using UDI.VTOD.Domain.Taxi.Const;
using UDI.VTOD.Common.Helper;
using System.Text.RegularExpressions;
using UDI.Utility.Helper;
using UDI.VTOD.Common.Track;



namespace UDI.VTOD.Domain.Taxi.UDI33
{
	public class UDI33TaxiService : ITaxiService
	{

		private ILog logger;
		private TaxiUtility utility;
		public UDI33TaxiService(TaxiUtility util, ILog log)
		{
			this.logger = log;
			this.utility = util;
		}

		#region Public method
		#region old way
		public Common.DTO.OTA.OTA_GroundBookRS Book(TaxiBookingParameter tbp,int fleetTripCode)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			OTA_GroundBookRS response = null;

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion

			DateTime? requestedLocalTime = null;
			if (tbp.Fleet != null && tbp.Fleet.ServerUTCOffset != null)
			{
				requestedLocalTime = DateTime.UtcNow.FromUtcNullAble(tbp.Fleet.ServerUTCOffset);
			}

			try
			{

				UDIXMLMsg returnMsg = null;

				#region [Taxi 1.3] Send booking request to UDI33
				UDIXMLSchema.UDI udi33Request = BuildBookOrderMsg(tbp);
				DateTime requestDateTime = DateTime.Now;
				DateTime responseDateTime;
				string errorMessage = "";

				if (SendRequestToUDI33(tbp.Fleet, udi33Request, out returnMsg, out errorMessage, "Book"))
				{
					responseDateTime = DateTime.Now;
					logger.Info("Successful booking.");
					UDIXMLSchema.UDIBookingSuccess succ = (UDIXMLSchema.UDIBookingSuccess)returnMsg.BookingSuccess;
					if (!string.IsNullOrWhiteSpace(succ.Order.ConfirmationNo))
					{
						//tbp.Trip = utility.UpdateTaxiTrip(tbp.Trip, succ);
                        tbp.Trip = utility.UpdateTaxiTrip(tbp.Trip, succ, fleetTripCode);

						if (tbp.EnableResponseAndLog)
						{
							GroundReservation reservation = tbp.request.GroundReservations.FirstOrDefault();
							response = new OTA_GroundBookRS();
							response.EchoToken = tbp.request.EchoToken;
							response.Target = tbp.request.Target;
							response.Version = tbp.request.Version;
							response.PrimaryLangID = tbp.request.PrimaryLangID;

							response.Success = new Success();

							//Reservation
							response.Reservations = new List<Reservation>();
							Reservation r = new Reservation();
							r.Confirmation = new Confirmation();
							//r.Confirmation.Type = "???";
							r.Confirmation.ID = tbp.Trip.Id.ToString();
							r.Confirmation.Type = ConfirmationType.Confirmation.ToString();

							r.Passenger = reservation.Passenger;
							r.Service = reservation.Service;
							r.Confirmation.TPA_Extensions = new TPA_Extensions();
							//r.Confirmation.TPA_Extensions.DispatchConfirmation = new Confirmation();
							//r.Confirmation.TPA_Extensions.DispatchConfirmation.ID = tbp.Trip.DispatchTripId;
							r.Confirmation.TPA_Extensions.Confirmations = new List<Confirmation>();
							r.Confirmation.TPA_Extensions.Confirmations.Add(new Confirmation { ID = tbp.Trip.taxi_trip.DispatchTripId, Type = ConfirmationType.DispatchConfirmation.ToString() });// = tbp.Trip.DispatchTripId;
							//r.Confirmation.TPA_Extensions.Statuses = new Statuses();
							//r.Confirmation.TPA_Extensions.Statuses.Status = new List<Common.DTO.OTA.Status>();
							response.Reservations.Add(r);

							#region Put status into taxi_trip_status
							//utility.SaveTripStatus(tbp.Trip.Id, TaxiTripStatusType.Booked);



							utility.SaveTripStatus(tbp.Trip.Id, TaxiTripStatusType.Booked, requestedLocalTime, null, null, null, null, null, null, null, null, string.Empty, string.Empty, null);
							#endregion

							//#region Set final status
							//utility.UpdateFinalStatus(tbp.Trip.Id, TaxiTripStatusType.Booked);
							//#endregion

						}
						else
						{
							logger.Info("Disable response and log for booking");
						}
					}
					else
					{
						string error = string.Format("Cannot get DispatchTripId. Or dispatchTripId is white space. message={0} ", errorMessage);
						logger.Error(error);
						utility.MarkThisTripAsError(tbp.Trip.Id, errorMessage);
						throw new TaxiException(error);
					}

				}
				else
				{
					logger.Error(returnMsg.BookingError.UDIHeader.ErrorString);
					utility.MarkThisTripAsError(tbp.Trip.Id, errorMessage);
					throw new TaxiException(returnMsg.BookingError.UDIHeader.ErrorString);
				}
				#endregion

				#region Write Taxi Log
				if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]) /*&& tbp.EnableResponseAndLog*/)
				{
					string requestMessage = udi33Request.XmlSerialize().ToString().Replace(" encoding=\"utf-16\"", "");
					string responseMessage = XMLObjectToString.StringFromMsgType<UDIXMLSchema.UDI>(returnMsg.UDIMsg);
					responseMessage = UDIUtils.IndentXMLString(responseMessage);
					utility.WriteTaxiLog(tbp.Trip.Id, TaxiServiceAPIConst.UDI33, TaxiServiceMethodType.Book, null, requestMessage, responseMessage, requestDateTime, responseDateTime);
				}
				#endregion
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);

				//change the status of trip
				utility.UpdateFinalStatus(tbp.Trip.Id, TaxiTripStatusType.Error, null, null, null, null, null);

				throw ex;
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_UDI33, "Book");
			#endregion

			return response;
		}

		public Common.DTO.OTA.OTA_GroundResRetrieveRS Status(TaxiStatusParameter tsp)
		{
			Stopwatch sw = new Stopwatch();
			sw.Start();
			OTA_GroundResRetrieveRS response = null;


			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion

			try
			{
				#region [Taxi 2.3] Send status request to UDI33
				UDIXMLSchema.UDI udi33Request = BuildTripStatusMsg(tsp);
				DateTime requestDateTime = DateTime.Now;
				DateTime responseDateTime;
				UDIXMLMsg returnMsg = null;
				string errorMessage = "";
				//if (SendRequestToUDI33(tsp.Fleet, udi33Request, out returnMsg, out errorMessage, "StatusWithoutFastMeter"))
				if (SendRequestToUDI33(tsp.Fleet, udi33Request, out returnMsg, out errorMessage, "Status"))
				{
					responseDateTime = DateTime.Now;

					logger.Info("Successful status.");

					if (returnMsg.MessageType == UDIMessageType.MTTripStatus)
					{
						UDIXMLSchema.UDITripStatus succ = (UDIXMLSchema.UDITripStatus)returnMsg.TripStatus;

						if (succ != null && succ.TripCount > 0 && succ.TripInfo[0] != null)
                        {
                            //update taxi _trip
                            var status = string.Empty;
                            string originalStatus = "";
                            //int? acceptedEtaWithTraffic = null;
                            int? etaWithTraffic = null;

                            int? minutesAway = utility.GetMinutesAway(succ.TripInfo[0].ETA);
                            logger.Info("Update taxi_trip");
                            utility.UpdateTaxiTrip(tsp.Trip.Id, succ.TripInfo[0].AssignedVehicleNo, minutesAway, succ.TripInfo[0].ConfirmationNo, succ.TripInfo[0].DriverName);



                            response = new OTA_GroundResRetrieveRS();
                            response.Success = new Success();
                            response.EchoToken = tsp.request.EchoToken;
                            response.Target = tsp.request.Target;
                            response.Version = tsp.request.Version;
                            response.PrimaryLangID = tsp.request.PrimaryLangID;

                            response.TPA_Extensions = new TPA_Extensions();
                            response.TPA_Extensions.Driver = utility.ExtractDriverName(succ.TripInfo[0].DriverName);
                            response.TPA_Extensions.Vehicles = new Vehicles();
                            response.TPA_Extensions.Vehicles.Items = new List<Vehicle>();

                            #region Modify Status
                            //convert trip status by our wrapper
                            if (!string.IsNullOrWhiteSpace(succ.TripInfo[0].Status))
                            {
                                status = succ.TripInfo[0].Status;
                                originalStatus = status;
                            }

                            #region Faster Meter option
                            //01. if status is Completed or Fare
                            //02. and fare exists and (Fare<=Min)
                            //03. then change status to fast meter
                            switch (tsp.Fleet.DetectFastMeter)
                            {
                                case FastMeterOption.ByMinFare:
                                    logger.Info("Detect fastmeter option: by min fare");
                                    if (utility.DetectFastMeterByMinFare(tsp.Trip.Id, status, succ.TripInfo[0].FareAmount, tsp.Fleet.MinPriceFastMeter))
                                    {
                                        status = TaxiTripStatusType.FastMeter;
                                    }
                                    break;
                                case FastMeterOption.Off:
                                    logger.Info("Detect fastmeter option is off");
                                    break;
                                default:
                                    logger.Info("Detect fastmeter option is off");
                                    break;
                            }
                            //isFastMeter = succ.TripInfo[0].IsFastMeter;
                            //if (isFastMeter)
                            //{
                            //	status = TaxiTripStatusType.FastMeter;
                            //}
                            #endregion

                            #region Status Wrapper
                            string target = utility.ConvertTripStatusForUDI33(status);
                            #endregion
                            //assign to response
                            status = target;
                            #endregion

                            #region Vehicle
                            var vehicle = new Vehicle();
                            vehicle.Geolocation = new Geolocation();
                            //vehicle.MinutesAway = succ.TripInfo[0].ETA;
                            vehicle.Distance = succ.TripInfo[0].Course.ToDecimal();
                            vehicle.Geolocation.Lat = succ.TripInfo[0].Latitude.ToDecimal();
                            vehicle.Geolocation.Long = succ.TripInfo[0].Longitude.ToDecimal();
                            if (!string.IsNullOrWhiteSpace(succ.TripInfo[0].Distance))
                            {
                                vehicle.Distance = succ.TripInfo[0].Distance.ToDecimal();
                            }
                            if (!string.IsNullOrWhiteSpace(succ.TripInfo[0].AssignedVehicleNo))
                            {
                                vehicle.ID = succ.TripInfo[0].AssignedVehicleNo;
                            }

                            #region Caculate ETA by ourself, not using UDI33

                            if (
                                (status.ToUpperInvariant().Equals(TaxiTripStatusType.Accepted.ToUpperInvariant()) || status.ToUpperInvariant().Equals(TaxiTripStatusType.Assigned.ToUpperInvariant()) || status.ToUpperInvariant().Equals(TaxiTripStatusType.InTransit.ToUpperInvariant()))
                                &&
                                (vehicle.Geolocation.Lat != 0 && vehicle.Geolocation.Long != 0)
                                &&
                                (tsp.Trip.taxi_trip.PickupLatitude.HasValue && tsp.Trip.taxi_trip.PickupLongitude.HasValue)
                            )
                            {
                                MapService ms = new MapService();
                                double d = ms.GetDirectDistanceInMeter(tsp.Trip.taxi_trip.PickupLatitude.Value, tsp.Trip.taxi_trip.PickupLongitude.Value, Convert.ToDouble(vehicle.Geolocation.Lat), Convert.ToDouble(vehicle.Geolocation.Long));
                                var rc = new UDI.SDS.RoutingController(tsp.tokenVTOD, null);
                                UDI.SDS.RoutingService.Point start = new UDI.SDS.RoutingService.Point { Latitude = tsp.Trip.taxi_trip.PickupLatitude.Value, Longitude = tsp.Trip.taxi_trip.PickupLongitude.Value };
                                UDI.SDS.RoutingService.Point end = new UDI.SDS.RoutingService.Point { Latitude = Convert.ToDouble(vehicle.Geolocation.Lat), Longitude = Convert.ToDouble(vehicle.Geolocation.Long) };
                                var result = rc.GetRouteMetrics(start, end);
                                vehicle.MinutesAway = result.TotalMinutes;

                            }
                            #endregion

                            #region ETA with Traffic
                            //ToDo: it should be configurable. It's hardcoded just for now but should be configuratble.
                            if (tsp.User.name.ToLower() == "ztrip" || !tsp.Trip.DriverAcceptedETA.HasValue)
                            {
                                if (
                                                        status.Trim().ToLower() == Const.TaxiTripStatusType.Accepted.Trim().ToLower() ||
                                                        status.Trim().ToLower() == Const.TaxiTripStatusType.Assigned.Trim().ToLower() ||
                                                        status.Trim().ToLower() == Const.TaxiTripStatusType.InTransit.Trim().ToLower()
                                                        )
                                {
                                    if (vehicle != null && vehicle.Geolocation != null && vehicle.Geolocation.Lat != 0 && vehicle.Geolocation.Long != 0)
                                    {
                                        if (tsp.Trip != null && tsp.Trip.taxi_trip != null && tsp.Trip.taxi_trip.PickupLatitude.HasValue && tsp.Trip.taxi_trip.PickupLatitude.Value != 0 && tsp.Trip.taxi_trip.PickupLongitude.HasValue && tsp.Trip.taxi_trip.PickupLongitude.Value != 0)
                                        {
                                            try
                                            {
                                                var error = string.Empty;
                                                var map = new UDI.Map.MapService(Map.DTO.MapApiProvider.Google);
                                                var etaResult = map.GetETA(new Map.DTO.Geolocation { Latitude = vehicle.Geolocation.Lat, Longitude = vehicle.Geolocation.Long },
                                                    new Map.DTO.Geolocation { Latitude = tsp.Trip.taxi_trip.PickupLatitude.Value.ToDecimal(), Longitude = tsp.Trip.taxi_trip.PickupLongitude.ToDecimal() }, UDI.Map.DTO.DistanceUnit.Mile, out error);

                                                if (string.IsNullOrWhiteSpace(error))
                                                {
                                                    etaWithTraffic = System.Math.Round((decimal)(etaResult.DurationInTrafficInSecond / 60)).ToInt32();
                                                    //if (etaWithTraffic < 3)
                                                    //	etaWithTraffic = 3;
                                                    vehicle.MinutesAwayWithTraffic = etaWithTraffic.ToDecimal();
                                                }
                                            }
                                            catch
                                            { }
                                        }
                                    }
                                }
                            }
                            #endregion

                            response.TPA_Extensions.Vehicles.Items.Add(vehicle);

                            #endregion

                            #region Get Driver Info
                            vtod_driver_info driverInfo = null;
                            if (tsp.Fleet.DriverInfo == UDI.VTOD.Common.Const.DriveInfoSource.TCS)
                            {
                                if (tsp.Trip.DriverInfoID.HasValue && tsp.Trip.DriverInfoID.Value > 0)
                                {
                                    using (var db = new DataAccess.VTOD.VTODEntities())
                                    {
                                        driverInfo = db.vtod_driver_info.FirstOrDefault(t => t.Id == tsp.Trip.DriverInfoID.Value);
                                    }
                                }
                                else
                                {
                                    if (tsp.tokenVTOD != null && succ.TripInfo[0].AssignedDriver != null && !string.IsNullOrWhiteSpace(succ.TripInfo[0].AssignedDriver))
                                    {
                                        driverInfo = utility.GetDriverInfoByDriverId(tsp.tokenVTOD.Username, tsp.Trip, status, succ.TripInfo[0].AssignedDriver);
                                    }
                                    if (driverInfo == null)
                                    {
                                        if (tsp.tokenVTOD != null && vehicle != null && !string.IsNullOrWhiteSpace(vehicle.ID))
                                        {
                                            driverInfo = utility.GetDriverInfoByVehicleNumber(tsp.tokenVTOD.Username, status, vehicle.ID, "", tsp.Trip);
                                        }
                                    }
                                }
                            }
                            else if (tsp.Fleet.DriverInfo == UDI.VTOD.Common.Const.DriveInfoSource.Local)
                            {
                                if (tsp.Trip.DriverInfoID.HasValue && tsp.Trip.DriverInfoID.Value > 0)
                                {
                                     using (var db = new DataAccess.VTOD.VTODEntities())
                                    {
                                        driverInfo = db.vtod_driver_info.FirstOrDefault(t => t.Id == tsp.Trip.DriverInfoID.Value);
                                    }
                                }
                            }
                            #endregion

                            #region Making the result
                            //Convert status to front-end status
                            //string frontEndStatus = utility.ConvertTripStatusMessageForFrontEndDevice(tsp.Fleet_User.UserId, status, tsp.Fleet.FastMeterAction);
                            #region Front End Status
                            string frontEndStatus = utility.ConvertTripStatusMessageForFrontEndDevice(tsp.Fleet_User.UserId, status);
                            #endregion

                            Status s = new Common.DTO.OTA.Status { Value = frontEndStatus, Fare = succ.TripInfo[0].FareAmount.ToDecimal() };
                            response.TPA_Extensions.Statuses = new Statuses();
                            response.TPA_Extensions.Statuses.Status = new List<Status>();
                            response.TPA_Extensions.Statuses.Status.Add(s);
                            response.TPA_Extensions.Contacts = new Contacts();
                            response.TPA_Extensions.Contacts.Items = new List<Contact>();

                            #region Adding Driver Info and DispatchInfo
                            Contact dispatchContact = null;
                            Contact driverContact = null;

                            //Disptch Info
                            string dispatchPhone = tsp.Fleet.PhoneNumber;
                            #region Work around for zTrip ver 2.2, please delete this part after release of zTrip ver 2.3
                            //if (driverInfo != null && !string.IsNullOrEmpty(driverInfo.CellPhone))
                            //{
                            //	dispatchPhone = driverInfo.CellPhone;
                            //}
                            #endregion

                            //todo add logic here for expedia and other companies logos
                            if (tsp.User.name.ToLower() == "ztrip" || tsp.User.name.ToLower() == "udi")
                            {
                                dispatchContact = new Contact
                                {
                                    Type = Common.DTO.Enum.ContactType.Dispatch.ToString(),
                                    Name = tsp.Fleet.Alias,
                                    Telephone = new Telephone { PhoneNumber = dispatchPhone.CleanPhone10Digit() },
                                    PhotoUrl = ConfigHelper.GetDriverPhotoVersionDefault()
                                };
                            }
                            else
                            {
                                dispatchContact = new Contact
                                {
                                    Type = Common.DTO.Enum.ContactType.Dispatch.ToString(),
                                    Name = tsp.Fleet.Alias,
                                    Telephone = new Telephone { PhoneNumber = dispatchPhone.CleanPhone10Digit() }

                                };
                            }

                            response.TPA_Extensions.Contacts.Items.Add(dispatchContact);

                            //Driver Info
                            if (driverInfo != null && !string.IsNullOrEmpty(driverInfo.CellPhone))
                            {
                                driverContact = new Contact { Type = Common.DTO.Enum.ContactType.Driver.ToString(), Name = string.Format("{0} {1}", driverInfo.FirstName, driverInfo.LastName), Telephone = new Telephone { PhoneNumber = driverInfo.CellPhone.CleanPhone10Digit() }, PhotoUrl = driverInfo.PhotoUrl, ZtripDateOfDesignation = driverInfo.QualificationDate, ZtripDesignation = driverInfo.zTripQualified };
                                response.TPA_Extensions.Contacts.Items.Add(driverContact);
                            }


                            #endregion

                            if (tsp.Trip != null && tsp.Trip.taxi_trip.PickupLatitude.HasValue && tsp.Trip.taxi_trip.PickupLatitude.Value != 0 && tsp.Trip.taxi_trip.PickupLongitude.HasValue && tsp.Trip.taxi_trip.PickupLongitude.Value != 0)
                            {
                                response.TPA_Extensions.PickupLocation = new Pickup_Dropoff_Stop();
                                response.TPA_Extensions.PickupLocation.Address = new Common.DTO.OTA.Address();
                                response.TPA_Extensions.PickupLocation.Address.Latitude = tsp.Trip.taxi_trip.PickupLatitude.Value.ToString();
                                response.TPA_Extensions.PickupLocation.Address.Longitude = tsp.Trip.taxi_trip.PickupLongitude.Value.ToString();
                            }
                            #endregion

                            #region Save status into taxi_trip_status

                            #region Get StatusTime
                            DateTime? statusTime = null;
                            DateTime? tripStartTime = null;
                            DateTime? tripEndTime = null;

                            switch (status)
                            {
                                case TaxiTripStatusType.Accepted:
                                    statusTime = succ.TripInfo[0].StateTimes.DispatchTime;
                                    break;
                                case TaxiTripStatusType.Booked:
                                    statusTime = succ.TripInfo[0].StateTimes.BookingTime;
                                    break;
                                case TaxiTripStatusType.Canceled:
                                    statusTime = succ.TripInfo[0].StateTimes.CancelTime;
                                    break;
                                case TaxiTripStatusType.MeterOff:
                                case TaxiTripStatusType.Completed:
                                    statusTime = succ.TripInfo[0].StateTimes.MeterOffTime;
                                    break;
                                case TaxiTripStatusType.DispatchPending:
                                    //No specific time
                                    break;
                                case TaxiTripStatusType.FastMeter:
                                    //No specific time
                                    break;
                                case TaxiTripStatusType.UnMatched:
                                    //No specific time
                                    break;
                                case TaxiTripStatusType.Matched:
                                    //No specific time
                                    break;
                                case TaxiTripStatusType.InTransit:
                                    //No specific time
                                    break;
                                case TaxiTripStatusType.InService:
                                    statusTime = succ.TripInfo[0].StateTimes.OnSiteTime;
                                    break;
                                case TaxiTripStatusType.MeterON:
                                    statusTime = succ.TripInfo[0].StateTimes.MeterOnTime;
                                    break;
                                case TaxiTripStatusType.NoShow:
                                    statusTime = succ.TripInfo[0].StateTimes.NoTripTime;
                                    break;
                            }
                            if (!statusTime.HasValue || statusTime.Value <= DateTime.MinValue || statusTime.Value < new DateTime(2010, 1, 1))
                            {
                                if (tsp.Fleet != null && tsp.Fleet.ServerUTCOffset != null)
                                {
                                    statusTime = DateTime.UtcNow.FromUtcNullAble(tsp.Fleet.ServerUTCOffset);
                                }
                            }

                            if ((status.Trim().ToLower() == TaxiTripStatusType.InService.Trim().ToLower() || status.Trim().ToLower() == TaxiTripStatusType.MeterON.Trim().ToLower()) && (tsp.Trip.TripStartTime == null))
                            {
                                tripStartTime = statusTime;
                            }
                            else
                            {
                                tripStartTime = tsp.Trip.TripStartTime;
                            }

                            if ((status.Trim().ToLower() == TaxiTripStatusType.Completed.Trim().ToLower() || status.Trim().ToLower() == TaxiTripStatusType.MeterOff.Trim().ToLower()) && (tsp.Trip.TripEndTime == null))
                            {
                                tripEndTime = statusTime;
                            }
                            else
                            {
                                tripEndTime = tsp.Trip.TripEndTime;
                            }


                            #endregion


                            //For all save and update
                            // If one of previous tripStatus <> FastMeter => Then Update, else do not update

                            if (status.ToUpperInvariant() == TaxiTripStatusType.Canceled.ToUpperInvariant())
                            {
                                //cancel status 
                                utility.SaveTripStatus(tsp.Trip.Id, TaxiTripStatusType.Canceled, statusTime, null, null, null, null, null, null, etaWithTraffic, null, string.Empty, originalStatus, null);
                                utility.UpdateFinalStatus(tsp.Trip.Id, TaxiTripStatusType.Canceled, null, null, null, null, null);
                            }

                            else if (status.ToUpperInvariant() == TaxiTripStatusType.FastMeter.ToUpperInvariant())
                            {
                                //FastMeter status 
                                //utility.SaveTripStatus(tsp.Trip.Id, TaxiTripStatusType.FastMeter, statusTime, null, null, null, null, null, null, null, string.Empty, originalStatus, null);
                                decimal? overwriteFare = utility.DetermineOverwriteFare(tsp, response.TPA_Extensions.Vehicles.Items.First().Geolocation.Long, response.TPA_Extensions.Vehicles.Items.First().Geolocation.Lat, succ.TripInfo[0].FareAmount.ToDecimal());

                                response.TPA_Extensions.Statuses.Status[0].Fare = overwriteFare.HasValue ? overwriteFare.Value : response.TPA_Extensions.Statuses.Status[0].Fare;


                                //Save Status
                                utility.SaveTripStatus(tsp.Trip.Id, TaxiTripStatusType.FastMeter, statusTime, response.TPA_Extensions.Vehicles.Items.First().ID, response.TPA_Extensions.Vehicles.Items.First().Geolocation.Lat, response.TPA_Extensions.Vehicles.Items.First().Geolocation.Long, overwriteFare, succ.TripInfo[0].FareAmount.ToDecimal(), response.TPA_Extensions.Vehicles.Items.First().MinutesAway.ToInt32(), etaWithTraffic, succ.TripInfo[0].DriverName, string.Empty, originalStatus, succ.TripInfo[0].AssignedDriver);

                                utility.UpdateFinalStatus(tsp.Trip.Id, TaxiTripStatusType.FastMeter, null, null, null, null, null);
                            }
                            //else if (status.ToUpperInvariant() == TaxiTripStatusType.FastMeter.ToUpperInvariant())
                            //{
                            //    if (tsp.Fleet.FastMeterAction == TaxiFleetFastMeterAction.ReDispatch)
                            //    {
                            //        utility.SaveTripStatus_ReDispatch(tsp.Trip.Id, statusTime, null, null, null, null, null, null, null, string.Empty, originalStatus, null);
                            //        utility.UpdateFinalStatus(tsp.Trip.Id, TaxiTripStatusType.Booked, null, null, null);
                            //    }
                            //    else if (tsp.Fleet.FastMeterAction == TaxiFleetFastMeterAction.Cancel)
                            //    {
                            //        utility.SaveTripStatus(tsp.Trip.Id, TaxiTripStatusType.FastMeter, statusTime, null, null, null, null, null, null, null, string.Empty, originalStatus, null);
                            //        utility.UpdateFinalStatus(tsp.Trip.Id, TaxiTripStatusType.FastMeter, null, null, null);
                            //    }
                            //}
                            else
                            {
                                decimal? overwriteFare = utility.DetermineOverwriteFare(tsp, response.TPA_Extensions.Vehicles.Items.First().Geolocation.Long, response.TPA_Extensions.Vehicles.Items.First().Geolocation.Lat, succ.TripInfo[0].FareAmount.ToDecimal());

                                response.TPA_Extensions.Statuses.Status[0].Fare = overwriteFare.HasValue ? overwriteFare.Value : response.TPA_Extensions.Statuses.Status[0].Fare;


                                //Save Status
                                utility.SaveTripStatus(tsp.Trip.Id, status, statusTime, response.TPA_Extensions.Vehicles.Items.First().ID, response.TPA_Extensions.Vehicles.Items.First().Geolocation.Lat, response.TPA_Extensions.Vehicles.Items.First().Geolocation.Long, overwriteFare, succ.TripInfo[0].FareAmount.ToDecimal(), response.TPA_Extensions.Vehicles.Items.First().MinutesAway.ToInt32(), etaWithTraffic, succ.TripInfo[0].DriverName, string.Empty, originalStatus, succ.TripInfo[0].AssignedDriver);

                                //Save final status
                                //utility.UpdateFinalStatus(tsp.Trip.Id, status, Convert.ToDecimal(succ.TripInfo[0].FareAmount), tripStartTime, tripEndTime);

                                if (tsp.Trip.FinalStatus.ToLower() == TaxiTripStatusType.Accepted.ToLower() || tsp.Trip.FinalStatus.ToLower() == TaxiTripStatusType.Assigned.ToLower())
                                {
                                    utility.UpdateFinalStatus(tsp.Trip.Id, status, overwriteFare, null, tripStartTime, tripEndTime, etaWithTraffic);
                                }
                                else
                                {
                                    utility.UpdateFinalStatus(tsp.Trip.Id, status, overwriteFare, null, tripStartTime, tripEndTime, null);

                                }
                            }



                            #endregion
                        }
						else
						{
							var vtodException = VtodException.CreateException(ExceptionType.Taxi, 3004);//string error = Messages.Taxi_TripNotFound;
							string error = vtodException.ExceptionMessage.Message;// Messages.Taxi_TripNotFound;
							logger.Error(error);
							throw vtodException;
						}
					}
					else
					{

						var vtodException = VtodException.CreateException(ExceptionType.Taxi, 3004);//string error = Messages.Taxi_TripNotFound;
						string error = vtodException.ExceptionMessage.Message;// Messages.Taxi_TripNotFound;

						logger.Error(error);
						logger.Error(returnMsg.ErrorString);
						throw vtodException; // new Exception(error + returnMsg.ErrorString);
					}

				}
				else
				{
					string error = returnMsg.ErrorString;
					logger.Error(error);
					throw new TaxiException(error);
				}
				#endregion

				#region Write Taxi Log
				if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]) /*&&tsp.EnableResponseAndLog*/)
				{
					string requestMessage = udi33Request.XmlSerialize().ToString().Replace(" encoding=\"utf-16\"", "");
					string responseMessage = XMLObjectToString.StringFromMsgType<UDIXMLSchema.UDI>(returnMsg.UDIMsg);
					responseMessage = UDIUtils.IndentXMLString(responseMessage);
					utility.WriteTaxiLog(tsp.Trip.Id, TaxiServiceAPIConst.UDI33, TaxiServiceMethodType.Status, null, requestMessage, responseMessage, requestDateTime, responseDateTime);
				}
				#endregion
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw new TaxiException(ex.Message);
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);


			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_UDI33, "StatusWithoutFastMeter");
			#endregion

			return response;
		}

		public Common.DTO.OTA.OTA_GroundCancelRS Cancel(TaxiCancelParameter tcp)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			OTA_GroundCancelRS response = null;

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion

			try
			{
				#region [Taxi 3.3] Do status first
				TaxiStatusParameter tsp = utility.ConvertTCPpToTSP(tcp);
				//bool isFastMeter = false;


				#region New way
				string tripStatus = tcp.Trip.FinalStatus;
				#endregion

				#region Old way
				//OTA_GroundResRetrieveRS status_response = Status(tsp);
				//string vtodStatus=utility.ConvertTripStatusForUDI33(status_response.TPA_Extensions.Statuses.Status[0].Value);
				#endregion

				if (utility.IsAbleToCancelThisTrip(tripStatus))
				{
					#region [Taxi 3.4] Send cancel request to UDI33
					response = CancelWithoutStatus(tcp);
					#endregion
				}
				else
				{
					throw VtodException.CreateException(ExceptionType.Taxi, 5001); //throw new TaxiException(Messages.Taxi_CancelFailure);
				}
				#endregion

			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw ex;
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);


			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_UDI33, "Cancel");
			#endregion

			return response;

		}

		public OTA_GroundAvailRS GetVehicleInfo(TaxiGetVehicleInfoParameter tgvip)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			var response = new OTA_GroundAvailRS();
			response.Success = new Success();
			response.TPA_Extensions = new TPA_Extensions();
			response.TPA_Extensions.Vehicles = new Vehicles();
			response.TPA_Extensions.Vehicles.Items = new List<Vehicle>();


			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion

			try
			{
				#region [Taxi 4.3] Send GetVehicleInfo request to UDI33
				UDIXMLSchema.UDI udi33Request = BuildGetVehicleInfoMsg(tgvip);
				DateTime requestDateTime = DateTime.Now;
				DateTime responseDateTime;
				UDIXMLMsg returnMsg = null;
				string errorMessage = "";

				if (SendRequestToUDI33(tgvip.Fleet, udi33Request, out returnMsg, out errorMessage, "GetVehicleInfo"))
				{
					responseDateTime = DateTime.Now;

					if (tgvip.EnableResponseAndLog)
					{

						if (returnMsg.MessageType == UDIMessageType.MTVehicleInfoSuccess)
						{

							UDIXMLSchema.UDIVehicleInfoSuccess succ = (UDIXMLSchema.UDIVehicleInfoSuccess)returnMsg.VehicleInfoSuccess;

							if (succ != null)
							{
								if (succ.Vehicles.Count() > 0)
								{

									MapService ms = new MapService();
									if (succ.Vehicles.Any())
									{
										//UDI33 cannot provide ETA for vehicles. We need to use supershuttle way to do it.
										//response.TPA_Extensions.Vehicles.NearestVehicleETA = Convert.ToDecimal(succ.Vehicles.OrderBy(x => x.ETA).Select(x => x.ETA).FirstOrDefault()).ToString();

										int shortestIndex = -1;
										double? shortestDistance = null;
										for (int index = 0; index < succ.Vehicles.Count(); index++)
										{
											double d = ms.GetDirectDistanceInMile(tgvip.Latitude, tgvip.Longtitude, succ.Vehicles[index].Latitude, succ.Vehicles[index].Longitude);
											succ.Vehicles[index].Distance = d;
											if (shortestDistance.HasValue)
											{
												if (shortestDistance.Value > d)
												{
													shortestDistance = d;
													shortestIndex = index;
												}
											}
											else
											{
												shortestDistance = d;
												shortestIndex = index;
											}
										}

										if (shortestIndex != -1)
										{
											//get ETA
											var rc = new UDI.SDS.RoutingController(tgvip.tokenVTOD, null);
											UDI.SDS.RoutingService.Point start = new UDI.SDS.RoutingService.Point { Latitude = tgvip.Latitude, Longitude = tgvip.Longtitude };
											UDI.SDS.RoutingService.Point end = new UDI.SDS.RoutingService.Point { Latitude = succ.Vehicles[shortestIndex].Latitude, Longitude = succ.Vehicles[shortestIndex].Longitude };
											var result = rc.GetRouteMetrics(start, end);
											response.TPA_Extensions.Vehicles.NearestVehicleETA = result.TotalMinutes.ToString();
										}

										response.TPA_Extensions.Vehicles.NumberOfVehicles = succ.Vehicles.Count().ToString();
									}
									else
									{
										response.TPA_Extensions.Vehicles.NumberOfVehicles = "0";
									}

									foreach (var item in succ.Vehicles.OrderBy(s => s.Distance))
									{
										var vehicle = new Vehicle();
										vehicle.Geolocation = new Geolocation();
										vehicle.Geolocation.Long = decimal.Parse(item.Longitude.ToString());
										vehicle.Geolocation.Lat = decimal.Parse(item.Latitude.ToString());
										vehicle.Geolocation.PriorLong = decimal.Parse(item.PriorLongitude.ToString());
										vehicle.Geolocation.PriorLat = decimal.Parse(item.PriorLatitude.ToString());

										vehicle.Course = decimal.Parse(item.Course.ToString());
										//vehicle.Distance = item.Distance;
										vehicle.DriverID = item.DriverID;
										vehicle.DriverName = item.DriverName;
										vehicle.FleetID = item.Fleet;
										vehicle.ID = item.VehicleNo;
										vehicle.VehicleStatus = item.VehicleStatus;
										vehicle.VehicleType = item.VehicleType;
										vehicle.Distance = item.Distance.ToDecimal();
										//vehicle.Zone = item.Zone;
										vehicle.MinutesAway = Convert.ToDecimal(item.ETA.ToString());
										response.TPA_Extensions.Vehicles.Items.Add(vehicle);
									}
								}
								else
								{
									// no vehicles found
									logger.Info("Found no vehicles");
									response.TPA_Extensions.Vehicles.NumberOfVehicles = succ.Vehicles.Count().ToString();
								}
							}
							else
							{
								//dispatchResp.Result = DispatchResult.Success;
								logger.Info("returnMsg.VehicleInfoSuccess is null");
							}
						}
						else
						{
							logger.Info("Disable response and log for get vehicle information");
						}
					}
					else
					{
						//dispatchResp.Result = DispatchResult.Success;
						logger.InfoFormat("returnMsg.MessageType is not UDIMessageType.MTVehicleInfoSuccess. MessageType={0}", returnMsg.MessageType);
					}

				}
				else
				{
					string error = returnMsg.ErrorString;
					logger.Error(error);
					throw new TaxiException(error);
				}
				#endregion


				#region Write Taxi Log
				if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]) && tgvip.EnableResponseAndLog)
				{
					string requestMessage = udi33Request.XmlSerialize().ToString().Replace(" encoding=\"utf-16\"", "");
					string responseMessage = XMLObjectToString.StringFromMsgType<UDIXMLSchema.UDI>(returnMsg.UDIMsg);
					responseMessage = UDIUtils.IndentXMLString(responseMessage);
					utility.WriteTaxiLog(null, TaxiServiceAPIConst.UDI33, TaxiServiceMethodType.GetVehicleInfo, null, requestMessage, responseMessage, requestDateTime, responseDateTime);
				}
				#endregion
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw new TaxiException(ex.Message);
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_UDI33, "GetVehicleInfo");
			#endregion

			return response;
		}

		public bool NotifyDriver(TaxiSendMessageToVehicleParameter tsmtvp)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			bool result = false;

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion

			try
			{
				#region [Taxi ?.?] SendMessageToVehicle
				UDIXMLSchema.UDI udi33Request = BuildSendMessageToVehicle(tsmtvp);
				DateTime requestDateTime = DateTime.Now;
				DateTime responseDateTime;
				UDIXMLMsg returnMsg = null;
				string errorMessage = "";
				if (SendRequestToUDI33(tsmtvp.Fleet, udi33Request, out returnMsg, out errorMessage, "SendMessageToVehicle"))
				{
					responseDateTime = DateTime.Now;

					logger.Info("Successful status.");

					if (returnMsg.MessageType == UDIMessageType.MTSendMDTSuccess)
					{
						UDIXMLSchema.UDIMDTSendMessage succ = (UDIXMLSchema.UDIMDTSendMessage)returnMsg.MDTSendMessage;

						if (succ != null)
						{
							result = true;
						}
						else
						{
							//string error = Messages.Taxi_UnableToSendMessageToVehicle;
							logger.Error("Sorry, we are unable to send message to vehicle.");
							throw VtodException.CreateException(ExceptionType.Taxi, 9003);//
						}
					}
					else
					{
						logger.Error("Sorry, we are unable to send message to vehicle.");
						throw VtodException.CreateException(ExceptionType.Taxi, 9003);//
					}

				}
				else
				{
					string error = returnMsg.ErrorString;
					logger.Error(error);
					throw new TaxiException(error);
				}
				#endregion

				#region Write Taxi Log
				if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]) /*&&tsp.EnableResponseAndLog*/)
				{
					string requestMessage = udi33Request.XmlSerialize().ToString().Replace(" encoding=\"utf-16\"", "");
					string responseMessage = XMLObjectToString.StringFromMsgType<UDIXMLSchema.UDI>(returnMsg.UDIMsg);
					responseMessage = UDIUtils.IndentXMLString(responseMessage);
					utility.WriteTaxiLog(tsmtvp.Trip.Id, TaxiServiceAPIConst.UDI33, TaxiServiceMethodType.SendMessageToVehicle, null, requestMessage, responseMessage, requestDateTime, responseDateTime);
				}
				#endregion
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw new TaxiException(ex.Message);
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_UDI33, "SendMessageToVehicle");
			#endregion

			return result;
		}

		#endregion

		#region new way

		public OTA_GroundAvailRS GetEstimation(TokenRS tokenVTOD, OTA_GroundAvailRQ request)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			OTA_GroundAvailRS response = new OTA_GroundAvailRS();
			try
			{
				TaxiUtility utility = new TaxiUtility(logger);

				#region [Taxi 5.0] Validate required fields
				TaxiGetEstimationParameter tgep = null;
				if (!utility.ValidateGetEstimationRequest(tokenVTOD, request, out tgep))
				{
					string error = string.Format("This GetEstimation request is invalid.");
					throw new ValidationException(error);
				}
				#endregion

				response = utility.GetRestimationResponse(tgep);

				#region Write Taxi Log
				utility.WriteTaxiLog(null, TaxiServiceAPIConst.None, TaxiServiceMethodType.Estimation, null, null, null, null, null);
				#endregion

				response.Success = new Success();
			}
			catch (Exception ex)
			{				
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw;				
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return response;
		}
        public  OTA_GroundBookRS ModifyBook(TokenRS tokenVTOD, OTA_GroundBookRQ request, out List<Tuple<string, int, long>> disptach_Rez_VTOD, int fleetTripCode, taxi_fleet fleet,long serviceAPIID)
        {
            throw new NotImplementedException();
        }
        public OTA_GroundBookRS Book(TokenRS tokenVTOD, OTA_GroundBookRQ request, out List<Tuple<string, int, long>> disptach_Rez_VTOD, int fleetTripCode, taxi_fleet fleet)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			OTA_GroundBookRS response = null;
			disptach_Rez_VTOD = new List<Tuple<string, int, long>>();

			TaxiUtility utility = new TaxiUtility(logger);

			#region [Taxi 1.0] Validate required fields
			TaxiBookingParameter tbp = new TaxiBookingParameter();
			if (!utility.ValidateBookingRequest(tokenVTOD, request, out tbp,fleet))
			{
				throw VtodException.CreateException(ExceptionType.Taxi, 2007);
			}
			#endregion

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion

			DateTime? requestedLocalTime = null;
            requestedLocalTime = tbp.PickupDateTime;
			try
			{

				UDIXMLMsg returnMsg = null;

				#region [Taxi 1.3] Send booking request to UDI33
				UDIXMLSchema.UDI udi33Request = BuildBookOrderMsg(tbp);
				DateTime requestDateTime = DateTime.Now;
				DateTime responseDateTime;
				string errorMessage = "";

				if (SendRequestToUDI33(tbp.Fleet, udi33Request, out returnMsg, out errorMessage, "Book"))
				{
					responseDateTime = DateTime.Now;
					logger.Info("Successful booking.");
					UDIXMLSchema.UDIBookingSuccess succ = (UDIXMLSchema.UDIBookingSuccess)returnMsg.BookingSuccess;
					if (!string.IsNullOrWhiteSpace(succ.Order.ConfirmationNo))
					{
						//tbp.Trip = utility.UpdateTaxiTrip(tbp.Trip, succ);
                        tbp.Trip = utility.UpdateTaxiTrip(tbp.Trip, succ, fleetTripCode);

						if (tbp.EnableResponseAndLog)
						{
							GroundReservation reservation = tbp.request.GroundReservations.FirstOrDefault();
							response = new OTA_GroundBookRS();
							response.EchoToken = tbp.request.EchoToken;
							response.Target = tbp.request.Target;
							response.Version = tbp.request.Version;
							response.PrimaryLangID = tbp.request.PrimaryLangID;

							response.Success = new Success();

							//Reservation
							response.Reservations = new List<Reservation>();
							Reservation r = new Reservation();
							r.Confirmation = new Confirmation();
							//r.Confirmation.Type = "???";
							r.Confirmation.ID = tbp.Trip.Id.ToString();
							r.Confirmation.Type = ConfirmationType.Confirmation.ToString();

							r.Passenger = reservation.Passenger;
							r.Service = reservation.Service;
							r.Confirmation.TPA_Extensions = new TPA_Extensions();
							//r.Confirmation.TPA_Extensions.DispatchConfirmation = new Confirmation();
							//r.Confirmation.TPA_Extensions.DispatchConfirmation.ID = tbp.Trip.DispatchTripId;
							r.Confirmation.TPA_Extensions.Confirmations = new List<Confirmation>();
							r.Confirmation.TPA_Extensions.Confirmations.Add(new Confirmation { ID = tbp.Trip.taxi_trip.DispatchTripId, Type = ConfirmationType.DispatchConfirmation.ToString() });// = tbp.Trip.DispatchTripId;
							//r.Confirmation.TPA_Extensions.Statuses = new Statuses();
							//r.Confirmation.TPA_Extensions.Statuses.Status = new List<Common.DTO.OTA.Status>();
                            #region FleetInfo
                            taxi_fleet fleetObj = null;
                            using (var db = new DataAccess.VTOD.VTODEntities())
                            {
                                Int64 fleetId = 0;
                                taxi_trip tripObj = db.taxi_trip.Where(p => p.Id == tbp.Trip.Id).FirstOrDefault();
                                if (tripObj != null)
                                {
                                    fleetId = tripObj.FleetId;
                                }
                                if (fleetId > 0)
                                {
                                    fleetObj = db.taxi_fleet.Where(p => p.Id == fleetId).FirstOrDefault();
                                }

                            }
                            if (fleetObj != null)
                            {
                                r.TPA_Extensions = new TPA_Extensions();
                                r.TPA_Extensions.Contacts = new Contacts();
                                r.TPA_Extensions.Contacts.Items = new List<Contact>();
                                var contactDetails = new Contact { Type = Common.DTO.Enum.ContactType.Dispatch.ToString() };
                                if (!string.IsNullOrWhiteSpace(fleetObj.Name))
                                {
                                    contactDetails.Name = fleetObj.Name;
                                }
                                if (!string.IsNullOrWhiteSpace(fleetObj.PhoneNumber))
                                {
                                    contactDetails.Telephone = new Telephone { PhoneNumber = fleetObj.PhoneNumber };
                                }
                                r.TPA_Extensions.Contacts.Items.Add(contactDetails);
                            }
                            #endregion
							response.Reservations.Add(r);

							#region Put status into taxi_trip_status
							//utility.SaveTripStatus(tbp.Trip.Id, TaxiTripStatusType.Booked);



							utility.SaveTripStatus(tbp.Trip.Id, TaxiTripStatusType.Booked, requestedLocalTime, null, null, null, null, null, null, null, null, string.Empty, string.Empty, null);
							#endregion

							//#region Set final status
							//utility.UpdateFinalStatus(tbp.Trip.Id, TaxiTripStatusType.Booked);
							//#endregion

						}
						else
						{
							logger.Info("Disable response and log for booking");
						}
					}
					else
					{
						string error = string.Format("Cannot get DispatchTripId. Or dispatchTripId is white space. message={0} ", errorMessage);
						logger.Error(error);
						utility.MarkThisTripAsError(tbp.Trip.Id, errorMessage);
						throw new TaxiException(error);
					}

				}
				else
				{
					logger.Error(returnMsg.BookingError.UDIHeader.ErrorString);
					utility.MarkThisTripAsError(tbp.Trip.Id, errorMessage);
					throw new TaxiException(returnMsg.BookingError.UDIHeader.ErrorString);
				}
				#endregion

				#region Write Taxi Log
				if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]) /*&& tbp.EnableResponseAndLog*/)
				{
					string requestMessage = udi33Request.XmlSerialize().ToString().Replace(" encoding=\"utf-16\"", "");
					string responseMessage = XMLObjectToString.StringFromMsgType<UDIXMLSchema.UDI>(returnMsg.UDIMsg);
					responseMessage = UDIUtils.IndentXMLString(responseMessage);
					utility.WriteTaxiLog(tbp.Trip.Id, TaxiServiceAPIConst.UDI33, TaxiServiceMethodType.Book, null, requestMessage, responseMessage, requestDateTime, responseDateTime);
				}
				#endregion
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);

				//change the status of trip
				utility.UpdateFinalStatus(tbp.Trip.Id, TaxiTripStatusType.Error, null, null, null, null, null);

				throw ex;
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_UDI33, "Book");
			#endregion

			return response;
		}

		public OTA_GroundResRetrieveRS Status(TokenRS tokenVTOD, OTA_GroundResRetrieveRQ request,string type)
		{
			Stopwatch sw = new Stopwatch();
			sw.Start();
			OTA_GroundResRetrieveRS response = null;

			TaxiUtility utility = new TaxiUtility(logger);

			#region [Taxi 2.0] Validate required fields
			TaxiStatusParameter tsp = null;
			if (!utility.ValidateStatusRequest(tokenVTOD, request, out tsp))
			{
				throw VtodException.CreateException(ExceptionType.Taxi, 3003);
			}
			#endregion

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion

			try
			{
				#region [Taxi 2.3] Send status request to UDI33
				UDIXMLSchema.UDI udi33Request = BuildTripStatusMsg(tsp);
				DateTime requestDateTime = DateTime.Now;
				DateTime responseDateTime;
				UDIXMLMsg returnMsg = null;
				string errorMessage = "";
				//if (SendRequestToUDI33(tsp.Fleet, udi33Request, out returnMsg, out errorMessage, "StatusWithoutFastMeter"))
				if (SendRequestToUDI33(tsp.Fleet, udi33Request, out returnMsg, out errorMessage, "Status"))
				{
					responseDateTime = DateTime.Now;

					logger.Info("Successful status.");

					if (returnMsg.MessageType == UDIMessageType.MTTripStatus)
					{
						UDIXMLSchema.UDITripStatus succ = (UDIXMLSchema.UDITripStatus)returnMsg.TripStatus;

						if (succ != null && succ.TripCount > 0 && succ.TripInfo[0] != null)
                        {
                            //update taxi _trip
                            var status = string.Empty;
                            string originalStatus = "";
                            //int? acceptedEtaWithTraffic = null;
                            int? etaWithTraffic = null;

                            int? minutesAway = utility.GetMinutesAway(succ.TripInfo[0].ETA);
                            logger.Info("Update taxi_trip");
                            utility.UpdateTaxiTrip(tsp.Trip.Id, succ.TripInfo[0].AssignedVehicleNo, minutesAway, succ.TripInfo[0].ConfirmationNo, succ.TripInfo[0].DriverName);



                            response = new OTA_GroundResRetrieveRS();
                            response.Success = new Success();
                            response.EchoToken = tsp.request.EchoToken;
                            response.Target = tsp.request.Target;
                            response.Version = tsp.request.Version;
                            response.PrimaryLangID = tsp.request.PrimaryLangID;

                            response.TPA_Extensions = new TPA_Extensions();
                            response.TPA_Extensions.Driver = utility.ExtractDriverName(succ.TripInfo[0].DriverName);
                            response.TPA_Extensions.Vehicles = new Vehicles();
                            response.TPA_Extensions.Vehicles.Items = new List<Vehicle>();

                            #region Modify Status
                            //convert trip status by our wrapper
                            if (!string.IsNullOrWhiteSpace(succ.TripInfo[0].Status))
                            {
                                status = succ.TripInfo[0].Status;
                                originalStatus = status;
                            }

                            #region Faster Meter option
                            //01. if status is Completed or Fare
                            //02. and fare exists and (Fare<=Min)
                            //03. then change status to fast meter
                            switch (tsp.Fleet.DetectFastMeter)
                            {
                                case FastMeterOption.ByMinFare:
                                    logger.Info("Detect fastmeter option: by min fare");
                                    if (utility.DetectFastMeterByMinFare(tsp.Trip.Id, status, succ.TripInfo[0].FareAmount, tsp.Fleet.MinPriceFastMeter))
                                    {
                                        status = TaxiTripStatusType.FastMeter;
                                    }
                                    break;
                                case FastMeterOption.Off:
                                    logger.Info("Detect fastmeter option is off");
                                    break;
                                default:
                                    logger.Info("Detect fastmeter option is off");
                                    break;
                            }
                            //isFastMeter = succ.TripInfo[0].IsFastMeter;
                            //if (isFastMeter)
                            //{
                            //	status = TaxiTripStatusType.FastMeter;
                            //}
                            #endregion

                            #region Status Wrapper
                            string target = utility.ConvertTripStatusForUDI33(status);
                            #endregion
                            //assign to response
                            status = target;
                            #endregion

                            #region Vehicle
                            var vehicle = new Vehicle();
                            vehicle.Geolocation = new Geolocation();
                            //vehicle.MinutesAway = succ.TripInfo[0].ETA;
                            vehicle.Distance = succ.TripInfo[0].Course.ToDecimal();
                            vehicle.Geolocation.Lat = succ.TripInfo[0].Latitude.ToDecimal();
                            vehicle.Geolocation.Long = succ.TripInfo[0].Longitude.ToDecimal();
                            if (!string.IsNullOrWhiteSpace(succ.TripInfo[0].Distance))
                            {
                                vehicle.Distance = succ.TripInfo[0].Distance.ToDecimal();
                            }
                            if (!string.IsNullOrWhiteSpace(succ.TripInfo[0].AssignedVehicleNo))
                            {
                                vehicle.ID = succ.TripInfo[0].AssignedVehicleNo;
                            }

                            #region Caculate ETA by ourself, not using UDI33

                            if (
                                (status.ToUpperInvariant().Equals(TaxiTripStatusType.Accepted.ToUpperInvariant()) || status.ToUpperInvariant().Equals(TaxiTripStatusType.Assigned.ToUpperInvariant()) || status.ToUpperInvariant().Equals(TaxiTripStatusType.InTransit.ToUpperInvariant()))
                                &&
                                (vehicle.Geolocation.Lat != 0 && vehicle.Geolocation.Long != 0)
                                &&
                                (tsp.Trip.taxi_trip.PickupLatitude.HasValue && tsp.Trip.taxi_trip.PickupLongitude.HasValue)
                            )
                            {
                                MapService ms = new MapService();
                                double d = ms.GetDirectDistanceInMeter(tsp.Trip.taxi_trip.PickupLatitude.Value, tsp.Trip.taxi_trip.PickupLongitude.Value, Convert.ToDouble(vehicle.Geolocation.Lat), Convert.ToDouble(vehicle.Geolocation.Long));
                                var rc = new UDI.SDS.RoutingController(tsp.tokenVTOD, null);
                                UDI.SDS.RoutingService.Point start = new UDI.SDS.RoutingService.Point { Latitude = tsp.Trip.taxi_trip.PickupLatitude.Value, Longitude = tsp.Trip.taxi_trip.PickupLongitude.Value };
                                UDI.SDS.RoutingService.Point end = new UDI.SDS.RoutingService.Point { Latitude = Convert.ToDouble(vehicle.Geolocation.Lat), Longitude = Convert.ToDouble(vehicle.Geolocation.Long) };
                                var result = rc.GetRouteMetrics(start, end);
                                vehicle.MinutesAway = result.TotalMinutes;

                            }
                            #endregion

                            #region ETA with Traffic
                            //ToDo: it should be configurable. It's hardcoded just for now but should be configuratble.
                            if (tsp.User.name.ToLower() == "ztrip" || !tsp.Trip.DriverAcceptedETA.HasValue)
                            {
                                if (
                                                        status.Trim().ToLower() == Const.TaxiTripStatusType.Accepted.Trim().ToLower() ||
                                                        status.Trim().ToLower() == Const.TaxiTripStatusType.Assigned.Trim().ToLower() ||
                                                        status.Trim().ToLower() == Const.TaxiTripStatusType.InTransit.Trim().ToLower()
                                                        )
                                {
                                    if (vehicle != null && vehicle.Geolocation != null && vehicle.Geolocation.Lat != 0 && vehicle.Geolocation.Long != 0)
                                    {
                                        if (tsp.Trip != null && tsp.Trip.taxi_trip != null && tsp.Trip.taxi_trip.PickupLatitude.HasValue && tsp.Trip.taxi_trip.PickupLatitude.Value != 0 && tsp.Trip.taxi_trip.PickupLongitude.HasValue && tsp.Trip.taxi_trip.PickupLongitude.Value != 0)
                                        {
                                            try
                                            {
                                                var error = string.Empty;
                                                var map = new UDI.Map.MapService(Map.DTO.MapApiProvider.Google);
                                                var etaResult = map.GetETA(new Map.DTO.Geolocation { Latitude = vehicle.Geolocation.Lat, Longitude = vehicle.Geolocation.Long },
                                                    new Map.DTO.Geolocation { Latitude = tsp.Trip.taxi_trip.PickupLatitude.Value.ToDecimal(), Longitude = tsp.Trip.taxi_trip.PickupLongitude.ToDecimal() }, UDI.Map.DTO.DistanceUnit.Mile, out error);

                                                if (string.IsNullOrWhiteSpace(error))
                                                {
                                                    etaWithTraffic = System.Math.Round((decimal)(etaResult.DurationInTrafficInSecond / 60)).ToInt32();
                                                    //if (etaWithTraffic < 3)
                                                    //	etaWithTraffic = 3;
                                                    vehicle.MinutesAwayWithTraffic = etaWithTraffic.ToDecimal();
                                                }
                                            }
                                            catch
                                            { }
                                        }
                                    }
                                }
                            }
                            #endregion

                            response.TPA_Extensions.Vehicles.Items.Add(vehicle);

                            #endregion

                            #region Get Driver Info
                            vtod_driver_info driverInfo = null;
                            if (tsp.Fleet.DriverInfo == UDI.VTOD.Common.Const.DriveInfoSource.TCS)
                            {
                                if (tsp.Trip.DriverInfoID.HasValue && tsp.Trip.DriverInfoID.Value > 0)
                                {
                                    using (var db = new DataAccess.VTOD.VTODEntities())
                                    {
                                        driverInfo = db.vtod_driver_info.FirstOrDefault(t => t.Id == tsp.Trip.DriverInfoID.Value);
                                    }
                                }
                                else
                                {
                                    if (tsp.tokenVTOD != null && succ.TripInfo[0].AssignedDriver != null && !string.IsNullOrWhiteSpace(succ.TripInfo[0].AssignedDriver))
                                    {
                                        driverInfo = utility.GetDriverInfoByDriverId(tsp.tokenVTOD.Username, tsp.Trip, status, succ.TripInfo[0].AssignedDriver);
                                    }
                                    if (driverInfo == null)
                                    {
                                        if (tsp.tokenVTOD != null && vehicle != null && !string.IsNullOrWhiteSpace(vehicle.ID))
                                        {
                                            driverInfo = utility.GetDriverInfoByVehicleNumber(tsp.tokenVTOD.Username, status, vehicle.ID, "", tsp.Trip);
                                        }
                                    }
                                }
                            }
                            else if (tsp.Fleet.DriverInfo == UDI.VTOD.Common.Const.DriveInfoSource.Local)
                            {
                                if (tsp.Trip.DriverInfoID.HasValue && tsp.Trip.DriverInfoID.Value > 0)
                                {
                                    if (tsp.Trip != null)
                                    {
                                        using (var db = new DataAccess.VTOD.VTODEntities())
                                        {
                                            driverInfo = db.vtod_driver_info.FirstOrDefault(t => t.Id == tsp.Trip.DriverInfoID.Value);
                                        }
                                    }
                                }
                                else
                                {
                                    if (tsp.tokenVTOD != null && tsp.lastTripStatus.DriverId != null && !string.IsNullOrWhiteSpace(tsp.lastTripStatus.DriverId))
                                    {
                                        using (var db = new DataAccess.VTOD.VTODEntities())
                                        {
                                            driverInfo = db.vtod_driver_info.FirstOrDefault(t => t.DriverID == tsp.lastTripStatus.DriverId && t.FleetID == tsp.Fleet.Id && t.FleetType == "Taxi");
                                            if (driverInfo != null)
                                            db.SP_vtod_Update_TripDriverInfoID(tsp.Trip.Id, driverInfo.Id);
                                        }

                                    }
                                }
                            }

                            #endregion

                            #region Making the result
                            //Convert status to front-end status
                            //string frontEndStatus = utility.ConvertTripStatusMessageForFrontEndDevice(tsp.Fleet_User.UserId, status, tsp.Fleet.FastMeterAction);
                            #region Front End Status
                            string frontEndStatus = utility.ConvertTripStatusMessageForFrontEndDevice(tsp.Fleet_User.UserId, status);
                            #endregion

                            Status s = new Common.DTO.OTA.Status { Value = frontEndStatus, Fare = succ.TripInfo[0].FareAmount.ToDecimal() };
                            response.TPA_Extensions.Statuses = new Statuses();
                            response.TPA_Extensions.Statuses.Status = new List<Status>();
                            response.TPA_Extensions.Statuses.Status.Add(s);
                            response.TPA_Extensions.Contacts = new Contacts();
                            response.TPA_Extensions.Contacts.Items = new List<Contact>();

                            #region Adding Driver Info and DispatchInfo
                            Contact dispatchContact = null;
                            Contact driverContact = null;

                            //Disptch Info
                            string dispatchPhone = tsp.Fleet.PhoneNumber;
                            #region Work around for zTrip ver 2.2, please delete this part after release of zTrip ver 2.3
                            //if (driverInfo != null && !string.IsNullOrEmpty(driverInfo.CellPhone))
                            //{
                            //	dispatchPhone = driverInfo.CellPhone;
                            //}
                            #endregion

                            //todo add logic here for expedia and other companies logos
                            if (tsp.User.name.ToLower() == "ztrip" || tsp.User.name.ToLower() == "udi")
                            {
                                dispatchContact = new Contact
                                {
                                    Type = Common.DTO.Enum.ContactType.Dispatch.ToString(),
                                    Name = tsp.Fleet.Alias,
                                    Telephone = new Telephone { PhoneNumber = dispatchPhone.CleanPhone10Digit() },
                                    PhotoUrl = ConfigHelper.GetDriverPhotoVersionDefault()
                                };
                            }
                            else
                            {
                                dispatchContact = new Contact
                                {
                                    Type = Common.DTO.Enum.ContactType.Dispatch.ToString(),
                                    Name = tsp.Fleet.Alias,
                                    Telephone = new Telephone { PhoneNumber = dispatchPhone.CleanPhone10Digit() }

                                };
                            }

                            response.TPA_Extensions.Contacts.Items.Add(dispatchContact);

                            //Driver Info
                            if (driverInfo != null && !string.IsNullOrEmpty(driverInfo.CellPhone))
                            {
                                driverContact = new Contact { Type = Common.DTO.Enum.ContactType.Driver.ToString(), Name = string.Format("{0} {1}", driverInfo.FirstName, driverInfo.LastName), Telephone = new Telephone { PhoneNumber = driverInfo.CellPhone.CleanPhone10Digit() }, PhotoUrl = driverInfo.PhotoUrl, ZtripDateOfDesignation = driverInfo.QualificationDate, ZtripDesignation = driverInfo.zTripQualified };
                                response.TPA_Extensions.Contacts.Items.Add(driverContact);
                            }


                            #endregion

                            if (tsp.Trip != null && tsp.Trip.taxi_trip.PickupLatitude.HasValue && tsp.Trip.taxi_trip.PickupLatitude.Value != 0 && tsp.Trip.taxi_trip.PickupLongitude.HasValue && tsp.Trip.taxi_trip.PickupLongitude.Value != 0)
                            {
                                response.TPA_Extensions.PickupLocation = new Pickup_Dropoff_Stop();
                                response.TPA_Extensions.PickupLocation.Address = new Common.DTO.OTA.Address();
                                response.TPA_Extensions.PickupLocation.Address.Latitude = tsp.Trip.taxi_trip.PickupLatitude.Value.ToString();
                                response.TPA_Extensions.PickupLocation.Address.Longitude = tsp.Trip.taxi_trip.PickupLongitude.Value.ToString();
                            }

                            if (tsp.Trip != null && tsp.Trip.taxi_trip.DropOffLatitude.HasValue && tsp.Trip.taxi_trip.DropOffLatitude.Value != 0 && tsp.Trip.taxi_trip.DropOffLongitude.HasValue && tsp.Trip.taxi_trip.DropOffLongitude.Value != 0)
                            {
                                response.TPA_Extensions.DropoffLocation = new Pickup_Dropoff_Stop();
                                response.TPA_Extensions.DropoffLocation.Address = new Common.DTO.OTA.Address();
                                response.TPA_Extensions.DropoffLocation.Address.Latitude = tsp.Trip.taxi_trip.DropOffLatitude.Value.ToString();
                                response.TPA_Extensions.DropoffLocation.Address.Longitude = tsp.Trip.taxi_trip.DropOffLongitude.Value.ToString();
                            }
                            #endregion
                            #region SharedETA Link
                            if (request.TPA_Extensions != null && request.TPA_Extensions.RateQualifiers != null && request.TPA_Extensions.RateQualifiers.Any())
                            {
                                string uniqueID = null;
                                string rateQualifier = null;
                                string sharedLink = null;
                                string appSharedLink = ConfigurationManager.AppSettings["SharedETALink"];
                                string link = null;
                                if (!String.IsNullOrWhiteSpace(type))
                                {
                                    link = "?t=" + type.Substring(0, 1).ToLower();
                                }
                                if (request.TPA_Extensions.RateQualifiers != null && request.TPA_Extensions.RateQualifiers.Any())
                                {
                                    rateQualifier = request.TPA_Extensions.RateQualifiers.FirstOrDefault().RateQualifierValue;
                                    link = link + "&RQ=" + rateQualifier;
                                }
                                if (request.Reference != null && request.Reference.Any())
                                {
                                    uniqueID = request.Reference.FirstOrDefault().ID;
                                }


                                link = link + "&id=" + UDI.Utility.Helper.Cryptography.ConvertStringToHex(uniqueID);
                                response.TPA_Extensions.SharedLink = appSharedLink + link;
                            }
                            #endregion

                            #region Save status into taxi_trip_status

                            #region Get StatusTime
                            DateTime? statusTime = null;
                            DateTime? tripStartTime = null;
                            DateTime? tripEndTime = null;

                            switch (status)
                            {
                                case TaxiTripStatusType.Accepted:
                                    statusTime = succ.TripInfo[0].StateTimes.DispatchTime;
                                    break;
                                case TaxiTripStatusType.Booked:
                                    statusTime = succ.TripInfo[0].StateTimes.BookingTime;
                                    break;
                                case TaxiTripStatusType.Canceled:
                                    statusTime = succ.TripInfo[0].StateTimes.CancelTime;
                                    break;
                                case TaxiTripStatusType.MeterOff:
                                case TaxiTripStatusType.Completed:
                                    statusTime = succ.TripInfo[0].StateTimes.MeterOffTime;
                                    break;
                                case TaxiTripStatusType.DispatchPending:
                                    //No specific time
                                    break;
                                case TaxiTripStatusType.FastMeter:
                                    //No specific time
                                    break;
                                case TaxiTripStatusType.UnMatched:
                                    //No specific time
                                    break;
                                case TaxiTripStatusType.Matched:
                                    //No specific time
                                    break;
                                case TaxiTripStatusType.InTransit:
                                    //No specific time
                                    break;
                                case TaxiTripStatusType.InService:
                                    statusTime = succ.TripInfo[0].StateTimes.OnSiteTime;
                                    break;
                                case TaxiTripStatusType.MeterON:
                                    statusTime = succ.TripInfo[0].StateTimes.MeterOnTime;
                                    break;
                                case TaxiTripStatusType.NoShow:
                                    statusTime = succ.TripInfo[0].StateTimes.NoTripTime;
                                    break;
                            }
                            if (!statusTime.HasValue || statusTime.Value <= DateTime.MinValue || statusTime.Value < new DateTime(2010, 1, 1))
                            {
                                if (tsp.Fleet != null && tsp.Fleet.ServerUTCOffset != null)
                                {
                                    statusTime = DateTime.UtcNow.FromUtcNullAble(tsp.Fleet.ServerUTCOffset);
                                }
                            }

                            if ((status.Trim().ToLower() == TaxiTripStatusType.InService.Trim().ToLower() || status.Trim().ToLower() == TaxiTripStatusType.MeterON.Trim().ToLower()) && (tsp.Trip.TripStartTime == null))
                            {
                                tripStartTime = statusTime;
                            }
                            else
                            {
                                tripStartTime = tsp.Trip.TripStartTime;
                            }

                            if ((status.Trim().ToLower() == TaxiTripStatusType.Completed.Trim().ToLower() || status.Trim().ToLower() == TaxiTripStatusType.MeterOff.Trim().ToLower()) && (tsp.Trip.TripEndTime == null))
                            {
                                tripEndTime = statusTime;
                            }
                            else
                            {
                                tripEndTime = tsp.Trip.TripEndTime;
                            }


                            #endregion


                            //For all save and update
                            // If one of previous tripStatus <> FastMeter => Then Update, else do not update

                            if (status.ToUpperInvariant() == TaxiTripStatusType.Canceled.ToUpperInvariant())
                            {
                                //cancel status 
                                utility.SaveTripStatus(tsp.Trip.Id, TaxiTripStatusType.Canceled, statusTime, null, null, null, null, null, null, etaWithTraffic, null, string.Empty, originalStatus, null);
                                utility.UpdateFinalStatus(tsp.Trip.Id, TaxiTripStatusType.Canceled, null, null, null, null, null);
                            }

                            else if (status.ToUpperInvariant() == TaxiTripStatusType.FastMeter.ToUpperInvariant())
                            {
                                //FastMeter status 
                                //utility.SaveTripStatus(tsp.Trip.Id, TaxiTripStatusType.FastMeter, statusTime, null, null, null, null, null, null, null, string.Empty, originalStatus, null);
                                decimal? overwriteFare = utility.DetermineOverwriteFare(tsp, response.TPA_Extensions.Vehicles.Items.First().Geolocation.Long, response.TPA_Extensions.Vehicles.Items.First().Geolocation.Lat, succ.TripInfo[0].FareAmount.ToDecimal());

                                response.TPA_Extensions.Statuses.Status[0].Fare = overwriteFare.HasValue ? overwriteFare.Value : response.TPA_Extensions.Statuses.Status[0].Fare;


                                //Save Status
                                utility.SaveTripStatus(tsp.Trip.Id, TaxiTripStatusType.FastMeter, statusTime, response.TPA_Extensions.Vehicles.Items.First().ID, response.TPA_Extensions.Vehicles.Items.First().Geolocation.Lat, response.TPA_Extensions.Vehicles.Items.First().Geolocation.Long, overwriteFare, succ.TripInfo[0].FareAmount.ToDecimal(), response.TPA_Extensions.Vehicles.Items.First().MinutesAway.ToInt32(), etaWithTraffic, succ.TripInfo[0].DriverName, string.Empty, originalStatus, succ.TripInfo[0].AssignedDriver);

                              
                                    utility.UpdateFinalStatus(tsp.Trip.Id, TaxiTripStatusType.FastMeter, null, null, null, null, null);
                            }
                            //else if (status.ToUpperInvariant() == TaxiTripStatusType.FastMeter.ToUpperInvariant())
                            //{
                            //    if (tsp.Fleet.FastMeterAction == TaxiFleetFastMeterAction.ReDispatch)
                            //    {
                            //        utility.SaveTripStatus_ReDispatch(tsp.Trip.Id, statusTime, null, null, null, null, null, null, null, string.Empty, originalStatus, null);
                            //        utility.UpdateFinalStatus(tsp.Trip.Id, TaxiTripStatusType.Booked, null, null, null);
                            //    }
                            //    else if (tsp.Fleet.FastMeterAction == TaxiFleetFastMeterAction.Cancel)
                            //    {
                            //        utility.SaveTripStatus(tsp.Trip.Id, TaxiTripStatusType.FastMeter, statusTime, null, null, null, null, null, null, null, string.Empty, originalStatus, null);
                            //        utility.UpdateFinalStatus(tsp.Trip.Id, TaxiTripStatusType.FastMeter, null, null, null);
                            //    }
                            //}
                            else
                            {
                                decimal? overwriteFare = utility.DetermineOverwriteFare(tsp, response.TPA_Extensions.Vehicles.Items.First().Geolocation.Long, response.TPA_Extensions.Vehicles.Items.First().Geolocation.Lat, succ.TripInfo[0].FareAmount.ToDecimal());

                                response.TPA_Extensions.Statuses.Status[0].Fare = overwriteFare.HasValue ? overwriteFare.Value : response.TPA_Extensions.Statuses.Status[0].Fare;


                                //Save Status
                                utility.SaveTripStatus(tsp.Trip.Id, status, statusTime, response.TPA_Extensions.Vehicles.Items.First().ID, response.TPA_Extensions.Vehicles.Items.First().Geolocation.Lat, response.TPA_Extensions.Vehicles.Items.First().Geolocation.Long, overwriteFare, succ.TripInfo[0].FareAmount.ToDecimal(), response.TPA_Extensions.Vehicles.Items.First().MinutesAway.ToInt32(), etaWithTraffic, succ.TripInfo[0].DriverName, string.Empty, originalStatus, succ.TripInfo[0].AssignedDriver);

                                //Save final status
                                //utility.UpdateFinalStatus(tsp.Trip.Id, status, Convert.ToDecimal(succ.TripInfo[0].FareAmount), tripStartTime, tripEndTime);

                                if (tsp.Trip.FinalStatus.ToLower() == TaxiTripStatusType.Accepted.ToLower() || tsp.Trip.FinalStatus.ToLower() == TaxiTripStatusType.Arrived.ToLower())
                                {
                                    utility.UpdateFinalStatus(tsp.Trip.Id, status, overwriteFare, null, tripStartTime, tripEndTime, etaWithTraffic);
                                }
                                else
                                {
                                    utility.UpdateFinalStatus(tsp.Trip.Id, status, overwriteFare, null, tripStartTime, tripEndTime, null);
                                }
                            }



                            #endregion
                        }
						else
						{
							var vtodException = VtodException.CreateException(ExceptionType.Taxi, 3004);//string error = Messages.Taxi_TripNotFound;
							string error = vtodException.ExceptionMessage.Message;// Messages.Taxi_TripNotFound;
							logger.Error(error);
							throw vtodException;
						}
					}
					else
					{

						var vtodException = VtodException.CreateException(ExceptionType.Taxi, 3004);//string error = Messages.Taxi_TripNotFound;
						string error = vtodException.ExceptionMessage.Message;// Messages.Taxi_TripNotFound;

						logger.Error(error);
						logger.Error(returnMsg.ErrorString);
						throw vtodException; // new Exception(error + returnMsg.ErrorString);
					}

				}
				else
				{
					string error = returnMsg.ErrorString;
					logger.Error(error);
					throw new TaxiException(error);
				}
				#endregion

				#region Write Taxi Log
				if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]) /*&&tsp.EnableResponseAndLog*/)
				{
					string requestMessage = udi33Request.XmlSerialize().ToString().Replace(" encoding=\"utf-16\"", "");
					string responseMessage = XMLObjectToString.StringFromMsgType<UDIXMLSchema.UDI>(returnMsg.UDIMsg);
					responseMessage = UDIUtils.IndentXMLString(responseMessage);
					utility.WriteTaxiLog(tsp.Trip.Id, TaxiServiceAPIConst.UDI33, TaxiServiceMethodType.Status, null, requestMessage, responseMessage, requestDateTime, responseDateTime);
				}
				#endregion
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw new TaxiException(ex.Message);
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);


			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_UDI33, "StatusWithoutFastMeter");
			#endregion

			return response;
		}

		public OTA_GroundCancelRS Cancel(TokenRS tokenVTOD, OTA_GroundCancelRQ request)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			OTA_GroundCancelRS response = null;

			TaxiUtility utility = new TaxiUtility(logger);

			#region [Taxi 3.0] Validate required fields
			TaxiCancelParameter tcp = null;
			if (!utility.ValidateCancelRequest(tokenVTOD, request, out tcp))
			{
				throw VtodException.CreateException(ExceptionType.Taxi, 5005);
			}
			#endregion

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion

			try
			{
				#region [Taxi 3.3] Do status first
				TaxiStatusParameter tsp = utility.ConvertTCPpToTSP(tcp);
				//bool isFastMeter = false;


				#region New way
				string tripStatus = tcp.Trip.FinalStatus;
				#endregion

				#region Old way
				//OTA_GroundResRetrieveRS status_response = Status(tsp);
				//string vtodStatus=utility.ConvertTripStatusForUDI33(status_response.TPA_Extensions.Statuses.Status[0].Value);
				#endregion

				if (utility.IsAbleToCancelThisTrip(tripStatus))
				{
					#region [Taxi 3.4] Send cancel request to UDI33
					response = CancelWithoutStatus(tcp);
					#endregion
				}
				else
				{
					throw VtodException.CreateException(ExceptionType.Taxi, 5001); //throw new TaxiException(Messages.Taxi_CancelFailure);
				}
				#endregion

			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw ex;
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);


			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_UDI33, "Cancel");
			#endregion

			return response;
		}
        public OTA_GroundGetFinalRouteRS GetFinalRoute(TokenRS tokenVTOD, OTA_GroundGetFinalRouteRQ request)
        {
            throw new NotImplementedException();
        }
        public OTA_GroundAvailRS GetVehicleInfo(TokenRS tokenVTOD, OTA_GroundAvailRQ request)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			var response = new OTA_GroundAvailRS();
			response.Success = new Success();
			response.TPA_Extensions = new TPA_Extensions();
			response.TPA_Extensions.Vehicles = new Vehicles();
			response.TPA_Extensions.Vehicles.Items = new List<Vehicle>();

			TaxiUtility utility = new TaxiUtility(logger);

			#region [Taxi 4.0] Validate required fields
			TaxiGetVehicleInfoParameter tgvip = null;
			if (!utility.ValidateGetVehicleInfoRequest(tokenVTOD, request, out tgvip))
			{
				string error = string.Format("This GetVehicleInfo request is invalid.");
				throw new ValidationException(error);
			}
			#endregion


			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion

			try
			{
				#region [Taxi 4.3] Send GetVehicleInfo request to UDI33
				UDIXMLSchema.UDI udi33Request = BuildGetVehicleInfoMsg(tgvip);
				DateTime requestDateTime = DateTime.Now;
				DateTime responseDateTime;
				UDIXMLMsg returnMsg = null;
				string errorMessage = "";

				if (SendRequestToUDI33(tgvip.Fleet, udi33Request, out returnMsg, out errorMessage, "GetVehicleInfo"))
				{
					responseDateTime = DateTime.Now;

					if (tgvip.EnableResponseAndLog)
					{

						if (returnMsg.MessageType == UDIMessageType.MTVehicleInfoSuccess)
						{

							UDIXMLSchema.UDIVehicleInfoSuccess succ = (UDIXMLSchema.UDIVehicleInfoSuccess)returnMsg.VehicleInfoSuccess;

							if (succ != null)
							{
								if (succ.Vehicles.Count() > 0)
								{

									MapService ms = new MapService();
									if (succ.Vehicles.Any())
									{
										//UDI33 cannot provide ETA for vehicles. We need to use supershuttle way to do it.
										//response.TPA_Extensions.Vehicles.NearestVehicleETA = Convert.ToDecimal(succ.Vehicles.OrderBy(x => x.ETA).Select(x => x.ETA).FirstOrDefault()).ToString();

										int shortestIndex = -1;
										double? shortestDistance = null;
										for (int index = 0; index < succ.Vehicles.Count(); index++)
										{
											double d = ms.GetDirectDistanceInMile(tgvip.Latitude, tgvip.Longtitude, succ.Vehicles[index].Latitude, succ.Vehicles[index].Longitude);
											succ.Vehicles[index].Distance = d;
											if (shortestDistance.HasValue)
											{
												if (shortestDistance.Value > d)
												{
													shortestDistance = d;
													shortestIndex = index;
												}
											}
											else
											{
												shortestDistance = d;
												shortestIndex = index;
											}
										}

										if (shortestIndex != -1)
										{
											//get ETA
											var rc = new UDI.SDS.RoutingController(tgvip.tokenVTOD, null);
											UDI.SDS.RoutingService.Point start = new UDI.SDS.RoutingService.Point { Latitude = tgvip.Latitude, Longitude = tgvip.Longtitude };
											UDI.SDS.RoutingService.Point end = new UDI.SDS.RoutingService.Point { Latitude = succ.Vehicles[shortestIndex].Latitude, Longitude = succ.Vehicles[shortestIndex].Longitude };
											var result = rc.GetRouteMetrics(start, end);
											response.TPA_Extensions.Vehicles.NearestVehicleETA = result.TotalMinutes.ToString();
										}

										response.TPA_Extensions.Vehicles.NumberOfVehicles = succ.Vehicles.Count().ToString();
									}
									else
									{
										response.TPA_Extensions.Vehicles.NumberOfVehicles = "0";
									}
                                    Random rnd = new Random();
									foreach (var item in succ.Vehicles.OrderBy(s => s.Distance))
									{
										var vehicle = new Vehicle();
										vehicle.Geolocation = new Geolocation();
										vehicle.Geolocation.Long = decimal.Parse(item.Longitude.ToString());
										vehicle.Geolocation.Lat = decimal.Parse(item.Latitude.ToString());
										vehicle.Geolocation.PriorLong = decimal.Parse(item.PriorLongitude.ToString());
										vehicle.Geolocation.PriorLat = decimal.Parse(item.PriorLatitude.ToString());
                                        if (item.Course != 0 && item.Course != null)
                                            vehicle.Course = decimal.Parse(item.Course.ToString());
                                        else
                                            vehicle.Course = rnd.Next(1, 359);
										//vehicle.Distance = item.Distance;
										vehicle.DriverID = item.DriverID;
										vehicle.DriverName = item.DriverName;
										vehicle.FleetID = item.Fleet;
										vehicle.ID = item.VehicleNo;
										vehicle.VehicleStatus = item.VehicleStatus;
										vehicle.VehicleType = item.VehicleType;
										vehicle.Distance = item.Distance.ToDecimal();
										//vehicle.Zone = item.Zone;
										vehicle.MinutesAway = Convert.ToDecimal(item.ETA.ToString());
										response.TPA_Extensions.Vehicles.Items.Add(vehicle);
									}
								}
								else
								{
									// no vehicles found
									logger.Info("Found no vehicles");
									response.TPA_Extensions.Vehicles.NumberOfVehicles = succ.Vehicles.Count().ToString();
								}
							}
							else
							{
								//dispatchResp.Result = DispatchResult.Success;
								logger.Info("returnMsg.VehicleInfoSuccess is null");
							}
						}
						else
						{
							logger.Info("Disable response and log for get vehicle information");
						}
					}
					else
					{
						//dispatchResp.Result = DispatchResult.Success;
						logger.InfoFormat("returnMsg.MessageType is not UDIMessageType.MTVehicleInfoSuccess. MessageType={0}", returnMsg.MessageType);
					}

				}
				else
				{
					string error = returnMsg.ErrorString;
					logger.Error(error);
					throw new TaxiException(error);
				}
				#endregion


				#region Write Taxi Log
				if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]) && tgvip.EnableResponseAndLog)
				{
					string requestMessage = udi33Request.XmlSerialize().ToString().Replace(" encoding=\"utf-16\"", "");
					string responseMessage = XMLObjectToString.StringFromMsgType<UDIXMLSchema.UDI>(returnMsg.UDIMsg);
					responseMessage = UDIUtils.IndentXMLString(responseMessage);
					utility.WriteTaxiLog(null, TaxiServiceAPIConst.UDI33, TaxiServiceMethodType.GetVehicleInfo, null, requestMessage, responseMessage, requestDateTime, responseDateTime);
				}
				#endregion
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw new TaxiException(ex.Message);
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_UDI33, "GetVehicleInfo");
			#endregion

			return response;
		}
		public bool NotifyDriver(TokenRS token, UtilityNotifyDriverRQ input)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			bool result = false;

			UtilityNotifyDriverRS rs = new UtilityNotifyDriverRS();
			TaxiUtility utility = new TaxiUtility(logger);
			TaxiSendMessageToVehicleParameter tsmtvp = new TaxiSendMessageToVehicleParameter();


			if (utility.ValidateSendMessageToVehicle(token, input, out tsmtvp))
			{


				//UDI33 only
				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
				#endregion

				try
				{
					#region [Taxi ?.?] SendMessageToVehicle
					UDIXMLSchema.UDI udi33Request = BuildSendMessageToVehicle(tsmtvp);
					DateTime requestDateTime = DateTime.Now;
					DateTime responseDateTime;
					UDIXMLMsg returnMsg = null;
					string errorMessage = "";
					if (SendRequestToUDI33(tsmtvp.Fleet, udi33Request, out returnMsg, out errorMessage, "SendMessageToVehicle"))
					{
						responseDateTime = DateTime.Now;

						logger.Info("Successful status.");

						if (returnMsg.MessageType == UDIMessageType.MTSendMDTSuccess)
						{
							UDIXMLSchema.UDIMDTSendMessage succ = (UDIXMLSchema.UDIMDTSendMessage)returnMsg.MDTSendMessage;

							if (succ != null)
							{
								result = true;
							}
							else
							{
								//string error = Messages.Taxi_UnableToSendMessageToVehicle;
								logger.Error("Sorry, we are unable to send message to vehicle.");
								throw VtodException.CreateException(ExceptionType.Taxi, 9003);//
							}
						}
						else
						{
							logger.Error("Sorry, we are unable to send message to vehicle.");
							throw VtodException.CreateException(ExceptionType.Taxi, 9003);//
						}

					}
					else
					{
						string error = returnMsg.ErrorString;
						logger.Error(error);
						throw new TaxiException(error);
					}
					#endregion

					#region Write Taxi Log
					if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]) /*&&tsp.EnableResponseAndLog*/)
					{
						string requestMessage = udi33Request.XmlSerialize().ToString().Replace(" encoding=\"utf-16\"", "");
						string responseMessage = XMLObjectToString.StringFromMsgType<UDIXMLSchema.UDI>(returnMsg.UDIMsg);
						responseMessage = UDIUtils.IndentXMLString(responseMessage);
						utility.WriteTaxiLog(tsmtvp.Trip.Id, TaxiServiceAPIConst.UDI33, TaxiServiceMethodType.SendMessageToVehicle, null, requestMessage, responseMessage, requestDateTime, responseDateTime);
					}
					#endregion
				}
				catch (Exception ex)
				{
					logger.ErrorFormat("Message:{0}", ex.Message);
					logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
					logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
					throw new TaxiException(ex.Message);
				}
				sw.Stop();
				logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_UDI33, "SendMessageToVehicle");
				#endregion


				if (result)
				{
					rs.Success = new Success();
				}
				else
				{
					throw VtodException.CreateException(ExceptionType.Taxi, 9003);// new TaxiException(Messages.Taxi_UnableToSendMessageToVehicle);
				}
			}
			else
			{
				throw VtodException.CreateException(ExceptionType.Taxi, 9003);//throw new TaxiException(Messages.Taxi_UnableToSendMessageToVehicle);
			}



			return result;
		}
        public  bool NotifyPickupDriver(TokenRS token, UtilityNotifyDriverRQ input)
        {
            throw new NotImplementedException();
        }
        public bool NotifyDispatchPendingDriver(TokenRS token, UtilityNotifyDriverRQ input)
        {
            throw new NotImplementedException();
        }
		public Common.DTO.OTA.Fleet GetAvailableFleets(TokenRS token, UtilityAvailableFleetsRQ input)
		{
			//result.FleetTypes.Items.Add(new Common.DTO.OTA.Fleet { Type = FleetType.Taxi.ToString(), Now = true, Later = true });
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			Common.DTO.OTA.Fleet result = null;

			try
			{
				TaxiUtility utility = new TaxiUtility(logger);
				bool pickMeUpNow = true;
				bool pickMeUpLater = true;
				if (utility.ValidateGetAvailableFleets(token, input, out pickMeUpNow, out pickMeUpLater))
				{
					result = new Common.DTO.OTA.Fleet { Type = FleetType.Taxi.ToString(), Now = pickMeUpNow.ToString(), Later = pickMeUpLater.ToString() };
				}
			}
			catch (Exception ex)
			{

				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw;
				//if (ex.GetType().Equals(typeof(ValidationException)))
				//{
				//	throw new ValidationException(ex.Message);
				//}
				//else
				//{
				//	throw VtodException.CreateException(ExceptionType.Taxi, 1011);// new TaxiException(Messages.Taxi_Common_UnableToGetAvailableFleets);
				//}
			}


			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
			return result;
		}
		#endregion
		#endregion

		#region UDI33 private part

		private Common.DTO.OTA.OTA_GroundCancelRS CancelWithoutStatus(TaxiCancelParameter tcp)
		{
			logger.Info("Start");
			Stopwatch sw = new Stopwatch();
			sw.Start();
			OTA_GroundCancelRS response = null;

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion

			DateTime? requestedLocalTime = null;
			if (tcp.Fleet != null && tcp.Fleet.ServerUTCOffset != null)
			{
				requestedLocalTime = DateTime.UtcNow.FromUtcNullAble(tcp.Fleet.ServerUTCOffset);
			}

			try
			{
				#region [Taxi 3.3] Send cancel request to UDI33
				UDIXMLSchema.UDI udi33Request = BuildCancelMsg(tcp);
				DateTime requestDateTime = DateTime.Now;
				DateTime responseDateTime;
				UDIXMLMsg returnMsg = null;
				string errorMessage = "";
				if (SendRequestToUDI33(tcp.Fleet, udi33Request, out returnMsg, out errorMessage, "CancelWithoutStatus"))
				{
					responseDateTime = DateTime.Now;

					logger.Info("Successful status.");
					if (tcp.EnableResponseAndLog)
					{
						if (returnMsg.MessageType == UDIMessageType.MTUpdateTripACK)
						{
							UDIXMLSchema.UDITripUpdateAck succ = (UDIXMLSchema.UDITripUpdateAck)returnMsg.UpdateTripAck;
							//utility.CancelTaxiTrip(tcp.Trip.Id);

							response = new OTA_GroundCancelRS();
							response.Success = new Success();
							response.EchoToken = tcp.request.EchoToken;
							response.Target = tcp.request.Target;
							response.Version = tcp.request.Version;
							response.PrimaryLangID = tcp.request.PrimaryLangID;

							response.Reservation = new Reservation();
							response.Reservation.CancelConfirmation = new CancelConfirmation();
							response.Reservation.CancelConfirmation.UniqueID = new UniqueID { ID = tcp.Trip.Id.ToString() };


							#region Set final status
							utility.UpdateFinalStatus(tcp.Trip.Id, TaxiTripStatusType.Canceled, null, null, null, null, null);
							#endregion

							#region Put status into taxi_trip_status
							//utility.SaveTripStatus(tcp.Trip.Id, TaxiTripStatusType.Canceled);
							//var time = DateTime.UtcNow.t
							//tcp.Fleet.ServerUTCOffset
							utility.SaveTripStatus(tcp.Trip.Id, TaxiTripStatusType.Canceled, requestedLocalTime, null, null, null, null, null, null, null, null, string.Empty, string.Empty, null);

							#endregion
						}
						else
						{
							string error = "Fail to do cancel. ";
							logger.Error(error);
							logger.Error(returnMsg.ErrorString);
							//throw new TaxiException(string.Format("{0}.\r\n{1}", error, returnMsg.ErrorString));
							throw VtodException.CreateException(ExceptionType.Taxi, 5001); //throw new TaxiException(Messages.Taxi_CancelFailure);
						}
					}
					else
					{
						logger.Info("Disable response and log for trip cancellation");
					}
				}
				else
				{
					string error = returnMsg.ErrorString;
					logger.Error(error);
					throw new TaxiException(error);
				}
				#endregion

				#region Write Taxi Log
				if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
				{
					string requestMessage = udi33Request.XmlSerialize().ToString().Replace(" encoding=\"utf-16\"", "");
					string responseMessage = XMLObjectToString.StringFromMsgType<UDIXMLSchema.UDI>(returnMsg.UDIMsg);
					responseMessage = UDIUtils.IndentXMLString(responseMessage);
					utility.WriteTaxiLog(tcp.Trip.Id, TaxiServiceAPIConst.UDI33, TaxiServiceMethodType.Cancel, null, requestMessage, responseMessage, requestDateTime, responseDateTime);
				}
				#endregion
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				throw new TaxiException(ex.Message);
			}
			sw.Stop();
			logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_UDI33, "CancelWithoutStatus");
			#endregion

			return response;

		}



		private UDIXMLHeader BuildUdiHeader(string DNI, string phoneNumber)
		{
			UDIXMLHeader oHeader = new UDIXMLHeader();
			// ---------------------------------
			// Create a new header for all types
			// ---------------------------------
			oHeader.CallNo = 0;
			oHeader.ChannelNo = 99;
			oHeader.ClientData = "ClientData";
			oHeader.ErrorString = "ErrorString";
			oHeader.HostData = "HostData";
			oHeader.LogToDB = true;
			oHeader.MessageTime = DateTime.Now;
			//oHeader.MessageType = (int)UDIMessageType.MTCallout; 
			//messagetype gets set within specific call.
			oHeader.PhoneInfo = new PhoneInfo();
			//   oHeader.PhoneInfo.AccountNo = "AcctNo";
			#region DNI
			oHeader.PhoneInfo.DialedNo = utility.ExtractDigits(DNI);
			#endregion

			#region Customer Phone#
			if (Regex.IsMatch(phoneNumber, "^[01](.*)"))
			{
				string convertPhone = Regex.Match(phoneNumber, "^[01](.*)").Groups[1].Value.ToString();
				oHeader.PhoneInfo.CallBack = utility.ExtractDigits(convertPhone);
				oHeader.PhoneInfo.CallerID = utility.ExtractDigits(convertPhone);

			}
			else
			{
				oHeader.PhoneInfo.CallBack = utility.ExtractDigits(phoneNumber);
				oHeader.PhoneInfo.CallerID = utility.ExtractDigits(phoneNumber);
			}

			#endregion


			oHeader.ReplyRequired = true;
			oHeader.SequenceNo = 1234;
			//oHeader.SourceType = "SrcType";
			//oHeader.Status = "Header Status";
			oHeader.Version = "Version1.0";

			return oHeader;
		}

		private UDIXMLSchema.UDI BuildBookOrderMsg(TaxiBookingParameter tbp)
		{
			vtod_trip t = tbp.Trip;
			//taxi_customer customer = tbp.Customer;
			UDIXMLSchema.UDI oUDI = new UDIXMLSchema.UDI();
			oUDI.Items = new object[1];
			UDIXMLSchema.Address oAddress = new UDIXMLSchema.Address();
			UDIBookOrder oBookOrder = new UDIBookOrder();

			#region Header
			oBookOrder.UDIHeader = BuildUdiHeader(t.taxi_trip.APIInformation, t.taxi_trip.PhoneNumber);
			oBookOrder.UDIHeader.MessageType = (int)UDIMessageType.MTBookingOpen;
			#endregion

			#region Rider
			oBookOrder.Rider = new Rider();
			#endregion

			#region BitFields
			oBookOrder.Rider.BitFields = new BitFields();

			if (t.taxi_trip.wheelchairAccessible)
			{
				oBookOrder.Rider.BitFields.VehicleAttr = "Accessible";
				oBookOrder.Rider.BitFields.VehicleBits = 1;
			}
			#endregion

			#region NameInfo
			oBookOrder.Rider.NameInfo = new NameInfo[1];
			oBookOrder.Rider.NameInfo[0] = new NameInfo();
			oBookOrder.ReferenceNo = t.ConsumerRef;
			oBookOrder.Rider.NameInfo[0].First = tbp.FirstName;
			oBookOrder.Rider.NameInfo[0].FullName = string.Format("{0} {1}", tbp.FirstName, tbp.LastName);
			oBookOrder.Rider.NameInfo[0].Last = tbp.LastName;
			#endregion

			#region PhoneInfo
			oBookOrder.Rider.PhoneInfo = new PhoneInfo(); //NEED DATA IN HERE
			//if (!string.IsNullOrWhiteSpace(t.AccountNumber))
			//{
			oBookOrder.Rider.PhoneInfo.AccountNo = t.taxi_trip.AccountNumber; //Users account # Configuration Setting
			//}

			//oBookOrder.Rider.PhoneInfo.CallBack = get9DigitPhone(phone);  //Users phone digits only
			//oBookOrder.Rider.PhoneInfo.CallerID = get9DigitPhone(phone); //Users phone digits only
			oBookOrder.Rider.PhoneInfo.CallBack = tbp.PhoneNumber;  //Users phone digits only
			oBookOrder.Rider.PhoneInfo.CallerID = tbp.PhoneNumber; //Users phone digits only
			oBookOrder.Rider.PhoneInfo.DialedNo = t.taxi_trip.APIInformation; //DNI
			#endregion

			#region Pickup
			oBookOrder.Rider.Pickup = new Pickup[1];
			oBookOrder.Rider.Pickup[0] = new Pickup();

			#region Pickup Address
			if (!string.IsNullOrEmpty(t.taxi_trip.PickupFullAddress))
			{
				oBookOrder.Rider.Pickup[0].PickupAddress = new UDIXMLSchema.Address[1];
				oBookOrder.Rider.Pickup[0].PickupAddress[0] = new UDIXMLSchema.Address();
				//if (!string.IsNullOrWhiteSpace(t.PickupAirport))
				//{
				//	oBookOrder.Rider.Pickup[0].PickupAddress[0].AirportName = t.PickupAirport;
				//	oBookOrder.Rider.Pickup[0].PickupAddress[0].Longitude = tbp.LongitudeForPickupAirport;
				//	oBookOrder.Rider.Pickup[0].PickupAddress[0].Latitude = tbp.LatitudeForPickupAirport;
				//}
				oBookOrder.Rider.Pickup[0].PickupAddress[0].AddressType = 1;
				oBookOrder.Rider.Pickup[0].PickupAddress[0].AddrType1 = new AddrType1();
				oBookOrder.Rider.Pickup[0].PickupAddress[0].AddrType1.StreetName = t.taxi_trip.PickupStreetName; //N Marengo Ave
				oBookOrder.Rider.Pickup[0].PickupAddress[0].AddrType1.AddrLine2 = t.taxi_trip.PickupFullAddress; //Full Address
				oBookOrder.Rider.Pickup[0].PickupAddress[0].Bldg = t.taxi_trip.PickupBuilding;
				oBookOrder.Rider.Pickup[0].PickupAddress[0].City = t.taxi_trip.PickupCity;
				//oBookOrder.Rider.Pickup[0].PickupAddress[0].Comments = new string[1];


				List<string> comments = new List<string>();
				//1. comment for driver: remark
				comments.Add(string.IsNullOrWhiteSpace(t.taxi_trip.RemarkForPickup) ? string.Empty : t.taxi_trip.RemarkForPickup);
				comments.Add(string.IsNullOrWhiteSpace(t.taxi_trip.RemarkForDropOff) ? string.Empty : t.taxi_trip.RemarkForDropOff);



				//2. range street number
				//Sometimes we have range of street number, lilne 16 – 50 Marengo ave
				//We take the first part “16” and put following sentence in remark[1]
				if (t.taxi_trip.PickupStreetNo != null && t.taxi_trip.PickupStreetNo.ToArray().Where(x => x.Equals('-')).Any())
				{
					string[] sep = { "-" };
					string[] streetNumbers = t.taxi_trip.PickupStreetNo.Split(sep, StringSplitOptions.RemoveEmptyEntries);
					oBookOrder.Rider.Pickup[0].PickupAddress[0].AddrType1.StreetNo = streetNumbers[0];
					comments.Add(string.Format("Customer on {0} block of {1}", t.taxi_trip.PickupStreetNo, t.taxi_trip.PickupStreetName));
				}
				else if (t.taxi_trip.PickupStreetNo != null && t.taxi_trip.PickupStreetNo.ToArray().Where(x => x.Equals('–')).Any())
				{
					string[] sep = { "–" };
					string[] streetNumbers = t.taxi_trip.PickupStreetNo.Split(sep, StringSplitOptions.RemoveEmptyEntries);
					oBookOrder.Rider.Pickup[0].PickupAddress[0].AddrType1.StreetNo = streetNumbers[0];
					comments.Add(string.Format("Customer on {0} block of {1}", t.taxi_trip.PickupStreetNo, t.taxi_trip.PickupStreetName));
				}
				else
				{
					oBookOrder.Rider.Pickup[0].PickupAddress[0].AddrType1.StreetNo = t.taxi_trip.PickupStreetNo;
				}

				//3. if the request contains only geolocation without correct address block, we use a closed adress for it.
				//But we need to remind the driver about this.
				if (tbp.PickupAddressOnlyContainsLatAndLong)
				{
					comments.Add(string.Format("Call customer for exact pickup location."));
				}




				oBookOrder.Rider.Pickup[0].PickupAddress[0].Comments = comments.ToArray();


				//  oRS.Rider.Pickup[0].PickupAddress[0].GateCode = "Gate 456";
				oBookOrder.Rider.Pickup[0].PickupAddress[0].PhoneNo = new string[1];
				oBookOrder.Rider.Pickup[0].PickupAddress[0].PhoneNo[0] = tbp.PhoneNumber;
				oBookOrder.Rider.Pickup[0].PickupAddress[0].State = t.taxi_trip.PickupStateCode;
				//  oRS.Rider.Pickup[0].PickupAddress[0].TimeZone = 1;
				oBookOrder.Rider.Pickup[0].PickupAddress[0].Unit = t.taxi_trip.PickupAptNo;
				oBookOrder.Rider.Pickup[0].PickupAddress[0].Zip = t.taxi_trip.PickupZipCode;

				if (t.taxi_trip.PickupLatitude.HasValue)
				{
					oBookOrder.Rider.Pickup[0].PickupAddress[0].Latitude = t.taxi_trip.PickupLatitude.Value;
				}
				if (t.taxi_trip.PickupLongitude.HasValue)
				{
					oBookOrder.Rider.Pickup[0].PickupAddress[0].Longitude = t.taxi_trip.PickupLongitude.Value;
				}
				oBookOrder.Rider.Pickup[0].PickupIndex = 0;
			}
			#endregion

			#endregion

			#region Dropoff
			oBookOrder.Rider.Dropoff = new Dropoff[1];
			oBookOrder.Rider.Dropoff[0] = new Dropoff();
			oBookOrder.Rider.Dropoff[0].DropoffAddress = new UDIXMLSchema.Address[1];
			oBookOrder.Rider.Dropoff[0].DropoffAddress[0] = new UDIXMLSchema.Address();



			#region Dropoff address
			if (!string.IsNullOrEmpty(t.taxi_trip.DropOffFullAddress))
			{
				//oBookOrder.Rider.Dropoff[0].DropoffAddress = new UDIXMLSchema.Address[1];
				//oBookOrder.Rider.Dropoff[0].DropoffAddress[0] = new UDIXMLSchema.Address();
				oBookOrder.Rider.Dropoff[0].DropoffAddress[0].AddressType = 1;
				if (!string.IsNullOrWhiteSpace(t.taxi_trip.DropOffAirport))
				{
					oBookOrder.Rider.Dropoff[0].DropoffAddress[0].AirportName = t.taxi_trip.DropOffAirport;
					oBookOrder.Rider.Dropoff[0].DropoffAddress[0].Longitude = tbp.LongitudeForDropoffAirport;
					oBookOrder.Rider.Dropoff[0].DropoffAddress[0].Latitude = tbp.LatitudeForDropoffAirport;
				}

				oBookOrder.Rider.Dropoff[0].DropoffAddress[0].AddrType1 = new AddrType1();
				//oBookOrder.Rider.Dropoff[0].DropoffAddress[0].AddrType1.StreetName = string.Format("{0} {1}", t.DropOffStreetName, t.DropOffStreetType); //N Marengo Ave
				oBookOrder.Rider.Dropoff[0].DropoffAddress[0].AddrType1.StreetName = t.taxi_trip.DropOffStreetName;
				oBookOrder.Rider.Dropoff[0].DropoffAddress[0].AddrType1.StreetNo = t.taxi_trip.DropOffStreetNo;
				oBookOrder.Rider.Dropoff[0].DropoffAddress[0].AddrType1.AddrLine2 = t.taxi_trip.DropOffFullAddress;
				oBookOrder.Rider.Dropoff[0].DropoffAddress[0].Bldg = t.taxi_trip.DropOffBuilding;
				oBookOrder.Rider.Dropoff[0].DropoffAddress[0].City = t.taxi_trip.DropOffCity;


				if (tbp.DropOffAddressOnlyContainsLatAndLong)
				{
					oBookOrder.Rider.Dropoff[0].DropoffAddress[0].Comments = new string[2];
					oBookOrder.Rider.Dropoff[0].DropoffAddress[0].Comments[0] = string.IsNullOrWhiteSpace(t.taxi_trip.RemarkForDropOff) ? string.Empty : t.taxi_trip.RemarkForDropOff;
					string dropoffaddresscomment = string.Format("Ask customer for exact drop location.");
					oBookOrder.Rider.Dropoff[0].DropoffAddress[0].Comments[1] = dropoffaddresscomment;
				}
				else
				{
					oBookOrder.Rider.Dropoff[0].DropoffAddress[0].Comments = new string[1];
					oBookOrder.Rider.Dropoff[0].DropoffAddress[0].Comments[0] = string.IsNullOrWhiteSpace(t.taxi_trip.RemarkForDropOff) ? string.Empty : t.taxi_trip.RemarkForDropOff;
				}


				oBookOrder.Rider.Dropoff[0].DropoffAddress[0].State = t.taxi_trip.DropOffStateCode;
				oBookOrder.Rider.Dropoff[0].DropoffAddress[0].Unit = t.taxi_trip.DropOffAptNo;
				oBookOrder.Rider.Dropoff[0].DropoffAddress[0].Zip = t.taxi_trip.DropOffZipCode;

				if (t.taxi_trip.DropOffLatitude.HasValue)
				{
					oBookOrder.Rider.Dropoff[0].DropoffAddress[0].Latitude = t.taxi_trip.DropOffLatitude.Value;
				}
				if (t.taxi_trip.DropOffLongitude.HasValue)
				{
					oBookOrder.Rider.Dropoff[0].DropoffAddress[0].Longitude = t.taxi_trip.DropOffLongitude.Value;
				}


			}
			#endregion
			#endregion

			#region PickupFlatRateZone
			oBookOrder.Rider.Pickup[0].PickupAddress[0].Zone = t.taxi_trip.PickupFlatRateZone;
			oBookOrder.Rider.Dropoff[0].DropoffAddress[0].Zone = t.taxi_trip.DropOffFlatRateZone;
			#endregion

			#region pickup up time
			oBookOrder.Rider.Pickup[0].PickupTimeString = t.PickupDateTime.Value.ToString();
			if (t.PickupDateTime.HasValue)
			{
				oBookOrder.Rider.Pickup[0].PickupTime = t.PickupDateTime.Value;
			}
			else
			{
				oBookOrder.Rider.Pickup[0].PickupTime = DateTime.MinValue;
			}
			#endregion

			#region RideInof
			oBookOrder.Rider.RideInfo = new RideInfo();
			//oRS.Rider.RideInfo.Other = "Ride Info other";
			//oRS.Rider.RideInfo.Passengers = 1;
			//oRS.Rider.RideInfo.SpecialNeeds = "Special Needs";
			//oRS.Rider.RideInfo.VehicleType = "Vehicle Type";
			//oRS.Rider.RiderID = "RiderID";
			#endregion

			#region Trip Info
			oBookOrder.Rider.TripInfo = new UDIXMLSchema.TripInfo();
            #endregion

            #region Tip Info

            if (tbp.Gratuity!=null)
            {

                if (tbp.Gratuity.Contains("%"))
                {
                    //calculate by Percentage;
                    tbp.Gratuity = tbp.Gratuity.Replace("%", "").ToString();
                    //oBookOrder.Rider.TripInfo.TipAmount = ecbp.Rate.ToDecimal() * t.Gratuity * new decimal(0.01);
                    oBookOrder.Rider.TripInfo.TipPercent = tbp.Gratuity.ToInt64();
                }
                else
                {
                    //calculate by gratuity;
                    oBookOrder.Rider.TripInfo.TipAmount = tbp.Gratuity.ToInt64();
                }


            }
          
            #endregion
            #region Driver and Vechilce attr,bits
            if (!string.IsNullOrWhiteSpace(tbp.Fleet.DriverAttr))
			{
				oBookOrder.Rider.BitFields.DriverAttr = tbp.Fleet.DriverAttr;
			}

			if (tbp.Fleet.DriverBits.HasValue)
			{
				oBookOrder.Rider.BitFields.DriverBits = tbp.Fleet.DriverBits.Value;
			}

			if (!string.IsNullOrWhiteSpace(tbp.Fleet.VehicleAttr))
			{
				oBookOrder.Rider.BitFields.VehicleAttr = tbp.Fleet.VehicleAttr;
			}

			if (tbp.Fleet.VehicleBits.HasValue)
			{
				oBookOrder.Rider.BitFields.VehicleBits = tbp.Fleet.VehicleBits.Value;
			}

			#endregion

			oUDI.Items[0] = oBookOrder;

			return oUDI;
		}

		private UDIXMLSchema.UDI BuildTripStatusMsg(TaxiStatusParameter tsp)
		{

			UDIXMLSchema.UDI oUDI = new UDIXMLSchema.UDI();
			oUDI.Items = new object[1];

			UDIGetTripStatus statusMsg = new UDIGetTripStatus();
			statusMsg.ConfirmationNo = tsp.Trip.taxi_trip.DispatchTripId;
			statusMsg.UDIHeader = BuildUdiHeader(tsp.Trip.taxi_trip.APIInformation, tsp.Trip.taxi_trip.PhoneNumber);
			statusMsg.UDIHeader.MessageType = (int)UDIMessageType.MTTripStatusOpen;
			statusMsg.UDIHeader.SourceType = tsp.User.name;
			oUDI.Items[0] = statusMsg;

			return oUDI;
		}

		private UDIXMLSchema.UDI BuildSendMessageToVehicle(TaxiSendMessageToVehicleParameter tsmtvp)
		{

			UDIXMLSchema.UDI oUDI = new UDIXMLSchema.UDI();
			oUDI.Items = new object[1];
			UDIMDTSendMessage tsntvpMsg = new UDIMDTSendMessage();

			#region Header
			tsntvpMsg.UDIHeader = BuildUdiHeader(tsmtvp.Trip.APIInformation, tsmtvp.Trip.PhoneNumber);
			tsntvpMsg.UDIHeader.MessageType = (int)UDIMessageType.MTSendMDTMessage;
			tsntvpMsg.UDIHeader.MessageName = UDIMessageType.MTSendMDTMessage.ToString();


			//tsntvpMsg.UDIHeader.Status=UDIMessageType.He
			#endregion

			#region body
			tsntvpMsg.MDTMessage = new SendMDTMessage();
			tsntvpMsg.MDTMessage.Company = false;
			tsntvpMsg.MDTMessage.AllCars = false;
			tsntvpMsg.MDTMessage.VehicleNo = tsmtvp.VehicleNunber;
			tsntvpMsg.MDTMessage.Message = tsmtvp.Message;
			#endregion

			oUDI.Items[0] = tsntvpMsg;

			return oUDI;
		}

		private UDIXMLSchema.UDI BuildCancelMsg(TaxiCancelParameter tcp)
		{
			UDIXMLSchema.UDI oUDI = new UDIXMLSchema.UDI();
			oUDI.Items = new object[1];

			UDITripUpdate tripUpdateMsg = new UDITripUpdate();
			tripUpdateMsg.ConfirmationNo = tcp.Trip.taxi_trip.DispatchTripId;
			tripUpdateMsg.UDIHeader = BuildUdiHeader(tcp.Trip.taxi_trip.APIInformation, tcp.Trip.taxi_trip.PhoneNumber);
			tripUpdateMsg.UDIHeader.MessageType = (int)UDIMessageType.MTTripStatusUpdate;
			tripUpdateMsg.UpdateType = "15"; //CANCEL
			tripUpdateMsg.UpdateData = string.Format("MM/dd/yyyy hh:mm:sst", DateTime.Now);
			oUDI.Items[0] = tripUpdateMsg;

			return oUDI;
		}

		private UDIXMLSchema.UDI BuildGetVehicleInfoMsg(TaxiGetVehicleInfoParameter tgvip)
		{
			UDIXMLSchema.UDI oUDI = new UDIXMLSchema.UDI();
			oUDI.Items = new object[1];

			UDIGetVehicleInfo vehicleInfoMsg = new UDIGetVehicleInfo();

			vehicleInfoMsg.Circle = new UDIXMLSchema.Circle { Latitude = tgvip.Latitude, Longitude = tgvip.Longtitude, Radius = tgvip.Radius };
			if (tgvip.VehicleStausList.Any())
			{
				vehicleInfoMsg.VehicleStatus = tgvip.VehicleStausList.Select(x => x.VehicleStatus).ToArray();
			}
			else
			{
				vehicleInfoMsg.VehicleStatus = new string[] { "" };
			}
			//tripUpdateMsg.ConfirmationNo = confirmationNo;
			//vehicleInfoMsg.UDIHeader = buildUdiHeader(dialedNo, customerPhoneNo);
			vehicleInfoMsg.UDIHeader = BuildUdiHeader(tgvip.APIInformation, tgvip.APIInformation);

			vehicleInfoMsg.UDIHeader.MessageType = (int)UDIMessageType.MTGetVehicleInfo;
			//tripUpdateMsg.UpdateType = "15"; //CANCEL
			//tripUpdateMsg.UpdateData = string.Format("MM/dd/yyyy hh:mm:sst", DateTime.Now);

			oUDI.Items[0] = vehicleInfoMsg;

			return oUDI;
		}

		private bool SendRequestToUDI33(taxi_fleet fleet, UDIXMLSchema.UDI udiMsg, out UDIXMLMsg XMLMsg, out string errorMessage, string actionName)
		{
			logger.Info("Start");
			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
			#endregion

			Stopwatch sw = new Stopwatch();
			sw.Start();

			bool success = false;
			int ShortOrLongXML = sizeof(long);
			XMLMsg = new UDIXMLMsg(ShortOrLongXML);
			XMLMsg.UDIMsg = udiMsg;
			XMLMsg.Compression = true;
			errorMessage = "";

			TcpClient _client = null;
			NetworkStream _netStream = null;
			try
			{

				#region OpenStream
				logger.InfoFormat("Udi33IpAddress={0}, Udi33PortNo={1}, ReceiveTimeout={2}", fleet.UDI33IPAddress, fleet.UDI33PortNo, fleet.UDI33ReceivedTimeout);
				_client = new TcpClient(fleet.UDI33IPAddress, fleet.UDI33PortNo.Value);
				_client.ReceiveTimeout = fleet.UDI33ReceivedTimeout.Value;
				logger.InfoFormat("GetStream");
				_netStream = _client.GetStream();
				#endregion

				//logger.InfoFormat("Message={0}",serializeMessage(udiMsg));
				var udi33RequestXml = udiMsg.XmlSerialize().ToString().Replace(" encoding=\"utf-16\"", "");
				logger.InfoFormat("Message={0}", udi33RequestXml);
				if (XMLMsg.Write(_client.Client))
				{
					logger.Debug("Write XMLMsg Successful");

				}
				else
				{
					logger.Error("Write XMLMsg Failed");
				}

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_Domain, "Call");
				#endregion

				string xmlMessage = "";
				UDIMessageType messageType = XMLMsg.Read(_client.Client);

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_UDI33, "GetXMLMessageFromUDI33 in SendRequestToUDI33");
				#endregion

				switch (messageType)
				{
					case UDIMessageType.MTRider:
						logger.Info(String.Format("  Rider SUCCESS:{0}", XMLMsg.RiderSuccess));
						xmlMessage = XMLObjectToString.StringFromMsgType<UDIRiderSuccess>(XMLMsg.RiderSuccess);
						success = true;
						break;
					case UDIMessageType.MTRiderError:
						logger.Info(String.Format("  Rider ERROR:{0}", XMLMsg.RiderError));
						logger.Info(String.Format("  Error String:{0}", XMLMsg.RiderError.UDIHeader.ErrorString));
						errorMessage = string.Format("  Rider ERROR:{0} Error String:{1}", XMLMsg.RiderError, XMLMsg.RiderError.UDIHeader.ErrorString);
						xmlMessage = XMLObjectToString.StringFromMsgType<UDIRiderError>(XMLMsg.RiderError);
						break;
					case UDIMessageType.MTBookingError:
						logger.Info(String.Format("  Booking ERROR:{0}", XMLMsg.BookingError));
						logger.Info(String.Format("  Error String:{0}", XMLMsg.BookingError.UDIHeader.ErrorString));
						errorMessage = string.Format("  Booking ERROR:{0} Error String:{1}", XMLMsg.BookingError, XMLMsg.BookingError.UDIHeader.ErrorString);
						xmlMessage = XMLObjectToString.StringFromMsgType<UDIBookingError>(XMLMsg.BookingError);
						break;
					case UDIMessageType.MTBooking:
						logger.Info(String.Format("  Booking SUCCESS:{0}", XMLMsg.BookingSuccess));
						logger.Info(String.Format("  Confirmation No:{0}", XMLMsg.BookingSuccess.Order.ConfirmationNo));
						xmlMessage = XMLObjectToString.StringFromMsgType<UDIBookingSuccess>(XMLMsg.BookingSuccess);
						success = true;
						break;
					case UDIMessageType.MTRiderOpen:
						logger.Info(String.Format("  RiderOpen Message Received:{0}", XMLMsg.GetRider));
						xmlMessage = XMLObjectToString.StringFromMsgType<UDIGetRider>(XMLMsg.GetRider);
						success = true;
						break;
					case UDIMessageType.MTCalloutACK:
						logger.Info(String.Format("  MTCalloutACK Message Received: {0}", XMLMsg.CalloutAck));
						xmlMessage = XMLObjectToString.StringFromMsgType<UDICalloutAck>(XMLMsg.CalloutAck);
						success = true;
						break;
					case UDIMessageType.MTCalloutError:
						logger.Info(String.Format("  MTCalloutError Message Received: {0}", XMLMsg.CalloutError));
						xmlMessage = XMLObjectToString.StringFromMsgType<UDICalloutError>(XMLMsg.CalloutError);
						break;
					case UDIMessageType.MTTripStatus:
						logger.Info(String.Format("  MTTripStatus Message Received: {0}", XMLMsg.TripStatus));
						xmlMessage = XMLObjectToString.StringFromMsgType<UDITripStatus>(XMLMsg.TripStatus);
						success = true;
						break;
					case UDIMessageType.MTTripStatusError:
						logger.Info(String.Format("  MTTripStatusError Message Received: {0}", XMLMsg.TripStatusError));
						errorMessage = string.Format("  MTTripStatusError Message Received: {0}", XMLMsg.TripStatusError);
						xmlMessage = XMLObjectToString.StringFromMsgType<UDITripStatusError>(XMLMsg.TripStatusError);
						break;
					//case UDIMessageType.MTCallout:
					//    logger.Info(String.Format("  MTCallout Message Received: {0}", XMLMsg.Callout));
					//    xmlMessage = XMLObjectToString.StringFromMsgType<UDICallout>(XMLMsg.Callout);
					//    success = true; 
					//    break;
					case UDIMessageType.MTUpdateTripACK:
						logger.Info(String.Format("  MTUpdateTripACL Message Received: {0}", XMLMsg.UpdateTripAck));
						xmlMessage = XMLObjectToString.StringFromMsgType<UDITripUpdateAck>(XMLMsg.UpdateTripAck);
						success = true;
						break;
					case UDIMessageType.MTValidateAddressSuccess:
						logger.Info(String.Format("  MTValidateAddressSuccess Message Received: {0}", XMLMsg.ValidateAddressSuccess));
						xmlMessage = XMLObjectToString.StringFromMsgType<UDIValidateAddressSuccess>(XMLMsg.ValidateAddressSuccess);
						success = true;
						break;
					case UDIMessageType.MTValidateAddressError:
						logger.Info(String.Format("  MTValidateAddressSuccess Message Received: {0}", XMLMsg.ValidateAddressError));
						xmlMessage = XMLObjectToString.StringFromMsgType<UDIValidateAddressError>(XMLMsg.ValidateAddressError);
						success = true;
						break;
					case UDIMessageType.MTVehicleInfoSuccess:
						logger.Info(String.Format("  MTVehicleInfoSuccess Message Received: {0}", XMLMsg.VehicleInfoSuccess));
						xmlMessage = XMLObjectToString.StringFromMsgType<UDIVehicleInfoSuccess>(XMLMsg.VehicleInfoSuccess);
						success = true;
						break;
					case UDIMessageType.MTVehicleInfoError:
						logger.Info(String.Format("  MTVehicleInfoError Message Received: {0}", XMLMsg.VehicleInfoError));
						xmlMessage = XMLObjectToString.StringFromMsgType<UDIVehicleInfoError>(XMLMsg.VehicleInfoError);
						success = true;
						break;
					case UDIMessageType.MTSendMDTSuccess:
						logger.Info(String.Format("  MTSendMDTSuccess Message Received: {0}", XMLMsg.MDTSendSuccess));
						xmlMessage = XMLObjectToString.StringFromMsgType<UDIMDTSendMessage>(XMLMsg.MDTSendMessage);
						success = true;
						break;
					case UDIMessageType.MTSendMDTError:
						logger.Info(String.Format("  MTSendMDTSuccess Message Received: {0}", XMLMsg.MDTSendError));
						xmlMessage = XMLObjectToString.StringFromMsgType<UDIMDTSendMessage>(XMLMsg.MDTSendMessage);
						success = true;
						break;
					default:
						logger.Info(String.Format("  Other MessageType received: {0}", XMLMsg.MessageType));
						success = true;
						xmlMessage = XMLMsg.ToString();
						break;


				}


				xmlMessage = XMLObjectToString.StringFromMsgType<UDIXMLSchema.UDI>(XMLMsg.UDIMsg);
				xmlMessage = UDIUtils.IndentXMLString(xmlMessage);
				if (actionName != "GetVehicleInfo")
				{
					logger.Info(String.Format("\r{0}\r", xmlMessage));
				}
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("Message:{0}", ex.Message);
				logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
				logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
				logger.Error("Cannot connect to UDI33");
				throw VtodException.CreateException(ExceptionType.Taxi, 302); //throw new TaxiException(Messages.Taxi_UnableToConnectUDI33);
			}
			finally
			{
				logger.Info("Closing Socket");
				if (_client != null)
				{
					_netStream.Flush();
					_netStream.Dispose();
					if (_client.Connected)
					{
						_client.Client.Disconnect(true);
					}
					_client.Close();
				}

			}

			logger.InfoFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Taxi_UDI33, string.Format("SendRequestToUDI33_{0}", actionName));
			#endregion

			return success;
		}

		#endregion

		#region Properties
		public TrackTime TrackTime { get; set; }
        #endregion

        public  bool GetDisabilityInd(TokenRS tokenVTOD, taxi_fleet f, out int condition, string AuthenticateServiceEndpointName, string BookingServiceEndpointName)
        {
            throw new NotImplementedException();
        }
        public UtilityCustomerInfoRS GetCustomerInfo(TokenRS token, UtilityCustomerInfoRQ input)
        {
            throw new NotImplementedException();
        }

        public UtilityGetLastTripRS GetLastTrip(TokenRS token, UtilityGetLastTripRQ input)
        {
            throw new NotImplementedException();
        }

        public UtilityGetPickupAddressRS GetPickupAddress(TokenRS token, UtilityGetPickupAddressRQ input)
        {
            throw new NotImplementedException();
        }


        public UtilityGetBlackoutPeriodRS GetBlackoutPeriod(TokenRS token, UtilityGetBlackoutPeriodRQ input)
        {
            throw new NotImplementedException();
        }





        public UtilityGetNotificationRS GetStaleNotification(TokenRS token, UtilityGetNotificationRQ input)
        {
            throw new NotImplementedException();
        }

        public UtilityGetNotificationRS GetArrivalNotification(TokenRS token, UtilityGetNotificationRQ input)
        {
            throw new NotImplementedException();
        }

        public UtilityGetNotificationRS GetNightBeforeReminder(TokenRS token, UtilityGetNotificationRQ input)
        {
            throw new NotImplementedException();
        }
    }



}
