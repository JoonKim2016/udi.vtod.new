﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;
using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO;
using UDI.VTOD.Common.DTO.OTA;
using UDI.Utility.Helper;
using UDI.VTOD.Common.Track;
using UDI.VTOD.Common.Helper;
using UDI.VTOD.DataAccess.VTOD;
using UDI.VTOD.Common.DTO.Enum;

namespace UDI.VTOD.Domain.Utility
{
	public class UtilityDomain : BaseSetting
	{
		internal UtilityGetServicedAirportsRS GetServicedAirports(TokenRS token, UtilityGetServicedAirportsRQ input, long? transactionID)
		{
			try
			{
				UDI.VTOD.Common.DTO.OTA.Airport rec = null;
				UtilityGetServicedAirportsRS result = null;
				var utilityController = new UDI.SDS.UtilityController(token, TrackTime);

				#region Input Conversion


				#endregion

				if (
					input.Address != null && !string.IsNullOrWhiteSpace(input.Address.Latitude) && !string.IsNullOrWhiteSpace(input.Address.Longitude) &&
					input.Address.TPA_Extensions != null && input.Address.TPA_Extensions.Zone != null && input.Address.TPA_Extensions.Zone.Circle != null
					)
				{
					var lat = input.Address.Latitude.ToDouble();
					var lon = input.Address.Longitude.ToDouble();
					var radius = Decimal.ToInt32(input.Address.TPA_Extensions.Zone.Circle.Radius);
					var sdsResult = utilityController.GetWebServicedAirports(lat, lon, radius);

					#region Output Conversion
					result = new UtilityGetServicedAirportsRS();

					if ((sdsResult != null) && (sdsResult.AirportRecords != null) && (sdsResult.AirportRecords.Count > 0))
					{
						result.Airports = new List<Airport>();
						result.Success = new Success();

						foreach (UDI.SDS.UtilityService.AirportRecord aRecord in sdsResult.AirportRecords)
						{
							rec = new Airport();

							rec.AirportCode = aRecord.AirportCode;
							rec.AirportName = aRecord.AirportName;
							rec.AllowASAPRequests = aRecord.AllowASAPRequests;
							rec.AllowChildSeatInput = aRecord.AllowChildSeatInput;
							rec.AllowInfantSeatInput = aRecord.AllowInfantSeatInput;
							rec.AllowWMVRequests = aRecord.AllowWMVRequests;
							rec.City = aRecord.City;
							rec.Country = aRecord.Country;
							rec.CountryCode = aRecord.CountryCode;
							rec.ECARServiced = aRecord.ECARServiced;
							rec.IsAffiliate = aRecord.IsAffiliate;
							rec.IsDefaultAirport = aRecord.IsDefaultAirport;
							rec.PortType = aRecord.PortType.ToString();
							rec.Serviced = aRecord.Serviced;
							rec.State = aRecord.State;
							rec.StateName = aRecord.StateName;
							rec.ZipCode = aRecord.ZipCode;

							result.Airports.Add(rec);
						}
					}

					#region Common
					result.EchoToken = input.EchoToken;
					result.PrimaryLangID = input.PrimaryLangID;
					result.Target = input.Target;
					result.Version = input.Version;
					#endregion
					#endregion
				}
				else
				{
					var sdsResult = utilityController.GetWebServicedAirports();

					#region Output Conversion
					result = new UtilityGetServicedAirportsRS();

					if ((sdsResult != null) && (sdsResult.AirportRecordsArray != null) && (sdsResult.AirportRecordsArray.Count > 0))
					{
						result.Airports = new List<Airport>();
						result.Success = new Success();

						foreach (UDI.SDS.UtilityService.AirportRecord aRecord in sdsResult.AirportRecordsArray)
						{
							rec = new Airport();

							rec.AirportCode = aRecord.AirportCode;
							rec.AirportName = aRecord.AirportName;
							rec.AllowASAPRequests = aRecord.AllowASAPRequests;
							rec.AllowChildSeatInput = aRecord.AllowChildSeatInput;
							rec.AllowInfantSeatInput = aRecord.AllowInfantSeatInput;
							rec.AllowWMVRequests = aRecord.AllowWMVRequests;
							rec.City = aRecord.City;
							rec.Country = aRecord.Country;
							rec.CountryCode = aRecord.CountryCode;
							rec.ECARServiced = aRecord.ECARServiced;
							rec.IsAffiliate = aRecord.IsAffiliate;
							rec.IsDefaultAirport = aRecord.IsDefaultAirport;
							rec.PortType = aRecord.PortType.ToString();
							rec.Serviced = aRecord.Serviced;
							rec.State = aRecord.State;
							rec.StateName = aRecord.StateName;
							rec.ZipCode = aRecord.ZipCode;

							result.Airports.Add(rec);
						}
					}

					#region Common
					result.EchoToken = input.EchoToken;
					result.PrimaryLangID = input.PrimaryLangID;
					result.Target = input.Target;
					result.Version = input.Version;
					#endregion
					#endregion

				}

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("UtilityDomain.GetServicedAirports:: {0}", ex.ToString() + ex.StackTrace);

				#region Throw UtilityException
				//throw new UtilityException(Messages.Utility_UtilityGetWebServicedAirports);
				throw new UtilityException(ex.Message);
				#endregion
			}
		}

		internal UtilityGetDefaultFleetTypeForPointRS GetDefaultFleetTypeForPoint(TokenRS token, UtilityGetDefaultFleetTypeForPointRQ input, long? transactionID)
		{
			try
			{
				UtilityGetDefaultFleetTypeForPointRS result = null;
				var vehilceController = new UDI.SDS.VehiclesController(token, TrackTime);

				#region Input Conversion

				#endregion

				var sdsResult = vehilceController.GetDefaultVehicleTypeForPoint(input.Latitude.ToDouble(), input.Longitude.ToDouble());

				#region Output Conversion
				result = new UtilityGetDefaultFleetTypeForPointRS();
				result.Success = new Success();
				result.FleetType = sdsResult;

				#region Common
				result.EchoToken = input.EchoToken;
				result.PrimaryLangID = input.PrimaryLangID;
				result.Target = input.Target;
				result.Version = input.Version;
				#endregion
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("vehilceController.GetDefaultVehicleTypeForPoint:: {0}", ex.ToString() + ex.StackTrace);

				#region Throw UtilityException
				//throw new UtilityException(Messages.Utility_VehicleGetDefaultVehicleTypeForPoint);
				throw new UtilityException(ex.Message);
				#endregion
			}
		}

		internal UtilityGetLocationListRS GetLocationList(TokenRS token, UtilityGetLocationListRQ input, long? transactionID)
		{
			try
			{
				UDI.VTOD.Common.DTO.OTA.Location loc = null;
				UtilityGetLocationListRS result = null;
				var utilityController = new UDI.SDS.UtilityController(token, TrackTime);

				#region Input Conversion


				#endregion

				var sdsResult = utilityController.GetCharterLocationsByType(0); // 0 is for Loc to Loc

				#region Output Conversion
				result = new UtilityGetLocationListRS();
				result.Success = new Success();
				if ((sdsResult != null) && (sdsResult.Count > 0))
				{
					result.Locations = new List<Location>();

					foreach (UDI.SDS.UtilityService.CharterLocation rsCharter in sdsResult)
					{
						loc = new Location();

						loc.ID = rsCharter.ID;
						loc.Address = new Address { LocationName = rsCharter.LocationName };
						//loc.LocationName = rsCharter.LocationName;

						result.Locations.Add(loc);
					}
				}

				#region Common
				result.EchoToken = input.EchoToken;
				result.PrimaryLangID = input.PrimaryLangID;
				result.Target = input.Target;
				result.Version = input.Version;
				#endregion
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("UtilityDomain.GetCharterLocationsByType:: {0}", ex.ToString() + ex.StackTrace);

				#region Throw UtilityException
				//throw new UtilityException(Messages.Utility_UtilityGetCharterLocationsByType);
				throw new UtilityException(ex.Message);
				#endregion
			}

		}

		internal UtilitySearchLocationRS SearchLocation(TokenRS token, UtilitySearchLocationRQ input, long? transactionID)
		{
			try
			{
				Location loc = null;
				UtilitySearchLocationRS result = null;
				var utilityController = new UDI.SDS.UtilityController(token, TrackTime);

				#region Input Conversion


				#endregion

				var sdsResult = utilityController.CharterSearchLandmark(input.LocationId, input.SearchExpression);

				#region Output Conversion
				result = new UtilitySearchLocationRS();
				result.Success = new Success();
				if ((sdsResult != null) && (sdsResult.Count > 0))
				{
					result.Locations = new List<Location>();

					foreach (var lRecord in sdsResult)
					{
						loc = new Location();

						//loc.HasScheduledService = lRecord.HasScheduledService;
						//loc.LandmarkID = lRecord.LandmarkID;
						//loc.MasterLandmarkID = lRecord.MasterLandmarkID;

						loc.Address = new Address();

						loc.Address.CountryName = new CountryName();

						if (!string.IsNullOrWhiteSpace(lRecord.LandmarkAddress.CountryCode))
							loc.Address.CountryName.Code = lRecord.LandmarkAddress.CountryCode;
						else
							loc.Address.CountryName.Code = UDI.SDS.DTO.Enum.Defaults.Country;

						loc.Address.StateProv = new StateProv();
						loc.Address.StateProv.StateCode = lRecord.LandmarkAddress.CountrySubDivision;
						loc.Address.Latitude = lRecord.LandmarkAddress.Latitude.ToString();
						loc.Address.Longitude = lRecord.LandmarkAddress.Longitude.ToString();
						loc.Address.LocationName = lRecord.LandmarkAddress.LocationName;
						//loc.Address.LocationType.Value = lRecord.LandmarkAddress.LocationType;
						loc.Address.CityName = lRecord.LandmarkAddress.Municipality;
						loc.Telephone = new Telephone();
						loc.Telephone.PhoneNumber = lRecord.LandmarkAddress.PhoneNumber;
						loc.Telephone.CountryAccessCode = lRecord.LandmarkAddress.PhoneNumberDialingPrefix;
						loc.Address.PostalCode = lRecord.LandmarkAddress.PostalCode;
						//loc.MemberAddressRecord.AddressLine = lRecord.LandmarkAddress.StreetAddress;
						loc.Address.AddressLine = lRecord.LandmarkAddress.StreetName;
						loc.Address.StreetNmbr = lRecord.LandmarkAddress.StreetNumber;
						//loc.Address.BldgRoom = lRecord.LandmarkAddress.UnitNumber;

						result.Locations.Add(loc);
					}
				}

				#region Common
				result.EchoToken = input.EchoToken;
				result.PrimaryLangID = input.PrimaryLangID;
				result.Target = input.Target;
				result.Version = input.Version;
				#endregion
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("UtilityDomain.CharterSearchLandmark:: {0}", ex.ToString() + ex.StackTrace);

				#region Throw UtilityException
				//throw new UtilityException(Messages.Utility_LandmarkCharterSearchLandmark);
				throw new UtilityException(ex.Message);
				#endregion
			}
		}

		internal UtilityGetAirportNameRS GetAirportName(TokenRS token, UtilityGetAirportNameRQ input, long? transactionID)
		{
			try
			{
				UtilityGetAirportNameRS result = null;
				var utilityController = new UDI.SDS.UtilityController(token, TrackTime);

				#region Input Conversion
				#endregion


				#region Output Conversion
				result = new UtilityGetAirportNameRS();
				result.Success = new Success();

				string airport = null;

				using (var db = new VTODEntities())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Utility_Domain, "Call");
					#endregion

					var airports = db.SP_vtod_GetAirportName(Convert.ToDecimal(input.Address.Latitude), Convert.ToDecimal(input.Address.Longitude)).ToList();

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_DB, "SP_vtod_GetAirportName");
					#endregion

					if (airports != null && airports.Any())
					{
						airport = airports.First();

						if ((airport != null) && (airport.Length > 0))
						{
							result.AirportName = airport;
						}
					}
				}


				#region Common
				result.EchoToken = input.EchoToken;
				result.PrimaryLangID = input.PrimaryLangID;
				result.Target = input.Target;
				result.Version = input.Version;
				#endregion
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("UtilityDomain.GetAirportName:: {0}", ex.ToString() + ex.StackTrace);

				#region Throw UtilityException
				throw new UtilityException(Messages.Utility_UtilityGetAirportName);
				//throw new UtilityException(ex.Message);
				#endregion
			}

		}

		internal List<Common.DTO.OTA.Fleet> GetAvailableFleets(TokenRS token, UtilityAvailableFleetsRQ input, long? transactionID)
		{
			//Common.DTO.OTA.Fleet { Type = fleet.ToString() }
			List<Common.DTO.OTA.Fleet> result = null;

			var postalCode = input.Address.PostalCode;

			var utilityController = new UDI.SDS.UtilityController(token, TrackTime);

			var sdsResult = utilityController.GetAvailableFleets(postalCode);

			if (sdsResult != null && sdsResult.fleets != null && sdsResult.fleets.Any())
			{
				result = new List<Common.DTO.OTA.Fleet>();
				foreach (var fleet in sdsResult.fleets)
				{
					if (fleet.ToLower() == "ecar")
					{
						var f = new Common.DTO.OTA.Fleet { Type = Common.DTO.Enum.FleetType.ExecuCar.ToString() };
						#region IsPickMeUpNow
						var rq = new UDI.SDS.UtilityService.PickMeUpNowServiceRequest();
						rq.City = input.Address.CityName;
						rq.Latitude = input.Address.Latitude.ToDouble();
						rq.Longitude = input.Address.Longitude.ToDouble();
						rq.Zip = input.Address.PostalCode;
						rq.State = input.Address.StateProv.StateCode;
						var now = utilityController.IsPickMeUpNowServiced(rq);

						if (now)
							f.Now = "true";
						else
							f.Now = "false";

						f.Later = "true";
						#endregion
						result.Add(f);
					}
					if (fleet.ToLower() == "bluev")
					{
						var f = new Common.DTO.OTA.Fleet { Type = Common.DTO.Enum.FleetType.SuperShuttle.ToString(), Later = "true", Now = "false" };
						result.Add(f);
					}
				}
			}

			return result;
		}

		internal UtilityGetAirlinesByAirportRS GetAirlinesByAirport(TokenRS token, UtilityGetAirlinesByAirportRQ input, long? transactionID)
		{
			UtilityGetAirlinesByAirportRS result = null;

			var utilityController = new UDI.SDS.UtilityController(token, TrackTime);

			var sdsResult = utilityController.GetAirlinesByAirport(input.AirportCode);

			#region Output Conversion
			result = new UtilityGetAirlinesByAirportRS();
			result.Success = new Success();

			result.Airlines = new List<Airline>();

			if (sdsResult.AirlineRecordsArray != null & sdsResult.AirlineRecordsArray.Any())
			{
				foreach (var item in sdsResult.AirlineRecordsArray)
				{
					var airline = new Airline();
					airline.Code = item.AirlineCode;
					airline.CompanyShortName = item.AirlineName;
					result.Airlines.Add(airline);
				}
			}



			#region Common
			result.EchoToken = input.EchoToken;
			result.PrimaryLangID = input.PrimaryLangID;
			result.Target = input.Target;
			result.Version = input.Version;
			#endregion
			#endregion

			return result;
		}

		#region Properties
		public TrackTime TrackTime { get; set; }
		#endregion
	}
}
