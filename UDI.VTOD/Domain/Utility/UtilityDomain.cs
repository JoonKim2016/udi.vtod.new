﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
//using System.Web.Security;
using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO;
using UDI.VTOD.Common.DTO.OTA;
using UDI.Utility.Helper;
using UDI.VTOD.Common.Track;
using UDI.VTOD.Common.Helper;
using UDI.VTOD.DataAccess.VTOD;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Domain.ECar.Aleph;
using UDI.VTOD.Helper;

namespace UDI.VTOD.Domain.Utility
{
	public class UtilityDomain : BaseSetting
	{
		internal UtilityGetServicedAirportsRS GetServicedAirports(TokenRS token, UtilityGetServicedAirportsRQ input)
		{
			try
			{
				UDI.VTOD.Common.DTO.OTA.Airport rec = null;
				UtilityGetServicedAirportsRS result = null;
				var utilityController = new UDI.SDS.UtilityController(token, TrackTime);

				#region Input Conversion


				#endregion

				if (
					input.Address != null && !string.IsNullOrWhiteSpace(input.Address.Latitude) && !string.IsNullOrWhiteSpace(input.Address.Longitude) &&
					input.Address.TPA_Extensions != null && input.Address.TPA_Extensions.Zone != null && input.Address.TPA_Extensions.Zone.Circle != null
					)
				{
					var lat = input.Address.Latitude.ToDouble();
					var lon = input.Address.Longitude.ToDouble();
					var radius = Decimal.ToInt32(input.Address.TPA_Extensions.Zone.Circle.Radius);
					var sdsResult = utilityController.GetWebServicedAirports(lat, lon, radius);

					#region Output Conversion
					result = new UtilityGetServicedAirportsRS();

					if ((sdsResult != null) && (sdsResult.AirportRecords != null) && (sdsResult.AirportRecords.Count > 0))
					{
						result.Airports = new List<Airport>();
						result.Success = new Success();

						foreach (UDI.SDS.UtilityService.AirportRecord aRecord in sdsResult.AirportRecords)
						{
							rec = new Airport();

							rec.AirportCode = aRecord.AirportCode;
							rec.AirportName = aRecord.AirportName;
							rec.AllowASAPRequests = aRecord.AllowASAPRequests;
							rec.AllowChildSeatInput = aRecord.AllowChildSeatInput;
							rec.AllowInfantSeatInput = aRecord.AllowInfantSeatInput;
							rec.AllowWMVRequests = aRecord.AllowWMVRequests;
							rec.City = aRecord.City;
							rec.Country = aRecord.Country;
							rec.CountryCode = aRecord.CountryCode;
							rec.ECARServiced = aRecord.ECARServiced;
							rec.IsAffiliate = aRecord.IsAffiliate;
							rec.IsDefaultAirport = aRecord.IsDefaultAirport;
							rec.PortType = aRecord.PortType.ToString();
							rec.Serviced = aRecord.Serviced;
							rec.State = aRecord.State;
							rec.StateName = aRecord.StateName;
							rec.ZipCode = aRecord.ZipCode;
							rec.Latitude = aRecord.Latitude.ToString();
							rec.Longitude = aRecord.Longitude.ToString();
                            rec.IsRideNowAllowed = aRecord.IsRideNowAllowed;
                            rec.CurrencyId = aRecord.CurrencyId;
                            rec.CultureCode = aRecord.CultureCode;
                            rec.PreventGratuity = aRecord.PreventGratuity;
                            rec.PreventAccessibleBookings = aRecord.PreventAccessibleBookings;
                            rec.IsoCurrencyCode = aRecord.IsoCurrencyCode;
                            result.Airports.Add(rec);
						}
					}

					#region Common
					result.EchoToken = input.EchoToken;
					result.PrimaryLangID = input.PrimaryLangID;
					result.Target = input.Target;
					result.Version = input.Version;
					#endregion
					#endregion
				}
				else
				{
					var sdsResult = utilityController.GetWebServicedAirports();

					#region Output Conversion
					result = new UtilityGetServicedAirportsRS();

					if ((sdsResult != null) && (sdsResult.AirportRecordsArray != null) && (sdsResult.AirportRecordsArray.Count > 0))
					{
						result.Airports = new List<Airport>();
						result.Success = new Success();

						foreach (UDI.SDS.UtilityService.AirportRecord aRecord in sdsResult.AirportRecordsArray)
						{
							rec = new Airport();

							rec.AirportCode = aRecord.AirportCode;
							rec.AirportName = aRecord.AirportName;
							rec.AllowASAPRequests = aRecord.AllowASAPRequests;
							rec.AllowChildSeatInput = aRecord.AllowChildSeatInput;
							rec.AllowInfantSeatInput = aRecord.AllowInfantSeatInput;
							rec.AllowWMVRequests = aRecord.AllowWMVRequests;
							rec.City = aRecord.City;
							rec.Country = aRecord.Country;
							rec.CountryCode = aRecord.CountryCode;
							rec.ECARServiced = aRecord.ECARServiced;
							rec.IsAffiliate = aRecord.IsAffiliate;
							rec.IsDefaultAirport = aRecord.IsDefaultAirport;
							rec.PortType = aRecord.PortType.ToString();
							rec.Serviced = aRecord.Serviced;
							rec.State = aRecord.State;
							rec.StateName = aRecord.StateName;
							rec.ZipCode = aRecord.ZipCode;
							rec.Latitude = aRecord.Latitude.ToString();
							rec.Longitude = aRecord.Longitude.ToString();
                            rec.IsRideNowAllowed = aRecord.IsRideNowAllowed;
                            rec.PreventGratuity = aRecord.PreventGratuity;
                            rec.PreventAccessibleBookings = aRecord.PreventAccessibleBookings;
                            rec.CurrencyId = aRecord.CurrencyId;
                            rec.CultureCode = aRecord.CultureCode;
                            rec.IsoCurrencyCode= aRecord.IsoCurrencyCode;
                            result.Airports.Add(rec);
						}
					}

					#region Common
					result.EchoToken = input.EchoToken;
					result.PrimaryLangID = input.PrimaryLangID;
					result.Target = input.Target;
					result.Version = input.Version;
					#endregion
					#endregion

				}

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("UtilityDomain.GetServicedAirports:: {0}", ex.ToString() + ex.StackTrace);

				#region Throw UtilityException
				//throw new UtilityException(Messages.Utility_UtilityGetWebServicedAirports);
				throw new UtilityException(ex.Message);
				#endregion
			}
		}

		internal UtilityGetDestinationsRS GetDestinations(TokenRS token, UtilityGetDestinationsRQ input)
		{
			try
			{
				UDI.VTOD.Common.DTO.OTA.Airport rec = null;
				UtilityGetDestinationsRS result = new UtilityGetDestinationsRS();
				var utilityController = new UDI.SDS.UtilityController(token, TrackTime);
                
				var sdsResult = utilityController.GetDestinations();

				#region Output Conversion

				if ((sdsResult != null) && (sdsResult.AirportRecordsArray != null) && (sdsResult.AirportRecordsArray.Count > 0))
				{
					result.Airports = new List<Airport>();
					result.Success = new Success();

					foreach (UDI.SDS.UtilityService.AirportRecord aRecord in sdsResult.AirportRecordsArray)
					{
						rec = new Airport();

						rec.AirportCode = aRecord.AirportCode;
						rec.AirportName = aRecord.AirportName;
						rec.AllowASAPRequests = aRecord.AllowASAPRequests;
						rec.AllowChildSeatInput = aRecord.AllowChildSeatInput;
						rec.AllowInfantSeatInput = aRecord.AllowInfantSeatInput;
						rec.AllowWMVRequests = aRecord.AllowWMVRequests;
						rec.City = aRecord.City;
						rec.Country = aRecord.Country;
						rec.CountryCode = aRecord.CountryCode;
						rec.ECARServiced = aRecord.ECARServiced;
						rec.IsAffiliate = aRecord.IsAffiliate;
						rec.IsDefaultAirport = aRecord.IsDefaultAirport;
						rec.PortType = aRecord.PortType.ToString();
						rec.Serviced = aRecord.Serviced;
						rec.State = aRecord.State;
						rec.StateName = aRecord.StateName;
						rec.ZipCode = aRecord.ZipCode;
						rec.Latitude = aRecord.Latitude.ToString();
						rec.Longitude = aRecord.Longitude.ToString();

						result.Airports.Add(rec);
					}

					#region Common
					result.EchoToken = input.EchoToken;
					result.PrimaryLangID = input.PrimaryLangID;
					result.Target = input.Target;
					result.Version = input.Version;
					#endregion
				#endregion

				}

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("UtilityDomain.GetServicedAirports:: {0}", ex.ToString() + ex.StackTrace);

				#region Throw UtilityException
				//throw new UtilityException(Messages.Utility_UtilityGetWebServicedAirports);
				throw new UtilityException(ex.Message);
				#endregion
			}
		}

		internal UtilityGetDefaultFleetTypeForPointRS GetDefaultFleetTypeForPoint(TokenRS token, UtilityGetDefaultFleetTypeForPointRQ input)
		{
			try
			{
				UtilityGetDefaultFleetTypeForPointRS result = null;
				var vehilceController = new UDI.SDS.VehiclesController(token, TrackTime);

				#region Input Conversion

				#endregion

				var sdsResult = vehilceController.GetDefaultVehicleTypeForPoint(input.Latitude.ToDouble(), input.Longitude.ToDouble());

				#region Output Conversion
				result = new UtilityGetDefaultFleetTypeForPointRS();
				result.Success = new Success();
				result.FleetType = sdsResult;

				#region Common
				result.EchoToken = input.EchoToken;
				result.PrimaryLangID = input.PrimaryLangID;
				result.Target = input.Target;
				result.Version = input.Version;
				#endregion
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("vehilceController.GetDefaultVehicleTypeForPoint:: {0}", ex.ToString() + ex.StackTrace);

				#region Throw UtilityException
				//throw new UtilityException(Messages.Utility_VehicleGetDefaultVehicleTypeForPoint);
				throw new UtilityException(ex.Message);
				#endregion
			}
		}

		internal UtilityGetLocationListRS GetLocationList(TokenRS token, UtilityGetLocationListRQ input)
		{
			try
			{
				UDI.VTOD.Common.DTO.OTA.Location loc = null;
				UtilityGetLocationListRS result = null;
				var utilityController = new UDI.SDS.UtilityController(token, TrackTime);

				#region Input Conversion


				#endregion

				var sdsResult = utilityController.GetCharterLocationsByType(0); // 0 is for Loc to Loc

				#region Output Conversion
				result = new UtilityGetLocationListRS();
				result.Success = new Success();
				if ((sdsResult != null) && (sdsResult.Count > 0))
				{
					result.Locations = new List<Location>();

					foreach (UDI.SDS.UtilityService.CharterLocation rsCharter in sdsResult)
					{
						loc = new Location();

						loc.ID = rsCharter.ID;
						loc.Address = new Address { LocationName = rsCharter.LocationName };
						//loc.LocationName = rsCharter.LocationName;

						result.Locations.Add(loc);
					}
				}

				#region Common
				result.EchoToken = input.EchoToken;
				result.PrimaryLangID = input.PrimaryLangID;
				result.Target = input.Target;
				result.Version = input.Version;
				#endregion
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("UtilityDomain.GetCharterLocationsByType:: {0}", ex.ToString() + ex.StackTrace);

				#region Throw UtilityException
				//throw new UtilityException(Messages.Utility_UtilityGetCharterLocationsByType);
				throw new UtilityException(ex.Message);
				#endregion
			}

		}

		internal UtilitySearchLocationRS SearchLocation(TokenRS token, UtilitySearchLocationRQ input)
		{
			try
			{
				Location loc = null;
				UtilitySearchLocationRS result = null;
				var utilityController = new UDI.SDS.UtilityController(token, TrackTime);
                
				var sdsResult = utilityController.CharterSearchLandmark(input.LocationId, input.SearchExpression);

				#region Output Conversion
				result = new UtilitySearchLocationRS();
				result.Success = new Success();
				if ((sdsResult != null) && (sdsResult.Count > 0))
				{
					result.Locations = new List<Location>();

					foreach (var lRecord in sdsResult)
					{
						loc = new Location();                        
						loc.Address = new Address();
						loc.Address.CountryName = new CountryName();

						if (!string.IsNullOrWhiteSpace(lRecord.LandmarkAddress.CountryCode))
							loc.Address.CountryName.Code = lRecord.LandmarkAddress.CountryCode;
						else
							loc.Address.CountryName.Code = UDI.SDS.DTO.Enum.Defaults.Country;

						loc.Address.StateProv = new StateProv();
						loc.Address.StateProv.StateCode = lRecord.LandmarkAddress.CountrySubDivision;
						loc.Address.Latitude = lRecord.LandmarkAddress.Latitude.ToString();
						loc.Address.Longitude = lRecord.LandmarkAddress.Longitude.ToString();
						loc.Address.LocationName = lRecord.LandmarkAddress.LocationName;
						//loc.Address.LocationType.Value = lRecord.LandmarkAddress.LocationType;
						loc.Address.CityName = lRecord.LandmarkAddress.Municipality;
						loc.Telephone = new Telephone();
						loc.Telephone.PhoneNumber = lRecord.LandmarkAddress.PhoneNumber;
						loc.Telephone.CountryAccessCode = lRecord.LandmarkAddress.PhoneNumberDialingPrefix;
						loc.Address.PostalCode = lRecord.LandmarkAddress.PostalCode;
						//loc.MemberAddressRecord.AddressLine = lRecord.LandmarkAddress.StreetAddress;
						loc.Address.AddressLine = lRecord.LandmarkAddress.StreetName;
						loc.Address.StreetNmbr = lRecord.LandmarkAddress.StreetNumber;
						//loc.Address.BldgRoom = lRecord.LandmarkAddress.UnitNumber;

						result.Locations.Add(loc);
					}
				}

				#region Common
				result.EchoToken = input.EchoToken;
				result.PrimaryLangID = input.PrimaryLangID;
				result.Target = input.Target;
				result.Version = input.Version;
				#endregion
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("UtilityDomain.CharterSearchLandmark:: {0}", ex.ToString() + ex.StackTrace);

				#region Throw UtilityException				
				throw new UtilityException(ex.Message);
				#endregion
			}
		}

		internal UtilityGetAirportNameRS GetAirportName(TokenRS token, UtilityGetAirportNameRQ input)
		{
			try
			{
				UtilityGetAirportNameRS result = null;
				//var utilityController = new UDI.SDS.UtilityController(token, TrackTime);

				#region Input Conversion
				#endregion


				#region Output Conversion
				result = new UtilityGetAirportNameRS();
				result.Success = new Success();

				string airport = null;

				using (var db = new VTODEntities())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Utility_Domain, "Call");
					#endregion

					var airports = db.SP_vtod_GetAirportName(Convert.ToDecimal(input.Address.Latitude), Convert.ToDecimal(input.Address.Longitude)).ToList();

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_DB, "SP_vtod_GetAirportName");
					#endregion

					if (airports != null && airports.Any())
					{
						airport = airports.First();

						if ((airport != null) && (airport.Length > 0))
						{
							result.AirportName = airport;
						}
					}
				}


				#region Common
				result.EchoToken = input.EchoToken;
				result.PrimaryLangID = input.PrimaryLangID;
				result.Target = input.Target;
				result.Version = input.Version;
				#endregion
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("UtilityDomain.GetAirportName:: {0}", ex.ToString() + ex.StackTrace);
                
				throw VtodException.CreateException(ExceptionType.Membership, 9004);				
				
			}

		}

		internal List<Common.DTO.OTA.Fleet> GetAvailableFleets(TokenRS token, UtilityAvailableFleetsRQ input, FleetType fleetType)
		{
			//Common.DTO.OTA.Fleet { Type = fleet.ToString() }
			var defaultRadiusForRegular = System.Configuration.ConfigurationManager.AppSettings["DefaultRadiusForRegular"].ToInt32();
			var restrictNowBookingMins = System.Configuration.ConfigurationManager.AppSettings["RestrictNowBookingMins"].ToInt32();

			List<Common.DTO.OTA.Fleet> result = null;

			var postalCode = input.Address.PostalCode;
            var memberID = input.MemberID == 0 ? 0 : input.MemberID;
			var utilityController = new UDI.SDS.UtilityController(token, TrackTime);

			var sdsResult = utilityController.GetAvailableFleets(postalCode);

			if (sdsResult != null && sdsResult.fleets != null && sdsResult.fleets.Any())
			{
				result = new List<Common.DTO.OTA.Fleet>();
				List<string> newFleetList = null;
				if (fleetType == FleetType.Taxi)
					newFleetList = sdsResult.fleets.Where(s => s.ToLower().Trim() == "taxi").ToList();
				else
					newFleetList = sdsResult.fleets.Where(s => s.ToLower().Trim() != "taxi").ToList();

				foreach (var fleet in newFleetList)
				{
					if (fleet.ToLower() == "ecar" || fleet.ToLower() == "taxi")
					{
						Fleet f = null;
						var rq = new UDI.SDS.UtilityService.PickMeUpNowServiceRequest();

						if (fleet.ToLower() == "taxi")
						{
							f = new Common.DTO.OTA.Fleet { Type = Common.DTO.Enum.FleetType.Taxi.ToString(), DefaultRadius = defaultRadiusForRegular, RestrictNowBookingMins = restrictNowBookingMins };
							rq.VehicleFleet = (int)UDI.VTOD.Common.DTO.Enum.VehicleType.Taxi;
						}
						else
						{
							f = new Common.DTO.OTA.Fleet { Type = Common.DTO.Enum.FleetType.ExecuCar.ToString(), DefaultRadius = defaultRadiusForRegular, RestrictNowBookingMins = restrictNowBookingMins };
							rq.VehicleFleet = (int)UDI.VTOD.Common.DTO.Enum.VehicleType.ExecuCar;
						}
						#region IsPickMeUpNow
						var now = false;
						try
						{
							rq.City = input.Address.CityName;
							rq.Latitude = input.Address.Latitude.ToDouble();
							rq.Longitude = input.Address.Longitude.ToDouble();
							rq.Zip = input.Address.PostalCode;
							rq.State = input.Address.StateProv.StateCode;
                            rq.MemberId = memberID;
                            now = utilityController.IsPickMeUpNowServiced(rq);
						}
						catch { }

						if (now)
							f.Now = "true";
						else
							f.Now = "false";

						f.Later = "true";
                        #endregion
                        f.DisabilityVehicleInd = false;
                        f.MaxPassengers = System.Configuration.ConfigurationManager.AppSettings["MaxPassengersForBlackCar"].ToInt32();
                        result.Add(f);
					}
					if (fleet.ToLower() == "bluev")
					{
						var f = new Common.DTO.OTA.Fleet { Type = Common.DTO.Enum.FleetType.SuperShuttle.ToString(), Later = "true", Now = "false", DefaultRadius = defaultRadiusForRegular, RestrictNowBookingMins = restrictNowBookingMins };
                        f.DisabilityVehicleInd = false;
                        result.Add(f);
					}
				}
			}

			return result;
		}

		internal UtilityGetAirlinesByAirportRS GetAirlinesByAirport(TokenRS token, UtilityGetAirlinesByAirportRQ input)
		{
			UtilityGetAirlinesByAirportRS result = null;

			var utilityController = new UDI.SDS.UtilityController(token, TrackTime);

			var sdsResult = utilityController.GetAirlinesByAirport(input.AirportCode);

			#region Output Conversion
			result = new UtilityGetAirlinesByAirportRS();
			result.Success = new Success();

			result.Airlines = new List<Airline>();

			if (sdsResult.AirlineRecordsArray != null & sdsResult.AirlineRecordsArray.Any())
			{
				foreach (var item in sdsResult.AirlineRecordsArray)
				{
					var airline = new Airline();
					airline.Code = item.AirlineCode;
					airline.CompanyShortName = item.AirlineName;
                    airline.IsCommon = item.IsCommon;
					result.Airlines.Add(airline);
				}
			}



			#region Common
			result.EchoToken = input.EchoToken;
			result.PrimaryLangID = input.PrimaryLangID;
			result.Target = input.Target;
			result.Version = input.Version;
			#endregion
			#endregion

			return result;
		}

		internal UtilityGetTripListRS GetTripList(TokenRS token, UtilityGetTripListRQ input)
		{
			UtilityGetTripListRS result = new UtilityGetTripListRS();
			result.Success = new Success();

			List<CompletedTripInfo> dbResult = null;

			if (input != null && input.RateQualifier != null && input.RateQualifier.RateQualifierValue.ToLower().Trim() == Common.DTO.Enum.FleetType.Taxi.ToString().ToLower())
			{
				if (input.Reference != null && !string.IsNullOrWhiteSpace(input.Reference.Type) && input.Reference.Type.ToLower().Trim() == Common.DTO.Enum.ConfirmationType.Confirmation.ToString().ToLower())
				{
					if (!string.IsNullOrWhiteSpace(input.Reference.ID) && !string.IsNullOrWhiteSpace(input.Reference.ID_Context))
					{
						using (var db = new VTODEntities())
						{
							if (input.Status != null)
							{
								switch (input.Status.Value)
								{
									case Taxi.Const.TaxiTripStatusType.Completed:
									case Taxi.Const.TaxiTripStatusType.MeterOff:
										dbResult = db.SP_Taxi_GetCompletedTripList(input.Reference.ID.ToInt64(), input.Reference.ID_Context.ToInt64()).ToList();
										break;
								}
							}
						}
					}
				}
			}

			#region Output Conversion
			if (dbResult != null && dbResult.Any())
			{
				result.Reservations = new List<Reservation>();

				foreach (var trip in dbResult)
				{
					try
					{
						var reservation = new Reservation();
						reservation.Confirmation = new Confirmation { ID = trip.Id.ToString(), Type = Common.DTO.Enum.ConfirmationType.Confirmation.ToString() };
						reservation.Confirmation.TPA_Extensions = new TPA_Extensions();
						reservation.Confirmation.TPA_Extensions.Confirmations = new List<Confirmation>();
						var dispatchConfirmation = new Confirmation { ID = trip.DispatchTripId, Type = Common.DTO.Enum.ConfirmationType.DispatchConfirmation.ToString() };
						reservation.Confirmation.TPA_Extensions.Confirmations.Add(dispatchConfirmation);
						reservation.Service = new Service();
						reservation.Service.Location = new Service();
						reservation.Service.Location.Pickup = new Pickup_Dropoff_Stop { DateTime = trip.PickupDateTime.ToString() };

						result.Reservations.Add(reservation);
					}
					catch (Exception ex)
					{
						logger.Error(string.Format("GetTripList:TripID: {0}", trip.Id), ex);
					}
				}
			}


			#region Common
			result.EchoToken = input.EchoToken;
			result.PrimaryLangID = input.PrimaryLangID;
			result.Target = input.Target;
			result.Version = input.Version;
			#endregion
			#endregion

			return result;
		}

		internal UtilityGetAirportInstructionsRS GetAirportInstructions(TokenRS token, UtilityGetAirportInstructionsRQ input)
		{
			try
			{
				UtilityGetAirportInstructionsRS result = null;

				if (input.Reference.Type.Trim().ToLower() == Common.DTO.Enum.ConfirmationType.FareID.ToString().ToLower())
				{

				}

				if (input.Reference.Type.Trim().ToLower() == Common.DTO.Enum.ConfirmationType.Confirmation.ToString().ToLower())
				{

				}

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("UtilityDomain.GetAirportInstructions:: {0}", ex.ToString() + ex.StackTrace);

				#region Throw UtilityException
				//throw new UtilityException(Messages.Utility_UtilityGetWebServicedAirports);
				throw VtodException.CreateException(ExceptionType.Utility, 9001);// new UtilityException(ex.Message);
				#endregion
			}
		}

		internal UtilityGetHonorificsRS GetHonorifics(TokenRS token, UtilityGetHonorificsRQ input)
		{
			UtilityGetHonorificsRS result = new UtilityGetHonorificsRS();
			result.Success = new Success();

			var languageID = "en-US";
			if (!string.IsNullOrWhiteSpace(input.PrimaryLangID))
			{
				languageID = input.PrimaryLangID;
			}

			var utilityController = new UDI.SDS.UtilityController(token, TrackTime);

			var sdsResult = utilityController.GetHonorifics(languageID);


			#region Output Conversion
			if (sdsResult != null && sdsResult.Honorifics != null && sdsResult.Honorifics.Any())
			{
				result.Honorifics = new List<Honorific>();
				foreach (var item in sdsResult.Honorifics)
				{
					var honorific = new Honorific();
					honorific.LanguageCode = item.LanguageCode;
					honorific.Title = item.Title;
					honorific.Gender = item.Gender;
					honorific.ID = item.Id;
					result.Honorifics.Add(honorific);
				}
			}

			#region Common
			result.EchoToken = input.EchoToken;
			result.PrimaryLangID = input.PrimaryLangID;
			result.Target = input.Target;
			result.Version = input.Version;
			#endregion
			#endregion

			return result;
		}

		#region Corporate
		internal UtilityGetBookingCriteriaRS UtilityGetBookingCriteria(TokenRS token, UtilityGetBookingCriteriaRQ input)
		{
			UtilityGetBookingCriteriaRS result = new UtilityGetBookingCriteriaRS();
			var corporate = input.Corporate.CorporateAccount;
			var corporateName = input.Corporate.CorporateAccount.Corporation.ToCoporateName();
			var username = input.Corporate.CorporateAccount.UserID;
			CorporateProfile profile = null;
			if (input.Corporate.CorporateProfiles != null && input.Corporate.CorporateProfiles.CorporateProfileList != null && input.Corporate.CorporateProfiles.CorporateProfileList.Any())
			{
				profile = input.Corporate.CorporateProfiles.CorporateProfileList.First();
			}

			#region Response Init
			result.Corporate = input.Corporate;
			result.Corporate.Criteria = new Criteria();
			result.Corporate.Criteria.SpecialInputs = new List<NameValue>();
			#endregion



			try
			{

				switch (corporateName)
				{
					case CorporateName.Aleph:
						var memberhshipController = new UDI.SDS.MembershipController(token, TrackTime);
						var sdsCorporateAccount = memberhshipController.GetCorporateAccountKey(corporate.Corporation, username);

						if (sdsCorporateAccount != null)
						{
							var alephAPICall = new AlephAPICall();
							AlephGetBookingCriteriaRS alephResult = null;
							if (profile != null)
							{
								alephResult = alephAPICall.GetBookingCriteria(profile.ProviderID.ToString(), profile.CompanyName, profile.ProviderAccountId, profile.ProfileID, username, sdsCorporateAccount.UserKey);
							}
							else
							{
								alephResult = alephAPICall.GetBookingCriteriaNow(string.Empty, string.Empty, username, sdsCorporateAccount.UserKey);
							}
							if (alephResult != null && alephResult.booking_criteria != null && alephResult.booking_criteria.Any())
							{

								foreach (var item in alephResult.booking_criteria)
								{
									NameValue criterium = new NameValue { Name = item.booking_criterion_name, Value = item.booking_criterion_value };
									result.Corporate.Criteria.SpecialInputs.Add(criterium);
								}
							}
						}

						break;

				}
			}
			catch (Exception ex)
			{
				logger.Error(ex);
			}

			return result;
		}

		internal UtilityGetCorporateRequirementsRS UtilityGetCorporateRequirements(TokenRS token, UtilityGetCorporateRequirementsRQ input)
		{
			var result = new UtilityGetCorporateRequirementsRS();
			var corporateAccount = input.Corporate.CorporateAccount;
			var corporateName = corporateAccount.Corporation.ToCoporateName();
			var username = corporateAccount.UserID;
            string alephCorporateName = string.Empty;
            string alephAccountName = string.Empty;
            if (input.Corporate != null)
            {
                if (input.Corporate.CorporateAccount != null)
                {
                    if (!string.IsNullOrEmpty(input.Corporate.CorporateAccount.CorporateName))
                    {
                        alephCorporateName = input.Corporate.CorporateAccount.CorporateName;
                    }
                    if (!string.IsNullOrEmpty(input.Corporate.CorporateAccount.Account))
                    {
                        alephAccountName = input.Corporate.CorporateAccount.Account;
                    }
                }
            }
			try
			{
				switch (corporateName)
				{
					case CorporateName.Aleph:
						var memberhshipController = new UDI.SDS.MembershipController(token, TrackTime);
						var sdsCorporateAccount = memberhshipController.GetCorporateAccountKey(corporateAccount.Corporation, username);
						if (sdsCorporateAccount != null)
						{
							var alephAPICall = new AlephAPICall();
                            var alephResult = alephAPICall.GetCorporateRequirements(alephCorporateName,
                                alephAccountName, string.Empty, username, sdsCorporateAccount.UserKey);
							if (alephResult != null)
							{
								#region Output Conversion
								result.CorporateCompanyRequirements = new List<CorporateCompanyRequirement>();
								foreach (var corporationRequirement in alephResult.CorporationRequirements)
								{
									if (corporationRequirement != null)
									{
										var cr = new CorporateCompanyRequirement
										{
											CorporateCompanyName = corporationRequirement.corporation_name,
											CorporateCompanyAccounts = new List<CorporationCompanyAccountRequirements>()
										};
										result.CorporateCompanyRequirements.Add(cr);

										if (corporationRequirement.corporation_accounts != null && corporationRequirement.corporation_accounts.Count > 0)
										{
											cr.CorporateCompanyAccounts = new List<CorporationCompanyAccountRequirements>();
											foreach (
												var corporationAccount in corporationRequirement.corporation_accounts)
											{
												if (corporationAccount != null)
												{
													var ar = new CorporationCompanyAccountRequirements
													{
														AccountName = corporationAccount.account_name,
														Requirements = new List<RequirementDetail>()
													};
													cr.CorporateCompanyAccounts.Add(ar);

													if (corporationAccount.corporation_requirements != null &&
														corporationAccount.corporation_requirements.Count > 0)
													{
														ar.Requirements = new List<RequirementDetail>();
														foreach (var requirement in corporationAccount.corporation_requirements)
														{
															ar.Requirements.Add(new RequirementDetail
															{
																IsRequired = requirement.is_required,
																ProvideHints = requirement.provide_hints,
																RequirementName = requirement.requirement_name,
																ShouldUseTypeaheadUri = requirement.should_use_typeahead_uri,
																//TypeaheadBaseUri = requirement.typeahead_base_uri
															});
														}
													}
												}
											}
										}
									}
								}

								result.Success = new Success();

								#region Common
								result.EchoToken = input.EchoToken;
								result.PrimaryLangID = input.PrimaryLangID;
								result.Target = input.Target;
								result.Version = input.Version;
								#endregion
								#endregion
							}
						}
						break;
				}
			}
			catch (Exception ex)
			{
				logger.Error(ex);
			}

			return result;
		}

      
		internal List<string> UtilityGetCorporateRequirementsTypeAhead(TokenRS token, string username, string corporate, string corporateCompanyName, string accountName, string requirementName, string letters)
		{
			try
			{
				switch (corporate.ToCoporateName())
				{
					case CorporateName.Aleph:
						var memberhshipController = new UDI.SDS.MembershipController(token, TrackTime);
						var sdsCorporateAccount = memberhshipController.GetCorporateAccountKey(corporate, username);
						if (sdsCorporateAccount != null)
						{
							var alephAPICall = new AlephAPICall();
							var alephResult = alephAPICall.GetCorporateRequirementsTypeAhead(corporateCompanyName,
								accountName, requirementName, letters, username, sdsCorporateAccount.UserKey);
							return alephResult;
						}
						break;
				}
			}
			catch (Exception ex)
			{
				logger.Error(ex);

				return null;
			}

			return null;
		}

		internal UtilityGetCorporateRequirementsRS UtilityGetCorporatePaymentMethod(TokenRS token, UtilityGetCorporateRequirementsRQ input)
		{
			var result = new UtilityGetCorporateRequirementsRS();
			var corporateAccount = input.Corporate.CorporateAccount;
			var corporateName = corporateAccount.Corporation.ToCoporateName();
			var username = corporateAccount.UserID;
            string alephCorporateName = string.Empty;
            string alephAccountName = string.Empty;
            if (input.Corporate != null)
            {
                if (input.Corporate.CorporateAccount != null)
                {
                    if (!string.IsNullOrEmpty(input.Corporate.CorporateAccount.CorporateName))
                    {
                        alephCorporateName = input.Corporate.CorporateAccount.CorporateName;
                    }
                    if (!string.IsNullOrEmpty(input.Corporate.CorporateAccount.Account))
                    {
                        alephAccountName = input.Corporate.CorporateAccount.Account;
                    }
                }
            }
			try
			{
				switch (corporateName)
				{
					case CorporateName.Aleph:
						var memberhshipController = new UDI.SDS.MembershipController(token, TrackTime);
						var sdsCorporateAccount = memberhshipController.GetCorporateAccountKey(corporateAccount.Corporation, username);
						if (sdsCorporateAccount != null)
						{
							var alephAPICall = new AlephAPICall();
                            var alephResult = alephAPICall.GetCorporatePaymentMethods(alephCorporateName, alephAccountName, username, sdsCorporateAccount.UserKey);
							if (alephResult != null)
							{
                                result.CorporateCompanyRequirements = alephResult.ToCorporateCompanyRequirements();
                            }
						}
						break;
				}
			}
			catch (Exception ex)
			{
				logger.Error(ex);
			}

			return result;
		}
		internal UtilityGetAirportPickupOptionsRS UtilityGetAirportPickupOptions(TokenRS token, UtilityGetAirportPickupOptionsRQ input)
		{
		
			var dummyResult = new UtilityGetAirportPickupOptionsRS();
            var corporateAccount = input.Corporate.CorporateAccount;
            var corporateName = corporateAccount.Corporation.ToCoporateName();
            var username = corporateAccount.UserID;
            UtilityGetAirportPickupOptionsRS result=new UtilityGetAirportPickupOptionsRS();
            try
            {
                switch (corporateName)
                {
                    case CorporateName.Aleph:
                      var memberhshipController = new UDI.SDS.MembershipController(token, TrackTime);
						var sdsCorporateAccount = memberhshipController.GetCorporateAccountKey(corporateAccount.Corporation, username);
						if (sdsCorporateAccount != null)
						{
							var alephAPICall = new AlephAPICall();
                            var alephResult = alephAPICall.GetAirportPickUpOptions(username, sdsCorporateAccount.UserKey);
							if (alephResult != null)
							{
                                #region Output Conversion
                               result.AirportPickupOptions = new AirportPickupOptions();
                               result.AirportPickupOptions.Items = new List<NameValue>();

                                foreach (AirportPickUp item in alephResult)
                                {
                                    if (item != null)
                                    {
                                        NameValue pickupItem=new  NameValue();
                                        pickupItem.Name = item.airport_pickup_value;
                                        pickupItem.Value = item.airport_pickup_description;
                                        result.AirportPickupOptions.Items.Add(pickupItem);
                                    }
                                }

                                result.Success = new Success();

                                #region Common
                                result.EchoToken = input.EchoToken;
                                result.PrimaryLangID = input.PrimaryLangID;
                                result.Target = input.Target;
                                result.Version = input.Version;
                                #endregion
                                #endregion
							}
						}
						break;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);

                return null;
            }
            //var nameValue1 = new NameValue { Name = "Curbside", Value = "Curbside" };
            //dummyResult.AirportPickupOptions.Items.Add(nameValue1);

            //var nameValue2 = new NameValue { Name = "Meet and Greet (addt'l fee)", Value = "Meet and Greet" };
            //dummyResult.AirportPickupOptions.Items.Add(nameValue2);

            //var nameValue3 = new NameValue { Name = "Standby (call Fleet)", Value = "Standby" };
            //dummyResult.AirportPickupOptions.Items.Add(nameValue3);


            //dummyResult.Success = new Success();
            return result;
		}
		#endregion
        
		#region Properties
		public TrackTime TrackTime { get; set; }
		#endregion
         
	}
}
