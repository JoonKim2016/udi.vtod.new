﻿using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using log4net;
using UDI.Utility.Helper;
using UDI.Utility.Serialization;
using UDI.VTOD.Common.Helper;
using UDI.VTOD.DataAccess.VTOD;

namespace UDI.VTOD.Domain.VTOD
{
	public class DriverInfo
	{
		ILog logger = null;

		public DriverInfo(ILog log)
		{
			logger = log;
		}

		public vtod_driver_info UpsertDriverDetails(vtod_driver_info driverInfo, byte[] driverImage)
		{
            string fleetID = null;
			vtod_driver_info result = null;
            var cryptoDriver = string.Empty;
			#region CryptoDriver
            if (!string.IsNullOrEmpty(driverInfo.FleetID.ToString()))
            {
                if (driverInfo.FleetID.ToString()!="0")
                {
                    fleetID = driverInfo.FleetID.ToString();
                }
            }
            if (string.IsNullOrEmpty(fleetID))
            {
                cryptoDriver = string.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}{11}{12}{13}{14}",
                       string.IsNullOrEmpty(driverInfo.DriverID) ? "" : driverInfo.DriverID, //{0}
                       string.IsNullOrEmpty(driverInfo.VehicleID) ? "" : driverInfo.VehicleID, //{1}
                       string.IsNullOrEmpty(driverInfo.FirstName) ? "" : driverInfo.FirstName, //{2}
                       string.IsNullOrEmpty(driverInfo.LastName) ? "" : driverInfo.LastName, //{3}
                       string.IsNullOrEmpty(driverInfo.CellPhone) ? "" : driverInfo.CellPhone, //{4}
                       string.IsNullOrEmpty(driverInfo.HomePhone) ? "" : driverInfo.HomePhone, //{5}
                       string.IsNullOrEmpty(driverInfo.AlternatePhone) ? "" : driverInfo.AlternatePhone, //{6}
                       !driverInfo.QualificationDate.HasValue ? "" : driverInfo.QualificationDate.ToString(), //{7}
                       !driverInfo.zTripQualified.HasValue ? "" : driverInfo.zTripQualified.ToString(), //{8}
                       !driverInfo.StartDate.HasValue ? "" : driverInfo.StartDate.ToString(), //{9}
                       string.IsNullOrEmpty(driverInfo.HrDays) ? "" : driverInfo.HrDays, //{10}
                       !driverInfo.LeaseDate.HasValue ? "" : driverInfo.LeaseDate.ToString(), //{11}
                       string.IsNullOrEmpty(driverInfo.FleetType) ? "" : driverInfo.FleetType, //{12}
                       string.IsNullOrEmpty(driverInfo.DispatchUniqueKey) ? "" : driverInfo.DispatchUniqueKey, //{13}
                       driverImage == null ? "" : driverImage.Base64Serialize() //{14}
                       );
            }
            else
            {
                 cryptoDriver = string.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}{11}{12}{13}{14}",
                                      string.IsNullOrEmpty(driverInfo.DriverID) ? "" : driverInfo.DriverID, //{0}
                                      string.IsNullOrEmpty(driverInfo.VehicleID) ? "" : driverInfo.VehicleID, //{1}
                                      string.IsNullOrEmpty(driverInfo.FirstName) ? "" : driverInfo.FirstName, //{2}
                                      string.IsNullOrEmpty(driverInfo.LastName) ? "" : driverInfo.LastName, //{3}
                                      string.IsNullOrEmpty(driverInfo.CellPhone) ? "" : driverInfo.CellPhone, //{4}
                                      string.IsNullOrEmpty(driverInfo.HomePhone) ? "" : driverInfo.HomePhone, //{5}
                                      string.IsNullOrEmpty(driverInfo.AlternatePhone) ? "" : driverInfo.AlternatePhone, //{6}
                                      !driverInfo.QualificationDate.HasValue ? "" : driverInfo.QualificationDate.ToString(), //{7}
                                      !driverInfo.zTripQualified.HasValue ? "" : driverInfo.zTripQualified.ToString(), //{8}
                                      !driverInfo.StartDate.HasValue ? "" : driverInfo.StartDate.ToString(), //{9}
                                      string.IsNullOrEmpty(driverInfo.HrDays) ? "" : driverInfo.HrDays, //{10}
                                      !driverInfo.LeaseDate.HasValue ? "" : driverInfo.LeaseDate.ToString(), //{11}
                                      string.IsNullOrEmpty(driverInfo.FleetType) ? "" : driverInfo.FleetType, //{12}
                                      string.IsNullOrEmpty(fleetID) ? "" : fleetID, //{13}
                                      string.IsNullOrEmpty(driverInfo.DispatchUniqueKey) ? "" : driverInfo.DispatchUniqueKey, //{14}
                                      driverImage == null ? "" : driverImage.Base64Serialize() //{14}
                                      );
            }

            driverInfo.CryptoDriver = cryptoDriver.ToSHA1();

			#endregion

			using (var db = new DataAccess.VTOD.VTODEntities())
			{
				var images = new Images(logger);

				var foundDriver = db.vtod_driver_info.FirstOrDefault(s => s.CryptoDriver == driverInfo.CryptoDriver);
				
				if (foundDriver == null)
				{
					#region set the driver image url
					var uniqueID = System.Guid.NewGuid();
					driverInfo.AppendDate = System.DateTime.Now;
					if (driverImage != null)
					{
						driverInfo.PhotoUrl = images.GetPhotoUrl(uniqueID);
					}
					driverInfo.UniqueID = uniqueID.ToString();
					#endregion


					db.vtod_driver_info.Add(driverInfo);					

					#region Add/Update image if the image has changed

					if (driverImage != null)
					{
						#region Save Image in CDN (mutithread)
						var thread =
												new Thread(
													() =>
													{
														try
														{
															//images.UpdateDriverImageSizesFromConfig(Image.FromStream(new MemoryStream(driverImage)), driverInfo.FleetType, driverInfo.DispatchUniqueKey, driverInfo.DriverID, fleetId.ToString());
															images.UpdateDriverImageSizesFromConfig(Image.FromStream(new MemoryStream(driverImage)), uniqueID);
														}
														catch (System.Exception ex)
														{
															logger.Error("thread:UpsertDriverDetails:UpdateDriverImageSizesFromConfig", ex);
														}
													});
						thread.Start();
						#endregion
					}

					#endregion

					result = driverInfo;
				}
				else
				{
                    if (foundDriver.DriverNumber == null && driverInfo.DriverNumber != null)
                    {
                        foundDriver.DriverNumber = driverInfo.DriverNumber;
                    }
                   
                    result = foundDriver;
				}

                db.SaveChanges();

            }

			return result;
		}

	}
}
