﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
//using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Common.Track;
using UDI.VTOD.DataAccess.VTOD;
using UDI.Utility.Serialization;
using UDI.Utility.Helper;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.DTO;
using UDI.VTOD.Common.Helper;
using UDI.Map;
using System.IO;
using UDI.Notification.Service.Class;
using UDI.Notification.Service.Controller;
using System.Net.Mail;
using System.Net;
using Renci.SshNet;

namespace UDI.VTOD.Domain.VTOD
{
	public class VTODDomain : BaseSetting
	{
		#region Constructors
		public VTODDomain()
		{
			OwnerType = LogOwnerType.VTOD;
		}
		#endregion

		//Just for test SVN

		public void InsertTripNotification(long vtodTripID)
		{
			long result;
			using (var db = new VTODEntities())
			{
				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_VTOD_Domain, "Call");
				#endregion

				result = db.SP_vtod_Insert_trip_notification(vtodTripID);

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_DB, "SP_vtod_Insert_trip_notification");
				#endregion
			}
		}
        public void InsertCheckTripStatus(long vtodTripID)
        {
            long result;
            using (var db = new VTODEntities())
            {
                #region Track
                if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_VTOD_Domain, "Call");
                #endregion

                result = db.SP_vtod_Insert_check_trip_status(vtodTripID);

                #region Track
                if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_DB, "SP_vtod_Insert_check_trip_status");
                #endregion
            }
        }

		public void InsertTripNotification(long vtodTripID, string name, int status, string error)
		{
			long result;
			using (var db = new VTODEntities())
			{
				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_VTOD_Domain, "Call");
				#endregion

				result = db.SP_vtod_Update_trip_notification(vtodTripID, name, status, error);

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_DB, "SP_vtod_Update_trip_notification");
				#endregion
			}
		}
        		
		public bool ValidateTripUser(long tripID, string userName)
		{
			bool result = false;
			using (var db = new VTODEntities())
			{
				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_VTOD_Domain, "Call");
				#endregion

				result = db.SP_vtod_ValidateTripUser(tripID, userName).FirstOrDefault().Value;

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_DB, "SP_vtod_ValidateTripUser");
				#endregion
			}

			return result;
		}
        
        		
        public vtod_polygon RetrieveLatitudeLongitude(string Type, string AirportName)
        {
            vtod_polygon polygonInfo=null;
            try
            {
                using (VTODEntities context = new VTODEntities())
                {

                    polygonInfo=context.vtod_polygon.Where(p => p.Type.ToLower().ToString() == Type.ToLower().ToString() && p.Name == AirportName).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
               
                logger.Error("Aleph: Error while saving Corporate Special Instructions to database.", ex);
            }

            return polygonInfo;
        }
		public long InsertTrack(TrackTime trackTime)
		{
			var result = 0;
			if (isTrack)
			{
				var em = trackTime.Stopwatch.ElapsedMilliseconds;
				trackTime.EM = em;
				var xml = trackTime.XmlSerialize().ToString();
				var quickAnalysis = trackTime.GetQuickAnalysis();

				long? transactionID = null;
				if (trackTime.TransactionID != 0)
					transactionID = trackTime.TransactionID;

				using (var db = new VTODEntities())
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_VTOD_Domain, "Call");
					#endregion

					//db.SP_vtod_InsertTrack(transactionID, trackTime.MethodName, trackTime.ThreadID, em, trackTime.Owner.ToString(), quickAnalysis, xml, DateTime.Now);

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_DB, "SP_vtod_InsertTrack");
					#endregion
				}
			}

			return result;
		}

		public my_aspnet_users GetUserInfo(string userName)
		{
			my_aspnet_users user = null;
			//vtod_users_info result = null;
			using (var db = new VTODEntities())
			{
				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_VTOD_Domain, "Call");
				#endregion

				user = db.my_aspnet_users.Include("vtod_users_info").Where(s => s.name == userName).FirstOrDefault();


				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_DB, "db.my_aspnet_users.Where(s => s.name == userName).ToList()");
				#endregion
			}
			//if (user != null)
			//{
			//	result = (vtod_users_info)results.First();
			//}

			return user;
		}

		public taxi_trip GetTaxiTrip(long tripID)
		{
			taxi_trip result = null;
			using (var db = new VTODEntities())
			{
				try
				{
					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_VTOD_Domain, "Call");
					#endregion

					//result = (taxi_trip)db.vtod_trip.Where(s => s.Id == tripID).FirstOrDefault();
					result = db.taxi_trip.Where(s => s.Id == tripID).FirstOrDefault();

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_DB, "(taxi_trip)db.vtod_trip.Where(s => s.Id == tripID).FirstOrDefault()");
					#endregion
				}
				catch
				{ }

			}

			return result;
		}

        public van_trip GetVanTrip(long tripID)
        {
            van_trip result = null;
            using (var db = new VTODEntities())
            {
                try
                {
                    #region Track
                    if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_VTOD_Domain, "Call");
                    #endregion
                    result = db.van_trip.Where(s => s.Id == tripID).FirstOrDefault();

                    #region Track
                    if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_DB, "(van_trip)db.vtod_trip.Where(s => s.Id == tripID).FirstOrDefault()");
                    #endregion
                }
                catch
                { }

            }

            return result;
        }
        public ecar_trip GetEcarTrip(long tripID)
        {
            ecar_trip result = null;
            using (var db = new VTODEntities())
            {
                try
                {
                    #region Track
                    if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_VTOD_Domain, "Call");
                    #endregion
                    result = db.ecar_trip.Where(s => s.Id == tripID).FirstOrDefault();

                    #region Track
                    if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_DB, "(ecar_trip)db.vtod_trip.Where(s => s.Id == tripID).FirstOrDefault()");
                    #endregion
                }
                catch
                { }

            }

            return result;
        }

        public RateSetRS RateSet(RateSetRQ request)
		{
			try
			{
				RateSetRS result = null;
				vtod_trip trip = null;
				var memberId = request.MemberId;
				var tripId = request.Reference.First().ID.ToInt64();
				var rate = request.Rate;

				var rateType = (int)Common.DTO.Enum.RatingType.Trip; //Defualt if Type is empty

				#region Set Rate Type & Rate
				if (request != null && !string.IsNullOrWhiteSpace(request.Type))
				{
					var rateString = request.Type;
					if (rateString.ToLower().Trim() == Common.DTO.Enum.RatingType.Cancel.ToString().ToLower())
					{
						rateType = (int)Common.DTO.Enum.RatingType.Cancel;
						rate = -1;
					}
					else if (rateString.ToLower().Trim() == Common.DTO.Enum.RatingType.Trip.ToString().ToLower())
					{
						rateType = (int)Common.DTO.Enum.RatingType.Trip;
					}
					else
					{
						throw VtodException.CreateFieldFormatValidationException("Type", "Trip or Cancel");
					}
				}
				#endregion

				using (var db = new VTODEntities())
				{
					#region Get Trip
					trip = db.vtod_trip.Where(s => s.Id == tripId).FirstOrDefault();
					#endregion

					if (trip == null)
					{
						logger.ErrorFormat("ReteSetMethod:: Trip# {0} not found for Rating", tripId);// new Exception(Messages.Taxi_TripNotFound);
					}
					else
					{
						#region Check MemberID
						var memberFromTrip = trip.MemberID;
						if (memberFromTrip != memberId.ToString())
						{
							throw VtodException.CreateFieldRequiredValidationException("MemberId");// new Exception(Messages.Membership_WrongMemberID);
						}
						#endregion

						vtod_trip_rate rateDAO = new vtod_trip_rate();
						rateDAO.TripID = tripId;
						rateDAO.Rate = rate;
						rateDAO.Type = rateType;
						rateDAO.AppendTime = DateTime.Now;
						rateDAO.Comment = request.Comment;

						if (request.Reasons != null && request.Reasons.Any())
						{
							rateDAO.vtod_trip_rate_reason = new List<vtod_trip_rate_reason>();
							foreach (var reason in request.Reasons)
							{
								var reasonDAO = new vtod_trip_rate_reason { Key = reason.Name, Value = reason.Value };
								rateDAO.vtod_trip_rate_reason.Add(reasonDAO);
							}
						}

						#region Track
						if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_VTOD_Domain, "Call");
						#endregion

						db.vtod_trip_rate.Add(rateDAO);
						db.SaveChanges();

						#region Track
						if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_DB, "db.vtod_trip_rate.Add(rateDAO)");
						#endregion
					}
				}

				#region Output Conversion
				result = new RateSetRS();
				result.Success = new Success();

				#region Common
				result.EchoToken = request.EchoToken;
				result.PrimaryLangID = request.PrimaryLangID;
				result.Target = request.Target;
				result.Version = request.Version;
				#endregion
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("MembershipDomain.CreateMembership:: {0}", ex.ToString() + ex.StackTrace);

				throw new Exception(ex.Message);
			}
		}

		public RateGetRS RateGet(RateGetRQ request)
		{
			try
			{
				RateGetRS result = null;
				vtod_trip trip = null;
				var memberId = request.MemberId;
				var tripId = request.Reference.First().ID.ToInt64();
				int rate = 0;
				using (var db = new VTODEntities())
				{
					#region Get Trip
					var trips = db.vtod_trip.Where(s => s.Id == tripId);
					if (trips != null && trips.Any())
					{
						trip = trips.First();

					}
					else
					{
						throw VtodException.CreateException(ExceptionType.Rate, 1012);//throw new Exception(Messages.Taxi_TripNotFound);
					}
					#endregion

					#region Check MemberID
					var memberFromTrip = trip.MemberID;
					if (memberFromTrip != memberId.ToString())
					{
						throw VtodException.CreateFieldRequiredValidationException("MemberId");// new Exception(Messages.Membership_WrongMemberID);
					}
					#endregion

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_VTOD_Domain, "Call");
					#endregion

					var rateDAOs = db.vtod_trip_rate.Where(s => s.TripID == tripId).ToList();//.Last();

					#region Track
					if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_DB, "db.vtod_trip_rate.Where(s => s.TripID == tripId).ToList()");
					#endregion

					if (rateDAOs != null && rateDAOs.Any())
					{
						rate = rateDAOs.Last().Rate;
					}
				}

				#region Output Conversion
				result = new RateGetRS();
				result.Success = new Success();
				result.Rate = rate.ToString();

				#region Common
				result.EchoToken = request.EchoToken;
				result.PrimaryLangID = request.PrimaryLangID;
				result.Target = request.Target;
				result.Version = request.Version;
				#endregion
				#endregion

				return result;
			}
			catch (Exception ex)
			{
				logger.ErrorFormat("MembershipDomain.CreateMembership:: {0}", ex.ToString() + ex.StackTrace);

				throw new Exception(ex.Message);
			}
		}

		public string ConvertSDSTripStatusMessageForFrontEndDevice(string userName, string statusMessage)
		{
			string result = "";

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_VTOD_Domain, "Call");
			#endregion

			using (VTODEntities context = new VTODEntities())
			{
				var user = context.my_aspnet_users.Where(s => s.name == userName).FirstOrDefault();
				result = context.sds_status_frontendwrapper.Where(x => x.UserId == user.id && x.VTODSDSStatus == statusMessage).Select(x => x.FrontEndStatus).FirstOrDefault();
				logger.InfoFormat("Convet trip status for frontend app. userID={0}, vtod_status={1}, frontendStatus={2}", user.id, statusMessage, result);
			}

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_DB, "ConvertSDSTripStatusMessageForFrontEndDevice");
			#endregion

			return result;
		}

		public long GetMappedEcarVtodID(long sdsTripID)
		{
			var result = sdsTripID;

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_VTOD_Domain, "Call");
			#endregion

			using (VTODEntities db = new VTODEntities())
			{
				var mappedTrip = db.vtod_map_sds_ecar_trip.Where(s => s.SDSTripID == sdsTripID).FirstOrDefault();

				if (mappedTrip != null)
				{
					result = mappedTrip.ECarTripId;
				}
			}

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_DB, "ConvertSDSTripStatusMessageForFrontEndDevice");
			#endregion

			return result;
		}

		public Common.DTO.Enum.DispatchType GetDispatchType(long vtodTripID)
		{
			var result = Common.DTO.Enum.DispatchType.Unkown;

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_VTOD_Domain, "Call");
			#endregion

			using (VTODEntities db = new VTODEntities())
			{
				var count = 0;
				if (count == 0)
				{
					count = db.sds_trip.Where(s => s.Id == vtodTripID).Count();
					if (count > 0)
					{
						result = DispatchType.SDS;
					}
				}

				if (count == 0)
				{
					count = db.ecar_trip.Where(s => s.Id == vtodTripID).Count();
					if (count > 0)
					{
						result = DispatchType.ECar;
					}
				}

				if (count == 0)
				{
					count = db.taxi_trip.Where(s => s.Id == vtodTripID).Count();
					if (count > 0)
					{
						result = DispatchType.Taxi;
					}
				}

			}

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_DB, "ConvertSDSTripStatusMessageForFrontEndDevice");
			#endregion

			return result;
		}


        public Common.DTO.Enum.DispatchType GetDispatchTypeWithDispatchConfirmation(string dispatchConfirmation)
        {   
            using (VTODEntities db = new VTODEntities())
            {

                if (db.sds_trip.Where(x => x.ConfirmationNumber.Equals(dispatchConfirmation)).Any())
                {
                    return DispatchType.SDS;
                }

                if (db.ecar_trip.Where(x => x.DispatchTripId.Equals(dispatchConfirmation)).Any())
                {
                    return DispatchType.ECar;
                }


                if (db.taxi_trip.Where(x => x.DispatchTripId.Equals(dispatchConfirmation)).Any())
                {
                    return DispatchType.Taxi;
                }
            }
          
            return DispatchType.Unkown; 
        }

        public string ConvertSDSTripStatus(string source)
		{
			string target = source;

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_VTOD_Domain, "Call");
			#endregion


			using (VTODEntities context = new VTODEntities())
			{
				var t = context.sds_status_wrapper.Where(x => x.Source == source).Select(x => x.Target).FirstOrDefault();
				if (!string.IsNullOrWhiteSpace(t))
				{
					target = t;
					logger.InfoFormat("Convert status from {0} to {1}", source, target);
				}
			}

			#region Track
			if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_DB, "ConvertSDSTripStatusForUDI33");
			#endregion

			return target;
		}


      
		public vtod_polygon GetAirport(string airportCode)
		{
			vtod_polygon result = null;
			using (var db = new VTODEntities())
			{
				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_VTOD_Domain, "Call");
				#endregion


				result = db.vtod_polygon.Where(s => s.Name == airportCode && s.Type == "Airport").FirstOrDefault();

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_DB, "db.GetAirportLatLon");
				#endregion
			}


			return result;
		}

		public string GetContextErrorCode(string userName, string contextCode, string languageCode)
		{
			string messgae = null;

			using (var db = new VTODEntities())
			{
				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_VTOD_Domain, "Call");
				#endregion

				messgae = db.SP_vtod_CustomMessage(userName, contextCode.ToInt64(), languageCode).FirstOrDefault();

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_DB, "SP_vtod_CustomMessage");
				#endregion
			}


			return messgae;
		}

		public string Get_WS_State(string tableName, long referenceID, string key)
		{
			string result = null;
			using (var db = new VTODEntities())
			{
				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_VTOD_Domain, "Call");
				#endregion


				var resultDAO = db.vtod_WS_State.Where(s => s.TableName == tableName && s.ReferenceId == referenceID && s.Key == key).FirstOrDefault();
				if (resultDAO != null)
				{
					result = resultDAO.State;
				}


				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_DB, "db.GetWS_State");
				#endregion
			}


			return result;
		}

		public void Set_WS_State(string tableName, long referenceID, string key, string state)
		{
			var stateDAO = new vtod_WS_State { TableName = tableName, ReferenceId = referenceID, Key = key, State = state, AppendTime = System.DateTime.Now };
			using (var db = new VTODEntities())
			{
				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_VTOD_Domain, "Call");
				#endregion

				db.vtod_WS_State.Add(stateDAO);
				db.SaveChanges();

				#region Track
				if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_DB, "db.Set_WS_State");
				#endregion
			}


		}

        public void InsertPushNotification(long? memberID,long? groupID,long? tripID,DateTime? datetimeUTC,string pushType,int fleetTripCode,string Status,string firstName,bool allowSMS,string userName,int workflow)
        {
            using (var db = new VTODEntities())
            {
                #region Track
                if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_VTOD_Domain, "Call");
                #endregion
                db.SP_PushNotificationInsertion(memberID, groupID, tripID, datetimeUTC, pushType, fleetTripCode, Status, null, firstName, allowSMS, userName, workflow);

                #region Track
                 if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_DB, "SP_PushNotificationInsertion");
                #endregion
            }
        }

        public vtod_trip GetVTODUTCTime(long? vtodTripID)
        {
            vtod_trip result=null;
            using (var db = new VTODEntities())
            {
                #region Track
                if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_VTOD_Domain, "Call");
                #endregion
                result=db.vtod_trip.Where(p => p.Id == vtodTripID).FirstOrDefault();

                #region Track
                if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_DB, "GetVTODUTCTime");
                #endregion
            }
            return result;
        }

        public OTA_GroundAvailRS CalculateVehicleMovement(TokenRS tokenVTOD, OTA_GroundAvailRQ request, OTA_GroundAvailRS response)
        {
            logger.Info("Start");
            Dictionary<string, string> vehicleList = new Dictionary<string, string>();
            Dictionary<string, string> newvehicleList = new Dictionary<string, string>();
            const int MaxVehiclesToReturnDefault = 5;
            int vehiclesToReturn = MaxVehiclesToReturnDefault;
            try
            {
                vehiclesToReturn = Convert.ToInt32(ConfigurationManager.AppSettings["NumberOfReturnedMovingVehicle"]);
            }
            catch
            {
                vehiclesToReturn = MaxVehiclesToReturnDefault;
            }
            try
            {
                var vehiclesListForMovement = response.TPA_Extensions.Vehicles.Items.Take(vehiclesToReturn);
                #region Chunkify
                var tasks = new List<Task>();

                logger.InfoFormat("Start - Dividing the List of records with respect to thread count");
                var chunks = vehiclesListForMovement.ToList().Chunkify(vehiclesToReturn).ToList();
                logger.InfoFormat("End - Dividing the List of records with respect to thread count");

                #endregion

                #region Run TPL

                foreach (var chunk in chunks)
                {

                    Task taskGoogleChunk = new Task(
                        () =>
                        {
                            try
                            {
                                Vehicle vehicleItem = (Vehicle)chunk.FirstOrDefault();
                                List<Geolocation> geolocationList = new List<Geolocation>();
                                if (request.TPA_Extensions != null)
                                {
                                    var specialInputRequest = request.TPA_Extensions.WebServiceState;
                                    if (!string.IsNullOrWhiteSpace(specialInputRequest))
                                    {
                                        string requestWebServiceState = UDI.Utility.Helper.Cryptography.DecryptDES(specialInputRequest, ConfigurationManager.AppSettings["Encryption_Key"], ConfigurationManager.AppSettings["Encryption_IV"]);
                                        vehicleList = requestWebServiceState.JsonDeserialize<Dictionary<string, string>>();
                                    }
                                }
                                if (vehicleList != null)
                                {
                                    if (vehicleList.Any())
                                    {
                                        var error = string.Empty;
                                        if (vehicleList.ContainsKey(vehicleItem.ID) == true)
                                        {
                                            string latValues = vehicleList[vehicleItem.ID].Substring(0, vehicleList[vehicleItem.ID].IndexOf("|"));
                                            string longValues = vehicleList[vehicleItem.ID].Substring(vehicleList[vehicleItem.ID].IndexOf("|") + 1, (vehicleList[vehicleItem.ID].IndexOf("#")) - (vehicleList[vehicleItem.ID].IndexOf("|") + 1));
                                            var map = new UDI.Map.MapService(Map.DTO.MapApiProvider.Google);
                                            List<UDI.Map.DTO.Geolocation> geolocationObj = new List<UDI.Map.DTO.Geolocation>();
                                            geolocationObj = map.CalculateVehicleMovements(new Map.DTO.Geolocation { Latitude = vehicleItem.Geolocation.Lat.ToDecimal(), Longitude = vehicleItem.Geolocation.Long.ToDecimal() }, new Map.DTO.Geolocation { Latitude = latValues.ToDecimal(), Longitude = longValues.ToDecimal() }, out error);
                                            if (geolocationObj != null)
                                            {
                                                geolocationList = geolocationObj.Select(s => new Geolocation
                                                {
                                                    Lat = s.Latitude,
                                                    Long = s.Longitude,
                                                    Course = s.Coarse
                                                }).ToList();
                                                string course = vehicleList[vehicleItem.ID].Split('#').Last();
                                                geolocationList[0].Course = course.ToDecimal();
                                                vehicleItem.Geolocations = new List<Geolocation>();
                                                vehicleItem.Geolocations = geolocationList;
                                              
                                            }
                                        
                                            newvehicleList.Add(vehicleItem.ID, (vehicleItem.Geolocations.LastOrDefault().Lat.ToString() + '|' + vehicleItem.Geolocations.LastOrDefault().Long.ToString() + '#' + vehicleItem.Geolocations.LastOrDefault().Course.ToString()));

                                        }

                                    }
                                    else
                                    {
                                        newvehicleList.Add(vehicleItem.ID, (vehicleItem.Geolocation.Lat.ToString() + '|' + vehicleItem.Geolocation.Long.ToString() + '#' + vehicleItem.Geolocation.Course.ToString()));
                                    }

                                }
                                else
                                {
                                    newvehicleList.Add(vehicleItem.ID, (vehicleItem.Geolocation.Lat.ToString() + '|' + vehicleItem.Geolocation.Long.ToString() + '#' + vehicleItem.Geolocation.Course.ToString()));
                                }

                            }
                            catch (Exception ex)
                            {
                                logger.Error(ex);
                            }
                        }
                        );


                    tasks.Add(taskGoogleChunk);
                }
                if (tasks.Any())
                {
                    foreach (var item in tasks)
                    {
                        item.Start();
                    }

                    Task.WaitAll(tasks.ToArray());
                }
                #endregion















                //foreach (var vehicleItem in response.TPA_Extensions.Vehicles.Items)
                //{
                //    if (vehiclesToReturn > 0)
                //    {
                //        List<Geolocation> geolocationList = new List<Geolocation>();

                //        try
                //        {
                //            if (request.TPA_Extensions != null)
                //            {
                //                var specialInputRequest = request.TPA_Extensions.WebServiceState;
                //                if (!string.IsNullOrWhiteSpace(specialInputRequest))
                //                {
                //                    string requestWebServiceState = UDI.Utility.Helper.Cryptography.DecryptDES(specialInputRequest, ConfigurationManager.AppSettings["Encryption_Key"], ConfigurationManager.AppSettings["Encryption_IV"]);
                //                    vehicleList = requestWebServiceState.JsonDeserialize<Dictionary<string, string>>();
                //                }
                //            }

                //            if (vehicleList != null)
                //            {
                //                if (vehicleList.Any())
                //                {
                //                    var error = string.Empty;
                //                    if (vehicleList.ContainsKey(vehicleItem.ID) == true)
                //                    {
                //                        string latValues = vehicleList[vehicleItem.ID].Substring(0, vehicleList[vehicleItem.ID].IndexOf("|"));
                //                        string longValues = vehicleList[vehicleItem.ID].Substring(vehicleList[vehicleItem.ID].IndexOf("|") + 1, vehicleList[vehicleItem.ID].Length - (vehicleList[vehicleItem.ID].IndexOf("|") + 1));
                //                        var map = new UDI.Map.MapService(Map.DTO.MapApiProvider.Google);
                //                        List<UDI.Map.DTO.Geolocation> geolocationObj = new List<UDI.Map.DTO.Geolocation>();
                //                        geolocationObj = map.CalculateVehicleMovements(new Map.DTO.Geolocation { Latitude = vehicleItem.Geolocation.Lat.ToDecimal(), Longitude = vehicleItem.Geolocation.Long.ToDecimal() },
                //                         new Map.DTO.Geolocation { Latitude = latValues.ToDecimal(), Longitude = longValues.ToDecimal() }, out error);
                //                        if (geolocationObj != null)
                //                        {
                //                            geolocationList = geolocationObj.Select(s => new Geolocation
                //                            {
                //                                Lat = s.Latitude,
                //                                Long = s.Longitude,
                //                                Course = s.Coarse
                //                            }).ToList();
                //                            vehicleItem.Geolocations = new List<Geolocation>();
                //                            vehicleItem.Geolocations = geolocationList;
                //                        }

                //                        newvehicleList.Add(vehicleItem.ID, (vehicleItem.Geolocation.Lat.ToString() + '|' + vehicleItem.Geolocation.Long.ToString()));
                                      
                //                    }

                //                }
                //                else
                //                {
                //                    newvehicleList.Add(vehicleItem.ID, (vehicleItem.Geolocation.Lat.ToString() + '|' + vehicleItem.Geolocation.Long.ToString()));
                //                }

                //            }
                //            else
                //            {
                //                newvehicleList.Add(vehicleItem.ID, (vehicleItem.Geolocation.Lat.ToString() + '|' + vehicleItem.Geolocation.Long.ToString()));
                //            }

                //        }


                //        catch (Exception ex)
                //        {

                //            logger.ErrorFormat("Message:{0}", ex.Message);
                //            logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
                //            logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
                //            response = null;

                //        }
                //        vehiclesToReturn = vehiclesToReturn - 1;
                //    }

                //}
                string webServiceState=UDI.Utility.Helper.Cryptography.EncryptDES(newvehicleList.JsonSerialize().ToString(), ConfigurationManager.AppSettings["Encryption_Key"], ConfigurationManager.AppSettings["Encryption_IV"]);
                response.TPA_Extensions.WebServiceState = webServiceState;
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Message:{0}", ex.Message);
                logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
                logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
            }

            return response;
        }

        public OTA_GroundAvailRS CalculateAverageETA(TokenRS tokenVTOD, OTA_GroundAvailRQ request, OTA_GroundAvailRS response)
        {
            Dictionary<string, string> vehicleList = new Dictionary<string, string>();
            Dictionary<string, string> newvehicleList = new Dictionary<string, string>();
            const int MaxVehiclesForAverageETA = 5;
            int vehiclesToReturn = MaxVehiclesForAverageETA;
            List<string> VehicleGeolocation = new List<string>();
            List<double> VehicleDistance = new List<double>();
            
            try
            {
                vehiclesToReturn = Convert.ToInt32(ConfigurationManager.AppSettings["NumberOfETAAverageVehicle"]);
            }
            catch
            {
                vehiclesToReturn = MaxVehiclesForAverageETA;
            }
            try
            {
                int? etaWithTraffic = null;
                double fromLatitude=0;
                double fromLongitude=0;
                var error = string.Empty;
                for(int k=0;k<response.TPA_Extensions.Vehicles.Items.Count();k++)
                {
                    VehicleGeolocation.Add(response.TPA_Extensions.Vehicles.Items[k].Geolocation.Lat + "##" + response.TPA_Extensions.Vehicles.Items[k].Geolocation.Long);
                    fromLatitude = request.Service.Pickup.Address.Latitude.ToDouble();
                    fromLongitude = request.Service.Pickup.Address.Longitude.ToDouble();
                    MapService ms = new MapService();
                    double d = ms.GetDirectDistanceInMeter(fromLatitude, fromLongitude, response.TPA_Extensions.Vehicles.Items[k].Geolocation.Lat.ToDouble(), response.TPA_Extensions.Vehicles.Items[k].Geolocation.Long.ToDouble());
                    VehicleDistance.Add(d);
                    if (k == vehiclesToReturn - 1)
                    break;
                }
               double average=  VehicleDistance.Average(x => x);
               double iteminList = VehicleDistance.LastOrDefault(n => n < average);
               int index = VehicleDistance.IndexOf(iteminList);
               string lat = string.Empty;
               string lon = string.Empty;
               if (index == -1)
               {
                   if (VehicleGeolocation != null)
                   {
                       if (VehicleGeolocation.Any())
                       {
                           string averageETAGelocation = VehicleGeolocation[0];
                           lat = averageETAGelocation.Substring(0, averageETAGelocation.IndexOf("##"));
                           lon = averageETAGelocation.Substring(averageETAGelocation.IndexOf("##") + 2);
                       }
                   }
               }
               else
               {
                   string averageETAGelocation = VehicleGeolocation[index];
                   lat = averageETAGelocation.Substring(0, averageETAGelocation.IndexOf("##"));
                   lon = averageETAGelocation.Substring(averageETAGelocation.IndexOf("##") + 2);
               }
               var map = new UDI.Map.MapService(Map.DTO.MapApiProvider.Google);
               var etaResult = map.GetETA(new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(fromLatitude), Longitude = Convert.ToDecimal(fromLongitude) },
                   new Map.DTO.Geolocation { Latitude = lat.ToDecimal(), Longitude = lon.ToDecimal() }, UDI.Map.DTO.DistanceUnit.Mile, out error);
               if (string.IsNullOrWhiteSpace(error))
               {
                   etaWithTraffic = System.Math.Round((decimal)(etaResult.DurationInTrafficInSecond / 60)).ToInt32();
                   response.TPA_Extensions.Vehicles.AverageVehicleETA = etaWithTraffic.ToString();
               }
             
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Message:{0}", ex.Message);
                logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
                logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
            }

            return response;
        }
        #region Texas  Cashering 
        internal void TaxiUploadToSFTP()
        {

            try
            {
                //========================================================================================
                // We want to retrieve static fields from database table.  In particular the SFTP
                // credentials and file directories for loading, uploading, and archiving.
                //========================================================================================
                string remoteRootDirectory = String.Empty, localArchiveDirectory = String.Empty, localTransportDirectory = String.Empty,
                        sftpHostName = String.Empty, sftpUsername = String.Empty, sftpPassword = String.Empty, sftpHostPort = String.Empty;

                GetTaxiSftpAndFileDirectoryConfigValues(out remoteRootDirectory, out localArchiveDirectory, out localTransportDirectory
                    , out sftpHostName, out sftpUsername, out sftpPassword, out sftpHostPort);

                // Retrieve the file names with paths from file upload directory. And check if there 
                // are any files to process.  If there are, connecto to SFTP and upload.  Otherwise
                // do nothing.
                string[] fileEntries = Directory.GetFiles(localTransportDirectory);

                if (fileEntries.Length > 0)
                {

                    // Log the database configuration values.
                    logger.Info("");
                    logger.Info(String.Format("Database Configuration Values: RemoteRootDirectory={0} LocalArchiveDirectory={1} LocalTransportDirectory={2} " +
                        "SftpHostName={3} SftpUsername={4} SftpPassword={5} SftpHostPort={6}", remoteRootDirectory,
                        localArchiveDirectory, localTransportDirectory, sftpHostName,
                        sftpUsername, sftpPassword, sftpHostPort));

                    // Create SSH.Net SftpClient
                    using (var Client = new SftpClient(sftpHostName, Convert.ToInt32(sftpHostPort), sftpUsername, sftpPassword))
                    {
                        // Connect
                        Client.Connect();

                        // Loop through each file in order to upload to SFTP one-by-one.
                        foreach (string fileName in fileEntries)
                        {

                            //========================================================================================
                            // We have to load files in different SFTP remote directories per Taxi Charger's 
                            // business requirements.  To facilitate this we appended the target remote directory 
                            // name to the file name.  This is how we identify which remote directory to place 
                            // the file in.
                            //========================================================================================

                            // Log file names.
                            logger.Info(String.Format("File to be uploaded: {0}", fileName ?? "None"));

                            // Extract the file name and remove path.
                            string fileNameWithoutPath = Path.GetFileName(fileName);

                            // The prefix of the file name determines the remote SFTP directory where the file will be uploaded to.
                            string remoteDirectory = fileNameWithoutPath.Substring(0, fileNameWithoutPath.IndexOf("_")).ToLower();

                            // Change into the target directory.
                            Client.ChangeDirectory(remoteRootDirectory + remoteDirectory);

                            using (var fileStream = new FileStream(fileName, FileMode.Open))
                            {
                                Client.BufferSize = 4 * 1024; //bypass Payload error for large files
                                Client.UploadFile(fileStream, fileNameWithoutPath);
                            }

                            //========================================================================================
                            // We want to ensure that the file was successfully uploaded.  We want to retrieve
                            // the files on the target remote directory and check if the file name matches 
                            // the file we uploaded.  If it does, we can move file to Archive directory.
                            //========================================================================================

                            // Retrieve the files on the remote 
                            var remoteFileEntries = Client.ListDirectory(remoteRootDirectory + remoteDirectory);

                            // Loop through remote directory files.
                            foreach (var file in remoteFileEntries)
                            {
                                // Confirm that we successfully uploaded file.
                                if (!file.IsDirectory && file.Name == fileNameWithoutPath)
                                {
                                    // Check if file exists locally otherwise exception will be thrown.
                                    if (File.Exists(fileName))
                                    {

                                        // Delete file in archive before moving.  
                                        // Should not be there but want to avoid exception.
                                        File.Delete(localArchiveDirectory + fileNameWithoutPath);

                                        File.Move(fileName, localArchiveDirectory + fileNameWithoutPath);

                                        // Log file that was successfully uploaded and moved to archive.
                                        logger.Info(String.Format("File successfully uploaded and moved to archive: {0}", fileName ?? "None"));

                                        break;

                                    }
                                }

                            } //  foreach remoteFileEntries

                        } // foreach fileEntries

                    } // using

                }

            }
            catch (Exception ex)
            {

                CaptureExceptionAndLog(ex);

                SendTaxiPushCaashieringExceptionEmail(ex);
            }

        }

        internal void ECarUploadToSFTP()
        {

            try
            {
                //========================================================================================
                // We want to retrieve static fields from database table.  In particular the SFTP
                // credentials and file directories for loading, uploading, and archiving.
                //========================================================================================
                string remoteRootDirectory = String.Empty, localArchiveDirectory = String.Empty, localTransportDirectory = String.Empty,
                        sftpHostName = String.Empty, sftpUsername = String.Empty, sftpPassword = String.Empty, sftpHostPort = String.Empty;

                GetECarSftpAndFileDirectoryConfigValues(out remoteRootDirectory, out localArchiveDirectory, out localTransportDirectory
                    , out sftpHostName, out sftpUsername, out sftpPassword, out sftpHostPort);

                // Retrieve the file names with paths from file upload directory. And check if there 
                // are any files to process.  If there are, connecto to SFTP and upload.  Otherwise
                // do nothing.
                string[] fileEntries = Directory.GetFiles(localTransportDirectory);

                if (fileEntries.Length > 0)
                {

                    // Log the database configuration values.
                    logger.Info("");
                    logger.Info(String.Format("Database Configuration Values: RemoteRootDirectory={0} LocalArchiveDirectory={1} LocalTransportDirectory={2} " +
                        "SftpHostName={3} SftpUsername={4} SftpPassword={5} SftpHostPort={6}", remoteRootDirectory,
                        localArchiveDirectory, localTransportDirectory, sftpHostName,
                        sftpUsername, sftpPassword, sftpHostPort));

                    // Create SSH.Net SftpClient
                    using (var Client = new SftpClient(sftpHostName, Convert.ToInt32(sftpHostPort), sftpUsername, sftpPassword))
                    {
                        // Connect
                        Client.Connect();

                        // Loop through each file in order to upload to SFTP one-by-one.
                        foreach (string fileName in fileEntries)
                        {

                            //========================================================================================
                            // We have to load files in different SFTP remote directories per Taxi Charger's 
                            // business requirements.  To facilitate this we appended the target remote directory 
                            // name to the file name.  This is how we identify which remote directory to place 
                            // the file in.
                            //========================================================================================

                            // Log file names.
                            logger.Info(String.Format("File to be uploaded: {0}", fileName ?? "None"));

                            // Extract the file name and remove path.
                            string fileNameWithoutPath = Path.GetFileName(fileName);

                            // The prefix of the file name determines the remote SFTP directory where the file will be uploaded to.
                            string remoteDirectory = fileNameWithoutPath.Substring(0, fileNameWithoutPath.IndexOf("_")).ToLower();

                            // Change into the target directory.
                            Client.ChangeDirectory(remoteRootDirectory + remoteDirectory);

                            using (var fileStream = new FileStream(fileName, FileMode.Open))
                            {
                                Client.BufferSize = 4 * 1024; //bypass Payload error for large files
                                Client.UploadFile(fileStream, fileNameWithoutPath);
                            }

                            //========================================================================================
                            // We want to ensure that the file was successfully uploaded.  We want to retrieve
                            // the files on the target remote directory and check if the file name matches 
                            // the file we uploaded.  If it does, we can move file to Archive directory.
                            //========================================================================================

                            // Retrieve the files on the remote 
                            var remoteFileEntries = Client.ListDirectory(remoteRootDirectory + remoteDirectory);

                            // Loop through remote directory files.
                            foreach (var file in remoteFileEntries)
                            {
                                // Confirm that we successfully uploaded file.
                                if (!file.IsDirectory && file.Name == fileNameWithoutPath)
                                {
                                    // Check if file exists locally otherwise exception will be thrown.
                                    if (File.Exists(fileName))
                                    {

                                        // Delete file in archive before moving.  
                                        // Should not be there but want to avoid exception.
                                        File.Delete(localArchiveDirectory + fileNameWithoutPath);

                                        File.Move(fileName, localArchiveDirectory + fileNameWithoutPath);

                                        // Log file that was successfully uploaded and moved to archive.
                                        logger.Info(String.Format("File successfully uploaded and moved to archive: {0}", fileName ?? "None"));

                                        break;

                                    }
                                }

                            } //  foreach remoteFileEntries

                        } // foreach fileEntries

                    } // using

                }

            }
            catch (Exception ex)
            {

                CaptureExceptionAndLog(ex);

                SendECarPushCaashieringExceptionEmail(ex);
            }

        }








        private static void GetTaxiSftpAndFileDirectoryConfigValues(out string remoteRootDirectory, out string localArchiveDirectory
                   , out string localTransportDirectory, out string sftpHostName, out string sftpUsername, out string sftpPassword, out string sftpHostPort)
        {
            using (var db = new DataAccess.VTOD.VTODEntities())
            {
                // The root of the remote directory. Should be "/upload/".
                remoteRootDirectory = db.TaxiCashieringFeedConfigurations
                                        .Where(x => x.Type == "FileDirectory"
                                            && x.Name == "SftpRootDirectory"
                                            && x.IsActive)
                                        .Select(x => x.Value).FirstOrDefault() ?? String.Empty;

                // The local archive directory. Should be @"C:\Exports\TaxiCharger\Archive\".
                localArchiveDirectory = db.TaxiCashieringFeedConfigurations
                                            .Where(x => x.Type == "FileDirectory"
                                                && x.Name == "LocalArchiveDirectory"
                                                && x.IsActive)
                                            .Select(x => x.Value).FirstOrDefault() ?? String.Empty;

                // Directory of the files that will be uploaded. Should be @"C:\Exports\TaxiCharger\TransportFiles\".
                localTransportDirectory = db.TaxiCashieringFeedConfigurations
                                            .Where(x => x.Type == "FileDirectory"
                                                && x.Name == "LocalTransportDirectory"
                                                && x.IsActive)
                                            .Select(x => x.Value).FirstOrDefault() ?? String.Empty;

                // SFTP Host.  Should be "sftp.taxicharger.com".
                sftpHostName = db.TaxiCashieringFeedConfigurations
                                    .Where(x => x.Type == "SftpCredentials"
                                            && x.Name == "SftpHostName"
                                            && x.IsActive)
                                    .Select(x => x.Value).FirstOrDefault() ?? String.Empty;

                // SFTP Host port number.  Should be 43210.
                sftpHostPort = db.TaxiCashieringFeedConfigurations
                                    .Where(x => x.Type == "SftpCredentials"
                                            && x.Name == "SftpHostPort"
                                            && x.IsActive)
                                    .Select(x => x.Value).FirstOrDefault() ?? String.Empty;

                // SFTP Username. Should be "texas-ztrip".
                sftpUsername = db.TaxiCashieringFeedConfigurations
                                    .Where(x => x.Type == "SftpCredentials"
                                            && x.Name == "SftpUsername"
                                            && x.IsActive)
                                    .Select(x => x.Value).FirstOrDefault() ?? String.Empty;

                // SFTP Password.  Should be m4Lp!dVwA3
                sftpPassword = db.TaxiCashieringFeedConfigurations
                                    .Where(x => x.Type == "SftpCredentials"
                                            && x.Name == "SftpPassword"
                                            && x.IsActive)
                                    .Select(x => x.Value).FirstOrDefault() ?? String.Empty;

            }

            if (remoteRootDirectory == String.Empty || localArchiveDirectory == String.Empty || localTransportDirectory == String.Empty ||
                    sftpHostName == String.Empty || sftpUsername == String.Empty || sftpPassword == String.Empty || sftpHostPort == String.Empty)
            {
                throw new Exception("Unable to retrieve dbo.TaxiCashieringFeedConfiguration configuration records.");
            }
        }


        private static void GetECarSftpAndFileDirectoryConfigValues(out string remoteRootDirectory, out string localArchiveDirectory
                 , out string localTransportDirectory, out string sftpHostName, out string sftpUsername, out string sftpPassword, out string sftpHostPort)
        {
            using (var db = new DataAccess.VTOD.VTODEntities())
            {
                // The root of the remote directory. Should be "/upload/".
                remoteRootDirectory = db.ecarCashieringFeedConfigurations
                                        .Where(x => x.Type == "FileDirectory"
                                            && x.Name == "SftpRootDirectory"
                                            && x.IsActive)
                                        .Select(x => x.Value).FirstOrDefault() ?? String.Empty;

                // The local archive directory. Should be @"C:\Exports\ECarCharger\Archive\".
                localArchiveDirectory = db.ecarCashieringFeedConfigurations
                                            .Where(x => x.Type == "FileDirectory"
                                                && x.Name == "LocalArchiveDirectory"
                                                && x.IsActive)
                                            .Select(x => x.Value).FirstOrDefault() ?? String.Empty;

                // Directory of the files that will be uploaded. Should be @"C:\Exports\ECarCharger\TransportFiles\".
                localTransportDirectory = db.ecarCashieringFeedConfigurations
                                            .Where(x => x.Type == "FileDirectory"
                                                && x.Name == "LocalTransportDirectory"
                                                && x.IsActive)
                                            .Select(x => x.Value).FirstOrDefault() ?? String.Empty;

                // SFTP Host.  Should be "sftp.taxicharger.com".
                sftpHostName = db.ecarCashieringFeedConfigurations
                                    .Where(x => x.Type == "SftpCredentials"
                                            && x.Name == "SftpHostName"
                                            && x.IsActive)
                                    .Select(x => x.Value).FirstOrDefault() ?? String.Empty;

                // SFTP Host port number.  Should be 43210.
                sftpHostPort = db.ecarCashieringFeedConfigurations
                                    .Where(x => x.Type == "SftpCredentials"
                                            && x.Name == "SftpHostPort"
                                            && x.IsActive)
                                    .Select(x => x.Value).FirstOrDefault() ?? String.Empty;

                // SFTP Username. Should be "texas-ztrip".
                sftpUsername = db.ecarCashieringFeedConfigurations
                                    .Where(x => x.Type == "SftpCredentials"
                                            && x.Name == "SftpUsername"
                                            && x.IsActive)
                                    .Select(x => x.Value).FirstOrDefault() ?? String.Empty;

                // SFTP Password.  Should be m4Lp!dVwA3
                sftpPassword = db.ecarCashieringFeedConfigurations
                                    .Where(x => x.Type == "SftpCredentials"
                                            && x.Name == "SftpPassword"
                                            && x.IsActive)
                                    .Select(x => x.Value).FirstOrDefault() ?? String.Empty;

            }

            if (remoteRootDirectory == String.Empty || localArchiveDirectory == String.Empty || localTransportDirectory == String.Empty ||
                    sftpHostName == String.Empty || sftpUsername == String.Empty || sftpPassword == String.Empty || sftpHostPort == String.Empty)
            {
                throw new Exception("Unable to retrieve dbo.ecarCashieringFeedConfiguration configuration records.");
            }
        }






        private void CaptureExceptionAndLog(Exception ex)
        {
            logger.Info("");
            if (ex.Message != null)
            {
                logger.Error(String.Format("Message:{0}", ex.Message));
            }
            else
            {
                logger.Error(String.Format("Message:{0}", "None"));
            }

            if (ex.InnerException != null)
            {
                logger.Error("Inner Exception:{0}", ex.InnerException);
            }
            else
            {
                logger.Error(String.Format("Inner Exception:{0}", "None"));
            }

            if (ex.StackTrace != null)
            {
                logger.Error(String.Format("StackTrace:{0}", ex.StackTrace));
            }
            else
            {
                logger.Error(String.Format("StackTrace:{0}", "None"));
            }
        }

        private void SendTaxiPushCaashieringExceptionEmail(Exception ex)
        {
            try
            {
                string errorMessage = String.Empty, message = String.Empty, stackTrace = String.Empty, innerException = String.Empty;

                // Format the error message for the email body.
                if (ex.Message != null) message = ex.Message;
                if (ex.InnerException != null) innerException = ex.InnerException.ToString();
                if (ex.StackTrace != null) stackTrace = ex.StackTrace;
                errorMessage = String.Format("Message={0} InnerException={1} StackTrace={2}", message, innerException, stackTrace);

                //========================================================================================
                // We implemented 2 methods for email delivery because our internal network is blocking
                // the port number for the NoReply@zTrip.com account.  Thus we can't test locally.  
                // The first is using a gmail account and the second is UDI account.  We want to remove
                // gmail account once we have successfully tested on testing environment.
                //========================================================================================

                try
                {
                    using (SmtpClient client = new SmtpClient("smtp.gmail.com", 587))
                    {

                        // Configure the client
                        client.EnableSsl = true;
                        client.UseDefaultCredentials = false;
                        client.Credentials = new NetworkCredential("jose.unified.dispatch@gmail.com", "S0miDat@");

                        client.DeliveryMethod = SmtpDeliveryMethod.Network;
                        MailMessage emailMessage = new MailMessage(
                        "jose.unified.dispatch@gmail.com", // From field
                        "jose@unified-dispatch.com", // Recipient field
                        "Error - Taxi Push Cashiering Window Service", // Subject of the email message
                        errorMessage // Email message body
                        );

                        client.Send(emailMessage);

                    }
                }
                catch (Exception gmailEx)
                {
                    CaptureExceptionAndLog(gmailEx);
                }

                var from = System.Configuration.ConfigurationManager.AppSettings["Notification_Email_From"];
                var host = System.Configuration.ConfigurationManager.AppSettings["Notification_Email_SMTP_Host"];
                var port = System.Configuration.ConfigurationManager.AppSettings["Notification_Email_SMTP_Port"];

                MessageController messageController = new MessageController();
                SMTPEmailMessage smtpMessage = new SMTPEmailMessage();
                smtpMessage.From = from;
                smtpMessage.To = new List<string> { "jose@unified-dispatch.com" };
                smtpMessage.Subject = "Error - Taxi Push Cashiering Window Service";
                smtpMessage.MessageBody = errorMessage;
                smtpMessage.SMTP_Host = host;
                smtpMessage.SMTP_Port = port.ToInt32();
                smtpMessage.IsBodyHTML = true;

                string messageControllerErrorMessage = String.Empty;
                if (!messageController.SendSMTPEmailMessage(smtpMessage, out messageControllerErrorMessage))
                {
                    logger.Error(String.Format("Email for Taxi Push Cashiering Window Service Failed to Send. Error {0}.", messageControllerErrorMessage));
                }

            }
            catch (Exception e)
            {
                CaptureExceptionAndLog(e);
            }
        }

        private void SendECarPushCaashieringExceptionEmail(Exception ex)
        {
            try
            {
                string errorMessage = String.Empty, message = String.Empty, stackTrace = String.Empty, innerException = String.Empty;

                // Format the error message for the email body.
                if (ex.Message != null) message = ex.Message;
                if (ex.InnerException != null) innerException = ex.InnerException.ToString();
                if (ex.StackTrace != null) stackTrace = ex.StackTrace;
                errorMessage = String.Format("Message={0} InnerException={1} StackTrace={2}", message, innerException, stackTrace);

                //========================================================================================
                // We implemented 2 methods for email delivery because our internal network is blocking
                // the port number for the NoReply@zTrip.com account.  Thus we can't test locally.  
                // The first is using a gmail account and the second is UDI account.  We want to remove
                // gmail account once we have successfully tested on testing environment.
                //========================================================================================

                try
                {
                    using (SmtpClient client = new SmtpClient("smtp.gmail.com", 587))
                    {

                        // Configure the client
                        client.EnableSsl = true;
                        client.UseDefaultCredentials = false;
                        client.Credentials = new NetworkCredential("jose.unified.dispatch@gmail.com", "S0miDat@");

                        client.DeliveryMethod = SmtpDeliveryMethod.Network;
                        MailMessage emailMessage = new MailMessage(
                        "jose.unified.dispatch@gmail.com", // From field
                        "jose@unified-dispatch.com", // Recipient field
                        "Error - ECar Push Cashiering Window Service", // Subject of the email message
                        errorMessage // Email message body
                        );

                        client.Send(emailMessage);

                    }
                }
                catch (Exception gmailEx)
                {
                    CaptureExceptionAndLog(gmailEx);
                }

                var from = System.Configuration.ConfigurationManager.AppSettings["Notification_Email_From"];
                var host = System.Configuration.ConfigurationManager.AppSettings["Notification_Email_SMTP_Host"];
                var port = System.Configuration.ConfigurationManager.AppSettings["Notification_Email_SMTP_Port"];

                MessageController messageController = new MessageController();
                SMTPEmailMessage smtpMessage = new SMTPEmailMessage();
                smtpMessage.From = from;
                smtpMessage.To = new List<string> { "jose@unified-dispatch.com" };
                smtpMessage.Subject = "Error - ECar Push Cashiering Window Service";
                smtpMessage.MessageBody = errorMessage;
                smtpMessage.SMTP_Host = host;
                smtpMessage.SMTP_Port = port.ToInt32();
                smtpMessage.IsBodyHTML = true;

                string messageControllerErrorMessage = String.Empty;
                if (!messageController.SendSMTPEmailMessage(smtpMessage, out messageControllerErrorMessage))
                {
                    logger.Error(String.Format("Email for ECar Push Cashiering Window Service Failed to Send. Error {0}.", messageControllerErrorMessage));
                }

            }
            catch (Exception e)
            {
                CaptureExceptionAndLog(e);
            }
        }
        #endregion

        #region Properties
        public TrackTime TrackTime { get; set; }
		#endregion
	}
}
