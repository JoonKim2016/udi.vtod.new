﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Common.DTO;
using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.Track;
using UDI.VTOD.Common.Helper;
using UDI.Utility.Helper;
using System.Text.RegularExpressions;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Domain.VTODUtility;
namespace UDI.VTOD.Domain.VTOD
{
	internal class ValidationDomain : BaseSetting
	{

		internal void Validate_GetToken(TokenRQ input)
		{
			//var result = true;
			//error = string.Empty;

			if (input == null)
			{
				throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_NullRequest);
				//return false;
			}

			#region Check Version
			validate_Version(input.Version);
			#endregion

			//return result;
		}

		internal void Validate_OTA_GroundAvailRQ(OTA_GroundAvailRQ input)
		{
			if (input == null)
			{
				throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_NullRequest);
			}

			#region Check Version
			validate_Version(input.Version);
			#endregion

			#region RateQualifiers
			if (input.RateQualifiers == null || !input.RateQualifiers.Any() || string.IsNullOrWhiteSpace(input.RateQualifiers.First().RateQualifierValue))
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("RateQualifierValue");
			}

			var rateQualifier = input.RateQualifiers.First().RateQualifierValue;
			var rateQualifiers = Enum.GetNames(typeof(Common.DTO.Enum.FleetType)).ToList();
			if (!rateQualifiers.Contains(rateQualifier))
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("RateQualifierValue");
			}
			#endregion
		}

        internal void Validate_OTA_GroundGetFinalRouteRQ(OTA_GroundGetFinalRouteRQ input)
        {
            if (input == null)
            {
                throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_NullRequest);
            }

            #region Check Version
            validate_Version(input.Version);
            #endregion

            #region RateQualifiers
            if (input.TPA_Extensions.RateQualifiers == null || !input.TPA_Extensions.RateQualifiers.Any() || string.IsNullOrWhiteSpace(input.TPA_Extensions.RateQualifiers.First().RateQualifierValue))
            {
                throw Common.DTO.VtodException.CreateFieldRequiredValidationException("RateQualifierValue");
            }

            var rateQualifier = input.TPA_Extensions.RateQualifiers.First().RateQualifierValue;
            var rateQualifiers = Enum.GetNames(typeof(Common.DTO.Enum.FleetType)).ToList();
            if (!rateQualifiers.Contains(rateQualifier))
            {
                throw Common.DTO.VtodException.CreateFieldRequiredValidationException("RateQualifierValue");
            }
            #endregion
        }
        internal void Validate_GroundCreateAsapRequestRQ(OTA_GroundAvailRQ input)
		{
			if (input == null)
			{
				throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_NullRequest);
			}

			#region Check Version
			validate_Version(input.Version);
			#endregion

			#region RateQualifiers
			if (input.RateQualifiers == null || !input.RateQualifiers.Any() || string.IsNullOrWhiteSpace(input.RateQualifiers.First().RateQualifierValue))
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("RateQualifierValue");
			}

			var rateQualifier = input.RateQualifiers.First().RateQualifierValue;
			var rateQualifiers = Enum.GetNames(typeof(Common.DTO.Enum.FleetType)).ToList();
			if (!rateQualifiers.Contains(rateQualifier))
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("RateQualifierValue");
			}
			#endregion
		}

		internal void Validate_OTA_GroundBookRQ(TokenRS token,OTA_GroundBookRQ input)
		{
            
			if (input == null)
			{
				throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_NullRequest);
			}

			#region Check Version
			validate_Version(input.Version);
			#endregion

			if (input.GroundReservations == null || !input.GroundReservations.Any())
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("GroundReservations"); //throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_OTA_GroundReservation_Null);
			}

			if (input.GroundReservations.First().RateQualifiers == null || !input.GroundReservations.First().RateQualifiers.Any() || string.IsNullOrWhiteSpace(input.GroundReservations.First().RateQualifiers.First().RateQualifierValue))
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("RateQualifierValue");
			}
            #region Check App/Platform Version
           
            if (Validate_App_PlatformVersion(token, input.TPA_Extensions.Source,input.TPA_Extensions.Device) == false)
            {

                string _message = System.Configuration.ConfigurationManager.AppSettings["AppVersionExceptionMessage"];
                if (input.GroundReservations.FirstOrDefault().RateQualifiers != null)
                {
                    if (input.GroundReservations.FirstOrDefault().RateQualifiers.FirstOrDefault().RateQualifierValue == "ExecuCar")
                    {
                        throw Common.DTO.VtodException.CreateBubbleUpException(ExceptionType.ECar, _message);
                    }
                    else if (input.GroundReservations.FirstOrDefault().RateQualifiers.FirstOrDefault().RateQualifierValue == "Taxi")
                    {
                        throw Common.DTO.VtodException.CreateBubbleUpException(ExceptionType.Taxi, _message);
                    }
                }

            }
            #endregion


            #region Check BOBO
            //BOBO Rules :1.Cash 2.Taxi 3.NowTrip 4.No Dropoff location(as directed)
            if (input.TPA_Extensions.IsBOBO.HasValue && input.TPA_Extensions.IsBOBO.Value)
            {
                if (input.Payments.Payments.Any(x => x.Cash == null) || input.GroundReservations.First().Service.Location.Dropoff != null ||
                    input.GroundReservations.First().RateQualifiers.First().RateQualifierValue != "Taxi" ||
                    !input.TPA_Extensions.PickMeUpNow.Value )
                {
                    throw Common.DTO.VtodException.CreateFieldRequiredValidationException("BOBO Requirement");
                }
            }
            #endregion


        }
        internal void Validate_OTA_GroundModifyBookRQ(TokenRS token, OTA_GroundBookRQ input)
        {

            if (input == null)
            {
                throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_NullRequest);
            }

            #region Check Version
            validate_Version(input.Version);
            #endregion

            if (input.GroundReservations == null || !input.GroundReservations.Any())
            {
                throw Common.DTO.VtodException.CreateFieldRequiredValidationException("GroundReservations");
            }

            if (input.GroundReservations.First().RateQualifiers == null || !input.GroundReservations.First().RateQualifiers.Any() || string.IsNullOrWhiteSpace(input.GroundReservations.First().RateQualifiers.First().RateQualifierValue))
            {
                throw Common.DTO.VtodException.CreateFieldRequiredValidationException("RateQualifierValue");
            }
        }

		internal void Validate_OTA_GroundCancelRQ(OTA_GroundCancelRQ input)
		{
			if (input == null)
			{
				throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_NullRequest);
			}

			#region Check Version
			validate_Version(input.Version);
			#endregion

			if (input.Reservation == null || input.Reservation.UniqueID == null || !input.Reservation.UniqueID.Any())
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("UniqueID"); //throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_OTA_UniqueID_Null);
			}

			if (input.Reservation.UniqueID == null || !input.Reservation.UniqueID.Any())
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("UniqueID");
			}

			if (input.TPA_Extensions == null || input.TPA_Extensions.RateQualifiers == null || !input.TPA_Extensions.RateQualifiers.Any() || string.IsNullOrWhiteSpace(input.TPA_Extensions.RateQualifiers.First().RateQualifierValue))
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("RateQualifier"); // Messages.VTOD_SDSDomain_GroundCancel_UniqueID;
			}
		}

		internal void Validate_OTA_GroundResRetrieveRQ(OTA_GroundResRetrieveRQ input)
		{
			if (input == null)
			{
				throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_NullRequest);
			}

			#region Check Version
			validate_Version(input.Version);
			#endregion

			if (input.Reference == null || !input.Reference.Any())
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("Reference"); //throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_OTA_UniqueID_Null);
			}

			if (input.TPA_Extensions == null || input.TPA_Extensions.RateQualifiers == null || !input.TPA_Extensions.RateQualifiers.Any() || string.IsNullOrWhiteSpace(input.TPA_Extensions.RateQualifiers.First().RateQualifierValue))
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("RateQualifierValue");
			}
		}


		internal void Validate_OTA_GroundOriginalResRetrieve(OTA_GroundResRetrieveRQ input)
		{
			if (input == null)
			{
				throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_NullRequest);
			}

			#region Check Version
			validate_Version(input.Version);
			#endregion

			if (input.Reference == null || !input.Reference.Any())
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("Reference"); //throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_OTA_UniqueID_Null);
			}

			if (input.TPA_Extensions == null || input.TPA_Extensions.RateQualifiers == null || !input.TPA_Extensions.RateQualifiers.Any() || string.IsNullOrWhiteSpace(input.TPA_Extensions.RateQualifiers.First().RateQualifierValue))
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("RateQualifierValue");
			}
		}
		
		internal void Validate_GroundCancelFeeRQ(OTA_GroundResRetrieveRQ input)
		{
			if (input == null)
			{
				throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_NullRequest);
			}

			#region Check Version
			validate_Version(input.Version);
			#endregion

			if (input.Reference == null || !input.Reference.Any())
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("Reference"); //throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_OTA_UniqueID_Null);
			}

			if (input.TPA_Extensions == null || input.TPA_Extensions.RateQualifiers == null || !input.TPA_Extensions.RateQualifiers.Any() || string.IsNullOrWhiteSpace(input.TPA_Extensions.RateQualifiers.First().RateQualifierValue))
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("RateQualifierValue");
			}
		}

        internal void Validate_SplitPaymentInvite(SplitPaymentInviteRQ input)
        {
            if (input == null)
            {
                throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_NullRequest);
            }

            #region Check Version
            validate_Version(input.Version);
            #endregion

            if (input.MemberA == null || string.IsNullOrWhiteSpace(input.MemberA.MemberID) || input.MemberA.MemberID == "0" || !Regex.IsMatch(input.MemberA.MemberID, @"^[0-9]*$"))
            {
                throw VtodException.CreateFieldRequiredValidationException("MemberA_MemberID");
            }

            if (input.MemberB == null || input.MemberB.TelephoneInfo == null || string.IsNullOrWhiteSpace(input.MemberB.TelephoneInfo.PhoneNumber))
            {
                throw VtodException.CreateFieldRequiredValidationException("MemberB_TelephoneInfo");
            }
        }

        internal void Validate_SplitPaymentUpdateStatus(SplitPaymentUpdateStatusRQ input)
        {
            if (input == null)
            {
                throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_NullRequest);
            }

            if ((string.IsNullOrWhiteSpace(input.MemberAID) && string.IsNullOrWhiteSpace(input.MemberBID))
                || (!string.IsNullOrWhiteSpace(input.MemberAID) && !string.IsNullOrWhiteSpace(input.MemberBID))
                || (!string.IsNullOrWhiteSpace(input.MemberAID) && !Regex.IsMatch(input.MemberAID, @"^[0-9]*$"))
                || (!string.IsNullOrWhiteSpace(input.MemberBID) && !Regex.IsMatch(input.MemberBID, @"^[0-9]*$")))
            {
                throw VtodException.CreateFieldRequiredValidationException("MemberA_MemberID or MemberB_MemberID");
            }

            #region Check Version
            validate_Version(input.Version);
            #endregion
        }

        internal void Validate_SplitPaymentGetInvitations(SplitPaymentGetInvitationsRQ input)
        {
            if (input == null)
            {
                throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_NullRequest);
            }

            if (string.IsNullOrWhiteSpace(input.MemberBID))
            {
                throw VtodException.CreateFieldRequiredValidationException("MemberB_MemberID");
            }

            #region Check Version
            validate_Version(input.Version);
            #endregion
        }

        internal void Validate_SplitPaymentCheckIfEnabled(SplitPaymentCheckIfEnabledRQ input)
        {
            if (input == null)
            {
                throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_NullRequest);
            }

            #region Check Version
            validate_Version(input.Version);
            #endregion

            if (input.TripID == 0)
            {
                throw VtodException.CreateFieldRequiredValidationException("TripID");
            }
        }


        internal void Validate_SDS_Token(TokenRS input)
		{
			#region Check Version
			//THis is an internal method. no need to validate version
			//if (!validate_Version(input.Version, out error))
			//{
			//	return false;
			//}
			#endregion


			if (input == null || string.IsNullOrWhiteSpace(input.Username) || string.IsNullOrWhiteSpace(input.SecurityKey) || input.SecurityKey.ToGuid() == Guid.Empty)
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("SecurityKey");
			}
		}

		internal void Validate_GroundGetASAPRequestStatus(GroundGetASAPRequestStatusRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion
		}

		internal void Validate_GroundAbandonASAPRequest(GroundAbandonASAPRequestRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion
		}

		internal void Validate_UtilityAvailableFleet(UtilityAvailableFleetsRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion

			//if (input == null || input.Address == null || input.Address.CountryName == null || string.IsNullOrWhiteSpace(input.Address.CountryName.Code) ||
			//	input.Address.StateProv == null || string.IsNullOrWhiteSpace(input.Address.StateProv.StateCode) ||
			//	string.IsNullOrWhiteSpace(input.Address.CityName) ||
			//	string.IsNullOrWhiteSpace(input.Address.Latitude) || string.IsNullOrWhiteSpace(input.Address.Longitude))
			//{
			//	throw Common.DTO.VtodException.CreateFieldRequiredValidationException("CountryName&StateProv&CityName&Latitude&Longitude");
			//}
		}
        internal void Validate_UtilityGetLeadTime(UtilityGetLeadTimeRQ input)
        {
            #region Check Version
            validate_Version(input.Version);
            #endregion
        }
        internal void Validate_UtilityGetClientToken(UtilityGetClientTokenRQ input)
        {
            #region Check Version
            validate_Version(input.Version);
            #endregion
        }
        #region Membership

        internal void Validate_CreateMembership(TokenRS token,MemberCreateRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion
            #region Check App/Platform Version
            if (Validate_App_PlatformVersion(token, input.TPA_Extensions.Source, input.TPA_Extensions.Device) == false)
            {
                string _message = System.Configuration.ConfigurationManager.AppSettings["AppVersionExceptionMessage"];
                throw Common.DTO.VtodException.CreateBubbleUpException(ExceptionType.Membership, _message);
            }
            #endregion
		}

		internal void Validate_MemberLogin(TokenRS token,MemberLoginRQ input)
		{

			#region Check Version
			validate_Version(input.Version);
            #region Check App/Platform Version
            if (Validate_App_PlatformVersion(token, input.Source, input.Device) == false)
            {
                string _message = System.Configuration.ConfigurationManager.AppSettings["AppVersionExceptionMessage"];
                throw Common.DTO.VtodException.CreateBubbleUpException(ExceptionType.Membership, _message);
            }
            #endregion
			#endregion

		}

		internal void Validate_MemberDelete(MemberDeleteRQ input)
		{

			#region Check Version
			validate_Version(input.Version);
			#endregion

		}
		
		internal void Validate_MemberForgotPassword(MemberForgotPasswordRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion
		}

		internal void Validate_MemberGetPasswordRequest(MemberGetPasswordRequestRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion
		}

		internal void Validate_MemberResetPassword(MemberResetPasswordRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion
		}
		
		internal void Validate_MemberUpdateProfile(MemberUpdateProfileRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion

            #region Check EmailAddress
            if (input.MembershipRecord!=null)
            {
                string emailPattern = @"^(?("")("".+?(?<!\\)""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
                                     @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]*\.)+[a-zA-Z0-9][\-a-zA-Z0-9]{0,22}[a-zA-Z0-9]))$";

                if (!string.IsNullOrWhiteSpace(input.MembershipRecord.EmailAddress) && Regex.IsMatch(input.MembershipRecord.EmailAddress, emailPattern, RegexOptions.IgnorePatternWhitespace))
                {
                    logger.Info(Messages.ECar_Common_ValidReservationPassengerPrimaryEmails);
                }
                else
                {
                    logger.Warn(Messages.ECar_Common_InvalidReservationPassengerPrimaryEmails);
                    throw VtodException.CreateException(ExceptionType.Membership, 7020);
                }
            }
            #endregion
            #region Check Phone Numbers
            if (input.MembershipRecord != null)
            {
                if (input.MembershipRecord.Telephone!=null)
                {
                    if (!string.IsNullOrWhiteSpace(input.MembershipRecord.Telephone.PhoneNumber))
                    {
                        logger.Info(Messages.ECar_Common_ValidReservationPassengerPrimaryEmails);
                    }
                    else
                    {
                        logger.Warn(Messages.ECar_Common_InvalidReservationPassengerPrimaryTelephones);
                        throw VtodException.CreateException(ExceptionType.Membership, 7021);
                    }
                }
                else
                {
                    logger.Warn(Messages.ECar_Common_InvalidReservationPassengerPrimaryTelephones);
                    throw VtodException.CreateException(ExceptionType.Membership, 7021);
                }
            }
            #endregion
		}

		internal void Validate_MemberRequestEmailConfirmation(MemberRequestEmailConfirmationRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion

		}
        internal void Validate_MemberRequestEmailCancellation(MemberRequestEmailCancellationRQ input)
        {
            #region Check Version
            validate_Version(input.Version);
            #endregion

        }
		internal void Validate_MemberGetLatestUnRatedTrip(MemberGetLatestUnRatedTripRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion
		}
        internal void Validate_MemberGetLatestTrip(MemberGetLatestTripRQ input)
        {
            #region Check Version
            validate_Version(input.Version);
            #endregion
        }
		internal void Validate_MemberGetCorporateProfile(MemberGetCorporateProfileRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion
		}
        internal void Validate_MemberGetCorporateProfile2(MemberGetCorporateProfile2RQ input)
        {
            #region Check Version
            validate_Version(input.Version);
            if(string.IsNullOrWhiteSpace(input.Address.Latitude) || string.IsNullOrWhiteSpace(input.Address.Longitude))
            {
                throw Common.DTO.VtodException.CreateFieldRequiredValidationException("CountryName&StateProv&CityName&Latitude&Longitude");
            }
            #endregion
        }
		internal void Validate_MemberChangeEmailAddress(MemberChangeEmailAddressRQ input)
		{

			#region Check Version
			validate_Version(input.Version);
			#endregion
		}

		internal void Validate_MemberChangePassword(MemberChangePasswordRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion
		}

		internal void Validate_MemberGetReservation(MemberGetReservationRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion
		}

		internal void Validate_MemberLinkReservation(MemberLinkReservationRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion
		}

		internal void Validate_MemberAddLocation(MemberAddLocationRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion
		}

		internal void Validate_MemberUpdateLocation(MemberUpdateLocationRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion
		}

		internal void Validate_MemberDeleteLocation(MemberDeleteLocationRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion
		}

		internal void Validate_MemberGetLocations(MemberGetLocationsRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion
		}

        internal bool? Validate_App_PlatformVersion(TokenRS token,string source,string device)
        {
            bool? result = true;
            if (!string.IsNullOrWhiteSpace(source) && !string.IsNullOrWhiteSpace(device))
            {
                #region Check Version
                string appName = source.ToUpper().Trim();
                string platform = device.ToUpper().Trim();
				string configurationPlatform=System.Configuration.ConfigurationManager.AppSettings["Platform"];
                string[] platformFormat = configurationPlatform.Split(',');
                string platformName = string.Empty;
                string platformVersion = string.Empty;
                string app = string.Empty;
                string appVersion = string.Empty;
				VTODUtilityDomain _utility = new VTODUtilityDomain();
                if (!string.IsNullOrWhiteSpace(appName))
                {
                    if (appName.Contains("ZTRIP") == true)
                    {
                        app = appName.Substring(appName.IndexOf("ZTRIP"), 5).Trim();
                        appVersion = appName.Substring(appName.IndexOf("ZTRIP") + 5).Trim();
                        if (!string.IsNullOrWhiteSpace(platform))
                        {
                            foreach (string s in platformFormat)
                            {
                                int len = s.Length;
                                if (platform.Contains(s))
                                {
                                    platformName = platform.Substring(0, len);
                                    platformVersion = platform.Substring(len);
                                }
                            }
                        }
                        if (platformName == string.Empty)
                        {
                            result = true;
                        }
                        else
                        {
                            result = _utility.CheckAppVersion(token, app, appVersion, platformName, platformVersion);
                        }

                    }
                    else
                    {
                        result = true;
                    }

                }
                else
                {
                    result = true;
                }
                #endregion
            }
             return result;
        }
        #endregion

        #region Accounting

        internal void Validate_CreateCreditCardAccountRQ(AccountingAddMemberPaymentCardRQ input)
        {
            #region Check Version
            validate_Version(input.Version);
            #endregion

            if (input == null || input.MemberId == "0" || string.IsNullOrWhiteSpace(input.MemberId) || !System.Text.RegularExpressions.Regex.IsMatch(input.MemberId.ToString(), @"^[0-9]*$"))
            {
                throw Common.DTO.VtodException.CreateFieldRequiredValidationException("MemberId"); // (Messages.Validation_Accounting_InvalidMemberId);
            }

            if (
            input.PaymentCard == null
            //|| string.IsNullOrEmpty(input.PaymentCard.SeriesCode.PlainText)
            //|| string.IsNullOrEmpty(input.PaymentCard.Address.AddressLine)
            //|| string.IsNullOrEmpty(input.PaymentCard.Address.CityName)
            || string.IsNullOrEmpty(input.PaymentCard.CardHolderName)
            //|| string.IsNullOrEmpty(input.PaymentCard.Address.StateProv.StateCode)
            //|| string.IsNullOrEmpty(input.PaymentCard.Address.PostalCode)
            //|| string.IsNullOrEmpty(input.PaymentCard.CardType.Value)
            || string.IsNullOrEmpty(input.PaymentCard.CardNumber.PlainText)
            || string.IsNullOrEmpty(input.PaymentCard.ExpireDate)
            )
            {
                if (input.PaymentCard.TPA_Extensions != null && input.PaymentCard.TPA_Extensions.SpecialInputs == null)
                    throw Common.DTO.VtodException.CreateFieldRequiredValidationException("PaymentCard PlainText&CardHolderName&PlainText&ExpireDate");
            }

            //if (input.PaymentCard.Address.PostalCode.Length != 5 || !System.Text.RegularExpressions.Regex.IsMatch(input.PaymentCard.Address.PostalCode, @"^[0-9]*$"))
            //{
            //	error = Messages.Validation_Accounting_InvalidZipCode;
            //	return false;
            //}

            if (!System.Text.RegularExpressions.Regex.IsMatch(input.PaymentCard.CardNumber.PlainText, @"^[0-9]*$"))
            {
                throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_InvalidCreditCardNumber);
            }

            try
            {
                if (input.PaymentCard.TPA_Extensions != null && input.PaymentCard.TPA_Extensions.SpecialInputs == null)
                {
                    if (input.PaymentCard.ExpireDate.ToDateTime().Value.Year < DateTime.Now.Year)
                    {
                        throw new Exception();
                    }
                    else if (input.PaymentCard.ExpireDate.ToDateTime().Value.Month < DateTime.Now.Month && input.PaymentCard.ExpireDate.ToDateTime().Value.Year == DateTime.Now.Year)
                    {
                        throw new Exception();
                    }
                }
            }
            catch (Exception)
            {
                throw Common.DTO.VtodException.CreateException(Common.DTO.Enum.ExceptionType.Validation, 13006); // (Messages.Validation_CardExpiredDate);
            }
        }

        internal void Validate_EditCreateCreditCardAccountRQ(AccountingEditMemberPaymentCardRQ input)
        {
            #region Check Version
            validate_Version(input.Version);
            #endregion

            if (input == null || input.MemberId == 0 ||  !System.Text.RegularExpressions.Regex.IsMatch(input.MemberId.ToString(), @"^[0-9]*$"))
            {
                throw Common.DTO.VtodException.CreateFieldRequiredValidationException("MemberId"); // (Messages.Validation_Accounting_InvalidMemberId);
            }

            if (
               input.AccountId == 0 || input.AccountId == 0 || !System.Text.RegularExpressions.Regex.IsMatch(input.AccountId.ToString(), @"^[0-9]*$")
             
                || string.IsNullOrEmpty(input.CardHolderName)
                || string.IsNullOrEmpty(input.CVV) || string.IsNullOrEmpty(input.PostalCode) || string.IsNullOrEmpty(input.ExpirationDate)
                )
            {
                throw Common.DTO.VtodException.CreateFieldRequiredValidationException("AccountId&CardHolderName&CVV&ExpireDate&PostalCode");
            }

            try
            {
                if (input.ExpirationDate.ToDateTime().Value < DateTime.Now)
                    throw new Exception();
            }
            catch (Exception)
            {
                throw Common.DTO.VtodException.CreateException(Common.DTO.Enum.ExceptionType.Validation, 13006); // (Messages.Validation_CardExpiredDate);
            }
        }
		internal void Validate_GetCreditCardAccountsRQ(AccountingGetMemberPaymentCardsRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion

			if (input == null || input.MemberId == 0 || !System.Text.RegularExpressions.Regex.IsMatch(input.MemberId.ToString(), @"^[0-9]*$"))
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("MemberId"); //throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_Accounting_InvalidMemberId);
			}
		}

		internal void Validate_SetDefaultCreditCardAccountRQ(AccountingSetMemberDefaultPaymentCardRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion

			if (input == null || input.MemberId == 0 || !System.Text.RegularExpressions.Regex.IsMatch(input.MemberId.ToString(), @"^[0-9]*$"))
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("MemberId"); //throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_Accounting_InvalidMemberId);
			}

			if (input.AccountId == 0)
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("AccountId");
			}
		}

		internal void Validate_DeleteCreditCardAccountRQ(AccountingDeleteMemberPaymentCardRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion

			if (input == null || input.MemberId == 0 || !System.Text.RegularExpressions.Regex.IsMatch(input.MemberId.ToString(), @"^[0-9]*$"))
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("MemberId"); //throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_Accounting_InvalidMemberId);
			}

			if (input.AccountId == 0 && input.TPA_Extensions == null)
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("AccountId");
			}
		}

		internal void Validate_AddDirectBillAccountToCustomerProfileRQ(AccountingAddMemberDirectBillRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion

			if (input == null || input.MemberId == "0" || string.IsNullOrWhiteSpace(input.MemberId) || !System.Text.RegularExpressions.Regex.IsMatch(input.MemberId.ToString(), @"^[0-9]*$"))
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("MemberId"); //throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_Accounting_InvalidMemberId);
			}

			if (input.DirectBill == null || string.IsNullOrEmpty(input.DirectBill.BillingNumber))
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("DirectBill.BillingNumber");// Common.DTO.VtodException.CreateValidationException(Messages.Validation_Accounting_InvalidAccountNumber);
			}
		}

		internal void Validate_GetDirectBillAccountsRQ(AccountingGetMemberDirectBillsRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion

			if (input == null || input.MemberId == 0 || !System.Text.RegularExpressions.Regex.IsMatch(input.MemberId.ToString(), @"^[0-9]*$"))
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("MemberId"); //throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_Accounting_InvalidMemberId);
			}
		}

		internal void Validate_SetDefaultDirectBillAccountRQ(AccountingSetMemberDefaultDirectBillRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion

			if (input == null || input.MemberId == 0 || !System.Text.RegularExpressions.Regex.IsMatch(input.MemberId.ToString(), @"^[0-9]*$"))
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("MemberId"); //throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_Accounting_InvalidMemberId);
			}

			if (input.AccountId == 0)
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("AccountId");
			}
		}

		internal void Validate_DeleteDirectBillAccountRQ(AccountingDeleteMemberDirectBillRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion

			if (input == null || input.MemberId == "0" || string.IsNullOrWhiteSpace(input.MemberId) || !System.Text.RegularExpressions.Regex.IsMatch(input.MemberId.ToString(), @"^[0-9]*$"))
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("MemberId"); //throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_Accounting_InvalidMemberId);
			}

			if (input.AccountId == 0)
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("AccountId");
			}
		}

		internal void Validate_GetAirlineRewardsProgramsRQ(RewardGetAirlineProgramsRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion

			if (input == null)
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("MemberId"); //throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_Accounting_InvalidMemberId);
			}
		}

		internal void Validate_CreateMembershipMileageAccountRQ(RewardCreateMemberAccountRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion

			if (input == null || input.MemberID == 0 || !System.Text.RegularExpressions.Regex.IsMatch(input.MemberID.ToString(), @"^[0-9]*$"))
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("MemberId"); //throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_Accounting_InvalidMemberId);
			}

			if (string.IsNullOrEmpty(input.AccountNumber))
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("AccountNumber"); //throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_Accounting_InvalidAccountNumber);
			}

			if (!System.Text.RegularExpressions.Regex.IsMatch(input.RewardID.ToString(), @"^[0-9]*$"))
			{
				throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_InvalidRewardIds);
			}
		}

		internal void Validate_DeleteMembershipMileageAccountRQ(RewardDeleteMemberAccountRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion

			if (input == null || input.MemberID == 0 || !System.Text.RegularExpressions.Regex.IsMatch(input.MemberID.ToString(), @"^[0-9]*$"))
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("MemberId"); //throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_Accounting_InvalidMemberId);
			}

			if (input.AccountID == 0)
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("AccountId");
			}
		}
        internal void Validate_ModifyMembershipMileageAccountRQ(RewardModifyMemberAccountRQ input)
        {
            #region Check Version
            validate_Version(input.Version);
            #endregion

            if (input == null || input.MemberID == 0 || !System.Text.RegularExpressions.Regex.IsMatch(input.MemberID.ToString(), @"^[0-9]*$"))
            {
                throw Common.DTO.VtodException.CreateFieldRequiredValidationException("MemberId"); 
            }

            if (string.IsNullOrEmpty(input.AccountNumber))
            {
                throw Common.DTO.VtodException.CreateFieldRequiredValidationException("AccountNumber"); 
            }

            if (!System.Text.RegularExpressions.Regex.IsMatch(input.RewardID.ToString(), @"^[0-9]*$"))
            {
                throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_InvalidRewardIds);
            }
            if (input.AccountID == 0)
            {
                throw Common.DTO.VtodException.CreateFieldRequiredValidationException("AccountId");
            }
        }

		internal void Validate_GetMembershipAirlineMileageAccountsRQ(RewardGetMemberAccountsRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion

			if (input == null || input.MemberID == 0 || !System.Text.RegularExpressions.Regex.IsMatch(input.MemberID.ToString(), @"^[0-9]*$"))
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("MemberId"); //throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_Accounting_InvalidMemberId);
			}
		}

		internal void Validate_RewardSetDefaultAccountRQ(RewardSetDefaultAccountRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion
		}

		internal void Validate_ActivateCreditsRQ(ZTripCreditActivateMemberCreditRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion

			if (input == null || input.MemberId == 0 || !System.Text.RegularExpressions.Regex.IsMatch(input.MemberId.ToString(), @"^[0-9]*$"))
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("MemberId"); //throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_Accounting_InvalidMemberId);
			}

			if (string.IsNullOrEmpty(input.ActivationCode))
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("ActivationCode");
			}
		}

		internal void Validate_GetCreditsBalanceRQ(ZTripCreditGetMemberBalanceRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion

			if (input == null || input.MemberId == 0 || !System.Text.RegularExpressions.Regex.IsMatch(input.MemberId.ToString(), @"^[0-9]*$"))
			{
				throw Common.DTO.VtodException.CreateFieldRequiredValidationException("MemberId"); //throw Common.DTO.VtodException.CreateValidationException(Messages.Validation_Accounting_InvalidMemberId);
			}
		}

		internal void Validate_GetFareDetailRQ(GetFareDetailRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion
		}

		internal void Validate_RateSetRQ(RateSetRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion
		}

		internal void Validate_RateGetRQ(RateGetRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion
		}

		internal void Validate_UtilityGetAirlines(UtilityGetAirlinesByAirportRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion
		}

		internal void Validate_UtilityGetTripList(UtilityGetTripListRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion
		}

		internal void Validate_UtilityGetAirportInstructions(UtilityGetAirportInstructionsRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion
		}

		internal void Validate_UtilityGetHonorifics(UtilityGetHonorificsRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion
		}

		internal void Validate_GroundGetPickupTime(GroundGetPickupTimeRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion
		}

		internal void Validate_GroundGetFareScheduleWithFees(GroundGetFareScheduleWithFeesRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion
		}
		
		internal void Validate_UtilityGetBookingCriteria(UtilityGetBookingCriteriaRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion
		}

        internal void Validate_UtilityGetCorporateRequirements(UtilityGetCorporateRequirementsRQ input)
        {
            #region Check Version
            validate_Version(input.Version);
            #endregion

            if (input.Corporate == null || input.Corporate.CorporateAccount == null
                || string.IsNullOrWhiteSpace(input.Corporate.CorporateAccount.Corporation))
            {
                throw Common.DTO.VtodException.CreateFieldRequiredValidationException("Corporation");
            }

            if (string.IsNullOrWhiteSpace(input.Corporate.CorporateAccount.UserID))
            {
                throw Common.DTO.VtodException.CreateFieldRequiredValidationException("UserID");
            }
        }

		#endregion

		#region Utility
		internal void Validate_UtilityGetDestinations(UtilityGetDestinationsRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion
		}

		internal void Validate_UtilityGetAirportPickupOptions(UtilityGetAirportPickupOptionsRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion
		}
		
		internal void Validate_GetServicedAirports(UtilityGetServicedAirportsRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion
		}
		
		internal void Validate_GetCharterLocationsByType(UtilityGetLocationListRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion
		}

		internal void Validate_GetDefaultVehicleTypeForPoint(UtilityGetDefaultFleetTypeForPointRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion
		}

		internal void Validate_CharterSearchLandmark(UtilitySearchLocationRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion
		}

		internal void Validate_GetAirportName(UtilityGetAirportNameRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion
		}
		#endregion Utility

		#region Group
		internal void Validate_GroupValidateDiscountCodeRQ(GroupValidateDiscountCodeRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion
		}

		internal void Validate_GroupGetDiscountRQ(GroupGetDiscountRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion

			//if (string.IsNullOrWhiteSpace(input.AirportCode))
			//{
			//	throw Common.DTO.VtodException.CreateFieldRequiredValidationException("AirportCode");
			//}

		}

		internal void Validate_GroupGetPromotionTypeRQ(GroupGetPromotionTypeRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion
		}

		internal void Validate_GroupGetDefaultsRQ(GroupGetDefaultsRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion
		}

		internal void Validate_GroupGetLandmarksRQ(GroupGetLandmarksRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion
		}

		internal void Validate_GroupGetLocalizedServicedAirportsRQ(GroupGetLocalizedServicedAirportsRQ input)
		{
			#region Check Version
			validate_Version(input.Version);
			#endregion
		}

		#endregion

		#region private
		private void validate_Version(string version)
		{
			var configVersion = System.Configuration.ConfigurationManager.AppSettings["Version"].Trim().ToLower();

			if (string.IsNullOrWhiteSpace(version) || version.Trim().ToLower() != configVersion)
			{
				throw Common.DTO.VtodException.CreateException(Common.DTO.Enum.ExceptionType.Validation, 13004);// (Messages.Validation_Version);
			}
		}
		//private bool validate_Version(string version, out string error)
		//{
		//	var result = true;
		//	error = string.Empty;

		//	var configVersion = System.Configuration.ConfigurationManager.AppSettings["Version"].Trim().ToLower();

		//	if (string.IsNullOrWhiteSpace(version) || version.Trim().ToLower() != configVersion)
		//	{
		//		error = Messages.Validation_Version;
		//		return false;
		//	}
		//	return result;
		//}
		#endregion

	}
}
