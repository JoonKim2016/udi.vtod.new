﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Utility.Model;
using UDI.Utility.Serialization;
using UDI.VTOD.Common.DTO;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.Abstract;
using UDI.VTOD.Common.Track;
using UDI.VTOD.Common.Const;
namespace UDI.VTOD.Domain.VTODUtility
{
	public class VTODUtilityDomain : BaseSetting
	{

		public bool IsPhoneVerified(TokenRS token, string phoneNumber, int memberID)
		{
			try
			{
				logger.InfoFormat("Start-VTODUtilityDomain method : IsPhoneVerified");
				#region Init
				bool isVerified = false;
				var headers = new Dictionary<string, string>();
				var status = string.Empty;
				var contentType = string.Empty;
				var method = string.Empty;
				string response = string.Empty;
				var url = string.Empty;
				IsVerifiedRS IsPhoneVerified = null;
				var content = string.Empty;
				// string Username=string.Empty;
				//string SecurityKey = string.Empty;
				#endregion

				#region Parameters
				string type = System.Configuration.ConfigurationManager.AppSettings["PhoneVerificationType"];
                contentType = "application/json; charset=utf-8";
				method = "GET";
				url = System.Configuration.ConfigurationManager.AppSettings["PhoneVerificationUrl"];
				url = url + UDI.VTOD.Common.Const.PhoneVerificationUrl.IsVerifiedURL;
				url = url.Replace("phoneNumber", phoneNumber.TrimStart('0'));
				url = url.Replace("memberID", memberID.ToString());
				url = url.Replace("type", type);
				url = url.Replace("-", "");
				logger.InfoFormat("Inputs :PhoneNumber{0},MemberID{1},URL{2}", phoneNumber.TrimStart('0'), memberID, url);
				#endregion
				#region Add Headers
				//Username = System.Configuration.ConfigurationManager.AppSettings["Username"];
				//SecurityKey = System.Configuration.ConfigurationManager.AppSettings["SecurityKey"];
				headers.Add("Username", token.Username);
				headers.Add("SecurityKey", token.SecurityKey);
				headers.Add("Accept", "application/json");
				headers.Add("Content-Length", content.Length.ToString());
				logger.InfoFormat("Username{0},Securitykey{1}", token.Username, token.SecurityKey);
				#endregion
				#region Process Request
				response = UDI.Utility.Helper.WebRequestHelper.ProcessWebRequest(
						 url,
						 content,
						 contentType,
						 method,
						 headers,
						 out status);
				#endregion
				#region Return
				IsPhoneVerified = response.JsonDeserialize<IsVerifiedRS>();
				isVerified = (bool)IsPhoneVerified.Verified;
				logger.InfoFormat("Isverified:{0}:", isVerified);
				logger.InfoFormat("End-VTODUtilityDomain method : IsPhoneVerified");
				return isVerified;
				#endregion
			}
            catch (Exception ex)
            {
                #region Return
                logger.InfoFormat("Start Exception : IsPhoneVerified");

                 List<UDI.VTOD.Utility.Model.Error> exp = ex.Message.JsonDeserialize<List<UDI.VTOD.Utility.Model.Error>>();
                 UDI.VTOD.Common.DTO.Enum.ExceptionType exceptionType = (UDI.VTOD.Common.DTO.Enum.ExceptionType)Enum.Parse(typeof(UDI.VTOD.Common.DTO.Enum.ExceptionType), exp.FirstOrDefault().Type);
                 logger.InfoFormat("Exception Code:{0},Exception Type:{1}:", exp.FirstOrDefault().Code, exp.FirstOrDefault().Type);
                 logger.InfoFormat("End Exception : IsPhoneVerified");
                 //logger.Error(ex);
                 throw VtodException.CreateException(exceptionType, exp.FirstOrDefault().Code);
                throw ex;
                #endregion
            }
		
		}

		public void VerifyPhone(TokenRS token, string phoneNumber, int memberID, string verificationType)
		{

			logger.InfoFormat("Start-VTODUtilityDomain method : VerifyPhone");
			#region Init
			var headers = new Dictionary<string, string>();
			var status = string.Empty;
			var contentType = string.Empty;
			var method = string.Empty;
			var response = string.Empty;
			var url = string.Empty;
			var content = string.Empty;
			//string Username = string.Empty;
			//string SecurityKey = string.Empty;

			#endregion

			#region Parameters
			string type = System.Configuration.ConfigurationManager.AppSettings["PhoneVerificationType"];
			url = System.Configuration.ConfigurationManager.AppSettings["PhoneVerificationUrl"];
			url = url + UDI.VTOD.Common.Const.PhoneVerificationUrl.VerifyPhoneURL;
			contentType = "application/json; charset=utf-8";
			method = "GET";
			url = url.Replace("phoneNumber", phoneNumber.TrimStart('0'));
			url = url.Replace("memberID", memberID.ToString());
			url = url.Replace("type", type);
            url = url.Replace("VerifyType", verificationType);
            url = url.Replace("-", "");
			logger.InfoFormat("Inputs :PhoneNumber{0},MemberID{1},URL{2},type{3}:", phoneNumber.TrimStart('0'), memberID, url, type);
			#endregion

			#region Add Headers
			// Username = System.Configuration.ConfigurationManager.AppSettings["Username"];
			//SecurityKey = System.Configuration.ConfigurationManager.AppSettings["SecurityKey"];
			headers.Add("Username", token.Username);
			headers.Add("SecurityKey", token.SecurityKey);
			headers.Add("Accept", "application/json");
			headers.Add("Content-Length", content.Length.ToString());
			logger.InfoFormat("Username{0},Securitykey{1}", token.Username, token.SecurityKey);
			#endregion
			try
			{
				response = UDI.Utility.Helper.WebRequestHelper.ProcessWebRequest(
						   url,
						   content,
						   contentType,
						   method,
						   headers,
						   out status);
				logger.InfoFormat("End-VTODUtilityDomain method : VerifyPhone");
			}
			catch (Exception ex)
			{
				#region Return
				logger.InfoFormat("Start Exception : VerifyPhone");
                List<UDI.VTOD.Utility.Model.Error> exp = ex.Message.JsonDeserialize<List<UDI.VTOD.Utility.Model.Error>>();
                UDI.VTOD.Common.DTO.Enum.ExceptionType exceptionType = (UDI.VTOD.Common.DTO.Enum.ExceptionType)Enum.Parse(typeof(UDI.VTOD.Common.DTO.Enum.ExceptionType), exp.FirstOrDefault().Type);
                logger.InfoFormat("Exception Code:{0},Exception Type:{1}:", exp.FirstOrDefault().Code, exp.FirstOrDefault().Type);
				logger.InfoFormat("End Exception : VerifyPhone");
				logger.Error(ex);
                throw VtodException.CreateException(exceptionType, exp.FirstOrDefault().Code);

				#endregion
			}
		}


       
        public bool? RegistrationPhoneVerificationStatus(TokenRS token, string phoneNumber)
		{

			logger.InfoFormat("Start-VTODUtilityDomain method : RegistrationPhoneVerificationStatus");
			#region Init
			var headers = new Dictionary<string, string>();
			var status = string.Empty;
			var contentType = string.Empty;
			var method = string.Empty;
			var response = string.Empty;
			var url = string.Empty;
			var content = string.Empty;
			//string Username = string.Empty;
			//string SecurityKey = string.Empty;
            IsPhoneTakenRS IsPhoneTaken = null;
            bool IsPhoneUsed = false;
			#endregion

			#region Parameters
			url = System.Configuration.ConfigurationManager.AppSettings["PhoneVerificationUrl"];
			url = url + UDI.VTOD.Common.Const.PhoneVerificationUrl.RegistrationVerifyPhoneURL;
			contentType = "application/json; charset=utf-8";
			method = "GET";
			url = url.Replace("phoneNumber", phoneNumber.TrimStart('0'));
			url = url.Replace("-", "");
			logger.InfoFormat("Inputs :PhoneNumber{0},URL{1}:", phoneNumber.TrimStart('0'), url);
			#endregion

			#region Add Headers
			// Username = System.Configuration.ConfigurationManager.AppSettings["Username"];
			//SecurityKey = System.Configuration.ConfigurationManager.AppSettings["SecurityKey"];
			headers.Add("Username", token.Username);
			headers.Add("SecurityKey", token.SecurityKey);
			headers.Add("Accept", "application/json");
			headers.Add("Content-Length", content.Length.ToString());
			logger.InfoFormat("Username{0},Securitykey{1}", token.Username, token.SecurityKey);
			#endregion
			try
			{
				response = UDI.Utility.Helper.WebRequestHelper.ProcessWebRequest(
						   url,
						   content,
						   contentType,
						   method,
						   headers,
						   out status);
                IsPhoneTaken = response.JsonDeserialize<IsPhoneTakenRS>();
                IsPhoneUsed = (bool)IsPhoneTaken.IsPhoneTaken;
                logger.InfoFormat("IsPhoneTaken:{0}:", IsPhoneUsed);
                logger.InfoFormat("End-VTODUtilityDomain method : RegistrationPhoneVerificationStatus");
                return IsPhoneUsed;
			}
			catch (Exception ex)
			{
				#region Return
				logger.InfoFormat("Start Exception : RegistrationPhoneVerificationStatus");
                List<UDI.VTOD.Utility.Model.Error> exp = ex.Message.JsonDeserialize<List<UDI.VTOD.Utility.Model.Error>>();
				if (exp != null)
				{
                    UDI.VTOD.Common.DTO.Enum.ExceptionType exceptionType = (UDI.VTOD.Common.DTO.Enum.ExceptionType)Enum.Parse(typeof(UDI.VTOD.Common.DTO.Enum.ExceptionType), exp.FirstOrDefault().Type);
                    logger.InfoFormat("Exception Code:{0},Exception Type:{1}:", exp.FirstOrDefault().Code, exp.FirstOrDefault().Type);
					logger.InfoFormat("End Exception : RegistrationPhoneVerificationStatus");
					logger.Error(ex);
                    throw VtodException.CreateException(exceptionType, exp.FirstOrDefault().Code);
				}
				else
				{
					logger.InfoFormat("Genreal Exception like Network, Domain Issue, ...");
					logger.Error(ex);
					throw VtodException.CreateException(Common.DTO.Enum.ExceptionType.Membership, 301);

				}
				#endregion
			}
		}


        public bool? CheckAppVersion(TokenRS token, string appName,string appVersion,string platformName,string platformVersion)
        {

            logger.InfoFormat("Start-VTODUtilityDomain method : VerifyAppVersion");
            #region Init
            var headers = new Dictionary<string, string>();
            var status = string.Empty;
            var contentType = string.Empty;
            var method = string.Empty;
            var response = string.Empty;
            var url = string.Empty;
            var content = string.Empty;
			// true would be the default to prevent stopping booking service
			bool? result=true;
            IsVersionActiveRS IsVersionActive = null;
            #endregion

            #region Parameters
            url = System.Configuration.ConfigurationManager.AppSettings["VersionActiveUrl"];
            url = url + UDI.VTOD.Common.Const.PhoneVerificationUrl.VerifyVersionURL;
            contentType = "application/json; charset=utf-8";
            method = "GET";
            url = url.Replace("AppVersion", appVersion);
            if (string.IsNullOrWhiteSpace(platformVersion))
                url = url.Replace("PlatformVersion", "all");
            else
                url = url.Replace("PlatformVersion", platformVersion);
            url = url.Replace("App", appName);
            url = url.Replace("Platform", platformName);
            #endregion

            #region Add Headers
            headers.Add("Username", token.Username);
            headers.Add("SecurityKey", token.SecurityKey);
            headers.Add("Accept", "application/json");
            headers.Add("Content-Length", content.Length.ToString());
            logger.InfoFormat("Username{0},Securitykey{1}", token.Username, token.SecurityKey);
            #endregion
            try
            {
                response = UDI.Utility.Helper.WebRequestHelper.ProcessWebRequest(
                           url,
                           content,
                           contentType,
                           method,
                           headers,
                           out status);
                IsVersionActive = response.JsonDeserialize<IsVersionActiveRS>();
                logger.InfoFormat("End-VTODUtilityDomain method : VerifyAppVersion");
                if (IsVersionActive!=null)
                {
                    result = IsVersionActive.IsActive;
                }
            }
            catch (Exception ex)
            {
                #region Return
                logger.InfoFormat("Start Exception : VerifyAppVersion");
                List<UDI.VTOD.Utility.Model.Error> exp = ex.Message.JsonDeserialize<List<UDI.VTOD.Utility.Model.Error>>();
                if (exp != null)
                {
                    UDI.VTOD.Common.DTO.Enum.ExceptionType exceptionType = (UDI.VTOD.Common.DTO.Enum.ExceptionType)Enum.Parse(typeof(UDI.VTOD.Common.DTO.Enum.ExceptionType), exp.FirstOrDefault().Type);
                    logger.InfoFormat("Exception Code:{0},Exception Type:{1}:", exp.FirstOrDefault().Code, exp.FirstOrDefault().Type);
                    logger.InfoFormat("End Exception : RegistrationPhoneVerificationStatus");
                    logger.Error(ex);
                    //throw VtodException.CreateException(exceptionType, exp.FirstOrDefault().Code);
                }
                else
                {
                    logger.InfoFormat("Genreal Exception like Network, Domain Issue, ...");
                    logger.Error(ex);
                    //throw VtodException.CreateException(Common.DTO.Enum.ExceptionType.Membership, 301);

                }
                #endregion
            }
			return result;
		}

		#region Properties
		public TrackTime TrackTime { get; set; }
		#endregion
	}
}
