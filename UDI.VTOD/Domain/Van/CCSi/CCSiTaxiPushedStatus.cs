﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.Van.Class
{
    public class CCSi_VanPushedStatus
    {
        public string DispatchTripID { get; set; }
        public string DispatchFleetID { get; set; }
        public string Status { get; set; }
        public string StatusTime { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string DriverID { get; set; }
        public string DriverName { get; set; }
        public string VehicleNumber { get; set; }
        public string FareAmount { get; set; }
        public string Company { get; set; }
        public int EventID { get; set; }
        public int EventStatus { get; set; }
        public string EventDesc { get; set; }
        public string ETA { get; set; }
    }
}
