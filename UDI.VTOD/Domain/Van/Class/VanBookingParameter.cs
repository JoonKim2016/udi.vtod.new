﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.DataAccess.VTOD;
using UDI.VTOD.Domain.Van.Const;

namespace UDI.VTOD.Domain.Van.Class
{
    public class VanBookingParameter
    {
        public VanBookingParameter()
        {
            this.EnableResponseAndLog = true;
            this.PickupAddressOnlyContainsLatAndLong = false;
            this.DropOffAddressOnlyContainsLatAndLong = false;
        }

        public TokenRS tokenVTOD { set; get; }
        public OTA_GroundBookRQ request { set; get; }

        public van_fleet Fleet { set; get; }

        public long serviceAPIID { set; get; }


        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string PhoneNumber { set; get; }

        public van_fleet_user User { get; set; }
        public vtod_trip Trip { get; set; }

        public DateTime PickupDateTime { set; get; }
        public DateTime? DropOffDateTime { set; get; }

        public bool PickupAddressOnlyContainsLatAndLong { set; get; }
        public bool DropOffAddressOnlyContainsLatAndLong { set; get; }
        public string PickupAddressType { set; get; }
        public Map.DTO.Address PickupAddress { set; get; }
        public string PickupAirport { set; get; }
        public double LongitudeForPickupAirport { set; get; }
        public double LatitudeForPickupAirport { set; get; }

        public string DropOffAddressType { set; get; }
        public Map.DTO.Address DropOffAddress { set; get; }
        public string DropoffAirport { set; get; }
        public double LongitudeForDropoffAirport { set; get; }
        public double LatitudeForDropoffAirport { set; get; }

        public bool EnableResponseAndLog { set; get; }

        public int? CreditCardID { get; set; }

        public int? MemberID { get; set; }

        public int? DirectBillAccountID { get; set; }

        public string Gratuity { get; set; }
        public int PromisedETA { get; set; }

        public Common.DTO.Enum.PaymentType PaymentType { get; set; }


        public bool PickupNow { set; get; }


        public string Source { get; set; }
        public string Device { get; set; }
        public string Ref { get; set; }

        public int FleetTripCode;

        public string EmailAddress { get; set; }

        public bool IsFixedPrice { set; get; }
        public double FixedPrice { set; get; }

    }
}
