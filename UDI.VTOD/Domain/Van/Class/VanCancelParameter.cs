﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.DataAccess.VTOD;

namespace UDI.VTOD.Domain.Van.Class
{
    public class VanCancelParameter
    {
        public VanCancelParameter()
		{
			this.EnableResponseAndLog = true;
		}

        public TokenRS tokenVTOD { set; get; }
        public OTA_GroundCancelRQ request { set; get; }

        public van_fleet Fleet { set; get; }

		public long serviceAPIID { set; get; }
        public van_fleet_user User { get; set; }
        public vtod_trip Trip { get; set; }

		public bool EnableResponseAndLog { set; get; }
    }
}
