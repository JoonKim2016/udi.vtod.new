﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.DataAccess.VTOD;
using UDI.VTOD.Domain.Taxi.Const;

namespace UDI.VTOD.Domain.Van.Class
{
    public class VanGetEstimationParameter
    {

        public VanGetEstimationParameter()
		{
			this.EnableResponseAndLog = true;
		}

        //common
		public string EstimationType { set; get; }
        public TokenRS tokenVTOD { set; get; }
        public OTA_GroundAvailRQ request { set; get; }
        
        //fleet
        public van_fleet Fleet { set; get; }

		public long serviceAPIID { set; get; }
        //Address or airportInfo
		public bool PickupAddressOnlyContainsLatAndLong { set; get; }
		public bool DropOffAddressOnlyContainsLatAndLong { set; get; }
        public string PickupAddressType { set; get; }
        public Map.DTO.Address PickupAddress { set; get; }
		public string PickupAirport { set; get; }
		//public decimal? PickupAirportLongitude { set; get; }
		//public decimal? PickupAirportLatitude { set; get; }



		public double LongitudeForPickupAirport { set; get; }
		public double LatitudeForPickupAirport { set; get; }

		


		public string DropOffAddressType { set; get; }
		public Map.DTO.Address DropoffAddress { set; get; }
		public string DropoffAirport { set; get; }
		//public decimal? DropoffAirportLongitude { set; get; }
		//public decimal? DropoffAirportLatitude { set; get; }

		public double LongitudeForDropoffAirport { set; get; }
		public double LatitudeForDropoffAirport { set; get; }

        //Zone
        public van_fleet_zone PickupZone { set; get; }
        public van_fleet_zone DropoffZone { set; get; }

        //public string EstimationResultType { set; get; }

        //response
        public van_fleet_rate Info_Rate { set; get; }
        public List<VanFlatRateData> Info_FlatRates { set; get; }
        public RateData RateEstimation { set; get; }
        public FlatRateData FlatRateEstimation { set; get; }

        //distance
        public decimal Distance_Mile { set; get; }
        public decimal Distance_Kilometer { set; get; }

		public bool EnableResponseAndLog { set; get; }
    }
}
