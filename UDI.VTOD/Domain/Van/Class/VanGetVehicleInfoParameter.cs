﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.DataAccess.VTOD;

namespace UDI.VTOD.Domain.Van.Class
{
    public class VanGetVehicleInfoParameter
    {
        public VanGetVehicleInfoParameter()
		{
			this.EnableResponseAndLog = true;
		}

        public TokenRS tokenVTOD { set; get; }
        public OTA_GroundAvailRQ request { set; get; }

        public string PickupAddressType { set; get; }
        public Map.DTO.Address PickupAddress { set; get; }
        public string PickupLandmark { set; get; }

		public List<Vehicle> VehicleStausList = new List<Vehicle>();
        public double Longtitude { set; get; }
        public double Latitude { set; get; }
        public string PolygonType { set; get; } //Circle|Retengle

        //Circle
        public double Radius { set; get; } 
        

        public van_fleet Fleet { set; get; }

		public long serviceAPIID { set; get; }

		public long? extendedServiceAPIID { set; get; }
		
		public string APIInformation { set; get; }

		public bool EnableResponseAndLog { set; get; }

        public string VehicleStatus { set; get; }
    }

    
}
