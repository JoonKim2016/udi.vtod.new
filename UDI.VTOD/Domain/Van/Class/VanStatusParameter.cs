﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.DataAccess.VTOD;

namespace UDI.VTOD.Domain.Van.Class
{
    public class VanStatusParameter
    {
		public VanStatusParameter()
		{
			this.EnableResponseAndLog = true;
		}

        public TokenRS tokenVTOD { set; get; }
        public OTA_GroundResRetrieveRQ request { set; get; }

        public van_fleet Fleet { set; get; }

		public long serviceAPIID { set; get; }
        public van_fleet_user Fleet_User { get; set; }
        public vtod_trip Trip { get; set; }
        public van_trip_status lastTripStatus {set;get;}
		public bool EnableResponseAndLog { set; get; }

		public my_aspnet_users User { set; get; }
    }
}
