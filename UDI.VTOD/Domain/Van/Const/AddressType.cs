﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.Van.Const
{
    public class AddressType
    {
		public const string Empty = "Empty";
		public const string Airport = "Airport";
		public const string Address = "Address";
	}
}
