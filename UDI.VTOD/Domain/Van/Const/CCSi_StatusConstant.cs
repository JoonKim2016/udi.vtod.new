﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.Van.Const
{
    public class CCSi_StatusConstant
    {
        public const string GFC_UPDATE_MODULE = "update_ride";					//The module that allows us to update a trip.
        public const string GFC_DATE_FORMAT = "yyyy-MM-ddTHH:mm:ss";			//The date/time format that they accept.
        public const string GFC_PARAM_TRIP_NBR = "provider_ride_id";			//Parameter when supplying them with our trip number.
        public const string TRIP_NBR = "trip_nbr";			//Parameter when supplying them with our trip number.
        public const string GFC_PARAM_MADS_NBR = "mads_ride_id";				//Parameter when supplying them with the mads call number.
        public const string GFC_PARAM_API_KEY = "api_key";						//Parameter when supplying them with our api key.
        public const string FLEET_ID = "fleet_id";						//Parameter when supplying them with our api key.
        public const string GFC_PARAM_STATUS = "event";						//Parameter when supplying them with our trip status field.
        public const string GFC_PARAM_STATUS_REASON = "status_reason";			//Parameter when supplying them with the reason for CANCEL status.
        public const string GFC_PARAM_TIME = "time";							//Parameter when supplying them with our event date/time
        public const string DATE_TIME = "date_time";							//Parameter when supplying them with our event date/time
        public const string GFC_PARAM_LAT = "latitude";						//Parameter when supplying them with our event lat
        public const string GFC_PARAM_LON = "longitude";						//Parameter when supplying them with our event lon
        public const string LAT = "lat";						//Parameter when supplying them with our event lat
        public const string LON = "lon";						//Parameter when supplying them with our event lon
        public const string GFC_PARAM_DRIVER_ID = "driver_id";					//Parameter when supplying them with our event driver id.
        public const string GFC_PARAM_DRIVER = "driver_name";					//Parameter when supplying them with our event driver information.
        public const string GFC_PARAM_VEHICLE = "vehicle_number";				//Parameter when supplying them with our event vehicle information.
        public const string VEHICLE_NBR = "vehicle_nbr";				//Parameter when supplying them with our event vehicle information.
        public const string GFC_PARAM_FARE = "fare";							//parameter when supplying them with the fare information
        public const string GFC_PARAM_COMPANY = "company";						//Parameter when supplying them with the company name (fleet name) information.
        public const string GFC_PARAM_GFC = "GFCtrip";							//Tells GFC if this event is for a trip entered through the GFC system or not.
        public const string EVENT_ID = "type";
        public const string EVENT_STATUS = "status";
        public const string EVENT_DESC = "desc";
        public const string Info1 = "info1";
    }
}
