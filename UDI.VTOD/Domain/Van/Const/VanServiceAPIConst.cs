﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.Van.Const
{
    public class VanServiceAPIConst
    {
        public const long None = 0;
        public const long CCSi = 1;
        public const long UDI33 = 2;
		public const long SDS = 3;
		public const long MTData = 4;
	}
}
