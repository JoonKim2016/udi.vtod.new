﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDI.VTOD.Domain.Van.Const
{
	public class VanServiceAPIPreferenceConst
	{
		public const string Book = "Book";
		public const string Status = "Status";
		public const string Cancel = "Cancel";
        public const string GetVehicleInfo = "GetVehicleInfo";
		public const string GetEstimation = "GetEstimation";
		public const string GetAvailableFleets = "GetAvailableFleets";
        public const string NotifyDriver = "NotifyDriver";
        public const string CalculateVehicleMovement = "CalculateVehicleMovement";
	}
}
