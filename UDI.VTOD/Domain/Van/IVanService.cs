﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.Track;
using UDI.VTOD.DataAccess.VTOD;
using UDI.VTOD.Domain.Van.Class;

namespace UDI.VTOD.Domain.Van
{
    interface IVanService
    {
        OTA_GroundBookRS Book(VanBookingParameter tbp, int fleetTripCode);

        OTA_GroundResRetrieveRS Status(VanStatusParameter tsp);

        OTA_GroundCancelRS Cancel(VanCancelParameter tcp);

        OTA_GroundAvailRS GetVehicleInfo(VanGetVehicleInfoParameter tgvip);

        bool NotifyDriver(VanSendMessageToVehicleParameter tsmtvp);

        Common.DTO.OTA.Fleet GetAvailableFleets(TokenRS token, UtilityAvailableFleetsRQ input);


        TrackTime TrackTime { get; set; }

        #region New Service API interface
        OTA_GroundBookRS Book(TokenRS tokenVTOD, OTA_GroundBookRQ request, out List<Tuple<string, int, long>> disptach_Rez_VTOD, int fleetTripCode, van_fleet fleet);

        OTA_GroundResRetrieveRS Status(TokenRS tokenVTOD, OTA_GroundResRetrieveRQ request, string type);

        OTA_GroundCancelRS Cancel(TokenRS tokenVTOD, OTA_GroundCancelRQ request);

        OTA_GroundAvailRS GetVehicleInfo(TokenRS tokenVTOD, OTA_GroundAvailRQ request);

        bool NotifyDriver(TokenRS token, UtilityNotifyDriverRQ input);

        bool NotifyPickupDriver(TokenRS token, UtilityNotifyDriverRQ input);

        OTA_GroundAvailRS GetEstimation(TokenRS tokenVTOD, OTA_GroundAvailRQ request);

        #endregion
    }
}
