﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;
using UDI.Map;
using UDI.SDS.ServiceAdapter;
using UDI.VTOD.Common.DTO;
using UDI.VTOD.Common.DTO.Enum;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Common.Helper;
using UDI.VTOD.DataAccess.VTOD;
using UDI.VTOD.Domain.Van.Class;
using UDI.VTOD.Domain.Van.Const;
using UDI.Utility.Helper;
using UDI.SDS.DTO.Enum;
using UDI.VTOD.Domain.Utility;
using UDI.VTOD.Common.Track;
using System.Transactions;
using System.Data;
using UDI.Utility.Serialization;
using System.Threading;
using System.Drawing;
using System.IO;
using System.Security.Policy;
using UDI.VTOD.Domain.VTOD;
using UDI.SDS.PaymentsService;
using UDI.VTOD.MTData.BookingWebService;
using Address = UDI.VTOD.Common.DTO.OTA.Address;
using AddressType = UDI.VTOD.Domain.Van.Const.AddressType;
using System.IO.Compression;

namespace UDI.VTOD.Domain.Van
{
        public class VanUtility
        {
            private ILog logger;

            private string VTODAPIVersion = string.Empty;

            public VanUtility(ILog log)
            {
                this.logger = log;
                VTODAPIVersion = ConfigurationManager.AppSettings["Version"];
            }

            #region Public method
            public OTA_GroundAvailRS GetRestimationResponse(VanGetEstimationParameter tgep)
            {
                OTA_GroundAvailRS response = new OTA_GroundAvailRS();

                #region [Van 5.1] Response OTA_GroundAvailRS
                //common
                response.Success = new Success();
                response.GroundServices = new GroundServiceList();
                response.GroundServices.GroundServices = new List<GroundService>();

                //Fare policy
                #region 01. Rate
                GroundService fpRate = new GroundService();
                fpRate.RateQualifier = new RateQualifier { Category = new Category { Code = FareType.FarePolicy.ToString(), Value = "Other_" } };
                fpRate.RateQualifier.SpecialInputs = new List<NameValue>();
                fpRate.ServiceCharges = new List<ServiceCharges>();
                ServiceCharges sc_Rate_Init = new ServiceCharges { ChargePurpose = new ChargePurpose { Code = FareDetail.FlagDrop.ToString(), Value = "Other_" }, CurrencyCode = tgep.Info_Rate.AmountUnit, /* Description = tgep.Info_Rate.DistanceUnit, */ Amount = tgep.Info_Rate.InitialAmount.ToString() };
                ServiceCharges sc_Rate_PerDistance = new ServiceCharges { ChargePurpose = new ChargePurpose { Code = FareDetail.PerDistance.ToString(), Value = "Other_" }, CurrencyCode = tgep.Info_Rate.AmountUnit, Description = tgep.Info_Rate.DistanceUnit, Amount = tgep.Info_Rate.PerDistanceAmount.ToString() };
                fpRate.ServiceCharges.Add(sc_Rate_Init);
                fpRate.ServiceCharges.Add(sc_Rate_PerDistance);
                response.GroundServices.GroundServices.Add(fpRate);
                #endregion

                #region 02. Flatrate part policy (don't remove this)
                //We turn this policy part off now. 
                //This is a part to verify the price policy 
                /*
                if (tgep.Info_FlatRates != null && tgep.Info_FlatRates.Any())
                    foreach (vanFlatRateData tfrd in tgep.Info_FlatRates)
                    {
                        GroundService fpFlatRate = new GroundService();
                        fpFlatRate.RateQualifier = new RateQualifier { Category = new Category { Code = FareType.FarePolicy.ToString(), Value = "Other_" } };
                        //fpFlatRate.RateQualifier.SpecialInputs = new List<KeyValuePair<string, string>>();
                        fpFlatRate.RateQualifier.SpecialInputs = new List<NameValue>();
                        //KeyValuePair<string, string> kvpTypeFlatRate = new KeyValuePair<string, string>("Type", "FlatRate");
                        //KeyValuePair<string, string> kvpTypePickup = new KeyValuePair<string, string>("Pickup", tfrd.PickupZoneName);
                        //KeyValuePair<string, string> kvpTypeDropoff = new KeyValuePair<string, string>("DropOff", tfrd.DropOffZoneName);
                        //NameValue kvpTypeFlatRate = new NameValue { Name = "Type", Value = FareType.FlatRate.ToString() };
                        NameValue kvpTypePickup = new NameValue { Name = "Pickup", Value = tfrd.PickupZoneName };
                        NameValue kvpTypeDropoff = new NameValue { Name = "DropOff", Value = tfrd.DropOffZoneName };

                        //fpFlatRate.RateQualifier.SpecialInputs.Add(kvpTypeFlatRate);
                        fpFlatRate.RateQualifier.SpecialInputs.Add(kvpTypePickup);
                        fpFlatRate.RateQualifier.SpecialInputs.Add(kvpTypeDropoff);
                        fpFlatRate.ServiceCharges = new List<ServiceCharges>();
                        ServiceCharges sc_FlatRate_PerDistance = new ServiceCharges { ChargePurpose = new ChargePurpose { Code = FareType.FlatRate.ToString(), Value = "Other_" }, CurrencyCode = tfrd.AmountUnit, Amount = decimal.Parse(tfrd.Amount.ToString()) };
                        fpFlatRate.ServiceCharges.Add(sc_FlatRate_PerDistance);
                        response.GroundServices.GroundServices.Add(fpFlatRate);
                    }
                */
                #endregion

                if (tgep.EstimationType == EstimationResultType.Info_FlatRate)
                {

                    #region 03. Estimation(FlatRate)
                    GroundService fpFlatRateEstimation = new GroundService();
                    fpFlatRateEstimation.RateQualifier = new RateQualifier { Category = new Category { Code = FareType.FlatRate.ToString(), Value = "Other_" } };
                    //fpFlatRateEstimation.RateQualifier.SpecialInputs = new List<KeyValuePair<string, string>>();
                    fpFlatRateEstimation.RateQualifier.SpecialInputs = new List<NameValue>();
                    //KeyValuePair<string, string> kvpTypeFlatRateEstimationPickup = new KeyValuePair<string, string>("Pickup", tgep.FlatRateestimation.PickupZone);
                    //KeyValuePair<string, string> kvpTypeFlatRateEstimationDropoff = new KeyValuePair<string, string>("DropOff", tgep.FlatRateestimation.DropoffZone);

                    if ((tgep.FlatRateEstimation != null))
                    {
                        NameValue kvpTypeFlatRateEstimationPickup = new NameValue { Name = "Pickup", Value = tgep.FlatRateEstimation.PickupZone };
                        NameValue kvpTypeFlatRateEstimationDropoff = new NameValue { Name = "DropOff", Value = tgep.FlatRateEstimation.DropoffZone };
                        fpFlatRateEstimation.RateQualifier.SpecialInputs.Add(kvpTypeFlatRateEstimationPickup);
                        fpFlatRateEstimation.RateQualifier.SpecialInputs.Add(kvpTypeFlatRateEstimationDropoff);
                        fpFlatRateEstimation.TotalCharge = new TotalCharge
                        {
                            EstimatedTotalAmount = decimal.Parse(tgep.FlatRateEstimation.EstimationTotalAmount.ToString()),
                            RateTotalAmount = decimal.Parse(tgep.FlatRateEstimation.EstimationTotalAmount.ToString())
                        };
                        response.GroundServices.GroundServices.Add(fpFlatRateEstimation);
                    }
                    else
                    {
                        logger.InfoFormat("No FlatRate estimation");
                        //throw new ValidationException(Messages.van_GetEstimationFailure);
                    }
                    #endregion
                }
                else if (tgep.EstimationType == EstimationResultType.Info_Rate)
                {
                    #region 04. Estimation(Rate)
                    GroundService fpRateEstimation = new GroundService();
                    fpRateEstimation.RateQualifier = new RateQualifier { Category = new Category { Code = FareType.Estimation.ToString(), Value = "Other_" } };
                    fpRateEstimation.RateQualifier.SpecialInputs = new List<NameValue>();
                    NameValue kvpTypeFPDistanceUnit = new NameValue { Name = "DistanceUnit", Value = tgep.RateEstimation.DistanceUnit };

                    NameValue kvpTypeFPDistance;
                    if (tgep.RateEstimation.DistanceUnit == Map.DTO.DistanceUnit.Mile.ToString())
                    {
                        kvpTypeFPDistance = new NameValue { Name = "Distance", Value = tgep.Distance_Mile.ToString() };
                        fpRateEstimation.RateQualifier.SpecialInputs.Add(kvpTypeFPDistance);
                    }
                    else if (tgep.RateEstimation.DistanceUnit == Map.DTO.DistanceUnit.Kilometer.ToString())
                    {
                        kvpTypeFPDistance = new NameValue { Name = "Distance", Value = tgep.Distance_Kilometer.ToString() };
                        fpRateEstimation.RateQualifier.SpecialInputs.Add(kvpTypeFPDistance);
                    }
                    fpRateEstimation.RateQualifier.SpecialInputs.Add(kvpTypeFPDistanceUnit);
                    fpRateEstimation.TotalCharge = new TotalCharge
                    {
                        EstimatedTotalAmount = decimal.Parse(tgep.RateEstimation.EstimationTotalAmount.ToString()),
                        RateTotalAmount = decimal.Parse(tgep.RateEstimation.EstimationTotalAmount.ToString())

                    };
                    response.GroundServices.GroundServices.Add(fpRateEstimation);
                    #endregion
                }


                #endregion

                return response;
            }

            public bool DetectFastMeterByMinFare(long tripId, string status, string fareString, decimal MinPriceFastMeter)
            {
                bool isFastMeter = false;
                List<string> fastMeterStatus = ConfigurationManager.AppSettings["Van_DetectFastMeterStatus"].Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();
                if (fastMeterStatus.Where(x => x.ToLowerInvariant().Equals(status.ToLowerInvariant())).Any())
                {
                    decimal fareAmount = 0;
                    if (decimal.TryParse(fareString, out fareAmount))
                    {
                        if (fareAmount <= MinPriceFastMeter && fareAmount > 0)
                        {
                            isFastMeter = true;
                        }
                        else
                        {
                            isFastMeter = false;
                        }
                    }
                    else if (CheckFastMeterPreviousTripStatus(tripId))
                    {
                        isFastMeter = true;
                    }
                    else
                    {
                        isFastMeter = false;
                    }
                }
                else
                {
                    isFastMeter = false;
                }

                logger.InfoFormat("Is fastmeter:{0}", isFastMeter);
                return isFastMeter;

            }

            public string ConvertTripStatusMessageForFrontEndDevice(int userID, string statusMessage)
            {
                string result = "";
                using (VTODEntities context = new VTODEntities())
                {
                    result = context.van_status_frontendwrapper.Where(x => x.UserId == userID && x.VTODVanStatus == statusMessage).Select(x => x.FrontEndStatus).FirstOrDefault();
                    logger.InfoFormat("Convet trip status for frontend app. userID={0}, vtod_status={1}, frontendStatus={2}", userID, statusMessage, result);
                }
                return result;
            }

            public List<string> GetPreviousTripStatus(long tripId)
            {
                List<string> statusList = new List<string>();

                using (VTODEntities context = new VTODEntities())
                {
                    statusList = context.van_trip_status.Where(x => x.TripID == tripId).Select(x => x.Status).Distinct().ToList();

                }

                return statusList;
            }
            public string CompressString(string text)
            {
                byte[] buffer = Encoding.UTF8.GetBytes(text);
                var memoryStream = new MemoryStream();
                using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Compress, true))
                {
                    gZipStream.Write(buffer, 0, buffer.Length);
                }

                memoryStream.Position = 0;

                var compressedData = new byte[memoryStream.Length];
                memoryStream.Read(compressedData, 0, compressedData.Length);

                var gZipBuffer = new byte[compressedData.Length + 4];
                Buffer.BlockCopy(compressedData, 0, gZipBuffer, 4, compressedData.Length);
                Buffer.BlockCopy(BitConverter.GetBytes(buffer.Length), 0, gZipBuffer, 0, 4);
                return Convert.ToBase64String(gZipBuffer);
            }
            public string Data_Hex_Asc(string Data)
            {
                string hex = "";
                foreach (char c in Data)
                {
                    int tmp = c;
                    hex += String.Format("{0:x2}", (uint)System.Convert.ToUInt32(tmp.ToString()));
                }
                return hex;
            }

            public int GetMinutesAway(int ETA)
            {
                int minutesAway = 0;
                TimeSpan duration = DateTime.UtcNow.AddMinutes(ETA) - DateTime.UtcNow;
                minutesAway = (int)System.Math.Abs(duration.TotalMinutes);
                return minutesAway;
            }

            public string ExtractDigits(string input)
            {
                return Regex.Replace(input, @"[^\d]+", "");
                //return String.Join("", input.ToCharArray().Where(c => c >= '0' && c <= '9').Select(c => c.ToString()).ToArray());
            }

            public OTA_GroundAvailRS GetEstimation(VanGetEstimationParameter tgep)
            {
                throw new NotImplementedException();
            }

            #region Validation



            public long GetServiceAPIIdByRequest(UtilityNotifyDriverRQ request)
            {
                long serviceAPIID = -1;

                using (VTODEntities context = new VTODEntities())
                {
                    var vanTrip = context.van_trip.FirstOrDefault(m => m.Id == request.TripID);
                    if (vanTrip != null)
                    {
                        try
                        {
                            serviceAPIID = GetServiceAPIId(vanTrip.FleetId, VanServiceAPIPreferenceConst.NotifyDriver);
                        }
                        catch (Exception)
                        {
                        }
                    }

                    if (serviceAPIID == -1)
                    {
                        //enforce to use UDI33
                        serviceAPIID = VanServiceAPIConst.CCSi;
                    }
                }

                if (serviceAPIID == -1)
                {
                    var vtodException = VtodException.CreateException(ExceptionType.Van, 1011);
                    logger.Warn(vtodException.ExceptionMessage.Message);
                    throw vtodException;
                }

                return serviceAPIID;
            }

            public long GetServiceAPIIdByRequest(OTA_GroundAvailRQ request, string action, out van_fleet fleet)
            {
                long serviceAPIID = -1;
                bool result = true;
                string PickupAddressType = "";
                Map.DTO.Address PickupAddress = new Map.DTO.Address();

                #region Validate Pickup Address
                if (result)
                {
                    Pickup_Dropoff_Stop stop = request.Service.Pickup;

                    if ((stop.Address != null) && (stop.AirportInfo == null))
                    {
                        if (!string.IsNullOrWhiteSpace(stop.Address.CountryName.Code) && !string.IsNullOrWhiteSpace(stop.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(stop.Address.CityName) && !string.IsNullOrWhiteSpace(stop.Address.PostalCode))
                        {
                            Map.DTO.Address pickupAddress = new Map.DTO.Address { Country = stop.Address.CountryName.Code, State = stop.Address.StateProv.StateCode, City = stop.Address.CityName, ZipCode = stop.Address.PostalCode, Geolocation = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(stop.Address.Latitude), Longitude = Convert.ToDecimal(stop.Address.Longitude) } };
                            PickupAddressType = UDI.VTOD.Domain.Van.Const.AddressType.Address;
                            PickupAddress = pickupAddress;
                        }
                        else if (!string.IsNullOrWhiteSpace(stop.Address.CountryName.Code) && !string.IsNullOrWhiteSpace(stop.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(stop.Address.CityName))
                        {
                            Map.DTO.Address pickupAddress = new Map.DTO.Address { Country = stop.Address.CountryName.Code, State = stop.Address.StateProv.StateCode, City = stop.Address.CityName, Geolocation = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(stop.Address.Latitude), Longitude = Convert.ToDecimal(stop.Address.Longitude) } };
                            PickupAddressType = UDI.VTOD.Domain.Van.Const.AddressType.Address;
                            PickupAddress = pickupAddress;

                        }
                        else if (!string.IsNullOrWhiteSpace(stop.Address.LocationName))
                        {
                            #region Not support now
                            //tgvip.PickupAddressType = UDI.VTOD.Domain.Van.Const.AddressType.LocationName;
                            //tgvip.PickupLandmark = stop.Address.LocationName;
                            #endregion

                            result = false;
                            logger.Warn("Cannot use locationName from address.");
                        }
                        else
                        {
                            result = false;
                            logger.Warn("Cannot find any address/locationName from address.");
                        }
                    }
                    else if ((stop.Address == null) && (stop.AirportInfo != null))
                    {
                        if ((stop.AirportInfo.Arrival != null) && (stop.AirportInfo.Departure == null))
                        {
                            #region Not support now
                            //tgvip.PickupAddressType = UDI.VTOD.Domain.van.Const.AddressType.AirportInfo;
                            //tgvip.PickupLandmark = stop.AirportInfo.Arrival.LocationCode;
                            #endregion

                            result = false;
                            logger.Warn("Cannot use aiportinfo for address");
                        }
                        else if ((stop.AirportInfo.Arrival == null) && (stop.AirportInfo.Departure != null))
                        {
                            #region Not support now
                            //tgvip.PickupAddressType = UDI.VTOD.Domain.van.Const.AddressType.AirportInfo;
                            //tgvip.PickupLandmark = stop.AirportInfo.Departure.LocationCode;
                            #endregion

                            result = false;
                            logger.Warn("Cannot use aiportinfo for address");
                        }
                        else if ((stop.AirportInfo.Arrival != null) && (stop.AirportInfo.Departure != null))
                        {
                            result = false;
                            logger.Warn("This OTA request contains both AirportInfo.Arrival and AirportInfo.Departure");
                        }
                        else
                        {
                            result = false;
                            logger.Warn("Cannot find any arrival aor depature from AirportInfo");
                        }
                    }
                    else
                    {
                        //find address by longtitude and latitude
                        double Longtitude = 0;
                        double Latitude = 0;
                        Map.DTO.Address pickupAddress2 = null;

                        if (!LookupAddress(Longtitude, Latitude, out pickupAddress2))
                        {
                            logger.WarnFormat("Cannot find longtitude={0} and latitude={1}", Longtitude, Latitude);
                            result = false;
                        }
                        PickupAddressType = UDI.VTOD.Domain.Van.Const.AddressType.Address;
                        PickupAddress = pickupAddress2;
                    }

                }
                else
                {
                    result = false;
                }
                #endregion

                #region Validate Fleet
                fleet = null;
                if (result)
                {
                    if (GetFleet(PickupAddressType, PickupAddress, string.Empty, out fleet))
                    {
                        logger.InfoFormat("Fleet found. ID={0}", fleet.Id);
                    }
                    else
                    {
                        result = false;
                        logger.Warn("Unable to find fleet by this address or landmark.");
                    }
                }
                else
                {
                    result = false;
                    logger.Warn("Unable to find fleet by invalid address or landmark.");
                }
                #endregion

                if (result)
                {
                    serviceAPIID = GetServiceAPIId(fleet.Id, action);
                    if (serviceAPIID == -1)
                    {
                        var vtodException = VtodException.CreateException(ExceptionType.Van, 1011);
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException;// new ValidationException(Messages.Van_Common_UnableToFindFleetByAddressOrAirportInfo);
                    }
                }

                return serviceAPIID;
            }

            public long GetServiceAPIIdByRequest(OTA_GroundCancelRQ request)
            {
                long serviceAPIID = -1;
                bool result = true;

                #region Validate Unique ID
                string uid = request.Reservation.UniqueID.FirstOrDefault().ID;
                if (request.Reservation.UniqueID.Any())
                {
                    if (!string.IsNullOrWhiteSpace(uid))
                    {
                        logger.Info("Request is valid.");
                    }
                    else
                    {
                        result = false;
                        logger.Info("request.Reservation.UniqueID.Frist().ID is empty.");
                    }
                }
                else
                {
                    result = false;
                    logger.Info("Reservation.UniqueID is empty.");
                }
                #endregion

                #region Validate Trip
                vtod_trip t = null;
                if (result)
                {
                    t = GetVanTrip(uid);
                    if (t != null)
                    {
                        logger.InfoFormat("Trip found. tripID={0}", uid);
                    }
                    else
                    {
                        result = false;
                        logger.WarnFormat("Cannot find this trip. tripID={0}", uid);
                    }
                }
                else
                {
                    result = false;
                    logger.Warn("Cannot find trip by invalid referenceId");
                }
                #endregion

                #region Validate Fleet
                van_fleet fleet = null;
                if (result)
                {
                    if (GetFleet(t.van_trip.FleetId, out fleet))
                    {
                        logger.InfoFormat("Fleet found. ID={0}", fleet.Id);
                    }
                    else
                    {
                        result = false;
                        logger.Warn("Unable to find fleet by this fleetID");
                    }
                }
                else
                {
                    result = false;
                    logger.Warn("Unable to find fleet by this fleetID");
                }
                #endregion

                if (result)
                {
                    serviceAPIID = GetServiceAPIId(fleet.Id, VanServiceAPIPreferenceConst.Cancel);
                    if (serviceAPIID == -1)
                    {
                        var vtodException = VtodException.CreateException(ExceptionType.Van, 1011);
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException;// new ValidationException(Messages.Van_Common_UnableToFindFleetByAddressOrAirportInfo);
                    }
                }

                return serviceAPIID;
            }

            public long GetServiceAPIIdByRequest(OTA_GroundResRetrieveRQ request)
            {
                long serviceAPIID = -1;
                long fleetId = -1;
                bool result = true;

                #region Validate Reference Id
                if (request.Reference == null || !request.Reference.Any() || string.IsNullOrWhiteSpace(request.Reference.First().ID))
                {
                    result = false;
                    logger.Warn("Reference.ID is empty.");
                }
                else
                {
                    logger.Info("Request is valid.");
                }
                #endregion

                #region Validate Trip
                vtod_trip t;
                if (result)
                {
                    t = GetVanTrip(request.Reference.First().ID);
                    if (t != null)
                    {
                        fleetId = t.van_trip.FleetId;
                        logger.InfoFormat("Trip found. tripID={0}", request.Reference.First().ID);
                    }
                    else
                    {
                        result = false;
                        logger.WarnFormat("Cannot find this trip. tripID={0}", request.Reference.First().ID);
                    }
                }
                else
                {
                    result = false;
                    logger.Warn("Cannot find trip by invalid referenceId");
                }
                #endregion

                if (result)
                {
                    serviceAPIID = GetServiceAPIId(fleetId, VanServiceAPIPreferenceConst.Status);
                    if (serviceAPIID == -1)
                    {
                        var vtodException = VtodException.CreateException(ExceptionType.Van, 1011);
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException;// new ValidationException(Messages.van_Common_UnableToFindFleetByAddressOrAirportInfo);
                    }
                }

                return serviceAPIID;
            }

            public long GetServiceAPIIdByRequest(OTA_GroundBookRQ request, out van_fleet fleet)
            {
                long serviceAPIID = -1;
                string PickupAddressType = "";
                var PickupAddress = new Map.DTO.Address();
                string PickupAirport = "";
                GroundReservation reservation = request.GroundReservations.FirstOrDefault();
                #region Validate Pickup Address

                if (reservation.Service != null)
                {
                    if (reservation.Service.Location != null)
                    {
                        if (reservation.Service.Location.Pickup != null)
                        {
                            if (((reservation.Service.Location.Pickup.Address != null) && (reservation.Service.Location.Pickup.AirportInfo != null)) || ((reservation.Service.Location.Pickup.Address == null) && (reservation.Service.Location.Pickup.AirportInfo == null)))
                            {
                                logger.Warn(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
                                throw new ValidationException(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
                            }
                            else if ((reservation.Service.Location.Pickup.Address != null) && (reservation.Service.Location.Pickup.AirportInfo == null))
                            {

                                if ((string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude) || (string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CountryName.Code)))
                                {
                                    logger.Warn(Messages.Validation_LocationDetail);
                                    throw new ValidationException(Messages.Validation_LocationDetail);
                                }
                                else if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.AddressLine) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CityName) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.PostalCode) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CountryName.Code))
                                {
                                    //Address
                                    Map.DTO.Address pickupAddress = null;
                                    string pickupAddressStr = GetAddressString(reservation.Service.Location.Pickup.Address);
                                    //if (reservation.Service.Location.Pickup.Address.TPA_Extensions != null)
                                    //{
                                    //	if (reservation.Service.Location.Pickup.Address.TPA_Extensions.AddressValidationRequired.HasValue ? reservation.Service.Location.Pickup.Address.TPA_Extensions.AddressValidationRequired.Value : false)
                                    //	{
                                    //		if (!LookupAddress(reservation.Service.Location.Pickup, pickupAddressStr, out pickupAddress))
                                    //		{

                                    //			logger.WarnFormat(Messages.Validation_LocationDetailByMapApi);
                                    //			throw new ValidationException(Messages.Validation_LocationDetailByMapApi);
                                    //		}
                                    //	}
                                    //	else
                                    //	{
                                    //		if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Latitude))
                                    //		{
                                    //			logger.Info("Skip MAP API Validation.");
                                    //			pickupAddress = ConvertAddress(reservation.Service.Location.Pickup);
                                    //		}
                                    //		else
                                    //		{

                                    //			logger.WarnFormat(Messages.Validation_LocationNoLatlon);
                                    //			throw new ValidationException(Messages.Validation_LocationNoLatlon);
                                    //		}
                                    //	}
                                    //}
                                    //else
                                    //{
                                    logger.Info("Skip MAP API Validation.");
                                    pickupAddress = ConvertAddress(reservation.Service.Location.Pickup);
                                    //}
                                    PickupAddressType = UDI.VTOD.Domain.Van.Const.AddressType.Address;
                                    PickupAddress = pickupAddress;
                                    logger.Info("Pickup AddressType: Address");
                                }
                                else if ((!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude) && (!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CountryName.Code)))
                                {
                                    //tbp.PickupAddressOnlyContainsLatAndLong = true;
                                    MapService ms = new MapService();
                                    Map.DTO.Geolocation g = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(reservation.Service.Location.Pickup.Address.Latitude), Longitude = Convert.ToDecimal(reservation.Service.Location.Pickup.Address.Longitude) };
                                    string error = "";
                                    var addressList = ms.GetAddressesFromGeoLocationWithProximity(g, out error);
                                    if (string.IsNullOrWhiteSpace(error) && addressList.Any())
                                    {
                                        var add = addressList.FirstOrDefault();
                                        reservation.Service.Location.Pickup.Address.StreetNmbr = add.StreetNo;
                                        reservation.Service.Location.Pickup.Address.AddressLine = add.Street;
                                        reservation.Service.Location.Pickup.Address.BldgRoom = add.ApartmentNo;
                                        reservation.Service.Location.Pickup.Address.CityName = add.City;
                                        reservation.Service.Location.Pickup.Address.PostalCode = add.ZipCode;
                                        reservation.Service.Location.Pickup.Address.StateProv = new StateProv { StateCode = add.State };
                                        reservation.Service.Location.Pickup.Address.CountryName = new CountryName { Code = add.Country };
                                        reservation.Service.Location.Pickup.Address.StreetNmbr = add.StreetNo;
                                        reservation.Service.Location.Pickup.Address.StreetNmbr = add.StreetNo;

                                        PickupAddressType = UDI.VTOD.Domain.Van.Const.AddressType.Address;
                                        PickupAddress = ConvertAddress(reservation.Service.Location.Pickup);
                                        logger.Info("Pickup AddressType: Address");

                                    }
                                    else
                                    {
                                        logger.Warn(Messages.Validation_LocationDetail);
                                        throw new ValidationException(Messages.Validation_LocationDetail);
                                    }
                                }
                                else
                                {
                                    var vtodException = VtodException.CreateException(ExceptionType.Van, 1);
                                    logger.Warn(vtodException.ExceptionMessage.Message); // Messages.van_Common_UnknownError);
                                    throw vtodException;
                                }
                            }
                            else if ((reservation.Service.Location.Pickup.Address == null) && (reservation.Service.Location.Pickup.AirportInfo != null))
                            {
                                //airport
                                if (((reservation.Service.Location.Pickup.AirportInfo.Arrival != null) && (reservation.Service.Location.Pickup.AirportInfo.Departure != null)) || ((reservation.Service.Location.Pickup.AirportInfo.Arrival == null) && (reservation.Service.Location.Pickup.AirportInfo.Departure == null)))
                                {
                                    logger.Warn(Messages.Validation_ArrivalAndDepartureAirportInfo);
                                    throw VtodException.CreateValidationException(Messages.Validation_ArrivalAndDepartureAirportInfo);
                                }
                                else if (reservation.Service.Location.Pickup.AirportInfo.Arrival != null && reservation.Service.Location.Pickup.AirportInfo.Departure == null)
                                {
                                    //Arrival
                                    PickupAddressType = AddressType.Airport;
                                    PickupAirport = reservation.Service.Location.Pickup.AirportInfo.Arrival.LocationCode;
                                    logger.Info("Pickup AddressType: AirportInfo.Arrival");
                                }
                                else if (reservation.Service.Location.Pickup.AirportInfo.Arrival == null && reservation.Service.Location.Pickup.AirportInfo.Departure != null)
                                {
                                    //Depature
                                    PickupAddressType = AddressType.Airport;
                                    PickupAirport = reservation.Service.Location.Pickup.AirportInfo.Departure.LocationCode;
                                    logger.Info("Pickup AddressType: AirportInfo.Depature");
                                }

                                //longitude and latitude

                                double? longitude = null;
                                double? latitude = null;
                                if (!GetLongitudeAndLatitudeForAirport(PickupAirport, out longitude, out latitude))
                                {
                                    throw VtodException.CreateException(ExceptionType.Van, 1010);// new ValidationException(Messages.van_Common_UnableToFindAirportLongitudeAndLatitude);
                                }


                                //throw new ValidationException(Messages.van_Common_InvalidAirportPickup);
                            }
                        }
                        else
                        {
                            var vtodException = VtodException.CreateFieldRequiredValidationException("Pickup");
                            logger.Warn(vtodException.ExceptionMessage.Message);
                            throw vtodException; // new ValidationException(Messages.van_Common_InvalidReservation);
                        }
                    }
                    else
                    {
                        var vtodException = VtodException.CreateFieldRequiredValidationException("Reservation.Service.Location");
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException; // new ValidationException(Messages.van_Common_InvalidReservation);
                    }
                }
                else
                {
                    var vtodException = VtodException.CreateFieldRequiredValidationException("GroundReservation.Service");
                    logger.Warn(vtodException.ExceptionMessage.Message);
                    throw vtodException; // new ValidationException(Messages.van_Common_InvalidReservation);
                }
                #endregion

                #region validate fleet
                //van_fleet fleet;
                if (GetFleet(PickupAddressType, PickupAddress, PickupAirport, out fleet))
                {
                    logger.Info("Found fleet");
                }
                else
                {
                    var vtodException = VtodException.CreateException(ExceptionType.Van, 1011);
                    logger.Warn(vtodException.ExceptionMessage.Message);
                    throw vtodException;// new ValidationException(Messages.van_Common_UnableToFindFleetByAddressOrAirportInfo);

                }
                #endregion

                #region Temprory HardCoding - Please comment out this block after 6/28 3AM
                var throwBlockOutException = false;

                #region KC 1
                try
                {
                    if (fleet.Id == 25)
                    {
                        if ((request.TPA_Extensions == null) || (!request.TPA_Extensions.PickMeUpNow.HasValue) || !request.TPA_Extensions.PickMeUpNow.Value)
                        {
                            //if (/* !tbp.PickupNow && */ DateTime.Now < new DateTime(2015, 01, 02, 0, 0, 0))
                            //{
                            var from = new DateTime(2015, 8, 1, 14, 0, 0);
                            var to = new DateTime(2015, 8, 2, 1, 0, 0);

                            var puTime = request.GroundReservations.FirstOrDefault().Service.Location.Pickup.DateTime.ToDateTime();
                            if (puTime >= from && puTime <= to)
                            {
                                throwBlockOutException = true;
                            }
                            //}
                        }
                    }
                }
                catch { }
                #endregion

                #region KC 2
                try
                {
                    if (fleet.Id == 25)
                    {
                        if ((request.TPA_Extensions == null) || (!request.TPA_Extensions.PickMeUpNow.HasValue) || !request.TPA_Extensions.PickMeUpNow.Value)
                        {
                            //if (/* !tbp.PickupNow && */ DateTime.Now < new DateTime(2015, 01, 02, 0, 0, 0))
                            //{
                            var from = new DateTime(2015, 7, 28, 16, 0, 0);
                            var to = new DateTime(2015, 7, 29, 1, 0, 0);

                            var puTime = request.GroundReservations.FirstOrDefault().Service.Location.Pickup.DateTime.ToDateTime();
                            if (puTime >= from && puTime <= to)
                            {
                                throwBlockOutException = true;
                            }
                            //}
                        }
                    }
                }
                catch { }
                #endregion

                #region Pittsburg
                try
                {
                    if (fleet.Id == 23)
                    {
                        if ((request.TPA_Extensions == null) || (!request.TPA_Extensions.PickMeUpNow.HasValue) || !request.TPA_Extensions.PickMeUpNow.Value)
                        {
                            //if (/* !tbp.PickupNow && */ DateTime.Now < new DateTime(2015, 01, 02, 0, 0, 0))
                            //{
                            var from = new DateTime(2015, 8, 1, 16, 0, 0);
                            var to = new DateTime(2015, 8, 2, 1, 0, 0);

                            var puTime = request.GroundReservations.FirstOrDefault().Service.Location.Pickup.DateTime.ToDateTime();
                            if (puTime >= from && puTime <= to)
                            {
                                throwBlockOutException = true;
                            }
                            //}
                        }
                    }
                }
                catch { }
                #endregion



                if (throwBlockOutException)
                {
                    throw VtodException.CreateException(ExceptionType.Van, 2013);// new vanException(Messages.van_BookingFailure);
                }
                #endregion


                serviceAPIID = GetServiceAPIId(fleet.Id, VanServiceAPIPreferenceConst.Book);

                if (serviceAPIID == -1)
                {
                    var vtodException = VtodException.CreateException(ExceptionType.Van, 1011);
                    logger.Warn(vtodException.ExceptionMessage.Message);
                    throw vtodException;// new ValidationException(Messages.van_Common_UnableToFindFleetByAddressOrAirportInfo);
                }

                return serviceAPIID;
            }

            public long GetServiceAPIIdByRequest(GetFareDetailRQ request)
            {
                long serviceAPIID = -1;
                long fleetId = -1;
                bool result = true;
                string tripID = null;

                #region Validate Reference Id
                if (request.Reference == null || !request.Reference.Any() || string.IsNullOrWhiteSpace(request.Reference.First().ID))
                {
                    result = false;
                    logger.Warn("Reference.ID is empty.");
                }
                else
                {
                    logger.Info("Request is valid.");
                }
                #endregion

                #region Validate Trip
                vtod_trip t;
                if (result)
                {

                    var confType = request.Reference.First().Type;

                    if (confType.ToLower().Trim() == Common.DTO.Enum.ConfirmationType.Confirmation.ToString().ToLower())
                    {
                        tripID = request.Reference.First().ID;
                    }
                    else if (confType.ToLower().Trim() == Common.DTO.Enum.ConfirmationType.DispatchConfirmation.ToString().ToLower())
                    {
                        var dispatchTripID = request.Reference.First().ID;
                        var fleetID = request.Reference.First().ID_Context.ToInt32();
                        using (var db = new VTODEntities())
                        {
                            tripID = db.van_trip.Where(s => s.DispatchTripId == dispatchTripID && s.FleetId == fleetID).First().Id.ToString();
                        }
                    }

                    t = GetVanTrip(tripID);

                    if (t != null)
                    {
                        fleetId = t.van_trip.FleetId;
                        logger.InfoFormat("Trip found. tripID={0}", tripID);
                    }
                    else
                    {
                        result = false;
                        logger.WarnFormat("Cannot find this trip. tripID={0}", tripID);
                    }
                }
                else
                {
                    result = false;
                    logger.Warn("Cannot find trip by invalid referenceId");
                }
                #endregion

                if (result)
                {
                    serviceAPIID = GetServiceAPIId(fleetId, VanServiceAPIPreferenceConst.Status);
                    if (serviceAPIID == -1)
                    {
                        var vtodException = VtodException.CreateException(ExceptionType.Van, 1011);
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException;// new ValidationException(Messages.van_Common_UnableToFindFleetByAddressOrAirportInfo);
                    }
                }

                return serviceAPIID;
            }

            //UtilityAvailableFleetsRQ
            public long GetServiceAPIIdByRequest(UtilityAvailableFleetsRQ request)
            {
                long serviceAPIID = -1;
                bool result = true;
                Map.DTO.Address address = new Map.DTO.Address();

                #region Validate Pickup Address
                if (result)
                {
                    //Pickup_Dropoff_Stop stop = request.Service.Pickup;

                    if (request.Address != null)
                    {
                        if (!string.IsNullOrWhiteSpace(request.Address.CountryName.Code) && !string.IsNullOrWhiteSpace(request.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(request.Address.CityName) && !string.IsNullOrWhiteSpace(request.Address.PostalCode))
                        {
                            address = new Map.DTO.Address { Country = request.Address.CountryName.Code, State = request.Address.StateProv.StateCode, City = request.Address.CityName, ZipCode = request.Address.PostalCode, Geolocation = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(request.Address.Latitude), Longitude = Convert.ToDecimal(request.Address.Longitude) } };
                        }
                        else
                        {
                            result = false;
                            logger.Warn("Cannot find any address/locationName from address.");
                        }
                    }
                    else
                    {
                        result = false;
                        logger.Warn("Cannot find any address/locationName from address.");
                    }
                }
                else
                {
                    result = false;
                }
                #endregion

                #region Validate Fleet
                van_fleet fleet = null;
                if (result)
                {
                    if (GetFleet(AddressType.Address, address, string.Empty, out fleet))
                    {
                        logger.InfoFormat("Fleet found. ID={0}", fleet.Id);
                    }
                    else
                    {
                        result = false;
                        logger.Warn("Unable to find fleet by this address or landmark.");
                    }
                }
                else
                {
                    result = false;
                    logger.Warn("Unable to find fleet by invalid address or landmark.");
                }
                #endregion

                if (result)
                {
                    serviceAPIID = GetServiceAPIId(fleet.Id, VanServiceAPIPreferenceConst.GetAvailableFleets);
                    if (serviceAPIID == -1)
                    {
                        var vtodException = VtodException.CreateException(ExceptionType.Van, 1011);
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException;// new ValidationException(Messages.van_Common_UnableToFindFleetByAddressOrAirportInfo);
                    }
                }

                return serviceAPIID;
            }

            public bool ValidateBookingRequest(TokenRS tokenVTOD, OTA_GroundBookRQ request, out VanBookingParameter tbp, van_fleet fleet)
            {
                bool result = true;
                Stopwatch sw = new Stopwatch();
                sw.Start();
                tbp = new VanBookingParameter();
                tbp.Fleet = fleet;
                Stopwatch swEachPart = new Stopwatch();



                #region Track
                if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Domain, "Call");
                #endregion

                #region Validate Reservation
                if (request.GroundReservations.Any())
                {
                    GroundReservation reservation = request.GroundReservations.FirstOrDefault();

                    #region Set token, request
                    tbp.tokenVTOD = tokenVTOD;
                    tbp.request = request;
                    #endregion

                    #region Set Source and Device and ref
                    if (request.TPA_Extensions != null && !string.IsNullOrWhiteSpace(request.TPA_Extensions.Source))
                    {
                        tbp.Source = request.TPA_Extensions.Source;
                    }

                    if (request.TPA_Extensions != null && !string.IsNullOrWhiteSpace(request.TPA_Extensions.Device))
                    {
                        tbp.Device = request.TPA_Extensions.Device;
                    }

                    if (!string.IsNullOrWhiteSpace(request.EchoToken))
                    {
                        tbp.Ref = request.EchoToken;
                    }
                    #endregion

                    swEachPart.Restart();
                    #region Validate Member Info
                    //As of this version, all requester should pass memberID.
                    //We need to modify this based on some others who do not pass memberID
                    if (request.TPA_Extensions == null && string.IsNullOrWhiteSpace(request.TPA_Extensions.MemberID))
                    {
                        result = false;
                        var vtodException = VtodException.CreateFieldRequiredValidationException("MemberID");//ExceptionType.Van, 2001);
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException;// new ValidationException(Messages.van_Common_UnableToFindFleetByAddressOrAirportInfo);
                    }
                    else
                    {
                        tbp.MemberID = request.TPA_Extensions.MemberID.ToInt32();
                    }
                    #endregion
                    if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility, "ValidateBookingRequest_ValidateMemberInfo");
                    swEachPart.Stop();
                    logger.DebugFormat("Validate member Info Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                    swEachPart.Restart();
                    #region Validate Customer
                    if (reservation.Passenger != null)
                    {
                        //Name
                        if (reservation.Passenger.Primary != null)
                        {
                            if (reservation.Passenger.Primary.PersonName != null)
                            {
                                if (string.IsNullOrWhiteSpace(reservation.Passenger.Primary.PersonName.GivenName) || string.IsNullOrWhiteSpace(reservation.Passenger.Primary.PersonName.Surname))
                                {
                                    result = false;
                                    var vtodException = VtodException.CreateFieldRequiredValidationException("PassengerGivenName|PassengerSurname");
                                    logger.Warn(vtodException.ExceptionMessage.Message);
                                    throw vtodException; // new ValidationException(Messages.van_Common_InvalidReservation);
                                }
                                else
                                {
                                    tbp.FirstName = reservation.Passenger.Primary.PersonName.GivenName.Trim();
                                    tbp.LastName = reservation.Passenger.Primary.PersonName.Surname.Trim();
                                    logger.Info("First name and last name are valid.");
                                }
                            }
                            else
                            {
                                result = false;
                                var vtodException = VtodException.CreateFieldRequiredValidationException("PassengerName");
                                logger.Warn(vtodException.ExceptionMessage.Message);
                                throw vtodException; // new ValidationException(Messages.van_Common_InvalidReservation);
                            }

                            //Telephone
                            if (reservation.Passenger.Primary.Telephones.Any())
                            {
                                Telephone p = reservation.Passenger.Primary.Telephones.FirstOrDefault();
                                string areacode = ExtractDigits(p.AreaCityCode);
                                string phonenumber = ExtractDigits(p.PhoneNumber);
                                if (areacode.Length.Equals(3) && phonenumber.Length.Equals(7))
                                {
                                    tbp.PhoneNumber = string.Format("{0}{1}", areacode, phonenumber);
                                    logger.Info("Phone number(s) is/are valid.");
                                }
                                else
                                {
                                    result = false;
                                    var vtodException = VtodException.CreateFieldFormatValidationException("Telephone", "10 digits");
                                    logger.Warn(vtodException.ExceptionMessage.Message);
                                    throw vtodException; // new ValidationException(Messages.van_Common_InvalidReservation);
                                }
                            }
                            else
                            {
                                result = false;
                                var vtodException = VtodException.CreateFieldRequiredValidationException("Telephones");
                                logger.Warn(vtodException.ExceptionMessage.Message);
                                throw vtodException; // new ValidationException(Messages.van_Common_InvalidReservation);
                            }
                            //email
                            if (reservation.Passenger.Primary.Emails.Any())
                            {
                                Email e = reservation.Passenger.Primary.Emails.FirstOrDefault();


                                string emailPattern = @"^(?("")("".+?(?<!\\)""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
                                                     @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]*\.)+[a-zA-Z0-9][\-a-zA-Z0-9]{0,22}[a-zA-Z0-9]))$";

                                if (!string.IsNullOrWhiteSpace(e.Value) && Regex.IsMatch(e.Value, emailPattern, RegexOptions.IgnorePatternWhitespace))
                                {
                                    tbp.EmailAddress = e.Value;
                                    logger.Info(Messages.ECar_Common_ValidReservationPassengerPrimaryTelephones);
                                }
                            }
                            if (result)
                            {
                                ////Create or GetCustomer
                                //van_customer customer = GetvanCustomer(reservation);
                                //tbp.Customer = customer;
                                logger.InfoFormat("Customer firstname={0}, lastname={1}, phonenumber={2}", tbp.FirstName, tbp.LastName, tbp.PhoneNumber);
                            }
                            else
                            {
                                result = false;
                                var vtodException = VtodException.CreateException(ExceptionType.Van, 2008);
                                logger.Warn(vtodException.ExceptionMessage.Message);
                                throw vtodException; // new ValidationException(Messages.van_Common_InvalidReservation);
                            }
                        }
                        else
                        {
                            result = false;
                            var vtodException = VtodException.CreateFieldRequiredValidationException("PassengerPrimary");
                            logger.Warn(vtodException.ExceptionMessage.Message);
                            throw vtodException; // new ValidationException(Messages.van_Common_InvalidReservation);
                        }
                    }
                    else
                    {
                        result = false;
                        var vtodException = VtodException.CreateFieldRequiredValidationException("Passenger");
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException; // new ValidationException(Messages.van_Common_InvalidReservation);
                    }

                    #endregion
                    if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility, "ValidateBookingRequest_ValidateCustomer");
                    swEachPart.Stop();
                    logger.DebugFormat("Validate Customer Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                    swEachPart.Restart();
                    #region Validate Pickup Address

                    if (reservation.Service != null)
                    {
                        if (reservation.Service.Location != null)
                        {
                            if (reservation.Service.Location.Pickup != null)
                            {
                                if (((reservation.Service.Location.Pickup.Address != null) && (reservation.Service.Location.Pickup.AirportInfo != null)) || ((reservation.Service.Location.Pickup.Address == null) && (reservation.Service.Location.Pickup.AirportInfo == null)))
                                {
                                    result = false;
                                    logger.Warn(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
                                    throw new ValidationException(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
                                }
                                else if ((reservation.Service.Location.Pickup.Address != null) && (reservation.Service.Location.Pickup.AirportInfo == null))
                                {

                                    if ((string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude) || (string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CountryName.Code)))
                                    {
                                        result = false;
                                        logger.Warn(Messages.Validation_LocationDetail);
                                        throw new ValidationException(Messages.Validation_LocationDetail);
                                    }
                                    else if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.AddressLine) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CityName) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.PostalCode) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CountryName.Code))
                                    {
                                        //Address
                                        Map.DTO.Address pickupAddress = null;
                                        string pickupAddressStr = GetAddressString(reservation.Service.Location.Pickup.Address);
                                        //if (reservation.Service.Location.Pickup.Address.TPA_Extensions != null)
                                        //{
                                        //	if (reservation.Service.Location.Pickup.Address.TPA_Extensions.AddressValidationRequired.HasValue ? reservation.Service.Location.Pickup.Address.TPA_Extensions.AddressValidationRequired.Value : false)
                                        //	{
                                        //		if (!LookupAddress(reservation.Service.Location.Pickup, pickupAddressStr, out pickupAddress))
                                        //		{
                                        //			result = false;
                                        //			logger.WarnFormat(Messages.Validation_LocationDetailByMapApi);
                                        //			throw new ValidationException(Messages.Validation_LocationDetailByMapApi);
                                        //		}
                                        //	}
                                        //	else
                                        //	{
                                        //		if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Latitude))
                                        //		{
                                        //			logger.Info("Skip MAP API Validation.");
                                        //			pickupAddress = ConvertAddress(reservation.Service.Location.Pickup);
                                        //		}
                                        //		else
                                        //		{
                                        //			result = false;
                                        //			logger.WarnFormat(Messages.Validation_LocationNoLatlon);
                                        //			throw new ValidationException(Messages.Validation_LocationNoLatlon);
                                        //		}
                                        //	}
                                        //}
                                        //else
                                        //{
                                        logger.Info("Skip MAP API Validation.");
                                        pickupAddress = ConvertAddress(reservation.Service.Location.Pickup);
                                        //}
                                        tbp.PickupAddressType = UDI.VTOD.Domain.Van.Const.AddressType.Address;
                                        tbp.PickupAddress = pickupAddress;
                                        logger.Info("Pickup AddressType: Address");
                                    }
                                    else if ((!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude) && (!string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CountryName.Code)))
                                    {
                                        tbp.PickupAddressOnlyContainsLatAndLong = true;
                                        MapService ms = new MapService();
                                        Map.DTO.Geolocation g = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(reservation.Service.Location.Pickup.Address.Latitude), Longitude = Convert.ToDecimal(reservation.Service.Location.Pickup.Address.Longitude) };
                                        string error = "";
                                        var addressList = ms.GetAddressesFromGeoLocationWithProximity(g, out error);
                                        if (string.IsNullOrWhiteSpace(error) && addressList.Any())
                                        {
                                            var add = addressList.FirstOrDefault();
                                            reservation.Service.Location.Pickup.Address.StreetNmbr = add.StreetNo;
                                            reservation.Service.Location.Pickup.Address.AddressLine = add.Street;
                                            reservation.Service.Location.Pickup.Address.BldgRoom = add.ApartmentNo;
                                            reservation.Service.Location.Pickup.Address.CityName = add.City;
                                            reservation.Service.Location.Pickup.Address.PostalCode = add.ZipCode;
                                            reservation.Service.Location.Pickup.Address.StateProv = new StateProv { StateCode = add.State };
                                            reservation.Service.Location.Pickup.Address.CountryName = new CountryName { Code = add.Country };
                                            reservation.Service.Location.Pickup.Address.StreetNmbr = add.StreetNo;
                                            reservation.Service.Location.Pickup.Address.StreetNmbr = add.StreetNo;

                                            tbp.PickupAddressType = UDI.VTOD.Domain.Van.Const.AddressType.Address;
                                            tbp.PickupAddress = ConvertAddress(reservation.Service.Location.Pickup);
                                            logger.Info("Pickup AddressType: Address");

                                        }
                                        else
                                        {
                                            result = false;
                                            logger.Warn(Messages.Validation_LocationDetail);
                                            throw new ValidationException(Messages.Validation_LocationDetail);
                                        }
                                    }
                                    else
                                    {
                                        result = false;
                                        var vtodException = VtodException.CreateException(ExceptionType.Van, 1);
                                        logger.Warn(vtodException.ExceptionMessage.Message); // Messages.van_Common_UnknownError);
                                        throw vtodException;
                                    }
                                }
                                else if ((reservation.Service.Location.Pickup.Address == null) && (reservation.Service.Location.Pickup.AirportInfo != null))
                                {
                                    //airport
                                    if (((reservation.Service.Location.Pickup.AirportInfo.Arrival != null) && (reservation.Service.Location.Pickup.AirportInfo.Departure != null)) || ((reservation.Service.Location.Pickup.AirportInfo.Arrival == null) && (reservation.Service.Location.Pickup.AirportInfo.Departure == null)))
                                    {
                                        result = false;
                                        logger.Warn(Messages.Validation_ArrivalAndDepartureAirportInfo);
                                        throw VtodException.CreateValidationException(Messages.Validation_ArrivalAndDepartureAirportInfo);
                                    }
                                    else if (reservation.Service.Location.Pickup.AirportInfo.Arrival != null && reservation.Service.Location.Pickup.AirportInfo.Departure == null)
                                    {
                                        //Arrival
                                        tbp.PickupAddressType = AddressType.Airport;
                                        tbp.PickupAirport = reservation.Service.Location.Pickup.AirportInfo.Arrival.LocationCode;
                                        logger.Info("Pickup AddressType: AirportInfo.Arrival");
                                    }
                                    else if (reservation.Service.Location.Pickup.AirportInfo.Arrival == null && reservation.Service.Location.Pickup.AirportInfo.Departure != null)
                                    {
                                        //Depature

                                        tbp.PickupAddressType = AddressType.Airport;
                                        tbp.PickupAirport = reservation.Service.Location.Pickup.AirportInfo.Departure.LocationCode;
                                        logger.Info("Pickup AddressType: AirportInfo.Depature");
                                    }

                                    //longitude and latitude
                                    if (result)
                                    {
                                        double? longitude = null;
                                        double? latitude = null;
                                        if (GetLongitudeAndLatitudeForAirport(tbp.PickupAirport, out longitude, out latitude))
                                        {
                                            tbp.LongitudeForPickupAirport = longitude.Value;
                                            tbp.LatitudeForPickupAirport = latitude.Value;
                                        }
                                        else
                                        {
                                            throw VtodException.CreateException(ExceptionType.Van, 1010);// new ValidationException(Messages.van_Common_UnableToFindAirportLongitudeAndLatitude);
                                        }
                                    }

                                    //throw new ValidationException(Messages.van_Common_InvalidAirportPickup);
                                }
                            }
                            else
                            {
                                result = false;
                                var vtodException = VtodException.CreateFieldRequiredValidationException("Pickup");
                                logger.Warn(vtodException.ExceptionMessage.Message);
                                throw vtodException; // new ValidationException(Messages.van_Common_InvalidReservation);
                            }
                        }
                        else
                        {
                            result = false;
                            var vtodException = VtodException.CreateFieldRequiredValidationException("Reservation.Service.Location");
                            logger.Warn(vtodException.ExceptionMessage.Message);
                            throw vtodException; // new ValidationException(Messages.van_Common_InvalidReservation);
                        }
                    }
                    else
                    {
                        result = false;
                        var vtodException = VtodException.CreateFieldRequiredValidationException("GroundReservation.Service");
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException; // new ValidationException(Messages.van_Common_InvalidReservation);
                    }
                    #endregion
                    if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility, "ValidateBookingRequest_ValidatePickupAddress");
                    swEachPart.Stop();
                    logger.DebugFormat("Validate Pickup address Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                    swEachPart.Restart();
                    #region Validate Fleet
                    if (result)
                    {
                        //van_fleet fleet = null;
                        if (tbp.Fleet != null)//GetFleet(tbp.PickupAddressType, tbp.PickupAddress, tbp.PickupAirport, out fleet))
                        {
                            //fleet=tbp.Fleet ;
                            logger.InfoFormat("Fleet found. ID={0}", tbp.Fleet.Id);

                            if (fleet != null)
                            {
                                tbp.serviceAPIID = GetvanFleetServiceAPIPreference(tbp.Fleet.Id, VanServiceAPIPreferenceConst.Book).ServiceAPIId;
                            }
                        }
                        else
                        {
                            result = false;
                            var vtodException = VtodException.CreateException(ExceptionType.Van, 1011);
                            logger.Warn(vtodException.ExceptionMessage.Message);
                            throw vtodException;// new ValidationException(Messages.van_Common_UnableToFindFleetByAddressOrAirportInfo);
                        }
                    }
                    else
                    {
                        result = false;
                        var vtodException = VtodException.CreateException(ExceptionType.Van, 2001);
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException;// new ValidationException(Messages.van_Common_UnableToFindFleetByAddressOrAirportInfo);
                    }
                    #endregion
                    if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility, "ValidateBookingRequest_ValidateFleet");
                    swEachPart.Stop();
                    logger.DebugFormat("Validate fleet Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                    swEachPart.Restart();
                    #region Validate User
                    if (result)
                    {
                        van_fleet_user user = null;
                        if (GetvanFleetUser(tbp.Fleet.Id, tokenVTOD.Username, out user))
                        {
                            tbp.User = user;
                            logger.Info("This user has sufficient privilege.");
                        }
                        else
                        {
                            result = false;
                            var vtodException = VtodException.CreateException(ExceptionType.Van, 102);
                            logger.Warn(vtodException.ExceptionMessage.Message);
                            throw vtodException; // new ValidationException(Messages.van_Common_InsufficientPrivilege(tokenVTOD.Username, tbp.Fleet.Id));
                        }
                    }
                    else
                    {
                        result = false;
                        var vtodException = VtodException.CreateException(ExceptionType.Van, 2001);
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException;// new ValidationException(Messages.van_Common_UnableToFindFleetByAddressOrAirportInfo);
                    }
                    #endregion
                    if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility, "ValidateBookingRequest_ValidateUser");
                    swEachPart.Stop();
                    logger.DebugFormat("Validate user Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                    swEachPart.Restart();
                    #region Validate PickupDateTime
                    if (result)
                    {
                        //pickiup now
                        //bool pickupNow=false;
                        #region determine Pick me up now
                        if (request.TPA_Extensions != null && request.TPA_Extensions.PickMeUpNow.HasValue)
                        {
                            if (request.TPA_Extensions.PickMeUpNow.Value)
                            {
                                tbp.PickupNow = true;
                            }
                            else
                            {
                                tbp.PickupNow = false;
                            }
                        }
                        else
                        {
                            //request
                            //if (tbp.PickupDateTime.ToUtc(tbp.Fleet.ServerUTCOffset) <= DateTime.UtcNow.AddMinutes(5))
                            try
                            {

                                if (reservation.Service.Location.Pickup.DateTime.ToDateTime().Value.ToUtc(tbp.Fleet.ServerUTCOffset) <= DateTime.UtcNow.AddMinutes(5))
                                {
                                    tbp.PickupNow = true;
                                }
                                else
                                {
                                    tbp.PickupNow = false;
                                }
                            }
                            catch
                            { }
                            //tbp.PickupNow = false;
                        }
                        #endregion

                        #region Validate Pick me up now against DB fleet
                        bool pickMeUpNow = true;
                        bool pickMeUpLater = true;
                        GetPickMeupOption(tbp.Fleet, out pickMeUpNow, out pickMeUpLater);
                        #region PMUL
                        if ((request.TPA_Extensions == null) || (!request.TPA_Extensions.PickMeUpNow.HasValue) || !request.TPA_Extensions.PickMeUpNow.Value)
                        {

                            if (!string.IsNullOrWhiteSpace(request.GroundReservations.FirstOrDefault().Service.Location.Pickup.DateTime))
                            {
                                bool pmulResult = CheckPMUL(tbp.Fleet, request.GroundReservations.FirstOrDefault().Service.Location.Pickup.DateTime.ToDateTime());
                                logger.InfoFormat("PMUL :{0}:", pmulResult);
                                if (pmulResult == false)
                                    throw VtodException.CreateException(ExceptionType.Van, 2013);

                            }
                        }
                        #endregion
                        if (tbp.PickupNow == pickMeUpNow || !tbp.PickupNow == pickMeUpLater)
                        {
                            logger.InfoFormat("Valid pick me up request. Fleet={0}, Trip={1}", tbp.Fleet.PickMeUpNowOption, tbp.PickupNow);

                        }
                        else
                        {
                            logger.WarnFormat("InValid pick me up request. Fleet={0}, Trip={1}", tbp.Fleet.PickMeUpNowOption, tbp.PickupNow);
                            result = false;
                            if (tbp.PickupNow)
                            {
                                throw VtodException.CreateException(ExceptionType.Van, 2011);
                            }
                            else
                            {
                                throw VtodException.CreateException(ExceptionType.Van, 2012);
                            }
                        }

                        #endregion


                        #region determine pick me up now Time

                        DateTime pickupDateTime;
                        //if (DateTime.TryParseExact(reservation.Service.Location.Pickup.DateTime, ConfigurationManager.AppSettings["DateTimeFormatForPickupAndDropoff"], null, DateTimeStyles.None, out pickupDateTime))
                        if (DateTime.TryParse(reservation.Service.Location.Pickup.DateTime, out pickupDateTime))
                        {
                            tbp.PickupDateTime = pickupDateTime;
                            logger.Info("PickupDateTime is correct");
                        }
                        else if (tbp.PickupNow && !DateTime.TryParse(reservation.Service.Location.Pickup.DateTime, out pickupDateTime))
                        {
                            tbp.PickupDateTime = DateTime.UtcNow.FromUtc(tbp.Fleet.ServerUTCOffset);
                            logger.InfoFormat("There is no Pickup datetime for this request. But it has pickup now value. So the pickup time will set as {0:yyyy/MM/dd HH:mm:ss}.", tbp.PickupDateTime);
                        }
                        else
                        {
                            result = false;
                            var vtodException = VtodException.CreateFieldFormatValidationException("PickupDateTime", "DateTime");
                            logger.Warn(vtodException.ExceptionMessage.Message);
                            throw vtodException; // new ValidationException(Messages.van_Common_InvalidReservationServiceLocationPickupDateTime(reservation.Service.Location.Pickup.DateTime));
                        }

                        #endregion
                    }
                    else
                    {
                        result = false;
                        var vtodException = VtodException.CreateException(ExceptionType.Van, 2001);
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException;// new ValidationException(Messages.van_Common_UnableToFindFleetByAddressOrAirportInfo);
                    }
                    #endregion
                    if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility, "ValidateBookingRequest_ValidatePickupDateTime");
                    swEachPart.Stop();
                    logger.DebugFormat("Validate PickupTime Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                    swEachPart.Restart();
                    #region Validate duplicated booking trip
                    if (result)
                    {
                        if (IsAbleToBookTrip(tbp))
                        {
                            logger.Info("This trip can be booked.");
                        }
                        else
                        {
                            result = false;
                            var vtodException = VtodException.CreateException(ExceptionType.Van, 2006);
                            logger.Warn(vtodException.ExceptionMessage.Message);
                            throw vtodException; // new ValidationException(Messages.van_Common_DuplicatedTrips);
                        }
                    }
                    else
                    {
                        result = false;
                        var vtodException = VtodException.CreateException(ExceptionType.Van, 2001);
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException;// new ValidationException(Messages.van_Common_UnableToFindFleetByAddressOrAirportInfo);
                    }
                    #endregion
                    if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility, "ValidateBookingRequest_ValidateDuplicatedBookingTrip");
                    swEachPart.Stop();
                    logger.DebugFormat("Validate duplicated trip Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                    swEachPart.Restart();
                    #region Validate Dropoff Address
                    //DropOff is optional: By_Pouyan
                    if (reservation.Service.Location != null && reservation.Service.Location.Dropoff != null)
                        if (result)
                        {
                            if (reservation.Service != null)
                            {
                                if (reservation.Service.Location != null)
                                {
                                    if (reservation.Service.Location.Dropoff != null)
                                    {
                                        using (VTODEntities context = new VTODEntities())
                                        {
                                            if (((reservation.Service.Location.Dropoff.Address != null) && (reservation.Service.Location.Dropoff.AirportInfo != null)) || ((reservation.Service.Location.Dropoff.Address == null) && (reservation.Service.Location.Dropoff.AirportInfo == null)))
                                            {
                                                result = false;
                                                logger.Warn(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
                                                throw VtodException.CreateValidationException(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
                                            }
                                            else if ((reservation.Service.Location.Dropoff.Address != null) && (reservation.Service.Location.Dropoff.AirportInfo == null))
                                            {
                                                //Address
                                                if ((string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.Longitude) || (string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CountryName.Code)))
                                                //if (string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CountryName.Code))
                                                {
                                                    result = false;
                                                    logger.Warn(Messages.Validation_LocationDetail);
                                                    throw VtodException.CreateValidationException(Messages.Validation_LocationDetail);
                                                }
                                                else if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.AddressLine) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CityName) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.PostalCode) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CountryName.Code))
                                                {
                                                    Map.DTO.Address dropoffAddress = null;
                                                    string dropoffAddressStr = GetAddressString(reservation.Service.Location.Dropoff.Address);
                                                    //if (reservation.Service.Location.Dropoff.Address.TPA_Extensions != null)
                                                    //{
                                                    //	if (reservation.Service.Location.Dropoff.Address.TPA_Extensions.AddressValidationRequired.HasValue ? reservation.Service.Location.Dropoff.Address.TPA_Extensions.AddressValidationRequired.Value : false)
                                                    //	{
                                                    //		if (!LookupAddress(reservation.Service.Location.Dropoff, dropoffAddressStr, out dropoffAddress))
                                                    //		{
                                                    //			result = false;
                                                    //			logger.WarnFormat(Messages.Validation_LocationDetailByMapApi);
                                                    //			throw VtodException.CreateValidationException(Messages.Validation_LocationDetailByMapApi);
                                                    //		}
                                                    //	}
                                                    //	else
                                                    //	{
                                                    //		if (!string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.Longitude) && !string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.Latitude))
                                                    //		{
                                                    //			logger.Info("Skip MAP API Validation.");
                                                    //			dropoffAddress = ConvertAddress(reservation.Service.Location.Dropoff);
                                                    //		}
                                                    //		else
                                                    //		{
                                                    //			result = false;
                                                    //			logger.WarnFormat(Messages.Validation_LocationNoLatlon);
                                                    //			throw new ValidationException(Messages.Validation_LocationNoLatlon);
                                                    //		}
                                                    //	}
                                                    //}
                                                    //else
                                                    //{
                                                    logger.Info("Skip MAP API Validation.");
                                                    dropoffAddress = ConvertAddress(reservation.Service.Location.Dropoff);
                                                    //}

                                                    tbp.DropOffAddress = dropoffAddress;
                                                    tbp.DropOffAddressType = UDI.VTOD.Domain.Van.Const.AddressType.Address;
                                                    logger.Info("Dropoff AddressType=Address");
                                                }
                                                else if ((!string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.Longitude) && (!string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CountryName.Code)))
                                                {
                                                    tbp.DropOffAddressOnlyContainsLatAndLong = true;
                                                    MapService ms = new MapService();
                                                    Map.DTO.Geolocation g = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(reservation.Service.Location.Dropoff.Address.Latitude), Longitude = Convert.ToDecimal(reservation.Service.Location.Dropoff.Address.Longitude) };
                                                    string error = "";
                                                    var addressList = ms.GetAddressesFromGeoLocationWithProximity(g, out error);
                                                    if (string.IsNullOrWhiteSpace(error) && addressList.Any())
                                                    {
                                                        var add = addressList.FirstOrDefault();
                                                        reservation.Service.Location.Dropoff.Address.StreetNmbr = add.StreetNo;
                                                        reservation.Service.Location.Dropoff.Address.AddressLine = add.Street;
                                                        reservation.Service.Location.Dropoff.Address.BldgRoom = add.ApartmentNo;
                                                        reservation.Service.Location.Dropoff.Address.CityName = add.City;
                                                        reservation.Service.Location.Dropoff.Address.PostalCode = add.ZipCode;
                                                        reservation.Service.Location.Dropoff.Address.StateProv = new StateProv { StateCode = add.State };
                                                        reservation.Service.Location.Dropoff.Address.CountryName = new CountryName { Code = add.Country };
                                                        reservation.Service.Location.Dropoff.Address.StreetNmbr = add.StreetNo;
                                                        reservation.Service.Location.Dropoff.Address.StreetNmbr = add.StreetNo;

                                                        tbp.DropOffAddressType = UDI.VTOD.Domain.Van.Const.AddressType.Address;
                                                        tbp.DropOffAddress = ConvertAddress(reservation.Service.Location.Dropoff);
                                                        logger.Info("Dropoff AddressType: Address");

                                                    }
                                                    else
                                                    {
                                                        result = false;
                                                        logger.Warn(Messages.Validation_LocationDetail);
                                                        throw new ValidationException(Messages.Validation_LocationDetail);
                                                    }
                                                }
                                                else
                                                {
                                                    result = false;
                                                    var vtodException = VtodException.CreateException(ExceptionType.Van, 1);
                                                    logger.Warn(vtodException.ExceptionMessage.Message);
                                                    throw vtodException;
                                                }
                                            }
                                            else if ((reservation.Service.Location.Dropoff.Address == null) && (reservation.Service.Location.Dropoff.AirportInfo != null))
                                            {

                                                //airport
                                                if (((reservation.Service.Location.Dropoff.AirportInfo.Arrival != null) && (reservation.Service.Location.Dropoff.AirportInfo.Departure != null)) || ((reservation.Service.Location.Dropoff.AirportInfo.Arrival == null) && (reservation.Service.Location.Dropoff.AirportInfo.Departure == null)))
                                                {
                                                    result = false;
                                                    logger.Warn(Messages.Validation_ArrivalAndDepartureAirportInfo);
                                                    throw VtodException.CreateValidationException(Messages.Validation_ArrivalAndDepartureAirportInfo);
                                                }
                                                else if (reservation.Service.Location.Dropoff.AirportInfo.Arrival != null && reservation.Service.Location.Dropoff.AirportInfo.Departure == null)
                                                {
                                                    //Arrival
                                                    tbp.DropOffAddressType = AddressType.Airport;
                                                    tbp.DropoffAirport = reservation.Service.Location.Dropoff.AirportInfo.Arrival.LocationCode;
                                                    logger.Info("Dropoff AddressType: AirportInfo.Arrival");
                                                }
                                                else if (reservation.Service.Location.Dropoff.AirportInfo.Arrival == null && reservation.Service.Location.Dropoff.AirportInfo.Departure != null)
                                                {
                                                    //Depature
                                                    tbp.DropOffAddressType = AddressType.Airport;
                                                    tbp.DropoffAirport = reservation.Service.Location.Dropoff.AirportInfo.Departure.LocationCode;
                                                    logger.Info("Dropoff AddressType: AirportInfo.Depature");
                                                }

                                                //longitude and latitude
                                                if (result)
                                                {
                                                    double? longitude = null;
                                                    double? latitude = null;
                                                    if (GetLongitudeAndLatitudeForAirport(tbp.DropoffAirport, out longitude, out latitude))
                                                    {
                                                        tbp.LongitudeForDropoffAirport = longitude.Value;
                                                        tbp.LatitudeForDropoffAirport = latitude.Value;
                                                    }
                                                    else
                                                    {
                                                        throw VtodException.CreateException(ExceptionType.Van, 1010);// new ValidationException(Messages.van_Common_UnableToFindAirportLongitudeAndLatitude);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    //else
                                    //{
                                    //	tbp.DropoffAddressType = AddressType.Empty;
                                    //	logger.Info(Messages.van_Common_InvalidReservationServiceLocationDropoff);
                                    //	throw new ValidationException(Messages.van_Common_InvalidReservationServiceLocationDropoff);
                                    //}
                                }
                                else
                                {
                                    result = false;
                                    var vtodException = VtodException.CreateFieldRequiredValidationException("Reservation.Service.Location");
                                    logger.Warn(vtodException.ExceptionMessage.Message);
                                    throw vtodException; // new ValidationException(Messages.van_Common_InvalidReservation);
                                }
                            }
                            else
                            {
                                result = false;
                                var vtodException = VtodException.CreateFieldRequiredValidationException("GroundReservation.Service");
                                logger.Warn(vtodException.ExceptionMessage.Message);
                                throw vtodException; // new ValidationException(Messages.van_Common_InvalidReservation);
                            }
                        }
                        else
                        {
                            result = false;
                            var vtodException = VtodException.CreateException(ExceptionType.Van, 2001);
                            logger.Warn(vtodException.ExceptionMessage.Message);
                            throw vtodException;// new ValidationException(Messages.van_Common_UnableToFindFleetByAddressOrAirportInfo);
                        }
                    #endregion
                    if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility, "ValidateBookingRequest_ValidateDropOffAddress");
                    swEachPart.Stop();
                    logger.DebugFormat("Validate dropoff address Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                    swEachPart.Restart();
                    #region Validate DropOffDateTime
                    //if (result)
                    //{
                    //	DateTime dropOffDateTime;
                    //	if (reservation.Service.Location.Dropoff != null)
                    //	{
                    //		if (DateTime.TryParse(reservation.Service.Location.Dropoff.DateTime, out dropOffDateTime))
                    //		{
                    //			tbp.DropOffDateTime = dropOffDateTime;
                    //		}
                    //		else
                    //		{
                    //			tbp.DropOffDateTime = null;
                    //		}
                    //	}
                    //	else
                    //	{
                    //		tbp.DropOffDateTime = null;
                    //	}
                    //}
                    //else
                    //{
                    //	logger.Info("No drop off datetime");
                    //}
                    #endregion
                    if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility, "ValidateBookingRequest_ValidateDropoffDateTime");
                    swEachPart.Stop();
                    logger.DebugFormat("Validate dropoff time Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                    swEachPart.Restart();
                    #region Validate Available address type
                    if (result)
                    {
                        //string pickupAddressType = tbp.PickupAddressType;
                        //string dropoffAddressType = tbp.DropOffAddressType;

                        //verify the address type of pickup address and drop off address
                        UtilityDomain ud = new UtilityDomain();
                        if (tbp.PickupAddressType == AddressType.Address)
                        {
                            if (tbp.PickupAddress.Geolocation == null)
                            {
                                result = false;
                                var vtodException = VtodException.CreateFieldRequiredValidationException("PickupAddress.Geolocation");
                                logger.Warn(vtodException.ExceptionMessage.Message);
                                throw vtodException;
                            }

                            UtilityGetAirportNameRQ uganRQ = new UtilityGetAirportNameRQ();
                            uganRQ.Address = new Address();
                            uganRQ.Address.Latitude = tbp.PickupAddress.Geolocation.Latitude.ToString();
                            uganRQ.Address.Longitude = tbp.PickupAddress.Geolocation.Longitude.ToString();

                            UtilityGetAirportNameRS uganRS = ud.GetAirportName(tokenVTOD, uganRQ);
                            if (uganRS.Success != null)
                            {
                                if (!string.IsNullOrWhiteSpace(uganRS.AirportName))
                                {
                                    //pickupAddressType = AddressType.Airport;
                                    tbp.PickupAddressType = AddressType.Airport;
                                    tbp.PickupAirport = uganRS.AirportName;
                                    tbp.LongitudeForPickupAirport = uganRQ.Address.Longitude.ToDouble();
                                    tbp.LatitudeForPickupAirport = uganRQ.Address.Longitude.ToDouble();
                                }
                            }

                        }

                        if (tbp.DropOffAddressType == AddressType.Address)
                        {
                            if (tbp.DropOffAddress.Geolocation == null)
                            {
                                result = false;
                                var vtodException = VtodException.CreateFieldRequiredValidationException("DropOffAddress.Geolocation");
                                logger.Warn(vtodException.ExceptionMessage.Message);
                                throw vtodException;
                            }

                            UtilityGetAirportNameRQ uganRQ = new UtilityGetAirportNameRQ();
                            uganRQ.Address = new Address();
                            uganRQ.Address.Latitude = tbp.DropOffAddress.Geolocation.Latitude.ToString();
                            uganRQ.Address.Longitude = tbp.DropOffAddress.Geolocation.Longitude.ToString();

                            UtilityGetAirportNameRS uganRS = ud.GetAirportName(tokenVTOD, uganRQ);
                            if (uganRS.Success != null)
                            {
                                if (!string.IsNullOrWhiteSpace(uganRS.AirportName))
                                {
                                    //dropoffAddressType = AddressType.Airport;
                                    tbp.DropOffAddressType = AddressType.Airport;
                                    tbp.DropoffAirport = uganRS.AirportName;
                                    tbp.LongitudeForDropoffAirport = uganRQ.Address.Longitude.ToDouble();
                                    tbp.LatitudeForDropoffAirport = uganRQ.Address.Longitude.ToDouble();
                                }
                            }
                        }

                        //check database setting
                        //1. get van_fleet_availableaddresstype
                        using (VTODEntities context = new VTODEntities())
                        {
                            long fleetId = tbp.Fleet.Id;
                            if (context.van_fleet_availableaddresstype.Where(x => fleetId == x.FleetId).Any())
                            {
                                van_fleet_availableaddresstype tfa = context.van_fleet_availableaddresstype.Where(x => fleetId == x.FleetId).Select(x => x).FirstOrDefault();
                                //2. check if the type is available or not
                                //van_Common_InvalidAddressTypeForFleet
                                if (AddressType.Address.ToString() == tbp.PickupAddressType && AddressType.Address.ToString() == tbp.DropOffAddressType)
                                {
                                    //address to address
                                    if (tfa.AddressToAddress)
                                    {
                                        result = true;
                                    }
                                    else
                                    {
                                        result = false;
                                        throw VtodException.CreateException(ExceptionType.Van, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                    }
                                }
                                else if (AddressType.Address.ToString() == tbp.PickupAddressType && AddressType.Airport.ToString() == tbp.DropOffAddressType)
                                {
                                    //address to airport
                                    if (tfa.AddressToAirport)
                                    {
                                        result = true;
                                    }
                                    else
                                    {
                                        result = false;
                                        throw VtodException.CreateException(ExceptionType.Van, 2020); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                    }
                                }
                                else if (AddressType.Address.ToString() == tbp.PickupAddressType && null == tbp.DropOffAddressType)
                                {
                                    //address to null
                                    if (tfa.AddressToNull)
                                    {
                                        result = true;
                                    }
                                    else
                                    {
                                        result = false;
                                        throw VtodException.CreateException(ExceptionType.Van, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                    }
                                }
                                else if (AddressType.Airport.ToString() == tbp.PickupAddressType && AddressType.Address.ToString() == tbp.DropOffAddressType)
                                {
                                    //airport to address
                                    if (tfa.AirportToAddress)
                                    {
                                        result = true;
                                    }
                                    else
                                    {
                                        result = false;
                                        throw VtodException.CreateException(ExceptionType.Van, 2019); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                    }
                                }
                                else if (AddressType.Airport.ToString() == tbp.PickupAddressType && AddressType.Airport.ToString() == tbp.DropOffAddressType)
                                {
                                    //airport to airport
                                    if (tfa.AirportToAirport)
                                    {
                                        result = true;
                                    }
                                    else
                                    {
                                        result = false;
                                        throw VtodException.CreateException(ExceptionType.Van, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                    }
                                }
                                else if (AddressType.Airport.ToString() == tbp.PickupAddressType && null == tbp.DropOffAddressType)
                                {
                                    //airport to null
                                    if (tfa.AirportToNull)
                                    {
                                        result = true;
                                    }
                                    else
                                    {
                                        result = false;
                                        throw VtodException.CreateException(ExceptionType.Van, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                    }
                                }
                            }
                            else
                            {
                                result = false;
                                throw VtodException.CreateException(ExceptionType.Van, 1011);// new ValidationException(Messages.van_Common_UnableToGetAvailableFleets);
                            }
                        }


                    }
                    else
                    {
                        result = false;
                        var vtodException = VtodException.CreateFieldRequiredValidationException("Paymnet");//ExceptionType.Van, 2001);
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException;// new ValidationException(Messages.van_Common_UnableToFindFleetByAddressOrAirportInfo);
                    }
                    #endregion
                    if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility, "ValidateBookingRequest_ValidateAvailableAddressType");
                    swEachPart.Stop();
                    logger.DebugFormat("Validate available address type Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                    swEachPart.Restart();
                    #region Validate PaymentInfo
                    //As of this version, all requester should pass payment Info.
                    //We need to modify this based on some others who do not pass payementInfo
                    if (request.Payments == null || request.Payments.Payments == null || !request.Payments.Payments.Any())
                    {
                        result = false;
                        var vtodException = Common.DTO.VtodException.CreateFieldRequiredValidationException("Payment");
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException; // new ValidationException(Messages.Validation_NullPayment);
                    }
                    else
                    {
                        if (request.Payments.Payments.First().PaymentCard != null && request.Payments.Payments.First().Cash == null)
                        {
                            if (request.Payments.Payments.First().PaymentCard.CardNumber == null || string.IsNullOrWhiteSpace(request.Payments.Payments.First().PaymentCard.CardNumber.ID))
                            {
                                result = false;
                                var vtodException = Common.DTO.VtodException.CreateFieldRequiredValidationException("CardNumber and CardNumberID");
                                logger.Warn(vtodException.ExceptionMessage.Message);
                                throw vtodException; // new ValidationException(Messages.Validation_NullPayment);
                            }
                            else
                            {
                                tbp.CreditCardID = request.Payments.Payments.First().PaymentCard.CardNumber.ID.ToInt32();
                                tbp.PaymentType = Common.DTO.Enum.PaymentType.PaymentCard;


                                #region Pre-Authorze creadit card
                                //1. Get pre auth information
                                //tbp.User.PreAuthCreditCardAmount
                                //tbp.User.PreAuthCreditCardOption
                                //tbp.MemberID



                                //2. Do pre authorization
                                if (tbp.User.PreAuthCreditCardOption)
                                {
                                    PaymentDomain pd = new PaymentDomain();
                                    PreSaleVerificationResponse preAuthResult;
                                    try
                                    {
                                        preAuthResult = pd.PreSaleCardVerification(
                                            tokenVTOD,
                                            tbp.CreditCardID.Value,
                                            tbp.User.PreAuthCreditCardAmount,
                                            tbp.MemberID.Value,
                                            Convert.ToInt32(tbp.Fleet.SDS_FleetMerchantID.Value.ToString())
                                            );

                                    }
                                    catch (Exception ex)
                                    {
                                        logger.Error("Unable to pre auth credit card information");
                                        logger.ErrorFormat("ex={0}", ex.Message);
                                        throw;
                                    }
                                    if (preAuthResult != null)
                                    {
                                        if (preAuthResult.IsPrepaidCard == true)
                                        {
                                            throw VtodException.CreateException(ExceptionType.Van, 2021);
                                        }
                                        if (preAuthResult.TransactionApproved == false)
                                        {
                                            throw VtodException.CreateException(ExceptionType.Van, 2010);
                                        }

                                    }
                                    else
                                        throw VtodException.CreateException(ExceptionType.Van, 2010);
                                }
                                #endregion


                            }
                        }
                        else if (request.Payments.Payments.First().Cash != null && request.Payments.Payments.First().PaymentCard == null && request.Payments.Payments.First().Cash.CashIndicator == true)
                        {
                            tbp.PaymentType = Common.DTO.Enum.PaymentType.Cash;
                        }
                        else if (request.Payments.Payments.First().MiscChargeOrder != null && !string.IsNullOrWhiteSpace(request.Payments.Payments.First().MiscChargeOrder.OriginalIssuePlace) && request.Payments.Payments.First().MiscChargeOrder.OriginalIssuePlace.ToUpper() == "SDS")
                        {
                            #region Authorize
                            //var membershipController = new UDI.SDS.MembershipController(tokenVTOD, TrackTime);
                            var membershipDomain = new UDI.VTOD.Domain.Membership.MembershipDomain();
                            //if (membershipDomain.IsUserInPaymentRole(tokenVTOD.Username, Common.DTO.Enum.PaymentType.ChargedBySDS.ToString()))
                            //{
                            //	tbp.PaymentType = Common.DTO.Enum.PaymentType.ChargedBySDS;
                            //}
                            //else
                            //{
                            //	result = false;
                            //	logger.Warn(Messages.Validation_InvalidPaymentType);
                            //	throw VtodException.CreateValidationException(Messages.Validation_InvalidPaymentType);// new ValidationException(Messages.General_InvalidPaymentType);
                            //}
                            #endregion

                        }
                        else if (request.Payments.Payments.First().MiscChargeOrder != null && !string.IsNullOrWhiteSpace(request.Payments.Payments.First().MiscChargeOrder.OriginalIssuePlace) && request.Payments.Payments.First().MiscChargeOrder.OriginalIssuePlace.ToUpper() == "CONSUMER")
                        {
                            #region Authorize
                            //var membershipController = new UDI.SDS.MembershipController(tokenVTOD, TrackTime);
                            var membershipDomain = new UDI.VTOD.Domain.Membership.MembershipDomain();
                            //if (membershipDomain.IsUserInPaymentRole(tokenVTOD.Username, Common.DTO.Enum.PaymentType.ChargedByConsumer.ToString()))
                            //{
                            //	tbp.PaymentType = Common.DTO.Enum.PaymentType.ChargedByConsumer;
                            //}
                            //else
                            //{
                            //	result = false;
                            //	logger.Warn(Messages.Validation_InvalidPaymentType);
                            //	throw VtodException.CreateValidationException(Messages.Validation_InvalidPaymentType);// ValidationException(Messages.General_InvalidPaymentType);
                            //}
                            #endregion

                        }
                        else
                        {
                            result = false;
                            logger.Warn(Messages.Validation_CreditCard);
                            throw VtodException.CreateValidationException(Messages.Validation_CreditCard); // new ValidationException(Messages.General_BadCreditCard);
                        }
                    }
                    #endregion
                    if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility, "ValidateBookingRequest_ValidatePaymentInfo");
                    swEachPart.Stop();
                    logger.DebugFormat("Validate payment Id Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                    swEachPart.Restart();
                    #region Get Gratuity
                    try
                    {
                        var rateQuialifier = request.GroundReservations.First().RateQualifiers.First();
                        if (rateQuialifier.SpecialInputs != null)
                        {
                            var gratuities = rateQuialifier.SpecialInputs.Where(s => s.Name.ToLower() == "gratuity").ToList();

                            if (gratuities != null && gratuities.Any())
                            {
                                tbp.Gratuity = gratuities.First().Value;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error("van:Gratuity", ex);
                    }
                    #endregion
                    if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility, "ValidateBookingRequest_ GetGratuity");
                    swEachPart.Stop();
                    logger.DebugFormat("Get Gratuity Duration:{0} ms.", swEachPart.ElapsedMilliseconds);


                    swEachPart.Restart();
                    #region Get PromisedETA
                    try
                    {
                        var rateQuialifier = request.GroundReservations.First().RateQualifiers.First();
                        if (rateQuialifier.SpecialInputs != null)
                        {
                            var promisedetas = rateQuialifier.SpecialInputs.Where(s => s.Name.ToLower() == "promisedeta").ToList();

                            if (promisedetas != null && promisedetas.Any())
                            {
                                tbp.PromisedETA = promisedetas.First().Value.ToInt32();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error("van:PromisedETA", ex);
                    }
                    #endregion
                    if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility, "ValidateBookingRequest_ PromisedETA");
                    swEachPart.Stop();
                    logger.DebugFormat("Get PromisedETA Duration:{0} ms.", swEachPart.ElapsedMilliseconds);
                    swEachPart.Restart();


                    #region Create this trip into database
                    if (result)
                    {
                        tbp.Trip = CreateNewTrip(tbp, tokenVTOD.Username);
                        //tbp.Trip = trip;
                        //tbp.Trip.van_trip = trip;
                    }
                    else
                    {
                        result = false;
                        var vtodException = VtodException.CreateException(ExceptionType.Van, 2001);
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException;// new ValidationException(Messages.van_Common_UnableCreateTrip);
                    }
                    #endregion
                    if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility, "ValidateBookingRequest_ GreateTripInDatabase");
                    swEachPart.Stop();
                    logger.DebugFormat("CreateNewTrip in db  Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                }
                else
                {
                    result = false;
                    var vtodException = VtodException.CreateFieldRequiredValidationException("GroundReservation");
                    logger.Warn(vtodException.ExceptionMessage.Message);
                    throw vtodException; // new ValidationException(Messages.van_Common_InvalidReservation);
                }
                #endregion

                sw.Stop();
                logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);


                #region Create van log
                if (result)
                {
                    if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
                    {
                        WriteVanLog(tbp.Trip.Id, tbp.serviceAPIID, VanServiceMethodType.Book, sw.ElapsedMilliseconds, string.Empty, string.Empty, null, null);
                    }
                }
                else
                {
                    result = false;
                    var vtodException = Common.DTO.VtodException.CreateException(ExceptionType.Van, 2001);// ("CardNumber and CardNumberID");
                    logger.Warn(vtodException.ExceptionMessage.Message);
                    throw vtodException; // new ValidationException(Messages.Validation_NullPayment);
                }
                #endregion

                #region Track
                if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility, "ValidateBookingRequest");
                #endregion


                return result;
            }

            public bool ValidateMTDataBookingRequest(TokenRS tokenVTOD, OTA_GroundBookRQ request, long? vtodTXId,
                out VanBookingParameter tbp, van_fleet fleet)
            {
                bool result = true;
                Stopwatch sw = new Stopwatch();
                sw.Start();
                tbp = new VanBookingParameter();
                tbp.Fleet = fleet;
                Stopwatch swEachPart = new Stopwatch();

                #region Track

                if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Domain, "Call");

                #endregion

                #region Validate Reservation

                if (request.GroundReservations.Any())
                {
                    GroundReservation reservation = request.GroundReservations.FirstOrDefault();

                    #region Set token, request

                    tbp.tokenVTOD = tokenVTOD;
                    tbp.request = request;

                    #endregion

                    #region Set Source and Device and ref

                    if (request.TPA_Extensions != null && !string.IsNullOrWhiteSpace(request.TPA_Extensions.Source))
                    {
                        tbp.Source = request.TPA_Extensions.Source;
                    }

                    if (request.TPA_Extensions != null && !string.IsNullOrWhiteSpace(request.TPA_Extensions.Device))
                    {
                        tbp.Device = request.TPA_Extensions.Device;
                    }

                    if (!string.IsNullOrWhiteSpace(request.EchoToken))
                    {
                        tbp.Ref = request.EchoToken;
                    }

                    #endregion

                    swEachPart.Restart();

                    #region Validate Member Info

                    //As of this version, all requester should pass memberID.
                    //We need to modify this based on some others who do not pass memberID
                    if (request.TPA_Extensions == null && string.IsNullOrWhiteSpace(request.TPA_Extensions.MemberID))
                    {
                        result = false;
                        var vtodException = VtodException.CreateFieldRequiredValidationException("MemberID");
                        //ExceptionType.Van, 2001);
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException;
                        // new ValidationException(Messages.van_Common_UnableToFindFleetByAddressOrAirportInfo);
                    }
                    else
                    {
                        tbp.MemberID = request.TPA_Extensions.MemberID.ToInt32();
                    }

                    #endregion

                    if (TrackTime != null)
                        TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility,
                            "ValidateBookingRequest_ValidateMemberInfo");
                    swEachPart.Stop();
                    logger.DebugFormat("Validate member Info Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                    swEachPart.Restart();

                    #region Validate Customer

                    if (reservation.Passenger != null)
                    {
                        //Name
                        if (reservation.Passenger.Primary != null)
                        {
                            if (reservation.Passenger.Primary.PersonName != null)
                            {
                                if (string.IsNullOrWhiteSpace(reservation.Passenger.Primary.PersonName.GivenName) ||
                                    string.IsNullOrWhiteSpace(reservation.Passenger.Primary.PersonName.Surname))
                                {
                                    result = false;
                                    var vtodException =
                                        VtodException.CreateFieldRequiredValidationException(
                                            "PassengerGivenName|PassengerSurname");
                                    logger.Warn(vtodException.ExceptionMessage.Message);
                                    throw vtodException; // new ValidationException(Messages.van_Common_InvalidReservation);
                                }
                                else
                                {
                                    tbp.FirstName = reservation.Passenger.Primary.PersonName.GivenName.Trim();
                                    tbp.LastName = reservation.Passenger.Primary.PersonName.Surname.Trim();
                                    logger.Info("First name and last name are valid.");
                                }
                            }
                            else
                            {
                                result = false;
                                var vtodException = VtodException.CreateFieldRequiredValidationException("PassengerName");
                                logger.Warn(vtodException.ExceptionMessage.Message);
                                throw vtodException; // new ValidationException(Messages.van_Common_InvalidReservation);
                            }

                            //Telephone
                            if (reservation.Passenger.Primary.Telephones.Any())
                            {
                                Telephone p = reservation.Passenger.Primary.Telephones.FirstOrDefault();
                                string areacode = ExtractDigits(p.AreaCityCode);
                                string phonenumber = ExtractDigits(p.PhoneNumber);
                                if (areacode.Length.Equals(3) && phonenumber.Length.Equals(7))
                                {
                                    tbp.PhoneNumber = string.Format("{0}{1}", areacode, phonenumber);
                                    logger.Info("Phone number(s) is/are valid.");
                                }
                                else
                                {
                                    result = false;
                                    var vtodException = VtodException.CreateFieldFormatValidationException("Telephone",
                                        "10 digits");
                                    logger.Warn(vtodException.ExceptionMessage.Message);
                                    throw vtodException; // new ValidationException(Messages.van_Common_InvalidReservation);
                                }
                            }
                            else
                            {
                                result = false;
                                var vtodException = VtodException.CreateFieldRequiredValidationException("Telephones");
                                logger.Warn(vtodException.ExceptionMessage.Message);
                                throw vtodException; // new ValidationException(Messages.van_Common_InvalidReservation);
                            }
                            if (reservation.Passenger.Primary.Emails.Any())
                            {
                                Email e = reservation.Passenger.Primary.Emails.FirstOrDefault();


                                string emailPattern = @"^(?("")("".+?(?<!\\)""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
                                                     @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]*\.)+[a-zA-Z0-9][\-a-zA-Z0-9]{0,22}[a-zA-Z0-9]))$";

                                if (!string.IsNullOrWhiteSpace(e.Value) && Regex.IsMatch(e.Value, emailPattern, RegexOptions.IgnorePatternWhitespace))
                                {
                                    tbp.EmailAddress = e.Value;
                                    logger.Info(Messages.ECar_Common_ValidReservationPassengerPrimaryTelephones);
                                }
                            }
                            if (result)
                            {
                                ////Create or GetCustomer
                                //van_customer customer = GetvanCustomer(reservation);
                                //tbp.Customer = customer;
                                logger.InfoFormat("Customer firstname={0}, lastname={1}, phonenumber={2}", tbp.FirstName,
                                    tbp.LastName, tbp.PhoneNumber);
                            }
                            else
                            {
                                result = false;
                                var vtodException = VtodException.CreateException(ExceptionType.Van, 2008);
                                logger.Warn(vtodException.ExceptionMessage.Message);
                                throw vtodException; // new ValidationException(Messages.van_Common_InvalidReservation);
                            }
                        }
                        else
                        {
                            result = false;
                            var vtodException = VtodException.CreateFieldRequiredValidationException("PassengerPrimary");
                            logger.Warn(vtodException.ExceptionMessage.Message);
                            throw vtodException; // new ValidationException(Messages.van_Common_InvalidReservation);
                        }
                    }
                    else
                    {
                        result = false;
                        var vtodException = VtodException.CreateFieldRequiredValidationException("Passenger");
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException; // new ValidationException(Messages.van_Common_InvalidReservation);
                    }

                    #endregion

                    if (TrackTime != null)
                        TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility, "ValidateBookingRequest_ValidateCustomer");
                    swEachPart.Stop();
                    logger.DebugFormat("Validate Customer Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                    swEachPart.Restart();

                    #region Validate Pickup Address

                    if (reservation.Service != null)
                    {
                        if (reservation.Service.Location != null)
                        {
                            if (reservation.Service.Location.Pickup != null)
                            {
                                if (((reservation.Service.Location.Pickup.Address != null) &&
                                     (reservation.Service.Location.Pickup.AirportInfo != null)) ||
                                    ((reservation.Service.Location.Pickup.Address == null) &&
                                     (reservation.Service.Location.Pickup.AirportInfo == null)))
                                {
                                    result = false;
                                    logger.Warn(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
                                    throw new ValidationException(
                                        Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
                                }
                                else if ((reservation.Service.Location.Pickup.Address != null) &&
                                         (reservation.Service.Location.Pickup.AirportInfo == null))
                                {

                                    if (
                                        (string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude) ||
                                         (string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.Longitude))) &&
                                        (string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.StreetNmbr) ||
                                         string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.AddressLine) ||
                                         string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CityName) ||
                                         string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.PostalCode) ||
                                         string.IsNullOrWhiteSpace(
                                             reservation.Service.Location.Pickup.Address.StateProv.StateCode) ||
                                         string.IsNullOrWhiteSpace(
                                             reservation.Service.Location.Pickup.Address.CountryName.Code)))
                                    {
                                        result = false;
                                        logger.Warn(Messages.Validation_LocationDetail);
                                        throw new ValidationException(Messages.Validation_LocationDetail);
                                    }
                                    else if (
                                    (!string.IsNullOrWhiteSpace(
                                        reservation.Service.Location.Pickup.Address.Longitude) &&
                                     (!string.IsNullOrWhiteSpace(
                                         reservation.Service.Location.Pickup.Address.Longitude))) &&
                                    (string.IsNullOrWhiteSpace(
                                        reservation.Service.Location.Pickup.Address.StreetNmbr) ||
                                     string.IsNullOrWhiteSpace(
                                         reservation.Service.Location.Pickup.Address.AddressLine) ||
                                     string.IsNullOrWhiteSpace(
                                         reservation.Service.Location.Pickup.Address.CityName) ||
                                     string.IsNullOrWhiteSpace(
                                         reservation.Service.Location.Pickup.Address.PostalCode) ||
                                     string.IsNullOrWhiteSpace(
                                         reservation.Service.Location.Pickup.Address.StateProv.StateCode) ||
                                     string.IsNullOrWhiteSpace(
                                         reservation.Service.Location.Pickup.Address.CountryName.Code)))
                                    {
                                        tbp.PickupAddressOnlyContainsLatAndLong = true;
                                        tbp.PickupAddressType = AddressType.Address;
                                        tbp.PickupAddress = ConvertAddress(reservation.Service.Location.Pickup);
                                    }
                                    else if (
                                        !string.IsNullOrWhiteSpace(
                                            reservation.Service.Location.Pickup.Address.AddressLine) &&
                                        !string.IsNullOrWhiteSpace(reservation.Service.Location.Pickup.Address.CityName) &&
                                        !string.IsNullOrWhiteSpace(
                                            reservation.Service.Location.Pickup.Address.PostalCode) &&
                                        !string.IsNullOrWhiteSpace(
                                            reservation.Service.Location.Pickup.Address.StateProv.StateCode) &&
                                        !string.IsNullOrWhiteSpace(
                                            reservation.Service.Location.Pickup.Address.CountryName.Code))
                                    {
                                        //Address
                                        Map.DTO.Address pickupAddress = null;
                                        //string pickupAddressStr = GetAddressString(reservation.Service.Location.Pickup.Address);
                                        logger.Info("Skip MAP API Validation.");
                                        pickupAddress = ConvertAddress(reservation.Service.Location.Pickup);
                                        tbp.PickupAddressType = AddressType.Address;
                                        tbp.PickupAddress = pickupAddress;
                                        logger.Info("Pickup AddressType: Address");
                                    }
                                    else
                                    {
                                        result = false;
                                        var vtodException = VtodException.CreateException(ExceptionType.Van, 1);
                                        logger.Warn(vtodException.ExceptionMessage.Message);
                                        // Messages.van_Common_UnknownError);
                                        throw vtodException;
                                    }
                                }
                                else if ((reservation.Service.Location.Pickup.Address == null) &&
                                         (reservation.Service.Location.Pickup.AirportInfo != null))
                                {
                                    //airport
                                    if (((reservation.Service.Location.Pickup.AirportInfo.Arrival != null) &&
                                         (reservation.Service.Location.Pickup.AirportInfo.Departure != null)) ||
                                        ((reservation.Service.Location.Pickup.AirportInfo.Arrival == null) &&
                                         (reservation.Service.Location.Pickup.AirportInfo.Departure == null)))
                                    {
                                        result = false;
                                        logger.Warn(Messages.Validation_ArrivalAndDepartureAirportInfo);
                                        throw VtodException.CreateValidationException(
                                            Messages.Validation_ArrivalAndDepartureAirportInfo);
                                    }
                                    else if (reservation.Service.Location.Pickup.AirportInfo.Arrival != null &&
                                             reservation.Service.Location.Pickup.AirportInfo.Departure == null)
                                    {
                                        //Arrival
                                        tbp.PickupAddressType = AddressType.Airport;
                                        tbp.PickupAirport =
                                            reservation.Service.Location.Pickup.AirportInfo.Arrival.LocationCode;
                                        logger.Info("Pickup AddressType: AirportInfo.Arrival");
                                    }
                                    else if (reservation.Service.Location.Pickup.AirportInfo.Arrival == null &&
                                             reservation.Service.Location.Pickup.AirportInfo.Departure != null)
                                    {
                                        //Depature

                                        tbp.PickupAddressType = AddressType.Airport;
                                        tbp.PickupAirport =
                                            reservation.Service.Location.Pickup.AirportInfo.Departure
                                                .LocationCode;
                                        logger.Info("Pickup AddressType: AirportInfo.Depature");
                                    }

                                    //longitude and latitude
                                    if (result)
                                    {
                                        double? longitude = null;
                                        double? latitude = null;
                                        if (GetLongitudeAndLatitudeForAirport(tbp.PickupAirport, out longitude,
                                            out latitude))
                                        {
                                            tbp.LongitudeForPickupAirport = longitude.Value;
                                            tbp.LatitudeForPickupAirport = latitude.Value;
                                        }
                                        else
                                        {
                                            throw VtodException.CreateException(ExceptionType.Van, 1010);
                                            // new ValidationException(Messages.van_Common_UnableToFindAirportLongitudeAndLatitude);
                                        }
                                    }

                                    //throw new ValidationException(Messages.van_Common_InvalidAirportPickup);
                                }
                            }
                            else
                            {
                                result = false;
                                var vtodException = VtodException.CreateFieldRequiredValidationException("Pickup");
                                logger.Warn(vtodException.ExceptionMessage.Message);
                                throw vtodException; // new ValidationException(Messages.van_Common_InvalidReservation);
                            }
                        }
                        else
                        {
                            result = false;
                            var vtodException =
                                VtodException.CreateFieldRequiredValidationException("Reservation.Service.Location");
                            logger.Warn(vtodException.ExceptionMessage.Message);
                            throw vtodException; // new ValidationException(Messages.van_Common_InvalidReservation);
                        }
                    }
                    else
                    {
                        result = false;
                        var vtodException = VtodException.CreateFieldRequiredValidationException("GroundReservation.Service");
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException; // new ValidationException(Messages.van_Common_InvalidReservation);
                    }

                    #endregion

                    if (TrackTime != null)
                        TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility,
                            "ValidateBookingRequest_ValidatePickupAddress");
                    swEachPart.Stop();
                    logger.DebugFormat("Validate Pickup address Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                    swEachPart.Restart();

                    #region Validate Fleet

                    if (result)
                    {
                        //van_fleet fleet = null;
                        if (tbp.Fleet != null)//GetFleet(tbp.PickupAddressType, tbp.PickupAddress, tbp.PickupAirport, out fleet))
                        {
                            //tbp.Fleet = fleet;
                            logger.InfoFormat("Fleet found. ID={0}", tbp.Fleet.Id);

                            if (tbp.Fleet != null)
                            {
                                tbp.serviceAPIID =
                                    GetvanFleetServiceAPIPreference(tbp.Fleet.Id, VanServiceAPIPreferenceConst.Book).ServiceAPIId;
                            }
                        }
                        else
                        {
                            result = false;
                            var vtodException = VtodException.CreateException(ExceptionType.Van, 1011);
                            logger.Warn(vtodException.ExceptionMessage.Message);
                            throw vtodException;
                            // new ValidationException(Messages.van_Common_UnableToFindFleetByAddressOrAirportInfo);
                        }
                    }
                    else
                    {
                        result = false;
                        var vtodException = VtodException.CreateException(ExceptionType.Van, 2001);
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException;
                        // new ValidationException(Messages.van_Common_UnableToFindFleetByAddressOrAirportInfo);
                    }

                    #endregion

                    if (TrackTime != null)
                        TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility, "ValidateBookingRequest_ValidateFleet");
                    swEachPart.Stop();
                    logger.DebugFormat("Validate fleet Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                    swEachPart.Restart();

                    #region Validate User

                    if (result)
                    {
                        van_fleet_user user = null;
                        if (GetvanFleetUser(tbp.Fleet.Id, tokenVTOD.Username, out user))
                        {
                            tbp.User = user;
                            logger.Info("This user has sufficient privilege.");
                        }
                        else
                        {
                            result = false;
                            var vtodException = VtodException.CreateException(ExceptionType.Van, 102);
                            logger.Warn(vtodException.ExceptionMessage.Message);
                            throw vtodException;
                            // new ValidationException(Messages.van_Common_InsufficientPrivilege(tokenVTOD.Username, tbp.Fleet.Id));
                        }
                    }

                    #endregion

                    if (TrackTime != null)
                        TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility, "ValidateBookingRequest_ValidateUser");
                    swEachPart.Stop();
                    logger.DebugFormat("Validate user Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                    swEachPart.Restart();

                    #region Validate PickupDateTime

                    if (result)
                    {
                        //pickiup now

                        #region determine Pick me up now

                        if (request.TPA_Extensions != null && request.TPA_Extensions.PickMeUpNow.HasValue)
                        {
                            tbp.PickupNow = request.TPA_Extensions.PickMeUpNow.Value;
                        }
                        else
                        {
                            try
                            {
                                tbp.PickupNow =
                                    reservation.Service.Location.Pickup.DateTime.ToDateTime()
                                        .Value.ToUtc(tbp.Fleet.ServerUTCOffset) <= DateTime.UtcNow.AddMinutes(5);
                            }
                            catch
                            {
                            }
                        }

                        #endregion

                        #region Validate Pick me up now against DB fleet

                        bool pickMeUpNow = true;
                        bool pickMeUpLater = true;
                        GetPickMeupOption(tbp.Fleet, out pickMeUpNow, out pickMeUpLater);
                        #region PMUL
                        if ((request.TPA_Extensions == null) || (!request.TPA_Extensions.PickMeUpNow.HasValue) || !request.TPA_Extensions.PickMeUpNow.Value)
                        {

                            if (!string.IsNullOrWhiteSpace(request.GroundReservations.FirstOrDefault().Service.Location.Pickup.DateTime))
                            {
                                bool pmulResult = CheckPMUL(tbp.Fleet, request.GroundReservations.FirstOrDefault().Service.Location.Pickup.DateTime.ToDateTime());
                                logger.InfoFormat("PMUL :{0}:", pmulResult);
                                if (pmulResult == false)
                                    throw VtodException.CreateException(ExceptionType.Van, 2013);

                            }
                        }
                        #endregion
                        if (tbp.PickupNow == pickMeUpNow || !tbp.PickupNow == pickMeUpLater)
                        {
                            logger.InfoFormat("Valid pick me up request. Fleet={0}, Trip={1}", tbp.Fleet.PickMeUpNowOption,
                                tbp.PickupNow);

                        }
                        else
                        {
                            logger.WarnFormat("InValid pick me up request. Fleet={0}, Trip={1}", tbp.Fleet.PickMeUpNowOption,
                                tbp.PickupNow);
                            result = false;
                            if (tbp.PickupNow)
                            {
                                throw VtodException.CreateException(ExceptionType.Van, 2011);
                            }
                            else
                            {
                                throw VtodException.CreateException(ExceptionType.Van, 2012);
                            }
                        }

                        #endregion


                        #region determine pick me up now Time

                        DateTime pickupDateTime;
                        //if (DateTime.TryParseExact(reservation.Service.Location.Pickup.DateTime, ConfigurationManager.AppSettings["DateTimeFormatForPickupAndDropoff"], null, DateTimeStyles.None, out pickupDateTime))
                        if (DateTime.TryParse(reservation.Service.Location.Pickup.DateTime, out pickupDateTime))
                        {
                            tbp.PickupDateTime = pickupDateTime;
                            logger.Info("PickupDateTime is correct");
                        }
                        else if (tbp.PickupNow &&
                                 !DateTime.TryParse(reservation.Service.Location.Pickup.DateTime, out pickupDateTime))
                        {
                            tbp.PickupDateTime = DateTime.UtcNow.FromUtc(tbp.Fleet.ServerUTCOffset);
                            logger.InfoFormat(
                                "There is no Pickup datetime for this request. But it has pickup now value. So the pickup time will set as {0:yyyy/MM/dd HH:mm:ss}.",
                                tbp.PickupDateTime);
                        }
                        else
                        {
                            result = false;
                            var vtodException = VtodException.CreateFieldFormatValidationException("PickupDateTime",
                                "DateTime");
                            logger.Warn(vtodException.ExceptionMessage.Message);
                            throw vtodException;
                            // new ValidationException(Messages.van_Common_InvalidReservationServiceLocationPickupDateTime(reservation.Service.Location.Pickup.DateTime));
                        }

                        #endregion
                    }
                    else
                    {
                        result = false;
                        var vtodException = VtodException.CreateException(ExceptionType.Van, 2001);
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException;
                        // new ValidationException(Messages.van_Common_UnableToFindFleetByAddressOrAirportInfo);
                    }

                    #endregion

                    if (TrackTime != null)
                        TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility,
                            "ValidateBookingRequest_ValidatePickupDateTime");
                    swEachPart.Stop();
                    logger.DebugFormat("Validate PickupTime Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                    swEachPart.Restart();

                    #region Validate duplicated booking trip

                    if (result)
                    {
                        if (IsAbleToBookTrip(tbp))
                        {
                            logger.Info("This trip can be booked.");
                        }
                        else
                        {
                            result = false;
                            var vtodException = VtodException.CreateException(ExceptionType.Van, 2006);
                            logger.Warn(vtodException.ExceptionMessage.Message);
                            throw vtodException; // new ValidationException(Messages.van_Common_DuplicatedTrips);
                        }
                    }
                    else
                    {
                        result = false;
                        var vtodException = VtodException.CreateException(ExceptionType.Van, 2001);
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException;
                    }

                    #endregion

                    if (TrackTime != null)
                        TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility,
                            "ValidateBookingRequest_ValidateDuplicatedBookingTrip");
                    swEachPart.Stop();
                    logger.DebugFormat("Validate duplicated trip Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                    swEachPart.Restart();

                    #region Validate Dropoff Address

                    //DropOff is optional: By_Pouyan
                    if (reservation.Service.Location != null && reservation.Service.Location.Dropoff != null)
                        if (result)
                        {
                            if (reservation.Service != null)
                            {
                                if (reservation.Service.Location != null)
                                {
                                    if (reservation.Service.Location.Dropoff != null)
                                    {
                                        using (VTODEntities context = new VTODEntities())
                                        {
                                            if (((reservation.Service.Location.Dropoff.Address != null) &&
                                                 (reservation.Service.Location.Dropoff.AirportInfo != null)) ||
                                                ((reservation.Service.Location.Dropoff.Address == null) &&
                                                 (reservation.Service.Location.Dropoff.AirportInfo == null)))
                                            {
                                                result = false;
                                                logger.Warn(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
                                                throw VtodException.CreateValidationException(
                                                    Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
                                            }
                                            else if ((reservation.Service.Location.Dropoff.Address != null) &&
                                                     (reservation.Service.Location.Dropoff.AirportInfo == null))
                                            {
                                                //Address
                                                if (
                                                    (string.IsNullOrWhiteSpace(
                                                        reservation.Service.Location.Dropoff.Address.Longitude) ||
                                                     (string.IsNullOrWhiteSpace(
                                                         reservation.Service.Location.Dropoff.Address.Longitude))) &&
                                                    (string.IsNullOrWhiteSpace(
                                                        reservation.Service.Location.Dropoff.Address.StreetNmbr) ||
                                                     string.IsNullOrWhiteSpace(
                                                         reservation.Service.Location.Dropoff.Address.AddressLine) ||
                                                     string.IsNullOrWhiteSpace(
                                                         reservation.Service.Location.Dropoff.Address.CityName) ||
                                                     string.IsNullOrWhiteSpace(
                                                         reservation.Service.Location.Dropoff.Address.PostalCode) ||
                                                     string.IsNullOrWhiteSpace(
                                                         reservation.Service.Location.Dropoff.Address.StateProv.StateCode) ||
                                                     string.IsNullOrWhiteSpace(
                                                         reservation.Service.Location.Dropoff.Address.CountryName.Code)))
                                                //if (string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Location.Dropoff.Address.CountryName.Code))
                                                {
                                                    result = false;
                                                    logger.Warn(Messages.Validation_LocationDetail);
                                                    throw VtodException.CreateValidationException(
                                                        Messages.Validation_LocationDetail);
                                                }
                                                else if (
                                                    (!string.IsNullOrWhiteSpace(
                                                        reservation.Service.Location.Dropoff.Address.Longitude) &&
                                                     (!string.IsNullOrWhiteSpace(
                                                         reservation.Service.Location.Dropoff.Address.Longitude))) &&
                                                    (string.IsNullOrWhiteSpace(
                                                        reservation.Service.Location.Dropoff.Address.StreetNmbr) ||
                                                     string.IsNullOrWhiteSpace(
                                                         reservation.Service.Location.Dropoff.Address.AddressLine) ||
                                                     string.IsNullOrWhiteSpace(
                                                         reservation.Service.Location.Dropoff.Address.CityName) ||
                                                     string.IsNullOrWhiteSpace(
                                                         reservation.Service.Location.Dropoff.Address.PostalCode) ||
                                                     string.IsNullOrWhiteSpace(
                                                         reservation.Service.Location.Dropoff.Address.StateProv
                                                             .StateCode) ||
                                                     string.IsNullOrWhiteSpace(
                                                         reservation.Service.Location.Dropoff.Address.CountryName
                                                             .Code)))
                                                {
                                                    tbp.DropOffAddressOnlyContainsLatAndLong = true;
                                                    tbp.DropOffAddressType = AddressType.Address;
                                                    tbp.DropOffAddress =
                                                        ConvertAddress(reservation.Service.Location.Dropoff);
                                                }
                                                else if (
                                                    !string.IsNullOrWhiteSpace(
                                                        reservation.Service.Location.Dropoff.Address.AddressLine) &&
                                                    !string.IsNullOrWhiteSpace(
                                                        reservation.Service.Location.Dropoff.Address.CityName) &&
                                                    !string.IsNullOrWhiteSpace(
                                                        reservation.Service.Location.Dropoff.Address.PostalCode) &&
                                                    !string.IsNullOrWhiteSpace(
                                                        reservation.Service.Location.Dropoff.Address.StateProv
                                                            .StateCode) &&
                                                    !string.IsNullOrWhiteSpace(
                                                        reservation.Service.Location.Dropoff.Address.CountryName.Code))
                                                {
                                                    Map.DTO.Address dropoffAddress = null;
                                                    string dropoffAddressStr =
                                                        GetAddressString(reservation.Service.Location.Dropoff.Address);
                                                    logger.Info("Skip MAP API Validation.");
                                                    dropoffAddress =
                                                        ConvertAddress(reservation.Service.Location.Dropoff);
                                                    tbp.DropOffAddress = dropoffAddress;
                                                    tbp.DropOffAddressType =
                                                        UDI.VTOD.Domain.Van.Const.AddressType.Address;
                                                    logger.Info("Dropoff AddressType=Address");
                                                }
                                                else
                                                {
                                                    result = false;
                                                    var vtodException =
                                                        VtodException.CreateException(ExceptionType.Van, 1);
                                                    logger.Warn(vtodException.ExceptionMessage.Message);
                                                    throw vtodException;
                                                }
                                            }
                                            else if ((reservation.Service.Location.Dropoff.Address == null) &&
                                                     (reservation.Service.Location.Dropoff.AirportInfo != null))
                                            {

                                                //airport
                                                if (((reservation.Service.Location.Dropoff.AirportInfo.Arrival != null) &&
                                                     (reservation.Service.Location.Dropoff.AirportInfo.Departure !=
                                                      null)) ||
                                                    ((reservation.Service.Location.Dropoff.AirportInfo.Arrival == null) &&
                                                     (reservation.Service.Location.Dropoff.AirportInfo.Departure ==
                                                      null)))
                                                {
                                                    result = false;
                                                    logger.Warn(Messages.Validation_ArrivalAndDepartureAirportInfo);
                                                    throw VtodException.CreateValidationException(
                                                        Messages.Validation_ArrivalAndDepartureAirportInfo);
                                                }
                                                else if (reservation.Service.Location.Dropoff.AirportInfo.Arrival !=
                                                         null &&
                                                         reservation.Service.Location.Dropoff.AirportInfo.Departure ==
                                                         null)
                                                {
                                                    //Arrival
                                                    tbp.DropOffAddressType = AddressType.Airport;
                                                    tbp.DropoffAirport =
                                                        reservation.Service.Location.Dropoff.AirportInfo.Arrival
                                                            .LocationCode;
                                                    logger.Info("Dropoff AddressType: AirportInfo.Arrival");
                                                }
                                                else if (reservation.Service.Location.Dropoff.AirportInfo.Arrival ==
                                                         null &&
                                                         reservation.Service.Location.Dropoff.AirportInfo.Departure !=
                                                         null)
                                                {
                                                    //Depature
                                                    tbp.DropOffAddressType = AddressType.Airport;
                                                    tbp.DropoffAirport =
                                                        reservation.Service.Location.Dropoff.AirportInfo
                                                            .Departure.LocationCode;
                                                    logger.Info("Dropoff AddressType: AirportInfo.Depature");
                                                }

                                                //longitude and latitude
                                                if (result)
                                                {
                                                    double? longitude = null;
                                                    double? latitude = null;
                                                    if (GetLongitudeAndLatitudeForAirport(tbp.DropoffAirport,
                                                        out longitude, out latitude))
                                                    {
                                                        tbp.LongitudeForDropoffAirport = longitude.Value;
                                                        tbp.LatitudeForDropoffAirport = latitude.Value;
                                                    }
                                                    else
                                                    {
                                                        throw VtodException.CreateException(ExceptionType.Van, 1010);
                                                        // new ValidationException(Messages.van_Common_UnableToFindAirportLongitudeAndLatitude);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    result = false;
                                    var vtodException =
                                        VtodException.CreateFieldRequiredValidationException("Reservation.Service.Location");
                                    logger.Warn(vtodException.ExceptionMessage.Message);
                                    throw vtodException; // new ValidationException(Messages.van_Common_InvalidReservation);
                                }
                            }
                            else
                            {
                                result = false;
                                var vtodException =
                                    VtodException.CreateFieldRequiredValidationException("GroundReservation.Service");
                                logger.Warn(vtodException.ExceptionMessage.Message);
                                throw vtodException; // new ValidationException(Messages.van_Common_InvalidReservation);
                            }
                        }
                        else
                        {
                            result = false;
                            var vtodException = VtodException.CreateException(ExceptionType.Van, 2001);
                            logger.Warn(vtodException.ExceptionMessage.Message);
                            throw vtodException;
                            // new ValidationException(Messages.van_Common_UnableToFindFleetByAddressOrAirportInfo);
                        }

                    #endregion

                    if (TrackTime != null)
                        TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility,
                            "ValidateBookingRequest_ValidateDropOffAddress");
                    swEachPart.Stop();
                    logger.DebugFormat("Validate dropoff address Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                    swEachPart.Restart();
                    if (TrackTime != null)
                        TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility,
                            "ValidateBookingRequest_ValidateDropoffDateTime");
                    swEachPart.Stop();
                    logger.DebugFormat("Validate dropoff time Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                    swEachPart.Restart();

                    #region Validate Available address type

                    if (result)
                    {
                        //string pickupAddressType = tbp.PickupAddressType;
                        //string dropoffAddressType = tbp.DropOffAddressType;

                        //verify the address type of pickup address and drop off address
                        UtilityDomain ud = new UtilityDomain();
                        if (tbp.PickupAddressType == AddressType.Address)
                        {
                            if (tbp.PickupAddress.Geolocation == null)
                            {
                                result = false;
                                var vtodException =
                                    VtodException.CreateFieldRequiredValidationException("PickupAddress.Geolocation");
                                logger.Warn(vtodException.ExceptionMessage.Message);
                                throw vtodException;
                            }

                            UtilityGetAirportNameRQ uganRQ = new UtilityGetAirportNameRQ();
                            uganRQ.Address = new Address();
                            uganRQ.Address.Latitude = tbp.PickupAddress.Geolocation.Latitude.ToString();
                            uganRQ.Address.Longitude = tbp.PickupAddress.Geolocation.Longitude.ToString();

                            UtilityGetAirportNameRS uganRS = ud.GetAirportName(tokenVTOD, uganRQ);
                            if (uganRS.Success != null)
                            {
                                if (!string.IsNullOrWhiteSpace(uganRS.AirportName))
                                {
                                    //pickupAddressType = AddressType.Airport;
                                    tbp.PickupAddressType = AddressType.Airport;
                                    tbp.PickupAirport = uganRS.AirportName;
                                    tbp.LongitudeForPickupAirport = uganRQ.Address.Longitude.ToDouble();
                                    tbp.LatitudeForPickupAirport = uganRQ.Address.Longitude.ToDouble();
                                }
                            }

                        }

                        if (tbp.DropOffAddressType == AddressType.Address)
                        {
                            if (tbp.DropOffAddress.Geolocation == null)
                            {
                                result = false;
                                var vtodException =
                                    VtodException.CreateFieldRequiredValidationException("DropOffAddress.Geolocation");
                                logger.Warn(vtodException.ExceptionMessage.Message);
                                throw vtodException;
                            }

                            UtilityGetAirportNameRQ uganRQ = new UtilityGetAirportNameRQ();
                            uganRQ.Address = new Address();
                            uganRQ.Address.Latitude = tbp.DropOffAddress.Geolocation.Latitude.ToString();
                            uganRQ.Address.Longitude = tbp.DropOffAddress.Geolocation.Longitude.ToString();

                            UtilityGetAirportNameRS uganRS = ud.GetAirportName(tokenVTOD, uganRQ);
                            if (uganRS.Success != null)
                            {
                                if (!string.IsNullOrWhiteSpace(uganRS.AirportName))
                                {
                                    //dropoffAddressType = AddressType.Airport;
                                    tbp.DropOffAddressType = AddressType.Airport;
                                    tbp.DropoffAirport = uganRS.AirportName;
                                    tbp.LongitudeForDropoffAirport = uganRQ.Address.Longitude.ToDouble();
                                    tbp.LatitudeForDropoffAirport = uganRQ.Address.Longitude.ToDouble();
                                }
                            }
                        }

                        //check database setting
                        //1. get van_fleet_availableaddresstype
                        using (VTODEntities context = new VTODEntities())
                        {
                            long fleetId = tbp.Fleet.Id;
                            if (context.van_fleet_availableaddresstype.Where(x => fleetId == x.FleetId).Any())
                            {
                                van_fleet_availableaddresstype tfa =
                                    context.van_fleet_availableaddresstype.Where(x => fleetId == x.FleetId)
                                        .Select(x => x)
                                        .FirstOrDefault();
                                //2. check if the type is available or not
                                //van_Common_InvalidAddressTypeForFleet
                                if (AddressType.Address.ToString() == tbp.PickupAddressType &&
                                    AddressType.Address.ToString() == tbp.DropOffAddressType)
                                {
                                    //address to address
                                    if (tfa.AddressToAddress)
                                    {
                                        result = true;
                                    }
                                    else
                                    {
                                        result = false;
                                        throw VtodException.CreateException(ExceptionType.Van, 1013);
                                        //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                    }
                                }
                                else if (AddressType.Address.ToString() == tbp.PickupAddressType &&
                                         AddressType.Airport.ToString() == tbp.DropOffAddressType)
                                {
                                    //address to airport
                                    if (tfa.AddressToAirport)
                                    {
                                        result = true;
                                    }
                                    else
                                    {
                                        result = false;
                                        throw VtodException.CreateException(ExceptionType.Van, 2020);
                                        //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                    }
                                }
                                else if (AddressType.Address.ToString() == tbp.PickupAddressType &&
                                         null == tbp.DropOffAddressType)
                                {
                                    //address to null
                                    if (tfa.AddressToNull)
                                    {
                                        result = true;
                                    }
                                    else
                                    {
                                        result = false;
                                        throw VtodException.CreateException(ExceptionType.Van, 1013);
                                        //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                    }
                                }
                                else if (AddressType.Airport.ToString() == tbp.PickupAddressType &&
                                         AddressType.Address.ToString() == tbp.DropOffAddressType)
                                {
                                    //airport to address
                                    if (tfa.AirportToAddress)
                                    {
                                        result = true;
                                    }
                                    else
                                    {
                                        result = false;
                                        throw VtodException.CreateException(ExceptionType.Van, 2019);
                                        //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                    }
                                }
                                else if (AddressType.Airport.ToString() == tbp.PickupAddressType &&
                                         AddressType.Airport.ToString() == tbp.DropOffAddressType)
                                {
                                    //airport to airport
                                    if (tfa.AirportToAirport)
                                    {
                                        result = true;
                                    }
                                    else
                                    {
                                        result = false;
                                        throw VtodException.CreateException(ExceptionType.Van, 1013);
                                        //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                    }
                                }
                                else if (AddressType.Airport.ToString() == tbp.PickupAddressType &&
                                         null == tbp.DropOffAddressType)
                                {
                                    //airport to null
                                    if (tfa.AirportToNull)
                                    {
                                        result = true;
                                    }
                                    else
                                    {
                                        result = false;
                                        throw VtodException.CreateException(ExceptionType.Van, 1013);
                                        //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                    }
                                }
                            }
                            else
                            {
                                result = false;
                                throw VtodException.CreateException(ExceptionType.Van, 1011);
                                // new ValidationException(Messages.van_Common_UnableToGetAvailableFleets);
                            }
                        }


                    }
                    else
                    {
                        result = false;
                        var vtodException = VtodException.CreateFieldRequiredValidationException("Paymnet");
                        //ExceptionType.Van, 2001);
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException;
                        // new ValidationException(Messages.van_Common_UnableToFindFleetByAddressOrAirportInfo);
                    }

                    #endregion

                    if (TrackTime != null)
                        TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility,
                            "ValidateBookingRequest_ValidateAvailableAddressType");
                    swEachPart.Stop();
                    logger.DebugFormat("Validate available address type Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                    swEachPart.Restart();

                    #region Validate PaymentInfo

                    //As of this version, all requester should pass payment Info.
                    //We need to modify this based on some others who do not pass payementInfo
                    if (request.Payments == null || request.Payments.Payments == null || !request.Payments.Payments.Any())
                    {
                        result = false;
                        var vtodException = Common.DTO.VtodException.CreateFieldRequiredValidationException("Payment");
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException; // new ValidationException(Messages.Validation_NullPayment);
                    }
                    else
                    {
                        if (request.Payments.Payments.First().PaymentCard != null &&
                            request.Payments.Payments.First().Cash == null)
                        {
                            if (request.Payments.Payments.First().PaymentCard.CardNumber == null ||
                                string.IsNullOrWhiteSpace(request.Payments.Payments.First().PaymentCard.CardNumber.ID))
                            {
                                result = false;
                                var vtodException =
                                    Common.DTO.VtodException.CreateFieldRequiredValidationException(
                                        "CardNumber and CardNumberID");
                                logger.Warn(vtodException.ExceptionMessage.Message);
                                throw vtodException; // new ValidationException(Messages.Validation_NullPayment);
                            }
                            else
                            {
                                tbp.CreditCardID = request.Payments.Payments.First().PaymentCard.CardNumber.ID.ToInt32();
                                tbp.PaymentType = Common.DTO.Enum.PaymentType.PaymentCard;


                                #region Pre-Authorze creadit card

                                //1. Get pre auth information
                                //tbp.User.PreAuthCreditCardAmount
                                //tbp.User.PreAuthCreditCardOption
                                //tbp.MemberID



                                //2. Do pre authorization
                                if (tbp.User.PreAuthCreditCardOption)
                                {
                                    PaymentDomain pd = new PaymentDomain();
                                    PreSaleVerificationResponse preAuthResult;
                                    try
                                    {
                                        preAuthResult = pd.PreSaleCardVerification(
                                            tokenVTOD,
                                            tbp.CreditCardID.Value,
                                            tbp.User.PreAuthCreditCardAmount,
                                            tbp.MemberID.Value,
                                            Convert.ToInt32(tbp.Fleet.SDS_FleetMerchantID.Value.ToString())
                                            );

                                    }
                                    catch (Exception ex)
                                    {
                                        logger.Error("Unable to pre auth credit card information");
                                        logger.ErrorFormat("ex={0}", ex.Message);
                                        throw;
                                    }
                                    if (preAuthResult != null)
                                    {
                                        if (preAuthResult.IsPrepaidCard == true)
                                        {
                                            throw VtodException.CreateException(ExceptionType.Van, 2021);
                                        }
                                        if (preAuthResult.TransactionApproved == false)
                                        {
                                            throw VtodException.CreateException(ExceptionType.Van, 2010);
                                        }

                                    }
                                    else
                                        throw VtodException.CreateException(ExceptionType.Van, 2010);
                                }

                                #endregion


                            }
                        }
                        else if (request.Payments.Payments.First().Cash != null &&
                                 request.Payments.Payments.First().PaymentCard == null &&
                                 request.Payments.Payments.First().Cash.CashIndicator == true)
                        {
                            tbp.PaymentType = Common.DTO.Enum.PaymentType.Cash;
                        }
                        else if (request.Payments.Payments.First().MiscChargeOrder != null &&
                                 !string.IsNullOrWhiteSpace(
                                     request.Payments.Payments.First().MiscChargeOrder.OriginalIssuePlace) &&
                                 request.Payments.Payments.First().MiscChargeOrder.OriginalIssuePlace.ToUpper() == "SDS")
                        {
                            #region Authorize

                            //var membershipController = new UDI.SDS.MembershipController(tokenVTOD, TrackTime);
                            var membershipDomain = new UDI.VTOD.Domain.Membership.MembershipDomain();
                            //if (membershipDomain.IsUserInPaymentRole(tokenVTOD.Username, Common.DTO.Enum.PaymentType.ChargedBySDS.ToString()))
                            //{
                            //	tbp.PaymentType = Common.DTO.Enum.PaymentType.ChargedBySDS;
                            //}
                            //else
                            //{
                            //	result = false;
                            //	logger.Warn(Messages.Validation_InvalidPaymentType);
                            //	throw VtodException.CreateValidationException(Messages.Validation_InvalidPaymentType);// new ValidationException(Messages.General_InvalidPaymentType);
                            //}

                            #endregion

                        }
                        else if (request.Payments.Payments.First().MiscChargeOrder != null &&
                                 !string.IsNullOrWhiteSpace(
                                     request.Payments.Payments.First().MiscChargeOrder.OriginalIssuePlace) &&
                                 request.Payments.Payments.First().MiscChargeOrder.OriginalIssuePlace.ToUpper() ==
                                 "CONSUMER")
                        {
                            #region Authorize

                            //var membershipController = new UDI.SDS.MembershipController(tokenVTOD, TrackTime);
                            var membershipDomain = new UDI.VTOD.Domain.Membership.MembershipDomain();
                            //if (membershipDomain.IsUserInPaymentRole(tokenVTOD.Username, Common.DTO.Enum.PaymentType.ChargedByConsumer.ToString()))
                            //{
                            //	tbp.PaymentType = Common.DTO.Enum.PaymentType.ChargedByConsumer;
                            //}
                            //else
                            //{
                            //	result = false;
                            //	logger.Warn(Messages.Validation_InvalidPaymentType);
                            //	throw VtodException.CreateValidationException(Messages.Validation_InvalidPaymentType);// ValidationException(Messages.General_InvalidPaymentType);
                            //}

                            #endregion

                        }
                        else
                        {
                            result = false;
                            logger.Warn(Messages.Validation_CreditCard);
                            throw VtodException.CreateValidationException(Messages.Validation_CreditCard);
                            // new ValidationException(Messages.General_BadCreditCard);
                        }
                    }

                    #endregion

                    if (TrackTime != null)
                        TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility,
                            "ValidateBookingRequest_ValidatePaymentInfo");
                    swEachPart.Stop();
                    logger.DebugFormat("Validate payment Id Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                    swEachPart.Restart();

                    #region Get Gratuity

                    try
                    {
                        var rateQuialifier = request.GroundReservations.First().RateQualifiers.First();
                        if (rateQuialifier.SpecialInputs != null)
                        {
                            var gratuities = rateQuialifier.SpecialInputs.Where(s => s.Name.ToLower() == "gratuity").ToList();

                            if (gratuities != null && gratuities.Any())
                            {
                                tbp.Gratuity = gratuities.First().Value;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error("van:Gratuity", ex);
                    }

                    #endregion

                    if (TrackTime != null)
                        TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility, "ValidateBookingRequest_ GetGratuity");
                    swEachPart.Stop();
                    logger.DebugFormat("Get Gratuity Duration:{0} ms.", swEachPart.ElapsedMilliseconds);


                    swEachPart.Restart();

                    #region Get PromisedETA

                    try
                    {
                        var rateQuialifier = request.GroundReservations.First().RateQualifiers.First();
                        if (rateQuialifier.SpecialInputs != null)
                        {
                            var promisedetas =
                                rateQuialifier.SpecialInputs.Where(s => s.Name.ToLower() == "promisedeta").ToList();

                            if (promisedetas != null && promisedetas.Any())
                            {
                                tbp.PromisedETA = promisedetas.First().Value.ToInt32();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error("van:PromisedETA", ex);
                    }

                    #endregion

                    if (TrackTime != null)
                        TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility, "ValidateBookingRequest_ PromisedETA");
                    swEachPart.Stop();
                    logger.DebugFormat("Get PromisedETA Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                    swEachPart.Restart();

                    #region Check if it's fixed price zone

                    if (result && tbp.Fleet.OverWriteFixedPrice)
                    {
                        var flatRates = new List<VanFlatRateData>(); ;
                        van_fleet_zone pickupZone = null;
                        van_fleet_zone dropoffZone = null;

                        #region Get FlatRate
                        using (VTODEntities context = new VTODEntities())
                        {
                            long pickupfleetID = tbp.Fleet.Id;
                            var tfzfList = context.van_fleet_zone_flatrate.Where(x => x.PickupZone.FleetId == pickupfleetID).Select(x => x).ToList();
                            if (tfzfList.Any())
                            {
                                foreach (var x in tfzfList)
                                {
                                    var tfrd = new VanFlatRateData { Amount = x.Amount, AmountUnit = x.AmountUnit, AppendTime = x.AppendTime, DropoffZoneId = x.DropoffZoneId, Id = x.Id, PickupZoneId = x.PickupZoneId };
                                    tfrd.PickupZoneName = context.van_fleet_zone.Where(xx => xx.Id == tfrd.PickupZoneId).Select(xxx => xxx.Name).FirstOrDefault();
                                    tfrd.DropOffZoneName = context.van_fleet_zone.Where(xx => xx.Id == tfrd.DropoffZoneId).Select(xxx => xxx.Name).FirstOrDefault();
                                    flatRates.Add(tfrd);
                                }
                            }
                        }
                        #endregion

                        #region Pickup zone and dropoff zone
                        if (result)
                        {
                            //pickup zone
                            if ((tbp.PickupAddressType == AddressType.Address && tbp.PickupAddress.Geolocation != null && tbp.PickupAddress.Geolocation.Latitude != 0 && tbp.PickupAddress.Geolocation.Longitude != 0)
                                ||
                                (tbp.PickupAddressType == AddressType.Airport))
                            {
                                van_fleet_zone zone = null;

                                if (tbp.PickupAddressType == AddressType.Address)
                                {
                                    zone = GetFleetZone(tbp.Fleet.Id, tbp.PickupAddress);
                                }
                                else if (tbp.PickupAddressType == AddressType.Airport)
                                {
                                    zone = GetFleetZoneByAirport(tbp.Fleet.Id, tbp.PickupAirport);
                                }

                                if (zone != null)
                                {
                                    pickupZone = zone;
                                }
                            }
                        }
                        else
                        {
                            result = false;
                            logger.Warn("Cannot process pickup zone by previous invalid request");
                        }

                        if (result)
                        {
                            if (reservation.Service.Location.Dropoff != null)
                            {
                                //drop off zone
                                if ((tbp.DropOffAddressType == AddressType.Address && tbp.DropOffAddress.Geolocation != null && tbp.DropOffAddress.Geolocation.Latitude != 0 && tbp.DropOffAddress.Geolocation.Longitude != 0)
                                    ||
                                    (tbp.DropOffAddressType == AddressType.Airport))
                                {
                                    van_fleet_zone zone = null;

                                    if (tbp.DropOffAddressType == AddressType.Address)
                                    {
                                        zone = GetFleetZone(tbp.Fleet.Id, tbp.DropOffAddress);
                                    }
                                    else if (tbp.DropOffAddressType == AddressType.Airport)
                                    {
                                        zone = GetFleetZoneByAirport(tbp.Fleet.Id, tbp.DropoffAirport);
                                    }

                                    if (zone != null)
                                    {
                                        dropoffZone = zone;
                                    }
                                }
                                else
                                {
                                    result = false;
                                    logger.Warn("Cannot process drop off zone by previous invalid request");
                                }
                            }
                        }
                        else
                        {
                            result = false;
                            logger.Warn("Cannot process drop off address by previous invalid request");
                        }
                        #endregion

                        #region Determine FlatRate or Rate
                        if (reservation.Service.Location.Dropoff != null)
                        {
                            long? pickupZoneId = null;
                            long? dropoffZoneId = null;

                            if ((pickupZone != null) && (dropoffZone != null))
                            {
                                pickupZoneId = pickupZone.Id;
                                dropoffZoneId = dropoffZone.Id;
                            }

                            if ((result) && pickupZoneId.HasValue && dropoffZoneId.HasValue && (flatRates.Any())
                                && flatRates.Any(x => x.PickupZoneId == pickupZoneId.Value && x.DropoffZoneId == dropoffZoneId.Value))
                            {
                                //flatrate
                                tbp.IsFixedPrice = true;

                                #region Do calculation for flatrate

                                //caculate flatrate
                                var tfzf = flatRates.Where(x => x.PickupZoneId == pickupZoneId && x.DropoffZoneId == dropoffZoneId).Select(x => x).FirstOrDefault();

                                tbp.FixedPrice = tfzf.Amount;

                                logger.Info("Flat rate found");

                                #endregion
                            }
                            else
                            {
                                tbp.IsFixedPrice = false;
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        tbp.IsFixedPrice = false;
                    }

                    #endregion

                    if (TrackTime != null)
                        TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility,
                            "ValidateBookingRequest_CheckFixedPriceZone");
                    swEachPart.Stop();
                    logger.DebugFormat("Check fixed price zone  Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                    swEachPart.Restart();

                    #region Create this trip into database

                    if (result)
                    {
                        tbp.Trip = CreateNewTrip(tbp, tokenVTOD.Username);
                        //tbp.Trip = trip;
                        //tbp.Trip.van_trip = trip;
                    }
                    else
                    {
                        result = false;
                        var vtodException = VtodException.CreateException(ExceptionType.Van, 2001);
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException; // new ValidationException(Messages.van_Common_UnableCreateTrip);
                    }

                    #endregion

                    if (TrackTime != null)
                        TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility,
                            "ValidateBookingRequest_ GreateTripInDatabase");
                    swEachPart.Stop();
                    logger.DebugFormat("CreateNewTrip in db  Duration:{0} ms.", swEachPart.ElapsedMilliseconds);


                }
                else
                {
                    var vtodException = VtodException.CreateFieldRequiredValidationException("GroundReservation");
                    logger.Warn(vtodException.ExceptionMessage.Message);
                    throw vtodException; // new ValidationException(Messages.van_Common_InvalidReservation);
                }

                #endregion

                sw.Stop();
                logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);


                #region Create van log

                if (result)
                {
                    if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
                    {
                        WriteVanLog(tbp.Trip.Id, tbp.serviceAPIID, VanServiceMethodType.Book, sw.ElapsedMilliseconds,
                            string.Empty, string.Empty, null, null);
                    }
                }
                else
                {
                    result = false;
                    var vtodException = Common.DTO.VtodException.CreateException(ExceptionType.Van, 2001);
                    // ("CardNumber and CardNumberID");
                    logger.Warn(vtodException.ExceptionMessage.Message);
                    throw vtodException; // new ValidationException(Messages.Validation_NullPayment);
                }

                #endregion

                #region Track

                if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility, "ValidateBookingRequest");

                #endregion


                return result;
            }

            public bool ValidateStatusRequest(TokenRS tokenVTOD, OTA_GroundResRetrieveRQ request, out VanStatusParameter tsp)
            {
                bool result = true;
                Stopwatch sw = new Stopwatch();
                sw.Start();
                tsp = new VanStatusParameter();
                #region Track
                if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Domain, "Call");
                #endregion

                if (request.Reference != null)
                {
                    #region Set token, request
                    tsp.tokenVTOD = tokenVTOD;
                    tsp.request = request;
                    #endregion

                    #region Validate Reference Id
                    if (request.Reference == null || !request.Reference.Any() || string.IsNullOrWhiteSpace(request.Reference.First().ID))
                    {
                        result = false;
                        logger.Warn("Reference.ID is empty.");
                    }
                    else
                    {
                        logger.Info("Request is valid.");
                    }
                    #endregion

                    #region Validate Trip
                    if (result)
                    {
                        vtod_trip t = GetVanTrip(request.Reference.First().ID);
                        if (t != null)
                        {
                            tsp.Trip = t;
                            logger.InfoFormat("Trip found. tripID={0}", request.Reference.First().ID);
                        }
                        else
                        {
                            result = false;
                            logger.WarnFormat("Cannot find this trip. tripID={0}", request.Reference.First().ID);
                        }
                    }
                    else
                    {
                        result = false;
                        logger.Warn("Cannot find trip by invalid referenceId");
                    }
                    #endregion

                    #region Validate Fleet
                    if (result)
                    {
                        van_fleet fleet = null;
                        if (GetFleet(tsp.Trip.van_trip.FleetId, out fleet))
                        {
                            tsp.Fleet = fleet;
                            logger.InfoFormat("Fleet found. ID={0}", fleet.Id);

                            if (fleet != null)
                            {
                                tsp.serviceAPIID = GetvanFleetServiceAPIPreference(fleet.Id, VanServiceAPIPreferenceConst.Status).ServiceAPIId;
                            }
                        }
                        else
                        {
                            result = false;
                            logger.Warn("Unable to find fleet by this fleetID");
                        }
                    }
                    else
                    {
                        result = false;
                        logger.Warn("Unable to find fleet by this fleetID");
                    }
                    #endregion

                    #region Validate Fleet User
                    if (result)
                    {
                        van_fleet_user fleet_user = null;
                        if (GetvanFleetUser(tsp.Trip.van_trip.FleetId, tokenVTOD.Username, out fleet_user))
                        {
                            tsp.Fleet_User = fleet_user;
                            logger.Info("This user has sufficient privilege.");

                            tsp.User = GetUser(fleet_user.UserId);
                        }
                        else
                        {
                            result = false;
                            logger.WarnFormat("Insufficient privilege. User={0}, FleetId={1}", tokenVTOD.Username, tsp.Fleet.Id);
                        }
                    }
                    else
                    {
                        result = false;
                        logger.Warn("Cannot validate this user by invalid fleet ");
                    }
                    #endregion

                    #region GetAllTripStatusHistory
                    if (result)
                    {
                        using (VTODEntities context = new VTODEntities())
                        {
                            long tripId = tsp.Trip.Id;
                            tsp.lastTripStatus = context.van_trip_status.Where(x => x.TripID == tripId).Select(x => x).OrderByDescending(x => x.Id).FirstOrDefault();
                        }
                    }
                    #endregion



                }
                else
                {
                    result = false;
                    logger.Info("Reference is null.");
                }

                sw.Stop();
                logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

                #region Create van log
                if (result)
                {
                    if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
                    {
                        WriteVanLog(tsp.Trip.Id, tsp.serviceAPIID, VanServiceMethodType.Status, sw.ElapsedMilliseconds, string.Empty, string.Empty, null, null);
                    }
                }
                else
                {
                    result = false;
                    logger.Warn("Cannot create or get this van log by invalid trip");
                }
                #endregion

                #region Track
                if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility, "ValidateStatusRequest");
                #endregion

                return result;
            }

            public bool ValidateCCSiStatusRequest(CCSi_VanPushedStatus ccsiTps, out VanStatusParameter tsp)
            {
                bool result = true;
                Stopwatch sw = new Stopwatch();
                sw.Start();
                tsp = new VanStatusParameter();
                #region Track
                if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Domain, "Call");
                #endregion

                tsp.Trip = GetVanTripForCCSi(ccsiTps.DispatchTripID, ccsiTps.DispatchFleetID);
                if (tsp.Trip != null)
                {
                    logger.InfoFormat("Trip ID={0}", tsp.Trip.Id);
                    //string unknownStatus=string.Empty;
                    //tsp.=ConvertTripStatusForCCSi(ccsiTps.EventID, ccsiTps.EventStatus, out unknownStatus);

                    //get fleet
                    tsp.Fleet = GetFleet(tsp.Trip.van_trip.FleetId);

                    //get user
                    tsp.User = GetUser(tsp.Trip.UserID);

                    //tsp.tokenVTOD = token;

                }
                else
                {
                    // trip not found 
                    logger.WarnFormat("Unable to find this trip ID={0}", ccsiTps.DispatchTripID);
                    result = false;
                }

                #region Create van log
                if (result)
                {
                    if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
                    {
                        WriteVanLog(tsp.Trip.Id, tsp.serviceAPIID, VanServiceMethodType.Status, sw.ElapsedMilliseconds, string.Empty, string.Empty, null, null);
                    }
                }
                else
                {
                    result = false;
                    logger.Warn("Cannot create or get this van log by invalid trip");
                }
                #endregion

                #region Track
                if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility, "ValidateCCSiStatusRequest");
                #endregion

                return result;
            }

            public bool ValidateCancelRequest(TokenRS tokenVTOD, OTA_GroundCancelRQ request, out VanCancelParameter tcp)
            {
                bool result = true;
                Stopwatch sw = new Stopwatch();
                sw.Start();
                tcp = new VanCancelParameter();

                #region Track
                if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Domain, "Call");
                #endregion

                #region Set token, request
                tcp.tokenVTOD = tokenVTOD;
                tcp.request = request;
                #endregion

                #region Validate Unique ID
                string uid = request.Reservation.UniqueID.FirstOrDefault().ID;
                if (request.Reservation.UniqueID.Any())
                {
                    if (!string.IsNullOrWhiteSpace(uid))
                    {
                        logger.Info("Request is valid.");
                    }
                    else
                    {
                        result = false;
                        logger.Info("request.Reservation.UniqueID.Frist().ID is empty.");
                    }
                }
                else
                {
                    result = false;
                    logger.Info("Reservation.UniqueID is empty.");
                }
                #endregion

                #region Validate Trip
                if (result)
                {
                    vtod_trip t = GetVanTrip(uid);
                    if (t != null)
                    {
                        tcp.Trip = t;
                        logger.InfoFormat("Trip found. tripID={0}", uid);
                    }
                    else
                    {
                        result = false;
                        logger.WarnFormat("Cannot find this trip. tripID={0}", uid);
                    }
                }
                else
                {
                    result = false;
                    logger.Warn("Cannot find trip by invalid referenceId");
                }
                #endregion

                #region Validate Fleet
                if (result)
                {
                    van_fleet fleet = null;
                    if (GetFleet(tcp.Trip.van_trip.FleetId, out fleet))
                    {
                        tcp.Fleet = fleet;
                        logger.InfoFormat("Fleet found. ID={0}", fleet.Id);

                        if (fleet != null)
                        {
                            tcp.serviceAPIID = GetvanFleetServiceAPIPreference(fleet.Id, VanServiceAPIPreferenceConst.Cancel).ServiceAPIId;
                        }
                    }
                    else
                    {
                        result = false;
                        logger.Warn("Unable to find fleet by this fleetID");
                    }
                }
                else
                {
                    result = false;
                    logger.Warn("Unable to find fleet by this fleetID");
                }
                #endregion

                #region Validate User
                if (result)
                {
                    van_fleet_user user = null;
                    if (GetvanFleetUser(tcp.Trip.van_trip.FleetId, tokenVTOD.Username, out user))
                    {
                        tcp.User = user;
                        logger.Info("This user has sufficient privilege.");
                    }
                    else
                    {
                        result = false;
                        logger.WarnFormat("Insufficient privilege. User={0}, FleetId={1}", tokenVTOD.Username, tcp.Fleet.Id);
                    }
                }
                else
                {
                    result = false;
                    logger.Warn("Cannot validate this user by invalid fleet ");
                }
                #endregion

                #region Validate customer
                if (result)
                {


                    //van_customer tc = null;
                    //if (GetvanCustomer(tcp.Trip.CustomerId, out tc))
                    //{
                    //    tcp.Customer = tc;
                    //}
                    //else
                    //{
                    //    result = false;
                    //    logger.WarnFormat("Cannot find customer by this customerId={0}", tcp.Trip.CustomerId);
                    //}
                }
                else
                {
                    result = false;
                    logger.Warn("Cannot find customer by Invalid trip");
                }
                #endregion

                sw.Stop();
                logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

                #region Create van log
                if (result)
                {
                    if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
                    {
                        WriteVanLog(tcp.Trip.Id, tcp.serviceAPIID, VanServiceMethodType.Cancel, sw.ElapsedMilliseconds, string.Empty, string.Empty, null, null);
                    }
                }
                else
                {
                    result = false;
                    logger.Warn("Cannot create or get this van log by invalid trip");
                }
                #endregion

                #region Track
                if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility, "ValidateCancelRequest");
                #endregion

                return result;
            }

            public bool ValidateGetVehicleInfoRequest(TokenRS tokenVTOD, OTA_GroundAvailRQ request, out VanGetVehicleInfoParameter tgvip)
            {
                bool result = true;
                Stopwatch sw = new Stopwatch();
                sw.Start();
                tgvip = new VanGetVehicleInfoParameter();


                #region Track
                if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Domain, "Call");
                #endregion

                #region Set token, request
                tgvip.tokenVTOD = tokenVTOD;
                tgvip.request = request;
                #endregion


                if (request.Service != null)
                {
                    if (request.Service.Pickup != null)
                    {
                        #region Validate longtitude, latitude
                        if (request.Service.Pickup.Address != null)
                        {
                            if (!string.IsNullOrWhiteSpace(request.Service.Pickup.Address.Latitude) && !string.IsNullOrWhiteSpace(request.Service.Pickup.Address.Longitude))
                            {
                                double longtitue = 0;
                                double latitude = 0;
                                if (double.TryParse(request.Service.Pickup.Address.Longitude, out longtitue) && double.TryParse(request.Service.Pickup.Address.Latitude, out latitude))
                                {
                                    tgvip.Longtitude = longtitue;
                                    tgvip.Latitude = latitude;
                                }
                                else
                                {
                                    result = false;
                                    logger.Warn("Invalid longtitude and latittude");
                                }
                            }
                            else
                            {
                                result = false;
                                logger.Warn("Invalid longtitude and latittude");
                            }
                        }
                        else
                        {
                            result = false;
                            logger.Warn("Cannot find any longtitude or latitude from address");
                        }
                        #endregion

                        #region Validate Pickup Address
                        if (result)
                        {
                            Pickup_Dropoff_Stop stop = request.Service.Pickup;

                            if ((stop.Address != null) && (stop.AirportInfo == null))
                            {
                                if (!string.IsNullOrWhiteSpace(stop.Address.CountryName.Code) && !string.IsNullOrWhiteSpace(stop.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(stop.Address.CityName) && !string.IsNullOrWhiteSpace(stop.Address.PostalCode))
                                {
                                    Map.DTO.Address pickupAddress = new Map.DTO.Address { Country = stop.Address.CountryName.Code, State = stop.Address.StateProv.StateCode, City = stop.Address.CityName, ZipCode = stop.Address.PostalCode, Geolocation = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(stop.Address.Latitude), Longitude = Convert.ToDecimal(stop.Address.Longitude) } };
                                    tgvip.PickupAddressType = UDI.VTOD.Domain.Van.Const.AddressType.Address;
                                    tgvip.PickupAddress = pickupAddress;
                                }
                                else if (!string.IsNullOrWhiteSpace(stop.Address.CountryName.Code) && !string.IsNullOrWhiteSpace(stop.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(stop.Address.CityName))
                                {
                                    Map.DTO.Address pickupAddress = new Map.DTO.Address { Country = stop.Address.CountryName.Code, State = stop.Address.StateProv.StateCode, City = stop.Address.CityName, Geolocation = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(stop.Address.Latitude), Longitude = Convert.ToDecimal(stop.Address.Longitude) } };
                                    tgvip.PickupAddressType = UDI.VTOD.Domain.Van.Const.AddressType.Address;
                                    tgvip.PickupAddress = pickupAddress;

                                }
                                else if (!string.IsNullOrWhiteSpace(stop.Address.LocationName))
                                {
                                    #region Not support now
                                    //tgvip.PickupAddressType = UDI.VTOD.Domain.van.Const.AddressType.LocationName;
                                    //tgvip.PickupLandmark = stop.Address.LocationName;
                                    #endregion

                                    result = false;
                                    logger.Warn("Cannot use locationName from address.");
                                }
                                else
                                {
                                    result = false;
                                    logger.Warn("Cannot find any address/locationName from address.");
                                }
                            }
                            else if ((stop.Address == null) && (stop.AirportInfo != null))
                            {
                                if ((stop.AirportInfo.Arrival != null) && (stop.AirportInfo.Departure == null))
                                {
                                    #region Not support now
                                    //tgvip.PickupAddressType = UDI.VTOD.Domain.van.Const.AddressType.AirportInfo;
                                    //tgvip.PickupLandmark = stop.AirportInfo.Arrival.LocationCode;
                                    #endregion

                                    result = false;
                                    logger.Warn("Cannot use aiportinfo for address");
                                }
                                else if ((stop.AirportInfo.Arrival == null) && (stop.AirportInfo.Departure != null))
                                {
                                    #region Not support now
                                    //tgvip.PickupAddressType = UDI.VTOD.Domain.van.Const.AddressType.AirportInfo;
                                    //tgvip.PickupLandmark = stop.AirportInfo.Departure.LocationCode;
                                    #endregion

                                    result = false;
                                    logger.Warn("Cannot use aiportinfo for address");
                                }
                                else if ((stop.AirportInfo.Arrival != null) && (stop.AirportInfo.Departure != null))
                                {
                                    result = false;
                                    logger.Warn("This OTA request contains both AirportInfo.Arrival and AirportInfo.Departure");
                                }
                                else
                                {
                                    result = false;
                                    logger.Warn("Cannot find any arrival aor depature from AirportInfo");
                                }
                            }
                            else
                            {
                                //find address by longtitude and latitude
                                Map.DTO.Address pickupAddress = null;
                                if (!LookupAddress(tgvip.Longtitude, tgvip.Latitude, out pickupAddress))
                                {
                                    logger.WarnFormat("Cannot find longtitude={0} and latitude={1}", tgvip.Longtitude, tgvip.Latitude);
                                    result = false;
                                }

                                tgvip.PickupAddressType = UDI.VTOD.Domain.Van.Const.AddressType.Address;
                                tgvip.PickupAddress = pickupAddress;
                            }
                        }
                        else
                        {
                            result = false;
                        }
                        #endregion

                        #region Validate Fleet
                        if (result)
                        {
                            van_fleet fleet = null;
                            if (GetFleet(tgvip.PickupAddressType, tgvip.PickupAddress, tgvip.PickupLandmark, out fleet))
                            {
                                tgvip.Fleet = fleet;
                                if (tgvip.Fleet.UDI33DNI != null)
                                {
                                    tgvip.APIInformation = tgvip.Fleet.UDI33DNI.ToString();
                                }
                                else if ((tgvip.Fleet.CCSiFleetId != null) && (tgvip.Fleet.CCSiSource != null))
                                {
                                    tgvip.APIInformation = string.Format("{0}_{1}", tgvip.Fleet.CCSiSource, tgvip.Fleet.CCSiFleetId);
                                }
                                logger.InfoFormat("Fleet found. ID={0}", fleet.Id);

                                if (fleet != null)
                                {
                                    var serviceAPIPreference = GetvanFleetServiceAPIPreference(fleet.Id, VanServiceAPIPreferenceConst.GetVehicleInfo);
                                    tgvip.serviceAPIID = serviceAPIPreference.ServiceAPIId;
                                    tgvip.extendedServiceAPIID = serviceAPIPreference.ExtendedServiceAPIId;
                                }
                            }
                            else
                            {
                                result = false;
                                logger.Warn("Unable to find fleet by this address or landmark.");
                            }
                        }
                        else
                        {
                            result = false;
                            logger.Warn("Unable to find fleet by invalid address or landmark.");
                        }
                        #endregion

                        #region Validate Zone
                        if (result)
                        {
                            //Default: Circle
                            if (request.Service.Pickup.Address.TPA_Extensions != null)
                            {
                                #region Validate Polygon Type
                                if (request.Service.Pickup.Address.TPA_Extensions.Zone != null)
                                {
                                    if (request.Service.Pickup.Address.TPA_Extensions.Zone.Circle != null)
                                    {
                                        tgvip.Radius = double.Parse(request.Service.Pickup.Address.TPA_Extensions.Zone.Circle.Radius.ToString());
                                        tgvip.PolygonType = PolygonTypeConst.Circle;
                                    }

                                    if (string.IsNullOrWhiteSpace(tgvip.PolygonType))
                                    {
                                        result = false;
                                        logger.Warn("Unable to find the polygon area for GetVehicleInfo");
                                    }
                                }
                                else
                                {
                                    result = false;
                                    logger.Warn("request.TPA_Extensions.Zone");
                                }
                                #endregion

                                #region Validate Vehicle status
                                if (request.Service.Pickup.Address.TPA_Extensions.Vehicles != null)
                                {
                                    if (request.Service.Pickup.Address.TPA_Extensions.Vehicles.Items.Any())
                                    {
                                        //tgvip.VehicleStausList = request.Service.Pickup.Address.TPA_Extensions.Vehicles.Items;
                                        if (!"all".Equals(request.Service.Pickup.Address.TPA_Extensions.Vehicles.Items.FirstOrDefault().VehicleStatus.ToLower()))
                                        {
                                            Vehicle v = new Vehicle { VehicleStatus = request.Service.Pickup.Address.TPA_Extensions.Vehicles.Items.FirstOrDefault().VehicleStatus.ToString() };
                                            tgvip.VehicleStausList.Add(v);
                                        }
                                        else
                                        {
                                            logger.Info("Vehicle status = ALL");
                                        }
                                    }
                                    else
                                    {
                                        result = false;
                                        logger.Warn("request.Service.Pickup.Address.TPA_Extensions.Vehicles.Items is empty");
                                    }

                                }
                                else
                                {
                                    result = false;
                                    logger.Info("Unable to find request.TPA_Extensions.Vehicles");
                                }

                                #endregion
                            }
                            else
                            {
                                result = false;
                                logger.Warn("Unable to find request.TPA_Extensions");
                            }
                        }
                        else
                        {
                            result = false;
                            logger.Warn("Cannot get polygon type by invalid request");
                        }
                        #endregion

                        //#region GetVehicleStatus
                        //if (result)
                        //{
                        //    if (request.Service.Pickup.Address.TPA_Extensions != null)
                        //    {
                        //        if (request.Service.Pickup.Address.TPA_Extensions.Vehicles != null)
                        //        {
                        //            if (request.Service.Pickup.Address.TPA_Extensions.Vehicles.Items.Any())
                        //            {
                        //                tgvip.VehicleStatus=request.Service.Pickup.Address.TPA_Extensions.Vehicles.Items.FirstOrDefault().VehicleStatus;
                        //            }
                        //            else {
                        //                tgvip.VehicleStatus = "";
                        //            }
                        //        }
                        //        else {
                        //            tgvip.VehicleStatus = "";
                        //        }
                        //    }
                        //    else {
                        //        tgvip.VehicleStatus = "";
                        //    }
                        //}
                        //else {
                        //    result = false;
                        //    logger.Warn("Cannot get vehicle status by invalid request");
                        //}
                        //#endregion

                    }
                    else
                    {
                        result = false;
                        logger.Warn("request.Service.Pickup is null");
                    }
                }
                else
                {
                    result = false;
                    logger.Warn("request.Service is null");
                }

                sw.Stop();
                logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);


                #region Create van log
                if (result)
                {
                    if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
                    {
                        WriteVanLog(null, tgvip.serviceAPIID, VanServiceMethodType.GetVehicleInfo, sw.ElapsedMilliseconds, string.Empty, string.Empty, null, null);
                    }
                }
                else
                {
                    result = false;
                    logger.Warn("Cannot create vanLog by invalid request");
                }
                #endregion

                #region Track
                if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility, "ValidateGetVehicleInfoRequest");
                #endregion

                return result;
            }

            public bool ValidateGetEstimationRequest(TokenRS tokenVTOD, OTA_GroundAvailRQ reservation,out VanGetEstimationParameter tgep)
            {
                bool result = true;
                Stopwatch sw = new Stopwatch();
                sw.Start();
                tgep = new VanGetEstimationParameter();

                #region Track
                if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Domain, "Call");
                #endregion


                if (reservation.Service != null)
                {

                    #region Token
                    tgep.tokenVTOD = tokenVTOD;
                    #endregion

                    #region Validate Pickup address
                    if (reservation.Service.Pickup != null)
                    {
                        if (((reservation.Service.Pickup.Address != null) && (reservation.Service.Pickup.AirportInfo != null)) || ((reservation.Service.Pickup.Address == null) && (reservation.Service.Pickup.AirportInfo == null)))
                        {
                            result = false;
                            logger.Warn(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
                            throw new ValidationException(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
                        }
                        else if (reservation.Service.Pickup.Address != null && reservation.Service.Pickup.AirportInfo == null)
                        {
                            if ((string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.Longitude) || (string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.CountryName.Code)))
                            {
                                result = false;
                                logger.Warn(Messages.Validation_LocationDetail);
                                throw new ValidationException(Messages.Validation_LocationDetail);
                            }
                            else if ((!string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.Longitude) && (!string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.CountryName.Code)))
                            {
                                tgep.PickupAddressOnlyContainsLatAndLong = true;
                                MapService ms = new MapService();
                                Map.DTO.Geolocation g = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(reservation.Service.Pickup.Address.Latitude), Longitude = Convert.ToDecimal(reservation.Service.Pickup.Address.Longitude) };
                                string error = "";
                                var addressList = ms.GetAddressesFromGeoLocationWithProximity(g, out error);

                                if (string.IsNullOrWhiteSpace(error) && addressList.Any())
                                {
                                    var add = addressList.FirstOrDefault();

                                    reservation.Service.Pickup.Address.StreetNmbr = add.StreetNo;
                                    reservation.Service.Pickup.Address.AddressLine = add.Street;
                                    reservation.Service.Pickup.Address.BldgRoom = add.ApartmentNo;
                                    reservation.Service.Pickup.Address.CityName = add.City;
                                    reservation.Service.Pickup.Address.PostalCode = add.ZipCode;
                                    reservation.Service.Pickup.Address.StateProv = new StateProv { StateCode = add.State };
                                    reservation.Service.Pickup.Address.CountryName = new CountryName { Code = add.Country };
                                    reservation.Service.Pickup.Address.StreetNmbr = add.StreetNo;
                                    reservation.Service.Pickup.Address.StreetNmbr = add.StreetNo;

                                    tgep.PickupAddressType = UDI.VTOD.Domain.Van.Const.AddressType.Address;
                                    tgep.PickupAddress = ConvertAddress(reservation.Service.Pickup);
                                    logger.Info("Pickup AddressType: Address");

                                }
                                else
                                {
                                    result = false;
                                    logger.Warn(Messages.Validation_LocationDetail);
                                    throw new ValidationException(Messages.Validation_LocationDetail);
                                }



                            }
                            else if (!string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.AddressLine) && !string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.CityName) && !string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.PostalCode) && !string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.CountryName.Code))
                            {
                                //Address
                                Map.DTO.Address pickupAddress = null;
                                string pickupAddressStr = GetAddressString(reservation.Service.Pickup.Address);
                                //if (reservation.Service.Pickup.Address.TPA_Extensions != null)
                                //{
                                //	if (reservation.Service.Pickup.Address.TPA_Extensions.AddressValidationRequired.HasValue ? reservation.Service.Pickup.Address.TPA_Extensions.AddressValidationRequired.Value : false)
                                //	{
                                //		if (!LookupAddress(reservation.Service.Pickup, pickupAddressStr, out pickupAddress))
                                //		{
                                //			result = false;
                                //			logger.WarnFormat(Messages.Validation_LocationDetailByMapApi);
                                //			throw new ValidationException(Messages.Validation_LocationDetailByMapApi);
                                //		}
                                //	}
                                //	else
                                //	{
                                //		if (!string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.Longitude) && !string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.Latitude))
                                //		{
                                //			logger.Info("Skip MAP API Validation.");
                                //			pickupAddress = ConvertAddress(reservation.Service.Pickup);
                                //		}
                                //		else
                                //		{
                                //			result = false;
                                //			logger.WarnFormat(Messages.Validation_LocationNoLatlon);
                                //			throw new ValidationException(Messages.Validation_LocationNoLatlon);
                                //		}
                                //	}
                                //}
                                //else
                                //{
                                logger.Info("Skip MAP API Validation.");
                                pickupAddress = ConvertAddress(reservation.Service.Pickup);
                                //}
                                tgep.PickupAddressType = UDI.VTOD.Domain.Van.Const.AddressType.Address;
                                tgep.PickupAddress = pickupAddress;
                                logger.Info("Pickup AddressType: Address");
                            }
                            else
                            {
                                result = false;
                                var vtodException = VtodException.CreateException(ExceptionType.Van, 1);
                                logger.Warn(vtodException.ExceptionMessage.Message);
                                throw vtodException;
                            }
                        }
                        else if ((reservation.Service.Pickup.Address == null) && (reservation.Service.Pickup.AirportInfo != null))
                        {
                            //airport
                            if (((reservation.Service.Pickup.AirportInfo.Arrival != null) && (reservation.Service.Pickup.AirportInfo.Departure != null)) || ((reservation.Service.Pickup.AirportInfo.Arrival == null) && (reservation.Service.Pickup.AirportInfo.Departure == null)))
                            {
                                result = false;
                                logger.Warn(Messages.Validation_ArrivalAndDepartureAirportInfo);
                                throw VtodException.CreateValidationException(Messages.Validation_ArrivalAndDepartureAirportInfo);
                            }
                            else if (reservation.Service.Pickup.AirportInfo.Arrival != null && reservation.Service.Pickup.AirportInfo.Departure == null)
                            {
                                //Arrival
                                tgep.PickupAddressType = AddressType.Airport;
                                tgep.PickupAirport = reservation.Service.Pickup.AirportInfo.Arrival.LocationCode;
                                logger.Info("Pickup AddressType: AirportInfo.Arrival");
                            }
                            else if (reservation.Service.Pickup.AirportInfo.Arrival == null && reservation.Service.Pickup.AirportInfo.Departure != null)
                            {
                                //Depature

                                tgep.PickupAddressType = AddressType.Airport;
                                tgep.PickupAirport = reservation.Service.Pickup.AirportInfo.Departure.LocationCode;
                                logger.Info("Pickup AddressType: AirportInfo.Depature");
                            }

                            //longitude and latitude
                            if (result)
                            {
                                double? longitude = null;
                                double? latitude = null;
                                if (GetLongitudeAndLatitudeForAirport(tgep.PickupAirport, out longitude, out latitude))
                                {
                                    tgep.LongitudeForPickupAirport = longitude.Value;
                                    tgep.LatitudeForPickupAirport = latitude.Value;
                                }
                                else
                                {
                                    throw VtodException.CreateException(ExceptionType.Van, 1010);// new ValidationException(Messages.van_Common_UnableToFindAirportLongitudeAndLatitude);
                                }
                            }

                            //throw new ValidationException(Messages.van_Common_InvalidAirportPickup);
                        }


                    }
                    else
                    {
                        result = false;
                        logger.Warn("request.Service.Pickup is null");
                    }

                    #endregion

                    #region Validate Pickup Fleet
                    if (result)
                    {
                        van_fleet fleet = null;
                        if (GetFleet(tgep.PickupAddressType, tgep.PickupAddress, tgep.PickupAirport, out fleet))
                        {
                            tgep.Fleet = fleet;
                            logger.InfoFormat("Fleet found. ID={0}", fleet.Id);
                        }
                        else
                        {
                            result = false;
                            logger.Warn("Unable to find fleet by this address or landmark.");
                        }
                    }
                    else
                    {
                        result = false;
                        logger.Warn("Unable to find fleet by invalid address or landmark.");
                    }
                    #endregion

                    #region Get Rate and FlatRate
                    if (result)
                    {
                        using (VTODEntities context = new VTODEntities())
                        {
                            long pickupfleetID = tgep.Fleet.Id;
                            //find Info
                            //van_rate
                            tgep.Info_Rate = context.van_fleet_rate.Where(x => x.FleetId == pickupfleetID).Select(x => x).FirstOrDefault();

                            //List<van_flatrate>

                            //if (context.van_fleet_zone_flatrate.Where(x => x.FleetId == pickupfleetID).Any())
                            List<van_fleet_zone_flatrate> tfzfList = context.van_fleet_zone_flatrate.Where(x => x.PickupZone.FleetId == pickupfleetID).Select(x => x).ToList();

                            if (tfzfList.Any())
                            {
                                tgep.Info_FlatRates = new List<VanFlatRateData>();
                                //foreach (van_fleet_flatrate x in context.van_fleet_zone_flatrate.Where(x => x.FleetId == pickupfleetID).Select(x => x).ToList())
                                foreach (van_fleet_zone_flatrate x in tfzfList)
                                {
                                    VanFlatRateData tfrd = new VanFlatRateData { Amount = x.Amount, AmountUnit = x.AmountUnit, AppendTime = x.AppendTime, DropoffZoneId = x.DropoffZoneId, Id = x.Id, PickupZoneId = x.PickupZoneId };
                                    tfrd.PickupZoneName = context.van_fleet_zone.Where(xx => xx.Id == tfrd.PickupZoneId).Select(xxx => xxx.Name).FirstOrDefault();
                                    tfrd.DropOffZoneName = context.van_fleet_zone.Where(xx => xx.Id == tfrd.DropoffZoneId).Select(xxx => xxx.Name).FirstOrDefault();
                                    tgep.Info_FlatRates.Add(tfrd);
                                }
                            }
                        }
                    }

                    #endregion

                    #region Validate Drop off address
                    if (result)
                    {
                        //drop off address can be address or airportInfo
                        if (reservation.Service.Dropoff != null)
                        {
                            using (VTODEntities context = new VTODEntities())
                            {
                                if (((reservation.Service.Dropoff.Address != null) && (reservation.Service.Dropoff.AirportInfo != null)) || ((reservation.Service.Dropoff.Address == null) && (reservation.Service.Dropoff.AirportInfo == null)))
                                {
                                    result = false;
                                    logger.Warn(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
                                    throw VtodException.CreateValidationException(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
                                }
                                else if ((reservation.Service.Dropoff.Address != null) && (reservation.Service.Dropoff.AirportInfo == null))
                                {
                                    //Address
                                    if ((string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.Longitude) || (string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.CountryName.Code)))
                                    {
                                        result = false;
                                        logger.Warn(Messages.Validation_LocationDetail);
                                        throw VtodException.CreateValidationException(Messages.Validation_LocationDetail);
                                    }
                                    else if ((!string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.Longitude) && (!string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.CountryName.Code)))
                                    {
                                        tgep.DropOffAddressOnlyContainsLatAndLong = true;
                                        MapService ms = new MapService();
                                        Map.DTO.Geolocation g = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(reservation.Service.Dropoff.Address.Latitude), Longitude = Convert.ToDecimal(reservation.Service.Dropoff.Address.Longitude) };
                                        string error = "";
                                        var addressList = ms.GetAddressesFromGeoLocationWithProximity(g, out error);

                                        if (string.IsNullOrWhiteSpace(error) && addressList.Any())
                                        {
                                            var add = addressList.FirstOrDefault();

                                            reservation.Service.Dropoff.Address.StreetNmbr = add.StreetNo;
                                            reservation.Service.Dropoff.Address.AddressLine = add.Street;
                                            reservation.Service.Dropoff.Address.BldgRoom = add.ApartmentNo;
                                            reservation.Service.Dropoff.Address.CityName = add.City;
                                            reservation.Service.Dropoff.Address.PostalCode = add.ZipCode;
                                            reservation.Service.Dropoff.Address.StateProv = new StateProv { StateCode = add.State };
                                            reservation.Service.Dropoff.Address.CountryName = new CountryName { Code = add.Country };
                                            reservation.Service.Dropoff.Address.StreetNmbr = add.StreetNo;
                                            reservation.Service.Dropoff.Address.StreetNmbr = add.StreetNo;

                                            tgep.DropOffAddressType = UDI.VTOD.Domain.Van.Const.AddressType.Address;
                                            tgep.DropoffAddress = ConvertAddress(reservation.Service.Dropoff);
                                            logger.Info("Dropoff AddressType: Address");

                                        }
                                        else
                                        {
                                            result = false;
                                            logger.Warn(Messages.Validation_LocationDetail);
                                            throw new ValidationException(Messages.Validation_LocationDetail);
                                        }



                                    }
                                    else if (!string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.AddressLine) && !string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.CityName) && !string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.PostalCode) && !string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.CountryName.Code))
                                    {
                                        Map.DTO.Address dropoffAddress = null;
                                        string dropoffAddressStr = GetAddressString(reservation.Service.Dropoff.Address);
                                        //if (reservation.Service.Dropoff.Address.TPA_Extensions != null)
                                        //{
                                        //	if (reservation.Service.Dropoff.Address.TPA_Extensions.AddressValidationRequired.HasValue ? reservation.Service.Dropoff.Address.TPA_Extensions.AddressValidationRequired.Value : false)
                                        //	{
                                        //		if (!LookupAddress(reservation.Service.Dropoff, dropoffAddressStr, out dropoffAddress))
                                        //		{
                                        //			result = false;
                                        //			logger.WarnFormat(Messages.Validation_LocationDetailByMapApi);
                                        //			throw VtodException.CreateValidationException(Messages.Validation_LocationDetailByMapApi);
                                        //		}
                                        //	}
                                        //	else
                                        //	{
                                        //		if (!string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.Longitude) && !string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.Latitude))
                                        //		{
                                        //			logger.Info("Skip MAP API Validation.");
                                        //			dropoffAddress = ConvertAddress(reservation.Service.Dropoff);
                                        //		}
                                        //		else
                                        //		{
                                        //			result = false;
                                        //			logger.WarnFormat(Messages.Validation_LocationNoLatlon);
                                        //			throw new ValidationException(Messages.Validation_LocationNoLatlon);
                                        //		}
                                        //	}
                                        //}
                                        //else
                                        //{
                                        logger.Info("Skip MAP API Validation.");
                                        dropoffAddress = ConvertAddress(reservation.Service.Dropoff);
                                        //}

                                        tgep.DropoffAddress = dropoffAddress;
                                        tgep.DropOffAddressType = UDI.VTOD.Domain.Van.Const.AddressType.Address;
                                        logger.Info("Dropoff AddressType=Address");
                                    }
                                    else
                                    {
                                        result = false;
                                        var vtodException = VtodException.CreateException(ExceptionType.Van, 1);
                                        logger.Warn(vtodException.ExceptionMessage.Message);
                                        throw vtodException;
                                    }
                                }
                                else if ((reservation.Service.Dropoff.Address == null) && (reservation.Service.Dropoff.AirportInfo != null))
                                {

                                    //airport
                                    if (((reservation.Service.Dropoff.AirportInfo.Arrival != null) && (reservation.Service.Dropoff.AirportInfo.Departure != null)) || ((reservation.Service.Dropoff.AirportInfo.Arrival == null) && (reservation.Service.Dropoff.AirportInfo.Departure == null)))
                                    {
                                        result = false;
                                        logger.Warn(Messages.Validation_ArrivalAndDepartureAirportInfo);
                                        throw VtodException.CreateValidationException(Messages.Validation_ArrivalAndDepartureAirportInfo);
                                    }
                                    else if (reservation.Service.Dropoff.AirportInfo.Arrival != null && reservation.Service.Dropoff.AirportInfo.Departure == null)
                                    {
                                        //Arrival
                                        tgep.DropOffAddressType = AddressType.Airport;
                                        tgep.DropoffAirport = reservation.Service.Dropoff.AirportInfo.Arrival.LocationCode;
                                        logger.Info("Dropoff AddressType: AirportInfo.Arrival");
                                    }
                                    else if (reservation.Service.Dropoff.AirportInfo.Arrival == null && reservation.Service.Dropoff.AirportInfo.Departure != null)
                                    {
                                        //Depature
                                        tgep.DropOffAddressType = AddressType.Airport;
                                        tgep.DropoffAirport = reservation.Service.Dropoff.AirportInfo.Departure.LocationCode;
                                        logger.Info("Dropoff AddressType: AirportInfo.Depature");
                                    }

                                    //longitude and latitude
                                    if (result)
                                    {
                                        double? longitude = null;
                                        double? latitude = null;
                                        if (GetLongitudeAndLatitudeForAirport(tgep.DropoffAirport, out longitude, out latitude))
                                        {
                                            tgep.LongitudeForDropoffAirport = longitude.Value;
                                            tgep.LatitudeForDropoffAirport = latitude.Value;
                                        }
                                        else
                                        {
                                            throw VtodException.CreateException(ExceptionType.Van, 1010);// new ValidationException(Messages.van_Common_UnableToFindAirportLongitudeAndLatitude);
                                        }
                                    }
                                }
                            }
                        }
                        //	else
                        //	{
                        //		tgep.DropoffAddressType = AddressType.Empty;
                        //		logger.Info(Messages.van_Common_InvalidReservationServiceLocationDropoff);
                        //		throw new ValidationException(Messages.van_Common_InvalidReservationServiceLocationDropoff);
                        //	}
                    }
                    else
                    {
                        result = false;
                        logger.Warn("Cannot get estimation by previous invalid request ");
                    }
                    #endregion

                    #region Validate Available address type
                    if (result)
                    {
                        //string pickupAddressType = tgep.PickupAddressType;
                        //string dropoffAddressType = tgep.DropOffAddressType;

                        //verify the address type of pickup address and drop off address
                        UtilityDomain ud = new UtilityDomain();
                        if (tgep.PickupAddressType == AddressType.Address.ToString())
                        {
                            UtilityGetAirportNameRQ uganRQ = new UtilityGetAirportNameRQ();
                            uganRQ.Address = new Address();
                            //tgep.PickupAddress.Geolocation = new Map.DTO.Geolocation();
                            uganRQ.Address.Latitude = tgep.PickupAddress.Geolocation.Latitude.ToString();
                            uganRQ.Address.Longitude = tgep.PickupAddress.Geolocation.Longitude.ToString();

                            UtilityGetAirportNameRS uganRS = ud.GetAirportName(tokenVTOD, uganRQ);
                            if (uganRS.Success != null)
                            {
                                if (!string.IsNullOrWhiteSpace(uganRS.AirportName))
                                {
                                    //pickupAddressType = AddressType.Airport;

                                    tgep.PickupAddressType = AddressType.Airport;
                                    tgep.PickupAirport = uganRS.AirportName;
                                    tgep.LongitudeForPickupAirport = uganRQ.Address.Longitude.ToDouble();
                                    tgep.LatitudeForPickupAirport = uganRQ.Address.Longitude.ToDouble();
                                }
                            }

                        }

                        if (tgep.DropOffAddressType == AddressType.Address.ToString())
                        {
                            UtilityGetAirportNameRQ uganRQ = new UtilityGetAirportNameRQ();
                            uganRQ.Address = new Address();
                            //tgep.DropoffAddress.Geolocation = new Map.DTO.Geolocation();
                            uganRQ.Address.Latitude = tgep.DropoffAddress.Geolocation.Latitude.ToString();
                            uganRQ.Address.Longitude = tgep.DropoffAddress.Geolocation.Longitude.ToString();

                            UtilityGetAirportNameRS uganRS = ud.GetAirportName(tokenVTOD, uganRQ);
                            if (uganRS.Success != null)
                            {
                                if (!string.IsNullOrWhiteSpace(uganRS.AirportName))
                                {
                                    //dropoffAddressType = AddressType.Airport;
                                    tgep.DropOffAddressType = AddressType.Airport;
                                    tgep.DropoffAirport = uganRS.AirportName;
                                    tgep.LongitudeForDropoffAirport = uganRQ.Address.Longitude.ToDouble();
                                    tgep.LatitudeForDropoffAirport = uganRQ.Address.Longitude.ToDouble();
                                }
                            }
                        }

                        //check database setting
                        //1. get van_fleet_availableaddresstype
                        using (VTODEntities context = new VTODEntities())
                        {
                            long fleetId = tgep.Fleet.Id;
                            if (context.van_fleet_availableaddresstype.Where(x => fleetId == x.FleetId).Any())
                            {
                                van_fleet_availableaddresstype tfa = context.van_fleet_availableaddresstype.Where(x => fleetId == x.FleetId).Select(x => x).FirstOrDefault();
                                //2. check if the type is available or not
                                //van_Common_InvalidAddressTypeForFleet
                                if (AddressType.Address.ToString() == tgep.PickupAddressType && AddressType.Address.ToString() == tgep.DropOffAddressType)
                                {
                                    //address to address
                                    if (tfa.AddressToAddress)
                                    {
                                        result = true;
                                    }
                                    else
                                    {
                                        result = false;
                                        throw VtodException.CreateException(ExceptionType.Van, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                    }
                                }
                                else if (AddressType.Address.ToString() == tgep.PickupAddressType && AddressType.Airport.ToString() == tgep.DropOffAddressType)
                                {
                                    //address to airport
                                    if (tfa.AddressToAirport)
                                    {
                                        result = true;
                                    }
                                    else
                                    {
                                        result = false;
                                        throw VtodException.CreateException(ExceptionType.Van, 2020); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                    }
                                }
                                else if (AddressType.Address.ToString() == tgep.PickupAddressType && null == tgep.DropOffAddressType)
                                {
                                    //address to null
                                    if (tfa.AddressToNull)
                                    {
                                        result = true;
                                    }
                                    else
                                    {
                                        result = false;
                                        throw VtodException.CreateException(ExceptionType.Van, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                    }
                                }
                                else if (AddressType.Airport.ToString() == tgep.PickupAddressType && AddressType.Address.ToString() == tgep.DropOffAddressType)
                                {
                                    //airport to address
                                    if (tfa.AirportToAddress)
                                    {
                                        result = true;
                                    }
                                    else
                                    {
                                        result = false;
                                        throw VtodException.CreateException(ExceptionType.Van, 2019); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                    }
                                }
                                else if (AddressType.Airport.ToString() == tgep.PickupAddressType && AddressType.Airport.ToString() == tgep.DropOffAddressType)
                                {
                                    //airport to airport
                                    if (tfa.AirportToAirport)
                                    {
                                        result = true;
                                    }
                                    else
                                    {
                                        result = false;
                                        throw VtodException.CreateException(ExceptionType.Van, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                    }
                                }
                                else if (AddressType.Airport.ToString() == tgep.PickupAddressType && null == tgep.DropOffAddressType)
                                {
                                    //airport to null
                                    if (tfa.AirportToNull)
                                    {
                                        result = true;
                                    }
                                    else
                                    {
                                        result = false;
                                        throw VtodException.CreateException(ExceptionType.Van, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                    }
                                }
                            }
                            else
                            {
                                result = false;
                                throw VtodException.CreateException(ExceptionType.Van, 1011);// VtodException.CreateException(ExceptionType.Van, 1011);// new ValidationException(Messages.van_Common_UnableToGetAvailableFleets);
                            }
                        }


                    }
                    else
                    {
                        result = false;
                        var vtodException = Common.DTO.VtodException.CreateFieldRequiredValidationException("Payment");
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException; // new ValidationException(Messages.Validation_NullPayment);
                    }
                    #endregion

                    #region Pickup zone and dropoff zone
                    if (result)
                    {
                        //pickup zone
                        if ((tgep.PickupAddressType == AddressType.Address && tgep.PickupAddress.Geolocation != null && tgep.PickupAddress.Geolocation.Latitude != 0 && tgep.PickupAddress.Geolocation.Longitude != 0)
                            ||
                            (tgep.PickupAddressType == AddressType.Airport))
                        {
                            van_fleet_zone zone = null;

                            if (tgep.PickupAddressType == AddressType.Address)
                            {
                                zone = GetFleetZone(tgep.Fleet.Id, tgep.PickupAddress);
                            }
                            else if (tgep.PickupAddressType == AddressType.Airport)
                            {
                                zone = GetFleetZoneByAirport(tgep.Fleet.Id, tgep.PickupAirport);
                            }

                            if (zone != null)
                            {
                                tgep.PickupZone = zone;
                            }
                        }
                    }
                    else
                    {
                        result = false;
                        logger.Warn("Cannot process pickup zone by previous invalid request");
                    }


                    if (result)
                    {
                        if (reservation.Service.Dropoff != null)
                        {
                            //drop off zone
                            if ((tgep.DropOffAddressType == AddressType.Address && tgep.DropoffAddress.Geolocation != null && tgep.DropoffAddress.Geolocation.Latitude != 0 && tgep.DropoffAddress.Geolocation.Longitude != 0)
                                ||
                                (tgep.DropOffAddressType == AddressType.Airport))
                            {

                                van_fleet_zone zone = null;

                                if (tgep.DropOffAddressType == AddressType.Address)
                                {
                                    zone = GetFleetZone(tgep.Fleet.Id, tgep.DropoffAddress);
                                }
                                else if (tgep.DropOffAddressType == AddressType.Airport)
                                {
                                    zone = GetFleetZoneByAirport(tgep.Fleet.Id, tgep.DropoffAirport);
                                }

                                if (zone != null)
                                {
                                    tgep.DropoffZone = zone;
                                }
                            }
                            else
                            {
                                result = false;
                                logger.Warn("Cannot process drop off zone by previous invalid request");
                            }
                        }
                    }
                    else
                    {
                        result = false;
                        logger.Warn("Cannot process drop off address by previous invalid request");
                    }
                    #endregion

                    #region Determine FlatRate or Rate
                    if (reservation.Service.Dropoff != null)
                    {

                        long? pickupZoneId = null;
                        long? dropoffZoneId = null;

                        if ((tgep.PickupZone != null) && (tgep.DropoffZone != null))
                        {
                            pickupZoneId = tgep.PickupZone.Id;
                            dropoffZoneId = tgep.DropoffZone.Id;
                        }

                        if ((result) && pickupZoneId.HasValue && dropoffZoneId.HasValue && (tgep.Info_FlatRates.Any()) && tgep.Info_FlatRates.Where(x => x.PickupZoneId == pickupZoneId.Value && x.DropoffZoneId == dropoffZoneId.Value).Any())
                        {
                            //flatrate
                            tgep.EstimationType = EstimationResultType.Info_FlatRate;

                            #region Do calculation for flatrate

                            //caculate flatrate
                            van_fleet_zone_flatrate tfzf = tgep.Info_FlatRates.Where(x => x.PickupZoneId == pickupZoneId && x.DropoffZoneId == dropoffZoneId).Select(x => x).FirstOrDefault();

                            tgep.FlatRateEstimation = new FlatRateData();
                            tgep.FlatRateEstimation.CurrencyMode = tfzf.AmountUnit;
                            tgep.FlatRateEstimation.PickupZone = tgep.PickupZone.Name;
                            tgep.FlatRateEstimation.DropoffZone = tgep.DropoffZone.Name;
                            tgep.FlatRateEstimation.EstimationTotalAmount = tfzf.Amount;
                            logger.Info("Flat rate found");

                            #endregion
                        }
                        else
                        {
                            //rate
                            tgep.EstimationType = EstimationResultType.Info_Rate;

                            #region Get distance
                            if (result)
                            {
                                //MapService ms = new MapService();
                                string error = "";

                                string pickupString = "";
                                string dropoffString = "";

                                if (tgep.PickupAddressType == AddressType.Address)
                                {
                                    pickupString = tgep.PickupAddress.FullAddress;
                                }
                                else
                                {
                                    pickupString = string.Format("{0} {1}", tgep.PickupZone.CenterLatitude, tgep.PickupZone.CenterLongitude);
                                }

                                if (tgep.DropOffAddressType == AddressType.Address)
                                {
                                    dropoffString = tgep.DropoffAddress.FullAddress;
                                }
                                else
                                {
                                    if (tgep.DropoffZone!=null)
                                    dropoffString = string.Format("{0} {1}", tgep.DropoffZone.CenterLatitude, tgep.DropoffZone.CenterLongitude);
                                }


                                //
                                MapService ms = new MapService();
                                var pLat = tgep.PickupAddress != null ? tgep.PickupAddress.Geolocation.Latitude.ToDouble() : tgep.PickupZone.CenterLatitude.ToDouble();
                                var pLon = tgep.PickupAddress != null ? tgep.PickupAddress.Geolocation.Longitude.ToDouble() : tgep.PickupZone.CenterLongitude.ToDouble();
                                var dLat = tgep.DropoffAddress != null ? tgep.DropoffAddress.Geolocation.Latitude.ToDouble() : tgep.DropoffZone.CenterLatitude.ToDouble();
                                var dLon = tgep.DropoffAddress != null ? tgep.DropoffAddress.Geolocation.Longitude.ToDouble() : tgep.DropoffZone.CenterLongitude.ToDouble();

                                double d = ms.GetDirectDistanceInMeter(pLat, pLon, dLat, dLon);
                                var rc = new UDI.SDS.RoutingController(tgep.tokenVTOD, null);
                                UDI.SDS.RoutingService.Point start = new UDI.SDS.RoutingService.Point { Latitude = pLat, Longitude = pLon };
                                UDI.SDS.RoutingService.Point end = new UDI.SDS.RoutingService.Point { Latitude = dLat, Longitude = dLon };
                                var rm = rc.GetRouteMetrics(start, end);
                                //vehicle.MinutesAway = result.TotalMinutes;
                                tgep.Distance_Mile = rm.Distance.ToDecimal();


                                //
                                //tgep.Distance_Mile = ms.GetDistance(pickupString, dropoffString, Map.DTO.DistanceUnit.Mile, out error);
                                if (!string.IsNullOrWhiteSpace(error))
                                {
                                    result = false;
                                    logger.Error(error);
                                }

                                if (result)
                                {
                                    //tgep.Distance_Kilometer = ms.GetDistance(pickupString, dropoffString, Map.DTO.DistanceUnit.Kilometer, out error);
                                    tgep.Distance_Kilometer = tgep.Distance_Mile * (1.609344).ToDecimal();
                                    if (!string.IsNullOrWhiteSpace(error))
                                    {
                                        result = false;
                                        logger.Error(error);
                                    }
                                }
                            }
                            else
                            {
                                result = false;
                                logger.Warn("Cannot get estimation by previous invalid request ");
                            }

                            #endregion

                            #region Do caculation for rate
                            //if ((result) && (tgep.Info_Rate != null) && ((tgep.PickupAddressType == AddressType.Address) || (tgep.DropOffAddressType == AddressType.Airport)))
                            //{
                            //caculate rate estimation
                            tgep.RateEstimation = new RateData { CurrencyMode = tgep.Info_Rate.AmountUnit, DistanceUnit = tgep.Info_Rate.DistanceUnit };
                            //tgep.RateEstimation = new RateData ();
                            if (tgep.RateEstimation.DistanceUnit == Map.DTO.DistanceUnit.Mile.ToString())
                            {
                                tgep.RateEstimation.Distance = double.Parse(tgep.Distance_Mile.ToString());

                                //If the distance is greater than InitialDistance
                                //tgep.RateEstimation.EstimationTotalAmount = tgep.Info_Rate.InitialAmount + double.Parse(tgep.Distance_Mile.ToString() - InitialDistance) * tgep.Info_Rate.PerDistanceAmount;
                                //If tgep.Distance_Mile <= InitialDistance  --- > ignore it.
                                //tgep.RateEstimation.EstimationTotalAmount = tgep.Info_Rate.InitialAmount + double.Parse(tgep.Distance_Mile.ToString()) * tgep.Info_Rate.PerDistanceAmount;

                                if (Convert.ToDouble(tgep.Distance_Mile) <= tgep.Info_Rate.InitialFreeDistance)
                                {
                                    tgep.RateEstimation.EstimationTotalAmount = tgep.Info_Rate.InitialAmount + Convert.ToDouble(tgep.Distance_Mile.ToString()) * tgep.Info_Rate.PerDistanceAmount;
                                }
                                else
                                {
                                    tgep.RateEstimation.EstimationTotalAmount = tgep.Info_Rate.InitialAmount + (Convert.ToDouble(tgep.Distance_Mile.ToString()) - tgep.Info_Rate.InitialFreeDistance) * tgep.Info_Rate.PerDistanceAmount;
                                }
                            }
                            else if (tgep.RateEstimation.DistanceUnit == Map.DTO.DistanceUnit.Kilometer.ToString())
                            {
                                //tgep.RateEstimation.Distance = double.Parse(tgep.Distance_Kilometer.ToString());
                                //tgep.RateEstimation.EstimationTotalAmount = tgep.Info_Rate.InitialAmount + double.Parse(tgep.Distance_Kilometer.ToString()) * tgep.Info_Rate.PerDistanceAmount;

                                if (Convert.ToDouble(tgep.Distance_Kilometer) <= tgep.Info_Rate.InitialFreeDistance)
                                {
                                    tgep.RateEstimation.EstimationTotalAmount = tgep.Info_Rate.InitialAmount + Convert.ToDouble(tgep.Distance_Kilometer.ToString()) * tgep.Info_Rate.PerDistanceAmount;
                                }
                                else
                                {
                                    tgep.RateEstimation.EstimationTotalAmount = tgep.Info_Rate.InitialAmount + (Convert.ToDouble(tgep.Distance_Kilometer.ToString()) - tgep.Info_Rate.InitialFreeDistance) * tgep.Info_Rate.PerDistanceAmount;
                                }
                            }
                            //}
                            #endregion

                        }
                    }
                    #endregion

                    sw.Stop();
                    logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
                }

                #region Create van log
                if (result)
                {
                    if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
                    {
                        WriteVanLog(null, VanServiceAPIConst.None, VanServiceMethodType.Estimation, sw.ElapsedMilliseconds, string.Empty, string.Empty, null, null);
                    }
                }
                else
                {
                    result = false;
                    logger.Warn("Cannot create or get this van log by invalid trip");
                }
                #endregion

                #region Track
                if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility, "ValidateGetEstimationRequest");
                #endregion

                return result;
            }

            public bool ValidateMTDataGetEstimationRequest(TokenRS tokenVTOD, OTA_GroundAvailRQ reservation, long? vtodTXId, out VanGetEstimationParameter tgep)
            {
                bool result = true;
                Stopwatch sw = new Stopwatch();
                sw.Start();
                tgep = new VanGetEstimationParameter();

                #region Track
                if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Domain, "Call");
                #endregion

                if (reservation.Service != null)
                {
                    #region Token
                    tgep.tokenVTOD = tokenVTOD;
                    #endregion

                    #region Validate Pickup Datetime
                    if (string.IsNullOrWhiteSpace(reservation.Service.Pickup.DateTime))
                    {
                        var vtodException = VtodException.CreateFieldRequiredValidationException("Pickup datetime");
                        throw vtodException;
                    }
                    #endregion

                    #region Validate Pickup address
                    if (reservation.Service.Pickup != null)
                    {
                        if (((reservation.Service.Pickup.Address != null) && (reservation.Service.Pickup.AirportInfo != null)) || ((reservation.Service.Pickup.Address == null) && (reservation.Service.Pickup.AirportInfo == null)))
                        {
                            result = false;
                            logger.Warn(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
                            throw new ValidationException(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
                        }
                        else if (reservation.Service.Pickup.Address != null && reservation.Service.Pickup.AirportInfo == null)
                        {
                            if ((string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.Longitude) || (string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.CountryName.Code)))
                            {
                                result = false;
                                logger.Warn(Messages.Validation_LocationDetail);
                                throw new ValidationException(Messages.Validation_LocationDetail);
                            }
                            else if ((!string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.Longitude) && (!string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.CountryName.Code)))
                            {
                                tgep.PickupAddressOnlyContainsLatAndLong = true;
                                tgep.PickupAddressType = AddressType.Address;
                                tgep.PickupAddress = ConvertAddress(reservation.Service.Pickup);
                            }
                            else if (!string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.AddressLine) && !string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.CityName) && !string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.PostalCode) && !string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(reservation.Service.Pickup.Address.CountryName.Code))
                            {
                                //Address
                                Map.DTO.Address pickupAddress = null;
                                string pickupAddressStr = GetAddressString(reservation.Service.Pickup.Address);
                                logger.Info("Skip MAP API Validation.");
                                pickupAddress = ConvertAddress(reservation.Service.Pickup);
                                tgep.PickupAddressType = AddressType.Address;
                                tgep.PickupAddress = pickupAddress;
                                logger.Info("Pickup AddressType: Address");
                            }
                            else
                            {
                                result = false;
                                var vtodException = VtodException.CreateException(ExceptionType.Van, 1);
                                logger.Warn(vtodException.ExceptionMessage.Message);
                                throw vtodException;
                            }
                        }
                        else if ((reservation.Service.Pickup.Address == null) && (reservation.Service.Pickup.AirportInfo != null))
                        {
                            //airport
                            if (((reservation.Service.Pickup.AirportInfo.Arrival != null) && (reservation.Service.Pickup.AirportInfo.Departure != null)) || ((reservation.Service.Pickup.AirportInfo.Arrival == null) && (reservation.Service.Pickup.AirportInfo.Departure == null)))
                            {
                                result = false;
                                logger.Warn(Messages.Validation_ArrivalAndDepartureAirportInfo);
                                throw VtodException.CreateValidationException(Messages.Validation_ArrivalAndDepartureAirportInfo);
                            }
                            else if (reservation.Service.Pickup.AirportInfo.Arrival != null && reservation.Service.Pickup.AirportInfo.Departure == null)
                            {
                                //Arrival
                                tgep.PickupAddressType = AddressType.Airport;
                                tgep.PickupAirport = reservation.Service.Pickup.AirportInfo.Arrival.LocationCode;
                                logger.Info("Pickup AddressType: AirportInfo.Arrival");
                            }
                            else if (reservation.Service.Pickup.AirportInfo.Arrival == null && reservation.Service.Pickup.AirportInfo.Departure != null)
                            {
                                //Depature

                                tgep.PickupAddressType = AddressType.Airport;
                                tgep.PickupAirport = reservation.Service.Pickup.AirportInfo.Departure.LocationCode;
                                logger.Info("Pickup AddressType: AirportInfo.Depature");
                            }

                            //longitude and latitude
                            if (result)
                            {
                                double? longitude = null;
                                double? latitude = null;
                                if (GetLongitudeAndLatitudeForAirport(tgep.PickupAirport, out longitude, out latitude))
                                {
                                    tgep.LongitudeForPickupAirport = longitude.Value;
                                    tgep.LatitudeForPickupAirport = latitude.Value;
                                }
                                else
                                {
                                    throw VtodException.CreateException(ExceptionType.Van, 1010);// new ValidationException(Messages.van_Common_UnableToFindAirportLongitudeAndLatitude);
                                }
                            }

                        }
                    }
                    else
                    {
                        result = false;
                        logger.Warn("request.Service.Pickup is null");
                    }

                    #endregion

                    #region Validate Pickup Fleet
                    if (result)
                    {
                        van_fleet fleet = null;
                        if (GetFleet(tgep.PickupAddressType, tgep.PickupAddress, tgep.PickupAirport, out fleet))
                        {
                            tgep.Fleet = fleet;
                            logger.InfoFormat("Fleet found. ID={0}", fleet.Id);
                        }
                        else
                        {
                            result = false;
                            logger.Warn("Unable to find fleet by this address or landmark.");
                        }
                    }
                    else
                    {
                        result = false;
                        logger.Warn("Unable to find fleet by invalid address or landmark.");
                    }
                    #endregion

                    #region Validate Drop off address
                    if (result)
                    {
                        //drop off address can be address or airportInfo
                        if (reservation.Service.Dropoff != null)
                        {
                            using (VTODEntities context = new VTODEntities())
                            {
                                if (((reservation.Service.Dropoff.Address != null) && (reservation.Service.Dropoff.AirportInfo != null)) || ((reservation.Service.Dropoff.Address == null) && (reservation.Service.Dropoff.AirportInfo == null)))
                                {
                                    result = false;
                                    logger.Warn(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
                                    throw VtodException.CreateValidationException(Messages.Validation_LocationContainsBothPickupAddressAndAirportInfo);
                                }
                                else if ((reservation.Service.Dropoff.Address != null) && (reservation.Service.Dropoff.AirportInfo == null))
                                {
                                    //Address
                                    if ((string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.Longitude) || (string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.CountryName.Code)))
                                    {
                                        result = false;
                                        logger.Warn(Messages.Validation_LocationDetail);
                                        throw VtodException.CreateValidationException(Messages.Validation_LocationDetail);
                                    }
                                    else if ((!string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.Longitude) && (!string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.Longitude))) && (string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.StreetNmbr) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.AddressLine) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.CityName) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.PostalCode) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.StateProv.StateCode) || string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.CountryName.Code)))
                                    {
                                        tgep.DropOffAddressOnlyContainsLatAndLong = true;
                                        tgep.DropOffAddressType = AddressType.Address;
                                        tgep.DropoffAddress = ConvertAddress(reservation.Service.Dropoff);
                                    }
                                    else if (!string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.AddressLine) && !string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.CityName) && !string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.PostalCode) && !string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.StateProv.StateCode) && !string.IsNullOrWhiteSpace(reservation.Service.Dropoff.Address.CountryName.Code))
                                    {
                                        Map.DTO.Address dropoffAddress = null;
                                        string dropoffAddressStr = GetAddressString(reservation.Service.Dropoff.Address);
                                        logger.Info("Skip MAP API Validation.");
                                        dropoffAddress = ConvertAddress(reservation.Service.Dropoff);

                                        tgep.DropoffAddress = dropoffAddress;
                                        tgep.DropOffAddressType = AddressType.Address;
                                        logger.Info("Dropoff AddressType=Address");
                                    }
                                    else
                                    {
                                        result = false;
                                        var vtodException = VtodException.CreateException(ExceptionType.Van, 1);
                                        logger.Warn(vtodException.ExceptionMessage.Message);
                                        throw vtodException;
                                    }
                                }
                                else if ((reservation.Service.Dropoff.Address == null) && (reservation.Service.Dropoff.AirportInfo != null))
                                {

                                    //airport
                                    if (((reservation.Service.Dropoff.AirportInfo.Arrival != null) && (reservation.Service.Dropoff.AirportInfo.Departure != null)) || ((reservation.Service.Dropoff.AirportInfo.Arrival == null) && (reservation.Service.Dropoff.AirportInfo.Departure == null)))
                                    {
                                        result = false;
                                        logger.Warn(Messages.Validation_ArrivalAndDepartureAirportInfo);
                                        throw VtodException.CreateValidationException(Messages.Validation_ArrivalAndDepartureAirportInfo);
                                    }
                                    else if (reservation.Service.Dropoff.AirportInfo.Arrival != null && reservation.Service.Dropoff.AirportInfo.Departure == null)
                                    {
                                        //Arrival
                                        tgep.DropOffAddressType = AddressType.Airport;
                                        tgep.DropoffAirport = reservation.Service.Dropoff.AirportInfo.Arrival.LocationCode;
                                        logger.Info("Dropoff AddressType: AirportInfo.Arrival");
                                    }
                                    else if (reservation.Service.Dropoff.AirportInfo.Arrival == null && reservation.Service.Dropoff.AirportInfo.Departure != null)
                                    {
                                        //Depature
                                        tgep.DropOffAddressType = AddressType.Airport;
                                        tgep.DropoffAirport = reservation.Service.Dropoff.AirportInfo.Departure.LocationCode;
                                        logger.Info("Dropoff AddressType: AirportInfo.Depature");
                                    }

                                    //longitude and latitude
                                    if (result)
                                    {
                                        double? longitude = null;
                                        double? latitude = null;
                                        if (GetLongitudeAndLatitudeForAirport(tgep.DropoffAirport, out longitude, out latitude))
                                        {
                                            tgep.LongitudeForDropoffAirport = longitude.Value;
                                            tgep.LatitudeForDropoffAirport = latitude.Value;
                                        }
                                        else
                                        {
                                            throw VtodException.CreateException(ExceptionType.Van, 1010);// new ValidationException(Messages.van_Common_UnableToFindAirportLongitudeAndLatitude);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        result = false;
                        logger.Warn("Cannot get estimation by previous invalid request ");
                    }
                    #endregion

                    #region Validate Available address type
                    if (result)
                    {
                        //string pickupAddressType = tgep.PickupAddressType;
                        //string dropoffAddressType = tgep.DropOffAddressType;

                        //verify the address type of pickup address and drop off address
                        UtilityDomain ud = new UtilityDomain();
                        if (tgep.PickupAddressType == AddressType.Address.ToString())
                        {
                            UtilityGetAirportNameRQ uganRQ = new UtilityGetAirportNameRQ();
                            uganRQ.Address = new Address();
                            //tgep.PickupAddress.Geolocation = new Map.DTO.Geolocation();
                            uganRQ.Address.Latitude = tgep.PickupAddress.Geolocation.Latitude.ToString();
                            uganRQ.Address.Longitude = tgep.PickupAddress.Geolocation.Longitude.ToString();

                            UtilityGetAirportNameRS uganRS = ud.GetAirportName(tokenVTOD, uganRQ);
                            if (uganRS.Success != null)
                            {
                                if (!string.IsNullOrWhiteSpace(uganRS.AirportName))
                                {
                                    //pickupAddressType = AddressType.Airport;

                                    tgep.PickupAddressType = AddressType.Airport;
                                    tgep.PickupAirport = uganRS.AirportName;
                                    tgep.LongitudeForPickupAirport = uganRQ.Address.Longitude.ToDouble();
                                    tgep.LatitudeForPickupAirport = uganRQ.Address.Longitude.ToDouble();
                                }
                            }

                        }

                        if (tgep.DropOffAddressType == AddressType.Address.ToString())
                        {
                            UtilityGetAirportNameRQ uganRQ = new UtilityGetAirportNameRQ();
                            uganRQ.Address = new Address();
                            //tgep.DropoffAddress.Geolocation = new Map.DTO.Geolocation();
                            uganRQ.Address.Latitude = tgep.DropoffAddress.Geolocation.Latitude.ToString();
                            uganRQ.Address.Longitude = tgep.DropoffAddress.Geolocation.Longitude.ToString();

                            UtilityGetAirportNameRS uganRS = ud.GetAirportName(tokenVTOD, uganRQ);
                            if (uganRS.Success != null)
                            {
                                if (!string.IsNullOrWhiteSpace(uganRS.AirportName))
                                {
                                    //dropoffAddressType = AddressType.Airport;
                                    tgep.DropOffAddressType = AddressType.Airport;
                                    tgep.DropoffAirport = uganRS.AirportName;
                                    tgep.LongitudeForDropoffAirport = uganRQ.Address.Longitude.ToDouble();
                                    tgep.LatitudeForDropoffAirport = uganRQ.Address.Longitude.ToDouble();
                                }
                            }
                        }

                        //check database setting
                        //1. get van_fleet_availableaddresstype
                        using (VTODEntities context = new VTODEntities())
                        {
                            long fleetId = tgep.Fleet.Id;
                            if (context.van_fleet_availableaddresstype.Where(x => fleetId == x.FleetId).Any())
                            {
                                van_fleet_availableaddresstype tfa = context.van_fleet_availableaddresstype.Where(x => fleetId == x.FleetId).Select(x => x).FirstOrDefault();
                                //2. check if the type is available or not
                                //van_Common_InvalidAddressTypeForFleet
                                if (AddressType.Address.ToString() == tgep.PickupAddressType && AddressType.Address.ToString() == tgep.DropOffAddressType)
                                {
                                    //address to address
                                    if (tfa.AddressToAddress)
                                    {
                                        result = true;
                                    }
                                    else
                                    {
                                        result = false;
                                        throw VtodException.CreateException(ExceptionType.Van, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                    }
                                }
                                else if (AddressType.Address.ToString() == tgep.PickupAddressType && AddressType.Airport.ToString() == tgep.DropOffAddressType)
                                {
                                    //address to airport
                                    if (tfa.AddressToAirport)
                                    {
                                        result = true;
                                    }
                                    else
                                    {
                                        result = false;
                                        throw VtodException.CreateException(ExceptionType.Van, 2020); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                    }
                                }
                                else if (AddressType.Address.ToString() == tgep.PickupAddressType && null == tgep.DropOffAddressType)
                                {
                                    //address to null
                                    if (tfa.AddressToNull)
                                    {
                                        result = true;
                                    }
                                    else
                                    {
                                        result = false;
                                        throw VtodException.CreateException(ExceptionType.Van, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                    }
                                }
                                else if (AddressType.Airport.ToString() == tgep.PickupAddressType && AddressType.Address.ToString() == tgep.DropOffAddressType)
                                {
                                    //airport to address
                                    if (tfa.AirportToAddress)
                                    {
                                        result = true;
                                    }
                                    else
                                    {
                                        result = false;
                                        throw VtodException.CreateException(ExceptionType.Van, 2019); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                    }
                                }
                                else if (AddressType.Airport.ToString() == tgep.PickupAddressType && AddressType.Airport.ToString() == tgep.DropOffAddressType)
                                {
                                    //airport to airport
                                    if (tfa.AirportToAirport)
                                    {
                                        result = true;
                                    }
                                    else
                                    {
                                        result = false;
                                        throw VtodException.CreateException(ExceptionType.Van, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                    }
                                }
                                else if (AddressType.Airport.ToString() == tgep.PickupAddressType && null == tgep.DropOffAddressType)
                                {
                                    //airport to null
                                    if (tfa.AirportToNull)
                                    {
                                        result = true;
                                    }
                                    else
                                    {
                                        result = false;
                                        throw VtodException.CreateException(ExceptionType.Van, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                    }
                                }
                            }
                            else
                            {
                                result = false;
                                throw VtodException.CreateException(ExceptionType.Van, 1011);// VtodException.CreateException(ExceptionType.Van, 1011);// new ValidationException(Messages.van_Common_UnableToGetAvailableFleets);
                            }
                        }


                    }
                    else
                    {
                        result = false;
                        var vtodException = Common.DTO.VtodException.CreateFieldRequiredValidationException("Payment");
                        logger.Warn(vtodException.ExceptionMessage.Message);
                        throw vtodException; // new ValidationException(Messages.Validation_NullPayment);
                    }
                    #endregion

                    if (result && tgep.Fleet.OverWriteFixedPrice)
                    {
                        #region Get FlatRate
                        using (VTODEntities context = new VTODEntities())
                        {
                            long pickupfleetID = tgep.Fleet.Id;
                            List<van_fleet_zone_flatrate> tfzfList = context.van_fleet_zone_flatrate.Where(x => x.PickupZone.FleetId == pickupfleetID).Select(x => x).ToList();
                            if (tfzfList.Any())
                            {
                                tgep.Info_FlatRates = new List<VanFlatRateData>();
                                foreach (var x in tfzfList)
                                {
                                    var tfrd = new VanFlatRateData { Amount = x.Amount, AmountUnit = x.AmountUnit, AppendTime = x.AppendTime, DropoffZoneId = x.DropoffZoneId, Id = x.Id, PickupZoneId = x.PickupZoneId };
                                    tfrd.PickupZoneName = context.van_fleet_zone.Where(xx => xx.Id == tfrd.PickupZoneId).Select(xxx => xxx.Name).FirstOrDefault();
                                    tfrd.DropOffZoneName = context.van_fleet_zone.Where(xx => xx.Id == tfrd.DropoffZoneId).Select(xxx => xxx.Name).FirstOrDefault();
                                    tgep.Info_FlatRates.Add(tfrd);
                                }
                            }
                        }
                        #endregion

                        #region Pickup zone and dropoff zone
                        if (result)
                        {
                            //pickup zone
                            if ((tgep.PickupAddressType == AddressType.Address && tgep.PickupAddress.Geolocation != null && tgep.PickupAddress.Geolocation.Latitude != 0 && tgep.PickupAddress.Geolocation.Longitude != 0)
                                ||
                                (tgep.PickupAddressType == AddressType.Airport))
                            {
                                van_fleet_zone zone = null;

                                if (tgep.PickupAddressType == AddressType.Address)
                                {
                                    zone = GetFleetZone(tgep.Fleet.Id, tgep.PickupAddress);
                                }
                                else if (tgep.PickupAddressType == AddressType.Airport)
                                {
                                    zone = GetFleetZoneByAirport(tgep.Fleet.Id, tgep.PickupAirport);
                                }

                                if (zone != null)
                                {
                                    tgep.PickupZone = zone;
                                }
                            }
                        }
                        else
                        {
                            result = false;
                            logger.Warn("Cannot process pickup zone by previous invalid request");
                        }

                        if (result)
                        {
                            if (reservation.Service.Dropoff != null)
                            {
                                //drop off zone
                                if ((tgep.DropOffAddressType == AddressType.Address && tgep.DropoffAddress.Geolocation != null && tgep.DropoffAddress.Geolocation.Latitude != 0 && tgep.DropoffAddress.Geolocation.Longitude != 0)
                                    ||
                                    (tgep.DropOffAddressType == AddressType.Airport))
                                {
                                    van_fleet_zone zone = null;

                                    if (tgep.DropOffAddressType == AddressType.Address)
                                    {
                                        zone = GetFleetZone(tgep.Fleet.Id, tgep.DropoffAddress);
                                    }
                                    else if (tgep.DropOffAddressType == AddressType.Airport)
                                    {
                                        zone = GetFleetZoneByAirport(tgep.Fleet.Id, tgep.DropoffAirport);
                                    }

                                    if (zone != null)
                                    {
                                        tgep.DropoffZone = zone;
                                    }
                                }
                                else
                                {
                                    result = false;
                                    logger.Warn("Cannot process drop off zone by previous invalid request");
                                }
                            }
                        }
                        else
                        {
                            result = false;
                            logger.Warn("Cannot process drop off address by previous invalid request");
                        }
                        #endregion

                        #region Determine FlatRate or Rate
                        if (reservation.Service.Dropoff != null)
                        {
                            long? pickupZoneId = null;
                            long? dropoffZoneId = null;

                            if ((tgep.PickupZone != null) && (tgep.DropoffZone != null))
                            {
                                pickupZoneId = tgep.PickupZone.Id;
                                dropoffZoneId = tgep.DropoffZone.Id;
                            }

                            if ((result) && pickupZoneId.HasValue && dropoffZoneId.HasValue && tgep.Info_FlatRates != null && tgep.Info_FlatRates.Any()
                                && tgep.Info_FlatRates.Any(x => x.PickupZoneId == pickupZoneId.Value && x.DropoffZoneId == dropoffZoneId.Value))
                            {
                                //flatrate
                                tgep.EstimationType = EstimationResultType.Info_FlatRate;

                                #region Do calculation for flatrate

                                //caculate flatrate
                                van_fleet_zone_flatrate tfzf = tgep.Info_FlatRates.Where(x => x.PickupZoneId == pickupZoneId && x.DropoffZoneId == dropoffZoneId).Select(x => x).FirstOrDefault();

                                tgep.FlatRateEstimation = new FlatRateData
                                {
                                    CurrencyMode = tfzf.AmountUnit,
                                    PickupZone = tgep.PickupZone.Name,
                                    DropoffZone = tgep.DropoffZone.Name,
                                    EstimationTotalAmount = tfzf.Amount
                                };
                                logger.Info("Flat rate found");

                                #endregion
                            }
                            else
                            {
                                //rate
                                tgep.EstimationType = EstimationResultType.Info_Rate;
                            }
                        }
                        #endregion
                    }

                    sw.Stop();
                    logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
                }

                #region Create van log
                if (result)
                {
                    if (bool.Parse(ConfigurationManager.AppSettings["IsDetailDebug"]))
                    {
                        WriteVanLog(null, VanServiceAPIConst.None, VanServiceMethodType.Estimation, sw.ElapsedMilliseconds, string.Empty, string.Empty, null, null);
                    }
                }
                else
                {
                    result = false;
                    logger.Warn("Cannot create or get this van log by invalid trip");
                }
                #endregion

                #region Track
                if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility, "ValidateGetEstimationRequest");
                #endregion

                return result;
            }
            public bool UpdateFlatRate(Int64 tripId, bool IsFlatRate)
            {
                vtod_trip trip = null;
                using (VTODEntities context = new VTODEntities())
                {
                    trip = context.vtod_trip.Where(x => x.Id == tripId).FirstOrDefault();
                    trip.IsFlatRate = IsFlatRate;
                    context.SaveChanges();
                }
                return false;
            }
            public decimal? DetermineOverwriteFare(VanStatusParameter tsp, decimal? longtitude, decimal? latitude, decimal? dispatchFare)
            {
                decimal? finalFareWithoutGratuity = dispatchFare;
                Stopwatch sw = new Stopwatch();
                sw.Start();

                try
                {

                    if (!longtitude.HasValue || !latitude.HasValue)
                    {
                        //get longtitude and latitude from last trip status
                        using (VTODEntities context = new VTODEntities())
                        {
                            if (context.van_trip_status.Where(x => x.TripID == tsp.Trip.Id && x.VehicleLongitude != null && x.VehicleLatitude != null).Select(x => x).OrderByDescending(x => x.Id).Any())
                            {
                                van_trip_status tts = context.van_trip_status.Where(x => x.TripID == tsp.Trip.Id && x.VehicleLongitude != null && x.VehicleLatitude != null).Select(x => x).OrderByDescending(x => x.Id).FirstOrDefault();
                                longtitude = tts.VehicleLongitude;
                                latitude = tts.VehicleLatitude;
                            }
                        }

                        //if there is no trip status from DB, just use dispatch fare
                        if (!longtitude.HasValue || !latitude.HasValue)
                        {
                            finalFareWithoutGratuity = dispatchFare;
                        }

                    }

                    //Get Flat Rate
                    if (longtitude.HasValue && latitude.HasValue && tsp.Trip.van_trip.PickupLongitude.HasValue && tsp.Trip.van_trip.PickupLatitude.HasValue && tsp.Fleet.OverWriteFixedPrice)
                    {
                        long pickupfleetID = tsp.Trip.van_trip.FleetId;
                        Map.DTO.Address pickupAddress = new Map.DTO.Address { Geolocation = new Map.DTO.Geolocation { Latitude = tsp.Trip.van_trip.PickupLatitude.ToDecimal(), Longitude = tsp.Trip.van_trip.PickupLongitude.ToDecimal() } };
                        van_fleet_zone pickupZone = GetFleetZone(pickupfleetID, pickupAddress);

                        Map.DTO.Address dropOffAddress = new Map.DTO.Address { Geolocation = new Map.DTO.Geolocation { Latitude = latitude.Value, Longitude = longtitude.Value } };
                        van_fleet_zone dropOffZone = GetFleetZone(pickupfleetID, dropOffAddress);

                        if (pickupZone != null && dropOffZone != null)
                        {
                            using (VTODEntities context = new VTODEntities())
                            {
                                van_fleet_zone_flatrate tfzf = context.van_fleet_zone_flatrate.Where(x => x.PickupZoneId == pickupZone.Id && x.DropoffZoneId == dropOffZone.Id).Select(x => x).OrderBy(x => x.Amount).FirstOrDefault();
                                if (tfzf != null)
                                {
                                    finalFareWithoutGratuity = tfzf.Amount.ToDecimal();
                                    UpdateFlatRate(tsp.Trip.Id, true);
                                }
                                else
                                {
                                    //unable to find flatrate
                                    finalFareWithoutGratuity = dispatchFare;
                                }
                            }
                        }
                        else
                        {
                            //unable to find any dropoff zone in the flatratezone
                            finalFareWithoutGratuity = dispatchFare;
                        }

                    }
                    else
                    {
                        //just use dispatchFare
                        finalFareWithoutGratuity = dispatchFare;
                    }

                }
                catch (Exception ex)
                {
                    logger.ErrorFormat("Message:{0}", ex.Message);
                    logger.ErrorFormat("Inner Exception:{0}", ex.InnerException);
                    logger.ErrorFormat("StackTrace:{0}", ex.StackTrace);
                    throw new VanException(ex.Message);
                }

                sw.Stop();
                logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

                #region Track
                if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility, "DetermineOverwritedFare");
                #endregion

                return finalFareWithoutGratuity;
            }

            public bool ValidateSendMessageToVehicle(TokenRS tokenVTOD, UtilityNotifyDriverRQ input, out VanSendMessageToVehicleParameter tsmtvp)
            {
                bool result = false;
                Stopwatch sw = new Stopwatch();
                sw.Start();
                tsmtvp = new VanSendMessageToVehicleParameter();

                #region Track
                if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Domain, "Call");
                #endregion


                using (VTODEntities context = new VTODEntities())
                {
                    tsmtvp.VehicleNunber = context.van_trip_status.Where(x => x.TripID == input.TripID && x.VehicleNumber != null).OrderByDescending(x => x.Id).Select(x => x.VehicleNumber).FirstOrDefault();
                    //tsmtvp.Trip = (van_trip)context.vtod_trip.Where(x => x.Id == input.TripID).FirstOrDefault();
                    tsmtvp.Trip = context.van_trip.Where(x => x.Id == input.TripID).FirstOrDefault();
                    long fleetId = tsmtvp.Trip.FleetId;
                    tsmtvp.Fleet = context.van_fleet.Where(x => x.Id == fleetId).FirstOrDefault();
                    if (!tsmtvp.Fleet.van_fleet_api_reference.Any())
                    {
                        tsmtvp.Fleet.van_fleet_api_reference.Add(context.van_fleet_api_reference.Where(x => x.FleetId == fleetId).ToList().FirstOrDefault());
                    }
                    tsmtvp.Message = input.Message;
                }

                if (!string.IsNullOrWhiteSpace(tsmtvp.VehicleNunber) && !string.IsNullOrWhiteSpace(tsmtvp.Message) && tsmtvp != null)
                {
                    result = true;
                }
                else
                {
                    result = false;
                    throw VtodException.CreateException(ExceptionType.Van, 9003);//
                }

                sw.Stop();
                logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

                #region Track
                if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility, "ValidateSendMessageToVehicle");
                #endregion

                return result;
            }

            public bool ValidateMTDataSendMessageToVehicle(TokenRS tokenVTOD, UtilityNotifyDriverRQ input, out VanSendMessageToVehicleParameter tsmtvp)
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();
                tsmtvp = new VanSendMessageToVehicleParameter();

                #region Track
                if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Domain, "Call");
                #endregion

                using (var context = new VTODEntities())
                {
                    tsmtvp.Trip = context.van_trip.FirstOrDefault(x => x.Id == input.TripID);
                    tsmtvp.Message = input.Message;
                }

                if (tsmtvp.Trip == null || string.IsNullOrWhiteSpace(tsmtvp.Message))
                {
                    throw VtodException.CreateException(ExceptionType.Van, 9003);
                }

                sw.Stop();
                logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

                #region Track
                if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility, "ValidateSendMessageToVehicle");
                #endregion

                return true;
            }

            public bool ValidateMTDataPickMessageToVehicle(TokenRS tokenVTOD, UtilityNotifyDriverRQ input, out VanSendMessageToVehicleParameter tsmtvp)
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();
                tsmtvp = new VanSendMessageToVehicleParameter();

                #region Track
                if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Domain, "Call");
                #endregion

                using (var context = new VTODEntities())
                {
                    tsmtvp.Trip = context.van_trip.FirstOrDefault(x => x.Id == input.TripID);
                }

                if (tsmtvp.Trip == null)
                {
                    throw VtodException.CreateException(ExceptionType.Van, 9003);
                }

                sw.Stop();
                logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

                #region Track
                if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility, "ValidateMTDataPickMessageToVehicle");
                #endregion

                return true;
            }
            public bool ValidateGetAvailableFleets(TokenRS token, UtilityAvailableFleetsRQ input, out bool PickMeUpNow, out bool PickMeUpLater)
            {
                bool result = false;
                PickMeUpNow = true;
                PickMeUpLater = true;
                Stopwatch sw = new Stopwatch();
                sw.Start();

                #region Track
                if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Domain, "Call");
                #endregion

                if (input.Address != null)
                {
                    using (VTODEntities context = new VTODEntities())
                    {
                        //convert to MAPQuest address
                        Map.DTO.Address address = new Map.DTO.Address { Country = input.Address.CountryName.Code, City = input.Address.CityName, State = input.Address.StateProv.StateCode, ZipCode = input.Address.PostalCode, Geolocation = new Map.DTO.Geolocation { Latitude = Convert.ToDecimal(input.Address.Latitude), Longitude = Convert.ToDecimal(input.Address.Longitude) } };
                        van_fleet f = new van_fleet();
                        string addressType = AddressType.Address.ToString();
                        try
                        {
                            if (GetFleet(addressType, address, "", out f))
                            {
                                if (f != null)
                                {
                                    #region Validate Available address type

                                    string pickupAddressType = "";


                                    //verify the address type 
                                    UtilityDomain ud = new UtilityDomain();

                                    UtilityGetAirportNameRQ uganRQ = new UtilityGetAirportNameRQ();
                                    uganRQ.Address = new Address();
                                    uganRQ.Address.Latitude = input.Address.Latitude.ToString();
                                    uganRQ.Address.Longitude = input.Address.Longitude.ToString();

                                    UtilityGetAirportNameRS uganRS = ud.GetAirportName(token, uganRQ);
                                    if (uganRS.Success != null)
                                    {
                                        if (!string.IsNullOrWhiteSpace(uganRS.AirportName))
                                        {
                                            pickupAddressType = AddressType.Airport;
                                        }
                                        else
                                        {
                                            pickupAddressType = AddressType.Address;
                                        }
                                    }
                                    else
                                    {
                                        pickupAddressType = AddressType.Address;
                                    }




                                    //check database setting
                                    //1. get van_fleet_availableaddresstype

                                    long fleetId = f.Id;
                                    if (context.van_fleet_availableaddresstype.Where(x => fleetId == x.FleetId).Any())
                                    {
                                        van_fleet_availableaddresstype tfa = context.van_fleet_availableaddresstype.Where(x => fleetId == x.FleetId).Select(x => x).FirstOrDefault();
                                        //2. check if the type is available or not
                                        //van_Common_InvalidAddressTypeForFleet
                                        if (AddressType.Address.ToString() == pickupAddressType)
                                        {
                                            //address to address
                                            if (tfa.AddressToAddress || tfa.AddressToAirport || tfa.AddressToNull)
                                            {
                                                result = true;
                                            }
                                            else
                                            {
                                                result = false;
                                                throw VtodException.CreateException(ExceptionType.Van, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                            }
                                        }
                                        else if (AddressType.Airport.ToString() == pickupAddressType)
                                        {
                                            //address to airport
                                            if (tfa.AirportToAddress || tfa.AirportToAirport || tfa.AirportToNull)
                                            {
                                                result = true;
                                            }
                                            else
                                            {
                                                result = false;
                                                throw VtodException.CreateException(ExceptionType.Van, 1013); //VtodException.CreateValidationException(Messages.Validation_InvalidAddressType);
                                            }
                                        }
                                        else
                                        {
                                            throw new ValidationException("Unknown address type setting");

                                        }

                                    }
                                    else
                                    {
                                        result = false;
                                        throw VtodException.CreateException(ExceptionType.Van, 1011);// new ValidationException(Messages.van_Common_UnableToGetAvailableFleets);
                                    }




                                    #endregion

                                    GetPickMeupOption(f, out PickMeUpNow, out PickMeUpLater);
                                    result = true;
                                }
                            }
                        }
                        catch
                        {
                            //throw;
                        }
                    }
                }

                sw.Stop();
                logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);

                #region Track
                if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_Van_Utility, "ValidateGetAvailableFleets");
                #endregion

                return result;
            }

            #endregion

            public bool GetLongitudeAndLatitudeForAirport(string airportCode, out double? longitude, out double? latitude)
            {
                bool result = false;


                //#region Track
                //if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_van_Domain, "Call");
                //#endregion

                using (VTODEntities context = new VTODEntities())
                {
                    van_fleet_zone zone = context.van_fleet_zone.Where(x => x.Name == airportCode && x.Type == AddressType.Airport).Select(x => x).FirstOrDefault();
                    if (zone != null)
                    {

                        double lon;
                        double lat;
                        if (zone.CenterLongitude.HasValue && double.TryParse(zone.CenterLongitude.Value.ToString(), out lon))
                        {
                            longitude = lon;
                            result = true;
                        }
                        else
                        {
                            logger.Error("Unable to convert longitude");
                            result = false;
                            longitude = null;
                        }

                        if (zone.CenterLatitude.HasValue && double.TryParse(zone.CenterLatitude.Value.ToString(), out lat))
                        {
                            latitude = lat;
                            result = true;
                        }
                        else
                        {
                            logger.Error("Unable to convert latitude");
                            result = false;
                            latitude = null;
                        }
                    }
                    else
                    {
                        result = false;
                        longitude = null;
                        latitude = null;
                    }

                }

                //#region Track
                //if (TrackTime != null) TrackTime.AddTrackTime(LogOwnerTypeDetail.VTOD_van_Utility, "GetLongitudeAndLatitudeForAirport");
                //#endregion

                return result;
            }

            public van_fleet GetFleet(long Id)
            {

                van_fleet fleet = null;

                using (VTODEntities context = new VTODEntities())
                {
                    fleet = context.van_fleet.Where(x => x.Id == Id).Select(x => x).FirstOrDefault();
                    if (fleet != null)
                    {
                        fleet.van_fleet_api_reference = context.van_fleet_api_reference.Where(x => x.FleetId == fleet.Id).Select(x => x).ToList();
                    }
                }

                return fleet;
            }

            public void UpdateFinalStatus(long Id, string status, decimal? fare, decimal? gratuity, DateTime? tripStartTime, DateTime? tripEndTime, int? driverAcceptedEta)
            {
                using (VTODEntities context = new VTODEntities())
                {
                    vtod_trip t = context.vtod_trip.Where(x => x.Id == Id).Select(x => x).FirstOrDefault();
                    //t.FinalStatus = status;
                    //t.TotalFareAmount = fare;
                    //decimal? gratuity = null;
                    decimal? totalFareAmount = fare;
                    DateTime? newTripStartTime = null;
                    DateTime? newTripEndTime = null;
                    int? newDriverAcceptedEta = null;

                    #region Update Fare
                    #region Calculate Gratuity
                    if (!gratuity.HasValue || gratuity.Value <= 0)
                    {
                        if (t.TotalFareAmount == null || t.TotalFareAmount.Value <= 0)
                        {
                            if (fare.HasValue && fare > 0)
                            {
                                if (!t.Gratuity.HasValue || t.Gratuity <= 0)
                                {
                                    if (t.GratuityRate.HasValue && t.GratuityRate > 0)
                                    {
                                        gratuity = fare.Value * t.GratuityRate.Value / 100;
                                        //t.Gratuity = gratuity;
                                    }
                                }
                                else
                                {
                                    gratuity = t.Gratuity;
                                }
                            }
                        }
                        else
                        {
                            gratuity = t.Gratuity;
                        }
                    }
                    #endregion

                    #region Calculate TotalFare
                    if (t.TotalFareAmount == null || t.TotalFareAmount.Value <= 0)
                    {
                        if (fare.HasValue && fare > 0 && gratuity.HasValue && gratuity.Value > 0)
                        {
                            //t.TotalFareAmount = fare + gratuity;
                            totalFareAmount = fare + gratuity;
                        }
                        else if (fare.HasValue && fare > 0)
                        {
                            totalFareAmount = fare;
                        }
                    }
                    else
                    {
                        totalFareAmount = t.TotalFareAmount;
                    }
                    #endregion
                    #endregion

                    #region Update Trip Time
                    if (t.TripStartTime == null)
                        newTripStartTime = tripStartTime;
                    else
                        newTripStartTime = t.TripStartTime;

                    if (t.TripEndTime == null)
                        newTripEndTime = tripEndTime;
                    else
                        newTripEndTime = t.TripEndTime;
                    #endregion

                    #region DriverAcceptedETA
                    if (!t.DriverAcceptedETA.HasValue)
                    {
                        newDriverAcceptedEta = driverAcceptedEta;
                    }
                    else
                    {
                        newDriverAcceptedEta = t.DriverAcceptedETA.Value;
                    }
                    #endregion

                    //context.SaveChanges();

                    context.SP_vtod_Update_FinalStatus(Id, status, totalFareAmount, gratuity, newTripStartTime, newTripEndTime, newDriverAcceptedEta);
                }
            }

            public void MarkThisTripAsError(long Id, string errorMessage)
            {
                using (VTODEntities context = new VTODEntities())
                {
                    vtod_trip t = context.vtod_trip.Where(x => x.Id == Id).Select(x => x).FirstOrDefault();
                    t.FinalStatus = VanTripStatusType.Error;
                    t.Error = errorMessage;
                    context.SaveChanges();
                }
            }

            public vtod_trip CreateNewTrip(VanBookingParameter tbp, string userName)
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();
                Stopwatch swEachPart = new Stopwatch();
                vtod_trip v = new vtod_trip();
                van_trip t = new van_trip();
                GroundReservation reservation = tbp.request.GroundReservations.FirstOrDefault();
                TimeHelper utcTimeHelper = new TimeHelper();
                using (VTODEntities context = new VTODEntities())
                {
                    swEachPart.Restart();
                    #region fleetId and DNI
                    t.FleetId = tbp.Fleet.Id;
                    //t.TripType = "Production";
                    if (tbp.Fleet.UDI33DNI != null)
                    {
                        t.APIInformation = tbp.Fleet.UDI33DNI.ToString();
                    }
                    else if ((tbp.Fleet.CCSiFleetId != null) && (tbp.Fleet.CCSiSource != null))
                    {
                        t.APIInformation = string.Format("{0}_{1}", tbp.Fleet.CCSiSource, tbp.Fleet.CCSiFleetId);
                    }

                    #endregion
                    swEachPart.Stop();
                    logger.DebugFormat("Set fletId and DNI Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                    swEachPart.Restart();
                    #region Counsumer Source, Device, Ref
                    v.ConsumerDevice = tbp.Device;
                    v.ConsumerSource = tbp.Source;
                    v.ConsumerRef = tbp.Ref;
                    #endregion
                    swEachPart.Stop();
                    logger.DebugFormat("Set Consumer Device, Source, Ref:{0} ms.", swEachPart.ElapsedMilliseconds);

                    swEachPart.Restart();
                    #region Expire
                    double timeToExpire = 0;
                    if (!double.TryParse(ConfigurationManager.AppSettings["van_TimeToExpire"], out timeToExpire))
                    {
                        timeToExpire = 10;
                    }
                    DateTime dtExpires;
                    if (DateTime.TryParse(reservation.Service.Location.Pickup.DateTime, out dtExpires))
                    {
                        t.Expires = dtExpires.AddMinutes(timeToExpire);
                    }
                    #endregion
                    swEachPart.Stop();
                    logger.DebugFormat("Set expire time Duration:{0} ms.", swEachPart.ElapsedMilliseconds);


                    swEachPart.Restart();
                    #region PickupTime and DropOffTime
                    v.PickupDateTime = tbp.PickupDateTime;
                    //v.PickupDateTimeUTC = tbp.PickupDateTime.ToUtc(tbp.Fleet.ServerUTCOffset);

                    //DateTime dropOffDateTime;
                    //if (tbp.DropOffDateTime.HasValue)
                    //{
                    //	t.DropOffDateTime = tbp.DropOffDateTime.Value;
                    //	t.PickupDateTimeUTC = tbp.DropOffDateTime.Value.ToUtc(tbp.Fleet.ServerUTCOffset);
                    //}
                    #endregion
                    swEachPart.Stop();
                    logger.DebugFormat("Set pickuptime and dropioffTime Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                    swEachPart.Restart();
                    #region Get zone ID from van_fleet_zone, Get Consumer AccountNumber
                    //Pickup
                    if (tbp.PickupAddressType == AddressType.Address)
                    {
                        var zone = GetFleetZone(tbp.Fleet.Id, tbp.PickupAddress);
                        if (zone != null)
                        {
                            t.PickupFlatRateZone = zone.Name;
                            logger.InfoFormat("Pickup flat rate zone={0}", t.PickupFlatRateZone);
                        }
                    }
                    else if (tbp.PickupAddressType == AddressType.Airport)
                    {
                        var zone = GetFleetZoneByAirport(tbp.Fleet.Id, tbp.PickupAirport);
                        if (zone != null)
                        {
                            t.PickupFlatRateZone = zone.Name;

                            try
                            {
                                t.PickupLatitude = System.Convert.ToDouble(zone.CenterLatitude);
                                t.PickupLongitude = System.Convert.ToDouble(zone.CenterLongitude);
                            }
                            catch { }

                            logger.InfoFormat("Pickup flat rate zone={0}", t.PickupFlatRateZone);
                        }

                    }

                    //Dropoff
                    if (tbp.DropOffAddressType == AddressType.Address)
                    {
                        var zone = GetFleetZone(tbp.Fleet.Id, tbp.DropOffAddress);
                        if (zone != null)
                        {
                            t.DropOffFlatRateZone = zone.Name;
                            logger.InfoFormat("Dropoff flat rate zone={0}", t.DropOffFlatRateZone);
                        }
                    }
                    else if (tbp.DropOffAddressType == AddressType.Airport)
                    {
                        var zone = GetFleetZoneByAirport(tbp.Fleet.Id, tbp.DropoffAirport);
                        if (zone != null)
                        {
                            t.DropOffFlatRateZone = zone.Name;

                            try
                            {
                                t.DropOffLatitude = System.Convert.ToDouble(zone.CenterLatitude);
                                t.DropOffLongitude = System.Convert.ToDouble(zone.CenterLongitude);
                            }
                            catch { }

                            logger.InfoFormat("Dropoff flat rate zone={0}", t.DropOffFlatRateZone);
                        }

                    }

                    #endregion
                    swEachPart.Stop();
                    logger.DebugFormat("Set zone Id for van_fleet_zone Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                    swEachPart.Restart();
                    #region Get Account number
                    //ToDo: it should not be hardcoded. It should read it from DB based on FleetID
                    using (var db = new DataAccess.VTOD.VTODEntities())
                    {
                        var accountNumber = db.SP_van_GetFleet_AccountNumber(t.FleetId, tbp.tokenVTOD.Username).ToList().FirstOrDefault();
                        if (!string.IsNullOrWhiteSpace(accountNumber))
                        {
                            t.AccountNumber = accountNumber;
                        }
                    }
                    #endregion
                    swEachPart.Stop();
                    logger.DebugFormat("Set expire time Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                    swEachPart.Restart();
                    #region pickup address
                    if (tbp.PickupAddressType == AddressType.Address)
                    {
                        t.PickupAddressType = tbp.PickupAddressType;
                        double pickupLat = 0;
                        double pickupLongt = 0;
                        if (Double.TryParse(tbp.PickupAddress.Geolocation.Latitude.ToString(), out pickupLat))
                        {
                            t.PickupLatitude = pickupLat;
                        }

                        if (Double.TryParse(tbp.PickupAddress.Geolocation.Longitude.ToString(), out pickupLongt))
                        {
                            t.PickupLongitude = pickupLongt;
                        }

                        t.PickupStreetNo = tbp.PickupAddress.StreetNo;
                        t.PickupStreetName = tbp.PickupAddress.Street;
                        t.PickupStreetType = ExtractStreetType(tbp.PickupAddress.Street);
                        t.PickupAptNo = tbp.PickupAddress.ApartmentNo;
                        t.PickupCity = tbp.PickupAddress.City;
                        t.PickupStateCode = tbp.PickupAddress.State;
                        t.PickupZipCode = tbp.PickupAddress.ZipCode;
                        t.PickupCountryCode = tbp.PickupAddress.Country;
                        t.PickupFullAddress = tbp.PickupAddress.FullAddress;
                    }
                    else if (tbp.PickupAddressType == AddressType.Airport)
                    {
                        t.PickupAddressType = tbp.PickupAddressType;
                        t.PickupAirport = tbp.PickupAirport;
                        t.PickupFullAddress = tbp.PickupAirport;
                    }
                    else
                    {
                        logger.Warn("No pickup location");
                        t.PickupAddressType = AddressType.Empty;
                    }
                    #endregion
                    swEachPart.Stop();
                    logger.DebugFormat("Set pickup address Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                    swEachPart.Restart();
                    #region PickUp UTC Time
                    try
                    {
                        if (t.PickupLatitude != null && t.PickupLongitude != null)
                        {
                            v.PickupDateTimeUTC = utcTimeHelper.GetUTCFromLocalTime(t.PickupLatitude, t.PickupLongitude, tbp.PickupDateTime);
                        }
                        else
                        {
                            if (tbp.PickupAddressType == AddressType.Airport)
                            {
                                double? longitude;
                                double? latitude;
                                if (GetLongitudeAndLatitudeForAirport(t.PickupAirport, out longitude, out latitude))
                                {
                                    v.PickupDateTimeUTC = utcTimeHelper.GetUTCFromLocalTime(t.PickupLatitude, t.PickupLongitude, tbp.PickupDateTime);
                                }

                            }
                            else
                            {
                                v.PickupDateTimeUTC = tbp.PickupDateTime.ToUtc(tbp.Fleet.ServerUTCOffset);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.InfoFormat("Exception in PickUpTime UTC:{0}", ex.Message);
                        v.PickupDateTimeUTC = tbp.PickupDateTime.ToUtc(tbp.Fleet.ServerUTCOffset);
                    }
                    #endregion
                    swEachPart.Stop();
                    logger.DebugFormat("Set pick time utc Duration:{0} ms.", swEachPart.ElapsedMilliseconds);
                    swEachPart.Restart();
                    #region dropoff address
                    //Dropoff is optioanl : By_Pouyan
                    if (reservation.Service.Location.Dropoff != null)
                    {
                        double dropoffLat = 0;
                        double dropoffLongt = 0;
                        if (tbp.DropOffAddressType == AddressType.Address)
                        {
                            t.DropOffAddressType = tbp.DropOffAddressType;
                            if (tbp.DropOffAddress.Geolocation != null)
                            {
                                if (Double.TryParse(tbp.DropOffAddress.Geolocation.Latitude.ToString(), out dropoffLat))
                                {
                                    t.DropOffLatitude = dropoffLat;
                                }
                                if (Double.TryParse(tbp.DropOffAddress.Geolocation.Longitude.ToString(), out dropoffLongt))
                                {
                                    t.DropOffLongitude = dropoffLongt;
                                }
                            }

                            t.DropOffStreetNo = tbp.DropOffAddress.StreetNo;
                            t.DropOffStreetName = tbp.DropOffAddress.Street;
                            t.DropOffStreetType = ExtractStreetType(tbp.DropOffAddress.Street);
                            t.DropOffAptNo = tbp.DropOffAddress.ApartmentNo;
                            t.DropOffCity = tbp.DropOffAddress.City;
                            t.DropOffStateCode = tbp.DropOffAddress.State;
                            t.DropOffZipCode = tbp.DropOffAddress.ZipCode;
                            t.DropOffCountryCode = tbp.DropOffAddress.Country;
                            t.DropOffFullAddress = tbp.DropOffAddress.FullAddress;

                        }
                        else if (tbp.DropOffAddressType == AddressType.Airport)
                        {
                            t.DropOffAddressType = tbp.DropOffAddressType;
                            t.DropOffAirport = tbp.DropoffAirport;
                            t.DropOffFullAddress = tbp.DropoffAirport;
                        }
                        else if (tbp.DropOffAddressType == AddressType.Empty)
                        {
                            t.DropOffAddressType = AddressType.Empty;
                            logger.Info("No drop off location");
                        }
                    }
                    #endregion
                    swEachPart.Stop();
                    logger.DebugFormat("Set dropoff address Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                    swEachPart.Restart();
                    #region common fields
                    t.ServiceAPIId = tbp.serviceAPIID;
                    //t.CustomerId = tbp.Customer.Id;
                    t.FirstName = tbp.FirstName;
                    t.LastName = tbp.LastName;
                    t.PhoneNumber = tbp.PhoneNumber;
                    //t.CabNumber = "";
                    t.MinutesAway = null;
                    t.NumberOfPassenger = tbp.request.TPA_Extensions.Passengers.Any() ? tbp.request.TPA_Extensions.Passengers.Sum(x => x.Quantity) : 1;
                    //t.DriverNotes = null;
                    v.ConsumerRef = tbp.request.EchoToken;
                    if (tbp.PickupAddressType == AddressType.Address)
                    {
                        t.RemarkForPickup = reservation.Service.Location.Pickup.Remark;
                    }
                    if (tbp.DropOffAddressType == AddressType.Address)
                    {
                        t.RemarkForDropOff = reservation.Service.Location.Dropoff.Remark;
                    }
                    t.wheelchairAccessible = reservation.Service.DisabilityVehicleInd;

                    v.AppendTime = DateTime.Now;
                    v.FleetType = Common.DTO.Enum.FleetType.Van.ToString();
                    #endregion
                    swEachPart.Stop();
                    logger.DebugFormat("Set other common fields Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                    swEachPart.Restart();
                    #region Payment
                    if (tbp.PaymentType == Common.DTO.Enum.PaymentType.PaymentCard)
                    {
                        v.PaymentType = tbp.PaymentType.ToString();
                        v.CreditCardID = tbp.CreditCardID.HasValue ? tbp.CreditCardID.Value.ToString() : null;
                    }
                    else
                    {
                        v.PaymentType = tbp.PaymentType.ToString();
                    }
                    #endregion
                    swEachPart.Stop();
                    logger.DebugFormat("Set payment type Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                    swEachPart.Restart();
                    #region MemberID
                    v.MemberID = tbp.MemberID.HasValue ? tbp.MemberID.Value.ToString() : null;
                    #endregion
                    swEachPart.Stop();
                    logger.DebugFormat("Set member Id:{0} ms.", swEachPart.ElapsedMilliseconds);


                    swEachPart.Restart();
                    #region Gratuity
                    #region Check if it's percentage or amount
                    if (!string.IsNullOrWhiteSpace(tbp.Gratuity))
                    {
                        var gratuity = tbp.Gratuity.GetPercentageAmount();
                        //GratuityType gratuityType = GratuityType.Unknown;
                        if (gratuity > 0)
                        {
                            //gratuityType = GratuityType.Percentage;
                            v.GratuityRate = gratuity;
                        }
                        else
                        {
                            gratuity = tbp.Gratuity.ToDecimal();
                            v.Gratuity = gratuity;
                            //gratuityType = GratuityType.Amount;
                        }
                    }
                    #endregion

                    #endregion
                    swEachPart.Stop();
                    logger.DebugFormat("Set Gratuity Duration:{0} ms.", swEachPart.ElapsedMilliseconds);


                    swEachPart.Restart();
                    #region PromisedETA
                    #region Check if it's percentage or amount
                    v.PromisedETA = tbp.PromisedETA;
                    #endregion

                    #endregion
                    swEachPart.Stop();
                    logger.DebugFormat("Set Gratuity Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                    swEachPart.Restart();
                    #region PickMeUpNow
                    v.PickMeUpNow = tbp.PickupNow;
                    //if (v.PickupDateTimeUTC <= DateTime.UtcNow.AddMinutes(5))
                    //{
                    //    v.PickMeUpNow = true;
                    //}
                    //else
                    //{
                    //    v.PickMeUpNow = false;
                    //}
                    #endregion
                    swEachPart.Stop();
                    logger.DebugFormat("Set Pickup now Duration:{0} ms.", swEachPart.ElapsedMilliseconds);


                    swEachPart.Restart();
                    #region User
                    #region GetUser
                    var user = context.my_aspnet_users.Where(s => s.name.ToLower() == userName.ToLower()).ToList().FirstOrDefault();
                    v.UserID = user.id;
                    if (string.IsNullOrEmpty(v.ConsumerDevice))
                    {
                        if (user != null)
                        {
                            v.ConsumerDevice = user.name;
                        }
                    }
                    #endregion

                    #endregion
                    swEachPart.Stop();
                    logger.DebugFormat("Set User Id Duration:{0} ms.", swEachPart.ElapsedMilliseconds);

                    swEachPart.Restart();
                    #region Server Booking time
                    v.BookingServerDateTime = System.DateTime.Now;
                    #endregion
                    swEachPart.Stop();
                    logger.DebugFormat("BookingServerDateTime:{0} ms.", swEachPart.ElapsedMilliseconds);

                    swEachPart.Restart();
                    #region Email Address
                    if (!string.IsNullOrEmpty(tbp.EmailAddress))
                    {
                        v.EmailAddress = tbp.EmailAddress;
                    }
                    #endregion
                    swEachPart.Stop();
                    logger.DebugFormat("Email Address :{0} ms.", swEachPart.ElapsedMilliseconds);

                    logger.Info("before we create a van trip");
                    swEachPart.Restart();
                    #region Save
                    //context.van_trip.Add(t);
                    v.IsFlatRate = tbp.IsFixedPrice;
                    v.van_trip = t;
                    context.vtod_trip.Add(v);
                    context.SaveChanges();
                    #endregion
                    swEachPart.Stop();
                    logger.DebugFormat("Save this trip Duration:{0} ms.", swEachPart.ElapsedMilliseconds);
                    logger.Info("after we create a van trip");
                }

                logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
                return v;
            }


            public vtod_trip UpdateVanTrip(vtod_trip t, UDIXMLSchema.UDIBookingSuccess succ, int fleetTripCode)
            {
                using (VTODEntities context = new VTODEntities())
                {
                    vtod_trip v = context.vtod_trip.Where(x => x.Id == t.Id).Select(x => x).FirstOrDefault();
                    van_trip returnvanTrip = context.van_trip.Where(x => x.Id == t.Id).Select(x => x).FirstOrDefault();
                    returnvanTrip.DispatchTripId = succ.Order.ConfirmationNo;
                    returnvanTrip.NumOfTry++;
                    v.AppendTime = DateTime.Now;
                    v.FinalStatus = VanTripStatusType.Booked;
                    v.FleetTripCode = fleetTripCode;
                    context.SaveChanges();

                    //
                    t.van_trip.DispatchTripId = returnvanTrip.DispatchTripId;
                    t.van_trip.NumOfTry++;
                    t.AppendTime = v.AppendTime;
                    t.FinalStatus = v.FinalStatus;



                }
                return t;
            }


            //public van_trip UpdatevanTrip(van_trip t, UDIXMLSchema.UDIBookingSuccess succ)
            //{
            //    vtod_trip v;
            //    van_trip returnvanTrip;
            //    using (VTODEntities context = new VTODEntities())
            //    {
            //        v= context.vtod_trip.Where(x => x.Id == t.Id).Select(x => x).FirstOrDefault();
            //        returnvanTrip = context.van_trip.Where(x => x.Id == t.Id).Select(x => x).FirstOrDefault();
            //        //trip = (van_trip)context.vtod_trip.Where(x => x.Id == t.Id).Select(x => x).FirstOrDefault();
            //        returnvanTrip.DispatchTripId = succ.Order.ConfirmationNo;
            //        returnvanTrip.NumOfTry++;
            //        v.AppendTime = DateTime.Now;
            //        v.FinalStatus = vanTripStatusType.Booked;
            //        context.SaveChanges();
            //    }
            //    return returnvanTrip;
            //}

            public void UpdateVanTrip(long tripId, string confirmationNo, int rezID, string status, int fleetTripCode)
            {
                using (VTODEntities context = new VTODEntities())
                {
                    vtod_trip vTrip = context.vtod_trip.Where(x => x.Id == tripId).FirstOrDefault();
                    vTrip.AppendTime = DateTime.Now;
                    vTrip.FinalStatus = status;
                    vTrip.FleetTripCode = fleetTripCode;
                    van_trip returnvanTrip = context.van_trip.Where(x => x.Id == tripId).Select(x => x).FirstOrDefault();
                    returnvanTrip.DispatchTripId = confirmationNo;
                    returnvanTrip.NumOfTry++;
                    returnvanTrip.RezId = rezID;
                    context.SaveChanges();
                }

            }


            public vtod_trip UpdateVanTrip(vtod_trip t, string confirmationNo, int fleetTripCode)
            {
                using (VTODEntities context = new VTODEntities())
                {
                    vtod_trip v = context.vtod_trip.Where(x => x.Id == t.Id).Select(x => x).FirstOrDefault();
                    van_trip returnvanTrip = context.van_trip.Where(x => x.Id == t.Id).Select(x => x).FirstOrDefault();
                    returnvanTrip.DispatchTripId = confirmationNo;
                    returnvanTrip.NumOfTry++;
                    v.AppendTime = DateTime.Now;
                    v.FinalStatus = VanTripStatusType.Booked;
                    v.FleetTripCode = fleetTripCode;
                    context.SaveChanges();

                    t.van_trip.DispatchTripId = returnvanTrip.DispatchTripId;
                    t.van_trip.NumOfTry++;
                    t.AppendTime = v.AppendTime;
                    t.FinalStatus = v.FinalStatus;

                }
                return t;
            }

            //public van_trip UpdatevanTrip(van_trip t, string confirmationNo)
            //{
            //    vtod_trip v;
            //    van_trip returnvanTrip;
            //    using (VTODEntities context = new VTODEntities())
            //    {
            //        v = context.vtod_trip.Where(x => x.Id == t.Id).Select(x => x).FirstOrDefault();
            //        returnvanTrip = context.van_trip.Where(x => x.Id == t.Id).Select(x => x).FirstOrDefault();
            //        //trip = (van_trip)context.vtod_trip.Where(x => x.Id == t.Id).Select(x => x).FirstOrDefault();
            //        returnvanTrip.DispatchTripId = confirmationNo;
            //        returnvanTrip.NumOfTry++;
            //        v.AppendTime = DateTime.Now;
            //        v.FinalStatus = vanTripStatusType.Booked;
            //        context.SaveChanges();
            //    }
            //    return returnvanTrip;
            //}

            public void UpdateVanTrip(long Id, string cabNumber, int? minutesAway, string dispatchTripId, string driverName)
            {
                using (VTODEntities context = new VTODEntities())
                {
                    vtod_trip v = context.vtod_trip.Where(x => x.Id == Id).Select(x => x).FirstOrDefault();
                    var vanTrip = context.van_trip.Where(x => x.Id == Id).Select(x => x).FirstOrDefault();
                    //var trip = (van_trip)context.vtod_trip.Where(x => x.Id == Id).Select(x => x).FirstOrDefault();
                    if (vanTrip != null)
                    {
                        if (!string.IsNullOrWhiteSpace(cabNumber))
                        {
                            vanTrip.CabNumber = cabNumber;
                        }

                        if (minutesAway.HasValue)
                        {
                            vanTrip.MinutesAway = minutesAway.Value;
                        }

                        if (!string.IsNullOrWhiteSpace(dispatchTripId))
                        {
                            vanTrip.DispatchTripId = dispatchTripId;
                        }

                        if (!string.IsNullOrWhiteSpace(driverName))
                        {
                            Person p = ExtractDriverName(driverName);
                            v.DriverName = string.Format("{0} {1}", p.PersonName.GivenName, p.PersonName.Surname);
                        }

                        v.AppendTime = DateTime.Now;

                        context.SaveChanges();
                    }
                }
            }

            public bool IsAbleToCancelThisTrip(string status)
            {
                bool isAbleToCancel = true;
                using (VTODEntities context = new VTODEntities())
                {
                    if (context.van_forbiddencancelstatus.Where(x => x.Status == status).Any())
                    {
                        isAbleToCancel = false;
                    }
                    else
                    {
                        isAbleToCancel = true;
                    }

                }
                return isAbleToCancel;
            }

            //public bool VerifyNextTryForFastMeter(long tripID, out van_trip trip)
            //{
            //    bool returnValue = false;

            //    using (VTODEntities context = new VTODEntities())
            //    {


            //        trip = (van_trip)context.vtod_trip.Where(x => x.Id == tripID).Select(x => x).FirstOrDefault();

            //        if (trip != null)
            //        {
            //            long fleetID = trip.FleetId;
            //            van_fleet fleet = context.van_fleet.Where(x => x.Id == fleetID).Select(x => x).FirstOrDefault();

            //            if (fleet != null)
            //            {
            //                if (fleet.MaxNumOfRetryForFastMeter >= trip.NumOfTry)
            //                {
            //                    //do the booking
            //                    returnValue = true;
            //                }
            //                else
            //                {
            //                    //reach to maximum number of tries
            //                    logger.Info("reach to maximum number of tries for fast meter");
            //                    returnValue = false;
            //                }
            //            }
            //            else
            //            {
            //                returnValue = false;
            //                logger.ErrorFormat("Unable to find this fleet in DB. fleetID={0}", trip.FleetId);
            //                throw new Exception(Messages.van_Common_UnableToFindFleetByFleetID);
            //            }
            //        }
            //        else
            //        {
            //            returnValue = false;
            //            logger.ErrorFormat("Unable to find this trip in DB. tripID={0}", tripID);
            //            throw new Exception(Messages.van_TripNotFound);
            //        }
            //    }
            //    return returnValue;
            //}

            public VanStatusParameter ConvertTCPpToTSP(VanCancelParameter tcp)
            {
                VanStatusParameter tsp = new VanStatusParameter();
                try
                {
                    tsp.EnableResponseAndLog = false;
                    tsp.Fleet = GetFleet(tcp.Fleet.Id);
                    //tsp.request = null;
                    tsp.request = new OTA_GroundResRetrieveRQ { EchoToken = tcp.request.EchoToken, Target = tcp.request.Target, Version = tcp.request.Version, PrimaryLangID = tcp.request.PrimaryLangID };
                    //response.EchoToken = tsp.request.EchoToken;
                    //response.Target = tsp.request.Target;
                    //response.Version = tsp.request.Version;
                    //response.PrimaryLangID = tsp.request.PrimaryLangID;
                    tsp.tokenVTOD = tcp.tokenVTOD;
                    tsp.Trip = GetVanTrip(tcp.Trip.Id.ToString());
                    tsp.Fleet_User = new van_fleet_user { AccountNumber = tcp.User.AccountNumber, AppendTime = tcp.User.AppendTime, FleetId = tcp.User.FleetId, Id = tcp.User.Id, UserId = tcp.User.UserId };
                    tsp.User = GetUser(tcp.User.UserId);
                }
                catch
                {
                    throw;
                }
                return tsp;
            }

            public VanBookingParameter ConvertTripToTBP(van_trip t)
            {
                VanBookingParameter tbp = new VanBookingParameter();

                try
                {
                    //tbp.DropoffAddress = null;
                    tbp.DropOffAddressType = t.DropOffAddressType;
                    tbp.DropoffAirport = t.DropOffAirport;
                    tbp.FirstName = t.FirstName;
                    tbp.Fleet = GetFleet(t.FleetId);
                    tbp.LastName = t.LastName;
                    tbp.PhoneNumber = t.PhoneNumber;
                    //tbp.PickupAddress=null;
                    tbp.PickupAddressType = t.PickupAddressType;
                    tbp.PickupAirport = t.PickupAirport;
                    //tbp.request = null;
                    //tbp.tokenVTOD = null;
                    tbp.Trip = GetVanTrip(t.Id.ToString());
                    //tbp.User = null;
                    tbp.EnableResponseAndLog = false;
                }
                catch
                {
                    throw;
                }
                return tbp;
            }



            public vtod_trip GetVanTrip(string Id)
            {
                vtod_trip t = null;
                long vanTripId = 0;
                if (long.TryParse(Id, out vanTripId))
                {
                    t = GetVanTrip(vanTripId);
                }

                return t;
            }

            //public van_trip GetvanTrip(string Id)
            //{
            //    van_trip t = null;
            //    using (VTODEntities context = new VTODEntities())
            //    {
            //        long vanTripId = 0;
            //        if (long.TryParse(Id, out vanTripId))
            //        {
            //            var trip = context.van_trip.Where(x => x.Id == vanTripId).Select(x => x).FirstOrDefault();
            //            //var trip = (van_trip)context.vtod_trip.Where(x => x.Id == vanTripId).Select(x => x).FirstOrDefault();
            //            t = trip;
            //        }
            //    }
            //    return t;
            //}


            public vtod_trip GetVanTripForCCSi(string CCSiTripId, string CCSiFleetId)
            {
                vtod_trip t = null;
                long tripId = 0;
                using (VTODEntities context = new VTODEntities())
                {
                    tripId = context.SP_van_GetCCSiTrip(CCSiTripId, CCSiFleetId).Select(x => (long)x.Value).FirstOrDefault();
                }
                t = GetVanTrip(tripId.ToString());

                return t;
            }

            //public van_trip GetvanTripForCCSi(string CCSiTripId, string CCSiFleetId)
            //{
            //    van_trip t = null;
            //    using (VTODEntities context = new VTODEntities())
            //    {
            //        long tripId = context.SP_van_GetCCSiTrip(CCSiTripId, CCSiFleetId).Select(x => (long)x.Value).FirstOrDefault();
            //        t = GetvanTrip(tripId.ToString());
            //    }

            //    return t;
            //}

           

            public string ConvertTripStatusForCCSi(int sourceEventId, string sourceEventStatus, out string unknownStatus)
            {
                string target = "";
                unknownStatus = "";
                using (VTODEntities context = new VTODEntities())
                {
                    if ((context.van_status_ccsiwrapper.Where(x => x.SourceEventId == sourceEventId).Select(x => x).Count() > 1))
                    {
                        if (context.van_status_ccsiwrapper.Where(x => x.SourceEventId == sourceEventId && x.SourceEventStatus == sourceEventStatus).Select(x => x).Count() == 1)
                        {
                            var t = context.van_status_ccsiwrapper.Where(x => x.SourceEventId == sourceEventId && x.SourceEventStatus == sourceEventStatus).Select(x => x.Target).FirstOrDefault();
                            if (!string.IsNullOrWhiteSpace(t))
                            {
                                target = t;
                                logger.InfoFormat("Convert status from eventId={0}, eventStatus={1} to {2}", sourceEventId, sourceEventStatus, target);
                            }
                            else
                            {
                                //unknown
                                target = "Unkonwn";
                                unknownStatus = string.Format("Convert status from eventId={0}, eventStatus={1} to {2}", sourceEventId, sourceEventStatus, target);
                                logger.Error(unknownStatus);

                            }
                        }
                        else
                        {
                            //unknown
                        }
                    }
                    else if ((context.van_status_ccsiwrapper.Where(x => x.SourceEventId == sourceEventId).Select(x => x).Count() == 1))
                    {
                        var t = context.van_status_ccsiwrapper.Where(x => x.SourceEventId == sourceEventId).Select(x => x.Target).FirstOrDefault();
                        if (!string.IsNullOrWhiteSpace(t))
                        {
                            target = t;
                            logger.InfoFormat("Convert status from eventId={0} to {1}", sourceEventId, target);
                        }
                        else
                        {
                            //unknown
                            target = "Unkonwn";
                            unknownStatus = string.Format("Convert status from eventId={0}, eventStatus={1} to {2}", sourceEventId, sourceEventStatus, target);
                            logger.Error(unknownStatus);
                        }
                    }
                    else
                    {
                        //unknown
                        target = "Unkonwn";
                        unknownStatus = string.Format("Convert status from eventId={0}, eventStatus={1} to {2}", sourceEventId, sourceEventStatus, target);
                        logger.Error(unknownStatus);
                    }

                }
                return target;
            }

            public void SaveTripStatus(long tripID, string status, DateTime? statusTime, string vehicleNumber, decimal? vehicleLatitude, decimal? vehicleLongitude, decimal? fare, decimal? dispatchFare, int? eta, int? etaWithTraffic, string driverName, string comment, string originalStatus, string driverId)
            {
                using (VTODEntities context = new VTODEntities())
                {
                    logger.InfoFormat("Trip ID={0}, status={1}", tripID, status);
                    //van_trip_status tts = new van_trip_status { Status = status, TripID = tripID, AppendTime = DateTime.Now, StatusTime = statusTime, VehicleNumber = vehicleNumber, VehicleLongitude = vehicleLongitude, VehicleLatitude = vehicleLatitude, Fare = fare };
                    //context.van_trip_status.Add(tts);
                    //context.SaveChanges();
                    context.SP_van_InsertTripStatus(tripID, status, statusTime, vehicleNumber, vehicleLatitude, vehicleLongitude, fare, dispatchFare, eta, etaWithTraffic, driverName, driverId, comment, originalStatus);
                }
            }

            public void SaveTripStatus_ReDispatch(long tripID, DateTime? statusTime, string vehicleNumber, decimal? vehicleLatitude, decimal? vehicleLongitude, decimal? fare, decimal? dispatchFare, int? eta, int? etaWithTraffic, string driverName, string comment, string originalStatus, string driverId)
            {
                using (VTODEntities context = new VTODEntities())
                {
                    using (TransactionScope tx = new TransactionScope())
                    {
                        try
                        {
                            context.SP_van_InsertTripStatus(tripID, VanTripStatusType.FastMeter, statusTime, vehicleNumber, vehicleLatitude, vehicleLongitude, fare, dispatchFare, eta, etaWithTraffic, driverName, comment, originalStatus, driverId);
                            logger.InfoFormat("Trip ID={0}, status={1}", tripID, VanTripStatusType.FastMeter);
                            context.SP_van_InsertTripStatus(tripID, VanTripStatusType.Booked, statusTime, vehicleNumber, vehicleLatitude, vehicleLongitude, fare, dispatchFare, eta, etaWithTraffic, driverName, comment, originalStatus, driverId);
                            logger.InfoFormat("Trip ID={0}, status={1}", tripID, VanTripStatusType.Booked);
                            tx.Complete();
                        }
                        catch (Exception ex)
                        {
                            logger.Error(ex.Message);
                            logger.Error(ex.InnerException);
                            logger.Error(ex.StackTrace);
                            tx.Dispose();
                        }
                    }
                }
            }

            public void WriteVanLog(long? TripId, long Type, int Action, long? validationDuration, string request, string response, DateTime? sendRequest, DateTime? getResponse)
            {
                #region This is eliminated for having better performance. Everything should be logged in Log4Net
                //using (VTODEntities context = new VTODEntities())
                //{
                //	if (validationDuration.HasValue)
                //	{
                //		//Save validation duration
                //		context.SP_van_InsertLogByValidation(TripId, Type, Action, validationDuration.Value, DateTime.Now);
                //	}
                //	else
                //	{
                //		//Save for request and response
                //		context.SP_van_InsertLogByDateTime(TripId, Type, Action, request, response, sendRequest, getResponse, DateTime.Now);
                //	}
                //} 
                #endregion
            }

            public Person ExtractDriverName(string driverName)
            {
                Person p = null;
                if (!string.IsNullOrWhiteSpace(driverName))
                {
                    p = new Person();
                    p.PersonName = new PersonName();
                    string[] sep = { " " };
                    string[] results = driverName.Split(sep, StringSplitOptions.RemoveEmptyEntries);
                    if (results.Length == 1)
                    {
                        p.PersonName.GivenName = driverName;
                    }
                    else if (results.Length >= 2)
                    {
                        p.PersonName.GivenName = string.Join(" ", results.Take(results.Length - 1).ToArray());
                        p.PersonName.Surname = results[results.Length - 1];
                    }
                }

                return p;
            }

            public decimal GetFareAmountForCCSi(long tripID)
            {
                decimal result = 0;
                using (VTODEntities context = new VTODEntities())
                {
                    logger.InfoFormat("Trip ID={0}", tripID);
                    //van_trip_status tts = new van_trip_status { Status = status, TripID = tripID, AppendTime = DateTime.Now, StatusTime = statusTime, VehicleNumber = vehicleNumber, VehicleLongitude = vehicleLongitude, VehicleLatitude = vehicleLatitude, Fare = fare };
                    //context.van_trip_status.Add(tts);
                    //context.SaveChanges();
                    //context.SP_van_InsertTripStatus(tripID, status, statusTime, vehicleNumber, vehicleLatitude, vehicleLongitude, fare, dispatchFare, eta, driverName, driverId, comment, originalStatus);

                    var status = context.van_trip_status.Where(s => s.TripID == tripID && s.Status.ToLower() == "fare").FirstOrDefault();
                    if (status != null && status.Fare.HasValue && status.Fare.Value > 0)
                    {
                        result = status.Fare.Value;
                    }
                }

                return result;
            }

            public vtod_driver_info GetDriverInfo(vtod_trip trip)
            {
                vtod_driver_info result = null;
                if (trip != null && trip.DriverInfoID.HasValue && trip.DriverInfoID.Value > 0)
                {
                    using (var db = new DataAccess.VTOD.VTODEntities())
                    {
                        result = db.vtod_driver_info.Where(s => s.Id == trip.DriverInfoID).FirstOrDefault();
                    }
                }
                return result;
            }

            //public vtod_driver_info GetDriverInfo(van_trip trip)
            //{
            //    vtod_driver_info result = null;
            //    if (trip != null && trip.DriverInfoID.HasValue && trip.DriverInfoID.Value > 0)
            //    {
            //        using (var db = new DataAccess.VTOD.VTODEntities())
            //        {
            //            result = db.vtod_driver_info.Where(s => s.Id == trip.DriverInfoID).FirstOrDefault();
            //        }
            //    }
            //    return result;
            //}

            public vtod_driver_info GetDriverInfoByDriverId(string userName, vtod_trip trip, string status, string driverId)
            {
                vtod_driver_info result = null;
                var vanDrvController = new UDI.SDS.VanDrvController(TrackTime);

                try
                {
                    if (!string.IsNullOrWhiteSpace(userName))
                    {
                        #region Check web config to see if pulling driver info is allowed
                        int vanUserNameForPulling_Count = 0;

                        try
                        {
                            var vanUserNameForPulling = System.Configuration.ConfigurationManager.AppSettings["van_UserName_For_Pulling_DriverInfo"];
                            if (!string.IsNullOrWhiteSpace(vanUserNameForPulling))
                            {
                                if (vanUserNameForPulling.Contains(","))
                                    vanUserNameForPulling_Count =
                                        vanUserNameForPulling.Split(',')
                                            .Where(s => !string.IsNullOrEmpty(s) && s.Trim().ToLower() == userName.Trim().ToLower())
                                            .Count();
                                else
                                    vanUserNameForPulling_Count = 1;
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.Error(ex.Message);
                            logger.Error(ex.InnerException);
                            logger.Error(ex.StackTrace);
                        }
                        #endregion

                        if (ConfigHelper.IsTest())
                        {
                            status = VanTripStatusType.Accepted;
                            trip.DriverInfoID = 0;
                        }


                        if (vanUserNameForPulling_Count > 0)
                        {
                            if (!trip.DriverInfoID.HasValue || trip.DriverInfoID.Value == 0)
                            {
                                if (
                                        status.ToLower().Trim() == VanTripStatusType.Accepted.ToLower().Trim()
                                        ||
                                        status.ToLower().Trim() == VanTripStatusType.InTransit.ToLower().Trim()
                                        ||
                                        status.ToLower().Trim() == VanTripStatusType.PickUp.ToLower().Trim()
                                        ||
                                        status.ToLower().Trim() == VanTripStatusType.MeterON.ToLower().Trim()
                                        ||
                                        status.ToLower().Trim() == VanTripStatusType.InService.ToLower().Trim()
                                        ||
                                        status.ToLower().Trim() == VanTripStatusType.Completed.ToLower().Trim()
                                        ||
                                        status.ToLower().Trim() == VanTripStatusType.MeterOff.ToLower().Trim()
                                        )
                                {
                                    #region Get Driver Info from SDS
                                    //use test ds if we're in test mode
                                    DataSet drvResult;
                                    drvResult = !ConfigHelper.IsTest() ? vanDrvController.GetVanDriverInfo(trip.van_trip.FleetId.ToString(), driverId, "") : vanDrvController.GetVanDriverInfo("16", "", "726");
                                    #endregion


                                    #region Insert in database
                                    if (drvResult != null && drvResult.Tables.Count > 0)
                                    {
                                        var drvDataTable = drvResult.Tables[0];
                                        if (drvDataTable != null)
                                        {
                                            if (drvDataTable.Rows != null && drvDataTable.Rows.Count > 0)
                                            {
                                                var newDriver = new vtod_driver_info();
                                                byte[] driverImage = null;
                                                foreach (DataRow row in drvDataTable.Rows)
                                                {
                                                    newDriver.DriverID = row["DriverID"].ToString();
                                                    newDriver.VehicleID = row["VehicleID"].ToString();
                                                    newDriver.FirstName = row["FirstName"].ToString();
                                                    newDriver.LastName = row["LastName"].ToString();
                                                    newDriver.CellPhone = row["CellPhone"].ToString();
                                                    newDriver.HomePhone = row["HomePhone"].ToString();
                                                    newDriver.AlternatePhone = row["AlternatePhone"].ToString();
                                                    newDriver.QualificationDate = row["QualificationDate"].ToDateTime();
                                                    newDriver.zTripQualified = row["zTripQualified"].ToBool();
                                                    newDriver.StartDate = row["StartDate"].ToDateTime();
                                                    newDriver.HrDays = row["HrDays"].ToString();
                                                    newDriver.LeaseDate = row["LeaseDate"].ToDateTime();
                                                    newDriver.FleetType = Common.DTO.Enum.FleetType.Van.ToString();
                                                    newDriver.AppendDate = System.DateTime.Now;
                                                    #region Image
                                                    // newDriver.Image = null;
                                                    if (row["Image"] != null && row["Image"] != System.DBNull.Value)
                                                    {
                                                        driverImage = (byte[])row["Image"];
                                                    }
                                                    #endregion
                                                    #region CryptoDriver
                                                    #region removed for driver info refactor
                                                    //var cryptoDriver = string.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}{11}{12}{13}",
                                                    //                                       string.IsNullOrEmpty(newDriver.DriverID) ? "" : newDriver.DriverID, //{0}
                                                    //                                       string.IsNullOrEmpty(newDriver.VehicleID) ? "" : newDriver.VehicleID, //{1}
                                                    //                                       string.IsNullOrEmpty(newDriver.FirstName) ? "" : newDriver.FirstName, //{2}
                                                    //                                       string.IsNullOrEmpty(newDriver.LastName) ? "" : newDriver.LastName, //{3}
                                                    //                                       string.IsNullOrEmpty(newDriver.CellPhone) ? "" : newDriver.CellPhone, //{4}
                                                    //                                       string.IsNullOrEmpty(newDriver.HomePhone) ? "" : newDriver.HomePhone, //{5}
                                                    //                                       string.IsNullOrEmpty(newDriver.AlternatePhone) ? "" : newDriver.AlternatePhone, //{6}
                                                    //                                       !newDriver.QualificationDate.HasValue ? "" : newDriver.QualificationDate.ToString(), //{7}
                                                    //                                       !newDriver.zTripQualified.HasValue ? "" : newDriver.zTripQualified.ToString(), //{8}
                                                    //                                       !newDriver.StartDate.HasValue ? "" : newDriver.StartDate.ToString(), //{9}
                                                    //                                       string.IsNullOrEmpty(newDriver.HrDays) ? "" : newDriver.HrDays, //{10}
                                                    //                                       !newDriver.LeaseDate.HasValue ? "" : newDriver.LeaseDate.ToString(), //{11}
                                                    //                                       string.IsNullOrEmpty(newDriver.FleetType) ? "" : newDriver.FleetType, //{12}
                                                    //                                       driverImage == null ? "" : driverImage.Base64Serialize() //{13}
                                                    //                                       ).ToSHA1();
                                                    // newDriver.CryptoDriver = cryptoDriver; 
                                                    #endregion
                                                    #endregion

                                                }

                                                #region Log
                                                logger.InfoFormat("Driver Found: ID:{0}, VehicleID:{1}", newDriver.DriverID, newDriver.VehicleID);
                                                #endregion

                                                using (var db = new VTODEntities())
                                                {
                                                    #region removed for driver info refactor
                                                    //var images = new Images(logger);

                                                    //var foundDriver = db.vtod_driver_info.FirstOrDefault(s => s.CryptoDriver == newDriver.CryptoDriver);
                                                    //{
                                                    //    if (foundDriver == null)
                                                    //    {

                                                    //        #region Add/Update image if the image has changed

                                                    //        try
                                                    //        {
                                                    //            if (driverImage != null)
                                                    //            {
                                                    //                var thread = new Thread(() => images.UpdateDriverImageSizesFromConfig(Image.FromStream(new MemoryStream(driverImage)), newDriver.DriverID, trip.van_trip.FleetId.ToString()));
                                                    //                thread.Start();
                                                    //            }
                                                    //        #endregion
                                                    //            #region set the driver image url
                                                    //            newDriver.PhotoUrl = images.GetPhotoUrl(newDriver.DriverID, trip.van_trip.FleetId.ToString());
                                                    //        }
                                                    //        catch (Exception ex)
                                                    //        {
                                                    //            logger.Error(ex.Message);
                                                    //            logger.Error(ex.InnerException);
                                                    //            logger.Error(ex.StackTrace);
                                                    //        }
                                                    //            #endregion
                                                    //        db.vtod_driver_info.Add(newDriver);
                                                    //        db.SaveChanges();

                                                    //        result = newDriver;
                                                    //    }
                                                    //    else
                                                    //    {
                                                    //        result = foundDriver;
                                                    //    }

                                                    //} 
                                                    #endregion

                                                    var driverInfo = new DriverInfo(logger);

                                                    result = driverInfo.UpsertDriverDetails(newDriver, driverImage);

                                                    db.SP_vtod_Update_TripDriverInfoID(trip.Id, result.Id);
                                                }

                                                //using (var db = new DataAccess.VTOD.VTODEntities())
                                                //{
                                                //	var foundDriver = db.vtod_driver_info.Where(s => s.CryptoDriver == newDriver.CryptoDriver).FirstOrDefault();
                                                //	if (foundDriver == null)
                                                //	{
                                                //		db.vtod_driver_info.Add(newDriver);
                                                //		db.SaveChanges();

                                                //		result = newDriver;
                                                //	}
                                                //	else
                                                //	{
                                                //		result = foundDriver;
                                                //	}

                                                //	db.SP_vtod_Update_TripDriverInfoID(trip.Id, result.Id);
                                                //}
                                            }
                                        }
                                    }
                                    #endregion

                                }
                            }
                            else
                            {
                                if (trip.DriverInfoID.HasValue && trip.DriverInfoID.Value > 0)
                                {
                                    using (var db = new DataAccess.VTOD.VTODEntities())
                                    {
                                        result = db.vtod_driver_info.Where(s => s.Id == trip.DriverInfoID.Value).FirstOrDefault();
                                    }
                                }
                            }
                        }
                    }
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                {
                    foreach (var eve in ex.EntityValidationErrors)
                    {
                        logger.ErrorFormat("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            logger.ErrorFormat("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }
                catch (Exception ex)
                {
                    logger.ErrorFormat("GetDriverInfo: {0}", ex.ToString() + ex.StackTrace);
                }

                return result;
            }

            public vtod_driver_info GetDriverInfoByVehicleNumber(string userName, vtod_trip trip, string status, string vehicleNumber, string driverNumber)
            {
                vtod_driver_info result = null;
                var vanDrvController = new UDI.SDS.VanDrvController(TrackTime);

                try
                {
                    if (!string.IsNullOrWhiteSpace(userName))
                    {
                        #region Check web config to see if pulling driver info is allowed
                        int vanUserNameForPulling_Count = 0;

                        try
                        {
                            var vanUserNameForPulling = System.Configuration.ConfigurationManager.AppSettings["van_UserName_For_Pulling_DriverInfo"];
                            if (!string.IsNullOrWhiteSpace(vanUserNameForPulling))
                            {
                                if (vanUserNameForPulling.Contains(","))
                                    vanUserNameForPulling_Count =
                                        vanUserNameForPulling.Split(',')
                                            .Where(s => !string.IsNullOrEmpty(s) && s.Trim().ToLower() == userName.Trim().ToLower())
                                            .Count();
                                else
                                    vanUserNameForPulling_Count = 1;
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.Error(ex.Message);
                            logger.Error(ex.InnerException);
                            logger.Error(ex.StackTrace);
                        }
                        #endregion


                        if (vanUserNameForPulling_Count > 0)
                        {
                            if (!trip.DriverInfoID.HasValue || trip.DriverInfoID.Value == 0)
                            {
                                if (
                                        status.ToLower().Trim() == VanTripStatusType.Accepted.ToLower().Trim()
                                        ||
                                        status.ToLower().Trim() == VanTripStatusType.InTransit.ToLower().Trim()
                                        ||
                                        status.ToLower().Trim() == VanTripStatusType.PickUp.ToLower().Trim()
                                        ||
                                        status.ToLower().Trim() == VanTripStatusType.MeterON.ToLower().Trim()
                                        ||
                                        status.ToLower().Trim() == VanTripStatusType.InService.ToLower().Trim()
                                        ||
                                        status.ToLower().Trim() == VanTripStatusType.Completed.ToLower().Trim()
                                        ||
                                        status.ToLower().Trim() == VanTripStatusType.MeterOff.ToLower().Trim()
                                        ||
                                        status.ToLower().Trim() == VanTripStatusType.Assigned.ToLower().Trim()
                                        )
                                {
                                    #region Get Driver Info from SDS
                                    //use test ds if we're in test mode
                                    DataSet drvResult;

                                    drvResult = vanDrvController.GetVanDriverInfo(trip.van_trip.FleetId.ToString(), string.IsNullOrWhiteSpace(driverNumber) ? "" : driverNumber, vehicleNumber);
                                    #endregion


                                    #region Insert in database
                                    if (drvResult != null && drvResult.Tables.Count > 0)
                                    {
                                        var drvDataTable = drvResult.Tables[0];
                                        if (drvDataTable != null)
                                        {
                                            if (drvDataTable.Rows != null && drvDataTable.Rows.Count > 0)
                                            {
                                                var newDriver = new vtod_driver_info();
                                                byte[] driverImage = null;
                                                foreach (DataRow row in drvDataTable.Rows)
                                                {
                                                    newDriver.DriverID = row["DriverID"].ToString();
                                                    newDriver.VehicleID = row["VehicleID"].ToString();
                                                    newDriver.FirstName = row["FirstName"].ToString();
                                                    newDriver.LastName = row["LastName"].ToString();
                                                    newDriver.CellPhone = row["CellPhone"].ToString();
                                                    newDriver.HomePhone = row["HomePhone"].ToString();
                                                    newDriver.AlternatePhone = row["AlternatePhone"].ToString();
                                                    newDriver.QualificationDate = row["QualificationDate"].ToDateTime();
                                                    newDriver.zTripQualified = row["zTripQualified"].ToBool();
                                                    newDriver.StartDate = row["StartDate"].ToDateTime();
                                                    newDriver.HrDays = row["HrDays"].ToString();
                                                    newDriver.LeaseDate = row["LeaseDate"].ToDateTime();
                                                    newDriver.FleetType = Common.DTO.Enum.FleetType.Van.ToString();
                                                    newDriver.AppendDate = System.DateTime.Now;
                                                    #region Image
                                                    //newDriver.Image = null;
                                                    if (row["Image"] != null && row["Image"] != System.DBNull.Value)
                                                    {
                                                        driverImage = (byte[])row["Image"];
                                                    }
                                                    #endregion


                                                    #region Removed in driver save refactor
                                                    //#region CryptoDriver
                                                    //var cryptoDriver = string.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}{11}{12}{13}",
                                                    //    string.IsNullOrEmpty(newDriver.DriverID) ? "" : newDriver.DriverID, //{0}
                                                    //    string.IsNullOrEmpty(newDriver.VehicleID) ? "" : newDriver.VehicleID, //{1}
                                                    //    string.IsNullOrEmpty(newDriver.FirstName) ? "" : newDriver.FirstName, //{2}
                                                    //    string.IsNullOrEmpty(newDriver.LastName) ? "" : newDriver.LastName, //{3}
                                                    //    string.IsNullOrEmpty(newDriver.CellPhone) ? "" : newDriver.CellPhone, //{4}
                                                    //    string.IsNullOrEmpty(newDriver.HomePhone) ? "" : newDriver.HomePhone, //{5}
                                                    //    string.IsNullOrEmpty(newDriver.AlternatePhone) ? "" : newDriver.AlternatePhone, //{6}
                                                    //    !newDriver.QualificationDate.HasValue ? "" : newDriver.QualificationDate.ToString(), //{7}
                                                    //    !newDriver.zTripQualified.HasValue ? "" : newDriver.zTripQualified.ToString(), //{8}
                                                    //    !newDriver.StartDate.HasValue ? "" : newDriver.StartDate.ToString(), //{9}
                                                    //    string.IsNullOrEmpty(newDriver.HrDays) ? "" : newDriver.HrDays, //{10}
                                                    //    !newDriver.LeaseDate.HasValue ? "" : newDriver.LeaseDate.ToString(), //{11}
                                                    //    string.IsNullOrEmpty(newDriver.FleetType) ? "" : newDriver.FleetType, //{12}
                                                    //   driverImage == null ? "" : driverImage.Base64Serialize() //{13}
                                                    //    ).ToSHA1();
                                                    //newDriver.CryptoDriver = cryptoDriver;
                                                    //#endregion 
                                                    #endregion

                                                }

                                                #region Log
                                                logger.InfoFormat("Driver Found: ID:{0}, VehicleID:{1}", newDriver.DriverID, newDriver.VehicleID);
                                                #endregion

                                                using (var db = new DataAccess.VTOD.VTODEntities())
                                                {
                                                    #region removed in driver save refactor
                                                    //var images = new Images(logger);

                                                    //var foundDriver = db.vtod_driver_info.FirstOrDefault(s => s.CryptoDriver == newDriver.CryptoDriver);
                                                    //{
                                                    //    if (foundDriver == null)
                                                    //    {

                                                    //        #region Add/Update image if the image has changed
                                                    //        if (driverImage != null)
                                                    //        {
                                                    //            var thread = new Thread(() => images.UpdateDriverImageSizesFromConfig(Image.FromStream(new MemoryStream(driverImage)), newDriver.DriverID, trip.van_trip.FleetId.ToString()));
                                                    //            thread.Start();

                                                    //            #region set the driver image url
                                                    //            newDriver.PhotoUrl = images.GetPhotoUrl(newDriver.DriverID, trip.van_trip.FleetId.ToString());
                                                    //            #endregion
                                                    //        }
                                                    //        #endregion

                                                    //        db.vtod_driver_info.Add(newDriver);
                                                    //        db.SaveChanges();

                                                    //        result = newDriver;
                                                    //    }
                                                    //    else
                                                    //    {
                                                    //        result = foundDriver;
                                                    //    }

                                                    //} 


                                                    #endregion


                                                    var driverInfo = new DriverInfo(logger);

                                                    result = driverInfo.UpsertDriverDetails(newDriver, driverImage);

                                                    db.SP_vtod_Update_TripDriverInfoID(trip.Id, result.Id);
                                                }

                                                //using (var db = new DataAccess.VTOD.VTODEntities())
                                                //{
                                                //	var foundDriver = db.vtod_driver_info.Where(s => s.CryptoDriver == newDriver.CryptoDriver).FirstOrDefault();
                                                //	if (foundDriver == null)
                                                //	{
                                                //		db.vtod_driver_info.Add(newDriver);
                                                //		db.SaveChanges();

                                                //		result = newDriver;
                                                //	}
                                                //	else
                                                //	{
                                                //		result = foundDriver;
                                                //	}

                                                //	db.SP_vtod_Update_TripDriverInfoID(trip.Id, result.Id);
                                                //}
                                            }
                                        }
                                    }
                                    #endregion

                                }
                            }
                            else
                            {
                                if (trip.DriverInfoID.HasValue && trip.DriverInfoID.Value > 0)
                                {
                                    using (var db = new DataAccess.VTOD.VTODEntities())
                                    {
                                        result = db.vtod_driver_info.Where(s => s.Id == trip.DriverInfoID.Value).FirstOrDefault();
                                    }
                                }
                            }
                        }
                    }
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                {
                    foreach (var eve in ex.EntityValidationErrors)
                    {
                        logger.ErrorFormat("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            logger.ErrorFormat("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }
                catch (Exception ex)
                {
                    logger.ErrorFormat("GetDriverInfo: {0}", ex.ToString() + ex.StackTrace);
                }

                return result;
            }



            public vtod_trip GetVanTrip(long tripId)
            {
                vtod_trip v = null;
                using (VTODEntities context = new VTODEntities())
                {
                    v = context.vtod_trip.Include("van_trip").Where(x => x.Id == tripId).Select(x => x).FirstOrDefault();
                    //   .Join(context.van_trip, x => x.Id, y => y.Id, (x, y) => new vtod_trip {
                    //Id=x.Id,
                    //UserID=x.UserID,
                    //FleetType=x.FleetType,
                    //MemberID=x.MemberID,
                    //CreditCardID=x.CreditCardID,
                    //DirectBillAccountID=x.DirectBillAccountID,
                    //FinalStatus=x.FinalStatus,
                    //Gratuity=x.Gratuity,
                    //GratuityRate=x.GratuityRate,
                    //TotalFareAmount=x.TotalFareAmount,
                    //PickupDateTime=x.PickupDateTime,
                    //PickupDateTimeUTC=x.PickupDateTimeUTC,
                    //BookingServerDateTime=x.BookingServerDateTime,
                    //PickMeUpNow=x.PickMeUpNow,
                    //ChargeTry=x.ChargeTry,
                    //ChargeStatus=x.ChargeStatus,
                    //PaymentType=x.PaymentType,
                    //TripStartTime=x.TripStartTime,
                    //TripEndTime=x.TripEndTime,
                    //Error=x.Error,
                    //ConsumerSource=x.ConsumerSource,
                    //ConsumerDevice=x.ConsumerDevice,
                    //ConsumerRef=x.ConsumerRef,
                    //DriverName=x.DriverName,
                    //DriverID=x.DriverID,
                    //PromisedETA=x.PromisedETA,
                    //DriverInfoID=x.DriverInfoID,
                    //AppendTime=x.AppendTime,
                    //van_trip=new van_trip{
                    //    Id=y.Id,
                    //    FirstName=y.FirstName,
                    //    LastName=y.LastName,
                    //    PhoneNumber=y.PhoneNumber,
                    //    FleetId=y.FleetId,
                    //    ServiceAPIId=y.ServiceAPIId,
                    //    TripType=y.TripType,
                    //    APIInformation=y.APIInformation,
                    //    CabNumber=y.CabNumber,
                    //    MinutesAway=y.MinutesAway,
                    //    Expires=y.Expires,
                    //    NumberOfPassenger=y.NumberOfPassenger,
                    //    DriverNotes=y.DriverNotes,
                    //    PickupFlatRateZone=y.PickupFlatRateZone,
                    //    DropOffFlatRateZone=y.DropOffFlatRateZone,
                    //    AccountNumber=y.AccountNumber,
                    //    PickupAddressType=y.PickupAddressType,
                    //    PickupLongitude=y.PickupLongitude,
                    //    PickupLatitude=y.PickupLatitude,
                    //    PickupBuilding=y.PickupBuilding,
                    //    PickupStreetNo=y.PickupStreetNo,
                    //    PickupStreetName=y.PickupStreetName,
                    //    PickupStreetType=y.PickupStreetType,
                    //    PickupAptNo=y.PickupAptNo,
                    //    PickupCity=y.PickupCity,
                    //    PickupStateCode=y.PickupStateCode,
                    //    PickupZipCode=y.PickupZipCode,
                    //    PickupCountryCode=y.PickupCountryCode,
                    //    PickupFullAddress=y.PickupFullAddress,
                    //    PickupAirport=y.PickupAirport,
                    //    DropOffAddressType=y.DropOffAddressType,
                    //    DropOffLongitude=y.DropOffLongitude,
                    //    DropOffLatitude=y.DropOffLatitude,
                    //    DropOffBuilding=y.DropOffBuilding,
                    //    DropOffStreetNo=y.DropOffStreetNo,
                    //    DropOffStreetName=y.DropOffStreetName,
                    //    DropOffStreetType=y.DropOffStreetType,
                    //    DropOffAptNo=y.DropOffAptNo,
                    //    DropOffCity=y.DropOffCity,
                    //    DropOffStateCode=y.DropOffStateCode,
                    //    DropOffZipCode=y.DropOffZipCode,
                    //    DropOffCountryCode=y.DropOffCountryCode,
                    //    DropOffFullAddress=y.DropOffFullAddress,
                    //    DropOffAirport=y.DropOffAirport,
                    //    DispatchTripId=y.DispatchTripId,
                    //    RemarkForPickup=y.RemarkForPickup,
                    //    RemarkForDropOff=y.RemarkForDropOff,
                    //    wheelchairAccessible=y.wheelchairAccessible,
                    //    NumOfTry=y.NumOfTry
                    //}




                }
                return v;
            }

            /// <summary>
            /// select all synonyms of a word
            /// </summary>
            /// <param name="word"></param>
            /// <returns></returns>
            public List<string> GetSynonyms(string word)
            {
                using (var db = new VTODEntities())
                {
                    return db.vtod_synonym.Where(m => m.Key == word).Select(m => m.Value).ToList();
                }
            }

            #endregion

            #region Private method

            private void GetPickMeupOption(van_fleet f, out bool PickMeUpNow, out bool PickMeUpLater)
            {
                PickMeUpNow = true;
                PickMeUpLater = true;

                #region Get Pick me up value
                if (f.PickMeUpNowOption == PickMeUpNowOptionConst.Anytime)
                {
                    PickMeUpNow = true;
                    PickMeUpLater = true;
                }
                else if (f.PickMeUpNowOption == PickMeUpNowOptionConst.DontPickMeUp)
                {
                    PickMeUpNow = false;
                    PickMeUpLater = false;
                }
                else if (f.PickMeUpNowOption == PickMeUpNowOptionConst.OnlyNow)
                {
                    PickMeUpNow = true;
                    PickMeUpLater = false;
                }
                else if (f.PickMeUpNowOption == PickMeUpNowOptionConst.OnlyFuture)
                {
                    PickMeUpNow = false;
                    PickMeUpLater = true;
                }
                #endregion


            }

            private bool CheckPMUL(van_fleet fleet, DateTime? PickUpDate)
            {
                bool result = true;
                try
                {
                    using (VTODEntities context = new VTODEntities())
                    {

                        van_fleet_PMUL vanPmulObj = context.van_fleet_PMUL.Where(p => p.FleetId == fleet.Id).FirstOrDefault();
                        if (vanPmulObj != null)
                        {
                            var puTime = PickUpDate;
                            if (puTime >= vanPmulObj.PickUpLaterStartDate && puTime <= vanPmulObj.PickUpLaterEndDate)
                            {
                                result = false;
                            }
                        }
                    }
                }
                catch
                {
                }
                return result;
            }

            private bool CheckFastMeterPreviousTripStatus(long tripId)
            {
                bool hasPreviousFastMeter = false;

                using (VTODEntities context = new VTODEntities())
                {

                    if (context.van_trip_status.Where(x => x.TripID == tripId && x.Status == "FastMeter").Any())
                    {
                        hasPreviousFastMeter = true;
                    }
                    else
                    {
                        hasPreviousFastMeter = false;
                    }

                }

                return hasPreviousFastMeter;
            }

            private string ExtractStreetType(string input)
            {
                string[] sep = { " " };
                string result = "";
                if (!string.IsNullOrWhiteSpace(input))
                {
                    result = input.Split(sep, StringSplitOptions.RemoveEmptyEntries).LastOrDefault();
                }
                return result;
            }

            private string ExtractForceZone(string forceZone, Map.DTO.Address address)
            {
                string result;
                int forceZoneNumber;

                if (int.TryParse(forceZone, out forceZoneNumber))
                {
                    //Digits
                    result = forceZoneNumber.ToString();
                }
                else
                {
                    //GPX
                    XDocument doc = XDocument.Parse(forceZone);
                    MapService map = new MapService();
                    result = map.FindZone(address.Geolocation, doc).Name;
                }

                return result;
            }

            private string GetAddressString(Address ad)
            {
                string pickupAddressStr = string.Format("{0} {1} {2} {3} {4} {5}", ad.StreetNmbr, ad.AddressLine, ad.CityName, ad.PostalCode, ad.StateProv.StateCode, ad.CountryName.Code);
                return pickupAddressStr;
            }

            private Map.DTO.Address ConvertAddress(Pickup_Dropoff_Stop stop)
            {
                Map.DTO.Address address = new Map.DTO.Address();
                address.AddressName = "";
                address.ApartmentNo = stop.Address.BldgRoom;
                address.City = stop.Address.CityName;

                //hard code
                //1. USA--> US
                //2. empty --> US
                if ((stop.Address.CountryName.Code.Equals("USA")) || (string.IsNullOrWhiteSpace(stop.Address.CountryName.Code)))
                {
                    address.Country = "US";
                }
                else
                {
                    address.Country = stop.Address.CountryName.Code;
                }

                //address.County = "";
                address.FullAddress = string.Format("{0} {1} {2} {3} {4} {5}", stop.Address.StreetNmbr, stop.Address.AddressLine, stop.Address.CityName, stop.Address.PostalCode, stop.Address.StateProv.StateCode, stop.Address.CountryName.Code);
                address.FullStreet = stop.Address.AddressLine;
                decimal longtitude = 0;
                decimal latitude = 0;
                if (decimal.TryParse(stop.Address.Latitude, out latitude) && decimal.TryParse(stop.Address.Longitude, out longtitude))
                {
                    address.Geolocation = new Map.DTO.Geolocation { Latitude = latitude, Longitude = longtitude };
                }
                //else {
                //	throw new Exception(Messages.van_Common_InvalidReservationServiceLocationInLatAndLong);
                //}
                address.SimpleAddress = null;


                address.State = ConvertUSStateCode(stop.Address.StateProv.StateCode);


                address.Street = stop.Address.AddressLine;
                address.StreetNo = stop.Address.StreetNmbr;
                address.ZipCode = stop.Address.PostalCode;

                if (!string.IsNullOrWhiteSpace(stop.Address.LocationName))
                {
                    address.AddressName = stop.Address.LocationName;
                }


                return address;
            }

            private string ConvertUSStateCode(string name)
            {
                string code;

                using (VTODEntities context = new VTODEntities())
                {
                    code = context.vtod_usstatecode.Where(x => x.Name == name).Select(x => x.Code).FirstOrDefault();
                    if (string.IsNullOrWhiteSpace(code))
                    {
                        code = name;
                    }
                }
                return code;
            }

            private bool GetAccountNumber(OTA_GroundBookRQ request, long fleetId, int userId, out string accountNumber)
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();
                accountNumber = null;
                bool result = false;

                using (VTODEntities context = new VTODEntities())
                {
                    accountNumber = context.van_fleet_user.Where(x => x.FleetId == fleetId && x.UserId == fleetId).Select(x => x.AccountNumber).FirstOrDefault();
                }

                sw.Stop();
                logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
                return result;
            }

            private bool LookupAddress(double longtitude, double latutude, out Map.DTO.Address returnAddress)
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();
                bool result = false;
                returnAddress = null;
                string errorMessage = "";
                MapService ms = new MapService();
                Map.DTO.Geolocation location = new Map.DTO.Geolocation { Longitude = decimal.Parse(longtitude.ToString()), Latitude = decimal.Parse(latutude.ToString()) };
                var addresses = ms.GetAddressesFromGeoLocation(location, out errorMessage);
                if (string.IsNullOrWhiteSpace(errorMessage))
                {
                    if (addresses.Count > 1)
                    {
                        logger.WarnFormat("Multiple addresses found. Source longtitude={0}, latitude={1}", longtitude, latutude);
                        logger.WarnFormat("Pickup the first one address= {0}", addresses.FirstOrDefault().FullAddress);
                    }
                    else
                    {
                        logger.InfoFormat("Pickup the first one address= {0}", addresses.FirstOrDefault().FullAddress);
                    }
                    returnAddress = addresses.FirstOrDefault();

                    //change Zip Code
                    if (returnAddress.Country == "US")
                    {
                        returnAddress.ZipCode = returnAddress.ZipCode.Substring(0, 5);
                    }
                    if (returnAddress.Country == "CA")
                    {
                        returnAddress.ZipCode = returnAddress.ZipCode.Substring(0, 6);
                    }
                    result = true;
                }
                else
                {
                    logger.ErrorFormat("Unable to look up address. Source longtitude={0}, latitude={1}", longtitude, latutude);
                    result = false;
                }

                sw.Stop();
                logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
                return result;
            }

            private bool LookupAddress(Pickup_Dropoff_Stop stop, string sourceAddress, out Map.DTO.Address returnAddress)
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();
                bool result = false;
                returnAddress = null;
                string errorMessage = "";
                MapService ms = new MapService();
                var addresses = ms.GetAddresses(sourceAddress, out errorMessage);
                if (string.IsNullOrWhiteSpace(errorMessage))
                {
                    if (addresses.Count > 1)
                    {
                        logger.WarnFormat("Multiple addresses found. Source address = {0}", sourceAddress);
                        logger.WarnFormat("Pickup the first one address= {0}", addresses.FirstOrDefault().FullAddress);
                    }
                    else
                    {
                        logger.InfoFormat("Pickup the first one address= {0}", addresses.FirstOrDefault().FullAddress);
                    }
                    returnAddress = addresses.FirstOrDefault();

                    //change Zip Code
                    if (!string.IsNullOrWhiteSpace(returnAddress.ZipCode))
                    {
                        if (returnAddress.Country == "US")
                        {
                            returnAddress.ZipCode = returnAddress.ZipCode.Substring(0, 5);
                        }
                        if (returnAddress.Country == "CA")
                        {
                            returnAddress.ZipCode = returnAddress.ZipCode.Substring(0, 6);
                        }
                    }

                    returnAddress.ApartmentNo = stop.Address.BldgRoom;

                    result = true;
                }
                else
                {
                    logger.ErrorFormat("Unable to look up address. Source address = {0}", sourceAddress);
                    result = false;
                }

                sw.Stop();
                logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
                return result;
            }

            private van_fleet_api_reference GetvanFleetServiceAPIPreference(long fleetID, string functionName)
            {
                van_fleet_api_reference preference = new van_fleet_api_reference();
                using (VTODEntities context = new VTODEntities())
                {
                    preference = context.van_fleet_api_reference.Where(x => x.FleetId == fleetID && x.Name == functionName).Select(x => x).FirstOrDefault();
                }
                return preference;
            }

            private bool GetFleet(string pickupAddressType, Map.DTO.Address address, string airport, out van_fleet fleet)
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();
                bool result = false;
                fleet = null;

                using (VTODEntities context = new VTODEntities())
                {
                    if (pickupAddressType == AddressType.Address)
                    {
                        #region Priority #1: find by Coordinate
                        //by Coordinate (Lat/Long)
                        if (fleet == null)
                            if (address != null && address.Geolocation != null && address.Geolocation.Latitude != 0 && address.Geolocation.Longitude != 0)
                            {
                                fleet = context.SP_van_GetFleetByCoordinate2(address.Geolocation.Latitude, address.Geolocation.Longitude).FirstOrDefault();

                            }
                        #endregion

                        #region Priority #2: find by zip code
                        //by Zip code
                        if (fleet == null)
                        {
                            fleet = context.SP_van_GetFleetByZipCountry2(address.ZipCode, address.Country).Select(x => x).FirstOrDefault();

                        }
                        #endregion

                        #region Priority #3: find by city
                        //by City name, State, Country
                        if (fleet == null)
                        {
                            fleet = context.SP_van_GetFleetByCityStateCountry2(address.City, address.State, address.Country).Select(x => x).FirstOrDefault();

                        }
                        #endregion
                    }
                    else if (pickupAddressType == AddressType.Airport)
                    {
                        #region Priority #4 by Airport (for this version, landmark is just Airport)
                        //by airport
                        fleet = context.SP_van_GetFleetByAirport2(airport).FirstOrDefault();

                        #endregion

                    }

                    if (fleet == null)
                    {
                        result = false;
                        string error = string.Format("Unable to find correct fleet with this address");
                        throw VtodException.CreateException(ExceptionType.Van, 1011);// new ValidationException(error);
                    }
                    else
                    {
                        result = true;
                    }
                }
                sw.Stop();
                logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
                return result;
            }

            private bool GetFleet(long fleetId, out van_fleet fleet)
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();
                bool result = false;
                fleet = null;
                using (VTODEntities context = new VTODEntities())
                {
                    fleet = context.van_fleet.Where(x => x.Id == fleetId).Select(x => x).FirstOrDefault();
                    if (fleet != null)
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
                sw.Stop();
                logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
                return result;
            }

            internal bool GetFleet(string pickupAddressType, Address address, string airport, out van_fleet fleet)
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();
                fleet = null;

                using (VTODEntities context = new VTODEntities())
                {
                    if (pickupAddressType == AddressType.Address)
                    {
                        #region Priority #1: find by Coordinate
                        //by Coordinate (Lat/Long)
                        if (address != null && !string.IsNullOrWhiteSpace(address.Latitude) && !string.IsNullOrWhiteSpace(address.Longitude))
                        {
                            fleet = context.SP_van_GetFleetByCoordinate2(decimal.Parse(address.Latitude), decimal.Parse(address.Longitude)).FirstOrDefault();
                        }
                        #endregion

                        #region Priority #2: find by zip code
                        //by Zip code
                        if (address != null && fleet == null && address.CountryName != null)
                        {
                            fleet = context.SP_van_GetFleetByZipCountry2(address.PostalCode, address.CountryName.Code).Select(x => x).FirstOrDefault();
                        }
                        #endregion

                        #region Priority #3: find by city
                        //by City name, State, Country
                        if (address != null && fleet == null && address.StateProv != null && address.CountryName != null)
                        {
                            fleet = context.SP_van_GetFleetByCityStateCountry2(address.CityName, address.StateProv.StateCode, address.CountryName.Code).Select(x => x).FirstOrDefault();
                        }
                        #endregion
                    }
                    else if (pickupAddressType == AddressType.Airport)
                    {
                        #region Priority #4 by Airport (for this version, landmark is just Airport)
                        //by airport
                        fleet = context.SP_van_GetFleetByAirport2(airport).FirstOrDefault();

                        #endregion
                    }

                    if (fleet == null)
                    {
                        //string error = string.Format("Unable to find correct fleet with this address");
                        throw VtodException.CreateException(ExceptionType.Van, 1011);
                    }
                }
                sw.Stop();
                logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
                return true;
            }

            private bool GetvanFleetUser(long fleetId, string userName, out van_fleet_user user)
            {

                Stopwatch sw = new Stopwatch();
                sw.Start();
                bool result = false;
                user = null;
                using (VTODEntities context = new VTODEntities())
                {
                    //user = context.SP_van_GetFleet_User(fleetId, userName).Select(x => x).FirstOrDefault();
                    user = context.SP_van_GetFleet_User2(fleetId, userName).Select(x => x).FirstOrDefault();
                    if (user != null)
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
                sw.Stop();
                logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
                return result;
            }

            public my_aspnet_users GetUser(long Id)
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();
                my_aspnet_users user = null;
                using (VTODEntities context = new VTODEntities())
                {
                    user = context.my_aspnet_users.Where(x => x.id == Id).Select(x => x).FirstOrDefault();
                }
                sw.Stop();
                logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
                return user;
            }

            public List<string> GetRemarks(string userName, long fleetID)
            {
                List<string> remarksList = new List<string>();
                Stopwatch sw = new Stopwatch();
                sw.Start();
                SP_Van_GetRemarksForPickUp_DropOff_Result remarks = null;
                using (VTODEntities context = new VTODEntities())
                {
                    remarks = context.SP_Van_GetRemarksForPickUp_DropOff(userName, fleetID).FirstOrDefault();
                }
                sw.Stop();
                logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
                remarksList.Add(remarks.RemarkForPickUpCash);
                remarksList.Add(remarks.RemarkForDropOffCash);
                remarksList.Add(remarks.RemarkForPickUpCreditCard);
                remarksList.Add(remarks.RemarkForDropOffCreditCard);
                return remarksList.ToList<string>();
            }

            private van_fleet_zone GetFleetZone(long fleetId, Map.DTO.Address address)
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();

                van_fleet_zone result = null;
                using (VTODEntities context = new VTODEntities())
                {
                    try
                    {
                        //find fleet zone by geolocation
                        if (address != null && address.Geolocation != null && address.Geolocation.Latitude != 0 && address.Geolocation.Longitude != 0)
                        {
                            result = context.SP_van_GetFleetZone(fleetId, address.Geolocation.Latitude, address.Geolocation.Longitude).FirstOrDefault();
                        }

                        //find flett zone by zip
                        if (result == null)
                        {
                            result = context.SP_van_GetFlletZoneByZip(fleetId, address.ZipCode.ToString()).FirstOrDefault();
                        }

                        //find flett zone by city
                        if (result == null)
                        {
                            result = context.SP_van_GetFlletZoneByCity(fleetId, address.City.ToString()).FirstOrDefault();
                        }
                    }
                    catch { }
                }
                sw.Stop();
                logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
                return result;
            }

            private van_fleet_zone GetFleetZoneByAirport(long fleetId, string airportName)
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();

                van_fleet_zone result = null;
                using (VTODEntities context = new VTODEntities())
                {
                    try
                    {
                        if (fleetId > 0 && !string.IsNullOrWhiteSpace(airportName))
                        {
                            result = context.SP_van_GetFleetZoneByAirport(fleetId, airportName).FirstOrDefault();
                        }
                    }
                    catch { }
                }
                sw.Stop();
                logger.DebugFormat("Duration:{0} ms.", sw.ElapsedMilliseconds);
                return result;
            }

            private string ExtractPhoneNumber(Telephone phone)
            {
                string result = string.Format("{0}{1}{2}", phone.CountryAccessCode, phone.AreaCityCode, phone.PhoneNumber).Trim();
                result = ExtractDigits(result);
                return result;
            }

            private bool IsAbleToBookTrip(VanBookingParameter tbp)
            {
                bool result = false;

                using (VTODEntities context = new VTODEntities())
                {
                    #region new logic
                    int blockInterval = -10;
                    blockInterval = tbp.Fleet.RestrictBookingMins.HasValue ? tbp.Fleet.RestrictBookingMins.Value : 10;

                    int? duplicateTrips = null;

                    if (tbp.Fleet.RestrictBookingOption == RestrictBookingOption.On)
                    {
                        if (tbp.PickupAddressType == AddressType.Address)
                        {
                            //by addresss
                            duplicateTrips = context.SP_van_IsDuplicateBookingByAddress(tbp.FirstName, tbp.LastName, tbp.PhoneNumber, tbp.PickupAddress.FullAddress, tbp.PickupAddress.Street, tbp.PickupAddress.City, tbp.PickupAddress.Country, tbp.PickupDateTime, blockInterval, tbp.MemberID.HasValue ? tbp.MemberID.Value.ToString() : null).FirstOrDefault();
                            //duplicateTrips = context.SP_van_IsDuplicateBookingByAddress(tbp.FirstName, tbp.LastName, tbp.PhoneNumber, tbp.PickupAddress.FullAddress, tbp.PickupAddress.Street, tbp.PickupAddress.City, tbp.PickupAddress.Country, blockedDateTime).FirstOrDefault();
                        }
                        else if (tbp.PickupAddressType == AddressType.Airport)
                        {
                            //by airport
                            duplicateTrips = context.SP_van_IsDuplicateBookingByAirport(tbp.FirstName, tbp.LastName, tbp.PhoneNumber, tbp.PickupAirport, tbp.PickupDateTime, blockInterval, tbp.MemberID.HasValue ? tbp.MemberID.Value.ToString() : null).FirstOrDefault();
                            //duplicateTrips = context.SP_van_IsDuplicateBookingByAirport(tbp.FirstName, tbp.LastName, tbp.PhoneNumber, tbp.PickupAirport, blockInterval).FirstOrDefault();
                        }
                    }
                    else
                    {
                        //bypass duplication check
                        duplicateTrips = 0;
                        logger.Warn("Bypass duplication check");
                    }

                    if (duplicateTrips.HasValue)
                    {
                        if (duplicateTrips.Value > 0)
                        {
                            result = false;
                            logger.Warn("Found duplicated trips");
                        }
                        else
                        {
                            result = true;
                            logger.Info("No duplicated trip");
                        }
                    }
                    else
                    {
                        string error = string.Format("Unable to look up check duplicated trip");
                        throw new Exception(error);
                    }

                    #endregion
                }
                return result;
            }

            private long GetServiceAPIId(long fleetId, string action)
            {
                long serviceAPIId = -1;

                try
                {
                    serviceAPIId = GetvanFleetServiceAPIPreference(fleetId, action).ServiceAPIId;
                }
                catch
                {
                    var vtodException = VtodException.CreateException(ExceptionType.Van, 1011);
                    logger.Warn(vtodException.ExceptionMessage.Message);
                    throw vtodException;// new ValidationException(Messages.van_Common_UnableToFindFleetByAddressOrAirportInfo);

                }
                return serviceAPIId;

            }

            #endregion

            #region Properties
            public TrackTime TrackTime { get; set; }
            #endregion
        }
    
}
