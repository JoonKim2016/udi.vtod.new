﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDI.VTOD.Common.DTO.OTA;
using UDI.VTOD.Domain.ECar.Aleph;

namespace UDI.VTOD.Helper
{
    public static class Converter
    {
        public static Address ToAddress(this UDI.Map.DTO.Address input, decimal? latitude, decimal? longitude)
        {
            UDI.VTOD.Common.DTO.OTA.Address result = null;

            if (input != null)
            {
                result = new Common.DTO.OTA.Address();
                result.AddressLine = input.Street;
                result.StreetNmbr = input.StreetNo;
                result.CityName = input.City;
                result.PostalCode = input.ZipCode;
                result.StateProv = new StateProv { StateCode = input.State };
                result.CountryName = new CountryName { Code = input.Country };
                result.Latitude = latitude.HasValue ? latitude.Value.ToString() : input.Geolocation.Latitude.ToString();
                result.Longitude = longitude.HasValue ? longitude.Value.ToString() : input.Geolocation.Longitude.ToString();
            }
            return result;
        }

        public static PaymentCard ToPaymentCard(this AlephCoporateCreditCardInfo alephResult)
        {
            var paymentCard = new PaymentCard();
            DateTime expireDate;
            if (!string.IsNullOrEmpty(alephResult.expiration_year) && !string.IsNullOrEmpty(alephResult.expiration_month))
            {
                int expirationYear = Convert.ToInt32(alephResult.expiration_year);
                int expirationMonth = Convert.ToInt32(alephResult.expiration_month);

                int lastDay = DateTime.DaysInMonth(expirationYear, expirationMonth);
                expireDate = new DateTime(expirationYear, expirationMonth, lastDay);
                paymentCard.ExpireDate = expireDate.ToShortDateString();
            }
            else
            {
                paymentCard.ExpireDate = null;
            }
            paymentCard.Nickname = alephResult.nick_name;

            paymentCard.CardNumber = new CardNumber_SeriesCode();
            paymentCard.CardNumber.EncryptedValue = alephResult.payment_method_token;
            paymentCard.CardNumber.LastFourDigit = alephResult.last_four;

            paymentCard.CardType = new CardType();
            paymentCard.CardType.Value = alephResult.short_card_type;
            paymentCard.CardType.Description = alephResult.card_type;

            paymentCard.TPA_Extensions = new TPA_Extensions();
            paymentCard.TPA_Extensions.IsDefault = alephResult.is_primary_card;
            paymentCard.TPA_Extensions.IsActive = alephResult.is_expired_card;
            paymentCard.TPA_Extensions.IsBlockedCard = alephResult.is_blocked_card;
            paymentCard.TPA_Extensions.IsCompanyCard = alephResult.is_company_card;
            paymentCard.TPA_Extensions.IsVerified = alephResult.is_verified_card;

            return paymentCard;
        }

        public static List<CorporateCompanyRequirement> ToCorporateCompanyRequirements(this AlephGetPaymentMethodRS alephResult)
        {            
            var CorporateCompanyRequirements = new List<CorporateCompanyRequirement>();
            foreach (var corporationPaymentMethods in alephResult.CorporationPaymentMethods)
            {
                if (corporationPaymentMethods != null)
                {
                    var corporateCompanyRequirement = new CorporateCompanyRequirement
                    {
                        CorporateCompanyName = corporationPaymentMethods.corporation_name,
                        CorporateCompanyAccounts = new List<CorporationCompanyAccountRequirements>()
                    };
                    CorporateCompanyRequirements.Add(corporateCompanyRequirement);

                    if (corporationPaymentMethods.corporation_accounts != null && corporationPaymentMethods.corporation_accounts.Count > 0)
                    {
                        corporateCompanyRequirement.CorporateCompanyAccounts = new List<CorporationCompanyAccountRequirements>();
                        foreach (
                            var corporationAccount in corporationPaymentMethods.corporation_accounts)
                            {
                            if (corporationAccount == null) continue;
                            try
                              {
                                corporationAccount.payment_methods.Remove("Credit Card");
                              }
                            catch (Exception ex)
                             {
                             }
                            var ar = new CorporationCompanyAccountRequirements
                            {
                                IsCreditCardEligible = corporationAccount.is_credit_card_eligible,
                                AccountName = corporationAccount.account_name,
                                PaymentMethods = corporationAccount.payment_methods,
                                Requirements = null
                            };
                            corporateCompanyRequirement.CorporateCompanyAccounts.Add(ar);
                        }
                    }
                }
            }            
            return CorporateCompanyRequirements;
        }
        
        public static AlephPaymentCard ToAlephPaymentCardInfo(this List<NameValue> specialInput)
        {
            var alephPaymentCardInfo = new AlephPaymentCard();

            if (specialInput != null)
            {
                var corporateProviderID = specialInput.Where(x => "CorporateProviderID".Equals(x.Name, StringComparison.OrdinalIgnoreCase)).SingleOrDefault();
                if (corporateProviderID != null)
                {
                    alephPaymentCardInfo.CorporateProviderID = corporateProviderID.Value;
                }

                var corporateProfileID = specialInput.Where(x => "CorporateProfileID".Equals(x.Name, StringComparison.OrdinalIgnoreCase)).SingleOrDefault();
                if (corporateProfileID != null)
                {
                    alephPaymentCardInfo.CorporateProfileID = corporateProfileID.Value;
                }

                var corporateEmail = specialInput.Where(x => "CorporateEmail".Equals(x.Name, StringComparison.OrdinalIgnoreCase)).SingleOrDefault();
                if (corporateEmail != null)
                {
                    alephPaymentCardInfo.CorporateEmail = corporateEmail.Value;
                }

                var corporatePaymentNonce = specialInput.Where(x => "CorporatePaymentNonce".Equals(x.Name, StringComparison.OrdinalIgnoreCase)).SingleOrDefault();
                if (corporatePaymentNonce != null)
                {
                    alephPaymentCardInfo.CorporatePaymentNonce = corporatePaymentNonce.Value;
                }

                var account = specialInput.Where(x => "Account".Equals(x.Name, StringComparison.OrdinalIgnoreCase)).SingleOrDefault();
                if (account != null)
                {
                    alephPaymentCardInfo.Account = account.Value;
                }

                var corporatePaymentMethodToken = specialInput.Where(x => "CorporatePaymentMethodToken".Equals(x.Name, StringComparison.OrdinalIgnoreCase)).SingleOrDefault();
                if (corporatePaymentMethodToken != null)
                {
                    alephPaymentCardInfo.CorporatePaymentMethodToken = corporatePaymentMethodToken.Value;
                }


                var corporateName = specialInput.Where(x => "CorporateName".Equals(x.Name, StringComparison.OrdinalIgnoreCase)).SingleOrDefault();
                if (corporateName != null)
                {
                    alephPaymentCardInfo.CorporateName = corporateName.Value;
                }

            }

            return alephPaymentCardInfo;
        }
    }
}
    
